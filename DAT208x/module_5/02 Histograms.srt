0
00:00:01,415 --> 00:00:04,100
In this video, I'll introduce you to the histogram.

1
00:00:04,100 --> 00:00:06,314
The histogram is a type of visualization that's

2
00:00:06,314 --> 00:00:08,531
particularly useful to explore your data sets.

3
00:00:08,531 --> 00:00:12,044
It can help you to get an idea about the distribution of your

4
00:00:12,044 --> 00:00:13,550
variables.

5
00:00:13,550 --> 00:00:17,263
To see how it works, imagine 12 values between 0 and 6.

6
00:00:17,263 --> 00:00:18,860
I've put them along a number line here.

7
00:00:19,910 --> 00:00:22,159
To build a histogram for these values,

8
00:00:22,159 --> 00:00:25,436
you can divide the line into equal chunks, called bins.

9
00:00:25,436 --> 00:00:29,120
Suppose we go for three bins that each have a width of 2.

10
00:00:29,120 --> 00:00:32,900
Next, we count how many data points sit inside each bin.

11
00:00:32,900 --> 00:00:34,460
There's 4 variables in the first bin,

12
00:00:34,460 --> 00:00:36,840
6 variables in the second bin, and 2 in the third bin.

13
00:00:37,890 --> 00:00:40,570
Finally, you draw a bar for each bin.

14
00:00:40,570 --> 00:00:42,572
The height of the bar corresponds to the number of

15
00:00:42,572 --> 00:00:44,069
data points that fall in this bin.

16
00:00:45,950 --> 00:00:49,090
Result is a histogram, which gives us a nice overview

17
00:00:49,090 --> 00:00:51,630
on how to develop values or distribute it.

18
00:00:51,630 --> 00:00:54,380
Most values are in the middle, and there are more values below

19
00:00:54,380 --> 00:00:56,180
two than there are values above four.

20
00:00:57,830 --> 00:01:01,200
Of course, also Matplotlib is able to build histograms.

21
00:01:01,200 --> 00:01:04,030
As before, you should start by importing the pyplot

22
00:01:04,030 --> 00:01:05,660
package that's inside Matplotlib.

23
00:01:06,760 --> 00:01:08,920
Next you can use the hist function.

24
00:01:08,920 --> 00:01:10,230
Let's open up its documentation.

25
00:01:11,340 --> 00:01:13,273
There's a bunch of arguments you can specify.

26
00:01:13,273 --> 00:01:16,267
But the first two here are the most important ones.

27
00:01:16,267 --> 00:01:18,565
X should be a list of values that you want to build

28
00:01:18,565 --> 00:01:20,270
a histogram for.

29
00:01:20,270 --> 00:01:22,320
You can use the second argument, bins,

30
00:01:22,320 --> 00:01:25,150
to tell Python in how many bins the data should be divided.

31
00:01:26,280 --> 00:01:27,450
Based on this number,

32
00:01:27,450 --> 00:01:29,950
hist will automatically find appropriate boundaries for

33
00:01:29,950 --> 00:01:32,970
all bins and calculate how many values are in each one.

34
00:01:34,770 --> 00:01:37,310
So to generate the histogram that you've seen before,

35
00:01:37,310 --> 00:01:39,461
let's start by building a list with the 12 values.

36
00:01:40,510 --> 00:01:43,092
Next, you simply call hist on this list and

37
00:01:43,092 --> 00:01:45,187
specify that you want three bins.

38
00:01:45,187 --> 00:01:47,052
If you finally call the show function,

39
00:01:47,052 --> 00:01:48,359
a nice histogram results.

40
00:01:50,420 --> 00:01:53,360
Histograms are really useful to give a bigger picture.

41
00:01:53,360 --> 00:01:55,780
As an example, have a look at this histogram

42
00:01:55,780 --> 00:01:59,090
that shows the age distribution for people in Europe.

43
00:01:59,090 --> 00:02:01,780
You can very clearly see where the bins are largest.

44
00:02:01,780 --> 00:02:03,120
This is the baby boom generation.

45
00:02:04,310 --> 00:02:05,472
This is very recent data, but

46
00:02:05,472 --> 00:02:07,380
what do you think will have changed in 2050?

47
00:02:07,380 --> 00:02:09,691
Let's have a look at a similar histogram.

48
00:02:09,691 --> 00:02:12,960
The distribution is flatter this time.

49
00:02:12,960 --> 00:02:15,660
And the baby boom generation has gotten older.

50
00:02:15,660 --> 00:02:18,560
With the blink of an eye, you can easily see how demographics

51
00:02:18,560 --> 00:02:20,030
can change over time.

52
00:02:20,030 --> 00:02:22,020
That's the true power of histograms at work here.

53
00:02:23,130 --> 00:02:25,760
Now head over to the exercises to experiment with histograms

54
00:02:25,760 --> 00:02:26,280
yourself.

