0
00:00:01,790 --> 00:00:06,040
After you&#39;ve created your very own Python list, you might wonder how you can access

1
00:00:06,040 --> 00:00:13,170
information in the list. Python uses the index to do this. Have a look at `fam` again here.

2
00:00:13,170 --> 00:00:18,930
The first element in the list has index 0, the second element has index 1, and so on.

3
00:00:18,930 --> 00:00:24,480
Suppose that you want to select the height of emma, the float 1.68. It&#39;s the fourth element,

4
00:00:24,480 --> 00:00:29,520
so it has index 3. To select it, you use 3 inside square brackets:

5
00:00:29,520 --> 00:00:33,830
Similarly, to select the string &quot;dad&quot; from the list, which is the sevent element in the

6
00:00:33,830 --> 00:00:36,470
list, you&#39;ll need to put the index 6 inside square brackets:

7
00:00:36,470 --> 00:00:41,960
You can also count backwards, using negative indexes. This is useful if you want to get

8
00:00:41,960 --> 00:00:46,230
some elements at the end of your list. To get your dad&#39;s height, for example, you&#39;ll

9
00:00:46,230 --> 00:00:52,600
need the index -1. These are the negative indexes for all list elements.

10
00:00:52,600 --> 00:00:58,210
This means that this line and this line, return the same result:

11
00:00:58,210 --> 00:01:02,260
Apart from indexing, there&#39;s also something called slicing, which allows you to select

12
00:01:02,260 --> 00:01:07,240
multiple elements from a list, thus creating a new list. You can do this by specifying

13
00:01:07,240 --> 00:01:12,110
a range, using a colon. Let&#39;s first have another look at the list, and then try this piece

14
00:01:12,110 --> 00:01:13,830
of code:

15
00:01:13,830 --> 00:01:18,790
Can you guess what it&#39;ll return? A list with the the float 1.68, the string &quot;mom&quot;, and

16
00:01:18,790 --> 00:01:23,940
the float 1.71, corresponding to the 4th, 5th and 6th element in the list maybe? Let&#39;s

17
00:01:23,940 --> 00:01:25,960
see what the output is.

18
00:01:25,960 --> 00:01:31,260
Apparently, only the elements with index 3 and 4, get returned. The element with index

19
00:01:31,260 --> 00:01:37,220
5 is not included. In general, this is the syntax: the index you specify before the colon,

20
00:01:37,220 --> 00:01:41,720
so where the slice starts, is included, while the index you specify after the colon, where

21
00:01:41,720 --> 00:01:43,690
the slice ends, is not.

22
00:01:43,690 --> 00:01:48,799
With this in mind, can you tell what this call will return? &lt;PAUSE&gt;

23
00:01:48,799 --> 00:01:52,659
You probably guessed correctly that this call gives you a list with three elements, corresponding

24
00:01:52,659 --> 00:01:57,619
to the elements with index 1, 2 and 3 of the fam list.

25
00:01:57,619 --> 00:02:01,840
You can also choose to just leave out the index before or after the colon. If you leave

26
00:02:01,840 --> 00:02:05,990
out the index where the slice should begin, you&#39;re telling Python to start the slice from

27
00:02:05,990 --> 00:02:09,879
index 0, like this example.

28
00:02:09,878 --> 00:02:13,629
If you leave out the index where the slice should end, you include all elements up to

29
00:02:13,629 --> 00:02:17,969
and including the last element in the list, like here:

30
00:02:17,969 --> 00:02:21,810
Now it&#39;s time to head over to the exercises, where you will continue to work on the list

31
00:02:21,810 --> 00:02:26,609
you&#39;ve created yourself before. You&#39;ll use different subsetting methods to get exactly

32
00:02:26,609 --> 00:02:28,049
the piece of information you need!

