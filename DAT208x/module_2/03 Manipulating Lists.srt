0
00:00:02,029 --> 00:00:07,250
After creation and subsetting, the final piece of the Python lists puzzle is manipulation,

1
00:00:07,250 --> 00:00:11,240
so ways to change elements in your list, or to add elements to and remove elements from

2
00:00:11,240 --> 00:00:13,150
your list.

3
00:00:13,150 --> 00:00:17,110
Changing list elements is pretty straightforward. You use the same square brackets that we&#39;ve

4
00:00:17,110 --> 00:00:22,860
used to subset lists, and then assign new elements to it using the equals sign. Suppose

5
00:00:22,860 --> 00:00:27,470
that after another look at `fam`, you realize that your dad&#39;s height is not up to date anymore

6
00:00:27,470 --> 00:00:33,790
as he&#39;s shrinking with age. Instead of 1.89 meters, it should be 1.86 meters. To change

7
00:00:33,790 --> 00:00:38,800
this list element, which is at index 7, you can use this line:

8
00:00:38,800 --> 00:00:42,440
If you now check out fam, you&#39;ll see that the value is updated:

9
00:00:42,440 --> 00:00:49,280
You can even change an entire list slice at once. To change the elements &quot;liz&quot; and 1.73,

10
00:00:49,280 --> 00:00:55,219
you access the first two elements with zero colon 2, and then assign a new list to it.

11
00:00:55,219 --> 00:00:59,929
Do you still remember how the plus operator was different for strings and integers? Well,

12
00:00:59,929 --> 00:01:04,239
it&#39;s again different for lists. If you use the plus sign with two lists, Python simply

13
00:01:04,239 --> 00:01:08,640
pastes together their contents in a single list. Suppose you want to add your own name

14
00:01:08,640 --> 00:01:11,950
and height to the fam height list. This will do the trick:

15
00:01:11,950 --> 00:01:18,149
Of course, you can also store this new list in a variable, `fam_ext` for example.

16
00:01:18,149 --> 00:01:22,659
Finally, deleting elements from a list is also pretty straightforward, you&#39;ll have to

17
00:01:22,659 --> 00:01:27,829
use `del` here. Take this line, for example, that deletes the element with index 2, so

18
00:01:27,829 --> 00:01:30,299
&quot;emma&quot;, from the list:

19
00:01:30,299 --> 00:01:34,840
If you check out fam now, you&#39;ll see that the &quot;emma&quot; string is gone now. Because you&#39;ve

20
00:01:34,840 --> 00:01:40,240
removed an index, all elements that came after &quot;emma&quot; scooted over by one index. If you again

21
00:01:40,240 --> 00:01:45,649
run the same line, you&#39;re again removing the element at index 2, which is emma&#39;s height,

22
00:01:45,649 --> 00:01:47,700
1.68 centimeters:

23
00:01:47,700 --> 00:01:51,189
Understanding how Python lists actually work behind the scenes becomes pretty important

24
00:01:51,189 --> 00:01:56,100
now. What actually happens when you create a new list, `x`, like this?

25
00:01:56,100 --> 00:02:00,689
Well, in a simplified sense, you&#39;re storing a list in your computer memory, and store

26
00:02:00,689 --> 00:02:06,429
the &#39;address&#39; of that list, so where the list is in your computer memory, in `x`. This means

27
00:02:06,429 --> 00:02:10,500
that `x` does not actually contain all the list elements, it rather contains a reference

28
00:02:10,500 --> 00:02:15,750
to the list. For basic operations, the difference is not that important, but it becomes more

29
00:02:15,750 --> 00:02:20,690
so when you start copying lists. Let me clarify this with an example.

30
00:02:20,690 --> 00:02:25,459
Let&#39;s store the list `x` as a new variable `y`, by simply using the equals sign:

31
00:02:25,459 --> 00:02:30,200
Let&#39;s now change the element with index one in the list `y`, as follows:

32
00:02:30,200 --> 00:02:35,480
The funky thing is that if you now check out `x` again, also here the second element was

33
00:02:35,480 --> 00:02:36,120
changed:

34
00:02:36,120 --> 00:02:39,810
That&#39;s because when you copied x to y with the equals sign, you copied the reference

35
00:02:39,810 --> 00:02:44,780
to the list, not the actual values themselves. When you&#39;re updating an element the list,

36
00:02:44,780 --> 00:02:48,860
though, it&#39;s one and the same list in the computer memory your changing. Both `x` and

37
00:02:48,860 --> 00:02:53,870
`y` point to this list, so the update is visible from both.

38
00:02:53,870 --> 00:02:57,560
If you want to create a list `y` that points to a new list in the memory with the same

39
00:02:57,560 --> 00:03:01,420
variables, you&#39;ll need to use something else than the equals sign. You can use the `list()`

40
00:03:01,420 --> 00:03:05,549
function, like this, or use slicing to select all list elements explicitly.

41
00:03:05,549 --> 00:03:11,909
If you now make a change to the list `y` points to, `x` is not affected:

42
00:03:11,909 --> 00:03:16,319
If this was a bit too much to take in, don&#39;t worry. The exercises will help you understand

43
00:03:16,319 --> 00:03:20,780
list manipulation and the subtle inner workings of lists. I&#39;m sure you&#39;ll do great!

