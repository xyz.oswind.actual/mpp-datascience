0
00:00:01,870 --> 00:00:06,350
By now, you&#39;ve played around with different data types. On the numbers side, there&#39;s the

1
00:00:06,350 --> 00:00:11,450
`float`, to represent a real number, and the `int`, to represent an integer. Next, we also

2
00:00:11,450 --> 00:00:16,330
have `str`, short for string, to represent text in Python, and `bool`, which can be either

3
00:00:16,329 --> 00:00:21,870
`True` or `False`. You can save these values as a variable, like these examples show. Each

4
00:00:21,870 --> 00:00:24,850
variable then represents a _single_ value.

5
00:00:24,850 --> 00:00:29,660
As a data scientist, you&#39;ll often want to work with many data points. If you for example

6
00:00:29,660 --> 00:00:33,610
want to measure the height of everybody in your family, and store this information in

7
00:00:33,610 --> 00:00:36,920
python, it would be inconvenient to create a new python variable for each data point

8
00:00:36,920 --> 00:00:38,079
you collected right?

9
00:00:38,079 --> 00:00:43,670
What you can do instead, is store all this information in a python _list_. You can build

10
00:00:43,670 --> 00:00:48,309
such a list with square brackets. Suppose you asked your two sisters and parents for

11
00:00:48,309 --> 00:00:52,969
their height, in meters. You can build the list as follows:

12
00:00:52,969 --> 00:00:57,620
Of course, also this data structure can be referenced to with a variable. Simply put

13
00:00:57,620 --> 00:01:02,179
the variable name and the equals sign in front, like here:

14
00:01:02,179 --> 00:01:07,440
A list is a way to give a single name to a collection of values. These values, or elements,

15
00:01:07,440 --> 00:01:12,140
can have any type; they can be floats, integer, booleans, strings, but also more advanced

16
00:01:12,140 --> 00:01:14,680
Python types, even lists.

17
00:01:14,680 --> 00:01:19,900
It&#39;s perfectly possible for a list to contain different types. Suppose, for example, that

18
00:01:19,900 --> 00:01:23,590
you want to add the names of your sisters and parents to the list, so that you know

19
00:01:23,590 --> 00:01:29,400
which height belongs to who. You can throw in some strings without issues.

20
00:01:29,400 --> 00:01:34,860
But that&#39;s not all. I just told you that lists can also contain lists themselves. Instead

21
00:01:34,860 --> 00:01:39,400
of putting the strings in between the numbers, you can create little sublists for each member

22
00:01:39,400 --> 00:01:45,320
of the family. One for liz, one for emma and so on. Now, you can tell Python that these

23
00:01:45,320 --> 00:01:50,390
sublists are the elements of another list, that I named fam2: the little lists are wrapped

24
00:01:50,390 --> 00:01:55,360
in square brackets and separated with commas. If you now print out fam2, you see that we

25
00:01:55,360 --> 00:02:00,340
have a list of lists. The main list contains 4 sub lists.

26
00:02:00,340 --> 00:02:04,160
We&#39;re dealing with a new Python type here, next to the strings, booleans, integers and

27
00:02:04,160 --> 00:02:09,950
floats you already know about: the list. These calls show that both `fam` and `fam2` are

28
00:02:09,949 --> 00:02:14,530
lists. Remember that I told you that each type has specific functionality and behavior

29
00:02:14,530 --> 00:02:20,709
associated? Well, for lists, this is also true. Python lists host a bunch of tools to

30
00:02:20,709 --> 00:02:25,379
subset and adapt them. But let&#39;s take this step by step, and have you experiment with

31
00:02:25,379 --> 00:02:26,220
list creation first!

