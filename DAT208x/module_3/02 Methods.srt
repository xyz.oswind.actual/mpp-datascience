0
00:00:01,910 --> 00:00:06,470
Built-in functions are only one part of the Python story. You already know about functions

1
00:00:06,470 --> 00:00:10,710
such as `max()`, to get the maximum of a list, `len()`, to get the length of a list or a

2
00:00:10,710 --> 00:00:15,780
string, and so on. But what about other basic things, such getting the index of a specific

3
00:00:15,780 --> 00:00:20,410
element in the list, or reversing a list? You can look very hard for built-in functions

4
00:00:20,410 --> 00:00:22,519
that do this, but you won&#39;t find them.

5
00:00:22,519 --> 00:00:28,330
In the past exercises, you&#39;ve already created a bunch of variables. Among other Python types,

6
00:00:28,330 --> 00:00:33,660
you&#39;ve created strings, floats and lists, like the ones you see here. Each one of these

7
00:00:33,660 --> 00:00:39,210
values or data structures are so-called Python object. This string is an object, this float

8
00:00:39,210 --> 00:00:44,460
is an object, but this list is also an object. These objects have a specific type, that you

9
00:00:44,460 --> 00:00:49,550
already know: string, float, and list, and of course they represent the values you gave

10
00:00:49,550 --> 00:00:56,000
them, such as &quot;liz&quot;, 1.73 and an entire list. But next to that, Python objects also come

11
00:00:56,000 --> 00:01:00,879
with a bunch of so-called &quot;methods&quot;. You can think of methods as _functions_ that &quot;belong

12
00:01:00,879 --> 00:01:07,000
to&quot; Python objects. A Python object of type string has methods, such as capitalize and

13
00:01:07,000 --> 00:01:11,470
replace, but also objects of type float and list have specific methods depending on the

14
00:01:11,470 --> 00:01:13,320
type.

15
00:01:13,320 --> 00:01:17,990
Enough for the theory now; let&#39;s try to use a method! Suppose you want to get the index

16
00:01:17,990 --> 00:01:22,910
of the string &quot;mom&quot; in the `fam` list. `fam` is an Python object with the type `list`,

17
00:01:22,910 --> 00:01:27,670
and has a method named `index()`. To call the method, you use the dot notation, like

18
00:01:27,670 --> 00:01:33,880
this. The only input is the string &quot;mom&quot;, the element you want to get the index for.

19
00:01:33,880 --> 00:01:38,580
Python returns 4, which indeed is the index of the string &quot;mom&quot;. I called the index()

20
00:01:38,580 --> 00:01:44,110
method &quot;on&quot; the fam list here, and the output was 4. Similarly, I can use the `count()`

21
00:01:44,110 --> 00:01:49,290
method on the `fam` list to count the number of times `1.73` occurs in the list:

22
00:01:49,290 --> 00:01:55,940
Python gives me 1, which makes sense, because only liz is 1.73 meters tall.

23
00:01:55,940 --> 00:02:00,850
But lists are not the only Python objects that have methods associated. Also floats,

24
00:02:00,850 --> 00:02:06,690
integers, booleans and strings are Python objects that have specific methods associated.

25
00:02:06,690 --> 00:02:10,920
Take the variable `sister` for example, that represents a string.

26
00:02:10,919 --> 00:02:14,890
You can call the method `capitalize()` on `sister`, without any inputs. It returns a

27
00:02:14,890 --> 00:02:18,590
string where the first letter is capitalized now:

28
00:02:18,590 --> 00:02:22,890
Or what if you want to replace some parts of the string with other parts: not a problem.

29
00:02:22,890 --> 00:02:26,980
Just call the method replace on sister, with two appropriate inputs:

30
00:02:26,980 --> 00:02:31,050
In the output, &quot;z&quot; is replaced with &quot;sa&quot;.

31
00:02:31,050 --> 00:02:36,240
I guess it&#39;s clear by now: in Python, everything is an object, and each object has specific

32
00:02:36,240 --> 00:02:42,340
methods associated. Depending on the type of the object, list, string, float, whatever,

33
00:02:42,340 --> 00:02:47,440
the available methods are different. A string object like `sister` has a replace method,

34
00:02:47,440 --> 00:02:52,670
but a list like `fam` doesn&#39;t have this, as you can see from this error. Objects of different

35
00:02:52,670 --> 00:02:57,070
types can have methods with the same name: Take the `index()` method. It&#39;s available

36
00:02:57,070 --> 00:03:01,590
for both strings and lists. If you call it on a string, you get the index of the letters

37
00:03:01,590 --> 00:03:06,710
in the string; If you call it on a list, you get the index of the element in the list.

38
00:03:06,710 --> 00:03:12,520
This means that, depending on the type of the object, the methods behave differently.

39
00:03:12,520 --> 00:03:16,050
Before I unleash you on some exercises on methods, there&#39;s one more thing I want to

40
00:03:16,050 --> 00:03:21,590
tell you. Some methods can change the objects they are called on. Let&#39;s retake the `fam`

41
00:03:21,590 --> 00:03:26,140
list, and call the `append()` method on it. As the input, we pass a string we want to

42
00:03:26,140 --> 00:03:28,290
add to the list:

43
00:03:28,290 --> 00:03:32,230
Python doesn&#39;t generate an output, but if we check the `fam` list again, we see that

44
00:03:32,230 --> 00:03:35,380
it has been extended with the string `&quot;me&quot;`:

45
00:03:35,380 --> 00:03:39,330
Let&#39;s do this again, this time to add my length to the list:

46
00:03:39,330 --> 00:03:41,819
Again, the fam list was extended:

47
00:03:41,819 --> 00:03:46,100
This is pretty cool, because you can write very concise code to update your data structures

48
00:03:46,100 --> 00:03:50,870
on the fly, but it can also be pretty dangerous. Some method calls don&#39;t change the object

49
00:03:50,870 --> 00:03:55,130
they&#39;re called on, while others do, so watch out.

50
00:03:55,130 --> 00:03:59,690
Let&#39;s take a step back here and summarise this. you have Python functions, like type(),

51
00:03:59,690 --> 00:04:02,849
max() and round(), that you can call like this:

52
00:04:02,849 --> 00:04:07,360
There&#39;s also methods, which are functions that are specific to Python objects. Depending

53
00:04:07,360 --> 00:04:11,310
on the type of the Python object you&#39;re dealing with, you&#39;ll be able to use different methods

54
00:04:11,310 --> 00:04:15,870
and they behave differently. You can call methods on the objects with the dot notation,

55
00:04:15,870 --> 00:04:18,540
like this for example.

56
00:04:18,540 --> 00:04:23,090
There&#39;s much more to tell about Python objects, methods and how Python works internally, but

57
00:04:23,090 --> 00:04:27,720
for now, let&#39;s stick to what I&#39;ve talked about here. It&#39;s time to get some exercises and

58
00:04:27,720 --> 00:04:29,750
add methods to your evergrowing skillset!

