0
00:00:02,409 --> 00:00:06,850
In this video, I&#39;m going to introduce you to functions. Functions aren&#39;t entirely new

1
00:00:06,850 --> 00:00:11,900
for you actually: you&#39;ve already used them. type, for example, is a function that returns

2
00:00:11,900 --> 00:00:17,910
the type of a value. But what is a function? Simply put, a function is a piece of reusable

3
00:00:17,910 --> 00:00:22,810
code, aimed at solving a particular task. You can call functions instead of having to

4
00:00:22,810 --> 00:00:28,039
write code yourself. Maybe an example can clarify things here.

5
00:00:28,039 --> 00:00:32,160
Suppose you have the list containing only the heights of your family, fam:

6
00:00:32,159 --> 00:00:36,420
Say that you want to get the maximum value in this list. Instead of writing your own

7
00:00:36,420 --> 00:00:40,090
piece of Python code that goes through the list and finds the highest value, you can

8
00:00:40,090 --> 00:00:45,399
also use Python&#39;s `max()` function. This is one of Python&#39;s built-in functions, just like

9
00:00:45,399 --> 00:00:50,390
`type()`. We simply pass `fam` to `max()` inside parentheses:

10
00:00:50,390 --> 00:00:54,050
The output makes sense: 1.89, the highest number in the list.

11
00:00:54,050 --> 00:00:59,539
`max()` worked kind of like a black box here: you passed it a list, then the implementation

12
00:00:59,539 --> 00:01:04,339
of `max()`, that you don&#39;t know, did its magic, and produced an output. How `max()` actually

13
00:01:04,339 --> 00:01:08,880
did this, is not important to you, it just does what it&#39;s supposed to, and you didn&#39;t

14
00:01:08,880 --> 00:01:12,690
have to write your own code, which made your life easier.

15
00:01:12,690 --> 00:01:17,369
Of course, it&#39;s possible to also assign the result of a function call to a new variable,

16
00:01:17,369 --> 00:01:22,300
like here. Now `tallest` is just like any other variable; you can use to continue your

17
00:01:22,300 --> 00:01:25,520
fancy calculations.

18
00:01:25,520 --> 00:01:30,390
Another one of these built-in functions is `round()`. It takes two inputs: first, a number

19
00:01:30,390 --> 00:01:35,280
you want to round, and second, the precision with which to round, so how many digits behind

20
00:01:35,280 --> 00:01:40,660
the decimal point you want to keep. Say you want to round `1.68` to one decimal place.

21
00:01:40,660 --> 00:01:46,810
The first input is 1.68, the second input is 1. You separate the inputs with a comma:

22
00:01:46,810 --> 00:01:52,500
But there&#39;s more. It&#39;s perfectly possible to call the `round()` function with only one

23
00:01:52,500 --> 00:01:57,750
input, like this. &lt;PAUSE&gt;. This time, Python figured out that you didn&#39;t specify the second

24
00:01:57,750 --> 00:02:03,970
input, and automatically chooses to round the number to the closest integer.

25
00:02:03,970 --> 00:02:08,550
To understand why both approaches work, let&#39;s open up the documentation. You can do this

26
00:02:08,550 --> 00:02:12,630
with yet another function, `help`, as follows:

27
00:02:12,630 --> 00:02:17,640
It appears that `round()` takes two inputs. In Python, these inputs, also called arguments,

28
00:02:17,640 --> 00:02:22,980
have names: `number` and `ndigits`. When you call the function `round()`, with these two

29
00:02:22,980 --> 00:02:29,030
inputs, Python matches the inputs to the arguments: `number` is set to `1.68` and `ndigits` is

30
00:02:29,030 --> 00:02:34,330
set to 1. Next, The `round()` function does its calculations with `number` and `ndigits`

31
00:02:34,330 --> 00:02:38,810
as if they are variables in a Python script. We don&#39;t know exactly what code Python executes.

32
00:02:38,810 --> 00:02:45,360
What _is_ important, though, is that the function produces an output, namely the number 1.68

33
00:02:45,360 --> 00:02:47,200
rounded to 1 decimal place.

34
00:02:47,200 --> 00:02:52,430
If you call the function `round()` with only one input, Python again tries to match the

35
00:02:52,430 --> 00:02:56,750
inputs to the arguments. There&#39;s no input to match to the `ndigits` argument though.

36
00:02:56,750 --> 00:03:02,340
Luckily, the internal machinery of the `round()` function knows how to handle this. When `ndigits`

37
00:03:02,340 --> 00:03:07,750
is not specified, the function simply rounds to the closest integer and returns that integer.

38
00:03:07,750 --> 00:03:10,620
That&#39;s why we got the number 2.

39
00:03:10,620 --> 00:03:15,240
How was I so sure that calling the function with a single input would work? Well, in the

40
00:03:15,240 --> 00:03:19,760
documentation, there are square brackets around the comma and the `ndigits` here. This tells

41
00:03:19,760 --> 00:03:25,380
us that you can call `round()` in this form, as well as in this one. In other words, `ndigits`

42
00:03:25,380 --> 00:03:30,740
is an optional argument. Actually, Python offers yet another way to show that a function

43
00:03:30,740 --> 00:03:34,540
has optional arguments, but that&#39;s something for the exercises.

44
00:03:34,540 --> 00:03:38,819
By now, you have an idea about how to _use_ `max()` and `round()`, but how could you know

45
00:03:38,819 --> 00:03:43,620
that a function such as `round()` exists in Python in the first place? Well, this is something

46
00:03:43,620 --> 00:03:47,640
you will learn with time. Whenever you are doing a rather standard task in Python, you

47
00:03:47,640 --> 00:03:51,760
can be pretty sure that there&#39;s already a function that can do this for you. In that

48
00:03:51,760 --> 00:03:56,349
case, you should definitely use it! Just do a quick internet search and you&#39;ll find the

49
00:03:56,349 --> 00:04:01,000
function you need with a nice usage example. And there is of course DataCamp, where you&#39;ll

50
00:04:01,000 --> 00:04:05,510
also learn about powerful functions and how to use them. Get straight to it in the interactive

51
00:04:05,510 --> 00:04:06,290
exercises!

