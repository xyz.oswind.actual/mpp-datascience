0
00:00:01,990 --> 00:00:07,640
Hi, my name is Filip and I&#39;ll be your host for _Introduction to Python for Data Science_.

1
00:00:07,640 --> 00:00:12,759
It&#39;s a long name, but that&#39;s to stress something: this is not just another Python tutorial.

2
00:00:12,759 --> 00:00:17,490
Instead, the focus will be on using Python specifically for data science. By the end

3
00:00:17,490 --> 00:00:22,240
of this course, you&#39;ll know about powerful ways to store and manipulate data and to deploy

4
00:00:22,240 --> 00:00:25,519
powerful data science tools for your own analyses.

5
00:00:25,519 --> 00:00:29,730
You will learn Python for Data Science through video lessons, like this one, and interactive

6
00:00:29,730 --> 00:00:34,200
exercises. You get your own Python session where you can experiment and try to come up

7
00:00:34,200 --> 00:00:38,940
with the correct code to solve the instructions. You&#39;re learning by doing, while receiving

8
00:00:38,940 --> 00:00:42,680
customized and instant feedback on your work.

9
00:00:42,680 --> 00:00:46,910
Python was conceived by Guido Van Rossum. What started as a hobby project, soon became

10
00:00:46,910 --> 00:00:52,330
a general purpose programming language: nowadays, you can use Python to build practically any

11
00:00:52,330 --> 00:00:58,010
piece of software. How did this happen? Well, first of all, Python is open source. It&#39;s

12
00:00:58,010 --> 00:01:03,100
free to use. Second, it&#39;s very easy to build packages in Python, which is code that you

13
00:01:03,100 --> 00:01:08,000
can share with other people to solve specific problems. Throughout time, more and more of

14
00:01:08,000 --> 00:01:11,960
these package specifically built for data science have been developed. Suppose you want

15
00:01:11,960 --> 00:01:16,280
to make some fancy visualizations of your company&#39;s sales? There&#39;s a package for that.

16
00:01:16,280 --> 00:01:20,670
Or what about connecting to a database to analyze sensor measurements? There&#39;s also

17
00:01:20,670 --> 00:01:21,499
a package for that.

18
00:01:21,499 --> 00:01:28,119
Currently, there are two common versions of Python, version 2.7 and 3.5 and later. Apart

19
00:01:28,119 --> 00:01:32,369
from some syntactical differences, they are pretty similar, but as support for version

20
00:01:32,369 --> 00:01:37,799
2 will fade over time, our courses focus on Python 3. To install Python 3 on your own

21
00:01:37,799 --> 00:01:41,920
system, follow the steps at this URL.

22
00:01:41,920 --> 00:01:45,990
Now that you&#39;re all eyes and ears for Python, let&#39;s start experimenting. I&#39;ll start with

23
00:01:45,990 --> 00:01:51,219
the Python shell, a place where you can type Python code and immediately see the results.

24
00:01:51,219 --> 00:01:56,520
In DataCamp&#39;s exercise interface, this shell is embedded here. Let&#39;s start off simple and

25
00:01:56,520 --> 00:02:01,950
use Python as a calculator. Let me type 4 + 5 and hit Enter. Python interprets what

26
00:02:01,950 --> 00:02:07,590
you typed and prints the result of your calculation, 9. The Python shell that&#39;s used here is actually

27
00:02:07,590 --> 00:02:12,420
not the original one; we&#39;re using IPython, short for Interactive Python, which is some

28
00:02:12,420 --> 00:02:17,080
kind of juiced up version of regular Python that&#39;ll be useful later on.

29
00:02:17,080 --> 00:02:21,470
Apart from interactively working with Python, you can also have Python run so called python

30
00:02:21,470 --> 00:02:28,300
scripts. These python scripts are simply text files with the extension _dot p y_. It&#39;s basically

31
00:02:28,300 --> 00:02:32,300
a list of Python commands that are executed, almost as if you where typing the commands

32
00:02:32,300 --> 00:02:36,830
in the shell yourself, line by line. Let&#39;s put the command from before in a script now,

33
00:02:36,830 --> 00:02:41,880
that can be found here in DataCamp&#39;s interface. The next step is executing the script, by

34
00:02:41,880 --> 00:02:43,550
clicking &#39;Submit Answer&#39;.

35
00:02:43,550 --> 00:02:48,750
If you execute this script in the DataCamp interface, there&#39;s nothing in the output pane..

36
00:02:48,750 --> 00:02:52,250
That&#39;s because you have to explicitly use `print()` inside scripts if you want to generate

37
00:02:52,250 --> 00:02:57,620
output during execution. Let&#39;s wrap our previous calculation a in `print()` call, and rerun

38
00:02:57,620 --> 00:03:03,170
the script. This time, the same output as before is generated, great!

39
00:03:03,170 --> 00:03:07,620
Putting your code in Python scripts instead of manually retyping every step interactively

40
00:03:07,620 --> 00:03:11,590
will help you to keep structure and avoid retyping everything over and over again if

41
00:03:11,590 --> 00:03:16,060
you want to make a change; you simply make the change in the script, and rerun the entire

42
00:03:16,060 --> 00:03:17,810
thing.

43
00:03:17,810 --> 00:03:21,480
Now that you&#39;ve got an idea about different ways of working with Python, I suggest you

44
00:03:21,480 --> 00:03:26,640
head over to the exercises. Use the IPython Shell for experimentation, and use the Python

45
00:03:26,640 --> 00:03:31,330
script editor to code the actual answer. If you click Submit Answer, your script will

46
00:03:31,330 --> 00:03:34,120
be executed and checked for correctness. Have fun!

