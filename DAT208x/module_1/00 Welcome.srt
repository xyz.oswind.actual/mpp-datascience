0
00:00:00,000 --> 00:00:03,451
[MUSIC PLAYING]

1
00:00:12,102 --> 00:00:13,060
FILIP SCHOUWENAARS: Hi.

2
00:00:13,060 --> 00:00:16,270
My name is Filip, and I am a teacher at DataCamp.

3
00:00:16,270 --> 00:00:18,780
DataCamp is an online data science school,

4
00:00:18,780 --> 00:00:21,980
and together with Microsoft, we have created a new course.

5
00:00:21,980 --> 00:00:25,190
Introduction to Python for Data Science.

6
00:00:25,190 --> 00:00:27,760
Python is a very powerful programming language

7
00:00:27,760 --> 00:00:31,030
that you can use to build practically any piece of software.

8
00:00:31,030 --> 00:00:33,890
But you can also use it to do data science.

9
00:00:33,890 --> 00:00:36,790
Model your sales data, to text mining on your latest Twitter

10
00:00:36,790 --> 00:00:39,890
feeds, or build the next movie recommendation engine.

11
00:00:39,890 --> 00:00:42,270
It&#39;s all possible with Python.

12
00:00:42,270 --> 00:00:44,380
Python is open source, and there&#39;s a bunch

13
00:00:44,380 --> 00:00:46,330
of programmers across the world that have

14
00:00:46,330 --> 00:00:51,180
written amazing extensions to Python, to help you achieve great things.

15
00:00:51,180 --> 00:00:53,700
The best way to learn Python is by doing.

16
00:00:53,700 --> 00:00:56,030
And that&#39;s why you will spend most of your time

17
00:00:56,030 --> 00:00:58,530
in our interactive learning interface.

18
00:00:58,530 --> 00:01:02,940
After fun video lessons on edX, you move to our interactive coding environment,

19
00:01:02,940 --> 00:01:05,280
where you get instant and personalized feedback that

20
00:01:05,280 --> 00:01:08,610
guides you to the correct solution.

21
00:01:08,610 --> 00:01:11,940
You&#39;ll learn about the basics of Python, such as variables, lists,

22
00:01:11,940 --> 00:01:13,320
and functions.

23
00:01:13,320 --> 00:01:16,200
Also, specific packages to do data science are covered.

24
00:01:16,200 --> 00:01:19,400
Numpy, Matplotlib, and Pandas.

25
00:01:19,400 --> 00:01:21,150
This intro course is the best way to learn

26
00:01:21,150 --> 00:01:23,650
Python, specifically for data science.

27
00:01:23,650 --> 00:01:26,520
No prior knowledge or technical background is required.

28
00:01:26,520 --> 00:01:30,690
So enroll now, and get yourself ready for the skill of the future.

