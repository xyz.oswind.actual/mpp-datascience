0
00:00:01,939 --> 00:00:06,230
It&#39;s clear that Python is a great calculator. If you want to do more complex calculations

1
00:00:06,230 --> 00:00:11,200
though, you will want to &quot;save&quot; values while you&#39;re coding along. You can do this by defining

2
00:00:11,200 --> 00:00:17,420
a variable, with a specific, case-sensitive name. Once you create (or declare) such a

3
00:00:17,420 --> 00:00:21,800
variable, you can later call up its value by typing the variable name.

4
00:00:21,800 --> 00:00:25,980
Suppose you measure your height and weight, in metric units: you are 1 point 79 meters

5
00:00:25,980 --> 00:00:32,590
tall, and weigh 68.7 kilograms. You can assign these values to two variables, named `height`

6
00:00:32,590 --> 00:00:34,159
and `weight`, with an equals sign:

7
00:00:34,159 --> 00:00:37,289
If you now type the name of the variable, height

8
00:00:37,289 --> 00:00:41,899
Python looks for the variable name, retrieves its value, and prints it out.

9
00:00:41,899 --> 00:00:46,949
Let&#39;s now calculate the Body Mass Index, or BMI, which is calculated as follows, with

10
00:00:46,949 --> 00:00:51,890
weight in kilograms and height in meter. You can do this with the actual values &lt;PAUSE&gt;

11
00:00:51,890 --> 00:00:57,309
but you can just as well use the variables height and weight, like in here. Every time

12
00:00:57,309 --> 00:01:01,409
you type the variable&#39;s name, you are asking Python to change it with the actual value

13
00:01:01,409 --> 00:01:06,870
of the variable. weight corresponds to 68.7, and height to 1.79.

14
00:01:06,870 --> 00:01:13,190
Finally, this version has Python store the result in a new variable, `bmi`. `bmi` now

15
00:01:13,190 --> 00:01:16,120
contains the same value as the one you calculated earlier.

16
00:01:16,120 --> 00:01:22,330
In Python, variables are used all the time. They help to make your code reproducible.

17
00:01:22,330 --> 00:01:26,600
Suppose the code to create the height, weight and bmi variable are in a script, like this.

18
00:01:26,600 --> 00:01:30,930
If you now want to recalculate the bmi for another weight, you can simply change the

19
00:01:30,930 --> 00:01:35,640
declaration of the weight variable, and rerun the script. The `bmi` changes accordingly,

20
00:01:35,640 --> 00:01:39,890
because the value of the variable `weight` has changed.

21
00:01:39,890 --> 00:01:45,420
So far, we&#39;ve only worked with numerical values, such as height and weight. In Python, these

22
00:01:45,420 --> 00:01:50,080
numbers all have a specific type. You can check out the type of a value with the `type()`

23
00:01:50,080 --> 00:01:56,250
function. To see the type of our bmi value, simply write type and then bmi inside parentheses.

24
00:01:56,250 --> 00:02:01,270
You can see that it&#39;s a float, which is python&#39;s way of representing a real number, so a number

25
00:02:01,270 --> 00:02:05,750
which can have both an integer part and a fractional part. Python als has a type for

26
00:02:05,750 --> 00:02:09,849
integers: `int`, like this example

27
00:02:09,848 --> 00:02:14,310
To do data science, you&#39;ll need more than ints and floats, though. Python features tons

28
00:02:14,310 --> 00:02:19,000
of other data types. The most common ones are strings and booleans.

29
00:02:19,000 --> 00:02:23,300
A string is Python&#39;s way to represent text. You can use both double and single quotes

30
00:02:23,300 --> 00:02:27,910
to build a string, as you can see from these examples. If you print the type of the last

31
00:02:27,910 --> 00:02:32,660
variable here, you see that it&#39;s str, short for string.

32
00:02:32,660 --> 00:02:37,310
The Boolean is a type that can either be True or False. You can think of it as &#39;Yes&#39; and

33
00:02:37,310 --> 00:02:42,260
&#39;No&#39; in everyday language. Booleans will be very useful in the future, to perform filtering

34
00:02:42,260 --> 00:02:45,330
operations on your data for example.

35
00:02:45,330 --> 00:02:49,480
There&#39;s something special about Python data types. Have a look at this line of code, that

36
00:02:49,480 --> 00:02:54,240
sums two integers, and then this line of code, that sums two strings.

37
00:02:54,240 --> 00:02:58,350
For the integers, the values were summed, while for the strings, the strings were pasted

38
00:02:58,350 --> 00:03:04,220
together. The plus operator behaved differently for different data types. This is a general

39
00:03:04,220 --> 00:03:08,330
principle: how the code behaves depends on the types you&#39;re working with.

40
00:03:08,330 --> 00:03:12,900
In the exercises that follow, you&#39;ll create your very first variables and experiment with

41
00:03:12,900 --> 00:03:17,250
some of Python&#39;s data types. I&#39;ll see you in the next video to explain all about lists.

