0
00:00:00,030 --> 00:00:04,740
So we&#39;ve worked with one vector at a time. What happens when you need to work

1
00:00:04,740 --> 00:00:09,330
with multiple vectors. For example how would you add two vectors together, and

2
00:00:09,330 --> 00:00:16,770
what does that really mean? Well here&#39;s a simple vector, and here&#39;s another. Let&#39;s

3
00:00:16,770 --> 00:00:20,880
add these vectors. We just add the individual components, so 3 plus 2

4
00:00:20,880 --> 00:00:25,070
is 5; and 1 plus -4 is -3.

5
00:00:25,070 --> 00:00:29,939
You can think of the addition is simply adding another leg to the journey; so if

6
00:00:29,939 --> 00:00:35,700
we follow vector V along 3 and up 1, and then follow vector W along 2 and down 4,

7
00:00:35,700 --> 00:00:40,710
we end up at the head of the vector we calculated by adding V and

8
00:00:40,710 --> 00:00:43,219
W together.

