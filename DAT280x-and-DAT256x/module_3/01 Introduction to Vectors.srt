0
00:00:00,230 --> 00:00:04,529
Vectors and vector spaces are fundamental to linear algebra, and

1
00:00:04,529 --> 00:00:08,540
they&#39;re used in many machine learning models and data manipulation scenarios.

2
00:00:08,540 --> 00:00:12,660
Vectors describe spatial lines and planes and able you to perform

3
00:00:12,660 --> 00:00:18,930
calculations that explore relationships in multi-dimensional space. Vectors are

4
00:00:18,930 --> 00:00:22,109
usually named as lowercase letters with an arrow above them to indicate that

5
00:00:22,109 --> 00:00:26,519
this name represents a vector. The vector itself consists of a set of values to

6
00:00:26,519 --> 00:00:31,590
describe direction, or amplitude; and length, or magnitude. So what does that

7
00:00:31,590 --> 00:00:35,940
mean? Well you can think of the values in a vector as describing the route you

8
00:00:35,940 --> 00:00:39,870
must take from an arbitrary starting point along a set of dimensions to get

9
00:00:39,870 --> 00:00:44,399
to a destination point. In this case, our vector has two dimensions, and it tells

10
00:00:44,399 --> 00:00:49,110
us we need to travel 2 units in one direction (let&#39;s call X), and 3 units

11
00:00:49,110 --> 00:00:55,170
in another direction (let&#39;s call that Y). So our vector looks like this. Now note

12
00:00:55,170 --> 00:00:59,579
that I&#39;ve assumed the starting point or origin of the vector is at 0 0, with X

13
00:00:59,579 --> 00:01:03,899
moving from left to right and Y moving from bottom to top. That&#39;s known as the

14
00:01:03,899 --> 00:01:08,580
standard position. There&#39;s nothing inherent in the vector definition itself

15
00:01:08,580 --> 00:01:12,000
to tell us the starting point, so the vector could just as easily be

16
00:01:12,000 --> 00:01:17,430
describing this movement. Vectors can actually have more than two

17
00:01:17,430 --> 00:01:22,590
dimensions; for example here&#39;s a three-dimensional vector. Now again the

18
00:01:22,590 --> 00:01:26,369
vector describes the journey from an arbitrary origin; so this time we go

19
00:01:26,369 --> 00:01:31,560
3 units in the X dimension, 2 units in the Y dimension, and 1 unit and the

20
00:01:31,560 --> 00:01:38,220
Z dimension; so our vector looks like this. Let&#39;s return to our simple

21
00:01:38,220 --> 00:01:43,320
two-dimensional vector. The components of this vector are known as its Cartesian

22
00:01:43,320 --> 00:01:46,979
coordinates, and they describe the vector in terms of units along each of its

23
00:01:46,979 --> 00:01:51,659
dimensions. For example to get from the origin to the destination, we might need

24
00:01:51,659 --> 00:01:56,340
to travel 2 miles east and 3 miles north. However sometimes you want

25
00:01:56,340 --> 00:02:00,329
to express vectors using their polar coordinates; in other words the distance

26
00:02:00,329 --> 00:02:04,259
you need to travel along a particular bearing. For example, travel 3 and a

27
00:02:04,259 --> 00:02:09,660
half miles on a bearing of 50 degrees. These polar coordinates are simply the

28
00:02:09,660 --> 00:02:16,910
length (or magnitude) and direction (or amplitude) of the vector. To calculate the length, or

29
00:02:16,910 --> 00:02:20,840
magnitude of the vector; we can treat the vector as the hypotenuse in a right

30
00:02:20,840 --> 00:02:25,310
angle triangle and apply pythagoras theorem -  the square of the hypotenuse is

31
00:02:25,310 --> 00:02:30,260
equal to sum of the squares on the other sides. So in this case the magnitude for

32
00:02:30,260 --> 00:02:35,420
a vector is the square root of 2-squared plus 3-squared, which is the square root

33
00:02:35,420 --> 00:02:43,489
of 4 plus 9 or 13 - approximately 3.6. In more general terms you can calculate the

34
00:02:43,489 --> 00:02:47,480
magnitude by finding the square root of all the squares of all the vector

35
00:02:47,480 --> 00:02:53,959
components. Calculating the direction, or amplitude, of our vector is a little more

36
00:02:53,959 --> 00:02:59,060
complex. What we&#39;re trying to find is the angle of the vector, which we generally

37
00:02:59,060 --> 00:03:04,549
refer to using the Greek letter Theta. You can calculate Theta as the inverse

38
00:03:04,549 --> 00:03:10,099
tangent of the opposite over the adjacent; or Y over X. So in this case, 3

39
00:03:10,099 --> 00:03:15,860
over 2. Now using a calculator, that turns out to be approximately 56.3

40
00:03:15,860 --> 00:03:20,390
degrees. Things get a little more complicated if the vector is not in the

41
00:03:20,390 --> 00:03:24,260
standard orientation pointing right and up, and there are rules for dealing with

42
00:03:24,260 --> 00:03:28,069
this as you&#39;ll see in the lab. However, most scripting languages and tools for

43
00:03:28,069 --> 00:03:32,859
working with vectors can handle the calculations for you.

