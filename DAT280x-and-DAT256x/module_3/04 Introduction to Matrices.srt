0
00:00:00,319 --> 00:00:05,640
We&#39;ve previously explored vectors by representing them as matrices. In more

1
00:00:05,640 --> 00:00:09,389
general terms, a matrix is an array of numbers that can be arranged into rows

2
00:00:09,389 --> 00:00:14,610
and columns. We generally name matrices with a capital letter. For example, here&#39;s

3
00:00:14,610 --> 00:00:20,670
a matrix named A, and here&#39;s another matrix named B. Both of these matrices

4
00:00:20,670 --> 00:00:26,820
have two rows and three columns, so we describe them as 2 by 3 matrices. You can

5
00:00:26,820 --> 00:00:30,599
add or subtract matrices of the same size by simply adding or subtracting the

6
00:00:30,599 --> 00:00:34,800
corresponding elements in the two matrices. So in our example in the first

7
00:00:34,800 --> 00:00:40,320
row: 3 plus 2 is 5, 5 plus -2 is 3,  1 plus 4 is 5;

8
00:00:40,320 --> 00:00:49,559
and in the second row, 1 plus -1 is 0, 4 plus 3 is 7, and 3 plus 1 is 4. The

9
00:00:49,559 --> 00:00:54,239
negative of a matrix is just a matrix with the sign of each element reversed,

10
00:00:54,239 --> 00:01:02,489
so negative B looks like this. You can also transpose a matrix to switch the

11
00:01:02,489 --> 00:01:06,150
row and column orientation, and we indicate transposition with a

12
00:01:06,150 --> 00:01:09,950
superscript T like this.

