0
00:00:00,030 --> 00:00:07,350
Here&#39;s a matrix A; and here&#39;s a vector v. Now we can use A as a transformation

1
00:00:07,350 --> 00:00:14,130
matrix for v, which gives us this transformed vector as a result. Now note

2
00:00:14,130 --> 00:00:17,880
that the transformation has affected the length of the original vector, but not its

3
00:00:17,880 --> 00:00:23,010
direction. In other words, it&#39;s simply scaled the vector - exactly as a scalar

4
00:00:23,010 --> 00:00:28,109
multiplication by 2 would do.  Now when we have a transformation like this, in

5
00:00:28,109 --> 00:00:31,650
which the matrix transformation of a vector can be performed using a scalar

6
00:00:31,650 --> 00:00:35,940
multiplication, we write the equivalence like this; with the Greek letter lambda

7
00:00:35,940 --> 00:00:41,790
used to indicate the scalar multiplier. The scalar multiplier lambda is known as

8
00:00:41,790 --> 00:00:47,360
an eigenvalue of the matrix A, and the vector as its corresponding eigenvector.

9
00:00:47,360 --> 00:00:53,579
Now a matrix generally has more than one eigenvalue and eigenvector pair. In this

10
00:00:53,579 --> 00:00:58,770
case, the vector (0,1) is also an eigenvector of A; and it&#39;s corresponding

11
00:00:58,770 --> 00:01:05,460
eigenvalue is once again 2. For any given matrix, you can represent its

12
00:01:05,459 --> 00:01:10,140
eigenvectors as a matrix with an eigenvector in each column. We usually

13
00:01:10,140 --> 00:01:16,770
call this matrix Q; and you can represent the corresponding eigenvalues as a

14
00:01:16,770 --> 00:01:22,920
diagonalized matrix, usually indicated as an uppercase Lambda. In the case of the

15
00:01:22,920 --> 00:01:29,430
matrix we&#39;ve been exploring, its eigenvectors are (0,1) and (1,0) and the

16
00:01:29,430 --> 00:01:35,150
corresponding eigenvalues are 2 and 2. For each of these pairs, our

17
00:01:35,150 --> 00:01:41,030
equivalent matrix and scalar multiplication definition holds true.

