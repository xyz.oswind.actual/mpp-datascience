0
00:00:00,030 --> 00:00:05,700
So if we can add vectors, can we also multiply them? Well yes, but there are

1
00:00:05,700 --> 00:00:08,849
some considerations that make vector multiplication a little more complex

2
00:00:08,849 --> 00:00:13,380
than simple arithmetic. There are three kinds of multiplication operation that

3
00:00:13,380 --> 00:00:18,210
we can apply to vectors. The first is to multiply a vector by a constant scalar

4
00:00:18,210 --> 00:00:23,910
value to produce a new vector -  this is called scalar multiplication. To perform

5
00:00:23,910 --> 00:00:27,930
scalar multiplication, you just multiply each element of the vector by the scalar

6
00:00:27,930 --> 00:00:35,399
value. So in this case 3 times 2 is 6, and 1 times 2 is 2. The second kind of

7
00:00:35,399 --> 00:00:38,960
multiplication is to calculate what&#39;s known as the dot product of two vectors -

8
00:00:38,960 --> 00:00:45,120
this produces a scalar value as a result. To calculate the dot product, you

9
00:00:45,120 --> 00:00:49,980
multiply the corresponding elements of the vectors and add the results In this

10
00:00:49,980 --> 00:00:55,079
case, 3 times 2 is 6, and 1 times -4 is -4; and adding these together

11
00:00:55,079 --> 00:01:01,500
gives us our scalar result of 2. The third kind of multiplication is to

12
00:01:01,500 --> 00:01:06,210
calculate the cross product of two vectors -  this produces a new vector that

13
00:01:06,210 --> 00:01:10,170
is perpendicular to the original two vectors in three-dimensional space, so

14
00:01:10,170 --> 00:01:14,729
only really makes sense when using 3 dimensional vectors. To calculate the

15
00:01:14,729 --> 00:01:18,210
cross product, you need to calculate the individual elements of the resulting

16
00:01:18,210 --> 00:01:23,909
vector. For the first element, you multiply the second element in vector 1

17
00:01:23,909 --> 00:01:28,920
by the third element in vector 2, and then you subtract the third element in

18
00:01:28,920 --> 00:01:35,009
vector 1 multiplied by the second element in vector 2. For the second

19
00:01:35,009 --> 00:01:37,560
element, you must multiply the third element in

20
00:01:37,560 --> 00:01:42,119
vector 1 by the first element of vector 2, and then subtract the first element in

21
00:01:42,119 --> 00:01:48,420
vector 1 multiplied by the third element in vector 2; and for element 3 it&#39;s the

22
00:01:48,420 --> 00:01:53,520
first element in vector 1 multiplied by the second element of vector 2, minus the

23
00:01:53,520 --> 00:01:59,100
second element of vector 1 times the first element of vector 3. And when we

24
00:01:59,100 --> 00:02:03,600
unravel all of that to find our cross product, it&#39;s a perpendicular vector with

25
00:02:03,600 --> 00:02:08,270
elements -4, 8, and -4.

