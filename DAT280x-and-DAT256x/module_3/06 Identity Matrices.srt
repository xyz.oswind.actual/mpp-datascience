0
00:00:00,000 --> 00:00:06,210
An identity matrix, usually indicated by capital I, is the equivalent in matrix

1
00:00:06,210 --> 00:00:14,460
terms of the number 1. Here&#39;s a 3x3 matrix. Now here&#39;s another 3x3 matrix, but

2
00:00:14,460 --> 00:00:18,510
this one has 0 values for most of the elements other than a diagonal set of

3
00:00:18,510 --> 00:00:23,850
elements with the value 1. This is an identity matrix -  the matrix equivalent of

4
00:00:23,850 --> 00:00:29,609
1. Now since that&#39;s the matrix equivalent of 1, if we calculate the dot product of

5
00:00:29,609 --> 00:00:34,009
D and I the result is simply D.

