0
00:00:00,030 --> 00:00:07,350
Suppose we have the vector v, with elements 1 and 2. Here it is on the plot.

1
00:00:07,350 --> 00:00:13,320
We could define a function T to operate on a vector, and apply it to v.

2
00:00:13,320 --> 00:00:17,220
in this case, the function multiplies the first element of the vector by 3,

3
00:00:17,220 --> 00:00:23,279
and the second by 2; which changes our vector elements to 3 and 4. This transforms our

4
00:00:23,279 --> 00:00:27,510
vector, changing both its length and direction. For this reason,

5
00:00:27,510 --> 00:00:33,420
functions that operate on vectors are known as transformations. Now let&#39;s

6
00:00:33,420 --> 00:00:40,020
suppose we have a matrix. We can express our transformation in the form of this

7
00:00:40,020 --> 00:00:45,329
matrix. To apply the transformation, we just perform the dot product

8
00:00:45,329 --> 00:00:50,760
multiplication of the matrix with the vector; which gives us a transformed vector

9
00:00:50,760 --> 00:00:54,079
as a result.

