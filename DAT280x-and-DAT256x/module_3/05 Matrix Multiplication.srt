0
00:00:00,800 --> 00:00:04,859
Multiplying  matrices is a little more complex than the operations we&#39;ve seen

1
00:00:04,859 --> 00:00:10,019
so far. There are two cases to consider: scalar multiplication, which is

2
00:00:10,019 --> 00:00:15,000
multiplying a matrix by a single number; and dot product matrix multiplication, or

3
00:00:15,000 --> 00:00:20,880
multiplying a matrix by another matrix. Scalar multiplication is fairly

4
00:00:20,880 --> 00:00:24,359
straightforward, you just multiply each element of the matrix by the scalar

5
00:00:24,359 --> 00:00:29,640
multiplier. Dot product multiplication requires two matrices in which the

6
00:00:29,640 --> 00:00:33,000
number of columns in the first matrix is the same as the number of rows in the

7
00:00:33,000 --> 00:00:37,350
second matrix. There are two rows being multiplied by two columns in this

8
00:00:37,350 --> 00:00:42,540
example, so the dot product of these matrices will have four elements. To

9
00:00:42,540 --> 00:00:46,469
calculate the element in the first row and first column of the result, we

10
00:00:46,469 --> 00:00:50,039
multiply each element of the first row in matrix one by the corresponding

11
00:00:50,039 --> 00:00:56,010
element in the first column of matrix two, and add the results. In this case,

12
00:00:56,010 --> 00:01:00,719
that&#39;s 3 times 2 plus 5 times 1 plus 1 times 3, which is

13
00:01:00,719 --> 00:01:06,450
14. Next we get the product of multiplying the first row in matrix one

14
00:01:06,450 --> 00:01:13,650
by the second column in matrix two; and in this case that comes out to 5. Then

15
00:01:13,650 --> 00:01:18,479
we move on to the second row and first column, which calculates out to 15;

16
00:01:18,479 --> 00:01:23,460
and finally we complete the second row and second column calculation which

17
00:01:23,460 --> 00:01:26,090
gives us 10.

