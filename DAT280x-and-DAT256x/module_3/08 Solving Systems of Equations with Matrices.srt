0
00:00:00,199 --> 00:00:04,380
One of the cool things about matrices is that they can help us solve systems of

1
00:00:04,380 --> 00:00:11,010
equations. Consider this system of linear equations. Now we'd like to solve these

2
00:00:11,010 --> 00:00:18,180
for both x and y. We can rewrite these equations in matrix form like this. The

3
00:00:18,180 --> 00:00:22,470
dot product of the first row in matrix one by the first column in matrix 2 is

4
00:00:22,470 --> 00:00:29,279
2x plus 4y, and yields 10; and the second row by the first column is 3x plus 3y

5
00:00:29,279 --> 00:00:37,100
with a result of 12. Now logically, if matrix one times matrix two gives us matrix three,

6
00:00:37,100 --> 00:00:44,190
then matrix two is matrix three divided by matrix one;  or multiplied by its inverse. So

7
00:00:44,190 --> 00:00:47,940
we can calculate the inverse of matrix one using the technique we saw previously,

8
00:00:47,940 --> 00:00:51,960
then complete the dot product calculation to find the values for x and

9
00:00:51,960 --> 00:00:54,180
y; which i've rounded to the nearest whole number.

10
00:00:54,180 --> 00:00:59,969
And if we substitute our answers back into the original equation; sure enough 2

11
00:00:59,969 --> 00:01:06,860
times 3 plus 4 times 1 is 10, and 3 times 3 plus 3 times 1 is 12.

