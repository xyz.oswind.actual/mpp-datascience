0
00:00:00,000 --> 00:00:06,150
You can't actually divide by a matrix; but when you want to divide matrices, you can

1
00:00:06,150 --> 00:00:09,900
take advantage of the fact that division by a given number is the same as

2
00:00:09,900 --> 00:00:16,440
multiplication by the reciprocal of that number. Before we look at matrices, let's

3
00:00:16,440 --> 00:00:21,619
take a look at a simple scalar division. In this case we want to divide 8 by 2.

4
00:00:21,619 --> 00:00:27,420
Now dividing  by 2 is the same as multiplying by 1/2, which is 1over 2; now

5
00:00:27,420 --> 00:00:32,000
note that that's the inverse of 2 over 1, which is of course the same thing as 2.

6
00:00:32,000 --> 00:00:37,980
Now that in turn is the same thing as 2 to the power of -1. All of these

7
00:00:37,980 --> 00:00:43,890
are equivalent operations that will give you the same result, in this case 4.

8
00:00:43,890 --> 00:00:50,640
1 over 2 and 2 to the -1 represent the reciprocal of 2, and multiplying by

9
00:00:50,640 --> 00:00:56,690
the reciprocal of a number has the same effect as dividing by that number. Now

10
00:00:56,690 --> 00:01:02,070
let's apply that principle to working with matrices. In this case we want to

11
00:01:02,070 --> 00:01:07,830
divide matrix A by matrix B. More specifically we want to multiply A by

12
00:01:07,830 --> 00:01:14,070
the inverse of B, so the first thing we need to do is find the inverse of B. Now

13
00:01:14,070 --> 00:01:17,909
there's a specific technique to calculate the inverse of a matrix -  here's

14
00:01:17,909 --> 00:01:22,920
the formula for the inverse of a 2x2 matrix. It involves a little playing around

15
00:01:22,920 --> 00:01:26,460
with the elements of the original matrix -  we've swapped the positions of the first

16
00:01:26,460 --> 00:01:31,200
row first column and second row second column elements, and we've multiplied the

17
00:01:31,200 --> 00:01:35,549
other elements by -1 to make them negative. Then we've multiplied the

18
00:01:35,549 --> 00:01:40,470
resulting matrix from that by 1 over the determinant of the matrix, which is

19
00:01:40,470 --> 00:01:45,810
calculated by subtracting the products of the diagonal elements. When we multiply

20
00:01:45,810 --> 00:01:50,100
all of that out, we end up with an inverse matrix containing elements

21
00:01:50,100 --> 00:01:57,479
0.8, -0.6, 0.2, and 0.4. Now we can dot product

22
00:01:57,479 --> 00:02:02,280
multiply A by the inverse of B to get our result, which is a matrix containing

23
00:02:02,280 --> 00:02:08,970
3, 0,  0.6, and 0.8. The most complicated part of the

24
00:02:08,970 --> 00:02:12,780
whole operation is to calculate the inverse of the matrix, and it gets even

25
00:02:12,780 --> 00:02:15,329
more complex matrices with more than two rows and two columns,

26
00:02:15,329 --> 00:02:20,760
as you'll see in the lab. However in reality, you'll likely use tools or

27
00:02:20,760 --> 00:02:24,360
scripting languages that do the heavy lifting for you. The important concept to

28
00:02:24,360 --> 00:02:28,019
understand here is that matrix division is actually achieved through

29
00:02:28,019 --> 00:02:32,090
multiplication by the inverse of the matrix.

