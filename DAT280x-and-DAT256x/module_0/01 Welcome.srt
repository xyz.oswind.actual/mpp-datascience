0
00:00:00,540 --> 00:00:05,970
Hi and welcome to this course on essential mathematics. I&#39;m Graeme Malcolm

1
00:00:05,970 --> 00:00:09,660
and I&#39;ll be your guide as we explore some core mathematical concepts

2
00:00:09,660 --> 00:00:13,230
techniques and notation that you&#39;re going to find useful if you want to work

3
00:00:13,230 --> 00:00:17,070
with machine learning and AI. Now the first thing you should know is that I&#39;m

4
00:00:17,070 --> 00:00:20,870
not a mathematician, and this course is not designed to make you one either.

5
00:00:20,870 --> 00:00:25,350
Instead, this course focuses on some essential mathematical foundations on

6
00:00:25,350 --> 00:00:30,060
which machine learning and artificial intelligence are built. We&#39;ll start off

7
00:00:30,060 --> 00:00:34,770
with some basic algebra to get started with equations and functions; then we&#39;ll

8
00:00:34,770 --> 00:00:39,050
dive into some differential calculus to explore derivatives and optimization.

9
00:00:39,050 --> 00:00:43,680
Next we&#39;ll look at some linear algebra and cover vectors and matrices, before

10
00:00:43,680 --> 00:00:48,060
finally getting to grips with some statistics and probability. So, if like me

11
00:00:48,060 --> 00:00:51,570
it&#39;s been a while since you studied math at school, this course is designed to

12
00:00:51,570 --> 00:00:55,500
help you get up to speed on the key concepts and notation on which machine

13
00:00:55,500 --> 00:00:58,550
learning an AI are based.

