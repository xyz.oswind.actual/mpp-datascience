0
00:00:00,589 --> 00:00:05,730
The course materials for this course are provided as Jupyter notebooks - an online

1
00:00:05,730 --> 00:00:09,269
form factor that combines notes and interactive code you can run to see

2
00:00:09,269 --> 00:00:13,170
visualizations of the mathematical concepts that we&#39;re discussing. Now, we

3
00:00:13,170 --> 00:00:16,080
recommend you use the free Azure Notebooks service to work with the

4
00:00:16,079 --> 00:00:19,199
notebooks, though you can use a locally installed instance of Jupyter if you

5
00:00:19,199 --> 00:00:24,539
have one. To use Azure Notebooks navigate to https://notebooks.azure.com and

6
00:00:24,539 --> 00:00:29,429
sign-in using a Microsoft account, like an outlook.com address or a hotmail

7
00:00:29,429 --> 00:00:32,850
address or something like that. Now the first time you&#39;ve logged in

8
00:00:32,850 --> 00:00:35,969
you&#39;ll be presented with something that looks like this. You can edit your

9
00:00:35,969 --> 00:00:39,149
profile and add a photo and all that kind of thing if you want; but the

10
00:00:39,149 --> 00:00:42,270
important thing you want to do is to start working with some libraries, and

11
00:00:42,270 --> 00:00:46,559
initially you won&#39;t have any. So we&#39;ll create a library into which you&#39;re going

12
00:00:46,559 --> 00:00:50,789
to store your files, and we&#39;ll give this library a friendly name. I&#39;m just going

13
00:00:50,789 --> 00:00:57,030
to name it after the course that DAT256x -  you can call it anything you like. I&#39;m

14
00:00:57,030 --> 00:00:59,609
not going to make this a public library -  I don&#39;t want to share it,  I&#39;m just going to use

15
00:00:59,609 --> 00:01:03,149
it for my own work. I can give it some sort of unique ID should I want to

16
00:01:03,149 --> 00:01:09,150
share it at some future point, I&#39;ll just do that; and I don&#39;t necessarily need a

17
00:01:09,150 --> 00:01:13,080
ReadMe either. So we&#39;ll just go ahead and create that library. That then

18
00:01:13,080 --> 00:01:17,220
appears here, and then when I go into that library I&#39;m ready to start adding

19
00:01:17,220 --> 00:01:20,970
some files. Now we&#39;ll provide you with the lab files for the course -  you&#39;ll be able

20
00:01:20,970 --> 00:01:24,479
to download and extract them locally on your computer; and when you have done

21
00:01:24,479 --> 00:01:27,900
that, you&#39;ll be able to come in to your library here and you&#39;ll be able to go

22
00:01:27,900 --> 00:01:32,700
and upload from your computer - just simply choosing a file. So if I go to my

23
00:01:32,700 --> 00:01:39,090
C Drive and look in my DAT256x folder in the first module, there&#39;s a bunch of

24
00:01:39,090 --> 00:01:45,240
different notebooks there. We&#39;ll go ahead and just upload the first of those and

25
00:01:45,240 --> 00:01:48,000
it will appear here in the notebook ready for me to use. I can just

26
00:01:48,000 --> 00:01:51,390
upload the notebooks as-and-when I&#39;m ready to work on them in the labs ,and

27
00:01:51,390 --> 00:01:55,439
when you&#39;re ready to work with it, you just simply go ahead and click that

28
00:01:55,439 --> 00:01:59,100
notebook. It will open up in a new tab that will create an instance of a

29
00:01:59,100 --> 00:02:02,939
notebook server in the background for you to use; and you just make sure that

30
00:02:02,939 --> 00:02:07,439
it&#39;s using this Python 3.6 kernel up here, and what you&#39;ll find in the

31
00:02:07,439 --> 00:02:11,910
notebooks is there&#39;s a whole bunch of notes in there and there&#39;ll be these

32
00:02:11,910 --> 00:02:15,630
cells that contain some Python code; and to run those cells you just

33
00:02:15,630 --> 00:02:19,710
simply put the cursor in that cell and you can either on the Cell menu choose

34
00:02:19,710 --> 00:02:25,050
Run Cells, or you can click this little icon here to run that cell and select

35
00:02:25,050 --> 00:02:29,580
the one below. Off it goes and runs, and we get the output appearing underneath it.

36
00:02:29,580 --> 00:02:34,740
So that&#39;s the basic introduction to Azure Notebooks. It&#39;s easy enough to

37
00:02:34,740 --> 00:02:38,640
use them. When you&#39;re finished working in your notebook you can just simply save

38
00:02:38,640 --> 00:02:42,959
your changes, and then close and halt it to stop it running; and you&#39;re back in

39
00:02:42,959 --> 00:02:46,670
your Azure Notebooks library.

