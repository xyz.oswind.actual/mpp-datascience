0
00:00:00,199 --> 00:00:04,710
Now that you know a little bit about what a polynomial is, let&#39;s look at how

1
00:00:04,710 --> 00:00:10,349
you can perform arithmetic operations on polynomials. Let&#39;s think about addition

2
00:00:10,349 --> 00:00:14,490
and subtraction. The result of adding or subtracting polynomials is always itself

3
00:00:14,490 --> 00:00:19,439
for polynomial. To add or subtract polynomials, you just add or subtract the

4
00:00:19,439 --> 00:00:27,359
individual like terms; so in this case, 4x-squared plus x-squared is 5x-squared, 3x

5
00:00:27,359 --> 00:00:37,140
added to -2x is just X, and 2 plus 7 is 9. Let&#39;s multiply polynomials. You need

6
00:00:37,140 --> 00:00:41,070
to multiply each term in the first expression with each term in the second

7
00:00:41,070 --> 00:00:49,920
expression; so in this case 2x-cubed times 4x-squared is 8x to the fifth, 2x-cubed

8
00:00:49,920 --> 00:00:59,789
times x is 2x to the fourth, and 2x-cubed times 2 is 4x-cubed; then 3 times

9
00:00:59,789 --> 00:01:11,670
4x-squared is 12x-cubed, 3x times x is 3x-squared, and 3x times 2 is 6x. Then

10
00:01:11,670 --> 00:01:18,840
-3 times 4x-squared is -12x-squared, -3 times x is -3x

11
00:01:18,840 --> 00:01:26,310
and -3 times 2 is -6. So now we&#39;ve multiplied all

12
00:01:26,310 --> 00:01:30,240
the individual terms, we can combine the like terms to determine the overall

13
00:01:30,240 --> 00:01:38,009
answer. To divide a polynomial by a single term expression, which we call a

14
00:01:38,009 --> 00:01:42,659
monomial, you can convert each term to a fraction and use simplification to

15
00:01:42,659 --> 00:01:47,310
determine the answer. So in this case we take each of our polynomial terms as the

16
00:01:47,310 --> 00:01:54,960
numerators, and we use the divisor as the denominator. 9x-squared is 3x times 3,

17
00:01:54,960 --> 00:02:05,159
and 3x goes into 6x twice; so our answer is 3x + 2. To divide more complex polynomials

18
00:02:05,159 --> 00:02:11,879
we need to use a long division, so for example let&#39;s divide 12x-squared + 9x

19
00:02:11,879 --> 00:02:16,799
by x - 3. We start with the highest order

20
00:02:16,799 --> 00:02:24,269
terms; x goes into 12x-squared 12x times. Then we multiply that through the

21
00:02:24,269 --> 00:02:31,260
divisor and subtract, so 12x times x is 12x-squared and 12x times -3 is

22
00:02:31,260 --> 00:02:40,170
=36x. 12x-squared - 12x=squared cancels out, and 9x minus -36x

23
00:02:40,170 --> 00:02:49,410
gives us a remainder of 45x Now we repeat the process. x goes into 45x 45

24
00:02:49,410 --> 00:03:00,810
times. 45 times x is 45x, and 45 times -3 is -135; so now we can

25
00:03:00,810 --> 00:03:06,930
see that the 45x - 45x cancels out, and that leaves us with 0 minus -135

26
00:03:06,930 --> 00:03:14,489
which is just +135. Now we can&#39;t divide that easily by x - 3, so

27
00:03:14,489 --> 00:03:24,000
our final remainder is 135 over x - 3; So 12x-squared + 9x divided by x - 3

28
00:03:24,000 --> 00:03:32,780
gives us a result of 12x + 45 +135 over x - 3.

