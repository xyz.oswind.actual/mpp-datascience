0
00:00:00,170 --> 00:00:03,870
Now up to this point all of our equations have contained a single

1
00:00:03,870 --> 00:00:08,639
variable and we&#39;ve solved the equation to find that variables value. Some

2
00:00:08,639 --> 00:00:11,910
equations contain two variables, and while we can&#39;t determine definitive

3
00:00:11,910 --> 00:00:15,809
values for both, we can solve the equation for one variable in the sense

4
00:00:15,809 --> 00:00:20,520
that we find out how to calculate it with respect to the other one. In this

5
00:00:20,520 --> 00:00:27,390
equation, we&#39;ve got the variables x and y so let&#39;s solve this equation for y. So

6
00:00:27,390 --> 00:00:35,670
first of all we need to isolate the y term. Then we can divide both sides by 2 to

7
00:00:35,670 --> 00:00:41,370
isolate y from its coefficient, and that gives us a definition of y with respect

8
00:00:41,370 --> 00:00:48,660
to x. Now that we have that definition, we can calculate the value of y for any

9
00:00:48,660 --> 00:00:58,109
given value of x. So for example when x is 1 then y is 3, when x is 2 y is 5, when

10
00:00:58,109 --> 00:01:06,210
x is 3 y is 7, and so on. If we use these x and y value pairs as coordinates, we

11
00:01:06,210 --> 00:01:11,520
could plot them like this; and the plotted points form a line on which

12
00:01:11,520 --> 00:01:15,960
every possible pair of x and y values for the equation can be found - which is

13
00:01:15,960 --> 00:01:20,180
why we call these things linear equations.

