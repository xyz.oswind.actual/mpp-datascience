0
00:00:01,250 --> 00:00:05,460
Factorization is the process of restating an expression as the product

1
00:00:05,460 --> 00:00:10,500
of multiple expressions; in other words expressions multiplied together. So for

2
00:00:10,500 --> 00:00:15,809
example, think of the number six. You can make six by multiplying 1 times 6 or you

3
00:00:15,809 --> 00:00:24,660
can multiply 2 times 3; so 1, 6, 2, and 3 are all factors of 6. The same is true with

4
00:00:24,660 --> 00:00:31,529
expressions that include variables, so x, 6x, 2x ,and 3x are all factors of 6x-squared.

5
00:00:31,529 --> 00:00:39,059
Now let&#39;s take two numbers and find their factors. What are the common

6
00:00:39,059 --> 00:00:48,780
factors of 6 and 15? Well 1 times 6 is 6, and 1 times 15 is 15; so 1 is a factor of

7
00:00:48,780 --> 00:00:55,680
both 6 and 15. 2 times 3 is 6, so 2 is a factor of 6; but 2 doesn&#39;t multiply

8
00:00:55,680 --> 00:01:03,030
equally into 15 so it&#39;s not a factor of 15. 3 times 2 is 6, and 3 times 5 is 15; so

9
00:01:03,030 --> 00:01:09,659
3 is a common factor of 6 and 15. 5 doesn&#39;t multiply evenly into 6, but 5

10
00:01:09,659 --> 00:01:16,740
times 3 is 15. So out of all the factors for both 6 and 15, it turns out that 3 is

11
00:01:16,740 --> 00:01:20,759
the largest number that multiplies equally into them both - it&#39;s what we call

12
00:01:20,759 --> 00:01:27,630
the greatest common factor. Now let&#39;s try two terms that include variables: 12x and

13
00:01:27,630 --> 00:01:34,890
20x-squared. First let&#39;s focus on the coefficients, 12 and 20. 1 is a common

14
00:01:34,890 --> 00:01:41,280
factor for both of these. 2 is also a common factor of 12 and 20. 3 is a factor

15
00:01:41,280 --> 00:01:46,829
of 12, but not of 20; but 4 is a common factor -  in fact it&#39;s the greatest common

16
00:01:46,829 --> 00:01:54,450
factor. Now let&#39;s look at the variable expression: x times 1 is x, and x times x

17
00:01:54,450 --> 00:02:01,560
is x-squared; so X is a common factor. In fact for the complete terms, 4x is the

18
00:02:01,560 --> 00:02:06,299
greatest common factor. As a general rule, the greatest common factor for variable

19
00:02:06,299 --> 00:02:09,660
expressions like these is always the greatest common factor of the

20
00:02:09,660 --> 00:02:13,690
coefficients, times the variable expression with the lowest exponent

21
00:02:13,690 --> 00:02:18,970
order. Well now that you know about factoring, there are some tricks that you

22
00:02:18,970 --> 00:02:23,590
can use to manipulate expressions and equations. For example, let&#39;s look at this

23
00:02:23,590 --> 00:02:31,690
expression: 9x + 12y. The greatest common factor of 9 and 12 is 3, so we could

24
00:02:31,690 --> 00:02:38,920
rewrite this expression like this: 3 times 3x plus 3 times 4y. Now remember

25
00:02:38,920 --> 00:02:42,700
the distributive property? We can use that to refactor the expression like

26
00:02:42,700 --> 00:02:49,450
this: 3 times 3x + 4y. Techniques like this can be really useful when you need

27
00:02:49,450 --> 00:02:54,660
to isolate or combine terms in an equation in order to solve it.

