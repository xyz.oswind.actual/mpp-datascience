0
00:00:00,140 --> 00:00:04,740
Let&#39;s start with some fundamental algebra. Now for many of you this module

1
00:00:04,740 --> 00:00:08,099
may be revision, in which case you can feel free to jump straight to the module

2
00:00:08,099 --> 00:00:11,969
assessment. However, it can be useful to spend a little bit of time on the basics -

3
00:00:11,969 --> 00:00:15,210
even if you&#39;ve already studied them, there may be something new to learn or a

4
00:00:15,210 --> 00:00:17,760
different way of thinking about something that will help strengthen the

5
00:00:17,760 --> 00:00:21,109
mathematical foundations that we&#39;re going to build on later in this course.

6
00:00:21,109 --> 00:00:26,699
OK, let&#39;s look at an equation. Now you can tell it&#39;s an equation because it

7
00:00:26,699 --> 00:00:31,679
equates two expressions. The expression on the left of the equal sign is equal to

8
00:00:31,679 --> 00:00:37,950
the expression on the right. Now in this case there is a variable called x. This

9
00:00:37,950 --> 00:00:41,190
is something for which we don&#39;t know the value. we&#39;re going to try and solve this

10
00:00:41,190 --> 00:00:46,860
equation for x; in other words figure out the value of x. Our x variable has a

11
00:00:46,860 --> 00:00:51,660
coefficient:  a number by which it&#39;s multiplied. In this case we have 2 times

12
00:00:51,660 --> 00:00:59,969
x. Then we have a couple of constants: a 3 and a 9. To solve for x we need to isolate

13
00:00:59,969 --> 00:01:04,830
the x-term; In this case 2x. So we need to get rid of the other terms on that side.

14
00:01:04,830 --> 00:01:09,930
In this case we have a plus 3 on the left side, and we can remove that by

15
00:01:09,930 --> 00:01:15,360
applying the opposite operation: subtracting 3. However we always need to

16
00:01:15,360 --> 00:01:20,720
balance the equation, so whatever we do on one side we must also do on the other.

17
00:01:20,720 --> 00:01:26,070
The plus 3 and the negative 3 on the left side cancel each other out leaving

18
00:01:26,070 --> 00:01:34,079
just our x-term; and on the right side, 9 minus 3 is 6. Now we still need to

19
00:01:34,079 --> 00:01:38,340
isolate x from its coefficient, and the opposite operation of multiplying by 2

20
00:01:38,340 --> 00:01:43,710
is 2 dividing by 2; so that&#39;s what we&#39;ll do. Of course what we do on the left,

21
00:01:43,710 --> 00:01:49,770
we must also do on the right. When we calculate out those divisions we get our

22
00:01:49,770 --> 00:01:53,540
answer for x: 3.

23
00:01:53,600 --> 00:01:57,380
To test our results we can plug that back into the original equation to

24
00:01:57,380 --> 00:02:04,490
verify it, so in this case our 2x becomes 2 times 3 which is 6, and 6 plus 3 is

25
00:02:04,490 --> 00:02:07,780
indeed 9.

