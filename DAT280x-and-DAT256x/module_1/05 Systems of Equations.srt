0
00:00:00,380 --> 00:00:05,279
Linear equations can be combined into systems of equations, which enable us to

1
00:00:05,279 --> 00:00:08,940
find values for all of the variables that satisfy all of the equations. Now

2
00:00:08,940 --> 00:00:14,880
this is useful if you need to find a specific pair of x and y values. Suppose

3
00:00:14,880 --> 00:00:18,560
for example you feel like a healthy snack, and you decide to buy some fruit.

4
00:00:18,560 --> 00:00:22,859
The fruit store has apples, which cost five cents, and oranges, which cost

5
00:00:22,859 --> 00:00:29,910
fifteen cents. Now let&#39;s say you spend 75 cents and you buy nine items of fruit. So

6
00:00:29,910 --> 00:00:34,290
how many apples and how many oranges did you buy? Well let&#39;s start with what we

7
00:00:34,290 --> 00:00:38,489
know. We know that you have an unknown number of apples, let&#39;s call that x; and

8
00:00:38,489 --> 00:00:43,579
an unknown number of oranges, which we&#39;ll call y; and in total we have nine items.

9
00:00:43,579 --> 00:00:48,059
We also know that our unknown number of five-cent apples and our unknown number

10
00:00:48,059 --> 00:00:54,750
of fifteen-cent oranges cost us a total of 75 cents. Now, our nine fruits could

11
00:00:54,750 --> 00:00:59,489
be nine apples and no oranges, or no apples and nine oranges, or any

12
00:00:59,489 --> 00:01:04,769
combination in between where y plus y is equal to nine. So here&#39;s a line for that

13
00:01:04,769 --> 00:01:12,270
equation. Similarly, 75 cents could buy you fifteen apples and no oranges, no apples

14
00:01:12,270 --> 00:01:17,220
and five oranges, or some other combination where 5x and 15y is equal to

15
00:01:17,220 --> 00:01:24,000
75. Now the lines from these equations only share one common point, where x is 6

16
00:01:24,000 --> 00:01:32,130
and y is 3; which indicates that you bought six apples and three oranges. We

17
00:01:32,130 --> 00:01:37,650
can also figure this out using what&#39;s called the elimination method. First we

18
00:01:37,650 --> 00:01:41,850
need to manipulate one of the equations so that the term we want to eliminate. in

19
00:01:41,850 --> 00:01:46,710
this case the x-term, cancels out the same term in the other equation. So to

20
00:01:46,710 --> 00:01:49,799
do this we&#39;re just going to multiply everything in the first equation by

21
00:01:49,799 --> 00:01:57,090
-5. Now we can add these two equations together: -5x plus 5x

22
00:01:57,090 --> 00:02:03,149
is 0 so that eliminates the x-term, and -5y plus 15y is 10y; and

23
00:02:03,149 --> 00:02:12,720
-45 plus 75 is 30. So if 10y equals 30, then y must be equal to 3.

24
00:02:12,720 --> 00:02:18,060
Now from our original equation, we know that x plus y is 9, so if y is 3 then x

25
00:02:18,060 --> 00:02:24,410
must be 6, which confirms you bought 6 apples and 3 oranges.

