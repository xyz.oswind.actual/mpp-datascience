0
00:00:01,460 --> 00:00:05,279
Up to this point all of our equations have included standard

1
00:00:05,279 --> 00:00:09,450
arithmetic operations, such as division, multiplication, addition, and subtraction.

2
00:00:09,450 --> 00:00:13,980
Now many real-world calculations involve exponential values in which numbers are

3
00:00:13,980 --> 00:00:21,720
raised by a specific power. So we know that 3 times 3 is 9, now that&#39;s the same

4
00:00:21,720 --> 00:00:29,010
thing as 3 to the power of 2; or 3-squared. Similarly 3 times 3 times 3 is

5
00:00:29,010 --> 00:00:35,520
27, and that&#39;s the same thing as 3 to the power of 3, or 3-cubed. Now this applies

6
00:00:35,520 --> 00:00:42,239
to any number; so for example to find 4 squared, we just simply multiply 4 by

7
00:00:42,239 --> 00:00:47,129
itself. We can also perform calculations in the opposite direction to find

8
00:00:47,129 --> 00:00:52,800
radicals, or roots. For example, the square root of 16 is the number that when

9
00:00:52,800 --> 00:00:59,969
multiplied with itself results in 16 -  which is we already know is 4. Now you

10
00:00:59,969 --> 00:01:04,830
can find the root of any non-negative number. Numbers less than 0 can&#39;t have

11
00:01:04,830 --> 00:01:08,130
a root, because you can&#39;t multiply a number by itself and get a negative

12
00:01:08,130 --> 00:01:12,930
result -  a positive times a positive is always a positive, and a negative times a

13
00:01:12,930 --> 00:01:22,020
negative is also always a positive. OK, so consider this. x in this case is the

14
00:01:22,020 --> 00:01:27,869
power to which we need to raise 4 in order to get the result 16. We call this

15
00:01:27,869 --> 00:01:34,860
kind of calculation a logarithm. In this case we&#39;re looking for log 4 of 16, which

16
00:01:34,860 --> 00:01:43,680
is 2; because 4 to the power of 2 (in other words squared) is 16. Let&#39;s take

17
00:01:43,680 --> 00:01:49,049
a look at a few rules for working with exponential values. First of all, when we

18
00:01:49,049 --> 00:01:54,810
add terms of the same power, we just need to add the coefficients -  so x squared is

19
00:01:54,810 --> 00:01:59,490
just shorthand for 1x-squared, and when we add that to 3x-squared we get 4x-squared.

20
00:01:59,490 --> 00:02:07,320
So what about multiplication? Well to multiply exponentials, just

21
00:02:07,320 --> 00:02:10,410
multiply the coefficients as you normally would, and then add the

22
00:02:10,410 --> 00:02:17,010
exponents. So to calculate 2x-cubed times 4x-squared, well 2 times 4 is 8

23
00:02:17,010 --> 00:02:25,950
and 3 plus 2 is 5 so we get 8x to the power of 5. Division is the opposite of

24
00:02:25,950 --> 00:02:30,780
multiplication, so to divide one exponential by another, just divide the

25
00:02:30,780 --> 00:02:36,000
coefficients and subtract the exponents. So in this case, 6 divided by 3 is 2, and

26
00:02:36,000 --> 00:02:44,000
5 minus 2 is 3; so our result is 2x to the third power, or 2x-cubed.

