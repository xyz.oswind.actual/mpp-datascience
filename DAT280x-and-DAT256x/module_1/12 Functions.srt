0
00:00:01,040 --> 00:00:05,670
So far we&#39;ve explored equations that perform algebraic operations to produce

1
00:00:05,670 --> 00:00:10,410
one or more results. A function is a way of encapsulating an operation that takes

2
00:00:10,410 --> 00:00:17,190
an input and produces exactly one output. Here&#39;s a function called f that takes in

3
00:00:17,190 --> 00:00:23,340
an input variable called x and returns x plus 2. So if we use this function with

4
00:00:23,340 --> 00:00:29,760
an x value of 3 to set the value of a variable called y, then of course y is

5
00:00:29,760 --> 00:00:36,180
set to 5, Now we could plot this function to show the output value for every

6
00:00:36,180 --> 00:00:41,550
possible input value. In this case, the function is a line in which y is always

7
00:00:41,550 --> 00:00:49,200
at the point X plus 2. OK, let&#39;s look at a different function. This time, the

8
00:00:49,200 --> 00:00:56,969
function is called g and g of x is 10 divided by x. So G of 5 is 10 divided by

9
00:00:56,969 --> 00:01:03,030
5, which is 2. However what happens if we try and use the function with an x value

10
00:01:03,030 --> 00:01:09,570
of 0? Well anything divided by zero is undefined, so the function is undefined

11
00:01:09,570 --> 00:01:15,119
for an x value of 0. Now we call the set of numbers for which the function is

12
00:01:15,119 --> 00:01:21,150
defined its domain, and in this case the domain of function g is defined with x

13
00:01:21,150 --> 00:01:27,299
being in the set of all real numbers such that x is not equal to 0. Less

14
00:01:27,299 --> 00:01:33,119
formally you might see this written in a function definition like this. When we

15
00:01:33,119 --> 00:01:38,220
plot this function the value of y for every possible x is shown, but note that

16
00:01:38,220 --> 00:01:42,810
as x gets closer to 0, the values of y never meet because the function is not

17
00:01:42,810 --> 00:01:50,009
defined for an x value of 0. All right, Let&#39;s look at another function. Function

18
00:01:50,009 --> 00:01:56,399
h returns x-squared. Again let&#39;s look at the graph for that function, and note

19
00:01:56,399 --> 00:02:00,540
that when you multiply any number by itself the result is always 0 or greater -

20
00:02:00,540 --> 00:02:05,969
you can&#39;t square a number and get a negative value. So the possible outputs

21
00:02:05,969 --> 00:02:10,259
for function h, which we call its range, is defined as being the set of all

22
00:02:10,258 --> 00:02:16,480
real numbers such that h of  x is greater than or equal to 0.

