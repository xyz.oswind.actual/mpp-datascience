0
00:00:00,589 --> 00:00:04,980
Now there are some particularly useful ways that you can employ factoring to deal

1
00:00:04,980 --> 00:00:08,790
with expressions that contain squared values; that is values with an

2
00:00:08,790 --> 00:00:16,260
exponential of two. Suppose we need to subtract 4 from x-squared. Now 4 is

3
00:00:16,260 --> 00:00:20,660
the same thing as 2-squared, so we could rewrite this equation like this.

4
00:00:20,660 --> 00:00:24,660
Now to subtract one square from another you can use a technique called &quot;the

5
00:00:24,660 --> 00:00:28,410
difference of squares&quot; and refactor the expression like this,

6
00:00:28,410 --> 00:00:33,239
removing the exponentials. A perfect square is the product of a number

7
00:00:33,239 --> 00:00:38,250
multiplied by itself, so for example 4 times 4 is 16, so 16 is a perfect

8
00:00:38,250 --> 00:00:44,579
square. Look at this expression, which includes a variable x and a coefficient

9
00:00:44,579 --> 00:00:49,530
and constant, of which 3 is the common factor. We could rewrite of this

10
00:00:49,530 --> 00:00:56,219
expression like this: x plus 3 times x plus 3. Well that&#39;s a number multiplied by

11
00:00:56,219 --> 00:01:01,250
itself, in other words it&#39;s a perfect square; so we could rewrite it like this

12
00:01:01,250 --> 00:01:08,010
x plus 3 all-squared. Now there&#39;s a general formula for refactoring perfect

13
00:01:08,010 --> 00:01:12,930
squares in this format, and it&#39;s worth memorizing this. The formula is a plus b-

14
00:01:12,930 --> 00:01:18,330
squared is always equal to a-squared plus b-squared plus 2ab; and you&#39;re

15
00:01:18,330 --> 00:01:21,479
going to find this really useful when you come to solve quadratic equations, which

16
00:01:21,479 --> 00:01:26,070
we&#39;ll cover later. In the meantime, let&#39;s just run through an example to prove to

17
00:01:26,070 --> 00:01:30,720
ourselves that works; and in this case for a we&#39;ll use the value 2 instead of x,

18
00:01:30,720 --> 00:01:38,310
and we&#39;ll leave b with the value 3 like we had before. 2-squared is 4, 3-squared

19
00:01:38,310 --> 00:01:46,680
is 9, and 2ab is 2 times 2 times 3. Well we can easily work out that our 2 plus 3

20
00:01:46,680 --> 00:01:53,759
all-squared is 5-squared, and 2 times 2 times 3 is 12. Now do you remember our

21
00:01:53,759 --> 00:02:00,570
original equation? x-squared plus 6x plus 9, which we factored as x plus 3 all-squared.

22
00:02:00,570 --> 00:02:06,060
So the a component was x, and the b component was 3. Well with an x

23
00:02:06,060 --> 00:02:13,800
value of 2, x-squared is 4 and 6x is 12; so you can see the formula holds true.

24
00:02:13,800 --> 00:02:19,530
For a value of x, we&#39;ve got exactly what we had with our original formula. Now of

25
00:02:19,530 --> 00:02:22,980
course, when we&#39;ve got the known values for both a and b, we can actually finish

26
00:02:22,980 --> 00:02:26,250
off both sides of the equation and see that the two expressions are indeed

27
00:02:26,250 --> 00:02:31,400
equal -  in this case our perfect square turns out to be 25.

