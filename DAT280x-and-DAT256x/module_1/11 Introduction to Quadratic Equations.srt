0
00:00:00,230 --> 00:00:04,980
We&#39;ve already seen some examples of equations that include squared terms, where

1
00:00:04,980 --> 00:00:09,990
the highest order term is a square. These equations behave in a particular way, and

2
00:00:09,990 --> 00:00:13,250
we call them quadratic equations. They have some pretty interesting

3
00:00:13,250 --> 00:00:18,779
characteristics. Here&#39;s a quadratic equation in which y is equal to 2x-squared

4
00:00:18,779 --> 00:00:25,350
plus x minus 3. Now if we plot the line for this equation, it forms a

5
00:00:25,350 --> 00:00:30,240
parabola; which is characteristic of quadratic equations. In this case, the

6
00:00:30,240 --> 00:00:35,219
parabola is open at the top and it closes at the bottom. Well here&#39;s a

7
00:00:35,219 --> 00:00:39,210
similar quadratic equation, but this time the x-squared term is a negative value;

8
00:00:39,210 --> 00:00:44,610
and when we plot this equation we still get a parabola, but this time it&#39;s open

9
00:00:44,610 --> 00:00:50,129
at the bottom and it&#39;s closed at the top. The point at which the parabola reaches

10
00:00:50,129 --> 00:00:53,940
its maximum or minimum y value (depending on whether it&#39;s downward opening or

11
00:00:53,940 --> 00:01:00,449
upward opening) is called its vertex. The vertex is located at the x value of the

12
00:01:00,449 --> 00:01:06,390
parabolas line of symmetry. In this case the line of the equation crosses the x

13
00:01:06,390 --> 00:01:11,070
axis, and because it&#39;s a parabola it crosses it twice -  so that are two

14
00:01:11,070 --> 00:01:16,530
x-intercepts at the point where y equals 0. Now it&#39;s possible for the parabola

15
00:01:16,530 --> 00:01:21,000
not to cross the x axis at all, and therefore have no x-intercepts, but

16
00:01:21,000 --> 00:01:25,590
regardless of whether it intersects the x axis or not; for every Y value there are

17
00:01:25,590 --> 00:01:30,950
two x values, and they&#39;re equidistant from the line of symmetry.

