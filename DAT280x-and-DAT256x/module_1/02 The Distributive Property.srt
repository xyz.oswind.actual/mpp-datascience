0
00:00:00,439 --> 00:00:04,620
One helpful thing to be aware of and working on equations is something called

1
00:00:04,620 --> 00:00:09,300
the distributive property. Let&#39;s take a look at how that works. Now the

2
00:00:09,300 --> 00:00:13,080
distributive property is a mathematical law that lets us distribute the same

3
00:00:13,080 --> 00:00:19,109
operation to terms within parentheses. For example, in this equation we&#39;re

4
00:00:19,109 --> 00:00:26,250
multiplying 3 times x plus 2. Well that&#39;s the same as adding 3 times x to 3 times 2,

5
00:00:26,250 --> 00:00:31,949
which is of course 6; so once we&#39;ve done that we can use the opposite operation to get

6
00:00:31,949 --> 00:00:37,950
rid of their constant 6, and then carry that through on both sides. And now we

7
00:00:37,950 --> 00:00:45,170
can just divide both sides by 3 to find x, which it turns out is 4.

