0
00:00:01,159 --> 00:00:05,400
Some of the equations we&#39;ve looked at so far include expressions that are

1
00:00:05,400 --> 00:00:10,800
actually polynomials. But what is a polynomial, and why should you care?

2
00:00:10,800 --> 00:00:15,900
So, the word polynomial just means &quot;many terms&quot;, and a polynomial is an algebraic

3
00:00:15,900 --> 00:00:21,410
expression that contains constants, variables with coefficients, and

4
00:00:21,410 --> 00:00:26,849
exponentials with a positive exponent. The terms can be combined using any

5
00:00:26,849 --> 00:00:31,949
arithmetic operation other than division by a variable, but we usually like to

6
00:00:31,949 --> 00:00:35,969
express polynomials in what&#39;s called &quot;standard form&quot;, with the highest

7
00:00:35,969 --> 00:00:40,290
exponentials first, followed by non exponential variables, and finally

8
00:00:40,290 --> 00:00:45,000
constants. When you rearrange the terms in a polynomial, you&#39;ve got to remember

9
00:00:45,000 --> 00:00:49,230
to include the preceding sign; so in this case we needed to remember that the

10
00:00:49,230 --> 00:00:55,980
exponential term is negative 3x squared. Now you can simplify complex polynomials

11
00:00:55,980 --> 00:01:02,219
by combining like-terms, so first we&#39;ll rearrange this expression so the like-terms

12
00:01:02,219 --> 00:01:08,369
are next to each other, and then we&#39;ll combine those terms arithmetically.

