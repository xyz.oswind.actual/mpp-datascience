0
00:00:00,319 --> 00:00:04,440
So we&#39;ve seen how a linear equation describes a line that can be plotted in

1
00:00:04,440 --> 00:00:09,269
terms of x and y coordinates, let&#39;s take a closer look at the line described by

2
00:00:09,269 --> 00:00:14,130
our equation. Note that the line extends to any

3
00:00:14,130 --> 00:00:18,900
possible pair of x and y values that can be calculated by the equation, including

4
00:00:18,900 --> 00:00:24,060
negative values and 0. Now the point where the line crosses the x axis is

5
00:00:24,060 --> 00:00:29,070
called the x-intercept, and the point where the line crosses the y axis is

6
00:00:29,070 --> 00:00:35,700
called the y-intercept. At the x-intercept the value of y is 0, and at

7
00:00:35,700 --> 00:00:41,969
the y-intercept the value of x is 0. So to calculate the x-intercept, we just

8
00:00:41,969 --> 00:00:46,700
need to solve the equation for x with a y-value of 0;

9
00:00:47,100 --> 00:00:51,839
and to calculate the y-intercept, we solve the equation for y with an x value

10
00:00:51,839 --> 00:00:59,730
of 0. So our x-intercept, let&#39;s call it a, is -0.5; and our y-intercept,

11
00:00:59,730 --> 00:01:06,630
let&#39;s call it b,  is 1. The slope of the line is calculated as the change in y

12
00:01:06,630 --> 00:01:12,810
divided by the change in x as we move along the line. Now by convention we call

13
00:01:12,810 --> 00:01:16,950
the slope m, and because mathematicians love using Greek symbols for things, in

14
00:01:16,950 --> 00:01:23,100
this case the Greek letter Delta is used to indicate change. We actually calculate

15
00:01:23,100 --> 00:01:27,360
the Delta by taking two points on the line and subtracting the x and y values

16
00:01:27,360 --> 00:01:32,159
of the second point from the first. We can use any two points on the line to do

17
00:01:32,159 --> 00:01:37,500
this, including the x and y intercepts that we calculated previously. In this

18
00:01:37,500 --> 00:01:46,920
case the slope is 1 over 0.5. Well 1 divided by 0.5 is 2; so if we go along

19
00:01:46,920 --> 00:01:53,880
one unit on the x axis, we go up two units on the y axis. Now let&#39;s return to

20
00:01:53,880 --> 00:01:59,909
our original equation. We usually like to arrange the terms in an equation so that

21
00:01:59,909 --> 00:02:03,869
the terms with variables come before constants, so we&#39;ll just swap these terms

22
00:02:03,869 --> 00:02:09,869
around to tidy that up. Now take a close look and note that the calculation for y

23
00:02:09,869 --> 00:02:16,799
is actually the slope multiplied by x plus the y-intercept. More generally this

24
00:02:16,799 --> 00:02:21,239
is known as the slope-intercept form of the equation, and it includes all the

25
00:02:21,239 --> 00:02:26,760
information necessary to draw the line. We can use the y intercept (or b) to find

26
00:02:26,760 --> 00:02:31,290
the point where x is 0, and we can use the slope to find a second point that is

27
00:02:31,290 --> 00:02:36,200
any number of units on the x-axis away from the first point.

