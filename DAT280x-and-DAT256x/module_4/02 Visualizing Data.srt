0
00:00:00,030 --> 00:00:04,770
Data visualization is one of the key ways in which we can examine data and

1
00:00:04,770 --> 00:00:08,970
get insights from it. If a picture&#39;s worth a thousand words, then a good graph

2
00:00:08,970 --> 00:00:14,940
or chart is worth any number of tables of data. Bar charts are a great way to

3
00:00:14,940 --> 00:00:19,650
compare numeric qualities or kinds, by categories. For example, this bar chart

4
00:00:19,650 --> 00:00:22,769
shows the kind of students for each of the three experience levels in our

5
00:00:22,769 --> 00:00:28,590
student data set. A histogram is a common type of chart in statistics, and it

6
00:00:28,590 --> 00:00:32,099
looks pretty similar to a bar chart; but it&#39;s used to show the comparative

7
00:00:32,098 --> 00:00:36,620
frequency of continuous quantitative values grouped into ranges known as bins.

8
00:00:36,620 --> 00:00:41,070
This histogram shows the distribution of student grades. The most frequently

9
00:00:41,070 --> 00:00:46,680
occurring grades are in the range 43 to 54, while grades between 2 and 12 or 85

10
00:00:46,680 --> 00:00:53,010
to 95 occur much less frequently. Pie charts are popular with business

11
00:00:53,010 --> 00:00:57,329
managers as a way to compare relative quantities by category. They&#39;re less

12
00:00:57,329 --> 00:01:00,840
commonly used by data scientists, but occasionally they can be useful to see

13
00:01:00,840 --> 00:01:07,200
how categories compare as a proportion of the entire data set. Scatter plots are

14
00:01:07,200 --> 00:01:10,950
used to compare quantitative data values, and they can be useful for finding

15
00:01:10,950 --> 00:01:15,780
relationships between fields in your data. This plot shows time studied on the

16
00:01:15,780 --> 00:01:20,520
x-axis and student grades on the y-axis, with the plotted points showing the

17
00:01:20,520 --> 00:01:26,369
intersection of time versus grade for each student. If you examine the data, you

18
00:01:26,369 --> 00:01:29,189
can discern an overall trend in which the students who spend more time

19
00:01:29,189 --> 00:01:35,610
studying tended to score higher grades. Line charts are used to show trends,

20
00:01:35,610 --> 00:01:40,590
usually over time. For example, suppose our student data included the date that

21
00:01:40,590 --> 00:01:44,579
the student enrolled in the class. We could use a line chart to show course

22
00:01:44,579 --> 00:01:50,100
enrollment over a period of time, like this. These are just some of the kinds of

23
00:01:50,100 --> 00:01:54,180
charts that you can use to visualize your data; making easier to explore

24
00:01:54,180 --> 00:01:58,969
trends and relationships that might help you understand it better.

