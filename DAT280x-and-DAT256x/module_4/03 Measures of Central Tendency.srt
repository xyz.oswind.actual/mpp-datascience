0
00:00:00,000 --> 00:00:04,830
A great deal of statistical analysis is based on the way that data values are

1
00:00:04,830 --> 00:00:10,050
distributed within the data set. The term &quot;measures of central tendency&quot; sounds a

2
00:00:10,050 --> 00:00:13,349
bit grand, but really is just a fancy way of saying that were interested in

3
00:00:13,349 --> 00:00:18,480
knowing where the middle value in our data is. Let&#39;s take a look at our student

4
00:00:18,480 --> 00:00:24,180
data. In particular, we&#39;ll explore the age of our students. Now the youngest

5
00:00:24,180 --> 00:00:29,130
student in the sample is 22 and the oldest is 48; all the others have ages

6
00:00:29,130 --> 00:00:32,610
somewhere in between these extremes, and we should expect that a typical student

7
00:00:32,610 --> 00:00:37,649
age is somewhere in this middle range. Now one value we can use to determine

8
00:00:37,649 --> 00:00:41,040
the middle of our data is the simple average, or the mean as it&#39;s more

9
00:00:41,040 --> 00:00:45,510
commonly referred to in statistics. Here&#39;s the formula for calculating the

10
00:00:45,510 --> 00:00:49,590
mean for a full population of data, and note that in this case the mean is

11
00:00:49,590 --> 00:00:54,329
represented by the Greek letter Mu. However, we&#39;re dealing with a sample of

12
00:00:54,329 --> 00:00:58,140
the data, not the whole population, so we write the formula a little differently.

13
00:00:58,140 --> 00:01:04,080
We write it like this and we call it x-bar. Either way, what the formula is

14
00:01:04,080 --> 00:01:08,490
telling you to do is to add together all of the individual data values, and divide

15
00:01:08,490 --> 00:01:13,140
by the total number of data values that there are. So in this case the ages

16
00:01:13,140 --> 00:01:18,659
total up to 166, and there are 6 ages in the sample, so we divide by 6 to get a

17
00:01:18,659 --> 00:01:25,610
mean age of around 27.67; which is about here on our range of data.

18
00:01:25,610 --> 00:01:29,970
Another way we can define the middle of the data is to calculate the median

19
00:01:29,970 --> 00:01:35,220
value; in other words the value that&#39;s at the middle position if we sort our data

20
00:01:35,220 --> 00:01:40,259
in order. If there&#39;s an odd number of observations in the data set, the median

21
00:01:40,259 --> 00:01:43,590
is the value of the one in the middle position, which you find using this

22
00:01:43,590 --> 00:01:47,880
formula. Just the total number of observations in the ordered set plus one,

23
00:01:47,880 --> 00:01:53,490
divided by two. For an even number of observations, you need to find the middle

24
00:01:53,490 --> 00:01:59,909
two values and take the average using this formula. In our case, we have an even

25
00:01:59,909 --> 00:02:04,770
number of observations -  our n value is 6; so we find the middle two whose

26
00:02:04,770 --> 00:02:11,099
positions are n divided by two, so position 3; and n divided by 2 plus 1, so

27
00:02:11,099 --> 00:02:17,690
position 4. The average of 23 and 24 is 23.5, so

28
00:02:17,690 --> 00:02:23,840
that&#39;s our median value. Finally we can look at the mode, which is simply the

29
00:02:23,840 --> 00:02:28,640
value that occurs most frequently; and in this case there are two instances of

30
00:02:28,640 --> 00:02:36,130
students with the age of 23, and only one of every other value -  so that&#39;s our mode.

31
00:02:36,130 --> 00:02:40,430
Now let&#39;s suppose we had a few more observations, and we create a histogram

32
00:02:40,430 --> 00:02:45,580
that shows the frequency of each age value. It might look something like this.

33
00:02:45,580 --> 00:02:50,299
Now note that by definition the mode has the highest frequency -  most of the data

34
00:02:50,299 --> 00:02:54,709
is bunched up on the left side, but are a small number of unusually high ages

35
00:02:54,709 --> 00:03:00,290
that&#39;s pulling the mean to the right. The distribution of the data forms a skewed

36
00:03:00,290 --> 00:03:04,970
curve. In this case we describe it as being right-skewed, because there&#39;s a

37
00:03:04,970 --> 00:03:10,849
long tail to the right. Now let&#39;s take a look at another field in our data set.

38
00:03:10,849 --> 00:03:17,299
This time we&#39;ll examine the grades. Now the mean grade in this example is 49, the

39
00:03:17,299 --> 00:03:24,019
median is 50, and the mode is also 50. Now note that the central measurements are

40
00:03:24,019 --> 00:03:28,069
much closer together than the ones for age; and they&#39;re much closer to the

41
00:03:28,069 --> 00:03:33,170
middle of the overall range of values. With more observations we might end up

42
00:03:33,170 --> 00:03:38,540
with a distribution that looks like this. This time the distribution forms a

43
00:03:38,540 --> 00:03:42,200
bell-shaped curve, with the most frequent values clustered in the middle and

44
00:03:42,200 --> 00:03:46,010
symmetric tails of reducing frequency as you get closer to either end of the

45
00:03:46,010 --> 00:03:51,319
range. This is what statisticians term a normal distribution, and it occurs naturally

46
00:03:51,319 --> 00:03:55,840
a lot of the time when you have sufficient volumes of data.

