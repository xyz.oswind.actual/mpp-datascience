0
00:00:00,000 --> 00:00:04,110
As we discussed earlier, when working with statistics we usually base our

1
00:00:04,110 --> 00:00:08,880
calculations on a sample, and not the full population of data. That means we

2
00:00:08,880 --> 00:00:12,360
need to allow for some variation between the sample statistics and the true

3
00:00:12,360 --> 00:00:18,300
parameters of the full population. Let&#39;s try another statistical experiment, this

4
00:00:18,300 --> 00:00:22,920
time we&#39;re interested in students who have red hair. Now unlike in our previous

5
00:00:22,920 --> 00:00:26,880
experiment with green eyes, we don&#39;t know the probability of a person having red

6
00:00:26,880 --> 00:00:31,590
hair. So we&#39;ve sampled 100 students and we&#39;ve counted the number of red heads

7
00:00:31,590 --> 00:00:36,540
and non red heads, and here&#39;s the resulting sample distribution. We&#39;ve

8
00:00:36,540 --> 00:00:40,140
labeled the count of students with red hair as 1, and the count of students

9
00:00:40,140 --> 00:00:44,670
with another hair color as 0. The proportion of students with red hair was

10
00:00:44,670 --> 00:00:49,710
34 out of the sample of a hundred, or 0.34; and we&#39;ll call that P for

11
00:00:49,710 --> 00:00:55,610
proportion; but because this is based on a sample, we&#39;ll use the notation p-hat.

12
00:00:55,610 --> 00:01:03,449
The remaining students can be calculated as 1 minus P, so 0.66. Since

13
00:01:03,449 --> 00:01:07,320
we&#39;ve labeled the two possible outcomes as numeric values of 0 and 1, we can

14
00:01:07,320 --> 00:01:13,950
calculate the mean -  it&#39;s actually just p-hat, and that&#39;s 0.34. So

15
00:01:13,950 --> 00:01:17,369
from that it would seem that out of 100 randomly sampled students, we should

16
00:01:17,369 --> 00:01:22,170
expect around 34 to have red hair. But is it really fair to base that on a single

17
00:01:22,170 --> 00:01:27,869
sample? What we could do to get more accurate results is to repeat the sample

18
00:01:27,869 --> 00:01:34,619
multiple times, likely getting different proportions each time. Then you can plot

19
00:01:34,619 --> 00:01:39,119
the sample proportions from each sample as a histogram to create a sampling

20
00:01:39,119 --> 00:01:42,840
distribution. In other words a distribution based on the means obtained

21
00:01:42,840 --> 00:01:47,460
from multiple samples. With enough samples, the central limit theorem will

22
00:01:47,460 --> 00:01:52,110
ensure that this produces a normal distribution. Its mean will be the mean

23
00:01:52,110 --> 00:01:56,759
of the sample means, which we assumed to be the population mean for the random

24
00:01:56,759 --> 00:02:00,899
variable - in this case the expected number of red heads in a 100 person

25
00:02:00,899 --> 00:02:06,479
random sample. We can also calculate the standard deviation, which we more

26
00:02:06,479 --> 00:02:09,509
properly refer to as the standard error when dealing with a probability

27
00:02:09,508 --> 00:02:13,260
distribution, to determine the amount of variance. In this

28
00:02:13,260 --> 00:02:17,930
case, it&#39;s just a little under 0.05.

