0
00:00:00,260 --> 00:00:04,770
You&#39;ll often want to compare data in your data set to see if you can discern

1
00:00:04,770 --> 00:00:10,410
trends or relationships. For example you might want to compare a quantitative

2
00:00:10,410 --> 00:00:14,549
variable, like grade, for two or more values of a qualitative variable, such as

3
00:00:14,549 --> 00:00:19,470
gender. In this case I&#39;ve used box and whisker plots to compare these, and I can

4
00:00:19,470 --> 00:00:22,830
see that the grades for male students have a wider variance and a lower median

5
00:00:22,830 --> 00:00:29,039
than for female students. Or maybe you want to use a scatterplot to compare two

6
00:00:29,039 --> 00:00:32,940
quantitative values, like student grades and the time spent on activities in the

7
00:00:32,940 --> 00:00:37,500
course. In this case, there appears to be a relationship between grade and time

8
00:00:37,500 --> 00:00:42,059
spent; and you can calculate a statistic named correlation to quantify that

9
00:00:42,059 --> 00:00:47,610
relationship as a value between negative 1 and 1. In this case the correlation

10
00:00:47,610 --> 00:00:51,690
looks to be positive, so the high grades tend to be matched with high activity

11
00:00:51,690 --> 00:00:57,360
times; so the correlation value is likely to be close to 1. Now it&#39;s important to

12
00:00:57,360 --> 00:01:00,899
note that we&#39;re not saying anything about causation here. We can observe that

13
00:01:00,899 --> 00:01:05,070
students with higher activity times tend to have higher grades, but we can&#39;t

14
00:01:05,069 --> 00:01:10,770
definitively say whether one is caused by the other. We can also draw a line of

15
00:01:10,770 --> 00:01:16,200
best fit through the scatterplot to try and define the relationship trend. To

16
00:01:16,200 --> 00:01:19,680
find this line, you can use the slope-intercept formula for a linear

17
00:01:19,680 --> 00:01:24,150
equation to define a line that minimizes the square of the difference between the

18
00:01:24,150 --> 00:01:28,259
plotted points and the line itself. This technique is called least squares

19
00:01:28,259 --> 00:01:32,430
regression, and it results in a formula that you can use to predict one value

20
00:01:32,430 --> 00:01:36,540
based on the other. For example, you can predict a student&#39;s grade based on the

21
00:01:36,540 --> 00:01:39,829
number of active hours they log.

