0
00:00:00,000 --> 00:00:03,899
Many of the problems we try to solve using statistics are to do with

1
00:00:03,899 --> 00:00:08,280
probability. For example what&#39;s the probable salary for a graduate who

2
00:00:08,280 --> 00:00:12,300
scored a given score in their final exam at school, or what&#39;s the likely height of a

3
00:00:12,300 --> 00:00:16,350
child given the height of his or her parents. To make sense of this kind of

4
00:00:16,350 --> 00:00:21,260
question, you need to understand some basic principles of probability.

5
00:00:21,260 --> 00:00:25,650
Probability is based on experiments, or trials, in which there&#39;s an uncertain

6
00:00:25,650 --> 00:00:30,869
outcome. For example ruling a die. Now assuming it&#39;s a regular six-faced die,

7
00:00:30,869 --> 00:00:38,190
The possible outcomes from a single roll are 1, 2, 3, 4, 5, or 6. This

8
00:00:38,190 --> 00:00:43,950
is known as the sample space for the experiment. An event is an instance of

9
00:00:43,950 --> 00:00:49,950
the experiment with a specific outcome -  for example rolling a six. There&#39;s exactly

10
00:00:49,950 --> 00:00:54,809
one sample point in our event space that satisfies that outcome, so assuming

11
00:00:54,809 --> 00:00:58,500
the die is fair, there&#39;s a one in six chance that the event will occur in any

12
00:00:58,500 --> 00:01:03,390
given trial roll of the die. Now more formally, we calculate probability as a

13
00:01:03,390 --> 00:01:08,400
value between zero and one. We use the letter P to denote probability, and we

14
00:01:08,400 --> 00:01:12,689
usually use an uppercase letter to represent the specific event. In this

15
00:01:12,689 --> 00:01:15,840
case we&#39;re trying to calculate the probability of ruling a six, which we&#39;ll

16
00:01:15,840 --> 00:01:21,090
call event A. To calculate the probability, we divide the number of

17
00:01:21,090 --> 00:01:25,290
sample points that result in the event, by the total number of sample points in

18
00:01:25,290 --> 00:01:31,189
this sample space. So in this case that&#39;s 1/6 for roughly

19
00:01:31,189 --> 00:01:34,680
0.167 which you could also express is sixteen

20
00:01:34,680 --> 00:01:40,170
point seven percent now the complement of an event is all the ways in which

21
00:01:40,170 --> 00:01:45,450
it&#39;s possible for that event not to occur there are five ways to not rule a

22
00:01:45,450 --> 00:01:50,100
sex out of our total sample space of six possible outcomes giving it a

23
00:01:50,100 --> 00:01:58,829
probability of 5 over 6 or 0.833 which is 83 point 3 percent now the important

24
00:01:58,829 --> 00:02:01,740
thing to note here is that the probability of an event and the

25
00:02:01,740 --> 00:02:06,710
probability of is compliment always add up to 1

