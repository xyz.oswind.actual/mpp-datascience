0
00:00:00,210 --> 00:00:04,859
When you plot the distribution of data in your data set, you usually find that

1
00:00:04,859 --> 00:00:10,290
the data values vary. Not everyone is the same height for example. Sometimes the

2
00:00:10,290 --> 00:00:15,660
values will vary widely, and other times they may be fairly close; and there are

3
00:00:15,660 --> 00:00:21,270
various measures that we can use to quantify that variance. Let&#39;s examine the

4
00:00:21,270 --> 00:00:28,470
distribution of some student grades. Suppose Kari scored a grade of 73. That

5
00:00:28,470 --> 00:00:31,650
seems like it might be a pretty good grade, but we need to compare it to her

6
00:00:31,650 --> 00:00:34,890
fellow students to get a sense of how well Kari has actually done in the

7
00:00:34,890 --> 00:00:37,200
course. Maybe the course was easy and most

8
00:00:37,200 --> 00:00:40,500
students actually scored 90 or more placing Kari among the poorest

9
00:00:40,500 --> 00:00:46,230
performers. Well one thing we can do is to rank the grades into percentiles. This

10
00:00:46,230 --> 00:00:49,649
tells us where a given grade lies in the distribution compared to all the other

11
00:00:49,649 --> 00:00:54,300
grades that were achieved. In this case Kari&#39;s grade is in the 80th percentile,

12
00:00:54,300 --> 00:01:00,359
so she scored a higher grade than 80 percent of the other students. Now often,

13
00:01:00,359 --> 00:01:04,049
instead of using individual percentiles, we divide the data distribution into

14
00:01:04,049 --> 00:01:09,720
quartiles that indicate the boundaries for 25 percent, 50 percent, 75 percent, and

15
00:01:09,720 --> 00:01:14,640
100 percent of the data. Often we visualize the quartiles of the

16
00:01:14,640 --> 00:01:19,320
distribution using a box and whiskers chart in which the box shows the inner

17
00:01:19,320 --> 00:01:24,900
two quartiles from 25 to 75 percent, with a line showing the 50 percent boundary,

18
00:01:24,900 --> 00:01:30,450
and the whiskers extend to include the first and fourth quartiles. Now note that

19
00:01:30,450 --> 00:01:34,320
that 50th percent boundary, where we have a line, is actually the median of

20
00:01:34,320 --> 00:01:38,310
the distribution; which is course makes sense, because by definition the median

21
00:01:38,310 --> 00:01:43,649
is the middle value. Box and whiskers plots can be shown in either orientation.

22
00:01:43,649 --> 00:01:51,780
It&#39;s very common to see them plotted vertically like this. The variance in our

23
00:01:51,780 --> 00:01:55,579
distribution determines how far from the mean the values in the distribution vary.

24
00:01:55,579 --> 00:02:00,149
For example, maybe most of our student grades are clumped together around the

25
00:02:00,149 --> 00:02:04,950
mean with only a few grades in the first and fourth quartile; or maybe the values

26
00:02:04,950 --> 00:02:08,369
are more evenly distributed across the entire range with only a small peak

27
00:02:08,369 --> 00:02:13,020
at the mean. We measure variance as the average squared

28
00:02:13,020 --> 00:02:16,860
difference of the values in the data set from the mean. We need to square the

29
00:02:16,860 --> 00:02:19,260
difference, otherwise the average difference from the mean would just be

30
00:02:19,260 --> 00:02:24,090
0. Now when we&#39;re working with a full population of data, we use the Greek

31
00:02:24,090 --> 00:02:28,140
letter Sigma-squared to indicate the variance, and we calculate it using this

32
00:02:28,140 --> 00:02:33,480
formula. When we&#39;re working with a sample, we call the variance s-squared and we

33
00:02:33,480 --> 00:02:38,460
calculate it slightly differently to allow for bias in the sample. Of course, a

34
00:02:38,460 --> 00:02:41,790
squared value means that the variance is not the same unit of measurement as our

35
00:02:41,790 --> 00:02:45,900
data; so we typically take the square root of the variance to calculate

36
00:02:45,900 --> 00:02:50,850
something known as the standard deviation. The standard deviation gives

37
00:02:50,850 --> 00:02:54,690
us a fixed unit of measurement from the mean that we can use to determine how

38
00:02:54,690 --> 00:03:00,690
much variance there is in the data. In a normal distribution, 68.26% of the data

39
00:03:00,690 --> 00:03:05,250
is within one standard deviation, plus or minus, of

40
00:03:05,250 --> 00:03:10,230
the mean. 95.45% of the data is within two

41
00:03:10,230 --> 00:03:17,270
standard deviations, and 99.73% of the data is within three standard deviations.

42
00:03:17,270 --> 00:03:24,480
So here&#39;s Kari&#39;s grade of 73. We can use the standard deviation to measure how

43
00:03:24,480 --> 00:03:27,890
far this varies from the mean by calculating what&#39;s called a z-score,

44
00:03:27,890 --> 00:03:33,450
using the appropriate formula for the full population or a sample. In this case,

45
00:03:33,450 --> 00:03:39,500
Kari&#39;s score is around one point three standard deviations above the mean.

