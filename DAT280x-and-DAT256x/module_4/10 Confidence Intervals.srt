0
00:00:00,000 --> 00:00:05,520
A confidence interval is a range of values around a sample statistic within

1
00:00:05,520 --> 00:00:10,740
which we&#39;re confident that the true parameter lies. Previously we collected

2
00:00:10,740 --> 00:00:14,340
multiple samples of a hundred students to determine the proportion of students

3
00:00:14,340 --> 00:00:18,710
with red hair, and we determined a sampling mean or an expected value of

4
00:00:18,710 --> 00:00:25,890
0.3175. In other words, for any sample of 100 students we expect around 32

5
00:00:25,890 --> 00:00:30,210
students to have red hair, with some variance. Sometimes there will be fewer sometimes,

6
00:00:30,210 --> 00:00:33,989
there will be more; but on average we expect around 32 redheads per 100

7
00:00:33,989 --> 00:00:39,719
students. Now, let&#39;s ask a slightly different question. What&#39;s the range

8
00:00:39,719 --> 00:00:44,700
within which we&#39;re confident the number of redheads will be in 95% of random

9
00:00:44,700 --> 00:00:52,739
samples? Well the mean, or expected value, is 0.3175; so we just need to calculate

10
00:00:52,739 --> 00:00:57,180
a margin of error above or below that value to define our 95% confidence

11
00:00:57,180 --> 00:01:01,739
interval. Now the central limit theorem has resulted in a normal distribution

12
00:01:01,739 --> 00:01:06,420
for our variable, so we can use a table of z-scores to determine the number of

13
00:01:06,420 --> 00:01:10,890
standard deviations above and below the mean within which 95% of the data falls,

14
00:01:10,890 --> 00:01:15,570
and then multiply by the standard deviation for our distribution. In a

15
00:01:15,570 --> 00:01:22,830
normal distribution, the z-score for 95% is 1.96; so our margin of error is plus or

16
00:01:22,830 --> 00:01:29,369
minus0.0466 times 1.96, which gives our confidence interval within which the mean will be in 95% of samples.

