0
00:00:00,030 --> 00:00:03,870
Now that we know something about probability, let&#39;s apply that to

1
00:00:03,870 --> 00:00:09,510
statistics. Statistics is about inferring measures for a full population based on

2
00:00:09,510 --> 00:00:13,710
samples, allowing for random variation. So we&#39;re going to have to consider the idea

3
00:00:13,710 --> 00:00:19,470
of a random variable. Now a random variable is a number that can vary in value.

4
00:00:19,470 --> 00:00:24,920
For example, the temperature on a given day or the number of students taking a class.

5
00:00:25,099 --> 00:00:28,949
Suppose you decide to conduct an experiment based on the eye color of

6
00:00:28,949 --> 00:00:33,980
students taking this class. Now you know that around 2% of the global population

7
00:00:33,980 --> 00:00:39,360
has green eyes, so all things being equal, the probability of a student chosen at

8
00:00:39,360 --> 00:00:46,920
random having green eyes is 2% or 0.02 - we&#39;ll call that p. Logically therefore, the

9
00:00:46,920 --> 00:00:51,289
probability of a student NOT having green eyes is 1 minus P; in this case

10
00:00:51,289 --> 00:00:57,449
0.98. Now suppose we randomly select one student and check their eye color to

11
00:00:57,449 --> 00:01:01,469
see if they have green eyes. The result is either going to be success -  they have

12
00:01:01,469 --> 00:01:06,540
green eyes; or failure -  they have some other eye color. Now what if we

13
00:01:06,540 --> 00:01:11,700
randomly check two students. Now there are four possible combinations of

14
00:01:11,700 --> 00:01:15,869
outcome. Both students might have green eyes, only the first student might have

15
00:01:15,869 --> 00:01:20,280
green eyes, only the second student might have green eyes, or neither student might

16
00:01:20,280 --> 00:01:25,259
have green eyes. If we add a third student to our sample, there are even

17
00:01:25,259 --> 00:01:30,630
more possible combinations. The number of students with green eyes from a sample

18
00:01:30,630 --> 00:01:35,729
of 3 is a random variable. To be more formal, it&#39;s an example of something

19
00:01:35,729 --> 00:01:39,540
called a binomial variable, which can be defined as the number of successful

20
00:01:39,540 --> 00:01:44,070
outcomes of a non-dependent experiment with two possible outcomes; success or

21
00:01:44,070 --> 00:01:50,909
failure. Let&#39;s call it X. So how do we calculate the probability for a given

22
00:01:50,909 --> 00:01:55,320
value of x -  for example what&#39;s the probability that two out of three

23
00:01:55,320 --> 00:02:01,409
students that we sample will have green eyes? For x to be 2, we need two out of

24
00:02:01,409 --> 00:02:05,009
our three sampled students to have green eyes, and the remaining one not to have

25
00:02:05,009 --> 00:02:10,020
green eyes. Well we know that the probability of one student having green

26
00:02:10,020 --> 00:02:15,180
eyes is p, which is 0.02; so the probability of two students having green

27
00:02:15,180 --> 00:02:20,580
eyes is p times p, or p-squared. And we know that the probability of a student

28
00:02:20,580 --> 00:02:25,650
NOT having green eyes is 1 minus p; and since there&#39;s only one more student in

29
00:02:25,650 --> 00:02:30,870
our sample, we&#39;ll take that to the power of 1. why have I bothered to show this

30
00:02:30,870 --> 00:02:34,920
raise to the power of 1? Well it helps us when we have to look at the general

31
00:02:34,920 --> 00:02:39,750
formula for this calculation. In this example, we&#39;re trying to find 2 successes

32
00:02:39,750 --> 00:02:44,550
out of 3 trials; which we could generify by saying k successes out of n trials.

33
00:02:44,550 --> 00:02:49,380
So our formula for calculating the probability that a single

34
00:02:49,380 --> 00:02:54,920
combination of n students includes k students with green eyes looks like this.

35
00:02:54,920 --> 00:03:03,180
So for a k value of 2 and an n value of 3, we calculate this; which works out to

36
00:03:03,180 --> 00:03:10,470
be just under 0.0004. However we also need to take into account the fact that

37
00:03:10,470 --> 00:03:14,519
there are multiple ways this could happen. To find out how many combinations

38
00:03:14,519 --> 00:03:18,630
there are that satisfied k students out of n, we use a technique called

39
00:03:18,630 --> 00:03:25,250
n choose K. It&#39;s sometimes written like this, and it&#39;s calculated like this.

40
00:03:25,250 --> 00:03:31,410
The exclamation points denote factorials, so for example 3 factorial would be 3

41
00:03:31,410 --> 00:03:40,380
times 2 times 1. In this case, n is 3 and K is 2; so the factorials work out as 3

42
00:03:40,380 --> 00:03:46,620
times 2 times 1 over 2 times 1 multiplied by 1; which all works out to

43
00:03:46,620 --> 00:03:53,370
be 6 over 2, or just 3. And if we check the sample space for X, sure enough there

44
00:03:53,370 --> 00:03:59,280
are 3 sample points that contain 2 green eyed students. So the probability of any

45
00:03:59,280 --> 00:04:02,549
of these combinations occurring is 3 times the probability of any one

46
00:04:02,549 --> 00:04:11,220
combination, which is 0.001176; or just a little under 1.2 percent. So when we put

47
00:04:11,220 --> 00:04:16,500
everything together we get this formula, which is known as the general binomial

48
00:04:16,500 --> 00:04:20,880
probability formula. You can use this to calculate the probability mass function,

49
00:04:20,880 --> 00:04:26,220
or PMF, of a binomial variable and determine the distribution of possible

50
00:04:26,220 --> 00:04:29,939
values based on their probability. With a large enough value for n,

51
00:04:29,939 --> 00:04:34,500
a phenomenon known as the central limit theorem causes the distribution of

52
00:04:34,500 --> 00:04:38,849
the PMF values to resemble a normal distribution, with the mean referred to

53
00:04:38,849 --> 00:04:42,889
as the expected value for the variable.

