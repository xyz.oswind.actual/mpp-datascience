0
00:00:00,380 --> 00:00:05,460
Suppose I want to evaluate how much you&#39;re enjoying this course. Now I might ask

1
00:00:05,460 --> 00:00:11,010
all students to rate the course on a scale of -5 to 5. Now all things

2
00:00:11,010 --> 00:00:14,490
being equal, you might expect that the mean rating will lie in the middle of

3
00:00:14,490 --> 00:00:19,619
all the possible values; so I would get a mean rating of 0. However I might be

4
00:00:19,619 --> 00:00:24,260
pretty confident and believe that the mean rating will be higher than this.

5
00:00:24,260 --> 00:00:28,050
Well we have tens of thousands of students on this course, so to perform

6
00:00:28,050 --> 00:00:33,000
this evaluation I might simply take a random sample of say 500 ratings and

7
00:00:33,000 --> 00:00:40,829
calculate the mean. Let&#39;s say the sample mean is around two. So does that confirm

8
00:00:40,829 --> 00:00:46,590
that I&#39;m right and my mean rating is higher than 0? Well no, we&#39;re looking at a

9
00:00:46,590 --> 00:00:51,420
sample and the true population mean could vary from this. So we need to be a

10
00:00:51,420 --> 00:00:57,840
bit more formal in our evaluation. We need to conduct a hypothesis test.

11
00:00:57,840 --> 00:01:03,899
Now we start by defining two mutually exclusive hypotheses. The first of these is known

12
00:01:03,899 --> 00:01:07,380
as the null hypothesis, and it&#39;s the default assumption that we&#39;re trying to

13
00:01:07,380 --> 00:01:11,850
disprove; in this case that the true population mean rating for all students

14
00:01:11,850 --> 00:01:17,909
is not greater than 0. The second hypothesis is called the alternative

15
00:01:17,909 --> 00:01:22,790
hypothesis, and this is the belief we&#39;re trying to find the evidence to support.

16
00:01:22,790 --> 00:01:29,040
OK, well our test is based on a single random sample of 500 ratings; so we&#39;ll

17
00:01:29,040 --> 00:01:32,970
give the null hypothesis the benefit of the doubt and compare it to a sampling

18
00:01:32,970 --> 00:01:39,000
distribution for 50-rating samples, based on a population mean of 0. Now we

19
00:01:39,000 --> 00:01:42,840
can see from our sampling distribution that it&#39;s possible to get a sample with

20
00:01:42,840 --> 00:01:47,520
a mean that varies quite considerably from our hypothesized mean of 0. However

21
00:01:47,520 --> 00:01:51,689
the further away from the mean, the less probable it is that you would get such a

22
00:01:51,689 --> 00:01:57,479
sample purely through random chance. The question is what level of probability

23
00:01:57,479 --> 00:02:02,340
are we prepared to accept. How improbable does our sample need to be for us to

24
00:02:02,340 --> 00:02:06,659
believe that it&#39;s just too unlikely to have occurred by chance? Now we call this

25
00:02:06,659 --> 00:02:11,099
threshold our critical value, and it&#39;s usually indicated using the Greek letter

26
00:02:11,099 --> 00:02:16,739
alpha. In this case, we&#39;ll use a value 0.05; which in effect  means that any

27
00:02:16,739 --> 00:02:20,730
sample mean in the top 5% of the distribution above the hypothesized

28
00:02:20,730 --> 00:02:25,519
population mean would be considered too improbable to have occurred by chance.

29
00:02:25,519 --> 00:02:29,970
Next we&#39;ll look at our sample mean and determine how many standard deviations

30
00:02:29,970 --> 00:02:34,739
above the hypothesized population mean it is; and we&#39;ll call this our test

31
00:02:34,739 --> 00:02:41,129
statistic, or t-statistic. From this point we calculate the probability of a sample

32
00:02:41,129 --> 00:02:46,739
mean being this high or higher, and we call that the p-value. If the p value is

33
00:02:46,739 --> 00:02:50,940
smaller than the critical value alpha, then we consider the chances of

34
00:02:50,940 --> 00:02:54,810
observing our sample mean too improbable to be caused by random chance, and that

35
00:02:54,810 --> 00:02:59,760
gives us the evidence that we need to reject the null hypothesis in favor of

36
00:02:59,760 --> 00:03:06,560
the alternative hypothesis. Now this particular example is a one tailed test.

37
00:03:06,560 --> 00:03:11,190
We&#39;re testing for extreme values above the mean under the right tail of the

38
00:03:11,190 --> 00:03:16,590
distribution; but we could also perform a two-tailed test in which the null

39
00:03:16,590 --> 00:03:20,639
hypothesis states that the mean is zero, and the alternative hypothesis states

40
00:03:20,639 --> 00:03:25,730
that it&#39;s less than or greater than zero. In this case the critical value alpha

41
00:03:25,730 --> 00:03:31,260
covers the extremes under both tails of the distribution, and the p-value is

42
00:03:31,260 --> 00:03:34,919
doubled to allow for the probability of observing the sample mean under either

43
00:03:34,919 --> 00:03:37,370
tail.

