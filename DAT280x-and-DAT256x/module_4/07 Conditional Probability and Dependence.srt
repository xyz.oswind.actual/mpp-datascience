0
00:00:00,000 --> 00:00:05,580
In some cases, events are completely independent of one another. For example,

1
00:00:05,580 --> 00:00:08,849
if you roll a die and get a six; that doesn&#39;t make you any more or less

2
00:00:08,849 --> 00:00:13,590
likely that you&#39;ll get a six if you roll again. Each die roll is an independent

3
00:00:13,590 --> 00:00:18,650
event, and there&#39;s a probability of one out of six that you roll a six each time.

4
00:00:18,650 --> 00:00:23,760
However, other events are dependent. For example, if you draw an ace from a deck

5
00:00:23,760 --> 00:00:27,390
of cards, and you don&#39;t replace it, there&#39;s less chance of you drawing an

6
00:00:27,390 --> 00:00:33,090
ace on the next draw -  because there&#39;s now one less ace in the deck. Let&#39;s start

7
00:00:33,090 --> 00:00:37,320
with independent events. We already know that when we roll a die, there&#39;s a one in

8
00:00:37,320 --> 00:00:42,090
six probability of rolling a six. Rolling the die again is a completely independent

9
00:00:42,090 --> 00:00:46,559
event. The outcome of the first roll has no influence over the second roll, so

10
00:00:46,559 --> 00:00:50,460
there&#39;s another one in six chance of rolling a six; and so on. If we roll a

11
00:00:50,460 --> 00:00:56,399
third time, there&#39;s still a one in six chance of rolling a six. As we saw

12
00:00:56,399 --> 00:01:00,390
previously. if event A is rolling a six, then its

13
00:01:00,390 --> 00:01:06,060
probability is 1 over six, or a 0.167; and each roll of the die is an independent

14
00:01:06,060 --> 00:01:10,170
event -  no matter how many times you roll the die, the probability of rolling a six

15
00:01:10,170 --> 00:01:15,689
remains the same on each roll. But what if we want to know the probability of

16
00:01:15,689 --> 00:01:20,549
rolling a die three times and getting a 1 on the first roll, a 2 on the second

17
00:01:20,549 --> 00:01:27,900
roll, and a 3 on the third roll? These are three independent events that we&#39;ll call

18
00:01:27,900 --> 00:01:33,390
A, B, and C. Now we know that each independent event has a probability of

19
00:01:33,390 --> 00:01:39,659
one in six, or 0.167; but what&#39;s the probability of all three events

20
00:01:39,659 --> 00:01:44,850
occurring? Now we write that like this -  we&#39;re looking for the intersection of

21
00:01:44,850 --> 00:01:50,850
all three events; and we calculate it like this -  it&#39;s the product of all of the

22
00:01:50,850 --> 00:01:56,729
events we want to combine, so we multiply them. So in this case, the probability of

23
00:01:56,729 --> 00:02:01,590
rolling of a one, then a two, then a three is 0.167 times 0.167 times 0.167;

24
00:02:01,590 --> 00:02:06,030
which is around 0.0047,  0.47%.

25
00:02:13,020 --> 00:02:17,940
OK, let&#39;s look at another way to combine independent events.

26
00:02:17,940 --> 00:02:21,940
This time we want to determine the probability of rolling the die once, and

27
00:02:21,940 --> 00:02:28,230
getting either an odd number or a number that is divisible by three.

28
00:02:28,230 --> 00:02:35,640
Let&#39;s call rolling an odd number event A, and rolling a factor of three event B.

29
00:02:35,640 --> 00:02:40,510
Well let&#39;s examine the sample space for this experiment -  there&#39;s a single die

30
00:02:40,510 --> 00:02:46,360
roll, so there are six sample points: 1, 2, 3, 4, 5, and 6. The set of

31
00:02:46,360 --> 00:02:52,810
sample points that satisfy event A (rolling an odd number) are 1, 3, and 5; and the

32
00:02:52,810 --> 00:02:59,560
set of sample points that satisfy event B (rolling a factor of 3) are 3 and 6.

33
00:02:59,560 --> 00:03:05,080
Now note the rolling a 3 satisfies both events -  it&#39;s in the intersection. This

34
00:03:05,080 --> 00:03:09,880
sample point is an odd number AND a factor of 3; but that&#39;s not what we&#39;re

35
00:03:09,880 --> 00:03:16,000
looking for. We need the probability of rolling an odd number OR a factor of 3; in

36
00:03:16,000 --> 00:03:20,500
other words, any sample point in either of the two sets. We call this the

37
00:03:20,500 --> 00:03:26,530
union of this sets, and we write it like this. To calculate this, we take the

38
00:03:26,530 --> 00:03:30,190
probability of event A, which includes all the sample points in the set for

39
00:03:30,190 --> 00:03:34,030
rolling an odd number, and we add it to the probability of event B, which

40
00:03:34,030 --> 00:03:38,980
includes all of the sample points for rolling a factor of 3. However we need to

41
00:03:38,980 --> 00:03:43,690
avoid double counting any sample points that exist in both sets, so we must also

42
00:03:43,690 --> 00:03:49,930
subtract the intersection. In this case, there are three sample points out of six

43
00:03:49,930 --> 00:03:54,580
that satisfy rolling an odd number, so that&#39;s 0.5; and there are two sample

44
00:03:54,580 --> 00:03:59,530
points out of six  for rolling a factor of 3, so that&#39;s a third or 0.33; and there&#39;s

45
00:03:59,530 --> 00:04:03,600
one sample point in the intersection, so we subtract 0.167.

46
00:04:03,600 --> 00:04:11,070
So the probability of rolling an odd number or a factor of 3is 0.633.

47
00:04:11,070 --> 00:04:16,870
Now let&#39;s consider another scenario. Suppose we hold a lottery based on 5

48
00:04:16,870 --> 00:04:21,100
numbered balls in a bowl. Let&#39;s say you have a lottery ticket, on which you&#39;ve

49
00:04:21,100 --> 00:04:27,240
chosen numbers 2, 5, and 4. The draw is about to begin, and you look at your card;

50
00:04:27,240 --> 00:04:31,770
and your first pick is 2 and you know that the odds of it being drawn are 1 in

51
00:04:31,770 --> 00:04:38,240
5, or 0.2. The first number is drawn, and sure enough it&#39;s a 2 - good start!

52
00:04:38,240 --> 00:04:43,560
Now you&#39;re hoping for a 5, but wait, the odds have changed. We&#39;ve removed a ball

53
00:04:43,560 --> 00:04:48,450
from the bowl, and not replaced it; so the probability of drawing a 5 is now 1 in 4,

54
00:04:48,450 --> 00:04:56,220
or 0.25. The next ball gets drawn, and you&#39;re in luck. It&#39;s a 5! Now your hopes

55
00:04:56,220 --> 00:05:00,480
for winning rely on the next ball being of 4, and the odds of that are now 1 in

56
00:05:00,480 --> 00:05:08,760
3, or 0.33. Sadly you&#39;re out of luck, the next ball is a 1! Now what&#39;s happening

57
00:05:08,760 --> 00:05:13,440
here is that the outcome of each event affects the next one. Put another way,

58
00:05:13,440 --> 00:05:17,610
the probability of each event is dependent on the previous one. So how

59
00:05:17,610 --> 00:05:20,730
would we calculate the overall probability for your ticket winning the

60
00:05:20,730 --> 00:05:24,990
lottery? Well we know that the probability of the first ball drawn

61
00:05:24,990 --> 00:05:31,020
being a 2 is 0.2. For the second ball we need to calculate the probability of

62
00:05:31,020 --> 00:05:35,550
drawing a 5 given that we&#39;ve already drawn a 2, so we&#39;re looking for the

63
00:05:35,550 --> 00:05:41,040
probability of event B, given event A; which we calculate like this -  the

64
00:05:41,040 --> 00:05:47,780
probability of A and B, divided by the probability of A; which is 0.2 times 0.25

65
00:05:47,780 --> 00:05:53,850
divided by 0.2; which is 0.25 -  and that&#39;s what we originally calculated so we know

66
00:05:53,850 --> 00:05:59,160
that the formula works. Now the clever bit is that because our formula is an

67
00:05:59,160 --> 00:06:03,900
algebraic expression, we could rearrange it like this, enabling us to calculate

68
00:06:03,900 --> 00:06:09,780
the combined probability of event A and event B allowing for the dependency. So

69
00:06:09,780 --> 00:06:16,890
the probability of the first two numbers being a 2 and a 5 is 0.05 or 5%; and we

70
00:06:16,890 --> 00:06:20,880
can extend this to calculate the probability for the entire ticket. It&#39;s

71
00:06:20,880 --> 00:06:25,169
the probability of A times the probability of B given A times the

72
00:06:25,169 --> 00:06:33,450
probability of C given A and B; which is 0.2 times 0.25 times 0.33, making the

73
00:06:33,450 --> 00:06:39,009
probability of having the winning ticket 0.0165 or around 1.65%.

