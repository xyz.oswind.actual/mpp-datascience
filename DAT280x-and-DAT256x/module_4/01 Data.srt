0
00:00:00,740 --> 00:00:05,609
Statistics are based on data, which consists of a collection of pieces of

1
00:00:05,609 --> 00:00:10,080
information about things you want to study. Now this information can take the

2
00:00:10,080 --> 00:00:15,590
form of descriptions, quantities, measurements, and other observations.

3
00:00:15,590 --> 00:00:19,590
Suppose for example we collected some information about students taking this

4
00:00:19,590 --> 00:00:24,930
course; such as their student ID, name, age gender, their level of experience with

5
00:00:24,930 --> 00:00:28,710
the subject, the number of previous courses they&#39;ve taken, the time spent in

6
00:00:28,710 --> 00:00:33,600
activities in this course, and their grade for the course. Some of these data

7
00:00:33,600 --> 00:00:37,890
are categorical, or qualitative. In other words, they describe the characteristics

8
00:00:37,890 --> 00:00:43,350
of the students. Now often qualitative data fields are text-based, but numeric

9
00:00:43,350 --> 00:00:47,129
fields can also be categorical. For example, student ID is a numeric

10
00:00:47,129 --> 00:00:50,820
identifier for each student. The fact that these are numbers doesn&#39;t imply any

11
00:00:50,820 --> 00:00:55,289
sort of quantifiable difference -  student 3 isn&#39;t greater or less than student 2;

12
00:00:55,289 --> 00:01:02,489
it&#39;s just an identifier. We call this type of data nominal. Now level is also a

13
00:01:02,489 --> 00:01:07,320
numeric field, but this time the numeric values imply some sort of order -  a level

14
00:01:07,320 --> 00:01:11,430
2 student has greater experience than a level 1 experience. We call this type of

15
00:01:11,430 --> 00:01:14,939
data ordinal. The remaining fields are

16
00:01:14,939 --> 00:01:19,770
quantitative -  they&#39;re measurements of one kind or another; and again there are two

17
00:01:19,770 --> 00:01:26,640
subtypes here: age, time, and grade are all measured on a continuous scale; whereas

18
00:01:26,640 --> 00:01:30,720
course is a discrete value that counts the number of previous courses a student

19
00:01:30,720 --> 00:01:36,299
has taken. In general, anything you measure that might include fractions is

20
00:01:36,299 --> 00:01:43,200
continuous, and anything that you count as whole numbers is discrete. One final

21
00:01:43,200 --> 00:01:46,680
thing to think about is that generally when we&#39;re working with data we&#39;re

22
00:01:46,680 --> 00:01:51,240
dealing with a sample from a larger population. It might be impractical to

23
00:01:51,240 --> 00:01:54,720
try and analyze the tens of thousands of students taking this course, so

24
00:01:54,720 --> 00:01:58,200
statistician would likely take a representative sample -  maybe of a few

25
00:01:58,200 --> 00:02:02,700
thousand students for analysis. This is actually the key concept to understand

26
00:02:02,700 --> 00:02:07,409
about statistics. A statistic is a measurement based on a sample, that

27
00:02:07,409 --> 00:02:11,450
approximates a parameter of the full population.

