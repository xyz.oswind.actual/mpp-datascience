0
00:00:00,179 --> 00:00:03,810
It&#39;s important to realize that a function may not be differentiable at

1
00:00:03,810 --> 00:00:07,589
every point; that is you might not be able to calculate the derivative for

2
00:00:07,589 --> 00:00:13,320
every point on the function line. To be differentiable at a given point, the

3
00:00:13,320 --> 00:00:16,920
function must be continuous at that point, the tangent line at that point

4
00:00:16,920 --> 00:00:21,689
cannot be vertical, and the line must be smooth at that point -  in other words it

5
00:00:21,689 --> 00:00:26,630
can&#39;t take on a sudden change of direction. Here&#39;s a somewhat unusual

6
00:00:26,630 --> 00:00:32,009
function. Now the function is not defined for an x value of 11, so it&#39;s not

7
00:00:32,009 --> 00:00:36,660
continuous at that point; and therefore the function is not differentiable for an x

8
00:00:36,660 --> 00:00:43,649
value of 11. The function is defined for x equals 7, but the one-sided negative

9
00:00:43,649 --> 00:00:47,969
and positive limits are not the same, so it&#39;s not continuous there and therefore

10
00:00:47,969 --> 00:00:55,170
not differentiable. When x is 17, the tangent line is vertical so the function

11
00:00:55,170 --> 00:01:00,239
is not differentiable here; and when x equals 4, the function takes on a sudden change

12
00:01:00,239 --> 00:01:05,090
in direction; so it&#39;s not differentiable there either.

