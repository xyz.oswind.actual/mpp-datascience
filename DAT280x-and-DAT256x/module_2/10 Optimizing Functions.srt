0
00:00:00,060 --> 00:00:04,890
The ability to use derivatives to find a minima and maxima of a function makes it

1
00:00:04,890 --> 00:00:08,040
a useful tool for scenarios where you need to optimize a function for a

2
00:00:08,039 --> 00:00:13,889
specific variable. Here's a definition of a function that we want to optimize by

3
00:00:13,889 --> 00:00:18,199
finding the value of x that produces the minimum value from the function.

4
00:00:18,199 --> 00:00:22,260
First we'll refactor the function like this to make it easier to find the

5
00:00:22,260 --> 00:00:27,810
derivative. Now using the power rule, 2 times 2x to the power of 1 is 4x,

6
00:00:27,810 --> 00:00:32,340
and 1 times -8x to the power of 0 is -8; and using the constant rule,

7
00:00:32,340 --> 00:00:41,340
the derivative of 11 is 0. So our derivative is 4x minus 8. Now let's see

8
00:00:41,340 --> 00:00:46,879
if we can find a critical point at which the derivative of the function is 0.

9
00:00:47,329 --> 00:00:56,850
4x minus 8 is 0, so 4x must be 8, so x must be 2; that means there's a critical

10
00:00:56,850 --> 00:01:04,049
point where x is 2; but is it a minimum, a maximum, or just an inflexion point? Well

11
00:01:04,049 --> 00:01:08,070
we can check by calculating the second-order derivative, and the power

12
00:01:08,070 --> 00:01:13,110
rule tells us that 1 times 4x to the power of 0 is 4. Well that's a constant,

13
00:01:13,110 --> 00:01:18,150
so we know that the second-order derivative is 4 for all points, including

14
00:01:18,150 --> 00:01:24,360
where the prime derivative is 0 at the point where X is 2. Since 4 is a positive

15
00:01:24,360 --> 00:01:28,860
value, we know that the prime derivative slope is increasing as it crosses 0, so

16
00:01:28,860 --> 00:01:33,420
that must be the minimum of the function. Now we can confirm that by graphing the

17
00:01:33,420 --> 00:01:38,700
function and its prime and second-order derivatives. The optimal x value that

18
00:01:38,700 --> 00:01:43,439
returns the minimum value of the function is 2, and that minimum value is

19
00:01:43,439 --> 00:01:46,820
itself 3.

