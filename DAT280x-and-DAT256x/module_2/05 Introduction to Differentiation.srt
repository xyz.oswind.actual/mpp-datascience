0
00:00:00,979 --> 00:00:05,959
So far we&#39;ve learned how to evaluate limits for points on a line. Now we&#39;re

1
00:00:05,959 --> 00:00:08,840
going to build on that knowledge and look at a calculus technique called

2
00:00:08,840 --> 00:00:13,370
differentiation. In differentiation, we use our knowledge of limits to calculate

3
00:00:13,370 --> 00:00:17,450
the derivative of a function in order to determine the rate of change at an

4
00:00:17,450 --> 00:00:24,560
individual point on its line. So here&#39;s a function and it&#39;s graph. Now we know that

5
00:00:24,560 --> 00:00:28,100
we can find the slope for a section of the graph by choosing two points and

6
00:00:28,100 --> 00:00:31,910
creating a straight secant line between them. To calculate the slope of the

7
00:00:31,910 --> 00:00:36,860
secant line we divide Delta y by Delta x, which is really just a fancy way of

8
00:00:36,860 --> 00:00:40,640
saying we divide the difference in the y-values by the difference in the

9
00:00:40,640 --> 00:00:46,250
x-values. Now let&#39;s think about this for a second. Delta x is just the distance

10
00:00:46,250 --> 00:00:51,650
between the x values for the two points -  sometimes we call this h; and if we

11
00:00:51,650 --> 00:00:56,810
know this value we can rewrite our slope equation like this. Delta y is the

12
00:00:56,810 --> 00:01:03,350
difference between f of x plus h, and f of x; and Delta x is just h. So how does

13
00:01:03,350 --> 00:01:09,560
all of this help us measure our slope at an individual point? Well suppose we move

14
00:01:09,560 --> 00:01:13,429
that second point so that it becomes infinitesimally closer to the first

15
00:01:13,429 --> 00:01:20,299
point. This reduces our h value to as close to 0 as we can make it. Now if

16
00:01:20,299 --> 00:01:24,049
we draw a line through the points, they&#39;re so close that on the graph they

17
00:01:24,049 --> 00:01:30,200
form a tiny straight line and it extends to a tangent from the curve. So how do we

18
00:01:30,200 --> 00:01:34,490
calculate a point that&#39;s infinitesimally close? Well we know that -  that&#39;s a limit;

19
00:01:34,490 --> 00:01:40,729
and in this case is the limit of the slope equation as h approaches 0. This

20
00:01:40,729 --> 00:01:47,119
whole formula calculates what&#39;s known as the derivative of the function. Let&#39;s put

21
00:01:47,119 --> 00:01:50,689
it to the test. Suppose we want to find the slope or

22
00:01:50,689 --> 00:01:56,719
derivative at the point where x is 3. We can substitute 3 into our derivative

23
00:01:56,719 --> 00:02:02,509
function. Now we know that the function f encapsulates x-squared plus 1, so we can

24
00:02:02,509 --> 00:02:08,330
incorporate that into the equation for our x value of 3. Now on the left we can

25
00:02:08,330 --> 00:02:13,129
factor out the perfect square as a-squared plus b-squared plus 2ab; and on

26
00:02:13,129 --> 00:02:18,080
the right, 3-squared is nine. Now we can consolidate the like

27
00:02:18,080 --> 00:02:25,310
terms, so 9 plus 1 is 10, and 10 minus 10 cancels out; leaving us with 8-squared

28
00:02:25,310 --> 00:02:33,500
plus 6h over h. Well h-squared divided by h is just h, and 6h divided by h is 6. so

29
00:02:33,500 --> 00:02:39,620
our limit as h approaches 0 is h plus 6; and of course if h is 0, then our

30
00:02:39,620 --> 00:02:47,989
derivative for f of 3 is simply 6. So the slope at the point where X is 3 is 6. Now

31
00:02:47,989 --> 00:02:51,500
let&#39;s see if that looks right. Here&#39;s the tangent line that passes through the

32
00:02:51,500 --> 00:02:59,890
point where X is 3, and sure enough for each unit along X we go up 6 units on y.

