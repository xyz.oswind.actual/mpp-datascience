0
00:00:00,589 --> 00:00:05,240
So how do we find the value for the limit of a function at a specific point?

1
00:00:05,240 --> 00:00:11,490
Well there are a few techniques we can employ. Let&#39;s take this function: f of x

2
00:00:11,490 --> 00:00:18,420
equals x-squared minus 1. Here&#39;s the graph for that function. Now suppose we

3
00:00:18,420 --> 00:00:22,400
want to find the limit of this function for an x value of 0;

4
00:00:22,400 --> 00:00:28,529
we can use the graph to see the value of f of x as x moves closer to 0 from the

5
00:00:28,529 --> 00:00:34,110
left, or negative, side. The limit on the negative side is getting closer to

6
00:00:34,110 --> 00:00:40,559
negative 1 as x increases towards 0. We can do the same from the positive side

7
00:00:40,559 --> 00:00:49,879
and we get the same result. In general as x gets closer to 0 from any direction,

8
00:00:49,879 --> 00:00:53,059
the function output gets closer to negative 1.

9
00:00:53,059 --> 00:00:58,620
When the one-sided negative and positive limits agree like this, then the value

10
00:00:58,620 --> 00:01:02,609
they both approach is the two-sided limit for the function - even if the

11
00:01:02,609 --> 00:01:07,350
function itself is not continuous and undefined at that point. In this case, the

12
00:01:07,350 --> 00:01:12,960
two-sided limit for our function as x approaches 0 is negative 1. Let&#39;s look at

13
00:01:12,960 --> 00:01:18,030
a more complex example. Here the function is non-continuous, and it&#39;s not defined

14
00:01:18,030 --> 00:01:23,670
for an x value of -1. Now from the negative side as x approaches

15
00:01:23,670 --> 00:01:29,520
negative 1, the function gets closer to approximately -3.7 so

16
00:01:29,520 --> 00:01:35,159
that&#39;s our negative limit. From the positive side, as x approaches -1,

17
00:01:35,159 --> 00:01:40,770
the function gets closer to approximately -0.3, so

18
00:01:40,770 --> 00:01:45,030
that&#39;s our positive limit. Now when the one-sided negative and

19
00:01:45,030 --> 00:01:49,740
positive limits don&#39;t agree, there is no two-sided limit; so the limit for this

20
00:01:49,740 --> 00:01:58,049
function as x approaches -1 does not exist. Here&#39;s another function. This

21
00:01:58,049 --> 00:02:04,770
one&#39;s not defined for an x value of 0. We can simply create a table and use the

22
00:02:04,770 --> 00:02:11,190
function to calculate f of x for various values of x. So for x equals -1, f of x

23
00:02:11,190 --> 00:02:14,175
is -1 plus 2, which is 1.

24
00:02:14,175 --> 00:02:17,280
For an x value of -0.5,

25
00:02:17,280 --> 00:02:25,260
f of x is 1.5. -0.01 plus two is1.99.

26
00:02:25,260 --> 00:02:29,550
Now the function is not defined for 0, but we could use it to

27
00:02:29,550 --> 00:02:35,280
calculate a value just above 0. So for an x value of 0.01, f of x

28
00:02:35,280 --> 00:02:40,470
is 2.01. when X is 0.5, the function returns 2.5

29
00:02:40,470 --> 00:02:47,940
and when x is 1, f of x is 3. Now if you examine that table you see

30
00:02:47,940 --> 00:02:52,410
that the values returned by the function are converging on the same value. As x

31
00:02:52,410 --> 00:02:58,430
approaches 0 from either direction, the value they are converging on is 2.

32
00:02:58,430 --> 00:03:06,569
Well let&#39;s take a look at that on the graph, and sure enough we can see that

33
00:03:06,569 --> 00:03:10,319
two lies on the line where x is 0 - even though the function is non-continuous

34
00:03:10,319 --> 00:03:15,299
at that point. So the limit for our function when x is approaching

35
00:03:15,299 --> 00:03:22,920
0 is two. You can also find limits analytically by simply substituting the

36
00:03:22,920 --> 00:03:28,350
x value that you want to find a limit for into the function itself. In this

37
00:03:28,350 --> 00:03:31,620
case we have a fairly straightforward function that&#39;s defined for every real

38
00:03:31,620 --> 00:03:37,590
number. To find the limit for an x value of 6, we can just substitute 6 into

39
00:03:37,590 --> 00:03:44,840
the function&#39;s formula. 6-squared is 36, and when we divide that by 2 we get 18.

40
00:03:44,840 --> 00:03:49,350
Now here&#39;s that point on the function graph, and you can see that it fits on

41
00:03:49,350 --> 00:03:54,900
the line where x is 6. This simple substitution approach works easily for

42
00:03:54,900 --> 00:03:58,410
our basic continuous function; but you can also calculate limits analytically

43
00:03:58,410 --> 00:04:02,370
for more complex functions using techniques like factorization and

44
00:04:02,370 --> 00:04:06,410
rationalization, which you&#39;ll see in the lab.

