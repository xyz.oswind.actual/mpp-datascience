0
00:00:00,030 --> 00:00:05,640
Let&#39;s take a look at a simple function. Now suppose we want to find the area

1
00:00:05,640 --> 00:00:11,580
under this function, between the function line and the x-axis . This is called the

2
00:00:11,580 --> 00:00:15,900
integral of the function, and it&#39;s expressed like this -  the integral of F of

3
00:00:15,900 --> 00:00:23,160
x with respect to x. To calculate this we need to find the antiderivative of F of

4
00:00:23,160 --> 00:00:28,920
x, which means we need a function whose derivative is F of x. By calculating the

5
00:00:28,920 --> 00:00:35,160
power rule in reverse, we can figure out that this is 1/2 x squared. Now this

6
00:00:35,160 --> 00:00:38,969
general integral function is most useful when we want to find the area of a

7
00:00:38,969 --> 00:00:43,469
specific part of the function line between two x-values, known as the limits

8
00:00:43,469 --> 00:00:49,890
of the integral. For example, in this case between 0 and 2. We write this like this,

9
00:00:49,890 --> 00:00:54,510
with the lower and upper limits indicated for the integral; and we

10
00:00:54,510 --> 00:00:59,670
complete the formula like this. We need to apply our general integral formula to

11
00:00:59,670 --> 00:01:06,180
each limit and find the difference, so that works out as 1/2 2-squared - 1/2 0-squared,

12
00:01:06,180 --> 00:01:15,240
which is 1/2 of 4 -1/2 of 0; or 2 -0, which is of course 2.

13
00:01:15,240 --> 00:01:20,640
Now looking at the filled area, you can clearly see that it fully covers one

14
00:01:20,640 --> 00:01:27,650
square and a half each of two others, so our calculated integral looks to be correct.

