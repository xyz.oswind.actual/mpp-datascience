0
00:00:00,140 --> 00:00:04,259
When working with derivatives, there are some rules or shortcuts that you can

1
00:00:04,259 --> 00:00:10,889
apply to make your life easier. One of the most basic rules is that if f of x

2
00:00:10,889 --> 00:00:17,369
is a constant, then the derivative for f of x is 0. For example if f of x is

3
00:00:17,369 --> 00:00:23,730
always 3, then the derivative of f of x is 0. This makes sense if you think

4
00:00:23,730 --> 00:00:28,260
about it -  the derivative is the measure of a slope of a function, and that&#39;s

5
00:00:28,260 --> 00:00:32,610
caused by different values being returned for different x values. If the

6
00:00:32,610 --> 00:00:35,910
same value is always returned for different x values, then the slope

7
00:00:35,910 --> 00:00:40,520
remains flat; so the derivative is 0.

8
00:00:40,850 --> 00:00:46,379
Another really useful rule is known as the power rule, and it states that when f

9
00:00:46,379 --> 00:00:51,899
of x is x to some power, then the derivative of f of x is the exponential

10
00:00:51,899 --> 00:00:58,649
times x to the same power minus 1; so for example if f of x is x to the power of 3,

11
00:00:58,649 --> 00:01:06,270
then the derivative of f of x is 3x to the power of 2. Here&#39;s the graph of f and

12
00:01:06,270 --> 00:01:11,540
its derivative. f of x is shown in blue, and the derivative is shown in orange.

13
00:01:11,540 --> 00:01:17,610
Now note that as we move from negative values of x the slope is reducing from a

14
00:01:17,610 --> 00:01:23,159
fairly steep upward tangent towards an almost flat tangent at 0. Now this is

15
00:01:23,159 --> 00:01:26,520
reflected in the fact that the derivative line shows a gradual

16
00:01:26,520 --> 00:01:31,920
reduction in slope towards zero. Then the function line changes direction and

17
00:01:31,920 --> 00:01:38,189
starts to increase its slope, and that&#39;s reflected in the increase in the

18
00:01:38,189 --> 00:01:43,500
derivative line. There are some other useful rules for working with functions

19
00:01:43,500 --> 00:01:48,090
and their derivatives, for example if a function is defined as the sum of two

20
00:01:48,090 --> 00:01:51,960
other functions, then it&#39;s derivative is the sum of the derivatives for those

21
00:01:51,960 --> 00:01:57,299
functions. If function f is defined as the product of functions g and h

22
00:01:57,299 --> 00:02:00,930
multiplied together, then the derivative of f can be

23
00:02:00,930 --> 00:02:04,740
calculated by adding together the results of multiplying the derivative of

24
00:02:04,740 --> 00:02:11,340
g by h, and the derivative of h by g. This is known as the product rule. Similarly,

25
00:02:11,340 --> 00:02:14,870
the quotient rule gives a formula to calculate the

26
00:02:14,870 --> 00:02:19,360
derivative of a function that&#39;s defined by the division of two other functions.

27
00:02:19,360 --> 00:02:23,990
Finally the chain rule is useful when a function can be represented as one

28
00:02:23,990 --> 00:02:28,850
function nested within another; you&#39;ll learn more about these rules in the lab

29
00:02:28,850 --> 00:02:31,570
for this section.

