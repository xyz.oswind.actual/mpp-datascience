0
00:00:00,050 --> 00:00:04,049
We&#39;ve explored various techniques that we can use to calculate the derivative

1
00:00:04,049 --> 00:00:08,490
of a function at a specific x-value. In other words we can determine the slope

2
00:00:08,490 --> 00:00:12,990
of the line created by the function at any point on the line. Now this ability

3
00:00:12,990 --> 00:00:16,770
to calculate the slope means that we can use derivatives to determine some

4
00:00:16,770 --> 00:00:22,859
interesting properties of the function. Let&#39;s examine this function in which f

5
00:00:22,859 --> 00:00:27,779
of x is x-squared. Here&#39;s the graph of that function. The

6
00:00:27,779 --> 00:00:31,949
graph shows that initially, as x is increasing, the value of the function is

7
00:00:31,949 --> 00:00:35,760
decreasing; but the tangent lines are getting progressively less steep as the

8
00:00:35,760 --> 00:00:41,940
rate of change slows. At this point, the graph bottoms out, and we reach the

9
00:00:41,940 --> 00:00:45,420
minimum value of the function; and the tangent line shows that the rate of

10
00:00:45,420 --> 00:00:49,680
change here is 0 - the line is neither increasing nor decreasing at the minimum

11
00:00:49,680 --> 00:00:55,140
point. Then the function starts to go up again, and the tangent lines get steeper

12
00:00:55,140 --> 00:01:01,140
showing an increasing rate of change. Now, using the power rule, we can easily

13
00:01:01,140 --> 00:01:05,549
calculate the derivative for this function as 2x to the power of 1, which

14
00:01:05,549 --> 00:01:12,540
is just 2x; and this looks like this when graphed. Now let&#39;s see if we can find a

15
00:01:12,540 --> 00:01:15,990
relationship between the derivative function line and the line for the

16
00:01:15,990 --> 00:01:22,619
function itself. When the function is reducing in value, the derivative is

17
00:01:22,619 --> 00:01:27,869
negative indicating a downward slope. Additionally the derivative is getting

18
00:01:27,869 --> 00:01:32,280
closer to 0, reflecting the fact that the rate of change is slowing; and it&#39;s a

19
00:01:32,280 --> 00:01:37,860
straight line indicating that the slope is decreasing at a constant rate. At the

20
00:01:37,860 --> 00:01:40,680
point where the function graph reaches the minimum point, where it stops

21
00:01:40,680 --> 00:01:46,380
decreasing before starting to increase, the derivative line crosses 0 indicating

22
00:01:46,380 --> 00:01:51,720
a flat slope or no change. Then as the function starts to increase again, and

23
00:01:51,720 --> 00:01:56,490
the rate of change is accelerating, the derivative at this point is positive

24
00:01:56,490 --> 00:02:00,719
indicating an upward slope; and it&#39;s increasing, indicating that the slope is

25
00:02:00,719 --> 00:02:07,469
steepening. Here&#39;s another function and its derivative, and here&#39;s the graph for

26
00:02:07,469 --> 00:02:13,380
both. Now this time the function is initially increasing,

27
00:02:13,380 --> 00:02:19,170
but the slope is becoming less steep as x increases. The derivative is therefore

28
00:02:19,170 --> 00:02:24,660
positive, indicating a rising function value; but it&#39;s decreasing indicating

29
00:02:24,660 --> 00:02:29,430
that the rate of positive change in the function is reducing. When the function

30
00:02:29,430 --> 00:02:33,320
reaches its maximum, where it stops increasing before it starts to decrease,

31
00:02:33,320 --> 00:02:39,530
the derivative crosses 0; indicating no change in the slope at that point.

32
00:02:39,530 --> 00:02:45,450
Then when the function starts to fall, the derivative is negative to tell us

33
00:02:45,450 --> 00:02:49,260
that the function is decreasing; and it&#39;s moving further from 0 to indicate

34
00:02:49,260 --> 00:02:54,330
that the rate of change is getting steeper. Alright let&#39;s look at one more

35
00:02:54,330 --> 00:03:03,630
function and its derivative, and here&#39;s the graph for both. This time the

36
00:03:03,630 --> 00:03:08,640
function is increasing, with the slope becoming less steep; and as expected the

37
00:03:08,640 --> 00:03:12,990
derivative of these points is positive because the function is increasing. The

38
00:03:12,990 --> 00:03:16,350
derivative is also reducing because the rate of the slope is getting shallower,

39
00:03:16,350 --> 00:03:20,880
but this time the rate at which the slope is shallowing is itself changing

40
00:03:20,880 --> 00:03:25,590
as the function gets closer to flattening out. The function then

41
00:03:25,590 --> 00:03:30,000
flattens out to create the saddle-like shape, and at this inflection point the

42
00:03:30,000 --> 00:03:35,760
derivative is 0. Then our function starts to increase again at an

43
00:03:35,760 --> 00:03:40,650
increasing rate, and at these points the derivative returns to being positive and

44
00:03:40,650 --> 00:03:47,040
curves to indicate an increase in the rate of change for the function. So a key

45
00:03:47,040 --> 00:03:50,640
takeaway from these three examples is that something interesting is happening

46
00:03:50,640 --> 00:03:55,350
to the function whenever the value of the derivative is 0; and we call these

47
00:03:55,350 --> 00:04:01,350
critical points. If the derivative has a constant value of 0, then the function

48
00:04:01,350 --> 00:04:07,470
has a constant value with no slope. If the derivative passes through 0 from

49
00:04:07,470 --> 00:04:12,090
positive to negative, then the function has a change of direction from upward to

50
00:04:12,090 --> 00:04:17,640
downward indicating a peak or a local maximum. Conversely if the derivative

51
00:04:17,640 --> 00:04:21,780
passes through 0 from negative to positive, then the function has changed

52
00:04:21,779 --> 00:04:27,300
direction from downward to upward indicating a trough or a local minimum;

53
00:04:27,300 --> 00:04:31,830
and if the derivative touches 0 without passing through it, then the

54
00:04:31,830 --> 00:04:35,940
function has an inflexion point where it flattens out and then resumes changing

55
00:04:35,940 --> 00:04:38,449
in the same direction.

