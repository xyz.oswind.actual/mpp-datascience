0
00:00:00,319 --> 00:00:05,100
Functions are often visualized as a line on a graph, and this line shows how the

1
00:00:05,100 --> 00:00:09,920
value returned by the function changes based on the changes in the input value.

2
00:00:09,920 --> 00:00:14,849
For example suppose the function f is used to calculate the number of metres

3
00:00:14,849 --> 00:00:19,230
traveled by a cyclist in a given number of seconds. Here&#39;s a

4
00:00:19,230 --> 00:00:23,039
chart for this function which shows how far the cyclist has traveled on the y

5
00:00:23,039 --> 00:00:28,859
axis against the time taken on the x axis. Now we can measure the slope of the

6
00:00:28,859 --> 00:00:34,800
line by dividing the change or Delta in y, which in this case is f of x, over the

7
00:00:34,800 --> 00:00:39,059
change in x; and that will give us the speed, or more accurately the velocity, of

8
00:00:39,059 --> 00:00:44,660
the cyclist. We calculate the deltas by taking two points on the line

9
00:00:44,660 --> 00:00:49,890
subtracting the y and x values from the two points and then dividing; so in this

10
00:00:49,890 --> 00:00:57,199
case we can take the points 3 by 5 and 2 by 4 and that gives us deltas of 1 and 1.

11
00:00:57,199 --> 00:01:02,730
Now 1 divided by 1 is just 1 so the rate of change in this case the cyclist

12
00:01:02,730 --> 00:01:08,670
velocity is 1. Now as you look at the slope of the line on the graph you can

13
00:01:08,670 --> 00:01:14,189
confirm that as we travel 1 in the X direction we go up 1 in the Y direction.

14
00:01:14,189 --> 00:01:20,670
Our cyclist is traveling at 1 meter per second. So we can find the velocity of

15
00:01:20,670 --> 00:01:24,299
our cyclist if we assume they&#39;re traveling at a constant speed; but what

16
00:01:24,299 --> 00:01:29,340
if the function for distance traveled in a given time looks like this? When we

17
00:01:29,340 --> 00:01:34,650
look at the chart for this function, you can see that it&#39;s curved. The cyclist is

18
00:01:34,650 --> 00:01:39,600
not traveling at a constant speed -  they&#39;re accelerating. Well we can find

19
00:01:39,600 --> 00:01:43,979
the average rate of change over a part of the function by drawing a straight

20
00:01:43,979 --> 00:01:49,460
line -  it&#39;s called a secant line, and it joins two points on the function line.

21
00:01:49,460 --> 00:01:54,659
Then we can calculate the slope for the secant line. In this case our points are

22
00:01:54,659 --> 00:02:04,049
4 by 17 and 1 by 2 which gives us deltas of 15 and 3. 15 divided by 3 is 5 so the

23
00:02:04,049 --> 00:02:08,369
average velocity between these points on the function line is 5 meters per second;

24
00:02:08,369 --> 00:02:13,560
and sure enough for every X unit along the secant line we

25
00:02:13,560 --> 00:02:16,670
increase Y by 5.

