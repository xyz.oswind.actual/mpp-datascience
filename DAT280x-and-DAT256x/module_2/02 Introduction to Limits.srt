0
00:00:00,030 --> 00:00:03,629
Well we&#39;ve seen how we can find the average rate of change between two

1
00:00:03,629 --> 00:00:07,830
points on a curve; but what if I need to find the exact rate of change at a

2
00:00:07,830 --> 00:00:12,090
single point? What&#39;s needed is the ability to find the slope of a curve at

3
00:00:12,090 --> 00:00:16,560
that point. Now one way to approach this problem is to find the slope at a

4
00:00:16,560 --> 00:00:21,720
specific x-value by calculating a delta for x1 and x2 values that are

5
00:00:21,720 --> 00:00:25,859
infinitesimally close together, and you can think of it as measuring the slope

6
00:00:25,859 --> 00:00:30,630
of a tiny straight line that comprises just a part of the curve. To find this

7
00:00:30,630 --> 00:00:36,059
point we need to understand something about a concept called limits. Here&#39;s our

8
00:00:36,059 --> 00:00:41,489
function for a cyclist&#39;s distance traveled in a given number of seconds. Now let me

9
00:00:41,489 --> 00:00:46,260
graph that function as a line. In reality that line is made up of individual

10
00:00:46,260 --> 00:00:51,300
points that plot the x value, in this case time, against the Y value, which is

11
00:00:51,300 --> 00:00:57,420
calculated as f of x; in this case that&#39;s distance. So for example, here&#39;s the point

12
00:00:57,420 --> 00:01:03,899
showing where x is 3 and f of x is 10. Now as we increase the value of x

13
00:01:03,899 --> 00:01:09,810
approaching 3, we&#39;ll find points in the line that are increasingly close to 3. So

14
00:01:09,810 --> 00:01:15,780
here&#39;s the point at x equals 2. If we increase x to 2.5 we get a point

15
00:01:15,780 --> 00:01:23,909
that is closer, and at 2.75 we&#39;re even closer; and if we keep going moving

16
00:01:23,909 --> 00:01:27,720
increasingly fractional distances we&#39;ll always end up at a point that is closer

17
00:01:27,720 --> 00:01:33,479
and closer to 3. Now the same is true in the opposite direction here&#39;s the point

18
00:01:33,479 --> 00:01:44,729
at x equals 4, and we can move closer and closer and closer to an x value that is

19
00:01:44,729 --> 00:01:50,040
ever closer to, but not quite, 3. So as we move in either direction there is a

20
00:01:50,040 --> 00:01:54,780
theoretical value for x there&#39;s infinitesimally close to 3, which

21
00:01:54,780 --> 00:01:58,170
we could use as the input for the function to generate a y-value for an

22
00:01:58,170 --> 00:02:03,000
adjacent point. Now symbolically we call this the limit of the function as x

23
00:02:03,000 --> 00:02:06,200
approaches 3.

