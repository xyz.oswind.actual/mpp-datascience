0
00:00:00,030 --> 00:00:03,929
Until now we&#39;ve considered functions that only have one input variable.

1
00:00:03,929 --> 00:00:07,770
However you can have multivariate functions that operate on multiple input

2
00:00:07,770 --> 00:00:13,170
variables like this. Conceptually, you can imagine the plot of this function as

3
00:00:13,170 --> 00:00:18,570
representing three dimensions. So how would you find the derivatives of this

4
00:00:18,570 --> 00:00:21,600
function? Well we tackle this by finding the

5
00:00:21,600 --> 00:00:26,160
derivatives for changes in the function with respect to x or y, and we call these

6
00:00:26,160 --> 00:00:32,489
partial derivatives. Let&#39;s start with the partial derivatives of the F function

7
00:00:32,488 --> 00:00:38,700
with respect to x. In this case that&#39;s the derivative of x-squared with respect

8
00:00:38,700 --> 00:00:46,050
to x plus the derivative of y-squared with respect to x. Well we can use the

9
00:00:46,050 --> 00:00:52,170
power rule to find the derivative of x-squared with respect to x -  it&#39;s 2x; and as

10
00:00:52,170 --> 00:00:56,550
for the derivative of y-squared with respect to x, well there&#39;s no x term here

11
00:00:56,550 --> 00:01:01,050
so this is 0. So the partial derivative of the function with respect

12
00:01:01,050 --> 00:01:09,180
to x is 2x + 0, which is of course just 2x. We can also find the partial

13
00:01:09,180 --> 00:01:14,760
derivative with respect to y. The derivative of x-squared with respect to

14
00:01:14,760 --> 00:01:23,369
y is 0, and the derivative of y-squared with respect to y is 2y; so the

15
00:01:23,369 --> 00:01:28,799
partial derivative of the function with respect to y is 0 + 2y, which is of

16
00:01:28,799 --> 00:01:31,820
course just 2y.

