0
00:00:00,120 --> 00:00:04,740
The functions we&#39;ve looked at so far are continuous for all real numbered

1
00:00:04,740 --> 00:00:09,570
values of x. Put simply, this means that you could draw the line created by the function

2
00:00:09,570 --> 00:00:15,470
without lifting your pen; but not all functions behave like this. For example,

3
00:00:15,470 --> 00:00:19,820
this function returns x-squared divided by x plus 1,

4
00:00:19,820 --> 00:00:25,289
but it&#39;s only defined for x values less than or equal to -2 and greater

5
00:00:25,289 --> 00:00:31,650
than or equal to 0. The graph of this function looks like this. Now you

6
00:00:31,650 --> 00:00:34,380
wouldn&#39;t be able to draw the line for this function without lifting your pen,

7
00:00:34,380 --> 00:00:38,460
because there&#39;s a gap in the function&#39;s domain that results in a function range

8
00:00:38,460 --> 00:00:43,050
that&#39;s split into two intervals. We typically indicate the end of an

9
00:00:43,050 --> 00:00:47,789
interval for which a function is defined with a circle and we fill that circle in

10
00:00:47,789 --> 00:00:51,149
if the indicated point is included in the functions range, and we leave it

11
00:00:51,149 --> 00:00:55,789
empty if the function is not defined for that point.

