0
00:00:00,000 --> 00:00:05,190
A common use of calculus is to find the minimum and maximum points in a function.

1
00:00:05,190 --> 00:00:09,240
For example we might want to find out how many seconds it took for a kicked

2
00:00:09,240 --> 00:00:13,950
football to reach its maximum height or how long it took for the application of

3
00:00:13,950 --> 00:00:17,690
some fertilizer to be effective in reversing the decline of flower growth.

4
00:00:17,690 --> 00:00:22,410
We've already seen that when a function changes direction to create a maximum

5
00:00:22,410 --> 00:00:27,510
peak or a minimum trough, the derivative of that function is 0, so a step towards

6
00:00:27,510 --> 00:00:30,960
finding these extreme points might be to simply find all the points in the

7
00:00:30,960 --> 00:00:36,600
function where the derivative is 0. So here's a very simple function that

8
00:00:36,600 --> 00:00:43,020
returns x-squared, and here's the graph for that function. We use the power rule

9
00:00:43,020 --> 00:00:48,770
to find the function's derivative -  it's 2 times x to the power of 1, so 2x.

10
00:00:48,770 --> 00:00:54,840
The derivative is itself a function, so let's graph that as well and we can see

11
00:00:54,840 --> 00:01:00,140
that the derivative slope is increasing at a constant rate as x increases.

12
00:01:00,140 --> 00:01:06,479
Now when the derivative is 0, we know we've found a critical point. In this

13
00:01:06,479 --> 00:01:11,580
case we need to find when 2x would return a value of 0, and that would be

14
00:01:11,580 --> 00:01:16,290
when x itself is 0; but without looking at the graph, how can we tell if

15
00:01:16,290 --> 00:01:20,540
it's a minimum, a maximum, or just an inflexion point?

16
00:01:20,540 --> 00:01:26,159
Well since the derivative is actually a function, it itself has a derivative that

17
00:01:26,159 --> 00:01:30,090
will tell us its slope. In other words it will tell us the rate of change for the

18
00:01:30,090 --> 00:01:34,710
rate of change of the function. Now formally we call the derivative of the

19
00:01:34,710 --> 00:01:38,610
function its prime derivative, and the derivative of the prime derivative is

20
00:01:38,610 --> 00:01:43,829
the second-order derivative; and we could use the power rule to calculate that the

21
00:01:43,829 --> 00:01:50,159
derivative of 2x is just 2. Now if we add the second-order derivative to the

22
00:01:50,159 --> 00:01:54,570
graph, we can see that it's a straight line reflecting the fact that the slope

23
00:01:54,570 --> 00:01:59,610
of the prime derivative is constant; so of course the second-order derivative

24
00:01:59,610 --> 00:02:04,740
for the critical point, where X is 0, is 2; which of course is a positive

25
00:02:04,740 --> 00:02:08,970
number. And that tells us that the slope of the prime derivative at that point is

26
00:02:08,970 --> 00:02:13,140
positive, so it's crossing 0 from negative to positive;

27
00:02:13,140 --> 00:02:18,480
which tells us we found a minimum value for the function. To find what that value

28
00:02:18,480 --> 00:02:22,500
is, we just used the function to calculate the f of x value for a

29
00:02:22,500 --> 00:02:27,330
critical point -  in this case 0; and that tells us the minimum value returned

30
00:02:27,330 --> 00:02:33,750
by the function when x is 0, is 0. Let's look at another example.

31
00:02:33,750 --> 00:02:39,390
This time the function returns -2x-squared plus 1. The prime

32
00:02:39,390 --> 00:02:44,780
derivative is -2x, and the second-order derivative is -2.

33
00:02:44,780 --> 00:02:49,200
There's a critical point where the prime derivative is 0; which once again

34
00:02:49,200 --> 00:02:54,150
happens when x is 0; and we know that the second-order derivative is a

35
00:02:54,150 --> 00:02:58,350
negative value, so when the prime derivative crosses 0 it's decreasing

36
00:02:58,350 --> 00:03:03,870
from positive to negative; which tells us the critical point is a maximum. The

37
00:03:03,870 --> 00:03:07,680
value returned by the function at that point is -2 times x-squared + 1

38
00:03:07,680 --> 00:03:14,880
so with an x value of 0, the function returns 1. So 1 is the maximum

39
00:03:14,880 --> 00:03:18,350
value for the function.

