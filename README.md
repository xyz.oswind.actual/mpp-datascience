MPP Data Science
================

### Ab archivum autem indicium-scientia doctrina anno MMXIX

This is an archive of my journey through the soon to be retired MPP data
science certification track. I make no assertion of ownership over the
educational materials contained in this repository. As the MPP program
winds down it’s unclear how long these materials will remain available.
All materials are freely available for personal use only.

This curriculum track requires proficiency to be demonstrated in 11
areas. The final revision of course content includes the following
courses. Where multiple options are available only one is required to
complete a checkpoint.

  - **Get Started with Data Science**
      - \[DAT101x\] Introduction to Data Science
  - **Analyze and Visualize Data**
      - \[DAT206x\] Analyzing and Visualizing Data with Excel
      - \[DAT207x\] Analyzing and Visualizing Data with Power BI
  - **Communicate Data Insights**
      - \[DAT248x\] Analytics Storytelling for Impact
  - **Apply Ethics and Law in Analytics**
      - \[DAT249x\] Ethics and Law in Data and Analytics
  - **Query Relational Data**
      - \[DAT201x\] Querying Data with Transact-SQL
  - **Explore data with Code**
      - \[DAT204x\] Introduction to R for Data Science
      - \[DAT208x\] Introduction to Python for Data Science
  - **Apply Math and Statistics to Data Science**
      - \[DAT280x\] Essential Math for Machine Learning: R Edition
      - \[DAT256x\] Essential Math for Machine Learning: Python Edition
      - \[DAT222x\] Essential Statistics for Data Analysis using Excel
  - **Plan and Conduct Data Studies**
      - \[DAT274x\] Data Science Research Methods: R Edition
      - \[DAT273x\] Data Science Research Methods: Python Edition
      - \[DAT203.1x\] Data Science Essentials
  - **Build Machine Learning Models**
      - \[DAT276x\] Principles of Machine Learning: R Edition
      - \[DAT275x\] Principles of Machine Learning: Python Edition
      - \[DAT203.2x\] Principles of Machine Learning
  - **Build Predictive Solutions at Scale**
      - \[DAT228x\] Developing Big Data Solutions with Azure Machine
        Learning
      - \[DAT213x\] Analyzing Big Data with Microsoft R
      - \[DAT202.3x\] Implementing Predictive Analytics with Spark in
        Azure HDInsight
  - **Final Project**
      - \[DAT102x\] Microsoft Professional Capstone: Data Science
