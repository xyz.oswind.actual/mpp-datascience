0
00:00:00,300 --> 00:00:03,480
>> In July 2014, Eric Holder,

1
00:00:03,480 --> 00:00:05,550
who was the U.S. attorney general at the time,

2
00:00:05,550 --> 00:00:07,020
expressed concern about the use

3
00:00:07,020 --> 00:00:08,535
of big data in sentencing.

4
00:00:08,535 --> 00:00:11,460
He stated that criminal sentencing must be based on

5
00:00:11,460 --> 00:00:14,585
the facts of the law and actual crimes committed,

6
00:00:14,585 --> 00:00:17,200
the circumstances surrounding each individual case.

7
00:00:17,200 --> 00:00:20,010
They should not be based on factors that a person can't

8
00:00:20,010 --> 00:00:21,930
control or the possibility

9
00:00:21,930 --> 00:00:24,665
of a future crime that has not taken place.

10
00:00:24,665 --> 00:00:26,560
They may exacerbate unwarranted

11
00:00:26,560 --> 00:00:27,840
and unjust asperities that

12
00:00:27,840 --> 00:00:29,340
are already far too common in

13
00:00:29,340 --> 00:00:32,215
our justice system and our society.

14
00:00:32,215 --> 00:00:33,585
So Nathan how does

15
00:00:33,585 --> 00:00:37,720
this Holder's quote relate to lab four.

16
00:00:37,720 --> 00:00:39,180
>> Yes, I think it's pretty directly

17
00:00:39,180 --> 00:00:41,130
relevant to what we've been concerned with,

18
00:00:41,130 --> 00:00:42,610
and what we're going to be concerned with at module four.

19
00:00:42,610 --> 00:00:44,490
Here you have one of the highest

20
00:00:44,490 --> 00:00:46,710
ranking legal officials in all the land,

21
00:00:46,710 --> 00:00:48,450
expressing the kinds of

22
00:00:48,450 --> 00:00:50,570
concerns that we've been talking about.

23
00:00:50,570 --> 00:00:55,260
He talks about, he's worried about using characteristics,

24
00:00:55,260 --> 00:00:56,880
variables in our language that you

25
00:00:56,880 --> 00:00:58,800
can't control. That would be unfair.

26
00:00:58,800 --> 00:01:01,080
He talks about the theory

27
00:01:01,080 --> 00:01:03,205
of punishment that we now know is recidivism,

28
00:01:03,205 --> 00:01:05,490
he doesn't use the word but he says it should be based on

29
00:01:05,490 --> 00:01:08,110
past crimes not the probability of future crimes.

30
00:01:08,110 --> 00:01:11,520
And so hopefully the stuff we've gone over in

31
00:01:11,520 --> 00:01:13,625
the first three labs have

32
00:01:13,625 --> 00:01:16,200
kind of set us up for what we're about to uncover.

33
00:01:16,200 --> 00:01:18,090
Because so far we've been thinking in terms of

34
00:01:18,090 --> 00:01:20,730
ethics but obviously that's not all to think about.

35
00:01:20,730 --> 00:01:21,770
>> Right.

36
00:01:21,770 --> 00:01:24,550
>> And so Eva what are they going to do in this lab?

37
00:01:24,550 --> 00:01:26,670
>> Well they'll do the lab and then as part

38
00:01:26,670 --> 00:01:29,850
of I guess in addition to the lab,

39
00:01:29,850 --> 00:01:31,350
they're going to do in other IRAC.

40
00:01:31,350 --> 00:01:34,020
So we've done three IRAC's up till now.

41
00:01:34,020 --> 00:01:36,725
I've led you through each one.

42
00:01:36,725 --> 00:01:39,230
And remember this is the legal analysis tool.

43
00:01:39,230 --> 00:01:43,500
So for mud for the lab will actually get tied to

44
00:01:43,500 --> 00:01:46,440
the last IRAC and without giving anything

45
00:01:46,440 --> 00:01:47,820
away because you actually have to

46
00:01:47,820 --> 00:01:49,822
complete the lab first to do the IRAC.

47
00:01:49,822 --> 00:01:51,510
You'll be given instructions

48
00:01:51,510 --> 00:01:53,190
and everything you need to actually go

49
00:01:53,190 --> 00:01:55,170
through that last legal analysis on your own

50
00:01:55,170 --> 00:01:57,291
and see if you can identify the issue,

51
00:01:57,291 --> 00:01:59,010
the rule, the application and the

52
00:01:59,010 --> 00:02:02,320
conclusion with respect to this particular lab.

53
00:02:02,320 --> 00:02:05,590
>> Excellent. Thank you. Let's dig in.

