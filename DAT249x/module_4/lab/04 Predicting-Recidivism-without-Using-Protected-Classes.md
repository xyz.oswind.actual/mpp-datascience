Predicting Recidivism without Using Protected Classes
================

### Background

In the answers to the hypothetical case study for Module Four, we
learned that “because State Z has no proof based on the facts here that
using race and gender is the only way to predict recidivism of County
Jailhouse inmates, use of these data inputs is a violation of inmate
rights and unconstitutional. County Jailhouse is not permitted to use
this algorithm for parole decisions”.

This is in line with Holder’s concern that we mentioned in the Lab 4
video earlier, that algorithms “should not be based on unchangeable
factors that a person cannot control”. This would include obviously
include race and gender. Age is more interesting, because on the one
hand, of course you cannot control your age, but since age applies to
everyone in the same way, it is not as ethically sensitive.

After consulting with a lawyer, you learn that age is a protected class
in employment, but that it for over 40 year olds. In this case, the
prediction does not disfavor older people, and this is not in the
context of employment. You conclude that any legal challenge based on
including age as a variable in predicting recidivism would not be
successful. 

**Instructions**

Since race and sex are ethically and legally sensitive, and since you
discovered in Lab 3 that the charge degree (whether the offense was a
“misdemeanor” or felony) was not strongly predictive, you want to see
how well you can predict recidivism using only prior record and age.

**Step 1:** Calculate the success rate of the given prediction Hint: use
to variables “score text” (the prediction of recidivism) and “is\_recid”
(the result of whether the person in fact was re-arrested)

**Step 2:** Using the same method with pivot tables for Mod 3 Lab, use
only the variables “age” and “prior\_count” to predict Hint: To use them
together, include both variables in “row”

**Step 3:** Answer these questions:

  - What was the success rate of current algorithm (calculated using
    “score text”)?
  - How well can you predict recidivism without using the variables
    “race” or “gender”?
