IRAC Suggested Answers
================

### PJLC v. State Z

**Issue:**

Whether the use of an algorithm that incorporates race and gender data
to predict recidivism is lawful.

**Rule:**

The rules applicable here originate in the U.S. Constitution, but the
concerns are similar to what we have seen in course content about the
use of race and gender inputs by private organizations that result in
bias and discrimination in consumer credit and employment decisions.
Gender and race are protected classes in U.S. law, meaning that
decisions and treatment based on these aspects are suspect and rarely
lawful.

The U.S. Constitution protects individuals from government actions that
infringe on civil and political human rights. When the government takes
an action that involves individual liberty, and that action treats
people differently on the basis of race, the courts apply a strict test
that requires the government to show it has a compelling purpose for the
difference in treatment, and to prove that the only way to achieve this
purpose is the means they have chosen.

When the government takes an action that treats people differently with
respect to liberty on the basis of gender, the courts apply an
intermediate scrutiny test which requires the government to show a
significant purpose for the difference in treatment, and to prove that
the only way to achieve this purpose is the means they have chosen.

**Application:**

The algorithm at issue uses race and gender data as inputs. Both are
protected classifications, and so this use will require scrutiny. The
hypothetical reveals that the County Jailhouse has a purpose for using
the algorithm: to reduce recidivism. While that purpose alone is lawful
and important to the community, State Z will have to prove that it is at
least a significant purpose (for gender), and at most a compelling
purpose (for race). Assuming State Z can show this, it will still need
to prove that using these inputs are this only way to accurately predict
recidivism. State Z will be challenged to show there are no other
options, or data inputs, which it can use to as accurately predict
recidivism in making parole decisions.

**Conclusion:**

Because State Z has no proof based on the facts here that using race and
gender is the only way to predict recidivism of County Jailhouse
inmates, use of these data inputs is a violation of inmate rights and
unconstitutional. County Jailhouse is not permitted to use this
algorithm for parole decisions. The court finds for the PJLC.

**NOTE:** While the data we are using is real, and we are looking at the
results of a real algorithm, this case study is strictly hypothetical
and in no way implies that the accuracy of the algorithm or what actual
variables the algorithm uses.
