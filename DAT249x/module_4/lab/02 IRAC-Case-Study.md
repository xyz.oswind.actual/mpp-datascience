IRAC Case Study
================

Please read the following hypothetical case and do your best to complete
the IRAC method.

### PJLC v. State Z

The County Jailhouse in State Z in the United States is working on
reducing recidivism, which is the tendency of a convicted criminal to
reoffend. In order to accomplish their objective, the Jailhouse hired a
data scientist to design an algorithm to predict the likelihood of
recidivism of current inmates. The data inputs for this new algorithm
include variables such as age, prior convictions, race and gender. The
Jailhouse will use the predictions to determine whether or not to
release inmates who come up for parole, or to grant early release from
their sentence.

One of the Jailhouse employees shared the plans to use the new algorithm
with a friend who is an attorney at a local nonprofit justice
organization, the Poverty Justice Law Center (“PJLC”). In response to
the plans, the PJLC filed a lawsuit against State Z to prevent the
Jailhouse from using the algorithm for parole decisions. State Z and the
Jailhouse are contesting this lawsuit and want to move forward with use
of the algorithm.

If you were the judge deciding this case, what would you rule? Apply the
IRAC method to identify the legal issue you think arises out of these
facts, and any rule or policy you think is on point. Then apply the rule
or policy to reach your conclusion.

Good luck\!
