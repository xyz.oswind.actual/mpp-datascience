0
00:00:00,000 --> 00:00:02,920
>> Now that we have an idea of the challenges

1
00:00:02,920 --> 00:00:04,960
of increasing algorithm, that complexity,

2
00:00:04,960 --> 00:00:06,880
let's review those options I mentioned for

3
00:00:06,880 --> 00:00:09,515
understanding the concept of explainability.

4
00:00:09,515 --> 00:00:11,545
In contrast that with another movement,

5
00:00:11,545 --> 00:00:12,850
that more or less gives up on

6
00:00:12,850 --> 00:00:14,920
the notion of explainability.

7
00:00:14,920 --> 00:00:17,170
First, what if we interpret the ideal of

8
00:00:17,170 --> 00:00:20,050
explainability as a kind of transparency,

9
00:00:20,050 --> 00:00:22,435
as several data scientists have suggested?

10
00:00:22,435 --> 00:00:24,093
There are a few concerns here,

11
00:00:24,093 --> 00:00:27,280
but let me just speak to what I regard as, the main one.

12
00:00:27,280 --> 00:00:28,705
As noted in module one,

13
00:00:28,705 --> 00:00:32,140
big data algorithms work by analyzing correlation,

14
00:00:32,140 --> 00:00:34,505
not by revealing causation.

15
00:00:34,505 --> 00:00:36,025
This is a mismatch between

16
00:00:36,025 --> 00:00:38,350
the cognition of human and of machines.

17
00:00:38,350 --> 00:00:41,335
We, humans are more comfortable saying we know something,

18
00:00:41,335 --> 00:00:43,640
when we can explain it's cause.

19
00:00:43,640 --> 00:00:45,550
So, even if we had a full view of

20
00:00:45,550 --> 00:00:48,520
all the correlations discovered by some algorithm,

21
00:00:48,520 --> 00:00:51,100
it would not be the kind of explanation that allows us

22
00:00:51,100 --> 00:00:54,680
to infer the causal link between the correlations.

23
00:00:54,680 --> 00:00:56,790
What about justification?

24
00:00:56,790 --> 00:00:57,840
David Weinberger,

25
00:00:57,840 --> 00:01:00,310
in an April 2017 article that is getting

26
00:01:00,310 --> 00:01:01,870
some traction linked in

27
00:01:01,870 --> 00:01:04,970
the further reading section, speaks in these terms.

28
00:01:04,970 --> 00:01:06,460
He contrasts the idea from

29
00:01:06,460 --> 00:01:09,635
ancient philosophy that we must justify our true beliefs

30
00:01:09,635 --> 00:01:11,260
on the one hand with the kind of

31
00:01:11,260 --> 00:01:12,580
justification that

32
00:01:12,580 --> 00:01:14,800
complex machines could give on the other.

33
00:01:14,800 --> 00:01:17,200
I think this is a fair contrast.

34
00:01:17,200 --> 00:01:20,020
If justify means something like giving the reasons in

35
00:01:20,020 --> 00:01:21,400
terms of the principles that could

36
00:01:21,400 --> 00:01:23,095
explain our true opinions,

37
00:01:23,095 --> 00:01:24,670
which it probably does,

38
00:01:24,670 --> 00:01:25,810
I think we're right toward

39
00:01:25,810 --> 00:01:28,000
that justification won't help much.

40
00:01:28,000 --> 00:01:29,830
Remember, in AI system like

41
00:01:29,830 --> 00:01:32,435
Deep Blue, learned from principles.

42
00:01:32,435 --> 00:01:33,940
And so we could potentially explain

43
00:01:33,940 --> 00:01:37,340
a particular action of Deep Blue in terms of a principle.

44
00:01:37,340 --> 00:01:40,240
But as noted, more complex AI aren't given

45
00:01:40,240 --> 00:01:43,445
principles nor do they find their own principles.

46
00:01:43,445 --> 00:01:45,280
So how could we ask for a justification

47
00:01:45,280 --> 00:01:47,640
in the traditional sense?

48
00:01:47,640 --> 00:01:50,165
What about interpretation? For my money,

49
00:01:50,165 --> 00:01:52,865
interpretability is the most promising, and yet,

50
00:01:52,865 --> 00:01:56,080
most poorly defined notion of explainability.

51
00:01:56,080 --> 00:01:59,690
At the 2016 International Conference on Machine Learning,

52
00:01:59,690 --> 00:02:01,670
the participants explicitly worked

53
00:02:01,670 --> 00:02:03,620
to find some common ground on

54
00:02:03,620 --> 00:02:05,630
what kinds of concepts are nested in

55
00:02:05,630 --> 00:02:08,600
interpretability with little agreement.

56
00:02:08,600 --> 00:02:09,770
It seems like this is going to be

57
00:02:09,770 --> 00:02:11,390
a wait and see situation.

58
00:02:11,390 --> 00:02:13,650
As AI becomes more sophisticated,

59
00:02:13,650 --> 00:02:16,250
we are going to get more and more information on whether

60
00:02:16,250 --> 00:02:19,785
humans can get a human interpretation of what's going on.

61
00:02:19,785 --> 00:02:22,325
We'll definitely keep our eye on that.

62
00:02:22,325 --> 00:02:25,120
And to make matters even more complicated,

63
00:02:25,120 --> 00:02:26,360
there is what I think of,

64
00:02:26,360 --> 00:02:28,940
as another basket of answers that is more or

65
00:02:28,940 --> 00:02:32,040
less prepared to give up on the goal of explainability.

66
00:02:32,040 --> 00:02:35,340
You might call this, Governable Artificial Intelligence.

67
00:02:35,340 --> 00:02:36,770
And if you like comparable terms,

68
00:02:36,770 --> 00:02:39,675
we can abbreviate as, GAI.

69
00:02:39,675 --> 00:02:42,650
The basic idea here is that we may never be able to

70
00:02:42,650 --> 00:02:45,613
understand how algorithms come up with their conclusions,

71
00:02:45,613 --> 00:02:47,590
but that's okay because well,

72
00:02:47,590 --> 00:02:50,920
XAI is not attainable for the more complex algorithms,

73
00:02:50,920 --> 00:02:52,930
it is not even desirable.

74
00:02:52,930 --> 00:02:54,710
Instead, what we should want and what

75
00:02:54,710 --> 00:02:56,780
is actually attainable by technology,

76
00:02:56,780 --> 00:03:00,220
is a kind of governance or regulation of the algorithms.

77
00:03:00,220 --> 00:03:03,185
Peter Norvig, head of AI research at Google,

78
00:03:03,185 --> 00:03:06,610
has articulated a version of this in June 2017.

79
00:03:06,610 --> 00:03:09,905
His claim is that analyzing the output of algorithms,

80
00:03:09,905 --> 00:03:11,810
the actual decisions, is more

81
00:03:11,810 --> 00:03:14,755
realistic than trying to peer inside the system.

82
00:03:14,755 --> 00:03:17,210
If we had to summarize this idea in a word,

83
00:03:17,210 --> 00:03:19,190
it would probably be, most likely

84
00:03:19,190 --> 00:03:23,000
what computer scientists have been calling, auditing.

85
00:03:23,000 --> 00:03:24,380
There are also old models from

86
00:03:24,380 --> 00:03:26,300
computer science that don't

87
00:03:26,300 --> 00:03:29,975
rely on auditing but something more like, verification.

88
00:03:29,975 --> 00:03:32,120
And there is another model getting traction in

89
00:03:32,120 --> 00:03:34,965
the AI literature around accountability.

90
00:03:34,965 --> 00:03:37,640
What these concepts have in common is an attempt to

91
00:03:37,640 --> 00:03:38,900
manage AI rather than

92
00:03:38,900 --> 00:03:42,000
explain how it reaches its conclusions.

