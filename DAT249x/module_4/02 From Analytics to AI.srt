0
00:00:00,230 --> 00:00:02,810
>> As we begin, I'd like to help

1
00:00:02,810 --> 00:00:05,090
define some of these terms we're using: analytics,

2
00:00:05,090 --> 00:00:06,710
big data, data science.

3
00:00:06,710 --> 00:00:08,360
Just so we can have some context

4
00:00:08,360 --> 00:00:10,430
as we proceed in these modules.

5
00:00:10,430 --> 00:00:12,555
So to begin, I like to think of

6
00:00:12,555 --> 00:00:16,100
an axis of scale and complexity for these terms.

7
00:00:16,100 --> 00:00:17,330
And I think of analytics or

8
00:00:17,330 --> 00:00:19,987
business intelligence as you've heard,

9
00:00:19,987 --> 00:00:22,340
on the lower end of scale and complexity.

10
00:00:22,340 --> 00:00:24,530
Not to say that it's not hard or we're not

11
00:00:24,530 --> 00:00:27,350
doing things that are complex in nature,

12
00:00:27,350 --> 00:00:31,010
but just that to date we deal with small to

13
00:00:31,010 --> 00:00:34,476
medium data that's maybe relative,

14
00:00:34,476 --> 00:00:37,045
by gigabytes or below

15
00:00:37,045 --> 00:00:41,184
perhaps, enterprise, reporting systems.

16
00:00:41,184 --> 00:00:42,885
Things you could do on your computer.

17
00:00:42,885 --> 00:00:46,035
As we get up in the scale access,

18
00:00:46,035 --> 00:00:48,100
I like to think of that as big data.

19
00:00:48,100 --> 00:00:49,165
It's not too different,

20
00:00:49,165 --> 00:00:50,900
it's not really that much more complex.

21
00:00:50,900 --> 00:00:53,735
It's really just perhaps faster,

22
00:00:53,735 --> 00:00:57,275
bigger and in higher velocity it's coming in.

23
00:00:57,275 --> 00:00:58,460
By doing some of things like real

24
00:00:58,460 --> 00:00:59,885
time analytics on top of that,

25
00:00:59,885 --> 00:01:02,520
there is some overlap with those two terms.

26
00:01:02,520 --> 00:01:05,342
As we see, there's some overlap again with data science.

27
00:01:05,342 --> 00:01:06,595
So, data science is

28
00:01:06,595 --> 00:01:08,298
maybe more in the complexity side of things.

29
00:01:08,298 --> 00:01:09,888
We're using some predictive modeling.

30
00:01:09,888 --> 00:01:12,740
We're using some more algorithmically driven things

31
00:01:12,740 --> 00:01:14,450
to answer specific questions.

32
00:01:14,450 --> 00:01:16,593
Maybe into the smaller scale you're sampling things,

33
00:01:16,593 --> 00:01:22,160
you're trying to to do that more complex analysis.

34
00:01:22,160 --> 00:01:24,725
Lastly, as we get into artificial intelligence,

35
00:01:24,725 --> 00:01:26,390
really it incorporates a lot of

36
00:01:26,390 --> 00:01:28,135
these different elements of big data,

37
00:01:28,135 --> 00:01:30,110
of data science but then gets into

38
00:01:30,110 --> 00:01:32,996
even more advanced techniques like deep learning.

39
00:01:32,996 --> 00:01:35,990
Just to recap analytics and big data, data science,

40
00:01:35,990 --> 00:01:37,768
I put on the left here.

41
00:01:37,768 --> 00:01:40,460
Visualization, reporting, storage.

42
00:01:40,460 --> 00:01:41,715
Some predictive modeling,

43
00:01:41,715 --> 00:01:44,230
regression, classification and clustering.

44
00:01:44,230 --> 00:01:46,670
Whereas, artificial intelligence we get into things like

45
00:01:46,670 --> 00:01:51,320
predictive modeling with human like augmented behavior.

46
00:01:51,320 --> 00:01:52,565
That's much more complex,

47
00:01:52,565 --> 00:01:54,860
we're trying to mimic things that are human like.

48
00:01:54,860 --> 00:01:57,080
Like understanding language or

49
00:01:57,080 --> 00:01:58,665
seeing things with our eyes,

50
00:01:58,665 --> 00:02:01,340
computer vision and the like.

