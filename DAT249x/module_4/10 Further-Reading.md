Further Reading
================

Bornstein, Aaron. “Is Artificial Intelligence Permanently Inscrutable?”
<http://nautil.us/issue/40/learning/is-artificial-intelligence-permanently-inscrutable>

Gunning, David. “Explainable Artificial Intelligence”
<https://www.darpa.mil/program/explainable-artificial-intelligence>

Weinberger, David. “Our Machines Now Have Knowledge We’ll Never
Understand”
<https://www.wired.com/story/our-machines-now-have-knowledge-well-never-understand>
