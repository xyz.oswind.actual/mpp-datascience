0
00:00:00,000 --> 00:00:02,320
>> One of the last issues I mentioned,

1
00:00:02,320 --> 00:00:04,855
was that we needed to get clear on what we want,

2
00:00:04,855 --> 00:00:06,775
when we want explainability.

3
00:00:06,775 --> 00:00:08,350
But before exploring that problem,

4
00:00:08,350 --> 00:00:10,645
we need to talk first about a previous problem.

5
00:00:10,645 --> 00:00:13,330
Namely that demanding explainability must

6
00:00:13,330 --> 00:00:15,130
recognize that there are different types of

7
00:00:15,130 --> 00:00:17,950
AI with different levels of complexity.

8
00:00:17,950 --> 00:00:19,030
Let's start with a simple kind

9
00:00:19,030 --> 00:00:20,470
of artificial intelligence.

10
00:00:20,470 --> 00:00:22,270
In fact, it's so simple that some wouldn't even

11
00:00:22,270 --> 00:00:24,820
classify it as a kind of artificial intelligence.

12
00:00:24,820 --> 00:00:26,440
To be sure what I have in mind is

13
00:00:26,440 --> 00:00:28,195
not an example of machine learning,

14
00:00:28,195 --> 00:00:29,875
but go with me here.

15
00:00:29,875 --> 00:00:31,570
Let's think about a system entirely

16
00:00:31,570 --> 00:00:33,790
governed by an if then program,

17
00:00:33,790 --> 00:00:35,910
such as a digital spreadsheet.

18
00:00:35,910 --> 00:00:38,720
This is a basic straightforward rule based program,

19
00:00:38,720 --> 00:00:41,800
but to our accountant ancestors in the 20th century,

20
00:00:41,800 --> 00:00:44,020
this may have well have been magic.

21
00:00:44,020 --> 00:00:45,490
After all, you could just change

22
00:00:45,490 --> 00:00:47,140
one number in your spreadsheet and

23
00:00:47,140 --> 00:00:48,880
the spreadsheet was intelligent enough to

24
00:00:48,880 --> 00:00:51,885
update all the other numbers automatically.

25
00:00:51,885 --> 00:00:54,390
So, how does Microsoft Excel work?

26
00:00:54,390 --> 00:00:56,195
How does it get its powers?

27
00:00:56,195 --> 00:00:57,520
Well that's my point, unless

28
00:00:57,520 --> 00:00:58,810
you're a computer scientist that would

29
00:00:58,810 --> 00:01:00,100
probably not even occurred to

30
00:01:00,100 --> 00:01:01,675
you to demand an explanation.

31
00:01:01,675 --> 00:01:04,000
Because we can safely assume that Excel is

32
00:01:04,000 --> 00:01:06,890
just executing functions exactly as we told it to.

33
00:01:06,890 --> 00:01:09,230
It's just following rules.

34
00:01:09,230 --> 00:01:12,020
But there are more recognizable kinds of AI,

35
00:01:12,020 --> 00:01:13,900
such as IBM's Deep Blue,

36
00:01:13,900 --> 00:01:17,260
which captured the popular imagination in the 90s.

37
00:01:17,260 --> 00:01:18,640
Deep Blue was programmed with

38
00:01:18,640 --> 00:01:21,820
many best practice chess principles such as,

39
00:01:21,820 --> 00:01:23,230
secure the center of the board

40
00:01:23,230 --> 00:01:25,675
before launching an attack on the flank.

41
00:01:25,675 --> 00:01:27,400
Deep Blue, the program,

42
00:01:27,400 --> 00:01:29,470
then optimize those principles by

43
00:01:29,470 --> 00:01:32,020
reviewing thousands of games and eventually,

44
00:01:32,020 --> 00:01:33,340
famously defeated the human

45
00:01:33,340 --> 00:01:36,180
chess champion Garry Kasparov.

46
00:01:36,180 --> 00:01:37,590
But that was 20 years ago.

47
00:01:37,590 --> 00:01:39,995
Which in Tech Time is an eternity.

48
00:01:39,995 --> 00:01:42,415
Today, we have systems such as AlphaGo,

49
00:01:42,415 --> 00:01:44,395
which learned to play the board game Go,

50
00:01:44,395 --> 00:01:46,895
by using neural network algorithms.

51
00:01:46,895 --> 00:01:50,095
This algorithm has recognizable outputs namely,

52
00:01:50,095 --> 00:01:51,575
moves in the game Go.

53
00:01:51,575 --> 00:01:54,250
But AlphaGo wasn't given principles

54
00:01:54,250 --> 00:01:57,280
that it then had to optimize like Deep Blue was.

55
00:01:57,280 --> 00:01:58,754
And here is the important part,

56
00:01:58,754 --> 00:02:00,415
it also did not generate

57
00:02:00,415 --> 00:02:02,645
its own principles from which to act.

58
00:02:02,645 --> 00:02:04,840
If that were the case, we could simply ask

59
00:02:04,840 --> 00:02:07,180
AlphaGo what principles it had discovered,

60
00:02:07,180 --> 00:02:09,730
so that we humans might learn them too.

61
00:02:09,730 --> 00:02:11,960
Now, it did generate something like a model,

62
00:02:11,960 --> 00:02:13,550
but there are two problems.

63
00:02:13,550 --> 00:02:17,520
First, this model is hidden under layers of complexity.

64
00:02:17,520 --> 00:02:19,720
And second, if you're imagining us digging

65
00:02:19,720 --> 00:02:22,120
through the top layers to get to the hidden model,

66
00:02:22,120 --> 00:02:26,220
what we would find still wouldn't be recognizable to us.

67
00:02:26,220 --> 00:02:29,110
And of course, any artificial intelligence system is

68
00:02:29,110 --> 00:02:30,640
what it is because of some kind

69
00:02:30,640 --> 00:02:32,305
of algorithm or algorithms.

70
00:02:32,305 --> 00:02:34,060
So a more accurate way to think about

71
00:02:34,060 --> 00:02:36,055
this increasing complexity of AI,

72
00:02:36,055 --> 00:02:39,265
is the varying level of complexity of algorithms.

73
00:02:39,265 --> 00:02:40,570
It seems safe to say that

74
00:02:40,570 --> 00:02:42,745
some algorithms are highly explainable,

75
00:02:42,745 --> 00:02:44,920
or at least could be designed to be that way.

76
00:02:44,920 --> 00:02:48,070
But this does not seem to be true of all algorithms.

77
00:02:48,070 --> 00:02:52,000
This is a major complication of the X.ai objective.

