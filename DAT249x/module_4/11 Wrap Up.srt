0
00:00:00,000 --> 00:00:02,620
>> Thanks for getting into the end of this course.

1
00:00:02,620 --> 00:00:05,505
We hope you've learned a ton about ethics, law,

2
00:00:05,505 --> 00:00:08,010
analytics and AI and how to apply it in

3
00:00:08,010 --> 00:00:11,465
your job and in the future work that you're going to do.

4
00:00:11,465 --> 00:00:15,480
So, any last comments or thoughts?

5
00:00:15,480 --> 00:00:17,610
>> Sure. I mean, in addition to our role as professors

6
00:00:17,610 --> 00:00:19,910
at Seattle University, we're also consultants.

7
00:00:19,910 --> 00:00:22,530
So search for us on the web at Digital Dignity.

8
00:00:22,530 --> 00:00:24,030
>> And it would be great.

9
00:00:24,030 --> 00:00:25,530
We'd love to hear from you. You can also

10
00:00:25,530 --> 00:00:27,695
reach out to us at Seattle University.

11
00:00:27,695 --> 00:00:29,160
We're easy to find and we'll also

12
00:00:29,160 --> 00:00:30,720
include our contact information.

13
00:00:30,720 --> 00:00:32,985
So if you have any questions

14
00:00:32,985 --> 00:00:36,375
or suggestions for future courses,

15
00:00:36,375 --> 00:00:37,560
definitely reach out to us

16
00:00:37,560 --> 00:00:39,720
by e-mail. Love to hear from you.

17
00:00:39,720 --> 00:00:42,000
>> Thanks so much. Bye.

