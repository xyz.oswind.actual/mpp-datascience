0
00:00:00,030 --> 00:00:04,210
>> Going back to mod one in our intro when

1
00:00:04,210 --> 00:00:07,120
Nathan had talked about how really on

2
00:00:07,120 --> 00:00:09,940
the ethics side of things, we talked about design.

3
00:00:09,940 --> 00:00:11,770
And that is, we must

4
00:00:11,770 --> 00:00:13,780
think about how to architect and build

5
00:00:13,780 --> 00:00:17,470
our systems in an ethical way before it goes to market,

6
00:00:17,470 --> 00:00:19,170
before it causes problems.

7
00:00:19,170 --> 00:00:20,410
And I think that's what

8
00:00:20,410 --> 00:00:22,565
this presentation is going to try to give you,

9
00:00:22,565 --> 00:00:24,160
are some fundamental principles

10
00:00:24,160 --> 00:00:26,185
that come from the Microsoft space,

11
00:00:26,185 --> 00:00:28,450
but really are trying to extend out

12
00:00:28,450 --> 00:00:30,220
our current thinking on

13
00:00:30,220 --> 00:00:33,640
best practices as it relates to the data in AI,

14
00:00:33,640 --> 00:00:35,775
and ethics, and design.

15
00:00:35,775 --> 00:00:40,490
So, Satya Nadella, back in 2016,

16
00:00:40,490 --> 00:00:44,485
had talked about how the debate that's out there,

17
00:00:44,485 --> 00:00:46,730
about ethics and AI, isn't really about good

18
00:00:46,730 --> 00:00:49,340
versus evil or it really shouldn't be.

19
00:00:49,340 --> 00:00:51,155
The debate rather should be about

20
00:00:51,155 --> 00:00:53,230
the values the people have,

21
00:00:53,230 --> 00:00:54,860
and the institutions have as

22
00:00:54,860 --> 00:00:56,670
well in creating this technology.

23
00:00:56,670 --> 00:00:58,280
And it's such a change.

24
00:00:58,280 --> 00:00:59,920
Right? It's just about

25
00:00:59,920 --> 00:01:03,140
the values as it relates to this technology,

26
00:01:03,140 --> 00:01:05,720
rather than it being

27
00:01:05,720 --> 00:01:07,970
bad or it shouldn't be allowed at all.

28
00:01:07,970 --> 00:01:09,770
I think the reality is it's here

29
00:01:09,770 --> 00:01:12,950
and what we do with it now is the question.

30
00:01:12,950 --> 00:01:14,530
So, when we're working with this,

31
00:01:14,530 --> 00:01:16,430
the first principle that he gives us is

32
00:01:16,430 --> 00:01:18,200
that AI must be designed to

33
00:01:18,200 --> 00:01:19,940
assist humanity and maximize

34
00:01:19,940 --> 00:01:23,210
efficiency without destroying the dignity of people.

35
00:01:23,210 --> 00:01:25,145
So, that's really about

36
00:01:25,145 --> 00:01:28,300
augmenting our abilities as humans, right?

37
00:01:28,300 --> 00:01:30,275
It's not to replace them,

38
00:01:30,275 --> 00:01:32,450
to destroy what we have,

39
00:01:32,450 --> 00:01:36,770
and empower those who perhaps even, let's say,

40
00:01:36,770 --> 00:01:39,800
many use cases today with artificial intelligence are

41
00:01:39,800 --> 00:01:42,680
being used to enhance a person

42
00:01:42,680 --> 00:01:45,050
with disabilities to be able to

43
00:01:45,050 --> 00:01:47,480
engage with technology more

44
00:01:47,480 --> 00:01:53,890
freely and enable diversity rather than constricted.

45
00:01:53,890 --> 00:01:57,470
So, the challenge right is that there are

46
00:01:57,470 --> 00:01:59,165
many different types of AI systems and

47
00:01:59,165 --> 00:02:01,790
applications in different industries and verticals.

48
00:02:01,790 --> 00:02:04,005
So, is there a one size fits all?

49
00:02:04,005 --> 00:02:06,928
Well, again, that's why we're trying to give a principle

50
00:02:06,928 --> 00:02:08,120
instead of

51
00:02:08,120 --> 00:02:11,060
just specific guidelines that you should step through.

52
00:02:11,060 --> 00:02:14,590
And some questions you might ask yourself around this on

53
00:02:14,590 --> 00:02:18,860
assisting in maximizing human efficiencies is that,

54
00:02:18,860 --> 00:02:21,640
what's a real world consequence of what I'm doing?

55
00:02:21,640 --> 00:02:25,120
Is there a benefit to the customer?

56
00:02:25,120 --> 00:02:27,200
Does my project show respect

57
00:02:27,200 --> 00:02:30,185
to individuals and communities?

58
00:02:30,185 --> 00:02:32,060
The second one that follows

59
00:02:32,060 --> 00:02:34,430
from this is always assisting humanity,

60
00:02:34,430 --> 00:02:36,860
we must also have these systems be

61
00:02:36,860 --> 00:02:39,338
transparent and have accountability as

62
00:02:39,338 --> 00:02:41,930
you're hearing more about from

63
00:02:41,930 --> 00:02:46,060
even Nathan so that we can undo unintended harm.

64
00:02:46,060 --> 00:02:49,190
AI should be fair and inclusive,

65
00:02:49,190 --> 00:02:52,070
but a bias is hard to detect.

66
00:02:52,070 --> 00:02:54,170
And, really,

67
00:02:54,170 --> 00:02:57,238
the system may reflect those beliefs of individuals,

68
00:02:57,238 --> 00:02:59,240
and also the data itself

69
00:02:59,240 --> 00:03:02,395
might even be opaque or hard to understand,

70
00:03:02,395 --> 00:03:05,270
perhaps even you've seen this in our lab that the data

71
00:03:05,270 --> 00:03:08,635
itself can be hard to interpret.

72
00:03:08,635 --> 00:03:11,380
So, when it comes to data though,

73
00:03:11,380 --> 00:03:13,600
not all bias is bad, really.

74
00:03:13,600 --> 00:03:16,570
If we're trying to create a service or a product that

75
00:03:16,570 --> 00:03:18,220
is catered towards an individual

76
00:03:18,220 --> 00:03:19,825
or population, of course,

77
00:03:19,825 --> 00:03:24,415
that service or product is going to be skewed or biased,

78
00:03:24,415 --> 00:03:27,510
perhaps, towards that particular use.

79
00:03:27,510 --> 00:03:29,760
And some questions,

80
00:03:29,760 --> 00:03:33,295
have you tested your assumptions out there?

81
00:03:33,295 --> 00:03:39,695
Can you detect harm from your work and move on?

82
00:03:39,695 --> 00:03:42,800
Have you made some disclosures like perhaps,

83
00:03:42,800 --> 00:03:47,215
the algorithm itself or the method is difficult to see?

84
00:03:47,215 --> 00:03:50,060
However, let's explain it in common terms,

85
00:03:50,060 --> 00:03:54,220
and make sure that people know what we're doing here.

86
00:03:54,220 --> 00:03:55,660
Do you feel good about it?

87
00:03:55,660 --> 00:03:59,030
Almost like a personal gut check.

88
00:03:59,030 --> 00:04:00,540
The most direct one

89
00:04:00,540 --> 00:04:02,810
towards biases is we must guard against it.

90
00:04:02,810 --> 00:04:06,808
Our customers trust us with their data,

91
00:04:06,808 --> 00:04:10,640
and we need to innovate on that data and create

92
00:04:10,640 --> 00:04:12,275
new and exciting things that

93
00:04:12,275 --> 00:04:16,866
bring new stuff to the world.

94
00:04:16,866 --> 00:04:21,630
This can be kind of systemic though.

95
00:04:21,630 --> 00:04:26,365
I think that we must guard against it actively.

96
00:04:26,365 --> 00:04:29,245
Again, the disclosure comes up,

97
00:04:29,245 --> 00:04:31,770
and that we should also

98
00:04:31,770 --> 00:04:35,040
respect the context in which this data arrives.

99
00:04:35,040 --> 00:04:38,260
We should minimize harm and increase trust.

100
00:04:38,260 --> 00:04:43,710
Some questions around bias come up in that,

101
00:04:43,710 --> 00:04:45,840
is their problem how I'm framing

102
00:04:45,840 --> 00:04:50,305
the whole the experiment or the whole service itself?

103
00:04:50,305 --> 00:04:54,015
Have I chosen the correct targets or variables?

104
00:04:54,015 --> 00:04:55,680
And, does bias in

105
00:04:55,680 --> 00:04:58,685
the data put some population at a disadvantage?

106
00:04:58,685 --> 00:05:00,235
The great question to ask is,

107
00:05:00,235 --> 00:05:03,150
you're architecting telemetry all the way

108
00:05:03,150 --> 00:05:06,875
through to the outputs in an AI system,

109
00:05:06,875 --> 00:05:09,705
and is there anyone I can consult with perhaps?

110
00:05:09,705 --> 00:05:12,650
Subject matter experts who know the field, who know,

111
00:05:12,650 --> 00:05:14,875
have that domain specific knowledge that you

112
00:05:14,875 --> 00:05:18,245
as a data practitioner might not have.

113
00:05:18,245 --> 00:05:22,775
Next, AI must be designed for intelligent privacy.

114
00:05:22,775 --> 00:05:26,870
This means it's not just privacy at all costs,

115
00:05:26,870 --> 00:05:29,690
it really means that we respect a number of things,

116
00:05:29,690 --> 00:05:31,850
a number of stakeholders, customer wishes.

117
00:05:31,850 --> 00:05:33,575
We don't leak customer data,

118
00:05:33,575 --> 00:05:36,200
and we leave those people alone,

119
00:05:36,200 --> 00:05:37,545
who would like to be left alone.

120
00:05:37,545 --> 00:05:39,740
They have an opt-in, opt-out ability.

121
00:05:39,740 --> 00:05:40,785
As we've seen with the GDPR,

122
00:05:40,785 --> 00:05:44,110
that kind of stuff is becoming more common.

123
00:05:44,110 --> 00:05:47,935
And the challenge as you've seen in lab two,

124
00:05:47,935 --> 00:05:53,085
there is more to PII than just credit card numbers.

125
00:05:53,085 --> 00:05:55,150
Proxies can come into play.

126
00:05:55,150 --> 00:05:58,055
Even just a simple thing like name, and address,

127
00:05:58,055 --> 00:06:00,315
and your date of birth,

128
00:06:00,315 --> 00:06:02,720
can individually identify you.

129
00:06:02,720 --> 00:06:05,750
And we should do as much as we can in

130
00:06:05,750 --> 00:06:08,675
an intelligent fashion to provide flexibility,

131
00:06:08,675 --> 00:06:11,005
but also that privacy.

132
00:06:11,005 --> 00:06:13,340
So, questions for you.

133
00:06:13,340 --> 00:06:14,580
Do I understand how

134
00:06:14,580 --> 00:06:18,550
personal data differs from PII? You now do.

135
00:06:18,550 --> 00:06:22,235
And, have I taken steps to

136
00:06:22,235 --> 00:06:24,285
plug holes if there are any or

137
00:06:24,285 --> 00:06:26,940
preemptively work against a breach?

138
00:06:26,940 --> 00:06:29,550
And perhaps, has my work or

139
00:06:29,550 --> 00:06:31,230
my system gone through a privacy review

140
00:06:31,230 --> 00:06:32,555
in my company or organization?

141
00:06:32,555 --> 00:06:34,140
And the last principle that we

142
00:06:34,140 --> 00:06:36,075
have is that AI must be secure,

143
00:06:36,075 --> 00:06:38,475
related to intelligent privacy.

144
00:06:38,475 --> 00:06:42,390
It's really about the consumer confidentiality

145
00:06:42,390 --> 00:06:43,920
that they give us,

146
00:06:43,920 --> 00:06:46,205
that we uphold their integrity.

147
00:06:46,205 --> 00:06:48,630
We provide them those protections that they

148
00:06:48,630 --> 00:06:52,350
need in order to trustfully use our services.

149
00:06:52,350 --> 00:06:54,780
And it's more than just basic security,

150
00:06:54,780 --> 00:06:56,695
it's really about being predictive

151
00:06:56,695 --> 00:06:58,320
of using that AI in a way

152
00:06:58,320 --> 00:07:00,210
that really leverages all of

153
00:07:00,210 --> 00:07:03,450
its capability to detect anomalies,

154
00:07:03,450 --> 00:07:09,895
to establish that foundation for a trusting AI future.

155
00:07:09,895 --> 00:07:11,340
There some questions that you can ask yourself

156
00:07:11,340 --> 00:07:15,195
about security and AI are,

157
00:07:15,195 --> 00:07:17,070
what are your specific use cases?

158
00:07:17,070 --> 00:07:18,465
How is the input coming in?

159
00:07:18,465 --> 00:07:20,235
Is it coming through gestures

160
00:07:20,235 --> 00:07:22,440
and human-like interactions?

161
00:07:22,440 --> 00:07:25,392
How can you document your assumptions?

162
00:07:25,392 --> 00:07:28,140
How can you build

163
00:07:28,140 --> 00:07:29,490
upon what your company already

164
00:07:29,490 --> 00:07:31,450
has in terms of best practices there?

165
00:07:31,450 --> 00:07:33,621
And also,

166
00:07:33,621 --> 00:07:37,240
it's thinking about the attacker, the potential attacker,

167
00:07:37,240 --> 00:07:40,380
are there any ways that the attacker could interact with

168
00:07:40,380 --> 00:07:44,220
your AI in such a way to make others

169
00:07:44,220 --> 00:07:47,940
look bad or use that information for

170
00:07:47,940 --> 00:07:50,365
other nefarious purposes or

171
00:07:50,365 --> 00:07:53,389
discriminate or incriminate others?

172
00:07:53,389 --> 00:07:58,445
In summary, we've seen that the design for

173
00:07:58,445 --> 00:08:01,490
AI is just as important as design

174
00:08:01,490 --> 00:08:05,308
for the right telemetry system in that,

175
00:08:05,308 --> 00:08:07,210
the more complex it gets large,

176
00:08:07,210 --> 00:08:09,613
the higher up you go in scale.

177
00:08:09,613 --> 00:08:11,480
The more important is that we

178
00:08:11,480 --> 00:08:14,500
treat humans with dignity and respect,

179
00:08:14,500 --> 00:08:17,660
as well as enable and unlock the full potential

180
00:08:17,660 --> 00:08:21,000
that this technology has in the world. Thank you.

