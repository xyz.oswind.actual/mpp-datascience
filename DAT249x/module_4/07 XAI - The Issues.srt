0
00:00:00,000 --> 00:00:03,080
>> There are several issues faced by anyone

1
00:00:03,080 --> 00:00:05,850
wanting to make Artificial Intelligence explainable.

2
00:00:05,850 --> 00:00:08,865
Let's review of a few of the basic ones.

3
00:00:08,865 --> 00:00:11,600
First, many data scientists are worried that there is a

4
00:00:11,600 --> 00:00:15,210
basic trade-off between explain ability and performance.

5
00:00:15,210 --> 00:00:17,240
On this view, at the extremes,

6
00:00:17,240 --> 00:00:18,590
we could perhaps make systems

7
00:00:18,590 --> 00:00:20,190
that are highly explainable,

8
00:00:20,190 --> 00:00:22,445
or we can make systems that work well,

9
00:00:22,445 --> 00:00:23,830
but we could not do both.

10
00:00:23,830 --> 00:00:25,445
And you can imagine a continuum,

11
00:00:25,445 --> 00:00:28,160
such that the more explainable an AI system is,

12
00:00:28,160 --> 00:00:30,495
the less effective it is.

13
00:00:30,495 --> 00:00:32,290
The concern from some data scientists,

14
00:00:32,290 --> 00:00:33,680
is that an algorithm would have to

15
00:00:33,680 --> 00:00:36,380
sacrifice a lot of its performance and accuracy

16
00:00:36,380 --> 00:00:37,910
in order to make it capable of

17
00:00:37,910 --> 00:00:39,440
delivering the kind of explanation

18
00:00:39,440 --> 00:00:43,895
expected by such legal frameworks as the GDPR.

19
00:00:43,895 --> 00:00:47,260
Secondly, when we demand explain ability,

20
00:00:47,260 --> 00:00:49,970
to whom are we expecting it to be explainable?

21
00:00:49,970 --> 00:00:51,870
To the leading experts in AI?

22
00:00:51,870 --> 00:00:54,845
To entry level data scientists right out of college?

23
00:00:54,845 --> 00:00:57,320
To the executives and other professionals that will use

24
00:00:57,320 --> 00:00:59,880
the algorithmic outputs to make decisions?

25
00:00:59,880 --> 00:01:03,170
To the regular people who are affected by the decision?

26
00:01:03,170 --> 00:01:06,215
Now, this particular problem doesn't seem insurmountable,

27
00:01:06,215 --> 00:01:07,390
but it is an important one that

28
00:01:07,390 --> 00:01:09,050
the XAI proponents must

29
00:01:09,050 --> 00:01:10,760
have a serious conversation about,

30
00:01:10,760 --> 00:01:12,470
because providing explanations at

31
00:01:12,470 --> 00:01:15,985
those different levels will present different challenges.

32
00:01:15,985 --> 00:01:18,620
Third, and I know you're going to think this sounds like

33
00:01:18,620 --> 00:01:21,418
a future dystopia dreamed up by Hollywood,

34
00:01:21,418 --> 00:01:23,420
but it is something we should think about now.

35
00:01:23,420 --> 00:01:26,345
The question is, how similar exactly

36
00:01:26,345 --> 00:01:28,340
Artificial Intelligence will eventually

37
00:01:28,340 --> 00:01:30,100
be to human intelligence?

38
00:01:30,100 --> 00:01:32,915
Remember, DARPA wants to design systems that,

39
00:01:32,915 --> 00:01:35,030
quote, can explain their rationale,

40
00:01:35,030 --> 00:01:37,025
characterize their strengths and weaknesses,

41
00:01:37,025 --> 00:01:38,585
and convey an understanding

42
00:01:38,585 --> 00:01:41,190
of how they will behave in the future.

43
00:01:41,190 --> 00:01:42,530
In other words, what they are

44
00:01:42,530 --> 00:01:43,670
saying is that we should want

45
00:01:43,670 --> 00:01:45,500
machine decision makers to be

46
00:01:45,500 --> 00:01:47,700
able to reflect on their own decisions,

47
00:01:47,700 --> 00:01:51,530
in something like the way human decision makers can do.

48
00:01:51,530 --> 00:01:53,180
But many brain researchers,

49
00:01:53,180 --> 00:01:55,655
social scientists, and even philosophers,

50
00:01:55,655 --> 00:01:57,200
believe that humans generate

51
00:01:57,200 --> 00:02:00,590
explanations for their decisions after the fact.

52
00:02:00,590 --> 00:02:02,160
That is, we make a decision for

53
00:02:02,160 --> 00:02:05,335
a reason that we ourselves don't understand.

54
00:02:05,335 --> 00:02:07,250
Perhaps, because it is an explanation that

55
00:02:07,250 --> 00:02:09,770
involves the collision of neurons.

56
00:02:09,770 --> 00:02:10,640
And then when asked to give

57
00:02:10,640 --> 00:02:12,185
an explanation for our decision,

58
00:02:12,185 --> 00:02:14,945
we come up with something that sounds plausible,

59
00:02:14,945 --> 00:02:18,080
but is not the true explanation.

60
00:02:18,080 --> 00:02:19,940
Now, this is not necessarily lying,

61
00:02:19,940 --> 00:02:22,180
because we might believe it ourselves.

62
00:02:22,180 --> 00:02:24,620
And, of course, it could be worse as and when humans lie

63
00:02:24,620 --> 00:02:27,245
about their own decision making process intentionally,

64
00:02:27,245 --> 00:02:29,210
perhaps to cover up some bias.

65
00:02:29,210 --> 00:02:32,780
So, will machine decision makers learn to mimic

66
00:02:32,780 --> 00:02:34,340
our bad habits and give us

67
00:02:34,340 --> 00:02:38,365
false explanations to satisfy our inquiries?

68
00:02:38,365 --> 00:02:40,910
Fourth, explain-ability can mean several things.

69
00:02:40,910 --> 00:02:43,130
Remember, so far, we have only been able to

70
00:02:43,130 --> 00:02:45,975
agree that we want non-inscrutable algorithms.

71
00:02:45,975 --> 00:02:47,255
But in the AI literature,

72
00:02:47,255 --> 00:02:50,195
explanation has been thought of as transparency,

73
00:02:50,195 --> 00:02:54,470
or justification, or demonstration, or interpretation.

74
00:02:54,470 --> 00:02:56,870
These words might show up together in a thesaurus,

75
00:02:56,870 --> 00:02:59,175
but they actually mean very different things.

76
00:02:59,175 --> 00:03:01,250
So we have to get clear on what we want,

77
00:03:01,250 --> 00:03:03,495
when we want an explanation.

78
00:03:03,495 --> 00:03:06,080
Lastly, it's important to recognize that not all

79
00:03:06,080 --> 00:03:08,210
Artificial Intelligence uses machine

80
00:03:08,210 --> 00:03:10,605
learning algorithms that work the same way,

81
00:03:10,605 --> 00:03:12,800
and this will inevitably lead to differences in

82
00:03:12,800 --> 00:03:16,110
both the quality and degree of available explanations.

83
00:03:16,110 --> 00:03:17,713
These last two issues,

84
00:03:17,713 --> 00:03:19,265
the meaning of explain ability,

85
00:03:19,265 --> 00:03:21,095
and the differences between algorithms

86
00:03:21,095 --> 00:03:22,865
are especially difficult issues,

87
00:03:22,865 --> 00:03:25,610
so let's explore them a little more.

