0
00:00:00,270 --> 00:00:02,545
In our final module,

1
00:00:02,545 --> 00:00:05,320
we'll be moving from analytics and right into

2
00:00:05,320 --> 00:00:06,490
artificial intelligence as it

3
00:00:06,490 --> 00:00:08,255
relates to ethical and legal issues.

4
00:00:08,255 --> 00:00:09,400
Not that we haven't been talking

5
00:00:09,400 --> 00:00:10,855
about those things before,

6
00:00:10,855 --> 00:00:15,430
but especially now we're going to get into the weeds.

7
00:00:15,430 --> 00:00:17,015
So, Eva and Nathan,

8
00:00:17,015 --> 00:00:19,870
what are students going to learn in this module?

9
00:00:19,870 --> 00:00:22,840
>> I think that this module is,

10
00:00:22,840 --> 00:00:26,425
we'll see some common themes from our earlier modules.

11
00:00:26,425 --> 00:00:27,520
But because we're talking about

12
00:00:27,520 --> 00:00:29,200
artificial intelligence and the level

13
00:00:29,200 --> 00:00:32,325
of technology that is just changing so rapidly,

14
00:00:32,325 --> 00:00:34,090
the relationship between law and

15
00:00:34,090 --> 00:00:37,520
this technology is even more obscure.

16
00:00:37,520 --> 00:00:38,770
So we'll have instances on

17
00:00:38,770 --> 00:00:40,540
applying existing law to what we

18
00:00:40,540 --> 00:00:41,770
can speculate might be

19
00:00:41,770 --> 00:00:43,885
happening in artificial intelligence.

20
00:00:43,885 --> 00:00:45,220
But in many cases, we're just

21
00:00:45,220 --> 00:00:49,480
really waiting to see how things develop.

22
00:00:49,480 --> 00:00:51,405
Some things are surprising.

23
00:00:51,405 --> 00:00:53,200
Like we look at machine learning

24
00:00:53,200 --> 00:00:56,770
that starts to act like humans.

25
00:00:56,770 --> 00:00:58,945
There is instances of robots

26
00:00:58,945 --> 00:01:01,675
becoming racist in their language for example,

27
00:01:01,675 --> 00:01:03,610
and wondering well first of all, how does that happen,

28
00:01:03,610 --> 00:01:05,150
and then secondly how do we address it,

29
00:01:05,150 --> 00:01:06,680
and who's responsible for it?

30
00:01:06,680 --> 00:01:09,640
So we have things that happen in the space like that.

31
00:01:09,640 --> 00:01:12,610
We also see that there's a shared responsibility,

32
00:01:12,610 --> 00:01:15,805
legal responsibility that we have to really tease out.

33
00:01:15,805 --> 00:01:21,200
New products, doing new things, making decisions.

34
00:01:21,200 --> 00:01:22,505
They might be good decisions,

35
00:01:22,505 --> 00:01:25,445
they might not work out to be so good in the long run.

36
00:01:25,445 --> 00:01:26,620
So we don't know where

37
00:01:26,620 --> 00:01:28,055
the legal liability is going to rest.

38
00:01:28,055 --> 00:01:30,280
So I think it might be a little bit troubling for

39
00:01:30,280 --> 00:01:33,170
students on one hand because it is so obscure.

40
00:01:33,170 --> 00:01:37,090
But I'd say for students in this course, just be patient,

41
00:01:37,090 --> 00:01:39,565
be open, and be creative when

42
00:01:39,565 --> 00:01:40,810
we see how we're applying the law

43
00:01:40,810 --> 00:01:42,365
to artificial intelligence.

44
00:01:42,365 --> 00:01:44,110
>> Great. Thank you.

45
00:01:44,110 --> 00:01:46,630
>> When people hear the phrase artificial intelligence,

46
00:01:46,630 --> 00:01:49,045
a lot of people think about the future,

47
00:01:49,045 --> 00:01:51,250
and maybe they think about beings walking

48
00:01:51,250 --> 00:01:54,930
around that look like humans but aren't really human.

49
00:01:54,930 --> 00:01:56,200
And if you're thinking about that in

50
00:01:56,200 --> 00:01:57,425
ethical context you might wondered, "Gee,

51
00:01:57,425 --> 00:02:00,540
do we have any moral responsibility to these beings?"

52
00:02:00,540 --> 00:02:02,950
I think those are interesting thought experiments to run.

53
00:02:02,950 --> 00:02:05,830
I actually talked about them in my university courses,

54
00:02:05,830 --> 00:02:07,090
but we're talking about things that

55
00:02:07,090 --> 00:02:08,230
are happening right now,

56
00:02:08,230 --> 00:02:10,255
issues that are important right now.

57
00:02:10,255 --> 00:02:13,757
So artificial intelligence relies on algorithms,

58
00:02:13,757 --> 00:02:16,420
and one of the really hot topics in ethics right now

59
00:02:16,420 --> 00:02:17,920
is what kind of

60
00:02:17,920 --> 00:02:20,185
decision making power do these beings have?

61
00:02:20,185 --> 00:02:22,240
Which is an ethical issue because if we

62
00:02:22,240 --> 00:02:23,680
fundamentally lose track of

63
00:02:23,680 --> 00:02:25,560
how machines are making decisions,

64
00:02:25,560 --> 00:02:27,970
there is a breakdown in trust as we've been

65
00:02:27,970 --> 00:02:30,790
seeing from the labs.

66
00:02:30,790 --> 00:02:32,830
There can be bias issues easily,

67
00:02:32,830 --> 00:02:34,120
if we don't really understand

68
00:02:34,120 --> 00:02:36,920
what variables are being taken into account.

69
00:02:36,920 --> 00:02:39,100
And there's also something important about autonomy.

70
00:02:39,100 --> 00:02:41,260
Autonomy being one of the ethical values.

71
00:02:41,260 --> 00:02:42,835
Is that when we don't have

72
00:02:42,835 --> 00:02:44,740
the power to control our destiny

73
00:02:44,740 --> 00:02:46,450
anymore and we can't make our own decisions

74
00:02:46,450 --> 00:02:49,160
when we outsource all that to a robot,

75
00:02:49,160 --> 00:02:52,570
there's something that's lost about our humanness.

76
00:02:52,570 --> 00:02:53,800
So, that's one of

77
00:02:53,800 --> 00:02:56,360
the big topics that we'll discuss in this course.

78
00:02:56,360 --> 00:02:58,420
>> Excellent. Thank you.

79
00:02:58,420 --> 00:03:00,860
And let's dig in one last time.

