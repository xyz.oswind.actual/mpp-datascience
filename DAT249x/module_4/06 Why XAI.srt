0
00:00:00,000 --> 00:00:02,760
The American Department of Defense has

1
00:00:02,760 --> 00:00:05,283
an important and well-known branch called DARPA,

2
00:00:05,283 --> 00:00:07,830
which is the branch that attempts to understand and

3
00:00:07,830 --> 00:00:09,540
use emerging technologies in

4
00:00:09,540 --> 00:00:12,065
the context of national security.

5
00:00:12,065 --> 00:00:14,574
Naturally, much of their work is top secret,

6
00:00:14,574 --> 00:00:16,110
but they do sometimes publicly

7
00:00:16,110 --> 00:00:18,030
disclose their general goals.

8
00:00:18,030 --> 00:00:19,785
One such goal is to create

9
00:00:19,785 --> 00:00:22,230
artificial intelligence systems that are

10
00:00:22,230 --> 00:00:26,155
explainable to human beings, abbreviated XAI.

11
00:00:26,155 --> 00:00:29,100
They say that if their goal is achieved and I quote,

12
00:00:29,100 --> 00:00:30,840
"New machine learning systems will have

13
00:00:30,840 --> 00:00:32,903
the ability to explain their rationale,

14
00:00:32,903 --> 00:00:35,070
characterize their strengths and weaknesses,

15
00:00:35,070 --> 00:00:36,960
and convey an understanding of how they

16
00:00:36,960 --> 00:00:39,355
will behave in the future."

17
00:00:39,355 --> 00:00:42,390
This language has also surfaced as the European Union

18
00:00:42,390 --> 00:00:43,684
moves to implement the

19
00:00:43,684 --> 00:00:45,885
General Data Protection Regulation,

20
00:00:45,885 --> 00:00:47,610
GDPR, next year,

21
00:00:47,610 --> 00:00:50,280
which we have already described in some detail.

22
00:00:50,280 --> 00:00:51,870
One element of this comprehensive

23
00:00:51,870 --> 00:00:53,400
regulatory framework is that it

24
00:00:53,400 --> 00:00:56,930
attempts to establish a right to explanation.

25
00:00:56,930 --> 00:00:59,190
So that when an algorithm makes a decision about you,

26
00:00:59,190 --> 00:01:02,520
such a decision to reject your credit application,

27
00:01:02,520 --> 00:01:05,605
you have the right to ask why that decision was made.

28
00:01:05,605 --> 00:01:07,665
Several researchers have been using the term

29
00:01:07,665 --> 00:01:11,085
inscrutable to refer to unexplainable algorithms.

30
00:01:11,085 --> 00:01:12,580
So at the most general level,

31
00:01:12,580 --> 00:01:16,610
inscrutable is the opposite of explainable.

32
00:01:16,610 --> 00:01:18,270
There is consensus that avoiding

33
00:01:18,270 --> 00:01:21,565
inscrutable algorithms is a praiseworthy goal.

34
00:01:21,565 --> 00:01:22,770
We believe there are legal and

35
00:01:22,770 --> 00:01:24,210
ethical reasons to be extremely

36
00:01:24,210 --> 00:01:25,890
suspicious of a future where

37
00:01:25,890 --> 00:01:27,585
decisions are made that affect,

38
00:01:27,585 --> 00:01:29,655
or even determine human well-being,

39
00:01:29,655 --> 00:01:30,885
and yet no human could know

40
00:01:30,885 --> 00:01:33,430
anything about how those decisions are made.

41
00:01:33,430 --> 00:01:35,160
The kind of future that is most likely

42
00:01:35,160 --> 00:01:37,710
compatible with human well-being is one where

43
00:01:37,710 --> 00:01:40,965
algorithmic discoveries support human decision making

44
00:01:40,965 --> 00:01:44,340
instead of actually replacing human decision making.

45
00:01:44,340 --> 00:01:45,450
This is the difference between

46
00:01:45,450 --> 00:01:47,400
using artificial intelligence as

47
00:01:47,400 --> 00:01:50,815
a tool to make our decisions and policies more accurate,

48
00:01:50,815 --> 00:01:53,690
intelligent, and humane and on the other hand,

49
00:01:53,690 --> 00:01:57,450
using AI as a crutch that does our thinking for us.

50
00:01:57,450 --> 00:01:59,340
In module one, we talked about how to

51
00:01:59,340 --> 00:02:02,130
respond properly to algorithmic conclusions.

52
00:02:02,130 --> 00:02:04,350
We believe that there are many options in between

53
00:02:04,350 --> 00:02:07,070
simply accepting them or rejecting them.

54
00:02:07,070 --> 00:02:08,640
Instead, it is important that

55
00:02:08,640 --> 00:02:10,373
we accept them critically and

56
00:02:10,373 --> 00:02:11,835
this kind of thinking requires

57
00:02:11,835 --> 00:02:14,510
asking thoughtful questions about them.

58
00:02:14,510 --> 00:02:16,965
But in addition to being properly cautious,

59
00:02:16,965 --> 00:02:18,480
in order for algorithms to serve

60
00:02:18,480 --> 00:02:20,295
the role of tools and not crutches,

61
00:02:20,295 --> 00:02:22,680
they must be on some level and in

62
00:02:22,680 --> 00:02:25,545
some sense, non inscrutable.

63
00:02:25,545 --> 00:02:27,030
So at this most basic level,

64
00:02:27,030 --> 00:02:28,560
we should all be able to agree that

65
00:02:28,560 --> 00:02:30,640
we want explainable algorithms.

66
00:02:30,640 --> 00:02:32,430
At least if we understand that term,

67
00:02:32,430 --> 00:02:35,650
simply meaning non inscrutable.

68
00:02:35,650 --> 00:02:37,170
But that is where the agreement

69
00:02:37,170 --> 00:02:39,480
stops and the complexity starts.

70
00:02:39,480 --> 00:02:42,060
I will further introduce XAI by naming some of

71
00:02:42,060 --> 00:02:45,950
the general complications with XAI in the next video.

