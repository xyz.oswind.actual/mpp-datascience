0
00:00:00,000 --> 00:00:03,685
What we're talking about in this video are

1
00:00:03,685 --> 00:00:08,560
Automatic or Semiautomatic vehicles.

2
00:00:08,560 --> 00:00:10,485
There is a range of

3
00:00:10,485 --> 00:00:13,380
automation that is actually in existence today.

4
00:00:13,380 --> 00:00:15,480
So we have cars in existence today that

5
00:00:15,480 --> 00:00:18,225
have automation like auto brakes,

6
00:00:18,225 --> 00:00:19,530
and then we go all the way to

7
00:00:19,530 --> 00:00:21,870
fully self-driving vehicles which we don't have

8
00:00:21,870 --> 00:00:23,550
yet but are being tested

9
00:00:23,550 --> 00:00:26,400
by certain companies like Tesla Motors right now.

10
00:00:26,400 --> 00:00:27,750
This is still futuristic but

11
00:00:27,750 --> 00:00:29,520
something that we're looking towards.

12
00:00:29,520 --> 00:00:32,430
Now with automobiles and vehicles,

13
00:00:32,430 --> 00:00:33,870
they are heavily regulated.

14
00:00:33,870 --> 00:00:36,390
So the design and manufactures of automobiles is heavily

15
00:00:36,390 --> 00:00:39,728
regulated by federal law, statutory law,

16
00:00:39,728 --> 00:00:42,060
and also in some respects by common law which

17
00:00:42,060 --> 00:00:44,410
provides legal remedies under products

18
00:00:44,410 --> 00:00:47,040
liability for defects in the design and

19
00:00:47,040 --> 00:00:50,490
manufacture that might cause consumer harm.

20
00:00:50,490 --> 00:00:52,260
We may have more defective warning

21
00:00:52,260 --> 00:00:55,015
claims as we move into more automation,

22
00:00:55,015 --> 00:00:56,335
as the facts change,

23
00:00:56,335 --> 00:00:57,960
the theories of products liability law

24
00:00:57,960 --> 00:01:00,180
will also evolve and expand.

25
00:01:00,180 --> 00:01:03,270
There's two issues to note here for our purposes.

26
00:01:03,270 --> 00:01:06,720
One is Cybersecurity Issues and the second is Changing

27
00:01:06,720 --> 00:01:08,550
Liability Issues as we

28
00:01:08,550 --> 00:01:10,860
become more autonomous with our vehicles.

29
00:01:10,860 --> 00:01:12,720
First, the cybersecurity threat.

30
00:01:12,720 --> 00:01:15,540
This is very unique to autonomous cars.

31
00:01:15,540 --> 00:01:17,670
It's actually possible for hackers to hack

32
00:01:17,670 --> 00:01:19,920
into the car and take control away

33
00:01:19,920 --> 00:01:22,344
from a driver in a semi-autonomous car

34
00:01:22,344 --> 00:01:24,570
and also a fully autonomous car.

35
00:01:24,570 --> 00:01:28,180
For example, in July of 2015,

36
00:01:28,180 --> 00:01:31,305
two hackers Charlie Miller and Chris Valasek,

37
00:01:31,305 --> 00:01:33,840
developed a tool to hijack the controls

38
00:01:33,840 --> 00:01:36,585
of a Jeep Grand Cherokee while the driver,

39
00:01:36,585 --> 00:01:39,624
senior writer for Wired Magazine,

40
00:01:39,624 --> 00:01:42,640
Andy Greenberg, was at the wheel in St. Louis.

41
00:01:42,640 --> 00:01:44,190
Andy did not know what exactly

42
00:01:44,190 --> 00:01:45,870
would happen during this experiment.

43
00:01:45,870 --> 00:01:47,295
He did know it was going to happen,

44
00:01:47,295 --> 00:01:49,105
but he didn't know the extent of it.

45
00:01:49,105 --> 00:01:50,520
He was surprised when his vehicle

46
00:01:50,520 --> 00:01:52,260
began blasting cold air,

47
00:01:52,260 --> 00:01:53,660
his radio station switched,

48
00:01:53,660 --> 00:01:55,065
the windshield wipers turned on,

49
00:01:55,065 --> 00:01:56,940
and the transmission was cut while he was

50
00:01:56,940 --> 00:02:00,510
traveling at 70 miles per hour down the highway.

51
00:02:00,510 --> 00:02:02,220
The hackers were able to take control of

52
00:02:02,220 --> 00:02:05,245
Andy's vehicle through the jeeps entertainment system.

53
00:02:05,245 --> 00:02:06,960
Fortunately for Andy, he

54
00:02:06,960 --> 00:02:08,970
knew as I said that this was going to happen,

55
00:02:08,970 --> 00:02:10,170
he was aware of it, he just

56
00:02:10,170 --> 00:02:11,910
wasn't aware of the extent of it.

57
00:02:11,910 --> 00:02:13,725
The hackers conducting this experiment

58
00:02:13,725 --> 00:02:15,120
were part of a grant funded by

59
00:02:15,120 --> 00:02:19,610
the Defense Advanced Research Projects Agency or DARPA.

60
00:02:19,610 --> 00:02:21,670
It's part of an effort to understand

61
00:02:21,670 --> 00:02:24,871
the vulnerability of vehicle systems to cyber attacks.

62
00:02:24,871 --> 00:02:28,570
Their findings indicated that more than 400,000 vehicles

63
00:02:28,570 --> 00:02:30,460
on the road today could be susceptible

64
00:02:30,460 --> 00:02:33,480
to cyber attacks like that one.

65
00:02:33,480 --> 00:02:35,080
So cybersecurity is also

66
00:02:35,080 --> 00:02:37,405
an issue for us with smart cities.

67
00:02:37,405 --> 00:02:41,560
Self-driving cars will require smart roads for operation.

68
00:02:41,560 --> 00:02:43,630
That means cameras and sensors are going to be

69
00:02:43,630 --> 00:02:46,240
built into roadways and street signs.

70
00:02:46,240 --> 00:02:47,620
Sensors will use radar and

71
00:02:47,620 --> 00:02:50,320
lidar to communicate with the vehicles.

72
00:02:50,320 --> 00:02:51,880
Some local governments are looking to

73
00:02:51,880 --> 00:02:53,680
invest in smart infrastructure that will

74
00:02:53,680 --> 00:02:55,450
allow communication between the roads

75
00:02:55,450 --> 00:02:57,270
and the vehicles traveling on them.

76
00:02:57,270 --> 00:03:00,970
For example, Columbus Ohio recently won $65 million in

77
00:03:00,970 --> 00:03:02,590
grants to become the first U.S.

78
00:03:02,590 --> 00:03:05,125
city to integrate self-driving cars,

79
00:03:05,125 --> 00:03:08,435
connected vehicles, and smart sensors.

80
00:03:08,435 --> 00:03:10,380
Atlanta Georgia has built

81
00:03:10,380 --> 00:03:11,970
a fiber and electrical network in

82
00:03:11,970 --> 00:03:13,500
its downtown Carter to support

83
00:03:13,500 --> 00:03:15,060
roadside sensors and cameras

84
00:03:15,060 --> 00:03:17,010
for driverless cars with hopes that

85
00:03:17,010 --> 00:03:18,960
the technology companies will develop and

86
00:03:18,960 --> 00:03:21,355
test their products in the city.

87
00:03:21,355 --> 00:03:23,160
These smart city designs involve

88
00:03:23,160 --> 00:03:24,930
extensive communication networks that

89
00:03:24,930 --> 00:03:26,935
could be vulnerable to cyber attack,

90
00:03:26,935 --> 00:03:29,280
and interruption in these interconnected systems

91
00:03:29,280 --> 00:03:31,230
could cause significant damage.

92
00:03:31,230 --> 00:03:32,605
Because it's so new,

93
00:03:32,605 --> 00:03:34,350
this technology has not yet been put through

94
00:03:34,350 --> 00:03:37,095
its paces and security testing.

95
00:03:37,095 --> 00:03:38,790
As cities begin to replace

96
00:03:38,790 --> 00:03:40,485
old infrastructure with the new,

97
00:03:40,485 --> 00:03:42,270
the evaluation and monitoring of

98
00:03:42,270 --> 00:03:46,230
these cybersecurity concerns is going to be critical.

99
00:03:46,230 --> 00:03:48,195
Now our second issue is shifting liability,

100
00:03:48,195 --> 00:03:49,995
meaning shifting liability between

101
00:03:49,995 --> 00:03:52,757
the driver and the autonomous car.

102
00:03:52,757 --> 00:03:54,525
Right now as we drive,

103
00:03:54,525 --> 00:03:56,700
if you're driving you are responsible,

104
00:03:56,700 --> 00:03:59,345
you're insured and you're responsible for any liability.

105
00:03:59,345 --> 00:04:01,455
But what happens when the driving is shared

106
00:04:01,455 --> 00:04:04,110
or is turned over to the technology?

107
00:04:04,110 --> 00:04:06,885
Who is responsible for the harm caused?

108
00:04:06,885 --> 00:04:09,030
Is it the car? Is it the driver?

109
00:04:09,030 --> 00:04:10,885
Is the car now the drivers agent?

110
00:04:10,885 --> 00:04:13,350
These are all questions that we're going to have to

111
00:04:13,350 --> 00:04:14,640
address eventually as we

112
00:04:14,640 --> 00:04:17,625
become more autonomous in our vehicles.

113
00:04:17,625 --> 00:04:19,280
So, an example of this with

114
00:04:19,280 --> 00:04:23,285
the product design happened in 2015.

115
00:04:23,285 --> 00:04:25,740
This is again a concern for shifting liability.

116
00:04:25,740 --> 00:04:27,900
In 2015, Tesla released

117
00:04:27,900 --> 00:04:32,180
an autopilot mode of its Model S. The following year,

118
00:04:32,180 --> 00:04:33,840
a driver of the Model S was

119
00:04:33,840 --> 00:04:35,715
operating it in autopilot mode,

120
00:04:35,715 --> 00:04:38,880
which means that the car was autonomously braking,

121
00:04:38,880 --> 00:04:41,280
steering, and lane switching.

122
00:04:41,280 --> 00:04:43,080
The car collided with a tractor trailer

123
00:04:43,080 --> 00:04:44,835
that was making a turn.

124
00:04:44,835 --> 00:04:46,590
The autopilot sensors failed to

125
00:04:46,590 --> 00:04:48,420
recognize the difference between the truck and

126
00:04:48,420 --> 00:04:52,100
the bright sky causing this collision.

127
00:04:52,100 --> 00:04:54,270
The incident resulted in the driver's death,

128
00:04:54,270 --> 00:04:57,480
the first fatality from a self-driving vehicle.

129
00:04:57,480 --> 00:04:58,560
The driver was not controlling

130
00:04:58,560 --> 00:05:00,390
the vehicle when the accident occurred so it might

131
00:05:00,390 --> 00:05:01,710
seem logical to assume that

132
00:05:01,710 --> 00:05:04,230
the autopilot system was at fault.

133
00:05:04,230 --> 00:05:06,378
Again, who is at fault here? Who's liable?

134
00:05:06,378 --> 00:05:08,900
The system itself or the driver?

135
00:05:08,900 --> 00:05:11,070
But interestingly, an investigation by

136
00:05:11,070 --> 00:05:12,420
the National Highway Traffic

137
00:05:12,420 --> 00:05:14,340
Safety Administration found that

138
00:05:14,340 --> 00:05:18,295
there were no defects in the Model S autopilot system.

139
00:05:18,295 --> 00:05:19,980
An investigator noted that

140
00:05:19,980 --> 00:05:21,180
some situations are beyond

141
00:05:21,180 --> 00:05:23,520
the capabilities of the autopilot system.

142
00:05:23,520 --> 00:05:25,290
So autopilot actually requires

143
00:05:25,290 --> 00:05:28,435
full driver engagement at all times.

144
00:05:28,435 --> 00:05:30,060
The investigation also found that

145
00:05:30,060 --> 00:05:31,830
the driver might not have been

146
00:05:31,830 --> 00:05:33,998
paying attention to the road.

147
00:05:33,998 --> 00:05:35,850
Tesla still may have some responsibility for

148
00:05:35,850 --> 00:05:39,090
this accident even though the technology did not fail.

149
00:05:39,090 --> 00:05:41,700
It's unknown whether Tesla properly informed the owner

150
00:05:41,700 --> 00:05:44,460
of the limits of the autopilot features.

151
00:05:44,460 --> 00:05:45,630
This is what I was referencing

152
00:05:45,630 --> 00:05:47,190
earlier in this video when I was talking

153
00:05:47,190 --> 00:05:51,065
about defective instruction or negligent instruction.

154
00:05:51,065 --> 00:05:53,130
It's an aspect of product liability law that

155
00:05:53,130 --> 00:05:55,380
requires manufacturers to inform

156
00:05:55,380 --> 00:05:57,690
us and instruct us on how to

157
00:05:57,690 --> 00:06:00,840
properly use whatever the consumer product is,

158
00:06:00,840 --> 00:06:02,760
in this case the vehicle.

159
00:06:02,760 --> 00:06:06,405
If there is no proper instruction including instruction

160
00:06:06,405 --> 00:06:11,055
on what might not work or what the possible risks are,

161
00:06:11,055 --> 00:06:12,510
then the manufacturer can be

162
00:06:12,510 --> 00:06:15,500
responsible under products liability law.

