0
00:00:00,000 --> 00:00:02,975
Unless we are foolish in the extreme,

1
00:00:02,975 --> 00:00:04,850
there are two coming realities that we

2
00:00:04,850 --> 00:00:07,090
as a human species must prepare for.

3
00:00:07,090 --> 00:00:09,740
First of all that artificial intelligence will eventually

4
00:00:09,740 --> 00:00:12,785
exceed human intelligence on just about every metric.

5
00:00:12,785 --> 00:00:14,690
And second, that it will eventually not

6
00:00:14,690 --> 00:00:17,425
be fully controllable by human beings.

7
00:00:17,425 --> 00:00:18,800
If you think about it, we're actually in

8
00:00:18,800 --> 00:00:20,445
a pretty exciting place.

9
00:00:20,445 --> 00:00:21,710
We know this is coming and we

10
00:00:21,710 --> 00:00:23,180
have a small window of time,

11
00:00:23,180 --> 00:00:24,680
however small, to think

12
00:00:24,680 --> 00:00:27,160
about how to make this happen properly.

13
00:00:27,160 --> 00:00:28,475
The apes on the other hand

14
00:00:28,475 --> 00:00:30,175
had no idea humans were coming.

15
00:00:30,175 --> 00:00:31,520
And I bet they wish they could have done

16
00:00:31,520 --> 00:00:33,425
some planning if they had to do it again.

17
00:00:33,425 --> 00:00:34,700
We humans have been just

18
00:00:34,700 --> 00:00:36,450
terrible at sharing the world with them.

19
00:00:36,450 --> 00:00:39,250
We kind of just took over the place once we got here.

20
00:00:39,250 --> 00:00:40,640
So how do we avoid the fate

21
00:00:40,640 --> 00:00:42,775
of our evolutionary ancestors.

22
00:00:42,775 --> 00:00:44,600
The general consensus is that we want

23
00:00:44,600 --> 00:00:46,340
these super intelligent machines to have

24
00:00:46,340 --> 00:00:49,515
values similar to human values.

25
00:00:49,515 --> 00:00:51,770
Now you're probably already thinking, "Hey,

26
00:00:51,770 --> 00:00:53,150
there's a lot of evil in the world,

27
00:00:53,150 --> 00:00:55,805
so maybe humans aren't such a great model."

28
00:00:55,805 --> 00:00:57,050
And yes, there are terrorists and

29
00:00:57,050 --> 00:00:59,015
other kinds of evil doers out there,

30
00:00:59,015 --> 00:01:00,710
but if you think about numbers,

31
00:01:00,710 --> 00:01:02,480
there are seven billion of us and

32
00:01:02,480 --> 00:01:04,490
almost all of those seven billion just want

33
00:01:04,490 --> 00:01:06,200
to achieve our goals peacefully

34
00:01:06,200 --> 00:01:08,855
without doing serious harm to other people.

35
00:01:08,855 --> 00:01:11,060
And so if these future super intelligent beings

36
00:01:11,060 --> 00:01:12,575
think in those basic terms,

37
00:01:12,575 --> 00:01:14,760
that's actually a pretty decent future.

38
00:01:14,760 --> 00:01:16,550
The extreme opposite possibility

39
00:01:16,550 --> 00:01:18,550
is that we'll program some machine to solve

40
00:01:18,550 --> 00:01:21,710
a problem and it will run some calculation and determine

41
00:01:21,710 --> 00:01:22,790
that the best way to solve

42
00:01:22,790 --> 00:01:25,290
that problem is eliminating the human race.

43
00:01:25,290 --> 00:01:27,080
In the academic community we call

44
00:01:27,080 --> 00:01:29,540
that a sub optimal outcome.

45
00:01:29,540 --> 00:01:33,245
So, how can we design the kind of future we want?

46
00:01:33,245 --> 00:01:34,850
Nick Bostrom, director of

47
00:01:34,850 --> 00:01:36,770
The Future of Humanity Institute at

48
00:01:36,770 --> 00:01:39,200
Oxford University has established

49
00:01:39,200 --> 00:01:41,720
a framework that seems basically right.

50
00:01:41,720 --> 00:01:43,490
He notes that designing super intelligence

51
00:01:43,490 --> 00:01:45,530
is obviously extremely difficult,

52
00:01:45,530 --> 00:01:47,420
but designing super intelligence that

53
00:01:47,420 --> 00:01:49,640
shares our values necessarily

54
00:01:49,640 --> 00:01:50,870
combines the difficulty of

55
00:01:50,870 --> 00:01:54,330
the first task and then adds a layer of complexity.

56
00:01:54,330 --> 00:01:56,240
And the specific danger is that we'll

57
00:01:56,240 --> 00:01:58,440
figure out how to design super intelligence.

58
00:01:58,440 --> 00:02:01,415
But before we can figure out how to program with values,

59
00:02:01,415 --> 00:02:04,405
we get that sub optimal outcome I described.

60
00:02:04,405 --> 00:02:08,505
Bostrom calls this the problem of value loading.

61
00:02:08,505 --> 00:02:10,880
It turns out that my full time job for

62
00:02:10,880 --> 00:02:13,170
over a decade now has been value loading.

63
00:02:13,170 --> 00:02:15,710
And like most people who have been working at something,

64
00:02:15,710 --> 00:02:17,390
I've had many successes and

65
00:02:17,390 --> 00:02:19,400
the chance to learn from many failures,

66
00:02:19,400 --> 00:02:23,065
and so I have a few opinions on best practices here.

67
00:02:23,065 --> 00:02:25,850
Now, I should probably mention as a disclaimer that I am

68
00:02:25,850 --> 00:02:28,970
paid to load value not into AI systems,

69
00:02:28,970 --> 00:02:31,760
but into organic college student intellects

70
00:02:31,760 --> 00:02:34,550
because I teach applied ethics at CL University.

71
00:02:34,550 --> 00:02:35,990
But I have a feeling that there are

72
00:02:35,990 --> 00:02:38,265
some lessons that can be transferred.

73
00:02:38,265 --> 00:02:40,985
Here are what seemed to be the two most basic.

74
00:02:40,985 --> 00:02:43,880
First, value loading must be something

75
00:02:43,880 --> 00:02:47,360
general not directed toward specific situations.

76
00:02:47,360 --> 00:02:48,650
It would probably make me more

77
00:02:48,650 --> 00:02:50,690
likely to be successful in my job as

78
00:02:50,690 --> 00:02:52,250
a professor if I could train

79
00:02:52,250 --> 00:02:55,155
my students with a list of 'if then statements.'

80
00:02:55,155 --> 00:02:56,975
If someone says or does X,

81
00:02:56,975 --> 00:02:59,755
then the ethical thing to do is Y.

82
00:02:59,755 --> 00:03:02,090
But situations are like snowflakes,

83
00:03:02,090 --> 00:03:04,280
there are no two exactly the same.

84
00:03:04,280 --> 00:03:06,590
So however value loading works,

85
00:03:06,590 --> 00:03:08,480
it has to be a moral capacity

86
00:03:08,480 --> 00:03:11,380
available for every contingency.

87
00:03:11,380 --> 00:03:13,490
Second, value loading in humans

88
00:03:13,490 --> 00:03:15,990
is only possible unless there is first,

89
00:03:15,990 --> 00:03:19,435
a basic concern with human well-being in general.

90
00:03:19,435 --> 00:03:20,900
We know this from module one

91
00:03:20,900 --> 00:03:22,565
when I talked about ethics in general.

92
00:03:22,565 --> 00:03:24,980
And if you remember, I was careful to distinguish

93
00:03:24,980 --> 00:03:26,660
this basic attitude from

94
00:03:26,660 --> 00:03:29,610
the five specific ethical values.

95
00:03:29,610 --> 00:03:32,120
On the first day of my college courses,

96
00:03:32,120 --> 00:03:33,800
I asked my students a question,

97
00:03:33,800 --> 00:03:36,140
"Can ethics be taught?"

98
00:03:36,140 --> 00:03:38,030
I give some time for the awkward silence

99
00:03:38,030 --> 00:03:39,335
to wash over people.

100
00:03:39,335 --> 00:03:40,670
As one by one they start

101
00:03:40,670 --> 00:03:42,695
realizing that if the answer is no,

102
00:03:42,695 --> 00:03:45,020
we are all wasting our time.

103
00:03:45,020 --> 00:03:46,955
The truth is that it depends.

104
00:03:46,955 --> 00:03:49,220
Specifically It depends on whether the student

105
00:03:49,220 --> 00:03:52,325
cares about human well-being in the first place.

106
00:03:52,325 --> 00:03:53,885
Because if they do,

107
00:03:53,885 --> 00:03:56,675
then talking about different values is very helpful.

108
00:03:56,675 --> 00:03:58,370
But if they don't, then yeah,

109
00:03:58,370 --> 00:04:01,135
talking about ethics is a waste of time.

110
00:04:01,135 --> 00:04:02,540
I wonder if the same thing will be

111
00:04:02,540 --> 00:04:04,060
true of super intelligence.

112
00:04:04,060 --> 00:04:05,775
We can attempt value loading,

113
00:04:05,775 --> 00:04:07,070
but if the basic concern with

114
00:04:07,070 --> 00:04:08,615
human well-being isn't there,

115
00:04:08,615 --> 00:04:10,520
the values won't stick.

116
00:04:10,520 --> 00:04:12,440
The problem of creating super intelligence with

117
00:04:12,440 --> 00:04:14,720
values like ours is a fascinating problem.

118
00:04:14,720 --> 00:04:17,060
And one that may turn out to influence the future of

119
00:04:17,060 --> 00:04:21,000
humanity more than any other problem we must confront.

