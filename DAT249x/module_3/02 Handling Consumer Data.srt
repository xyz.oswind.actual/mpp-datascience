0
00:00:00,110 --> 00:00:03,000
>> In this area, we are talking about

1
00:00:03,000 --> 00:00:05,310
customer analytics and the legal protections

2
00:00:05,310 --> 00:00:06,975
for customers in the US,

3
00:00:06,975 --> 00:00:08,820
mostly with respect to privacy and

4
00:00:08,820 --> 00:00:10,740
security of data about them

5
00:00:10,740 --> 00:00:12,180
and how that data is used

6
00:00:12,180 --> 00:00:15,190
appropriately or not in ways that affect their lives.

7
00:00:15,190 --> 00:00:16,920
In this video, I'm going to introduce

8
00:00:16,920 --> 00:00:18,360
the Federal Trade Commission's role

9
00:00:18,360 --> 00:00:19,890
in this area and highlight

10
00:00:19,890 --> 00:00:21,570
two laws that have been used to protect

11
00:00:21,570 --> 00:00:25,950
consumers and also provide you with two case examples.

12
00:00:25,950 --> 00:00:27,210
The Federal Trade Commission is

13
00:00:27,210 --> 00:00:28,710
the federal agency charged with

14
00:00:28,710 --> 00:00:32,615
protecting consumers under the Fair Credit Reporting Act.

15
00:00:32,615 --> 00:00:34,140
The Fair Credit Reporting Act

16
00:00:34,140 --> 00:00:35,595
applies to companies known as

17
00:00:35,595 --> 00:00:37,950
Consumer Reporting Agencies or

18
00:00:37,950 --> 00:00:41,145
CRAs that compile and sell consumer reports,

19
00:00:41,145 --> 00:00:42,870
which contain consumer information

20
00:00:42,870 --> 00:00:44,055
that is used for credit,

21
00:00:44,055 --> 00:00:45,420
employment, insurance,

22
00:00:45,420 --> 00:00:47,430
housing, and other similar decisions.

23
00:00:47,430 --> 00:00:50,160
CRAs must implement reasonable procedures

24
00:00:50,160 --> 00:00:52,410
to insure a maximum possible accuracy of

25
00:00:52,410 --> 00:00:55,470
consumer reports and provide consumers with access

26
00:00:55,470 --> 00:00:59,460
to their information and the ability to correct errors.

27
00:00:59,460 --> 00:01:00,660
This is an area concerning

28
00:01:00,660 --> 00:01:03,090
the relationship between law and technology where

29
00:01:03,090 --> 00:01:04,830
we see the law being stretched to

30
00:01:04,830 --> 00:01:07,300
apply to technological advances.

31
00:01:07,300 --> 00:01:08,565
Data brokers might not

32
00:01:08,565 --> 00:01:10,350
otherwise consider themselves subject to

33
00:01:10,350 --> 00:01:12,090
this or any other law that

34
00:01:12,090 --> 00:01:14,085
protect someone else's customers,

35
00:01:14,085 --> 00:01:17,190
but based on the enforcement actions taken by the FTC,

36
00:01:17,190 --> 00:01:19,385
they are now on notice.

37
00:01:19,385 --> 00:01:21,600
For example, the FTC entered

38
00:01:21,600 --> 00:01:27,000
a consent decree with the online data brokers, Spokeo.

39
00:01:27,000 --> 00:01:29,100
Spokeo assembles personal information from hundreds of

40
00:01:29,100 --> 00:01:31,770
online and offline data sources including

41
00:01:31,770 --> 00:01:34,350
social networks and merge that data to

42
00:01:34,350 --> 00:01:37,380
create detailed personal profiles including names,

43
00:01:37,380 --> 00:01:41,100
addresses, age, hobbies, ethnicities, religion,

44
00:01:41,100 --> 00:01:43,860
and mark these digital biographies for use by

45
00:01:43,860 --> 00:01:47,070
human resource departments in making hiring decisions.

46
00:01:47,070 --> 00:01:48,960
According to the FTC,

47
00:01:48,960 --> 00:01:52,800
Spokeo's actions made them a CRA and as such,

48
00:01:52,800 --> 00:01:54,470
subject to the rules of the FCRA,

49
00:01:54,470 --> 00:01:57,125
which they were not following.

50
00:01:57,125 --> 00:02:00,385
They paid $800,000 in penalties.

51
00:02:00,385 --> 00:02:02,520
Note, too, that Spokeo had tried to protect

52
00:02:02,520 --> 00:02:06,235
itself from the risk of having to comply with the FCRA.

53
00:02:06,235 --> 00:02:08,145
It included a disclaimer on its site,

54
00:02:08,145 --> 00:02:10,980
which claimed it was not a CRA and that users should

55
00:02:10,980 --> 00:02:12,255
not use their site for credit

56
00:02:12,255 --> 00:02:14,565
eligibility purposes per se.

57
00:02:14,565 --> 00:02:16,500
This disclaimer was not effective to

58
00:02:16,500 --> 00:02:20,055
protect the company from FTC enforcement action.

59
00:02:20,055 --> 00:02:22,020
This is a good example of what is

60
00:02:22,020 --> 00:02:23,250
a central objective of

61
00:02:23,250 --> 00:02:25,470
this course with respect to regulatory law,

62
00:02:25,470 --> 00:02:27,210
that is organizations and

63
00:02:27,210 --> 00:02:29,060
participants in data analytics and

64
00:02:29,060 --> 00:02:30,840
AI should not assume that

65
00:02:30,840 --> 00:02:33,175
existing law does not regulate them.

66
00:02:33,175 --> 00:02:35,970
The FCRA predates this new technology

67
00:02:35,970 --> 00:02:38,070
but as this case example shows us,

68
00:02:38,070 --> 00:02:39,480
federal authorities can apply

69
00:02:39,480 --> 00:02:43,080
existing law to new ways of conducting business.

70
00:02:43,080 --> 00:02:47,325
Data privacy and security are top of mind for the FTC.

71
00:02:47,325 --> 00:02:50,580
In addition to enforcement authority for the FCRA,

72
00:02:50,580 --> 00:02:52,320
the FTC also uses

73
00:02:52,320 --> 00:02:55,470
Section 5 of the FTC act to take

74
00:02:55,470 --> 00:02:57,270
enforcement actions for violations

75
00:02:57,270 --> 00:02:59,610
of consumer privacy rights.

76
00:02:59,610 --> 00:03:01,830
Section 5 is generally applicable to

77
00:03:01,830 --> 00:03:03,120
most companies acting in

78
00:03:03,120 --> 00:03:05,935
commerce regardless of market sector.

79
00:03:05,935 --> 00:03:08,745
Section 5 of the FTC Act states that,

80
00:03:08,745 --> 00:03:10,500
"unfair or deceptive acts or

81
00:03:10,500 --> 00:03:13,830
practices in or affecting commerce are unlawful."

82
00:03:13,830 --> 00:03:15,690
Unfair and deceptive practices can

83
00:03:15,690 --> 00:03:17,280
arise when companies are maintaining

84
00:03:17,280 --> 00:03:19,095
large amounts of sensitive data about

85
00:03:19,095 --> 00:03:22,440
individuals and are not securing it from misuse.

86
00:03:22,440 --> 00:03:24,555
The more sensitive and complex the data,

87
00:03:24,555 --> 00:03:26,895
the more security should be in place.

88
00:03:26,895 --> 00:03:28,710
Companies can be held liable for failure to

89
00:03:28,710 --> 00:03:30,960
secure or for sharing or selling

90
00:03:30,960 --> 00:03:33,090
data analytics products to customers who

91
00:03:33,090 --> 00:03:35,685
may use that data for fraudulent purposes.

92
00:03:35,685 --> 00:03:37,560
One recent case in this area relates to

93
00:03:37,560 --> 00:03:39,810
the Internet of Things that all

94
00:03:39,810 --> 00:03:42,450
encompassing network of internet communication

95
00:03:42,450 --> 00:03:44,955
connecting everyday consumer devices.

96
00:03:44,955 --> 00:03:47,100
Trendnet provides cameras for

97
00:03:47,100 --> 00:03:49,950
consumers to conduct safety monitoring in

98
00:03:49,950 --> 00:03:52,200
their homes and allows consumers to access

99
00:03:52,200 --> 00:03:53,520
live video and audio

100
00:03:53,520 --> 00:03:56,080
feeds from their cameras over the Internet.

101
00:03:56,080 --> 00:03:57,630
The Trendnet action is brought by

102
00:03:57,630 --> 00:04:00,150
the FTC because Trendnet misrepresented

103
00:04:00,150 --> 00:04:03,180
its security measures to consumers and failed to use

104
00:04:03,180 --> 00:04:04,530
reasonable security measures to

105
00:04:04,530 --> 00:04:06,755
prevent unauthorized access.

106
00:04:06,755 --> 00:04:08,940
As a result, hackers were able to compromise

107
00:04:08,940 --> 00:04:11,070
the live feeds and gain access to

108
00:04:11,070 --> 00:04:14,580
surveillance inside customer homes including viewing

109
00:04:14,580 --> 00:04:16,470
children sleeping and playing and

110
00:04:16,470 --> 00:04:18,930
adults engaging in daily activities.

111
00:04:18,930 --> 00:04:20,370
This also allowed access to

112
00:04:20,370 --> 00:04:23,190
information inside the home and the potential for

113
00:04:23,190 --> 00:04:25,665
larger threats like robberies

114
00:04:25,665 --> 00:04:27,090
and other kinds of things that could

115
00:04:27,090 --> 00:04:29,275
happen with that information.

116
00:04:29,275 --> 00:04:31,710
The FTC has not issued former rules for

117
00:04:31,710 --> 00:04:33,240
the new data security issues that are

118
00:04:33,240 --> 00:04:35,055
being created by data analytics,

119
00:04:35,055 --> 00:04:38,130
the Internet of Things, and AI but rather uses

120
00:04:38,130 --> 00:04:39,420
existing law to enforce

121
00:04:39,420 --> 00:04:42,480
its authority through complaints and consent orders.

122
00:04:42,480 --> 00:04:44,400
A great resource for organizations

123
00:04:44,400 --> 00:04:46,340
concerned with legal compliance is

124
00:04:46,340 --> 00:04:48,960
a 2016 FTC report

125
00:04:48,960 --> 00:04:50,460
suggested as additional reading

126
00:04:50,460 --> 00:04:52,270
for students in this course.

127
00:04:52,270 --> 00:04:54,480
The report is called Big Data- A

128
00:04:54,480 --> 00:04:57,360
Tool for Inclusion or Exclusion.

129
00:04:57,360 --> 00:04:58,980
Take a look at the list of questions on

130
00:04:58,980 --> 00:05:00,865
page 24 of that report,

131
00:05:00,865 --> 00:05:02,573
which will help you begin to road

132
00:05:02,573 --> 00:05:03,790
map legal compliance for

133
00:05:03,790 --> 00:05:06,235
customer concerns in the United States.

134
00:05:06,235 --> 00:05:08,260
What should also be on the radar of

135
00:05:08,260 --> 00:05:11,050
organizations and participants is forthcoming law that

136
00:05:11,050 --> 00:05:13,270
might be passed to address regulatory concerns in

137
00:05:13,270 --> 00:05:18,090
the US government in situations where no law yet exists.

