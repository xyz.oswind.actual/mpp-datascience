0
00:00:00,000 --> 00:00:02,950
The relationship between privacy law and

1
00:00:02,950 --> 00:00:06,123
technology has been evolving since the 1970s.

2
00:00:06,123 --> 00:00:07,990
In both the U.S. and Europe,

3
00:00:07,990 --> 00:00:10,060
the first generation of modern privacy

4
00:00:10,060 --> 00:00:12,190
and data protection laws grew out

5
00:00:12,190 --> 00:00:13,810
of the increased use of computers for

6
00:00:13,810 --> 00:00:16,940
information processing by governments and businesses.

7
00:00:16,940 --> 00:00:20,620
Harvard law professor Urs Gasser recently published

8
00:00:20,620 --> 00:00:22,570
an article which traces the history and

9
00:00:22,570 --> 00:00:25,995
opines about the future direction of this relationship.

10
00:00:25,995 --> 00:00:28,210
The laws of the 1970s were anchored in

11
00:00:28,210 --> 00:00:30,760
a set of fair information practices which are

12
00:00:30,760 --> 00:00:33,370
still arguably universally accepted as

13
00:00:33,370 --> 00:00:34,540
foundation principles to

14
00:00:34,540 --> 00:00:36,905
protect individual privacy rights.

15
00:00:36,905 --> 00:00:39,805
These five principles concern access to stored records,

16
00:00:39,805 --> 00:00:41,470
transparency of process,

17
00:00:41,470 --> 00:00:44,770
control options for individuals, and security.

18
00:00:44,770 --> 00:00:47,350
We see these original principles incorporated later

19
00:00:47,350 --> 00:00:49,930
in the 1990s with the widespread adoption of

20
00:00:49,930 --> 00:00:52,840
Internet technology and the wave of legislation and

21
00:00:52,840 --> 00:00:56,245
regulations aimed at dealing with privacy concerns.

22
00:00:56,245 --> 00:00:57,880
And even more recently in

23
00:00:57,880 --> 00:01:00,070
more ambitious privacy codes and laws

24
00:01:00,070 --> 00:01:02,650
such as the OECD's privacy guidelines

25
00:01:02,650 --> 00:01:04,390
at the international level,

26
00:01:04,390 --> 00:01:06,280
the General Data Protection Regulation

27
00:01:06,280 --> 00:01:07,675
in the European Union,

28
00:01:07,675 --> 00:01:08,980
and the proposed Consumer

29
00:01:08,980 --> 00:01:11,180
Privacy Bill of Rights in the US.

30
00:01:11,180 --> 00:01:14,590
Today we are experiencing even more rapidly advancing

31
00:01:14,590 --> 00:01:16,450
technologies and applications such as

32
00:01:16,450 --> 00:01:18,820
cloud computing, analytics and AI.

33
00:01:18,820 --> 00:01:20,980
And as we know there is just no way for

34
00:01:20,980 --> 00:01:23,975
law to keep the pace of development with technology.

35
00:01:23,975 --> 00:01:25,750
The law technology tension theme is

36
00:01:25,750 --> 00:01:28,390
present in privacy with a capital P. But

37
00:01:28,390 --> 00:01:30,880
Professor Gasser has an intriguing proposal that

38
00:01:30,880 --> 00:01:32,710
re-imagines the relationship between

39
00:01:32,710 --> 00:01:34,630
privacy law and technology.

40
00:01:34,630 --> 00:01:38,020
He uses the term privacy by design to describe

41
00:01:38,020 --> 00:01:40,450
an umbrella philosophy that promotes a means to

42
00:01:40,450 --> 00:01:43,955
manage privacy challenges from emerging technologies.

43
00:01:43,955 --> 00:01:47,005
This is a holistic privacy risk assessment model

44
00:01:47,005 --> 00:01:49,150
that relies on modeling approaches from

45
00:01:49,150 --> 00:01:51,730
information security and an understanding of

46
00:01:51,730 --> 00:01:52,990
privacy that is informed by

47
00:01:52,990 --> 00:01:55,150
multiple disciplines including statistics,

48
00:01:55,150 --> 00:01:58,240
computer science, law, and social sciences.

49
00:01:58,240 --> 00:01:59,740
This privacy by design is

50
00:01:59,740 --> 00:02:01,985
a collaboration of law and technology.

51
00:02:01,985 --> 00:02:03,670
I have to agree with Professor Gasser

52
00:02:03,670 --> 00:02:05,170
when he says and I quote,

53
00:02:05,170 --> 00:02:07,150
"traditional legal approaches for

54
00:02:07,150 --> 00:02:09,625
protecting privacy while transferring data,

55
00:02:09,625 --> 00:02:11,575
making data release decisions,

56
00:02:11,575 --> 00:02:13,660
and drafting data sharing agreements,

57
00:02:13,660 --> 00:02:16,405
among other activities are time intensive

58
00:02:16,405 --> 00:02:19,600
and not readily scalable to big data context.

59
00:02:19,600 --> 00:02:21,880
Technological approaches can be designed with

60
00:02:21,880 --> 00:02:24,190
compliance with legal standards and practices

61
00:02:24,190 --> 00:02:27,475
in mind in order to help automate data sharing decisions

62
00:02:27,475 --> 00:02:29,590
and ensure consistent privacy protections

63
00:02:29,590 --> 00:02:31,725
on a massive scale."

64
00:02:31,725 --> 00:02:34,140
We have seen similar proposals for

65
00:02:34,140 --> 00:02:36,510
design solutions to legal and ethical issues

66
00:02:36,510 --> 00:02:38,180
in this course already.

67
00:02:38,180 --> 00:02:40,382
Privacy by design is a way for law and

68
00:02:40,382 --> 00:02:43,470
technology to have a mutually productive relationship.

69
00:02:43,470 --> 00:02:45,360
As Professor Gasser states and again

70
00:02:45,360 --> 00:02:47,370
I quote, "taken together,

71
00:02:47,370 --> 00:02:49,500
the development of privacy tools that aim to

72
00:02:49,500 --> 00:02:52,200
integrate legal and technical approaches could help pave

73
00:02:52,200 --> 00:02:54,930
the way for a more strategic and systematic way

74
00:02:54,930 --> 00:02:56,820
to conceptualize and orchestrate

75
00:02:56,820 --> 00:02:58,650
the contemporary interplay between law and

76
00:02:58,650 --> 00:03:02,530
technology in the field of information privacy."

