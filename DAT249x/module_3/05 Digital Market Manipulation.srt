0
00:00:00,050 --> 00:00:02,610
>> We assume that we, more or less,

1
00:00:02,610 --> 00:00:04,500
understand ourselves and our reasons

2
00:00:04,500 --> 00:00:06,170
for the decisions we make.

3
00:00:06,170 --> 00:00:09,240
So when it comes to choosing to buy or not buy something,

4
00:00:09,240 --> 00:00:10,950
we assume that we are doing so for

5
00:00:10,950 --> 00:00:13,795
reasons that we ourselves understand.

6
00:00:13,795 --> 00:00:16,350
It turns out, however, that people in general are

7
00:00:16,350 --> 00:00:19,069
strikingly irrational in surprising ways,

8
00:00:19,069 --> 00:00:21,108
especially when it comes to money.

9
00:00:21,108 --> 00:00:23,685
In a series of books and talks, Dan Ariely,

10
00:00:23,685 --> 00:00:25,080
professor of Psychology and

11
00:00:25,080 --> 00:00:27,165
Behavioral Science at Duke University,

12
00:00:27,165 --> 00:00:30,840
argues that we do not know our own preferences very well.

13
00:00:30,840 --> 00:00:32,670
One unsettling thing about this is

14
00:00:32,670 --> 00:00:34,590
that if we suppose that there are people

15
00:00:34,590 --> 00:00:36,180
out there who are able to influence

16
00:00:36,180 --> 00:00:39,054
our preferences without our awareness of it,

17
00:00:39,054 --> 00:00:42,335
they can actually hack our decision making process.

18
00:00:42,335 --> 00:00:44,220
We think we are in charge of our decisions,

19
00:00:44,220 --> 00:00:46,820
but that might be less true than we think.

20
00:00:46,820 --> 00:00:48,830
This fact of the human condition is

21
00:00:48,830 --> 00:00:51,178
sometimes used for social benefit.

22
00:00:51,178 --> 00:00:53,840
For example, behavioral economists advise

23
00:00:53,840 --> 00:00:55,190
governments on how to get people to

24
00:00:55,190 --> 00:00:56,950
save more for retirement,

25
00:00:56,950 --> 00:00:58,970
or to volunteer to be organ donors,

26
00:00:58,970 --> 00:01:02,305
or to be more honest on financial disclosure forms.

27
00:01:02,305 --> 00:01:04,370
Likewise, for-profit businesses can

28
00:01:04,370 --> 00:01:06,935
use these psychological insights for good,

29
00:01:06,935 --> 00:01:09,770
or at least in ways that are good for their investors

30
00:01:09,770 --> 00:01:13,240
while not being overly harmful for their consumers.

31
00:01:13,240 --> 00:01:16,310
Most of us realize that things cost $4.99

32
00:01:16,310 --> 00:01:17,630
because the brain interprets

33
00:01:17,630 --> 00:01:20,960
$4.99 as closer to $4 than to $5,

34
00:01:20,960 --> 00:01:22,640
and we all suspect that

35
00:01:22,640 --> 00:01:25,490
the outrageously expensive TV is probably just put

36
00:01:25,490 --> 00:01:27,325
on the shelf to make the TV we're

37
00:01:27,325 --> 00:01:30,680
actually interested in seem like a good deal.

38
00:01:30,680 --> 00:01:31,910
Businesses have always used

39
00:01:31,910 --> 00:01:33,410
little tricks like these and most

40
00:01:33,410 --> 00:01:36,845
of us aren't particularly offended by such practices.

41
00:01:36,845 --> 00:01:39,320
But the history of marketing is littered with

42
00:01:39,320 --> 00:01:42,180
practices that we have to call manipulative.

43
00:01:42,180 --> 00:01:44,360
One recent marketing firm noticed that

44
00:01:44,360 --> 00:01:47,165
women feel the least attractive on Monday morning,

45
00:01:47,165 --> 00:01:49,700
and so recommended that makeup companies spend most of

46
00:01:49,700 --> 00:01:50,900
their advertising dollars at

47
00:01:50,900 --> 00:01:53,470
that exact moment of vulnerability.

48
00:01:53,470 --> 00:01:55,780
Many people are, if not outright offended,

49
00:01:55,780 --> 00:01:58,040
at least a little bit uncomfortable when they hear about

50
00:01:58,040 --> 00:02:01,615
marketing techniques that exploit our vulnerabilities.

51
00:02:01,615 --> 00:02:03,470
Offensive as it may be, however,

52
00:02:03,470 --> 00:02:05,120
when marketing departments were limited

53
00:02:05,120 --> 00:02:07,156
to TV and radio ads,

54
00:02:07,156 --> 00:02:08,945
direct mail, billboards,

55
00:02:08,945 --> 00:02:13,010
their powers of manipulation were very limited.

56
00:02:13,010 --> 00:02:14,138
But Ryan Calo, in

57
00:02:14,138 --> 00:02:17,690
his 2014 article "Digital Market Manipulation",

58
00:02:17,690 --> 00:02:19,520
explains how big data makes

59
00:02:19,520 --> 00:02:21,350
manipulation strategies such as

60
00:02:21,350 --> 00:02:22,990
selling makeup on Mondays,

61
00:02:22,990 --> 00:02:25,080
much much more serious.

62
00:02:25,080 --> 00:02:27,095
One reason he gives is because big data

63
00:02:27,095 --> 00:02:29,690
allows businesses to combine the two ingredients

64
00:02:29,690 --> 00:02:31,498
for traditional marketing,

65
00:02:31,498 --> 00:02:34,440
personalization, and systemization.

66
00:02:34,440 --> 00:02:37,790
An example of personalization occurs when a car salesman

67
00:02:37,790 --> 00:02:39,080
notices the signs of

68
00:02:39,080 --> 00:02:41,435
a middle-age crisis in one of his customers,

69
00:02:41,435 --> 00:02:43,940
and uses smooth language to get him to buy

70
00:02:43,940 --> 00:02:45,260
a wasteful car that makes him

71
00:02:45,260 --> 00:02:47,640
feel more youthful in that moment.

72
00:02:47,640 --> 00:02:49,190
This is personalization because

73
00:02:49,190 --> 00:02:50,690
this particular selling strategy

74
00:02:50,690 --> 00:02:52,750
would not work on most people.

75
00:02:52,750 --> 00:02:54,240
Systemization describes

76
00:02:54,240 --> 00:02:55,970
a general marketing strategy such as

77
00:02:55,970 --> 00:02:57,530
a car commercial on TV that is

78
00:02:57,530 --> 00:03:00,770
intended to provoke a feeling of youthfulness.

79
00:03:00,770 --> 00:03:02,570
But in the age of Big Data,

80
00:03:02,570 --> 00:03:04,460
systemization can be combined with

81
00:03:04,460 --> 00:03:07,185
personalization like it never has been,

82
00:03:07,185 --> 00:03:08,900
and this is because of big data and

83
00:03:08,900 --> 00:03:11,120
the algorithms that process them.

84
00:03:11,120 --> 00:03:14,180
Most of us have experienced this when an online seller

85
00:03:14,180 --> 00:03:15,650
uses an algorithm to give us

86
00:03:15,650 --> 00:03:17,645
a list of books we might be interested in,

87
00:03:17,645 --> 00:03:20,870
based on what book we have already looked at.

88
00:03:20,870 --> 00:03:22,670
This may lead to a win win.

89
00:03:22,670 --> 00:03:26,120
The business algorithm helps me discover books I like,

90
00:03:26,120 --> 00:03:28,185
all the while while they make money.

91
00:03:28,185 --> 00:03:30,200
But what if these algorithms can track

92
00:03:30,200 --> 00:03:33,335
your digital trail more widely, and for example,

93
00:03:33,335 --> 00:03:35,850
use patterns of your likes on social media,

94
00:03:35,850 --> 00:03:37,730
to know when you have had a hard day,

95
00:03:37,730 --> 00:03:40,130
and market a harmful tempting product to you

96
00:03:40,130 --> 00:03:42,553
at that exact moment of vulnerability?

97
00:03:42,553 --> 00:03:43,580
This is why it's more

98
00:03:43,580 --> 00:03:45,170
important than ever that we think about

99
00:03:45,170 --> 00:03:46,725
marketing in the digital age

100
00:03:46,725 --> 00:03:49,670
from a legal and ethical perspective.

