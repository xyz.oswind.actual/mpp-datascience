0
00:00:03,550 --> 00:00:07,540
In this presentation, we're going to be talking about

1
00:00:07,540 --> 00:00:10,660
the unique relationship between the GDPR and

2
00:00:10,660 --> 00:00:12,265
its requirements and

3
00:00:12,265 --> 00:00:14,980
data analytics and artificial intelligence.

4
00:00:14,980 --> 00:00:17,280
Because of the nature of the technology,

5
00:00:17,280 --> 00:00:19,870
there are nine data protection principles

6
00:00:19,870 --> 00:00:23,610
that are threatened by its use.

7
00:00:23,610 --> 00:00:27,305
The first one, big data analytics must be fair.

8
00:00:27,305 --> 00:00:30,745
The GDPR requires fairness.

9
00:00:30,745 --> 00:00:32,895
We know that there can be instances

10
00:00:32,895 --> 00:00:35,500
in analytics and in artificial intelligence,

11
00:00:35,500 --> 00:00:38,260
like predictive analytics, that there can be some bias or

12
00:00:38,260 --> 00:00:39,910
discrimination that we're working to

13
00:00:39,910 --> 00:00:43,095
eradicate but we acknowledge that they can exist.

14
00:00:43,095 --> 00:00:46,410
Big data analytics are required to be fair in the GDPR.

15
00:00:46,410 --> 00:00:49,860
So this is going to be a tension right out of the gate.

16
00:00:49,860 --> 00:00:51,610
Number two, you need permission to

17
00:00:51,610 --> 00:00:53,855
process data under the GDPR.

18
00:00:53,855 --> 00:00:55,940
And it has to be unambiguous.

19
00:00:55,940 --> 00:00:57,810
So consent is very important.

20
00:00:57,810 --> 00:00:59,594
It has to be clear, affirmative,

21
00:00:59,594 --> 00:01:02,795
it can be taken back at any time.

22
00:01:02,795 --> 00:01:06,100
There are points in the process with analytics and

23
00:01:06,100 --> 00:01:08,680
artificial intelligence where you can't just

24
00:01:08,680 --> 00:01:11,800
continuously be asking for consent.

25
00:01:11,800 --> 00:01:14,500
So business people already

26
00:01:14,500 --> 00:01:17,110
they've got their eyes on this requirement, considering,

27
00:01:17,110 --> 00:01:18,340
how are we going to have a

28
00:01:18,340 --> 00:01:20,905
"just in time" notification process

29
00:01:20,905 --> 00:01:24,420
when there are downstream uses of the data.

30
00:01:24,420 --> 00:01:26,290
Number three, purpose limitation.

31
00:01:26,290 --> 00:01:29,710
The GDPR requires that you collect and use data for

32
00:01:29,710 --> 00:01:32,200
very specific purposes and that you

33
00:01:32,200 --> 00:01:35,760
don't use the data inconsistently with that purpose.

34
00:01:35,760 --> 00:01:37,765
Again, the technology of analytics

35
00:01:37,765 --> 00:01:39,820
and artificial intelligence makes this almost

36
00:01:39,820 --> 00:01:41,740
impossible because it's expected

37
00:01:41,740 --> 00:01:43,540
that the data might be used for

38
00:01:43,540 --> 00:01:48,410
other purposes than what it was originally collected for.

39
00:01:48,410 --> 00:01:51,210
Number four, holding on to data.

40
00:01:51,210 --> 00:01:55,235
Under the GDPR and also the earlier privacy directive,

41
00:01:55,235 --> 00:01:57,545
there is an objective of

42
00:01:57,545 --> 00:02:00,645
keeping the data in a minimalistic space.

43
00:02:00,645 --> 00:02:03,215
Meaning you take what you need,

44
00:02:03,215 --> 00:02:04,846
you only have what you need,

45
00:02:04,846 --> 00:02:06,245
you minimize the data.

46
00:02:06,245 --> 00:02:08,960
Which, again, based on the technology of

47
00:02:08,960 --> 00:02:11,265
artificial intelligence and analytics

48
00:02:11,265 --> 00:02:13,030
we're actually seeking more data,

49
00:02:13,030 --> 00:02:14,510
we're not seeking to minimize data,

50
00:02:14,510 --> 00:02:16,850
we're actually looking for more.

51
00:02:16,850 --> 00:02:19,825
Number five is accuracy.

52
00:02:19,825 --> 00:02:21,485
We know that when data sets are large,

53
00:02:21,485 --> 00:02:24,900
inaccurate data might be passed over or dismissed.

54
00:02:24,900 --> 00:02:26,570
And also that big data might not

55
00:02:26,570 --> 00:02:29,010
represent a general population.

56
00:02:29,010 --> 00:02:34,010
We talked about this in mod two when we were introducing

57
00:02:34,010 --> 00:02:36,710
concepts of inclusion and exclusion and

58
00:02:36,710 --> 00:02:40,475
Kate Crawford's work in this area.

59
00:02:40,475 --> 00:02:43,060
Again, not entirely possible to promise

60
00:02:43,060 --> 00:02:45,625
exact accuracy working towards that,

61
00:02:45,625 --> 00:02:48,211
but the GDPR requires it,

62
00:02:48,211 --> 00:02:49,870
next year when it

63
00:02:49,870 --> 00:02:52,350
comes into force, it's going to require it.

64
00:02:52,350 --> 00:02:54,280
This is one of it's central principles.

65
00:02:54,280 --> 00:02:57,645
Number six, individual rights and access to data.

66
00:02:57,645 --> 00:03:01,000
The GDPR requires that individuals are allowed to

67
00:03:01,000 --> 00:03:02,620
access the personal data and actually

68
00:03:02,620 --> 00:03:05,345
correct if it's not correct.

69
00:03:05,345 --> 00:03:07,560
Again, the technology here and analytics,

70
00:03:07,560 --> 00:03:09,855
this is something that is not considered,

71
00:03:09,855 --> 00:03:11,990
or is certainly not a practice,

72
00:03:11,990 --> 00:03:15,030
but we're going to have to make some accommodations here.

73
00:03:15,030 --> 00:03:17,610
Number seven, security measures and risk.

74
00:03:17,610 --> 00:03:19,770
Security is very important in the GDPR.

75
00:03:19,770 --> 00:03:22,080
We know that large data sets

76
00:03:22,080 --> 00:03:24,210
and the nature of big data processing can throw up

77
00:03:24,210 --> 00:03:26,760
specific information security risks that are going to

78
00:03:26,760 --> 00:03:30,720
have to be addressed in the European Union.

79
00:03:30,720 --> 00:03:32,910
Number eight, accountability.

80
00:03:32,910 --> 00:03:35,130
There are provisions promoting

81
00:03:35,130 --> 00:03:37,110
accountability in the GDPR.

82
00:03:37,110 --> 00:03:39,570
And, again, mostly mod two

83
00:03:39,570 --> 00:03:40,860
when we were introducing some of

84
00:03:40,860 --> 00:03:42,755
the problems with algorithmic decisions,

85
00:03:42,755 --> 00:03:47,040
we see that the erroneous algorithmic decisions based

86
00:03:47,040 --> 00:03:49,140
on biased profiling can

87
00:03:49,140 --> 00:03:52,110
create these accountability issues.

88
00:03:52,110 --> 00:03:55,410
And finally number nine, controllers and processes.

89
00:03:55,410 --> 00:03:58,110
The GDPR defines data controllers

90
00:03:58,110 --> 00:04:00,004
differently from data processors,

91
00:04:00,004 --> 00:04:01,710
but it's going to be very important for

92
00:04:01,710 --> 00:04:04,290
anyone in the business of artificial intelligence

93
00:04:04,290 --> 00:04:07,425
and analytics to really get clear on

94
00:04:07,425 --> 00:04:09,210
what definition they fit and

95
00:04:09,210 --> 00:04:13,000
what rules and regulations are going to apply to them.

