0
00:00:00,000 --> 00:00:02,120
In this presentation, we're

1
00:00:02,120 --> 00:00:03,740
going to be highlighting some of

2
00:00:03,740 --> 00:00:07,160
the suggested best practices based on

3
00:00:07,160 --> 00:00:09,440
the FTC's enforcement actions

4
00:00:09,440 --> 00:00:11,825
with respect to the Internet of things.

5
00:00:11,825 --> 00:00:14,060
There are two attorneys from the law firm

6
00:00:14,060 --> 00:00:16,400
of Kelley Drye and Warren, Alysa Hutnik,

7
00:00:16,400 --> 00:00:20,750
who's a partner in the DC offices and Crystal Skelton,

8
00:00:20,750 --> 00:00:23,150
who's an associate in the LA offices,

9
00:00:23,150 --> 00:00:25,770
who have basically put this presentation together.

10
00:00:25,770 --> 00:00:27,530
And we're sharing it for with you as

11
00:00:27,530 --> 00:00:30,485
our students because we just think it's got some really,

12
00:00:30,485 --> 00:00:33,590
really good suggestions for how to manage and navigate

13
00:00:33,590 --> 00:00:35,480
the regulatory actions that we're

14
00:00:35,480 --> 00:00:37,520
seeing coming out of the FTC.

15
00:00:37,520 --> 00:00:38,960
So I'm not going to go through all of

16
00:00:38,960 --> 00:00:41,170
this presentation with you in this video.

17
00:00:41,170 --> 00:00:43,640
You are welcome to go through it yourself.

18
00:00:43,640 --> 00:00:45,575
I'm going to highlight some of the slides,

19
00:00:45,575 --> 00:00:47,105
and speak to those,

20
00:00:47,105 --> 00:00:49,070
and then I'm going to ask you to return to

21
00:00:49,070 --> 00:00:51,480
them later and really take a deeper look.

22
00:00:51,480 --> 00:00:54,200
So first do's and don'ts on

23
00:00:54,200 --> 00:00:57,170
this list is to know your data.

24
00:00:57,170 --> 00:00:58,595
Some of these are very obvious,

25
00:00:58,595 --> 00:01:00,350
but they're really good reminders.

26
00:01:00,350 --> 00:01:02,300
You want to understand the devices

27
00:01:02,300 --> 00:01:04,655
that you are using to collect the data,

28
00:01:04,655 --> 00:01:07,100
how it's being collected and so forth.

29
00:01:07,100 --> 00:01:09,860
The FTC takes an expansive view as to the types

30
00:01:09,860 --> 00:01:12,935
of data it considers personal, and quite frankly,

31
00:01:12,935 --> 00:01:16,910
they are probably just as motivated as

32
00:01:16,910 --> 00:01:19,355
the European Union is in Europe

33
00:01:19,355 --> 00:01:22,420
with respect to protecting consumer privacy.

34
00:01:22,420 --> 00:01:23,840
We don't talk about it as much

35
00:01:23,840 --> 00:01:25,280
because the European Union is getting

36
00:01:25,280 --> 00:01:28,145
a lot of attention for its recent regulation,

37
00:01:28,145 --> 00:01:29,450
and us getting ready for that.

38
00:01:29,450 --> 00:01:31,190
But the FTC, as we can see

39
00:01:31,190 --> 00:01:33,290
from the cases that we've talked about,

40
00:01:33,290 --> 00:01:35,750
is very concerned with security and privacy.

41
00:01:35,750 --> 00:01:37,035
So know the data,

42
00:01:37,035 --> 00:01:39,830
know that they take an expansive approach to it.

43
00:01:39,830 --> 00:01:42,110
Don't avoid a privacy by design approach.

44
00:01:42,110 --> 00:01:43,955
We've talked a lot about this.

45
00:01:43,955 --> 00:01:45,440
Europe's going to require it.

46
00:01:45,440 --> 00:01:47,440
The FTC wants to see it.

47
00:01:47,440 --> 00:01:49,295
This is where technologists,

48
00:01:49,295 --> 00:01:51,800
computer scientists, engineers,

49
00:01:51,800 --> 00:01:56,075
lawyers and ethicists are working together to create

50
00:01:56,075 --> 00:01:58,715
technology that is going to have built

51
00:01:58,715 --> 00:02:02,240
into it consumer privacy protections.

52
00:02:02,240 --> 00:02:03,350
Third one on this list,

53
00:02:03,350 --> 00:02:05,160
do consider whether and how users

54
00:02:05,160 --> 00:02:07,520
should be provided notice and choice.

55
00:02:07,520 --> 00:02:11,225
The clearer, the more user friendly, the better.

56
00:02:11,225 --> 00:02:14,315
And four, do have a privacy policy

57
00:02:14,315 --> 00:02:17,425
covering the Internet of things,

58
00:02:17,425 --> 00:02:19,900
device, and ensure that it's accurate.

59
00:02:19,900 --> 00:02:21,500
We've seen from the case examples in

60
00:02:21,500 --> 00:02:24,140
an earlier video that the Internet of

61
00:02:24,140 --> 00:02:25,850
things can create all kinds

62
00:02:25,850 --> 00:02:28,740
of interesting privacy issues.

63
00:02:28,740 --> 00:02:31,535
And knowing that you have a privacy policy in place,

64
00:02:31,535 --> 00:02:34,715
and that is constantly updated is going to be critical.

65
00:02:34,715 --> 00:02:36,680
We'll go through one more of these slides

66
00:02:36,680 --> 00:02:38,480
in the do's and don'ts for privacy,

67
00:02:38,480 --> 00:02:39,740
and then we're going to fast

68
00:02:39,740 --> 00:02:42,405
forward towards some of the general ones.

69
00:02:42,405 --> 00:02:43,790
Number five, don't forget to

70
00:02:43,790 --> 00:02:45,620
update your privacy practices,

71
00:02:45,620 --> 00:02:47,180
which is what I was just saying earlier.

72
00:02:47,180 --> 00:02:48,435
You're going to have the policy,

73
00:02:48,435 --> 00:02:50,120
make sure you update it to be

74
00:02:50,120 --> 00:02:52,975
consistent and current with what you're doing.

75
00:02:52,975 --> 00:02:56,990
The next few slides talk about security,

76
00:02:56,990 --> 00:02:59,810
and at the end, these two counselors

77
00:02:59,810 --> 00:03:03,615
have included for us some general do's and don'ts.

78
00:03:03,615 --> 00:03:05,765
Number 11 of the general do's and don'ts,

79
00:03:05,765 --> 00:03:07,580
do apply extra scrutiny when it

80
00:03:07,580 --> 00:03:09,410
comes to collecting sensitive data.

81
00:03:09,410 --> 00:03:11,405
We've been talking about the difference between

82
00:03:11,405 --> 00:03:14,015
personal data and sensitive data,

83
00:03:14,015 --> 00:03:15,655
and the sensitive data,

84
00:03:15,655 --> 00:03:16,820
the more sensitive it is,

85
00:03:16,820 --> 00:03:19,290
the more secure it has to be.

86
00:03:19,290 --> 00:03:23,005
Lastly, I want to highlight this last slide.

87
00:03:23,005 --> 00:03:25,455
Don't forget to monitor new developments at the FTC.

88
00:03:25,455 --> 00:03:27,230
I can't enforce enough that you've

89
00:03:27,230 --> 00:03:29,150
got resources at your disposal.

90
00:03:29,150 --> 00:03:32,630
The FTC website is amazing and including all kinds of

91
00:03:32,630 --> 00:03:36,950
information about their interpretations of the FTC Act,

92
00:03:36,950 --> 00:03:39,235
cases, consent degrees and so forth.

93
00:03:39,235 --> 00:03:41,390
So do make sure that you check that out.

94
00:03:41,390 --> 00:03:42,700
And good luck to you.

95
00:03:42,700 --> 00:03:44,645
Take a look at the whole slide presentation,

96
00:03:44,645 --> 00:03:46,550
and make sure that you understand

97
00:03:46,550 --> 00:03:50,390
all that is included here for you. Thank you.

