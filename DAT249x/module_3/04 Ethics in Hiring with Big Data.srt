0
00:00:00,040 --> 00:00:04,078
>> If you spend much time in organizations of any kind,

1
00:00:04,078 --> 00:00:05,810
you likely know how great it is to have

2
00:00:05,810 --> 00:00:08,255
a good boss and trustworthy coworkers.

3
00:00:08,255 --> 00:00:11,030
You've also probably had the opposite experience.

4
00:00:11,030 --> 00:00:13,835
And so you know that bad bosses or bad coworkers

5
00:00:13,835 --> 00:00:16,963
can make a work environment nearly intolerable.

6
00:00:16,963 --> 00:00:18,320
And you probably won't be surprised

7
00:00:18,320 --> 00:00:19,580
to learn that in studies,

8
00:00:19,580 --> 00:00:22,220
healthy work environments are positively associated with

9
00:00:22,220 --> 00:00:25,850
productivity and vice versa for toxic work cultures.

10
00:00:25,850 --> 00:00:27,710
So it's really easy to appreciate why

11
00:00:27,710 --> 00:00:30,780
businesses are so concerned to hire the right candidate.

12
00:00:30,780 --> 00:00:32,330
This is the basic problem that keeps

13
00:00:32,330 --> 00:00:35,695
human resources managers up at night.

14
00:00:35,695 --> 00:00:37,240
There have always been major barriers

15
00:00:37,240 --> 00:00:38,895
to hiring the right person.

16
00:00:38,895 --> 00:00:41,410
First of all, it's really hard to tell from a resume,

17
00:00:41,410 --> 00:00:43,930
recommendation and a half hour interview if

18
00:00:43,930 --> 00:00:45,190
the candidate in front of you will

19
00:00:45,190 --> 00:00:46,910
be good for your organization.

20
00:00:46,910 --> 00:00:50,140
This is the basic problem of measuring the qualitative.

21
00:00:50,140 --> 00:00:52,000
We know both through research and

22
00:00:52,000 --> 00:00:54,550
basic everyday experience what qualities

23
00:00:54,550 --> 00:00:56,020
are desirable in a worker.

24
00:00:56,020 --> 00:00:58,540
For example, emotional intelligence.

25
00:00:58,540 --> 00:01:01,450
But it's really hard to find a way to measure that.

26
00:01:01,450 --> 00:01:03,160
Then there's the problem of bias.

27
00:01:03,160 --> 00:01:04,660
In all societies there has been

28
00:01:04,660 --> 00:01:06,840
a significant bias against hiring women.

29
00:01:06,840 --> 00:01:08,500
And in the U.S., there has also been

30
00:01:08,500 --> 00:01:11,530
a major hiring bias against people of color and of course

31
00:01:11,530 --> 00:01:12,760
the truth is that an equally

32
00:01:12,760 --> 00:01:14,350
distributed number of those victims of

33
00:01:14,350 --> 00:01:15,610
bias would have been

34
00:01:15,610 --> 00:01:18,555
great bosses and wonderful coworkers.

35
00:01:18,555 --> 00:01:20,110
So wouldn't it be just great if

36
00:01:20,110 --> 00:01:23,290
an algorithm existed that could find the ideal employee

37
00:01:23,290 --> 00:01:25,840
and even better this algorithm wouldn't be biased

38
00:01:25,840 --> 00:01:28,570
against women and people of color, right?

39
00:01:28,570 --> 00:01:30,920
It probably won't surprise you to learn that

40
00:01:30,920 --> 00:01:33,710
there are many such algorithms already in use.

41
00:01:33,710 --> 00:01:36,380
And there is now a full blown discipline called people

42
00:01:36,380 --> 00:01:39,140
analytics and sometimes HR analytics,

43
00:01:39,140 --> 00:01:40,994
standing for human resources,

44
00:01:40,994 --> 00:01:42,560
that studies how algorithms can be

45
00:01:42,560 --> 00:01:44,660
used to find those great candidates.

46
00:01:44,660 --> 00:01:46,940
The experts on people analytics predict

47
00:01:46,940 --> 00:01:50,570
a complete change in how organizations find employees.

48
00:01:50,570 --> 00:01:54,202
Has people analytics brought the revolution it promised?

49
00:01:54,202 --> 00:01:55,730
Well, there is evidence that

50
00:01:55,730 --> 00:01:58,685
these algorithms are doing a pretty good job so far.

51
00:01:58,685 --> 00:02:00,500
In the further reading section of the module,

52
00:02:00,500 --> 00:02:02,720
you will find a link to a white paper published

53
00:02:02,720 --> 00:02:05,915
by McKinsey that details some of these successes.

54
00:02:05,915 --> 00:02:08,030
But remember, a theme of this course is that

55
00:02:08,030 --> 00:02:11,345
all revolutions have both winners and losers.

56
00:02:11,345 --> 00:02:13,070
In particular, there are two issues that

57
00:02:13,070 --> 00:02:15,205
should trouble us from an ethical perspective.

58
00:02:15,205 --> 00:02:17,540
And these issues are intention such that

59
00:02:17,540 --> 00:02:19,190
the solution to one tends to

60
00:02:19,190 --> 00:02:21,855
butt up against the solution to the other.

61
00:02:21,855 --> 00:02:24,115
Machines learn from data.

62
00:02:24,115 --> 00:02:26,195
So people analytics algorithms

63
00:02:26,195 --> 00:02:28,715
learn from whatever data we provide them.

64
00:02:28,715 --> 00:02:30,920
On the one side, the most obvious kind

65
00:02:30,920 --> 00:02:32,210
of relevant data about

66
00:02:32,210 --> 00:02:36,170
a potential worker is his or her employment history.

67
00:02:36,170 --> 00:02:38,265
But when algorithms read this data,

68
00:02:38,265 --> 00:02:40,460
all of the long standing human biases of

69
00:02:40,460 --> 00:02:41,900
the past are imported

70
00:02:41,900 --> 00:02:44,130
into the algorithm through the data.

71
00:02:44,130 --> 00:02:45,590
So the algorithm learns from

72
00:02:45,590 --> 00:02:47,960
past data regarding women and people of color

73
00:02:47,960 --> 00:02:50,150
without knowing the fact that the data was

74
00:02:50,150 --> 00:02:52,845
created in a context of bias.

75
00:02:52,845 --> 00:02:54,530
So far, we are not sure how to tell

76
00:02:54,530 --> 00:02:56,750
the algorithms about the context.

77
00:02:56,750 --> 00:02:58,400
So even though employment history data

78
00:02:58,400 --> 00:02:59,600
is extremely relevant,

79
00:02:59,600 --> 00:03:01,900
it is tainted by bias.

80
00:03:01,900 --> 00:03:04,100
One way to address that problem is to find

81
00:03:04,100 --> 00:03:07,145
data that does not have to do with employment history.

82
00:03:07,145 --> 00:03:08,525
If you remember from the lesson

83
00:03:08,525 --> 00:03:10,550
on customer relationship management,

84
00:03:10,550 --> 00:03:12,815
this amounts to finding proxies,

85
00:03:12,815 --> 00:03:14,900
bits of data from your digital trail that are

86
00:03:14,900 --> 00:03:17,210
not obviously related to your employment or

87
00:03:17,210 --> 00:03:19,340
work history but are used

88
00:03:19,340 --> 00:03:23,000
as predictors of success in the workplace.

89
00:03:23,000 --> 00:03:25,925
Everything we were worried about then still applies.

90
00:03:25,925 --> 00:03:28,460
The overall worry being that for all we know.

91
00:03:28,460 --> 00:03:29,840
Your digital trail may paint

92
00:03:29,840 --> 00:03:32,055
a badly inaccurate picture of you.

93
00:03:32,055 --> 00:03:34,235
For example, the algorithms

94
00:03:34,235 --> 00:03:36,380
might learn to avoid people with

95
00:03:36,380 --> 00:03:37,490
a six month gap in

96
00:03:37,490 --> 00:03:41,090
their employment history but maybe at one point,

97
00:03:41,090 --> 00:03:44,525
you took off six months to care for a parent or child,

98
00:03:44,525 --> 00:03:46,040
or maybe because you have

99
00:03:46,040 --> 00:03:47,623
a highly specialized job

100
00:03:47,623 --> 00:03:49,850
it took six months to find the new one.

101
00:03:49,850 --> 00:03:51,770
The algorithm won't care about the context.

102
00:03:51,770 --> 00:03:54,690
It just sees the six month gap.

103
00:03:54,690 --> 00:03:56,680
Now let me be clear, I have no idea if

104
00:03:56,680 --> 00:03:58,680
this six month employment gap is actually

105
00:03:58,680 --> 00:04:00,270
a proxy being used by people

106
00:04:00,270 --> 00:04:03,110
analytics but perhaps, that is the point.

107
00:04:03,110 --> 00:04:05,880
We just have no idea what these proxies are.

108
00:04:05,880 --> 00:04:06,990
It would be a real harm to

109
00:04:06,990 --> 00:04:10,253
the organization but more importantly, to you,

110
00:04:10,253 --> 00:04:12,060
if you were excluded from a job or

111
00:04:12,060 --> 00:04:13,665
even an entire job market

112
00:04:13,665 --> 00:04:16,780
because of some irrelevant proxy.

