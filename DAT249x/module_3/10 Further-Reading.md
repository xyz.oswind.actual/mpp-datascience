Further Reading
================

“Are you ready for the EU’s General Data Protection Regulation (GDPR)?
Our two new tools can help you find out” Microsoft Corporate Blog.
<https://blogs.microsoft.com/on-the-issues/2017/07/17/ready-eus-general-data-protection-regulation-gdpr-two-new-tools-can-help-find/>

Smith, Brad. “Keynote address, APP Global Privacy Summit 2016”
<https://www.youtube.com/watch?v=BnxlSObym88>
