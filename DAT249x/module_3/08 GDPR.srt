0
00:00:00,000 --> 00:00:03,655
In 2016, the European Parliament

1
00:00:03,655 --> 00:00:06,333
which is the legislative body of the European Union,

2
00:00:06,333 --> 00:00:09,160
enacted a sweeping data privacy law called the

3
00:00:09,160 --> 00:00:13,410
general data protection regulation or GDPR.

4
00:00:13,410 --> 00:00:15,105
Since the 1990s.

5
00:00:15,105 --> 00:00:16,850
The European Union has made it clear that

6
00:00:16,850 --> 00:00:19,580
privacy is a fundamental right for people in Europe.

7
00:00:19,580 --> 00:00:21,695
In fact, unlike our U.S. law,

8
00:00:21,695 --> 00:00:24,395
which as you recall approaches privacy by regulating

9
00:00:24,395 --> 00:00:27,560
access and management of personal data by sector,

10
00:00:27,560 --> 00:00:31,683
such as health data or financial data or student data,

11
00:00:31,683 --> 00:00:35,050
the European Union has taken a very expansive approach.

12
00:00:35,050 --> 00:00:36,695
Prior to the GDPR,

13
00:00:36,695 --> 00:00:40,420
the EU had implemented a privacy directive in 1994.

14
00:00:40,420 --> 00:00:42,890
That directive covered all personal data in

15
00:00:42,890 --> 00:00:45,920
the EU and had clear requirements.

16
00:00:45,920 --> 00:00:47,650
So why did the European Parliament pass

17
00:00:47,650 --> 00:00:49,985
a new data protection law, you might ask.

18
00:00:49,985 --> 00:00:52,340
Well, the GDPR permits regulators to

19
00:00:52,340 --> 00:00:54,710
fine corporations for noncompliance in

20
00:00:54,710 --> 00:00:59,735
excess of 20 million euros or 23 million U.S. dollars,

21
00:00:59,735 --> 00:01:03,680
or 4 percent of global revenue, whichever is greater.

22
00:01:03,680 --> 00:01:05,690
That's a huge number and a lot

23
00:01:05,690 --> 00:01:07,830
more than what was allowed under the previous law,

24
00:01:07,830 --> 00:01:10,850
the Privacy Directive. What's more?

25
00:01:10,850 --> 00:01:13,040
Companies without a physical presence in

26
00:01:13,040 --> 00:01:14,540
the European Union may be

27
00:01:14,540 --> 00:01:16,955
subject to regulation and liability.

28
00:01:16,955 --> 00:01:20,800
We learned about the term extraterritorial in MOD 1.

29
00:01:20,800 --> 00:01:22,970
The GDPR is extraterritorial

30
00:01:22,970 --> 00:01:25,085
to the extent it is regulating companies

31
00:01:25,085 --> 00:01:27,020
and processes that occur outside of

32
00:01:27,020 --> 00:01:30,135
the physical territory of the European Union.

33
00:01:30,135 --> 00:01:32,990
The concern of the EU seems to really be with respect to

34
00:01:32,990 --> 00:01:36,055
personal data transfers outside of the EU.

35
00:01:36,055 --> 00:01:37,335
The EU does not trust that

36
00:01:37,335 --> 00:01:39,785
other jurisdictions like the United States,

37
00:01:39,785 --> 00:01:42,845
will regulate data as protectively as it does.

38
00:01:42,845 --> 00:01:44,840
Past attempts to reach agreements on

39
00:01:44,840 --> 00:01:46,760
ways to protect data flows outside of

40
00:01:46,760 --> 00:01:48,620
the European Union had been invalidated

41
00:01:48,620 --> 00:01:50,705
by the European Union court of justice,

42
00:01:50,705 --> 00:01:52,340
because those agreements failed to

43
00:01:52,340 --> 00:01:55,990
adequately protect EU citizen rights.

44
00:01:55,990 --> 00:01:59,015
So what do businesses need to know about the GDPR.

45
00:01:59,015 --> 00:02:00,320
First, it applies to

46
00:02:00,320 --> 00:02:03,100
more businesses than the EU privacy directive.

47
00:02:03,100 --> 00:02:05,976
That is because it applies to all data processors,

48
00:02:05,976 --> 00:02:08,000
those who collect data and everyone in

49
00:02:08,000 --> 00:02:11,060
the chain of service distribution that handles it,

50
00:02:11,060 --> 00:02:13,640
regardless of where the business is actually located.

51
00:02:13,640 --> 00:02:17,180
So, a company processing in India information about

52
00:02:17,180 --> 00:02:19,310
individuals from France for

53
00:02:19,310 --> 00:02:20,780
purposes of predicting their credit,

54
00:02:20,780 --> 00:02:22,280
worthiness score will be

55
00:02:22,280 --> 00:02:24,470
subject to the reach of the new law and need

56
00:02:24,470 --> 00:02:25,700
to comply with its rules and

57
00:02:25,700 --> 00:02:29,630
regulations or face high financial penalties.

58
00:02:29,630 --> 00:02:31,040
Second, the definition of

59
00:02:31,040 --> 00:02:33,620
personal data has been expanded to include

60
00:02:33,620 --> 00:02:35,870
a person's name location data

61
00:02:35,870 --> 00:02:39,615
online identifiers and genetic information.

62
00:02:39,615 --> 00:02:42,615
Third, there are severe penalties for noncompliance.

63
00:02:42,615 --> 00:02:44,470
As I said, four percent of

64
00:02:44,470 --> 00:02:46,620
a company's gross worldwide revenue

65
00:02:46,620 --> 00:02:49,355
or 20 million euros whichever's greater.

66
00:02:49,355 --> 00:02:51,800
Factors considered in fining include whether

67
00:02:51,800 --> 00:02:55,310
the company acted negligently or intentionally.

68
00:02:55,310 --> 00:02:57,605
We learned about those theories in MOD 2.

69
00:02:57,605 --> 00:02:59,540
Remember, intentional behavior is

70
00:02:59,540 --> 00:03:03,260
purposeful and negligence is carelessness.

71
00:03:03,260 --> 00:03:05,210
Finally, data controllers must notify

72
00:03:05,210 --> 00:03:07,760
supervisory authorities within 72 hours

73
00:03:07,760 --> 00:03:09,470
of discovering a data breach.

74
00:03:09,470 --> 00:03:12,520
That's a short window of time.

75
00:03:12,520 --> 00:03:14,150
In addition, the GDPR has

76
00:03:14,150 --> 00:03:16,340
additional technical and process requirements which

77
00:03:16,340 --> 00:03:18,800
have many corporations vested right now,

78
00:03:18,800 --> 00:03:20,210
preparing for implementation of

79
00:03:20,210 --> 00:03:23,090
the law in the spring of 2018.

80
00:03:23,090 --> 00:03:24,230
For example, there are

81
00:03:24,230 --> 00:03:26,585
new specific record-keeping requirements

82
00:03:26,585 --> 00:03:28,430
and records must be produced for

83
00:03:28,430 --> 00:03:30,915
supervisory authorities on demand.

84
00:03:30,915 --> 00:03:33,095
With respect to individual consent,

85
00:03:33,095 --> 00:03:36,080
consent given by data subjects must be clear,

86
00:03:36,080 --> 00:03:38,810
freely given, informed and specific and

87
00:03:38,810 --> 00:03:42,710
can be withdrawn at any time without consequences.

88
00:03:42,710 --> 00:03:45,310
The good news is that unlike the privacy directive,

89
00:03:45,310 --> 00:03:47,360
the GDPR is a regulation.

90
00:03:47,360 --> 00:03:49,805
The privacy directive, as a directive,

91
00:03:49,805 --> 00:03:52,160
has to be implemented by every member country in

92
00:03:52,160 --> 00:03:54,705
the EU into its national law,

93
00:03:54,705 --> 00:03:56,210
which meant that there were

94
00:03:56,210 --> 00:04:00,140
as many variations of the directive as member countries.

95
00:04:00,140 --> 00:04:02,360
Since a GDPR is a regulation,

96
00:04:02,360 --> 00:04:04,100
there will only be one version of

97
00:04:04,100 --> 00:04:06,526
it and if in compliance with it's rules,

98
00:04:06,526 --> 00:04:08,090
companies will be in compliance

99
00:04:08,090 --> 00:04:10,190
in all of the member states.

100
00:04:10,190 --> 00:04:12,350
What steps should companies take to

101
00:04:12,350 --> 00:04:14,890
prepare for the GDPR's effective date?

102
00:04:14,890 --> 00:04:17,760
Louis Tague and Thomas Markey

103
00:04:17,760 --> 00:04:20,075
of the firm McNiece Wallis and Nurick,

104
00:04:20,075 --> 00:04:21,500
recommend the companies asked

105
00:04:21,500 --> 00:04:22,970
the following questions as they

106
00:04:22,970 --> 00:04:25,165
audit their data operations.

107
00:04:25,165 --> 00:04:27,170
One, what personal data does

108
00:04:27,170 --> 00:04:30,245
my company collect in store and how is it used?

109
00:04:30,245 --> 00:04:34,275
Two, do our activities fall within the scope of the GDPR?

110
00:04:34,275 --> 00:04:36,910
Three, do we meet

111
00:04:36,910 --> 00:04:40,982
the definition of a data processor or a data controller?

112
00:04:40,982 --> 00:04:42,650
Four, do we have

113
00:04:42,650 --> 00:04:45,992
high level employee responsibility for data security?

114
00:04:45,992 --> 00:04:50,295
And five, have we developed a data breach response plan?

115
00:04:50,295 --> 00:04:52,010
These are good questions for any company

116
00:04:52,010 --> 00:04:54,170
engaged in data management and processing.

117
00:04:54,170 --> 00:04:55,580
For companies using

118
00:04:55,580 --> 00:04:57,537
analytics and artificial intelligence,

119
00:04:57,537 --> 00:05:00,125
there are some pronounced issues based on the nature

120
00:05:00,125 --> 00:05:03,640
of the big data and the sheer volume and variety.

121
00:05:03,640 --> 00:05:05,870
Companies can't assume that tools like

122
00:05:05,870 --> 00:05:08,890
anonymization of data will suffice.

123
00:05:08,890 --> 00:05:11,910
The EU will require that in that case,

124
00:05:11,910 --> 00:05:13,610
there is a thorough assessment of

125
00:05:13,610 --> 00:05:16,700
any risk of re-identification.

126
00:05:16,700 --> 00:05:19,610
Also, privacy by design and approach we've been

127
00:05:19,610 --> 00:05:22,760
talking about since MOD 2 must be considered.

128
00:05:22,760 --> 00:05:24,905
Under the GDPR, it is called

129
00:05:24,905 --> 00:05:27,545
data protection by design or by default.

130
00:05:27,545 --> 00:05:29,075
And according to some authorities,

131
00:05:29,075 --> 00:05:30,860
it will become a legal requirement.

132
00:05:30,860 --> 00:05:32,484
In our next video,

133
00:05:32,484 --> 00:05:34,580
I will highlight some of the challenges of adapting

134
00:05:34,580 --> 00:05:38,980
data analytics and artificial intelligence to the GDPR.

