0
00:00:00,480 --> 00:00:03,760
>> In this video, we're talking about

1
00:00:03,760 --> 00:00:07,690
regulations in the United States that apply to employees,

2
00:00:07,690 --> 00:00:09,040
to protect them from

3
00:00:09,040 --> 00:00:14,105
discrimination and other misuses of their information.

4
00:00:14,105 --> 00:00:15,775
There's a list here for you to see,

5
00:00:15,775 --> 00:00:19,150
that the main laws that apply in the US to

6
00:00:19,150 --> 00:00:22,925
regulate and protect against employee discrimination.

7
00:00:22,925 --> 00:00:24,194
And so, in this area we're talking

8
00:00:24,194 --> 00:00:26,170
basically about people analytics.

9
00:00:26,170 --> 00:00:28,630
How do we protect people in

10
00:00:28,630 --> 00:00:31,366
the context of analytics and artificial intelligence?

11
00:00:31,366 --> 00:00:33,280
Looking first at existing law

12
00:00:33,280 --> 00:00:35,155
to see what the law requires.

13
00:00:35,155 --> 00:00:38,470
And then, considering instances in

14
00:00:38,470 --> 00:00:42,790
how the law will be applied to new technologies.

15
00:00:42,790 --> 00:00:44,615
So I'm not going to talk about all of these laws,

16
00:00:44,615 --> 00:00:47,350
I'm just going to highlight a few things for you.

17
00:00:47,350 --> 00:00:50,320
Title VII of the Civil Rights Act of 1964,

18
00:00:50,320 --> 00:00:52,720
is our primary law protecting against

19
00:00:52,720 --> 00:00:54,100
employment discrimination on

20
00:00:54,100 --> 00:00:57,605
the basis of protected characteristics.

21
00:00:57,605 --> 00:00:59,260
So generally, in the US,

22
00:00:59,260 --> 00:01:01,820
if you fall into a suspect classification

23
00:01:01,820 --> 00:01:04,270
and all of us fall into at least one,

24
00:01:04,270 --> 00:01:06,340
then you are protected from

25
00:01:06,340 --> 00:01:08,965
employment decisions based on,

26
00:01:08,965 --> 00:01:10,810
that harm you on the basis of

27
00:01:10,810 --> 00:01:13,420
your membership in that protected class.

28
00:01:13,420 --> 00:01:16,060
The protected classes are race,

29
00:01:16,060 --> 00:01:18,720
gender, religion and so forth.

30
00:01:18,720 --> 00:01:23,590
Title VII covers a host of the protected classifications.

31
00:01:23,590 --> 00:01:25,030
Then these other laws,

32
00:01:25,030 --> 00:01:26,845
the Americans with Disabilities Act,

33
00:01:26,845 --> 00:01:29,695
that protects those who are disabled,

34
00:01:29,695 --> 00:01:32,350
and disabled is defined very broadly from

35
00:01:32,350 --> 00:01:35,140
decisions that are made that harm them in employment,

36
00:01:35,140 --> 00:01:36,970
whether it's not hiring,

37
00:01:36,970 --> 00:01:38,900
firing, demoting or whatnot.

38
00:01:38,900 --> 00:01:40,840
Anything that is done to

39
00:01:40,840 --> 00:01:43,525
a person and employee with respect to a disability,

40
00:01:43,525 --> 00:01:46,015
would be covered under the the ADA.

41
00:01:46,015 --> 00:01:48,494
The Age Discrimination in Employment Act,

42
00:01:48,494 --> 00:01:50,560
just to let you know what that is,

43
00:01:50,560 --> 00:01:53,425
that is based on the classification of age.

44
00:01:53,425 --> 00:01:55,975
And in the United States under this regulation,

45
00:01:55,975 --> 00:01:57,820
if you are over 40,

46
00:01:57,820 --> 00:01:59,905
which doesn't really seem like it's that old,

47
00:01:59,905 --> 00:02:01,390
but if you're over 40,

48
00:02:01,390 --> 00:02:04,090
you are protected from employment decisions that cause

49
00:02:04,090 --> 00:02:07,405
you harm on the basis of that age.

50
00:02:07,405 --> 00:02:07,930
The last one,

51
00:02:07,930 --> 00:02:10,120
the Genetic Information Nondiscrimination Act

52
00:02:10,120 --> 00:02:12,725
is super interesting.

53
00:02:12,725 --> 00:02:15,700
This was the most recent of all of these laws.

54
00:02:15,700 --> 00:02:18,295
And it was passed by the U.S. Congress,

55
00:02:18,295 --> 00:02:20,680
because what was happening,

56
00:02:20,680 --> 00:02:22,015
was employers,

57
00:02:22,015 --> 00:02:23,560
and actually insurance companies

58
00:02:23,560 --> 00:02:25,285
on the consumer analytics side,

59
00:02:25,285 --> 00:02:27,970
but employers were using genetic information

60
00:02:27,970 --> 00:02:30,865
about their employees or their potential hires,

61
00:02:30,865 --> 00:02:33,505
to make decisions about hiring,

62
00:02:33,505 --> 00:02:36,060
firing, promoting and so forth.

63
00:02:36,060 --> 00:02:41,230
So you can imagine that if an employer gets access to

64
00:02:41,230 --> 00:02:43,030
your genetic information as part of

65
00:02:43,030 --> 00:02:47,470
a pre-employment physical and then decides,

66
00:02:47,470 --> 00:02:49,840
in fact, not to hire you because you have

67
00:02:49,840 --> 00:02:53,230
the genetic marker for prostate cancer,

68
00:02:53,230 --> 00:02:55,195
then that would be discrimination.

69
00:02:55,195 --> 00:02:56,350
Now, there was no law that

70
00:02:56,350 --> 00:02:58,475
protected us from that before GINA.

71
00:02:58,475 --> 00:02:59,605
Now that we have GINA,

72
00:02:59,605 --> 00:03:02,620
employers and insurance companies are not allowed to use

73
00:03:02,620 --> 00:03:04,150
that information to make employment

74
00:03:04,150 --> 00:03:07,160
or insurance decisions about us.

75
00:03:07,160 --> 00:03:09,190
So, looking at these laws,

76
00:03:09,190 --> 00:03:11,185
we realized that again,

77
00:03:11,185 --> 00:03:13,805
they were written before technology advanced

78
00:03:13,805 --> 00:03:16,630
and so we're going to be concerned to see whether or not,

79
00:03:16,630 --> 00:03:19,630
we can stretch them in their application to

80
00:03:19,630 --> 00:03:24,205
these new areas of analytics and artificial intelligence.

81
00:03:24,205 --> 00:03:25,993
The classifications under Title VII.

82
00:03:25,993 --> 00:03:27,280
Title VII is the one I'm going to

83
00:03:27,280 --> 00:03:30,100
focus most on here today.

84
00:03:30,100 --> 00:03:32,080
These are our protected classes,

85
00:03:32,080 --> 00:03:35,365
race, ethnicity, national origin, gender, and religion,

86
00:03:35,365 --> 00:03:37,975
all protected under Title VII.

87
00:03:37,975 --> 00:03:41,545
So, if you are treated differently

88
00:03:41,545 --> 00:03:46,225
or you experience some sort of employment harm,

89
00:03:46,225 --> 00:03:48,070
and you are a member of one

90
00:03:48,070 --> 00:03:49,765
of these classes, and in fact,

91
00:03:49,765 --> 00:03:51,190
the decision was made

92
00:03:51,190 --> 00:03:53,305
because of your membership in the class,

93
00:03:53,305 --> 00:03:56,470
then that is discrimination that is prohibited by

94
00:03:56,470 --> 00:04:01,220
Title VII of the Civil Rights Act of 1964.

95
00:04:01,220 --> 00:04:05,940
The theories here of liability, there's two.

96
00:04:05,940 --> 00:04:08,980
And these theories we see consistently

97
00:04:08,980 --> 00:04:11,440
across laws that are

98
00:04:11,440 --> 00:04:14,485
protecting us from discrimination in every context.

99
00:04:14,485 --> 00:04:16,690
So it's important to understand them here with respect

100
00:04:16,690 --> 00:04:19,140
to Title VII in the employment context.

101
00:04:19,140 --> 00:04:21,400
But you've seen and we've heard and talked

102
00:04:21,400 --> 00:04:24,063
about these different theories in other contexts.

103
00:04:24,063 --> 00:04:27,370
Any time there's an issue of bias or discrimination,

104
00:04:27,370 --> 00:04:30,400
these theories actually are relevant.

105
00:04:30,400 --> 00:04:32,985
So disparate treatment and disparate impact,

106
00:04:32,985 --> 00:04:35,095
how are these two things different?

107
00:04:35,095 --> 00:04:38,710
Disparate treatment with respect to Title VII is

108
00:04:38,710 --> 00:04:42,190
actually considered intentional discrimination.

109
00:04:42,190 --> 00:04:44,170
If you are not hired because of

110
00:04:44,170 --> 00:04:46,825
your membership in a protected class,

111
00:04:46,825 --> 00:04:48,700
that is considered intentional

112
00:04:48,700 --> 00:04:51,485
discrimination or disparate treatment.

113
00:04:51,485 --> 00:04:52,690
Treating you differently on

114
00:04:52,690 --> 00:04:55,225
the basis of your membership in that class.

115
00:04:55,225 --> 00:04:58,000
In data analytics, this may come up if

116
00:04:58,000 --> 00:05:00,990
an employer disfavors a particular group,

117
00:05:00,990 --> 00:05:04,095
has made some conclusions based on analytics,

118
00:05:04,095 --> 00:05:07,140
that certain group of people

119
00:05:07,140 --> 00:05:10,122
will be less desirable as employees.

120
00:05:10,122 --> 00:05:13,920
For example, we've talked about markers that might

121
00:05:13,920 --> 00:05:16,170
reveal that a person is more

122
00:05:16,170 --> 00:05:18,795
likely to quit in the first five years,

123
00:05:18,795 --> 00:05:22,860
or is more likely to be less productive,

124
00:05:22,860 --> 00:05:26,805
or whatever the conclusion is.

125
00:05:26,805 --> 00:05:28,860
If an employer is taking

126
00:05:28,860 --> 00:05:30,615
that conclusion and treating

127
00:05:30,615 --> 00:05:32,085
group differently on the basis,

128
00:05:32,085 --> 00:05:34,560
that's intentional, intentional harm.

129
00:05:34,560 --> 00:05:35,685
Now unfortunately,

130
00:05:35,685 --> 00:05:39,630
if the group isn't one of these protected classes,

131
00:05:39,630 --> 00:05:42,750
that's not considered a problem under Title VII.

132
00:05:42,750 --> 00:05:45,030
So we don't really see a whole lot of Title VII issues

133
00:05:45,030 --> 00:05:47,580
come up in analytics and artificial intelligence.

134
00:05:47,580 --> 00:05:49,140
What we do see, at least

135
00:05:49,140 --> 00:05:50,749
with respect to disparate treatment,

136
00:05:50,749 --> 00:05:53,235
we do see more of the disparate impact case

137
00:05:53,235 --> 00:05:55,230
in analytics and artificial intelligence.

138
00:05:55,230 --> 00:05:57,135
And impact, disparate impact

139
00:05:57,135 --> 00:05:59,520
is unintentional discrimination.

140
00:05:59,520 --> 00:06:04,415
Basically, this happens when neutral information is used.

141
00:06:04,415 --> 00:06:07,065
But, although it's neutral,

142
00:06:07,065 --> 00:06:09,030
it is impacting members of

143
00:06:09,030 --> 00:06:11,965
a protected class differently than others.

144
00:06:11,965 --> 00:06:15,625
So historically, in Title VII cases,

145
00:06:15,625 --> 00:06:17,160
disparate impact would come up,

146
00:06:17,160 --> 00:06:19,710
when an employer is using a certain kind of

147
00:06:19,710 --> 00:06:23,775
test to determine eligibility for a job.

148
00:06:23,775 --> 00:06:25,950
So if a job requires that you

149
00:06:25,950 --> 00:06:29,280
are able to lift 100 pounds of weight,

150
00:06:29,280 --> 00:06:31,620
you might have to pass a test for that,

151
00:06:31,620 --> 00:06:33,000
to see that you are physically

152
00:06:33,000 --> 00:06:35,580
able to do what the job requires.

153
00:06:35,580 --> 00:06:37,710
A lot of scholars are using

154
00:06:37,710 --> 00:06:41,010
disparate impact theory to talk about the potential for

155
00:06:41,010 --> 00:06:46,850
bias and discrimination in employment in this area.

156
00:06:46,850 --> 00:06:49,125
But it's not entirely in alignment,

157
00:06:49,125 --> 00:06:53,535
because when we're looking at analytics information,

158
00:06:53,535 --> 00:06:56,640
it's all information, it's all variables, it's all data.

159
00:06:56,640 --> 00:06:58,830
So we're not actually looking to correlate

160
00:06:58,830 --> 00:07:03,600
necessarily the data with what the job requires.

161
00:07:03,600 --> 00:07:06,960
But to the degree and extent that this is all we have for

162
00:07:06,960 --> 00:07:11,050
employment protections under Title VII disparate impact,

163
00:07:11,050 --> 00:07:13,230
is really going to be the theory that gets a lot of

164
00:07:13,230 --> 00:07:16,170
attention to see if groups are

165
00:07:16,170 --> 00:07:20,100
being disfavorably affected as

166
00:07:20,100 --> 00:07:24,390
a result of using algorithms, for example.

167
00:07:24,390 --> 00:07:26,655
And as we talked about in mod two,

168
00:07:26,655 --> 00:07:28,680
it's one thing to understand that this can

169
00:07:28,680 --> 00:07:30,540
happen and to know that it can happen,

170
00:07:30,540 --> 00:07:31,590
it's another thing to actually

171
00:07:31,590 --> 00:07:33,150
really be able to identify it,

172
00:07:33,150 --> 00:07:35,130
because there's really no transparency here.

173
00:07:35,130 --> 00:07:37,035
So an employee, who has an impact,

174
00:07:37,035 --> 00:07:39,090
might not even realize that he or she

175
00:07:39,090 --> 00:07:41,720
has been discriminated against in the first place.

176
00:07:41,720 --> 00:07:43,500
But things to look at and

177
00:07:43,500 --> 00:07:45,440
these two theories are really important to learn.

178
00:07:45,440 --> 00:07:46,770
And if we understand that we

179
00:07:46,770 --> 00:07:48,360
can see how they come up really

180
00:07:48,360 --> 00:07:53,590
in all law that protects us from bias and discrimination.

