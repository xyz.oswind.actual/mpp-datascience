0
00:00:00,000 --> 00:00:02,650
>> In the introduction to the lab

1
00:00:02,650 --> 00:00:04,420
at the end of module one,

2
00:00:04,420 --> 00:00:05,740
I told you how important

3
00:00:05,740 --> 00:00:07,090
getting the context was going to be,

4
00:00:07,090 --> 00:00:10,060
and part of that was getting the ethical context.

5
00:00:10,060 --> 00:00:11,530
And so I asked you to think about

6
00:00:11,530 --> 00:00:13,490
some values that would enter into it.

7
00:00:13,490 --> 00:00:16,975
The ones I came up with were quality and autonomy.

8
00:00:16,975 --> 00:00:20,050
Now we're going to get a little more specific.

9
00:00:20,050 --> 00:00:22,415
So, we're not talking about the values necessarily,

10
00:00:22,415 --> 00:00:24,460
we're talking about specific moral issues related

11
00:00:24,460 --> 00:00:26,963
to this kind of data set,

12
00:00:26,963 --> 00:00:29,360
the recidivism data set.

13
00:00:29,360 --> 00:00:30,850
So let's go through these.

14
00:00:30,850 --> 00:00:34,360
There's going to be three things

15
00:00:34,360 --> 00:00:36,245
that I want you to think about.

16
00:00:36,245 --> 00:00:39,085
The first two problems are indeed ethical problems,

17
00:00:39,085 --> 00:00:40,360
but they're really, really hard to

18
00:00:40,360 --> 00:00:43,045
test in a lab setting, I don't know how we would do that.

19
00:00:43,045 --> 00:00:45,100
But it's important that you understand these

20
00:00:45,100 --> 00:00:46,120
and have these in the back of your

21
00:00:46,120 --> 00:00:47,430
mind as you're working.

22
00:00:47,430 --> 00:00:49,855
The third one is going to be a little bit technical,

23
00:00:49,855 --> 00:00:52,000
and it's going to be the direct test

24
00:00:52,000 --> 00:00:54,810
that you have in your lab.

25
00:00:54,810 --> 00:00:58,680
So let's turn to problem one,

26
00:00:58,680 --> 00:01:02,295
the problem of unequal gains.

27
00:01:02,295 --> 00:01:04,805
So let's say we did a study,

28
00:01:04,805 --> 00:01:06,920
that's hypothetic, I'm completely

29
00:01:06,920 --> 00:01:08,765
making up the numbers and the results.

30
00:01:08,765 --> 00:01:11,080
But let's say we did this study,

31
00:01:11,080 --> 00:01:13,640
and what we found is that human judges,

32
00:01:13,640 --> 00:01:15,210
I don't know, what exactly

33
00:01:15,210 --> 00:01:17,610
our methodology was, it doesn't matter.

34
00:01:17,610 --> 00:01:19,970
We did a study and we found that human judges overpredict

35
00:01:19,970 --> 00:01:22,635
recidivism 30 percent of the time,

36
00:01:22,635 --> 00:01:25,010
meaning that they they

37
00:01:25,010 --> 00:01:29,355
hold people in jail more often than they need to.

38
00:01:29,355 --> 00:01:33,563
Now, we tested that historical data against an algorithm,

39
00:01:33,563 --> 00:01:35,020
we gave the algorithm historical data

40
00:01:35,020 --> 00:01:36,235
to see how it would do,

41
00:01:36,235 --> 00:01:39,130
and the algorithm overpredicted

42
00:01:39,130 --> 00:01:42,015
recidivism but only 12.5 percent of the time.

43
00:01:42,015 --> 00:01:44,230
So, math,

44
00:01:44,230 --> 00:01:47,110
you're thinking the algorithm, that wins, right?

45
00:01:47,110 --> 00:01:49,150
That's so much more accurate, that's that's causing

46
00:01:49,150 --> 00:01:51,207
less human suffering than the human judges,

47
00:01:51,207 --> 00:01:53,760
so let's go to the algorithm.

48
00:01:53,760 --> 00:01:57,465
Well, there are some things to think about here.

49
00:01:57,465 --> 00:02:02,160
So, further analysis of this hypothetical study shows

50
00:02:02,160 --> 00:02:04,575
that the algorithm reduces overprediction,

51
00:02:04,575 --> 00:02:06,405
but now we're breaking it down by race.

52
00:02:06,405 --> 00:02:07,740
So with Caucasian defendants,

53
00:02:07,740 --> 00:02:09,980
they get a reduction from 30 percent to 10 percent,

54
00:02:09,980 --> 00:02:11,745
so they're pretty well off.

55
00:02:11,745 --> 00:02:15,930
But then we look at African-American defendants and, yes,

56
00:02:15,930 --> 00:02:19,695
the algorithm made their lives better as a whole group,

57
00:02:19,695 --> 00:02:21,730
because it reduced

58
00:02:21,730 --> 00:02:26,175
the unnecessary jail time that they had,

59
00:02:26,175 --> 00:02:28,825
but not by as much, right?

60
00:02:28,825 --> 00:02:33,125
The reduction was only from 30 percent to 15 percent.

61
00:02:33,125 --> 00:02:35,500
Now you might be thinking, but wait a minute.

62
00:02:35,500 --> 00:02:38,200
That's still good for everybody, everybody benefits,

63
00:02:38,200 --> 00:02:41,760
and so obviously we got to still do this algorithm, right?

64
00:02:41,760 --> 00:02:44,295
It's better than the human judge.

65
00:02:44,295 --> 00:02:46,585
Well, I mean, there's something positive here,

66
00:02:46,585 --> 00:02:48,775
you have in fact reduced the amount of

67
00:02:48,775 --> 00:02:49,990
unnecessary jail time that

68
00:02:49,990 --> 00:02:51,865
people are suffering and that's good,

69
00:02:51,865 --> 00:02:54,933
but there's also a huge negative here, right?

70
00:02:54,933 --> 00:02:59,080
Now, it's more, this is obviously a hypothetical study,

71
00:02:59,080 --> 00:03:02,120
this is pretend, but if this kind of thing happened,

72
00:03:02,120 --> 00:03:05,285
we would have to say that now it's even more obvious how,

73
00:03:05,285 --> 00:03:06,910
and we can be mathematically precise,

74
00:03:06,910 --> 00:03:08,935
about how the color of a person's skin

75
00:03:08,935 --> 00:03:11,545
affects how much time they spend in jail.

76
00:03:11,545 --> 00:03:13,990
And that is not Okay, right?

77
00:03:13,990 --> 00:03:15,040
So what I would say about this

78
00:03:15,040 --> 00:03:16,390
is it's not time to abandon

79
00:03:16,390 --> 00:03:19,315
the project but just because everybody gained,

80
00:03:19,315 --> 00:03:21,340
because the gains were unequal,

81
00:03:21,340 --> 00:03:22,411
that's an ethical problem,

82
00:03:22,411 --> 00:03:24,385
that's an ethical issue.

83
00:03:24,385 --> 00:03:27,850
So, we would not throw our algorithm out entirely,

84
00:03:27,850 --> 00:03:29,470
but we have to work to improve it,

85
00:03:29,470 --> 00:03:32,775
so that there wouldn't be this unequal gain.

86
00:03:32,775 --> 00:03:35,130
Okay, let's talk about another problem,

87
00:03:35,130 --> 00:03:37,790
I call this the problem of divergent goals,

88
00:03:37,790 --> 00:03:40,120
and this has to do

89
00:03:40,120 --> 00:03:42,670
with what we're trying to do when we punish somebody,

90
00:03:42,670 --> 00:03:43,690
when we put somebody in jail,

91
00:03:43,690 --> 00:03:45,935
why would we even do that?

92
00:03:45,935 --> 00:03:46,770
It turns out that there's

93
00:03:46,770 --> 00:03:48,910
three justifications for punishment,

94
00:03:48,910 --> 00:03:50,620
and that's going to be a little tricky because

95
00:03:50,620 --> 00:03:53,350
the algorithm is only going to care about one of those,

96
00:03:53,350 --> 00:03:54,990
and it gets even more specific,

97
00:03:54,990 --> 00:03:56,140
I said three or four,

98
00:03:56,140 --> 00:03:57,910
because one of the justifications

99
00:03:57,910 --> 00:03:59,735
for punishment breaks down into two.

100
00:03:59,735 --> 00:04:03,700
So, there might be four depending on how you count that.

101
00:04:03,700 --> 00:04:05,705
The first theory is retributivism,

102
00:04:05,705 --> 00:04:08,530
so the idea that the only time you can punish somebody,

103
00:04:08,530 --> 00:04:10,210
the only time punishment is justified,

104
00:04:10,210 --> 00:04:13,110
is if it's for a crime that they did in the past,

105
00:04:13,110 --> 00:04:14,583
right, something that they've actually done,

106
00:04:14,583 --> 00:04:16,450
now it's time to punish them.

107
00:04:16,450 --> 00:04:18,310
But if you think about the algorithm

108
00:04:18,310 --> 00:04:20,095
that we've been working with,

109
00:04:20,095 --> 00:04:21,820
it doesn't really seem to have

110
00:04:21,820 --> 00:04:23,770
any acknowledgement of that importance, right?

111
00:04:23,770 --> 00:04:25,300
It's all about predicting the future,

112
00:04:25,300 --> 00:04:26,625
not about the past.

113
00:04:26,625 --> 00:04:28,330
So this algorithm doesn't take

114
00:04:28,330 --> 00:04:31,865
this theory of punishment into account at all.

115
00:04:31,865 --> 00:04:34,000
Another theory of punishment, rehabilitationism,

116
00:04:34,000 --> 00:04:35,110
and truth in advertising,

117
00:04:35,110 --> 00:04:37,650
the word rehab is there, that's exactly what it means.

118
00:04:37,650 --> 00:04:39,760
Punishment is justified if and only if

119
00:04:39,760 --> 00:04:43,560
it's likely to rehabilitate somebody.

120
00:04:43,560 --> 00:04:46,600
And this recidivism algorithm

121
00:04:46,600 --> 00:04:49,500
isn't trying to do that if that's not its goal.

122
00:04:49,500 --> 00:04:51,738
So, it's trying to be accurate in

123
00:04:51,738 --> 00:04:54,190
its prediction of who is going to re-offend,

124
00:04:54,190 --> 00:04:55,780
but it's not taking this, well,

125
00:04:55,780 --> 00:04:57,935
will it help them become a better person in the future?

126
00:04:57,935 --> 00:05:01,870
That's off the radar screen in this algorithm.

127
00:05:01,870 --> 00:05:03,280
This brings us to our third theory

128
00:05:03,280 --> 00:05:05,605
of punishment, consequentialism, again,

129
00:05:05,605 --> 00:05:08,740
has to do with consequences and punishment's

130
00:05:08,740 --> 00:05:10,345
justified only when it

131
00:05:10,345 --> 00:05:13,870
has good consequences for the whole society.

132
00:05:13,870 --> 00:05:15,040
So to help you understand this,

133
00:05:15,040 --> 00:05:20,110
let me rework an example that John Stuart Mill,

134
00:05:20,110 --> 00:05:23,630
a famous consequentialist, gave.

135
00:05:23,630 --> 00:05:26,050
So what if we

136
00:05:26,050 --> 00:05:28,325
found a serial killer, that sounds bad, right,

137
00:05:28,325 --> 00:05:31,324
but the serial killer is very,

138
00:05:31,324 --> 00:05:33,765
very, very old and frail.

139
00:05:33,765 --> 00:05:35,920
So, he's definitely not going

140
00:05:35,920 --> 00:05:39,690
to re-offend just because of the frailty.

141
00:05:39,690 --> 00:05:41,590
So, the question is,

142
00:05:41,590 --> 00:05:43,625
you're the only person who knows this.

143
00:05:43,625 --> 00:05:45,490
No one else knows it, it's just you,

144
00:05:45,490 --> 00:05:46,870
and you also happen to know that

145
00:05:46,870 --> 00:05:49,070
this person is not going to re-offend.

146
00:05:49,070 --> 00:05:50,680
Should they be punished for

147
00:05:50,680 --> 00:05:53,066
their life of being a serial killer?

148
00:05:53,066 --> 00:05:56,610
Now a consequentialist would say, clearly, no.

149
00:05:56,610 --> 00:05:58,810
All you're doing is increasing the amount of suffering in

150
00:05:58,810 --> 00:06:00,995
the world and it's not doing society any good, right?

151
00:06:00,995 --> 00:06:02,250
No one knows about it,

152
00:06:02,250 --> 00:06:03,906
and this person isn't going to re-offend so it would

153
00:06:03,906 --> 00:06:05,710
just be unnecessarily cruel,

154
00:06:05,710 --> 00:06:07,660
immoral even, to punish this person.

155
00:06:07,660 --> 00:06:09,340
That's the idea of consequentialism

156
00:06:09,340 --> 00:06:11,435
as a theory of punishment.

157
00:06:11,435 --> 00:06:13,570
Okay, so it's a little complicated because there's

158
00:06:13,570 --> 00:06:16,360
two ways to achieve consequentialism.

159
00:06:16,360 --> 00:06:19,180
These aren't really two extra theories,

160
00:06:19,180 --> 00:06:21,940
they're ways to achieve consequentialism.

161
00:06:21,940 --> 00:06:24,970
So one is deterrence, and it is what it sounds.

162
00:06:24,970 --> 00:06:26,830
It achieve social good by

163
00:06:26,830 --> 00:06:28,950
discouraging somebody from re-offending.

164
00:06:28,950 --> 00:06:31,720
So, they know that the penalty is this number of years in

165
00:06:31,720 --> 00:06:33,310
jail if they do that crime and so we're

166
00:06:33,310 --> 00:06:35,050
appealing to their rational side.

167
00:06:35,050 --> 00:06:36,418
Because they don't want to be in jail for that long,

168
00:06:36,418 --> 00:06:39,510
they don't do the crime, that's deterrence.

169
00:06:39,510 --> 00:06:41,270
I'm not sure how that would be relevant

170
00:06:41,270 --> 00:06:44,395
to the algorithms that we're looking at.

171
00:06:44,395 --> 00:06:46,460
I might be missing something.

172
00:06:46,460 --> 00:06:47,660
Someone could write a paper

173
00:06:47,660 --> 00:06:48,875
about it, I'd be happy to read that,

174
00:06:48,875 --> 00:06:50,660
but it doesn't seem like this has any relevance

175
00:06:50,660 --> 00:06:54,188
to the recidivism algorithms.

176
00:06:54,188 --> 00:06:56,720
What the recidivism algorithm does care about,

177
00:06:56,720 --> 00:06:58,250
the one of the four methods that it

178
00:06:58,250 --> 00:07:00,635
does care about, is incapacitation,

179
00:07:00,635 --> 00:07:03,380
which achieves the consequentialist objective

180
00:07:03,380 --> 00:07:04,970
by keeping criminals literally

181
00:07:04,970 --> 00:07:06,590
away from society because they're locked in jail,

182
00:07:06,590 --> 00:07:07,910
they can't commit crimes that way.

183
00:07:07,910 --> 00:07:08,960
That's what the recidivism

184
00:07:08,960 --> 00:07:10,265
algorithm is trying to predict,

185
00:07:10,265 --> 00:07:12,335
who those future criminals are going to be,

186
00:07:12,335 --> 00:07:14,195
let's keep them away from society,

187
00:07:14,195 --> 00:07:15,710
and the ones that don't need to be in jail,

188
00:07:15,710 --> 00:07:17,240
let's them out in society so they can

189
00:07:17,240 --> 00:07:20,795
be healthy contributors to society.

190
00:07:20,795 --> 00:07:23,180
Now as I mentioned, those first two problems are

191
00:07:23,180 --> 00:07:24,230
important to keep in the back of

192
00:07:24,230 --> 00:07:25,930
your mind as you're doing this module,

193
00:07:25,930 --> 00:07:29,155
but there's no way to directly test them in a lab.

194
00:07:29,155 --> 00:07:31,985
This third problem, however,

195
00:07:31,985 --> 00:07:33,245
you are going to spend some time

196
00:07:33,245 --> 00:07:34,880
working on a data set to try to

197
00:07:34,880 --> 00:07:36,650
figure out how you can deal with

198
00:07:36,650 --> 00:07:39,265
the problem of unfair proxies.

199
00:07:39,265 --> 00:07:41,437
So let me give you, this is really,

200
00:07:41,437 --> 00:07:44,810
really general, don't memorize

201
00:07:44,810 --> 00:07:46,550
this slide and then go and then pretend you're

202
00:07:46,550 --> 00:07:48,435
a data scientist, that's not going to do the trick.

203
00:07:48,435 --> 00:07:50,650
But I want to give you a little picture of how

204
00:07:50,650 --> 00:07:53,540
a predictive algorithm is set up.

205
00:07:53,540 --> 00:07:55,550
So first, we take a set of individuals,

206
00:07:55,550 --> 00:07:56,990
and these are past individuals because this

207
00:07:56,990 --> 00:07:58,874
has to be historical data,

208
00:07:58,874 --> 00:08:01,738
and these individuals were booked in jail,

209
00:08:01,738 --> 00:08:03,575
they spent some time in jail.

210
00:08:03,575 --> 00:08:06,550
So we know their names and everything, we have that data.

211
00:08:06,550 --> 00:08:07,775
But as a matter of fact,

212
00:08:07,775 --> 00:08:09,950
in a period of time, let's say in two years,

213
00:08:09,950 --> 00:08:14,050
some of them re-offended and some of them did not.

214
00:08:14,050 --> 00:08:16,220
So what you would do if you were a data scientist

215
00:08:16,220 --> 00:08:17,395
is you would say aha, OK,

216
00:08:17,395 --> 00:08:18,500
so there's the fact, some of them

217
00:08:18,500 --> 00:08:19,517
re-offended and some of them didn't,

218
00:08:19,517 --> 00:08:22,115
so those are just pure data.

219
00:08:22,115 --> 00:08:23,480
But now we want to try to find

220
00:08:23,480 --> 00:08:25,406
a variable, let's call it X,

221
00:08:25,406 --> 00:08:28,640
that correlates strongly with re-offending and at

222
00:08:28,640 --> 00:08:33,265
the same time does not correlate with not re-offending.

223
00:08:33,265 --> 00:08:35,005
So if that makes sense, think about that.

224
00:08:35,005 --> 00:08:36,200
It should make sense, right?

225
00:08:36,200 --> 00:08:41,360
We want to find what is connected to the re-offenders.

226
00:08:41,360 --> 00:08:43,040
And then what we're going to do with that

227
00:08:43,040 --> 00:08:44,810
is we're going to take that variable,

228
00:08:44,810 --> 00:08:46,970
we're calling it X, and drop it into

229
00:08:46,970 --> 00:08:50,360
this new formula that predicts behavior.

230
00:08:50,360 --> 00:08:52,240
So we have a set of new individuals,

231
00:08:52,240 --> 00:08:54,140
we don't know if they're going to re-offend in

232
00:08:54,140 --> 00:08:56,840
the future or not but we say aha,

233
00:08:56,840 --> 00:08:57,440
we don't know if they're going to

234
00:08:57,440 --> 00:08:58,460
re-offend but they do have

235
00:08:58,460 --> 00:08:59,840
this variable x that's been

236
00:08:59,840 --> 00:09:01,890
strongly correlated with re-offending,

237
00:09:01,890 --> 00:09:03,380
and so we're going to predict,

238
00:09:03,380 --> 00:09:04,820
this algorithm is going to predict

239
00:09:04,820 --> 00:09:08,150
that they're more likely to re-offend.

240
00:09:08,150 --> 00:09:11,840
That's the very, very basic level of how this all works.

241
00:09:11,840 --> 00:09:13,610
So that actually introduces

242
00:09:13,610 --> 00:09:17,360
the problem because whatever we come up with for X,

243
00:09:17,360 --> 00:09:21,095
whatever that variable is that we choose, it probably,

244
00:09:21,095 --> 00:09:22,682
there might be some exceptions,

245
00:09:22,682 --> 00:09:23,930
but as a general rule,

246
00:09:23,930 --> 00:09:25,460
it probably shouldn't be something that

247
00:09:25,460 --> 00:09:27,355
someone can't change about themselves.

248
00:09:27,355 --> 00:09:29,300
Classic example would be race.

249
00:09:29,300 --> 00:09:30,565
So if we said aha,

250
00:09:30,565 --> 00:09:35,360
people whose parents have this level of income,

251
00:09:35,360 --> 00:09:36,950
they're more likely to re-offend,

252
00:09:36,950 --> 00:09:39,470
therefore we're going to make you sit in

253
00:09:39,470 --> 00:09:42,584
jail for longer because of something your parents did.

254
00:09:42,584 --> 00:09:45,365
That rightly strikes as unethical

255
00:09:45,365 --> 00:09:48,230
because how could they change that, right?

256
00:09:48,230 --> 00:09:49,190
This has to be something that they

257
00:09:49,190 --> 00:09:50,540
could have done differently

258
00:09:50,540 --> 00:09:54,005
such as offending the first time.

259
00:09:54,005 --> 00:09:55,470
So, as the data scientist,

260
00:09:55,470 --> 00:09:56,790
you can't use something like race.

261
00:09:56,790 --> 00:09:58,140
You can't say aha, people

262
00:09:58,140 --> 00:09:59,765
with this race tend to re-offend,

263
00:09:59,765 --> 00:10:02,310
so let's put that information into the algorithm.

264
00:10:02,310 --> 00:10:04,405
That's not moral.

265
00:10:04,405 --> 00:10:05,730
So what you have to do is find

266
00:10:05,730 --> 00:10:07,710
another variable that strongly

267
00:10:07,710 --> 00:10:10,260
correlates with race or

268
00:10:10,260 --> 00:10:12,630
whatever it is that you're trying to find out.

269
00:10:12,630 --> 00:10:14,850
But it's really tricky because

270
00:10:14,850 --> 00:10:17,670
this new variable has

271
00:10:17,670 --> 00:10:19,750
to be strongly correlated on the one hand.

272
00:10:19,750 --> 00:10:21,930
On the other hand, it has to have new information.

273
00:10:21,930 --> 00:10:24,870
That is say, it can't be a proxy that simply

274
00:10:24,870 --> 00:10:28,050
imports race in in a way that you might not understand.

275
00:10:28,050 --> 00:10:30,000
So the example that we used a couple of times

276
00:10:30,000 --> 00:10:31,980
in this course is zip code.

277
00:10:31,980 --> 00:10:34,830
So in America, there

278
00:10:34,830 --> 00:10:37,615
tends to be racial segregation by zip codes.

279
00:10:37,615 --> 00:10:39,435
So you can't say well we're not going to use race,

280
00:10:39,435 --> 00:10:40,845
but we are going to use zip code.

281
00:10:40,845 --> 00:10:43,620
Well, yes, you're using a proxy but it's still

282
00:10:43,620 --> 00:10:46,330
unfair because it's still a proxy for race,

283
00:10:46,330 --> 00:10:48,780
so it's not it's not discovering any new information,

284
00:10:48,780 --> 00:10:50,070
it's just importing

285
00:10:50,070 --> 00:10:53,455
the same biased information in a new way.

286
00:10:53,455 --> 00:10:55,950
So good luck, your questions are going to be around

287
00:10:55,950 --> 00:10:59,155
this problem of getting at unfair proxies,

288
00:10:59,155 --> 00:11:01,920
and I hope it goes well for you.

