0
00:00:00,290 --> 00:00:02,820
In this video we're going to do

1
00:00:02,820 --> 00:00:04,745
IRAC exercise three together.

2
00:00:04,745 --> 00:00:06,810
Recall that the IRAC exercises

3
00:00:06,810 --> 00:00:08,640
are a legal analysis tool to

4
00:00:08,640 --> 00:00:13,090
help us move from issue identification to action.

5
00:00:13,090 --> 00:00:16,230
The IRAC stands for Issue, rule,

6
00:00:16,230 --> 00:00:19,160
application and conclusion and

7
00:00:19,160 --> 00:00:21,450
we'll read through this first story together.

8
00:00:21,450 --> 00:00:25,260
This story is about a company, company A,

9
00:00:25,260 --> 00:00:27,750
that has a new technology using

10
00:00:27,750 --> 00:00:30,510
the combination of a smartphone camera

11
00:00:30,510 --> 00:00:33,120
and machine learning to analyze the pupil of

12
00:00:33,120 --> 00:00:36,245
someone suffering from a potential concussion.

13
00:00:36,245 --> 00:00:37,980
The use of this technology is really

14
00:00:37,980 --> 00:00:40,860
meant to assist coaches,

15
00:00:40,860 --> 00:00:42,690
soccer coaches, football coaches,

16
00:00:42,690 --> 00:00:47,070
basketball coaches in time to identify if

17
00:00:47,070 --> 00:00:49,620
a person has a concussion and or

18
00:00:49,620 --> 00:00:52,200
not and if they do to keep them off the field,

19
00:00:52,200 --> 00:00:53,905
if they don't send them back in.

20
00:00:53,905 --> 00:00:56,460
But of course it has other multiple uses

21
00:00:56,460 --> 00:00:57,720
for anybody who might be

22
00:00:57,720 --> 00:01:00,030
concerned from sports or

23
00:01:00,030 --> 00:01:02,720
an injury that they suffered a concussion.

24
00:01:02,720 --> 00:01:06,850
So you can read through here what the app actually does.

25
00:01:06,850 --> 00:01:09,030
And then you're told that you

26
00:01:09,030 --> 00:01:12,095
use student are a recent hire company A.

27
00:01:12,095 --> 00:01:14,790
Your new boss knows that you took this course in

28
00:01:14,790 --> 00:01:17,490
law and ethics in data analytics and

29
00:01:17,490 --> 00:01:19,590
artificial intelligence and he wants

30
00:01:19,590 --> 00:01:21,750
or she wants your opinion about

31
00:01:21,750 --> 00:01:24,360
any possible legal concerns that you have for

32
00:01:24,360 --> 00:01:28,050
this new app on the smartphone.

33
00:01:28,050 --> 00:01:31,980
Now, we're going to look at this problem outside of

34
00:01:31,980 --> 00:01:34,980
any known FDA regulatory approvals because

35
00:01:34,980 --> 00:01:36,450
of course this would have to

36
00:01:36,450 --> 00:01:38,220
go through a regulatory approval process.

37
00:01:38,220 --> 00:01:40,140
That's not what our concern is here.

38
00:01:40,140 --> 00:01:42,210
We're going to be looking at other issues that we've

39
00:01:42,210 --> 00:01:44,370
learned about that might come up,

40
00:01:44,370 --> 00:01:45,870
legal concerns that might come up

41
00:01:45,870 --> 00:01:47,755
with respect to this new app.

42
00:01:47,755 --> 00:01:49,575
So the issue here,

43
00:01:49,575 --> 00:01:53,680
the issue arises out of the context of the story.

44
00:01:53,680 --> 00:01:55,770
The issue for us is,

45
00:01:55,770 --> 00:01:58,920
whether there is a possibility that consumers using

46
00:01:58,920 --> 00:02:00,630
the new app to diagnose

47
00:02:00,630 --> 00:02:03,045
a health condition here a concussion,

48
00:02:03,045 --> 00:02:06,360
will experience an invasion of privacy as a result of

49
00:02:06,360 --> 00:02:09,990
the collection and use of that personal medical data.

50
00:02:09,990 --> 00:02:11,430
That is our issue. Is there

51
00:02:11,430 --> 00:02:13,290
a possible legal issue with respect

52
00:02:13,290 --> 00:02:17,460
to privacy using an app like this.

53
00:02:17,460 --> 00:02:22,395
Now the rules that are relevant here are regulatory and

54
00:02:22,395 --> 00:02:24,735
the regulatory law that is

55
00:02:24,735 --> 00:02:27,150
immediately applicable is something

56
00:02:27,150 --> 00:02:28,692
called HIPAA in the US,

57
00:02:28,692 --> 00:02:31,637
the Health Insurance Portability and Accountability Act.

58
00:02:31,637 --> 00:02:33,630
HIPAA protect health data.

59
00:02:33,630 --> 00:02:36,720
Remember in the US the regulation of

60
00:02:36,720 --> 00:02:40,095
privacy concerns is bisector so health data,

61
00:02:40,095 --> 00:02:42,285
financial data, student data and so-forth.

62
00:02:42,285 --> 00:02:45,255
HIPAA is our law to protect

63
00:02:45,255 --> 00:02:48,681
health and medical data so medical records, health data.

64
00:02:48,681 --> 00:02:51,510
Generally HIPAA regulates organizations that

65
00:02:51,510 --> 00:02:52,980
meet two criteria and they're

66
00:02:52,980 --> 00:02:54,890
listed here for you in this rule.

67
00:02:54,890 --> 00:02:57,390
First an organization must play

68
00:02:57,390 --> 00:02:58,590
a certain role in the provision

69
00:02:58,590 --> 00:02:59,910
of health care to patients.

70
00:02:59,910 --> 00:03:01,740
So if you're regulated by HIPAA,

71
00:03:01,740 --> 00:03:04,380
you are likely an organization that is having

72
00:03:04,380 --> 00:03:05,640
something to do with the provision

73
00:03:05,640 --> 00:03:07,715
of health care to patients.

74
00:03:07,715 --> 00:03:09,540
If it does and it meets

75
00:03:09,540 --> 00:03:11,400
that definition it will be

76
00:03:11,400 --> 00:03:13,735
something called a covered entity.

77
00:03:13,735 --> 00:03:16,500
Also there are business associates under HIPAA.

78
00:03:16,500 --> 00:03:18,060
So these are legally defined terms,

79
00:03:18,060 --> 00:03:20,070
you're either a covered entity providing

80
00:03:20,070 --> 00:03:23,535
healthcare or you are a business associate.

81
00:03:23,535 --> 00:03:26,310
Business associates are those that

82
00:03:26,310 --> 00:03:28,860
work with covered entities.

83
00:03:28,860 --> 00:03:32,790
So service providers, cloud service providers,

84
00:03:32,790 --> 00:03:34,350
storing data for example could

85
00:03:34,350 --> 00:03:37,020
be considered a business associate.

86
00:03:37,020 --> 00:03:41,075
Secondly HIPAA only applies when the organization,

87
00:03:41,075 --> 00:03:43,160
covered entity or business associate

88
00:03:43,160 --> 00:03:46,120
handles protected health information PHI.

89
00:03:46,120 --> 00:03:48,580
Therefore to determine whether HIPAA

90
00:03:48,580 --> 00:03:51,230
applies to this situation we're going to be

91
00:03:51,230 --> 00:03:53,570
thinking about if the organization

92
00:03:53,570 --> 00:03:54,770
meets these definitions of

93
00:03:54,770 --> 00:03:55,940
covered entity or business

94
00:03:55,940 --> 00:03:59,085
associate and if it is handling PHI.

95
00:03:59,085 --> 00:04:01,160
Now, this is a new area, right?

96
00:04:01,160 --> 00:04:03,730
It's a technology.

97
00:04:03,730 --> 00:04:05,690
HIPAA was written before

98
00:04:05,690 --> 00:04:08,495
apps were being created at rapid pace.

99
00:04:08,495 --> 00:04:09,740
So we're going to be taking

100
00:04:09,740 --> 00:04:12,440
existing law HIPAA and applying it here

101
00:04:12,440 --> 00:04:17,670
in the applications piece to reach a conclusion.

102
00:04:17,670 --> 00:04:20,400
There is no case law in this area.

103
00:04:20,400 --> 00:04:24,035
We can't go just look up a case about apps and HIPAA

104
00:04:24,035 --> 00:04:27,980
that's unfortunately we don't have that luxury.

105
00:04:27,980 --> 00:04:30,020
So we're going to have to reason through and this

106
00:04:30,020 --> 00:04:32,450
as you as the new employee of company.

107
00:04:32,450 --> 00:04:33,965
This is what you would be doing.

108
00:04:33,965 --> 00:04:36,575
So based on what you know about this new app,

109
00:04:36,575 --> 00:04:40,880
how it works, who will be using it,

110
00:04:40,880 --> 00:04:42,890
do you think that the government regulators

111
00:04:42,890 --> 00:04:44,870
would conclude that your new company is

112
00:04:44,870 --> 00:04:49,365
subject to HIPAA's privacy and security compliance rules?

113
00:04:49,365 --> 00:04:51,020
So that's really the most important thing here.

114
00:04:51,020 --> 00:04:52,340
If we think that we're subject to

115
00:04:52,340 --> 00:04:54,920
HIPAA then we have to make sure that in

116
00:04:54,920 --> 00:04:59,885
our business of marketing and distributing this app and

117
00:04:59,885 --> 00:05:02,410
taking data and managing data that

118
00:05:02,410 --> 00:05:05,263
we're following along with all of HIPAA's various rules.

119
00:05:05,263 --> 00:05:08,655
So questions to consider here.

120
00:05:08,655 --> 00:05:10,190
These come from the US

121
00:05:10,190 --> 00:05:12,465
Department of Health and Human Services.

122
00:05:12,465 --> 00:05:13,970
And these are suggestions about

123
00:05:13,970 --> 00:05:17,090
how companies in technology might

124
00:05:17,090 --> 00:05:19,670
consider HIPAA and think about

125
00:05:19,670 --> 00:05:21,050
whether or not they should be complying

126
00:05:21,050 --> 00:05:22,770
with the compliance rules.

127
00:05:22,770 --> 00:05:25,325
One, who are the apps customers?

128
00:05:25,325 --> 00:05:28,385
Two, who directs use of health data?

129
00:05:28,385 --> 00:05:31,610
Three, does company A have a formal relationship

130
00:05:31,610 --> 00:05:35,305
with a covered entity like a health care provider?

131
00:05:35,305 --> 00:05:37,400
And four, will a covered entity

132
00:05:37,400 --> 00:05:40,205
require the use of the app?

133
00:05:40,205 --> 00:05:42,485
So these are all things that help us

134
00:05:42,485 --> 00:05:44,915
tease through and think about

135
00:05:44,915 --> 00:05:47,510
whether or not this new technology should

136
00:05:47,510 --> 00:05:50,835
be regulated by existing law and HIPAA.

137
00:05:50,835 --> 00:05:53,900
And the thinking, the general thinking right

138
00:05:53,900 --> 00:05:56,790
now about this is that the apps,

139
00:05:56,790 --> 00:05:58,280
there are a lot of apps in

140
00:05:58,280 --> 00:05:59,885
the area of health care you know,

141
00:05:59,885 --> 00:06:01,910
just keeping data, managing

142
00:06:01,910 --> 00:06:04,280
data and there's a lot of power that

143
00:06:04,280 --> 00:06:06,440
consumers can have with

144
00:06:06,440 --> 00:06:08,390
respect to their health data because

145
00:06:08,390 --> 00:06:12,610
they have access to their own information,

146
00:06:12,610 --> 00:06:13,635
they know where to share it,

147
00:06:13,635 --> 00:06:15,410
who they want to share it with.

148
00:06:15,410 --> 00:06:20,685
It seems like in the app area at least and

149
00:06:20,685 --> 00:06:24,065
presently that unless the app developer

150
00:06:24,065 --> 00:06:26,570
is working with a covered entity,

151
00:06:26,570 --> 00:06:30,125
like a doctor's office or a health insurance plan,

152
00:06:30,125 --> 00:06:32,645
it's likely that the app is going to fall

153
00:06:32,645 --> 00:06:36,135
outside of the regulation of HIPAA.

154
00:06:36,135 --> 00:06:38,450
So the distance from being

155
00:06:38,450 --> 00:06:39,530
a covered entity or

156
00:06:39,530 --> 00:06:41,610
a business associate is really important.

157
00:06:41,610 --> 00:06:43,670
And also because consumers can really

158
00:06:43,670 --> 00:06:46,790
direct the use of the data that seems to

159
00:06:46,790 --> 00:06:50,230
leave that the US HHS to

160
00:06:50,230 --> 00:06:52,040
conclude that apps like this are

161
00:06:52,040 --> 00:06:54,080
not going to be subject to regulation,

162
00:06:54,080 --> 00:06:55,310
at least not now.

163
00:06:55,310 --> 00:06:57,260
Again, remember we're looking in this course

164
00:06:57,260 --> 00:06:59,480
at existing law to see if it applies to

165
00:06:59,480 --> 00:07:03,470
new technology but we're also aware that the law may

166
00:07:03,470 --> 00:07:07,670
and at some point likely will catch up to the technology.

167
00:07:07,670 --> 00:07:09,140
So at the moment

168
00:07:09,140 --> 00:07:11,420
although there are certainly other regulations that

169
00:07:11,420 --> 00:07:13,040
could apply to the design of

170
00:07:13,040 --> 00:07:15,385
the app the regulation of HIPAA,

171
00:07:15,385 --> 00:07:18,725
the privacy and security regulations likely do not

172
00:07:18,725 --> 00:07:20,450
apply to this situation and

173
00:07:20,450 --> 00:07:24,090
Company A does not have to be concerned with those.

