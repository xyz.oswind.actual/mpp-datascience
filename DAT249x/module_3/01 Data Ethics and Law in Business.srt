0
00:00:00,000 --> 00:00:02,950
>> In our previous module, we had talked about how

1
00:00:02,950 --> 00:00:05,350
data relates to the individual on society,

2
00:00:05,350 --> 00:00:07,120
and issues and opportunities around

3
00:00:07,120 --> 00:00:09,490
bias and privacy come up.

4
00:00:09,490 --> 00:00:12,385
But as we move in this module towards more

5
00:00:12,385 --> 00:00:16,225
about businesses and actual corporate entities,

6
00:00:16,225 --> 00:00:18,115
I can only imagine how those become

7
00:00:18,115 --> 00:00:21,070
more impactful and important to discuss.

8
00:00:21,070 --> 00:00:23,020
So, Eva, Nathan, could you tell us

9
00:00:23,020 --> 00:00:25,680
about this module and what students are going to learn?

10
00:00:25,680 --> 00:00:26,995
>> Yes, absolutely.

11
00:00:26,995 --> 00:00:28,660
In this module we're going to be turning

12
00:00:28,660 --> 00:00:31,165
our attention from individuals and society,

13
00:00:31,165 --> 00:00:33,475
to businesses as the dominant player

14
00:00:33,475 --> 00:00:36,355
in artificial intelligence and data analytics,

15
00:00:36,355 --> 00:00:38,395
and we'll see that, unlike

16
00:00:38,395 --> 00:00:39,785
the earlier modules where we're

17
00:00:39,785 --> 00:00:41,170
looking at law in general,

18
00:00:41,170 --> 00:00:42,455
and sources of law,

19
00:00:42,455 --> 00:00:46,300
the constitution and statutes and case law,

20
00:00:46,300 --> 00:00:48,400
here we're really looking at regulation,

21
00:00:48,400 --> 00:00:49,615
at least from the legal perspective,

22
00:00:49,615 --> 00:00:51,100
we're looking at how is business

23
00:00:51,100 --> 00:00:54,460
regulated in data analytics and artificial intelligence?

24
00:00:54,460 --> 00:00:56,585
So we'll look at some U.S. laws that apply.

25
00:00:56,585 --> 00:00:58,725
We'll look at European law that applies,

26
00:00:58,725 --> 00:01:00,645
and also see how there's

27
00:01:00,645 --> 00:01:02,530
some conflict because the law

28
00:01:02,530 --> 00:01:05,015
predates all of this technology.

29
00:01:05,015 --> 00:01:06,895
So we've got that ongoing theme of,

30
00:01:06,895 --> 00:01:10,485
how is the law going to be interpreted,

31
00:01:10,485 --> 00:01:13,080
with respect to these new technologies?

32
00:01:13,080 --> 00:01:15,970
>> Yes, so as well as regulation we have

33
00:01:15,970 --> 00:01:18,750
this other side of, perhaps responsibility right?

34
00:01:18,750 --> 00:01:21,860
Of what not only companies shouldn't or should do,

35
00:01:21,860 --> 00:01:22,945
from the legal perspective,

36
00:01:22,945 --> 00:01:25,420
but what are they actually called to

37
00:01:25,420 --> 00:01:28,136
do as an organization.

38
00:01:28,136 --> 00:01:29,960
>> Yes. Great transition

39
00:01:29,960 --> 00:01:32,185
to Nathan into the ethical frameworks,

40
00:01:32,185 --> 00:01:35,350
because what we call corporate responsibility or

41
00:01:35,350 --> 00:01:37,000
corporate social responsibility or

42
00:01:37,000 --> 00:01:39,115
corporate citizenship even,

43
00:01:39,115 --> 00:01:43,350
is looking at more than what the law is going to require.

44
00:01:43,350 --> 00:01:45,745
Remember that the law is the moral minimum.

45
00:01:45,745 --> 00:01:48,060
It sets a minimal standard for

46
00:01:48,060 --> 00:01:50,210
businesses but what Nathan's

47
00:01:50,210 --> 00:01:51,850
going to be teaching in this course,

48
00:01:51,850 --> 00:01:53,040
in this module of the course,

49
00:01:53,040 --> 00:01:56,825
is actually speaking more to the responsibility side.

50
00:01:56,825 --> 00:01:57,113
>> Got it.

51
00:01:57,113 --> 00:02:00,285
>> Yes. This is such a huge and interesting topic.

52
00:02:00,285 --> 00:02:04,355
You can't overestimate how important this is.

53
00:02:04,355 --> 00:02:06,730
So we have this discipline, business ethics,

54
00:02:06,730 --> 00:02:07,900
it's near and dear to my heart,

55
00:02:07,900 --> 00:02:09,580
it's the class I teach the most,

56
00:02:09,580 --> 00:02:11,290
and the way people think

57
00:02:11,290 --> 00:02:13,000
about business is all about greed and

58
00:02:13,000 --> 00:02:16,320
exploitation and they think about ethics as being kind,

59
00:02:16,320 --> 00:02:18,010
and they're like, How can business ethics,

60
00:02:18,010 --> 00:02:20,570
how do those two words even go together?

61
00:02:20,570 --> 00:02:22,360
But it turns out, lots of interesting ways.

62
00:02:22,360 --> 00:02:23,440
That's not a very good way to

63
00:02:23,440 --> 00:02:25,120
understand business or ethics.

64
00:02:25,120 --> 00:02:29,025
So we talked about the five specific ethical values,

65
00:02:29,025 --> 00:02:32,500
and it turns out that trust and character excellence,

66
00:02:32,500 --> 00:02:35,433
but now it's translated as culture excellence,

67
00:02:35,433 --> 00:02:37,270
turn out to be very important for ethics

68
00:02:37,270 --> 00:02:39,745
at an organizational level.

69
00:02:39,745 --> 00:02:42,175
And what's definitely true

70
00:02:42,175 --> 00:02:44,860
is that there's lots of moving pieces with business.

71
00:02:44,860 --> 00:02:47,005
But every one of those pieces is

72
00:02:47,005 --> 00:02:50,405
totally being transformed by data and AI.

73
00:02:50,405 --> 00:02:53,069
So it is very important to have these conversations,

74
00:02:53,069 --> 00:02:58,055
and this was my favorite module to work on with you guys.

75
00:02:58,055 --> 00:02:59,560
This is a very important conversation and I

76
00:02:59,560 --> 00:03:01,630
hope this gets people thinking.

77
00:03:01,630 --> 00:03:02,177
>> Yes.

78
00:03:02,177 --> 00:03:04,930
>> Excellent. Thank you both, and

79
00:03:04,930 --> 00:03:08,000
we look forward to joining you. Thank you.

