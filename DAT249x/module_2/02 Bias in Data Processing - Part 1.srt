0
00:00:00,030 --> 00:00:02,980
There will obviously be any number of

1
00:00:02,980 --> 00:00:06,095
differences between human bias and machine bias.

2
00:00:06,095 --> 00:00:07,510
And in this course, we are, of course,

3
00:00:07,510 --> 00:00:09,335
concerned with the bias of machines.

4
00:00:09,335 --> 00:00:11,650
But you may be surprised about

5
00:00:11,650 --> 00:00:14,500
how large the similarities are and how many they are.

6
00:00:14,500 --> 00:00:17,350
So let's begin with a basic distinction in human bias,

7
00:00:17,350 --> 00:00:19,420
and then we will use that to help us think about

8
00:00:19,420 --> 00:00:21,870
the problem of machine bias.

9
00:00:21,870 --> 00:00:23,590
When we speak of human bias,

10
00:00:23,590 --> 00:00:24,760
we are either talking about

11
00:00:24,760 --> 00:00:27,490
conscious bias or unconscious bias.

12
00:00:27,490 --> 00:00:29,025
And a quick programming note,

13
00:00:29,025 --> 00:00:30,365
many of you have heard the terms

14
00:00:30,365 --> 00:00:33,045
explicit and implicit in describing bias,

15
00:00:33,045 --> 00:00:35,065
and I mean the same thing with this distinction,

16
00:00:35,065 --> 00:00:36,505
but the terms conscious and

17
00:00:36,505 --> 00:00:38,590
unconscious are more descriptive,

18
00:00:38,590 --> 00:00:40,170
and so I'll use those terms instead.

19
00:00:40,170 --> 00:00:44,250
A conscious bias is simply one you are aware of.

20
00:00:44,250 --> 00:00:46,520
For example, you may be a male hiring manager and

21
00:00:46,520 --> 00:00:49,480
are convinced that women are less good employees,

22
00:00:49,480 --> 00:00:53,065
and so you disadvantage them in the hiring process.

23
00:00:53,065 --> 00:00:55,120
On the other hand, an unconscious bias

24
00:00:55,120 --> 00:00:56,425
is still a real bias,

25
00:00:56,425 --> 00:00:59,540
but by definition, you are unaware that you have it.

26
00:00:59,540 --> 00:01:01,180
In fact, you might very strongly and

27
00:01:01,180 --> 00:01:03,820
sincerely insist that you are not biased.

28
00:01:03,820 --> 00:01:05,680
But if the bias is subconscious,

29
00:01:05,680 --> 00:01:08,050
it could still be affecting your decision-making,

30
00:01:08,050 --> 00:01:10,345
and you might never realize it.

31
00:01:10,345 --> 00:01:12,480
We are here not thinking about

32
00:01:12,480 --> 00:01:14,895
the machine equivalent of conscious bias.

33
00:01:14,895 --> 00:01:17,575
That would be, to use our earlier example,

34
00:01:17,575 --> 00:01:20,100
if a male data scientist intentionally designs

35
00:01:20,100 --> 00:01:23,370
a hiring algorithm that disadvantages female candidates.

36
00:01:23,370 --> 00:01:25,050
It goes without saying that this is

37
00:01:25,050 --> 00:01:27,315
extremely unethical and certainly illegal,

38
00:01:27,315 --> 00:01:28,830
and so must be rooted out.

39
00:01:28,830 --> 00:01:31,205
But this is not the present concern.

40
00:01:31,205 --> 00:01:33,090
We are worried about the machine equivalent

41
00:01:33,090 --> 00:01:35,585
of human subconscious bias.

42
00:01:35,585 --> 00:01:37,680
The kind that slips into our mind over

43
00:01:37,680 --> 00:01:39,795
time through our experiences,

44
00:01:39,795 --> 00:01:42,300
but by definition, we are not aware of how it

45
00:01:42,300 --> 00:01:45,020
affects our assumptions and perceptions of other people.

46
00:01:45,020 --> 00:01:47,870
There is indeed a machine equivalent

47
00:01:47,870 --> 00:01:49,310
of this kind of bias because

48
00:01:49,310 --> 00:01:51,020
bias can easily enter data

49
00:01:51,020 --> 00:01:54,720
processing without us noticing it in two ways.

50
00:01:54,720 --> 00:01:57,230
First, machines learn their decision rules

51
00:01:57,230 --> 00:01:58,760
from the data that we give it.

52
00:01:58,760 --> 00:02:00,965
It first, identifies patterns in the data,

53
00:02:00,965 --> 00:02:02,390
and then infers decision making

54
00:02:02,390 --> 00:02:05,715
procedures in accordance with those patterns.

55
00:02:05,715 --> 00:02:08,195
Now, those patterns are probably quite accurate,

56
00:02:08,195 --> 00:02:11,415
but because machines are really good at their jobs.

57
00:02:11,415 --> 00:02:13,535
But if the machine learns from data

58
00:02:13,535 --> 00:02:16,070
that was itself the product of bias,

59
00:02:16,070 --> 00:02:18,495
whatever historical biases existed

60
00:02:18,495 --> 00:02:22,905
are now in the subconscious of the machine so to speak.

61
00:02:22,905 --> 00:02:25,550
And this is not some hypothetical future concern.

62
00:02:25,550 --> 00:02:26,840
In the previous video,

63
00:02:26,840 --> 00:02:28,280
Eva gave some examples of how

64
00:02:28,280 --> 00:02:30,975
bias exists in the collection process.

65
00:02:30,975 --> 00:02:32,120
This is simply a version of

66
00:02:32,120 --> 00:02:35,355
the age old garbage in, garbage out principle.

67
00:02:35,355 --> 00:02:37,775
If you feed the machine biased data,

68
00:02:37,775 --> 00:02:39,395
what do you think it's going to learn?

69
00:02:39,395 --> 00:02:41,000
In human terms this is like sending

70
00:02:41,000 --> 00:02:43,475
a child to school that uses biased textbooks,

71
00:02:43,475 --> 00:02:45,530
or introduces them to movies

72
00:02:45,530 --> 00:02:48,235
or language that shows racial bias.

73
00:02:48,235 --> 00:02:50,790
What do you think the child's going to learn?

