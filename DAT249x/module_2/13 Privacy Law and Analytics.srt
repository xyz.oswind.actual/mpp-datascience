0
00:00:00,090 --> 00:00:02,830
>> The sources of US privacy law are

1
00:00:02,830 --> 00:00:05,305
identified here on the screen for you.

2
00:00:05,305 --> 00:00:08,200
We've talked a little about the US Constitution,

3
00:00:08,200 --> 00:00:09,520
and we're going to talk more about

4
00:00:09,520 --> 00:00:13,510
statutory law that comes up mostly in a business context.

5
00:00:13,510 --> 00:00:16,540
We'll identify that in mod three.

6
00:00:16,540 --> 00:00:18,220
For purposes of the video

7
00:00:18,220 --> 00:00:20,740
here and in mod two where we're talking about

8
00:00:20,740 --> 00:00:23,140
the relationship of the individual and

9
00:00:23,140 --> 00:00:26,417
society and data analytics and artificial intelligence,

10
00:00:26,417 --> 00:00:27,580
we're going to focus on

11
00:00:27,580 --> 00:00:29,560
the common law source

12
00:00:29,560 --> 00:00:32,630
of privacy law in the US, which is tort law.

13
00:00:32,630 --> 00:00:36,790
Tort law is law that protects us from emotional harm,

14
00:00:36,790 --> 00:00:39,520
financial harm and physical harm.

15
00:00:39,520 --> 00:00:41,680
It is state law, it's common law,

16
00:00:41,680 --> 00:00:43,720
meaning we find the rules of law in

17
00:00:43,720 --> 00:00:46,840
the cases that we read about what these torts are.

18
00:00:46,840 --> 00:00:48,855
So these have all been created,

19
00:00:48,855 --> 00:00:50,560
these torts were created by

20
00:00:50,560 --> 00:00:53,390
our court system in the United States.

21
00:00:53,390 --> 00:00:56,680
There are many scholars that are looking

22
00:00:56,680 --> 00:00:59,740
at tort law as one aspect of privacy law

23
00:00:59,740 --> 00:01:02,470
that might be very successfully used to protect

24
00:01:02,470 --> 00:01:06,860
individuals from the use and misuse of their data.

25
00:01:06,860 --> 00:01:09,760
The answer to this question is still outstanding.

26
00:01:09,760 --> 00:01:11,440
We don't know the answer but

27
00:01:11,440 --> 00:01:15,280
we're opining at this point that this may be an area

28
00:01:15,280 --> 00:01:16,780
that we can find some protections

29
00:01:16,780 --> 00:01:18,280
for individuals who feel that

30
00:01:18,280 --> 00:01:20,500
their private information has been used and

31
00:01:20,500 --> 00:01:23,320
misused in a way that causes them emotional,

32
00:01:23,320 --> 00:01:25,555
physical or financial harm.

33
00:01:25,555 --> 00:01:28,990
There are four intentional torts in privacy,

34
00:01:28,990 --> 00:01:31,324
intrusion into seclusion,

35
00:01:31,324 --> 00:01:33,340
appropriation of name or likeness,

36
00:01:33,340 --> 00:01:37,966
public disclosure of private facts, and false light.

37
00:01:37,966 --> 00:01:40,442
Each one of these addresses specific,

38
00:01:40,442 --> 00:01:42,005
different aspects of privacy,

39
00:01:42,005 --> 00:01:43,670
but what they have in common is

40
00:01:43,670 --> 00:01:46,160
that in each case we have to be able to

41
00:01:46,160 --> 00:01:49,160
identify that the individual who's alleging

42
00:01:49,160 --> 00:01:53,025
harm had a reasonable expectation of privacy.

43
00:01:53,025 --> 00:01:54,710
That's not always easy in

44
00:01:54,710 --> 00:01:56,630
this new technology but is

45
00:01:56,630 --> 00:01:59,150
definitely mandatory that we are able to show that.

46
00:01:59,150 --> 00:02:00,495
So intrusion into seclusion,

47
00:02:00,495 --> 00:02:03,470
the first one, basically is what it sounds like.

48
00:02:03,470 --> 00:02:04,790
You were intruding, someone has

49
00:02:04,790 --> 00:02:07,970
intruded into the seclusion of somebody else.

50
00:02:07,970 --> 00:02:08,990
Classic case,

51
00:02:08,990 --> 00:02:12,260
people in a women's dressing room and somebody is

52
00:02:12,260 --> 00:02:14,420
peering in unwantedly into

53
00:02:14,420 --> 00:02:17,060
this private area of the dressing room space.

54
00:02:17,060 --> 00:02:19,725
In the context of new technologies,

55
00:02:19,725 --> 00:02:20,990
some are arguing that

56
00:02:20,990 --> 00:02:23,120
the internet of things, in particular,

57
00:02:23,120 --> 00:02:27,620
this interconnected network of sensor data from

58
00:02:27,620 --> 00:02:29,720
consumer devices like Fitbits and

59
00:02:29,720 --> 00:02:32,630
smartphones and smart appliances and so forth,

60
00:02:32,630 --> 00:02:35,210
that the misuse of that information

61
00:02:35,210 --> 00:02:38,120
might result in an intrusion into seclusion.

62
00:02:38,120 --> 00:02:39,230
It's a tough argument

63
00:02:39,230 --> 00:02:42,380
because you have to show that there has been

64
00:02:42,380 --> 00:02:44,990
an unconsented use of it and you also have to

65
00:02:44,990 --> 00:02:48,350
show that it is highly offensive to a reasonable person.

66
00:02:48,350 --> 00:02:49,910
But we're going to revisit this

67
00:02:49,910 --> 00:02:51,710
in mod three when we look at some of

68
00:02:51,710 --> 00:02:54,140
the statutory protections as well in some

69
00:02:54,140 --> 00:02:56,635
of the cases related to the internet of things,

70
00:02:56,635 --> 00:03:00,240
particularly those that have been addressed by the FTC.

71
00:03:00,240 --> 00:03:02,180
The second one, appropriation

72
00:03:02,180 --> 00:03:03,770
of name or likeness or sometimes

73
00:03:03,770 --> 00:03:05,570
called misappropriation of name or

74
00:03:05,570 --> 00:03:07,760
likeness, protects our identity.

75
00:03:07,760 --> 00:03:09,500
So if somebody uses an aspect

76
00:03:09,500 --> 00:03:11,360
of our name, likeness, picture,

77
00:03:11,360 --> 00:03:12,710
identity to

78
00:03:12,710 --> 00:03:15,800
their commercial advantage without our permission,

79
00:03:15,800 --> 00:03:19,110
that is an appropriation and that's a privacy violation

80
00:03:19,110 --> 00:03:20,915
because we have an expectation

81
00:03:20,915 --> 00:03:23,430
of privacy in our identity.

82
00:03:23,430 --> 00:03:27,140
Classic case is somebody taking a picture of

83
00:03:27,140 --> 00:03:29,540
a famous person and putting it on T-shirts

84
00:03:29,540 --> 00:03:32,360
that they then sell to their commercial advantage.

85
00:03:32,360 --> 00:03:35,685
In the context of new technologies,

86
00:03:35,685 --> 00:03:39,170
there's a question of whether or not data brokers,

87
00:03:39,170 --> 00:03:42,140
other entities, organizations in

88
00:03:42,140 --> 00:03:45,680
this new space who are using parts of our identity,

89
00:03:45,680 --> 00:03:47,878
if that is an appropriation.

90
00:03:47,878 --> 00:03:50,555
If it has been unconsented to, it could be,

91
00:03:50,555 --> 00:03:52,490
but it's not clear because

92
00:03:52,490 --> 00:03:54,860
the commercial advantage piece has to be established.

93
00:03:54,860 --> 00:03:56,675
And again, we have to also show that there's

94
00:03:56,675 --> 00:04:00,155
a highly offensive misuse.

95
00:04:00,155 --> 00:04:02,780
The third one, public disclosure of private facts,

96
00:04:02,780 --> 00:04:05,810
occurs when somebody discloses

97
00:04:05,810 --> 00:04:07,850
private facts about another and

98
00:04:07,850 --> 00:04:09,985
those private facts are highly personal,

99
00:04:09,985 --> 00:04:11,615
the disclosure is public,

100
00:04:11,615 --> 00:04:14,225
and it is highly offensive to a reasonable person.

101
00:04:14,225 --> 00:04:17,960
So if somebody discloses to a community that

102
00:04:17,960 --> 00:04:20,750
their neighbor has a highly infectious disease

103
00:04:20,750 --> 00:04:22,115
without their consent,

104
00:04:22,115 --> 00:04:24,800
that might be considered the public disclosure of a

105
00:04:24,800 --> 00:04:28,455
private albeit true fact because it's offensive,

106
00:04:28,455 --> 00:04:29,510
it's a violation of

107
00:04:29,510 --> 00:04:32,310
that person's privacy and private life.

108
00:04:32,310 --> 00:04:34,160
In the new technology space,

109
00:04:34,160 --> 00:04:37,495
in data analytics, artificial intelligence,

110
00:04:37,495 --> 00:04:38,990
there can be aspects of this that

111
00:04:38,990 --> 00:04:40,580
happen as well where things

112
00:04:40,580 --> 00:04:44,570
about us that are true but private come out.

113
00:04:44,570 --> 00:04:47,000
Again, questionable whether or not in

114
00:04:47,000 --> 00:04:48,410
this age of sharing forward

115
00:04:48,410 --> 00:04:49,900
so much information about ourselves,

116
00:04:49,900 --> 00:04:51,620
we can argue that we're

117
00:04:51,620 --> 00:04:54,020
offended because we share so much,

118
00:04:54,020 --> 00:04:55,910
but at least some scholars are saying there's

119
00:04:55,910 --> 00:04:57,740
a possibility here that this kind

120
00:04:57,740 --> 00:05:01,670
of disclosure could be

121
00:05:01,670 --> 00:05:05,715
actionable in this new technology arena.

122
00:05:05,715 --> 00:05:07,550
The problem here is that the publicity

123
00:05:07,550 --> 00:05:08,685
piece is not clear.

124
00:05:08,685 --> 00:05:12,410
Publicity isn't necessarily equated

125
00:05:12,410 --> 00:05:14,690
to the trading of our information.

126
00:05:14,690 --> 00:05:16,880
So when data brokers are trading forward

127
00:05:16,880 --> 00:05:20,090
our information, is that publicity?

128
00:05:20,090 --> 00:05:21,929
Is it highly offensive?

129
00:05:21,929 --> 00:05:26,345
It's not entirely answerable at this point.

130
00:05:26,345 --> 00:05:27,530
The last one, false light,

131
00:05:27,530 --> 00:05:31,950
this occurs when a person is put in a false light

132
00:05:31,950 --> 00:05:35,090
either it has to be publicly but it has

133
00:05:35,090 --> 00:05:38,480
to essentially just a misalignment of who we are.

134
00:05:38,480 --> 00:05:39,680
So taking a picture of

135
00:05:39,680 --> 00:05:41,780
somebody and putting them under a headline in

136
00:05:41,780 --> 00:05:44,480
a newspaper that says something

137
00:05:44,480 --> 00:05:47,810
that is false would portray that person falsely.

138
00:05:47,810 --> 00:05:48,890
This one is really,

139
00:05:48,890 --> 00:05:52,545
really new in data analytics and artificial intelligence.

140
00:05:52,545 --> 00:05:53,975
We haven't seen a lot of it,

141
00:05:53,975 --> 00:05:55,520
but again it's possible that you

142
00:05:55,520 --> 00:05:58,020
could be falsely portrayed.

143
00:05:58,020 --> 00:06:01,310
Certainly you've seen cases where our data has resulted

144
00:06:01,310 --> 00:06:05,234
in false information and false conclusions about us,

145
00:06:05,234 --> 00:06:06,290
that we are falsely portrayed

146
00:06:06,290 --> 00:06:07,460
and that causes an emotional,

147
00:06:07,460 --> 00:06:09,530
physical or financial harm to us.

148
00:06:09,530 --> 00:06:12,025
Still, we have yet to see any of these cases.

149
00:06:12,025 --> 00:06:14,255
No data traders have been sued

150
00:06:14,255 --> 00:06:17,125
under US tort law to my knowledge.

151
00:06:17,125 --> 00:06:18,425
This is all speculation,

152
00:06:18,425 --> 00:06:21,590
but to the degree that we can stretch these towards a

153
00:06:21,590 --> 00:06:25,093
little bit more than we can stretch statutory law,

154
00:06:25,093 --> 00:06:26,300
there's argument that there is

155
00:06:26,300 --> 00:06:29,000
protection here for individuals.

