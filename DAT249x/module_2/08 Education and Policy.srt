0
00:00:00,050 --> 00:00:03,150
This is one of my favorite policy areas to

1
00:00:03,150 --> 00:00:05,955
explore because of my role as an academic.

2
00:00:05,955 --> 00:00:08,160
Universities hold an enormous amount of data

3
00:00:08,160 --> 00:00:10,650
about applicants and admitted students.

4
00:00:10,650 --> 00:00:11,985
From financial aid,

5
00:00:11,985 --> 00:00:14,520
recruitment, admissions, class performance,

6
00:00:14,520 --> 00:00:16,578
extracurricular engagement, etc.,

7
00:00:16,578 --> 00:00:19,985
students leave digital tracks all over the place.

8
00:00:19,985 --> 00:00:22,560
In many cases, universities are not doing nearly

9
00:00:22,560 --> 00:00:25,365
enough to share that information across divisions,

10
00:00:25,365 --> 00:00:26,640
and to use it in ways to

11
00:00:26,640 --> 00:00:28,670
improve the quality of education.

12
00:00:28,670 --> 00:00:30,575
But that's for another day.

13
00:00:30,575 --> 00:00:33,300
The opportunities to use big data in higher ed

14
00:00:33,300 --> 00:00:36,310
can either produce or prevent discrimination.

15
00:00:36,310 --> 00:00:38,130
The same technology that can help

16
00:00:38,130 --> 00:00:40,935
identify and serve students who need assistance,

17
00:00:40,935 --> 00:00:43,380
can also be used to deny them admission,

18
00:00:43,380 --> 00:00:45,180
or if already admitted,

19
00:00:45,180 --> 00:00:46,920
to discourage them from selecting

20
00:00:46,920 --> 00:00:49,380
a course of study that they are passionate about.

21
00:00:49,380 --> 00:00:52,050
For example, some universities are already

22
00:00:52,050 --> 00:00:55,695
using data points to map pathways to student's success.

23
00:00:55,695 --> 00:00:57,360
In some cases, this has improved

24
00:00:57,360 --> 00:00:59,630
retention and graduation rates.

25
00:00:59,630 --> 00:01:02,100
Universities can use analytics to try to predict

26
00:01:02,100 --> 00:01:03,300
a student's success in a

27
00:01:03,300 --> 00:01:06,030
particular major and course of study.

28
00:01:06,030 --> 00:01:09,125
The Family Educational Rights and Privacy Act, FERPA,

29
00:01:09,125 --> 00:01:12,135
applies to protect student information,

30
00:01:12,135 --> 00:01:13,260
but it has not been

31
00:01:13,260 --> 00:01:15,060
a roadblock to the use of analytics for

32
00:01:15,060 --> 00:01:18,840
universities wanting to apply analytics projects.

33
00:01:18,840 --> 00:01:22,440
The 2016 White House big data report does

34
00:01:22,440 --> 00:01:23,790
recommend that universities

35
00:01:23,790 --> 00:01:25,980
obtain Institutional Review Board,

36
00:01:25,980 --> 00:01:28,530
or IRB, approval as

37
00:01:28,530 --> 00:01:29,700
these projects do involve

38
00:01:29,700 --> 00:01:32,730
human subjects, the students themselves.

39
00:01:32,730 --> 00:01:34,290
Even if universities can ensure

40
00:01:34,290 --> 00:01:36,555
compliance with federal law for their projects,

41
00:01:36,555 --> 00:01:38,955
there is another important concern here,

42
00:01:38,955 --> 00:01:41,730
and that is student autonomy and the right to

43
00:01:41,730 --> 00:01:43,800
self-determine a path of study that is

44
00:01:43,800 --> 00:01:46,050
consistent with the student's passions,

45
00:01:46,050 --> 00:01:50,130
regardless of any algorithmic diagnostic or prediction.

46
00:01:50,130 --> 00:01:51,510
We might call this the right to

47
00:01:51,510 --> 00:01:55,380
autonomy privacy or the right to identity privacy.

48
00:01:55,380 --> 00:01:57,530
I'm just scratching at the surface of this here,

49
00:01:57,530 --> 00:02:00,420
but the concern is very real and ultimately involves

50
00:02:00,420 --> 00:02:01,500
the individual's right to

51
00:02:01,500 --> 00:02:04,470
self-determination and self-creation.

52
00:02:04,470 --> 00:02:07,050
We each have a right to create our identity.

53
00:02:07,050 --> 00:02:09,090
When institutions or organizations

54
00:02:09,090 --> 00:02:10,500
start messing with that right by

55
00:02:10,500 --> 00:02:12,870
relying solely on data analytics

56
00:02:12,870 --> 00:02:14,630
to direct student growth,

57
00:02:14,630 --> 00:02:17,655
regardless of any altruistic intention,

58
00:02:17,655 --> 00:02:21,020
they may be committing an act of disrespect.

59
00:02:21,020 --> 00:02:23,430
What more important foundational right is there than

60
00:02:23,430 --> 00:02:27,130
the right to create your very own definition of self?

