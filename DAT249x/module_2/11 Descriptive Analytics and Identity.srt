0
00:00:00,010 --> 00:00:02,919
>> By now, you have heard both terms;

1
00:00:02,919 --> 00:00:05,345
descriptive and prescriptive analytics.

2
00:00:05,345 --> 00:00:08,075
I want to focus now on descriptive analytics.

3
00:00:08,075 --> 00:00:09,950
Predictive and descriptive analytics

4
00:00:09,950 --> 00:00:11,300
are similar in that they

5
00:00:11,300 --> 00:00:14,339
both depend on massive sets of aggregated data,

6
00:00:14,339 --> 00:00:16,415
the difference is in what they do with it.

7
00:00:16,415 --> 00:00:18,020
Predictive analytics takes that

8
00:00:18,020 --> 00:00:19,520
aggregated data and then uses

9
00:00:19,520 --> 00:00:21,455
statistical models to forecast

10
00:00:21,455 --> 00:00:23,555
or predict future behavior.

11
00:00:23,555 --> 00:00:24,950
Descriptive analytics takes that

12
00:00:24,950 --> 00:00:27,515
aggregated data and then feeds it to machine learning

13
00:00:27,515 --> 00:00:29,660
algorithms to form a picture of

14
00:00:29,660 --> 00:00:31,010
the identity of the person or

15
00:00:31,010 --> 00:00:33,020
thing it is trying to describe.

16
00:00:33,020 --> 00:00:34,978
So one tries to describe the present or past,

17
00:00:34,978 --> 00:00:36,944
and one tries to predict the future,

18
00:00:36,944 --> 00:00:40,200
both based on massive amounts of aggregated data.

19
00:00:40,200 --> 00:00:42,250
We've already looked at predictive analytics,

20
00:00:42,250 --> 00:00:44,330
so what about descriptive analytics?

21
00:00:44,330 --> 00:00:46,970
I find it's sometimes helpful when talking about

22
00:00:46,970 --> 00:00:49,836
ethical issues to make them as personal as possible.

23
00:00:49,836 --> 00:00:50,970
So some questions that you would

24
00:00:50,970 --> 00:00:52,740
want to know about your own data.

25
00:00:52,740 --> 00:00:55,085
What data is being aggregated about me?

26
00:00:55,085 --> 00:00:57,925
And is it painting an accurate picture of who I am?

27
00:00:57,925 --> 00:01:00,575
And how is the data going to be used?

28
00:01:00,575 --> 00:01:03,660
The knee-jerk response many people have is to demand

29
00:01:03,660 --> 00:01:05,370
privacy and just insist that

30
00:01:05,370 --> 00:01:07,920
no one should have access to their data at all.

31
00:01:07,920 --> 00:01:09,570
We'll talk about that problem shortly,

32
00:01:09,570 --> 00:01:11,205
it's actually a pretty tricky one.

33
00:01:11,205 --> 00:01:12,660
But let's say this,

34
00:01:12,660 --> 00:01:16,080
at least if they're going to find out a lot about me,

35
00:01:16,080 --> 00:01:18,720
I hope whatever they conclude is accurate.

36
00:01:18,720 --> 00:01:21,630
This anxiety that is going in two directions at once is

37
00:01:21,630 --> 00:01:25,000
nicely described by Kate Crawford with an analogy.

38
00:01:25,000 --> 00:01:26,460
"We are worried that our data is

39
00:01:26,460 --> 00:01:28,140
like a harsh fluorescent light

40
00:01:28,140 --> 00:01:29,880
that shows both too much of us and

41
00:01:29,880 --> 00:01:31,725
not enough at the very same time,

42
00:01:31,725 --> 00:01:33,435
and therefore displays a false and

43
00:01:33,435 --> 00:01:35,930
unflattering picture of us."

44
00:01:35,930 --> 00:01:38,250
For one thing, many crumbs of our digital trail

45
00:01:38,250 --> 00:01:41,055
depend deeply on context for their meaning.

46
00:01:41,055 --> 00:01:42,420
For example, what if you posted

47
00:01:42,420 --> 00:01:43,970
a highly sarcastic comment on

48
00:01:43,970 --> 00:01:45,540
a social media site that got

49
00:01:45,540 --> 00:01:47,940
a quick laugh from your friends but might make you

50
00:01:47,940 --> 00:01:49,350
seem like a menacing person

51
00:01:49,350 --> 00:01:51,235
if an algorithm took it literally.

52
00:01:51,235 --> 00:01:52,560
I mean, do we know how to make

53
00:01:52,560 --> 00:01:55,535
algorithms that understand sarcasm?

54
00:01:55,535 --> 00:01:57,270
But what if we give the algorithm

55
00:01:57,270 --> 00:01:59,750
something a little less open to interpretation?

56
00:01:59,750 --> 00:02:02,610
In a highly publicized 2013 study

57
00:02:02,610 --> 00:02:04,800
conducted by the University of Cambridge,

58
00:02:04,800 --> 00:02:06,465
researchers were able to train

59
00:02:06,465 --> 00:02:08,691
algorithms to discern a user's race,

60
00:02:08,691 --> 00:02:09,930
gender, religion,

61
00:02:09,930 --> 00:02:11,400
political affiliation

62
00:02:11,400 --> 00:02:13,230
and other personal characteristics with

63
00:02:13,230 --> 00:02:15,570
a high degree of accuracy simply

64
00:02:15,570 --> 00:02:18,825
by correlating their likes on Facebook.

65
00:02:18,825 --> 00:02:20,310
That's kind of cool and has

66
00:02:20,310 --> 00:02:22,230
obvious uses for businesses looking

67
00:02:22,230 --> 00:02:23,610
to develop new products and

68
00:02:23,610 --> 00:02:26,330
politicians trying to craft their message.

69
00:02:26,330 --> 00:02:28,665
But when I say high degree of accuracy,

70
00:02:28,665 --> 00:02:30,750
I'm not talking about 100 percent,

71
00:02:30,750 --> 00:02:32,595
I'm talking about 90, 80,

72
00:02:32,595 --> 00:02:34,500
or 70 or worse,

73
00:02:34,500 --> 00:02:35,925
and that should give us pause

74
00:02:35,925 --> 00:02:37,410
because as a matter of fact,

75
00:02:37,410 --> 00:02:40,785
there are already many proprietary systems out there

76
00:02:40,785 --> 00:02:42,720
trying to figure out who the bad guys

77
00:02:42,720 --> 00:02:45,410
are by combing through these massive data sets.

78
00:02:45,410 --> 00:02:46,800
Now I'm all for cops catching

79
00:02:46,800 --> 00:02:48,570
bad guys, but you have to wonder.

80
00:02:48,570 --> 00:02:51,180
Is there some part of your digital profile that is being

81
00:02:51,180 --> 00:02:52,830
misinterpreted and putting you

82
00:02:52,830 --> 00:02:55,095
on some kind of bad guy list?

83
00:02:55,095 --> 00:02:57,460
In response to this, you might say that

84
00:02:57,460 --> 00:02:59,620
at least you should have a right to see

85
00:02:59,620 --> 00:03:01,300
what descriptive analytics thinks it

86
00:03:01,300 --> 00:03:03,430
knows about who you are and then you

87
00:03:03,430 --> 00:03:04,690
should have a chance to challenge

88
00:03:04,690 --> 00:03:07,745
those findings if you believe they are inaccurate.

89
00:03:07,745 --> 00:03:09,910
Actually, that's also complicated

90
00:03:09,910 --> 00:03:12,380
because let me tell you what the response will be.

91
00:03:12,380 --> 00:03:14,230
The users of descriptive analytics will

92
00:03:14,230 --> 00:03:16,285
say that access to your digital trail

93
00:03:16,285 --> 00:03:18,400
actually lets them know you

94
00:03:18,400 --> 00:03:20,915
better in some ways than you know yourself.

95
00:03:20,915 --> 00:03:22,240
So of course you know things

96
00:03:22,240 --> 00:03:23,650
about yourself such as religion and

97
00:03:23,650 --> 00:03:25,745
sexual orientation because you're

98
00:03:25,745 --> 00:03:28,120
the one who defines those things about yourself,

99
00:03:28,120 --> 00:03:29,825
but consider this example.

100
00:03:29,825 --> 00:03:31,105
Maybe you don't think of yourself

101
00:03:31,105 --> 00:03:32,885
as a materialistic person.

102
00:03:32,885 --> 00:03:34,390
In fact, you go on rants

103
00:03:34,390 --> 00:03:36,160
about the evils of materialism and people

104
00:03:36,160 --> 00:03:37,510
spending money on things they don't

105
00:03:37,510 --> 00:03:40,278
need and maybe even post a blog about it,

106
00:03:40,278 --> 00:03:41,980
but then maybe an algorithm can tell us

107
00:03:41,980 --> 00:03:43,870
how much time you spend online looking at

108
00:03:43,870 --> 00:03:45,190
luxury goods and what

109
00:03:45,190 --> 00:03:47,530
your past purchases are and discover that you

110
00:03:47,530 --> 00:03:50,020
actually are on the materialistic side

111
00:03:50,020 --> 00:03:51,455
and maybe that surprises you.

112
00:03:51,455 --> 00:03:55,000
Maybe you are much more materialistic than you realize.

113
00:03:55,000 --> 00:03:56,170
So, what users of

114
00:03:56,170 --> 00:03:58,390
proprietary descriptive analytic algorithms

115
00:03:58,390 --> 00:04:00,130
would say is that allowing you

116
00:04:00,130 --> 00:04:01,450
to go back and challenge

117
00:04:01,450 --> 00:04:02,620
the characteristics that they have

118
00:04:02,620 --> 00:04:05,510
discovered about you is going in the wrong direction.

119
00:04:05,510 --> 00:04:07,480
Descriptive analytics tells us things about

120
00:04:07,480 --> 00:04:10,300
yourself you might not know.

121
00:04:10,300 --> 00:04:13,815
So, do we have identity rights in this new world?

122
00:04:13,815 --> 00:04:15,370
Will governments have a standard of

123
00:04:15,370 --> 00:04:16,690
due process similar to the one

124
00:04:16,690 --> 00:04:18,355
they have for the physical world?

125
00:04:18,355 --> 00:04:20,050
As Eva has been pointing out,

126
00:04:20,050 --> 00:04:22,725
there is currently no laws governing this space.

127
00:04:22,725 --> 00:04:24,980
So, we'll have to see.

