0
00:00:00,080 --> 00:00:03,548
We're going to learn about Negligence Law in this video.

1
00:00:03,548 --> 00:00:06,385
Negligence law is state law,

2
00:00:06,385 --> 00:00:08,665
it is sourced in judge-made decisions,

3
00:00:08,665 --> 00:00:10,860
so it's common law and it is

4
00:00:10,860 --> 00:00:13,770
relevant to all kinds of behavior,

5
00:00:13,770 --> 00:00:17,105
including personal behavior and business behavior.

6
00:00:17,105 --> 00:00:18,690
The thing that is really interesting

7
00:00:18,690 --> 00:00:20,175
about negligence law,

8
00:00:20,175 --> 00:00:21,990
is that if you understand

9
00:00:21,990 --> 00:00:24,420
the basic elements of negligence,

10
00:00:24,420 --> 00:00:25,950
then you can see how it can be

11
00:00:25,950 --> 00:00:28,560
applied to any kind of behavior.

12
00:00:28,560 --> 00:00:30,390
And we'll see some examples of that,

13
00:00:30,390 --> 00:00:33,390
including behavior in data analytics and

14
00:00:33,390 --> 00:00:35,520
artificial intelligence and the management

15
00:00:35,520 --> 00:00:37,990
of data and use of that data.

16
00:00:37,990 --> 00:00:40,905
So the basic elements of any negligence claim,

17
00:00:40,905 --> 00:00:42,825
are these four: Duty,

18
00:00:42,825 --> 00:00:46,335
breach, causation and damages.

19
00:00:46,335 --> 00:00:48,855
The duty that we all have,

20
00:00:48,855 --> 00:00:50,760
is the duty to act as

21
00:00:50,760 --> 00:00:54,890
"reasonable person under the circumstances".

22
00:00:54,890 --> 00:00:58,035
That means that we are to act carefully,

23
00:00:58,035 --> 00:01:00,900
under the circumstances of whatever it is we're doing.

24
00:01:00,900 --> 00:01:03,705
For example, if you were driving an automobile,

25
00:01:03,705 --> 00:01:08,385
you are under a duty to drive that automobile reasonably,

26
00:01:08,385 --> 00:01:10,470
under the circumstances of the day that

27
00:01:10,470 --> 00:01:12,785
you're driving, which may change.

28
00:01:12,785 --> 00:01:15,445
So on a day that is clear and sunny,

29
00:01:15,445 --> 00:01:18,405
it might be reasonable to drive the speed limit.

30
00:01:18,405 --> 00:01:23,280
On a day when it is cloudy or raining or there's storms,

31
00:01:23,280 --> 00:01:26,580
then it might not be reasonable to drive the speed limit.

32
00:01:26,580 --> 00:01:28,680
So the circumstances change,

33
00:01:28,680 --> 00:01:30,720
and it is ultimately the courts that

34
00:01:30,720 --> 00:01:33,810
decide what these duties are and how they're defined.

35
00:01:33,810 --> 00:01:36,795
Which is why we read cases to understand

36
00:01:36,795 --> 00:01:40,640
the court's interpretations in the various context.

37
00:01:40,640 --> 00:01:43,830
So we always are under a duty to act carefully.

38
00:01:43,830 --> 00:01:45,450
We breach that duty,

39
00:01:45,450 --> 00:01:48,145
when we fail to act carefully.

40
00:01:48,145 --> 00:01:50,250
So if I'm driving a car and I'm

41
00:01:50,250 --> 00:01:52,950
driving excessively over the speed limit,

42
00:01:52,950 --> 00:01:54,930
on a day when it is cloudy and

43
00:01:54,930 --> 00:01:57,360
stormy and I cause an accident,

44
00:01:57,360 --> 00:02:00,625
then I have failed to live up to my duty.

45
00:02:00,625 --> 00:02:02,310
I have breached my duty to act as

46
00:02:02,310 --> 00:02:05,370
a reasonable person under the circumstances.

47
00:02:05,370 --> 00:02:07,530
Causation is the link between

48
00:02:07,530 --> 00:02:11,005
that breach and the damages that are caused.

49
00:02:11,005 --> 00:02:14,595
It's a factual inquiry and also a legal inquiry,

50
00:02:14,595 --> 00:02:16,200
but it's basically looking

51
00:02:16,200 --> 00:02:17,970
to see that there is some connection

52
00:02:17,970 --> 00:02:19,515
between the failure to act

53
00:02:19,515 --> 00:02:21,720
reasonably and the harm that was caused.

54
00:02:21,720 --> 00:02:24,690
In negligence law, negligence is tort law,

55
00:02:24,690 --> 00:02:27,930
which basically is law that protects us from emotional,

56
00:02:27,930 --> 00:02:30,180
physical and financial harm.

57
00:02:30,180 --> 00:02:31,575
The damages are that,

58
00:02:31,575 --> 00:02:32,610
they can be physical,

59
00:02:32,610 --> 00:02:35,340
they can be emotional and in a business context,

60
00:02:35,340 --> 00:02:37,675
they can be largely financial.

61
00:02:37,675 --> 00:02:39,300
So these are the four basic elements

62
00:02:39,300 --> 00:02:42,110
of any negligence claim.

63
00:02:42,110 --> 00:02:45,810
Now it's important to understand negligence because,

64
00:02:45,810 --> 00:02:49,035
these can come up as claims in different contexts,

65
00:02:49,035 --> 00:02:51,685
including negligent product design.

66
00:02:51,685 --> 00:02:55,230
So we might talk later about in the context of

67
00:02:55,230 --> 00:02:56,940
artificial intelligence and things

68
00:02:56,940 --> 00:03:00,630
like you know self-driving cars.

69
00:03:00,630 --> 00:03:01,975
Who designed that car,

70
00:03:01,975 --> 00:03:03,180
who designed that vehicle,

71
00:03:03,180 --> 00:03:05,860
was it designed carefully.

72
00:03:05,860 --> 00:03:08,250
Negligent product design can come up as a claim,

73
00:03:08,250 --> 00:03:09,600
when a product is not designed

74
00:03:09,600 --> 00:03:11,460
carefully and as a result it

75
00:03:11,460 --> 00:03:15,935
causes harm to the person using the product.

76
00:03:15,935 --> 00:03:17,595
You can be negligent in hiring.

77
00:03:17,595 --> 00:03:20,940
So if companies are not careful in hiring,

78
00:03:20,940 --> 00:03:23,715
then they can be accused of negligence,

79
00:03:23,715 --> 00:03:25,710
or careless hiring, in the event that

80
00:03:25,710 --> 00:03:29,100
the person hired actually causes harm to another.

81
00:03:29,100 --> 00:03:31,110
Negligent distribution, can be

82
00:03:31,110 --> 00:03:33,355
negligent distribution of products.

83
00:03:33,355 --> 00:03:35,670
If I'm a marketer and I'm distributing products,

84
00:03:35,670 --> 00:03:37,395
if I don't do so carefully,

85
00:03:37,395 --> 00:03:40,590
then I might cause harm and be responsible for that.

86
00:03:40,590 --> 00:03:43,680
We've seen these cases in allegations for example,

87
00:03:43,680 --> 00:03:47,970
against gun manufacturers who might be distributing guns,

88
00:03:47,970 --> 00:03:52,440
in areas where there is less regulation to their access,

89
00:03:52,440 --> 00:03:54,330
knowing that they might flood up into

90
00:03:54,330 --> 00:03:57,045
a black market and become more available,

91
00:03:57,045 --> 00:04:00,615
causing harm as a result of criminal activity,

92
00:04:00,615 --> 00:04:03,600
in those places where the regulations were stricter.

93
00:04:03,600 --> 00:04:06,630
So again we can be careless doing anything.

94
00:04:06,630 --> 00:04:09,165
Product design, hiring, distribution

95
00:04:09,165 --> 00:04:12,490
and relevant to our course content here,

96
00:04:12,490 --> 00:04:15,720
negligent data collection or management.

97
00:04:15,720 --> 00:04:17,400
There can be a claim that

98
00:04:17,400 --> 00:04:20,250
the data broker or that data user,

99
00:04:20,250 --> 00:04:23,040
is collecting information in a way that is not

100
00:04:23,040 --> 00:04:26,700
careful enough or that they're managing the information,

101
00:04:26,700 --> 00:04:27,990
sharing the information in

102
00:04:27,990 --> 00:04:29,700
a way that is not careful enough.

103
00:04:29,700 --> 00:04:31,185
So if we consider things like

104
00:04:31,185 --> 00:04:34,455
big data breaches, there's negligence there.

105
00:04:34,455 --> 00:04:36,750
Those are allegations of negligence as well as

106
00:04:36,750 --> 00:04:39,995
violations of other federal statutory law.

107
00:04:39,995 --> 00:04:42,225
But just in a carelessness realm,

108
00:04:42,225 --> 00:04:45,030
we need to be careful in how we collect, manage, use,

109
00:04:45,030 --> 00:04:48,120
share and so forth the data that we are accessing,

110
00:04:48,120 --> 00:04:51,370
in data analytics and artificial intelligence.

