0
00:00:00,050 --> 00:00:02,460
>> Kate Crawford, principal

1
00:00:02,460 --> 00:00:04,373
researcher with Microsoft Research,

2
00:00:04,373 --> 00:00:06,270
has stated that we are living right now in

3
00:00:06,270 --> 00:00:09,015
a great social experiment where the volume of data

4
00:00:09,015 --> 00:00:11,190
being accumulated about us is increasing to

5
00:00:11,190 --> 00:00:14,205
proportions beyond comprehension and awareness.

6
00:00:14,205 --> 00:00:15,870
Data sets are created,

7
00:00:15,870 --> 00:00:18,815
correlations are identified, and conclusions are made.

8
00:00:18,815 --> 00:00:20,370
Ms Crawford is concerned with what she

9
00:00:20,370 --> 00:00:23,100
calls "Big Data fundamentalism" or

10
00:00:23,100 --> 00:00:24,930
the belief that the conclusions made

11
00:00:24,930 --> 00:00:27,650
concerning data are objective truth.

12
00:00:27,650 --> 00:00:29,250
However, it's not obvious how

13
00:00:29,250 --> 00:00:31,373
all this data is being accumulated,

14
00:00:31,373 --> 00:00:33,355
managed and shared about us.

15
00:00:33,355 --> 00:00:34,570
There's no transparency.

16
00:00:34,570 --> 00:00:36,030
Meaning, we are not able to see

17
00:00:36,030 --> 00:00:38,160
how the data is collected and used.

18
00:00:38,160 --> 00:00:40,920
Facebook can determine based on our likes,

19
00:00:40,920 --> 00:00:43,320
what our race is, our sexual orientation,

20
00:00:43,320 --> 00:00:45,200
and possibly our addictions.

21
00:00:45,200 --> 00:00:47,340
But we are not aware how that information is

22
00:00:47,340 --> 00:00:50,060
cooked or who it is being shared with.

23
00:00:50,060 --> 00:00:52,200
There's a risk of bias against us based on

24
00:00:52,200 --> 00:00:54,900
a fundamentalist belief that data is truth.

25
00:00:54,900 --> 00:00:56,580
Based on data, we might be denied

26
00:00:56,580 --> 00:00:59,378
a loan or admission to a university,

27
00:00:59,378 --> 00:01:01,830
but we wouldn't know why because the decision is made by

28
00:01:01,830 --> 00:01:05,135
an algorithm or some form of artificial intelligence.

29
00:01:05,135 --> 00:01:07,650
So the initial premise here is that we have to get clear

30
00:01:07,650 --> 00:01:10,320
that we don't know what data is being collected,

31
00:01:10,320 --> 00:01:13,810
and how it is being used for good or for bad.

32
00:01:13,810 --> 00:01:15,960
This is an issue of transparency which

33
00:01:15,960 --> 00:01:18,715
we will continue to revisit in this course.

34
00:01:18,715 --> 00:01:20,130
Also, we don't know who is being

35
00:01:20,130 --> 00:01:21,938
included in the data gathered,

36
00:01:21,938 --> 00:01:23,820
and who is being excluded,

37
00:01:23,820 --> 00:01:25,410
and what harms might accrue to those

38
00:01:25,410 --> 00:01:28,120
left out of the big data revolution.

39
00:01:28,120 --> 00:01:29,970
Ms. Crawford uses the story of

40
00:01:29,970 --> 00:01:32,040
data collected during Hurricane Sandy to make

41
00:01:32,040 --> 00:01:33,900
a persuasive point that the data

42
00:01:33,900 --> 00:01:36,465
collected from consolidated tweets in Manhattan,

43
00:01:36,465 --> 00:01:38,460
did not reveal the objective truth of

44
00:01:38,460 --> 00:01:40,545
how people experienced the hurricane,

45
00:01:40,545 --> 00:01:42,300
mostly because masses of people in

46
00:01:42,300 --> 00:01:44,600
the heart of the hurricane were excluded.

47
00:01:44,600 --> 00:01:45,780
Their data was not part of

48
00:01:45,780 --> 00:01:47,700
the set because they were not nearly as

49
00:01:47,700 --> 00:01:48,810
connected to the Internet and

50
00:01:48,810 --> 00:01:51,740
social media as those in Manhattan.

51
00:01:51,740 --> 00:01:53,160
What we're talking about here is

52
00:01:53,160 --> 00:01:54,600
the bias that is created by

53
00:01:54,600 --> 00:01:56,100
virtue of the fact that we are not

54
00:01:56,100 --> 00:01:58,520
equally connected to technology.

55
00:01:58,520 --> 00:02:00,825
The truth that is established by data,

56
00:02:00,825 --> 00:02:02,473
therefore is really only the truth

57
00:02:02,473 --> 00:02:04,910
of those included in the data.

58
00:02:04,910 --> 00:02:06,900
Attorney Jonas Lerman distinguishes

59
00:02:06,900 --> 00:02:08,670
between those included and those

60
00:02:08,670 --> 00:02:10,680
executed by giving us examples of

61
00:02:10,680 --> 00:02:13,815
two hypothetical people living in the United States.

62
00:02:13,815 --> 00:02:15,960
Person A is a 30-year-old

63
00:02:15,960 --> 00:02:18,135
white collar resident of Manhattan.

64
00:02:18,135 --> 00:02:19,440
She has a smartphone,

65
00:02:19,440 --> 00:02:20,850
a Gmail account, Netflix,

66
00:02:20,850 --> 00:02:21,885
Spotify,

67
00:02:21,885 --> 00:02:23,310
she's a Facebook user who

68
00:02:23,310 --> 00:02:25,640
dates through Tinder and OKCupid.

69
00:02:25,640 --> 00:02:29,330
She has a GPS in her car and an easy pass for tolls.

70
00:02:29,330 --> 00:02:32,918
Person B, is a 40-year-old resident of Camden New Jersey,

71
00:02:32,918 --> 00:02:35,035
one of America's poorest cities.

72
00:02:35,035 --> 00:02:38,008
He's underemployed, working part time in a restaurant,

73
00:02:38,008 --> 00:02:39,745
paid cash under the table.

74
00:02:39,745 --> 00:02:40,800
He has no cell phone,

75
00:02:40,800 --> 00:02:42,345
no computer, no cable.

76
00:02:42,345 --> 00:02:44,440
He rarely travels and has no passport,

77
00:02:44,440 --> 00:02:46,105
no car or GPS.

78
00:02:46,105 --> 00:02:49,635
He uses the Internet at the library and he rides the bus.

79
00:02:49,635 --> 00:02:52,170
Lerman's concern is with person B,

80
00:02:52,170 --> 00:02:53,880
as a representative of all people

81
00:02:53,880 --> 00:02:56,580
excluded from the big data revolution.

82
00:02:56,580 --> 00:02:58,020
Those with a light or

83
00:02:58,020 --> 00:03:01,200
nonexistent data footprint may experience, one,

84
00:03:01,200 --> 00:03:03,270
tangible economic detriment because

85
00:03:03,270 --> 00:03:05,925
businesses will undervalue their preferences,

86
00:03:05,925 --> 00:03:08,340
and two, possible exclusion from

87
00:03:08,340 --> 00:03:10,020
civil and political life because

88
00:03:10,020 --> 00:03:12,565
they're just not counted or considered.

89
00:03:12,565 --> 00:03:15,110
In legal terms, what we're talking about here are

90
00:03:15,110 --> 00:03:17,540
principles of fairness and equality.

91
00:03:17,540 --> 00:03:20,585
The equal protection doctrine in the United States

92
00:03:20,585 --> 00:03:24,494
requires that we are all treated equally under the law.

93
00:03:24,494 --> 00:03:27,020
When we are treated differently on the basis of our race,

94
00:03:27,020 --> 00:03:28,600
ethnicity or gender, there's

95
00:03:28,600 --> 00:03:31,350
a violation of our rights to equal treatment.

96
00:03:31,350 --> 00:03:33,776
Lerman points out that equal protection doctrine however,

97
00:03:33,776 --> 00:03:35,030
does not protect us when we're

98
00:03:35,030 --> 00:03:36,620
treated differently on the basis of

99
00:03:36,620 --> 00:03:38,120
our economic worth or

100
00:03:38,120 --> 00:03:39,995
how often we connect to the Internet,

101
00:03:39,995 --> 00:03:42,440
and it does not protect us from actions taken by

102
00:03:42,440 --> 00:03:45,005
private organizations like corporations.

103
00:03:45,005 --> 00:03:46,430
It only protects us when

104
00:03:46,430 --> 00:03:48,710
the government treats us differently on the basis of

105
00:03:48,710 --> 00:03:50,360
our status in a protected class

106
00:03:50,360 --> 00:03:53,375
such as race, ethnicity and gender.

107
00:03:53,375 --> 00:03:55,730
If equal protection doctrine does not protect those

108
00:03:55,730 --> 00:03:58,708
excluded, what law does?

109
00:03:58,708 --> 00:04:02,115
For that matter, what law or law protects those included?

110
00:04:02,115 --> 00:04:04,565
Person A, and those she represents,

111
00:04:04,565 --> 00:04:05,990
from organisations making

112
00:04:05,990 --> 00:04:08,295
determinations about individual identity,

113
00:04:08,295 --> 00:04:10,573
preferences, abilities, and the like.

114
00:04:10,573 --> 00:04:12,200
This is a question for us to

115
00:04:12,200 --> 00:04:15,390
explore throughout this module and this course.

