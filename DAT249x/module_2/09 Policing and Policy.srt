0
00:00:00,000 --> 00:00:02,050
>> In this last video,

1
00:00:02,050 --> 00:00:03,100
we're going to be talking about

2
00:00:03,100 --> 00:00:06,205
criminal justice issues and predictive policing,

3
00:00:06,205 --> 00:00:08,830
which you know from your labs in

4
00:00:08,830 --> 00:00:10,630
Mod I and you'll see in

5
00:00:10,630 --> 00:00:13,615
future labs that this can raise some concerns,

6
00:00:13,615 --> 00:00:16,165
both in law and in ethics.

7
00:00:16,165 --> 00:00:18,130
Law enforcement agencies have

8
00:00:18,130 --> 00:00:20,170
long been trying to identify patterns in

9
00:00:20,170 --> 00:00:22,330
criminal activity in order to

10
00:00:22,330 --> 00:00:25,900
allocate resources more efficiently.

11
00:00:25,900 --> 00:00:27,640
Today, many police departments use

12
00:00:27,640 --> 00:00:29,410
sophisticated computer modeling

13
00:00:29,410 --> 00:00:32,350
to identify crime hotspots.

14
00:00:32,350 --> 00:00:34,150
One of the issues in this policy area

15
00:00:34,150 --> 00:00:35,830
is predictive policing,

16
00:00:35,830 --> 00:00:38,200
which is technically defined as

17
00:00:38,200 --> 00:00:41,200
"Directed, information based patrol;

18
00:00:41,200 --> 00:00:43,270
rapid response supported by

19
00:00:43,270 --> 00:00:46,045
fact-based prepositioning of assets;

20
00:00:46,045 --> 00:00:48,700
and proactive, intelligence-based tactics,

21
00:00:48,700 --> 00:00:51,215
strategy and policy."

22
00:00:51,215 --> 00:00:53,395
The data sets in criminal justice can be of

23
00:00:53,395 --> 00:00:55,240
poor quality and often are

24
00:00:55,240 --> 00:00:57,160
generated with subjective information,

25
00:00:57,160 --> 00:01:00,040
such as discretion of officers and prosecutors.

26
00:01:00,040 --> 00:01:02,830
To prevent discriminatory practices in this area,

27
00:01:02,830 --> 00:01:04,810
it is critical to eliminate data that

28
00:01:04,810 --> 00:01:07,315
serves as a proxy for race or poverty,

29
00:01:07,315 --> 00:01:09,370
to ensure historical bias is not

30
00:01:09,370 --> 00:01:12,270
replicated in the algorithmic process.

31
00:01:12,270 --> 00:01:14,680
This is essential to ensure that the relationship between

32
00:01:14,680 --> 00:01:16,435
criminal justice organizations

33
00:01:16,435 --> 00:01:19,120
and communities is healthy.

34
00:01:19,120 --> 00:01:22,360
The same issues, lack of knowledge about data used and

35
00:01:22,360 --> 00:01:25,720
lack of transparency in the technology are repeated here,

36
00:01:25,720 --> 00:01:27,520
and can, and do generate

37
00:01:27,520 --> 00:01:29,950
community distrust and allegations

38
00:01:29,950 --> 00:01:33,100
of discriminatory practices by police.

39
00:01:33,100 --> 00:01:35,875
Interestingly, data from body cameras

40
00:01:35,875 --> 00:01:37,750
removes the subjectivity of

41
00:01:37,750 --> 00:01:40,060
traditional data points used here.

42
00:01:40,060 --> 00:01:42,670
There is work being done to make body camera data more

43
00:01:42,670 --> 00:01:45,445
searchable and interoperable with other systems,

44
00:01:45,445 --> 00:01:47,635
and the White House Big Data Report makes a point of

45
00:01:47,635 --> 00:01:51,540
emphasizing the potential benefits of improvements here.

