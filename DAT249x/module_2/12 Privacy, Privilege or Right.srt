0
00:00:00,040 --> 00:00:02,900
>> The human right to privacy is quite

1
00:00:02,900 --> 00:00:05,595
simply the right to be let alone.

2
00:00:05,595 --> 00:00:08,340
There are two aspects to this right.

3
00:00:08,340 --> 00:00:11,645
Decisional privacy and informational privacy.

4
00:00:11,645 --> 00:00:14,000
Decisional privacy is the right to be let

5
00:00:14,000 --> 00:00:16,725
alone with respect to how we live our lives,

6
00:00:16,725 --> 00:00:18,125
who we partner with,

7
00:00:18,125 --> 00:00:19,655
how we raise our children,

8
00:00:19,655 --> 00:00:23,225
what medical treatment we accept and reject.

9
00:00:23,225 --> 00:00:26,360
This right is most affected by government actions that

10
00:00:26,360 --> 00:00:28,550
attempt to restrict the full expression

11
00:00:28,550 --> 00:00:30,350
of privacy by citizens.

12
00:00:30,350 --> 00:00:32,660
For example, attempts by

13
00:00:32,660 --> 00:00:34,730
states in the United States to restrict

14
00:00:34,730 --> 00:00:36,410
the right to reproductive freedom for

15
00:00:36,410 --> 00:00:39,550
women raise the issue of the right to privacy.

16
00:00:39,550 --> 00:00:40,850
And because these cases

17
00:00:40,850 --> 00:00:42,650
are about government restrictions,

18
00:00:42,650 --> 00:00:44,840
they concern the right to privacy as it

19
00:00:44,840 --> 00:00:47,525
is protected by the US Constitution.

20
00:00:47,525 --> 00:00:49,940
Informational privacy is the right

21
00:00:49,940 --> 00:00:51,350
to be let alone with respect to

22
00:00:51,350 --> 00:00:55,700
information or data that reveals knowledge about us.

23
00:00:55,700 --> 00:00:57,350
Sensitive information such as

24
00:00:57,350 --> 00:01:00,470
Social Security numbers, race, gender, religion,

25
00:01:00,470 --> 00:01:04,310
health data including fitness levels, genetic data,

26
00:01:04,310 --> 00:01:08,000
all of these information reveals a part of who we are.

27
00:01:08,000 --> 00:01:10,730
Outside of constitutional protections against

28
00:01:10,730 --> 00:01:13,795
government actions that restrict our privacy,

29
00:01:13,795 --> 00:01:16,115
most of the law that protects privacy,

30
00:01:16,115 --> 00:01:18,830
particularly informational privacy,

31
00:01:18,830 --> 00:01:22,140
is protected by sector in the United States.

32
00:01:22,140 --> 00:01:24,980
There is not one overarching rule that otherwise

33
00:01:24,980 --> 00:01:26,420
protects individuals from

34
00:01:26,420 --> 00:01:28,220
having their information accessed,

35
00:01:28,220 --> 00:01:30,770
used, or shared without their consent.

36
00:01:30,770 --> 00:01:34,365
Instead, the statutes protect sectors of information.

37
00:01:34,365 --> 00:01:37,370
And so there are laws that protect our health data,

38
00:01:37,370 --> 00:01:41,735
our financial data, student data, credit data,

39
00:01:41,735 --> 00:01:44,690
and even our genetic data from being accessed,

40
00:01:44,690 --> 00:01:47,925
used, and shared without our permission.

41
00:01:47,925 --> 00:01:49,700
The limitation of these laws is

42
00:01:49,700 --> 00:01:51,560
that they are very specific.

43
00:01:51,560 --> 00:01:54,830
For example, the Genetic Information Privacy Act,

44
00:01:54,830 --> 00:01:56,021
otherwise known as GINA,

45
00:01:56,021 --> 00:01:57,995
protects against access, use,

46
00:01:57,995 --> 00:01:59,960
and sharing genetic information

47
00:01:59,960 --> 00:02:02,795
by employers and insurance companies.

48
00:02:02,795 --> 00:02:04,970
In the context of new technologies

49
00:02:04,970 --> 00:02:07,275
such as data analytics and AI,

50
00:02:07,275 --> 00:02:08,870
these existing laws will need to get

51
00:02:08,870 --> 00:02:11,530
stretched in our application.

52
00:02:11,530 --> 00:02:13,970
Finally, in the US there is common law that

53
00:02:13,970 --> 00:02:17,025
protects privacy in an area of law called torts.

54
00:02:17,025 --> 00:02:18,410
We've already seen torts in

55
00:02:18,410 --> 00:02:20,585
our discussion of negligence law.

56
00:02:20,585 --> 00:02:23,750
Tort law, intentional tort law protects

57
00:02:23,750 --> 00:02:25,400
individuals from wrongs committed

58
00:02:25,400 --> 00:02:26,930
by another that caused physical,

59
00:02:26,930 --> 00:02:28,895
financial, and emotional harm.

60
00:02:28,895 --> 00:02:31,340
When a person or organization intrudes into

61
00:02:31,340 --> 00:02:33,500
areas where we have a reason to

62
00:02:33,500 --> 00:02:35,960
expect privacy or accesses

63
00:02:35,960 --> 00:02:37,490
and shares personal information

64
00:02:37,490 --> 00:02:40,350
that we expect to be able to keep personal,

65
00:02:40,350 --> 00:02:42,050
then there may be a civil lawsuit

66
00:02:42,050 --> 00:02:45,075
claiming damages for that harm.

67
00:02:45,075 --> 00:02:47,120
All they said about privacy law in the US,

68
00:02:47,120 --> 00:02:50,810
the current story of US privacy law may be that

69
00:02:50,810 --> 00:02:52,700
at least Americans consider to be

70
00:02:52,700 --> 00:02:55,810
more of a privilege than a right.

71
00:02:55,810 --> 00:02:57,080
The willingness of individuals in

72
00:02:57,080 --> 00:02:58,700
the United States to share for

73
00:02:58,700 --> 00:03:01,855
their information in transactional exchanges online,

74
00:03:01,855 --> 00:03:03,620
in fitness challenges at work,

75
00:03:03,620 --> 00:03:05,585
and on social media like Facebook,

76
00:03:05,585 --> 00:03:07,910
Twitter, snapchat and dating sites,

77
00:03:07,910 --> 00:03:10,370
suggests that there's an evolution of how we consider

78
00:03:10,370 --> 00:03:13,990
information that is intimately personal to us.

79
00:03:13,990 --> 00:03:15,890
What happens, however, when we come to

80
00:03:15,890 --> 00:03:18,080
learn that all of this personal data is being

81
00:03:18,080 --> 00:03:21,020
accumulated to create web privacy scholars

82
00:03:21,020 --> 00:03:22,790
like Daniel Solove call

83
00:03:22,790 --> 00:03:26,010
digital biographies about us and all of

84
00:03:26,010 --> 00:03:27,800
a sudden technologies are having a hand in

85
00:03:27,800 --> 00:03:30,182
defining the very persons that we are,

86
00:03:30,182 --> 00:03:33,470
because we are so willing to share that information,

87
00:03:33,470 --> 00:03:35,360
it becomes harder to argue that we have

88
00:03:35,360 --> 00:03:39,000
any expectation of privacy in the data at all.

