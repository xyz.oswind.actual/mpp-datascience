0
00:00:00,010 --> 00:00:02,080
In the previous video,

1
00:00:02,080 --> 00:00:05,110
we looked at one way that bias can enter the

2
00:00:05,110 --> 00:00:07,360
subconscious of machines if you will and that

3
00:00:07,360 --> 00:00:10,355
was through historically biased data.

4
00:00:10,355 --> 00:00:13,150
Now this is true on a second level.

5
00:00:13,150 --> 00:00:15,010
Not only are machines bound to learn

6
00:00:15,010 --> 00:00:17,005
biased by biased historical data,

7
00:00:17,005 --> 00:00:19,210
but they may also develop biased by paying

8
00:00:19,210 --> 00:00:22,030
attention to the irrelevant data.

9
00:00:22,030 --> 00:00:24,835
In module three, we will name this as the problem of

10
00:00:24,835 --> 00:00:28,690
irrelevant proxies and examine the issue in more detail.

11
00:00:28,690 --> 00:00:31,650
For now, let's think about how an algorithm is created.

12
00:00:31,650 --> 00:00:34,260
Well, it necessarily needs input of data.

13
00:00:34,260 --> 00:00:36,505
And as we saw in the module one lab,

14
00:00:36,505 --> 00:00:39,115
you can call these variables.

15
00:00:39,115 --> 00:00:41,365
But of course the machine algorithm does not use

16
00:00:41,365 --> 00:00:45,440
all the variables that ever were or could be collected.

17
00:00:45,440 --> 00:00:48,505
It only uses the variables that we decide to feed it,

18
00:00:48,505 --> 00:00:50,290
which is a step of the process

19
00:00:50,290 --> 00:00:52,825
sometimes called feature selection.

20
00:00:52,825 --> 00:00:54,910
And remember, the ones who decide which

21
00:00:54,910 --> 00:00:57,395
features to select are human beings.

22
00:00:57,395 --> 00:00:59,230
The same as you and me with all of

23
00:00:59,230 --> 00:01:02,215
our human temptation and biases.

24
00:01:02,215 --> 00:01:04,570
So if you tell the machine to incorporate the data

25
00:01:04,570 --> 00:01:07,680
relating to zip code, it will.

26
00:01:07,680 --> 00:01:10,030
But if zip code turns out to be

27
00:01:10,030 --> 00:01:12,640
a proxy for race as it often is,

28
00:01:12,640 --> 00:01:14,440
you have just selected a feature

29
00:01:14,440 --> 00:01:16,300
that is going to make the machine's decision

30
00:01:16,300 --> 00:01:20,875
making process more biased and perhaps, even just racist.

31
00:01:20,875 --> 00:01:22,240
It makes no difference that you

32
00:01:22,240 --> 00:01:24,955
intentionally introduced race or not.

33
00:01:24,955 --> 00:01:27,955
The disparate impact is the same.

34
00:01:27,955 --> 00:01:30,520
And it is important to note that the problem of

35
00:01:30,520 --> 00:01:33,340
feature selection and the problem of bias in data,

36
00:01:33,340 --> 00:01:34,900
introduces bias in machines in

37
00:01:34,900 --> 00:01:37,135
ways that are often invisible.

38
00:01:37,135 --> 00:01:39,364
And when the biases are invisible,

39
00:01:39,364 --> 00:01:41,710
they have the potential to be much more harmful,

40
00:01:41,710 --> 00:01:43,225
in exactly the same way that

41
00:01:43,225 --> 00:01:46,025
unconscious bias of humans can be.

42
00:01:46,025 --> 00:01:48,840
We all agree that conscious bias is bad,

43
00:01:48,840 --> 00:01:52,635
but at least, it's more likely to be easy to spot.

44
00:01:52,635 --> 00:01:55,990
And it's much easier to fight an enemy that can be seen.

45
00:01:55,990 --> 00:01:57,797
If this makes you disappointed,

46
00:01:57,797 --> 00:02:00,835
that's an understandable response because sometimes,

47
00:02:00,835 --> 00:02:04,300
AI system such as predictive algorithms are sold

48
00:02:04,300 --> 00:02:06,190
specifically on the promise that they will

49
00:02:06,190 --> 00:02:09,520
remove human bias from the decision making process.

50
00:02:09,520 --> 00:02:12,070
A perfect example is the recidivism algorithm

51
00:02:12,070 --> 00:02:13,475
that we are working on in the labs

52
00:02:13,475 --> 00:02:15,430
at the end of each module.

53
00:02:15,430 --> 00:02:18,040
Inevitably, you will hear someone say something like,

54
00:02:18,040 --> 00:02:20,350
"See how great this predictive algorithm is,

55
00:02:20,350 --> 00:02:22,360
now we don't have to worry about a jury or

56
00:02:22,360 --> 00:02:24,875
lawyers or a judge with bias in their hearts.

57
00:02:24,875 --> 00:02:27,670
An objective machine will do the job for us."

58
00:02:27,670 --> 00:02:30,400
And while it's certainly true that algorithms don't

59
00:02:30,400 --> 00:02:32,900
have the same temptations as humans,

60
00:02:32,900 --> 00:02:34,810
it is very important to realize that we have

61
00:02:34,810 --> 00:02:37,240
not solved the problem of bias.

62
00:02:37,240 --> 00:02:39,820
A much better way to think about this is that we have

63
00:02:39,820 --> 00:02:43,570
substituted one kind of bias problem for another.

64
00:02:43,570 --> 00:02:45,940
And even when or if it turns out that

65
00:02:45,940 --> 00:02:48,520
machines are less bias or even when

66
00:02:48,520 --> 00:02:50,935
and if it turns out to be true that it is

67
00:02:50,935 --> 00:02:53,700
easier to fix machine bias than human bias,

68
00:02:53,700 --> 00:02:56,110
the fight against bias must continue.

69
00:02:56,110 --> 00:02:58,970
It just has a new level of complexity.

