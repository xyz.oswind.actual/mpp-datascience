Personal Data and Privacy
================

When we think of data values that typically comprise “personally
identifiable information”, or PII, the easiest ones to recall are
probably financial in nature. Our credit card numbers aren’t something
that we’d want to broadcast for fear of it being used by someone else to
make a purchase.

However, as we’ve heard so far in our course, PII actually covers a
range of values, such as Social Security Numbers, phone numbers, and
more. Name, age, gender, race, Date of Birth, biometrics, address,
immunization records are all typically included in this PII list, and
are data values we want to keep locked down. Today, personal data can
also include many other data types, including your fingerprint or images
of your face, social media posts and more.

Some questions to ask yourself when you find or have PII in your data
set:

  - What are your company’s needs around using these PII values
  - Who needs access to this data
  - Can you lock down access to those who need it, or is that not
    possible with your system

In order to lock these values down, companies use multiple methods to
protect information. Let’s learn a few of these in the next section of
the course.
