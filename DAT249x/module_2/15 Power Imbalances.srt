0
00:00:00,000 --> 00:00:04,045
If you look at the way the word democracy is constructed,

1
00:00:04,045 --> 00:00:05,710
it comes from two Greek words:

2
00:00:05,710 --> 00:00:07,795
one being "people" (demos),

3
00:00:07,795 --> 00:00:10,610
and the other being "power" (kratos).

4
00:00:10,610 --> 00:00:12,385
So in a democracy theoretically,

5
00:00:12,385 --> 00:00:15,020
the common people are the ones with the ultimate power.

6
00:00:15,020 --> 00:00:18,595
In business, corporations are legal creations.

7
00:00:18,595 --> 00:00:20,680
That is, they came into existence only because

8
00:00:20,680 --> 00:00:23,405
the government decided to allow them to exist.

9
00:00:23,405 --> 00:00:25,900
So in the abstract, people have power over

10
00:00:25,900 --> 00:00:29,090
government and government has power over business.

11
00:00:29,090 --> 00:00:31,270
Now I think most of us know that's an ideal.

12
00:00:31,270 --> 00:00:33,430
Even in a fairly healthy democracy,

13
00:00:33,430 --> 00:00:34,900
it's not going to be difficult to

14
00:00:34,900 --> 00:00:36,820
find examples when the government

15
00:00:36,820 --> 00:00:38,425
has an unhealthy amount of power

16
00:00:38,425 --> 00:00:40,855
over a particular individual or group.

17
00:00:40,855 --> 00:00:42,490
It will be equally easy to find

18
00:00:42,490 --> 00:00:44,500
examples where the government is unable to

19
00:00:44,500 --> 00:00:46,570
control a particular business even when

20
00:00:46,570 --> 00:00:48,795
the business is doing something unethical,

21
00:00:48,795 --> 00:00:50,110
even though the government might

22
00:00:50,110 --> 00:00:52,060
be a generally a well-functioning one.

23
00:00:52,060 --> 00:00:54,305
Part of the reason is because when I refer to

24
00:00:54,305 --> 00:00:57,520
power I don't mean complete control.

25
00:00:57,520 --> 00:00:59,925
Rather, I mean something like "final authority",

26
00:00:59,925 --> 00:01:01,510
that people have the right to change

27
00:01:01,510 --> 00:01:03,055
their government if they don't like it,

28
00:01:03,055 --> 00:01:04,180
and the government has the right

29
00:01:04,180 --> 00:01:05,830
to regulate business activity,

30
00:01:05,830 --> 00:01:08,265
and tax it in the interests of the people.

31
00:01:08,265 --> 00:01:09,490
So we need to fine tune

32
00:01:09,490 --> 00:01:11,500
our understanding of power in this sense.

33
00:01:11,500 --> 00:01:14,560
It means final authority not complete control.

34
00:01:14,560 --> 00:01:16,810
And I believe that the record of history shows that

35
00:01:16,810 --> 00:01:19,000
the healthiest societies are the ones where

36
00:01:19,000 --> 00:01:20,380
the common people have final

37
00:01:20,380 --> 00:01:21,580
authority over the government,

38
00:01:21,580 --> 00:01:23,380
and the law gives government

39
00:01:23,380 --> 00:01:26,310
final authority over business organizations.

40
00:01:26,310 --> 00:01:28,255
But something is happening in

41
00:01:28,255 --> 00:01:29,830
only the last few years that

42
00:01:29,830 --> 00:01:32,050
has totally changed this balance.

43
00:01:32,050 --> 00:01:34,920
There is a saying that "knowledge is power".

44
00:01:34,920 --> 00:01:37,690
It seems that this saying rings true on many levels,

45
00:01:37,690 --> 00:01:40,000
but perhaps it's time to revise this

46
00:01:40,000 --> 00:01:43,005
generally true saying by making it more specific.

47
00:01:43,005 --> 00:01:45,010
Yes, knowledge is power but these

48
00:01:45,010 --> 00:01:47,575
days you get knowledge from data,

49
00:01:47,575 --> 00:01:49,090
and so maybe a more to the point

50
00:01:49,090 --> 00:01:51,770
saying is "data is power".

51
00:01:51,770 --> 00:01:53,080
Now that should make us think,

52
00:01:53,080 --> 00:01:54,850
because the time tested formula,

53
00:01:54,850 --> 00:01:56,455
people have authority over government,

54
00:01:56,455 --> 00:01:59,940
government via law, has authority over businesses,

55
00:01:59,940 --> 00:02:01,450
that doesn't seem to fit.

56
00:02:01,450 --> 00:02:04,555
Because currently businesses are the ones with the data,

57
00:02:04,555 --> 00:02:06,400
and government can pretty easily get

58
00:02:06,400 --> 00:02:08,410
data on its citizens but it usually has to

59
00:02:08,410 --> 00:02:09,580
ask the businesses to

60
00:02:09,580 --> 00:02:13,390
cooperate by giving away their data.

61
00:02:13,390 --> 00:02:15,400
So it seems that the government has a level of power

62
00:02:15,400 --> 00:02:17,665
over its citizens that it never had before,

63
00:02:17,665 --> 00:02:19,775
because it suddenly has a ton of our data

64
00:02:19,775 --> 00:02:22,290
but we have very little data on the government.

65
00:02:22,290 --> 00:02:23,380
There are small examples of

66
00:02:23,380 --> 00:02:24,865
citizens getting government data,

67
00:02:24,865 --> 00:02:28,045
such as the Freedom of Information Act or FOIA,

68
00:02:28,045 --> 00:02:30,850
where citizens can request certain data from

69
00:02:30,850 --> 00:02:32,350
government and the government is

70
00:02:32,350 --> 00:02:34,345
legally obligated to give it over.

71
00:02:34,345 --> 00:02:36,590
In fact, this is how the recidivism data

72
00:02:36,590 --> 00:02:39,560
set that we are using in our labs was obtained.

73
00:02:39,560 --> 00:02:41,720
But these examples are few and far between.

74
00:02:41,720 --> 00:02:43,240
It's mostly the case that government has

75
00:02:43,240 --> 00:02:46,060
our data and we don't have the government data.

76
00:02:46,060 --> 00:02:47,800
And businesses have a lot of

77
00:02:47,800 --> 00:02:49,935
data that the government would like to have.

78
00:02:49,935 --> 00:02:51,310
It sometimes tries to force

79
00:02:51,310 --> 00:02:52,715
business to hand over that data,

80
00:02:52,715 --> 00:02:54,945
but business does not always comply.

81
00:02:54,945 --> 00:02:56,560
So if you think about it, we are now

82
00:02:56,560 --> 00:02:58,450
entering a new era of history,

83
00:02:58,450 --> 00:03:00,100
where business has a kind of power

84
00:03:00,100 --> 00:03:02,020
over the government that it never had,

85
00:03:02,020 --> 00:03:04,660
and government has a kind of power over its citizens,

86
00:03:04,660 --> 00:03:05,920
at least to a degree,

87
00:03:05,920 --> 00:03:08,015
that it never had before.

88
00:03:08,015 --> 00:03:09,340
It would be one thing if this were

89
00:03:09,340 --> 00:03:12,400
a slow evolution that we all had time to adjust to,

90
00:03:12,400 --> 00:03:14,380
but this is all happening at a speed that

91
00:03:14,380 --> 00:03:16,491
we can't really even conceptualize.

92
00:03:16,491 --> 00:03:19,480
It is a revolution in knowledge distribution and it's

93
00:03:19,480 --> 00:03:20,890
important that we understand and

94
00:03:20,890 --> 00:03:23,480
talk about this power shift.

