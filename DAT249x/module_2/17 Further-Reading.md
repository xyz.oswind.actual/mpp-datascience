Further Reading
================

“Big Data: A Tool for Inclusion or Exclusion? Understanding the Issues”
(FTC Report January 2016) U.S. Federal Trade Commission.
<https://www.ftc.gov/system/files/documents/reports/big-data-tool-inclusion-or-exclusion-understanding-issues/160106big-data-rpt.pdf>

“Big Data: A Report on Algorithmic Systems, Opportunity, and Civil
Rights” (Executive Office of the President May 2016) U.S. Executive
Office.
<https://obamawhitehouse.archives.gov/sites/default/files/microsites/ostp/2016_0504_data_discrimination.pdf>

More on encryption:
<https://www.howtogeek.com/howto/33949/htg-explains-what-is-encryption-and-how-does-it-work/>

More on differential privacy:
<https://privacytools.seas.harvard.edu/differential-privacy>
