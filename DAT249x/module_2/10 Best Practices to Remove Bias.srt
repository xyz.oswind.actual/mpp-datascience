0
00:00:00,000 --> 00:00:02,840
In this video, we're going to look at some

1
00:00:02,840 --> 00:00:04,760
of the best practices suggested from

2
00:00:04,760 --> 00:00:06,590
the White House big data report of

3
00:00:06,590 --> 00:00:12,915
2016 to eliminate bias and to prevent discrimination.

4
00:00:12,915 --> 00:00:15,035
So this quote is directly from

5
00:00:15,035 --> 00:00:17,570
the report itself, "Moving forward,

6
00:00:17,570 --> 00:00:20,000
it is essential that the public and private sectors

7
00:00:20,000 --> 00:00:23,330
continue to have collaborative conversations about how

8
00:00:23,330 --> 00:00:25,290
to achieve the most out of

9
00:00:25,290 --> 00:00:26,961
the big data technologies while

10
00:00:26,961 --> 00:00:29,375
deliberatively applying these tools to avoid.

11
00:00:29,375 --> 00:00:33,468
And when appropriate, address discrimination."

12
00:00:33,468 --> 00:00:36,790
Best practice one, the White House suggests supporting

13
00:00:36,790 --> 00:00:40,330
research into mitigating algorithmic discrimination,

14
00:00:40,330 --> 00:00:41,530
building systems that

15
00:00:41,530 --> 00:00:43,345
support fairness and accountability,

16
00:00:43,345 --> 00:00:46,460
and developing strong data ethics frameworks.

17
00:00:46,460 --> 00:00:49,360
So you see, the usefulness of this course is even

18
00:00:49,360 --> 00:00:52,635
more obvious in this first best practice.

19
00:00:52,635 --> 00:00:54,580
We're learning about ethics and

20
00:00:54,580 --> 00:00:59,110
legal frameworks to prevent discrimination and bias.

21
00:00:59,110 --> 00:01:02,005
This learning is essential

22
00:01:02,005 --> 00:01:04,330
not only because it's suggested as

23
00:01:04,330 --> 00:01:07,360
a best practice but because we will see as the law

24
00:01:07,360 --> 00:01:10,608
does eventually catch up to the technology,

25
00:01:10,608 --> 00:01:12,520
those of us who are prepared and

26
00:01:12,520 --> 00:01:15,460
understand ethics and legal frameworks

27
00:01:15,460 --> 00:01:17,860
will be in a strategic position to do

28
00:01:17,860 --> 00:01:20,583
better in this industry.

29
00:01:20,583 --> 00:01:22,960
The second best practice suggested by

30
00:01:22,960 --> 00:01:25,750
the White House is to encourage market participants

31
00:01:25,750 --> 00:01:28,210
to design the best systems including

32
00:01:28,210 --> 00:01:31,345
transparency and accountability mechanisms

33
00:01:31,345 --> 00:01:33,685
such as the ability for subjects to correct

34
00:01:33,685 --> 00:01:35,290
inaccurate data and to

35
00:01:35,290 --> 00:01:38,015
appeal algorithmic based decisions.

36
00:01:38,015 --> 00:01:40,990
So this practice is coming out of what

37
00:01:40,990 --> 00:01:43,960
we know to be issues of the lack of transparency,

38
00:01:43,960 --> 00:01:45,370
the lack of knowing about

39
00:01:45,370 --> 00:01:47,485
what's happening behind the scenes.

40
00:01:47,485 --> 00:01:51,490
And it's meant to encourage those that are in

41
00:01:51,490 --> 00:01:53,710
the industry to not wait for

42
00:01:53,710 --> 00:01:56,855
legal regulatory reform to be enforced upon them.

43
00:01:56,855 --> 00:01:59,530
But to take action and to use

44
00:01:59,530 --> 00:02:03,430
design practices to address the problems.

45
00:02:03,430 --> 00:02:05,650
The third practice, promoting

46
00:02:05,650 --> 00:02:08,050
academic research and industry development of

47
00:02:08,050 --> 00:02:10,790
algorithm auditing and external testing of

48
00:02:10,790 --> 00:02:12,220
big data systems to ensure

49
00:02:12,220 --> 00:02:13,780
that people are being treated fairly.

50
00:02:13,780 --> 00:02:14,860
This is a little bit like the

51
00:02:14,860 --> 00:02:16,390
second big practice and I would say

52
00:02:16,390 --> 00:02:19,300
that Nathan has addressed both

53
00:02:19,300 --> 00:02:20,560
of these practices in

54
00:02:20,560 --> 00:02:23,260
his videos and his discussions about

55
00:02:23,260 --> 00:02:25,810
design systems and the importance of

56
00:02:25,810 --> 00:02:29,851
continually researching and developing in this area.

57
00:02:29,851 --> 00:02:31,430
The fourth one, broaden

58
00:02:31,430 --> 00:02:34,190
participation in computer science and data science,

59
00:02:34,190 --> 00:02:35,945
including opportunities to improve

60
00:02:35,945 --> 00:02:39,535
basic fluencies and the capabilities of all Americans.

61
00:02:39,535 --> 00:02:40,875
Super important here.

62
00:02:40,875 --> 00:02:44,285
So many people are just not really aware at all

63
00:02:44,285 --> 00:02:48,440
about what is happening in the big data revolution.

64
00:02:48,440 --> 00:02:49,855
That this data is being collected,

65
00:02:49,855 --> 00:02:50,878
that it's being used,

66
00:02:50,878 --> 00:02:53,730
that there's possibilities of bias and discrimination,

67
00:02:53,730 --> 00:02:55,612
they don't know to advocate for themselves,

68
00:02:55,612 --> 00:02:57,715
they don't understand the science.

69
00:02:57,715 --> 00:02:59,970
So I would say that those of you participating in

70
00:02:59,970 --> 00:03:02,900
this course today are ahead of the game.

71
00:03:02,900 --> 00:03:04,760
That you're taking and of course has been developed

72
00:03:04,760 --> 00:03:06,740
by Microsoft to actually

73
00:03:06,740 --> 00:03:09,320
broaden the participation in computer science and

74
00:03:09,320 --> 00:03:12,095
data science so that we're learning about this,

75
00:03:12,095 --> 00:03:14,325
so that we can become better professionals.

76
00:03:14,325 --> 00:03:16,430
Universities like Seattle University are

77
00:03:16,430 --> 00:03:18,975
also taking a lead in this by designing

78
00:03:18,975 --> 00:03:21,370
graduate programs and undergraduate programs

79
00:03:21,370 --> 00:03:24,005
in data analytics and law and ethics.

80
00:03:24,005 --> 00:03:26,450
So that in learning about the science,

81
00:03:26,450 --> 00:03:27,470
we're also learning about

82
00:03:27,470 --> 00:03:30,507
the ethical and legal frameworks that are important.

83
00:03:30,507 --> 00:03:32,480
The fifth best practice,

84
00:03:32,480 --> 00:03:34,910
consider the role of the government and private sector in

85
00:03:34,910 --> 00:03:38,030
setting the rules of the road for how data is used.

86
00:03:38,030 --> 00:03:39,920
I would also say here that you also

87
00:03:39,920 --> 00:03:42,240
have to consider the role of civil society.

88
00:03:42,240 --> 00:03:45,290
There are a lot of nonprofits, think tanks,

89
00:03:45,290 --> 00:03:47,975
and groups that are involved in

90
00:03:47,975 --> 00:03:49,400
understanding what is happening in

91
00:03:49,400 --> 00:03:51,525
data analytics and artificial intelligence.

92
00:03:51,525 --> 00:03:55,000
And those voices need to be heard as well.

