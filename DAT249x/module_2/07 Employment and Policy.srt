0
00:00:00,000 --> 00:00:03,480
New hiring practices use algorithms in

1
00:00:03,480 --> 00:00:07,155
place of industrial psychologists and hiring recruiters.

2
00:00:07,155 --> 00:00:09,060
It makes sense that an algorithm could be

3
00:00:09,060 --> 00:00:11,565
designed to take bias out of hiring

4
00:00:11,565 --> 00:00:14,610
such as the "like me" bias that leads people to hire

5
00:00:14,610 --> 00:00:16,095
people who are like themselves

6
00:00:16,095 --> 00:00:18,600
over others not like themselves.

7
00:00:18,600 --> 00:00:20,640
But there is here a risk that

8
00:00:20,640 --> 00:00:23,540
since the algorithm is designed by humans,

9
00:00:23,540 --> 00:00:26,340
I will somehow incorporate human bias.

10
00:00:26,340 --> 00:00:28,545
An example given in the 2016

11
00:00:28,545 --> 00:00:31,275
White House Big Data Report is this.

12
00:00:31,275 --> 00:00:33,750
What is a data point used in the algorithm?

13
00:00:33,750 --> 00:00:35,640
Is the age that the job candidate

14
00:00:35,640 --> 00:00:38,075
became interested in computing?

15
00:00:38,075 --> 00:00:40,320
There are cultural messages

16
00:00:40,320 --> 00:00:42,420
and assumptions that associate computing with

17
00:00:42,420 --> 00:00:44,940
boys more often than girls and use of

18
00:00:44,940 --> 00:00:46,170
this data point could promote

19
00:00:46,170 --> 00:00:48,630
more male hires than female hires.

20
00:00:48,630 --> 00:00:52,380
It could skew hiring in favor of men over women.

21
00:00:52,380 --> 00:00:54,830
Here we see potential disparate impact where

22
00:00:54,830 --> 00:00:57,425
the use of what seems like a neutral data point,

23
00:00:57,425 --> 00:00:58,890
the age at which the person

24
00:00:58,890 --> 00:01:00,705
became interested in computers,

25
00:01:00,705 --> 00:01:02,520
can have a discriminatory impact

26
00:01:02,520 --> 00:01:04,650
on a protected class, women.

27
00:01:04,650 --> 00:01:07,380
Existing employment discrimination law such as

28
00:01:07,380 --> 00:01:09,840
the Civil Rights Act might protect victims

29
00:01:09,840 --> 00:01:12,315
of this type of discrimination, but again,

30
00:01:12,315 --> 00:01:14,505
because of the transparency issue,

31
00:01:14,505 --> 00:01:17,768
unknown data points unknown processes,

32
00:01:17,768 --> 00:01:20,040
it is nearly impossible for a job applicant

33
00:01:20,040 --> 00:01:23,370
to realize she has been discriminated against.

