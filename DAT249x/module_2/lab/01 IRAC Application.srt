0
00:00:00,030 --> 00:00:04,300
In this video, we're going to go through an exercise.

1
00:00:04,300 --> 00:00:08,140
Our exercise for module two in IRAC,

2
00:00:08,140 --> 00:00:11,440
which is a legal critical thinking exercise to help us

3
00:00:11,440 --> 00:00:13,405
understand how to spot legal issues

4
00:00:13,405 --> 00:00:15,970
and work our way through to a conclusion.

5
00:00:15,970 --> 00:00:17,290
So if you remember,

6
00:00:17,290 --> 00:00:19,345
IRAC stands for Issue,

7
00:00:19,345 --> 00:00:22,570
rule, application and conclusion.

8
00:00:22,570 --> 00:00:25,415
We always start out with this story.

9
00:00:25,415 --> 00:00:27,865
This story from module two IRAC

10
00:00:27,865 --> 00:00:30,280
is tied to the content that we've learned in

11
00:00:30,280 --> 00:00:33,160
module two about the law that's relevant

12
00:00:33,160 --> 00:00:37,000
to individuals and identity and privacy.

13
00:00:37,000 --> 00:00:39,115
So the story is about Facebook,

14
00:00:39,115 --> 00:00:42,150
which is a social media firm.

15
00:00:42,150 --> 00:00:43,960
And in this story,

16
00:00:43,960 --> 00:00:47,800
we learned that one of Facebook's advertising practices,

17
00:00:47,800 --> 00:00:50,740
which is called Sponsored Stories, has,

18
00:00:50,740 --> 00:00:53,290
according to some of its members,

19
00:00:53,290 --> 00:00:57,505
taken pictures of the members and identified them

20
00:00:57,505 --> 00:01:01,540
with particular advertisers based on likes.

21
00:01:01,540 --> 00:01:03,400
So, a group of 50 members in

22
00:01:03,400 --> 00:01:07,390
this story have identified that Facebook has taken

23
00:01:07,390 --> 00:01:09,190
their names and their pictures without

24
00:01:09,190 --> 00:01:11,590
their knowledge or consent and used them for

25
00:01:11,590 --> 00:01:13,405
a sponsored story to promote

26
00:01:13,405 --> 00:01:15,880
advertisers that are in

27
00:01:15,880 --> 00:01:18,140
a contractual arrangement with Facebook.

28
00:01:18,140 --> 00:01:19,840
They claim they suffered harm and as

29
00:01:19,840 --> 00:01:22,145
a result of this harm they want to sue Facebook.

30
00:01:22,145 --> 00:01:24,380
That's our underlying story.

31
00:01:24,380 --> 00:01:25,720
Now the issue here or

32
00:01:25,720 --> 00:01:29,710
the legal question is whether Facebook violated

33
00:01:29,710 --> 00:01:32,740
its members rights to privacy when it used

34
00:01:32,740 --> 00:01:34,405
member photographs to promote

35
00:01:34,405 --> 00:01:37,090
the Sponsored Stories advertisers.

36
00:01:37,090 --> 00:01:39,885
That's our legal question to consider.

37
00:01:39,885 --> 00:01:43,355
The rule here, as we've learned in module two,

38
00:01:43,355 --> 00:01:45,110
is about privacy towards.

39
00:01:45,110 --> 00:01:46,610
The privacy towards include

40
00:01:46,610 --> 00:01:49,160
the four distinct intentional torts.

41
00:01:49,160 --> 00:01:50,930
The one that's applicable here is

42
00:01:50,930 --> 00:01:53,300
called appropriation of name or likeness.

43
00:01:53,300 --> 00:01:54,770
And it just sort of jump out at

44
00:01:54,770 --> 00:01:56,720
you like when you hear that somebody has

45
00:01:56,720 --> 00:01:59,540
taken somebody else's identity or an aspect of

46
00:01:59,540 --> 00:02:02,480
their identity and used it to commercial advantage,

47
00:02:02,480 --> 00:02:04,460
you're going to think, "Oh, that sounds

48
00:02:04,460 --> 00:02:06,625
like the appropriation tort."

49
00:02:06,625 --> 00:02:08,660
It happens when there

50
00:02:08,660 --> 00:02:11,300
has been a taking without permission.

51
00:02:11,300 --> 00:02:13,515
It's considered an invasion of privacy.

52
00:02:13,515 --> 00:02:15,110
So the tort defines invasion of

53
00:02:15,110 --> 00:02:17,420
privacy as occurring when one party

54
00:02:17,420 --> 00:02:19,610
without permission takes an aspect of

55
00:02:19,610 --> 00:02:22,895
the person's identity and uses it for commercial benefit.

56
00:02:22,895 --> 00:02:24,260
The harm recognized here is

57
00:02:24,260 --> 00:02:25,670
mental distress of the person,

58
00:02:25,670 --> 00:02:27,455
whose identity was used,

59
00:02:27,455 --> 00:02:31,185
or damage to the value of that person's identity.

60
00:02:31,185 --> 00:02:33,360
Now in the application piece,

61
00:02:33,360 --> 00:02:36,800
this is really where a lot of the logic happens.

62
00:02:36,800 --> 00:02:39,525
You're going to take the rules that we've identified

63
00:02:39,525 --> 00:02:43,440
and apply them to the facts of the case.

64
00:02:43,440 --> 00:02:45,060
The question here for you would be,

65
00:02:45,060 --> 00:02:47,880
how would you apply the privacy tort of appropriation

66
00:02:47,880 --> 00:02:51,065
of name or likeness to the story of this case?

67
00:02:51,065 --> 00:02:53,940
The story of the 50 Facebook members who had

68
00:02:53,940 --> 00:02:55,620
their pictures used in

69
00:02:55,620 --> 00:02:58,530
the Sponsored Stories advertisements.

70
00:02:58,530 --> 00:03:00,210
What conclusion would you reach if

71
00:03:00,210 --> 00:03:02,890
you were the judge deciding the case?

72
00:03:02,890 --> 00:03:05,255
This is a real case, actually.

73
00:03:05,255 --> 00:03:06,510
And in this real case,

74
00:03:06,510 --> 00:03:09,165
it was in California's state court.

75
00:03:09,165 --> 00:03:12,060
Facebook wanted to have the case dismissed,

76
00:03:12,060 --> 00:03:14,730
and I believe was arguing that there

77
00:03:14,730 --> 00:03:17,630
was no claim here for a misappropriation.

78
00:03:17,630 --> 00:03:19,860
But ultimately, the company settled with

79
00:03:19,860 --> 00:03:21,390
the plaintiffs and that means that we don't

80
00:03:21,390 --> 00:03:23,430
know what a court would have decided.

81
00:03:23,430 --> 00:03:26,425
The court was letting the case go forward.

82
00:03:26,425 --> 00:03:28,650
Meaning, they were recognizing that the plaintiffs,

83
00:03:28,650 --> 00:03:31,540
the 50 members, should at least be heard in court.

84
00:03:31,540 --> 00:03:33,630
But at some point, Facebook just decided to

85
00:03:33,630 --> 00:03:36,120
settle with the plaintiffs. Settled out of court.

86
00:03:36,120 --> 00:03:37,470
So, we don't know entirely what

87
00:03:37,470 --> 00:03:39,255
that decision would have been.

88
00:03:39,255 --> 00:03:40,470
But you might, for yourself,

89
00:03:40,470 --> 00:03:42,195
reason and think about,

90
00:03:42,195 --> 00:03:44,430
do you think that this is

91
00:03:44,430 --> 00:03:46,890
a misappropriation of name or likeness,

92
00:03:46,890 --> 00:03:49,740
or do you think that the plaintiffs did not

93
00:03:49,740 --> 00:03:52,633
really have a claim against Facebook in this case?

94
00:03:52,633 --> 00:03:55,190
I'll leave that for you to decide.

