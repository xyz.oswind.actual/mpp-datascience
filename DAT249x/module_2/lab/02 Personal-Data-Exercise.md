Personal Data Exercise
================

With the COMPAS data set, your task is to take a look at the various
fields and determine the following:

  - What attributes would fall under the “personal data” category in
    this set?

After you’ve looked at it with this lens, try to utilize one of the
methods described in the “Masking Personal Data” section of Module 2
(scrambling, deletion, etc).
