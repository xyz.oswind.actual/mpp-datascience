0
00:00:00,020 --> 00:00:03,540
According to the Consumer Financial Protection Bureau,

1
00:00:03,540 --> 00:00:04,980
as many as 11 percent of

2
00:00:04,980 --> 00:00:07,740
US consumers are "credit invisible",

3
00:00:07,740 --> 00:00:09,030
meaning they do not have

4
00:00:09,030 --> 00:00:11,310
enough up to date repayment history for

5
00:00:11,310 --> 00:00:12,480
an algorithm to produce

6
00:00:12,480 --> 00:00:15,960
a credit score so that they can access credit.

7
00:00:15,960 --> 00:00:18,090
Data analytics can actually alleviate

8
00:00:18,090 --> 00:00:20,820
this problem by using new data inputs such as,

9
00:00:20,820 --> 00:00:23,205
other bill payments, like phone bills,

10
00:00:23,205 --> 00:00:25,840
public records education, social media,

11
00:00:25,840 --> 00:00:28,500
et cetera and can improve chances for

12
00:00:28,500 --> 00:00:30,450
otherwise ineligible consumers to

13
00:00:30,450 --> 00:00:32,875
receive credit and loans.

14
00:00:32,875 --> 00:00:34,320
This could have a positive impact

15
00:00:34,320 --> 00:00:35,610
on African-Americans and

16
00:00:35,610 --> 00:00:37,230
Latinos who are more likely

17
00:00:37,230 --> 00:00:39,345
to be credit invisible than whites.

18
00:00:39,345 --> 00:00:40,830
But if not used of due care,

19
00:00:40,830 --> 00:00:42,120
these new scoring mechanisms

20
00:00:42,120 --> 00:00:43,470
could actually make the problem

21
00:00:43,470 --> 00:00:47,785
worse and perpetuate or mask discrimination.

22
00:00:47,785 --> 00:00:49,350
Remember, the challenges here

23
00:00:49,350 --> 00:00:51,525
are data inputs and accuracy

24
00:00:51,525 --> 00:00:53,610
and lack of transparency with respect

25
00:00:53,610 --> 00:00:56,010
to the design and workings of the system.

26
00:00:56,010 --> 00:00:58,020
As algorithms develop to measure

27
00:00:58,020 --> 00:00:59,700
creditworthiness in new ways,

28
00:00:59,700 --> 00:01:02,430
it will be critical to design and test them to guard

29
00:01:02,430 --> 00:01:04,380
against carelessly using information

30
00:01:04,380 --> 00:01:05,910
that's a proxy for race,

31
00:01:05,910 --> 00:01:09,475
gender, and other protected characteristics.

32
00:01:09,475 --> 00:01:11,710
There may be legal protection here for consumers and

33
00:01:11,710 --> 00:01:14,640
negligence law and in federal statutory law,

34
00:01:14,640 --> 00:01:16,575
such as the Fair Credit Reporting Act

35
00:01:16,575 --> 00:01:18,855
and the Equal Credit Opportunity Act.

36
00:01:18,855 --> 00:01:21,060
But it is really challenging for consumers,

37
00:01:21,060 --> 00:01:23,190
particularly those with less experience dealing with

38
00:01:23,190 --> 00:01:26,235
large institutions or complex data products,

39
00:01:26,235 --> 00:01:30,390
to even identify when and if a violation has occurred.

