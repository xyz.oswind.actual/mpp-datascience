0
00:00:00,040 --> 00:00:03,160
Data isn't just about the bits and bytes.

1
00:00:03,160 --> 00:00:04,560
It can be really personal.

2
00:00:04,560 --> 00:00:08,740
It can be about you and about you in society at large.

3
00:00:08,740 --> 00:00:12,025
And what Eva and Nathan and I are going to talk about

4
00:00:12,025 --> 00:00:15,800
is this intersection of individuals, data and society.

5
00:00:15,800 --> 00:00:17,710
So Nathan, can you tell us a little bit

6
00:00:17,710 --> 00:00:20,060
more about what this is going to entail?

7
00:00:20,060 --> 00:00:21,520
>> Yeah, I think the conversation

8
00:00:21,520 --> 00:00:22,990
has to start around the topic of

9
00:00:22,990 --> 00:00:26,020
bias because bias obviously affects individuals,

10
00:00:26,020 --> 00:00:27,550
but it's also a societal question

11
00:00:27,550 --> 00:00:28,870
at the same time because we have to ask

12
00:00:28,870 --> 00:00:32,035
ourselves what kind of society we want to be.

13
00:00:32,035 --> 00:00:34,450
And the predictive algorithms,

14
00:00:34,450 --> 00:00:37,745
algorithms that predict your future behavior,

15
00:00:37,745 --> 00:00:39,190
may take large amounts of

16
00:00:39,190 --> 00:00:42,145
aggregated data about you that's actually been collected,

17
00:00:42,145 --> 00:00:43,990
and then use statistical models

18
00:00:43,990 --> 00:00:46,000
to predict what's going to happen in the future.

19
00:00:46,000 --> 00:00:50,110
So as we now know from the lab in Mod one,

20
00:00:50,110 --> 00:00:52,030
we're looking at recidivism.

21
00:00:52,030 --> 00:00:53,470
There are algorithms out there that

22
00:00:53,470 --> 00:00:55,960
predict are you likely to commit a crime again?

23
00:00:55,960 --> 00:00:58,935
Does that mean we should hold you in jail longer?

24
00:00:58,935 --> 00:01:02,335
So those type of algorithms are often sold

25
00:01:02,335 --> 00:01:04,360
as this is great because we're going to

26
00:01:04,360 --> 00:01:06,460
remove the human bias from these situations.

27
00:01:06,460 --> 00:01:07,660
There's all these studies about

28
00:01:07,660 --> 00:01:10,540
how judges and juries et cetera are biased,

29
00:01:10,540 --> 00:01:12,780
and so we can just get the bias out.

30
00:01:12,780 --> 00:01:14,500
But I think the

31
00:01:14,500 --> 00:01:16,510
most fundamental thing to understand about

32
00:01:16,510 --> 00:01:21,130
this is that we haven't solved the problem of bias.

33
00:01:21,130 --> 00:01:22,935
That's the wrong way to think about.

34
00:01:22,935 --> 00:01:24,810
We have a new problem

35
00:01:24,810 --> 00:01:26,800
of bias which isn't to say it's a worse problem,

36
00:01:26,800 --> 00:01:28,540
but it turned out to be easier to solve than

37
00:01:28,540 --> 00:01:30,140
the human bias problem because that's

38
00:01:30,140 --> 00:01:32,390
a pretty persistent problem.

39
00:01:32,390 --> 00:01:34,129
But we have to start thinking in terms of,

40
00:01:34,129 --> 00:01:37,150
okay now we got a new bias problem on our hands.

41
00:01:37,150 --> 00:01:39,430
And so that's a challenge. We have to confront it.

42
00:01:39,430 --> 00:01:42,460
>> Great. Thank you. And Eva, how about you?

43
00:01:42,460 --> 00:01:43,660
>> I think that this module's really

44
00:01:43,660 --> 00:01:44,830
going to help us identify

45
00:01:44,830 --> 00:01:47,565
the relationship between privacy and identity.

46
00:01:47,565 --> 00:01:51,075
So, Nathan's talking about using this data,

47
00:01:51,075 --> 00:01:52,900
the data is largely private.

48
00:01:52,900 --> 00:01:54,190
It's personal to us.

49
00:01:54,190 --> 00:01:55,920
It identifies us.

50
00:01:55,920 --> 00:01:58,270
And there are zones of privacy that

51
00:01:58,270 --> 00:02:02,255
include questions of autonomy and self-determination,

52
00:02:02,255 --> 00:02:03,935
so we're going to be exploring

53
00:02:03,935 --> 00:02:06,970
the ways in which this bias

54
00:02:06,970 --> 00:02:09,460
is actually also taking away our

55
00:02:09,460 --> 00:02:12,595
right to fundamentally define ourselves.

56
00:02:12,595 --> 00:02:15,345
And this is a fundamental right in international law,

57
00:02:15,345 --> 00:02:18,235
in human rights law in US Constitutional law,

58
00:02:18,235 --> 00:02:19,840
that recognizing that there

59
00:02:19,840 --> 00:02:21,460
is a relationship between the two things,

60
00:02:21,460 --> 00:02:23,380
and recognizing the importance of not

61
00:02:23,380 --> 00:02:26,230
turning over to data science,

62
00:02:26,230 --> 00:02:28,430
data analytics and artificial intelligence

63
00:02:28,430 --> 00:02:29,865
our fundamental right to define

64
00:02:29,865 --> 00:02:32,290
ourselves and create our own lives.

65
00:02:32,290 --> 00:02:33,635
>> Brilliant. Thank you.

66
00:02:33,635 --> 00:02:35,260
Yeah, really important stuff.

67
00:02:35,260 --> 00:02:37,960
And we look forward to digging in. Thank you.

