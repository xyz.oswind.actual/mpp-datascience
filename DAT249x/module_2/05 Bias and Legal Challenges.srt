0
00:00:00,000 --> 00:00:02,190
The executive office of

1
00:00:02,190 --> 00:00:05,895
President Barack Obama issued a report in 2016,

2
00:00:05,895 --> 00:00:07,680
which identifies the opportunities

3
00:00:07,680 --> 00:00:09,270
and challenges of big data,

4
00:00:09,270 --> 00:00:10,980
the law that protects civil rights

5
00:00:10,980 --> 00:00:12,660
and protects against discrimination,

6
00:00:12,660 --> 00:00:14,400
and also some best practices for

7
00:00:14,400 --> 00:00:16,950
organizations in the data industry.

8
00:00:16,950 --> 00:00:18,660
This report really highlights two of

9
00:00:18,660 --> 00:00:21,465
the course themes we identified in Mod one.

10
00:00:21,465 --> 00:00:23,820
The tension between law and technology,

11
00:00:23,820 --> 00:00:25,710
and the tension between individual rights

12
00:00:25,710 --> 00:00:27,595
and organizational rights.

13
00:00:27,595 --> 00:00:29,730
In this video, I will share some challenges and

14
00:00:29,730 --> 00:00:32,295
highlight some principles of law that are relevant.

15
00:00:32,295 --> 00:00:34,170
In later videos, we will look at how

16
00:00:34,170 --> 00:00:37,740
these challenges and laws show up in four distinct areas,

17
00:00:37,740 --> 00:00:39,960
consumer rights, employee rights,

18
00:00:39,960 --> 00:00:42,885
student rights, and criminal justice.

19
00:00:42,885 --> 00:00:44,775
And finally, in a separate video,

20
00:00:44,775 --> 00:00:47,100
we will review the best practices suggested by

21
00:00:47,100 --> 00:00:50,850
the US government to alleviate and eliminate bias.

22
00:00:50,850 --> 00:00:52,710
The White House big data report identifies

23
00:00:52,710 --> 00:00:56,550
two challenges to promoting fairness and overcoming bias.

24
00:00:56,550 --> 00:00:58,515
One, the challenge related to

25
00:00:58,515 --> 00:01:01,965
the data inputs themselves that are used in algorithms;

26
00:01:01,965 --> 00:01:05,685
and two, the design of the algorithm systems.

27
00:01:05,685 --> 00:01:07,200
For the first challenge,

28
00:01:07,200 --> 00:01:08,280
the concern is related to

29
00:01:08,280 --> 00:01:11,860
our earlier discussion of inclusion/exclusion.

30
00:01:11,860 --> 00:01:13,620
The decision to use certain inputs and

31
00:01:13,620 --> 00:01:15,420
not others in an algorithm can,

32
00:01:15,420 --> 00:01:17,480
in fact, result in discrimination.

33
00:01:17,480 --> 00:01:20,070
For example, in an algorithm designed

34
00:01:20,070 --> 00:01:23,055
to determine the fastest route between points a and b,

35
00:01:23,055 --> 00:01:24,690
the architect of the system might

36
00:01:24,690 --> 00:01:26,490
include information about roads,

37
00:01:26,490 --> 00:01:29,700
but not bike routes or public transportation.

38
00:01:29,700 --> 00:01:33,110
This negatively impacts those who do not own a vehicle.

39
00:01:33,110 --> 00:01:35,070
Similarly, where the data inputs do not

40
00:01:35,070 --> 00:01:37,165
reflect accurately a population,

41
00:01:37,165 --> 00:01:38,640
there can be conclusions made that

42
00:01:38,640 --> 00:01:40,575
favor certain groups over others.

43
00:01:40,575 --> 00:01:43,665
For example, in the fastest route problem,

44
00:01:43,665 --> 00:01:45,345
if speed data is collected

45
00:01:45,345 --> 00:01:47,350
only from those who own smartphones,

46
00:01:47,350 --> 00:01:49,920
then the system result may be more accurate for

47
00:01:49,920 --> 00:01:51,360
wealthier populations with

48
00:01:51,360 --> 00:01:53,610
higher concentrations of smartphones,

49
00:01:53,610 --> 00:01:54,990
and less accurate for

50
00:01:54,990 --> 00:01:58,325
poor areas where smartphone concentrations are lower.

51
00:01:58,325 --> 00:02:01,830
This otherwise neutral collection of data can have

52
00:02:01,830 --> 00:02:04,500
a disparate or disproportionate impact

53
00:02:04,500 --> 00:02:07,405
on socially and economically disadvantaged people.

54
00:02:07,405 --> 00:02:09,700
This is discrimination.

55
00:02:09,700 --> 00:02:11,970
We're going to hear from Nathan about challenge

56
00:02:11,970 --> 00:02:14,620
two the actual design of the system.

57
00:02:14,620 --> 00:02:16,380
But basically, the issue here is that

58
00:02:16,380 --> 00:02:19,185
the technical processes involved in describing,

59
00:02:19,185 --> 00:02:21,600
diagnosing and predicting human preferences

60
00:02:21,600 --> 00:02:24,280
and behaviors are completely unknown.

61
00:02:24,280 --> 00:02:26,040
There's no transparency.

62
00:02:26,040 --> 00:02:28,200
And because we can't see the process,

63
00:02:28,200 --> 00:02:29,390
we can't determine if there's

64
00:02:29,390 --> 00:02:32,160
been bias or discrimination.

65
00:02:32,160 --> 00:02:34,290
The owner of the algorithmic system has a

66
00:02:34,290 --> 00:02:37,365
confidential trade secret right in that system.

67
00:02:37,365 --> 00:02:39,090
It is not required to disclose it,

68
00:02:39,090 --> 00:02:41,610
so we can never really tell what's happening.

69
00:02:41,610 --> 00:02:43,590
The owner, organization, or

70
00:02:43,590 --> 00:02:45,720
business has what we call intellectual

71
00:02:45,720 --> 00:02:48,150
property rights that are here at tension

72
00:02:48,150 --> 00:02:51,860
with individual rights be free from discrimination.

