Data Set Instructions
================

In the US, probation and parole officers along with judges are often
using algorithms to assess a criminal defendant’s likelihood to
re-offend (called recidivism). One of the leading tools to do this is
the COMPAS algorithm, which stands for “Correctional Offender Management
Profiling for Alternative Sanctions”.

The data set you’ll be using in these labs is part of the [COMPAS
algorithm’s feature
set](https://gitlab.com/xyz.oswind.actual/mpp-datascience/blob/master/DAT249x/compas-analysis-data).
Download the zipped folder of the files. Then un-zip the folder once it
is downloaded.

We will use the file compas-two-years-violent.csv.
