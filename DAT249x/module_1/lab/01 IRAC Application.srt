0
00:00:00,050 --> 00:00:04,020
We're going to do our first IRAC exercise now.

1
00:00:04,020 --> 00:00:05,580
IRAC as you recall is

2
00:00:05,580 --> 00:00:08,550
our legal analysis tool to understand how to move from

3
00:00:08,550 --> 00:00:11,280
identifying a legal issue to reaching

4
00:00:11,280 --> 00:00:14,515
a conclusion and a decision about how to take action.

5
00:00:14,515 --> 00:00:16,440
For this first one, we're actually going to use

6
00:00:16,440 --> 00:00:18,330
an existing case so that

7
00:00:18,330 --> 00:00:20,265
we can learn about the component parts,

8
00:00:20,265 --> 00:00:24,075
the issue, rule, application and conclusion.

9
00:00:24,075 --> 00:00:27,210
And also just more easily understand how

10
00:00:27,210 --> 00:00:28,770
this particular story in

11
00:00:28,770 --> 00:00:30,450
this particular case relates

12
00:00:30,450 --> 00:00:32,190
back to our three things for the courts,

13
00:00:32,190 --> 00:00:34,695
which are that laws territorial,

14
00:00:34,695 --> 00:00:38,061
that there's a tension between law and technology,

15
00:00:38,061 --> 00:00:41,100
and finally that there is a strange relationship

16
00:00:41,100 --> 00:00:43,410
between organizational and human rights

17
00:00:43,410 --> 00:00:45,505
in instances of law and technology.

18
00:00:45,505 --> 00:00:48,620
So, all of these cases start with a story and again,

19
00:00:48,620 --> 00:00:52,440
this case is about a real case that concerned Microsoft.

20
00:00:52,440 --> 00:00:55,320
In this story, Microsoft stores data about

21
00:00:55,320 --> 00:00:58,935
its customers both in the United States and overseas.

22
00:00:58,935 --> 00:01:02,010
The information is stored in e-mail communications.

23
00:01:02,010 --> 00:01:04,080
The U.S. government was issuing

24
00:01:04,080 --> 00:01:06,480
a warrant under a U.S. law called

25
00:01:06,480 --> 00:01:08,820
the Stored Communications Act because it was in

26
00:01:08,820 --> 00:01:11,858
an investigation about a particular individual,

27
00:01:11,858 --> 00:01:13,320
and it wanted access to

28
00:01:13,320 --> 00:01:15,270
e-mail communications that Microsoft had

29
00:01:15,270 --> 00:01:18,845
in its possession both in the United States and overseas.

30
00:01:18,845 --> 00:01:22,295
In this case, overseas in Ireland.

31
00:01:22,295 --> 00:01:24,345
So, the U.S. government

32
00:01:24,345 --> 00:01:26,745
issued a warrant to try to get the information.

33
00:01:26,745 --> 00:01:29,430
Microsoft turned over the information

34
00:01:29,430 --> 00:01:31,140
within the territory, the United States.

35
00:01:31,140 --> 00:01:33,580
And that means within the territorial reach

36
00:01:33,580 --> 00:01:35,110
because they were on servers and

37
00:01:35,110 --> 00:01:36,810
in data centers in the U.S..

38
00:01:36,810 --> 00:01:39,675
But with respect to the information held in Ireland,

39
00:01:39,675 --> 00:01:42,220
it refused to turn over the information.

40
00:01:42,220 --> 00:01:44,175
At which point, the government

41
00:01:44,175 --> 00:01:46,140
issued this warning and

42
00:01:46,140 --> 00:01:48,315
then actually held them in contempt.

43
00:01:48,315 --> 00:01:50,655
So coming out of this story,

44
00:01:50,655 --> 00:01:53,235
the issue is in this case,

45
00:01:53,235 --> 00:01:55,860
whether that law, the Stored communication Act,

46
00:01:55,860 --> 00:01:57,990
allows the U.S. government to obtain

47
00:01:57,990 --> 00:02:00,840
customer information stored in e-mail from

48
00:02:00,840 --> 00:02:02,790
computer service providers like

49
00:02:02,790 --> 00:02:05,160
Microsoft when the information is stored on

50
00:02:05,160 --> 00:02:07,470
servers and in data centers that are outside

51
00:02:07,470 --> 00:02:10,255
of the physical territory of the United States.

52
00:02:10,255 --> 00:02:12,280
Remember, law is territorial.

53
00:02:12,280 --> 00:02:14,310
So we're really concerned with where

54
00:02:14,310 --> 00:02:15,900
the physical boundaries are for

55
00:02:15,900 --> 00:02:19,210
countries and nation states and governments.

56
00:02:19,210 --> 00:02:20,940
The rules here, once we

57
00:02:20,940 --> 00:02:23,205
identify with the legal question is,

58
00:02:23,205 --> 00:02:25,190
we look to see what the rules are.

59
00:02:25,190 --> 00:02:27,900
And again, I'm giving you the rules in this IRAC so

60
00:02:27,900 --> 00:02:31,200
that you can start to learn how the process works.

61
00:02:31,200 --> 00:02:34,995
The first rule is that the Stored Communication Act,

62
00:02:34,995 --> 00:02:36,480
which is a part of something called

63
00:02:36,480 --> 00:02:39,672
the Electronic Communications Privacy Act,

64
00:02:39,672 --> 00:02:43,305
does actually protect consumer privacy,

65
00:02:43,305 --> 00:02:47,010
consumer private electronic communications.

66
00:02:47,010 --> 00:02:50,460
That's what the intention of the ECPA is.

67
00:02:50,460 --> 00:02:53,520
However, there's exceptions to everything and

68
00:02:53,520 --> 00:02:55,020
the Stored Communications Act does

69
00:02:55,020 --> 00:02:56,910
allow the U.S. government to

70
00:02:56,910 --> 00:03:00,120
access electronic communications of individuals

71
00:03:00,120 --> 00:03:02,040
pursuant to its surveillance

72
00:03:02,040 --> 00:03:04,680
and law enforcement activities.

73
00:03:04,680 --> 00:03:07,703
The second rule that's applicable here is a presumption.

74
00:03:07,703 --> 00:03:10,560
And there's a presumption in U.S. law against what

75
00:03:10,560 --> 00:03:13,900
we call extraterritorial application of law.

76
00:03:13,900 --> 00:03:15,855
What does that mean?

77
00:03:15,855 --> 00:03:18,420
It means that there is a presumption against

78
00:03:18,420 --> 00:03:21,000
any government reaching outside

79
00:03:21,000 --> 00:03:23,580
of its physical boundaries and trying to control

80
00:03:23,580 --> 00:03:26,665
what happens outside of those physical boundaries.

81
00:03:26,665 --> 00:03:28,155
So when the government,

82
00:03:28,155 --> 00:03:29,605
like the U.S. government,

83
00:03:29,605 --> 00:03:31,335
wants to try to control something

84
00:03:31,335 --> 00:03:33,643
outside the United States,

85
00:03:33,643 --> 00:03:34,665
we say that that's an

86
00:03:34,665 --> 00:03:37,740
extraterritorial application or reach.

87
00:03:37,740 --> 00:03:39,660
And there's a presumption against that.

88
00:03:39,660 --> 00:03:40,890
The Supreme Court recently in

89
00:03:40,890 --> 00:03:43,620
this Morrisson decision which is cited for you here

90
00:03:43,620 --> 00:03:45,480
has said that there is a presumption against

91
00:03:45,480 --> 00:03:48,720
the extraterritorial application of U.S. law.

92
00:03:48,720 --> 00:03:50,370
So in the application component,

93
00:03:50,370 --> 00:03:52,890
what we do is we take the rules,

94
00:03:52,890 --> 00:03:55,955
and we apply them to the story of the case.

95
00:03:55,955 --> 00:03:57,540
And my question for you here would be,

96
00:03:57,540 --> 00:03:59,970
how would you apply those two rules on

97
00:03:59,970 --> 00:04:01,590
the previous slide to

98
00:04:01,590 --> 00:04:03,360
the facts of this particular case where

99
00:04:03,360 --> 00:04:06,015
Microsoft is contending that it is not

100
00:04:06,015 --> 00:04:07,620
responsible to turn over

101
00:04:07,620 --> 00:04:11,485
customer information held overseas.

102
00:04:11,485 --> 00:04:13,800
Finally, the conclusion is

103
00:04:13,800 --> 00:04:15,330
actually we know that the conclusion is in

104
00:04:15,330 --> 00:04:16,920
this case because there was a decision,

105
00:04:16,920 --> 00:04:18,565
the Second Circuit Court of Appeals,

106
00:04:18,565 --> 00:04:19,830
which is an appellate court in the

107
00:04:19,830 --> 00:04:21,550
U.S. federal court system,

108
00:04:21,550 --> 00:04:23,125
actually held for Microsoft.

109
00:04:23,125 --> 00:04:24,291
They found for Microsoft.

110
00:04:24,291 --> 00:04:25,950
Microsoft won this case,

111
00:04:25,950 --> 00:04:28,590
and the court said that section 2703 of

112
00:04:28,590 --> 00:04:31,035
the Stored Communications Act was not

113
00:04:31,035 --> 00:04:34,045
intended to extend beyond the reach of the United States.

114
00:04:34,045 --> 00:04:36,660
Yes, it extends in the United States,

115
00:04:36,660 --> 00:04:39,150
where the territorial jurisdiction is,

116
00:04:39,150 --> 00:04:41,295
but it does not extend beyond.

117
00:04:41,295 --> 00:04:42,570
There's a presumption against

118
00:04:42,570 --> 00:04:45,960
that and Microsoft is actually within its rights to

119
00:04:45,960 --> 00:04:48,150
protect consumer privacy and

120
00:04:48,150 --> 00:04:49,710
refuse to provide the information

121
00:04:49,710 --> 00:04:50,900
to the U.S. authorities.

122
00:04:50,900 --> 00:04:53,325
Now interestingly, you see here that,

123
00:04:53,325 --> 00:04:56,685
that relationship between an organizational rights

124
00:04:56,685 --> 00:04:59,386
and individual rights is in alignment.

125
00:04:59,386 --> 00:05:00,955
Microsoft was in fact,

126
00:05:00,955 --> 00:05:04,025
protecting its customers' privacy.

127
00:05:04,025 --> 00:05:06,760
And so the rights or concerns of

128
00:05:06,760 --> 00:05:08,470
the organization Microsoft were in

129
00:05:08,470 --> 00:05:11,490
alignment with the customer in this case.

