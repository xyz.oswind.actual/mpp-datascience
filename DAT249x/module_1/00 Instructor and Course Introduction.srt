0
00:00:00,000 --> 00:00:02,325
>> Hi. I'm Ben Olsen,

1
00:00:02,325 --> 00:00:04,085
Content Developer at Microsoft,

2
00:00:04,085 --> 00:00:05,850
and welcome to Ethics and Law in

3
00:00:05,850 --> 00:00:08,100
Analytics and Artificial Intelligence.

4
00:00:08,100 --> 00:00:10,560
This course is meant to give you the foundations you

5
00:00:10,560 --> 00:00:13,760
need in both ethics and law as it relates to data space.

6
00:00:13,760 --> 00:00:15,895
Not only do your job well, that's a given,

7
00:00:15,895 --> 00:00:18,570
but to take it to the next level in terms of making

8
00:00:18,570 --> 00:00:20,010
the impact that you want in

9
00:00:20,010 --> 00:00:21,900
the world and the society at large,

10
00:00:21,900 --> 00:00:24,655
whether you're in a nonprofit or a for-profit.

11
00:00:24,655 --> 00:00:27,270
All the principles, we're going to apply to

12
00:00:27,270 --> 00:00:30,040
actual data work in this course. We'll come to bear.

13
00:00:30,040 --> 00:00:33,880
And with me to teach this course is Eva Lasprogata and

14
00:00:33,880 --> 00:00:36,555
Nathan Colaner and I'd love to have you to

15
00:00:36,555 --> 00:00:37,650
introduce yourselves and tell

16
00:00:37,650 --> 00:00:39,710
the audience why you care about this course.

17
00:00:39,710 --> 00:00:40,590
>> Thank you, Ben.

18
00:00:40,590 --> 00:00:41,380
>> Thank you.

19
00:00:41,380 --> 00:00:43,500
>> I'm a law professor at Seattle University.

20
00:00:43,500 --> 00:00:46,625
I teach business law and data analytics and law.

21
00:00:46,625 --> 00:00:50,145
I'm also a partner in a firm called Digital Dignity.

22
00:00:50,145 --> 00:00:53,190
I've been investigating, thinking about,

23
00:00:53,190 --> 00:00:54,960
and writing about that intersection of

24
00:00:54,960 --> 00:00:57,720
human rights entrepreneurship and technology for

25
00:00:57,720 --> 00:00:59,550
many years and I'm really

26
00:00:59,550 --> 00:01:01,890
excited to explore that in this course and

27
00:01:01,890 --> 00:01:06,960
share with the audience what law does apply to big data.

28
00:01:06,960 --> 00:01:10,239
And where there is not enough law or there is no law,

29
00:01:10,239 --> 00:01:12,450
what are we going to do about it and considering

30
00:01:12,450 --> 00:01:13,860
the ethical framework that we might

31
00:01:13,860 --> 00:01:15,690
explore with Nathan as well.

32
00:01:15,690 --> 00:01:17,074
>> Okay. Thank you.

33
00:01:17,074 --> 00:01:19,730
>> And I'm Nathan. I'm also a professor at

34
00:01:19,730 --> 00:01:22,100
Seattle University teaching business ethics,

35
00:01:22,100 --> 00:01:23,480
and along with Eva, and

36
00:01:23,480 --> 00:01:26,365
co-founder of the consulting firm Digital Dignity.

37
00:01:26,365 --> 00:01:28,655
And this is a really important course for me.

38
00:01:28,655 --> 00:01:31,260
We understand principles of law and principles of ethics.

39
00:01:31,260 --> 00:01:33,180
We have them pretty well worked out,

40
00:01:33,180 --> 00:01:35,040
but there are some conversations

41
00:01:35,040 --> 00:01:36,090
that we need to have as we

42
00:01:36,090 --> 00:01:39,870
enter into this new space of data.

43
00:01:39,870 --> 00:01:42,790
>> Thanks so much and thank you for joining us and we

44
00:01:42,790 --> 00:01:44,220
will look forward to working with you

45
00:01:44,220 --> 00:01:46,940
through this course. Let's dive right in.

