0
00:00:00,000 --> 00:00:01,950
Law tells a story of

1
00:00:01,950 --> 00:00:04,260
civil society through rules that govern the rights

2
00:00:04,260 --> 00:00:06,029
and responsibilities of individuals,

3
00:00:06,029 --> 00:00:08,055
organizations, and governments.

4
00:00:08,055 --> 00:00:09,420
In a later video,

5
00:00:09,420 --> 00:00:11,325
we will examine sources of law,

6
00:00:11,325 --> 00:00:13,080
or where the law comes from.

7
00:00:13,080 --> 00:00:15,287
But for now, let's consider the role of law in

8
00:00:15,287 --> 00:00:18,355
society and its relationship with technology.

9
00:00:18,355 --> 00:00:21,360
The rules of law of society reflect its values.

10
00:00:21,360 --> 00:00:23,205
For example, the European Union

11
00:00:23,205 --> 00:00:24,540
and its soon to be implemented

12
00:00:24,540 --> 00:00:28,695
GDPR reveals that it values individual privacy.

13
00:00:28,695 --> 00:00:30,750
The US Constitution's First Amendment

14
00:00:30,750 --> 00:00:33,625
reveals that America values free speech.

15
00:00:33,625 --> 00:00:35,430
One of our first themes for the course

16
00:00:35,430 --> 00:00:37,450
is that law is territorial.

17
00:00:37,450 --> 00:00:39,020
It's confined to the territory

18
00:00:39,020 --> 00:00:40,745
of the place that created it.

19
00:00:40,745 --> 00:00:43,005
Like an old school hardbound textbook,

20
00:00:43,005 --> 00:00:44,700
the law story is confined to

21
00:00:44,700 --> 00:00:47,830
the geographical boundaries of the nation that wrote it.

22
00:00:47,830 --> 00:00:50,845
This simple concept's territoriality

23
00:00:50,845 --> 00:00:52,915
adds complexity for organizations that

24
00:00:52,915 --> 00:00:54,745
operate in more than one nation

25
00:00:54,745 --> 00:00:55,990
because they must comply with

26
00:00:55,990 --> 00:00:58,510
new and sometimes conflicting legal obligations

27
00:00:58,510 --> 00:01:00,635
from one nation to another.

28
00:01:00,635 --> 00:01:02,920
It adds even more complexity when operations

29
00:01:02,920 --> 00:01:06,010
are computer driven and internet transmitted.

30
00:01:06,010 --> 00:01:08,200
Law likes what is physical and present

31
00:01:08,200 --> 00:01:11,110
within boundaries of states, regions, and nations.

32
00:01:11,110 --> 00:01:12,445
It likes less what is

33
00:01:12,445 --> 00:01:15,355
intangible such as that which is digital.

34
00:01:15,355 --> 00:01:17,230
So the relationship between law and

35
00:01:17,230 --> 00:01:19,700
technology starts off rocky.

36
00:01:19,700 --> 00:01:21,940
Intensifying this misalignment is the fact

37
00:01:21,940 --> 00:01:24,590
that technology evolves at a rapid pace.

38
00:01:24,590 --> 00:01:25,705
Law, on the other hand,

39
00:01:25,705 --> 00:01:27,525
moves as slow as a snail.

40
00:01:27,525 --> 00:01:30,370
It is always catching up to technology.

41
00:01:30,370 --> 00:01:32,020
In June of this year, Brad Smith,

42
00:01:32,020 --> 00:01:34,510
President and Chief Legal Counsel of Microsoft,

43
00:01:34,510 --> 00:01:37,520
noted this relationship in a talk referencing a U.S. law,

44
00:01:37,520 --> 00:01:42,200
the Electronic Communications Privacy Act or the ECPA.

45
00:01:42,200 --> 00:01:43,960
This law was passed by Congress in

46
00:01:43,960 --> 00:01:48,065
1986 during the age of police wiretapping.

47
00:01:48,065 --> 00:01:49,380
This was also the age of

48
00:01:49,380 --> 00:01:51,755
the historical relic, the phone booth.

49
00:01:51,755 --> 00:01:54,190
In an ongoing struggle courts have considered

50
00:01:54,190 --> 00:01:56,860
the privacy protections of the ECPA and

51
00:01:56,860 --> 00:01:59,560
evolving needs of law enforcement to access

52
00:01:59,560 --> 00:02:01,960
communications of non-U.S. persons

53
00:02:01,960 --> 00:02:04,565
stored overseas by corporations.

54
00:02:04,565 --> 00:02:07,450
It's a misfit but it's a great example of the struggle to

55
00:02:07,450 --> 00:02:11,800
take antiquated law and apply it to modern technologies.

56
00:02:11,800 --> 00:02:13,390
This leads to our second theme,

57
00:02:13,390 --> 00:02:15,385
the strained relationship between law,

58
00:02:15,385 --> 00:02:17,395
data analytics, and AI.

59
00:02:17,395 --> 00:02:20,240
Like law, data tells stories.

60
00:02:20,240 --> 00:02:22,360
The issue is that there is little of any law

61
00:02:22,360 --> 00:02:24,625
to regulate how the data is collected,

62
00:02:24,625 --> 00:02:28,135
managed, shared and used for the story.

63
00:02:28,135 --> 00:02:29,560
We will see many examples in

64
00:02:29,560 --> 00:02:31,510
our course of how some extraordinarily,

65
00:02:31,510 --> 00:02:33,970
brilliant researchers are thinking about how

66
00:02:33,970 --> 00:02:36,670
existing law may apply to rapidly

67
00:02:36,670 --> 00:02:39,295
evolving technology or what new laws

68
00:02:39,295 --> 00:02:42,390
we need to set right this relationship.

69
00:02:42,390 --> 00:02:44,050
Our last course theme to consider at

70
00:02:44,050 --> 00:02:46,510
the outset is the legal tension that exists in

71
00:02:46,510 --> 00:02:48,070
this space with respect to

72
00:02:48,070 --> 00:02:51,390
human rights and organizational rights.

73
00:02:51,390 --> 00:02:53,770
There are many organizational players to consider

74
00:02:53,770 --> 00:02:56,320
that engage human data in different context,

75
00:02:56,320 --> 00:02:59,400
creating legal questions almost at every turn.

76
00:02:59,400 --> 00:03:00,728
For example, does

77
00:03:00,728 --> 00:03:03,580
a social media organization that has access to

78
00:03:03,580 --> 00:03:05,380
a person's shared information

79
00:03:05,380 --> 00:03:08,275
have the right to define the identity of that person?

80
00:03:08,275 --> 00:03:10,800
Who controls human autonomy?

81
00:03:10,800 --> 00:03:13,235
Human beings or data owners?

82
00:03:13,235 --> 00:03:15,280
With respect to issues of bias,

83
00:03:15,280 --> 00:03:17,110
if a person believes he was discriminated

84
00:03:17,110 --> 00:03:19,900
against as a result of algorithmic failure,

85
00:03:19,900 --> 00:03:22,060
will the law demand transparency of

86
00:03:22,060 --> 00:03:24,760
that algorithm to prevent future bias?

87
00:03:24,760 --> 00:03:27,900
Or will the organization's intellectual property rights

88
00:03:27,900 --> 00:03:30,220
in the algorithm win?

