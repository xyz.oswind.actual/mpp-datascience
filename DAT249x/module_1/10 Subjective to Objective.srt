0
00:00:00,050 --> 00:00:03,330
There's an old saying, that there are lies,

1
00:00:03,330 --> 00:00:05,905
damn lies and statistics.

2
00:00:05,905 --> 00:00:07,320
The idea is that lying with

3
00:00:07,320 --> 00:00:09,570
statistics is actually even easier than

4
00:00:09,570 --> 00:00:11,175
lying with regular words

5
00:00:11,175 --> 00:00:13,620
because when many people see numbers,

6
00:00:13,620 --> 00:00:15,720
charts and graphs, their brains just

7
00:00:15,720 --> 00:00:19,080
tend to shut down and they accept what they see.

8
00:00:19,080 --> 00:00:20,550
This view was popularized with

9
00:00:20,550 --> 00:00:22,350
the classic 1954 book

10
00:00:22,350 --> 00:00:24,825
called "How to Lie with Statistics",

11
00:00:24,825 --> 00:00:27,435
which has been a bestseller ever since.

12
00:00:27,435 --> 00:00:29,940
Many of us assume that numbers speak for

13
00:00:29,940 --> 00:00:31,815
themselves and that they give us

14
00:00:31,815 --> 00:00:34,235
the unfiltered objective truth,

15
00:00:34,235 --> 00:00:37,610
but that is a seriously mistaken assumption.

16
00:00:37,610 --> 00:00:39,960
The fact is, it is really easy to

17
00:00:39,960 --> 00:00:41,670
misunderstand data even if

18
00:00:41,670 --> 00:00:43,690
you're doing your best to be honest.

19
00:00:43,690 --> 00:00:46,365
And of course, if your goal is to manipulate someone,

20
00:00:46,365 --> 00:00:49,335
you can really do a lot of mischief.

21
00:00:49,335 --> 00:00:52,493
This is true of data of any kind, small or big,

22
00:00:52,493 --> 00:00:55,020
especially when it comes to collection,

23
00:00:55,020 --> 00:00:58,345
visualization and interpretation of its meaning.

24
00:00:58,345 --> 00:01:01,200
These mistakes happen respectively when data is

25
00:01:01,200 --> 00:01:04,075
derived from a sample that is not properly random,

26
00:01:04,075 --> 00:01:05,760
that's a collection problem,

27
00:01:05,760 --> 00:01:09,045
or when a graph is presented with misleading proportions,

28
00:01:09,045 --> 00:01:11,163
that's a visualization problem,

29
00:01:11,163 --> 00:01:13,200
or when the unit of analysis is overlooked,

30
00:01:13,200 --> 00:01:15,280
that's an interpretation problem.

31
00:01:15,280 --> 00:01:18,010
We must emphasize that with big data,

32
00:01:18,010 --> 00:01:20,655
these are still important worries and

33
00:01:20,655 --> 00:01:24,205
actually most of them are more complicated now.

34
00:01:24,205 --> 00:01:25,830
We will talk about some of them in

35
00:01:25,830 --> 00:01:27,810
module three of this course.

36
00:01:27,810 --> 00:01:31,470
However, a new kind of subjectivity in data is now

37
00:01:31,470 --> 00:01:33,735
possible and that is because data

38
00:01:33,735 --> 00:01:36,900
can now be processed by algorithms.

39
00:01:36,900 --> 00:01:38,385
There are actually two problems

40
00:01:38,385 --> 00:01:40,620
because before data can be

41
00:01:40,620 --> 00:01:45,390
processed it must be pre-processed or cleaned.

42
00:01:45,390 --> 00:01:46,800
We will talk about the issues with

43
00:01:46,800 --> 00:01:48,300
processing data in module

44
00:01:48,300 --> 00:01:52,650
three since that can lead to systematic bias in society.

45
00:01:52,650 --> 00:01:57,320
Data scientists often refer to cleaning data as ETL,

46
00:01:57,320 --> 00:02:00,660
extraction, transformation and loading.

47
00:02:00,660 --> 00:02:02,460
This is because any data set you come

48
00:02:02,460 --> 00:02:04,920
across will almost certainly not be

49
00:02:04,920 --> 00:02:07,035
able to be uploaded to a program to be

50
00:02:07,035 --> 00:02:10,785
processed by an algorithm just the way it is.

51
00:02:10,785 --> 00:02:13,320
Many data scientists will actually tell you

52
00:02:13,320 --> 00:02:16,675
that this is the most time consuming part of their job.

53
00:02:16,675 --> 00:02:19,470
The problem is that it is extremely common

54
00:02:19,470 --> 00:02:22,885
for a large data set to have inconsistencies,

55
00:02:22,885 --> 00:02:26,400
duplicates, corrupted data, missing data,

56
00:02:26,400 --> 00:02:28,875
outliers and entry errors.

57
00:02:28,875 --> 00:02:31,980
The data scientist must make many decisions about how to

58
00:02:31,980 --> 00:02:35,655
deal with these problems and they will create changes.

59
00:02:35,655 --> 00:02:37,560
And it is an open question about whether

60
00:02:37,560 --> 00:02:41,155
these changes make important changes in the results.

61
00:02:41,155 --> 00:02:44,395
In the "Promise and Peril of Big Data",

62
00:02:44,395 --> 00:02:47,655
David Bollier defines data cleaning as the process of

63
00:02:47,655 --> 00:02:49,260
deciding which attributes and

64
00:02:49,260 --> 00:02:51,900
variables matter and which can be ignored.

65
00:02:51,900 --> 00:02:54,675
In quotes, tech CEO Jesper Andersen,

66
00:02:54,675 --> 00:02:57,120
that cleaning removes the objectivity

67
00:02:57,120 --> 00:02:58,995
from the data itself because

68
00:02:58,995 --> 00:03:01,065
it is a very opinionated process

69
00:03:01,065 --> 00:03:04,290
of deciding what variables matter.

70
00:03:04,290 --> 00:03:06,900
The tendency to turn your brain off when confronted with

71
00:03:06,900 --> 00:03:08,415
big data conclusions is

72
00:03:08,415 --> 00:03:11,325
exactly the opposite of what you should do.

73
00:03:11,325 --> 00:03:13,515
When big data offers you answers,

74
00:03:13,515 --> 00:03:15,920
it is time to think critically.

75
00:03:15,920 --> 00:03:18,450
Two researchers at the University of Washington have

76
00:03:18,450 --> 00:03:21,030
focused on the problem of lying with big data

77
00:03:21,030 --> 00:03:24,450
and advise asking as many questions as possible before

78
00:03:24,450 --> 00:03:26,190
accepting conclusions from big data

79
00:03:26,190 --> 00:03:28,755
such as who's telling you this?

80
00:03:28,755 --> 00:03:30,655
How does it advance their interests?

81
00:03:30,655 --> 00:03:34,125
What were the methods used to arrive at the end result?

82
00:03:34,125 --> 00:03:35,640
We will link you to some of their work

83
00:03:35,640 --> 00:03:37,710
in the further reading section.

84
00:03:37,710 --> 00:03:40,095
Also in his book, "Naked Statistics",

85
00:03:40,095 --> 00:03:42,900
Charles Wheelan gives the same sort of advice

86
00:03:42,900 --> 00:03:45,240
but directed toward the statisticians

87
00:03:45,240 --> 00:03:47,160
who present the findings.

88
00:03:47,160 --> 00:03:50,865
He cautions that statistical analysis is not like math,

89
00:03:50,865 --> 00:03:52,680
which yields a correct answer,

90
00:03:52,680 --> 00:03:55,045
but more like detective work.

91
00:03:55,045 --> 00:03:56,565
This requires constant,

92
00:03:56,565 --> 00:03:58,590
honest and humble communication among

93
00:03:58,590 --> 00:04:00,990
the detectives and even the best may

94
00:04:00,990 --> 00:04:04,790
still end up disagreeing about what the results mean.

95
00:04:04,790 --> 00:04:08,210
None of this is to say that big data is a bad way to go.

96
00:04:08,210 --> 00:04:09,960
In fact, it's often the best way

97
00:04:09,960 --> 00:04:11,955
we have to gain important insights,

98
00:04:11,955 --> 00:04:13,980
but both producers and consumers

99
00:04:13,980 --> 00:04:17,190
of big data must use caution.

