Beneficial and Questionable Uses
================

### Here is a useful cheat sheet for ethical Big Data practice:

![](12_Beneficial-and-Questionable-Uses.png)

Martin, Kirsten, “Ethical Issues in the Big Data Industry”, MIS
Quarterly Executive June 2015 (14:2)
