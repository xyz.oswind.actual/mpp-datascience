0
00:00:03,250 --> 00:00:06,410
So you heard from Nathan about

1
00:00:06,410 --> 00:00:10,130
ethical data practice as it relates to moral maximums.

2
00:00:10,130 --> 00:00:11,240
What one ought to

3
00:00:11,240 --> 00:00:13,825
do even if there are no legal repercussions.

4
00:00:13,825 --> 00:00:17,420
And from Eva you heard about the legal side of things,

5
00:00:17,420 --> 00:00:18,920
where it's we have restrictions

6
00:00:18,920 --> 00:00:20,420
as perhaps a moral minimum

7
00:00:20,420 --> 00:00:24,320
provides sort of the guardrails for data practice.

8
00:00:24,320 --> 00:00:26,570
But what should a practitioner actually

9
00:00:26,570 --> 00:00:29,210
look at maybe day to day to inspire or her,

10
00:00:29,210 --> 00:00:32,885
him to do ethical data work?

11
00:00:32,885 --> 00:00:35,540
And I look to other disciplines to

12
00:00:35,540 --> 00:00:38,840
draw inspiration for this and healthcare comes to mind.

13
00:00:38,840 --> 00:00:42,920
There's this old guy named Hippocrates who lived

14
00:00:42,920 --> 00:00:44,930
way back when and created what's called

15
00:00:44,930 --> 00:00:47,745
the Hippocratic Oath for healthcare practitioners.

16
00:00:47,745 --> 00:00:49,115
And what's fun is,

17
00:00:49,115 --> 00:00:53,210
it's sort of inspiring and it's declarative like,

18
00:00:53,210 --> 00:00:56,565
I swear by this or that to do these things well.

19
00:00:56,565 --> 00:01:00,185
But it's actually used in modern healthcare.

20
00:01:00,185 --> 00:01:02,030
Oftentimes it will be framed or

21
00:01:02,030 --> 00:01:04,670
referenced and that's mainly to provide

22
00:01:04,670 --> 00:01:09,600
some sort of ethical guideline for healthcare work.

23
00:01:09,600 --> 00:01:11,330
And in data, we don't really

24
00:01:11,330 --> 00:01:13,455
have a centralized version of this,

25
00:01:13,455 --> 00:01:17,590
but there are examples out in the wild perhaps one is

26
00:01:17,590 --> 00:01:20,475
UN's 1985 declaration for

27
00:01:20,475 --> 00:01:23,270
statisticians and it's extensive,

28
00:01:23,270 --> 00:01:25,700
it's focused on best practices

29
00:01:25,700 --> 00:01:27,890
as it relates to that subdiscipline.

30
00:01:27,890 --> 00:01:30,963
It's extremely long, and thorough, and great.

31
00:01:30,963 --> 00:01:33,480
I doubt that a lot of data practitioners have read it.

32
00:01:33,480 --> 00:01:36,155
So we're going to reference in the further notes,

33
00:01:36,155 --> 00:01:39,050
but please take a look at that if you'd like.

34
00:01:39,050 --> 00:01:41,625
It is a good example, but it's a little outdated.

35
00:01:41,625 --> 00:01:42,830
Another one would be certified

36
00:01:42,830 --> 00:01:43,910
analytics professionals code of

37
00:01:43,910 --> 00:01:45,860
conduct. And this one is fun.

38
00:01:45,860 --> 00:01:49,580
It is focused on the obligation of others and not

39
00:01:49,580 --> 00:01:51,280
a lot about what I as a data practitioner

40
00:01:51,280 --> 00:01:53,750
am going to do perhaps.

41
00:01:53,750 --> 00:01:56,000
Maybe missing that fun declarative aspect

42
00:01:56,000 --> 00:01:58,045
of 'I swear by this or that.'

43
00:01:58,045 --> 00:01:59,810
So I'm going to take another crack at this

44
00:01:59,810 --> 00:02:01,505
and give you what I call an updated data

45
00:02:01,505 --> 00:02:05,690
oath and it's perhaps what I would say if I were

46
00:02:05,690 --> 00:02:10,830
doing data work to do data ethically and do it well.

47
00:02:10,830 --> 00:02:12,920
So you can say it with me,

48
00:02:12,920 --> 00:02:14,600
you can put it on your wall.

49
00:02:14,600 --> 00:02:16,190
I as a data practitioner

50
00:02:16,190 --> 00:02:17,690
will promote the well-being of others and

51
00:02:17,690 --> 00:02:19,220
myself (that's harking back to

52
00:02:19,220 --> 00:02:20,960
Nathan's work on the ethics

53
00:02:20,960 --> 00:02:23,024
stuff) while striving to do no harm with data,

54
00:02:23,024 --> 00:02:24,440
perhaps the legal side,

55
00:02:24,440 --> 00:02:26,000
through professional application of

56
00:02:26,000 --> 00:02:27,665
analytical techniques, number one.

57
00:02:27,665 --> 00:02:29,120
I do my job well.

58
00:02:29,120 --> 00:02:31,730
I know how to interpret P-values and

59
00:02:31,730 --> 00:02:35,320
correlation and not bump into causation.

60
00:02:35,320 --> 00:02:38,205
I know how to do data visualization well,

61
00:02:38,205 --> 00:02:40,760
proper cleaning, and transforming,

62
00:02:40,760 --> 00:02:43,450
and analysis of data sets.

63
00:02:43,450 --> 00:02:46,155
That's all part and parcel of doing analytics well.

64
00:02:46,155 --> 00:02:49,055
And of course fundamental to our job.

65
00:02:49,055 --> 00:02:51,450
And maybe next would be humility and analytic claims.

66
00:02:51,450 --> 00:02:53,780
So that's made a step further

67
00:02:53,780 --> 00:02:54,805
with the correlation stuff

68
00:02:54,805 --> 00:02:56,285
moving into predictive analytics.

69
00:02:56,285 --> 00:02:58,820
Moving into more of these inferences that you make.

70
00:02:58,820 --> 00:03:01,320
Perhaps you give some confidence interval

71
00:03:01,320 --> 00:03:03,430
and range in what you're claiming.

72
00:03:03,430 --> 00:03:06,980
You know, you speak with as

73
00:03:06,980 --> 00:03:09,080
much honesty and humility as you can

74
00:03:09,080 --> 00:03:11,675
when it comes to these analytic products.

75
00:03:11,675 --> 00:03:14,990
And also hopefully what this course is prepping you

76
00:03:14,990 --> 00:03:18,250
to do is to anticipate legal and regulatory needs,

77
00:03:18,250 --> 00:03:20,305
and that goes without saying.

78
00:03:20,305 --> 00:03:23,765
And almost in transparency and computation documentation

79
00:03:23,765 --> 00:03:25,790
is also something that's going to reoccur

80
00:03:25,790 --> 00:03:27,485
throughout the modules in this course,

81
00:03:27,485 --> 00:03:31,070
where we want to share what's happening in

82
00:03:31,070 --> 00:03:34,955
the data processing and in the output space so

83
00:03:34,955 --> 00:03:38,330
that we can avoid things like bias and protect

84
00:03:38,330 --> 00:03:40,970
privacy and all those things

85
00:03:40,970 --> 00:03:43,130
that we're going to investigate in the next modules.

86
00:03:43,130 --> 00:03:45,290
And lastly, I'd say fidelity to this oath beyond

87
00:03:45,290 --> 00:03:47,635
the bottom line and maybe in ethics we call

88
00:03:47,635 --> 00:03:50,210
a supererogatory claim to promote

89
00:03:50,210 --> 00:03:54,950
people and encourage them to do good with

90
00:03:54,950 --> 00:03:59,465
their data work beyond corporate bottom lines or

91
00:03:59,465 --> 00:04:02,135
individual success for the good

92
00:04:02,135 --> 00:04:05,830
of society and the world at large. Thank you.

