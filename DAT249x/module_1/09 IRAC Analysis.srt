0
00:00:00,060 --> 00:00:03,460
One of the objectives for our course is to move beyond

1
00:00:03,460 --> 00:00:06,057
learning about analytics and artificial intelligence,

2
00:00:06,057 --> 00:00:08,050
to applying legal analysis to issues that

3
00:00:08,050 --> 00:00:10,930
arise for you as a data professional in this field.

4
00:00:10,930 --> 00:00:13,535
This course is no substitute for legal counsel,

5
00:00:13,535 --> 00:00:15,010
but you can learn the foundations of

6
00:00:15,010 --> 00:00:17,530
legal analysis which should be really helpful

7
00:00:17,530 --> 00:00:19,630
for identifying potential legal problems and

8
00:00:19,630 --> 00:00:22,565
making strategic decisions that are lawful.

9
00:00:22,565 --> 00:00:25,060
What's also revealed in applying legal analysis is,

10
00:00:25,060 --> 00:00:26,815
the lack of law that exists,

11
00:00:26,815 --> 00:00:28,090
and that is on point for

12
00:00:28,090 --> 00:00:30,350
these rapidly evolving technologies.

13
00:00:30,350 --> 00:00:32,500
In those cases, the ethical frameworks

14
00:00:32,500 --> 00:00:35,100
provided and explained by Professor Colinera,

15
00:00:35,100 --> 00:00:37,575
will become even more compelling.

16
00:00:37,575 --> 00:00:39,760
The legal analysis framework we will be using in

17
00:00:39,760 --> 00:00:42,520
this course has four parts: issue,

18
00:00:42,520 --> 00:00:45,535
rule, application, and conclusion.

19
00:00:45,535 --> 00:00:48,610
We refer to this framework as IRAC.

20
00:00:48,610 --> 00:00:50,740
It is the traditional method for analyzing

21
00:00:50,740 --> 00:00:53,680
legal problems that arise in any context.

22
00:00:53,680 --> 00:00:56,290
It's also just a time-tested straightforward problem

23
00:00:56,290 --> 00:00:57,760
solving tool that can be applied to

24
00:00:57,760 --> 00:00:59,890
other disciplines to help us work logically

25
00:00:59,890 --> 00:01:03,220
from issue identification to action.

26
00:01:03,220 --> 00:01:04,270
I will explain the parts of

27
00:01:04,270 --> 00:01:06,820
the framework and provide an example of how it

28
00:01:06,820 --> 00:01:10,900
is applied using the target teen pregnancy case in 2012,

29
00:01:10,900 --> 00:01:14,710
which is now somewhat famous for its creepiness factor.

30
00:01:14,710 --> 00:01:17,050
Almost every major retailer from grocery chains,

31
00:01:17,050 --> 00:01:18,670
to investment banks, to the U.S.

32
00:01:18,670 --> 00:01:21,520
Postal Service has a predictive analytics department

33
00:01:21,520 --> 00:01:22,720
devoted to understanding

34
00:01:22,720 --> 00:01:24,745
not just consumer shopping habits,

35
00:01:24,745 --> 00:01:26,930
but also their personal habits,

36
00:01:26,930 --> 00:01:28,990
so as to more efficiently market to them.

37
00:01:28,990 --> 00:01:31,720
The story that got national attention in the U.S.

38
00:01:31,720 --> 00:01:34,420
began when an irritated father at a target in

39
00:01:34,420 --> 00:01:37,360
Minneapolis complained to the store manager

40
00:01:37,360 --> 00:01:38,800
about sending his daughter

41
00:01:38,800 --> 00:01:40,240
a sale booklet for baby clothes,

42
00:01:40,240 --> 00:01:41,445
cribs, and diapers,

43
00:01:41,445 --> 00:01:43,480
even though she was still in high school.

44
00:01:43,480 --> 00:01:45,160
Target apologized and learned

45
00:01:45,160 --> 00:01:47,195
some hard marketing lessons in the process.

46
00:01:47,195 --> 00:01:48,580
But the reality was that the daughter

47
00:01:48,580 --> 00:01:50,195
was, in fact, pregnant.

48
00:01:50,195 --> 00:01:53,710
Target knew about it before the mom and dad did.

49
00:01:53,710 --> 00:01:55,180
Let's look at the scenario through

50
00:01:55,180 --> 00:01:57,125
our legal lens using IRAC.

51
00:01:57,125 --> 00:01:59,170
The first component, the issue,

52
00:01:59,170 --> 00:02:00,610
is defined as a legal problem that

53
00:02:00,610 --> 00:02:02,545
is inherent in the situation.

54
00:02:02,545 --> 00:02:04,210
Spotting a legal issue or issues in

55
00:02:04,210 --> 00:02:07,114
a factual scenario is always our starting point.

56
00:02:07,114 --> 00:02:09,220
Here, the issue might be stated as whether

57
00:02:09,220 --> 00:02:11,500
Target infringed on the teenager's rights by

58
00:02:11,500 --> 00:02:14,735
using data about her purchasing behavior to identify her

59
00:02:14,735 --> 00:02:16,240
as pregnant and to then market to

60
00:02:16,240 --> 00:02:18,780
her appropriate consumer products.

61
00:02:18,780 --> 00:02:20,260
Next is the rule. What if

62
00:02:20,260 --> 00:02:22,752
any rules are relevant to the situation?

63
00:02:22,752 --> 00:02:24,130
Here, the overall situation is

64
00:02:24,130 --> 00:02:26,230
highly personal, even intimate.

65
00:02:26,230 --> 00:02:27,250
Does that mean that there is a

66
00:02:27,250 --> 00:02:29,605
possible privacy rights violation?

67
00:02:29,605 --> 00:02:32,401
If so, because Target is an organization,

68
00:02:32,401 --> 00:02:33,520
versus a part of the government

69
00:02:33,520 --> 00:02:35,314
subject to the constitution,

70
00:02:35,314 --> 00:02:36,490
we would start by considering

71
00:02:36,490 --> 00:02:39,720
common law privacy towards to see if they are relevant.

72
00:02:39,720 --> 00:02:41,290
We will learn more about these privacy

73
00:02:41,290 --> 00:02:42,935
towards in later modules.

74
00:02:42,935 --> 00:02:44,530
We might also research if there are

75
00:02:44,530 --> 00:02:45,940
any statutes on point to

76
00:02:45,940 --> 00:02:48,095
protect her privacy interest here.

77
00:02:48,095 --> 00:02:49,960
Unfortunately for this young teen,

78
00:02:49,960 --> 00:02:52,195
although the algorithm revealed a highly personal,

79
00:02:52,195 --> 00:02:53,890
even intimate facts about her,

80
00:02:53,890 --> 00:02:56,280
there are no privacy laws that protect her.

81
00:02:56,280 --> 00:02:59,350
The data used was provided by her purchasing behavior.

82
00:02:59,350 --> 00:03:01,825
Target used internal customer information,

83
00:03:01,825 --> 00:03:04,330
and externally publicly available data.

84
00:03:04,330 --> 00:03:05,890
In applying the facts here to

85
00:03:05,890 --> 00:03:07,900
the existing rules of privacy,

86
00:03:07,900 --> 00:03:09,300
there is no breach.

87
00:03:09,300 --> 00:03:12,790
Thus, we can conclude that Target behaved lawfully.

88
00:03:12,790 --> 00:03:14,260
That's the conclusion.

89
00:03:14,260 --> 00:03:16,450
However, the fact that Target could legally

90
00:03:16,450 --> 00:03:18,880
learn something private about it's customers,

91
00:03:18,880 --> 00:03:21,573
doesn't mean that it was ethically appropriate.

92
00:03:21,573 --> 00:03:24,860
Just because you can do it, doesn't mean that you should.

