0
00:00:00,090 --> 00:00:03,160
>> There are a variety of types of law that are

1
00:00:03,160 --> 00:00:05,725
relevant in data analytics and AI.

2
00:00:05,725 --> 00:00:07,120
In the U.S, these include

3
00:00:07,120 --> 00:00:09,340
privacy law, consumer protection,

4
00:00:09,340 --> 00:00:10,750
employment discrimination,

5
00:00:10,750 --> 00:00:12,550
negligence due process and

6
00:00:12,550 --> 00:00:14,610
intellectual property, to name some.

7
00:00:14,610 --> 00:00:16,930
These types of law can be classified in and

8
00:00:16,930 --> 00:00:19,261
across three basic sources of U.S. law.

9
00:00:19,261 --> 00:00:21,610
Classifying law by sources helps

10
00:00:21,610 --> 00:00:24,070
us understand the big picture of law as it relates

11
00:00:24,070 --> 00:00:26,410
here to big data and also to better

12
00:00:26,410 --> 00:00:27,700
understand how why the law

13
00:00:27,700 --> 00:00:30,365
applies to issues of technology.

14
00:00:30,365 --> 00:00:32,530
The three principal sources of U.S. law are;

15
00:00:32,530 --> 00:00:34,615
common law or judge made law,

16
00:00:34,615 --> 00:00:38,170
statutory law and constitutional law.

17
00:00:38,170 --> 00:00:40,495
Common law is law created by courts.

18
00:00:40,495 --> 00:00:42,949
In the US and other former British colonies,

19
00:00:42,949 --> 00:00:44,080
judges have authority to

20
00:00:44,080 --> 00:00:45,850
actually create rules of law that

21
00:00:45,850 --> 00:00:48,070
determine individual and organizational rights

22
00:00:48,070 --> 00:00:49,570
and responsibilities.

23
00:00:49,570 --> 00:00:52,060
Certain types of law are traditionally common law.

24
00:00:52,060 --> 00:00:54,520
For example, negligence law which is about

25
00:00:54,520 --> 00:00:57,100
our responsibility to be careful in everything we do,

26
00:00:57,100 --> 00:00:59,290
from driving a car, to hiring employees,

27
00:00:59,290 --> 00:01:02,730
to managing data is sourced in common law.

28
00:01:02,730 --> 00:01:04,735
If I want to know the rules of negligence,

29
00:01:04,735 --> 00:01:07,165
I will research and review court decisions.

30
00:01:07,165 --> 00:01:09,580
Statutory law, by contrast,

31
00:01:09,580 --> 00:01:10,630
is law created by

32
00:01:10,630 --> 00:01:13,550
representative bodies such as the US Congress.

33
00:01:13,550 --> 00:01:15,040
Whereas courts create law in

34
00:01:15,040 --> 00:01:16,995
response to cases brought before them

35
00:01:16,995 --> 00:01:19,660
representative bodies proactively create law

36
00:01:19,660 --> 00:01:22,880
that regulate individual and organizational behavior.

37
00:01:22,880 --> 00:01:24,670
For example, Congress passed

38
00:01:24,670 --> 00:01:27,415
the Genetic Information Nondiscrimination Act

39
00:01:27,415 --> 00:01:28,945
otherwise known as GINA,

40
00:01:28,945 --> 00:01:30,430
to regulate how employers and

41
00:01:30,430 --> 00:01:32,720
insurance companies can if at all,

42
00:01:32,720 --> 00:01:35,860
access our genetic information for purposes of making

43
00:01:35,860 --> 00:01:37,090
decisions about whether we're

44
00:01:37,090 --> 00:01:39,940
employable or whether we're insurable.

45
00:01:39,940 --> 00:01:42,040
Constitutional law, the last source,

46
00:01:42,040 --> 00:01:43,300
gives the government the authority to

47
00:01:43,300 --> 00:01:45,760
act and restricts that authority to ensure that

48
00:01:45,760 --> 00:01:47,920
the branches don't overstepped their bounds or

49
00:01:47,920 --> 00:01:50,824
infringe unnecessarily on individual rights,

50
00:01:50,824 --> 00:01:53,375
such as rights to fairness and equality.

51
00:01:53,375 --> 00:01:55,900
When the government takes actions with respect to data,

52
00:01:55,900 --> 00:01:58,000
it is likely the constitution will have a role

53
00:01:58,000 --> 00:02:01,025
in restricting what the government can and can't do.

54
00:02:01,025 --> 00:02:04,600
Now some types of law outsource in all three sources.

55
00:02:04,600 --> 00:02:06,365
Privacy for example, it's sourced in

56
00:02:06,365 --> 00:02:08,920
common law in what we call privacy torts.

57
00:02:08,920 --> 00:02:12,010
It's sourced in statutory law such as GINA and other

58
00:02:12,010 --> 00:02:13,150
legislation that protects

59
00:02:13,150 --> 00:02:15,865
financial health and student privacy.

60
00:02:15,865 --> 00:02:19,360
And it is sourced in the US constitution amendments.

61
00:02:19,360 --> 00:02:20,860
Now remember, this summary is just

62
00:02:20,860 --> 00:02:23,385
about sources of US law.

63
00:02:23,385 --> 00:02:26,425
If an organization is operating in another country,

64
00:02:26,425 --> 00:02:29,480
that organization is subject to the law of that nation,

65
00:02:29,480 --> 00:02:32,890
its variety and its sources of law.

66
00:02:32,890 --> 00:02:34,810
Recall that one of our principal themes is

67
00:02:34,810 --> 00:02:36,890
that law's grounded in territory.

68
00:02:36,890 --> 00:02:38,350
It is territorial, It's

69
00:02:38,350 --> 00:02:41,455
determined by geographical boundaries.

70
00:02:41,455 --> 00:02:43,450
So finally, we want to look at

71
00:02:43,450 --> 00:02:45,340
what international law is and how that

72
00:02:45,340 --> 00:02:47,500
relates to these three sources

73
00:02:47,500 --> 00:02:49,240
of U.S. law we've been talking about.

74
00:02:49,240 --> 00:02:51,730
International law or global law is really

75
00:02:51,730 --> 00:02:54,700
unique and it's relevant in very specific context,

76
00:02:54,700 --> 00:02:56,380
such as international trade.

77
00:02:56,380 --> 00:02:59,380
For our purposes, there is human rights law that is

78
00:02:59,380 --> 00:03:03,310
international and that informs us about what individual,

79
00:03:03,310 --> 00:03:05,425
civil and political rights are,

80
00:03:05,425 --> 00:03:07,335
which ones are protected,

81
00:03:07,335 --> 00:03:09,570
which ones are valued.

82
00:03:09,570 --> 00:03:10,675
These rights are also

83
00:03:10,675 --> 00:03:14,290
partly enshrined in our U.S. Constitution.

84
00:03:14,290 --> 00:03:15,935
What's common to all of them,

85
00:03:15,935 --> 00:03:17,770
the rights in the U.S. Constitution and

86
00:03:17,770 --> 00:03:20,140
the rights in international human rights law,

87
00:03:20,140 --> 00:03:22,105
is that the cornerstone of

88
00:03:22,105 --> 00:03:25,080
all of these rights is human dignity.

89
00:03:25,080 --> 00:03:26,410
There's the United Nations

90
00:03:26,410 --> 00:03:27,880
Declaration of Human Rights which is

91
00:03:27,880 --> 00:03:31,090
signed after World War Two and several related treaties.

92
00:03:31,090 --> 00:03:33,190
However these documents are not per se

93
00:03:33,190 --> 00:03:35,440
binding and to the extent that they are binding,

94
00:03:35,440 --> 00:03:37,000
they are binding on governments

95
00:03:37,000 --> 00:03:40,230
not private organisational parties.

