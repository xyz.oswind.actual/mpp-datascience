0
00:00:02,490 --> 00:00:07,805
>> So let's talk a minute about how this applies to data.

1
00:00:07,805 --> 00:00:10,210
Well, if ethics is the study of

2
00:00:10,210 --> 00:00:12,860
human well-being, data ethics,

3
00:00:12,860 --> 00:00:14,470
we get a nice tidy definition,

4
00:00:14,470 --> 00:00:18,350
is just the study of how data affects human well-being.

5
00:00:18,350 --> 00:00:20,005
And let me tell you, it affects

6
00:00:20,005 --> 00:00:22,690
human well-being in a lot of positive ways,

7
00:00:22,690 --> 00:00:24,470
in a lot of negative ways.

8
00:00:24,470 --> 00:00:25,690
We can talk about this from

9
00:00:25,690 --> 00:00:29,800
a process side: data is collected,

10
00:00:29,800 --> 00:00:33,610
stored, processed, visualized, used in decision making.

11
00:00:33,610 --> 00:00:36,225
And it turns out that in each of those five steps,

12
00:00:36,225 --> 00:00:38,254
there's specific things to talk about,

13
00:00:38,254 --> 00:00:40,120
about how human well-being is

14
00:00:40,120 --> 00:00:43,850
affected either positively or negatively.

15
00:00:43,850 --> 00:00:45,730
We can also talk about this from

16
00:00:45,730 --> 00:00:48,320
the perspective of who is being affected.

17
00:00:48,320 --> 00:00:49,840
We can talk about individuals,

18
00:00:49,840 --> 00:00:51,355
we'll do that in module two.

19
00:00:51,355 --> 00:00:55,180
We can talk about groups and societies and citizens.

20
00:00:55,180 --> 00:00:56,795
That's where we're going to do in module three.

21
00:00:56,795 --> 00:00:57,940
We can also talk about

22
00:00:57,940 --> 00:01:00,655
customers and employees and businesses.

23
00:01:00,655 --> 00:01:02,710
This is what we'll do in module four.

24
00:01:02,710 --> 00:01:04,115
There's all these dimensions,

25
00:01:04,115 --> 00:01:05,470
all these groups that are affected in

26
00:01:05,470 --> 00:01:07,830
different ways by the use of data.

27
00:01:07,830 --> 00:01:08,985
Their well-being is affected

28
00:01:08,985 --> 00:01:11,405
and that's what makes it ethical.

29
00:01:11,405 --> 00:01:14,570
Now, a previous video

30
00:01:14,570 --> 00:01:16,970
we talked about how data is actually a very old thing.

31
00:01:16,970 --> 00:01:18,160
It just means recorded facts.

32
00:01:18,160 --> 00:01:20,675
So markings on a cave wall count as data.

33
00:01:20,675 --> 00:01:23,410
But now we're living in this age of big data.

34
00:01:23,410 --> 00:01:26,120
So, whatever this data ethics thing was

35
00:01:26,120 --> 00:01:29,635
that data ethics has existed as long as data has.

36
00:01:29,635 --> 00:01:32,375
Now, all those problems are still with that

37
00:01:32,375 --> 00:01:34,985
but because data is big there is more problems,

38
00:01:34,985 --> 00:01:38,570
and they're also sometimes more complicated.

39
00:01:38,570 --> 00:01:39,890
So in this course we want to

40
00:01:39,890 --> 00:01:42,520
talk about some of those issues.

