0
00:00:00,020 --> 00:00:02,280
The age of big data will

1
00:00:02,280 --> 00:00:04,376
bring a socioeconomic revolution,

2
00:00:04,376 --> 00:00:06,780
because over a very short period of time,

3
00:00:06,780 --> 00:00:09,285
big data will reorganize the way we live,

4
00:00:09,285 --> 00:00:11,055
work and interact socially.

5
00:00:11,055 --> 00:00:12,990
Much like the Agrarian revolution

6
00:00:12,990 --> 00:00:15,100
and Industrial revolution did.

7
00:00:15,100 --> 00:00:17,235
This revolution is inevitable,

8
00:00:17,235 --> 00:00:19,725
because of the incomprehensible amount of data

9
00:00:19,725 --> 00:00:21,030
suddenly available about

10
00:00:21,030 --> 00:00:23,115
human activity in the things we use.

11
00:00:23,115 --> 00:00:26,055
The technology necessary to collect, store,

12
00:00:26,055 --> 00:00:28,680
process and analyze this raw data in

13
00:00:28,680 --> 00:00:31,350
order to make it useful for gaining insights,

14
00:00:31,350 --> 00:00:32,700
data mining it's called,

15
00:00:32,700 --> 00:00:35,730
is quickly increasing in sophistication.

16
00:00:35,730 --> 00:00:38,280
Businesses, governments and other organizations are

17
00:00:38,280 --> 00:00:39,780
scrambling to take advantage

18
00:00:39,780 --> 00:00:41,900
of these technological advancements.

19
00:00:41,900 --> 00:00:44,535
With due respect to King Solomon's great wisdom,

20
00:00:44,535 --> 00:00:47,865
his proclamation that there is nothing new under the sun,

21
00:00:47,865 --> 00:00:50,335
is getting a run for its money.

22
00:00:50,335 --> 00:00:52,880
The data revolution is coming and it will

23
00:00:52,880 --> 00:00:55,535
benefit a lot of people in a lot of ways.

24
00:00:55,535 --> 00:00:57,200
We are data optimists,

25
00:00:57,200 --> 00:00:58,985
which means among other things,

26
00:00:58,985 --> 00:01:00,230
that we are excited to see

27
00:01:00,230 --> 00:01:02,360
the changes that big data will bring.

28
00:01:02,360 --> 00:01:04,820
Enormous industries such as healthcare,

29
00:01:04,820 --> 00:01:07,040
transportation and criminal justice,

30
00:01:07,040 --> 00:01:08,615
will become more intelligent,

31
00:01:08,615 --> 00:01:10,800
more efficient and more accurate.

32
00:01:10,800 --> 00:01:13,025
Many needless harms of the past that were

33
00:01:13,025 --> 00:01:16,335
ultimately rooted in a lack of data, will be eliminated.

34
00:01:16,335 --> 00:01:17,750
From an ethical perspective,

35
00:01:17,750 --> 00:01:19,980
this coming benefit is welcome.

36
00:01:19,980 --> 00:01:22,210
But like all revolutions,

37
00:01:22,210 --> 00:01:24,965
this one will have both winners and losers.

38
00:01:24,965 --> 00:01:26,410
You don't need a crystal ball to

39
00:01:26,410 --> 00:01:28,235
see that while many will benefit,

40
00:01:28,235 --> 00:01:31,240
the data revolution will inflict harms in two ways.

41
00:01:31,240 --> 00:01:34,000
It will give bad people new ways to be bad and will

42
00:01:34,000 --> 00:01:37,980
also inflict harm in unintended and unforeseen ways.

43
00:01:37,980 --> 00:01:40,495
Considering this mix of benefits and harms,

44
00:01:40,495 --> 00:01:44,440
there are four possible responses to the data revolution.

45
00:01:44,440 --> 00:01:46,420
One response is a desire to return to

46
00:01:46,420 --> 00:01:49,995
a pre-digital age or to stop technological progress,

47
00:01:49,995 --> 00:01:53,345
but short of some severe global and permanent crisis.

48
00:01:53,345 --> 00:01:55,535
This is simply not realistic.

49
00:01:55,535 --> 00:01:58,220
The world is not going back to small data.

50
00:01:58,220 --> 00:02:01,640
A second response is unchecked optimism.

51
00:02:01,640 --> 00:02:03,700
Some people often embrace new technology

52
00:02:03,700 --> 00:02:06,025
simply because it is new and cool.

53
00:02:06,025 --> 00:02:07,865
But this is also a mistake.

54
00:02:07,865 --> 00:02:09,940
We believe that there will be coming harms and to

55
00:02:09,940 --> 00:02:12,980
ignore these is irresponsible.

56
00:02:12,980 --> 00:02:14,525
A third response is to feel

57
00:02:14,525 --> 00:02:17,135
uneasy about the big data revolution,

58
00:02:17,135 --> 00:02:19,350
but accept it as inevitable.

59
00:02:19,350 --> 00:02:22,220
We all hear regular people voice this position.

60
00:02:22,220 --> 00:02:23,690
When someone mentions how this or

61
00:02:23,690 --> 00:02:26,055
that new technology is a bit creepy.

62
00:02:26,055 --> 00:02:27,290
And then in the next sentence you

63
00:02:27,290 --> 00:02:29,385
find out they use it anyway.

64
00:02:29,385 --> 00:02:31,850
We prefer a fourth response which is to

65
00:02:31,850 --> 00:02:34,145
accept the revolution as inevitable,

66
00:02:34,145 --> 00:02:38,260
but to believe the revolution can and should be designed.

67
00:02:38,260 --> 00:02:39,290
It might help to think of

68
00:02:39,290 --> 00:02:41,240
Kevin Kelly's use of the word inevitable,

69
00:02:41,240 --> 00:02:44,480
in his two thousand sixteen book by the same name.

70
00:02:44,480 --> 00:02:46,130
Inevitability in this sense,

71
00:02:46,130 --> 00:02:47,870
allows for the possibility that

72
00:02:47,870 --> 00:02:50,205
the specifics might be otherwise.

73
00:02:50,205 --> 00:02:52,280
For example, we might say that

74
00:02:52,280 --> 00:02:54,890
given that the human species is social,

75
00:02:54,890 --> 00:02:57,650
it was always inevitable that the digital world,

76
00:02:57,650 --> 00:03:00,130
would eventually give us social media.

77
00:03:00,130 --> 00:03:03,290
But while social media itself is inevitable,

78
00:03:03,290 --> 00:03:05,300
it's not inevitable that, Twitter,

79
00:03:05,300 --> 00:03:06,710
Facebook and Instagram would be

80
00:03:06,710 --> 00:03:08,435
the biggest social media platforms

81
00:03:08,435 --> 00:03:09,860
or that they would have developed

82
00:03:09,860 --> 00:03:12,260
the policies and procedures they did.

83
00:03:12,260 --> 00:03:15,980
That just happened. We believe strongly that while it

84
00:03:15,980 --> 00:03:17,630
is not possible or desirable

85
00:03:17,630 --> 00:03:19,475
to stop the big data revolution,

86
00:03:19,475 --> 00:03:21,860
it is our responsibility to consider the kinds

87
00:03:21,860 --> 00:03:24,580
of harm that the use of data can inflict.

88
00:03:24,580 --> 00:03:26,045
And once we have identified them,

89
00:03:26,045 --> 00:03:27,380
we must do what we can,

90
00:03:27,380 --> 00:03:29,420
to design the revolution so that

91
00:03:29,420 --> 00:03:31,190
the predicted benefits of big data will

92
00:03:31,190 --> 00:03:33,460
come to complete fruition.

93
00:03:33,460 --> 00:03:36,570
It is thoughtful considerate design of the revolution.

94
00:03:36,570 --> 00:03:39,440
That is one of the biggest challenges of our generation.

95
00:03:39,440 --> 00:03:43,065
Just letting it happen is a recipe for disaster.

96
00:03:43,065 --> 00:03:45,850
How is such design possible?

97
00:03:45,850 --> 00:03:48,340
We believe that the two most important tools

98
00:03:48,340 --> 00:03:49,855
for designing this revolution,

99
00:03:49,855 --> 00:03:51,310
are bodies of knowledge that have

100
00:03:51,310 --> 00:03:53,070
been with us for millennia.

101
00:03:53,070 --> 00:03:55,560
Principles of law and principles of ethics.

102
00:03:55,560 --> 00:03:56,910
We hope that this course shows

103
00:03:56,910 --> 00:03:59,695
how these time tested principles are relevant guides,

104
00:03:59,695 --> 00:04:02,280
in this new human era.

