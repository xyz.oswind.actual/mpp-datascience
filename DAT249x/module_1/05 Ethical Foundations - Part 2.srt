0
00:00:00,000 --> 00:00:03,087
>> So, let's talk about those values.

1
00:00:03,087 --> 00:00:05,460
And unlike the theories and

2
00:00:05,460 --> 00:00:08,085
the principles that they come from,

3
00:00:08,085 --> 00:00:12,755
partly we already understand these values,

4
00:00:12,755 --> 00:00:14,210
but they are things that we

5
00:00:14,210 --> 00:00:15,680
can get in our minds and think

6
00:00:15,680 --> 00:00:19,665
about and constantly review.

7
00:00:19,665 --> 00:00:23,310
So, there are two sets of values.

8
00:00:23,310 --> 00:00:25,270
The first set are values that

9
00:00:25,270 --> 00:00:27,325
are based on the well-being of others,

10
00:00:27,325 --> 00:00:29,895
well-being of other people.

11
00:00:29,895 --> 00:00:31,730
There are three, as I count them.

12
00:00:31,730 --> 00:00:34,895
So, one is why should I care about another person?

13
00:00:34,895 --> 00:00:36,625
What makes another person

14
00:00:36,625 --> 00:00:38,740
or other people morally valuable?

15
00:00:38,740 --> 00:00:41,950
So, one observation is that what makes people valuable,

16
00:00:41,950 --> 00:00:44,610
is that they are capable of suffering.

17
00:00:44,610 --> 00:00:46,450
They can feel pain, physical,

18
00:00:46,450 --> 00:00:48,250
emotional, if there's other kinds

19
00:00:48,250 --> 00:00:50,570
of pain that too, that would all count.

20
00:00:50,570 --> 00:00:52,180
Alright? I should care about other people

21
00:00:52,180 --> 00:00:53,830
just because they can suffer.

22
00:00:53,830 --> 00:00:55,780
Because they will suffer, it means I have to think

23
00:00:55,780 --> 00:00:58,510
about them before I act.

24
00:00:58,510 --> 00:01:02,920
Also autonomy- autonomy just means

25
00:01:02,920 --> 00:01:04,555
something like people are

26
00:01:04,555 --> 00:01:07,630
able to control their own actions,

27
00:01:07,630 --> 00:01:08,860
to make their own plans,

28
00:01:08,860 --> 00:01:10,985
to live life the way they want to.

29
00:01:10,985 --> 00:01:14,041
Now obviously, there's going to be limits to that,

30
00:01:14,041 --> 00:01:16,796
but the fact that other people are autonomous,

31
00:01:16,796 --> 00:01:18,070
the fact that other people can

32
00:01:18,070 --> 00:01:20,140
control themselves and think

33
00:01:20,140 --> 00:01:21,670
about problems and do things the

34
00:01:21,670 --> 00:01:23,935
way they want to do them,

35
00:01:23,935 --> 00:01:25,495
already that means that

36
00:01:25,495 --> 00:01:26,980
I have to respect that about them.

37
00:01:26,980 --> 00:01:29,200
It would be wrong for me to use them because

38
00:01:29,200 --> 00:01:31,850
they already have their own agenda.

39
00:01:31,850 --> 00:01:33,395
Equality.

40
00:01:33,395 --> 00:01:35,145
That would be another reason

41
00:01:35,145 --> 00:01:36,930
that I should care about other people

42
00:01:36,930 --> 00:01:39,015
because I'm not superior to them

43
00:01:39,015 --> 00:01:41,160
or inferior to them they are equal to me.

44
00:01:41,160 --> 00:01:46,020
So, when I act, I have to respect that equality.

45
00:01:46,020 --> 00:01:48,755
There's another set of values.

46
00:01:48,755 --> 00:01:50,160
These values are not

47
00:01:50,160 --> 00:01:52,140
strictly based on other people's well-being,

48
00:01:52,140 --> 00:01:55,325
although they are in the end.

49
00:01:55,325 --> 00:01:56,460
But in the first place,

50
00:01:56,460 --> 00:01:58,475
this is about my own well-being,

51
00:01:58,475 --> 00:02:01,630
this is about what makes life go well for me.

52
00:02:01,630 --> 00:02:04,690
So, one of these values is

53
00:02:04,690 --> 00:02:06,790
character excellence or virtue

54
00:02:06,790 --> 00:02:09,245
it's called in the ethics literature.

55
00:02:09,245 --> 00:02:13,900
This just means that part of being a good person,

56
00:02:13,900 --> 00:02:15,190
the kind of person I want to be,

57
00:02:15,190 --> 00:02:18,450
is to having a certain kind of character.

58
00:02:18,450 --> 00:02:22,025
The other value here is trust.

59
00:02:22,025 --> 00:02:24,475
Trust is something that makes my life go well

60
00:02:24,475 --> 00:02:27,712
when I'm in a high trust environment,

61
00:02:27,712 --> 00:02:30,575
many of the things that I want can happen.

62
00:02:30,575 --> 00:02:31,780
If there's no trust

63
00:02:31,780 --> 00:02:33,820
or if you're in a society that doesn't have

64
00:02:33,820 --> 00:02:35,560
very much trust or if you have

65
00:02:35,560 --> 00:02:38,479
the reputation of a liar and people don't trust you,

66
00:02:38,479 --> 00:02:41,050
your life's not going to go as well.

67
00:02:41,050 --> 00:02:44,490
So, I believe that these things,

68
00:02:44,490 --> 00:02:47,300
and many ethicists do as well, count as ethics.

69
00:02:47,300 --> 00:02:49,125
So, both the well-being of others

70
00:02:49,125 --> 00:02:52,625
and your own well-being.

71
00:02:52,625 --> 00:02:55,195
So, to illustrate how these values work together,

72
00:02:55,195 --> 00:02:57,250
we're going to do a little thought experiment.

73
00:02:57,250 --> 00:02:59,800
This is a purposely absurd thought experiment.

74
00:02:59,800 --> 00:03:02,855
Hope you will never hopefully actually ask yourself this.

75
00:03:02,855 --> 00:03:05,935
But let's say for the sake of being extreme,

76
00:03:05,935 --> 00:03:07,930
you're asking yourself should

77
00:03:07,930 --> 00:03:09,550
I torture an innocent person,

78
00:03:09,550 --> 00:03:12,790
not like to find the secret bomb or something like that.

79
00:03:12,790 --> 00:03:14,650
Not even talking about that.

80
00:03:14,650 --> 00:03:16,750
I just mean like torturing someone for fun.

81
00:03:16,750 --> 00:03:18,215
Should I do that?

82
00:03:18,215 --> 00:03:19,926
That's what you're asking yourself.

83
00:03:19,926 --> 00:03:23,383
Well, I hope that you would say no,

84
00:03:23,383 --> 00:03:26,137
that's the obvious answer.

85
00:03:26,137 --> 00:03:30,520
And there are a few reasons that you might come up with.

86
00:03:30,520 --> 00:03:32,740
>> And all these reasons are ultimately

87
00:03:32,740 --> 00:03:35,581
rooted in the values that we just talked about.

88
00:03:35,581 --> 00:03:37,480
Torturing another person would

89
00:03:37,480 --> 00:03:40,410
cause them suffering, and that's bad.

90
00:03:40,410 --> 00:03:42,460
Torturing another person would be not

91
00:03:42,460 --> 00:03:45,370
respecting their autonomy, and that's bad.

92
00:03:45,370 --> 00:03:47,530
Torturing another person would be treating them as

93
00:03:47,530 --> 00:03:50,350
inferior to you, and that's bad.

94
00:03:50,350 --> 00:03:53,920
Torturing another person would ruin my character.

95
00:03:53,920 --> 00:03:58,120
You can ask some of the soldiers in the past,

96
00:03:58,120 --> 00:03:59,140
in past wars who have been

97
00:03:59,140 --> 00:04:01,715
forced to torture their prisoners.

98
00:04:01,715 --> 00:04:06,075
They talk about how that really affected their character.

99
00:04:06,075 --> 00:04:07,660
If I torture other people,

100
00:04:07,660 --> 00:04:09,880
that's going to destroy the trust and

101
00:04:09,880 --> 00:04:12,670
that relationship certainly and maybe social trust

102
00:04:12,670 --> 00:04:14,635
if it just becomes normal for people to

103
00:04:14,635 --> 00:04:17,868
randomly torture each other.

104
00:04:17,868 --> 00:04:21,310
So, that's our thought experiment. Don't torture.

105
00:04:21,310 --> 00:04:23,795
It's not a good idea at all.

106
00:04:23,795 --> 00:04:24,920
Let's ask ourselves what we

107
00:04:24,920 --> 00:04:27,390
learn from that thought experiment.

108
00:04:27,390 --> 00:04:30,440
The first thing we learned is that engaging in

109
00:04:30,440 --> 00:04:33,190
an ethical theory is not complicated.

110
00:04:33,190 --> 00:04:35,240
So, remember the complicated part is talking

111
00:04:35,240 --> 00:04:37,835
about and comparing and contrasting all those theories.

112
00:04:37,835 --> 00:04:41,730
But you didn't need me to walk you through that,

113
00:04:41,730 --> 00:04:43,340
whether you should torture or other people,

114
00:04:43,340 --> 00:04:44,640
you already knew that.

115
00:04:44,640 --> 00:04:47,850
I just gave you some specific reasons based on values.

116
00:04:47,850 --> 00:04:51,290
So, it's not complicated to engage in ethical reasoning.

117
00:04:51,290 --> 00:04:53,420
Now, maybe the best conclusions are there are going to

118
00:04:53,420 --> 00:04:56,450
be cases that are more difficult.

119
00:04:56,450 --> 00:04:57,830
And that's okay.

120
00:04:57,830 --> 00:05:02,290
But just engaging in the process is not complicated.

121
00:05:02,290 --> 00:05:03,670
The second thing we learned

122
00:05:03,670 --> 00:05:05,560
is that if you're an ethical person,

123
00:05:05,560 --> 00:05:08,200
you appreciate all of those values.

124
00:05:08,200 --> 00:05:11,530
Think about how absurd it would be if someone said well,

125
00:05:11,530 --> 00:05:13,060
I really care about the suffering of

126
00:05:13,060 --> 00:05:14,875
other people that's why I don't torture,

127
00:05:14,875 --> 00:05:17,262
but I don't care about their equality.

128
00:05:17,262 --> 00:05:19,795
That would be such a strange thing to hear.

129
00:05:19,795 --> 00:05:21,280
An ethical person, a person that's

130
00:05:21,280 --> 00:05:22,975
concerned with human well-being,

131
00:05:22,975 --> 00:05:24,370
is going to take all five of

132
00:05:24,370 --> 00:05:26,200
those values into consideration.

133
00:05:26,200 --> 00:05:29,670
The last thing we learned,

134
00:05:29,670 --> 00:05:31,940
and this is implied in the thought experiment,

135
00:05:31,940 --> 00:05:35,300
that the reason you wanted to apply those values is

136
00:05:35,300 --> 00:05:37,100
because you cared about other

137
00:05:37,100 --> 00:05:40,720
people's well-being in the first place.

138
00:05:40,720 --> 00:05:42,310
There's nothing I can really do.

139
00:05:42,310 --> 00:05:44,495
If you don't care about human well-being,

140
00:05:44,495 --> 00:05:48,720
there's no ethics that can really get you to care.

141
00:05:48,720 --> 00:05:49,930
But ethics,

142
00:05:49,930 --> 00:05:51,380
applying those values depending

143
00:05:51,380 --> 00:05:53,000
on you caring in the first place.

