0
00:00:00,050 --> 00:00:03,195
>> It may be tempting to think of data as

1
00:00:03,195 --> 00:00:05,960
alphanumeric digital expressions of some kind.

2
00:00:05,960 --> 00:00:08,250
But that's only one kind of data.

3
00:00:08,250 --> 00:00:10,080
Data are simply recorded facts,

4
00:00:10,080 --> 00:00:11,190
and they are neutral with

5
00:00:11,190 --> 00:00:13,020
regard to what counts as a fact,

6
00:00:13,020 --> 00:00:15,290
and how those facts are recorded.

7
00:00:15,290 --> 00:00:17,160
One application of this idea is

8
00:00:17,160 --> 00:00:19,050
that data have existed from the moment

9
00:00:19,050 --> 00:00:20,880
our ancient ancestors had

10
00:00:20,880 --> 00:00:24,295
the ability to record facts and found it useful to do so.

11
00:00:24,295 --> 00:00:26,730
Some very early examples of data creation

12
00:00:26,730 --> 00:00:29,160
could be a symbol etched on a cave wall,

13
00:00:29,160 --> 00:00:31,170
or scratchings on a tree indicating

14
00:00:31,170 --> 00:00:34,170
the number of necessary foot paces to a burial site.

15
00:00:34,170 --> 00:00:36,945
For most of this age data recording would have been

16
00:00:36,945 --> 00:00:40,250
done by hand or by some related physical means.

17
00:00:40,250 --> 00:00:42,245
In the middle of the 19th century,

18
00:00:42,245 --> 00:00:43,410
a new kind of data became

19
00:00:43,410 --> 00:00:45,210
possible when scientists discovered

20
00:00:45,210 --> 00:00:48,750
different ways to record sound using analog technology.

21
00:00:48,750 --> 00:00:50,460
Recorded images have

22
00:00:50,460 --> 00:00:52,275
an even more complicated history with

23
00:00:52,275 --> 00:00:54,060
even ancient societies using

24
00:00:54,060 --> 00:00:56,415
principles of light to record images.

25
00:00:56,415 --> 00:00:58,290
For our purposes we might put

26
00:00:58,290 --> 00:00:59,880
these and other technologies together

27
00:00:59,880 --> 00:01:03,266
and call them the pre-digital age of data.

28
00:01:03,266 --> 00:01:06,645
In the last couple of decades the term, Big Data,

29
00:01:06,645 --> 00:01:08,970
has been increasing in popularity.

30
00:01:08,970 --> 00:01:10,020
There are some who don't like

31
00:01:10,020 --> 00:01:12,180
the term because it's not technical.

32
00:01:12,180 --> 00:01:14,700
They might note that big is a relative concept,

33
00:01:14,700 --> 00:01:17,825
and so what counts as big today will not be big tomorrow.

34
00:01:17,825 --> 00:01:19,755
There are also attempts to define

35
00:01:19,755 --> 00:01:21,510
the term Big Data along

36
00:01:21,510 --> 00:01:24,400
the lines of the features and benefits of data.

37
00:01:24,400 --> 00:01:26,265
For example you've probably heard of

38
00:01:26,265 --> 00:01:28,890
the so-called five V's of Big Data,

39
00:01:28,890 --> 00:01:31,350
but we don't need to get too technical to

40
00:01:31,350 --> 00:01:33,495
understand the most fundamental difference

41
00:01:33,495 --> 00:01:35,380
of this new age of data.

42
00:01:35,380 --> 00:01:37,095
It is right to call data big,

43
00:01:37,095 --> 00:01:40,165
simply because there is so much of it.

44
00:01:40,165 --> 00:01:42,780
One estimate puts the amount of all existing data,

45
00:01:42,780 --> 00:01:46,095
from the beginning of recorded history until 2003,

46
00:01:46,095 --> 00:01:48,645
at 5 billion gigabytes.

47
00:01:48,645 --> 00:01:50,460
That might sound like a big number,

48
00:01:50,460 --> 00:01:52,860
except the same study estimates that,

49
00:01:52,860 --> 00:01:54,060
just a few years later,

50
00:01:54,060 --> 00:01:56,595
by 2011 the amount of data

51
00:01:56,595 --> 00:02:00,640
generated every two days was 5 billion gigabytes.

52
00:02:00,640 --> 00:02:02,130
Probably the best place to start

53
00:02:02,130 --> 00:02:03,630
the story of Big Data is the late

54
00:02:03,630 --> 00:02:06,795
1940's when the transistor was invented,

55
00:02:06,795 --> 00:02:10,035
which for reasons far too technical to describe here,

56
00:02:10,035 --> 00:02:13,590
allowed us for the first time to record facts digitally.

57
00:02:13,590 --> 00:02:16,610
That is, as encoded in ones and zeros.

58
00:02:16,610 --> 00:02:19,270
This is the digital age.

59
00:02:19,270 --> 00:02:20,670
This is important in explaining

60
00:02:20,670 --> 00:02:22,185
this massive increase in data,

61
00:02:22,185 --> 00:02:24,465
because non digital data storage

62
00:02:24,465 --> 00:02:26,505
is remarkably inefficient.

63
00:02:26,505 --> 00:02:28,410
Yes, you can create data by

64
00:02:28,410 --> 00:02:30,850
writing down someone's name on a piece of paper,

65
00:02:30,850 --> 00:02:32,310
but that requires a lot of

66
00:02:32,310 --> 00:02:35,830
space and does not record a lot of data.

67
00:02:35,830 --> 00:02:40,084
Digital storage partly explains why data got so big.

68
00:02:40,084 --> 00:02:42,270
The other part of the equation has to do

69
00:02:42,270 --> 00:02:45,720
with who or what is recording the data.

70
00:02:45,720 --> 00:02:48,670
When a human being enters data into a database,

71
00:02:48,670 --> 00:02:50,130
there is a natural barrier to

72
00:02:50,130 --> 00:02:51,898
how much data can be recorded,

73
00:02:51,898 --> 00:02:54,135
simply because of the biological limits

74
00:02:54,135 --> 00:02:56,250
of human recording abilities.

75
00:02:56,250 --> 00:02:58,950
But now, data about you and me is

76
00:02:58,950 --> 00:03:01,950
recorded automatically, by machines.

77
00:03:01,950 --> 00:03:03,665
To take an easy example,

78
00:03:03,665 --> 00:03:05,745
your smartphone records your location

79
00:03:05,745 --> 00:03:06,975
every second of the day.

80
00:03:06,975 --> 00:03:08,550
All the websites you visit,

81
00:03:08,550 --> 00:03:09,630
songs you listen to,

82
00:03:09,630 --> 00:03:12,270
texts you send, et cetera.

83
00:03:12,270 --> 00:03:14,150
This means that you alone,

84
00:03:14,150 --> 00:03:16,800
even on your most unremarkable day,

85
00:03:16,800 --> 00:03:20,150
create a mind numbing amount of data.

86
00:03:20,150 --> 00:03:22,575
Now, all this data is recorded in databases,

87
00:03:22,575 --> 00:03:24,270
but not by a human.

88
00:03:24,270 --> 00:03:26,460
No person is furiously updating

89
00:03:26,460 --> 00:03:29,383
your location in a database each time it changes.

90
00:03:29,383 --> 00:03:33,305
Rather, a machine records it all automatically.

91
00:03:33,305 --> 00:03:34,530
So what is important is that we

92
00:03:34,530 --> 00:03:36,360
have crossed two thresholds.

93
00:03:36,360 --> 00:03:37,770
The first, in the ability to

94
00:03:37,770 --> 00:03:39,510
store data in ones and zeros,

95
00:03:39,510 --> 00:03:41,310
and the second, in the advent of

96
00:03:41,310 --> 00:03:43,860
machines automatically recording that data.

97
00:03:43,860 --> 00:03:45,420
Crossing the first threshold

98
00:03:45,420 --> 00:03:47,130
took us into the digital age,

99
00:03:47,130 --> 00:03:49,140
and crossing the second threshold took

100
00:03:49,140 --> 00:03:52,240
us fully into the age of Big Data.

