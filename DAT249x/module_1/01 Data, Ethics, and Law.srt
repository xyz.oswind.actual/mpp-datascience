0
00:00:00,040 --> 00:00:02,770
>> Analytics and artificial intelligence

1
00:00:02,770 --> 00:00:04,840
it's pretty evident that they're powerful.

2
00:00:04,840 --> 00:00:07,595
They're means of amazing digital transformation.

3
00:00:07,595 --> 00:00:10,120
We have things like almost instant communication

4
00:00:10,120 --> 00:00:11,380
between people who are speaking

5
00:00:11,380 --> 00:00:13,810
different languages and self-driving cars

6
00:00:13,810 --> 00:00:16,590
coming to market, predictive health care.

7
00:00:16,590 --> 00:00:19,090
Almost every industry and vertical is being touched by

8
00:00:19,090 --> 00:00:22,125
this advancement in data and technology.

9
00:00:22,125 --> 00:00:23,590
But, in some ways,

10
00:00:23,590 --> 00:00:26,650
it kind of begs the question of why ethics

11
00:00:26,650 --> 00:00:29,950
and law in relation to these powerful advancements?

12
00:00:29,950 --> 00:00:31,630
What does it bring to the table,

13
00:00:31,630 --> 00:00:34,600
and what are students going to learn in this module?

14
00:00:34,600 --> 00:00:36,705
>> That's a great question.

15
00:00:36,705 --> 00:00:40,480
Actually, typically, when there is innovation,

16
00:00:40,480 --> 00:00:44,700
law and ethics are chasing after the innovation.

17
00:00:44,700 --> 00:00:46,010
There's a tension.

18
00:00:46,010 --> 00:00:48,265
Law can never move as fast as business.

19
00:00:48,265 --> 00:00:51,845
Law can never move as fast as technology.

20
00:00:51,845 --> 00:00:55,590
And rather than wait for the law to eventually catch up,

21
00:00:55,590 --> 00:00:58,912
which it will, we can be prepared.

22
00:00:58,912 --> 00:01:00,840
So there's a couple of things happening here.

23
00:01:00,840 --> 00:01:03,690
I mean, we're looking at law, and law and ethics,

24
00:01:03,690 --> 00:01:05,550
the relationship between the two of them really are that

25
00:01:05,550 --> 00:01:07,500
the law is going to be a moral minimum.

26
00:01:07,500 --> 00:01:09,615
Ethics is always going to be a higher bar,

27
00:01:09,615 --> 00:01:11,250
and Nathan will be

28
00:01:11,250 --> 00:01:14,115
providing ethical frameworks in this module for us.

29
00:01:14,115 --> 00:01:15,833
I'm going to be providing legal frameworks.

30
00:01:15,833 --> 00:01:18,120
And the point of learning those is to prepare

31
00:01:18,120 --> 00:01:21,180
students to know what the law is,

32
00:01:21,180 --> 00:01:25,050
what law does exist so that they can use

33
00:01:25,050 --> 00:01:27,000
it creatively and not be waiting

34
00:01:27,000 --> 00:01:29,240
for the hammer to fall, necessarily.

35
00:01:29,240 --> 00:01:30,065
>> Yeah, almost they got

36
00:01:30,065 --> 00:01:31,530
a competitive advantage of some sort.

37
00:01:31,530 --> 00:01:33,420
>> Yeah. It is a competitive advantage,

38
00:01:33,420 --> 00:01:36,345
and we teach our students constantly that

39
00:01:36,345 --> 00:01:38,445
we're not talking about law and

40
00:01:38,445 --> 00:01:41,485
ethics so that they can feel constrained.

41
00:01:41,485 --> 00:01:43,935
We're teaching principles of law and

42
00:01:43,935 --> 00:01:46,845
ethics so that they can use that creatively,

43
00:01:46,845 --> 00:01:48,935
progressively, creatively,

44
00:01:48,935 --> 00:01:52,245
innovatively and be prepared and ahead of the game.

45
00:01:52,245 --> 00:01:53,670
So they can be competitive,

46
00:01:53,670 --> 00:01:57,765
strategic and also, at the same time, be doing good.

47
00:01:57,765 --> 00:02:00,150
>> That's actually a great point about getting out

48
00:02:00,150 --> 00:02:02,895
ahead of the technology.

49
00:02:02,895 --> 00:02:07,470
I mean, really, what we're talking about now is design.

50
00:02:07,470 --> 00:02:10,800
So it reminds me of two of

51
00:02:10,800 --> 00:02:12,780
the best thinkers working in

52
00:02:12,780 --> 00:02:15,685
this environmental sustainability space,

53
00:02:15,685 --> 00:02:17,235
are McDonough and Braungart.

54
00:02:17,235 --> 00:02:19,470
They are more well known for their books,

55
00:02:19,470 --> 00:02:21,420
"The Upcycle" and "Cradle to Cradle".

56
00:02:21,420 --> 00:02:23,665
They have an earlier academic paper,

57
00:02:23,665 --> 00:02:25,440
where they make the case that

58
00:02:25,440 --> 00:02:28,980
our ecological crisis that we have right now.

59
00:02:28,980 --> 00:02:31,230
I mean, think about how much of a pain it is

60
00:02:31,230 --> 00:02:33,875
that we're trying to go around cleaning up problems.

61
00:02:33,875 --> 00:02:35,475
And they argue that

62
00:02:35,475 --> 00:02:38,355
back when the industrial revolution started,

63
00:02:38,355 --> 00:02:40,260
there was evidence that it

64
00:02:40,260 --> 00:02:42,235
wasn't designed for sustainability.

65
00:02:42,235 --> 00:02:44,085
The Industrial Revolution wasn't,

66
00:02:44,085 --> 00:02:45,255
because no one had

67
00:02:45,255 --> 00:02:48,950
any idea that humans could damage the environment.

68
00:02:48,950 --> 00:02:50,124
It was like the environment is

69
00:02:50,124 --> 00:02:53,305
this big huge thing that can just eat up human activity.

70
00:02:53,305 --> 00:02:56,040
Now, it turns out that prediction

71
00:02:56,040 --> 00:02:59,055
or that assumption was quite wrong.

72
00:02:59,055 --> 00:03:00,485
But the point is that,

73
00:03:00,485 --> 00:03:04,110
in the environmental crisis that we have now is because

74
00:03:04,110 --> 00:03:05,910
the Industrial Revolution happened

75
00:03:05,910 --> 00:03:07,868
without anybody thinking about,

76
00:03:07,868 --> 00:03:09,790
"Gee, do we need to think about sustainability?"

77
00:03:09,790 --> 00:03:12,730
Because no one did. They just assumed that, "Oh,

78
00:03:12,730 --> 00:03:13,740
we can just do what we want and

79
00:03:13,740 --> 00:03:16,755
Mother Nature will take care of the rest".

80
00:03:16,755 --> 00:03:19,620
I'm kind of thinking that we're at a similar point

81
00:03:19,620 --> 00:03:23,610
now within the digital space with big data.

82
00:03:23,610 --> 00:03:27,410
We can imagine a time in a few years,

83
00:03:27,410 --> 00:03:29,365
a decade, where we're trying to clean up

84
00:03:29,365 --> 00:03:32,425
little problems with legal and ethical principles.

85
00:03:32,425 --> 00:03:34,280
But it would just be so much more elegant,

86
00:03:34,280 --> 00:03:35,660
so much better, if we just

87
00:03:35,660 --> 00:03:37,750
thought about it the right way from the beginning.

88
00:03:37,750 --> 00:03:40,110
>> Thanks Nathan. Thanks Eva for your perspectives.

89
00:03:40,110 --> 00:03:41,790
I think what you're saying is critical

90
00:03:41,790 --> 00:03:45,320
and definitely why we're launching into this first module

91
00:03:45,320 --> 00:03:48,470
with foundations of ethics and law to get

92
00:03:48,470 --> 00:03:50,540
that competitive advantage of

93
00:03:50,540 --> 00:03:53,155
having those frameworks when we do these analytics,

94
00:03:53,155 --> 00:03:54,350
where artificial intelligence work.

95
00:03:54,350 --> 00:03:57,790
So thank you, and let's dig in.

