0
00:00:02,970 --> 00:00:06,130
In this video, we'll talk about ethics.

1
00:00:06,130 --> 00:00:07,480
We're going to talk about what it

2
00:00:07,480 --> 00:00:09,490
is and how it relates to data and

3
00:00:09,490 --> 00:00:13,500
creates this kind of new field called data ethics.

4
00:00:13,500 --> 00:00:15,906
And I think I know what you're doing right now,

5
00:00:15,906 --> 00:00:17,200
you're probably typing what is

6
00:00:17,200 --> 00:00:21,295
ethics or ethics definition into your search engine.

7
00:00:21,295 --> 00:00:23,875
I mean if you want to do that, that's fine, you can.

8
00:00:23,875 --> 00:00:26,080
I just don't think it's going to be very useful because

9
00:00:26,080 --> 00:00:29,240
what's going to pop up are about a million results.

10
00:00:29,240 --> 00:00:31,677
Some of those definitions are going to be pretty good,

11
00:00:31,677 --> 00:00:35,540
some of them are not quite as good,

12
00:00:35,540 --> 00:00:39,460
but the abstract definitions just end up being

13
00:00:39,460 --> 00:00:42,763
complicated and/or hard to remember.

14
00:00:42,763 --> 00:00:44,830
So what I want to do is just make it as simple as

15
00:00:44,830 --> 00:00:47,263
possible instead of an abstract definition that,

16
00:00:47,263 --> 00:00:49,590
let's be honest, you're going to forget about anyway.

17
00:00:49,590 --> 00:00:51,010
Let's just talk about what it means to

18
00:00:51,010 --> 00:00:52,756
be an ethical person,

19
00:00:52,756 --> 00:00:55,310
what it means to have ethical concern.

20
00:00:55,310 --> 00:00:59,080
And that just means a concern with human well-being.

21
00:00:59,080 --> 00:01:00,580
So if you're an ethical person,

22
00:01:00,580 --> 00:01:03,861
you care about the well-being of people.

23
00:01:03,861 --> 00:01:06,977
It's as simple as that.

24
00:01:06,977 --> 00:01:09,740
So, there is something complicated or at

25
00:01:09,740 --> 00:01:14,290
least we can make it complicated if we want to.

26
00:01:14,290 --> 00:01:16,190
There is this concern.

27
00:01:16,190 --> 00:01:18,440
I mean, if you know anything about ethics,

28
00:01:18,440 --> 00:01:21,320
there is a lot of different theories and there's a lot of

29
00:01:21,320 --> 00:01:23,060
different principles and some of them it

30
00:01:23,060 --> 00:01:25,550
takes a while to understand.

31
00:01:25,550 --> 00:01:27,440
And certainly, if we have to go

32
00:01:27,440 --> 00:01:29,375
back and compare and contrast them,

33
00:01:29,375 --> 00:01:31,885
that can get really messy.

34
00:01:31,885 --> 00:01:34,308
And in fact, if you really wanted,

35
00:01:34,308 --> 00:01:36,460
you could probably get in your car and

36
00:01:36,460 --> 00:01:39,450
drive to an ethics conference somewhere,

37
00:01:39,450 --> 00:01:40,420
somewhere in the world where

38
00:01:40,420 --> 00:01:43,480
ethics conferences are happening.

39
00:01:43,480 --> 00:01:46,330
And what's happening at these conferences are experts who

40
00:01:46,330 --> 00:01:49,075
spent their entire lives reading

41
00:01:49,075 --> 00:01:53,140
these complicated texts are arguing about

42
00:01:53,140 --> 00:01:55,090
the best ethical theories or how

43
00:01:55,090 --> 00:01:59,385
to interpret these principles.

44
00:01:59,385 --> 00:02:03,093
Look, I don't think, I mean,

45
00:02:03,093 --> 00:02:05,660
if that's what it takes to be an ethical person,

46
00:02:05,660 --> 00:02:08,420
there's probably no hope for the rest of us, right?

47
00:02:08,420 --> 00:02:10,910
Because most of us just can't afford to spend

48
00:02:10,910 --> 00:02:13,415
our entire life going to

49
00:02:13,415 --> 00:02:16,950
ethics conferences and talking about ethics.

50
00:02:16,950 --> 00:02:19,550
So the good news is that

51
00:02:19,550 --> 00:02:23,130
the actual process of just engaging in ethical thinking,

52
00:02:23,130 --> 00:02:26,975
being an ethical person, is much simpler.

53
00:02:26,975 --> 00:02:29,700
Okay, so here's how this works.

54
00:02:29,700 --> 00:02:30,980
You've probably heard of

55
00:02:30,980 --> 00:02:34,145
specific ethical theories: utilitarianism,

56
00:02:34,145 --> 00:02:37,892
deontology or maybe it's called Kantianism sometimes,

57
00:02:37,892 --> 00:02:40,644
contractarianism, there's virtue ethics.

58
00:02:40,644 --> 00:02:43,795
All these are ethical theories.

59
00:02:43,795 --> 00:02:46,244
What they have in common,

60
00:02:46,244 --> 00:02:47,965
now not all moral theories,

61
00:02:47,965 --> 00:02:49,080
there are some theories like

62
00:02:49,080 --> 00:02:51,384
cultural relative or divine command theory,

63
00:02:51,384 --> 00:02:53,485
you might have heard of those.

64
00:02:53,485 --> 00:02:56,720
They're not built on any human value.

65
00:02:56,720 --> 00:02:58,890
But really good moral theories,

66
00:02:58,890 --> 00:02:59,970
such as the ones I just

67
00:02:59,970 --> 00:03:01,935
mentioned and there are a couple of others,

68
00:03:01,935 --> 00:03:05,988
are built on the observation about a particular value,

69
00:03:05,988 --> 00:03:09,600
something that makes life go well for human beings,

70
00:03:09,600 --> 00:03:11,285
something that's central to human well-being.

71
00:03:11,285 --> 00:03:13,545
They take that value, and then they build

72
00:03:13,545 --> 00:03:16,950
a complicated theory around it and there comes out

73
00:03:16,950 --> 00:03:18,840
with lots of principles

74
00:03:18,840 --> 00:03:20,970
and then we can get to the business of comparing

75
00:03:20,970 --> 00:03:23,190
and contrasting those principles and comparing and

76
00:03:23,190 --> 00:03:25,535
contrasting those ethical theories,

77
00:03:25,535 --> 00:03:27,780
and that's where it gets complicated.

78
00:03:27,780 --> 00:03:30,855
Now, if our job were to attempt to

79
00:03:30,855 --> 00:03:32,670
understand even the basics

80
00:03:32,670 --> 00:03:35,990
of those arguments and how they work,

81
00:03:35,990 --> 00:03:37,440
we would have a lot of work to do.

82
00:03:37,440 --> 00:03:38,595
And as I suggested,

83
00:03:38,595 --> 00:03:40,440
the experts are still busy

84
00:03:40,440 --> 00:03:42,690
disagreeing with each other about how this all works,

85
00:03:42,690 --> 00:03:45,350
and so maybe an infinite amount of work to do,

86
00:03:45,350 --> 00:03:47,795
which is just not going to work for us.

87
00:03:47,795 --> 00:03:50,350
So, what I propose

88
00:03:50,350 --> 00:03:53,110
doing is let's leave

89
00:03:53,110 --> 00:03:54,770
the theories and principles where they are.

90
00:03:54,770 --> 00:03:57,580
Are theories and principles bad? Absolutely not.

91
00:03:57,580 --> 00:03:59,350
My job is to teach

92
00:03:59,350 --> 00:04:01,870
the theories and principles and I like my job very much.

93
00:04:01,870 --> 00:04:04,000
I think there's definitely a role for it.

94
00:04:04,000 --> 00:04:05,320
But when we're talking about

95
00:04:05,320 --> 00:04:06,820
what it means to be an ethical person,

96
00:04:06,820 --> 00:04:08,500
what it means to think ethically,

97
00:04:08,500 --> 00:04:10,800
we don't need to understand the theories,

98
00:04:10,800 --> 00:04:11,935
we need to understand

99
00:04:11,935 --> 00:04:14,650
the values underlying those theories.

100
00:04:14,650 --> 00:04:16,985
And remember what I said about the values?

101
00:04:16,985 --> 00:04:19,660
Any good moral theory is

102
00:04:19,660 --> 00:04:22,855
built on an observation of a particular value.

103
00:04:22,855 --> 00:04:24,660
And the other good news,

104
00:04:24,660 --> 00:04:27,040
we're continuing the string of good news,

105
00:04:27,040 --> 00:04:29,350
there are really only five values.

106
00:04:29,350 --> 00:04:30,820
Now that's my list.

107
00:04:30,820 --> 00:04:32,710
Someone else, another professional ethicist

108
00:04:32,710 --> 00:04:33,980
might have a slightly longer,

109
00:04:33,980 --> 00:04:38,850
shorter list, but I count five values.

