Machine Bias
================

``` 
                  Low  High
                 +---------+
Didn't Reoffend  |____|____|
Reoffended       |    |    |
                 +---------+
```

This folder contains a
[mirror](https://github.com/propublica/compas-analysis) of materials for
the ProPublica story “Machine
Bias”.

[Story](https://www.propublica.org/article/machine-bias-risk-assessments-in-criminal-sentencing/)

[Methodology](https://www.propublica.org/article/how-we-analyzed-the-compas-recidivism-algorithm/)

[Notebook](https://gitlab.com/xyz.oswind.actual/mpp-datascience/blob/master/DAT249x/compas-analysis-data/Compas%20Analysis.ipynb)

**Main Dataset:** compas.db - a sqlite3 database containing criminal
history, jail and prison time, demographics and COMPAS risk scores for
defendants from Broward County.

Other files as needed for the analysis.
