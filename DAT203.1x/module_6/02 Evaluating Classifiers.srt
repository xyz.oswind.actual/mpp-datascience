0
00:00:04,594 --> 00:00:07,370
Now, being able to evaluate a classifier is the key to

1
00:00:07,370 --> 00:00:08,490
building a good one.

2
00:00:08,490 --> 00:00:12,350
So, we&#39;re gonna talk about ways to evaluate classifiers.

3
00:00:12,350 --> 00:00:15,430
So, let&#39;s remember the example of the manholes.

4
00:00:15,430 --> 00:00:19,900
We have the features for each manhole, over here and

5
00:00:19,900 --> 00:00:21,940
we have the labels, each observation&#39;s labeled.

6
00:00:24,480 --> 00:00:27,270
And then the machine learning algorithm comes along and

7
00:00:27,270 --> 00:00:29,930
it gives a number to each observation which is sort of

8
00:00:29,930 --> 00:00:31,920
what it thinks is going on.

9
00:00:31,920 --> 00:00:32,800
And the number says how

10
00:00:32,800 --> 00:00:35,880
far away from the decision boundary the observation is.

11
00:00:35,880 --> 00:00:40,884
And also the sign of f, this f, is the predicted label,

12
00:00:40,884 --> 00:00:45,584
and we&#39;ll put those in another column right there.

13
00:00:45,584 --> 00:00:48,640
So the y hat, that&#39;s just the sign of f.

14
00:00:50,350 --> 00:00:52,970
It just tells you which side of the decision boundary

15
00:00:52,970 --> 00:00:55,130
the point is on, right?

16
00:00:55,130 --> 00:00:57,941
And if the classifier is right,

17
00:00:57,941 --> 00:01:00,968
then y hat agrees with the truth y.

18
00:01:03,975 --> 00:01:06,159
Now let&#39;s just take a look at these two columns for

19
00:01:06,159 --> 00:01:07,200
a few minutes.

20
00:01:07,200 --> 00:01:09,260
If the classifier is really good,

21
00:01:09,260 --> 00:01:12,200
then the predicted labels often agree with the true labels.

22
00:01:14,420 --> 00:01:16,450
Okay, so let&#39;s give a few more examples just for fun.

23
00:01:18,060 --> 00:01:22,110
And then this example here is called a true positive.

24
00:01:22,110 --> 00:01:25,841
Because the true label is positive but also the predicted

25
00:01:25,841 --> 00:01:29,504
label&#39;s positive, so this is called a true positive.

26
00:01:31,319 --> 00:01:33,554
And this is called a true negative,

27
00:01:33,554 --> 00:01:36,991
where the prediction and the truth are both negative.

28
00:01:36,991 --> 00:01:40,051
And this is a false positive or a Type I error,

29
00:01:40,051 --> 00:01:43,770
where you think it is positive but it&#39;s actually not.

30
00:01:46,500 --> 00:01:49,060
And then this is a false negative where you think it is

31
00:01:49,060 --> 00:01:50,850
negative but it&#39;s actually not.

32
00:01:52,520 --> 00:01:55,950
And then down here is another true negative and

33
00:01:55,950 --> 00:01:57,910
below that is another false negative and so on.

34
00:01:59,690 --> 00:02:01,350
So errors come in these two flavors.

35
00:02:02,850 --> 00:02:04,900
So how can we judge the quality of a classifier?

36
00:02:05,940 --> 00:02:09,040
Well, we can count the number of errors that the classifier

37
00:02:09,039 --> 00:02:11,370
makes and we can put it in a table.

38
00:02:11,370 --> 00:02:15,300
Okay, now this table is called the Confusion Matrix.

39
00:02:15,300 --> 00:02:19,370
Now in the upper left, we put the number of true positives.

40
00:02:19,370 --> 00:02:23,860
And then the false positives go in the upper right, and so on.

41
00:02:23,860 --> 00:02:25,120
So here, there.

42
00:02:25,120 --> 00:02:27,260
True positives go in the upper left,

43
00:02:27,260 --> 00:02:29,370
false positives go in the upper right, and so on.

44
00:02:31,160 --> 00:02:34,620
And now, if you just want to compute the error rate,

45
00:02:34,620 --> 00:02:36,800
you can compute the misclassification error.

46
00:02:36,800 --> 00:02:40,930
Now, the misclassification error is just the fraction of errors

47
00:02:40,930 --> 00:02:42,370
in the dataset.

48
00:02:42,370 --> 00:02:46,420
It&#39;s also called the classification error.

49
00:02:46,420 --> 00:02:48,590
It&#39;s also called the misclassification rate.

50
00:02:50,590 --> 00:02:51,948
And I can also write it like this,

51
00:02:51,948 --> 00:02:57,760
where it&#39;s 1 over n times the number of times when y,

52
00:02:57,760 --> 00:03:00,430
the truth, does not equal y hat, the prediction.

53
00:03:00,430 --> 00:03:03,430
And this includes both the false positives and

54
00:03:03,430 --> 00:03:05,200
the false negatives.

55
00:03:05,200 --> 00:03:07,770
Now the converse of misclassification error is

56
00:03:07,770 --> 00:03:11,670
accuracy, which is just the fraction of the time that

57
00:03:11,670 --> 00:03:14,490
the prediction is right, okay?

58
00:03:14,490 --> 00:03:18,320
Note that it&#39;s just 1- the misclassification error.

59
00:03:18,320 --> 00:03:20,584
So sometimes people say that they are using accuracy and

60
00:03:20,584 --> 00:03:22,514
they actually mean misclassification error,

61
00:03:22,514 --> 00:03:25,000
it&#39;s the same type of calculation.

62
00:03:25,000 --> 00:03:27,090
So how should I report the result?

63
00:03:27,090 --> 00:03:27,810
Out of sample.

64
00:03:29,290 --> 00:03:31,810
I got so sick of people messing this up that I wrote it

65
00:03:31,810 --> 00:03:32,870
three times on the slide.

66
00:03:34,255 --> 00:03:36,800
So you should report the result on the test set,

67
00:03:36,800 --> 00:03:38,570
not on the training set.

68
00:03:38,570 --> 00:03:41,340
You&#39;re trying to figure out how well you can predict.

69
00:03:41,340 --> 00:03:44,210
Predict means predict on data you haven&#39;t seen yet,

70
00:03:44,210 --> 00:03:46,190
not on data you created the model from.

71
00:03:47,450 --> 00:03:48,300
Please don&#39;t mess this up.

72
00:03:48,300 --> 00:03:50,760
This is a core concept in data mining and machine learning.

73
00:03:50,760 --> 00:03:54,560
The prediction quality should always be judged out of sample.

74
00:03:54,560 --> 00:03:57,000
We want to make sure we&#39;re not overfitting the data,

75
00:03:57,000 --> 00:03:58,030
which is a concept that I&#39;ll

76
00:03:58,030 --> 00:04:00,170
talk more about in the machine learning course.

77
00:04:00,170 --> 00:04:02,706
But please, please, use the training set to

78
00:04:02,706 --> 00:04:06,395
create the classifier but report the evaluation on the test set.

79
00:04:06,395 --> 00:04:09,583
So before you start, you should identify what are the training

80
00:04:09,583 --> 00:04:12,420
set, What is the training set and what&#39;s the test set?

81
00:04:13,810 --> 00:04:15,960
And sometimes it&#39;s clear what those should be.

82
00:04:15,960 --> 00:04:18,590
So, for instance, if you wanna train the model on data from one

83
00:04:18,589 --> 00:04:20,730
product and then test it on another product.

84
00:04:20,730 --> 00:04:22,480
But sometimes we just want to make sure that

85
00:04:22,480 --> 00:04:25,080
the model performs well in the static setting.

86
00:04:25,080 --> 00:04:27,880
In that case, you might just wanna randomly permute the data

87
00:04:27,880 --> 00:04:29,180
and just divide it randomly and

88
00:04:29,180 --> 00:04:32,810
you would designate let&#39;s say a fifth of it to be the test set.

89
00:04:32,810 --> 00:04:34,240
And so, we&#39;ll teach you cross-validation

90
00:04:34,240 --> 00:04:35,330
in the next course.

91
00:04:35,330 --> 00:04:38,159
And then that is actually the standard way to

92
00:04:38,159 --> 00:04:40,537
set up the training and the test set.

