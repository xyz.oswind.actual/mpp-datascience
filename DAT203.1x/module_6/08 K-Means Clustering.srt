0
00:00:04,793 --> 00:00:06,870
Hello and welcome back.

1
00:00:06,870 --> 00:00:11,964
So Cynthia&#39;s discussed the theory of unsupervised learning,

2
00:00:11,964 --> 00:00:14,992
particularly k-means clustering.

3
00:00:14,992 --> 00:00:20,428
And so with those methods we attempt to bind structure and

4
00:00:20,428 --> 00:00:25,530
data that otherwise doesn&#39;t have labels.

5
00:00:25,530 --> 00:00:28,450
So let me show you a practical example of building and

6
00:00:28,450 --> 00:00:31,500
evaluating an experiment to do K means clustering.

7
00:00:33,490 --> 00:00:36,840
So we&#39;ve already worked with this adult census income

8
00:00:36,840 --> 00:00:37,560
data set.

9
00:00:37,560 --> 00:00:41,690
As you recall, we used it for the classification example.

10
00:00:41,690 --> 00:00:46,446
In this case we&#39;re going to perform cluster

11
00:00:46,446 --> 00:00:51,971
analysis on it and one thing I wanted to show you here

12
00:00:51,971 --> 00:00:57,127
is something to keep in mind until the very end.

13
00:00:57,127 --> 00:01:00,569
So remember there was a label here of income greater than or

14
00:01:00,569 --> 00:01:04,221
equal to 50,000 or less than or equal to 50,000 or

15
00:01:04,221 --> 00:01:05,997
greater than 50,000.

16
00:01:05,997 --> 00:01:09,079
And notice the proportions there&#39;s about three times

17
00:01:09,079 --> 00:01:12,563
roughly or three and half times more people in the lower income

18
00:01:12,563 --> 00:01:15,050
category than the higher income category.

19
00:01:15,050 --> 00:01:17,820
So just, just bear that in mind as we go.

20
00:01:18,980 --> 00:01:21,020
So the prep we&#39;ve talked about, so

21
00:01:21,020 --> 00:01:23,780
I&#39;m just going to breeze through that very quickly here.

22
00:01:23,780 --> 00:01:27,240
We&#39;ve talked about this extensively, we removed some

23
00:01:27,240 --> 00:01:29,910
columns and this is very similar to what we did for

24
00:01:29,910 --> 00:01:33,070
the classification example, except,

25
00:01:34,260 --> 00:01:37,510
I&#39;ve added income because we&#39;re doing clustering here,

26
00:01:37,510 --> 00:01:39,010
we&#39;re doing unsupervised learning.

27
00:01:39,010 --> 00:01:41,790
So I need to remove that label column, so

28
00:01:41,790 --> 00:01:45,128
this is now a dataset without a label.

29
00:01:45,128 --> 00:01:46,810
And the other columns we remove recall,

30
00:01:46,810 --> 00:01:50,400
were problematic for various reasons having say

31
00:01:50,400 --> 00:01:53,260
lots of categories with few members or something like that.

32
00:01:54,320 --> 00:01:59,608
Normalize the data, in this case I just use very simple min max

33
00:01:59,608 --> 00:02:04,998
Normalization, convert the String columns to Categorical,

34
00:02:04,998 --> 00:02:09,576
and then we&#39;re gonna make Indicator variables out of

35
00:02:09,576 --> 00:02:12,135
those Categorical columns.

36
00:02:12,135 --> 00:02:16,168
And that&#39;s one of the reasons we needed to remove these columns

37
00:02:16,168 --> 00:02:18,990
that had excessive numbers of categories.

38
00:02:20,200 --> 00:02:23,660
So let me just show you what we&#39;re starting,

39
00:02:23,660 --> 00:02:25,890
this is our starting point then for clustering.

40
00:02:27,070 --> 00:02:28,350
Have quick look at this data.

41
00:02:31,290 --> 00:02:34,390
And we have a combination of say numerical columns like age

42
00:02:34,390 --> 00:02:37,330
that&#39;s now if I scroll down you&#39;ll see it&#39;s min max

43
00:02:37,330 --> 00:02:39,130
normalized.

44
00:02:39,130 --> 00:02:41,440
And then things like these indicator variables for

45
00:02:41,440 --> 00:02:45,650
education, like here is a person who had an 11th

46
00:02:45,650 --> 00:02:49,670
grade education for example and you see there is quite a few

47
00:02:49,670 --> 00:02:55,140
categories here like, occupations of different kinds.

48
00:02:55,140 --> 00:02:58,120
Here is a person who is in some sort of service industry with

49
00:02:58,120 --> 00:03:00,610
a 1 there etc.

50
00:03:00,610 --> 00:03:04,100
And notice we&#39;ve got a fair expansion of the number

51
00:03:04,100 --> 00:03:05,095
of columns, 54.

52
00:03:05,095 --> 00:03:08,140
So this increase in dimensionality is another reason

53
00:03:08,140 --> 00:03:13,300
to be somewhat cautious about columns with

54
00:03:15,110 --> 00:03:19,256
very many categories with very few members in the category all

55
00:03:19,256 --> 00:03:23,150
right, so, and then one other thing.

56
00:03:23,150 --> 00:03:27,150
Notice I normalize the numeric variables

57
00:03:27,150 --> 00:03:30,240
before I created indicator values, I want those indicator

58
00:03:30,240 --> 00:03:34,890
column values to be essentially zero or one, binary.

59
00:03:34,890 --> 00:03:38,520
If I did various types of normalization,

60
00:03:38,520 --> 00:03:40,220
say a z score normalization,

61
00:03:40,220 --> 00:03:43,130
some other sort of numeric normalization like that.

62
00:03:43,130 --> 00:03:46,070
I could wind up with some very odd statistics depending

63
00:03:46,070 --> 00:03:49,230
on how many zeroes and ones I have,

64
00:03:49,230 --> 00:03:52,060
which gives weird weights to things and

65
00:03:52,060 --> 00:03:55,570
causes some statistical strangeness, so avoid that.

66
00:03:55,570 --> 00:03:57,520
Always normalize.

67
00:03:57,520 --> 00:03:59,270
And convert to indicator values.

68
00:04:00,850 --> 00:04:05,010
So let me cut to the chase, we&#39;re trying to define a K and

69
00:04:05,010 --> 00:04:07,920
run a K-Means clustering model.

70
00:04:07,920 --> 00:04:11,530
We have the K-Means clustering module.

71
00:04:11,530 --> 00:04:15,880
And I&#39;m just gonna try 2 centroids here, so

72
00:04:15,880 --> 00:04:20,460
K equals 2 I&#39;m going to use random initialization,

73
00:04:20,459 --> 00:04:22,640
which is the typical thing you do.

74
00:04:22,640 --> 00:04:24,500
I just gave it a random seed.

75
00:04:26,970 --> 00:04:30,540
Euclidean distance metric just tell the distance is just

76
00:04:30,540 --> 00:04:34,640
the square root of the sum of the squares of the distances

77
00:04:34,640 --> 00:04:38,610
on however many dimensions we have, which in this case is 54.

78
00:04:38,610 --> 00:04:39,990
As you saw before.

79
00:04:39,990 --> 00:04:44,230
We&#39;re gonna try 100 iterations of doing that and well we&#39;ll

80
00:04:44,230 --> 00:04:47,320
ignore the label column but we&#39;ve already removed it.

81
00:04:47,320 --> 00:04:51,240
So then I have to train that clustering model, so

82
00:04:51,240 --> 00:04:54,830
notice because there&#39;s no labels and

83
00:04:54,830 --> 00:04:59,660
there&#39;s no way to objectively evaluate and unsupervised

84
00:04:59,660 --> 00:05:03,600
learning model the way we&#39;ve done with classification and

85
00:05:03,600 --> 00:05:05,550
regression there&#39;s no split here right.

86
00:05:05,550 --> 00:05:10,857
We just take all the data into the train clustering model,

87
00:05:10,857 --> 00:05:11,832
module and

88
00:05:11,832 --> 00:05:17,590
we take our un trained K-means clustering model as the input.

89
00:05:19,680 --> 00:05:23,740
And we simply take All columns, All features,

90
00:05:23,740 --> 00:05:24,990
that&#39;s our column selection.

91
00:05:24,990 --> 00:05:28,080
And we&#39;re gonna append, let me blow that up so

92
00:05:28,080 --> 00:05:29,600
you can see what&#39;s going on.

93
00:05:29,600 --> 00:05:30,455
We&#39;re gonna append.

94
00:05:34,678 --> 00:05:35,640
The results.

95
00:05:35,640 --> 00:05:40,020
So if you just wanted the cluster numbers you could

96
00:05:40,020 --> 00:05:43,090
uncheck that box, but that isn&#39;t what we want.

97
00:05:43,090 --> 00:05:46,390
And then I&#39;ve just added a select columns here,

98
00:05:46,390 --> 00:05:48,130
cuz I wanted to show you something.

99
00:05:48,130 --> 00:05:51,977
And I&#39;ve just picked again all columns, all features,

100
00:05:51,977 --> 00:05:55,502
and an evaluate model module so we can get a little bit

101
00:05:55,502 --> 00:05:58,483
of quantitative information on how we did.

102
00:05:58,483 --> 00:06:01,490
So oops, sorry.

103
00:06:01,490 --> 00:06:03,040
Let me just run this module.

104
00:06:05,160 --> 00:06:05,660
Okay.

105
00:06:07,410 --> 00:06:10,480
So there&#39;s a number of ways we can look at what&#39;s

106
00:06:10,480 --> 00:06:12,490
happened here, right?

107
00:06:12,490 --> 00:06:16,080
But first off let&#39;s just look at what happened with the training

108
00:06:16,080 --> 00:06:20,440
and we can create this visualization and

109
00:06:20,440 --> 00:06:23,240
I don&#39;t want to go into a lot of detail about this.

110
00:06:23,240 --> 00:06:25,460
We&#39;ll save that for another time.

111
00:06:25,460 --> 00:06:28,520
But basically, these two ellipses.

112
00:06:28,520 --> 00:06:32,142
You see there&#39;s a big blue one, or kind of blue gray, and

113
00:06:32,142 --> 00:06:33,930
a smaller red one.

114
00:06:33,930 --> 00:06:36,910
The thing to notice is that the major axis,

115
00:06:36,910 --> 00:06:39,850
that is, the long axis of these ellipses.

116
00:06:39,850 --> 00:06:42,310
Are almost perpendicular, and

117
00:06:42,310 --> 00:06:45,840
the lengths are quite different as well.

118
00:06:45,840 --> 00:06:48,070
So that indicates we have a bigger cluster and

119
00:06:48,070 --> 00:06:51,560
a smaller cluster, it indicates that the separation between

120
00:06:51,560 --> 00:06:54,520
those two clusters is probably pretty good.

121
00:06:54,520 --> 00:06:58,000
If there was poor separation those major axis&#39;

122
00:06:58,000 --> 00:07:00,400
would be pointing more or less in the same direction and

123
00:07:00,400 --> 00:07:01,710
they&#39;re clearly not.

124
00:07:01,710 --> 00:07:05,030
They&#39;re almost at 90 degrees, which is good.

125
00:07:05,030 --> 00:07:08,752
That means we have two clusters with pretty good separation.

126
00:07:08,752 --> 00:07:13,353
So we can look at that a little more quantitatively with

127
00:07:13,353 --> 00:07:15,959
this Evaluate Models module.

128
00:07:15,959 --> 00:07:21,115
And so first off, [COUGH] notice that we

129
00:07:21,115 --> 00:07:26,280
have these two clusters zero and one.

130
00:07:27,480 --> 00:07:30,450
And we have the average distance to cluster&#39;s center.

131
00:07:30,450 --> 00:07:34,738
So that&#39;s the average distance from each point in

132
00:07:34,738 --> 00:07:39,959
the cluster to that center that we&#39;ve finally converge on.

133
00:07:39,959 --> 00:07:43,290
And we have the average distance to the other center.

134
00:07:45,880 --> 00:07:49,670
And what you notice is that this average distance to the other

135
00:07:49,670 --> 00:07:50,460
centers.

136
00:07:50,460 --> 00:07:55,450
They are a bit bigger than this number, in both cases.

137
00:07:55,450 --> 00:07:56,840
For both of our clusters.

138
00:07:56,840 --> 00:08:00,714
So that tells us that the clusters are fairly tight and

139
00:08:00,714 --> 00:08:02,012
well separated.

140
00:08:02,012 --> 00:08:03,690
You can see the number of points.

141
00:08:05,220 --> 00:08:09,609
But you can see that the maximum distance here in this particular

142
00:08:09,609 --> 00:08:13,601
case is about the same as the distance to the other center.

143
00:08:13,601 --> 00:08:15,069
And it&#39;s actually greater so

144
00:08:15,069 --> 00:08:17,682
the maximum distance is actually greater.

145
00:08:17,682 --> 00:08:20,375
So there&#39;s some overlap in these clusters, there&#39;s no doubt about

146
00:08:20,375 --> 00:08:23,290
it, but they are generally fairly well separated.

147
00:08:25,150 --> 00:08:28,080
So let me show you one last attribute.

148
00:08:28,080 --> 00:08:32,560
You can see, the reason I stuck this select columns in dataset

149
00:08:32,559 --> 00:08:34,283
module here is simply so

150
00:08:34,283 --> 00:08:38,263
I could visualize something that I wanted to show you.

151
00:08:41,496 --> 00:08:46,226
So I&#39;m just gonna scroll all the way over and you can see some

152
00:08:46,226 --> 00:08:50,769
statistics about these distances, that&#39;s where those

153
00:08:50,769 --> 00:08:55,515
came from, but more importantly here&#39;s the assignment.

154
00:08:55,515 --> 00:08:59,387
So you can see, every row is assigned either cluster zero or

155
00:08:59,387 --> 00:09:01,110
cluster one.

156
00:09:01,110 --> 00:09:04,390
So you could use that for in other parts of your

157
00:09:04,390 --> 00:09:07,050
further analysis, if you want to drill into this.

158
00:09:07,050 --> 00:09:09,450
And remember I told you to remember that

159
00:09:10,860 --> 00:09:14,190
there were about three times more people

160
00:09:14,190 --> 00:09:16,460
with the lower income than with the higher income, but

161
00:09:16,460 --> 00:09:18,970
notice our clusters aren&#39;t quite that way.

162
00:09:18,970 --> 00:09:21,570
There&#39;s maybe two-thirds as many in cluster

163
00:09:21,570 --> 00:09:23,070
one as in cluster zero.

164
00:09:24,390 --> 00:09:27,630
So, clearly, our model has picked up on some

165
00:09:27,630 --> 00:09:31,970
aspect of the structure of this data, but isn&#39;t quite those

166
00:09:31,970 --> 00:09:35,850
income divisions we used for classification.

167
00:09:35,850 --> 00:09:41,700
But, we&#39;d have to spend more time trying to evaluate that,

168
00:09:41,700 --> 00:09:43,069
exactly what that was.

169
00:09:44,390 --> 00:09:50,520
So just to wrap up here I hope this gave you a quick idea of

170
00:09:50,520 --> 00:09:55,298
how to build and evaluate a k means

171
00:09:55,298 --> 00:10:00,120
clustering model and obviously we could go further.

172
00:10:00,120 --> 00:10:04,620
We could try other knot values of k and some other parameters

173
00:10:04,620 --> 00:10:07,560
and keep comparing and see where that takes us.

174
00:10:07,560 --> 00:10:10,084
But for now, we&#39;ll just leave it here and

175
00:10:10,084 --> 00:10:11,826
I hope you find that useful.

