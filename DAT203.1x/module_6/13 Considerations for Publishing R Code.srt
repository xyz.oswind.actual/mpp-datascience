0
00:00:05,107 --> 00:00:07,020
Hi and welcome back.

1
00:00:07,020 --> 00:00:12,060
So in a previous demo, I showed you how to publish

2
00:00:12,060 --> 00:00:14,730
an Azure Machine Learning model as a web service.

3
00:00:14,730 --> 00:00:17,930
And I demonstrated that, and I was able to use it from Excel,

4
00:00:17,930 --> 00:00:19,090
and if you&#39;ve done the lab,

5
00:00:19,090 --> 00:00:21,380
you&#39;ve done the same thing on your own.

6
00:00:22,550 --> 00:00:26,870
But let me show you what is a possible pitfall if you&#39;re using

7
00:00:26,870 --> 00:00:28,410
R code.

8
00:00:28,410 --> 00:00:31,660
So my screen here, I have this experiment that&#39;s

9
00:00:31,660 --> 00:00:34,088
similar to the one we&#39;ve already worked with.

10
00:00:34,088 --> 00:00:39,781
Except I&#39;ve replaced a lot of the Azure Machine Learning

11
00:00:39,781 --> 00:00:43,160
built-in modules with R code.

12
00:00:43,160 --> 00:00:44,330
So for example,

13
00:00:44,330 --> 00:00:49,630
here we&#39;re doing quite a bit of the prep with R code.

14
00:00:49,630 --> 00:00:55,621
Removing columns by name, sorry, computing the log of the price,

15
00:00:55,621 --> 00:00:59,943
removing missing values and duplicates, etc.

16
00:01:03,674 --> 00:01:10,920
Then here, we&#39;re clipping the values that were the outliers.

17
00:01:10,920 --> 00:01:14,337
And most importantly, in this module,

18
00:01:14,337 --> 00:01:18,061
we have a list of the numeric columns names.

19
00:01:20,230 --> 00:01:25,326
And we apply the R scale function to,

20
00:01:27,214 --> 00:01:31,040
Data that&#39;s in that list of column names, okay.

21
00:01:32,410 --> 00:01:36,360
And the rest of this by now, should be familiar, in terms of

22
00:01:36,360 --> 00:01:40,670
how we built the, and trained the machine learning model.

23
00:01:40,670 --> 00:01:44,500
So, let me go to the predictive experiment that resulted.

24
00:01:47,470 --> 00:01:49,620
And I&#39;ve run it to test it, and

25
00:01:49,620 --> 00:01:52,190
let me just show you what happened at the end.

26
00:01:53,900 --> 00:01:57,417
So I got this column of numbers and they look,

27
00:01:57,417 --> 00:02:01,965
reasonable numbers for the prices of these automobiles.

28
00:02:04,017 --> 00:02:05,518
But let me go to Excel and

29
00:02:05,518 --> 00:02:09,420
try to run that web service that was published.

30
00:02:09,419 --> 00:02:14,413
So I&#39;m gonna go here, select that one and you see,

31
00:02:14,413 --> 00:02:19,630
I&#39;ve already loaded one row of data there.

32
00:02:19,630 --> 00:02:22,830
And I&#39;ve set my output to this cell,

33
00:02:22,830 --> 00:02:27,120
AA1, and let me run the predictive experiment.

34
00:02:29,980 --> 00:02:34,690
Notice that the column header changed here, so it now is

35
00:02:34,690 --> 00:02:39,320
the column header that we expect.

36
00:02:39,320 --> 00:02:43,270
But we got nothing back, basically a null result.

37
00:02:43,270 --> 00:02:44,280
So what went wrong?

38
00:02:46,860 --> 00:02:49,870
So maybe we could compare that to what happened

39
00:02:49,870 --> 00:02:52,680
when we published an experiment with,

40
00:02:52,680 --> 00:02:56,700
just using Azure Machine Learning modules.

41
00:02:56,700 --> 00:03:00,950
So this experiment does not use any R code,

42
00:03:00,950 --> 00:03:04,860
it just uses, let me explain this, so you can see it.

43
00:03:04,860 --> 00:03:10,440
It just uses Azure Machine Learning built-in modules and

44
00:03:10,440 --> 00:03:12,720
here&#39;s the normalize.

45
00:03:12,720 --> 00:03:16,787
We&#39;re gonna do a z-score normalization, and

46
00:03:16,787 --> 00:03:21,270
after I ran it, I created a predictive experiment.

47
00:03:23,930 --> 00:03:29,810
And let&#39;s go down to, we clipped some values and

48
00:03:32,720 --> 00:03:36,290
then, so here&#39;s the normalized.

49
00:03:36,290 --> 00:03:38,990
And notice there&#39;s this transform, and

50
00:03:38,990 --> 00:03:43,670
that transform saved the key coefficients

51
00:03:43,670 --> 00:03:47,350
required for z-score normalization.

52
00:03:47,350 --> 00:03:51,878
Which is just the mean and the standard deviation, and

53
00:03:51,878 --> 00:03:56,413
it is one of the inputs to the supply transform module.

54
00:03:56,413 --> 00:03:58,606
And therefore I&#39;m not,

55
00:03:58,606 --> 00:04:04,142
If you give this experiment a single row, it&#39;s not attempting

56
00:04:04,142 --> 00:04:08,881
to compute another normalization of those columns.

57
00:04:08,881 --> 00:04:13,170
It&#39;s using the stored data from the original training set and

58
00:04:13,170 --> 00:04:16,250
applying that as a transform here.

59
00:04:16,250 --> 00:04:19,180
So that&#39;s the secret of why this works when you

60
00:04:19,180 --> 00:04:20,992
use the built-in modules.

61
00:04:20,992 --> 00:04:23,720
So how do we make that work, then, with R?

62
00:04:25,370 --> 00:04:28,900
So I have another experiment here, which looks like

63
00:04:28,900 --> 00:04:32,240
the first one I showed you, where everything is in R.

64
00:04:32,240 --> 00:04:38,255
But after this module, where I clipped the outliers,

65
00:04:38,255 --> 00:04:42,850
I&#39;ve added this summarize data module.

66
00:04:42,850 --> 00:04:45,750
And that, let me just show you what comes out of that.

67
00:04:47,850 --> 00:04:51,060
We have a mean for all the numeric columns, and

68
00:04:51,060 --> 00:04:54,840
if I scroll over, you&#39;ll see there&#39;s a standard deviation for

69
00:04:54,840 --> 00:04:56,190
all the numeric columns.

70
00:04:57,350 --> 00:05:01,770
And I can save that as a data set, which I&#39;ve done.

71
00:05:03,360 --> 00:05:08,280
So I go to my predictive experiment and

72
00:05:08,280 --> 00:05:11,250
I&#39;ve called it auto.stats, that saved result.

73
00:05:11,250 --> 00:05:15,219
And you can just see that it&#39;s the same as what I computed in

74
00:05:15,219 --> 00:05:18,059
the training experiment with the means.

75
00:05:18,059 --> 00:05:21,471
I won&#39;t bother scrolling over here, and

76
00:05:21,471 --> 00:05:26,440
I&#39;ve changed one column name to STD to get rid of the spaces,

77
00:05:26,440 --> 00:05:28,830
which R doesn&#39;t like.

78
00:05:28,830 --> 00:05:32,150
But I&#39;ve had to change this code quite a bit,

79
00:05:32,150 --> 00:05:36,340
where I have the execute R script to apply that.

80
00:05:36,340 --> 00:05:40,300
So, I have now a function called normalize, and

81
00:05:40,300 --> 00:05:41,420
it has three arguments.

82
00:05:41,420 --> 00:05:43,800
The column, the mean of the column, and

83
00:05:43,800 --> 00:05:45,200
the standard deviation of the column,

84
00:05:45,200 --> 00:05:48,790
just using the standard scaling result.

85
00:05:48,790 --> 00:05:52,040
And you see there&#39;s another input now to this execute

86
00:05:52,040 --> 00:05:55,790
R script, so we have something coming in on port 2.

87
00:05:55,790 --> 00:05:59,420
And we select all the columns

88
00:05:59,420 --> 00:06:03,510
where the feature name is in that numeric column list.

89
00:06:03,510 --> 00:06:05,730
We call it either mean or standard deviation.

90
00:06:08,710 --> 00:06:13,388
Using the R map function, which is kind of like L Apply.

91
00:06:15,710 --> 00:06:20,850
I apply that normalize function to three lists of arguments.

92
00:06:20,850 --> 00:06:24,000
One is the columns that I want to scale.

93
00:06:24,000 --> 00:06:25,210
The list of their means and

94
00:06:25,210 --> 00:06:26,890
the list of their standard deviations.

95
00:06:26,890 --> 00:06:28,980
So, there&#39;s two points about this.

96
00:06:28,980 --> 00:06:32,980
One is, I&#39;m using the mean and standard deviation that

97
00:06:32,980 --> 00:06:35,520
I originally calculated from my training dataset.

98
00:06:35,520 --> 00:06:37,500
So I&#39;m not using some other mean and

99
00:06:37,500 --> 00:06:41,300
standard deviation that could come from whatever values I&#39;m

100
00:06:41,300 --> 00:06:44,340
being given in my web service.

101
00:06:44,340 --> 00:06:48,710
The other is, I don&#39;t have to do any aggregation operation.

102
00:06:48,710 --> 00:06:52,727
I don&#39;t have to recompute a mean by summing the values in

103
00:06:52,727 --> 00:06:53,667
the column,

104
00:06:53,667 --> 00:06:58,465
opr something like that where I actually needed multiple values.

105
00:06:58,465 --> 00:07:02,870
And let me show you that this worked correctly.

106
00:07:04,360 --> 00:07:08,340
So again, I get numbers that look pretty reasonable for

107
00:07:08,340 --> 00:07:09,989
the prices of the automobiles.

108
00:07:11,800 --> 00:07:14,921
And let me go back to Excel here, and

109
00:07:14,921 --> 00:07:17,949
I&#39;m gonna pick that web service.

110
00:07:20,974 --> 00:07:25,067
And I don&#39;t have to change any of this, cuz it&#39;s the same.

111
00:07:25,067 --> 00:07:29,667
Let me just change that, so you can see it&#39;s back to column one

112
00:07:29,667 --> 00:07:33,240
again and I&#39;ll run the prediction and there.

113
00:07:33,240 --> 00:07:35,880
So the header changed and I got a correct value back.

114
00:07:38,010 --> 00:07:43,720
So keep that in mind, if you&#39;re using R in line,

115
00:07:43,720 --> 00:07:46,870
in your work flow, in your Azure Machine Learning experiment.

116
00:07:46,870 --> 00:07:49,640
That you may need to make some changes so

117
00:07:49,640 --> 00:07:53,370
that the R code operates correctly.

118
00:07:53,370 --> 00:07:54,560
That you&#39;ve saved,

119
00:07:54,560 --> 00:07:58,170
maybe values as I&#39;ve done from the training data.

120
00:07:58,170 --> 00:08:02,160
And then, apply those more as a transform than trying

121
00:08:02,160 --> 00:08:06,250
to recompute some value that requires multiple rows.

122
00:08:06,250 --> 00:08:10,203
And hopefully, that will give you some more flexibility on how

123
00:08:10,203 --> 00:08:13,046
you can apply R with Azure Machine Learning.

