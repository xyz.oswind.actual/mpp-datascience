0
00:00:05,476 --> 00:00:06,930
Hi and welcome.

1
00:00:06,930 --> 00:00:10,671
So Cynthia&#39;s talked about regression model a little a bit

2
00:00:10,671 --> 00:00:14,259
about the theory, and little bit about how you evaluate

3
00:00:14,259 --> 00:00:17,670
the performance of regression models.

4
00:00:17,670 --> 00:00:20,470
So in this sequence, I&#39;d like to show you a practical example of

5
00:00:20,470 --> 00:00:24,330
constructing and evaluating a regression model.

6
00:00:24,330 --> 00:00:27,930
So if you look at my screen here, first of, we call,

7
00:00:27,930 --> 00:00:31,710
we&#39;ve been preparing this automobile&#39;s dataset for

8
00:00:32,870 --> 00:00:35,670
quite some time now in other demos.

9
00:00:35,670 --> 00:00:38,510
So we have a long chain of things that are happening

10
00:00:38,510 --> 00:00:41,040
here which we&#39;re not gonna go back and talk about,

11
00:00:41,040 --> 00:00:43,210
cuz we&#39;ve already done that.

12
00:00:43,210 --> 00:00:46,520
I&#39;m just gonna zoom in on the bottom here, so

13
00:00:46,520 --> 00:00:48,220
you can see the new stuff.

14
00:00:49,960 --> 00:00:53,430
And, so the first things, so we called the last step

15
00:00:53,430 --> 00:00:56,638
we did in preparing was to normalize the numeric columns.

16
00:00:56,638 --> 00:01:00,852
So then,

17
00:01:00,852 --> 00:01:06,230
we select columns from the data set because, there&#39;s two columns

18
00:01:06,230 --> 00:01:11,240
here that we now need to remove and let me just show

19
00:01:11,240 --> 00:01:13,790
you the column selector here and you&#39;ll see what&#39;s going on.

20
00:01:15,190 --> 00:01:18,530
So I used the width rules, all columns and

21
00:01:18,530 --> 00:01:21,180
I&#39;m gonna exclude by column name, number of cylinders.

22
00:01:21,180 --> 00:01:23,366
So remember we have number of cylinders and

23
00:01:23,366 --> 00:01:25,014
we have number of cylinders2.

24
00:01:25,014 --> 00:01:28,635
Where we consolidate at the number of categories of that

25
00:01:28,635 --> 00:01:30,485
categorical variable, and

26
00:01:30,485 --> 00:01:34,412
we&#39;re also going to fit our model on log of price, LN price,

27
00:01:34,412 --> 00:01:37,510
which we compute it with one of the transforms.

28
00:01:38,890 --> 00:01:42,789
So having price in the data set is going to really confound

29
00:01:42,789 --> 00:01:46,455
the training of the model, so we need to remove that.

30
00:01:48,224 --> 00:01:49,810
So what&#39;s the next step?

31
00:01:49,810 --> 00:01:53,133
Well, we need to split the data as we always do and

32
00:01:53,133 --> 00:01:56,781
we need to split it at least into a training data set and

33
00:01:56,781 --> 00:01:58,340
evaluation data set.

34
00:02:00,560 --> 00:02:04,637
And so I&#39;m just gonna do that simple two way split here.

35
00:02:04,637 --> 00:02:09,755
And I&#39;m gonna split by rows, I am gonna use 70% of my

36
00:02:09,755 --> 00:02:15,117
data to train and 30% of my data to score and evaluate.

37
00:02:15,117 --> 00:02:18,864
I am gonna pick a randomized because I don&#39;t I just want

38
00:02:18,864 --> 00:02:22,611
some random selection of those rows that I can train and

39
00:02:22,611 --> 00:02:23,601
evaluate on.

40
00:02:23,601 --> 00:02:25,307
I have set a seed and

41
00:02:25,307 --> 00:02:29,477
I am not going to do any fancy stratification.

42
00:02:29,477 --> 00:02:31,187
So just to keep things simple here,

43
00:02:31,187 --> 00:02:33,190
we&#39;re just gonna do linear regression.

44
00:02:33,190 --> 00:02:35,960
We&#39;re gonna use linear regression to

45
00:02:35,960 --> 00:02:38,150
try to predict the price of the automobile.

46
00:02:38,150 --> 00:02:41,704
So pick ordinary list wares.

47
00:02:41,704 --> 00:02:47,363
Pick a fairly small L2 regularization weight.

48
00:02:47,363 --> 00:02:50,390
[COUGH] And without going into any details here.

49
00:02:52,240 --> 00:02:58,620
We&#39;re not gonna force model parameters towards zero to hard.

50
00:02:58,620 --> 00:03:01,369
Now we could try other values of this of course to try to

51
00:03:01,369 --> 00:03:02,293
improve things.

52
00:03:02,293 --> 00:03:06,450
I&#39;m just gonna take an initial guess of a fairly small weight.

53
00:03:06,450 --> 00:03:09,206
Notice I&#39;ve unchecked the include intercept term.

54
00:03:09,206 --> 00:03:12,116
And this is something that&#39;s important to think about, and

55
00:03:12,116 --> 00:03:13,340
a little subtle.

56
00:03:13,340 --> 00:03:16,170
We have a lot of categorical variables,

57
00:03:16,170 --> 00:03:20,285
which means that we have different levels for

58
00:03:20,285 --> 00:03:22,715
different categories of multiple variables.

59
00:03:22,715 --> 00:03:27,326
So by forcing also an intercept term there&#39;s a chance where,

60
00:03:27,326 --> 00:03:31,581
which is the point of at which the regression line crosses

61
00:03:31,581 --> 00:03:32,126
zero.

62
00:03:32,126 --> 00:03:33,235
First off, it&#39;s a little strange.

63
00:03:33,235 --> 00:03:37,640
There are no zero priced cars, conceptually, right?

64
00:03:37,640 --> 00:03:42,218
And second off you don&#39;t wanna wind up with an over

65
00:03:42,218 --> 00:03:47,342
parameterized model and I&#39;ve picked a random seed here

66
00:03:47,342 --> 00:03:53,132
just truly at random and I&#39;m gonna allow a known categorical.

67
00:03:53,132 --> 00:03:56,230
So now I have to set up to train the model, so

68
00:03:56,230 --> 00:04:00,451
my train model module as usual I take the untrained model as

69
00:04:00,451 --> 00:04:04,839
one input here on the left, and my 70% of the data that I&#39;m

70
00:04:04,839 --> 00:04:09,170
using for training as my input, the training on the right.

71
00:04:10,420 --> 00:04:14,514
And I&#39;ve used the column selector and I&#39;ve just picked

72
00:04:14,514 --> 00:04:18,794
that log price LN price column that&#39;s gonna be our label.

73
00:04:18,793 --> 00:04:21,501
To everything else that&#39;s down to this point

74
00:04:21,500 --> 00:04:24,930
is a feature except for that one column which a is a label.

75
00:04:26,200 --> 00:04:29,664
And then I&#39;m gonna score that model I&#39;m just gonna

76
00:04:29,664 --> 00:04:31,932
append the scores and evaluate.

77
00:04:31,932 --> 00:04:35,410
And there&#39;s no parameters for an evaluation module.

78
00:04:35,410 --> 00:04:39,698
So let me save my experiment, and I&#39;m gonna run it for you.

79
00:04:44,791 --> 00:04:45,507
Okay, there.

80
00:04:45,507 --> 00:04:47,130
The experiment has run.

81
00:04:47,130 --> 00:04:52,305
But let me show you a little bit about the results.

82
00:04:52,305 --> 00:04:57,729
So first, I&#39;m gonna visualize just the raw scored output here.

83
00:04:57,729 --> 00:05:01,066
And there&#39;s a couple of things we can note about that.

84
00:05:01,066 --> 00:05:05,643
And remember, this is done from the 30% of data that was not

85
00:05:05,643 --> 00:05:07,500
used to train the model.

86
00:05:07,500 --> 00:05:10,735
So we scored a label, for

87
00:05:10,735 --> 00:05:15,740
30% of the data without using the Noan price.

88
00:05:15,740 --> 00:05:19,580
But we have this Noan log price that we can compare to.

89
00:05:19,580 --> 00:05:21,610
So first off, notice a couple of things about

90
00:05:22,700 --> 00:05:25,970
this Noan log price versus the scored label.

91
00:05:25,970 --> 00:05:29,252
So the first row, the match is pretty good, right?

92
00:05:29,252 --> 00:05:35,219
It&#39;s 1.91 effectively, versus 1.94.

93
00:05:35,219 --> 00:05:38,397
So we&#39;re really close, and this one isn&#39;t too bad.

94
00:05:38,397 --> 00:05:40,390
And a few, like this one, is not so good.

95
00:05:43,610 --> 00:05:49,530
So, and the other thing to notice is there&#39;s

96
00:05:49,530 --> 00:05:54,790
a standard deviation of that log price of about one, okay?

97
00:05:54,790 --> 00:05:57,100
Just keep that number in mind as we go through this.

98
00:05:58,280 --> 00:06:01,938
So if I click on Scored Label, and scroll down so

99
00:06:01,938 --> 00:06:07,070
you can see the visualization, I can do a visual comparison here.

100
00:06:07,070 --> 00:06:10,717
Of scored label on this horizontal axis, which is what

101
00:06:10,717 --> 00:06:14,763
the machine learning algorithm computed versus log price,

102
00:06:14,763 --> 00:06:18,440
l n price, which is the actual right answer.

103
00:06:18,440 --> 00:06:22,060
So for the majority of these points they fall pretty closely

104
00:06:22,060 --> 00:06:24,940
to a straight line which is just what we expect for

105
00:06:24,940 --> 00:06:27,920
a linear regression model, but there are these outliers.

106
00:06:27,920 --> 00:06:30,520
See there&#39;s some up here and some down there.

107
00:06:30,520 --> 00:06:33,570
So, if we are going to continue to work on this experiment and

108
00:06:33,570 --> 00:06:36,260
try to improve it, we need to understand those outliers.

109
00:06:36,260 --> 00:06:41,187
For example, recall that in a previous demo I

110
00:06:41,187 --> 00:06:46,505
noted that luxury cars seem to have some behavior

111
00:06:46,505 --> 00:06:51,055
that&#39;s difference from ordinary cars.

112
00:06:51,055 --> 00:06:52,500
But that looks pretty promising.

113
00:06:54,250 --> 00:06:56,880
Now we can look at this little more quantitatively,

114
00:06:56,880 --> 00:06:58,340
that was a qualitative look.

115
00:06:59,420 --> 00:07:04,226
If we visualize the output of this evaluate model.

116
00:07:04,226 --> 00:07:07,231
So there&#39;s a number of statistics here and

117
00:07:07,231 --> 00:07:10,322
I&#39;m not gonna talk about everyone of them.

118
00:07:10,322 --> 00:07:16,210
But here&#39;s root mean squared error and remember we had

119
00:07:16,210 --> 00:07:22,868
the actual standard deviation of the log price was about one.

120
00:07:22,868 --> 00:07:24,132
And it&#39;s been reduced.

121
00:07:24,132 --> 00:07:25,736
The squared error is about half that.

122
00:07:25,736 --> 00:07:28,405
It&#39;s about 0.5.

123
00:07:28,405 --> 00:07:30,649
And there are some other,

124
00:07:30,649 --> 00:07:34,222
relative squared error is about .24.

125
00:07:34,222 --> 00:07:36,620
So that&#39;s relative to the value of the price.

126
00:07:36,620 --> 00:07:38,290
So that&#39;s all looking pretty good.

127
00:07:39,650 --> 00:07:42,670
And then also there&#39;s this coefficient of determination.

128
00:07:42,670 --> 00:07:49,130
So as you recall that the reduction in

129
00:07:49,130 --> 00:07:54,510
variants that the model gave over simply

130
00:07:54,510 --> 00:07:59,460
saying the labels were the same as the original values.

131
00:07:59,460 --> 00:08:00,280
Or I&#39;m sorry. The scored

132
00:08:00,280 --> 00:08:03,570
labels were the same as the original values of the labels.

133
00:08:03,570 --> 00:08:08,889
So we&#39;ve reduced that variance by 75%, almost 76%.

134
00:08:08,889 --> 00:08:12,253
So that&#39;s, for a first try, that&#39;s pretty good.

135
00:08:12,253 --> 00:08:14,653
And if we look at the error histogram,

136
00:08:14,653 --> 00:08:17,878
we see part of what&#39;s skewing those statistics and

137
00:08:17,878 --> 00:08:21,778
making them a little high is we do have these outliers with high

138
00:08:21,778 --> 00:08:24,880
error and the rest look pretty well behaved.

139
00:08:24,880 --> 00:08:29,745
So that gives you some quick practical

140
00:08:29,745 --> 00:08:33,851
overview of how we construct and

141
00:08:33,851 --> 00:08:41,312
evaluate a regression machine learning module, model.

