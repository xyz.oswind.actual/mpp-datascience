0
00:00:05,227 --> 00:00:10,182
Hi, so in the previous sequence, I discussed the concepts

1
00:00:10,182 --> 00:00:13,750
of publishing a model as a web service, and

2
00:00:13,750 --> 00:00:18,903
how you can use that capability of Azure Machine Learning, to

3
00:00:18,903 --> 00:00:24,280
publish your model out to some large number of potential users.

4
00:00:24,280 --> 00:00:26,530
So let me show you how that actually works.

5
00:00:26,530 --> 00:00:29,090
How do you actually publish a machine learning model.

6
00:00:29,090 --> 00:00:34,100
So on my screen here, I have the model we&#39;ve worked on for

7
00:00:34,100 --> 00:00:36,320
income classification.

8
00:00:36,320 --> 00:00:38,890
And you can see it&#39;s been run.

9
00:00:38,890 --> 00:00:40,550
So we know it works.

10
00:00:40,550 --> 00:00:41,840
We looked at that already.

11
00:00:43,570 --> 00:00:45,340
So all I have to do is,

12
00:00:45,340 --> 00:00:48,690
I go down here to this icon that says set up web service.

13
00:00:49,950 --> 00:00:53,752
And then I click on predictive web service recommended.

14
00:00:53,752 --> 00:00:55,830
And note it says recommended,

15
00:00:55,830 --> 00:00:58,600
so, yes go with the recommendation.

16
00:00:58,600 --> 00:01:01,140
Let me just click on that and we&#39;ll watch what happens,

17
00:01:01,140 --> 00:01:01,780
it&#39;s kinda cool.

18
00:01:04,910 --> 00:01:07,372
And see it&#39;s transforming the experiment.

19
00:01:07,372 --> 00:01:11,828
All right, let me rearrange this just for

20
00:01:11,828 --> 00:01:16,538
a minute so that you can see what happened and

21
00:01:16,538 --> 00:01:21,648
you&#39;ll get a feel I think for the outcome here.

22
00:01:27,881 --> 00:01:30,755
And let us rearrange this a little bit so

23
00:01:30,755 --> 00:01:32,941
we can look at the thing here.

24
00:01:35,362 --> 00:01:36,450
All right, there, so.

25
00:01:39,331 --> 00:01:42,900
So, we can zoom in a little bit so you can read it I hope.

26
00:01:44,000 --> 00:01:48,790
So, notice that a couple of things have happened here.

27
00:01:48,790 --> 00:01:54,350
What was this normalization has now been converted

28
00:01:54,350 --> 00:01:59,080
into a transform and an applied transform module for example.

29
00:01:59,080 --> 00:02:03,110
And that happens to any sort of transformation that you&#39;re

30
00:02:03,110 --> 00:02:08,890
performing on the data inline where you needed multiple

31
00:02:08,889 --> 00:02:12,970
values say to come up with the normalization in this case.

32
00:02:12,970 --> 00:02:17,880
Our trained model is now save here as this module and

33
00:02:17,880 --> 00:02:19,800
go as an input to the score model.

34
00:02:19,800 --> 00:02:22,500
So we have our web services input,

35
00:02:22,500 --> 00:02:25,930
which flows through our transformations.

36
00:02:27,190 --> 00:02:30,580
We score the model and we have a web services output.

37
00:02:30,580 --> 00:02:34,641
We also notice we still have our original data set hanging here.

38
00:02:34,641 --> 00:02:38,949
And don&#39;t remove that, that&#39;s where the schema is defined,

39
00:02:38,949 --> 00:02:41,318
it also is there for you to test it.

40
00:02:41,318 --> 00:02:44,192
And so I mentioned this before but

41
00:02:44,192 --> 00:02:49,333
it&#39;s a good idea to get a select columns in data set module.

42
00:02:52,281 --> 00:02:56,803
And we can stick that right at the end here so that we&#39;re not

43
00:02:56,803 --> 00:03:00,970
publishing things that our users don&#39;t want to see.

44
00:03:00,970 --> 00:03:04,477
The default would be to publish back all the columns that they

45
00:03:04,477 --> 00:03:05,852
have already given us,

46
00:03:05,852 --> 00:03:08,416
which is not probably what they had in mind.

47
00:03:12,561 --> 00:03:15,680
I&#39;m just gonna return the scored labels in this case.

48
00:03:15,680 --> 00:03:18,370
We could return the scored probabilities as well but

49
00:03:18,370 --> 00:03:19,935
we&#39;ll just return one column.

50
00:03:19,935 --> 00:03:21,300
Just keep this really simple.

51
00:03:22,400 --> 00:03:24,892
So let me run that and make sure everything works.

52
00:03:30,130 --> 00:03:34,278
There, so the experiment has now run and

53
00:03:34,278 --> 00:03:37,819
I can test what the output is here.

54
00:03:41,504 --> 00:03:46,180
And there&#39;s my scored labels, so it looks like this is working.

55
00:03:48,180 --> 00:03:50,650
Now, let me just show you one

56
00:03:52,350 --> 00:03:55,160
other bit here which is now we have to deploy.

57
00:03:56,840 --> 00:04:00,890
So we&#39;ve got our model, we&#39;ve got it ready for publication.

58
00:04:00,890 --> 00:04:03,410
We&#39;re actually gonna deploy it.

59
00:04:03,410 --> 00:04:08,410
And there, so now we have this dashboard and it has things like

60
00:04:08,410 --> 00:04:14,920
the API key, and how the request responds, and things like that.

61
00:04:14,920 --> 00:04:16,990
Which I&#39;m gonna talk about in the next sequence

62
00:04:16,990 --> 00:04:20,970
when I actually demonstrate using this web services input.

63
00:04:20,970 --> 00:04:22,630
But let me go back to the model here.

64
00:04:25,090 --> 00:04:30,950
So this model has now been published as a web service and

65
00:04:30,950 --> 00:04:35,870
anybody with that API key and the URL can get to it.

66
00:04:35,870 --> 00:04:39,690
So you might wanna keep that API key secret.

67
00:04:39,690 --> 00:04:42,370
And notice there&#39;s two tabs here, for now there&#39;s a training

68
00:04:42,370 --> 00:04:44,180
experiment and the predictive experiment.

69
00:04:44,180 --> 00:04:49,200
So what if I decided that I had a better model, maybe I say,

70
00:04:49,200 --> 00:04:52,740
well maybe I&#39;m gonna use something that isn&#39;t a logistic

71
00:04:52,740 --> 00:04:58,090
regression model or maybe I have some new data that I think

72
00:04:58,090 --> 00:05:02,600
is more comprehensively covering the space I&#39;m trying to model.

73
00:05:02,600 --> 00:05:05,850
So I could add those here or

74
00:05:05,850 --> 00:05:10,600
here or maybe I&#39;ve come up with some better filtering in here.

75
00:05:10,600 --> 00:05:14,170
And then all I have to do, once I change it,

76
00:05:14,170 --> 00:05:17,810
is I go down again to the set Up web services and

77
00:05:17,810 --> 00:05:20,370
I hit Update Predictive Experiment.

78
00:05:20,370 --> 00:05:23,370
What&#39;s that gonna do is it&#39;s gonna create a new

79
00:05:23,370 --> 00:05:24,800
predictive experiment.

80
00:05:25,970 --> 00:05:28,380
But what&#39;s cool about this is it,

81
00:05:28,380 --> 00:05:33,290
the API key and the URL don&#39;t change.

82
00:05:33,290 --> 00:05:37,840
So any user who&#39;s using that published web service,

83
00:05:37,840 --> 00:05:40,330
isn&#39;t gonna need to update anything.

84
00:05:40,330 --> 00:05:42,620
And if you&#39;ve done your job well,

85
00:05:42,620 --> 00:05:45,950
all they should see is somewhat improved results.

86
00:05:48,570 --> 00:05:53,980
So, in this sequence, I hoped you&#39;ve seen just how easy it is

87
00:05:53,980 --> 00:05:58,410
to go from a model that you&#39;re happy with, you&#39;ve tested it out

88
00:05:58,410 --> 00:06:02,030
and you feel like it&#39;s ready for your colleagues to use it.

89
00:06:02,030 --> 00:06:05,800
And with just a few clicks here, we&#39;ve published it and

90
00:06:05,800 --> 00:06:07,620
it&#39;s ready for them to use.

91
00:06:07,620 --> 00:06:09,943
And next I&#39;ll show you how you do that.

