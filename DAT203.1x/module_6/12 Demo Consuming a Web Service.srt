0
00:00:04,725 --> 00:00:05,890
Hi.

1
00:00:05,890 --> 00:00:08,890
So in the last demo, I published

2
00:00:08,890 --> 00:00:12,950
the two class classification model as a web service.

3
00:00:12,950 --> 00:00:14,180
So in this demo,

4
00:00:14,180 --> 00:00:20,810
I&#39;d like to show you how we can consume that in Excel.

5
00:00:20,810 --> 00:00:25,229
So recall, we ended up with this dashboard with various

6
00:00:25,229 --> 00:00:27,775
properties of our web service.

7
00:00:28,925 --> 00:00:35,770
And over on this tab, I&#39;ve started an Excel Online session.

8
00:00:35,770 --> 00:00:39,810
So what I need to do is import a plugin, so

9
00:00:39,810 --> 00:00:44,791
I&#39;m gonna click on the Insert tab, on Office Add-ins.

10
00:00:44,791 --> 00:00:46,990
Then I&#39;m gonna click on the Store.

11
00:00:49,860 --> 00:00:54,495
And I&#39;m gonna search for Azure Machine Learning,

12
00:00:54,495 --> 00:00:57,095
I&#39;ll just search for that.

13
00:01:01,659 --> 00:01:02,368
And there it is.

14
00:01:05,101 --> 00:01:07,064
And I&#39;m gonna say I want to trust it, of course, and

15
00:01:07,064 --> 00:01:08,147
it&#39;s gonna get installed.

16
00:01:11,593 --> 00:01:12,820
All right, there.

17
00:01:12,820 --> 00:01:15,370
So you see there&#39;s a couple of web services that are just for

18
00:01:15,370 --> 00:01:17,055
demo that you can play with if you want.

19
00:01:17,055 --> 00:01:20,513
But I&#39;m gonna add a new web service here with, obviously,

20
00:01:20,513 --> 00:01:21,560
Add Web Service.

21
00:01:23,160 --> 00:01:25,110
So there&#39;s two things I need right away.

22
00:01:25,110 --> 00:01:27,550
I need the URL and the API key.

23
00:01:27,550 --> 00:01:30,970
So I go back to that dashboard, and

24
00:01:30,970 --> 00:01:35,340
you see here there&#39;s a request response batch execution.

25
00:01:35,340 --> 00:01:38,650
So in this case, I&#39;m gonna do request response.

26
00:01:38,650 --> 00:01:42,320
And if I click on that, right-click on that, and

27
00:01:42,320 --> 00:01:43,940
I can copy that link.

28
00:01:43,940 --> 00:01:45,353
Notice, that&#39;s the URL I should use.

29
00:01:45,353 --> 00:01:47,904
It&#39;s not the URL of the dashboard page or

30
00:01:47,904 --> 00:01:51,141
some other URL, it&#39;s that request response URL.

31
00:01:51,141 --> 00:01:54,580
And so I&#39;ll just paste that in there.

32
00:01:57,530 --> 00:01:58,860
And now I need an API key.

33
00:02:00,090 --> 00:02:02,790
I&#39;ll go back to my dashboard, and here&#39;s an API key,

34
00:02:02,790 --> 00:02:05,100
this big, gnarly thing,

35
00:02:05,100 --> 00:02:09,341
which hopefully you guys will all keep it a secret for me.

36
00:02:09,341 --> 00:02:10,458
Oops.

37
00:02:14,341 --> 00:02:15,123
Just copy all that.

38
00:02:20,935 --> 00:02:23,500
And that&#39;s all I need, so I&#39;ve got those two pieces.

39
00:02:23,500 --> 00:02:26,160
So now I&#39;m gonna add a new web service.

40
00:02:29,740 --> 00:02:30,790
And, there, it&#39;s been added.

41
00:02:31,820 --> 00:02:35,761
And you notice, there&#39;s no way here I can get the,

42
00:02:35,761 --> 00:02:40,170
I don&#39;t see that API key anymore, I don&#39;t see that URL.

43
00:02:40,170 --> 00:02:44,199
It&#39;s all encrypted and embedded here.

44
00:02:44,199 --> 00:02:46,943
But there are some interesting bits here I just want to show

45
00:02:46,943 --> 00:02:47,507
you as I go.

46
00:02:47,507 --> 00:02:52,061
So here, I can see the schema for the input and for

47
00:02:52,061 --> 00:02:53,295
the output.

48
00:02:53,295 --> 00:02:55,103
And.

49
00:02:56,883 --> 00:03:00,823
So let me go ahead and make this go.

50
00:03:00,823 --> 00:03:04,608
The first thing I can do is, I&#39;m gonna say my data has headers,

51
00:03:04,608 --> 00:03:07,321
you can say yes or no, you can click that, but

52
00:03:07,321 --> 00:03:08,790
I&#39;m gonna click it on.

53
00:03:08,790 --> 00:03:13,755
So let me, if you do this use sample data, it will,

54
00:03:13,755 --> 00:03:17,334
let me select a starting point here,

55
00:03:17,334 --> 00:03:21,501
and then I&#39;m gonna say, Use sample data.

56
00:03:21,501 --> 00:03:24,580
And there.

57
00:03:24,580 --> 00:03:28,680
So now you see the headers of the input data

58
00:03:28,680 --> 00:03:31,040
are now displayed for me for my convenience.

59
00:03:33,080 --> 00:03:34,500
And let me get some sample data.

60
00:03:34,500 --> 00:03:40,560
So on my desktop, not on, I&#39;m using Excel Online here,

61
00:03:40,560 --> 00:03:45,320
from OneDrive, so this is Excel just running locally.

62
00:03:45,320 --> 00:03:50,887
I&#39;m just gonna grab four or five rows here.

63
00:03:53,544 --> 00:03:55,415
And I&#39;m gonna copy them.

64
00:03:55,415 --> 00:04:00,220
And I&#39;ll go back to my Excel Online.

65
00:04:01,290 --> 00:04:02,485
I&#39;m just gonna paste that in.

66
00:04:10,000 --> 00:04:16,868
Okay, and so now I need to, since I said I&#39;m using headers,

67
00:04:16,868 --> 00:04:24,050
I need to select all that data, including the header, okay?

68
00:04:24,050 --> 00:04:26,247
So those first six rows, which includes that header.

69
00:04:26,247 --> 00:04:30,596
And if I click here, you see, I&#39;ve got that range there, so

70
00:04:30,596 --> 00:04:34,230
I&#39;m gonna say that&#39;s what I want it.

71
00:04:34,230 --> 00:04:35,330
And where do I want my output?

72
00:04:35,330 --> 00:04:39,242
Well, let&#39;s put it in column P here, starting with P1, and

73
00:04:39,242 --> 00:04:41,810
that&#39;s gonna be cuz it&#39;ll be a header.

74
00:04:42,950 --> 00:04:43,998
So if I did this right,

75
00:04:43,998 --> 00:04:45,772
all I should have to do is hit Predict.

76
00:04:50,397 --> 00:04:57,920
And there, I have my scored label.

77
00:04:57,920 --> 00:05:00,147
Let me make this a little bigger so you can see it.

78
00:05:00,147 --> 00:05:03,345
And I&#39;ve got the original income.

79
00:05:03,345 --> 00:05:10,142
So I&#39;ve got everybody correct answers except for one.

80
00:05:10,142 --> 00:05:14,899
So out of my five, I&#39;ve got four right answers and

81
00:05:14,899 --> 00:05:16,720
one wrong answer.

82
00:05:18,770 --> 00:05:23,570
So anyway, that&#39;s how easy it is to install

83
00:05:24,650 --> 00:05:28,510
the plugin and set it up to use it.

84
00:05:28,510 --> 00:05:31,060
And now that, by the way, now that I have that installed

85
00:05:31,060 --> 00:05:33,960
in that spreadsheet, I could distribute that to

86
00:05:33,960 --> 00:05:36,960
any number of colleagues who can then run my model.

87
00:05:36,960 --> 00:05:40,987
So it really simplifies the whole process of making machine

88
00:05:40,987 --> 00:05:44,942
learning models accessible if you&#39;re a data scientist.

89
00:05:44,942 --> 00:05:52,246
[BLANK AUDIO]

