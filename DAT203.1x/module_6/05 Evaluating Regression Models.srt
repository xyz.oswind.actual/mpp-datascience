0
00:00:05,490 --> 00:00:07,710
Let&#39;s talk about how to evaluate regression models.

1
00:00:08,780 --> 00:00:13,468
So, here I have our features and then the true labels

2
00:00:13,468 --> 00:00:17,200
which are real-valued because we&#39;re doing regression.

3
00:00:17,200 --> 00:00:20,370
And, here are our predicted labels.

4
00:00:20,370 --> 00:00:24,191
So this is the income, and this is the predicted income,

5
00:00:24,191 --> 00:00:25,389
which is f(x).

6
00:00:25,389 --> 00:00:29,981
Now, we have to figure out how to evaluate the closeness of

7
00:00:29,981 --> 00:00:34,401
the truth, y, to what we predicted, which is f(x).

8
00:00:34,401 --> 00:00:38,887
Okay, so here&#39;s what I propose, which is y-f(x).

9
00:00:38,887 --> 00:00:41,054
Looks great?

10
00:00:41,054 --> 00:00:41,639
Well, not really.

11
00:00:41,639 --> 00:00:44,370
Because see, why didn&#39;t I choose that one instead?

12
00:00:45,530 --> 00:00:49,444
Yeah, if I choose this one or this one, it&#39;s just bad because

13
00:00:49,444 --> 00:00:52,551
I wanna penalize mistakes in either direction.

14
00:00:52,551 --> 00:00:55,900
If f is bigger than y, it&#39;s a mistake.

15
00:00:55,900 --> 00:00:58,750
If f is smaller than y, it&#39;s a mistake.

16
00:00:58,750 --> 00:01:03,759
So I&#39;m gonna use this penalty here, which makes sure that we

17
00:01:03,759 --> 00:01:08,687
count deviations in both directions, either direction.

18
00:01:11,789 --> 00:01:15,944
So we could assess what&#39;s called the mean absolute error which is

19
00:01:15,944 --> 00:01:20,570
the sum of all of these errors, and that&#39;s one way to do it.

20
00:01:20,570 --> 00:01:24,982
But you could also square all of them and get a different thing.

21
00:01:24,982 --> 00:01:28,440
Okay, so this is what&#39;s called least square error.

22
00:01:29,500 --> 00:01:32,066
It&#39;s called the sum of squares errors or

23
00:01:32,066 --> 00:01:34,942
the mean square error but that&#39;s what it is.

24
00:01:34,942 --> 00:01:37,443
You should think of it as capturing errors in both

25
00:01:37,443 --> 00:01:39,958
directions like the absolute value that we had.

26
00:01:39,958 --> 00:01:45,106
The same deal, analyze how far y is from f in either direction.

27
00:01:50,266 --> 00:01:54,086
And then the root mean squared error means take the square root

28
00:01:54,086 --> 00:01:55,858
of the least squares error.

29
00:01:58,955 --> 00:02:00,270
Okay, so here&#39;s a picture of it.

30
00:02:02,240 --> 00:02:06,613
So here, we would want to measure

31
00:02:06,613 --> 00:02:11,309
the arrow using f(x)- y, okay?

32
00:02:11,309 --> 00:02:17,885
And here we would want to use y minus f.

33
00:02:17,885 --> 00:02:20,615
So we could use absolute values to get errors in both

34
00:02:20,615 --> 00:02:21,384
directions.

35
00:02:23,769 --> 00:02:27,657
And instead we&#39;ll use the squared loss or

36
00:02:27,657 --> 00:02:29,551
the squared error.

37
00:02:29,551 --> 00:02:31,951
And then if we add up all those errors,

38
00:02:31,951 --> 00:02:33,984
it&#39;s the sum of squares error.

39
00:02:33,984 --> 00:02:37,611
And we&#39;ll get back to that in a minute.

40
00:02:37,611 --> 00:02:41,630
Now for these particular data, a line for f(x) works really well.

41
00:02:42,790 --> 00:02:45,208
It explains most of the variation, and

42
00:02:45,208 --> 00:02:47,783
the sum of squares error is pretty small.

43
00:02:47,783 --> 00:02:51,374
But even though here the regression function explains

44
00:02:51,374 --> 00:02:53,637
most of the variation in the data,

45
00:02:53,637 --> 00:02:55,596
that isn&#39;t always the case.

46
00:02:55,596 --> 00:02:58,821
In many times, the regression can&#39;t really explain

47
00:02:58,821 --> 00:03:00,909
the variation in the data as well.

48
00:03:00,909 --> 00:03:03,862
Like in this case.

49
00:03:03,862 --> 00:03:07,358
So let&#39;s say that we wanna measure how much of

50
00:03:07,358 --> 00:03:11,500
the variation in the y&#39;s is explained by our model f.

51
00:03:12,820 --> 00:03:15,038
So I&#39;m leading up to the definition of r squared here, so

52
00:03:15,038 --> 00:03:16,019
give me a second to do it.

53
00:03:18,405 --> 00:03:20,560
Lets just start with the data.

54
00:03:20,560 --> 00:03:25,415
What is the total variation of these data?

55
00:03:25,415 --> 00:03:29,090
Let us define the total sum of squares, which defines all of

56
00:03:29,090 --> 00:03:33,058
the variation around the data, and then we&#39;ll define it around

57
00:03:33,058 --> 00:03:36,016
a good starting point which is the sample mean.

58
00:03:36,016 --> 00:03:40,563
So, this thing is just the average value of y&#39;s,

59
00:03:40,563 --> 00:03:44,800
the sample mean of the y&#39;s, okay, this guy.

60
00:03:48,200 --> 00:03:51,980
Now to compute the total variation we look at all of

61
00:03:51,980 --> 00:03:55,400
the deviations from the sample mean, okay?

62
00:03:55,400 --> 00:03:57,629
So, this is called the total variation or

63
00:03:57,629 --> 00:03:59,037
the total sum of squares.

64
00:03:59,037 --> 00:04:02,901
You add up all the square distances between the y&#39;s and

65
00:04:02,901 --> 00:04:04,502
the sample mean y bar.

66
00:04:07,790 --> 00:04:11,724
Okay, so now, let&#39;s just look at

67
00:04:11,724 --> 00:04:16,720
the variation of f around y bar, okay?

68
00:04:16,720 --> 00:04:19,035
So, we&#39;ll take the distance between the predictions,

69
00:04:19,035 --> 00:04:22,890
f(x), and the y bar.

70
00:04:24,180 --> 00:04:30,362
And we&#39;ll call this regression sum of squares, so SSR.

71
00:04:30,362 --> 00:04:32,687
Sum of squares regression.

72
00:04:32,687 --> 00:04:36,028
And let&#39;s say that we&#39;re lucky and f is really good.

73
00:04:36,028 --> 00:04:39,200
Okay, so f(x), let&#39;s say it&#39;s almost on top of all the y&#39;s for

74
00:04:39,200 --> 00:04:40,490
all the data points.

75
00:04:40,490 --> 00:04:41,730
In that case,

76
00:04:41,730 --> 00:04:45,280
f is gonna explain a lot of the variation around y bar.

77
00:04:45,280 --> 00:04:48,490
So that total sum of squares from the last slide,

78
00:04:48,490 --> 00:04:52,485
it&#39;s gonna be pretty close to this regression sum of squares

79
00:04:52,485 --> 00:04:54,542
that I have here on this slide.

80
00:04:57,457 --> 00:05:00,659
Okay, and then the last one I&#39;ll talk about is the sum of

81
00:05:00,659 --> 00:05:03,195
squares error that the least squares loss.

82
00:05:04,410 --> 00:05:07,130
And this is the distance between the predictions and

83
00:05:07,130 --> 00:05:10,840
the truth, and I think you can see where I&#39;m going here.

84
00:05:10,840 --> 00:05:12,509
So, if the function&#39;s really good,

85
00:05:12,509 --> 00:05:15,130
then the sum of squares error is just gonna be really small.

86
00:05:16,510 --> 00:05:17,930
So what we want is for

87
00:05:17,930 --> 00:05:21,777
the regression sum of squares to be really big, so that

88
00:05:21,777 --> 00:05:26,042
the regression explains most of the variation in the data and

89
00:05:26,042 --> 00:05:29,655
we want the sum of squares error to be really small.

90
00:05:29,655 --> 00:05:33,291
Now, that doesn&#39;t always happen.

91
00:05:33,291 --> 00:05:35,920
Though here&#39;s a case where it doesn&#39;t.

92
00:05:35,920 --> 00:05:39,580
In this case there&#39;s a lot of variation in the y&#39;s and

93
00:05:39,580 --> 00:05:40,940
the regression function, this thing,

94
00:05:40,940 --> 00:05:42,980
it just can&#39;t explain all that variation.

95
00:05:42,980 --> 00:05:46,660
So the total variation around y-bar,

96
00:05:46,660 --> 00:05:49,260
that&#39;s really large, this guy is really large.

97
00:05:50,870 --> 00:05:54,270
And the SSR is kinda small.

98
00:05:55,930 --> 00:05:57,781
And then the sum of squares error is large, so

99
00:05:57,781 --> 00:05:58,570
that&#39;s not good.

100
00:06:01,380 --> 00:06:03,950
Now sometimes, as it turns out,

101
00:06:03,950 --> 00:06:06,990
if you&#39;re doing least squares regression, the sum of squares,

102
00:06:06,990 --> 00:06:10,280
the total sum of squares is actually equal to the sum of

103
00:06:10,280 --> 00:06:14,490
squares error plus the sum of squares regression, okay?

104
00:06:14,490 --> 00:06:17,346
It doesn&#39;t always hold, but it holds at least for

105
00:06:17,346 --> 00:06:20,204
least squares regression and it intuitively, it

106
00:06:20,204 --> 00:06:23,889
almost holds regardless of what method you use for regression.

107
00:06:23,889 --> 00:06:27,850
You see that the total variation has to get explained somehow.

108
00:06:27,850 --> 00:06:30,535
So, either the model f(x) is explaining it or

109
00:06:30,535 --> 00:06:32,020
it&#39;s not explaining it.

110
00:06:32,020 --> 00:06:36,451
And if the model is explaining it, then this SSR is large, and

111
00:06:36,451 --> 00:06:39,364
that SSE is small, and then vice versa.

112
00:06:39,364 --> 00:06:43,268
If the model&#39;s not doing very much, then the errors are large,

113
00:06:43,268 --> 00:06:45,959
this guy&#39;s large, and that guy&#39;s small.

114
00:06:45,959 --> 00:06:47,814
Okay, so let&#39;s look at that ratio.

115
00:06:47,814 --> 00:06:49,767
If the sum of squares error is small,

116
00:06:49,767 --> 00:06:51,345
we&#39;re in really good shape.

117
00:06:51,345 --> 00:06:55,040
The model is close to the true, truth, the y&#39;s.

118
00:06:56,190 --> 00:06:58,550
And if the sum of squares error is large, that&#39;s bad.

119
00:06:58,550 --> 00:06:59,590
Then we have large errors.

120
00:07:01,770 --> 00:07:03,246
And then we can just do 1 minus that.

121
00:07:03,246 --> 00:07:08,519
So if this thing is close to 0, then we&#39;re sad,

122
00:07:08,519 --> 00:07:13,160
and if it’s close to 1, then it’s good.

123
00:07:14,740 --> 00:07:17,776
So this is actually the formula for R squared.

124
00:07:17,776 --> 00:07:20,207
You can calculate R squared for any regression,

125
00:07:20,207 --> 00:07:22,519
you just calculate the sum of squares error and

126
00:07:22,519 --> 00:07:24,930
then the total sum of squares.

127
00:07:24,930 --> 00:07:25,980
And then compute 1 minus that,

128
00:07:25,980 --> 00:07:28,560
and if it’s close to 1 you can be happy.

129
00:07:28,560 --> 00:07:32,700
But if it&#39;s close to 0, it means you have a lot of unexplained

130
00:07:32,700 --> 00:07:34,305
variance in your data.

131
00:07:34,305 --> 00:07:38,557
Now remember, a lot of people mistakenly report R squared

132
00:07:38,557 --> 00:07:41,680
only in sample, on the training set.

133
00:07:41,680 --> 00:07:43,350
It&#39;s a huge mistake.

134
00:07:43,350 --> 00:07:46,320
A perfect R squared on the training sample just means your

135
00:07:46,320 --> 00:07:47,890
model memorized the training points.

136
00:07:47,890 --> 00:07:51,595
It doesn&#39;t mean the model can perform well out of sample.

137
00:07:51,595 --> 00:07:54,449
So, make sure you report the evaluation on data that you

138
00:07:54,449 --> 00:07:56,099
didn&#39;t create the model from.

