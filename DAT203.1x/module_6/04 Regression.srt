0
00:00:05,024 --> 00:00:06,270
Let&#39;s talk about regression.

1
00:00:07,790 --> 00:00:11,650
Regression is for predicting real valued outcomes.

2
00:00:11,650 --> 00:00:13,630
These are examples of regression problems,

3
00:00:13,630 --> 00:00:16,590
how many customers will arrive at our website next week,

4
00:00:16,590 --> 00:00:19,500
how many TVs will we sell next year.

5
00:00:19,500 --> 00:00:22,100
Can we predict someone&#39;s income from their click through

6
00:00:22,100 --> 00:00:23,480
information?

7
00:00:23,480 --> 00:00:25,440
These are all relevant regression problems.

8
00:00:27,200 --> 00:00:29,390
Now as in classification,

9
00:00:29,390 --> 00:00:33,030
each observation is represented by a set of numbers.

10
00:00:33,030 --> 00:00:35,570
We have features called X and we have labels called Y.

11
00:00:35,570 --> 00:00:38,930
The difference between classification and regression,

12
00:00:38,930 --> 00:00:42,295
is that in classification these labels are plus or minus one.

13
00:00:42,295 --> 00:00:43,539
Whereas in regression,

14
00:00:43,539 --> 00:00:45,441
the labels can take on any real value.

15
00:00:48,398 --> 00:00:51,674
Let&#39;s start with this simple example where we have a single

16
00:00:51,674 --> 00:00:53,490
feature called X.

17
00:00:53,490 --> 00:00:56,840
And let&#39;s say that X is the number of

18
00:00:56,840 --> 00:00:59,050
clicks the person makes on the Business Week website.

19
00:01:00,340 --> 00:01:02,400
And then we&#39;ll use that to predict the person&#39;s income.

20
00:01:03,610 --> 00:01:06,607
And maybe people who clicked more on the business week

21
00:01:06,607 --> 00:01:09,213
website have a higher income, I&#39;m not sure.

22
00:01:09,213 --> 00:01:12,712
So formally we&#39;re given our training points,

23
00:01:12,712 --> 00:01:14,993
XiYi, and there are n of them.

24
00:01:14,993 --> 00:01:17,255
And we want to create a regression model,

25
00:01:17,255 --> 00:01:20,500
F, that can predict the label y for a new x that comes along.

26
00:01:22,180 --> 00:01:25,530
So here since x is just the number of clicks,

27
00:01:25,530 --> 00:01:28,400
we have our function f which is

28
00:01:28,400 --> 00:01:29,952
just a function of the number of Business Week clicks.

29
00:01:31,380 --> 00:01:32,660
And it might be a really complicated

30
00:01:32,660 --> 00:01:33,960
function like this one.

31
00:01:33,960 --> 00:01:36,390
Is there anything wrong with this function?

32
00:01:36,390 --> 00:01:40,350
Hm, you might say that this function fits the data a little

33
00:01:40,350 --> 00:01:43,270
bit too tightly, it&#39;s a bit over fitted.

34
00:01:43,270 --> 00:01:45,790
And that means it probably won&#39;t predict very well on new

35
00:01:45,790 --> 00:01:47,120
observations that come along.

36
00:01:48,270 --> 00:01:51,740
And you might very well be right about that, in this case.

37
00:01:51,740 --> 00:01:53,590
This model definitely looks overfitted.

38
00:01:53,590 --> 00:01:57,350
So, here&#39;s an alternative model, this model is much simpler.

39
00:01:58,360 --> 00:02:00,980
So, this model is actually that each person&#39;s baseline

40
00:02:00,980 --> 00:02:02,640
income is $100,000.

41
00:02:02,640 --> 00:02:04,570
And then, for each time they click on the Business Week

42
00:02:04,570 --> 00:02:07,930
website, we add $5,000 to the estimate of their income.

43
00:02:07,930 --> 00:02:11,580
Okay, so the regression model is a straight line.

44
00:02:11,580 --> 00:02:15,150
And you might say, well, that&#39;s a pretty simple model,

45
00:02:15,150 --> 00:02:17,520
it might not capture a lot of the nuances in the data,

46
00:02:17,520 --> 00:02:19,500
so it won&#39;t predict very well.

47
00:02:20,550 --> 00:02:22,510
And you might indeed be right.

48
00:02:22,510 --> 00:02:25,540
So this is an example of under fitting.

49
00:02:25,540 --> 00:02:27,052
Third times a charm,

50
00:02:27,052 --> 00:02:31,620
here&#39;s a model that fits really nicely and its not over fitted.

51
00:02:31,620 --> 00:02:35,106
Finding models like this is exactly the goal of machine

52
00:02:35,106 --> 00:02:35,870
learning.

53
00:02:37,160 --> 00:02:38,812
Now, we don&#39;t just have to consider the number of

54
00:02:38,812 --> 00:02:41,720
Business Week clicks to predict someone&#39;s income.

55
00:02:41,720 --> 00:02:43,163
That&#39;s a really simple model,

56
00:02:43,163 --> 00:02:45,888
we could include all kinds of other feature like the number of

57
00:02:45,888 --> 00:02:48,146
visits they made to upscale furniture websites.

58
00:02:48,146 --> 00:02:50,968
The number of distinct people they emailed per day,

59
00:02:50,968 --> 00:02:54,690
the number of purchases of over $5,000 within the last month,

60
00:02:54,690 --> 00:02:55,603
whatever it is.

61
00:02:55,603 --> 00:02:57,942
And we could even consider these things multiplied together.

62
00:02:57,942 --> 00:03:01,296
Like you can multiply the number of visits to airlines times

63
00:03:01,296 --> 00:03:04,995
the number of visits to upscale furniture websites, whatever.

64
00:03:04,995 --> 00:03:07,917
Or I can have a complicated function like a log or

65
00:03:07,917 --> 00:03:11,495
a square root, as long as my function fits the data well and

66
00:03:11,495 --> 00:03:14,590
doesn&#39;t over fit, we&#39;re happy.

67
00:03:14,590 --> 00:03:17,820
So here are some examples of good regression applications.

68
00:03:17,820 --> 00:03:21,694
Regression is very often used to predict monetary amounts.

69
00:03:21,694 --> 00:03:24,281
And it&#39;s also often used to predict consumption or

70
00:03:24,281 --> 00:03:25,912
demand for products or energy.

71
00:03:25,912 --> 00:03:29,056
Like for instance power companies rely very heavily on

72
00:03:29,056 --> 00:03:30,294
regression models.

73
00:03:30,294 --> 00:03:34,175
Because they have to predict exactly how much energy is gonna

74
00:03:34,175 --> 00:03:35,095
be consumed.

75
00:03:35,095 --> 00:03:39,165
Because they have to make almost exactly that amount of energy

76
00:03:39,165 --> 00:03:40,740
for the following day.

77
00:03:41,750 --> 00:03:44,308
And Steve has some excellent examples for

78
00:03:44,308 --> 00:03:47,826
you on regression including one on automobile pricing.

