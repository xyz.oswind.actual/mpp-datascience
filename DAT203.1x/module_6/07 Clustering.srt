0
00:00:05,048 --> 00:00:08,343
Now we&#39;re gonna talk about unsupervised learning and

1
00:00:08,343 --> 00:00:09,345
in particular,

2
00:00:09,345 --> 00:00:12,788
clustering which is an unsupervised learning problem.

3
00:00:12,788 --> 00:00:16,617
And we&#39;ll talk about k-means, which is probably one

4
00:00:16,617 --> 00:00:20,457
of the most widely used machine learning techniques.

5
00:00:20,457 --> 00:00:25,178
So unsupervised means that the data has no ground truth labels

6
00:00:25,178 --> 00:00:26,368
to learn from.

7
00:00:26,368 --> 00:00:28,876
Now in the supervised setting that we were talking

8
00:00:28,876 --> 00:00:29,652
about before,

9
00:00:29,652 --> 00:00:32,710
each observation is labeled with some ground truth labels.

10
00:00:34,620 --> 00:00:38,090
But in the unsupervised setting, you just have some observations,

11
00:00:38,090 --> 00:00:39,540
and you need to do something with them.

12
00:00:42,120 --> 00:00:45,459
Now clustering is a key unsupervised problem.

13
00:00:45,459 --> 00:00:48,390
Now in clustering your goal is to group the observations into

14
00:00:48,390 --> 00:00:49,950
ones that are somehow similar.

15
00:00:51,860 --> 00:00:54,700
Because the problem is unsupervised,

16
00:00:55,720 --> 00:00:58,520
the unsupervised learning methods are much hard to

17
00:00:58,520 --> 00:01:00,620
evaluate because there&#39;s no ground truth.

18
00:01:01,930 --> 00:01:04,926
We&#39;re gonna focus on clustering on most of this lecture.

19
00:01:07,699 --> 00:01:11,290
So let&#39;s say that each dot here represents a person.

20
00:01:11,290 --> 00:01:15,422
Represented by two features, feature one and feature two.

21
00:01:15,422 --> 00:01:17,450
So you can see we have a bunch of groups of people.

22
00:01:17,450 --> 00:01:19,980
In fact, there are five groups of people, one, two,

23
00:01:19,980 --> 00:01:21,720
three, four, five.

24
00:01:21,720 --> 00:01:24,400
And we want the computer to automatically be able to figure

25
00:01:24,400 --> 00:01:26,170
out who is in what cluster.

26
00:01:28,750 --> 00:01:31,550
Now applications for clustering include

27
00:01:31,550 --> 00:01:34,315
automatically grouping documents or webpages into topics.

28
00:01:34,315 --> 00:01:38,444
For instance, trying to group news stories from today into

29
00:01:38,444 --> 00:01:40,186
different categories.

30
00:01:40,186 --> 00:01:44,532
A clustering large number of products for example let&#39;s say,

31
00:01:44,532 --> 00:01:47,812
you had a great paper last year where they showed

32
00:01:47,812 --> 00:01:49,702
a clustering of products.

33
00:01:49,702 --> 00:01:52,447
And then clustering customers into those with similar purchase

34
00:01:52,447 --> 00:01:52,965
behavior.

35
00:01:54,454 --> 00:01:55,007
Now again,

36
00:01:55,007 --> 00:01:57,661
it&#39;s tricky to a value with the quality of a clustering,

37
00:01:57,661 --> 00:01:59,330
because there was no ground truth.

38
00:02:00,720 --> 00:02:03,927
Now let me focus on one of these clustering algorithms so

39
00:02:03,927 --> 00:02:07,273
you can see how it works and really get some intuition, and

40
00:02:07,273 --> 00:02:08,989
that algorithm is k-means.

41
00:02:13,205 --> 00:02:15,097
Here are the steps for k-means, and

42
00:02:15,097 --> 00:02:18,220
I&#39;m gonna walk you through them with an example in a minute.

43
00:02:18,220 --> 00:02:23,460
But first, we take all of our points and we input

44
00:02:23,460 --> 00:02:29,748
into the computer the number of clusters we think it has.

45
00:02:29,748 --> 00:02:34,293
And then we randomly initialize where the centers of those

46
00:02:34,293 --> 00:02:35,506
clusters are.

47
00:02:35,506 --> 00:02:40,433
Then we assign all of the points to the closest cluster center.

48
00:02:40,433 --> 00:02:44,796
And then we change the cluster centers to be in the middle of

49
00:02:44,796 --> 00:02:48,814
its points, then we repeat this until convergence.

50
00:02:48,814 --> 00:02:51,476
So let me show you with an example.

51
00:02:51,476 --> 00:02:56,214
Here is our data, and the first thing we do is we input

52
00:02:56,214 --> 00:03:00,430
the number of clusters which we think is five.

53
00:03:01,480 --> 00:03:04,177
And then we randomly initialize the cluster centers.

54
00:03:04,177 --> 00:03:06,116
So here they are, the red dots and

55
00:03:06,116 --> 00:03:09,453
these cluster centers were definitely not chosen well.

56
00:03:09,453 --> 00:03:11,443
But that&#39;s okay, because they&#39;re gonna move.

57
00:03:14,339 --> 00:03:18,655
And then we assign all of the points to the closest

58
00:03:18,655 --> 00:03:20,242
cluster center.

59
00:03:20,242 --> 00:03:24,027
And this produces what&#39;s called the Voronoi diagram,

60
00:03:24,027 --> 00:03:28,135
where the space is divided up into different regions based on

61
00:03:28,135 --> 00:03:31,936
who is assigned to what cluster, what cluster center.

62
00:03:35,874 --> 00:03:39,122
And now we&#39;re gonna change the cluster centers to be in

63
00:03:39,122 --> 00:03:42,166
the middle of the points assigned to that cluster.

64
00:03:42,166 --> 00:03:45,450
So here you can see there are a whole lots of point over here

65
00:03:45,450 --> 00:03:47,447
assigned to that cluster center.

66
00:03:47,447 --> 00:03:50,282
And that&#39;s gonna make that cluster center move over in this

67
00:03:50,282 --> 00:03:50,911
direction.

68
00:03:53,913 --> 00:03:56,388
And then we repeat this untill convergence.

69
00:03:56,388 --> 00:03:58,936
So hopefully after a few alliterations of this,

70
00:03:58,936 --> 00:04:01,678
you&#39;ll geta lovely solution that looks like this.

71
00:04:01,678 --> 00:04:04,367
And now I should warn you that this doesn&#39;t always work.

72
00:04:04,367 --> 00:04:07,560
It depends on where you place those initial seeds.

73
00:04:07,560 --> 00:04:09,470
Sometimes people try several times with different

74
00:04:09,470 --> 00:04:12,120
initial seeds to see if they can get a better clustering.

75
00:04:12,120 --> 00:04:13,688
But that&#39;s the general idea, and

76
00:04:13,688 --> 00:04:16,345
this is what it&#39;s supposed to look like when it works out.

77
00:04:18,689 --> 00:04:20,470
Are you ready?

78
00:04:20,470 --> 00:04:21,150
Shall I let her rip?

79
00:04:22,420 --> 00:04:25,538
Here&#39;s our new data.

80
00:04:25,538 --> 00:04:28,831
Here&#39;s my cluster centers assigned randomly.

81
00:04:28,831 --> 00:04:33,257
So these guys on the left, who are they gonna get assigned to?

82
00:04:33,257 --> 00:04:36,639
Probably that guy, and so where is that center gonna go?

83
00:04:36,639 --> 00:04:38,330
That&#39;s probably gonna shift in that direction.

84
00:04:40,090 --> 00:04:42,885
So here&#39;s what happens after one iteration of k-means.

85
00:04:43,970 --> 00:04:48,644
Looks like that And you can see all the points

86
00:04:48,644 --> 00:04:51,029
assigned to each cluster are colored.

87
00:04:54,514 --> 00:04:56,003
Second iteration, and

88
00:04:56,003 --> 00:04:58,984
now I am just going to keep flying through here.

89
00:05:11,429 --> 00:05:14,145
So that&#39;s where the clusters ended when our time was up, and

90
00:05:14,145 --> 00:05:15,806
as you can see it did a beautiful job.

91
00:05:19,973 --> 00:05:23,109
So k-means, it&#39;s a popular clustering algorithm,

92
00:05:23,109 --> 00:05:26,048
it&#39;s actually very computationally efficient.

93
00:05:26,048 --> 00:05:28,169
It doesn&#39;t always work that well,

94
00:05:28,169 --> 00:05:30,643
sometimes it comes up with bad clusters,

95
00:05:30,643 --> 00:05:34,750
depending on where you put those initial cluster centers.

96
00:05:34,750 --> 00:05:37,620
And depending on actually how spherical the clusters on

97
00:05:37,620 --> 00:05:39,630
in the high dimensional space.

98
00:05:39,630 --> 00:05:41,800
And that my friends, is another story for another day.

99
00:05:41,800 --> 00:05:45,331
But another issue you should be aware of is that the user needs

100
00:05:45,331 --> 00:05:47,558
to determine the number of clusters.

101
00:05:47,558 --> 00:05:51,484
But there are actually ways to do this and in any case,

102
00:05:51,484 --> 00:05:53,762
this is a powerful technique.

103
00:05:53,762 --> 00:05:58,089
It&#39;s very popular, and it can be amazingly useful for

104
00:05:58,089 --> 00:05:59,979
clustering products.

105
00:05:59,979 --> 00:06:02,992
If you work in an online retailer distributer or is great

106
00:06:02,992 --> 00:06:06,325
for clustering patient, if you are looking at medical data or

107
00:06:06,325 --> 00:06:09,355
drugs or situations from the past or whatever you like.

