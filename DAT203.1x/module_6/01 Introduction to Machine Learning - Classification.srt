0
00:00:04,195 --> 00:00:05,055
I&#39;m gonna give you

1
00:00:05,055 --> 00:00:07,420
an introduction to machine learning.

2
00:00:07,420 --> 00:00:11,020
Machine learning is a super powerful set of techniques

3
00:00:11,020 --> 00:00:12,220
for prediction.

4
00:00:12,220 --> 00:00:14,840
Machine learning allows you to make predictions and

5
00:00:14,840 --> 00:00:17,600
detect patterns that otherwise would have gone unnoticed.

6
00:00:17,600 --> 00:00:19,510
Now, machine learning started as the subfield of

7
00:00:19,510 --> 00:00:21,990
artificial intelligence, and

8
00:00:21,990 --> 00:00:24,620
its goal is to allow computers to learn by example.

9
00:00:25,850 --> 00:00:28,670
So I&#39;m gonna just launch right into it and give you an overview

10
00:00:28,670 --> 00:00:31,700
of some of the most important machine learning problems.

11
00:00:31,700 --> 00:00:34,180
So in particular I&#39;ll talk about classification,

12
00:00:34,180 --> 00:00:37,390
where we predict the answers to yes or no questions.

13
00:00:37,390 --> 00:00:39,300
We are gonna talk about regression where

14
00:00:39,300 --> 00:00:41,380
we&#39;re predicting real values.

15
00:00:41,380 --> 00:00:44,100
Clustering where we find patterns of similar objects.

16
00:00:44,100 --> 00:00:45,257
And then we will talk about how

17
00:00:45,257 --> 00:00:46,792
to evaluate machine learning models.

18
00:00:50,043 --> 00:00:51,884
Let&#39;s start with classification.

19
00:00:51,884 --> 00:00:57,080
Now, machine learning, as a field, grew

20
00:00:57,080 --> 00:01:00,230
out of artificial intelligence within computer science.

21
00:01:00,230 --> 00:01:02,720
And the goal was to teach computers by example.

22
00:01:04,200 --> 00:01:06,500
And machine learning,

23
00:01:06,500 --> 00:01:12,960
if we wanna teach the computer to recognize images of chairs,

24
00:01:12,960 --> 00:01:15,850
then we give the computer a whole bunch of images and

25
00:01:15,850 --> 00:01:18,790
we tell it which ones are chairs and which ones are not.

26
00:01:18,790 --> 00:01:21,390
And, then it&#39;s suppose to learn to recognize chairs,

27
00:01:21,390 --> 00:01:23,990
even ones that it hasn&#39;t seen before.

28
00:01:23,990 --> 00:01:25,480
It&#39;s not like we tell the computer how to

29
00:01:25,480 --> 00:01:26,910
recognize a chair.

30
00:01:26,910 --> 00:01:30,230
We don&#39;t tell it, a chair has four legs and a back and flat

31
00:01:30,230 --> 00:01:33,300
surface to sit on and so on, we just give it a lot of examples.

32
00:01:34,300 --> 00:01:37,330
Now machine learning has close ties to statistics, and

33
00:01:37,330 --> 00:01:40,080
in fact, it&#39;s hard to say what&#39;s different about predictive

34
00:01:40,080 --> 00:01:41,320
statistics and machine learning,

35
00:01:41,320 --> 00:01:44,860
and these fields are very closely linked right now.

36
00:01:44,860 --> 00:01:50,550
So for classification, we have a training set of observations and

37
00:01:50,550 --> 00:01:53,170
these are labeled images and

38
00:01:53,170 --> 00:01:56,880
we have a test set that we use only for evaluation.

39
00:01:56,880 --> 00:02:02,250
So we use the training set to learn a model of what a chair

40
00:02:02,250 --> 00:02:06,120
is, and the test set are images that are not in the training

41
00:02:06,120 --> 00:02:09,570
set, and we want to be able to make predictions on those,

42
00:02:09,570 --> 00:02:12,650
as to whether or not each image is a chair.

43
00:02:13,770 --> 00:02:16,050
Now, it could be that some of the labels on the training set

44
00:02:16,050 --> 00:02:17,220
are noisy.

45
00:02:17,220 --> 00:02:17,982
See, that could happen,

46
00:02:17,982 --> 00:02:23,220
in fact one of these labels is actually noisy, right?

47
00:02:23,220 --> 00:02:24,590
This is labeled as not a chair.

48
00:02:25,880 --> 00:02:28,290
Now that&#39;s okay, because as long as there isn&#39;t too much noise in

49
00:02:28,290 --> 00:02:29,470
these labels,

50
00:02:29,470 --> 00:02:31,636
we still should be able to learn a model for a chair.

51
00:02:31,636 --> 00:02:36,173
It&#39;s just that the algorithm won&#39;t be able to classify things

52
00:02:36,173 --> 00:02:38,490
perfectly, and that happens.

53
00:02:39,660 --> 00:02:41,980
Some prediction problems are harder than others, but

54
00:02:41,980 --> 00:02:44,920
that&#39;s okay, we just do the best we can from the training data.

55
00:02:46,170 --> 00:02:48,130
And in terms of the size of the training data the more

56
00:02:48,130 --> 00:02:49,190
the merrier.

57
00:02:49,190 --> 00:02:51,640
We want as much data as we can to train these models.

58
00:02:52,850 --> 00:02:56,670
Now, each observation in the training set and

59
00:02:56,670 --> 00:02:59,860
in the test set is represented by a set of numbers which

60
00:02:59,860 --> 00:03:00,760
are called features.

61
00:03:00,760 --> 00:03:04,660
So how do we represent an image of a chair, or a flower or

62
00:03:04,660 --> 00:03:05,670
whatever in the training set?

63
00:03:06,710 --> 00:03:09,650
Now, I zoomed into a little piece of this image right here.

64
00:03:09,650 --> 00:03:13,085
And you can see the pixels on the image.

65
00:03:13,085 --> 00:03:17,880
So we can represent each pixel according to its red,

66
00:03:17,880 --> 00:03:21,320
green, blue values, rgb values kinda like that.

67
00:03:23,130 --> 00:03:26,720
And so we get three numbers representing each pixel

68
00:03:26,720 --> 00:03:27,230
in the image.

69
00:03:28,380 --> 00:03:31,270
So you can represent the whole image as a collection of these

70
00:03:31,270 --> 00:03:32,370
rgb values.

71
00:03:33,470 --> 00:03:36,440
So this image becomes this very large vector of numbers.

72
00:03:38,250 --> 00:03:40,810
In general, when we are doing machine learning

73
00:03:40,810 --> 00:03:43,290
we need to represent each observation in the training and

74
00:03:43,290 --> 00:03:46,450
the test sets as a vector of numbers.

75
00:03:46,450 --> 00:03:49,100
And the label is also represented by a number.

76
00:03:50,110 --> 00:03:53,980
So here the number is -1 because the image is not a chair, and

77
00:03:53,980 --> 00:03:56,004
the chairs will all get labeled +1.

78
00:03:56,004 --> 00:03:59,500
Here&#39;s another example.

79
00:03:59,500 --> 00:04:01,730
Now, this is a problem that comes from New York City&#39;s power

80
00:04:01,730 --> 00:04:04,660
company, where they wanted to predict which manholes were

81
00:04:04,660 --> 00:04:05,460
gonna have a fire.

82
00:04:06,680 --> 00:04:09,754
So we would represent each manhole as a vector, and

83
00:04:09,754 --> 00:04:12,850
here are the components in the vector.

84
00:04:12,850 --> 00:04:16,307
So the first component might be the number of manholes events

85
00:04:16,307 --> 00:04:19,628
last year like the number of smoking manholes and fires and

86
00:04:19,628 --> 00:04:20,730
things like that.

87
00:04:22,060 --> 00:04:24,860
And then the second component might be the number of serious

88
00:04:24,860 --> 00:04:29,270
events they had last year, the explosions, fires,

89
00:04:29,270 --> 00:04:31,380
serious fires.

90
00:04:31,380 --> 00:04:33,100
And then the third component might be the number of

91
00:04:33,100 --> 00:04:36,600
electrical cables in the manhole, and the next one might

92
00:04:36,600 --> 00:04:39,630
be the number of cables from before 1930 and so on.

93
00:04:42,070 --> 00:04:46,360
Now in general, the first step in machine learning is to figure

94
00:04:46,360 --> 00:04:49,290
out how to represent your data as a vector like this.

95
00:04:49,290 --> 00:04:52,190
And you can make the vector very large, you could include a lot

96
00:04:52,190 --> 00:04:54,614
of factors if you like, that&#39;s fine.

97
00:04:54,614 --> 00:04:57,300
Now computationally things are easier if you

98
00:04:57,300 --> 00:05:00,290
use fewer features, but then you risk leaving out information.

99
00:05:00,290 --> 00:05:03,340
So there is a trade off right there that you&#39;re gonna have to

100
00:05:03,340 --> 00:05:06,870
worry about, and we&#39;ll talk a little more about that later.

101
00:05:06,870 --> 00:05:09,860
But in any case, you can&#39;t do machine learning if you don&#39;t

102
00:05:09,860 --> 00:05:12,000
have your data represented this way.

103
00:05:12,000 --> 00:05:13,020
So this is the first step.

104
00:05:14,870 --> 00:05:19,780
Now, you&#39;d think that manholes with more cables and more recent

105
00:05:19,780 --> 00:05:23,010
serious events and so on, would be more prone to explosions and

106
00:05:23,010 --> 00:05:24,810
fires in the future.

107
00:05:24,810 --> 00:05:29,400
But what combination of these things would give you the best

108
00:05:29,400 --> 00:05:31,680
predictor, how do you combine them together?

109
00:05:31,680 --> 00:05:33,530
You could add them all up but

110
00:05:33,530 --> 00:05:35,660
that might not be the best thing.

111
00:05:35,660 --> 00:05:38,130
You could give them all weights and add them all up, but

112
00:05:38,130 --> 00:05:40,070
how do you know what the weights are?

113
00:05:40,070 --> 00:05:42,800
And that&#39;s what machine learning does for you.

114
00:05:42,800 --> 00:05:45,690
It tells you what combination to use to get the best predictor.

115
00:05:46,880 --> 00:05:49,200
Okay, so for the manhole problem we

116
00:05:49,200 --> 00:05:52,280
wanna use the data from the past to predict the future.

117
00:05:53,580 --> 00:05:58,008
So, for instance, the training data might be from 2014 and

118
00:05:58,008 --> 00:06:03,491
before, And then we would label

119
00:06:03,491 --> 00:06:08,647
the manhole with Label 1 if it had a fire in 2015.

120
00:06:11,609 --> 00:06:13,752
And then for the test set,

121
00:06:13,752 --> 00:06:18,723
the features would be created from 2015 and before, and

122
00:06:18,723 --> 00:06:22,734
then we would predict what happens in 2016.

123
00:06:22,734 --> 00:06:27,230
Okay, so just to be formal about it.

124
00:06:27,230 --> 00:06:31,576
So each observation is represented by

125
00:06:31,576 --> 00:06:36,473
a vector of features, they&#39;re called X.

126
00:06:36,473 --> 00:06:40,538
And those are also called predictors, covariates,

127
00:06:40,538 --> 00:06:45,000
explanatory variables, or independent variables.

128
00:06:45,000 --> 00:06:47,406
You can use whatever terminology you like,

129
00:06:47,406 --> 00:06:49,233
I generally call them features.

130
00:06:49,233 --> 00:06:52,780
And then also each observation gets a label which is called Y.

131
00:06:54,500 --> 00:06:58,891
Now even more formally, we&#39;re given a training set of

132
00:06:58,891 --> 00:07:03,000
feature label pairs Xi, Yi and there are n of them.

133
00:07:03,000 --> 00:07:07,092
And we wanna create a classification model f that can

134
00:07:07,092 --> 00:07:09,233
predict label y for a new x.

135
00:07:12,311 --> 00:07:15,367
So let&#39;s take a simple version of the manhole example where we

136
00:07:15,367 --> 00:07:17,410
have only two features.

137
00:07:17,410 --> 00:07:19,850
We have the year that the oldest cable was installed and

138
00:07:19,850 --> 00:07:21,100
the number of events last year.

139
00:07:22,220 --> 00:07:25,510
So each observation, which is each manhole,

140
00:07:25,510 --> 00:07:29,670
can be represented as a point on a two dimensional graph,

141
00:07:29,670 --> 00:07:31,370
which means I can plot the whole data set.

142
00:07:33,550 --> 00:07:38,085
So there it is, for each manhole I plot oldest cable

143
00:07:38,085 --> 00:07:43,147
against events last year and then if it did not have a fire I

144
00:07:43,147 --> 00:07:48,134
label it minus 1, if it did have a fire label at plus 1.

145
00:07:48,134 --> 00:07:51,920
And then,

146
00:07:51,920 --> 00:07:58,980
I wanna create a function that has as many of the positives

147
00:07:58,980 --> 00:08:01,970
on one side and as many of the negatives on the other.

148
00:08:03,730 --> 00:08:08,530
So I&#39;m gonna have that function be a function of the features,

149
00:08:08,530 --> 00:08:12,590
of the number of events last year in the oldest cable.

150
00:08:12,590 --> 00:08:15,920
Now, we would really like this decision boundary

151
00:08:15,920 --> 00:08:20,540
to divide the space into a region where the labels

152
00:08:20,540 --> 00:08:23,286
are generally positive and where they&#39;re generally negative.

153
00:08:23,286 --> 00:08:30,040
And we&#39;ll use this function to predict whether or not a new

154
00:08:30,040 --> 00:08:34,630
observation will have a positive label or have a negative label.

155
00:08:34,630 --> 00:08:39,910
So this function will have it&#39;s decision boundary at f(x)=0 and

156
00:08:39,909 --> 00:08:43,080
then f will be positive in the region that

157
00:08:43,080 --> 00:08:46,880
we will predict positive, and the region we predict negative.

158
00:08:48,530 --> 00:08:52,330
So then if we have the problem of detecting chairs,

159
00:08:52,330 --> 00:08:54,295
hopefully we would create our decision boundary so

160
00:08:54,295 --> 00:08:56,980
that all the chairs would be on one side and

161
00:08:56,980 --> 00:08:59,280
all the non chairs would be on the other side.

162
00:08:59,280 --> 00:09:01,050
So the machine learning algorithm is gonna create

163
00:09:01,050 --> 00:09:02,440
the function f.

164
00:09:02,440 --> 00:09:04,610
And then the predicted value y for

165
00:09:04,610 --> 00:09:07,790
a new x is gonna be the sign of that function.

166
00:09:07,790 --> 00:09:09,710
If the sign&#39;s positive, we&#39;ll predict y equals 1.

167
00:09:09,710 --> 00:09:14,620
If the sign is negative, we predict y equals minus 1.

168
00:09:14,620 --> 00:09:18,810
Now, the function itself, might be very complicated.

169
00:09:18,810 --> 00:09:21,130
But the way to use it is always very simple,

170
00:09:21,130 --> 00:09:25,460
it&#39;s always this, the predicted value is just the sign of it.

171
00:09:25,460 --> 00:09:30,050
Now, you can do a lot if you can answer yes or no questions.

172
00:09:30,050 --> 00:09:33,472
For instance, think about handwriting recognition, for

173
00:09:33,472 --> 00:09:34,773
each letter on a page,

174
00:09:34,773 --> 00:09:37,940
you&#39;re gonna evaluate if it&#39;s a letter a, yes or no.

175
00:09:37,940 --> 00:09:41,860
Your spam detector in your computer has machine learning in

176
00:09:41,860 --> 00:09:46,390
it, each email that comes in has to be evaluated as to whether or

177
00:09:46,390 --> 00:09:47,220
not it&#39;s spam.

178
00:09:48,702 --> 00:09:51,700
Credit defaults, whether or

179
00:09:51,700 --> 00:09:55,820
not you get a loan, depends on whether the bank predicts

180
00:09:55,820 --> 00:09:58,840
you&#39;re gonna default on that loan or not, yes or no?

181
00:10:00,220 --> 00:10:02,900
Now in my lab we do a lot of work on predicting medical

182
00:10:02,900 --> 00:10:05,410
outcomes like strokes and side effects and so on,

183
00:10:05,410 --> 00:10:08,410
and we want to know whether something will happen to

184
00:10:08,410 --> 00:10:13,260
a patient within a period of time, yes or no?

185
00:10:13,260 --> 00:10:15,553
And here are some of the common machine learning methods

186
00:10:15,553 --> 00:10:17,306
here and we&#39;re gonna go over a lot of them,

187
00:10:17,306 --> 00:10:19,880
in particular machine in the learning course.

188
00:10:19,880 --> 00:10:24,010
Now most likely, unless you&#39;re interested in developing your

189
00:10:24,010 --> 00:10:27,590
own algorithms, you will never need to program these yourself.

190
00:10:27,590 --> 00:10:29,670
They&#39;re already programmed in by someone else.

191
00:10:29,670 --> 00:10:32,060
If you are just gonna be a consumer of these,

192
00:10:32,060 --> 00:10:35,640
you can just use the code that&#39;s already written for them.

193
00:10:35,640 --> 00:10:36,960
All of these things are covered in

194
00:10:36,960 --> 00:10:39,030
the Intro to Machine Learning course.

195
00:10:39,030 --> 00:10:41,560
But in order to use it effectively, you have to know

196
00:10:41,560 --> 00:10:44,860
what they&#39;re doing, otherwise you could into issues.

197
00:10:44,860 --> 00:10:46,740
But if you can figure out how to use these,

198
00:10:46,740 --> 00:10:49,200
you have an extremely powerful tool on your hands.

