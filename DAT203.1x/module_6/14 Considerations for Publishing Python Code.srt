0
00:00:05,279 --> 00:00:09,208
Hi, in a previous demo, I showed you how to take

1
00:00:09,208 --> 00:00:12,648
an Azure Machine Learning experiment and

2
00:00:12,648 --> 00:00:15,700
publish it as a web service.

3
00:00:15,700 --> 00:00:17,000
And if you&#39;ve done the lab,

4
00:00:17,000 --> 00:00:19,560
you&#39;ve done the same thing yourself.

5
00:00:19,560 --> 00:00:22,530
But I want to alert you to a possible pitfall

6
00:00:22,530 --> 00:00:25,580
if you&#39;re using Python in line,

7
00:00:25,580 --> 00:00:30,530
in the workflow of your machine learning experiment.

8
00:00:30,530 --> 00:00:35,690
So I have here an experiment that&#39;s similar to the one

9
00:00:35,690 --> 00:00:39,770
we already worked with, except, all the prep, all the data prep,

10
00:00:39,770 --> 00:00:41,130
is being done in Python.

11
00:00:41,130 --> 00:00:46,030
I&#39;m not using any of the native Azure Machine Learning modules.

12
00:00:46,030 --> 00:00:54,787
So for example, here&#39;s the join using pandas.merge method.

13
00:00:54,787 --> 00:00:58,627
Here&#39;s various

14
00:00:58,627 --> 00:01:03,880
preps like dropping some columns we don&#39;t need,

15
00:01:03,880 --> 00:01:08,150
removing null values from the rows,

16
00:01:08,150 --> 00:01:12,270
computing the log of the price, removing duplicate rows, etc.

17
00:01:13,900 --> 00:01:19,310
And in this script, we&#39;re pruning outliers.

18
00:01:21,060 --> 00:01:23,313
And then pay a particular attention to this.

19
00:01:23,313 --> 00:01:29,362
So, using the scikit-learn preprocessing module,

20
00:01:29,362 --> 00:01:34,488
we apply a scale operator, a scale function,

21
00:01:34,488 --> 00:01:39,760
to the numeric columns of that dataset, okay?

22
00:01:41,110 --> 00:01:45,890
And the rest of this should be familiar by now in terms of

23
00:01:45,890 --> 00:01:50,210
defining and training a machine learning model.

24
00:01:50,210 --> 00:01:52,096
So let&#39;s go to the predictive experiment.

25
00:01:54,693 --> 00:01:57,965
And when I created that predictive experiment,

26
00:01:57,965 --> 00:02:00,970
it looks kinda normal, right?

27
00:02:00,970 --> 00:02:02,220
And in fact, when I ran and

28
00:02:02,220 --> 00:02:06,240
tested it, I can visualize the results.

29
00:02:07,590 --> 00:02:11,810
And I get numbers here that look somewhat like

30
00:02:12,930 --> 00:02:16,980
numbers should be for the price of automobiles.

31
00:02:16,980 --> 00:02:18,750
So so far, that all looks good.

32
00:02:21,090 --> 00:02:25,410
But let me go over here, and I&#39;m gonna select that web service.

33
00:02:27,080 --> 00:02:31,334
And you can see, I&#39;ve already loaded one row of data here for

34
00:02:31,334 --> 00:02:34,172
the purpose of this demonstration, and

35
00:02:34,172 --> 00:02:38,362
I&#39;ve defined the column and row where my result should go.

36
00:02:38,362 --> 00:02:45,378
So let me click on Predict, and I get a number back.

37
00:02:45,378 --> 00:02:49,701
That number looks suspiciously low, it&#39;s about 5,000.

38
00:02:49,701 --> 00:02:52,610
So what could have gone wrong there?

39
00:02:52,610 --> 00:02:55,000
Where did something go wrong there?

40
00:02:55,000 --> 00:02:58,490
So to get some insight into this, first off I have this

41
00:02:58,490 --> 00:03:01,983
experiment, which is doing exactly the same thing,

42
00:03:01,983 --> 00:03:05,939
the same data prep steps that I just showed you in Python using

43
00:03:05,939 --> 00:03:08,678
Azure Machine Learning native modules.

44
00:03:10,103 --> 00:03:12,212
So there&#39;s really nothing going on new there,

45
00:03:12,212 --> 00:03:14,700
it&#39;s just a different way to do exactly the same thing.

46
00:03:15,990 --> 00:03:18,460
And when I created the predictive experiment,

47
00:03:23,265 --> 00:03:28,196
Notice that the, oops, sorry, scroll down here.

48
00:03:28,196 --> 00:03:35,310
The normalize down here changed, so

49
00:03:35,310 --> 00:03:40,330
what I wound up with was a transform and an apply

50
00:03:40,330 --> 00:03:46,480
transform module, so there&#39;s no normalization module here.

51
00:03:46,480 --> 00:03:49,810
Now there&#39;s this normalization transform and apply transform.

52
00:03:49,810 --> 00:03:55,610
So the normalization transform has saved the key

53
00:03:55,610 --> 00:03:59,060
parameters or data that I need to apply that transform.

54
00:03:59,060 --> 00:04:02,760
Which, in the case of a z score normalization is just the mean

55
00:04:02,760 --> 00:04:04,230
and the standard deviation.

56
00:04:04,230 --> 00:04:06,320
So those are saved, and

57
00:04:06,320 --> 00:04:10,615
then used to apply the correct mathematical operation here.

58
00:04:10,615 --> 00:04:14,765
So I&#39;ve saved those values from my training dataset.

59
00:04:14,765 --> 00:04:16,045
I&#39;m just going to use them for

60
00:04:16,045 --> 00:04:20,335
however many rows of data I get from the web service.

61
00:04:23,205 --> 00:04:26,755
So how do we achieve that sort of behavior with Python?

62
00:04:27,780 --> 00:04:31,830
So this is an experiment that looks similar to the first one

63
00:04:31,830 --> 00:04:36,640
using Python, but out of here where I

64
00:04:36,640 --> 00:04:41,620
pruned my outliers, I have added a summarized data module,

65
00:04:41,620 --> 00:04:45,890
and let me just show you what that computed.

66
00:04:48,070 --> 00:04:54,230
So I have a mean value for every numeric feature here.

67
00:04:55,760 --> 00:05:00,744
And I have the sample standard deviation here, again,

68
00:05:00,744 --> 00:05:03,190
for every numeric column.

69
00:05:06,338 --> 00:05:08,520
And I can save those.

70
00:05:08,520 --> 00:05:09,990
So this is from my training.

71
00:05:09,990 --> 00:05:12,276
I saved that as a data set, and

72
00:05:12,276 --> 00:05:15,578
let me jump to the predictive experiment.

73
00:05:19,358 --> 00:05:23,040
So now here&#39;s that saved data.

74
00:05:23,040 --> 00:05:27,520
I called it Auto-stats-Python, and if we look at it again,

75
00:05:27,520 --> 00:05:31,200
you see we have the mean column and

76
00:05:31,200 --> 00:05:33,490
the sample standard deviation column.

77
00:05:34,510 --> 00:05:35,925
And just to simplify things,

78
00:05:35,925 --> 00:05:39,510
I&#39;ve changed the name of that sample standard deviation column

79
00:05:39,510 --> 00:05:43,640
to just STD, using an edit metadata module.

80
00:05:43,640 --> 00:05:47,710
But the Python code is now gonna be somewhat different, in fact,

81
00:05:47,710 --> 00:05:49,720
quite different in how it works.

82
00:05:49,720 --> 00:05:53,630
So I still have my list of numeric columns, but

83
00:05:53,630 --> 00:05:58,400
I have a second argument to my azureml_main,

84
00:05:58,400 --> 00:06:00,240
which I&#39;m calling stats.

85
00:06:00,240 --> 00:06:02,320
So those are those stats that I&#39;ve saved

86
00:06:02,320 --> 00:06:03,770
from the training data.

87
00:06:03,770 --> 00:06:07,140
So I&#39;m no longer going to attempt to compute those on

88
00:06:07,140 --> 00:06:11,040
the fly, and you don&#39;t wanna do that, because you&#39;re getting,

89
00:06:11,040 --> 00:06:14,330
who knows how many rows of who knows what data.

90
00:06:14,330 --> 00:06:19,893
You wanna apply the transform that you started with.

91
00:06:19,893 --> 00:06:26,393
And I&#39;m just going to, using the indexing operator here or

92
00:06:26,393 --> 00:06:30,169
method for that stats data frame.

93
00:06:30,169 --> 00:06:35,329
I&#39;m going to just take the features that are in that where

94
00:06:35,329 --> 00:06:40,704
the name isin, so I&#39;m using that isin method with that list

95
00:06:40,704 --> 00:06:45,880
of numeric columns for the mean and standard deviation.

96
00:06:45,880 --> 00:06:49,740
So, I get two Python lists here, and

97
00:06:49,740 --> 00:06:54,730
then the last bit is pretty simple, so I just use the zip.

98
00:06:54,730 --> 00:06:58,359
So, I zip the names in the numeric columns,

99
00:06:58,359 --> 00:07:03,590
I have the list of means, the list of standard deviations.

100
00:07:03,590 --> 00:07:09,841
So those become, Those just get expanded or

101
00:07:09,841 --> 00:07:15,228
iterated in the for loop, and so I&#39;m simply scaling each column

102
00:07:15,228 --> 00:07:20,240
by name that&#39;s a numeric column and returning the result.

103
00:07:22,380 --> 00:07:28,760
And if I go to the bottom here, let&#39;s just see that, that.

104
00:07:28,760 --> 00:07:33,795
Yeah, and again, I have numbers that look reasonable for

105
00:07:33,795 --> 00:07:36,793
the prices of those automobiles.

106
00:07:36,793 --> 00:07:39,221
And let me go back to the Excel.

107
00:07:39,221 --> 00:07:43,281
I&#39;m gonna find the other web service, so

108
00:07:43,281 --> 00:07:48,737
this is the web service for that model I just showed you.

109
00:07:48,737 --> 00:07:53,652
And I&#39;ve already got the, well here, I&#39;m gonna clear that and

110
00:07:53,652 --> 00:07:54,664
clear that.

111
00:07:54,664 --> 00:07:57,374
Remember, the number we originally had there was around

112
00:07:57,374 --> 00:08:00,250
5,000, which seems suspiciously low.

113
00:08:00,250 --> 00:08:04,999
And I&#39;m going to click Predict, And

114
00:08:04,999 --> 00:08:06,962
now I get a number about 7,500.

115
00:08:06,962 --> 00:08:08,935
Well, that&#39;s still a bit low, but

116
00:08:08,935 --> 00:08:10,765
it looks much more reasonable.

117
00:08:10,765 --> 00:08:12,948
And obviously it&#39;s significantly different.

118
00:08:12,948 --> 00:08:16,002
That changed by a factor of 50%.

119
00:08:16,002 --> 00:08:17,195
What&#39;s the lesson here?

120
00:08:17,195 --> 00:08:23,125
The lesson is, if you&#39;re using any Python code in-line that

121
00:08:23,125 --> 00:08:27,085
does operations like aggregations on multiple rows,

122
00:08:27,085 --> 00:08:29,450
really think about what you&#39;re doing.

123
00:08:29,450 --> 00:08:33,945
And think about how you can convert that operation

124
00:08:33,945 --> 00:08:38,654
to a transform using some saved, like in this case,

125
00:08:38,654 --> 00:08:43,363
it was a saved set of summary statistics that I used,

126
00:08:43,363 --> 00:08:47,021
and so that&#39;ll keep you out of trouble.

127
00:08:47,021 --> 00:08:49,379
In terms of getting odd results or

128
00:08:49,379 --> 00:08:52,928
sometimes maybe just experiments failing even.

