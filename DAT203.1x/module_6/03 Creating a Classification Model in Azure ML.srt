0
00:00:04,704 --> 00:00:08,520
Hi, so Cynthia spent some time explaining

1
00:00:08,520 --> 00:00:11,950
the theory of classification a bit.

2
00:00:11,950 --> 00:00:15,020
And also how you interpret the results of

3
00:00:15,020 --> 00:00:17,220
classification models.

4
00:00:17,220 --> 00:00:20,610
So let me show you an example in Azure Machine Learning.

5
00:00:22,260 --> 00:00:25,550
So we&#39;re starting with this adult census income

6
00:00:25,550 --> 00:00:27,720
data set which we&#39;ve all ready looked at a bit.

7
00:00:29,000 --> 00:00:31,050
And let me just refresh your memory there by,

8
00:00:31,050 --> 00:00:33,290
I&#39;ll just look at it real quick.

9
00:00:33,290 --> 00:00:38,040
So we have a number of features here, some string features like

10
00:00:38,040 --> 00:00:41,470
working class or education level.

11
00:00:41,470 --> 00:00:44,650
Some numeric features like how many years of education

12
00:00:44,650 --> 00:00:48,880
the person had, etc.

13
00:00:48,880 --> 00:00:54,280
And we go across here and we

14
00:00:54,280 --> 00:00:58,390
find the label column, so this is what we&#39;re trying classify.

15
00:00:58,390 --> 00:01:01,640
Either the person is lower income,

16
00:01:01,640 --> 00:01:03,920
which means they make less than or equal to 50,000.

17
00:01:03,920 --> 00:01:05,660
Or they&#39;re higher income,

18
00:01:05,660 --> 00:01:08,630
which means they make greater than 50,000.

19
00:01:08,630 --> 00:01:11,780
And just again, to refresh your memory.

20
00:01:11,780 --> 00:01:15,270
Remember, there are a few columns like this native country

21
00:01:15,270 --> 00:01:17,625
where there&#39;s a great number of categories.

22
00:01:17,625 --> 00:01:22,260
And almost nobody in most of the categories except one,

23
00:01:22,260 --> 00:01:24,360
United States.

24
00:01:24,360 --> 00:01:28,385
So there&#39;s 41 countries but 40 of those are almost useless, so

25
00:01:28,385 --> 00:01:30,231
we&#39;ll have to deal with that.

26
00:01:32,336 --> 00:01:36,192
And we&#39;ve already talked about prep of data sets quite a bit so

27
00:01:36,192 --> 00:01:38,650
I&#39;m not gonna spend much time on this.

28
00:01:38,650 --> 00:01:43,374
But the first bit we do is, with the column selector I remove

29
00:01:43,374 --> 00:01:48,101
a few of those columns that we&#39;ve determined previously,

30
00:01:48,101 --> 00:01:52,941
especially in the visualization, are in some way useless.

31
00:01:55,254 --> 00:01:59,847
We have normalized, in this case I used a min max normalization

32
00:01:59,847 --> 00:02:03,930
of all the numeric columns, so you can say numeric all.

33
00:02:03,930 --> 00:02:07,200
It&#39;s very important of course to normalize before you

34
00:02:07,200 --> 00:02:08,540
build a machine learning model.

35
00:02:09,550 --> 00:02:14,240
And then, one last bit of prep here for all the string

36
00:02:14,240 --> 00:02:18,300
features, I&#39;ve made them into categorical variables, okay?

37
00:02:19,830 --> 00:02:20,630
So let&#39;s talk about

38
00:02:20,630 --> 00:02:22,700
actually building the machine learning model.

39
00:02:22,700 --> 00:02:29,630
So the first thing we have to do is we need to split the rows.

40
00:02:29,630 --> 00:02:31,560
Because we need a training data set and

41
00:02:31,560 --> 00:02:34,000
we need an evaluation data set at least.

42
00:02:35,320 --> 00:02:38,030
We&#39;re just gonna keep it simple here and split to that.

43
00:02:38,030 --> 00:02:43,050
So, with the split data module that I&#39;ve brought in here

44
00:02:43,050 --> 00:02:44,850
I split the rows.

45
00:02:44,850 --> 00:02:48,329
I&#39;m gonna use 60%, 0.6 for training the model,

46
00:02:48,329 --> 00:02:51,450
40% for evaluating the model.

47
00:02:51,450 --> 00:02:53,580
I&#39;ve given it a random seed and

48
00:02:53,580 --> 00:02:56,520
I&#39;m not gonna deal with any special stratification here.

49
00:02:57,730 --> 00:03:02,580
And I&#39;m gonna use simple linear two-class logistic regression

50
00:03:02,580 --> 00:03:04,349
as my classifier here.

51
00:03:05,410 --> 00:03:07,370
And there&#39;s a number of parameters I need to

52
00:03:07,370 --> 00:03:09,030
think about.

53
00:03:09,030 --> 00:03:10,610
And just for a first guess here, so

54
00:03:10,610 --> 00:03:12,860
I&#39;m gonna go with the single parameter case,

55
00:03:12,860 --> 00:03:16,140
I&#39;m not gonna try different combinations just yet.

56
00:03:17,410 --> 00:03:22,530
And I&#39;m just gonna go with the default optimization tolerance,

57
00:03:22,530 --> 00:03:27,170
just tells the algorithm when to stop at a certain accuracy.

58
00:03:27,170 --> 00:03:29,970
The L1 and L2 regularization weights,

59
00:03:29,970 --> 00:03:35,770
we&#39;re not gonna get into exactly how those work too much.

60
00:03:35,770 --> 00:03:40,350
But I&#39;m giving them fairly small values which means I&#39;m letting

61
00:03:40,350 --> 00:03:44,770
the data rule more than say the algorithm.

62
00:03:44,770 --> 00:03:47,130
And some others, this memory size,

63
00:03:47,130 --> 00:03:48,530
I just go with the default.

64
00:03:48,530 --> 00:03:51,870
And I have to give it a random seed, so

65
00:03:51,870 --> 00:03:53,840
I just made up some number.

66
00:03:53,840 --> 00:03:57,650
So now, the train model module takes that definition.

67
00:03:57,650 --> 00:04:01,460
So here, I have defined my two-class regression.

68
00:04:01,460 --> 00:04:03,690
I&#39;ve split out my training data and

69
00:04:03,690 --> 00:04:06,060
I need to train a model based on it.

70
00:04:06,060 --> 00:04:08,580
And the only thing I have to do to train it

71
00:04:08,580 --> 00:04:11,000
is just say what&#39;s that label?

72
00:04:11,000 --> 00:04:13,010
So by column name,

73
00:04:13,010 --> 00:04:15,710
it&#39;s just that label column we all ready talked about, income.

74
00:04:17,260 --> 00:04:20,810
Easy enough, now I need to score the model.

75
00:04:20,810 --> 00:04:24,170
So I&#39;ve got my 40% of the data that I split,

76
00:04:24,170 --> 00:04:26,890
that I said I was gonna use for evaluation.

77
00:04:26,890 --> 00:04:30,120
I have what&#39;ll be my trained model coming in here.

78
00:04:31,370 --> 00:04:34,330
And the only thing I need to say is, I&#39;ll just check that I&#39;m

79
00:04:34,330 --> 00:04:39,050
gonna just append the scored label column to everything else.

80
00:04:39,050 --> 00:04:42,620
So I keep all my other columns, and then I&#39;m gonna evaluate.

81
00:04:42,620 --> 00:04:45,852
And evaluation has no parameters so that&#39;s easy enough.

82
00:04:45,852 --> 00:04:49,174
So let me save that, and run it and

83
00:04:49,174 --> 00:04:52,394
we&#39;ll look at the evaluation.

84
00:04:55,729 --> 00:04:59,242
All right, my model is run, so first off,

85
00:04:59,242 --> 00:05:04,670
let&#39;s look at the output of this scored model module.

86
00:05:04,670 --> 00:05:09,420
Just look what very qualitatively what&#39;s happened.

87
00:05:09,420 --> 00:05:11,826
So remember, we had income was our label.

88
00:05:11,826 --> 00:05:15,138
And now it&#39;s added a couple of columns here, that module,

89
00:05:15,138 --> 00:05:18,160
scored labels and scored probability.

90
00:05:18,160 --> 00:05:19,650
So in scored labels, so

91
00:05:19,650 --> 00:05:22,910
those are what the machine learning algorithm

92
00:05:22,910 --> 00:05:28,020
has assigned as the categories of the two possible categories.

93
00:05:28,020 --> 00:05:31,960
Income is the no incorrect label, and remember, this is on

94
00:05:31,960 --> 00:05:35,390
the 40% of the data we didn&#39;t use to train the model.

95
00:05:35,390 --> 00:05:38,790
So this is kind of a blind test if you will.

96
00:05:38,790 --> 00:05:43,863
So you see the first row got it right, second row incorrect,

97
00:05:43,863 --> 00:05:49,640
third row incorrect and then a bunch of correct one&#39;s here.

98
00:05:49,640 --> 00:05:54,010
And then another incorrect, incorrect, correct, etc.

99
00:05:54,010 --> 00:05:56,940
So you get the idea that the machine learning algorithm got

100
00:05:56,940 --> 00:05:59,660
some things right, some things wrong.

101
00:05:59,660 --> 00:06:02,260
So we can look at a little more quantitative

102
00:06:02,260 --> 00:06:03,620
way of evaluating that.

103
00:06:03,620 --> 00:06:07,785
If I evisualize the evaluate model module

104
00:06:10,249 --> 00:06:14,686
All right, the first thing we have in that visualization is

105
00:06:14,686 --> 00:06:19,870
this ROC curve, radio operating characteristic curve.

106
00:06:19,870 --> 00:06:25,630
And recall that a perfect model, this curve would go straight

107
00:06:25,630 --> 00:06:29,960
up and straight across and we&#39;d have an area of 1.0 under it.

108
00:06:29,960 --> 00:06:33,790
A model that&#39;s completely useless it was just random

109
00:06:33,790 --> 00:06:38,060
guessing would more or less follow this gray curvy line.

110
00:06:38,060 --> 00:06:42,255
Basically, if you random guess, and you have two classes and

111
00:06:42,255 --> 00:06:45,084
they&#39;re more or less equal numbers you got a 50-50

112
00:06:45,084 --> 00:06:47,980
chance of guessing correctly, or not.

113
00:06:47,980 --> 00:06:49,540
Our curve is somewhere in the middle.

114
00:06:49,540 --> 00:06:53,310
You can see it&#39;s closer to the perfect than the random guess,

115
00:06:53,310 --> 00:06:54,510
so that&#39;s encouraging.

116
00:06:56,330 --> 00:06:57,900
So let&#39;s look down here.

117
00:06:57,900 --> 00:07:00,470
So now we have over on the left

118
00:07:00,470 --> 00:07:02,290
here this is our confusion matrix.

119
00:07:03,580 --> 00:07:08,528
And so true positives, we have almost

120
00:07:08,528 --> 00:07:13,380
1,600, so those are cases where

121
00:07:14,730 --> 00:07:20,490
we said the person&#39;s income was upper income, you see?

122
00:07:22,120 --> 00:07:23,807
And it indeed was.

123
00:07:23,807 --> 00:07:27,382
And true negatives, we have about 9,000 of those,

124
00:07:27,382 --> 00:07:30,808
where we said the person&#39;s true income was less than or

125
00:07:30,808 --> 00:07:32,385
equal to 50,000.

126
00:07:32,385 --> 00:07:36,910
And it was, but then we have some errors.

127
00:07:36,910 --> 00:07:38,390
So false negatives,

128
00:07:38,390 --> 00:07:42,660
we said the person&#39;s income was less than or equal to 50,000.

129
00:07:42,660 --> 00:07:45,455
And it was actually greater than 50,000, so

130
00:07:45,455 --> 00:07:48,168
we have 1,600 errors there.

131
00:07:48,168 --> 00:07:51,630
The false positive, so that&#39;s a case where

132
00:07:51,630 --> 00:07:55,530
the algorithm said the person&#39;s income was greater than 50,000.

133
00:07:55,530 --> 00:07:57,010
But in fact, it was less than or

134
00:07:57,010 --> 00:08:00,100
equal to 50,000, so we&#39;ve got about 700 of those.

135
00:08:00,100 --> 00:08:06,050
So you can see there&#39;s quite a few more true positives,

136
00:08:06,050 --> 00:08:10,100
true negatives than the false negatives and

137
00:08:10,100 --> 00:08:12,210
false positives so that&#39;s encouraging.

138
00:08:13,930 --> 00:08:16,710
You can quantify that a little more with some of these

139
00:08:16,710 --> 00:08:18,110
statistics you see over here.

140
00:08:18,110 --> 00:08:22,553
Accuracy for example was 82%, so that means that,

141
00:08:22,553 --> 00:08:26,030
that&#39;s the total true positives plus true

142
00:08:26,030 --> 00:08:29,522
negatives divided by the sum of all these.

143
00:08:29,522 --> 00:08:33,226
So the sum of true positive, false negative, false positive,

144
00:08:33,226 --> 00:08:35,021
true negative, so all cases.

145
00:08:35,020 --> 00:08:38,875
So 82% of the time we got it right.

146
00:08:38,875 --> 00:08:42,352
You can see precision is pretty good at about 0.7.

147
00:08:42,352 --> 00:08:44,210
Our recall is a little low.

148
00:08:44,210 --> 00:08:46,780
And the reason for that is,

149
00:08:46,780 --> 00:08:52,031
you can see that we have a fair number of false negatives

150
00:08:52,031 --> 00:08:57,310
compared to true negatives and that impacted that.

151
00:08:57,310 --> 00:09:01,120
And here, quantitatively, remember this Roc curve, or

152
00:09:01,120 --> 00:09:05,360
the AUC, the area under the curve, ideally it would be 1.0.

153
00:09:05,360 --> 00:09:10,022
It was about .8, well almost .88, so

154
00:09:10,022 --> 00:09:15,852
overall pretty good statistics there especially for

155
00:09:15,852 --> 00:09:19,250
a first guess at trying this.

156
00:09:19,250 --> 00:09:23,692
And so hopefully now you see that it’s very easy for

157
00:09:23,692 --> 00:09:26,700
stuff to build that kind of machine

158
00:09:26,700 --> 00:09:30,330
learning model given the right data preparation in Azure ML.

159
00:09:30,330 --> 00:09:33,650
It&#39;s also very easy to run it, evaluate it.

160
00:09:33,650 --> 00:09:35,510
And if we wanted to go further with this and

161
00:09:35,510 --> 00:09:38,940
try some other improvements, we could quickly do those as well.

162
00:09:38,940 --> 00:09:42,460
So, gives you a really nice environment to

163
00:09:42,460 --> 00:09:44,070
test out new new ideas.

164
00:09:44,070 --> 00:09:46,708
And figure out if you&#39;re making progress or

165
00:09:46,708 --> 00:09:49,282
not as you build machine learning models.

