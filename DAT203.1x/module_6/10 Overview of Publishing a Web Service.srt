0
00:00:05,252 --> 00:00:10,688
Hi, now that we&#39;ve looked at several major areas of machine

1
00:00:10,688 --> 00:00:16,882
learning, which is clustering, classification and regression.

2
00:00:16,882 --> 00:00:20,842
I&#39;d like to show you how to publish a model using the tools

3
00:00:20,842 --> 00:00:22,830
in Azure Machine Learning.

4
00:00:22,830 --> 00:00:25,950
I really think this is one of the nicest features of

5
00:00:25,950 --> 00:00:27,250
Azure Machine Learning.

6
00:00:27,250 --> 00:00:30,050
This ability to take a model you&#39;ve built and

7
00:00:30,050 --> 00:00:35,050
tested, and almost instantly publish it out to,

8
00:00:35,050 --> 00:00:38,400
conceivably a huge number of your colleagues.

9
00:00:38,400 --> 00:00:43,140
In this sequence, I&#39;m just gonna go over the background of

10
00:00:44,170 --> 00:00:47,570
web services and how they work with Azure Machine Learning.

11
00:00:47,570 --> 00:00:52,180
And then the next two sequences will show demos of how to

12
00:00:52,180 --> 00:00:56,200
actually build the web service, and how to use it from Excel.

13
00:00:57,840 --> 00:01:01,590
So on my screen here, I have the outline,

14
00:01:01,590 --> 00:01:04,940
so I&#39;m gonna give an overview as I said, of web services.

15
00:01:04,940 --> 00:01:07,490
I&#39;m gonna talk a little bit about some things you need to

16
00:01:07,490 --> 00:01:10,480
know to successfully publish models

17
00:01:10,480 --> 00:01:13,370
as a web service with Azure Machine Learning.

18
00:01:13,370 --> 00:01:17,480
And then I&#39;m gonna give a few tips on using the web services

19
00:01:17,480 --> 00:01:18,960
from Excel.

20
00:01:18,960 --> 00:01:21,010
So first off, why are we doing this?

21
00:01:21,010 --> 00:01:24,756
Why am I so excited about this?

22
00:01:24,756 --> 00:01:26,303
Well, for one thing,

23
00:01:26,303 --> 00:01:30,460
just the ability to publish a machine learning solution very

24
00:01:30,460 --> 00:01:33,485
quickly enable the users to get it, use it.

25
00:01:33,485 --> 00:01:36,962
And actually take some action that hopefully is valuable to

26
00:01:36,962 --> 00:01:38,790
the organization.

27
00:01:38,790 --> 00:01:44,320
Without conceivably weeks or even months of working with IT

28
00:01:44,320 --> 00:01:48,410
people to deploy some new system or platform.

29
00:01:48,410 --> 00:01:49,800
I find that very exciting.

30
00:01:51,590 --> 00:01:56,854
You can publish numerical or graphical results.

31
00:01:56,854 --> 00:01:59,685
It&#39;s a little more involved to do the graphical results, but

32
00:01:59,685 --> 00:02:00,943
it&#39;s possible to do that.

33
00:02:00,943 --> 00:02:04,498
So you have a fairly rich set of results you

34
00:02:04,498 --> 00:02:06,840
can publish to your users.

35
00:02:07,970 --> 00:02:10,454
And because it&#39;s web services,

36
00:02:10,454 --> 00:02:15,157
you can integrate with desktop tools like Microsoft Power BI,

37
00:02:15,157 --> 00:02:18,540
or as I&#39;m gonna demonstrate later, Excel.

38
00:02:18,540 --> 00:02:22,962
And there&#39;s two choices, you can do interactive, which is,

39
00:02:22,962 --> 00:02:27,060
you can imagine some analyst working in Excel or Power BI.

40
00:02:27,060 --> 00:02:31,068
And they just have a few lines of data, and they wanna see

41
00:02:31,068 --> 00:02:34,739
the prediction from the machine learning model for

42
00:02:34,739 --> 00:02:36,460
that few lines of data.

43
00:02:37,530 --> 00:02:40,120
Or if you have a great amount of data,

44
00:02:40,120 --> 00:02:44,170
a new batch basically you can submit it as a batch and

45
00:02:44,170 --> 00:02:46,710
let Azure Machine Learning process it as a batch.

46
00:02:48,190 --> 00:02:50,270
So let&#39;s talk about what is a web service,

47
00:02:50,270 --> 00:02:53,510
in case you&#39;re not familiar with that terminology.

48
00:02:53,510 --> 00:02:55,870
So I have over here on the right

49
00:02:55,870 --> 00:03:00,660
the Azure Machine Learning Service and, just conceptually,

50
00:03:00,660 --> 00:03:03,005
we&#39;ve wrapped that in a secure endpoint.

51
00:03:04,380 --> 00:03:07,370
And we have a network, presumably the Internet.

52
00:03:08,610 --> 00:03:11,140
And we have our end user applications.

53
00:03:11,140 --> 00:03:15,516
This is how our analyst is going to interact with our machine

54
00:03:15,516 --> 00:03:19,818
learning model over in Azure Machine Learning Services.

55
00:03:19,818 --> 00:03:23,134
So the analyst has some data, maybe a couple of columns and

56
00:03:23,134 --> 00:03:23,954
a few values.

57
00:03:23,954 --> 00:03:27,728
And they launch a request, which goes over the network through

58
00:03:27,728 --> 00:03:31,330
the secure gateway and into Azure Machine Learning.

59
00:03:31,330 --> 00:03:34,780
Azure machine learning will process that and

60
00:03:34,780 --> 00:03:39,020
provide them with a score, basically score the model

61
00:03:39,020 --> 00:03:41,520
that you&#39;ve provided given those inputs.

62
00:03:41,520 --> 00:03:42,560
So, that&#39;s all there is to it,

63
00:03:42,560 --> 00:03:45,220
that&#39;s conceptually what&#39;s going on here.

64
00:03:47,420 --> 00:03:49,880
A little more specifics on the data flow.

65
00:03:51,890 --> 00:03:54,830
So, how does our data flow within

66
00:03:54,830 --> 00:03:56,250
Azure Machine Learning look?

67
00:03:56,250 --> 00:03:58,120
So we start with published inputs, so

68
00:03:58,120 --> 00:04:00,340
that&#39;s what I showed on that other diagram.

69
00:04:00,340 --> 00:04:02,600
The input our analyst give us,

70
00:04:02,600 --> 00:04:05,180
we do whatever transformations we needed to do.

71
00:04:05,180 --> 00:04:07,860
Recall we&#39;ve spent a lot of time in this course on

72
00:04:07,860 --> 00:04:09,322
transformations of data.

73
00:04:09,322 --> 00:04:16,010
Then we put that now transformed and

74
00:04:16,010 --> 00:04:21,970
ready data into our trained model to get a score.

75
00:04:23,010 --> 00:04:26,760
We do any output transformation, that could be unit conversions,

76
00:04:26,760 --> 00:04:29,530
pruning columns, various things we want do.

77
00:04:29,530 --> 00:04:33,620
So it looks the way our end user would like it to look.

78
00:04:33,620 --> 00:04:35,458
And then we publish it out to our end user.

79
00:04:35,458 --> 00:04:42,138
So that&#39;s a very simple workflow we&#39;re gonna use here.

80
00:04:42,138 --> 00:04:47,202
And in terms of actually doing this, it&#39;s very simple because

81
00:04:47,202 --> 00:04:52,100
it&#39;s highly automated and it&#39;s gonna create an endpoint.

82
00:04:52,100 --> 00:04:57,690
It&#39;s gonna have a unique URL, it&#39;s gonna have an API key, and

83
00:04:57,690 --> 00:05:02,415
you need to limit access to who has that API key.

84
00:05:02,415 --> 00:05:04,288
Cuz anyone with that URL and

85
00:05:04,288 --> 00:05:08,611
API key can use that machine learning model you&#39;ve deployed.

86
00:05:08,611 --> 00:05:11,781
And there&#39;s a couple ways you can get to it, like I said,

87
00:05:11,781 --> 00:05:12,850
you can use Excel.

88
00:05:12,850 --> 00:05:16,208
But there&#39;s also, just be aware if you&#39;re more of a developer or

89
00:05:16,208 --> 00:05:18,407
if you&#39;re working with some developers,

90
00:05:18,407 --> 00:05:19,704
there&#39;s a restful API.

91
00:05:19,704 --> 00:05:21,787
And there&#39;s actually,

92
00:05:21,787 --> 00:05:26,250
the interface actually auto generates code for C#,

93
00:05:26,250 --> 00:05:29,732
Python and R which I think is really nice.

94
00:05:29,732 --> 00:05:34,530
And the whole thing is gonna work over a secure https

95
00:05:34,530 --> 00:05:35,874
connection.

96
00:05:35,874 --> 00:05:38,032
So there&#39;s a few tips I&#39;d like to give you.

97
00:05:38,032 --> 00:05:40,982
Because as you prepare an experiment to publish,

98
00:05:40,982 --> 00:05:42,950
especially if you use Custom R and

99
00:05:42,950 --> 00:05:46,690
Python code there&#39;s a few tricks you might wanna keep in mind.

100
00:05:47,810 --> 00:05:52,650
So remember the data has to flow straight through, you could be

101
00:05:52,650 --> 00:05:55,960
getting just a single row at a time or a few rows at a time.

102
00:05:55,960 --> 00:05:58,690
So anything that&#39;s an aggregation,

103
00:05:58,690 --> 00:06:01,420
or anything operational like that,

104
00:06:01,420 --> 00:06:04,410
that requires inherently multiple rows.

105
00:06:04,410 --> 00:06:08,027
You need to remove that from your workflow,

106
00:06:08,027 --> 00:06:10,482
or figure out a way around it.

107
00:06:10,482 --> 00:06:13,968
A lot of the modules, and you&#39;ll see that in the demo, a lot of

108
00:06:13,968 --> 00:06:17,474
the Azure Machine Modules will actually create a transform.

109
00:06:17,474 --> 00:06:23,118
That apply what they&#39;ve learned

110
00:06:23,118 --> 00:06:29,180
from the training data to each row.

111
00:06:29,180 --> 00:06:30,690
And just to reiterate,

112
00:06:30,690 --> 00:06:36,450
anything that requires multiple data value, multiple rows and

113
00:06:36,450 --> 00:06:40,970
doesn&#39;t create a transform needs to be removed.

114
00:06:40,970 --> 00:06:45,040
And particularly I find R in Python code is where you need to

115
00:06:45,040 --> 00:06:46,570
think about that.

116
00:06:46,570 --> 00:06:48,950
The Azure Machine Learning modules are pretty good of

117
00:06:48,950 --> 00:06:49,890
taking care of that.

118
00:06:49,890 --> 00:06:53,290
But your own R on Python code you may need to

119
00:06:53,290 --> 00:06:56,570
give it a look over, make sure you don&#39;t have any places where

120
00:06:56,570 --> 00:06:59,280
you&#39;re doing multi row operations.

121
00:06:59,280 --> 00:07:02,070
And at the end of the experiment flow,

122
00:07:02,070 --> 00:07:05,640
I always like to add a select columns module.

123
00:07:05,640 --> 00:07:10,910
And the reason for that is that, this way I can control which

124
00:07:10,910 --> 00:07:14,040
columns are returned back to my end user.

125
00:07:14,040 --> 00:07:17,130
I&#39;m not returning all the columns they just gave

126
00:07:18,500 --> 00:07:23,130
as the data input, back to them, I&#39;m just returning the columns

127
00:07:23,130 --> 00:07:24,500
that they might be interested in.

128
00:07:24,500 --> 00:07:28,920
Such as, particularly the label, maybe the probabilities, and

129
00:07:28,920 --> 00:07:30,620
maybe a few other things.

130
00:07:30,620 --> 00:07:32,820
But really limit what they get back, so

131
00:07:32,820 --> 00:07:36,140
that it isn&#39;t cluttering up their work space.

132
00:07:36,140 --> 00:07:40,870
So another cool feature of Azure Machine Learning,

133
00:07:40,870 --> 00:07:44,070
once you publish a model through web service you can actually

134
00:07:44,070 --> 00:07:45,650
retrain it on the fly.

135
00:07:47,090 --> 00:07:50,275
So why would you wanna do that, what&#39;s the advantage of this,

136
00:07:50,275 --> 00:07:51,488
and why is it a big deal?

137
00:07:51,488 --> 00:07:55,993
Well it&#39;s often the case once you&#39;ve run a model for a while,

138
00:07:55,993 --> 00:07:58,171
maybe you&#39;ve got more data.

139
00:07:58,171 --> 00:08:03,163
A bigger, better data set that has more cases that you can take

140
00:08:03,163 --> 00:08:08,350
care of some of the issues that your original training data set.

141
00:08:09,670 --> 00:08:10,944
Maybe from a proof of concept or

142
00:08:10,944 --> 00:08:13,208
something like that didn&#39;t take care of it.

143
00:08:13,208 --> 00:08:15,890
The other thing is, maybe you&#39;ve continue to work on your model,

144
00:08:15,890 --> 00:08:19,570
and you realize there&#39;s just a better way to do it.

145
00:08:19,570 --> 00:08:22,450
Maybe better cleaning the data, maybe a better choice of machine

146
00:08:22,450 --> 00:08:23,880
learning models, something like that.

147
00:08:25,570 --> 00:08:28,090
You can just do an update, like I said.

148
00:08:28,090 --> 00:08:31,790
You run the training experiment,

149
00:08:31,790 --> 00:08:33,700
you update the scoring experiment.

150
00:08:33,700 --> 00:08:36,910
I&#39;ll show you an example of this when we get to the demo, what I

151
00:08:36,909 --> 00:08:39,040
mean by a training experiment and scoring experiment.

152
00:08:40,220 --> 00:08:43,980
As long as you don&#39;t change the schema, it&#39;s going to update.

153
00:08:43,980 --> 00:08:47,960
And your users will, without really noticing,

154
00:08:47,960 --> 00:08:53,990
will just get a better more accurate model, conceivably.

155
00:08:53,990 --> 00:08:56,740
And they won&#39;t know, their endpoints won&#39;t change,

156
00:08:56,740 --> 00:08:58,680
the key won&#39;t change, anything like that.

157
00:09:00,030 --> 00:09:02,180
So let me just say a few words about using

158
00:09:02,180 --> 00:09:05,850
Azure Machine Learning Web Services in Excel.

159
00:09:09,380 --> 00:09:13,743
So you can use a downloadable Excel file.

160
00:09:13,743 --> 00:09:19,984
Or what I recommend is use the Azure Machine Learning plug-in.

161
00:09:19,984 --> 00:09:22,710
And there&#39;s a couple advantage to this.

162
00:09:22,710 --> 00:09:25,360
I find, in practice, it&#39;s more reliable.

163
00:09:25,360 --> 00:09:31,408
And also, once you set up the plug-in, it encrypts your key.

164
00:09:31,408 --> 00:09:33,317
And for example,

165
00:09:33,317 --> 00:09:38,536
you could set this up in Excel Online within say,

166
00:09:38,536 --> 00:09:43,129
OneDrive, and then it becomes sharable.

167
00:09:43,129 --> 00:09:47,061
Regardless of how you do it, once you have the plug-ins setup

168
00:09:47,061 --> 00:09:50,548
with the key and the end point and those are encrypted.

169
00:09:50,548 --> 00:09:55,079
You can copy that Excel file to as many of your colleagues as

170
00:09:55,079 --> 00:09:59,240
need to access your Machine Learning Service through

171
00:09:59,240 --> 00:10:00,550
Azure ML.

172
00:10:00,550 --> 00:10:05,240
So it&#39;s all very nice stuff that cuts out a lot of overhead that

173
00:10:06,900 --> 00:10:08,380
data scientists have struggled with,

174
00:10:08,380 --> 00:10:10,470
frankly we&#39;ve struggled with this for a long time.

175
00:10:10,470 --> 00:10:13,695
And the way this has been streamlined into these

176
00:10:13,695 --> 00:10:17,565
Azure Machine Learning Web Services is just really great

177
00:10:17,565 --> 00:10:18,785
and really easy.

