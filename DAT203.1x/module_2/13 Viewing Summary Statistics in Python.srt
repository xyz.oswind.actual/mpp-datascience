0
00:00:00,000 --> 00:00:03,210
so Cynthia has been discussing summary

1
00:00:03,210 --> 00:00:05,310
statistics and how we compute summary

2
00:00:05,310 --> 00:00:09,150
statistics and how we interpret summary

3
00:00:09,150 --> 00:00:11,010
statistics and in this demo I&#39;m going to

4
00:00:11,010 --> 00:00:13,769
show you some actual code where we&#39;re

5
00:00:13,769 --> 00:00:15,809
computing summary statistics and

6
00:00:15,809 --> 00:00:17,070
specifically we&#39;re going to use some

7
00:00:17,070 --> 00:00:20,160
Python to compute some summary

8
00:00:20,160 --> 00:00:22,740
statistics and we&#39;ll talk a little bit

9
00:00:22,740 --> 00:00:25,699
about what they mean

10
00:00:25,710 --> 00:00:29,250
so my screen here i have my notebook

11
00:00:29,250 --> 00:00:31,199
that i started in Azure machine learning

12
00:00:31,199 --> 00:00:33,900
and this first

13
00:00:33,900 --> 00:00:37,620
cell is the auto-generated code from

14
00:00:37,620 --> 00:00:39,780
Azure machine learning when I started

15
00:00:39,780 --> 00:00:42,510
the notebook from this automobile price

16
00:00:42,510 --> 00:00:45,300
data raw dot CSV data set

17
00:00:45,300 --> 00:00:48,060
ok so let me just load that&#39;ll get us a

18
00:00:48,060 --> 00:00:50,300
data frame

19
00:00:50,300 --> 00:00:53,540
there we go and the auto-generated code

20
00:00:53,540 --> 00:00:55,850
would just give you the whole frame it

21
00:00:55,850 --> 00:00:57,350
would just have the word frame here

22
00:00:57,350 --> 00:00:59,809
which is the the name of our pandas

23
00:00:59,809 --> 00:01:02,989
dataframe but i&#39;ve added this dot head

24
00:01:02,989 --> 00:01:06,170
so we just see the first five rows

25
00:01:06,170 --> 00:01:09,229
there we go and I&#39;m not going to discuss

26
00:01:09,229 --> 00:01:13,729
all these wrote columns but surprises to

27
00:01:13,729 --> 00:01:17,960
say there&#39;s about 200 automobiles in

28
00:01:17,960 --> 00:01:20,420
this sample and what we&#39;re going to look

29
00:01:20,420 --> 00:01:23,330
at only is the horsepower of each car

30
00:01:23,330 --> 00:01:26,780
the park city miles per gallon of each

31
00:01:26,780 --> 00:01:28,759
car and the price of each car so we&#39;re

32
00:01:28,759 --> 00:01:30,350
going to do some summary statistics on

33
00:01:30,350 --> 00:01:33,530
those three columns but there&#39;s a little

34
00:01:33,530 --> 00:01:35,240
issue there are some missing values

35
00:01:35,240 --> 00:01:38,600
there so this code here for some of

36
00:01:38,600 --> 00:01:42,050
these numeric columns we that look

37
00:01:42,050 --> 00:01:45,200
through and find where the value is

38
00:01:45,200 --> 00:01:51,170
coded as a ? a text string ? and then we

39
00:01:51,170 --> 00:01:54,950
remove those rows we drop those rows so

40
00:01:54,950 --> 00:01:56,300
we&#39;re just going to throw them out

41
00:01:56,300 --> 00:01:58,039
not going to worry about them and then

42
00:01:58,039 --> 00:02:00,349
we have to coerce them back to numeric

43
00:02:00,349 --> 00:02:02,209
so that&#39;s all we&#39;re doing there

44
00:02:02,209 --> 00:02:06,140
let me run that for you and I printed

45
00:02:06,140 --> 00:02:09,560
what&#39;s called the info on that data

46
00:02:09,560 --> 00:02:11,510
frame and let&#39;s just scroll to the

47
00:02:11,510 --> 00:02:14,390
bottom so which are the columns we care

48
00:02:14,390 --> 00:02:15,260
about

49
00:02:15,260 --> 00:02:17,850
so we&#39;ve got horsepower

50
00:02:17,850 --> 00:02:19,260
and we see there&#39;s a hundred ninety-five

51
00:02:19,260 --> 00:02:22,350
values that are non null and it&#39;s a

52
00:02:22,350 --> 00:02:25,650
integer we have city miles per gallon

53
00:02:25,650 --> 00:02:28,950
which again the 995 non null values

54
00:02:28,950 --> 00:02:31,170
which is also an integer in our price

55
00:02:31,170 --> 00:02:33,780
which is the same as the others also an

56
00:02:33,780 --> 00:02:36,510
integer so that&#39;s what we&#39;re going to

57
00:02:36,510 --> 00:02:40,960
work on us those three so let&#39;s start by

58
00:02:40,960 --> 00:02:43,410
using

59
00:02:43,410 --> 00:02:47,640
some the pandas described method here

60
00:02:47,640 --> 00:02:51,630
computes summary statistics so this

61
00:02:51,630 --> 00:02:53,790
little function we give it the name of a

62
00:02:53,790 --> 00:02:56,490
data frame when we call it and we give

63
00:02:56,490 --> 00:02:58,920
it the column name that we want the

64
00:02:58,920 --> 00:03:00,870
summary statistics for so in this case

65
00:03:00,870 --> 00:03:04,940
price and

66
00:03:04,940 --> 00:03:06,410
there&#39;s a little bit of managing here

67
00:03:06,410 --> 00:03:10,610
just to get the name median in our list

68
00:03:10,610 --> 00:03:14,300
so that&#39;s just a detail and I&#39;ve

69
00:03:14,300 --> 00:03:17,120
computed those summary statistics and

70
00:03:17,120 --> 00:03:19,220
you can see as we already knew there&#39;s a

71
00:03:19,220 --> 00:03:22,790
hundred ninety five cases

72
00:03:22,790 --> 00:03:26,269
the mean is a little over 13 thousand

73
00:03:26,269 --> 00:03:30,349
dollars the median is quite a bit less

74
00:03:30,349 --> 00:03:33,109
it&#39;s just barely ten thousand dollars so

75
00:03:33,109 --> 00:03:34,790
that would indicate to me that there&#39;s

76
00:03:34,790 --> 00:03:37,069
quite a lot of asymmetry in the

77
00:03:37,069 --> 00:03:38,540
distribution of the price of these

78
00:03:38,540 --> 00:03:41,599
automobiles the standard deviations

79
00:03:41,599 --> 00:03:44,599
about eight thousand dollars are so we

80
00:03:44,599 --> 00:03:48,340
expect quite a spread of

81
00:03:48,370 --> 00:03:50,920
auto prices the minimum is a little over

82
00:03:50,920 --> 00:03:53,319
five thousand dollars and the maximum is

83
00:03:53,319 --> 00:03:55,510
over 45,000 dollars so again confirming

84
00:03:55,510 --> 00:03:59,349
we have a widespread asymmetric

85
00:03:59,349 --> 00:04:02,260
distribution here arm and if you look at

86
00:04:02,260 --> 00:04:05,920
these twenty-five percent 75% want

87
00:04:05,920 --> 00:04:08,920
quantiles that also confirms that

88
00:04:08,920 --> 00:04:12,530
initial hypothesis

89
00:04:12,530 --> 00:04:14,390
but we can also visualize that

90
00:04:14,390 --> 00:04:17,540
distribution and I&#39;ve just got a little

91
00:04:17,540 --> 00:04:22,380
bit of code here to do that and

92
00:04:22,380 --> 00:04:24,510
all I&#39;m going to do is compute a

93
00:04:24,510 --> 00:04:26,710
histogram

94
00:04:26,710 --> 00:04:30,580
and a boxplot

95
00:04:30,580 --> 00:04:33,039
of those two and i&#39;m going to stack them

96
00:04:33,039 --> 00:04:34,599
one on top of the other so that&#39;s all

97
00:04:34,599 --> 00:04:37,060
that code is doing and we&#39;ll talk about

98
00:04:37,060 --> 00:04:40,479
later the details of Python plotting but

99
00:04:40,479 --> 00:04:43,770
let me just run that for you

100
00:04:45,490 --> 00:04:49,030
and there you have it so as Cynthia

101
00:04:49,030 --> 00:04:52,199
discuss the box plot

102
00:04:52,400 --> 00:04:55,040
we&#39;ve got the median shown in the red

103
00:04:55,040 --> 00:04:58,430
bar here we&#39;ve got the this first lower

104
00:04:58,430 --> 00:05:01,220
quartile here in this first upper

105
00:05:01,220 --> 00:05:03,889
quartile here around the median so again

106
00:05:03,889 --> 00:05:06,080
there&#39;s definitely a symmetry look at

107
00:05:06,080 --> 00:05:08,840
this range here is much shorter than

108
00:05:08,840 --> 00:05:12,229
this range there and we have a short

109
00:05:12,229 --> 00:05:14,419
whisker here a long whisker they&#39;re

110
00:05:14,419 --> 00:05:17,690
going out to one of the head it&#39;ll be

111
00:05:17,690 --> 00:05:19,850
one and a half times the interquartile

112
00:05:19,850 --> 00:05:22,850
range and then we have a bunch of

113
00:05:22,850 --> 00:05:25,190
outlier so basically we have a lot of

114
00:05:25,190 --> 00:05:29,180
relatively inexpensive cars

115
00:05:29,180 --> 00:05:32,509
in a few very expensive cars if we look

116
00:05:32,509 --> 00:05:34,430
at the histogram we get a different view

117
00:05:34,430 --> 00:05:36,830
of that that&#39;s basically has the same

118
00:05:36,830 --> 00:05:39,139
interpretation you can see the most

119
00:05:39,139 --> 00:05:42,590
frequent auto price is probably around

120
00:05:42,590 --> 00:05:44,539
seven or eight thousand dollars in this

121
00:05:44,539 --> 00:05:48,470
data set and only a few cars or say over

122
00:05:48,470 --> 00:05:50,180
thirty thousand dollars so it&#39;s very

123
00:05:50,180 --> 00:05:52,550
asymmetric and of course because it&#39;s

124
00:05:52,550 --> 00:05:57,410
the price of cars there are no cars the

125
00:05:57,410 --> 00:06:02,120
at or near zero and so let&#39;s look at one

126
00:06:02,120 --> 00:06:04,160
other statistic here we&#39;ll just look at

127
00:06:04,160 --> 00:06:06,430
engine size

128
00:06:06,430 --> 00:06:09,610
we&#39;re just going to plot that and again

129
00:06:09,610 --> 00:06:13,030
it it&#39;s a little more symmetric looking

130
00:06:13,030 --> 00:06:14,890
in the box plot but again we have a few

131
00:06:14,890 --> 00:06:18,330
outliers here

132
00:06:18,330 --> 00:06:20,879
and if we look at that histogram we can

133
00:06:20,879 --> 00:06:23,250
see that the most frequent value is

134
00:06:23,250 --> 00:06:25,919
probably in the low hundreds of horse

135
00:06:25,919 --> 00:06:27,719
pass hundred and something hundred and

136
00:06:27,719 --> 00:06:31,110
twenty horsepower maybe are very few

137
00:06:31,110 --> 00:06:34,460
cars with extremely low horsepower

138
00:06:34,460 --> 00:06:39,270
and a number of cars just a few cars

139
00:06:39,270 --> 00:06:43,280
with very high horsepower

140
00:06:43,280 --> 00:06:46,340
so I hope that gives you some idea of

141
00:06:46,340 --> 00:06:49,400
how to compute in practice some summary

142
00:06:49,400 --> 00:06:51,320
statistics and how we think about them

143
00:06:51,320 --> 00:06:56,720
to to do an initial exploration of some

144
00:06:56,720 --> 00:06:59,080
data

