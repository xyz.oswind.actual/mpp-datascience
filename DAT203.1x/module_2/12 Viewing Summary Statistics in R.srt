0
00:00:01,070 --> 00:00:03,650
so Cynthia has been discussing how

1
00:00:03,650 --> 00:00:05,870
summary statistics are computed with the

2
00:00:05,870 --> 00:00:08,029
formulas are a little bit about how we

3
00:00:08,029 --> 00:00:10,969
interpret them and this demo i&#39;d like to

4
00:00:10,969 --> 00:00:12,650
show you how we actually compute the

5
00:00:12,650 --> 00:00:15,950
summary statistics using our and we&#39;ll

6
00:00:15,950 --> 00:00:19,010
talk about some practical issues of what

7
00:00:19,010 --> 00:00:20,630
we can do to interpret summary

8
00:00:20,630 --> 00:00:23,470
statistics

9
00:00:23,720 --> 00:00:26,509
so my screen here i have a notebook

10
00:00:26,509 --> 00:00:28,340
which I&#39;ve started in Azure machine

11
00:00:28,340 --> 00:00:32,000
learning and this first cell contains

12
00:00:32,000 --> 00:00:36,410
the auto-generated code which takes my

13
00:00:36,410 --> 00:00:37,940
data set

14
00:00:37,940 --> 00:00:42,019
automobile price data raw dot CSV and

15
00:00:42,019 --> 00:00:44,480
gives me back and our data frame here so

16
00:00:44,480 --> 00:00:47,650
let me just run that code

17
00:00:48,670 --> 00:00:52,719
they&#39;re so we have the data frame loaded

18
00:00:52,719 --> 00:00:55,110
it&#39;s named at

19
00:00:55,110 --> 00:00:58,350
in another auto-generated piece of code

20
00:00:58,350 --> 00:01:00,090
we can see the head so basically the

21
00:01:00,090 --> 00:01:05,900
first five rows of that data frame

22
00:01:06,300 --> 00:01:08,700
about 200 automobiles in this data set

23
00:01:08,700 --> 00:01:11,010
and i&#39;m not going to talk about every

24
00:01:11,010 --> 00:01:14,010
column at this point let&#39;s just talk

25
00:01:14,010 --> 00:01:17,220
about the ones we care about so this

26
00:01:17,220 --> 00:01:19,890
column called horsepower gives you the

27
00:01:19,890 --> 00:01:24,120
horsepower of the car&#39;s engine city

28
00:01:24,120 --> 00:01:27,120
miles per gallon gives you the miles per

29
00:01:27,120 --> 00:01:30,090
gallon of that car and city driving and

30
00:01:30,090 --> 00:01:32,460
price is the price of that car

31
00:01:32,460 --> 00:01:35,300
simple enough

32
00:01:35,350 --> 00:01:37,210
but there is a problem there are missing

33
00:01:37,210 --> 00:01:39,880
values in this

34
00:01:39,880 --> 00:01:44,140
in some of these numeric columns so this

35
00:01:44,140 --> 00:01:45,920
code

36
00:01:45,920 --> 00:01:48,290
basically iterates over these numeric

37
00:01:48,290 --> 00:01:51,590
columns we&#39;re looking for a text string

38
00:01:51,590 --> 00:01:54,320
? that indicates a missing value we&#39;re

39
00:01:54,320 --> 00:01:57,619
going to replace it with and RNA value

40
00:01:57,619 --> 00:02:00,710
and then we use there&#39;s an R function

41
00:02:00,710 --> 00:02:04,580
called complete cases so complete cases

42
00:02:04,580 --> 00:02:07,700
returns a true if the row has no missing

43
00:02:07,700 --> 00:02:10,300
values it returns false

44
00:02:10,300 --> 00:02:14,350
if the row has a missing value so we

45
00:02:14,350 --> 00:02:16,780
wind up with a clean data frame and then

46
00:02:16,780 --> 00:02:18,520
we make sure we have coerced all those

47
00:02:18,520 --> 00:02:22,450
columns to numeric so let me run this

48
00:02:22,450 --> 00:02:23,620
for you oh and then we&#39;re going to run

49
00:02:23,620 --> 00:02:27,340
the stir function on that so we&#39;ll we&#39;ll

50
00:02:27,340 --> 00:02:31,950
see a summary of some of those data

51
00:02:31,950 --> 00:02:35,340
alright so there&#39;s the summary of these

52
00:02:35,340 --> 00:02:37,940
columns in the data

53
00:02:37,940 --> 00:02:40,220
the ones we care about horsepower it&#39;s a

54
00:02:40,220 --> 00:02:42,860
numeric looks like they&#39;re all integers

55
00:02:42,860 --> 00:02:46,130
city miles per gallon again now is

56
00:02:46,130 --> 00:02:48,860
actually integer is an integer and they

57
00:02:48,860 --> 00:02:53,460
look like integers price is numeric

58
00:02:53,460 --> 00:02:55,890
so those are the three columns that we

59
00:02:55,890 --> 00:02:56,880
care about

60
00:02:56,880 --> 00:02:57,900
we&#39;re not going to look at the rest of

61
00:02:57,900 --> 00:02:59,770
these just now

62
00:02:59,770 --> 00:03:01,360
and we can compute some summary

63
00:03:01,360 --> 00:03:04,800
statistics so there&#39;s a

64
00:03:04,800 --> 00:03:08,710
summary function in our

65
00:03:08,710 --> 00:03:11,790
actually called summary

66
00:03:11,790 --> 00:03:13,799
and we&#39;ll so we select the column we

67
00:03:13,799 --> 00:03:15,870
want from the data frame we run summary

68
00:03:15,870 --> 00:03:20,390
on it i&#39;m doing some things too

69
00:03:20,700 --> 00:03:22,920
add the standard deviation which doesn&#39;t

70
00:03:22,920 --> 00:03:26,879
normally show up in the summary

71
00:03:26,879 --> 00:03:29,910
statistics the way our does it and give

72
00:03:29,910 --> 00:03:33,660
it some proper names and so we&#39;ll just

73
00:03:33,660 --> 00:03:35,819
do that for one column will do it for

74
00:03:35,819 --> 00:03:38,360
price

75
00:03:39,360 --> 00:03:41,530
and there we have it

76
00:03:41,530 --> 00:03:44,740
so first off let&#39;s look at the mean mean

77
00:03:44,740 --> 00:03:48,550
is just a little over 13,000

78
00:03:48,550 --> 00:03:50,200
but notice that the median is just

79
00:03:50,200 --> 00:03:52,330
barely over 10,000 so it&#39;s much lower

80
00:03:52,330 --> 00:03:55,210
and that indicates that discrepancy

81
00:03:55,210 --> 00:03:57,940
between mean and median indicates that

82
00:03:57,940 --> 00:04:01,060
we have probably some asymmetry in this

83
00:04:01,060 --> 00:04:04,930
distribution we also have a pretty wide

84
00:04:04,930 --> 00:04:08,560
standard deviation about of 8,000 on a

85
00:04:08,560 --> 00:04:10,900
mean of 13,000 so it indicates a

86
00:04:10,900 --> 00:04:15,190
widespread and you can see the minimum

87
00:04:15,190 --> 00:04:17,170
is only about five thousand whereas our

88
00:04:17,170 --> 00:04:19,060
most expensive car in this data set is

89
00:04:19,060 --> 00:04:22,960
over 45,000 so again indicating there&#39;s

90
00:04:22,960 --> 00:04:26,020
a wide range of data values a wide

91
00:04:26,020 --> 00:04:30,130
spread or dispersion of those data and

92
00:04:30,130 --> 00:04:31,940
if you look at the

93
00:04:31,940 --> 00:04:34,350
these first

94
00:04:34,350 --> 00:04:36,690
third quartile so it&#39;s the 25-percent in

95
00:04:36,690 --> 00:04:41,010
75% you see there&#39;s quite a around the

96
00:04:41,010 --> 00:04:43,050
median there&#39;s quite a bit of asymmetry

97
00:04:43,050 --> 00:04:45,540
in those differences so we&#39;re expecting

98
00:04:45,540 --> 00:04:47,790
a distribution where they&#39;re more

99
00:04:47,790 --> 00:04:49,830
cheaper cars and fewer more fewer

100
00:04:49,830 --> 00:04:51,900
expensive cars just from examining those

101
00:04:51,900 --> 00:04:55,340
summary statistics

102
00:04:55,480 --> 00:04:57,310
and we can check that we can check that

103
00:04:57,310 --> 00:05:01,200
visually so this code here

104
00:05:01,670 --> 00:05:04,790
i&#39;m using the g plot to package which

105
00:05:04,790 --> 00:05:07,640
we&#39;ll talk about in future lessons but

106
00:05:07,640 --> 00:05:10,040
i&#39;m going to make a histogram and I&#39;m

107
00:05:10,040 --> 00:05:13,210
going to make a box plot

108
00:05:13,900 --> 00:05:15,289
and

109
00:05:15,289 --> 00:05:17,240
and we&#39;re just going to lay those one on

110
00:05:17,240 --> 00:05:19,930
top of the other

111
00:05:20,210 --> 00:05:23,330
two robes basically so let me run that

112
00:05:23,330 --> 00:05:25,750
for you

113
00:05:27,370 --> 00:05:29,590
alright so let&#39;s start with our box plot

114
00:05:29,590 --> 00:05:32,770
Cynthia discuss the box plot the dark

115
00:05:32,770 --> 00:05:36,460
line here is our median value which as

116
00:05:36,460 --> 00:05:39,130
we expect is just over 10,000 and look

117
00:05:39,130 --> 00:05:42,970
at this first lower quartile it&#39;s pretty

118
00:05:42,970 --> 00:05:44,979
narrow compared to this first upper

119
00:05:44,979 --> 00:05:47,320
quartile again very much indicating

120
00:05:47,320 --> 00:05:50,710
strong asymmetry and this whisker is

121
00:05:50,710 --> 00:05:53,199
pretty short here down to the minimum

122
00:05:53,199 --> 00:05:55,630
value which was around little over five

123
00:05:55,630 --> 00:05:58,270
thousand whereas this whisker is quite

124
00:05:58,270 --> 00:06:00,550
long as it&#39;s one and a half times the

125
00:06:00,550 --> 00:06:03,100
interquartile range which is as long as

126
00:06:03,100 --> 00:06:06,430
the whisker can be and then we have some

127
00:06:06,430 --> 00:06:10,570
cars that are really quite expensive you

128
00:06:10,570 --> 00:06:12,490
see these few outlier cars very

129
00:06:12,490 --> 00:06:16,289
expensive cars are shown in the dots

130
00:06:16,289 --> 00:06:18,479
we can get a different view of that from

131
00:06:18,479 --> 00:06:21,119
the histogram and you see the most

132
00:06:21,119 --> 00:06:24,569
frequent price of cars is in here maybe

133
00:06:24,569 --> 00:06:26,550
maybe it&#39;s around eight thousand dollars

134
00:06:26,550 --> 00:06:28,349
or something so relatively inexpensive

135
00:06:28,349 --> 00:06:30,479
cars there&#39;s a lot of an expensive cars

136
00:06:30,479 --> 00:06:33,719
and the distribution tapers off and we

137
00:06:33,719 --> 00:06:37,319
only have these few cars that are over

138
00:06:37,319 --> 00:06:39,180
30 thousand dollars those correspond to

139
00:06:39,180 --> 00:06:42,070
are outliers here

140
00:06:42,070 --> 00:06:44,670
and we can look at one other

141
00:06:44,670 --> 00:06:50,540
column engine size which will use later

142
00:06:51,409 --> 00:06:53,629
to get we see it&#39;s a little less

143
00:06:53,629 --> 00:06:55,789
asymmetric because you see the court

144
00:06:55,789 --> 00:06:58,099
interquartile the quartile range of this

145
00:06:58,099 --> 00:07:00,409
little first lower quartile is only

146
00:07:00,409 --> 00:07:03,769
slightly smaller than the range of this

147
00:07:03,769 --> 00:07:06,229
first upper quartile the whiskers only

148
00:07:06,229 --> 00:07:08,119
slightly shorter than the upper whisker

149
00:07:08,119 --> 00:07:11,929
we still do have a few outliers and the

150
00:07:11,929 --> 00:07:14,899
histogram helps confirm that idea

151
00:07:14,899 --> 00:07:16,580
you see we have a few cars with very

152
00:07:16,580 --> 00:07:19,789
small engines we have kind of a cluster

153
00:07:19,789 --> 00:07:23,360
of cars plus or minus around a hundred

154
00:07:23,360 --> 00:07:26,749
cubic inches and we have a few cars with

155
00:07:26,749 --> 00:07:30,279
really large engines

156
00:07:31,620 --> 00:07:36,360
so I hope that this demo has given you

157
00:07:36,360 --> 00:07:39,550
some idea of

158
00:07:39,550 --> 00:07:41,110
practical aspects of just computing

159
00:07:41,110 --> 00:07:42,790
summary statistics but also when you

160
00:07:42,790 --> 00:07:44,380
look at a dataset you&#39;re trying to

161
00:07:44,380 --> 00:07:47,500
understand the variables how a few

162
00:07:47,500 --> 00:07:49,330
summary statistics can really give you

163
00:07:49,330 --> 00:07:52,120
an initial guidance into some aspects of

164
00:07:52,120 --> 00:07:55,530
the behavior of those variables

