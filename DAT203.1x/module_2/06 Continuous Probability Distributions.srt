0
00:00:04,846 --> 00:00:07,442
Okay, so let&#39;s talk about continuous probability

1
00:00:07,442 --> 00:00:09,988
distributions and continuous random variables.

2
00:00:09,988 --> 00:00:14,700
Now if I wanted to write a table that

3
00:00:15,820 --> 00:00:19,300
would tell me for each outcome what the probability is for

4
00:00:19,300 --> 00:00:23,410
a continuous random variable, this would not work.

5
00:00:23,410 --> 00:00:25,850
This would fail very, very badly.

6
00:00:25,850 --> 00:00:29,605
There are uncountably infinite amount of values in this table

7
00:00:29,605 --> 00:00:32,580
and all the probabilities are exactly zero.

8
00:00:32,580 --> 00:00:33,500
So that does not work.

9
00:00:34,860 --> 00:00:39,570
But I can draw a curve that represents something

10
00:00:39,570 --> 00:00:43,140
meaningful for continuous random variables,

11
00:00:43,140 --> 00:00:45,520
which is the probability distribution function.

12
00:00:46,530 --> 00:00:52,500
There&#39;s Steve, y&#39;all know him, and this is a distribution.

13
00:00:52,500 --> 00:00:57,348
And so the higher this curve is, the more likely the value is.

14
00:00:57,348 --> 00:01:01,982
So this particular distribution is the distribution of how much

15
00:01:01,982 --> 00:01:06,442
coffee Steve Elston is going to consume in liters over the next

16
00:01:06,442 --> 00:01:06,976
week.

17
00:01:06,976 --> 00:01:10,589
Okay, so he&#39;s more likely to consume somewhere around ten

18
00:01:10,589 --> 00:01:14,210
liters than he is to consume somewhere around zero liters.

19
00:01:15,550 --> 00:01:18,660
But the probability that he&#39;ll consume any

20
00:01:18,660 --> 00:01:22,232
particular exact amount of coffee is exactly zero.

21
00:01:22,232 --> 00:01:27,790
Right, so it no longer make sense to say something like

22
00:01:27,790 --> 00:01:30,890
the probability he&#39;ll consume 8.23576 liters of coffee is 0.1,

23
00:01:30,890 --> 00:01:35,110
all right, that makes no sense here.

24
00:01:36,430 --> 00:01:38,310
The probability is zero.

25
00:01:38,310 --> 00:01:43,172
It makes much more sense to say what is the probability

26
00:01:43,172 --> 00:01:48,180
that Steve will consume between 10 and 15 liters?

27
00:01:48,180 --> 00:01:51,556
And that is going to be this much.

28
00:01:51,556 --> 00:01:57,840
The shaded area here is the area under this curve, okay?

29
00:01:57,840 --> 00:02:01,700
That&#39;s the probability, to get between these two values.

30
00:02:03,720 --> 00:02:06,230
So if I try to ask what the probability is to land on a

31
00:02:06,230 --> 00:02:11,360
particular value, the area under that single value will be zero.

32
00:02:11,360 --> 00:02:15,034
And if I look at a range of values it won&#39;t be zero.

33
00:02:18,659 --> 00:02:21,235
Or you could say what&#39;s the probability

34
00:02:21,235 --> 00:02:24,630
that Steve will have less than ten liters?

35
00:02:24,630 --> 00:02:30,229
And that&#39;s about a 50% probability or 0.5.

36
00:02:30,229 --> 00:02:32,430
And now you could say,

37
00:02:32,430 --> 00:02:36,290
what&#39;s the probability that he&#39;ll have less than 20 liters?

38
00:02:36,290 --> 00:02:38,783
Looks like he pretty much always has less than 20 liters.

39
00:02:38,783 --> 00:02:40,540
That&#39;s a good thing,

40
00:02:40,540 --> 00:02:45,634
because it&#39;s probably not good to drink quite that much coffee.

41
00:02:45,634 --> 00:02:48,039
So this probability is about one.

42
00:02:48,039 --> 00:02:51,300
Okay, that&#39;s the largest probability you can ever get.

43
00:02:51,300 --> 00:02:53,940
Right, one is the total area under this whole thing.

44
00:02:56,680 --> 00:02:59,409
So this thing integrates to one and

45
00:02:59,409 --> 00:03:02,523
the areas under it are probabilities.

46
00:03:02,523 --> 00:03:05,850
Those are the key aspects of the probability density function.

47
00:03:08,268 --> 00:03:10,850
Now these PDFs come in all shapes and

48
00:03:10,850 --> 00:03:13,570
sizes, it doesn&#39;t have to look like a bump.

49
00:03:13,570 --> 00:03:16,390
It can look like two bumps or it can look very flat, or

50
00:03:16,390 --> 00:03:17,430
it can, whatever.

51
00:03:17,430 --> 00:03:18,870
It just has to integrate to one.

52
00:03:20,210 --> 00:03:22,380
So this is the probability density function for

53
00:03:22,380 --> 00:03:24,080
what&#39;s called the uniform distribution.

54
00:03:24,080 --> 00:03:26,052
It&#39;s completely flat.

55
00:03:26,052 --> 00:03:32,076
So this uniform distribution means that any value between,

56
00:03:32,076 --> 00:03:39,450
any value between 10 and 20 is sort of equally probable, okay?

57
00:03:39,450 --> 00:03:42,386
So, but remember it doesn&#39;t make sense to talk about

58
00:03:42,386 --> 00:03:43,569
a particular value.

59
00:03:43,569 --> 00:03:48,365
But what I can say is that the probability to get between

60
00:03:48,365 --> 00:03:51,290
10 and 12 is that area there.

61
00:03:51,290 --> 00:03:55,577
And you can probably figure out what that area is if the whole

62
00:03:55,577 --> 00:04:00,122
thing has to integrate to 1 and that&#39;s 10 and that&#39;s 20,

63
00:04:00,122 --> 00:04:02,465
then the area is 0.2, okay.

64
00:04:02,465 --> 00:04:07,411
So 2 wide by one-tenth high, so, 0.2.

65
00:04:10,860 --> 00:04:12,670
So back to this.

66
00:04:12,670 --> 00:04:14,280
How do I calculate these things?

67
00:04:16,620 --> 00:04:18,930
Well, the way we usually do

68
00:04:18,930 --> 00:04:23,100
it is we do it by subtracting two quantities.

69
00:04:23,100 --> 00:04:27,550
So we&#39;d calculate this quantity over here, and

70
00:04:27,550 --> 00:04:32,920
then I would subtract this one, to get the probability I want,

71
00:04:32,920 --> 00:04:34,030
which is the thing in pink.

72
00:04:35,980 --> 00:04:40,668
So now the question of computing probabilities becomes a question

73
00:04:40,668 --> 00:04:43,848
of computing the areas to the left of things,

74
00:04:43,848 --> 00:04:46,624
to the left of any particular outcome.

75
00:04:46,624 --> 00:04:49,975
And this brings me to the discussion of cumulative

76
00:04:49,975 --> 00:04:52,303
distribution functions or CDFs.

