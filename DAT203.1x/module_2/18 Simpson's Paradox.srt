0
00:00:04,791 --> 00:00:08,431
Simpson&#39;s paradox is a trap and you can easily fall into it and

1
00:00:08,431 --> 00:00:11,791
once you know about it, it&#39;s hard to believe any summary

2
00:00:11,791 --> 00:00:14,181
statistics than anyone ever tells you.

3
00:00:14,181 --> 00:00:16,155
It&#39;s really cool though.

4
00:00:16,155 --> 00:00:20,853
Okay so Simpson&#39;s paradox deals with aggregating smaller

5
00:00:20,853 --> 00:00:25,340
data sets into larger ones and it&#39;s when conclusions

6
00:00:25,340 --> 00:00:28,400
drawn about the smaller data sets are actually the opposite

7
00:00:28,400 --> 00:00:32,120
of conclusions drawn from the larger data sets.

8
00:00:32,119 --> 00:00:36,280
And you&#39;ll see that it occurs when there is a lurking variable

9
00:00:36,280 --> 00:00:39,620
and uneven-sized groups being combined, okay, so

10
00:00:39,620 --> 00:00:42,100
there are these two ingredients the lurking variable and

11
00:00:42,100 --> 00:00:43,300
the uneven-sized groups.

12
00:00:44,500 --> 00:00:46,350
Let me give you an example.

13
00:00:46,350 --> 00:00:48,650
This is about kidney stone treatments, okay.

14
00:00:48,650 --> 00:00:52,000
So there are two treatments, treatment A and treatment B and

15
00:00:52,000 --> 00:00:56,500
then this is the rate of success of the treatment.

16
00:00:56,500 --> 00:00:58,140
So clearly,

17
00:00:58,140 --> 00:01:02,052
it looks like treatment B is more effective right,

18
00:01:02,052 --> 00:01:07,410
because 83% of people who have treatment B are cured, right?

19
00:01:07,410 --> 00:01:11,765
289 out of the 350 people who took treatment B were cured,

20
00:01:11,765 --> 00:01:15,003
whereas with treatment A, it was only 78%.

21
00:01:15,003 --> 00:01:18,106
So which treatment would you take?

22
00:01:18,106 --> 00:01:19,120
It looks like treatment B.

23
00:01:19,120 --> 00:01:22,230
Now I&#39;m gonna give you a little bit more information.

24
00:01:23,830 --> 00:01:29,552
What I will tell you is that if you have a small kidney stone,

25
00:01:29,552 --> 00:01:34,255
then which treatment is more effective for you?

26
00:01:34,255 --> 00:01:38,310
Treatment A, because you&#39;re cured 93% of the time.

27
00:01:39,650 --> 00:01:41,220
If you have a large kidney stone,

28
00:01:42,460 --> 00:01:44,210
which treatment is more effective for you?

29
00:01:46,350 --> 00:01:51,470
Treatment A, cuz it&#39;s 73 versus 69.

30
00:01:51,470 --> 00:01:55,710
So treatment A is more effective for you whether you have a small

31
00:01:55,710 --> 00:01:57,740
kidney stone or a large kidney stone.

32
00:01:59,150 --> 00:02:00,730
But if you look at both,

33
00:02:00,730 --> 00:02:03,590
if you combine the data together, it looks like

34
00:02:03,590 --> 00:02:07,810
treatment B is more effective because 83% of people are cured

35
00:02:07,810 --> 00:02:11,340
from treatment B who took it versus 78 from treatment A.

36
00:02:11,340 --> 00:02:13,780
So what happened?

37
00:02:13,780 --> 00:02:16,690
I give you more information and

38
00:02:16,690 --> 00:02:18,850
we get the opposite results, right?

39
00:02:18,850 --> 00:02:21,150
No matter whether you have small stones or large stones,

40
00:02:21,150 --> 00:02:23,400
treatment A is more effective,

41
00:02:23,400 --> 00:02:25,570
whereas if you just combine the information together,

42
00:02:25,570 --> 00:02:27,130
it looks like treatment B is more effective.

43
00:02:28,450 --> 00:02:31,180
Okay, so the answer is, this is a case of Simpson&#39;s paradox.

44
00:02:31,180 --> 00:02:34,090
There&#39;s a lurking variable which is the size of the kidney stone,

45
00:02:34,090 --> 00:02:37,370
and there are uneven sized groups being combined.

46
00:02:37,370 --> 00:02:39,100
In particular,

47
00:02:39,100 --> 00:02:43,430
since most of the small stone patients took treatment B and

48
00:02:43,430 --> 00:02:47,050
since most of the large stone patients took treatment A,

49
00:02:47,050 --> 00:02:51,200
you end up combining basically group two and group three.

50
00:02:51,200 --> 00:02:54,920
You end up combining the small stone people who took

51
00:02:54,920 --> 00:02:59,310
treatment B to the large stone patients that took treatment A,

52
00:02:59,310 --> 00:03:01,560
which is not fair, okay.

53
00:03:01,560 --> 00:03:03,691
So when you combine all that stuff together,

54
00:03:03,691 --> 00:03:05,935
you actually lost a whole lot of information and

55
00:03:05,935 --> 00:03:08,532
you got the opposite result than you were supposed to.

56
00:03:08,532 --> 00:03:11,258
Okay, so why do you think they give people treatment B at

57
00:03:11,258 --> 00:03:11,959
all, right?

58
00:03:11,959 --> 00:03:14,831
So if you have a small kidney stones, you get treatment B

59
00:03:14,831 --> 00:03:18,071
whereas, clearly treatment A is the better treatment and they

60
00:03:18,071 --> 00:03:22,120
give it to pretty much only to people with large kidney stones.

61
00:03:22,120 --> 00:03:25,806
And so the answer might be that treatment B is less invasive,

62
00:03:25,806 --> 00:03:29,058
it might have of less side effects, less expensive,

63
00:03:29,058 --> 00:03:31,320
maybe it&#39;s more easily available.

64
00:03:31,320 --> 00:03:35,890
So there&#39;s a clear reason why you would see data like this and

65
00:03:35,890 --> 00:03:38,310
it&#39;s very clear that treatment A is more effective.

66
00:03:38,310 --> 00:03:41,120
It&#39;s just that when you did this particular calculation that

67
00:03:41,120 --> 00:03:43,876
combine everything together, there is a case of Simpson&#39;s

68
00:03:43,876 --> 00:03:45,830
paradox and you got the wrong conclusion.

69
00:03:48,745 --> 00:03:50,990
Here&#39;s another example.

70
00:03:50,990 --> 00:03:53,930
We have two airlines, Blue Yonder Airlines and

71
00:03:53,930 --> 00:03:54,980
Consolidated Messenger.

72
00:03:56,220 --> 00:04:01,350
And what I&#39;m showing you is the delay rate on Blue Yonder

73
00:04:01,350 --> 00:04:06,110
is 13.3% and then Consolidated Messenger&#39;s delay rate is 10.9%.

74
00:04:06,110 --> 00:04:09,610
So which one would you rather take?

75
00:04:09,610 --> 00:04:13,215
Clearly Consolidated Messenger, because there are fewer delays.

76
00:04:15,105 --> 00:04:17,750
But now I&#39;m gonna give you more information.

77
00:04:17,750 --> 00:04:20,330
I&#39;m gonna break it down by city here and

78
00:04:20,329 --> 00:04:23,360
I&#39;m giving you the delay rate for each city and

79
00:04:23,360 --> 00:04:28,860
you can see that for every single city, Blue Yonder

80
00:04:30,050 --> 00:04:34,070
actually has a lower delay rate than Consolidated Messenger.

81
00:04:35,900 --> 00:04:37,772
So what happened?

82
00:04:37,772 --> 00:04:42,075
Well as it turns out, Consolidated Messenger flies

83
00:04:42,075 --> 00:04:46,963
mostly out of Phoenix, right, that&#39;s where most of it&#39;s

84
00:04:46,963 --> 00:04:52,167
flights are, out of Phoenix where the delay rate is 7.9%.

85
00:04:52,167 --> 00:04:55,888
Whereas Blue Yonder flies mostly out of Seattle, right, that&#39;s

86
00:04:55,888 --> 00:04:59,209
where a lot of its flights are, with a much higher delay rate

87
00:04:59,209 --> 00:05:03,540
because Seattle is a much bigger city, so delay rates are higher.

88
00:05:03,540 --> 00:05:05,690
So when you combine everything together,

89
00:05:05,690 --> 00:05:10,160
you&#39;re essentially comparing Blue Yonder&#39;s Seattle rate

90
00:05:10,160 --> 00:05:14,630
to Consolidated Messenger&#39;s Phoenix rate which is not fair.

91
00:05:14,630 --> 00:05:18,290
And so that is again a case of Simpson&#39;s Paradox,

92
00:05:18,290 --> 00:05:21,310
with a lurking variable which is the city, and

93
00:05:21,310 --> 00:05:23,150
uneven sized groups being combined.

94
00:05:24,200 --> 00:05:27,240
Though this is Simpson&#39;s Paradox, it deals with

95
00:05:27,240 --> 00:05:30,240
aggregating smaller data sets into larger ones and

96
00:05:30,240 --> 00:05:33,370
it&#39;s where conclusions drawn from the smaller data sets

97
00:05:33,370 --> 00:05:36,010
are actually the opposite of conclusions drawn from

98
00:05:36,010 --> 00:05:39,910
the larger data set and it occurs when there

99
00:05:39,910 --> 00:05:44,020
is a lurking variable and uneven sized groups being combined.

100
00:05:44,020 --> 00:05:46,460
You have to have those two ingredients.

101
00:05:46,460 --> 00:05:49,676
And after you hear about Simpson&#39;s Paradox, you&#39;re less

102
00:05:49,676 --> 00:05:52,902
likely to believe any statistics that you hear in the news.

103
00:05:52,902 --> 00:05:56,605
So I often wonder whether there&#39;s a lurking variable that

104
00:05:56,605 --> 00:06:00,385
would explain things in exactly the opposite way of what&#39;s

105
00:06:00,385 --> 00:06:01,940
reported.

106
00:06:01,940 --> 00:06:04,816
So I hope you enjoyed hearing about Simpson&#39;s paradox.

