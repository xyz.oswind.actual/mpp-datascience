0
00:00:05,053 --> 00:00:09,573
So this lecture is all about how to view data with respect to

1
00:00:09,573 --> 00:00:10,597
other data.

2
00:00:10,597 --> 00:00:13,703
So if you&#39;re telling me that you&#39;re an excellent salesman

3
00:00:13,703 --> 00:00:16,561
because you sold $100,000 worth of widgets,

4
00:00:16,561 --> 00:00:18,650
I have no idea what that means.

5
00:00:18,650 --> 00:00:21,410
You can be terrible compared to your peers, but

6
00:00:21,410 --> 00:00:24,040
how would I know that based on what you told me?

7
00:00:24,040 --> 00:00:28,518
If you tell me the z-score or what percentile you were,

8
00:00:28,518 --> 00:00:33,690
then that&#39;s a different story, right, that&#39;s meaningful.

9
00:00:33,690 --> 00:00:37,200
Obviously, variance in correlation are about

10
00:00:37,200 --> 00:00:41,570
the relationships between two random variables.

11
00:00:41,570 --> 00:00:43,275
Let&#39;s start.

12
00:00:43,275 --> 00:00:46,450
It&#39;s helpful to think of values relative to other values within

13
00:00:46,450 --> 00:00:47,600
the same distribution, and

14
00:00:47,600 --> 00:00:50,360
that&#39;s what a z-score tells you, it tells you where a point is

15
00:00:50,360 --> 00:00:53,100
relative to other points in the distribution.

16
00:00:53,100 --> 00:00:55,727
It&#39;s the number of standard deviations above or

17
00:00:55,727 --> 00:00:58,495
below the mean for a particular point.

18
00:00:58,495 --> 00:01:01,070
It&#39;s helpful to think of values relative to other values within

19
00:01:01,070 --> 00:01:02,560
the same distribution.

20
00:01:02,560 --> 00:01:05,670
If I tell you that I just sold 1200 units,

21
00:01:05,670 --> 00:01:09,110
that doesn&#39;t mean much, cuz you have no idea how good of

22
00:01:09,110 --> 00:01:12,400
a salesman I am, because you don&#39;t have enough context.

23
00:01:12,400 --> 00:01:15,610
What if I told you the mean was a 1000 units?

24
00:01:15,610 --> 00:01:16,782
Still, that doesn&#39;t tell you very much.

25
00:01:16,782 --> 00:01:20,600
You have no idea how unusual it is to go above 1200 units.

26
00:01:20,600 --> 00:01:22,190
Is that very unusual?

27
00:01:22,190 --> 00:01:25,322
Or it was just I was slightly above the mean?

28
00:01:25,322 --> 00:01:28,330
What you need also is the standard deviation.

29
00:01:28,330 --> 00:01:31,666
If I tell you it is 100 units, then you know I&#39;m in business

30
00:01:31,666 --> 00:01:35,079
because I&#39;m selling two standard deviations above the mean.

31
00:01:37,564 --> 00:01:40,595
Now, here&#39;s say a histogram here and

32
00:01:40,595 --> 00:01:45,840
I&#39;m selling here at 1200 and the mean is over here.

33
00:01:45,840 --> 00:01:48,850
Now the vast majority of other salespeople sell much

34
00:01:48,850 --> 00:01:51,890
less than 1200, so I&#39;m at the top of the pile if I&#39;m up here.

35
00:01:53,240 --> 00:01:57,336
Of course, this assumes that salesmen have an approximately

36
00:01:57,336 --> 00:02:00,879
normal distribution, which may or may not be true for

37
00:02:00,879 --> 00:02:04,361
a specific company, but we&#39;ll let that go for now.

38
00:02:04,361 --> 00:02:09,080
The z score of a point x is the number of standard deviations

39
00:02:09,080 --> 00:02:11,400
above or below the mean of X.

40
00:02:11,400 --> 00:02:14,474
And an easy way to compute that is to use this formula

41
00:02:14,474 --> 00:02:15,266
right here.

42
00:02:15,266 --> 00:02:17,958
But that looks a little complicated, so

43
00:02:17,958 --> 00:02:19,787
let&#39;s break it down a bit.

44
00:02:19,787 --> 00:02:22,820
Well, let us start with the original PDF of X.

45
00:02:22,820 --> 00:02:27,840
Let&#39;s say that this is X&#39;s PDF, X is random variable.

46
00:02:30,280 --> 00:02:33,927
Then I&#39;m gonna subtract the mean, so now this

47
00:02:33,927 --> 00:02:38,709
thing has mean 0 because I subtracted the mean, what I did.

48
00:02:41,804 --> 00:02:44,952
When I divide by the standard deviation here,

49
00:02:44,952 --> 00:02:47,697
I actually squish the distribution, so

50
00:02:47,697 --> 00:02:51,832
the distribution now has mean 0 and standard deviation 1.

51
00:02:51,832 --> 00:02:56,720
When I think about one standard deviation above the mean of X,

52
00:02:56,720 --> 00:03:01,421
it&#39;s exactly at the point 1 of this new distribution where

53
00:03:01,421 --> 00:03:02,089
z is 1.

54
00:03:02,089 --> 00:03:05,852
What I did when I subtracted the mean and divided by the standard

55
00:03:05,852 --> 00:03:09,615
deviation is that I shifted the distribution to have mean 0 and

56
00:03:09,615 --> 00:03:12,171
I scaled it to have standard deviation 1,

57
00:03:12,171 --> 00:03:15,590
where I standardized the distribution.

58
00:03:15,590 --> 00:03:19,870
In this way, z measures how many standard deviations X is

59
00:03:19,870 --> 00:03:21,090
above or below the mean.

60
00:03:22,510 --> 00:03:26,300
Now if you&#39;re working with data, you don&#39;t actually have

61
00:03:26,300 --> 00:03:29,170
the mean mu, you only have the sample mean X bar.

62
00:03:30,250 --> 00:03:33,948
People often get confused and call these z-scores, in fact,

63
00:03:33,948 --> 00:03:37,660
I do it myself but they&#39;re actually really sample z-scores.

64
00:03:37,660 --> 00:03:41,310
The sample z-score of X is actually the number of sample

65
00:03:41,310 --> 00:03:44,820
standard deviations above or below the sample mean.

66
00:03:46,090 --> 00:03:51,177
Okay, so here is a histogram of my data, and

67
00:03:51,177 --> 00:03:56,414
you can see that the sample mean is 1000.

68
00:03:56,414 --> 00:04:01,577
And my sample z-score is about 2 because

69
00:04:01,577 --> 00:04:09,920
I&#39;m 2 sample standard deviations above the sample mean.

70
00:04:09,920 --> 00:04:13,730
Just to give you some perspective, let me discuss for

71
00:04:13,730 --> 00:04:15,639
you how rare that actually is.

72
00:04:17,750 --> 00:04:21,765
Here is a standard normal with mean 0 and variance 1.

73
00:04:25,045 --> 00:04:28,941
Now it turns out that 68% of the time,

74
00:04:28,941 --> 00:04:33,993
you&#39;re within one standard deviation of the mean.

75
00:04:33,993 --> 00:04:36,702
Now you can&#39;t calculate this analytically, by the way,

76
00:04:36,702 --> 00:04:39,417
you actually need a computer to do this to get that 68%.

77
00:04:43,773 --> 00:04:46,205
As it turns out that 95% of the time,

78
00:04:46,205 --> 00:04:49,542
you&#39;re within two standard deviations of the mean.

79
00:04:51,538 --> 00:04:53,426
And 99.7% of the time,

80
00:04:53,426 --> 00:04:57,130
you&#39;re within three standard deviations of the mean.

81
00:04:58,760 --> 00:05:00,982
Now you can put it into context.

82
00:05:00,982 --> 00:05:03,788
Though I sold 1200 units, the mean is 1000 and

83
00:05:03,788 --> 00:05:07,310
the standard deviation is 100, but the z-score is 2.

84
00:05:07,310 --> 00:05:10,610
So I sold two standard deviations above the mean and

85
00:05:10,610 --> 00:05:14,691
the probability to be that extreme is actually only 2.5%.

86
00:05:14,691 --> 00:05:16,842
So I&#39;m a pretty unusual sales person.

