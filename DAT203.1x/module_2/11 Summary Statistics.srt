0
00:00:04,897 --> 00:00:07,889
So we just discussed visual ways to summarize data,

1
00:00:07,889 --> 00:00:11,020
now let&#39;s discuss numerical ways to summarize data.

2
00:00:12,140 --> 00:00:14,750
So, I&#39;ll present some summary statistics

3
00:00:14,750 --> 00:00:16,970
starting with the sample mean.

4
00:00:16,970 --> 00:00:20,438
So, the sample mean is just the average of the data points you

5
00:00:20,438 --> 00:00:23,380
have, and you shouldn&#39;t confuse x-bar with mu.

6
00:00:23,380 --> 00:00:25,480
Usually, mu is the mean of the whole distribution,

7
00:00:25,480 --> 00:00:28,110
whereas x-bar is just the average of the points

8
00:00:28,110 --> 00:00:30,370
that we draw from the distribution.

9
00:00:30,370 --> 00:00:32,590
So just be careful, because everything in these slides

10
00:00:32,590 --> 00:00:35,280
are quantities that we compute from a sample of the data,

11
00:00:35,280 --> 00:00:36,960
they&#39;re not the computations that we would

12
00:00:36,960 --> 00:00:41,530
do if we had the underlying population or the PDF.

13
00:00:41,530 --> 00:00:43,610
So, as long as the sample&#39;s large enough though,

14
00:00:43,610 --> 00:00:46,000
as long as the n is large enough, then x-bar is very,

15
00:00:46,000 --> 00:00:48,800
very similar to mu, the population mean.

16
00:00:48,800 --> 00:00:51,300
So that&#39;s called the sample mean.

17
00:00:52,640 --> 00:00:54,103
And then quantiles and

18
00:00:54,103 --> 00:00:57,418
percentiles are also useful summary statistics.

19
00:00:57,418 --> 00:01:00,956
And to compute those, you&#39;d order the data, and

20
00:01:00,956 --> 00:01:04,663
then you find which data point is the one that divides

21
00:01:04,663 --> 00:01:07,864
the data into two parts, where a fraction p of

22
00:01:07,864 --> 00:01:11,655
the data values are less than or equal to this guy, and

23
00:01:11,655 --> 00:01:15,324
the remaining fraction are greater than that guy.

24
00:01:15,324 --> 00:01:18,715
Okay, so just to graphically put it out, so

25
00:01:18,715 --> 00:01:23,032
you put the data points in order from lowest to largest.

26
00:01:23,032 --> 00:01:27,336
And then you find the point, let&#39;s say you&#39;re looking for

27
00:01:27,336 --> 00:01:31,813
the 0.55 quantile or the 55th percentile, you look for

28
00:01:31,813 --> 00:01:35,343
the point where 55% of the data are below it and

29
00:01:35,343 --> 00:01:37,606
the other 45% are above it.

30
00:01:37,606 --> 00:01:42,360
And that&#39;s your point, that&#39;s that quantile, okay?

31
00:01:43,620 --> 00:01:48,108
And then the sample median is just the middle data point.

32
00:01:48,108 --> 00:01:52,105
It&#39;s also, you can use the definition of quantiles and

33
00:01:52,105 --> 00:01:56,785
percentiles to get to it because it&#39;s on the 50th percentile or

34
00:01:56,785 --> 00:01:58,417
the quantile 0.5.

35
00:01:58,417 --> 00:02:02,439
Now, I&#39;ve put up the formula for the sample variance here, and

36
00:02:02,439 --> 00:02:06,160
there&#39;s something strange going on here.

37
00:02:06,160 --> 00:02:08,570
So this part you would expect, right?

38
00:02:08,570 --> 00:02:11,670
This looks very similar to the definition of the population

39
00:02:11,670 --> 00:02:12,290
variance.

40
00:02:12,290 --> 00:02:15,135
You take each point, you subtract something like a mean,

41
00:02:15,135 --> 00:02:16,830
and you square it and you add it up.

42
00:02:18,580 --> 00:02:20,950
But you&#39;d think this would be a 1/n here, wouldn&#39;t you?

43
00:02:22,750 --> 00:02:26,335
But, surprise, it&#39;s n-1.

44
00:02:26,335 --> 00:02:28,190
So what is that n-1 doing there?

45
00:02:28,190 --> 00:02:32,700
And the reason for this is actually very technical,

46
00:02:32,700 --> 00:02:34,920
it has to do with something called bias.

47
00:02:34,920 --> 00:02:38,300
If we put a 1/n there, the sample variance would be what&#39;s

48
00:02:38,300 --> 00:02:41,650
called a biased estimator of the true population variance.

49
00:02:41,650 --> 00:02:43,400
But you shouldn&#39;t worry about this, but

50
00:02:43,400 --> 00:02:44,960
let the software do it for you.

51
00:02:44,960 --> 00:02:50,000
Every piece of software that I have ever used has had a command

52
00:02:50,000 --> 00:02:51,820
to compute the sample variance.

53
00:02:51,820 --> 00:02:54,870
You just give it the data, and you ask it for the variance, and

54
00:02:54,870 --> 00:02:59,180
it will give you, it will remember this 1/n-1 to give you

55
00:02:59,180 --> 00:03:02,410
the sample variance so that you don&#39;t have to remember it.

56
00:03:02,410 --> 00:03:05,120
And then luckily, the sample standard deviation is the same,

57
00:03:05,120 --> 00:03:06,990
it&#39;s just the square root of the sample variance.

58
00:03:09,230 --> 00:03:12,380
I have 500 points, they&#39;re in this giant vector,

59
00:03:12,380 --> 00:03:14,100
which I didn&#39;t feel like writing the whole thing out.

60
00:03:15,470 --> 00:03:17,570
And I decided to make a histogram.

61
00:03:17,570 --> 00:03:19,530
So I type my histogram command, and this is what I got.

62
00:03:21,220 --> 00:03:23,110
And now, I just want to summarize the important bits.

63
00:03:23,110 --> 00:03:25,857
I don&#39;t wanna preserve all of this detail in here which I

64
00:03:25,857 --> 00:03:27,211
don&#39;t necessarily need.

65
00:03:29,316 --> 00:03:35,000
So one thing I could look at is the five number summary.

66
00:03:36,080 --> 00:03:38,979
Let me break down for you what this five number summary is.

67
00:03:41,360 --> 00:03:44,550
The range is the distance between the minimum and

68
00:03:44,550 --> 00:03:45,158
the maximum.

69
00:03:45,158 --> 00:03:46,418
No surprise.

70
00:03:48,573 --> 00:03:53,494
The interquartile range is the distance between

71
00:03:53,494 --> 00:03:59,015
the first quartile, which is the 25th percentile,

72
00:03:59,015 --> 00:04:05,387
and then the 75th percentile, which is the third quartile.

73
00:04:05,387 --> 00:04:07,806
Now that you know how to compute percentiles,

74
00:04:07,806 --> 00:04:09,424
you can compute these things.

75
00:04:12,413 --> 00:04:17,686
Okay, so the five number summary is actually the smallest value,

76
00:04:17,685 --> 00:04:21,732
and the largest value, the median, which is Q2,

77
00:04:21,732 --> 00:04:24,837
here the median, and then Q1 and Q3,

78
00:04:24,837 --> 00:04:29,663
which are the 25th percentile and the 75th percentile.

79
00:04:29,663 --> 00:04:31,350
So once you&#39;ve got that,

80
00:04:31,350 --> 00:04:34,270
that tells you a lot about the distribution.

81
00:04:34,270 --> 00:04:38,118
So the five number summary is this collection of five values,

82
00:04:38,118 --> 00:04:40,344
it&#39;s the minimum and the maximum.

83
00:04:40,344 --> 00:04:43,911
The median, which is the 50th percentile.

84
00:04:43,911 --> 00:04:47,739
And then Q1 and Q3, which are the 25th percentile and

85
00:04:47,739 --> 00:04:49,426
the 75th percentile.

86
00:04:49,426 --> 00:04:53,588
So it&#39;s a nice simple way to characterize the distribution.

87
00:04:53,588 --> 00:04:55,960
Now, back to the visualizations.

88
00:04:55,960 --> 00:05:00,340
A box plot is a great visual way to represent a distribution,

89
00:05:00,340 --> 00:05:02,850
it has less information than the full distribution.

90
00:05:03,910 --> 00:05:06,860
This is the histogram, the full distribution.

91
00:05:06,860 --> 00:05:08,330
Here&#39;s the box plot.

92
00:05:08,330 --> 00:05:11,890
And it just conveys a summary of information.

93
00:05:11,890 --> 00:05:15,030
Now, you can see that the center is the same, right, instead of,

94
00:05:15,030 --> 00:05:18,700
the middle is about at 4, and so that&#39;s where the median is.

95
00:05:18,700 --> 00:05:19,728
This is the median over here.

96
00:05:19,728 --> 00:05:24,701
And if I&#39;m okay with summarizing a probability distribution

97
00:05:24,701 --> 00:05:28,618
like this, then I can get away with plotting many

98
00:05:28,618 --> 00:05:32,270
distributions succinctly on the same page.

99
00:05:32,270 --> 00:05:35,381
Cuz it&#39;s much easier to plot 50 of these things next to each

100
00:05:35,381 --> 00:05:37,820
other than 50 of those things.

101
00:05:37,820 --> 00:05:40,660
It&#39;s just visually easier for humans to understand.

102
00:05:42,020 --> 00:05:44,400
But how to get from the distribution to the box plot is

103
00:05:44,400 --> 00:05:46,570
a little bit weird, so let me tell you about that.

104
00:05:48,840 --> 00:05:51,050
Okay, so first, the middle bit is the median,

105
00:05:51,050 --> 00:05:54,580
as I mentioned, it&#39;s not the mean, it&#39;s the median.

106
00:05:55,600 --> 00:05:57,300
And strangely enough,

107
00:05:57,300 --> 00:06:01,040
the mean usually doesn&#39;t get marked on a box plot.

108
00:06:01,040 --> 00:06:02,890
I&#39;m not sure why not.

109
00:06:02,890 --> 00:06:05,650
Some people mark it on there, which I think is a good idea.

110
00:06:05,650 --> 00:06:09,220
But just because I think it&#39;s a good idea doesn&#39;t mean that

111
00:06:09,220 --> 00:06:12,590
people who write visualization software think it&#39;s a good idea.

112
00:06:12,590 --> 00:06:14,380
But in any case, that is the median.

113
00:06:16,550 --> 00:06:19,370
And then the first quartile is marked and

114
00:06:19,370 --> 00:06:20,450
the third quartile is marked.

115
00:06:22,430 --> 00:06:25,570
This main box just tells you where the quartiles are.

116
00:06:25,570 --> 00:06:29,124
By the way, don&#39;t get the word quartile mixed up with quantile.

117
00:06:29,124 --> 00:06:31,710
There are only four quartiles.

118
00:06:31,710 --> 00:06:33,839
Quartiles sounds like quarter, okay?

119
00:06:33,839 --> 00:06:36,129
But there&#39;s an arbitrary number of quantiles.

120
00:06:38,671 --> 00:06:41,690
And then, the whiskers are more complicated.

121
00:06:41,690 --> 00:06:44,729
So this point here is actually,

122
00:06:44,729 --> 00:06:50,585
you take the inter-quartile range, which is Q3 minus Q1,

123
00:06:50,585 --> 00:06:55,211
you multiply that by 1.5 and you add it to Q3.

124
00:06:55,211 --> 00:07:00,612
Okay, so it&#39;s Q3 + 1.5 times the inter-quartile range,

125
00:07:00,612 --> 00:07:02,620
which is Q3 minus Q1.

126
00:07:03,770 --> 00:07:07,380
And then you go, you find the point,

127
00:07:07,380 --> 00:07:10,430
the data point, the point that&#39;s actually in your data

128
00:07:10,430 --> 00:07:14,400
that is slightly below that, okay?

129
00:07:14,400 --> 00:07:17,240
It&#39;s a very weird set of instructions,

130
00:07:17,240 --> 00:07:18,460
but that&#39;s what it is.

131
00:07:18,460 --> 00:07:21,820
They want these whiskers to be exactly at a data point.

132
00:07:21,820 --> 00:07:25,660
So they have you go up 1.5 times the IQR, and

133
00:07:25,660 --> 00:07:26,860
then down to the nearest data point.

134
00:07:29,610 --> 00:07:32,803
And then this bottom one,

135
00:07:32,803 --> 00:07:38,611
analogously is the data point just above 1.5

136
00:07:38,611 --> 00:07:44,520
times the IQR below the first quartile.

137
00:07:44,520 --> 00:07:46,262
Okay, and then the other, and

138
00:07:46,262 --> 00:07:49,679
any other point outside the whiskers get marked with a plus.

139
00:07:49,679 --> 00:07:51,500
So the five number summary is here.

140
00:07:51,500 --> 00:07:55,442
One, two, three, four, and five.

141
00:07:55,442 --> 00:07:57,980
So you&#39;ve got the min and the max marked.

142
00:07:57,980 --> 00:08:02,500
Also, the first, second, and third quartiles.

143
00:08:02,500 --> 00:08:05,330
And the nice thing about these box plots, as I mentioned, is

144
00:08:05,330 --> 00:08:08,020
that you can visually understand the distribution very quickly.

145
00:08:08,020 --> 00:08:10,960
You don&#39;t need all the little wiggles in the histogram

146
00:08:10,960 --> 00:08:12,430
to understand what&#39;s going on.

147
00:08:12,430 --> 00:08:15,112
And yet, you can still pack several box plots into a figure

148
00:08:15,112 --> 00:08:17,739
to have many of them on the same screen because they&#39;re so

149
00:08:17,739 --> 00:08:18,245
compact.

