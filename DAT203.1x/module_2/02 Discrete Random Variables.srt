0
00:00:04,886 --> 00:00:08,768
Let&#39;s talk about discrete random variables.

1
00:00:08,768 --> 00:00:12,372
Now, the kind of quintessential discreet random

2
00:00:12,372 --> 00:00:15,731
variable is the value on a die when you role it.

3
00:00:15,731 --> 00:00:19,392
Now, the name of the random variable is x,

4
00:00:19,392 --> 00:00:23,173
it&#39;s the value of that die when you roll it.

5
00:00:23,173 --> 00:00:26,317
Now, little x is gonna be an outcome.

6
00:00:26,317 --> 00:00:32,384
So here it&#39;s gonna take values either 1, 2, 3, 4, 5 or 6.

7
00:00:32,384 --> 00:00:37,426
And then the probability that random variable X takes

8
00:00:37,426 --> 00:00:42,030
outcome value little x Is denoted this way okay?

9
00:00:42,030 --> 00:00:44,750
So this is the probability that random variable

10
00:00:44,750 --> 00:00:46,040
X will have outcome little x.

11
00:00:47,060 --> 00:00:48,995
And the probabilities have to sum to 1.

12
00:00:48,995 --> 00:00:54,050
But for the role of a die, the outcomes are equally probable,

13
00:00:54,050 --> 00:00:55,860
they all have probability 1/6.

14
00:00:55,860 --> 00:00:59,168
So make sure you understand the notation here, okay?

15
00:00:59,168 --> 00:01:03,660
So this says that the probability

16
00:01:03,660 --> 00:01:08,750
that random variable x takes outcome 4 is 1/6th.

17
00:01:08,750 --> 00:01:12,650
And this table is called the probability mass function.

18
00:01:12,650 --> 00:01:16,231
It tells your for each outcome what is

19
00:01:16,231 --> 00:01:20,627
the probability of attaining that outcome.

20
00:01:20,627 --> 00:01:24,305
Now, here the probability mass function is constant,

21
00:01:24,305 --> 00:01:25,799
it&#39;s always 1/6.

22
00:01:25,799 --> 00:01:30,250
But there are other random variables where it&#39;s not

23
00:01:30,250 --> 00:01:31,269
constant.

24
00:01:31,269 --> 00:01:36,710
So here, for instance, is a PMF or a very weird die.

25
00:01:36,710 --> 00:01:40,579
This die, instead of having values 1, 2, 3, 4, 5, for

26
00:01:40,579 --> 00:01:44,968
6 on its sides, it has 10,.20, 30, 40, 50, and 60.

27
00:01:44,968 --> 00:01:47,286
And it&#39;s also a weighted die,

28
00:01:47,286 --> 00:01:50,710
where not all the probabilities are 1/6.

29
00:01:50,710 --> 00:01:55,400
So here, one of the probabilities is 3/12 and

30
00:01:55,400 --> 00:01:57,700
another is 1/12.

31
00:01:57,700 --> 00:02:01,588
So just double check for yourself that the probabilities

32
00:02:01,588 --> 00:02:04,509
all add up to 1, and it looks like they do.

33
00:02:04,509 --> 00:02:11,060
Well, okay, so let&#39;s go back and review the general notation.

34
00:02:11,060 --> 00:02:14,807
So the random variable is called capital X, okay, and

35
00:02:14,807 --> 00:02:19,206
the possible outcomes are called little x, and the probability

36
00:02:19,206 --> 00:02:22,640
for outcome little x is denoted like this, okay?

37
00:02:22,640 --> 00:02:28,324
So, this is the probability that random variable X equals

38
00:02:28,324 --> 00:02:34,127
outcome little x, little x3, say, and that equals P3.

39
00:02:34,127 --> 00:02:38,738
Okay, so when I&#39;m talking about a discrete random variable,

40
00:02:38,738 --> 00:02:40,920
I am talking about it&#39;s PMF.

41
00:02:41,920 --> 00:02:46,801
You cannot refer to a discrete random variable without

42
00:02:46,801 --> 00:02:51,680
at least thinking about what its PMF is because that&#39;s

43
00:02:51,680 --> 00:02:55,616
what defines probability in this context.

44
00:02:55,616 --> 00:02:57,512
Okay.

45
00:02:57,512 --> 00:03:01,080
So, armed with this notation, let us talk about how to

46
00:03:01,080 --> 00:03:03,730
summarize a discreet random variable.

47
00:03:05,200 --> 00:03:09,470
Now, it&#39;s important that when I refer to a random variable,

48
00:03:09,470 --> 00:03:12,350
I&#39;m referring to its probability mass function.

49
00:03:13,820 --> 00:03:17,660
Let us talk about how one might summarize a PMF

50
00:03:17,660 --> 00:03:19,910
without having to present the whole thing.

51
00:03:21,040 --> 00:03:22,722
The whole thing could be very big,

52
00:03:22,722 --> 00:03:25,333
it could be very overwhelming, and we just want one or

53
00:03:25,333 --> 00:03:28,010
two numbers that really summarize what it looks like.

54
00:03:31,234 --> 00:03:35,123
Okay, so how are we gonna compute the mean of the PMF,

55
00:03:35,123 --> 00:03:37,544
mean of the random variable, and

56
00:03:37,544 --> 00:03:40,580
how are we gonna compute the spread of it?

57
00:03:43,552 --> 00:03:44,947
So let&#39;s talk about the mean.

58
00:03:44,947 --> 00:03:47,067
The mean is a measure of centrality for

59
00:03:47,067 --> 00:03:49,840
the probability mass function.

60
00:03:49,840 --> 00:03:52,460
What is the middle number of the PMF?

61
00:03:52,460 --> 00:03:58,020
So we&#39;ll use this die&#39;s PMF, which is constant here.

62
00:03:58,020 --> 00:04:00,760
And this is the die that

63
00:04:00,760 --> 00:04:02,599
has the labeled sides 10, 20, 30, 40, 50, 60.

64
00:04:03,930 --> 00:04:06,650
Okay, so what is the average outcome that you should get when

65
00:04:06,650 --> 00:04:09,770
you roll this die over and over again?

66
00:04:09,770 --> 00:04:12,960
And I&#39;m sure you all know the answer, which is that it&#39;s 35,

67
00:04:12,960 --> 00:04:14,580
which is right in the middle there.

68
00:04:16,149 --> 00:04:18,910
And here&#39;s the computation you sorta did in your head in order

69
00:04:18,910 --> 00:04:19,510
to get that.

70
00:04:20,570 --> 00:04:23,240
You multiplied each outcome

71
00:04:23,240 --> 00:04:26,620
by the probability that you get that outcome.

72
00:04:26,620 --> 00:04:27,900
And then you add them all up.

73
00:04:29,960 --> 00:04:32,650
So that&#39;s the computation you did to get to the middle

74
00:04:32,650 --> 00:04:33,190
number there.

75
00:04:34,920 --> 00:04:37,480
So I can write that in general notation this way.

76
00:04:37,480 --> 00:04:41,230
I can say that it&#39;s outcome 1 times the probability of

77
00:04:41,230 --> 00:04:45,530
outcome 1 plus outcome 2 times the probability of outcome 2

78
00:04:45,530 --> 00:04:46,120
and so on.

79
00:04:46,120 --> 00:04:51,230
And then I can write that in summation notation like this.

80
00:04:51,230 --> 00:04:54,953
Okay, it&#39;s the sum over i for outcome i and

81
00:04:54,953 --> 00:04:57,341
probability of outcome i.

82
00:04:57,341 --> 00:05:01,437
Okay, and that&#39;s the formula for the mean of a discrete random

83
00:05:01,437 --> 00:05:06,852
variable Okay, so let&#39;s try this die.

84
00:05:06,852 --> 00:05:08,826
So it&#39;s PMF is slightly different, but

85
00:05:08,826 --> 00:05:10,340
we can just apply the formula.

86
00:05:10,340 --> 00:05:15,460
So it&#39;s each outcome times its probability of occurring,

87
00:05:16,480 --> 00:05:18,650
add them all up, and that is the mean.

88
00:05:19,790 --> 00:05:20,640
Okay, and then here,

89
00:05:20,640 --> 00:05:23,509
when I did that, it was a little bit less than 35.

90
00:05:25,010 --> 00:05:28,120
And you can see why that is by looking at the probability

91
00:05:28,120 --> 00:05:28,880
mass function.

92
00:05:29,885 --> 00:05:33,730
See, we&#39;ve got more mass on the smaller values here, right?

93
00:05:33,730 --> 00:05:39,292
We moved some of the mass down lower, so we get a smaller mean.

94
00:05:41,120 --> 00:05:43,791
Okay, so we&#39;re now likely to choose 20 more often, so

95
00:05:43,791 --> 00:05:45,024
that lowers the average.

96
00:05:47,498 --> 00:05:48,777
Okay, more practice.

97
00:05:48,777 --> 00:05:51,600
Here&#39;s a new random variable, and here is its PMF.

98
00:05:51,600 --> 00:05:53,840
And you can sorta look at that for

99
00:05:53,840 --> 00:05:58,200
a bit, and realize that the values in the middle are more

100
00:05:58,200 --> 00:06:03,210
likely than the values at the extremes, okay?

101
00:06:03,210 --> 00:06:04,530
And you can see that from looking at these

102
00:06:04,530 --> 00:06:05,190
numbers directly.

103
00:06:05,190 --> 00:06:08,260
But see what if there were heck of a lot more numbers,

104
00:06:08,260 --> 00:06:11,432
and what if the table was sort of hundreds of

105
00:06:11,432 --> 00:06:14,040
lines longer, thousands of lines long, then you wouldn&#39;t be

106
00:06:14,040 --> 00:06:16,550
able to look all the numbers and figure out that some of the ones

107
00:06:16,550 --> 00:06:19,860
in the middle are higher than the ones at the extremes.

108
00:06:19,860 --> 00:06:24,610
Okay, so one way to handle that is to actually visualize it.

109
00:06:24,610 --> 00:06:26,590
So I&#39;m gonna do a bar chart of the PMF.

110
00:06:27,830 --> 00:06:31,130
So for outcome zero, I plotted the probability to get zero.

111
00:06:31,130 --> 00:06:36,150
For outcome 1, I plotted the probability to get 1, and so on.

112
00:06:36,150 --> 00:06:38,571
And you can see the nice PMF without having to try and

113
00:06:38,571 --> 00:06:40,601
summarize a table of numbers in your head.

114
00:06:43,359 --> 00:06:45,400
Okay, more practice.

115
00:06:45,400 --> 00:06:50,880
So computing the mean using the formula that I discussed

116
00:06:50,880 --> 00:06:55,450
earlier with you, insanity check, does it look right?

117
00:06:55,450 --> 00:06:57,960
Is 2.45 in the center of the distribution?

118
00:06:59,060 --> 00:07:01,258
Yes, it is, so that looks good.

119
00:07:03,528 --> 00:07:05,734
Okay, so there&#39;s the formula for the mean,

120
00:07:05,734 --> 00:07:07,310
that&#39;s what you just learned.

121
00:07:08,500 --> 00:07:11,670
Now the mean is also called the expectation, by the way, or

122
00:07:11,670 --> 00:07:13,450
the expected value.

123
00:07:13,450 --> 00:07:16,902
And I&#39;m gonna use both terminology and both notations

124
00:07:16,902 --> 00:07:20,729
kind of throughout, so let&#39;s just remember that they stand

125
00:07:20,729 --> 00:07:26,675
for the same thing Okay, so now that

126
00:07:26,675 --> 00:07:30,704
we have a measure of the center of the distribution, let us try

127
00:07:30,704 --> 00:07:34,669
to get some way of measuring the spread of the distribution.

128
00:07:37,160 --> 00:07:39,613
Here&#39;s my random variable.

129
00:07:39,613 --> 00:07:43,560
This is the PMF for my random variable, and I want some

130
00:07:43,560 --> 00:07:47,540
measure of the spread of this thing around the mean.

131
00:07:47,540 --> 00:07:51,878
I wanna know how spread out it is, so let me try a guess.

132
00:07:55,228 --> 00:07:56,960
So here&#39;s my guess.

133
00:07:56,960 --> 00:08:00,160
I take the distance of each outcome xi from the mean,

134
00:08:00,160 --> 00:08:01,700
that&#39;s the first step.

135
00:08:03,220 --> 00:08:06,490
Now, remember that xi could be on either side of the mean, so

136
00:08:06,490 --> 00:08:09,160
I need the absolute value here, right, to compute distance.

137
00:08:11,010 --> 00:08:16,310
And then I&#39;m going to multiply each distance

138
00:08:16,310 --> 00:08:19,890
by how often it occurs, and I will call that the spread.

139
00:08:21,060 --> 00:08:25,620
Okay, so if the distances are very large, fairly often,

140
00:08:25,620 --> 00:08:26,930
then this thing will be large.

141
00:08:30,020 --> 00:08:32,059
Okay, so what do you think of that?

142
00:08:32,058 --> 00:08:33,212
Cool?

143
00:08:34,219 --> 00:08:39,351
Well, so this thing, it&#39;s a good guess but

144
00:08:39,351 --> 00:08:43,945
it&#39;s not quite what I&#39;m looking for.

145
00:08:43,945 --> 00:08:47,040
But it is something that is only a little bit different.

146
00:08:47,040 --> 00:08:49,185
But the intuition for this thing holds for

147
00:08:49,185 --> 00:08:52,133
the real definition of the spread that I&#39;m going to use.

148
00:08:54,503 --> 00:08:56,332
So here&#39;s the real definition.

149
00:08:56,332 --> 00:09:02,027
Instead of computing the mean distance from the mean outcome,

150
00:09:02,027 --> 00:09:06,655
it&#39;s actually the mean squared distance, okay?

151
00:09:06,655 --> 00:09:10,422
So, since you&#39;re still looking at distances from the center of

152
00:09:10,422 --> 00:09:13,910
the distribution this really is the measure of the spread,

153
00:09:13,910 --> 00:09:14,410
right?

154
00:09:14,410 --> 00:09:17,908
This is the official definition of the variance of

155
00:09:17,908 --> 00:09:19,163
a distribution.

156
00:09:19,163 --> 00:09:21,107
Okay, so when you look at this,

157
00:09:21,107 --> 00:09:23,989
what you should see is distances from the mean of

158
00:09:23,989 --> 00:09:28,830
the distribution weighted by their probability of occurring.

159
00:09:28,830 --> 00:09:30,410
And that is the variance.

160
00:09:32,210 --> 00:09:34,170
Let&#39;s do this computation here.

161
00:09:34,170 --> 00:09:35,240
Let&#39;s compute the variance of x.

162
00:09:35,240 --> 00:09:40,770
Here are the outcomes and here are the probabilities.

163
00:09:40,770 --> 00:09:43,790
And here is the formula that we just derived.

164
00:09:45,450 --> 00:09:46,920
Now, lets look at this top line here.

165
00:09:48,150 --> 00:09:55,680
We have probability of 0.03 times the distance of x,

166
00:09:55,680 --> 00:09:59,910
which is zero minus 2.45, which is the mean we computed earlier.

167
00:10:01,160 --> 00:10:04,266
And then we square that, the squared distance from the mean.

168
00:10:06,421 --> 00:10:07,873
Okay, good.

169
00:10:07,873 --> 00:10:11,089
So that&#39;s for the first term, so that&#39;s this term, and

170
00:10:11,089 --> 00:10:13,100
now let&#39;s do the rest of the terms.

171
00:10:15,040 --> 00:10:17,039
Now here&#39;s the second term.

172
00:10:17,039 --> 00:10:19,670
0.14 is the probability of that outcome

173
00:10:20,880 --> 00:10:24,250
times the distance of 1 from 2.45 squared.

174
00:10:26,000 --> 00:10:27,980
And then I just put some dot dot dots just so

175
00:10:27,980 --> 00:10:29,220
we don&#39;t have to write them all out.

176
00:10:29,220 --> 00:10:32,840
And then here is the last term for that line right there.

177
00:10:32,840 --> 00:10:35,570
And you get 1.0675.

178
00:10:38,020 --> 00:10:38,665
Now that&#39;s great.

179
00:10:38,665 --> 00:10:41,190
So we&#39;ve got the variance, but

180
00:10:41,190 --> 00:10:44,310
the problem with the variance is that it&#39;s not in units that

181
00:10:44,310 --> 00:10:48,530
really make any sense, cuz it&#39;s in units of distance squared.

182
00:10:48,530 --> 00:10:51,810
So that&#39;s why we wanna talk about the standard deviation.

183
00:10:51,810 --> 00:10:55,273
Now, the standard deviation is just the square root

184
00:10:55,273 --> 00:10:56,485
of the variance.

185
00:10:56,485 --> 00:10:58,750
Okay, so I put it here.

186
00:10:58,750 --> 00:11:01,840
Standard deviation, it&#39;s also written sigma,

187
00:11:02,840 --> 00:11:04,730
it&#39;s just the square root of the variance.

188
00:11:04,730 --> 00:11:06,810
And that is in units that make sense.

189
00:11:06,810 --> 00:11:11,220
So it&#39;s back in dollars again, not dollars squared.

190
00:11:12,660 --> 00:11:16,010
Okay, so the value here is 1.033 and you can

191
00:11:16,010 --> 00:11:19,740
actually measure that along the horizontal axis here, and it

192
00:11:19,740 --> 00:11:23,410
makes sense because it&#39;s in the same units as the outcomes are.

193
00:11:23,410 --> 00:11:25,346
So if these are in dollars,

194
00:11:25,346 --> 00:11:28,134
the standard deviation is in dollars.

195
00:11:28,134 --> 00:11:30,699
And you can see that by moving away from the mean here,

196
00:11:30,699 --> 00:11:33,276
the mean&#39;s 2.45, which is sort of right there.

197
00:11:33,276 --> 00:11:36,225
And you can see what one standard deviation will get you.

