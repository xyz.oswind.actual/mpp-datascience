0
00:00:04,753 --> 00:00:06,701
Hi, so we&#39;re gonna talk about probability now.

1
00:00:06,701 --> 00:00:10,479
Now probability, I believe, is the most essential skill that

2
00:00:10,479 --> 00:00:13,720
you need to make your case as a data scientist.

3
00:00:13,720 --> 00:00:16,250
You don&#39;t need a ton of advance probability theory,

4
00:00:16,250 --> 00:00:19,710
you just need a really solid grasp of the basics.

5
00:00:19,710 --> 00:00:20,961
And that&#39;s what I&#39;m gonna give you here.

6
00:00:20,961 --> 00:00:23,624
So this is gonna allow you to ask really solid,

7
00:00:23,624 --> 00:00:25,481
maybe even pointed questions.

8
00:00:25,481 --> 00:00:30,007
Things like, in this context, probability is not precisely

9
00:00:30,007 --> 00:00:34,444
defined, but it should be, or are you sure this isn&#39;t a case

10
00:00:34,444 --> 00:00:37,941
of Simpson&#39;s paradox, questions like that.

11
00:00:37,941 --> 00:00:41,748
Now, I want to start by discussing the most common

12
00:00:41,748 --> 00:00:47,280
mistakes that I see among people communicating with data.

13
00:00:47,280 --> 00:00:51,190
So first, they discuss the notion of probability without

14
00:00:51,190 --> 00:00:54,520
actually defining what it means in that context.

15
00:00:54,520 --> 00:00:57,170
You have to define what a random variable means,

16
00:00:57,170 --> 00:01:00,690
in order to talk about probabilities.

17
00:01:00,690 --> 00:01:04,020
Now the word probability by itself is sort of meaningless.

18
00:01:04,019 --> 00:01:05,590
Now, correlation is not causation,

19
00:01:05,590 --> 00:01:06,920
they are quite different.

20
00:01:08,170 --> 00:01:11,348
Now, assuming that because a hypothesis test failed,

21
00:01:11,348 --> 00:01:15,190
people often assume that the null hypothesis must be true.

22
00:01:16,225 --> 00:01:18,890
Now hypothesis testing should not be done by people who don&#39;t

23
00:01:18,890 --> 00:01:20,740
know what hypothesis testing means.

24
00:01:20,740 --> 00:01:23,510
And we&#39;re gonna make sure that you&#39;re not one of those people.

25
00:01:24,800 --> 00:01:27,530
And actually if you goof up any of those things,

26
00:01:27,530 --> 00:01:29,222
people are not going to take you seriously.

27
00:01:29,222 --> 00:01:30,330
They&#39;re gonna think that you don&#39;t know

28
00:01:30,330 --> 00:01:31,800
what you&#39;re talking about.

29
00:01:31,800 --> 00:01:34,530
And so this happens all the time,

30
00:01:34,530 --> 00:01:37,490
because data scientists who are often computer scientists

31
00:01:37,490 --> 00:01:39,458
don&#39;t actually have the training in statistics that they need.

32
00:01:39,458 --> 00:01:42,227
So I&#39;m gonna start this lecture by making sure that you

33
00:01:42,227 --> 00:01:43,921
know the basics of probability.

34
00:01:43,921 --> 00:01:46,141
I&#39;m gonna start with random variables.

35
00:01:48,661 --> 00:01:50,541
Okay, so what&#39;s a random variable?

36
00:01:50,541 --> 00:01:54,468
A random variable assigns a numerical value to each possible

37
00:01:54,468 --> 00:01:57,520
outcome of a random experiment.

38
00:01:57,520 --> 00:02:00,620
Again, it&#39;s something whose value depends on chance.

39
00:02:00,620 --> 00:02:03,590
It has to have a numerical value.

40
00:02:03,590 --> 00:02:09,130
The color of a car chosen at random is not a random variable,

41
00:02:09,130 --> 00:02:13,680
but it would be if you assigned a number to each color that

42
00:02:13,680 --> 00:02:14,760
you&#39;re talking about.

43
00:02:16,790 --> 00:02:18,620
So are the following random variables?

44
00:02:20,020 --> 00:02:20,550
Today&#39;s weather.

45
00:02:22,980 --> 00:02:23,480
No, it&#39;s not.

46
00:02:24,878 --> 00:02:28,170
But if you said, the number of millimeters of rainfall tomorrow

47
00:02:28,170 --> 00:02:31,224
in Redmond, that would be a random variable.

48
00:02:31,224 --> 00:02:34,480
Right, if the number of inches of rainfall

49
00:02:34,480 --> 00:02:37,030
over a certain time and place, that is a number.

50
00:02:38,990 --> 00:02:41,250
The color of a car chosen at random.

51
00:02:41,250 --> 00:02:45,440
That is not a random variable, color is not a number.

52
00:02:45,440 --> 00:02:48,751
But if you assigned 1, if the next car we see is blue,

53
00:02:48,751 --> 00:02:52,355
2 if it&#39;s green, 4 if it&#39;s black, then that would be,

54
00:02:52,355 --> 00:02:53,921
cuz that&#39;s numerical.

55
00:02:53,921 --> 00:02:56,812
The result of a coin flip, heads or tails.

56
00:02:56,812 --> 00:02:59,194
No, that&#39;s not a random variable but if you assign one for

57
00:02:59,194 --> 00:03:00,952
heads and two for tails, then it would be.

58
00:03:00,952 --> 00:03:03,572
The price of Microsoft stock.

59
00:03:03,572 --> 00:03:06,858
Not yesterday&#39;s price, cuz that&#39;s not random, but

60
00:03:06,858 --> 00:03:08,152
tomorrow&#39;s price.

61
00:03:08,152 --> 00:03:10,374
That would be a random variable.

62
00:03:10,374 --> 00:03:14,132
The number of laps between yellow flags in an F1 race.

63
00:03:14,132 --> 00:03:16,434
That&#39;s another example of a random variable,

64
00:03:16,434 --> 00:03:17,360
that&#39;s a number.

65
00:03:19,740 --> 00:03:22,130
Now, random variables come in two flavors, discrete and

66
00:03:22,130 --> 00:03:22,859
continuous.

67
00:03:24,610 --> 00:03:27,360
A discrete random variable has a number of

68
00:03:27,360 --> 00:03:28,880
outcomes that you could count.

69
00:03:30,030 --> 00:03:34,620
So like truffle, number of truffles in a box for instance.

70
00:03:34,620 --> 00:03:37,348
I assume that the machines that put the truffles in a box

71
00:03:37,348 --> 00:03:40,195
aren&#39;t, they&#39;re not so consistent that you always get

72
00:03:40,195 --> 00:03:42,462
exactly the same number of truffles in a box.

73
00:03:42,462 --> 00:03:46,962
Truffles, Cheerios, peanuts, you can count those, so that would

74
00:03:46,962 --> 00:03:51,162
be a discrete random variable, the number of truffles in a box.

75
00:03:51,162 --> 00:03:55,839
Now continuous random variables are different in that you

76
00:03:55,839 --> 00:03:59,658
can&#39;t count the number of possible outcomes,

77
00:03:59,658 --> 00:04:04,390
like you can&#39;t count the amount of ice cream here.

78
00:04:04,390 --> 00:04:07,100
Yeah, you can have one cup of ice cream, or

79
00:04:07,100 --> 00:04:08,650
two cups of ice cream.

80
00:04:08,650 --> 00:04:11,260
But you can have anything in between, and you can&#39;t count

81
00:04:11,260 --> 00:04:14,890
the possibilities because that would take on any real value.

82
00:04:16,290 --> 00:04:18,710
Now, in case you were wondering,

83
00:04:18,709 --> 00:04:22,420
this is actually a raisin on top of that ice cream.

84
00:04:22,420 --> 00:04:24,770
I got compote on top of the ice cream.

85
00:04:24,770 --> 00:04:26,670
So the number of raisins there would be discrete,

86
00:04:26,670 --> 00:04:29,450
but the amount of ice cream would be continuous.

87
00:04:30,510 --> 00:04:33,538
So we&#39;re gonna talk first about discrete random variables and

88
00:04:33,538 --> 00:04:35,602
then we&#39;re gonna talk about continuous.

