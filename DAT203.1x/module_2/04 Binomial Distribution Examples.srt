0
00:00:05,033 --> 00:00:08,874
So let&#39;s do some examples of the binomial distribution.

1
00:00:08,874 --> 00:00:13,487
All right, so let&#39;s say that we flip 20 coins at random and

2
00:00:13,487 --> 00:00:17,382
each coin has a probability .5 of landing heads.

3
00:00:17,382 --> 00:00:22,045
These are all fair coins and then a random variable is gonna

4
00:00:22,045 --> 00:00:26,243
be the number of them that land heads out of the 20.

5
00:00:26,243 --> 00:00:31,115
Now I wanna know what the probability

6
00:00:31,115 --> 00:00:35,207
is that ten of them land heads.

7
00:00:35,207 --> 00:00:38,022
And here&#39;s the formula, but the formula for

8
00:00:38,022 --> 00:00:39,879
the binomial distribution.

9
00:00:39,879 --> 00:00:44,121
That is the key to answering this problem cuz all I have to

10
00:00:44,121 --> 00:00:47,649
do is plug in n, p, and x and then I&#39;m all set.

11
00:00:47,649 --> 00:00:50,225
Okay, so what is x?

12
00:00:50,225 --> 00:00:52,000
It&#39;s ten.

13
00:00:52,000 --> 00:00:53,106
What is n?

14
00:00:53,106 --> 00:00:58,088
That&#39;s the number of per newly trials which is 20 cuz

15
00:00:58,088 --> 00:01:03,179
we&#39;re flipping 20 coins and then p is the probability

16
00:01:03,179 --> 00:01:08,394
of success of each per newly trial and they&#39;re all .5.

17
00:01:08,394 --> 00:01:13,255
If I plug all those numbers in, then I get 0.1762.

18
00:01:13,255 --> 00:01:14,650
And that&#39;s happy.

19
00:01:14,650 --> 00:01:18,176
That&#39;s the answer.

20
00:01:18,176 --> 00:01:21,381
Okay so now, the question then is, okay,

21
00:01:21,381 --> 00:01:23,617
I&#39;m changing the question.

22
00:01:23,617 --> 00:01:26,780
Instead of what&#39;s the probability that x equals ten,

23
00:01:26,780 --> 00:01:30,078
I&#39;m going to ask you what the probability is that x is less

24
00:01:30,078 --> 00:01:31,330
than or equal to ten.

25
00:01:31,330 --> 00:01:35,714
Okay, so in order to get that, then x could be either zero,

26
00:01:35,714 --> 00:01:38,803
one, two, three, all the way up to ten.

27
00:01:38,803 --> 00:01:44,173
I add all those probabilities up and the answer is .5881.

28
00:01:44,173 --> 00:01:47,416
Do you believe this number?

29
00:01:47,416 --> 00:01:51,736
Well if you think about it, I&#39;m asking you what

30
00:01:51,736 --> 00:01:56,488
the probability is that out of the 20 coins I flip,

31
00:01:56,488 --> 00:02:00,926
then less than or equal to 10 of them are heads.

32
00:02:00,926 --> 00:02:02,630
And you should think to yourself, yeah,

33
00:02:02,630 --> 00:02:04,392
that&#39;s about half, that&#39;s about half.

34
00:02:04,392 --> 00:02:09,688
So good, we&#39;ve gotten .5881, so that looks good.

35
00:02:09,687 --> 00:02:14,439
A little bit more than half, okay.

36
00:02:14,439 --> 00:02:20,086
Now you know how to compute the answer to these kinds

37
00:02:20,086 --> 00:02:27,060
of problems and what I&#39;d like to do then is plot these answers.

38
00:02:27,060 --> 00:02:30,384
But if I ask you what&#39;s the probability that random variable

39
00:02:30,384 --> 00:02:33,773
x equals some outcome little x for any x between zero and 20,

40
00:02:33,773 --> 00:02:37,031
you could give me a whole table of numbers or you could give me

41
00:02:37,031 --> 00:02:40,061
a nice plot and this is what the plot looks like, okay?

42
00:02:40,061 --> 00:02:44,667
So zero to 20 and you can see that you&#39;re more likely to get

43
00:02:44,667 --> 00:02:47,339
ten heads than to get zero heads and

44
00:02:47,339 --> 00:02:51,500
that makes sense because these coins are fair coins.

45
00:02:52,760 --> 00:02:56,381
Now this is a nice beautiful plot of the binomial probability

46
00:02:56,381 --> 00:02:59,789
mass function and you should remember it looks like a bump

47
00:02:59,789 --> 00:03:02,639
because you&#39;ll need that information later.

48
00:03:02,639 --> 00:03:06,107
And what&#39;s the mean, the mean, the middle value?

49
00:03:06,107 --> 00:03:08,236
It&#39;s n times p.

50
00:03:08,236 --> 00:03:10,924
Whatever n is, the number of trials,

51
00:03:10,924 --> 00:03:15,628
p is the probability of success for each trial, that&#39;s the mean.

52
00:03:15,628 --> 00:03:19,747
And the variance again is np times 1 minus p.

53
00:03:19,747 --> 00:03:21,031
And obviously,

54
00:03:21,031 --> 00:03:25,626
the standard deviation is the square root of the variance.

55
00:03:25,626 --> 00:03:28,673
So let&#39;s do an example that uses Roulette.

56
00:03:28,673 --> 00:03:30,945
Roulette is one of the most popular casino games.

57
00:03:30,945 --> 00:03:32,079
It&#39;s very, very glamorous.

58
00:03:32,079 --> 00:03:34,079
You got the spinning wheel,

59
00:03:34,079 --> 00:03:38,412
and it&#39;s all based on the idea of independent, random trials

60
00:03:38,412 --> 00:03:42,328
because you assume that every time you spin that wheel,

61
00:03:42,328 --> 00:03:46,687
it&#39;s totally separate from anything that happened before.

62
00:03:46,687 --> 00:03:50,852
Now the ball rolls around on the wheel, and

63
00:03:50,852 --> 00:03:53,744
it lands in one of the slots and

64
00:03:53,744 --> 00:03:59,427
it lands in either a green slot or a red slot or a black slot.

65
00:03:59,427 --> 00:04:01,203
And if you&#39;re playing roulette,

66
00:04:01,203 --> 00:04:03,844
you can choose to either bet on a number or a colour.

67
00:04:03,844 --> 00:04:07,397
So let&#39;s say here that we&#39;re gonna bet on black,

68
00:04:07,397 --> 00:04:10,019
meaning that when we spin the wheel,

69
00:04:10,019 --> 00:04:12,657
the little ball lands in a black bin.

70
00:04:12,657 --> 00:04:14,596
Okay so actually believe it or

71
00:04:14,596 --> 00:04:18,084
not, gamblers use the expected value to tell them how

72
00:04:18,084 --> 00:04:21,042
well they&#39;ll do over a series of fixed bets.

73
00:04:21,041 --> 00:04:25,095
So we&#39;re gonna talk about betting on black, okay?

74
00:04:25,095 --> 00:04:26,131
So here&#39;s the question.

75
00:04:26,131 --> 00:04:30,135
If I bet on black ten times, so I&#39;m going to roll that wheel ten

76
00:04:30,135 --> 00:04:33,985
times and bet on black all ten times, what is the chance that

77
00:04:33,985 --> 00:04:37,469
I&#39;m going to win at least four out of those ten times?

78
00:04:40,030 --> 00:04:41,156
So how do you calculate that?

79
00:04:41,156 --> 00:04:44,578
I&#39;m not gonna tell you the answer, but

80
00:04:44,578 --> 00:04:47,073
I&#39;ll tell you how to get it.

81
00:04:47,073 --> 00:04:49,966
I&#39;m gonna use exactly the formula for

82
00:04:49,966 --> 00:04:53,408
the binomial distribution that we derived.

83
00:04:53,408 --> 00:04:57,965
Now the probability that x is at least four,

84
00:04:57,965 --> 00:05:02,532
okay, x again is the number of times we win.

85
00:05:02,532 --> 00:05:05,430
The probability it&#39;s at least four.

86
00:05:05,430 --> 00:05:09,233
I have to sum up over winning four times, five times,

87
00:05:09,233 --> 00:05:14,065
six times, seven times, eight times, nine times, or ten times.

88
00:05:14,065 --> 00:05:18,142
The probability that I will get exactly that outcome,

89
00:05:18,142 --> 00:05:22,131
and then the probability to get the outcome is given by

90
00:05:22,131 --> 00:05:25,513
the formula for the binomial distribution.

91
00:05:25,513 --> 00:05:27,057
What is n?

92
00:05:27,057 --> 00:05:31,998
It&#39;s the number of trials, which is ten.

93
00:05:31,998 --> 00:05:34,658
What is the probability of success?

94
00:05:34,658 --> 00:05:41,565
Well, there are 18 black slots and there are 38 total slots.

95
00:05:41,565 --> 00:05:44,912
So the probability of success is 18 out of 38.

96
00:05:44,912 --> 00:05:48,415
So, I chose not to give you the answer to that problem because

97
00:05:48,415 --> 00:05:51,645
I&#39;m worried that there are some of you who actually like

98
00:05:51,645 --> 00:05:54,895
gambling and this is definitely going to ruin your fun.

99
00:05:54,895 --> 00:05:57,180
If you know a lot about probability,

100
00:05:57,180 --> 00:06:00,584
you&#39;re probably not gonna want to gamble that often.

