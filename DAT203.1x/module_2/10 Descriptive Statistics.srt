0
00:00:05,282 --> 00:00:07,663
Let&#39;s talk about some basic descriptive statistics and

1
00:00:07,663 --> 00:00:09,710
visualization techniques.

2
00:00:09,710 --> 00:00:14,130
Now, the most useful command that I find

3
00:00:14,130 --> 00:00:19,420
in the entire world of statistics is the histogram.

4
00:00:19,420 --> 00:00:22,330
It&#39;s a single command that I use

5
00:00:22,330 --> 00:00:24,900
the most often out of every command.

6
00:00:24,900 --> 00:00:28,480
The histogram, if you have a collection of values here,

7
00:00:28,480 --> 00:00:31,260
you can do a histogram of those values.

8
00:00:31,260 --> 00:00:33,110
And then you get something that looks like this.

9
00:00:33,110 --> 00:00:36,110
And this approximates the probability distribution

10
00:00:36,110 --> 00:00:38,280
function of random variable.

11
00:00:38,280 --> 00:00:40,450
So, if you have a pile of numbers, in my view,

12
00:00:40,450 --> 00:00:43,630
the first thing you should do is look at it.

13
00:00:43,630 --> 00:00:47,160
And you can do this in one line of code in almost any piece of

14
00:00:47,160 --> 00:00:48,210
software.

15
00:00:48,210 --> 00:00:49,851
And it&#39;ll create equal sized bins.

16
00:00:49,851 --> 00:00:51,702
And it&#39;ll plop your data into them.

17
00:00:51,702 --> 00:00:53,960
And it tells you how many points are in each bin.

18
00:00:53,960 --> 00:00:55,431
Boom, that&#39;s a histogram.

19
00:00:55,431 --> 00:00:58,517
So this is telling you, for instance,

20
00:00:58,517 --> 00:01:02,631
that there are 23 numbers in your data set, here,

21
00:01:02,631 --> 00:01:06,205
between 6.86 and 6.95 or whatever.

22
00:01:06,205 --> 00:01:09,190
Now, another plotting function that I use pretty often is a bar

23
00:01:09,190 --> 00:01:12,210
chart, which is useful for categorical data.

24
00:01:12,210 --> 00:01:15,080
Okay, so let&#39;s say that for each person, we know how they get

25
00:01:15,080 --> 00:01:17,779
to work, whether it&#39;s bike, train, car, whatever, bus.

26
00:01:19,015 --> 00:01:20,000
And you can just plot

27
00:01:20,000 --> 00:01:21,930
the probability of each one of those categories.

28
00:01:23,180 --> 00:01:27,470
Now, a Pareto chart is exactly a bar chart except that all of

29
00:01:27,470 --> 00:01:32,710
the categories are ordered by frequency, decreasing frequency.

30
00:01:32,710 --> 00:01:37,435
Okay, so these plots are how you as a data scientist is

31
00:01:37,435 --> 00:01:40,061
gonna tell a story with data.

32
00:01:40,061 --> 00:01:43,161
So these are the building blocks of your story, the words,

33
00:01:43,161 --> 00:01:44,700
if you like.

34
00:01:44,700 --> 00:01:47,930
Now scatter plots are for when you have two variables,

35
00:01:47,930 --> 00:01:52,360
say your advertising budget and then the amount of sales.

36
00:01:52,360 --> 00:01:54,561
And you can plot them against each other here.

37
00:01:54,561 --> 00:01:57,807
So you can see as the advertising budget increases,

38
00:01:57,807 --> 00:02:00,030
the sales do, too.

39
00:02:00,030 --> 00:02:03,581
And then the very first point here, which is an advertising

40
00:02:03,581 --> 00:02:07,501
budget of 40 and sales of 43, is just that point right there.

