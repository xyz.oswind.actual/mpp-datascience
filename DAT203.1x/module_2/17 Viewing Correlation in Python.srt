0
00:00:00,000 --> 00:00:03,360
Cynthia has been discussing correlations

1
00:00:03,360 --> 00:00:06,089
and how they&#39;re the formulas for how

2
00:00:06,089 --> 00:00:09,269
they&#39;re computed, and some information

3
00:00:09,269 --> 00:00:12,630
on how they are interpreted. In this demo

4
00:00:12,630 --> 00:00:14,190
i&#39;d like to show you how we&#39;re going to

5
00:00:14,190 --> 00:00:17,010
use some tools in Python to compute some

6
00:00:17,010 --> 00:00:19,500
correlations and we&#39;ll talk about what

7
00:00:19,500 --> 00:00:22,000
those results mean

8
00:00:22,000 --> 00:00:25,119
so my screen here I have the same

9
00:00:25,119 --> 00:00:28,240
notebook we started in the previous demo

10
00:00:28,240 --> 00:00:29,529
where we looked at some summary

11
00:00:29,529 --> 00:00:33,820
statistics for price and engine size and

12
00:00:33,820 --> 00:00:36,010
I have this function here where I can

13
00:00:36,010 --> 00:00:37,870
now

14
00:00:37,870 --> 00:00:39,460
first off I look at the relationship

15
00:00:39,460 --> 00:00:43,230
between two variables

16
00:00:44,020 --> 00:00:45,820
we&#39;re just going to make a scatter plot

17
00:00:45,820 --> 00:00:48,040
of those and then we&#39;re going to compute

18
00:00:48,040 --> 00:00:52,240
the correlation and the covariance using

19
00:00:52,240 --> 00:00:53,110
this

20
00:00:53,110 --> 00:00:56,500
not too surprisingly the CORR method

21
00:00:56,500 --> 00:01:00,580
and the covariance method COV. One little

22
00:01:00,580 --> 00:01:02,980
trick those are numpy methods so you

23
00:01:02,980 --> 00:01:05,950
have to always make sure you convert to

24
00:01:05,950 --> 00:01:10,330
a matrix any values you&#39;re feeding to

25
00:01:10,330 --> 00:01:13,520
those functions or else

26
00:01:13,520 --> 00:01:16,250
they have things are going to happen so

27
00:01:16,250 --> 00:01:20,000
anyway not too complicated but let me

28
00:01:20,000 --> 00:01:23,679
just run it and we&#39;ll see what happens

29
00:01:24,280 --> 00:01:26,860
alright so first off let&#39;s look at the

30
00:01:26,860 --> 00:01:29,770
covariance it&#39;s a little hard for me to

31
00:01:29,770 --> 00:01:32,110
interpret that number you know it&#39;s it&#39;s

32
00:01:32,110 --> 00:01:35,890
about 30,000 but we&#39;re computing that

33
00:01:35,890 --> 00:01:39,610
based on units of price of the

34
00:01:39,610 --> 00:01:43,420
automobile and engine size which is the

35
00:01:43,420 --> 00:01:45,430
engine size and it turns out cubic

36
00:01:45,430 --> 00:01:48,909
inches of this data set so it&#39;s not

37
00:01:48,909 --> 00:01:52,060
clear to me just thinking about that is

38
00:01:52,060 --> 00:01:57,130
29,000 or 20 or 30,000 high low or what

39
00:01:57,130 --> 00:01:59,470
but here&#39;s correlation which is the

40
00:01:59,470 --> 00:02:01,899
normalized version of covariance right

41
00:02:01,899 --> 00:02:04,090
we&#39;ve normalized by the variance of both

42
00:02:04,090 --> 00:02:09,340
of engine size and price and it&#39;s almost

43
00:02:09,340 --> 00:02:11,200
point nine so that indicates to me a

44
00:02:11,200 --> 00:02:15,160
fairly high positive correlation between

45
00:02:15,160 --> 00:02:18,670
those two variables and if i look at my

46
00:02:18,670 --> 00:02:21,240
scatter plot here

47
00:02:21,240 --> 00:02:23,160
it does look like there&#39;s this pretty

48
00:02:23,160 --> 00:02:25,530
good relationship between those two

49
00:02:25,530 --> 00:02:27,330
variables we have engine size on the

50
00:02:27,330 --> 00:02:30,450
vertical axis we have price of the

51
00:02:30,450 --> 00:02:33,480
automobile on the horizontal axis and

52
00:02:33,480 --> 00:02:36,570
you can see there&#39;s a pretty straight

53
00:02:36,570 --> 00:02:40,440
line relationship there for the most

54
00:02:40,440 --> 00:02:43,680
part is not exactly but for the most

55
00:02:43,680 --> 00:02:47,490
part and so you can say these variables

56
00:02:47,490 --> 00:02:49,320
are reason have a reasonably strong

57
00:02:49,320 --> 00:02:52,130
positive correlation

58
00:02:52,880 --> 00:02:55,490
but let&#39;s try another set let&#39;s try this

59
00:02:55,490 --> 00:02:58,910
time City miles per gallon and price of

60
00:02:58,910 --> 00:03:02,380
the automobile so i&#39;m going to run that

61
00:03:03,420 --> 00:03:06,150
and it&#39;s okay so my covariance now is

62
00:03:06,150 --> 00:03:10,440
negative and it&#39;s a bit larger then what

63
00:03:10,440 --> 00:03:12,330
we had before but I&#39;m not sure whether

64
00:03:12,330 --> 00:03:14,280
larger really means much but it

65
00:03:14,280 --> 00:03:16,410
definitely negative

66
00:03:16,410 --> 00:03:18,480
if i look at my correlation again it&#39;s

67
00:03:18,480 --> 00:03:22,650
negative but it&#39;s there&#39;s less court

68
00:03:22,650 --> 00:03:25,110
that the magnitude is less than what we

69
00:03:25,110 --> 00:03:30,120
saw with engine size so it&#39;s now about

70
00:03:30,120 --> 00:03:32,100
points7 and if we look at the

71
00:03:32,100 --> 00:03:35,340
scatterplot we see why there is a pretty

72
00:03:35,340 --> 00:03:38,010
strong relationship here but it&#39;s it&#39;s

73
00:03:38,010 --> 00:03:40,500
it&#39;s not anything like a straight line

74
00:03:40,500 --> 00:03:42,840
it&#39;s definitely some sort of curve

75
00:03:42,840 --> 00:03:48,210
um so there&#39;s a less direct relationship

76
00:03:48,210 --> 00:03:51,030
between City miles per gallon price if

77
00:03:51,030 --> 00:03:52,110
you think about this

78
00:03:52,110 --> 00:03:53,640
both of these numbers make sense

79
00:03:53,640 --> 00:03:58,680
correlation of engine size with price

80
00:03:58,680 --> 00:04:00,960
cars with big engines tend to be big

81
00:04:00,960 --> 00:04:04,080
cars tend to be more expensive small

82
00:04:04,080 --> 00:04:07,020
cars that get high

83
00:04:07,020 --> 00:04:09,510
fuel efficiency

84
00:04:09,510 --> 00:04:12,780
like up here tend to be are cheap cars

85
00:04:12,780 --> 00:04:16,049
and big expensive cars tend to have low

86
00:04:16,048 --> 00:04:19,070
fuel efficiency

87
00:04:19,339 --> 00:04:21,620
so I hope this little demo has given you

88
00:04:21,620 --> 00:04:24,620
some idea of how to think about giving

89
00:04:24,620 --> 00:04:28,040
you a feel for the practical uses and

90
00:04:28,040 --> 00:04:33,190
limitations of covariance in correlation

