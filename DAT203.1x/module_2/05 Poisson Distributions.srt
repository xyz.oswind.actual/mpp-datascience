0
00:00:05,052 --> 00:00:07,910
Let&#39;s talk about the Poisson distribution.

1
00:00:07,910 --> 00:00:11,420
The Poisson distribution is a limiting version

2
00:00:11,420 --> 00:00:13,830
of the Binomial distribution.

3
00:00:13,830 --> 00:00:16,360
When the number of trials gets really large but

4
00:00:17,460 --> 00:00:21,430
the probability of success gets smaller at the same rate

5
00:00:22,670 --> 00:00:25,770
that np essentially becomes this value lambda.

6
00:00:27,420 --> 00:00:32,920
So you&#39;re no longer gonna have an n and p in the formula for

7
00:00:32,920 --> 00:00:34,970
the pmf, you&#39;re just gonna have that lambda in there.

8
00:00:36,540 --> 00:00:40,730
So we would write the X as a Poisson random variable,

9
00:00:40,730 --> 00:00:41,530
with parameter lambda,

10
00:00:41,530 --> 00:00:45,750
and then the formula for the PMF looks like this.

11
00:00:46,770 --> 00:00:49,302
So as long as I know lambda, I have the PMF and

12
00:00:49,302 --> 00:00:51,360
then I can compute probabilities.

13
00:00:53,000 --> 00:00:56,070
I&#39;m going to tell you some useful facts about the Poisson

14
00:00:56,070 --> 00:01:01,678
distribution, namely that its mean is exactly lambda.

15
00:01:01,678 --> 00:01:04,290
And its variance is also exactly lambda,

16
00:01:04,290 --> 00:01:06,810
which I think is really cool because it helps me remember it.

17
00:01:08,020 --> 00:01:11,542
Now whenever I think of the Poisson distribution I think of,

18
00:01:11,542 --> 00:01:14,729
I&#39;ll be honest, I think of getting kicked by a horse.

19
00:01:14,729 --> 00:01:19,324
Because in 1898 this was like the kind of key example

20
00:01:19,324 --> 00:01:23,822
of the usage of the Poisson distribution because, and

21
00:01:23,822 --> 00:01:28,807
it was used to model the number of soldiers that were kicked by

22
00:01:28,807 --> 00:01:31,196
horses in the Prussian War.

23
00:01:31,196 --> 00:01:34,440
Now the Poisson distribution is useful for

24
00:01:34,440 --> 00:01:37,560
modeling rare events, I&#39;m very glad to hear that it&#39;s

25
00:01:37,560 --> 00:01:39,440
rare to be kicked by a horse in the Prussian War.

26
00:01:41,070 --> 00:01:44,610
Now here&#39;s the PMF, this is the PMF for

27
00:01:44,610 --> 00:01:46,190
the Poisson distribution.

28
00:01:46,190 --> 00:01:48,810
And you should think to yourself that this looks familiar,

29
00:01:48,810 --> 00:01:51,770
because it looks very similar to the binomial distribution.

30
00:01:51,770 --> 00:01:53,934
It looks kind of like a shifted and

31
00:01:53,934 --> 00:01:57,258
squished version of the binomial, and it should,

32
00:01:57,258 --> 00:02:00,682
given that it is a limiting version of the binomial.

33
00:02:00,682 --> 00:02:04,365
Now it&#39;s useful for modeling waiting times for

34
00:02:04,365 --> 00:02:06,790
independent events.

35
00:02:06,790 --> 00:02:10,810
You could think about n is being like little time intervals, and

36
00:02:10,810 --> 00:02:13,670
p is being the probability of success in each

37
00:02:13,670 --> 00:02:15,030
little time interval.

38
00:02:15,030 --> 00:02:18,907
And so as the interval gets smaller there are a lot of them

39
00:02:18,907 --> 00:02:22,549
and the probabilities get smaller at the same rate.

40
00:02:22,549 --> 00:02:26,201
Now, let me give you a sort of a more modern example of

41
00:02:26,201 --> 00:02:30,269
the Poisson Distribution than getting kicked by a horse,

42
00:02:30,269 --> 00:02:34,680
which is the number of emails that I get per hour.

43
00:02:34,680 --> 00:02:39,110
So, I get about seven emails per hour.

44
00:02:39,110 --> 00:02:41,860
So, I&#39;ll be completely honest I get much, much, much,

45
00:02:41,860 --> 00:02:43,760
more than seven emails per hour.

46
00:02:43,760 --> 00:02:45,010
But seven is the number of

47
00:02:45,010 --> 00:02:46,590
emails that I actually have to respond to.

48
00:02:47,680 --> 00:02:52,710
So what I did was I kept track of how many emails that I get

49
00:02:52,710 --> 00:02:56,930
every hour for a whole week.

50
00:02:56,930 --> 00:03:00,160
So this is just the number of emails that require a response.

51
00:03:03,000 --> 00:03:05,580
And you&#39;re thinking I still couldn&#39;t possibly

52
00:03:05,580 --> 00:03:08,370
answer that many emails, and you&#39;re right but I do my best.

53
00:03:09,700 --> 00:03:13,440
So for each hour, I counted the number of emails I got in that

54
00:03:13,440 --> 00:03:18,830
hour and I tallied them all up, and it looks like this.

55
00:03:18,830 --> 00:03:23,520
So what that means is that there were seven hours in which

56
00:03:23,520 --> 00:03:27,340
I got eight emails in that hour.

57
00:03:29,230 --> 00:03:33,097
And the cool thing about this, is it looks very much like

58
00:03:33,097 --> 00:03:37,415
the Poisson distribution with lambda = 7, which is great.

59
00:03:37,415 --> 00:03:41,706
So that&#39;s a really good example you can remember of a Poisson

60
00:03:41,706 --> 00:03:46,179
distribution, and that&#39;s our introduction to the Poisson.

