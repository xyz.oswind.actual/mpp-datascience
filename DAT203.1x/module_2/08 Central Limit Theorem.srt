0
00:00:04,730 --> 00:00:08,286
Well, let me tell you about the central limit theorem and

1
00:00:08,286 --> 00:00:10,000
the normal distribution.

2
00:00:11,700 --> 00:00:17,240
Now, everyday a bunch of customers go into a store,

3
00:00:17,240 --> 00:00:19,980
each with their own kooky distribution

4
00:00:19,980 --> 00:00:21,380
of how much they wanna spend.

5
00:00:22,540 --> 00:00:26,072
Like, this guy wants to spend, sometimes a little bit,

6
00:00:26,072 --> 00:00:28,730
sometimes a lot, but not much in between.

7
00:00:28,730 --> 00:00:33,152
This person spends a little bit, a medium amount, or

8
00:00:33,152 --> 00:00:35,810
a lot, sometimes an awful lot.

9
00:00:35,810 --> 00:00:39,480
And this guy, he just likes ties.

10
00:00:39,480 --> 00:00:41,374
So he always buys ties and there&#39;s always this price.

11
00:00:41,374 --> 00:00:44,794
Somewhere in this price range.

12
00:00:44,794 --> 00:00:47,720
Now, the question is, at the end of the day,

13
00:00:47,720 --> 00:00:50,723
the store counts the total amount of money that

14
00:00:50,723 --> 00:00:53,500
they collect from all of these customers.

15
00:00:54,540 --> 00:00:57,040
So how much total do they spend at the store?

16
00:00:58,220 --> 00:01:00,840
And let&#39;s assume we have a lot of customers in the store.

17
00:01:00,840 --> 00:01:04,280
And we even have some students taking this course going

18
00:01:04,280 --> 00:01:07,300
to the store, and bringing their computers to the store just for

19
00:01:07,300 --> 00:01:10,340
fun, while they&#39;re learning data science.

20
00:01:10,340 --> 00:01:12,068
And each of them has their own distribution

21
00:01:12,068 --> 00:01:13,030
of how much they spend.

22
00:01:14,070 --> 00:01:18,980
Every evening, the store counts their total profit, and every

23
00:01:18,980 --> 00:01:22,270
day it was slightly different than it was the day before.

24
00:01:23,570 --> 00:01:24,960
And now we ask the store to

25
00:01:26,000 --> 00:01:27,810
plot the distribution of it&#39;s profits.

26
00:01:29,310 --> 00:01:31,011
Here&#39;s what it looks like.

27
00:01:31,011 --> 00:01:33,351
A bump.

28
00:01:33,351 --> 00:01:38,582
And we ask them what the center of the bump is, and magically as

29
00:01:38,582 --> 00:01:43,914
it turns out, the center of this distribution of total profits

30
00:01:43,914 --> 00:01:49,060
is exactly the sum of the means of the individual customers.

31
00:01:50,560 --> 00:01:52,041
My goodness, that is cool.

32
00:01:52,041 --> 00:01:54,301
Now, start over again.

33
00:01:54,301 --> 00:01:57,516
Now, this is a totally different store with totally different

34
00:01:57,516 --> 00:01:58,780
customers.

35
00:01:58,780 --> 00:02:00,870
In fact, it&#39;s on the other side of the world.

36
00:02:00,870 --> 00:02:01,910
Everything&#39;s totally different.

37
00:02:02,960 --> 00:02:03,770
And now we ask,

38
00:02:03,770 --> 00:02:07,200
what is the distribution of this store&#39;s profits?

39
00:02:07,200 --> 00:02:09,910
And rather interestingly, ha, ha,

40
00:02:09,910 --> 00:02:14,810
they also end up with a very similar looking sort of bump.

41
00:02:14,810 --> 00:02:16,090
Totally different store.

42
00:02:16,090 --> 00:02:17,800
Totally different customers, and

43
00:02:17,800 --> 00:02:19,790
yet, a single bump with the same shape.

44
00:02:21,240 --> 00:02:24,090
And the mean has the same formula too.

45
00:02:24,090 --> 00:02:29,000
It&#39;s the sum of the means of the customers&#39; individual means.

46
00:02:29,000 --> 00:02:30,280
But what the heck is going on here?

47
00:02:30,280 --> 00:02:32,010
Are these stores in cahoots?

48
00:02:32,010 --> 00:02:33,010
Did they to each other and

49
00:02:33,010 --> 00:02:35,220
negotiate the distribution of their sales?

50
00:02:35,220 --> 00:02:36,221
No way.

51
00:02:36,221 --> 00:02:40,360
It turns out, and get this, it always happens.

52
00:02:40,360 --> 00:02:43,380
The same bump with the same shape, right?

53
00:02:43,380 --> 00:02:46,837
It might be stretched out a bit, or it might be squeezed a bit,

54
00:02:46,837 --> 00:02:47,900
or scaled a bit, or

55
00:02:47,900 --> 00:02:50,640
shifted a bit, but it&#39;s really the same shape.

56
00:02:50,640 --> 00:02:53,210
So this is called the normal distribution.

57
00:02:53,210 --> 00:02:55,576
And its shape is given by this particular formula, and

58
00:02:55,576 --> 00:02:56,970
it&#39;s always the same formula.

59
00:02:56,970 --> 00:03:00,290
It&#39;s always the same shape.

60
00:03:00,290 --> 00:03:04,970
And the formula has a mean in it, mu, the mean of this thing.

61
00:03:04,970 --> 00:03:06,363
And the standard deviation sigma,

62
00:03:06,363 --> 00:03:08,601
that&#39;s the measure of the spread of that distribution.

63
00:03:08,601 --> 00:03:11,891
Now if you know those two things, if you know the mean and

64
00:03:11,891 --> 00:03:14,761
you know the sigma, you have the whole formula.

65
00:03:14,761 --> 00:03:17,000
So you know the whole shape of that curve.

66
00:03:18,330 --> 00:03:19,670
And it alway integrates to one.

67
00:03:21,240 --> 00:03:23,490
Now funny things happen when you fiddle with the mean and

68
00:03:23,490 --> 00:03:24,510
the variance there.

69
00:03:25,780 --> 00:03:30,657
So you can get these very kinda peaky normal distributions

70
00:03:30,657 --> 00:03:33,551
with small standard deviations.

71
00:03:33,551 --> 00:03:36,975
Or you could get these very broad ones with large standard

72
00:03:36,975 --> 00:03:38,160
deviations.

73
00:03:38,160 --> 00:03:41,301
And the mean can actually be any value it wants to be.

74
00:03:41,301 --> 00:03:43,808
And the standard deviation, as long as it&#39;s positive,

75
00:03:43,808 --> 00:03:45,470
can be anything.

76
00:03:45,470 --> 00:03:47,160
And there&#39;s that formula again.

77
00:03:48,350 --> 00:03:50,230
So, as long as you know the mean and the standard deviation,

78
00:03:50,230 --> 00:03:51,990
you got the full shape.

79
00:03:51,990 --> 00:03:54,490
Now, the cool thing is this fact that I told you,

80
00:03:55,660 --> 00:03:58,650
that the sum of a large number of independent random variables

81
00:03:58,650 --> 00:03:59,840
is approximately normal.

82
00:04:01,030 --> 00:04:03,260
And this is actually called the central limit theorem.

83
00:04:03,260 --> 00:04:06,691
And this is one of the most famous theorems in the world,

84
00:04:06,691 --> 00:04:08,447
the central limit theorem.

85
00:04:08,447 --> 00:04:11,850
So, even though all of the different customers had

86
00:04:11,850 --> 00:04:14,305
a totally different distribution,

87
00:04:14,305 --> 00:04:18,040
when you add them up, the sum is approximately normal.

88
00:04:20,079 --> 00:04:26,240
So let&#39;s have X1 through Xn be independent random variables.

89
00:04:26,240 --> 00:04:27,813
Their means are mu 1 through mu n.

90
00:04:27,813 --> 00:04:30,980
The standard deviations are sigma 1 through sigma n.

91
00:04:30,980 --> 00:04:32,560
Now I take their sum.

92
00:04:32,560 --> 00:04:36,340
Okay, this is the total sales for the store.

93
00:04:36,340 --> 00:04:40,301
And the Xs are the sales for the individual customers.

94
00:04:40,301 --> 00:04:44,969
And now as it turns out, that sum is approximately

95
00:04:44,969 --> 00:04:49,540
normal with mean, which is the sum of the means.

96
00:04:49,540 --> 00:04:54,401
So the mean of the sum is the sum of the means.

97
00:04:54,401 --> 00:04:56,988
And the variance of the sum turns out to be

98
00:04:56,988 --> 00:05:01,039
the sum of the variances of the independent random variables.

99
00:05:02,330 --> 00:05:04,780
Of course, the standard deviation is just the square

100
00:05:04,780 --> 00:05:05,410
root of the variance.

101
00:05:06,820 --> 00:05:09,520
Now, this theorem only works when

102
00:05:09,520 --> 00:05:11,640
the variables are independent.

103
00:05:11,640 --> 00:05:13,551
So you just have to make sure that that&#39;s true.

104
00:05:13,551 --> 00:05:14,584
And that&#39;s true for

105
00:05:14,584 --> 00:05:17,259
sales because each customer comes into the store and

106
00:05:17,259 --> 00:05:20,070
doesn&#39;t worry about what another customer is doing.

107
00:05:21,520 --> 00:05:27,535
Now the larger n is, the closer to normal,

108
00:05:27,535 --> 00:05:30,554
that s, that sum is.

109
00:05:30,554 --> 00:05:33,946
And it turns out that if the Xn&#39;s are actually normal in

110
00:05:33,946 --> 00:05:37,131
the first place, then their sum is normal anyway.

111
00:05:37,131 --> 00:05:38,851
Exactly normal.

112
00:05:38,851 --> 00:05:42,430
So if you start out with weird distributions though,

113
00:05:42,430 --> 00:05:46,370
then n needs to be a bit larger to look normal.

114
00:05:46,370 --> 00:05:48,438
Now, because of the central limit theorem,

115
00:05:48,438 --> 00:05:54,080
this distribution and this formula pops up all the time.

116
00:05:54,080 --> 00:05:57,930
Now, this formula is not something that a person made up,

117
00:05:57,930 --> 00:06:00,070
it&#39;s some thing that exists in nature.

118
00:06:00,070 --> 00:06:02,620
It&#39;s just as natural as the patterns you might find when

119
00:06:02,620 --> 00:06:04,720
looking at water waves or seashells.

120
00:06:04,720 --> 00:06:07,000
It&#39;s something that comes with the earth.

121
00:06:07,000 --> 00:06:09,610
It&#39;s the distribution of the amount of rain in

122
00:06:09,610 --> 00:06:10,830
Boston over the year.

123
00:06:10,830 --> 00:06:13,260
It&#39;s the distribution of test grades

124
00:06:13,260 --> 00:06:15,340
assuming no systematic cheating.

125
00:06:15,340 --> 00:06:18,200
And it&#39;s the distribution of anything that&#39;s a sum of

126
00:06:18,200 --> 00:06:19,740
independent events.

127
00:06:19,740 --> 00:06:22,150
This is like equals mc squared for probability.

128
00:06:23,280 --> 00:06:27,190
You might say, well, but I already

129
00:06:27,190 --> 00:06:31,510
thought you told us about sums of independent random variables?

130
00:06:31,510 --> 00:06:34,230
Doesn&#39;t the binomial distribution come from a sum of

131
00:06:34,230 --> 00:06:35,740
independent random variables?

132
00:06:37,330 --> 00:06:42,150
Well, as it turns out, the limiting binomial distribution

133
00:06:42,150 --> 00:06:45,090
is actually normal, so isn&#39;t that lovely?

134
00:06:46,310 --> 00:06:51,341
So, if I start out with just one trial with probability 0.5,

135
00:06:51,341 --> 00:06:54,975
the binomial distribution is pretty boring,

136
00:06:54,975 --> 00:06:57,603
it just looks like this, right?

137
00:06:57,603 --> 00:07:00,250
This is just a fair coin with a single coin flip.

138
00:07:00,250 --> 00:07:02,400
Half the time you&#39;ll get 0, half the time you&#39;ll get 1.

139
00:07:03,610 --> 00:07:06,600
Then when I start flipping more coins

140
00:07:06,600 --> 00:07:08,940
it starts to look a little bit more normal.

141
00:07:10,060 --> 00:07:11,660
Flip 3 coins.

142
00:07:11,660 --> 00:07:12,510
4 coins.

143
00:07:13,510 --> 00:07:17,799
5, and then we can flip 10 coins, 100 coins, and

144
00:07:17,799 --> 00:07:19,182
1000 coins.

145
00:07:19,182 --> 00:07:23,319
And then you&#39;ll see what looks like very much a beautiful

146
00:07:23,319 --> 00:07:25,653
normal looking distribution.

147
00:07:25,653 --> 00:07:28,620
So the limiting binomial is a normal.

148
00:07:28,620 --> 00:07:29,280
Isn&#39;t that lovely?

149
00:07:30,370 --> 00:07:32,080
Okay, so, yes, so that&#39;s the point.

150
00:07:32,080 --> 00:07:34,594
For large n, the binomial distribution actually becomes

151
00:07:34,594 --> 00:07:35,779
the normal distribution.

