0
00:00:04,977 --> 00:00:07,809
Let&#39;s talk about covariance and correlation.

1
00:00:07,809 --> 00:00:13,240
There&#39;s a company that rents bikes and scooters to commuters.

2
00:00:13,240 --> 00:00:15,640
And people get to choose either a bike or a scooter,

3
00:00:15,640 --> 00:00:16,820
whichever they want.

4
00:00:18,080 --> 00:00:22,300
And as it turns out, there&#39;s a negative correlation between

5
00:00:22,300 --> 00:00:26,640
the number of bike customers and the number of scooter customers.

6
00:00:26,640 --> 00:00:27,880
So what does that actually mean?

7
00:00:28,930 --> 00:00:32,327
So you can see a negative correlation in the scatter plot

8
00:00:32,327 --> 00:00:33,194
clearly, but

9
00:00:33,194 --> 00:00:36,312
what is the technical definition of correlation?

10
00:00:36,312 --> 00:00:39,415
Well, let&#39;s think about it this way.

11
00:00:39,415 --> 00:00:43,978
So this is the average number of scoot customers per day here,

12
00:00:43,978 --> 00:00:47,770
and here&#39;s the average number of bike customers.

13
00:00:49,090 --> 00:00:53,917
So what it says is that if there are more than average scoot

14
00:00:53,917 --> 00:00:58,747
customers on a particular day, there are probably fewer

15
00:00:58,747 --> 00:01:02,586
than average bike customers and vice versa.

16
00:01:05,250 --> 00:01:06,240
Okay, so there that is.

17
00:01:06,240 --> 00:01:08,280
Higher than average scoot corresponds to lower than

18
00:01:08,280 --> 00:01:11,830
average bike, right, higher than average scoot and

19
00:01:11,830 --> 00:01:12,740
lower than average bike.

20
00:01:14,490 --> 00:01:18,227
Okay, so, vice versa, if Monster Scoot has fewer customers than

21
00:01:18,227 --> 00:01:21,833
their usual average, it probably means Monster Bike has higher

22
00:01:21,833 --> 00:01:25,040
than average number of customers that day.

23
00:01:25,040 --> 00:01:29,096
And so that brings me to a formal definition of

24
00:01:29,096 --> 00:01:31,887
covariance and correlation.

25
00:01:31,887 --> 00:01:34,278
Let&#39;s start with covariance.

26
00:01:34,278 --> 00:01:39,470
Okay, so let&#39;s say whether X is above or below its mean.

27
00:01:39,470 --> 00:01:42,257
And I&#39;m writing the mean here, the mean of X,

28
00:01:42,257 --> 00:01:43,769
as the expectation of X.

29
00:01:43,769 --> 00:01:46,568
And I used the notation mu before, but

30
00:01:46,568 --> 00:01:48,590
this means the same thing.

31
00:01:50,040 --> 00:01:52,570
And this notation is just as common as the mu notation.

32
00:01:54,140 --> 00:01:58,533
So we&#39;re looking at whether X is above or below its mean.

33
00:01:58,533 --> 00:02:03,025
And now let&#39;s see if Y is above or below its mean.

34
00:02:03,025 --> 00:02:06,421
So two random variables, X and Y,

35
00:02:06,421 --> 00:02:11,927
if X is higher than its mean when Y is lower than its mean,

36
00:02:11,927 --> 00:02:15,692
then this thing is gonna be negative.

37
00:02:15,692 --> 00:02:17,920
Actually let&#39;s just go through all the combinations.

38
00:02:17,920 --> 00:02:20,794
So if X is higher than its mean and

39
00:02:20,794 --> 00:02:25,270
Y is higher than its mean, then this is positive.

40
00:02:25,270 --> 00:02:29,728
Now, if X is above its mean, and Y is below its mean, when you

41
00:02:29,728 --> 00:02:34,459
multiply them together, you get something that&#39;s negative.

42
00:02:37,702 --> 00:02:39,309
And I filled in the rest.

43
00:02:39,309 --> 00:02:41,974
Now if X is below its mean when Y is below its mean,

44
00:02:41,974 --> 00:02:45,550
we&#39;re gonna be multiplying two negative numbers together, and

45
00:02:45,550 --> 00:02:47,319
then we&#39;ll get a positive one.

46
00:02:52,366 --> 00:02:55,040
Okay, so this is the definition of the covariance.

47
00:02:55,040 --> 00:02:58,800
It&#39;s just the expectation of this quantity in here.

48
00:02:58,800 --> 00:03:01,560
So, now it&#39;s all about how often things happen.

49
00:03:01,560 --> 00:03:06,620
If we&#39;re often in a situation where X and Y vary together,

50
00:03:07,700 --> 00:03:10,990
then the product in here will often be positive,

51
00:03:10,990 --> 00:03:14,020
and then the expectation will be positive.

52
00:03:18,250 --> 00:03:21,480
Okay, so that&#39;s for the population.

53
00:03:21,480 --> 00:03:23,680
What if you only have data?

54
00:03:23,680 --> 00:03:26,994
Then you need to compute the sample covariance which is

55
00:03:26,994 --> 00:03:28,840
this thing down here.

56
00:03:28,840 --> 00:03:31,790
It&#39;s exactly the same thing as the covariance.

57
00:03:31,790 --> 00:03:35,598
The only difference is that you can compute it from your data

58
00:03:35,598 --> 00:03:36,670
which is 1 to n.

59
00:03:36,670 --> 00:03:40,235
And then again there&#39;s this mysterious n-1 over there that

60
00:03:40,235 --> 00:03:43,248
you really should not pay attention to cuz it&#39;s just

61
00:03:43,248 --> 00:03:44,290
an issue of bias.

62
00:03:45,570 --> 00:03:49,080
Like I said, it&#39;s already in the software to compute that for

63
00:03:49,080 --> 00:03:52,010
you so you shouldn&#39;t pay attention to it.

64
00:03:54,130 --> 00:04:01,853
Then what happens if you take the covariance of X with itself?

65
00:04:01,853 --> 00:04:06,120
So X, the Y is also equal to X.

66
00:04:06,120 --> 00:04:09,120
Look at this, does that look familiar?

67
00:04:10,910 --> 00:04:14,520
It should, because it is exactly the variance, and

68
00:04:14,520 --> 00:04:17,280
this is the same definition we had earlier.

69
00:04:17,279 --> 00:04:19,650
Take the distance of X from its mean,

70
00:04:19,649 --> 00:04:23,243
square it to get the squared distance from the mean, and

71
00:04:23,243 --> 00:04:26,541
take the expectation, and that&#39;s the variance.

72
00:04:31,021 --> 00:04:35,228
And then this one here is the sample variance, again,

73
00:04:35,228 --> 00:04:37,890
with that silly n-1 over there.

74
00:04:37,890 --> 00:04:41,187
And the sample variance has a particular notation, and

75
00:04:41,187 --> 00:04:42,599
it is called s squared.

76
00:04:47,761 --> 00:04:52,190
Now the issue with covariance is that it&#39;s not in units that make

77
00:04:52,190 --> 00:04:53,021
any sense.

78
00:04:53,021 --> 00:04:58,830
So correlation is a version of covariance, but where we scaled

79
00:04:58,830 --> 00:05:04,310
its value so that the largest possible correlation is one,

80
00:05:04,310 --> 00:05:08,600
and the smallest possible value is minus one.

81
00:05:10,260 --> 00:05:11,890
And so that&#39;s what these terms are here.

82
00:05:11,890 --> 00:05:14,010
So this is the standard deviation of X, and

83
00:05:14,010 --> 00:05:17,070
this is the standard deviation of Y.

84
00:05:17,070 --> 00:05:20,226
And when you scale this quantity by these two terms, then

85
00:05:20,226 --> 00:05:23,592
the largest value is one and the smallest value is minus one.

86
00:05:28,787 --> 00:05:31,050
Okay, and here is the sample correlation.

87
00:05:31,050 --> 00:05:35,167
And again, you get that silly n-1 term.

88
00:05:35,167 --> 00:05:38,548
And then these become quantities that you can compute from your

89
00:05:38,548 --> 00:05:41,758
data, namely the sample standard deviations of X and of Y.

90
00:05:46,158 --> 00:05:49,731
Now since correlations are always between +1 and

91
00:05:49,731 --> 00:05:54,610
-1, a correlation of 0.7 say, that&#39;s actually meaningful.

92
00:05:54,610 --> 00:05:59,842
I can really understand it on this scale between 1 and -1.

93
00:06:03,186 --> 00:06:07,520
Here&#39;s a nice plot showing some of the data from the automobile

94
00:06:07,520 --> 00:06:08,840
pricing data set.

95
00:06:11,070 --> 00:06:17,463
Now each car in this data set gets a dot which represents its

96
00:06:17,463 --> 00:06:22,958
price, and then the size of the engine of the car.

97
00:06:22,958 --> 00:06:25,486
And you can definitely see that there&#39;s

98
00:06:25,486 --> 00:06:28,810
a clear positive correlation there.

99
00:06:28,810 --> 00:06:30,590
As price goes up,

100
00:06:30,590 --> 00:06:34,070
you would definitely expect the engine size of a car to go up.

101
00:06:34,070 --> 00:06:36,060
If you paid more for the car, you&#39;re paying for

102
00:06:36,060 --> 00:06:36,790
a bigger engine.

103
00:06:38,360 --> 00:06:41,570
And the increase pretty much follows a straight line here.

104
00:06:41,570 --> 00:06:44,280
And it follows the straight line well enough

105
00:06:44,280 --> 00:06:47,926
that the correlation is actually 0.89.

106
00:06:47,926 --> 00:06:51,019
See the covariance here, that&#39;s in weird units, so it&#39;s really

107
00:06:51,019 --> 00:06:53,631
hard to figure out what that number means intuitively.

108
00:06:53,631 --> 00:06:55,438
But the correlation,

109
00:06:55,438 --> 00:06:58,962
that&#39;s actually much easier to deal with.

110
00:06:58,962 --> 00:07:04,559
And then here&#39;s more of that same data set, so this is bore

111
00:07:04,559 --> 00:07:10,517
diameter which is the size of a part of the engine, and price.

112
00:07:10,517 --> 00:07:14,313
This bore diameter, it does correlate with price, but

113
00:07:14,313 --> 00:07:17,883
it&#39;s not as closely correlated as the engine size.

114
00:07:17,883 --> 00:07:21,812
So here the correlation is 0.55, and again I have this mysterious

115
00:07:21,812 --> 00:07:25,220
covariance value that I don&#39;t know exactly what to do with.

116
00:07:26,770 --> 00:07:30,614
Now this is city miles per gallon versus price,

117
00:07:30,614 --> 00:07:34,275
and here you can see a negative correlation.

118
00:07:34,275 --> 00:07:36,864
I guess that as price goes up the cars must not be as

119
00:07:36,864 --> 00:07:39,962
efficient, probably because they have bigger engines or

120
00:07:39,962 --> 00:07:41,490
something, I don&#39;t know.

121
00:07:42,900 --> 00:07:46,162
Anyway, so the correlation is -0.7.

122
00:07:46,162 --> 00:07:49,974
And that again that&#39;s meaningful because the lowest it can be is

123
00:07:49,974 --> 00:07:50,474
a -1.

124
00:07:50,474 --> 00:07:54,882
Now be careful because people widely confuse

125
00:07:54,882 --> 00:07:57,903
correlation with causation.

126
00:07:57,903 --> 00:08:02,354
Correlation is really, really not causation.

127
00:08:02,354 --> 00:08:07,488
And here is the quintessential example of why that&#39;s not true.

128
00:08:07,488 --> 00:08:10,380
It turns out that health, human health,

129
00:08:10,380 --> 00:08:14,110
has a negative correlation with how wealthy you are.

130
00:08:16,010 --> 00:08:18,440
So you see points like this.

131
00:08:18,440 --> 00:08:22,298
If you plot sort of the individuals around the world,

132
00:08:22,298 --> 00:08:27,102
you&#39;ll notice that the healthier they are, the less wealthy they

133
00:08:27,102 --> 00:08:31,753
are, and the more wealthy they are, the less healthy they are.

134
00:08:31,753 --> 00:08:34,894
So why in the world is that?

135
00:08:34,894 --> 00:08:38,202
And if you think for a minute, you&#39;ll figure it out.

136
00:08:38,202 --> 00:08:42,318
And you&#39;ll figure out that there is some piece of information I

137
00:08:42,318 --> 00:08:46,134
didn&#39;t tell you, which is the age of these individuals.

138
00:08:46,134 --> 00:08:50,670
Because older people tend to be wealthier but less healthy, and

139
00:08:50,670 --> 00:08:53,862
younger people tend to be not as wealthy, but

140
00:08:53,862 --> 00:08:55,801
definitely more healthy.

141
00:08:55,801 --> 00:08:58,950
So what does that mean?

142
00:08:58,950 --> 00:09:00,120
This is not causal.

143
00:09:00,120 --> 00:09:03,673
It doesn&#39;t mean that poor health causes you to be rich.

144
00:09:03,673 --> 00:09:07,342
And it also doesn&#39;t mean that being rich causes you to be

145
00:09:07,342 --> 00:09:08,390
unhealthy.

146
00:09:08,390 --> 00:09:09,824
There&#39;s nothing causal here.

147
00:09:09,824 --> 00:09:12,367
But there&#39;s a correlation.

148
00:09:12,367 --> 00:09:15,090
I can predict health from wealth.

149
00:09:15,090 --> 00:09:19,386
I can predict wealth from health without anything causal in there

150
00:09:19,386 --> 00:09:19,938
at all.

151
00:09:19,938 --> 00:09:22,438
Now in machine learning, which we&#39;ll get to,

152
00:09:22,438 --> 00:09:24,420
prediction is the name of the game.

153
00:09:24,420 --> 00:09:26,587
It&#39;s not causality necessarily.

154
00:09:26,587 --> 00:09:29,961
There are some special branches of machine learning that

155
00:09:29,961 --> 00:09:32,288
are sort of concerned with causality.

156
00:09:32,288 --> 00:09:33,745
But for the most part,

157
00:09:33,745 --> 00:09:37,963
machine learning is just about predicting the future from data,

158
00:09:37,963 --> 00:09:41,657
not necessarily understanding the causal mechanisms.

159
00:09:41,657 --> 00:09:43,307
And so that&#39;s a longer discussion that

160
00:09:43,307 --> 00:09:45,210
I really don&#39;t wanna get into too much here.

161
00:09:46,610 --> 00:09:47,410
It&#39;s just nice,

162
00:09:47,410 --> 00:09:50,180
though, that you don&#39;t have to worry about causality because

163
00:09:50,180 --> 00:09:53,110
not having to worry about that makes things much

164
00:09:53,110 --> 00:09:54,800
easier from a practical perspective.

165
00:09:54,800 --> 00:09:59,230
You can just predict without worrying about the causes.

166
00:09:59,230 --> 00:10:03,215
So let&#39;s say that you&#39;re trying to predict whether someone will

167
00:10:03,215 --> 00:10:05,035
want a coupon for your store.

168
00:10:05,035 --> 00:10:07,923
But you don&#39;t have to worry about why they will take that

169
00:10:07,923 --> 00:10:10,140
coupon or why they might want the product.

170
00:10:11,540 --> 00:10:14,994
All you have to do is use correlations to guess whether

171
00:10:14,994 --> 00:10:16,772
she might want the coupon.

172
00:10:16,772 --> 00:10:19,339
So for instance you could say, it&#39;s Tuesday, and

173
00:10:19,339 --> 00:10:21,690
she often takes coupons on Tuesdays.

174
00:10:21,690 --> 00:10:23,221
So let&#39;s enter a coupon.

175
00:10:23,221 --> 00:10:25,944
Who cares why she likes coupons on Tuesdays,

176
00:10:25,944 --> 00:10:27,311
we just know she does.

177
00:10:27,311 --> 00:10:32,428
Or we could say, people like her want this coupon,

178
00:10:32,428 --> 00:10:34,933
so let&#39;s send it to her.

179
00:10:34,933 --> 00:10:38,147
Now machine learning is all about combining the right

180
00:10:38,147 --> 00:10:41,085
correlations to be able to predict accurately.

181
00:10:41,085 --> 00:10:44,191
But you can&#39;t make conclusion about causality unless you learn

182
00:10:44,191 --> 00:10:47,183
more about causal inference techniques, which is really for

183
00:10:47,183 --> 00:10:47,889
another day.

