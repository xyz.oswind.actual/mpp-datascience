0
00:00:05,036 --> 00:00:08,928
Now the CDF tells you the probability for

1
00:00:08,928 --> 00:00:13,406
random variable x to be below a certain value.

2
00:00:13,406 --> 00:00:18,961
So in our case over here, it&#39;s 14.

3
00:00:18,961 --> 00:00:23,239
Okay, so this area under here, that&#39;s the probability that

4
00:00:23,239 --> 00:00:26,789
the random variable&#39;s less than or equal to 14.

5
00:00:26,789 --> 00:00:30,420
And the CDF function is written just like this.

6
00:00:30,420 --> 00:00:35,364
F(14), that&#39;s the probability of the random variable to take

7
00:00:35,364 --> 00:00:37,021
on a value below 14.

8
00:00:37,021 --> 00:00:39,336
And I can write it more generally, so

9
00:00:39,336 --> 00:00:41,750
if I can write it for a general x.

10
00:00:41,750 --> 00:00:45,871
So for any of amount of coffee x that Steve might drink,

11
00:00:45,871 --> 00:00:50,451
the CDF gives the probability to drink less than that value.

12
00:00:50,451 --> 00:00:53,794
Now, does it matter whether I write less than or

13
00:00:53,794 --> 00:00:56,150
equal to here or just less than?

14
00:00:56,150 --> 00:01:00,407
And the answer is no, it doesn&#39;t matter,

15
00:01:00,407 --> 00:01:04,668
because the probability that he&#39;ll drink

16
00:01:04,668 --> 00:01:09,415
exactly that value x down to the molecule is 0.

17
00:01:09,415 --> 00:01:13,201
For continuous distributions, doesn&#39;t matter whether this is

18
00:01:13,201 --> 00:01:15,620
strict equality or less than or equal to.

19
00:01:16,690 --> 00:01:21,230
So this subtraction that we were doing earlier,

20
00:01:21,230 --> 00:01:24,300
this can actually be written in terms of the CDFs.

21
00:01:24,300 --> 00:01:27,620
So if we want the probability that X is between 10 and 14,

22
00:01:27,620 --> 00:01:32,070
it&#39;s the CDF at 14 minus the CDF at 10.

23
00:01:32,070 --> 00:01:33,010
And I can write it like this.

24
00:01:34,270 --> 00:01:39,387
And if I want the probability that Steve is gonna drink more

25
00:01:39,387 --> 00:01:44,714
than a certain value, like 11 liters, then I can actually

26
00:01:44,714 --> 00:01:49,831
get that by remembering that the area under the whole thing

27
00:01:49,831 --> 00:01:54,652
is 1 and subtracting the area to the left of 11 here.

28
00:01:54,652 --> 00:01:58,730
Okay, so what is the smallest possible value for the CDF?

29
00:01:58,730 --> 00:02:03,747
And at 0 all the way on the left, okay?

30
00:02:03,747 --> 00:02:10,380
So if X is -10, that&#39;s way over here, then F is about 0.

31
00:02:10,380 --> 00:02:16,009
Now what about 0.25, okay?

32
00:02:16,009 --> 00:02:20,904
So if X is 8, then the area under here is about 0.25.

33
00:02:20,904 --> 00:02:24,623
This is about a quarter of the area under the whole thing.

34
00:02:24,623 --> 00:02:29,593
That&#39;s a quarter probability to get less than 8.

35
00:02:29,593 --> 00:02:31,281
Then what about 10?

36
00:02:31,281 --> 00:02:32,845
F(10) is about 0.5,

37
00:02:32,845 --> 00:02:36,440
because that&#39;s really where the center of the distribution is.

38
00:02:37,690 --> 00:02:41,642
F(12) is about 0.75 and then F(20) is about 1,

39
00:02:41,642 --> 00:02:46,660
so if I actually plot out what F looks like, it looks like this.

40
00:02:46,660 --> 00:02:48,633
So it&#39;s 0 all the way to the left, and

41
00:02:48,633 --> 00:02:50,988
then when I get to about 8, it&#39;s 0.25.

42
00:02:50,988 --> 00:02:56,178
When I get to 10 it&#39;s about 0.5, I get up to 12 it&#39;s about 0.75,

43
00:02:56,178 --> 00:02:59,398
and when I get up to 20, it&#39;s 1, okay.

44
00:02:59,398 --> 00:03:03,232
So now you

45
00:03:03,232 --> 00:03:07,870
see I seem to be drawing bumps very often as my example of what

46
00:03:07,870 --> 00:03:12,180
a PDF ought to look like, which means the CDF

47
00:03:12,180 --> 00:03:15,870
always ends up looking kinda like that function down here.

48
00:03:17,290 --> 00:03:21,622
Now, there is a very good reason that I keep drawing bell curves

49
00:03:21,622 --> 00:03:24,341
like this and I will tell you what it is.

50
00:03:24,341 --> 00:03:28,445
It is called the central limit theorem and that is up next.

