0
00:00:00,012 --> 00:00:05,770
Hi, so

1
00:00:05,770 --> 00:00:10,410
now that you know what a random variable is, let&#39;s talk about

2
00:00:10,410 --> 00:00:13,938
some special discrete probability distributions.

3
00:00:13,938 --> 00:00:16,850
And in particular the Bernoulli and binomial distributions.

4
00:00:18,260 --> 00:00:20,210
Whenever I think of the Bernoulli distribution,

5
00:00:20,210 --> 00:00:21,140
I think of a coin flip.

6
00:00:22,270 --> 00:00:26,332
I think of a variable that attains outcome 1 probability

7
00:00:26,332 --> 00:00:29,385
with p and outcome 0 with probability 1-p.

8
00:00:29,385 --> 00:00:34,190
So it&#39;s a weighted coin, and it lands on heads with probability

9
00:00:34,190 --> 00:00:38,870
p, and tails with probability 1-p.

10
00:00:38,870 --> 00:00:43,568
Now the way we write it is that x is a random variable that

11
00:00:43,568 --> 00:00:47,963
has a Bernoulli distribution with probability p,

12
00:00:47,963 --> 00:00:49,819
of landing on heads.

13
00:00:52,787 --> 00:00:57,826
So, for example, each American, so each American currently

14
00:00:57,826 --> 00:01:02,400
has a 0.89 probability of having health insurance.

15
00:01:03,510 --> 00:01:08,009
So, we&#39;ll assign a 1 to each person if they have health

16
00:01:08,009 --> 00:01:10,412
insurance and 0 otherwise.

17
00:01:10,412 --> 00:01:14,708
So, x would be here, a Bernoulli distribution of 0.89.

18
00:01:14,708 --> 00:01:17,467
And X is the random variable that represents whether a random

19
00:01:17,467 --> 00:01:19,800
person, a random American has health insurance.

20
00:01:21,510 --> 00:01:23,130
Now the binomial distribution

21
00:01:25,120 --> 00:01:26,670
builds on the Bernoulli distribution.

22
00:01:28,140 --> 00:01:30,430
When I think of the binomial distribution,

23
00:01:30,430 --> 00:01:33,280
I think of repeated Bernoulli trials

24
00:01:33,280 --> 00:01:35,460
each with the same probability of success.

25
00:01:37,860 --> 00:01:42,580
Now, let us talk more about the binomial distribution, and

26
00:01:42,580 --> 00:01:44,440
in particular I&#39;m gonna derive it for you.

27
00:01:45,640 --> 00:01:46,990
Let&#39;s start with many Bernoulli&#39;s.

28
00:01:48,330 --> 00:01:51,184
So, let&#39;s pick ten Americans at random and

29
00:01:51,184 --> 00:01:53,894
each American gets their own Bernoulli.

30
00:01:53,894 --> 00:01:57,091
And we&#39;ll define a new random variable which is,

31
00:01:57,091 --> 00:01:58,240
we&#39;ll call it X.

32
00:01:58,240 --> 00:02:02,895
And it&#39;s gonna be the number of those ten Americans that

33
00:02:02,895 --> 00:02:04,985
have health insurance.

34
00:02:04,985 --> 00:02:07,686
And we wanna be able to calculate things like

35
00:02:07,686 --> 00:02:11,849
the probability that 3 of those Americans have health insurance.

36
00:02:11,849 --> 00:02:13,690
So how in the world do we calculate that?

37
00:02:15,020 --> 00:02:16,910
But let us try some simpler problems first.

38
00:02:18,530 --> 00:02:22,180
Now let&#39;s consider these two problems.

39
00:02:22,180 --> 00:02:26,169
So the first problem is of 10 randomly chosen Americans,

40
00:02:26,169 --> 00:02:30,079
what&#39;s the probability that the first 8 will have health

41
00:02:30,079 --> 00:02:32,410
insurance and the next 2 won&#39;t?

42
00:02:33,660 --> 00:02:37,595
And then the second problem that we&#39;ll consider is what&#39;s

43
00:02:37,595 --> 00:02:41,844
the probability that the first 3 will have health insurance and

44
00:02:41,844 --> 00:02:43,191
the next 7 won&#39;t.

45
00:02:43,191 --> 00:02:49,336
Okay so let&#39;s consider the first one of those two problems.

46
00:02:49,336 --> 00:02:52,530
And the answer to this is pretty simple, so it&#39;s the probability

47
00:02:52,530 --> 00:02:54,920
that the first 8 of them have health insurance.

48
00:02:54,920 --> 00:02:58,320
So yes, yes, yes, yes, yes, yes, yes, yes, and then two no&#39;s.

49
00:02:59,740 --> 00:03:04,155
And the probability to have health insurance is 0.89, and

50
00:03:04,155 --> 00:03:07,913
there are 8 of them, that&#39;s where that comes from.

51
00:03:07,913 --> 00:03:13,364
Then there are 2 no&#39;s, the probability not to have health

52
00:03:13,364 --> 00:03:19,042
insurance is 0.11, there are 2 of them, that&#39;s that.

53
00:03:19,042 --> 00:03:22,274
Now what&#39;s the probability the first 3 will have health

54
00:03:22,274 --> 00:03:24,235
insurance and the next 7 won&#39;t?

55
00:03:24,235 --> 00:03:28,680
So we have 3 yes&#39;s, so 0.89 cubed and

56
00:03:28,680 --> 00:03:33,746
then for the 7 no&#39;s I have 0.11 to the 7th.

57
00:03:33,746 --> 00:03:38,700
And if you multiply that all out you get 1.37 times 10 to

58
00:03:38,700 --> 00:03:40,167
the negative 7.

59
00:03:40,167 --> 00:03:43,940
So we&#39;re getting closer here, so

60
00:03:43,940 --> 00:03:50,195
what I did here is all I did was switch one yes, and one no.

61
00:03:50,195 --> 00:03:53,311
And so the probability doesn&#39;t change, cuz there&#39;s still 3

62
00:03:53,311 --> 00:03:56,853
yes&#39;s and there&#39;s still 7 no&#39;s, so that&#39;s the same probability.

63
00:03:56,853 --> 00:04:00,061
And in fact, it doesn&#39;t matter how you rearrange those 3 yes&#39;s,

64
00:04:00,061 --> 00:04:01,763
you&#39;ll still get the same answer.

65
00:04:04,016 --> 00:04:06,825
So now, what&#39;s the probability that I

66
00:04:06,825 --> 00:04:11,170
get 3 of these Americans with health insurance?

67
00:04:11,170 --> 00:04:15,060
Well, I have to add up all of the possibilities with X=3.

68
00:04:15,060 --> 00:04:17,710
So I have to add up the probability that

69
00:04:17,709 --> 00:04:19,110
that happens, and

70
00:04:19,110 --> 00:04:22,510
then I have to add that to the probability that this happens.

71
00:04:22,510 --> 00:04:27,790
And then all the other ways that I have 3 yes&#39;s and 7 no&#39;s.

72
00:04:27,790 --> 00:04:29,500
That&#39;s gonna be the answer to my problem.

73
00:04:32,040 --> 00:04:35,550
And all these probabilities are gonna be 1.37 times 10 to

74
00:04:35,550 --> 00:04:36,220
the negative 7.

75
00:04:36,220 --> 00:04:40,705
And the question is how many terms are there?

76
00:04:40,705 --> 00:04:43,101
How many of these things do I have to add up?

77
00:04:46,741 --> 00:04:50,560
Now that brings us to the question of how many ways

78
00:04:50,560 --> 00:04:53,550
are there to rearrange 10 numbers?

79
00:04:58,880 --> 00:05:00,668
This is the same question as the following.

80
00:05:00,668 --> 00:05:04,749
So let&#39;s suppose that there are 10 data scientists seated

81
00:05:04,749 --> 00:05:05,395
in a row.

82
00:05:05,395 --> 00:05:09,335
How many ways are there to pick a group of 3?

83
00:05:09,335 --> 00:05:13,630
Now, there are how many ways to arrange them?

84
00:05:13,630 --> 00:05:17,844
There are 10 ways to pick first data scientist.

85
00:05:17,844 --> 00:05:19,788
And then once he&#39;s chosen,

86
00:05:19,788 --> 00:05:24,087
how many ways are there to pick the second data scientist, 9.

87
00:05:24,087 --> 00:05:29,238
And then once that ones chosen, then there are eight left and

88
00:05:29,238 --> 00:05:33,191
so on and so forth, so that&#39;s ten factorial.

89
00:05:33,191 --> 00:05:35,875
But then how many ways are there to arrange the 3 I picked?

90
00:05:35,875 --> 00:05:39,757
Because it doesn&#39;t matter in which order I pick those 3,

91
00:05:39,757 --> 00:05:41,680
I just have to get 3 of them.

92
00:05:41,680 --> 00:05:44,930
And, in fact, that, of course, is then 3 factorial, but

93
00:05:44,930 --> 00:05:45,630
the same logic.

94
00:05:48,630 --> 00:05:52,046
And then I don&#39;t really care how the 7 that I didn&#39;t pick

95
00:05:52,046 --> 00:05:53,670
are arranged.

96
00:05:53,670 --> 00:06:00,430
So how many ways to arrange them are there, it&#39;s 7 factorial.

97
00:06:00,430 --> 00:06:06,675
So we&#39;re almost there, so the answer to this question then,

98
00:06:06,675 --> 00:06:12,800
s 10 factorial divided by 3 factorial 7 factorial.

99
00:06:13,990 --> 00:06:15,570
This is the number of ways to

100
00:06:15,570 --> 00:06:16,920
rearrange all the data scientists.

101
00:06:16,920 --> 00:06:21,120
This is the number of ways to rearrange the 3 that I pick,

102
00:06:21,120 --> 00:06:22,640
cuz I don&#39;t care what arrangement they&#39;re in.

103
00:06:22,640 --> 00:06:25,440
And then this is the number of ways to arrange the 7

104
00:06:25,440 --> 00:06:26,060
that I don&#39;t pick.

105
00:06:27,510 --> 00:06:29,121
And the answer here is 120.

106
00:06:32,229 --> 00:06:36,980
And more generally, the answer is n choose k.

107
00:06:39,250 --> 00:06:41,850
This is the notation for n choose k.

108
00:06:41,850 --> 00:06:43,570
n is the number of data scientists,

109
00:06:43,570 --> 00:06:45,710
k is the number of them I&#39;m gonna pick.

110
00:06:46,900 --> 00:06:50,150
And so, this is the number of ways to choose k out of n.

111
00:06:50,150 --> 00:06:55,510
With that, we now know how many terms there are in this sum.

112
00:06:56,540 --> 00:07:00,150
Remember, we knew that each probability was 1.37,

113
00:07:00,150 --> 00:07:02,480
times 10 to the negative 7th, and

114
00:07:02,480 --> 00:07:05,198
we didn&#39;t know how many terms there were.

115
00:07:05,198 --> 00:07:09,459
And the answer is, actually, 10 factorial divided by

116
00:07:09,459 --> 00:07:12,932
3 factorial 7 factorial, which is 120.

117
00:07:12,932 --> 00:07:17,676
It&#39;s the number of ways to pick

118
00:07:17,676 --> 00:07:21,720
3 Americans out of 10.

119
00:07:21,720 --> 00:07:23,598
So the answer to the problem,

120
00:07:23,598 --> 00:07:26,164
this problem over here looks like that.

121
00:07:26,164 --> 00:07:31,083
It&#39;s 120 terms times each term which is 1.37

122
00:07:31,083 --> 00:07:33,884
time 10 to the negative 7.

123
00:07:36,670 --> 00:07:38,191
Let us take a deep breath and

124
00:07:38,191 --> 00:07:40,980
review the process by which we got to that answer.

125
00:07:41,990 --> 00:07:44,541
So, let us go over the problem first.

126
00:07:44,541 --> 00:07:47,312
So, we want to pick 10 Americans at random,

127
00:07:47,312 --> 00:07:50,879
X is gonna be the number of them that have health insurance.

128
00:07:50,879 --> 00:07:56,690
And I want to know what the probability is, that X is 3.

129
00:07:56,690 --> 00:07:59,505
And other words, what&#39;s the probability that of the 10

130
00:07:59,505 --> 00:08:02,100
Americans I pick, 3 of them have health insurance.

131
00:08:03,350 --> 00:08:06,000
We&#39;ve already solve the problem, so I wanna go backward through

132
00:08:06,000 --> 00:08:09,790
the calculation that we made to get to that answer.

133
00:08:11,630 --> 00:08:16,276
So we decided that the answer was 120 times 1.37

134
00:08:16,276 --> 00:08:18,906
times 10 to the negative 7.

135
00:08:18,906 --> 00:08:24,789
Now this number here, 1.37 times 10 to the negative 7,

136
00:08:24,789 --> 00:08:30,006
we got that thing by probability 0.89 to have health

137
00:08:30,006 --> 00:08:35,009
insurance, and 3 Americans out of our 10 have it.

138
00:08:35,009 --> 00:08:39,858
And then 0.11 is the probability not to have health insurance in

139
00:08:39,857 --> 00:08:43,100
7 Americans in our 10 are gonna have that.

140
00:08:45,990 --> 00:08:49,530
And then 120 we got from this calculation over here

141
00:08:50,930 --> 00:08:56,042
which is the number of ways I can pick 3 out of 10.

142
00:08:58,280 --> 00:09:01,080
And that ended up being the answer.

143
00:09:02,100 --> 00:09:04,630
And then if I multiply it all out I get this number over here

144
00:09:04,630 --> 00:09:05,579
which is the final one.

145
00:09:07,888 --> 00:09:11,400
What I wanna do now is write

146
00:09:11,400 --> 00:09:14,650
this in more general notation to solve a more general problem.

147
00:09:16,540 --> 00:09:19,750
And that&#39;s the formula that I want to show you.

148
00:09:19,750 --> 00:09:22,550
This is actually the punchline, this is the formula for

149
00:09:22,550 --> 00:09:23,870
the binomial distribution.

150
00:09:25,200 --> 00:09:26,960
Now I&#39;m gonna discuss it more in the next slide.

151
00:09:26,960 --> 00:09:30,870
But I want you to see that it looks just like

152
00:09:30,870 --> 00:09:31,990
what we computed already.

153
00:09:36,020 --> 00:09:41,400
So pick n Americans at random, before n was 10,

154
00:09:41,400 --> 00:09:42,610
but now it&#39;s general.

155
00:09:44,560 --> 00:09:47,840
Each American has probability p of having health insurance,

156
00:09:49,650 --> 00:09:53,170
the p before was 0.89.

157
00:09:53,170 --> 00:09:55,710
And then my random variable is the number of them

158
00:09:55,710 --> 00:09:56,600
with health insurance.

159
00:09:57,840 --> 00:10:01,900
I want to know, I wanna formula of the probability that

160
00:10:01,900 --> 00:10:05,070
that random variable equals a particular value, little x.

161
00:10:07,285 --> 00:10:08,940
So here is the formula, right here.

162
00:10:10,750 --> 00:10:14,270
So this thing is the number of ways I can choose

163
00:10:15,470 --> 00:10:18,340
x objects out of n total.

164
00:10:19,660 --> 00:10:22,670
And then p is the probability of success for

165
00:10:22,670 --> 00:10:26,680
each of the Bernoulli trials, each of the n Bernoulli trials.

166
00:10:26,680 --> 00:10:30,645
And we&#39;re gonna have x successes and n-x failures.

167
00:10:30,645 --> 00:10:35,450
And so this is exactly what we just computed in the example.

168
00:10:37,230 --> 00:10:39,360
So this is the binomial distribution.

169
00:10:39,360 --> 00:10:41,150
You have n independent trials,

170
00:10:41,150 --> 00:10:43,389
each one has the same probability of success.

171
00:10:44,450 --> 00:10:46,950
And the probability of x successes out of n,

172
00:10:46,950 --> 00:10:48,190
little x successes out of n,

173
00:10:48,190 --> 00:10:51,730
is exactly this formula, this is the binomial distribution.

174
00:10:55,600 --> 00:10:59,090
Now, you got through that, that was the hard part.

175
00:10:59,090 --> 00:11:01,880
I will tell you some interesting facts about the binomial

176
00:11:01,880 --> 00:11:04,580
distribution that you might wanna keep in your head.

177
00:11:06,340 --> 00:11:11,197
So here they are, the mean of the binomial distribution, or

178
00:11:11,197 --> 00:11:15,970
the expectation of the binomial distribution is n times p.

179
00:11:15,970 --> 00:11:20,125
So you have n independent trials each with

180
00:11:20,125 --> 00:11:25,357
the probability of success p, the mean is n times p.

181
00:11:25,357 --> 00:11:29,426
And then the spread of that distribution,

182
00:11:29,426 --> 00:11:35,535
the spread of the binomial, the variance of it is np(1- p).

183
00:11:35,535 --> 00:11:38,240
Okay so these are two facts that I keep in my head.

184
00:11:38,240 --> 00:11:40,885
And I carry them with me all the time because I end up using them

185
00:11:40,885 --> 00:11:42,690
fairly often.

186
00:11:42,690 --> 00:11:44,828
And now you understand the binomial distribution.

