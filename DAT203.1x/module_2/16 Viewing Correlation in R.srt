0
00:00:00,000 --> 00:00:03,840
Cynthia has been discussing how the

1
00:00:03,840 --> 00:00:06,569
formulas for computing correlations and

2
00:00:06,569 --> 00:00:08,309
a bit about how you interpret

3
00:00:08,309 --> 00:00:11,370
correlations in this demo i&#39;d like to

4
00:00:11,370 --> 00:00:13,889
show you how to compute correlations

5
00:00:13,889 --> 00:00:16,500
using our and we&#39;ll talk about some

6
00:00:16,500 --> 00:00:19,710
practical aspects of how we understand

7
00:00:19,710 --> 00:00:21,600
what those correlations mean when we

8
00:00:21,600 --> 00:00:25,630
apply them to real-world data

9
00:00:25,630 --> 00:00:29,199
so on my screen here i have a function

10
00:00:29,199 --> 00:00:31,929
that I&#39;ve created which i call auto dot

11
00:00:31,929 --> 00:00:33,970
core so it&#39;s going to compute the

12
00:00:33,970 --> 00:00:38,019
correlation between some variable sum in

13
00:00:38,019 --> 00:00:39,970
like in this case the defaults engine

14
00:00:39,970 --> 00:00:43,180
size and the price and we&#39;re first off

15
00:00:43,180 --> 00:00:46,480
we&#39;re going to use ggplot2 again to just

16
00:00:46,480 --> 00:00:49,600
make a scatter plot of those two

17
00:00:49,600 --> 00:00:52,300
variables and then we&#39;re going to

18
00:00:52,300 --> 00:00:55,089
compute the covariance using that are

19
00:00:55,089 --> 00:00:57,579
cold function

20
00:00:57,579 --> 00:01:01,719
and the correlation using the core

21
00:01:01,719 --> 00:01:03,550
function

22
00:01:03,550 --> 00:01:07,120
and we&#39;re going to just print print some

23
00:01:07,120 --> 00:01:10,360
summaries of that result so let me do

24
00:01:10,360 --> 00:01:13,530
that for the first case

25
00:01:14,990 --> 00:01:19,290
ok so coral covariance

26
00:01:19,290 --> 00:01:22,860
between engine size and price is around

27
00:01:22,860 --> 00:01:25,860
30,000 well it&#39;s positive it&#39;s a big

28
00:01:25,860 --> 00:01:27,090
number but what does that big number

29
00:01:27,090 --> 00:01:29,070
mean it&#39;s very hard to interpret in my

30
00:01:29,070 --> 00:01:32,110
view because

31
00:01:32,110 --> 00:01:34,960
we have price and its units and engine

32
00:01:34,960 --> 00:01:38,410
size and in units of cubic inches it&#39;s

33
00:01:38,410 --> 00:01:40,990
it&#39;s it&#39;s not clear what thirty thousand

34
00:01:40,990 --> 00:01:43,750
means is that highly is there a strong

35
00:01:43,750 --> 00:01:45,850
relationship or a weak relationship but

36
00:01:45,850 --> 00:01:48,460
correlation we have the advantage that

37
00:01:48,460 --> 00:01:50,290
we&#39;ve normalized by the variance of

38
00:01:50,290 --> 00:01:51,910
those variables and we can see it&#39;s just

39
00:01:51,910 --> 00:01:54,790
about almost point nine which is fairly

40
00:01:54,790 --> 00:01:58,300
strong correlation and if we look at the

41
00:01:58,300 --> 00:02:00,400
scatter plot of engine size on the

42
00:02:00,400 --> 00:02:04,360
vertical and price on the horizontal you

43
00:02:04,360 --> 00:02:07,900
can see that indeed there&#39;s a pretty

44
00:02:07,900 --> 00:02:10,600
strong and pretty almost straight line

45
00:02:10,600 --> 00:02:12,940
relationship between engine size and

46
00:02:12,940 --> 00:02:15,190
price of the car and that makes sense of

47
00:02:15,190 --> 00:02:19,390
babe a more expensive car with a is

48
00:02:19,390 --> 00:02:21,640
going to have a big engine or conversely

49
00:02:21,640 --> 00:02:25,090
large engines tend to be cost more so

50
00:02:25,090 --> 00:02:27,940
the cars are costs more

51
00:02:27,940 --> 00:02:30,880
let&#39;s look at another relationship here

52
00:02:30,880 --> 00:02:32,350
so this case we&#39;re going to look at the

53
00:02:32,350 --> 00:02:34,510
relationship between price and city

54
00:02:34,510 --> 00:02:37,190
miles per gallon

55
00:02:37,190 --> 00:02:40,730
and now we&#39;ve got numerically a larger

56
00:02:40,730 --> 00:02:45,500
at magnitude of covariance 36,000 as

57
00:02:45,500 --> 00:02:48,740
opposed to 30,000 before against clear

58
00:02:48,740 --> 00:02:50,570
what that means is that really a

59
00:02:50,570 --> 00:02:52,880
stronger relationship or not it&#39;s

60
00:02:52,880 --> 00:02:55,350
definitely negative

61
00:02:55,350 --> 00:02:57,480
but if i look at the correlation the

62
00:02:57,480 --> 00:03:00,690
normalized value its minus so negative

63
00:03:00,690 --> 00:03:03,780
point seven so the magnitude is quite a

64
00:03:03,780 --> 00:03:07,410
bit less for the relationship between

65
00:03:07,410 --> 00:03:10,200
City miles per gallon and price as we

66
00:03:10,200 --> 00:03:11,970
just saw between engine size and price

67
00:03:11,970 --> 00:03:15,210
and if i look at this plot i can see

68
00:03:15,210 --> 00:03:17,550
there&#39;s kind of a more of a curved

69
00:03:17,550 --> 00:03:19,900
relationship

70
00:03:19,900 --> 00:03:21,819
now that relationship makes sense again

71
00:03:21,819 --> 00:03:25,989
that small fuel-efficient cars that

72
00:03:25,989 --> 00:03:31,629
caught costless large gas guzzlers cost

73
00:03:31,629 --> 00:03:32,560
more

74
00:03:32,560 --> 00:03:37,120
okay fair enough but we&#39;re not properly

75
00:03:37,120 --> 00:03:39,340
with us with any sort of straight line

76
00:03:39,340 --> 00:03:41,290
type statistic like correlation we&#39;re

77
00:03:41,290 --> 00:03:43,390
not properly capturing this curvy

78
00:03:43,390 --> 00:03:46,080
relationship

79
00:03:46,080 --> 00:03:48,240
so I hope this little demo has given you

80
00:03:48,240 --> 00:03:51,060
some insight into the uses and

81
00:03:51,060 --> 00:03:52,890
limitations of covariance and

82
00:03:52,890 --> 00:03:55,320
correlation and how you can use them to

83
00:03:55,320 --> 00:03:59,540
gain some insight into your data sets

