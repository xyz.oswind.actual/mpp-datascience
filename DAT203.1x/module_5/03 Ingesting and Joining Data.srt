0
00:00:04,891 --> 00:00:06,940
Hi and welcome back!

1
00:00:06,940 --> 00:00:13,390
So we&#39;ve talked about data flow in data science experiments.

2
00:00:13,390 --> 00:00:15,889
We&#39;ve talked about joins.

3
00:00:15,889 --> 00:00:21,525
So let me show you a practical example of performing

4
00:00:21,525 --> 00:00:26,897
a join from a data flow that has two data sources,

5
00:00:26,897 --> 00:00:31,890
a data table and some reference data, okay?

6
00:00:33,780 --> 00:00:38,090
So you can see on my screen, this little experiment here.

7
00:00:39,210 --> 00:00:44,880
And we have over on the left, this Auto Data data set,

8
00:00:44,880 --> 00:00:51,826
and if you look at it, I&#39;ll just show you what&#39;s going on there.

9
00:00:55,848 --> 00:01:01,641
So we have various attributes of automobiles and

10
00:01:01,641 --> 00:01:05,040
their price, for example.

11
00:01:06,630 --> 00:01:07,740
But you can&#39;t tell,

12
00:01:07,740 --> 00:01:09,830
there&#39;s nothing here that tells you the manufacturer,

13
00:01:09,830 --> 00:01:12,630
there&#39;s just this Make_ID.

14
00:01:12,630 --> 00:01:16,570
So we need reference data to tell us

15
00:01:16,570 --> 00:01:21,094
what the manufacturer of that car is just given this Make_ID.

16
00:01:23,020 --> 00:01:27,630
And so I&#39;m using the Import Data module here.

17
00:01:27,630 --> 00:01:30,771
And we have a Azure SQL Database.

18
00:01:30,771 --> 00:01:33,230
So it&#39;s a SQL relational database.

19
00:01:34,480 --> 00:01:37,730
And that reference data is in a table there.

20
00:01:39,410 --> 00:01:43,323
And so what we have to do is configure this so

21
00:01:43,323 --> 00:01:45,294
we can connect to it.

22
00:01:45,294 --> 00:01:48,040
Now let me make this a little bigger so you can see it.

23
00:01:48,040 --> 00:01:52,937
So we have a database server name, it&#39;s just whatever it is.

24
00:01:52,937 --> 00:01:57,583
We have the actual database name which is autodata.

25
00:01:57,583 --> 00:02:00,560
We have our username, our password, and

26
00:02:00,560 --> 00:02:04,230
we&#39;re just gonna accept any server certificate.

27
00:02:05,230 --> 00:02:10,659
And then we have put here the SQL for our database query.

28
00:02:10,658 --> 00:02:13,660
It&#39;s a really simple select statement.

29
00:02:13,660 --> 00:02:15,260
We just want these two columns,

30
00:02:15,260 --> 00:02:18,620
the Make_ID, which is the thing we&#39;re gonna join on, and

31
00:02:18,620 --> 00:02:23,590
the Make, which is the name of the car manufacturer.

32
00:02:23,590 --> 00:02:27,730
And the table in this database is called auto_makes, so

33
00:02:27,730 --> 00:02:30,330
that&#39;s really all there is to it.

34
00:02:30,330 --> 00:02:32,333
Now, we wanna join those, so

35
00:02:32,333 --> 00:02:36,192
let&#39;s look at what&#39;s going on in this Join Data module.

36
00:02:36,192 --> 00:02:40,360
So we need to pick a key column for the left and the right.

37
00:02:41,910 --> 00:02:47,975
And all you do with the column selector is I&#39;ve gone down and

38
00:02:47,975 --> 00:02:52,843
I&#39;ve selected Make_ID, cuz that&#39;s my key.

39
00:02:52,843 --> 00:02:54,920
And I&#39;ve done the same thing again.

40
00:02:54,920 --> 00:02:57,890
So that&#39;s for the left and I&#39;ve done the same for the right.

41
00:02:59,220 --> 00:03:01,140
I&#39;m gonna match the case on that key.

42
00:03:02,410 --> 00:03:03,941
And I&#39;m gonna do an Inner Join,

43
00:03:03,941 --> 00:03:06,619
recal there&#39;s several types of joins we&#39;re gonna do.

44
00:03:06,619 --> 00:03:10,897
In this case, we&#39;re gonna do an Inner Join, so if we don&#39;t find

45
00:03:10,897 --> 00:03:14,720
a matching key we simply don&#39;t have a row at all in there.

46
00:03:16,100 --> 00:03:19,010
And we&#39;re gonna keep the extra key column in this case.

47
00:03:21,050 --> 00:03:28,549
So let me save and run that experiment.

48
00:03:28,549 --> 00:03:33,200
[COUGH] All right there, my experiment is run.

49
00:03:34,810 --> 00:03:35,600
So first off,

50
00:03:35,600 --> 00:03:39,770
let&#39;s look at what came in from this Import Data module.

51
00:03:39,770 --> 00:03:43,230
Remember, we were reading from a Azure SQL database.

52
00:03:43,230 --> 00:03:46,614
So we have the two columns, we have Make_ID, and

53
00:03:46,614 --> 00:03:48,314
there&#39;s 22 of those.

54
00:03:48,314 --> 00:03:54,850
And then we have 22 names of automobile manufacturers.

55
00:03:54,850 --> 00:03:59,749
So that&#39;s what we want it to join to this Auto Data data set.

56
00:03:59,749 --> 00:04:03,463
So let&#39;s look at the join result, and

57
00:04:03,463 --> 00:04:05,907
see that that happened.

58
00:04:05,907 --> 00:04:09,298
So again, here&#39;s where we started, with a Make_ID,

59
00:04:09,298 --> 00:04:11,110
with our 22 unique values.

60
00:04:11,110 --> 00:04:12,998
And I&#39;m just gonna scroll all the way over here.

61
00:04:12,998 --> 00:04:16,872
We have the second Make_ID, which was from that column we

62
00:04:16,872 --> 00:04:20,230
imported from the SQL Server database.

63
00:04:20,230 --> 00:04:24,860
And we have now the make of the car, so we can now tell

64
00:04:26,880 --> 00:04:30,123
which manufacturer made which cars, as a result of that join.

65
00:04:32,070 --> 00:04:36,570
So I hope this little demo has given you some idea about

66
00:04:36,570 --> 00:04:40,270
data flow in data science experiments, and

67
00:04:40,270 --> 00:04:44,870
also some practical ideas about how we might use join,

68
00:04:44,870 --> 00:04:49,083
say, to join reference data to another dataset.

