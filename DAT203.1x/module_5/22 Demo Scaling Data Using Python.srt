0
00:00:05,225 --> 00:00:10,087
Hi, so in this sequence, I&#39;m going to show you how to

1
00:00:10,087 --> 00:00:14,690
scale using the tools available to you in Python.

2
00:00:14,690 --> 00:00:17,200
So we&#39;ve already scaled

3
00:00:17,200 --> 00:00:21,720
using the tool in Azure Machine Learning, and this will give you

4
00:00:21,720 --> 00:00:25,750
a parallel way of scaling using the Python environment.

5
00:00:26,910 --> 00:00:30,590
If you&#39;re in the R track, you may wish to skip this video and

6
00:00:30,590 --> 00:00:34,400
view the one doing exactly the same thing with the tools in R.

7
00:00:34,400 --> 00:00:40,110
So on my screen here, I&#39;ve added two execute Python

8
00:00:40,110 --> 00:00:46,010
script modules to this now rather complicated experiment.

9
00:00:46,010 --> 00:00:49,060
So of course, along the right side here we have all

10
00:00:49,060 --> 00:00:51,990
the Azure Machine Learning modules

11
00:00:51,990 --> 00:00:53,940
where all the data prep has been done.

12
00:00:55,160 --> 00:00:59,954
And I have these two new execute Python script modules here,

13
00:00:59,954 --> 00:01:03,159
and let me show you what they&#39;re doing.

14
00:01:03,159 --> 00:01:06,002
So we&#39;ll look inside here.

15
00:01:06,002 --> 00:01:10,008
So we already talked about this id_outlier function in

16
00:01:10,008 --> 00:01:13,940
a previous sequence when we were creating the graphs.

17
00:01:15,270 --> 00:01:18,280
So all that&#39;s going on here now is very simple.

18
00:01:18,280 --> 00:01:23,815
So in my main function, my azureml_main I just call

19
00:01:23,815 --> 00:01:30,710
id_outlier with the data frame as an argument.

20
00:01:30,710 --> 00:01:35,050
I get the marked data frame back, and then I just return

21
00:01:35,050 --> 00:01:39,710
the data frame where dataframe.outlier is equal to 0,

22
00:01:39,710 --> 00:01:45,280
so there&#39;s the rows that are not outliers.

23
00:01:45,280 --> 00:01:46,820
So that&#39;s simple enough.

24
00:01:48,410 --> 00:01:50,880
So then, in this execute Python script,

25
00:01:50,880 --> 00:01:55,030
all I do is apply that scale.

26
00:01:55,030 --> 00:02:01,600
So I just have this one main azureML_main function.

27
00:02:01,600 --> 00:02:03,540
And notice what I&#39;m doing here,

28
00:02:03,540 --> 00:02:09,240
I&#39;m importing from scikit-learn pre-processing as PR, okay?

29
00:02:10,830 --> 00:02:13,570
And I create a list of the numeric

30
00:02:13,570 --> 00:02:16,580
columns that I wanna scale, there&#39;s quite a few.

31
00:02:18,070 --> 00:02:20,610
Now it&#39;s a little tricky here, so

32
00:02:20,610 --> 00:02:25,180
the preprocessing.scale function works on a numpy array.

33
00:02:25,180 --> 00:02:30,400
It will not work on a Pandas data frame or

34
00:02:30,400 --> 00:02:36,110
a Pandas series, which is a single Pandas column.

35
00:02:37,500 --> 00:02:45,920
So, to get it, the data frame into the form we need it,

36
00:02:45,920 --> 00:02:50,080
I first off select those columns from the data frame.

37
00:02:50,080 --> 00:02:54,185
You see df[num_cols] and

38
00:02:54,185 --> 00:02:59,690
then I apply this as_matrix method.

39
00:02:59,690 --> 00:03:04,480
So that gives me a NumPy array, and

40
00:03:04,480 --> 00:03:09,610
then I apply preprocessing.scale to that array.

41
00:03:09,610 --> 00:03:13,560
And make sure the axis is set to 0, so that I&#39;m scaling

42
00:03:13,560 --> 00:03:17,710
along the columns, not on the rows or something else.

43
00:03:17,710 --> 00:03:22,250
And then I simply assign that back into those numeric columns

44
00:03:22,250 --> 00:03:26,220
of the panda&#39;s data frame, and return the data frame.

45
00:03:28,340 --> 00:03:30,580
So that&#39;s fairly simple as well.

46
00:03:30,580 --> 00:03:32,720
So let me just save this experiment, and

47
00:03:32,720 --> 00:03:33,560
I&#39;ll run it for you.

48
00:03:37,110 --> 00:03:38,560
And there it&#39;s run successfully.

49
00:03:40,110 --> 00:03:42,364
So let&#39;s visualize the output here.

50
00:03:46,882 --> 00:03:49,090
And I&#39;m gonna scroll all the way over to the right, so

51
00:03:49,090 --> 00:03:50,820
we can see some of these numeric columns.

52
00:03:50,820 --> 00:03:52,640
Select highway miles per gallon.

53
00:03:52,640 --> 00:03:57,267
Can see from the histogram it has a range of -2 to

54
00:03:57,267 --> 00:04:02,450
2.7 city miles per gallon -1.6 to 2.3,

55
00:04:02,450 --> 00:04:06,590
and you can see 0 as sort of in there.

56
00:04:06,590 --> 00:04:08,940
So it looks like the scalings worked

57
00:04:10,060 --> 00:04:15,230
that they&#39;re 0 centered and have unit standard deviation.

58
00:04:15,230 --> 00:04:21,180
Again, this one is -2.1, 3.2 for peak rpm,

59
00:04:21,180 --> 00:04:26,466
horsepower, compression ratio, etc, so.

60
00:04:29,050 --> 00:04:32,909
So now you&#39;ve seen how to scale data with

61
00:04:32,909 --> 00:04:36,315
the Azure Machine Learning tool and

62
00:04:36,315 --> 00:04:41,779
using the tool from the Python scikit-learn packages.

63
00:04:42,810 --> 00:04:45,978
So that gives you two ways to do it, and so for

64
00:04:45,978 --> 00:04:48,730
more flexibility with your tool kit.

