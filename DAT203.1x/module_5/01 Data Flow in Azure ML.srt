0
00:00:05,092 --> 00:00:07,030
Hi and welcome.

1
00:00:07,030 --> 00:00:10,800
In this sequence, I&#39;m going to talk about data sources,

2
00:00:10,800 --> 00:00:13,270
data formats and data flow.

3
00:00:15,020 --> 00:00:17,970
So on my screen here, I have

4
00:00:17,970 --> 00:00:22,000
some typical formats you may deal with as a data scientist.

5
00:00:23,230 --> 00:00:26,969
Most of the time, if you are just experimenting,

6
00:00:26,969 --> 00:00:31,510
testing algorithms, or models, or whatever you&#39;ve got,

7
00:00:31,510 --> 00:00:35,530
graphics, you&#39;re gonna be working with text data.

8
00:00:35,530 --> 00:00:38,190
It could be just plain text files.

9
00:00:38,190 --> 00:00:39,600
It could be delimited.

10
00:00:39,600 --> 00:00:42,930
For example the common delimited file,

11
00:00:42,930 --> 00:00:45,830
CSV file is pretty ubiquitous.

12
00:00:46,860 --> 00:00:51,190
But in other cases, if you have larger data sets or

13
00:00:51,190 --> 00:00:55,540
production quality data that&#39;s part of a larger system,

14
00:00:55,540 --> 00:00:58,130
you may have other sources.

15
00:00:58,130 --> 00:01:04,110
For example, hive tables, hive being the database

16
00:01:04,110 --> 00:01:09,710
part of a dupe SQL Database tables from various

17
00:01:09,710 --> 00:01:14,120
relational databases and some other formats that you may run

18
00:01:14,120 --> 00:01:16,290
into that are more specialized.

19
00:01:17,330 --> 00:01:20,960
And those come from a variety of sources.

20
00:01:20,960 --> 00:01:27,680
Typically text comes from local files, but not always.

21
00:01:27,680 --> 00:01:33,350
An example of a hive tables could be from Azure,

22
00:01:33,350 --> 00:01:37,200
you could have blobs and Azure tables.

23
00:01:37,200 --> 00:01:43,730
There&#39;s feeds over HTTP from say URLs or

24
00:01:43,730 --> 00:01:49,430
other kinds of data feeds and lots of other SQL server,

25
00:01:49,430 --> 00:01:52,690
lots of other possible data sources here.

26
00:01:54,540 --> 00:01:57,240
So let&#39;s look at the data flow.

27
00:01:58,870 --> 00:02:04,270
So this box here is kind of conceptually our experiment,

28
00:02:04,270 --> 00:02:06,840
or an Azure ML experiment.

29
00:02:06,840 --> 00:02:08,420
And we get a Dataset there.

30
00:02:10,199 --> 00:02:11,550
And we&#39;re gonna do something with it,

31
00:02:11,550 --> 00:02:13,950
we&#39;re gonna do some Transformations, etc.

32
00:02:16,260 --> 00:02:19,382
And the goal is to create a Scored Model.

33
00:02:21,930 --> 00:02:26,091
But we may have some other reference data, so

34
00:02:26,091 --> 00:02:30,034
reference data could be things like look up

35
00:02:30,034 --> 00:02:34,525
a customer&#39;s name given their account number,

36
00:02:34,525 --> 00:02:39,234
look up the city given some sort of reference code,

37
00:02:39,234 --> 00:02:42,980
maybe a zip code or something like that.

38
00:02:42,980 --> 00:02:47,410
There&#39;s lots of possible things that a Reference Data can mean.

39
00:02:48,450 --> 00:02:53,785
But we need to use the Import Data module in Azure ML,

40
00:02:53,785 --> 00:02:59,607
we import from those outside sources whether it&#39;s SQL,

41
00:02:59,607 --> 00:03:00,956
hive, etc.

42
00:03:00,956 --> 00:03:06,892
And then we can do say adjoin in our Transformations to bind that

43
00:03:06,892 --> 00:03:12,380
Reference Data to the correct bits of our input data cell.

44
00:03:14,460 --> 00:03:15,160
And finally,

45
00:03:15,160 --> 00:03:19,790
when we go to production, we&#39;re gonna create web services.

46
00:03:19,790 --> 00:03:24,380
We&#39;re gonna publish this scored model as Web Service, and so

47
00:03:24,380 --> 00:03:28,860
we&#39;ll have a web service input as you see and

48
00:03:28,860 --> 00:03:31,210
that will be where the data is coming from.

49
00:03:31,210 --> 00:03:34,820
We&#39;re still gonna have our Reference Data,

50
00:03:34,820 --> 00:03:37,630
which we&#39;re gonna importing and using and joining.

51
00:03:37,630 --> 00:03:40,770
We&#39;re still gonna have our scored model and

52
00:03:40,770 --> 00:03:45,740
then we&#39;re gonna publish the web services output to our users.

53
00:03:47,790 --> 00:03:53,960
So very briefly that was an overview of data type,

54
00:03:53,960 --> 00:03:58,060
data sources and the data flow for

55
00:03:58,060 --> 00:04:01,210
Azure ML and I should mention that that data flow, even though

56
00:04:01,210 --> 00:04:05,330
I was specifically talking about Azure ML, is very general.

57
00:04:05,330 --> 00:04:09,980
Lots and lots of different platforms used for data science

58
00:04:09,980 --> 00:04:14,840
and machine learning have essentially that same data flow.

59
00:04:14,840 --> 00:04:20,219
So there&#39;s nothing particularly special about

60
00:04:20,219 --> 00:04:24,947
Azure ML in that general schematic level.

