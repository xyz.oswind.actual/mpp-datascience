0
00:00:04,903 --> 00:00:09,712
So you&#39;ve just seen how to do a left join using the tools

1
00:00:09,712 --> 00:00:12,780
in Azure Machine Learning.

2
00:00:12,780 --> 00:00:16,860
So let&#39;s look about doing exactly the same join

3
00:00:16,860 --> 00:00:20,680
using the tools in R dplyr.

4
00:00:20,680 --> 00:00:25,563
So I have added to this experiment we started.

5
00:00:25,563 --> 00:00:28,825
You see we have our join here, which we are already did, and

6
00:00:28,825 --> 00:00:32,021
our two pieces of data that we need to put together so that we

7
00:00:32,021 --> 00:00:34,780
know what the manufacturer of the automobile is.

8
00:00:36,050 --> 00:00:39,076
And let&#39;s look inside this Execute R Script,

9
00:00:39,076 --> 00:00:41,022
at the code I&#39;ve put in here.

10
00:00:41,022 --> 00:00:46,905
So I&#39;ve created a little function called join.auto,

11
00:00:46,905 --> 00:00:52,161
and it doesn&#39;t do that much, it just takes the two

12
00:00:52,161 --> 00:00:56,917
data frames, it makes sure dplyr is loaded,

13
00:00:56,917 --> 00:01:01,796
and it does a left join of the two tables by, and

14
00:01:01,796 --> 00:01:08,570
in this case, the key column name is the same autonum.

15
00:01:08,570 --> 00:01:13,295
And then I just have some other code here, I have autos and

16
00:01:13,295 --> 00:01:18,324
makes, are two data frames loaded from port one, port two.

17
00:01:18,324 --> 00:01:23,726
And I create this out data frame by doing

18
00:01:23,726 --> 00:01:29,470
the calling join.autos and I output it.

19
00:01:29,470 --> 00:01:30,970
So this is pretty simple.

20
00:01:32,870 --> 00:01:37,334
And I&#39;ll just save it for you and run it for you.

21
00:01:41,654 --> 00:01:43,852
And there, the experiment has run, and

22
00:01:43,852 --> 00:01:45,640
let&#39;s look at that output.

23
00:01:45,640 --> 00:01:48,413
We&#39;ll visualize that output and

24
00:01:48,413 --> 00:01:52,580
make sure things work the way we expected them to.

25
00:01:52,580 --> 00:01:58,368
So all the columns from the autos file.

26
00:02:00,288 --> 00:02:05,329
And we get all the way over here, and we have autonum,

27
00:02:05,329 --> 00:02:11,280
we&#39;ve got this extra column, and there we&#39;ve got the makes.

28
00:02:13,285 --> 00:02:17,055
So we&#39;ve got 22 unique features of make, and

29
00:02:17,055 --> 00:02:19,440
it&#39;s a string feature.

30
00:02:19,440 --> 00:02:23,120
And we have, as we expected,

31
00:02:23,120 --> 00:02:27,400
208 rows and now 29 columns.

32
00:02:27,400 --> 00:02:30,620
So exactly what we got when we did the other join.

33
00:02:33,210 --> 00:02:39,616
So that gives you an alternative way of doing a join using R.

34
00:02:39,616 --> 00:02:43,100
So now you know how to do it in Azure Machine Learning or in R.

