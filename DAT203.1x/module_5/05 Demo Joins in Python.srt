0
00:00:04,970 --> 00:00:08,452
Hi, so you&#39;ve just seen how to do a join using the tools

1
00:00:08,452 --> 00:00:12,238
available in Azure Machine Learning which is, as you saw,

2
00:00:12,238 --> 00:00:13,163
very simple.

3
00:00:13,163 --> 00:00:16,325
But there may be occasions where you wanna do the same join in

4
00:00:16,325 --> 00:00:17,800
Python.

5
00:00:17,800 --> 00:00:20,790
And so, let&#39;s talk about how that can be done.

6
00:00:20,790 --> 00:00:24,210
So my screen here I have our experiment,

7
00:00:24,210 --> 00:00:28,070
but I&#39;ve added an execute Python script module.

8
00:00:28,070 --> 00:00:30,500
And I&#39;ll just show you the code inside there and

9
00:00:31,600 --> 00:00:33,330
it&#39;s pretty short actually.

10
00:00:34,560 --> 00:00:42,720
So I&#39;ve got this function, auto joins and import pandas as pd.

11
00:00:42,720 --> 00:00:49,210
And I do it and I set a name autonum.

12
00:00:49,210 --> 00:00:50,450
Notice it&#39;s in a list.

13
00:00:50,450 --> 00:00:53,908
So if I had multiple key columns I could just keep adding them to

14
00:00:53,908 --> 00:00:57,515
that list and so here&#39;s where the real work gets done, though.

15
00:00:57,515 --> 00:01:01,140
I return pd.merge of autos,

16
00:01:01,140 --> 00:01:07,960
makes on whatever that list is and how it&#39;s a left join.

17
00:01:09,170 --> 00:01:11,730
So all I have to do in my main

18
00:01:11,730 --> 00:01:15,700
is just return the result of that.

19
00:01:18,550 --> 00:01:21,382
So very easy, let me just save this.

20
00:01:25,286 --> 00:01:31,173
And run it for you, and it&#39;s run looks like successfully.

21
00:01:31,173 --> 00:01:34,838
So let&#39;s visualize that output and

22
00:01:34,838 --> 00:01:38,390
make sure we got what we expected.

23
00:01:38,390 --> 00:01:42,980
So notice we got 208 rows, 29 columns so that&#39;s looking good,

24
00:01:42,980 --> 00:01:44,810
and I keep scrolling over.

25
00:01:44,810 --> 00:01:49,290
So these are all the columns from the autos file.

26
00:01:50,460 --> 00:01:53,870
And I scroll all the way over here, and there&#39;s columns

27
00:01:53,870 --> 00:01:58,670
in the automobile make from the make file.

28
00:01:59,760 --> 00:02:04,300
And I got my 22 unique values there for

29
00:02:04,300 --> 00:02:10,330
automotive makers, and so there&#39;s obviously some,

30
00:02:10,330 --> 00:02:13,510
we have that same issue with the left joint, where we have now

31
00:02:13,510 --> 00:02:18,680
some duplicate rows, which we don&#39;t know what they are.

32
00:02:18,680 --> 00:02:22,620
But the important point is we got the same result.

33
00:02:23,810 --> 00:02:27,580
So now just to summarize you&#39;ve seen how to do joins two

34
00:02:27,580 --> 00:02:31,550
different ways using the tool in Azure Machine Learning and

35
00:02:31,550 --> 00:02:38,330
using the joined or merged method in Python Pandas.

36
00:02:38,330 --> 00:02:38,910
So now you have

37
00:02:38,910 --> 00:02:41,770
two different tools you can use whenever you need to do a join.

38
00:02:41,770 --> 00:02:43,912
So I hope that&#39;s helpful to you.

