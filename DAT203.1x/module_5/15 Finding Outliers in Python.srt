0
00:00:04,827 --> 00:00:09,104
Hi, so in the previous sequence I&#39;ve

1
00:00:09,104 --> 00:00:14,056
talked about the idea of binding outliers.

2
00:00:14,056 --> 00:00:18,133
How we find them, validate maybe that we have an outlier,

3
00:00:18,133 --> 00:00:20,058
and possible treatments.

4
00:00:20,058 --> 00:00:25,423
So in this sequence, we&#39;re gonna look at finding outliers

5
00:00:25,423 --> 00:00:30,320
using Python tools and mostly visualization tools.

6
00:00:30,320 --> 00:00:33,350
So I have on my screen here

7
00:00:34,720 --> 00:00:37,490
some code to help us get started on that.

8
00:00:37,490 --> 00:00:40,330
So the first function in this little bit of code

9
00:00:40,330 --> 00:00:45,030
is auto_scatter_simple,

10
00:00:45,030 --> 00:00:47,760
that just is, it is actually fairly simple.

11
00:00:47,760 --> 00:00:50,710
It has two arguments, a data frame and

12
00:00:50,710 --> 00:00:55,680
a set of columns I wanna plot and the usual import.

13
00:00:55,680 --> 00:01:02,299
And I loop through that list of columns and create a figure.

14
00:01:02,299 --> 00:01:06,622
And I&#39;m gonna make a scatter plot of price versus

15
00:01:06,622 --> 00:01:11,900
whatever the column is that I&#39;m iterating over.

16
00:01:11,900 --> 00:01:17,600
And I&#39;m gonna make it dark blue and give it a title.

17
00:01:18,980 --> 00:01:23,900
And so here in my azureml_main function I have my

18
00:01:23,900 --> 00:01:26,680
plot columns defined, just three we&#39;re gonna look at,

19
00:01:26,680 --> 00:01:28,930
curb weight, engine size, city mpg.

20
00:01:30,270 --> 00:01:36,720
And then I call that auto.scatter.

21
00:01:36,720 --> 00:01:39,060
So let&#39;s go to Azure ML and look at this.

22
00:01:40,070 --> 00:01:42,700
So I&#39;ve already copied and

23
00:01:42,700 --> 00:01:48,660
pasted that code into an execute Python script module here.

24
00:01:48,660 --> 00:01:53,230
And you can see I have connected that to the output of that

25
00:01:54,410 --> 00:01:56,420
module where we did the processing.

26
00:01:58,430 --> 00:02:00,980
So let me just save this experiment and run it, and

27
00:02:00,980 --> 00:02:02,320
we can talk about the results.

28
00:02:05,780 --> 00:02:08,910
Okay, the experiment has run, so let me first

29
00:02:11,950 --> 00:02:15,710
visualize the output from the Python device port.

30
00:02:17,260 --> 00:02:19,280
And I have a series of scatter plots here.

31
00:02:19,280 --> 00:02:22,724
This first one is price versus city MPG.

32
00:02:22,724 --> 00:02:25,396
So, notice there are some outliers here, but

33
00:02:25,396 --> 00:02:26,775
they are more in price.

34
00:02:26,775 --> 00:02:30,424
They overlap a lot in terms of city MPG.

35
00:02:30,424 --> 00:02:34,256
But there these three cars that have extremely high mileage,

36
00:02:34,256 --> 00:02:37,350
extremely high fuel efficiency.

37
00:02:37,350 --> 00:02:39,710
And they don&#39;t seem to be on any of the other trends.

38
00:02:39,710 --> 00:02:41,940
So those are good candidates for outliers.

39
00:02:41,940 --> 00:02:46,777
So, maybe we can say any automobile with fuel efficiency

40
00:02:46,777 --> 00:02:48,500
greater than 40.

41
00:02:48,500 --> 00:02:51,530
What about curb weight?

42
00:02:53,040 --> 00:02:56,860
Well again, we&#39;ve got this cluster here.

43
00:02:56,860 --> 00:03:00,230
And then these out here we&#39;ve got a number of really heavy

44
00:03:00,230 --> 00:03:03,488
cars that seem to be a world under themselves.

45
00:03:03,488 --> 00:03:08,177
Maybe we could say any curb weight over about 3500 is

46
00:03:08,177 --> 00:03:11,985
there&#39;s something different or odd about.

47
00:03:11,985 --> 00:03:14,140
And we still have these three points here.

48
00:03:15,430 --> 00:03:17,070
So let&#39;s keep going.

49
00:03:17,070 --> 00:03:18,881
Also some very light cars,

50
00:03:18,881 --> 00:03:22,431
maybe that&#39;s one of our highly fuel efficient cars.

51
00:03:22,431 --> 00:03:25,176
So here&#39;s price versus engine size.

52
00:03:25,176 --> 00:03:28,030
And this one gives a rather dramatic break.

53
00:03:28,030 --> 00:03:32,090
So we have a kind of a group that goes like this maybe and

54
00:03:32,090 --> 00:03:35,080
then all of a sudden this cluster here.

55
00:03:35,080 --> 00:03:39,311
So, any engine size over maybe a 190,

56
00:03:39,311 --> 00:03:43,250
200, something like that might be an outlier.

57
00:03:44,670 --> 00:03:48,495
So, that&#39;s a couple of hypothesis we can test and

58
00:03:48,495 --> 00:03:52,501
of course I just happen to have code here to test those

59
00:03:52,501 --> 00:03:53,605
hypothesis.

60
00:03:55,815 --> 00:04:03,420
So this function called id_outlier basically does that.

61
00:04:03,420 --> 00:04:05,086
It&#39;ll go through and for

62
00:04:05,086 --> 00:04:09,300
each of those conditions I just noticed marked tag an outlier.

63
00:04:09,300 --> 00:04:15,080
So I&#39;m gonna create a list called temp and I&#39;m gonna

64
00:04:15,080 --> 00:04:20,580
have a value of 0 times the number of rows in my data frame.

65
00:04:22,710 --> 00:04:25,740
And then I&#39;m just go through a series of for loops here,

66
00:04:25,740 --> 00:04:30,610
it&#39;s kind of a brute force approach where I enumerate

67
00:04:30,610 --> 00:04:35,530
over engine-size, curb-weight and city-mpg.

68
00:04:35,530 --> 00:04:40,160
And if the value x, so if you don&#39;t know it, enumerate.

69
00:04:40,160 --> 00:04:42,160
Let&#39;s reiterate over those values but

70
00:04:42,160 --> 00:04:46,440
also gives me an indicator value or an index value.

71
00:04:46,440 --> 00:04:48,090
Here up I&#39;m calling it i.

72
00:04:48,090 --> 00:04:51,680
So if x is greater than 190 for example for engine size,

73
00:04:51,680 --> 00:04:54,690
then temp i gets one, etc.

74
00:04:54,690 --> 00:04:58,690
Enumerate curb-weight so x over 3500.

75
00:04:58,690 --> 00:05:02,480
Enumerate city-mpg x over 40.

76
00:05:02,480 --> 00:05:05,870
So enumerate&#39;s kind of a cool thing if you

77
00:05:05,870 --> 00:05:07,090
don&#39;t know about it already.

78
00:05:08,240 --> 00:05:14,970
And then I have this other code here called auto_scatter_outlier

79
00:05:14,970 --> 00:05:19,650
so you can imagine I&#39;m gonna make outliers or

80
00:05:19,650 --> 00:05:22,815
plots of the outliers so we can test my hypothesis.

81
00:05:22,815 --> 00:05:26,504
And so it&#39;s a little tricky here, so

82
00:05:26,504 --> 00:05:30,078
I&#39;ve got these four lists, okay?

83
00:05:30,078 --> 00:05:35,237
So I&#39;ve got outlier equal 0,0,1,1.

84
00:05:35,237 --> 00:05:38,180
Fuel equals gas diesel, gas diesel.

85
00:05:38,180 --> 00:05:41,460
And color equals dark blue, dark blue, red, red.

86
00:05:41,460 --> 00:05:43,910
And marker equals x, o, o, x.

87
00:05:43,910 --> 00:05:48,150
So the idea is, I&#39;m gonna come up with a plot with a couple

88
00:05:48,150 --> 00:05:52,510
different sets of points depending on some values here.

89
00:05:52,510 --> 00:05:57,614
So, of outlier, not whether a car is gas or diesel.

90
00:05:57,614 --> 00:06:04,803
So, let me explain that,

91
00:06:04,803 --> 00:06:07,690
and hopefully this will make sense to you.

92
00:06:07,690 --> 00:06:11,170
So, I iterate over my plot calls, I get a column, and

93
00:06:11,170 --> 00:06:13,140
I set up my figures, I always do.

94
00:06:14,510 --> 00:06:17,330
And so, this is a for loop.

95
00:06:18,590 --> 00:06:22,280
So remember I had these four lists, so

96
00:06:22,280 --> 00:06:23,870
I zip those four lists.

97
00:06:27,140 --> 00:06:31,530
And so I have four, and then I have O for outlier, F for

98
00:06:31,530 --> 00:06:34,380
fuel, C for color, M for marker,

99
00:06:34,380 --> 00:06:38,330
N the zipped combination of outlier fuel color marker.

100
00:06:38,330 --> 00:06:43,070
So now, I have four variables that I&#39;m iterating

101
00:06:43,070 --> 00:06:48,190
over in my for loop because I&#39;ve zipped those four, and

102
00:06:48,190 --> 00:06:52,430
by the way they have to be equal length, of course, list.

103
00:06:55,880 --> 00:06:59,680
So I&#39;ll set up a temp here, and if temp of

104
00:07:00,950 --> 00:07:05,965
that beta frame, I&#39;m using the IX, the indicator, method,

105
00:07:05,965 --> 00:07:10,428
if the logic is if DF outlier equals

106
00:07:10,428 --> 00:07:16,500
zero and DF, Fuel type equals zero,

107
00:07:16,500 --> 00:07:19,470
o sorry, remember o is one of my iterators.

108
00:07:19,470 --> 00:07:24,000
And df fuel type equals f, which is one of my fuel types.

109
00:07:24,000 --> 00:07:25,870
So you can see how that&#39;s gonna go.

110
00:07:25,870 --> 00:07:30,759
So I&#39;m gonna go, outlier, no, gas.

111
00:07:31,950 --> 00:07:34,000
Outlier no diesel.

112
00:07:34,000 --> 00:07:35,490
Outlier yes, gas.

113
00:07:35,490 --> 00:07:37,466
Outlier yes, diesel.

114
00:07:37,466 --> 00:07:41,490
So then, I&#39;m gonna set the shape.

115
00:07:43,000 --> 00:07:48,020
Remember I had a shape which

116
00:07:48,020 --> 00:07:52,730
is a color or the marker is the shape.

117
00:07:53,810 --> 00:07:55,140
And the color is m.

118
00:07:56,490 --> 00:08:00,430
And so I make sure, and I also make sure this temp.

119
00:08:02,560 --> 00:08:05,380
Two things about shape, there&#39;s a shape that&#39;s the marker

120
00:08:05,380 --> 00:08:09,890
I&#39;m gonna use on the plot and then there&#39;s the shape of

121
00:08:09,890 --> 00:08:13,700
this data frame that&#39;s been subset here, okay?

122
00:08:13,700 --> 00:08:17,480
So, don&#39;t get confused by that

123
00:08:17,480 --> 00:08:20,630
unfortunate dual terminology here.

124
00:08:20,630 --> 00:08:24,920
So, I make sure that there&#39;s at least some rows,

125
00:08:24,920 --> 00:08:29,060
that I have more than zero rows in my temp data frame.

126
00:08:30,660 --> 00:08:34,990
And so you can see, for example, if it&#39;s gas, so

127
00:08:34,990 --> 00:08:36,550
if it&#39;s not an outlier and

128
00:08:36,549 --> 00:08:41,390
it&#39;s gas, I&#39;m gonna use dark blue and x as my marker.

129
00:08:41,390 --> 00:08:44,030
If it&#39;s not an outlier but it&#39;s a diesel,

130
00:08:44,030 --> 00:08:47,090
I&#39;m gonna use dark blue and an o as my marker etc.

131
00:08:47,090 --> 00:08:49,910
So that&#39;s how this is gonna work.

132
00:08:52,830 --> 00:08:56,180
So that I set my title and I save the figure.

133
00:08:56,180 --> 00:09:00,160
You are probably glad that we are through that.

134
00:09:00,160 --> 00:09:05,160
Then, my main function here is pretty simple thankfully.

135
00:09:05,160 --> 00:09:07,630
So I just defined my plot columns.

136
00:09:07,630 --> 00:09:09,576
I call that function that tells me,

137
00:09:09,576 --> 00:09:16,150
that tags the outliers based on my rules I&#39;ve hypothesized here.

138
00:09:16,150 --> 00:09:19,920
And then I call the scatter plot function and I return,

139
00:09:19,920 --> 00:09:21,900
notice I&#39;m returning a data frame

140
00:09:22,900 --> 00:09:25,210
with only the values where outliers went.

141
00:09:25,210 --> 00:09:28,195
So where it is in fact an outlier, okay.

142
00:09:28,195 --> 00:09:34,234
So let me copy all that code and I&#39;m gonna go back to Azure ML.

143
00:09:34,234 --> 00:09:39,610
And I&#39;m gonna find my execute Python script module here.

144
00:09:39,610 --> 00:09:44,573
And I&#39;ll connect the results data set of that previous module

145
00:09:44,573 --> 00:09:46,968
to the data set 1 input port.

146
00:09:46,968 --> 00:09:51,341
And copy and paste in that code I just walked you through,

147
00:09:55,285 --> 00:09:57,871
And now I&#39;m gonna run it.

148
00:09:57,871 --> 00:10:00,523
And hopefully I haven&#39;t made any mistakes and it will just go.

149
00:10:05,777 --> 00:10:07,510
Okay, it looks like I have succeeded.

150
00:10:07,510 --> 00:10:10,720
The experiment has run and

151
00:10:10,720 --> 00:10:13,610
let&#39;s visualize that output and see what we can make of it.

152
00:10:15,520 --> 00:10:19,184
So, here is our scatter plot of city, of price.

153
00:10:19,184 --> 00:10:21,546
Sorry, versus city miles per gallon.

154
00:10:21,546 --> 00:10:24,455
And yeah those ones that look like outliers,

155
00:10:24,455 --> 00:10:26,829
notice they do show up as outliers and

156
00:10:26,829 --> 00:10:30,757
we&#39;ve got a couple of xs here indicating gasoline engines.

157
00:10:30,757 --> 00:10:33,540
So there&#39;s a diesel here, so, see there&#39;s

158
00:10:33,540 --> 00:10:38,660
an overlap of the diesel, bunch of diesel cars here.

159
00:10:38,660 --> 00:10:43,860
But the three gasoline cars there and then these.

160
00:10:43,860 --> 00:10:46,400
I&#39;m sorry these three diesel cars here and

161
00:10:46,400 --> 00:10:48,560
then these gasoline cars up here.

162
00:10:50,390 --> 00:10:54,347
And then a diesel and two gasoline cars down there

163
00:10:54,347 --> 00:10:58,240
are pretty definitely seem to be outliers.

164
00:10:58,240 --> 00:11:00,860
And we see a similar pattern here where we have

165
00:11:02,200 --> 00:11:06,870
high priced cars mostly gasoline and

166
00:11:06,870 --> 00:11:11,470
a few diesels that happen to have high curb weight.

167
00:11:11,470 --> 00:11:13,165
Those look to be outliers.

168
00:11:15,085 --> 00:11:21,281
And then finally, we have some gasoline cars and a few diesels,

169
00:11:21,281 --> 00:11:26,217
these three diesels, which have very large engine

170
00:11:26,217 --> 00:11:31,520
sizes that also may indicate that they&#39;re outliers.

171
00:11:32,740 --> 00:11:36,090
And here&#39;s our three high fuel-efficiency cars down here.

172
00:11:36,090 --> 00:11:38,450
Okay, so, that&#39;s interesting.

173
00:11:38,450 --> 00:11:41,720
So, let&#39;s look at the other part of this.

174
00:11:41,720 --> 00:11:49,060
Recall, we output as a Panda&#39;s data frame only the cars that

175
00:11:49,060 --> 00:11:52,510
we thought might be outliers based on my initial hypothesis.

176
00:11:52,510 --> 00:11:54,980
So, let&#39;s look at the manufacturers.

177
00:11:54,980 --> 00:11:59,620
So, the first three are BMWs, then we Chevrolet and

178
00:11:59,620 --> 00:12:04,340
a Honda, which are probably the high efficiency cars.

179
00:12:04,340 --> 00:12:07,410
Then we have some Jaguars, Mercedes-Benz,

180
00:12:07,410 --> 00:12:08,880
quite a few Mercedez-Benz.

181
00:12:08,880 --> 00:12:12,980
A Nissan, which I think is the high efficiency diesel.

182
00:12:12,980 --> 00:12:14,260
And then three Porsches.

183
00:12:14,260 --> 00:12:15,960
So what do we make of this?

184
00:12:15,960 --> 00:12:21,470
Well, It seems to be the case that luxury cars

185
00:12:21,470 --> 00:12:26,460
might have their own behavior for wherein they

186
00:12:26,460 --> 00:12:31,500
don&#39;t follow the price rules of other cars because of the nature

187
00:12:31,500 --> 00:12:33,970
of how they&#39;re made and how they&#39;re sold and all that.

188
00:12:33,970 --> 00:12:37,190
There are also these three high efficiency cars, which,

189
00:12:37,190 --> 00:12:39,990
again don&#39;t seem to follow the rules.

190
00:12:39,990 --> 00:12:44,030
So, when you&#39;re modeling, it might be important to

191
00:12:44,030 --> 00:12:47,650
think about, do I need a separate model, or, somehow,

192
00:12:47,650 --> 00:12:51,900
deal with luxury cars differently from ordinary cars,

193
00:12:51,900 --> 00:12:55,830
and, maybe, likewise, very high fuel efficiency cars.

194
00:12:55,830 --> 00:12:59,720
Might be another category that we need to think about.

195
00:12:59,720 --> 00:13:04,664
I hope this gives you some idea of the practical aspects of

196
00:13:04,664 --> 00:13:07,342
how we go about searching for,

197
00:13:07,342 --> 00:13:11,777
validating and generally dealing with outliers.

