0
00:00:05,184 --> 00:00:08,251
So the previous sequence I showed you how to scale data

1
00:00:08,251 --> 00:00:10,580
using the Azure Machine Learning Tool.

2
00:00:11,930 --> 00:00:14,790
In this sequence I&#39;m gonna show you how to do exactly the same

3
00:00:14,790 --> 00:00:18,340
thing using the R scale function.

4
00:00:20,390 --> 00:00:24,830
And so if you&#39;re in the Python track, you may wanna skip

5
00:00:24,830 --> 00:00:29,840
this video and see the exactly parallel one for Python.

6
00:00:29,840 --> 00:00:35,040
On my screen here, I have the experiment we&#39;ve been working on

7
00:00:35,040 --> 00:00:39,200
using R over here on the left to prepare the data, and

8
00:00:39,200 --> 00:00:43,330
of course along the right is the analogous preparations

9
00:00:43,330 --> 00:00:47,670
that we&#39;ve done in using the Azure Machine Learning Tools.

10
00:00:47,670 --> 00:00:51,690
So I have added two new execute R scripts over here

11
00:00:54,210 --> 00:00:57,650
and let me just show you what the code is inside.

12
00:00:57,650 --> 00:01:01,830
So the first one, we&#39;re going to tag the outliers.

13
00:01:01,830 --> 00:01:06,240
And so this Id.outlier function we&#39;ve already talked about

14
00:01:06,240 --> 00:01:09,020
is just identifying the outliers based on that

15
00:01:09,020 --> 00:01:12,740
hypothesis of what their values might be with this filter.

16
00:01:15,220 --> 00:01:18,514
So of course read in a data frame called auto.price

17
00:01:18,514 --> 00:01:19,440
from port 1.

18
00:01:19,440 --> 00:01:23,768
Make sure we got the d pliers library loaded.

19
00:01:23,768 --> 00:01:27,440
So, then auto.price becomes the argument to

20
00:01:27,440 --> 00:01:33,610
the id.outlier function, and you get auto.price back.

21
00:01:33,610 --> 00:01:38,447
And, finally, I&#39;m gonna use this dplyr filter, here, where

22
00:01:38,447 --> 00:01:44,030
outlier = zero, so that&#39;s not an outlier.

23
00:01:44,030 --> 00:01:50,130
So I chain in the data frame with the chain

24
00:01:50,130 --> 00:01:53,900
operator and I get a data frame called out, which I output.

25
00:01:53,900 --> 00:01:55,900
All right, simple enough.

26
00:01:59,180 --> 00:02:03,250
Then, in this next execute R script,

27
00:02:03,250 --> 00:02:05,620
I have a list of the numeric columns.

28
00:02:05,620 --> 00:02:11,340
Again I read the data frame from port 1 and I

29
00:02:11,340 --> 00:02:17,040
just used this R scale function on just the numeric columns,

30
00:02:17,040 --> 00:02:20,670
you see I&#39;ve selected just the numeric columns of auto.price.

31
00:02:20,670 --> 00:02:24,110
And I assign those back to just the numeric

32
00:02:24,110 --> 00:02:28,450
columns of auto.price and output the data frame.

33
00:02:29,510 --> 00:02:31,788
So let me just save this experiment and

34
00:02:31,788 --> 00:02:33,076
I&#39;ll run it for you.

35
00:02:38,301 --> 00:02:43,760
Experiments run successfully, let&#39;s have a look at the output

36
00:02:43,760 --> 00:02:48,620
from the results data set port here, let&#39;s visualize.

37
00:02:51,290 --> 00:02:55,010
I&#39;ll scroll all the way over to the right here, and we&#39;ll look

38
00:02:55,010 --> 00:02:59,869
at a few of these numeric columns like highway.mpg.

39
00:03:01,790 --> 00:03:08,680
And again, minus 2 to about 2.7 city mpg.

40
00:03:08,680 --> 00:03:15,600
Again, similar to what we got, peak rpm, horsepower, etcetera.

41
00:03:15,600 --> 00:03:21,200
So again, you can see its pretty close to zero centered and

42
00:03:21,200 --> 00:03:23,560
standard deviation of one just by eyeball.

43
00:03:25,620 --> 00:03:31,049
So we&#39;ve successfully now scaled the data using the tool

44
00:03:31,049 --> 00:03:36,827
in Azure Machine Learning and using the scale function in R.

