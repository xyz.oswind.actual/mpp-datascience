0
00:00:05,144 --> 00:00:08,648
Hi, in this sequence, I&#39;d like to talk about a very deep and

1
00:00:08,648 --> 00:00:11,951
complex subject which we&#39;re only gonna touch on briefly

2
00:00:11,951 --> 00:00:15,350
in this course called feature engineering.

3
00:00:15,350 --> 00:00:19,810
So when you get a data set, even a fairly simple data set,

4
00:00:19,810 --> 00:00:22,910
there&#39;s a high chance that the features you&#39;re gonna use to

5
00:00:22,910 --> 00:00:26,210
try to say, predict the label with the machine learning model,

6
00:00:26,210 --> 00:00:28,010
aren&#39;t the best.

7
00:00:28,010 --> 00:00:30,000
Maybe there&#39;s some transformation needed,

8
00:00:30,000 --> 00:00:32,790
maybe you need to sum them, multiply them,

9
00:00:32,790 --> 00:00:35,760
transform them by some other way.

10
00:00:35,760 --> 00:00:38,661
And we call that whole process of trying to come up with

11
00:00:38,661 --> 00:00:43,190
a better feature set, feature engineering, makes sense, right?

12
00:00:43,190 --> 00:00:46,070
So let&#39;s just look at a fairly simple example here,

13
00:00:46,070 --> 00:00:48,690
just to give you a flavor of how that can work.

14
00:00:48,690 --> 00:00:53,203
So you can see on my screen here, I have a scatter plot

15
00:00:53,203 --> 00:00:57,627
matrix of three of my features and our label price.

16
00:00:57,627 --> 00:01:02,233
And especially notice with features like city.mpg versus

17
00:01:02,233 --> 00:01:07,020
price, it&#39;s a fairly curved looking relationship.

18
00:01:07,020 --> 00:01:10,766
Engine size maybe a little straighter,

19
00:01:10,766 --> 00:01:14,843
curb weight also does seem to curve over so,

20
00:01:14,843 --> 00:01:18,380
depending on which way you look at it.

21
00:01:20,500 --> 00:01:23,880
So, it&#39;s possible that some transformation

22
00:01:23,880 --> 00:01:28,290
of price might actually be a better

23
00:01:29,410 --> 00:01:34,550
way to represent that label than simply price.

24
00:01:35,800 --> 00:01:43,240
So let me go to my Azure Machine Learning Studio here,

25
00:01:43,240 --> 00:01:46,700
and I&#39;ve set up to do just that transformation.

26
00:01:46,700 --> 00:01:53,015
So I have the supply math operations module and I&#39;m

27
00:01:53,015 --> 00:01:55,820
gonna do a basic transformation, which is a math function.

28
00:01:55,820 --> 00:01:58,720
So you can see there&#39;s quite a few different.

29
00:01:58,720 --> 00:02:02,190
You can do you can do comparisons for logical.

30
00:02:02,190 --> 00:02:06,530
You can do operations, trigonometric special.

31
00:02:06,530 --> 00:02:08,380
You can round numbers.

32
00:02:08,380 --> 00:02:11,790
There&#39;s lots of things you can do with a supply math operation.

33
00:02:11,790 --> 00:02:16,620
And I&#39;m just going to use the natural log here.

34
00:02:16,620 --> 00:02:19,240
And you can see there&#39;s a whole bunch of other things I

35
00:02:19,240 --> 00:02:19,760
could do.

36
00:02:19,760 --> 00:02:22,920
I could try squarings, square roots, whatever.

37
00:02:24,250 --> 00:02:26,790
And when I launch the column selector,

38
00:02:26,790 --> 00:02:30,120
I just selected price and I&#39;m gonna say, Append.

39
00:02:31,310 --> 00:02:34,690
Now there&#39;s one thing about this that&#39;s a little funny.

40
00:02:34,690 --> 00:02:40,270
It&#39;s going to give that new column a machine-generated name,

41
00:02:40,270 --> 00:02:41,350
which is a little funny.

42
00:02:41,350 --> 00:02:44,630
So with the metadata editor it&#39;s gonna be LN(price).

43
00:02:44,630 --> 00:02:47,140
It&#39;s actually going to give you the formula.

44
00:02:47,140 --> 00:02:49,560
And I&#39;d rather see that as lnprice.

45
00:02:49,560 --> 00:02:54,020
So I&#39;m just going to use the metadata editor to give myself

46
00:02:54,020 --> 00:02:57,560
a simpler column name.

47
00:02:57,560 --> 00:03:00,468
I&#39;m just saving my experiment and I&#39;m gonna run it.

48
00:03:03,864 --> 00:03:06,134
And let&#39;s have a quick look at the results here.

49
00:03:10,627 --> 00:03:14,395
Let&#39;s scroll over and you see I have a new column, lnprice.

50
00:03:15,650 --> 00:03:17,170
And so

51
00:03:17,170 --> 00:03:20,870
actually one thing to look at is, here&#39;s original price.

52
00:03:20,870 --> 00:03:26,590
And you see also it&#39;s a pretty odd distribution

53
00:03:26,590 --> 00:03:32,340
in the sense that it steps down like this.

54
00:03:32,340 --> 00:03:35,550
That there are very few high priced cars,

55
00:03:35,550 --> 00:03:37,756
lots of low priced cars.

56
00:03:37,756 --> 00:03:43,390
And lnprice has a kind of a more uniform looking distribution and

57
00:03:43,390 --> 00:03:46,040
obviously kinda humpy in the middle,

58
00:03:46,040 --> 00:03:47,950
but it&#39;s a little bit different.

59
00:03:47,950 --> 00:03:50,530
So maybe that&#39;s better behaved when we go to model it.

60
00:03:51,550 --> 00:03:56,342
But also, let&#39;s look against say, city miles per gallon, and

61
00:03:56,342 --> 00:04:00,248
remember how curvy it was that relationship and it,

62
00:04:00,248 --> 00:04:04,954
other than maybe what might be some outliers here it&#39;s looking

63
00:04:04,954 --> 00:04:06,403
pretty straight.

64
00:04:06,403 --> 00:04:11,518
Or maybe let&#39;s look at, I think we are looking at engine size,

65
00:04:11,518 --> 00:04:15,870
or here is curb weight, yeah again pretty straight.

66
00:04:16,930 --> 00:04:17,890
Doesn&#39;t look too bad.

67
00:04:18,930 --> 00:04:20,950
And here&#39;s engine size.

68
00:04:23,960 --> 00:04:28,060
Maybe a little curvy, but maybe, those could be outliers too, so

69
00:04:30,090 --> 00:04:34,510
overall that transformation, that little bit of engineering,

70
00:04:34,510 --> 00:04:36,630
although technically that was engineering on a label,

71
00:04:36,630 --> 00:04:40,398
we still tend to call it feature engineering.

72
00:04:40,398 --> 00:04:45,038
So, that gives you just a glimpse into how we might go

73
00:04:45,038 --> 00:04:47,780
about creating new features or

74
00:04:47,780 --> 00:04:53,160
transforming features to improve how well we can analyze and

75
00:04:53,160 --> 00:04:57,290
build predictive models for a given data set.

