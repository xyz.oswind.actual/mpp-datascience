0
00:00:04,523 --> 00:00:09,981
So now let&#39;s tackle the problem of missing and repeated values.

1
00:00:09,981 --> 00:00:14,405
And missing and repeated values are just common.

2
00:00:14,405 --> 00:00:16,718
They show up all the time and

3
00:00:16,718 --> 00:00:20,849
we&#39;re gonna have to learn how to deal with them.

4
00:00:20,849 --> 00:00:24,798
And you need to be deliberate about tracking them down and

5
00:00:24,798 --> 00:00:26,864
doing something about them.

6
00:00:26,864 --> 00:00:31,522
Many machine learning algorithms just aren&#39;t gonna deal well with

7
00:00:31,522 --> 00:00:32,750
missing values.

8
00:00:32,750 --> 00:00:35,510
They may just actually not run at all,

9
00:00:35,510 --> 00:00:37,100
giving you an error message.

10
00:00:37,100 --> 00:00:41,150
Or they may converge to some strange solution that

11
00:00:41,150 --> 00:00:43,170
isn&#39;t at all what you intend.

12
00:00:43,170 --> 00:00:45,920
So the more deliberate you are about dealing

13
00:00:45,920 --> 00:00:47,990
with missing values, the better off you are.

14
00:00:49,040 --> 00:00:51,640
Repeated values are a somewhat different problem.

15
00:00:51,640 --> 00:00:53,702
They&#39;re not gonna break your machine learning algorithm.

16
00:00:53,702 --> 00:00:59,014
But, imagine if you have a great number of the same row,

17
00:00:59,014 --> 00:01:04,779
the same case in your data set that somehow just is repeated,

18
00:01:04,779 --> 00:01:07,379
it causes bias because that

19
00:01:07,379 --> 00:01:11,127
row should just be one case out of many.

20
00:01:11,127 --> 00:01:14,622
But in fact, now it has a weight of, if it&#39;s repeated

21
00:01:14,622 --> 00:01:18,992
a 100 times, it has a weight of a 100 in training your model.

22
00:01:18,992 --> 00:01:23,026
And so these can show up very easily, for

23
00:01:23,026 --> 00:01:27,070
example, like in restaurant reviews.

24
00:01:27,070 --> 00:01:30,960
Maybe the same person forgot they reviewed this restaurant

25
00:01:30,960 --> 00:01:34,520
and reviewed it again with a slightly different title or

26
00:01:34,520 --> 00:01:37,450
something but it&#39;s basically the same person reviewing the same

27
00:01:37,450 --> 00:01:40,090
restaurant or the same person reviewing the same movie.

28
00:01:40,090 --> 00:01:43,950
Lots of reasons you&#39;d wind up with repeated values that may

29
00:01:43,950 --> 00:01:45,390
bias your outcome.

30
00:01:46,670 --> 00:01:49,642
So specifically, what do you do about missing values?

31
00:01:49,642 --> 00:01:55,060
So here&#39;s an example of a table with some missing values.

32
00:01:55,060 --> 00:01:58,440
And one thing I&#39;m trying to show here is they can show up

33
00:01:58,440 --> 00:02:00,733
in many guises.

34
00:02:00,733 --> 00:02:05,333
So Column 1 looks okay, Column 2, notice we&#39;ve got

35
00:02:05,333 --> 00:02:10,160
these small numbers, and then we have this 99999.

36
00:02:10,160 --> 00:02:15,183
So this is an old approach to inputting a missing value.

37
00:02:15,183 --> 00:02:19,126
It goes way back to the mainframe days of the 1960s but

38
00:02:19,126 --> 00:02:21,270
you still find it in data sets.

39
00:02:21,270 --> 00:02:24,300
I found it in a data set fairly recently just in

40
00:02:24,300 --> 00:02:25,510
the last few years.

41
00:02:25,510 --> 00:02:29,344
It must have had an origin way back in the mainframe days, or

42
00:02:29,344 --> 00:02:33,332
how these data were assembled, because they were still using

43
00:02:33,332 --> 00:02:37,951
9999 as the missing value or sometimes it&#39;s like minus 9999.

44
00:02:37,951 --> 00:02:41,243
So in that case you actually have to do some filtering to

45
00:02:41,243 --> 00:02:43,822
figure out that that&#39;s a missing value but

46
00:02:43,822 --> 00:02:45,840
in this case it&#39;s pretty clear.

47
00:02:45,840 --> 00:02:49,650
Everything else is a small number around 1 and

48
00:02:49,650 --> 00:02:51,280
then suddenly there&#39;s this big number.

49
00:02:51,280 --> 00:02:54,620
That was kind of the idea those guys had back in the,

50
00:02:54,620 --> 00:02:57,130
say the 1960s, just use some value that couldn&#39;t show up.

51
00:02:58,670 --> 00:03:01,656
Maybe not the safest way to do it, but you may encounter that.

52
00:03:01,656 --> 00:03:03,972
There&#39;s also just a blank or a null,

53
00:03:03,972 --> 00:03:06,380
which I&#39;m showing in Column 3 here.

54
00:03:06,380 --> 00:03:08,810
There&#39;s just nothing there.

55
00:03:10,140 --> 00:03:13,860
Sometimes you will see something like an NA or sometimes it&#39;s

56
00:03:13,860 --> 00:03:21,500
NAN, not a number, that&#39;s a missing value.

57
00:03:21,500 --> 00:03:24,553
And many times missing values show up as something

58
00:03:24,553 --> 00:03:27,605
in a character string or a categorical variable,

59
00:03:27,605 --> 00:03:30,816
like question mark or other or something like that.

60
00:03:30,816 --> 00:03:34,462
Where they just don&#39;t know what it is and

61
00:03:34,462 --> 00:03:40,220
so there&#39;s some default value that&#39;s put in there.

62
00:03:40,220 --> 00:03:43,120
So these are some examples of things you should be on

63
00:03:43,120 --> 00:03:46,450
the lookout for when you&#39;re trying to tackle missing values.

64
00:03:48,190 --> 00:03:48,982
So what do you do when you find them?

65
00:03:48,982 --> 00:03:52,430
Well, there&#39;s a number of treatments you can apply.

66
00:03:52,430 --> 00:03:54,676
You can just remove a row and

67
00:03:54,676 --> 00:03:58,002
a lot of times that&#39;s a safe thing to do.

68
00:03:58,002 --> 00:04:01,414
But you need to be careful because you may wind up

69
00:04:01,414 --> 00:04:06,020
decimating your data set if it&#39;s particularly messy.

70
00:04:06,020 --> 00:04:08,700
Substitute a specific value, maybe it

71
00:04:08,700 --> 00:04:12,170
makes sense that missing values can just be set to 0 or

72
00:04:12,170 --> 00:04:14,330
to other or something like that.

73
00:04:16,500 --> 00:04:20,090
Interpolate, maybe if it&#39;s numerical, maybe

74
00:04:20,089 --> 00:04:23,970
linearly interpolate if there&#39;s a little gap in the data.

75
00:04:23,970 --> 00:04:25,390
Especially time series data,

76
00:04:25,390 --> 00:04:27,143
this might be a really good approach.

77
00:04:27,143 --> 00:04:30,198
And there&#39;s something we called forward fill,

78
00:04:30,198 --> 00:04:34,223
where maybe if it&#39;s categorical data or something like that and

79
00:04:34,223 --> 00:04:37,055
something that doesn&#39;t change very often,

80
00:04:37,055 --> 00:04:39,306
you just look to the previous case.

81
00:04:39,306 --> 00:04:42,720
And you say, well, what was the value for the previous case?

82
00:04:42,720 --> 00:04:44,430
I&#39;ll just fill with that.

83
00:04:44,430 --> 00:04:46,270
Or backward fill, it&#39;s the same idea but

84
00:04:46,270 --> 00:04:49,290
you&#39;re going back instead of forward.

85
00:04:49,290 --> 00:04:52,805
So these are all legitimate treatments of missing values.

86
00:04:54,430 --> 00:04:55,280
And finally,

87
00:04:55,280 --> 00:04:59,140
there&#39;s a more sophisticated approach called imputation.

88
00:04:59,140 --> 00:05:01,720
So there you&#39;ve built some sort of statistical model

89
00:05:02,750 --> 00:05:06,450
of the values and you try to compute the most likely or

90
00:05:06,450 --> 00:05:09,400
most probable value to miss.

91
00:05:09,400 --> 00:05:12,170
We won&#39;t be talking about that in this course because

92
00:05:12,170 --> 00:05:13,750
it is fairly sophisticated and

93
00:05:13,750 --> 00:05:17,390
can be problematic if you&#39;re not extraordinarily careful.

94
00:05:19,210 --> 00:05:23,380
So how about cleaning missing and repeated values?

95
00:05:23,380 --> 00:05:25,590
Let&#39;s talk about the practical aspects of that.

96
00:05:27,050 --> 00:05:32,116
So in Azure ML there&#39;s a Clean Missing Data module and

97
00:05:32,116 --> 00:05:34,148
we&#39;ll look at that.

98
00:05:34,148 --> 00:05:38,320
R, missing values generally show up as an na.

99
00:05:38,320 --> 00:05:44,309
Now as I&#39;ve showed you, so if it&#39;s a null, a blank, an na,

100
00:05:44,309 --> 00:05:50,660
any of those things will show up as an na or is.na() function.

101
00:05:50,660 --> 00:05:54,391
Python-pandas you&#39;ve gotta be a little more

102
00:05:54,391 --> 00:05:57,069
careful because there is an na but

103
00:05:57,069 --> 00:06:01,290
that&#39;s not the only way missing values can show up.

104
00:06:01,290 --> 00:06:04,440
So the safer approach is the isnull() method for

105
00:06:04,440 --> 00:06:05,494
the data frame.

106
00:06:05,494 --> 00:06:10,051
What about repeated values?

107
00:06:10,051 --> 00:06:13,554
What&#39;s some appropriate ways to deal with that?

108
00:06:13,554 --> 00:06:18,890
So first off, what do I mean by repeated values?

109
00:06:19,910 --> 00:06:22,468
So notice in this leftmost column,

110
00:06:22,468 --> 00:06:24,777
which I&#39;ve called Key Column.

111
00:06:24,777 --> 00:06:28,100
So it&#39;s some column, you generally need one or

112
00:06:28,100 --> 00:06:32,350
more columns that at least in combination should be unique.

113
00:06:33,790 --> 00:06:36,350
Maybe it&#39;s the person&#39;s name and the restaurant name or

114
00:06:36,350 --> 00:06:40,090
the person&#39;s name and the movie name, or something, or

115
00:06:40,090 --> 00:06:43,010
maybe it&#39;s the customer account that a customer should only have

116
00:06:43,010 --> 00:06:46,140
one account on your website, or something like that.

117
00:06:46,140 --> 00:06:47,880
So we have this Key Column and

118
00:06:47,880 --> 00:06:51,690
notice that this particular value is repeated four times.

119
00:06:53,140 --> 00:06:57,086
And in fact, it&#39;s not exactly the same data either,

120
00:06:57,086 --> 00:06:59,112
which could be troubling.

121
00:06:59,112 --> 00:07:02,240
But much of it is repeated.

122
00:07:04,270 --> 00:07:05,630
But we have this one different row.

123
00:07:05,630 --> 00:07:08,720
So you need to be a little bit

124
00:07:08,720 --> 00:07:11,479
thoughtful about what you&#39;re gonna do with repeated values.

125
00:07:14,670 --> 00:07:19,835
So there&#39;s a Clean Repeated Values module in Azure ML.

126
00:07:19,835 --> 00:07:24,821
In R, you can say, not duplicated.

127
00:07:24,821 --> 00:07:29,142
So this will, by doing !duplicated(), will give

128
00:07:29,142 --> 00:07:34,140
you just the rows of this data frame that aren&#39;t duplicates.

129
00:07:36,090 --> 00:07:40,305
And in Python-pandas there&#39;s

130
00:07:40,305 --> 00:07:46,560
a DataFrame.drop_duplicates() method.

131
00:07:46,560 --> 00:07:47,591
So that works fairly well.

132
00:07:47,591 --> 00:07:51,325
These all drop, well with the exception of this Azure ML

133
00:07:51,325 --> 00:07:55,144
approach, they&#39;re simply gonna drop the duplicates.

134
00:07:55,144 --> 00:07:58,167
And like I said, sometimes you need to be a little cautious,

135
00:07:58,167 --> 00:08:01,030
that might not be exactly what you want.

136
00:08:01,030 --> 00:08:03,842
That gives you some idea of how to hunt for

137
00:08:03,842 --> 00:08:07,251
and deal with both missing and repeated values.

