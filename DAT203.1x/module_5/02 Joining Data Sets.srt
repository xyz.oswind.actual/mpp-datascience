0
00:00:04,724 --> 00:00:08,380
Hi, and welcome to the sequence on joins.

1
00:00:08,380 --> 00:00:11,280
A very important topic if you&#39;re gonna be a practicing

2
00:00:11,280 --> 00:00:12,160
data scientist.

3
00:00:14,130 --> 00:00:17,550
Joins are something you&#39;ll do quite a lot of

4
00:00:17,550 --> 00:00:20,820
because of a simple fact, data comes from many sources.

5
00:00:22,450 --> 00:00:26,308
Very rarely in a project is someone just gonna say, look,

6
00:00:26,308 --> 00:00:30,560
look in this database table, and everything you need is in those

7
00:00:30,560 --> 00:00:33,653
rows, already lined up for you, ready to go.

8
00:00:33,653 --> 00:00:37,570
It just, I don&#39;t think that&#39;s ever happened to me in my life.

9
00:00:37,570 --> 00:00:39,620
Maybe someday, but never yet.

10
00:00:41,220 --> 00:00:44,170
And because of that, the reality is,

11
00:00:44,170 --> 00:00:49,370
the join comes from many tables and many sources and

12
00:00:49,370 --> 00:00:52,740
you have to integrate those in some sensible way to make

13
00:00:52,740 --> 00:00:56,670
effectively, at least logically, sort of a superset table

14
00:00:56,670 --> 00:00:59,020
that has all the columns joined together.

15
00:01:01,055 --> 00:01:06,408
And the main requirement is, you need a common key.

16
00:01:06,408 --> 00:01:10,489
And there have to be at least some key values between the key

17
00:01:10,489 --> 00:01:13,320
columns in the tables you&#39;re joining so

18
00:01:13,320 --> 00:01:15,000
that you can join them.

19
00:01:15,000 --> 00:01:20,140
Now, if you have three tables, say, you could have table A,

20
00:01:20,140 --> 00:01:24,060
table B have a common key column and table B,

21
00:01:24,060 --> 00:01:27,870
table C have a common key column.

22
00:01:27,870 --> 00:01:31,135
There&#39;s no need to have all tables have a common key column,

23
00:01:31,135 --> 00:01:34,091
as long as there&#39;s some way you can piece it together.

24
00:01:36,503 --> 00:01:38,370
What are some examples?

25
00:01:38,370 --> 00:01:40,000
Well, there&#39;s just a lot of them.

26
00:01:40,000 --> 00:01:41,930
Just throwing out a few here.

27
00:01:41,930 --> 00:01:45,450
Customer ID, Project ID, Date, Location.

28
00:01:45,450 --> 00:01:47,155
It could be aircraft tail number.

29
00:01:47,155 --> 00:01:53,087
It could be some sort of, the name of the country,

30
00:01:53,087 --> 00:01:58,612
or a person&#39;s name, or something like that.

31
00:01:58,612 --> 00:02:00,953
So lots of ways you can do joins.

32
00:02:04,627 --> 00:02:07,741
So let&#39;s talk a little bit about the mechanics.

33
00:02:07,741 --> 00:02:11,573
So let&#39;s start with two tables A and B, and

34
00:02:11,573 --> 00:02:15,280
assume we have a common key column.

35
00:02:15,280 --> 00:02:19,158
Now, the key columns needn&#39;t have the same name, they just

36
00:02:19,158 --> 00:02:23,123
have to have some corresponding values that you can match up.

37
00:02:23,123 --> 00:02:26,750
So maybe in one table it&#39;s called customer name, and

38
00:02:26,750 --> 00:02:30,135
in the other table it&#39;s called name of customer or

39
00:02:30,135 --> 00:02:32,040
something like that.

40
00:02:32,040 --> 00:02:35,690
Well, that&#39;s okay as long as those names are the same that

41
00:02:35,690 --> 00:02:39,680
you can actually match them up, match some keys.

42
00:02:39,680 --> 00:02:44,587
So, the generic operator here is whatever sort of join, and

43
00:02:44,587 --> 00:02:48,725
we&#39;ll talk about what those joins are in a minute,

44
00:02:48,725 --> 00:02:53,552
of table A, table B, based on one or more key column names.

45
00:02:53,552 --> 00:02:56,300
So that&#39;s just the most generic join operator.

46
00:02:57,790 --> 00:02:59,400
So let&#39;s think about an inner join.

47
00:02:59,400 --> 00:03:03,120
So an inner join is a very common type of join where

48
00:03:06,110 --> 00:03:10,180
at least one key in A and B match

49
00:03:10,180 --> 00:03:14,500
that gives you a row in the new table, in the join table.

50
00:03:14,500 --> 00:03:21,589
So if you have a customer name of Smith in a row in one table,

51
00:03:21,589 --> 00:03:28,100
table A, and you have Smith in another row in table B,

52
00:03:28,100 --> 00:03:32,333
you&#39;re gonna get a row for Smith.

53
00:03:32,333 --> 00:03:35,331
And you may have actually multiple,

54
00:03:35,331 --> 00:03:39,206
if there&#39;s ten Smiths or something like that.

55
00:03:39,206 --> 00:03:43,872
A left join, so a left join, the table on the left, which in our

56
00:03:43,872 --> 00:03:48,540
little example here is table A, all the rows from the left table

57
00:03:48,540 --> 00:03:52,080
with all the matching rows of the right table.

58
00:03:52,080 --> 00:03:57,410
So if you have ten rows with

59
00:03:57,410 --> 00:04:02,520
Smith in the left table and only one row with Smith in the right

60
00:04:02,520 --> 00:04:08,000
table, all those rows in the left table are gonna

61
00:04:08,000 --> 00:04:13,170
get the same data appended to them from that right table.

62
00:04:13,170 --> 00:04:18,350
So that&#39;s why we call it a left join.

63
00:04:18,350 --> 00:04:20,217
A right join is just the opposite.

64
00:04:20,216 --> 00:04:25,373
So now we have, if Smith shows up ten times in table B,

65
00:04:25,373 --> 00:04:29,499
our right table, and only once in table A,

66
00:04:29,499 --> 00:04:33,971
our left table, we&#39;re gonna have ten rows for

67
00:04:33,971 --> 00:04:37,430
Smith in our resulting join table.

68
00:04:38,460 --> 00:04:43,262
And a full join retains all values, all rows.

69
00:04:43,262 --> 00:04:47,880
And thing to keep in mind about things like a full join,

70
00:04:47,880 --> 00:04:52,940
sometimes called an outer join, is you can have missing values.

71
00:04:52,940 --> 00:04:56,546
Maybe table A has ten rows of Smith,

72
00:04:56,546 --> 00:04:59,682
table B has no rows with Smith.

73
00:04:59,682 --> 00:05:03,141
So we&#39;re still gonna have ten rows with Smith, but

74
00:05:03,141 --> 00:05:06,440
any values that you were expecting from table B,

75
00:05:06,440 --> 00:05:08,786
they&#39;re not just gonna be there.

76
00:05:08,786 --> 00:05:11,640
So let&#39;s talk about doing joins.

77
00:05:11,640 --> 00:05:14,020
So in Azure ML, there&#39;s a join module.

78
00:05:16,680 --> 00:05:19,320
And we&#39;ll show that in the demonstration and

79
00:05:19,320 --> 00:05:21,410
you&#39;ll work with that in the lab.

80
00:05:21,410 --> 00:05:26,602
In the R dplyr package, there&#39;s essentially what

81
00:05:26,602 --> 00:05:32,771
I&#39;ve called xxx_join, so inner, left, right, outer.

82
00:05:35,431 --> 00:05:40,550
And in the Pandas package there&#39;s pandas.DataFrame.merge.

83
00:05:40,550 --> 00:05:44,720
Now, keep that in mind, it&#39;s not called join, it&#39;s called merge.

84
00:05:44,720 --> 00:05:48,453
There&#39;s a few other things you can do with it,

85
00:05:48,453 --> 00:05:51,515
which is why it was named that way, but

86
00:05:51,515 --> 00:05:55,853
the basic thing you do is a join with the merge method.

87
00:05:58,321 --> 00:06:02,794
So let&#39;s just look at some examples of the dplyr join,

88
00:06:02,794 --> 00:06:04,943
these are fairly simple.

89
00:06:04,943 --> 00:06:09,402
So I have an inner_join of a, b, by, and I&#39;ll just say they

90
00:06:09,402 --> 00:06:13,250
have a common column named 1 in all these examples,

91
00:06:13,250 --> 00:06:15,010
that&#39;s my key column.

92
00:06:16,200 --> 00:06:21,139
left_join, again, the syntax is very simple, a b by that same

93
00:06:21,139 --> 00:06:24,706
column, right_join a b by that same column,

94
00:06:24,706 --> 00:06:27,459
full_join a b by that same column.

95
00:06:27,459 --> 00:06:31,824
Or, because I have the chaining in dplyr, I have,

96
00:06:31,824 --> 00:06:36,391
A gets chained into whatever type of join I have for b,

97
00:06:36,391 --> 00:06:39,536
notice I don&#39;t have to say what A is,

98
00:06:39,536 --> 00:06:42,912
cuz I&#39;ve already input it, by Col1.

99
00:06:46,079 --> 00:06:51,250
Pandas joins, similarly, fairly simple syntax.

100
00:06:51,250 --> 00:06:53,230
But, remember, we&#39;re using the merge.

101
00:06:53,230 --> 00:07:00,870
So, we got merge, a, b, how inner, right, left, outer.

102
00:07:00,870 --> 00:07:05,181
And then you can do left on Col1, right on Col1.

103
00:07:05,181 --> 00:07:09,329
You can also do just on Col1 if it&#39;s the same, you see,

104
00:07:09,329 --> 00:07:11,552
that would work just as well.

105
00:07:15,692 --> 00:07:19,924
And you can also not quite chain this,

106
00:07:19,924 --> 00:07:23,950
but you can do a.merge b, etc.

107
00:07:23,950 --> 00:07:27,200
So that&#39;s another way to apply that operator.

108
00:07:27,200 --> 00:07:33,590
So let&#39;s talk about a similar operation, which is appending.

109
00:07:33,590 --> 00:07:35,050
So what&#39;s the difference?

110
00:07:35,050 --> 00:07:37,250
Appending rows and

111
00:07:37,250 --> 00:07:42,560
columns is not as sophisticated a process as a join,

112
00:07:42,560 --> 00:07:46,580
because join is done on a key and appending is not.

113
00:07:46,580 --> 00:07:50,114
So that has some advantages and disadvantages.

114
00:07:50,114 --> 00:07:54,950
First off, if you&#39;re appending rows,

115
00:07:54,950 --> 00:07:57,390
they have to have the same column type, and

116
00:07:57,390 --> 00:08:01,200
you have to have the same number of rows, cuz you&#39;re just simply,

117
00:08:01,200 --> 00:08:04,110
on a data frame, imagine that you&#39;re just

118
00:08:04,110 --> 00:08:09,610
taking two data frames, concatenating them row on row.

119
00:08:09,610 --> 00:08:13,800
So if there was a discontinuity in column types or

120
00:08:13,800 --> 00:08:17,710
the data types in those columns, that&#39;s gonna be a real problem.

121
00:08:19,270 --> 00:08:22,040
Columns, now, columns don&#39;t have to have the same type, but

122
00:08:22,040 --> 00:08:22,965
they have to have the same length,

123
00:08:22,965 --> 00:08:25,070
cuz you&#39;re simply appending a column,

124
00:08:25,070 --> 00:08:28,970
you&#39;re simply putting a column onto that data frame.

125
00:08:28,970 --> 00:08:33,237
And data frames have to be rectangular,

126
00:08:33,236 --> 00:08:38,643
which means all columns have to be the same length.

127
00:08:38,643 --> 00:08:42,499
And in Azure ML, there&#39;s an Add Columns module and

128
00:08:42,499 --> 00:08:45,520
there&#39;s an Add Rows module.

129
00:08:45,520 --> 00:08:48,810
So similarly, we have tools for appending columns and

130
00:08:48,810 --> 00:08:52,410
rows using R or Python Pandas.

131
00:08:53,490 --> 00:08:57,930
So we have the same rules again, append rows with the same column

132
00:08:57,930 --> 00:09:01,340
types, append columns of the same length.

133
00:09:02,800 --> 00:09:05,780
So let&#39;s see how we do that, say, for R data frame notation.

134
00:09:06,840 --> 00:09:11,245
So here we have whatever the name of our data frame is,

135
00:09:11,245 --> 00:09:15,410
data.frame, and then the square bracket operator.

136
00:09:15,410 --> 00:09:19,050
Remember the comma, because we&#39;re appending a column here,

137
00:09:19,050 --> 00:09:20,550
not a row.

138
00:09:20,550 --> 00:09:23,940
So we don&#39;t have anything there, but we do need the comma.

139
00:09:23,940 --> 00:09:26,890
And then we just give it a name, whatever, new.column or

140
00:09:26,890 --> 00:09:28,060
whatever we want to call it.

141
00:09:30,860 --> 00:09:32,700
So we can also

142
00:09:35,820 --> 00:09:40,290
concatenate effectively data frames where the column

143
00:09:40,290 --> 00:09:42,865
types and the number of columns and the types of columns

144
00:09:42,865 --> 00:09:46,233
are exactly the same using the rbind function.

145
00:09:48,090 --> 00:09:53,100
Now, in Pandas, it&#39;s virtually the same as in R, so we have

146
00:09:53,100 --> 00:09:56,720
whatever our Pandas data frame is with the bracket operator.

147
00:09:56,720 --> 00:10:01,002
We don&#39;t need the comma in Pandas,

148
00:10:01,002 --> 00:10:05,711
we just need new, say, _column, and

149
00:10:05,711 --> 00:10:10,015
then equals whatever it is, okay?

150
00:10:10,015 --> 00:10:17,640
And there&#39;s a Pandas.DataFrame.append method.

151
00:10:17,640 --> 00:10:20,479
So we have our DataFrame.append,

152
00:10:20,479 --> 00:10:25,038
and then data_frame2 that we want to append on the end.

153
00:10:25,038 --> 00:10:28,470
So basically, concatenate the first data frame to the second

154
00:10:28,470 --> 00:10:32,010
data frame, assuming all the column types line up.

155
00:10:33,010 --> 00:10:35,680
So just to wrap up,

156
00:10:35,680 --> 00:10:38,970
we&#39;ve gone through a number of the basics of joins.

157
00:10:38,970 --> 00:10:41,850
You&#39;ll be doing a lot of joins in your life as a data

158
00:10:41,850 --> 00:10:42,650
scientist.

159
00:10:43,970 --> 00:10:46,419
There&#39;s lots of good ways to do joins.

160
00:10:47,670 --> 00:10:49,510
Azure ML has some simple stuff,

161
00:10:49,510 --> 00:10:55,690
there&#39;s some very powerful tools in R dplyr or in Python Pandas.

162
00:10:55,690 --> 00:10:59,864
And I hope this gets you started thinking about how you can best

163
00:10:59,864 --> 00:11:02,761
do joins as you encounter the need to do it.

