0
00:00:04,936 --> 00:00:09,781
Hi, so we&#39;ve done a lot of data preparation in this module, and

1
00:00:09,781 --> 00:00:13,281
there&#39;s just one last bit we have to address,

2
00:00:13,281 --> 00:00:14,740
which is scaling.

3
00:00:15,888 --> 00:00:20,950
And so n this sequence I&#39;m going to show you a simple demo of how

4
00:00:20,950 --> 00:00:25,060
to use the tool in Azure Machine Learning to scale numeric data.

5
00:00:26,420 --> 00:00:30,190
So here on my screen you can see this long chain of

6
00:00:30,190 --> 00:00:33,180
data preparation modules we&#39;ve been working on

7
00:00:33,180 --> 00:00:34,950
throughout this module.

8
00:00:34,950 --> 00:00:39,090
And at the very bottom here is this Normalized Data module,

9
00:00:39,090 --> 00:00:41,740
which does exactly what you&#39;d imagine,

10
00:00:41,740 --> 00:00:44,380
it has various methods for normalizing data.

11
00:00:44,380 --> 00:00:45,470
So let me click on that.

12
00:00:47,380 --> 00:00:51,395
And you can see there&#39;s a number of transformation methods.

13
00:00:51,395 --> 00:00:54,040
Z-score, which is what I&#39;m gonna use here.

14
00:00:54,040 --> 00:00:58,028
But I could use minmax, logistic, lognormal,

15
00:00:58,028 --> 00:01:02,502
which is nice for some cases where you have logarithmic

16
00:01:02,502 --> 00:01:06,198
relationships, and tanh, which is nice for

17
00:01:06,198 --> 00:01:10,985
when you have certain types of classification problems.

18
00:01:10,985 --> 00:01:14,700
But let&#39;s just stick with a simple z-score.

19
00:01:14,700 --> 00:01:16,740
And the only other,

20
00:01:16,740 --> 00:01:20,480
I&#39;m not gonna worry about the constants here.

21
00:01:20,480 --> 00:01:25,350
And the only other thing you need to set is by a rule, I&#39;m

22
00:01:25,350 --> 00:01:30,600
gonna say start with NO COLUMNS, Include, column type, Numeric.

23
00:01:30,600 --> 00:01:34,230
So that means I have all numeric columns selected.

24
00:01:35,570 --> 00:01:37,120
Let me just save my experiment.

25
00:01:40,080 --> 00:01:41,120
And we&#39;re gonna run it.

26
00:01:43,990 --> 00:01:44,820
And it&#39;s completed.

27
00:01:46,520 --> 00:01:51,432
So, we&#39;ll visualize the output here for you.

28
00:01:51,432 --> 00:01:56,010
And let me scroll over here so I can find some numeric columns.

29
00:01:56,010 --> 00:01:59,570
So the first one on the far right here is

30
00:01:59,570 --> 00:02:01,010
highway miles per gallon.

31
00:02:01,010 --> 00:02:06,105
I&#39;ll just scroll down, look at the histogram and

32
00:02:06,105 --> 00:02:11,819
you see that it&#39;s now in a range of -2 to about 2.7.

33
00:02:11,819 --> 00:02:15,960
0, is right about in here.

34
00:02:15,960 --> 00:02:19,650
That&#39;s pretty balanced, the math, between that and

35
00:02:19,650 --> 00:02:26,550
does look like it has a mean of 0 and a standard deviation of 1.

36
00:02:26,550 --> 00:02:29,666
Similarly, city miles per gallon,

37
00:02:29,666 --> 00:02:33,696
maybe some others like peak RPM, horsepower,

38
00:02:35,532 --> 00:02:39,450
Compression ratio, stroke, so you get the idea.

39
00:02:41,180 --> 00:02:42,930
So that&#39;s really all there is to it.

40
00:02:44,520 --> 00:02:48,520
Very simple to normalize data with a tool like that, and

41
00:02:48,520 --> 00:02:53,520
the results are fairly simple to check if you got it right.

42
00:02:53,520 --> 00:02:55,843
So now you know how to scale data.

