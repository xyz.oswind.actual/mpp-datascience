0
00:00:05,291 --> 00:00:09,912
Hi, so having now discussed kind of the principles and

1
00:00:09,912 --> 00:00:14,633
ideas behind looking for and treating outliers, let me

2
00:00:14,633 --> 00:00:19,563
show you a practical example of how we go about doing this.

3
00:00:19,563 --> 00:00:23,007
So for the automotive data set on my screen here,

4
00:00:23,007 --> 00:00:24,268
I have some code.

5
00:00:24,268 --> 00:00:28,210
It&#39;s just gonna make some simple scatterplots, and

6
00:00:28,210 --> 00:00:31,986
I&#39;ve got a function that I call vis.simple here.

7
00:00:31,986 --> 00:00:36,311
And, So there&#39;s just some R code.

8
00:00:36,311 --> 00:00:41,374
It takes a column name, the data frame, which I&#39;ve set

9
00:00:41,374 --> 00:00:46,810
the default value to the one where I was using auto.price.

10
00:00:46,810 --> 00:00:49,570
And then, a second column name,

11
00:00:49,570 --> 00:00:54,311
which I&#39;ve also set to a default value for the log price.

12
00:00:54,311 --> 00:00:56,350
And by now this should be familiar.

13
00:00:56,350 --> 00:01:01,604
We&#39;re gonna make sure ggplot is installed, create a title using

14
00:01:01,604 --> 00:01:07,680
paste, and call ggplot function for the data frame,

15
00:01:07,680 --> 00:01:12,440
and it&#39;s gonna be column 1 versus column 2 is what

16
00:01:12,440 --> 00:01:17,810
we&#39;re gonna plot as a point plot, scatter plot with a title.

17
00:01:17,810 --> 00:01:20,319
And so, the rest of the code is pretty simple.

18
00:01:20,319 --> 00:01:23,380
I&#39;m just gonna read the data frame from Port 1 as we

19
00:01:23,380 --> 00:01:24,124
always do.

20
00:01:24,124 --> 00:01:27,820
I&#39;m gonna define three plot columns here,

21
00:01:27,820 --> 00:01:33,230
curb weight, engine size, and city miles per gallon.

22
00:01:33,230 --> 00:01:37,385
And then using lapply, we&#39;re just gonna loop through, or

23
00:01:37,385 --> 00:01:41,634
step through that list of plot columns and make those plots.

24
00:01:41,634 --> 00:01:44,249
And then we&#39;re gonna output auto.price at the end so

25
00:01:44,249 --> 00:01:45,710
that we can do the next thing.

26
00:01:45,710 --> 00:01:51,088
So, let me go to my Azure Machine Learning Studio here.

27
00:01:51,088 --> 00:01:55,769
And I&#39;ve already added, so this is the code we looked at

28
00:01:55,769 --> 00:01:59,864
previously for preparing the automotive data.

29
00:01:59,864 --> 00:02:01,597
So now, I&#39;ve already cut and

30
00:02:01,597 --> 00:02:05,120
paste that code I just showed you into here.

31
00:02:05,120 --> 00:02:07,901
I&#39;ll just save it and run it.

32
00:02:11,701 --> 00:02:16,323
There, it&#39;s run, so let me look at those visualizations here.

33
00:02:16,323 --> 00:02:19,656
And we&#39;ll just talk about those for a minute.

34
00:02:19,656 --> 00:02:23,740
[COUGH] So this is plot of curb weight versus price.

35
00:02:23,740 --> 00:02:27,910
So price on the vertical, curb weight on the horizontal.

36
00:02:27,910 --> 00:02:30,329
And you see, there&#39;s kind of a trend here but

37
00:02:30,329 --> 00:02:31,800
there&#39;s some odd things.

38
00:02:31,800 --> 00:02:34,910
So we&#39;ve got this cluster of

39
00:02:34,910 --> 00:02:39,530
high curb weight cars that seem to be out by themselves.

40
00:02:40,920 --> 00:02:41,725
And see,

41
00:02:41,725 --> 00:02:46,964
it&#39;s sort of anything above maybe a curb weight of 3500.

42
00:02:46,964 --> 00:02:49,229
There&#39;s also these outliers here.

43
00:02:49,229 --> 00:02:53,666
But you see, you&#39;d have to have some sort of two-dimensional

44
00:02:53,666 --> 00:02:57,276
filter to get at those, so let&#39;s keep going here.

45
00:02:57,276 --> 00:03:02,113
So engine size, if we look at engine size, again,

46
00:03:02,113 --> 00:03:07,172
it looks like all of a sudden the curve turns this way,

47
00:03:07,172 --> 00:03:10,560
right around 200 maybe 190.

48
00:03:10,560 --> 00:03:14,500
So these are candidates to be considered outliers here,

49
00:03:14,500 --> 00:03:15,270
in my opinion.

50
00:03:16,500 --> 00:03:20,745
And if we look at city miles per gallon versus log price,

51
00:03:20,745 --> 00:03:24,850
here we&#39;ve got these three cars that just aren&#39;t on any trend,

52
00:03:24,850 --> 00:03:26,540
they&#39;re just out by themselves.

53
00:03:29,430 --> 00:03:31,920
So those are some candidates to do that.

54
00:03:31,920 --> 00:03:34,070
So let me go back to our Studio here, and

55
00:03:34,070 --> 00:03:37,530
I&#39;ll show you some code where we can test my hypothesis.

56
00:03:39,590 --> 00:03:43,434
So I&#39;ve created this code called vis.outlier.

57
00:03:44,920 --> 00:03:45,519
So what does it do?

58
00:03:45,519 --> 00:03:50,814
Again, it uses ggplot2, and

59
00:03:50,814 --> 00:03:56,922
if the code, So

60
00:03:56,922 --> 00:04:00,653
it creates a factor from the outlier column, and

61
00:04:00,653 --> 00:04:03,735
it creates a factor from the fuel type.

62
00:04:03,735 --> 00:04:07,122
Just replaces those columns with factor type, and creates

63
00:04:07,122 --> 00:04:10,654
a title, and then the rest is pretty straightforward again.

64
00:04:10,654 --> 00:04:14,100
We have the data frame name, the string,

65
00:04:14,100 --> 00:04:18,639
whatever that argument is, it starts the city.mpg.

66
00:04:18,639 --> 00:04:23,851
We have the lnprice, and we&#39;re gonna use some aesthetics here,

67
00:04:23,851 --> 00:04:28,780
so in geom_point I have some aesthetics that we&#39;re gonna use

68
00:04:28,780 --> 00:04:30,988
within this AES function.

69
00:04:30,988 --> 00:04:32,987
So, color is gonna be an outlier.

70
00:04:32,987 --> 00:04:36,720
A shape is gonna be what kind of fuel type the car uses.

71
00:04:36,720 --> 00:04:41,790
I&#39;m gonna give it an alpha of 0.5 and a size of 4.

72
00:04:41,790 --> 00:04:47,718
So there&#39;s this other function I&#39;ve created that uses that rule

73
00:04:47,718 --> 00:04:52,810
I just hypothesized, whether something&#39;s an outlier or not.

74
00:04:52,810 --> 00:04:57,790
So there&#39;s a nested, or there&#39;s an if else here and

75
00:04:57,790 --> 00:04:59,100
it&#39;s a little bit complicated.

76
00:04:59,100 --> 00:05:03,140
But basically if engine size, if any value in the column,

77
00:05:03,140 --> 00:05:05,045
notice the column&#39;s the little comma there,

78
00:05:05,045 --> 00:05:10,010
engine.size is greater than 190 if the curb.weight is greater

79
00:05:10,010 --> 00:05:16,016
than 3500, or, see the ors, these vertical bars?

80
00:05:16,016 --> 00:05:19,149
Or, the city.mpg is greater than 40,

81
00:05:19,149 --> 00:05:22,914
I&#39;m gonna give that a value of 1, the else is 0.

82
00:05:22,914 --> 00:05:25,323
So that&#39;s how I&#39;m marking the outlier.

83
00:05:27,348 --> 00:05:31,157
And then the rest of the code is not too surprising, okay?

84
00:05:31,157 --> 00:05:33,307
So again, read a data frame,

85
00:05:33,307 --> 00:05:37,791
and I&#39;m gonna name it auto.price again from the first port.

86
00:05:37,791 --> 00:05:40,880
Make sure dplyr is loaded.

87
00:05:43,261 --> 00:05:46,250
I&#39;m gonna loop through those same three columns.

88
00:05:47,700 --> 00:05:51,385
I call that outlier id function we just talked about with my

89
00:05:51,385 --> 00:05:52,218
data frame.

90
00:05:54,062 --> 00:05:58,485
Now I do lapply of plot.cols, sorry,

91
00:05:58,485 --> 00:06:03,578
I&#39;m applying this list of three column names

92
00:06:03,578 --> 00:06:10,180
to that vis.outlier function I just talked about.

93
00:06:10,180 --> 00:06:13,112
And finally I&#39;m gonna use the dplyr filter,

94
00:06:13,112 --> 00:06:15,827
that&#39;s why I used dplyr on auto.price.

95
00:06:15,827 --> 00:06:18,727
So the filter is, if outlier is 1.

96
00:06:18,727 --> 00:06:23,110
I just wanna output the outlier, so we can see what those are.

97
00:06:24,600 --> 00:06:27,499
So let me copy all that, go back to

98
00:06:27,499 --> 00:06:33,102
Azure Machine Learning Studio, and I&#39;m gonna get an Execute,

99
00:06:37,313 --> 00:06:40,490
R Script module here.

100
00:06:40,490 --> 00:06:42,231
I&#39;m gonna hook that up.

101
00:06:42,231 --> 00:06:48,184
The output of this one to this first dataset1 InputPort,

102
00:06:48,184 --> 00:06:50,862
and select all and paste.

103
00:06:50,862 --> 00:06:55,045
Assuming I haven&#39;t made an error, this should all run.

104
00:06:59,715 --> 00:07:03,588
Okay, let&#39;s have a look at, first off, at the charts in

105
00:07:03,588 --> 00:07:06,680
the visualization in the R Device port here.

106
00:07:06,680 --> 00:07:09,966
[COUGH] That&#39;s interesting!

107
00:07:09,966 --> 00:07:15,081
So outliers are in this kind of greenish-blue or

108
00:07:15,081 --> 00:07:19,585
turquoise color, non-outliers in red.

109
00:07:19,585 --> 00:07:24,162
And you see the shape, a round for diesel and

110
00:07:24,162 --> 00:07:29,097
a triangle for, Gas.

111
00:07:29,097 --> 00:07:32,889
So these outliers were almost all gas, except for

112
00:07:32,889 --> 00:07:35,110
a few diesels here.

113
00:07:35,110 --> 00:07:38,340
This diesel, apparently, we didn&#39;t label as an outlier.

114
00:07:38,340 --> 00:07:41,763
Maybe that&#39;s a mistake, maybe not.

115
00:07:41,763 --> 00:07:44,979
And then we&#39;ve got these three down here, a diesel, and

116
00:07:44,979 --> 00:07:45,523
two gas.

117
00:07:48,990 --> 00:07:50,491
So let&#39;s go on and look at this view.

118
00:07:50,491 --> 00:07:54,559
Remember we had these funny dots here in this view and

119
00:07:54,559 --> 00:07:55,954
these outliers.

120
00:07:55,954 --> 00:08:00,592
So, these pretty clearly are outliers, maybe those are,

121
00:08:00,592 --> 00:08:02,648
maybe we&#39;ve missed one.

122
00:08:02,648 --> 00:08:06,260
And again, we&#39;ve got some diesel and gas down here.

123
00:08:06,260 --> 00:08:11,786
And now, this is the plot of city.mpg.

124
00:08:11,786 --> 00:08:15,197
So we&#39;ve got a nice cluster of outliers, those three.

125
00:08:15,197 --> 00:08:17,160
But actually, it turns out there was four.

126
00:08:17,160 --> 00:08:18,575
See, there&#39;s another one there.

127
00:08:18,575 --> 00:08:22,184
So maybe we shouldn&#39;t add that one in our category,

128
00:08:22,184 --> 00:08:23,420
it&#39;s not clear.

129
00:08:24,860 --> 00:08:26,596
And certainly, those three.

130
00:08:26,596 --> 00:08:28,830
So, the diesel and two gas.

131
00:08:30,130 --> 00:08:32,874
Okay, so that looks promising.

132
00:08:32,874 --> 00:08:35,420
It looks like my initial hypothesis wasn&#39;t too bad.

133
00:08:35,419 --> 00:08:37,354
But let&#39;s look at what these are,

134
00:08:37,354 --> 00:08:40,630
what kind of cars are these?

135
00:08:40,630 --> 00:08:45,180
And so remember, I filtered for just those types and

136
00:08:45,180 --> 00:08:46,980
I&#39;m gonna scroll over here.

137
00:08:48,750 --> 00:08:52,187
And let&#39;s look at the make, what&#39;s the make here?

138
00:08:52,187 --> 00:08:56,071
And you see BMWs, a Chevrolet and

139
00:08:56,071 --> 00:08:59,967
a Honda which are kind of low end.

140
00:08:59,967 --> 00:09:02,530
Those are presumably are high mileage cars.

141
00:09:02,530 --> 00:09:05,670
Then some Jaguars, Mercedes-Benz.

142
00:09:05,670 --> 00:09:06,220
A Nissan,

143
00:09:06,220 --> 00:09:10,090
apparently another one of the high fuel efficiency cars.

144
00:09:10,090 --> 00:09:11,070
And Porsches.

145
00:09:11,070 --> 00:09:15,441
So it looks like we maybe have two classes of cars here that

146
00:09:15,441 --> 00:09:17,822
don&#39;t fit the general trend.

147
00:09:17,822 --> 00:09:21,717
These extremely high fuel efficiency, very small cars by

148
00:09:21,717 --> 00:09:25,620
these three manufacturers, and then these luxury cars.

149
00:09:25,620 --> 00:09:30,683
So maybe if we&#39;re gonna model automobile price, we might wanna

150
00:09:30,683 --> 00:09:35,652
have at least a special category for luxury cars, because they

151
00:09:35,652 --> 00:09:40,451
seem to have a different price behavior than ordinary cars.

152
00:09:40,451 --> 00:09:44,871
So anyway, I hope that&#39;s been instructive in terms of how we

153
00:09:44,871 --> 00:09:48,685
search for and identify valid, and to some extent,

154
00:09:48,685 --> 00:09:52,952
we&#39;ve done a little bit of validation of those outliers.

155
00:09:52,952 --> 00:09:55,854
If you were doing this for a very serious project, for

156
00:09:55,854 --> 00:09:59,145
a paying client, you&#39;d probably do some considerably more

157
00:09:59,145 --> 00:10:01,450
work than I&#39;ve showed you here.

158
00:10:01,450 --> 00:10:05,841
But at least I think you get the idea now of how we do this

159
00:10:05,841 --> 00:10:07,701
process in practice.

