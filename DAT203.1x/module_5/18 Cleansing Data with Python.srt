0
00:00:04,997 --> 00:00:09,349
So in this sequence, I&#39;m going to talk about doing

1
00:00:09,349 --> 00:00:13,316
a number of data manipulations with Python.

2
00:00:13,316 --> 00:00:18,757
So you&#39;ve already seen my example, my demo of doing

3
00:00:18,757 --> 00:00:24,440
some metadata manipulations, cleaning

4
00:00:24,440 --> 00:00:29,200
missing values, dealing with repeated values and a little bit

5
00:00:29,200 --> 00:00:32,420
of feature engineering using the tools in Azure Machine Learning.

6
00:00:32,420 --> 00:00:36,710
So in this sequence I&#39;m gonna do exactly the same operations but

7
00:00:36,710 --> 00:00:40,010
I&#39;m gonna do it in Python.

8
00:00:40,010 --> 00:00:44,660
Now if you&#39;re in the R track, you may wanna skip this video.

9
00:00:44,660 --> 00:00:49,949
And make sure you&#39;ve seen the parallel video for R.

10
00:00:49,949 --> 00:00:53,175
So if you look at my screen here,

11
00:00:53,175 --> 00:00:58,793
you&#39;ll see here&#39;s the chain of Azure Machine Learning

12
00:00:58,793 --> 00:01:02,629
modules that we&#39;ve been working on.

13
00:01:02,629 --> 00:01:05,361
So a number here for Metadata,

14
00:01:05,361 --> 00:01:10,195
then Clean Missing Data, Remove Duplicate Rows, and

15
00:01:10,195 --> 00:01:14,308
finally the bit for the feature engineering.

16
00:01:14,308 --> 00:01:19,306
So I&#39;ve added to where this code we had for the join in using

17
00:01:19,306 --> 00:01:24,920
the pandas merge method is, another Execute Python Script.

18
00:01:24,920 --> 00:01:28,769
So let me show you the code that goes there.

19
00:01:28,769 --> 00:01:32,947
So I have this function prep_auto which has two

20
00:01:32,947 --> 00:01:34,140
arguments.

21
00:01:34,140 --> 00:01:38,010
One is a data frame and the other is to set a column names.

22
00:01:39,200 --> 00:01:42,230
And I&#39;ll go through this, it&#39;s fairly systematic.

23
00:01:42,230 --> 00:01:43,730
I&#39;ll just go through it step by step.

24
00:01:43,730 --> 00:01:48,420
So the first thing we do is we rename the columns of that data

25
00:01:48,420 --> 00:01:52,169
frame with the column names, simple enough.

26
00:01:52,169 --> 00:01:54,120
Let me make this big.

27
00:01:54,120 --> 00:01:57,140
So then I create a list, which I&#39;m calling drop_list,

28
00:01:57,140 --> 00:02:00,940
of the names of the columns I want to remove.

29
00:02:00,940 --> 00:02:05,280
And I used the pandas drop method here.

30
00:02:05,280 --> 00:02:06,909
And so I&#39;ve got my drop_list,

31
00:02:06,909 --> 00:02:11,920
axis = 1 means I want to drop columns, not rows.

32
00:02:11,920 --> 00:02:13,600
And I&#39;m gonna do inplace = True.

33
00:02:13,600 --> 00:02:17,357
Notice I don&#39;t have to have data df equals df.drop,

34
00:02:17,357 --> 00:02:21,290
it&#39;s just df.drop because it&#39;s inplace.

35
00:02:21,290 --> 00:02:22,969
Now this is a little bit tricky.

36
00:02:22,969 --> 00:02:25,509
I&#39;m gonna remove the rows with missing values.

37
00:02:25,509 --> 00:02:28,795
So I already told you that there&#39;s,

38
00:02:28,795 --> 00:02:34,209
the isnull method is the best way to look for missing values.

39
00:02:34,209 --> 00:02:41,720
But I need to find rows with any missing value and remove those.

40
00:02:41,720 --> 00:02:45,489
So that&#39;s why I do the .any with (axis=1).

41
00:02:45,489 --> 00:02:52,739
So slightly tricky syntax there but hopefully, that&#39;s clear now.

42
00:02:52,739 --> 00:02:57,533
Then I wanna make sure that certain columns may show up as

43
00:02:57,533 --> 00:03:02,335
character columns when they really should be numeric.

44
00:03:02,335 --> 00:03:07,011
So I&#39;ve used this convert_objects method, and

45
00:03:07,011 --> 00:03:10,720
I&#39;m gonna convert_numeric=True.

46
00:03:10,720 --> 00:03:15,690
So you can use convert_objects method for

47
00:03:15,690 --> 00:03:17,230
various type conversions.

48
00:03:17,230 --> 00:03:19,580
But I&#39;m just gonna do that one here.

49
00:03:19,580 --> 00:03:21,750
I&#39;m going to create a new column, so

50
00:03:21,750 --> 00:03:26,070
I&#39;ve given it a new column name of lnprice.

51
00:03:26,070 --> 00:03:29,980
And I&#39;m using the numpy log of price.

52
00:03:29,980 --> 00:03:34,918
Notice I&#39;m taking just that one column but because it&#39;s numpy,

53
00:03:34,918 --> 00:03:37,628
I have to add an as_matrix method.

54
00:03:37,628 --> 00:03:41,218
Because numpy doesn&#39;t know how to work with pandas,

55
00:03:41,218 --> 00:03:43,060
data frames, and series.

56
00:03:43,060 --> 00:03:45,990
This is actually gonna give me a series at this point because I

57
00:03:45,990 --> 00:03:50,770
only have one column but numpy would choke on that.

58
00:03:50,770 --> 00:03:54,942
But it knows about arrays and matrices so

59
00:03:54,942 --> 00:03:59,368
that&#39;s what the as_matrix method does.

60
00:03:59,368 --> 00:04:02,650
And finally, I wanna get rid of the duplicates.

61
00:04:02,650 --> 00:04:05,873
So I&#39;m gonna use the drop_duplicates method

62
00:04:05,873 --> 00:04:06,870
from pandas.

63
00:04:06,870 --> 00:04:11,007
And I&#39;m gonna subset, that is, that&#39;s my key column, but

64
00:04:11,007 --> 00:04:13,371
they call it subset, is autonum.

65
00:04:13,371 --> 00:04:17,889
And again, I can have an option to do inplace = True.

66
00:04:17,889 --> 00:04:21,628
So you see I don&#39;t have to have a df equals df.drop duplicates.

67
00:04:21,628 --> 00:04:27,710
Finally, this somewhat funny nested list comprehension here.

68
00:04:27,710 --> 00:04:31,562
So I&#39;m gonna create a new column called num-cylinders, so

69
00:04:31,562 --> 00:04:33,989
that&#39;s the metadata manipulation.

70
00:04:33,989 --> 00:04:36,041
It&#39;s also, I guess you could say,

71
00:04:36,041 --> 00:04:37,829
a bit of feature engineering.

72
00:04:37,829 --> 00:04:42,264
And so I&#39;m gonna give it a name &#39;four-or-less&#39; if

73
00:04:42,264 --> 00:04:46,206
the value is &#39;two&#39;, &#39;three&#39;, &#39;four&#39;,

74
00:04:46,206 --> 00:04:51,232
else I&#39;m gonna call it &#39;five-six&#39; if it&#39;s in &#39;five&#39;,

75
00:04:51,232 --> 00:04:54,405
&#39;six&#39;, else &#39;eight-twelve&#39;.

76
00:04:54,405 --> 00:04:58,905
And so here&#39;s the iterator, for x in df, and

77
00:04:58,905 --> 00:05:04,236
then the column with the original number of cylinders

78
00:05:04,236 --> 00:05:09,579
which had seven levels, &#39;num-of-cylinders&#39;.

79
00:05:09,579 --> 00:05:10,890
So hopefully, you can follow that.

80
00:05:10,890 --> 00:05:16,178
That there&#39;s essentially two if-elses

81
00:05:16,178 --> 00:05:21,319
nested here and then this bit, for x in.

82
00:05:21,319 --> 00:05:25,099
So basically, the iterator runs through that column.

83
00:05:25,099 --> 00:05:32,919
You could do that with map as well.

84
00:05:32,919 --> 00:05:34,619
That would be another way to do that.

85
00:05:36,559 --> 00:05:38,479
So what&#39;s left to do?

86
00:05:38,479 --> 00:05:41,096
So I need a main function here, and

87
00:05:41,096 --> 00:05:44,899
I define these column names I want that are names.

88
00:05:44,899 --> 00:05:47,420
This is how I want it to look when I&#39;m done.

89
00:05:48,580 --> 00:05:51,370
And all I have to do is

90
00:05:51,370 --> 00:05:54,261
return what comes back from the prep_auto function.

91
00:05:55,270 --> 00:05:59,230
So simple enough, let me go back to Azure Machine Learning.

92
00:05:59,230 --> 00:06:03,750
And you see I&#39;ve already copied and paste that code,

93
00:06:03,750 --> 00:06:09,189
those two functions, into that Execute Python Script module.

94
00:06:09,189 --> 00:06:15,467
So I&#39;m just gonna save it, And

95
00:06:15,467 --> 00:06:19,449
run it, and my experiment has run.

96
00:06:19,449 --> 00:06:21,241
So let&#39;s visualize the output and

97
00:06:21,241 --> 00:06:24,529
just get a sanity check here, whether this makes sense or not.

98
00:06:27,388 --> 00:06:30,189
Now recall before, we had 193 columns.

99
00:06:30,189 --> 00:06:34,450
Yep, I&#39;m sorry, 193 rows and yep, we&#39;ve got them.

100
00:06:34,450 --> 00:06:37,760
So you can see some of the columns that were superfluous

101
00:06:37,760 --> 00:06:39,410
are now gone.

102
00:06:40,660 --> 00:06:44,742
And you can also see, for example, that where we had bore

103
00:06:44,742 --> 00:06:47,989
had some missing values, it no longer does.

104
00:06:47,989 --> 00:06:54,008
Stroke no longer has missing values.

105
00:06:54,008 --> 00:06:57,320
Price no longer has missing values.

106
00:06:57,320 --> 00:07:01,039
And we have some new columns, too, we have lnprice,

107
00:07:05,599 --> 00:07:10,120
Which has now a somewhat more uniform distribution.

108
00:07:10,120 --> 00:07:14,110
Remember, originally that price was pretty

109
00:07:14,110 --> 00:07:16,630
asymmetric distribution.

110
00:07:16,630 --> 00:07:19,420
Because most cars are fairly inexpensive and

111
00:07:19,420 --> 00:07:22,350
it&#39;s only the few cars that are very expensive.

112
00:07:22,350 --> 00:07:26,090
So by doing this log transformation, not only maybe

113
00:07:26,090 --> 00:07:29,798
have we made those relationships with some of those other

114
00:07:29,798 --> 00:07:36,730
variables more linear but we&#39;ve also changed

115
00:07:36,730 --> 00:07:40,060
the distributional properties that might be favorable.

116
00:07:40,060 --> 00:07:43,370
And finally we have num-cylinders here.

117
00:07:43,370 --> 00:07:47,889
And you can see it just has these three unique values,

118
00:07:47,889 --> 00:07:52,412
four-or-fewer, five-six, eight-twelve, so

119
00:07:52,412 --> 00:07:54,732
exactly what we expected.

120
00:07:54,732 --> 00:07:59,917
All right then, so now you&#39;ve seen how to do exactly the same

121
00:07:59,917 --> 00:08:04,698
data cleaning and manipulation in two different ways,

122
00:08:04,698 --> 00:08:09,680
one using the drag and drop tools in Azure Machine Learning

123
00:08:09,680 --> 00:08:13,370
and exactly analogous changes in Python.

124
00:08:13,370 --> 00:08:16,350
So now you have a somewhat bigger tool bag.

125
00:08:16,350 --> 00:08:20,078
And you can use either or both depending on the circumstance or

126
00:08:20,078 --> 00:08:21,950
what environment you&#39;re in.

127
00:08:21,950 --> 00:08:28,936
[BLANK AUDIO]

