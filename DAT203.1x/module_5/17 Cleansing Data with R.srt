0
00:00:05,242 --> 00:00:06,096
Hi.

1
00:00:06,096 --> 00:00:10,148
In previous demo sequences in this module,

2
00:00:10,148 --> 00:00:15,733
I&#39;ve been showing you how to use Azure Machine Learning to do

3
00:00:15,733 --> 00:00:21,757
a number of data manipulation, some metadata transformation,

4
00:00:21,757 --> 00:00:27,124
removing missing values, dealing with duplicate rows,

5
00:00:27,124 --> 00:00:32,623
and one feature engineering transformation of our label.

6
00:00:32,622 --> 00:00:35,864
So we&#39;ve done all that in Azure Machine Learning, and

7
00:00:35,864 --> 00:00:39,532
in this sequence, I&#39;m going to talk about how to do that in R.

8
00:00:39,532 --> 00:00:41,199
If you&#39;re in the Python track,

9
00:00:41,199 --> 00:00:44,039
you may want to skip this sequence and go to the parallel

10
00:00:44,039 --> 00:00:46,410
sequence where I do the same thing in Python.

11
00:00:47,490 --> 00:00:48,980
So if you look at my screen here,

12
00:00:48,980 --> 00:00:54,430
I&#39;m just going to remind you we had created this long chain

13
00:00:54,430 --> 00:00:59,610
of modules in Azure Machine Learning

14
00:00:59,610 --> 00:01:03,900
and done the operations.

15
00:01:03,900 --> 00:01:07,740
I talked about several metadata type operations,

16
00:01:07,740 --> 00:01:11,190
cleaned our missing data, removed duplicate rows and

17
00:01:11,190 --> 00:01:14,380
transformed our label column.

18
00:01:14,380 --> 00:01:15,480
So we can use and

19
00:01:15,480 --> 00:01:19,060
execute our script to do exactly that same thing.

20
00:01:19,060 --> 00:01:22,900
So let me switch to our Studio here.

21
00:01:22,900 --> 00:01:27,320
And I have this function prep.auto and it has two

22
00:01:27,320 --> 00:01:30,370
arguments, a data frame and some column name, okay?

23
00:01:32,280 --> 00:01:33,940
And let me just go through this step by step.

24
00:01:33,940 --> 00:01:37,840
It&#39;s very simple, systematic for the most part, and

25
00:01:37,840 --> 00:01:39,690
it&#39;s doing exactly what you&#39;ve seen.

26
00:01:39,690 --> 00:01:41,470
Just doing it in R cc,

27
00:01:41,470 --> 00:01:46,290
a parallel way you can do the exact same things in R.

28
00:01:47,450 --> 00:01:50,920
So I give the data frame some new names from this

29
00:01:50,920 --> 00:01:52,734
argument call.names.

30
00:01:54,450 --> 00:01:57,110
We filter out those unneeded columns.

31
00:01:57,110 --> 00:01:59,750
Remember there were four columns we didn&#39;t really want,

32
00:01:59,750 --> 00:02:01,820
so we filter those out.

33
00:02:01,820 --> 00:02:03,550
So we do that by,

34
00:02:03,550 --> 00:02:06,900
it&#39;s a slightly tricky syntax in this case.

35
00:02:06,900 --> 00:02:12,320
So there&#39;s a comma because we&#39;re filtering columns not rows,

36
00:02:12,320 --> 00:02:18,110
so if the name is not in this vector,

37
00:02:18,110 --> 00:02:21,950
you see the not and

38
00:02:21,950 --> 00:02:25,660
the in operator then we leave it in the data frame.

39
00:02:25,660 --> 00:02:28,770
So the next thing we do

40
00:02:28,770 --> 00:02:34,210
is we apply a log transformation using deep wire mutate.

41
00:02:34,210 --> 00:02:37,270
We create a new column named lnprice,

42
00:02:37,270 --> 00:02:39,790
which is just the log of price.

43
00:02:41,700 --> 00:02:43,268
We output that.

44
00:02:43,268 --> 00:02:47,522
We removed duplicate rows with complete.cases, so

45
00:02:47,522 --> 00:02:51,403
there&#39;s a R function called complete.cases.

46
00:02:51,403 --> 00:02:53,565
There&#39;s several ways to do this but

47
00:02:53,565 --> 00:02:56,240
this is the way I&#39;m doing it here.

48
00:02:56,240 --> 00:03:00,410
And, so I do that and notice the column here.

49
00:03:00,410 --> 00:03:04,200
So rows that are not complete incases,

50
00:03:04,200 --> 00:03:07,940
are going to be removed from the dataframe at that point.

51
00:03:07,940 --> 00:03:12,700
And then I remove duplicate rows, so the dataframe.

52
00:03:14,150 --> 00:03:18,080
Again, I&#39;m using the dplyr, in this case, filter function.

53
00:03:18,080 --> 00:03:21,274
So, if it&#39;s not duplicated,

54
00:03:21,274 --> 00:03:27,140
that autonum column, then it will be output, here,

55
00:03:28,160 --> 00:03:33,000
and then, I need to consolidate that number of cylinders.

56
00:03:33,000 --> 00:03:36,230
So, I am going to use mutate again and I am going to create

57
00:03:36,230 --> 00:03:41,526
a column with a different name in this case num.cylinders.

58
00:03:41,526 --> 00:03:44,180
So I have some nested if else.

59
00:03:44,180 --> 00:03:48,388
So the first ifl&#39;s is num of

60
00:03:48,388 --> 00:03:55,325
num.of.cylenders is in four, three or two, I&#39;m gonna call it,

61
00:03:55,325 --> 00:03:59,060
three-four as it turns out the two is get filtered out anyway.

62
00:04:00,420 --> 00:04:04,765
Then the else is another, so the else of this if else is

63
00:04:04,765 --> 00:04:09,130
another if else num.of.cylinders in five, six.

64
00:04:09,130 --> 00:04:10,760
Remember we call it five, six.

65
00:04:10,760 --> 00:04:14,630
Well, there&#39;s only one other choice left which is 8, 12.

66
00:04:14,630 --> 00:04:16,960
Okay, so simple enough.

67
00:04:16,959 --> 00:04:19,060
And then we just return that data frame.

68
00:04:19,060 --> 00:04:25,340
Now the rest of the code, this, set of column names and then

69
00:04:25,340 --> 00:04:29,280
some very basic Azure Machine Learning code that you&#39;ve seen.

70
00:04:29,280 --> 00:04:33,240
So I read a data frame which I&#39;m gonna call auto.price from

71
00:04:33,240 --> 00:04:34,830
port one.

72
00:04:34,830 --> 00:04:39,380
I call that prep.autos function with auto.price is the data

73
00:04:39,380 --> 00:04:42,880
frame and the column names as the column names, and

74
00:04:42,880 --> 00:04:47,190
I output the result to the results data set port.

75
00:04:47,190 --> 00:04:51,880
Now let me go back to Azure machine learning here, and

76
00:04:51,880 --> 00:04:53,630
here&#39;s that execute r script I&#39;ve added,

77
00:04:53,630 --> 00:04:56,240
I&#39;ve already copied that code in there, You see.

78
00:04:58,000 --> 00:05:02,510
So, all I&#39;ve got to do is save my experiment and run it.

79
00:05:02,510 --> 00:05:06,280
And there it&#39;s run.

80
00:05:06,280 --> 00:05:09,540
And let&#39;s visualize the output, there&#39;s a sanity check here.

81
00:05:09,540 --> 00:05:12,600
The kinda right thing of course, always helps.

82
00:05:14,760 --> 00:05:16,830
Okay, so we have the names that I wanted.

83
00:05:17,950 --> 00:05:20,730
Looks good and let&#39;s look at some of those

84
00:05:21,840 --> 00:05:25,356
columns like bore and stroke do they have any missing values?

85
00:05:25,356 --> 00:05:27,873
No and no.

86
00:05:27,873 --> 00:05:34,030
And notice I have the 193 rows and 27 columns here.

87
00:05:34,030 --> 00:05:37,440
I don&#39;t have any of the columns I don&#39;t want,

88
00:05:37,440 --> 00:05:41,647
price has no missing values, and here&#39;s the log price.

89
00:05:43,482 --> 00:05:46,031
And the new column also num.cylanders, and

90
00:05:46,031 --> 00:05:48,050
you see it has three unique values.

91
00:05:48,050 --> 00:05:52,609
So, it looks like all my R code did the transformations I wanted

92
00:05:52,609 --> 00:05:53,131
here.

93
00:05:53,131 --> 00:05:56,225
So, hopefully, that gives you some idea that you can do these

94
00:05:56,225 --> 00:05:58,668
transformations with Azure Machine Learning,

95
00:05:58,668 --> 00:05:59,694
you can do them in R.

96
00:05:59,694 --> 00:06:04,460
Now I&#39;ve done them in R, running an Azure Machine Learning to

97
00:06:04,460 --> 00:06:06,486
execute R script module.

98
00:06:06,486 --> 00:06:09,693
But there&#39;s no reason I couldn&#39;t have done the same thing in say,

99
00:06:09,693 --> 00:06:11,880
R Studio, or some other R environment.

100
00:06:11,880 --> 00:06:15,845
So that gives you a number of ways that you can use

101
00:06:15,845 --> 00:06:20,320
the tools in your bag to solve these kind of problems.

