0
00:00:04,902 --> 00:00:09,352
So in previous sequence, you&#39;ve seen how to search for

1
00:00:09,352 --> 00:00:13,233
and validate outliers using either R or Python,

2
00:00:13,233 --> 00:00:16,276
depending on which track you&#39;re on.

3
00:00:16,276 --> 00:00:20,541
So having some, I&#39;ve made the hypothesis,

4
00:00:20,541 --> 00:00:25,368
as you know, about which values are outliers, and

5
00:00:25,368 --> 00:00:31,960
we&#39;ve tested it, and it seems for the most part to hold up.

6
00:00:31,960 --> 00:00:35,730
So let&#39;s go about filtering those outliers.

7
00:00:35,730 --> 00:00:38,290
So in this demo, I&#39;m gonna show you how to do that

8
00:00:38,290 --> 00:00:40,389
using the tools in Azure Machine Learning.

9
00:00:41,660 --> 00:00:44,295
So you can look at my screen here, and

10
00:00:44,295 --> 00:00:46,594
I have this Clip Values module.

11
00:00:46,594 --> 00:00:51,640
Now, unfortunately, I can only do one condition at a time.

12
00:00:52,810 --> 00:00:57,900
So, remember, we had this long chain

13
00:00:57,900 --> 00:01:02,570
of data preparation, ending with our

14
00:01:02,570 --> 00:01:07,880
transformation of the price column to a log price, and

15
00:01:07,880 --> 00:01:12,870
giving it a more sensible name with the metadata editor module.

16
00:01:13,950 --> 00:01:18,360
So let&#39;s start here, we&#39;ve got a first clip value.

17
00:01:18,360 --> 00:01:23,300
So the way this works is, I can ClipPeaks, Subpeaks,

18
00:01:23,300 --> 00:01:26,010
by subpeaks, they mean lower values.

19
00:01:26,010 --> 00:01:29,210
So highest values, lowest values, or you can clip both.

20
00:01:29,210 --> 00:01:32,450
But I only want to clip peaks in this case.

21
00:01:32,450 --> 00:01:35,410
I&#39;m gonna use a constant as my threshold,

22
00:01:35,410 --> 00:01:37,010
you can also do a percentile.

23
00:01:38,370 --> 00:01:43,650
And I&#39;m gonna use engine-size, so in my column selector here,

24
00:01:43,650 --> 00:01:45,640
let me just make this little bigger for you,

25
00:01:47,310 --> 00:01:51,870
I&#39;ve set my column selection to engine-size.

26
00:01:51,870 --> 00:01:57,291
And we said any engine size over 190 was a suspicious value.

27
00:01:57,291 --> 00:01:59,410
And I&#39;m gonna substitute a missing value.

28
00:01:59,410 --> 00:02:00,590
See, I can do other things.

29
00:02:00,590 --> 00:02:03,847
I could substitute the threshold, so I could just say,

30
00:02:03,847 --> 00:02:06,271
if the car has an engine size over 190,

31
00:02:06,271 --> 00:02:08,226
I&#39;m just gonna say it is 190.

32
00:02:08,226 --> 00:02:11,892
I could take the mean or median of engine size,

33
00:02:11,892 --> 00:02:16,320
but I&#39;m just gonna make it a missing value in this case.

34
00:02:16,320 --> 00:02:19,231
And I&#39;m gonna overwrite, I&#39;m not gonna add an indicator column.

35
00:02:21,552 --> 00:02:25,307
So now that means I&#39;m gonna just clip and

36
00:02:25,307 --> 00:02:30,731
replace with missing values any engine size over 190.

37
00:02:30,731 --> 00:02:31,753
But, remember,

38
00:02:31,753 --> 00:02:34,990
we had three different variables we were filtering on.

39
00:02:34,990 --> 00:02:39,307
So this one, see, it&#39;s curb-weight now, and

40
00:02:39,307 --> 00:02:43,837
I&#39;m gonna say it&#39;s the same ClipPeaks constant,

41
00:02:43,837 --> 00:02:49,120
this time it&#39;s any car with a curb weight over 3,500.

42
00:02:49,120 --> 00:02:51,720
And again, I&#39;m gonna substitute a missing value.

43
00:02:52,970 --> 00:02:56,400
And finally, the same settings,

44
00:02:56,400 --> 00:03:01,530
but this time for city-mpg, city&#39;s miles per gallon.

45
00:03:01,530 --> 00:03:05,270
And we said any value over 40 was a suspect outlier.

46
00:03:06,420 --> 00:03:10,100
So, but notice I&#39;ve substituted missing values in one,

47
00:03:10,100 --> 00:03:11,580
two, three cases here.

48
00:03:11,580 --> 00:03:16,040
So I need to add a Clean Missing Data module to

49
00:03:16,040 --> 00:03:18,760
actually get rid of the rows with those outliers.

50
00:03:18,760 --> 00:03:21,370
So I&#39;ve launched the column selector.

51
00:03:21,370 --> 00:03:24,432
I selected all columns, because I don&#39;t really care,

52
00:03:24,432 --> 00:03:27,440
any missing values at this point I&#39;m gonna get rid of.

53
00:03:27,440 --> 00:03:30,094
That ratio, the missing value minimum and

54
00:03:30,094 --> 00:03:32,157
maximum ratios are set to zero and

55
00:03:32,157 --> 00:03:35,290
one, and I&#39;m just gonna remove the entire row.

56
00:03:35,290 --> 00:03:39,410
So let me save my experiment here then.

57
00:03:40,780 --> 00:03:44,039
And I&#39;m gonna run it for you and we&#39;ll see how we did.

58
00:03:46,587 --> 00:03:47,640
Okay, it&#39;s run.

59
00:03:49,320 --> 00:03:53,263
And let me visualize this output.

60
00:03:53,263 --> 00:03:59,390
So, remember, we had 193 rows before, now we&#39;ve got 174.

61
00:03:59,390 --> 00:04:03,020
So we&#39;ve removed 19 rows of automobile data.

62
00:04:03,020 --> 00:04:09,885
But let&#39;s look at some plots here real quick.

63
00:04:09,885 --> 00:04:16,655
Let me scroll over, and we&#39;ll do that versus log price.

64
00:04:16,654 --> 00:04:18,190
Put it down where you guys can see it.

65
00:04:21,170 --> 00:04:25,952
And, let&#39;s see, so first one we&#39;re

66
00:04:25,952 --> 00:04:30,744
gonna do is respect to curb-weight.

67
00:04:30,744 --> 00:04:36,470
So, you can see, any curb weight over 3500 is not there.

68
00:04:36,470 --> 00:04:40,520
So that group that was that kinda way out there is now no

69
00:04:40,520 --> 00:04:43,941
longer there, so that might be reasonable.

70
00:04:43,941 --> 00:04:46,543
And then we had engine-size.

71
00:04:46,543 --> 00:04:48,545
And remember, at 190,

72
00:04:48,545 --> 00:04:52,930
engine-size took on sort of a different characteristic.

73
00:04:52,930 --> 00:04:57,555
So, we&#39;ve chopped all those values off, although still quite

74
00:04:57,555 --> 00:05:01,249
a bit of dispersion with respect to price.

75
00:05:01,249 --> 00:05:04,364
And finally, city-mpg.

76
00:05:04,364 --> 00:05:09,263
So we don&#39;t have those very high fuel-efficiency cars anymore,

77
00:05:09,263 --> 00:05:14,410
the ones that were between 40 and 50 miles to the gallon.

78
00:05:14,410 --> 00:05:16,000
So it looks like our filtering worked.

79
00:05:17,430 --> 00:05:22,266
So you can see that, with a little bit of work,

80
00:05:22,266 --> 00:05:28,838
because we had three different filters that I needed to apply,

81
00:05:28,838 --> 00:05:34,418
we&#39;ve cut out what we suspect are outliers, and then

82
00:05:34,418 --> 00:05:40,629
used the Clean Missing Values module to remove those rows.

83
00:05:40,629 --> 00:05:43,487
So, with luck, we&#39;ve cleaned up our data set and

84
00:05:43,487 --> 00:05:46,017
got it in a form more suitable for analysis.

