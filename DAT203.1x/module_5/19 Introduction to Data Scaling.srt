0
00:00:04,764 --> 00:00:09,692
Today I&#39;m gonna discuss one last data transformation topic which

1
00:00:09,692 --> 00:00:12,511
is a very important topic of scaling.

2
00:00:12,511 --> 00:00:14,855
So if you have numeric features,

3
00:00:14,855 --> 00:00:19,209
they can be on all different scales, temperature, weight,

4
00:00:19,209 --> 00:00:23,145
number of people who walk through the door of the store,

5
00:00:23,145 --> 00:00:26,517
weight of railroad cars, all sorts of things.

6
00:00:26,517 --> 00:00:29,766
And there is no reason they have to be on the same scale.

7
00:00:29,766 --> 00:00:33,283
But to do a lot of serious predictive analytics and machine

8
00:00:33,283 --> 00:00:37,534
learning, having proper scale is really, really quite important.

9
00:00:37,534 --> 00:00:41,343
But let&#39;s dig in here and talk about that.

10
00:00:41,343 --> 00:00:46,985
When you&#39;re doing serious analytics, or machine learning,

11
00:00:46,985 --> 00:00:51,354
numeric variables need to be on a similar scale.

12
00:00:51,354 --> 00:00:54,894
And there are number of ways you can do that, for example,

13
00:00:54,894 --> 00:00:58,371
a simple one is, to scale to zero mean and unit variance.

14
00:00:58,371 --> 00:01:03,796
This is what Cynthia called Z score scaling.

15
00:01:03,796 --> 00:01:07,595
You also need to think about, you may need to de-trend,

16
00:01:07,595 --> 00:01:10,921
this is particularly for data that have an order,

17
00:01:10,921 --> 00:01:12,519
like time series data.

18
00:01:12,519 --> 00:01:16,976
If there&#39;s a strong trend and you&#39;re really only interested in

19
00:01:16,976 --> 00:01:21,432
the deviations from that trend, you need to remove that trend,

20
00:01:21,432 --> 00:01:24,009
so that&#39;s another form of scaling.

21
00:01:24,009 --> 00:01:27,340
Another very common use of scaling,

22
00:01:27,340 --> 00:01:32,392
especially if your distribution of data is some oddball

23
00:01:32,392 --> 00:01:36,911
thing that doesn&#39;t fit any simple description.

24
00:01:36,911 --> 00:01:41,081
So maybe you&#39;re questioning why is Z score scaling any good,

25
00:01:41,081 --> 00:01:42,926
well, min-max scaling,

26
00:01:42,926 --> 00:01:46,546
where you simply say I&#39;m gonna put all those values.

27
00:01:46,546 --> 00:01:50,792
I&#39;m just going to compress them, say into a range of zero to one,

28
00:01:50,792 --> 00:01:52,425
or something like that.

29
00:01:52,425 --> 00:01:57,248
It&#39;s a really powerful technique, it&#39;s very simple,

30
00:01:57,248 --> 00:02:01,176
and it&#39;s fairly distribution independent.

31
00:02:01,176 --> 00:02:04,128
One tip here, for example,

32
00:02:04,128 --> 00:02:10,159
we&#39;ve talked about identifying and treating outliers.

33
00:02:10,158 --> 00:02:13,246
Make sure you&#39;ve treated your outliers before you scale,

34
00:02:13,246 --> 00:02:16,826
because especially, imagine if you&#39;re doing min-max scaling and

35
00:02:16,826 --> 00:02:19,851
you have some outlier that&#39;s several orders of magnitude

36
00:02:19,851 --> 00:02:21,724
bigger than all your other values.

37
00:02:21,724 --> 00:02:26,245
You&#39;re gonna crunch all your real values down to some tiny

38
00:02:26,245 --> 00:02:28,828
range because you have this one or

39
00:02:28,828 --> 00:02:33,361
two outliers out here at some orders of magnitude bigger.

40
00:02:33,361 --> 00:02:37,413
So, let&#39;s look at a scatterplot here on the screen and

41
00:02:37,413 --> 00:02:41,553
you&#39;ll see, I&#39;ll develop an example for you here, so

42
00:02:41,553 --> 00:02:44,034
we&#39;ve looked at this plot a lot.

43
00:02:44,034 --> 00:02:47,715
So we have engine size on the vertical axis and cities,

44
00:02:47,715 --> 00:02:50,516
miles per gallon on the horizontal axis,

45
00:02:50,516 --> 00:02:53,892
this is obviously, for the automotive dataset.

46
00:02:53,892 --> 00:03:01,124
Notice the range, so it runs from about 50 to about 350 for

47
00:03:01,124 --> 00:03:06,173
engine size, and city miles per gallon runs

48
00:03:06,173 --> 00:03:11,232
from about 10 up to just over 50, okay.

49
00:03:11,232 --> 00:03:14,088
So right away we&#39;ve got more or less,

50
00:03:14,088 --> 00:03:16,587
almost in order of magnitude, or

51
00:03:16,587 --> 00:03:20,713
half in order of magnitude difference in the scaling.

52
00:03:20,713 --> 00:03:23,782
So if I adjust the plot now so

53
00:03:23,782 --> 00:03:28,720
that I&#39;ve scaled it on the widest range, so

54
00:03:28,720 --> 00:03:34,604
basically from zero to about 350, on both axis.

55
00:03:34,604 --> 00:03:37,235
Notice how that&#39;s now the same, so

56
00:03:37,235 --> 00:03:41,193
look at how compressed city miles to the gallon look.

57
00:03:41,193 --> 00:03:45,846
Its just nothing out here because the highest mirage car

58
00:03:45,846 --> 00:03:49,818
we have is 50 miles to the gallon more or less.

59
00:03:49,818 --> 00:03:54,526
So in our lowest mirage car likewise is probably ten or

60
00:03:54,526 --> 00:03:59,454
something so we&#39;ve got terribly compress scale here.

61
00:03:59,454 --> 00:04:04,129
Now imagine if you&#39;re trying to do some sort of machine learning

62
00:04:04,129 --> 00:04:07,082
with this and you haven&#39;t scaled this.

63
00:04:07,082 --> 00:04:11,282
Well, engine size, just because of larger numeric values is just

64
00:04:11,282 --> 00:04:13,918
going to dominate city miles per gallon.

65
00:04:13,918 --> 00:04:17,193
But there is no reason to believe that engine size is

66
00:04:17,192 --> 00:04:21,231
a better predictor of the price of an automobile than city miles

67
00:04:21,231 --> 00:04:22,068
per gallon,

68
00:04:22,067 --> 00:04:25,670
at least till this point we haven&#39;t done any analysis.

69
00:04:25,670 --> 00:04:30,733
So that&#39;s not really what we mean.

70
00:04:30,733 --> 00:04:36,708
Now I&#39;ve scaled, in this case, I&#39;ve Z-score scaled and

71
00:04:36,708 --> 00:04:41,608
you can see it&#39;s still biased because we still do

72
00:04:41,608 --> 00:04:45,432
have some outliers quite possibly but

73
00:04:45,432 --> 00:04:50,946
now we&#39;re on a scale of -2 to about 5 on both axises.

74
00:04:50,946 --> 00:04:54,216
Or maybe 4 and a half on both axises.

75
00:04:54,216 --> 00:04:58,375
So again like I said, we should have, I should have trimmed

76
00:04:58,375 --> 00:05:02,533
the outliers before they did that but for the purpose of this

77
00:05:02,533 --> 00:05:05,965
discussion you can see that I&#39;ve put the scale.

78
00:05:05,965 --> 00:05:10,087
So it&#39;s very similar so that engine size, just because engine

79
00:05:10,087 --> 00:05:14,285
size happened to be in units, they&#39;re numerically larger than

80
00:05:14,285 --> 00:05:18,111
city miles per gallon, doesn&#39;t dominate the solution.

81
00:05:18,111 --> 00:05:20,900
So let&#39;s talk about one last thing here which is

82
00:05:20,900 --> 00:05:22,300
the practical aspect.

83
00:05:22,300 --> 00:05:23,813
How do I scale?

84
00:05:23,813 --> 00:05:27,470
Well, there&#39;s a normalize data module which is quite nice and

85
00:05:27,470 --> 00:05:30,097
quite easy to use in Azure machine learning.

86
00:05:30,097 --> 00:05:34,825
In R, there&#39;s a simple function called scale, but

87
00:05:34,825 --> 00:05:39,134
make sure you scale only your numeric columns.

88
00:05:39,134 --> 00:05:46,043
And in Python there&#39;s nothing in pandas but scikit-learn.preprocessing

89
00:05:46,043 --> 00:05:48,598
package has a scale method.

90
00:05:48,598 --> 00:05:51,132
It also has some other if you look at the doc for

91
00:05:51,132 --> 00:05:53,335
that you&#39;ll see there&#39;s quite a few,

92
00:05:53,335 --> 00:05:55,828
there&#39;s a min-max which has a funny name.

93
00:05:55,828 --> 00:05:59,386
But you&#39;ll notice there&#39;s quite a lot of

94
00:05:59,386 --> 00:06:02,233
ways you can do that with Python,

95
00:06:02,233 --> 00:06:07,631
in this preprocessing package of the scikit- learn tool kit.

96
00:06:07,631 --> 00:06:11,795
So those are some practical ways that you can without really

97
00:06:11,795 --> 00:06:16,041
thinking about it much get the kind of scaling you want to make

98
00:06:16,041 --> 00:06:20,620
sure your numerical columns are in some range, similar range for

99
00:06:20,620 --> 00:06:22,472
all the numeric columns.

100
00:06:22,472 --> 00:06:26,548
So nobody is gonna dominate your training or your model and

101
00:06:26,548 --> 00:06:28,218
lead to weird results.

102
00:06:28,218 --> 00:06:32,189
So keep that in mind whenever you have numeric data that you

103
00:06:32,189 --> 00:06:33,736
really need to scale.

