0
00:00:04,884 --> 00:00:09,509
Hi, in previous demo, I showed you joins and

1
00:00:09,509 --> 00:00:13,176
we talked about joins in data flow.

2
00:00:13,176 --> 00:00:18,300
And in another demo, we talked about working with metadata.

3
00:00:19,580 --> 00:00:24,216
And so now in this demo, I&#39;m gonna continue with data

4
00:00:24,216 --> 00:00:27,617
preparation and talk about finding and

5
00:00:27,617 --> 00:00:32,785
treating missing values and dealing with duplicate data.

6
00:00:32,785 --> 00:00:34,558
So if you look on my screen here,

7
00:00:34,558 --> 00:00:36,598
I&#39;ve organize the experiment now,

8
00:00:36,598 --> 00:00:39,179
a little differently from what you&#39;ve seen.

9
00:00:39,179 --> 00:00:45,108
So I now have the upper part here is where we did the join.

10
00:00:45,108 --> 00:00:49,736
We had our reference data using that import data module

11
00:00:49,736 --> 00:00:54,563
where we imported some data about the make of the cars from

12
00:00:54,563 --> 00:00:56,287
a sequel database.

13
00:00:56,287 --> 00:01:02,130
And we have the auto data which had the car data in,

14
00:01:02,130 --> 00:01:04,367
and we join those.

15
00:01:04,367 --> 00:01:09,768
We did a number of steps here to work with metadata

16
00:01:09,768 --> 00:01:14,128
which I&#39;m not going to reiterate now.

17
00:01:14,128 --> 00:01:17,815
But what I&#39;d like to call your attention to are these last two

18
00:01:17,815 --> 00:01:22,460
steps which is clean missing data and remove duplicate rows.

19
00:01:22,460 --> 00:01:26,674
So let&#39;s just visualize the output here of where we

20
00:01:26,674 --> 00:01:31,093
are at this point in getting this data set ready to go.

21
00:01:31,093 --> 00:01:36,363
So recall we had the Make ID from the join,

22
00:01:36,363 --> 00:01:41,334
and we had this different columns, and

23
00:01:41,334 --> 00:01:48,429
if I go over here you&#39;ll see 4, has 4 Missing Values.

24
00:01:50,828 --> 00:01:59,080
Stroke has 4 missing values, and price which is an important.

25
00:01:59,080 --> 00:02:01,950
Attribute of this data set, also has four missing data.

26
00:02:03,380 --> 00:02:07,830
So we need to deal with that in some way.

27
00:02:09,440 --> 00:02:12,733
So in the clean missing data module,

28
00:02:12,733 --> 00:02:16,248
I can launch the column selector here.

29
00:02:16,248 --> 00:02:20,632
And just use a rule and select all columns, and

30
00:02:20,632 --> 00:02:24,689
then I have this missing data value ratio.

31
00:02:24,689 --> 00:02:28,384
So let me just expand that so you can see the whole x.

32
00:02:31,330 --> 00:02:35,811
So you could have a situation where you have a lot of

33
00:02:35,811 --> 00:02:37,954
missing values where.

34
00:02:37,954 --> 00:02:40,942
And you don&#39;t wanna completely decimate your data sets.

35
00:02:40,942 --> 00:02:44,800
So you can say, I&#39;m gonna accept the minimum and

36
00:02:44,800 --> 00:02:48,671
the maximum ratio of missing data in my data set.

37
00:02:48,671 --> 00:02:52,963
In this case, there&#39;s only maximum of 12 missing value.

38
00:02:52,963 --> 00:02:56,690
So we&#39;re not gonna lose that many rows, so I&#39;m just

39
00:02:56,690 --> 00:03:01,019
setting those ratios to zero and one, which is the default.

40
00:03:01,019 --> 00:03:03,043
And then I have to pick a cleaning mode and

41
00:03:03,043 --> 00:03:05,580
you need to kind of think about that.

42
00:03:05,580 --> 00:03:09,450
So I&#39;m picking remove the entire row, but you could also do

43
00:03:09,450 --> 00:03:14,450
things like Replace with MICE or Replace with Probabilistic PCA.

44
00:03:14,450 --> 00:03:20,210
These are fairly sophisticated imputation methods, we&#39;re not

45
00:03:20,210 --> 00:03:22,720
really gonna talk about them, but they&#39;re available to you.

46
00:03:22,720 --> 00:03:25,187
I could say, let&#39;s have a custom substitution value.

47
00:03:25,187 --> 00:03:30,780
I could say, let&#39;s just replace those values with zeros or with

48
00:03:30,780 --> 00:03:36,383
some string value like other or unknown or something like that.

49
00:03:36,383 --> 00:03:40,553
I could replace with a mean, a median, a mode, in ONL 7.

50
00:03:40,553 --> 00:03:43,709
And I could just remove an entire column if it has missing

51
00:03:43,709 --> 00:03:45,790
values, but I don&#39;t wanna do that,

52
00:03:45,790 --> 00:03:47,620
I&#39;m just gonna remove the row.

53
00:03:49,490 --> 00:03:52,280
Likewise, we have suspect

54
00:03:52,280 --> 00:03:56,470
that we have some duplicate values in this data set, and

55
00:03:56,470 --> 00:03:58,040
there are a number of ways you could do that.

56
00:03:58,040 --> 00:04:02,700
You could have columns that are say, key values that you know

57
00:04:02,700 --> 00:04:06,340
have to be unique, but we don&#39;t really have that here.

58
00:04:06,340 --> 00:04:10,769
So when I launch the column selector I have a rule of

59
00:04:10,769 --> 00:04:14,785
all features that I&#39;m going to look across.

60
00:04:14,785 --> 00:04:19,981
So if I find a row that&#39;s an exact duplicate across all

61
00:04:19,981 --> 00:04:24,930
columns of a previous row, I&#39;m gonna remove it.

62
00:04:24,930 --> 00:04:29,403
So I&#39;m gonna keep the first row of the duplicate and

63
00:04:29,403 --> 00:04:34,724
remove any and all subsequent instances of that duplicate.

64
00:04:34,724 --> 00:04:36,564
So let me run this experiment, and

65
00:04:36,564 --> 00:04:38,349
we&#39;ll see what the results are.

66
00:04:42,137 --> 00:04:47,246
The experiment is run, and we can look at what happened

67
00:04:47,246 --> 00:04:52,487
first off with our clean missing data processing here.

68
00:04:52,487 --> 00:04:56,627
And recall there were 212 rows when we started, and

69
00:04:56,627 --> 00:04:59,786
we had 12 missing values essentially.

70
00:04:59,786 --> 00:05:02,413
So apparently those were in different rows,

71
00:05:02,413 --> 00:05:05,370
because we&#39;re now down to 200 rows.

72
00:05:05,370 --> 00:05:10,650
But if I go over those columns, like bore

73
00:05:10,650 --> 00:05:17,060
has now 0 missing values, stroke has 0 missing values.

74
00:05:17,060 --> 00:05:22,550
And price has 0 missing values, exactly what I was hoping for.

75
00:05:22,550 --> 00:05:26,711
And I remove the duplicate rows, And

76
00:05:26,711 --> 00:05:28,213
we&#39;ll see what happened there.

77
00:05:31,342 --> 00:05:33,350
And we&#39;re down to 193 rows.

78
00:05:33,350 --> 00:05:38,345
So evidently, there were seven rows that were

79
00:05:38,345 --> 00:05:42,330
duplicates from this auto data set.

80
00:05:42,330 --> 00:05:47,290
So that gives you some basic background in how to do two very

81
00:05:47,290 --> 00:05:49,732
important data prep steps.

82
00:05:49,732 --> 00:05:52,547
Deal with missing values and

83
00:05:52,547 --> 00:05:56,775
deal with duplicate data in your data set.

