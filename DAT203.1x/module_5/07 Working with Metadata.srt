0
00:00:04,827 --> 00:00:09,148
So I just showed you some theoretical aspects of

1
00:00:09,148 --> 00:00:11,421
working with metadata.

2
00:00:11,421 --> 00:00:14,858
And in this sequence I&#39;d like to bring that down to earth and

3
00:00:14,858 --> 00:00:19,060
show you some concrete examples of how we work with metadata.

4
00:00:19,060 --> 00:00:23,330
And how what I just told you translates into actual

5
00:00:23,330 --> 00:00:26,200
steps you take when you&#39;re preparing your data set for,

6
00:00:26,200 --> 00:00:27,300
say, a machine learning.

7
00:00:28,390 --> 00:00:31,873
So I have on my screen here an experiment in

8
00:00:31,873 --> 00:00:34,069
Azure Machine Learning.

9
00:00:34,069 --> 00:00:35,497
So I&#39;m gonna show you these steps

10
00:00:35,497 --> 00:00:37,542
first in Azure Machine Learning, obviously.

11
00:00:37,542 --> 00:00:44,050
And there&#39;s five steps here that are all metadata steps.

12
00:00:44,050 --> 00:00:46,211
So I read in this data set, and

13
00:00:46,211 --> 00:00:49,421
let me just show you a little bit about that.

14
00:00:49,421 --> 00:00:51,810
And I&#39;m just gonna visualize it.

15
00:00:51,810 --> 00:00:54,206
So it has to do with the price of automobiles and

16
00:00:54,206 --> 00:00:56,356
it has a number of columns, for example,

17
00:00:56,356 --> 00:00:57,902
the make is the manufacture.

18
00:00:57,902 --> 00:01:00,876
And you can see it&#39;s a string feature, and

19
00:01:00,876 --> 00:01:03,610
there&#39;s 22 manufacturers.

20
00:01:03,610 --> 00:01:08,018
Fuel type has two features and basically, if you look down here

21
00:01:08,018 --> 00:01:11,856
you can see all the cars are either gas or diesel, etc.

22
00:01:11,856 --> 00:01:16,162
Then we have a number of other categorical features, and

23
00:01:16,162 --> 00:01:20,240
then we have things like, what&#39;s the wheel base.

24
00:01:20,240 --> 00:01:23,900
So that&#39;s a numeric feature now, okay?

25
00:01:24,940 --> 00:01:29,627
And that&#39;s all part of the types in the metadata.

26
00:01:29,627 --> 00:01:33,006
And then we have some other columns,

27
00:01:33,006 --> 00:01:36,940
like this is number of cylinders.

28
00:01:36,940 --> 00:01:37,780
And just kind of

29
00:01:37,780 --> 00:01:40,238
remember there&#39;s seven different values there.

30
00:01:40,238 --> 00:01:44,117
And notice that like 12 cylinders and 3 cylinders, and

31
00:01:44,117 --> 00:01:48,322
there really aren&#39;t many cars with that number of cylinders.

32
00:01:48,322 --> 00:01:51,749
In fact, I think that there is only one each, so

33
00:01:51,749 --> 00:01:53,570
it&#39;s pretty limited.

34
00:01:53,570 --> 00:01:59,080
Then finally, the variable that we&#39;re interested in predicting,

35
00:01:59,080 --> 00:02:00,590
which we&#39;re gonna call our label,

36
00:02:02,530 --> 00:02:06,490
is a continuous variable called price, okay?

37
00:02:06,490 --> 00:02:11,220
So that&#39;s just a quick tour through the data set,

38
00:02:11,220 --> 00:02:16,190
where the idea is to use those different aspects or features of

39
00:02:16,190 --> 00:02:20,240
a car to try to forecast what the price of that car could be.

40
00:02:21,860 --> 00:02:23,440
So what&#39;s the first thing we&#39;re gonna do?

41
00:02:24,780 --> 00:02:28,207
So some of those names were a little awkward with all

42
00:02:28,207 --> 00:02:29,648
the dashes and what.

43
00:02:29,648 --> 00:02:34,410
So we could, in this metadata editor module here,

44
00:02:34,410 --> 00:02:39,726
I&#39;ve selected, as you see, with the column selector,

45
00:02:39,726 --> 00:02:44,420
I start with no columns, I include all features.

46
00:02:44,420 --> 00:02:47,920
So I&#39;ve included all columns, all features, okay?

47
00:02:49,070 --> 00:02:53,733
And then I have this list here of, I hope you can see that in

48
00:02:53,733 --> 00:02:58,410
the lower right, of the featured names, column names.

49
00:02:58,410 --> 00:03:01,620
And I&#39;ve removed the dashes and I put them in this camel case.

50
00:03:01,620 --> 00:03:07,503
So normalized upper-case L losses, fuel, upper-case T type,

51
00:03:07,503 --> 00:03:12,977
etc., so just some slightly more convenient column names.

52
00:03:12,977 --> 00:03:17,000
But that&#39;s a typical metadata manipulation that you maybe take

53
00:03:17,000 --> 00:03:20,202
column names that aren&#39;t what you want for your own

54
00:03:20,202 --> 00:03:24,078
understanding or perhaps for presentation of your results to

55
00:03:24,078 --> 00:03:28,860
your colleagues, and give them some more meaningful names.

56
00:03:28,860 --> 00:03:32,490
Another bit of kind of metadata is there&#39;s a couple of

57
00:03:32,490 --> 00:03:35,610
columns in this data set that we really aren&#39;t gonna work with,

58
00:03:35,610 --> 00:03:39,040
and we should just remove them right up front.

59
00:03:39,040 --> 00:03:44,020
And so we&#39;ve got, we start with all columns and

60
00:03:44,020 --> 00:03:45,840
we&#39;re gonna exclude by column name,

61
00:03:45,840 --> 00:03:47,820
symbolizing and normalized losses.

62
00:03:47,820 --> 00:03:49,970
Don&#39;t worry about that red there.

63
00:03:49,970 --> 00:03:54,120
That is because I changed the column names.

64
00:03:54,120 --> 00:03:58,660
So it&#39;s now normalized upper case L losses and

65
00:03:58,660 --> 00:03:59,290
that should work.

66
00:04:01,090 --> 00:04:03,870
Then we&#39;re gonna do a little bit more metadata editing here,

67
00:04:03,870 --> 00:04:09,290
and recall that we had string features when we started.

68
00:04:09,290 --> 00:04:13,100
And we really want to make those into categorical features.

69
00:04:13,100 --> 00:04:16,310
So again with the column selector, I start with no

70
00:04:16,310 --> 00:04:20,460
columns, include by column type, just string features.

71
00:04:22,830 --> 00:04:26,570
And down here where it says categorical,

72
00:04:26,570 --> 00:04:28,290
I&#39;m gonna make them categorical.

73
00:04:31,180 --> 00:04:32,790
All right, so I showed you for

74
00:04:32,790 --> 00:04:37,220
example that that number of cylinders category had some

75
00:04:37,220 --> 00:04:39,500
categories that aren&#39;t that useful.

76
00:04:39,500 --> 00:04:42,150
And that&#39;s a very typical problem you run into.

77
00:04:42,150 --> 00:04:44,823
Where like in those cases, the 3 cylinder car,

78
00:04:44,823 --> 00:04:47,836
there was one in the 12 cylinder car, there was one.

79
00:04:47,836 --> 00:04:52,952
You can&#39;t do a lot of analysis with one data point.

80
00:04:52,952 --> 00:04:57,848
So it&#39;s very common with categorical variables that you

81
00:04:57,848 --> 00:05:01,520
wanna group them, bring them into fewer,

82
00:05:01,520 --> 00:05:06,730
more meaningful groupings for the categorical variables.

83
00:05:06,730 --> 00:05:10,600
So to do that I&#39;m using this Group Categorical Values module.

84
00:05:11,880 --> 00:05:16,118
And so on my column selector, you can see it&#39;s very simple.

85
00:05:16,118 --> 00:05:22,280
I&#39;ve selected the number of cylinders as my column.

86
00:05:24,740 --> 00:05:25,510
And what am I gonna do?

87
00:05:25,510 --> 00:05:27,694
So I&#39;m gonna say, I&#39;m gonna reduce that to,

88
00:05:27,694 --> 00:05:33,430
I&#39;m actually gonna have three, Three new levels,

89
00:05:33,430 --> 00:05:38,022
but I have to call it four here, because if there&#39;s a data

90
00:05:38,022 --> 00:05:41,849
value that doesn&#39;t fit into my new groupings,

91
00:05:41,849 --> 00:05:44,350
it&#39;s gonna get called other.

92
00:05:44,350 --> 00:05:46,380
I&#39;ve just defined that to be other, so

93
00:05:46,380 --> 00:05:48,320
that&#39;s my fourth column.

94
00:05:48,320 --> 00:05:51,655
So I have a new name of a level here,

95
00:05:51,655 --> 00:05:56,740
four-or-less, and I am going to group into that any car with

96
00:05:58,050 --> 00:06:00,568
with four, three, or two cylinders.

97
00:06:00,568 --> 00:06:05,695
Then I&#39;m gonna create another level five-six with five,six so

98
00:06:05,695 --> 00:06:08,860
any car with five or six cylinders.

99
00:06:08,860 --> 00:06:10,520
And then finally eight-twelve and

100
00:06:10,520 --> 00:06:12,010
you can probably guess that&#39;s eight, twelve.

101
00:06:12,010 --> 00:06:17,530
Okay, so now I have created just three levels from

102
00:06:17,530 --> 00:06:22,550
what were seven, but they&#39;re grouped more sensibly.

103
00:06:22,550 --> 00:06:27,000
So I have more cars, you know a better sample of cars per group.

104
00:06:27,000 --> 00:06:30,270
So finally, let me talk about one last

105
00:06:30,270 --> 00:06:35,110
thing we have to do here, which is create some indicator values.

106
00:06:35,110 --> 00:06:38,400
Now in certain cases, for example, which we&#39;ll get to,

107
00:06:39,750 --> 00:06:44,390
like clustering, you&#39;ll need to have only numeric columns.

108
00:06:44,390 --> 00:06:47,120
So we have these categorical values at this point.

109
00:06:48,910 --> 00:06:52,396
Essentially, you&#39;re trying to pivot those so

110
00:06:52,396 --> 00:06:55,356
that you wind up with a numeric column for

111
00:06:55,356 --> 00:06:58,768
each category of your categorical variable.

112
00:06:58,768 --> 00:07:03,728
And those numeric columns will be binary, for example,

113
00:07:03,728 --> 00:07:08,699
either a car has has five or six cylinders or it doesn&#39;t.

114
00:07:08,699 --> 00:07:12,549
So you have a 0 or a 1 value in that new categorical column,

115
00:07:12,549 --> 00:07:15,629
where cars with 8 and 12 cylinders, again,

116
00:07:15,629 --> 00:07:17,170
you&#39;d have a 0 or a 1.

117
00:07:17,170 --> 00:07:21,810
So to set that up, all I did

118
00:07:21,810 --> 00:07:25,670
was in the column selector, I start again with no columns.

119
00:07:25,670 --> 00:07:29,712
I include my column type, just the categorical.

120
00:07:29,712 --> 00:07:32,226
This doesn&#39;t make sense to do this for anything but

121
00:07:32,226 --> 00:07:33,910
categorical features.

122
00:07:33,910 --> 00:07:37,770
So we&#39;re trying to pivot categorical features and

123
00:07:37,770 --> 00:07:41,910
convert them to a set of binary numeric features.

124
00:07:41,910 --> 00:07:45,289
And then I&#39;ve clicked this overwrite, let me expand that,

125
00:07:45,289 --> 00:07:47,220
overwrite categorical columns.

126
00:07:47,220 --> 00:07:49,750
I don&#39;t want those old categorical columns if I&#39;m

127
00:07:49,750 --> 00:07:53,630
trying to only have numeric features in my data set.

128
00:07:53,630 --> 00:07:57,970
So this is a special case of metadata editing that

129
00:07:57,970 --> 00:07:58,970
you may run into,

130
00:07:58,970 --> 00:08:03,200
in fact you will run into in later parts of this course.

131
00:08:03,200 --> 00:08:06,815
So let me save my experiment and then I&#39;m gonna run it.

132
00:08:06,815 --> 00:08:08,900
All right, the experiment has run.

133
00:08:10,990 --> 00:08:17,039
And let&#39;s visualize the first output here.

134
00:08:17,039 --> 00:08:20,404
So first off, notice there&#39;s 26 columns presently, so

135
00:08:20,404 --> 00:08:21,759
remember that number.

136
00:08:21,759 --> 00:08:26,555
And you see that I have my new names like normalized uppercase

137
00:08:26,555 --> 00:08:31,080
L Losses, or numOfDoors with an upper case o and d, etc.

138
00:08:31,080 --> 00:08:35,330
So I made the column names something I&#39;m happier with.

139
00:08:38,380 --> 00:08:40,460
So then I&#39;ve selected some columns,

140
00:08:40,460 --> 00:08:42,287
remember I removed two columns.

141
00:08:42,287 --> 00:08:43,510
So let&#39;s see if that worked.

142
00:08:45,960 --> 00:08:50,060
And yes, I went from 26 columns to 24 columns, and

143
00:08:50,060 --> 00:08:54,760
you don&#39;t see, for example, that it was those first two columns.

144
00:08:54,760 --> 00:09:00,168
So the first column is now make as opposed to the symbolized and

145
00:09:00,168 --> 00:09:02,670
normalizedLosses, okay?

146
00:09:06,220 --> 00:09:09,951
Now this next step we made categorical

147
00:09:09,951 --> 00:09:13,574
features from the string features.

148
00:09:17,524 --> 00:09:20,458
And so let&#39;s just look at make here and

149
00:09:20,458 --> 00:09:23,580
indeed it is now a categorical feature.

150
00:09:23,580 --> 00:09:26,779
Still has 22 unique values, there&#39;s 22 levels.

151
00:09:29,398 --> 00:09:33,698
And if we go over to numOfCylinders,

152
00:09:33,698 --> 00:09:40,830
you see we still have seven different, unique values.

153
00:09:40,830 --> 00:09:45,230
But it&#39;s now categorical, all right, so remember that.

154
00:09:46,250 --> 00:09:49,579
So then we grouped our categorical variables.

155
00:09:49,579 --> 00:09:52,483
So that was for that numOfCylinders column.

156
00:09:55,348 --> 00:09:59,270
And we just looked at the original, with seven levels.

157
00:09:59,270 --> 00:10:04,930
Now we have this numOfCylinders(2) column and

158
00:10:04,930 --> 00:10:06,330
it has three unique values.

159
00:10:08,410 --> 00:10:12,350
Basically four or five, or four or

160
00:10:12,350 --> 00:10:15,970
fewer, five,six or eight and twelve.

161
00:10:15,970 --> 00:10:19,764
And you can see there&#39;s still not that many cars with say 8

162
00:10:19,764 --> 00:10:23,713
and 12 cylinders, but its maybe ten cars, or eight cars,

163
00:10:23,713 --> 00:10:27,160
as opposed to one car that had 12 cylinders.

164
00:10:27,160 --> 00:10:31,434
So we&#39;ve kinda cleaned up that situation with having these

165
00:10:31,434 --> 00:10:34,861
stray categories that only had one car in them.

166
00:10:37,974 --> 00:10:42,564
Finally, let me show you what happened with these indicator

167
00:10:42,564 --> 00:10:43,560
variables.

168
00:10:46,190 --> 00:10:48,750
So remember the first column was make.

169
00:10:48,750 --> 00:10:53,760
So now we have things like make-alfa-romeo, and

170
00:10:53,760 --> 00:10:59,470
we have noticed the first three cars were Alfa Romeo cars,

171
00:10:59,470 --> 00:11:01,230
so they&#39;re one.

172
00:11:01,230 --> 00:11:05,308
And then we have a whole bunch of zeros in that column,

173
00:11:05,308 --> 00:11:09,843
cuz we don&#39;t actually have that many Alfa Romeo cars in this

174
00:11:09,843 --> 00:11:11,220
data set and etc.

175
00:11:11,220 --> 00:11:13,990
So here we&#39;ve got Audis and

176
00:11:13,990 --> 00:11:17,350
you can see there&#39;s a few Audis that have ones, but again,

177
00:11:17,350 --> 00:11:23,590
mostly zeroes, cuz there are not that many Audis.

178
00:11:23,590 --> 00:11:25,055
So we actually, if you counted them,

179
00:11:25,055 --> 00:11:29,260
you&#39;d find that now there are 22 of these binary columns where we

180
00:11:29,260 --> 00:11:34,570
had 22 unique car makers.

181
00:11:34,570 --> 00:11:37,645
So for a simpler case it&#39;s like fuel, so

182
00:11:37,645 --> 00:11:39,640
we only had diesel or gas.

183
00:11:39,640 --> 00:11:44,628
So diesel, we could see they&#39;re mostly gas cars,

184
00:11:44,628 --> 00:11:48,340
and there&#39;s a few diesels down here.

185
00:11:48,340 --> 00:11:50,750
So they have ones and lots of gas cars.

186
00:11:50,750 --> 00:11:56,190
There&#39;s lots of ones and etc.

187
00:11:56,190 --> 00:12:01,300
So let&#39;s look at, The numOfCylinders.

188
00:12:01,300 --> 00:12:06,210
Remember number, that was the column we combined categories.

189
00:12:06,210 --> 00:12:10,074
So here we&#39;ve got now seven columns and

190
00:12:10,074 --> 00:12:15,600
some of them like this one, numOfCylinders-twelve.

191
00:12:15,600 --> 00:12:16,770
There&#39;s actually only

192
00:12:18,830 --> 00:12:23,490
one in that entire column and I&#39;m not sure where it is.

193
00:12:23,490 --> 00:12:26,498
I don&#39;t think we can get down, scroll down that far, nope.

194
00:12:26,498 --> 00:12:29,158
But it&#39;s kind of silly, right,

195
00:12:29,158 --> 00:12:32,960
to have a column with all zeros and a single one.

196
00:12:32,960 --> 00:12:38,300
It&#39;s not that meaningful statistically, so we fixed that.

197
00:12:38,300 --> 00:12:44,069
So we&#39;ve got, say, this column here with 8 and

198
00:12:44,069 --> 00:12:49,696
12, so now we have the eight cars that had 8 or

199
00:12:49,696 --> 00:12:56,055
12 cylinder engines are indicated in that column.

200
00:12:56,055 --> 00:12:59,570
So I hope that gives you some insight into how

201
00:12:59,570 --> 00:13:03,655
we take the principles of working with metadata and

202
00:13:03,655 --> 00:13:08,405
put them into practice in terms of how we can start preparing

203
00:13:08,405 --> 00:13:11,070
a data set for further analysis.

