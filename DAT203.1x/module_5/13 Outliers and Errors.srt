0
00:00:04,503 --> 00:00:07,290
Hi, welcome back.

1
00:00:07,290 --> 00:00:11,350
So in this sequence, I&#39;m gonna talk about the very important

2
00:00:11,350 --> 00:00:15,400
problem of trying to first off, locate and

3
00:00:15,400 --> 00:00:18,230
then treat or deal with outliers and errors.

4
00:00:18,230 --> 00:00:22,810
And this is a very difficult problem because first off,

5
00:00:22,810 --> 00:00:26,621
you need to determine is it really an outlier?

6
00:00:26,621 --> 00:00:28,734
Or maybe it’s an interesting value,

7
00:00:28,734 --> 00:00:31,958
just an unexpected value that you didn&#39;t expect.

8
00:00:31,958 --> 00:00:36,378
It may be really valuable data, so you need to be cautious when

9
00:00:36,378 --> 00:00:40,212
you approach this problem regardless of what you do.

10
00:00:40,212 --> 00:00:44,843
But the problem is if you don&#39;t treat true outliers and

11
00:00:44,843 --> 00:00:49,978
errors, they can bias your model training and throw off your

12
00:00:49,978 --> 00:00:55,115
analysis tremendously because they may just be meaningless

13
00:00:55,115 --> 00:01:00,067
garbage, assuming they&#39;re not some unexpected value.

14
00:01:00,067 --> 00:01:03,955
And errors and outliers can come from all sorts of sources,

15
00:01:03,955 --> 00:01:05,754
erroneous measurements.

16
00:01:09,641 --> 00:01:13,341
Data entry errors are very common, that someone types some

17
00:01:13,341 --> 00:01:16,302
meaningless something, cuz they think they in

18
00:01:16,302 --> 00:01:20,090
are another column of a form or something like that.

19
00:01:20,090 --> 00:01:22,710
Transposed values in a table,

20
00:01:22,710 --> 00:01:25,930
they put the temperature in the wrong column and

21
00:01:25,930 --> 00:01:30,845
the weight in the opposite column or something like that.

22
00:01:30,845 --> 00:01:35,140
Missed decimal places, that&#39;s a really common error instead of

23
00:01:35,140 --> 00:01:39,820
coming out as 23.0 you get 230 or something like that.

24
00:01:39,820 --> 00:01:42,950
So there&#39;s lots of human and

25
00:01:42,950 --> 00:01:46,650
sometimes systematic sources of outliers and errors.

26
00:01:48,110 --> 00:01:52,324
And generally, when we&#39;re hunting for outliers and errors,

27
00:01:52,324 --> 00:01:56,634
we use a combination of summary statistics and visualization.

28
00:01:56,634 --> 00:01:59,702
And we&#39;ve already talked a lot about summary statistics in

29
00:01:59,702 --> 00:02:02,230
an earlier module, and visualization as well.

30
00:02:04,520 --> 00:02:06,714
But let&#39;s just look at an example of visualizing

31
00:02:06,714 --> 00:02:07,469
some outliers.

32
00:02:07,469 --> 00:02:13,655
So one go to tool for me is the scatter plot matrix.

33
00:02:17,896 --> 00:02:22,072
And we&#39;ve looked at these tools already, R pairs plot or

34
00:02:22,072 --> 00:02:26,092
the Python, pandas.tools.plotting.scatter_m-

35
00:02:26,092 --> 00:02:26,710
atrix.

36
00:02:26,710 --> 00:02:28,720
So we&#39;ve already looked at those and

37
00:02:28,720 --> 00:02:32,190
you&#39;ve worked with those, but here&#39;s an example.

38
00:02:32,190 --> 00:02:37,730
So this is from the automotive price data set, so we have log

39
00:02:37,730 --> 00:02:41,440
price, city miles per gallon, engine size, curb weight.

40
00:02:42,670 --> 00:02:47,130
And notice that for example, engine size versus curb weight,

41
00:02:47,130 --> 00:02:53,070
we&#39;ve got these values out here, let me see them here.

42
00:02:53,070 --> 00:02:54,080
We&#39;ve got some values,

43
00:02:54,080 --> 00:02:58,360
engine size versus city miles per gallon that are out here or

44
00:02:58,360 --> 00:03:02,780
price versus engine size, we&#39;ve got some odd values, etc.

45
00:03:02,780 --> 00:03:06,980
So down here city miles per gallon and price.

46
00:03:06,980 --> 00:03:08,920
So by looking at different dimensions we&#39;re getting

47
00:03:08,920 --> 00:03:13,820
the idea that there might be some outliers in this data.

48
00:03:14,920 --> 00:03:19,320
So then how do we go in detail to track them down?

49
00:03:21,940 --> 00:03:25,960
So you can start maybe with just a histogram like this.

50
00:03:25,960 --> 00:03:29,880
So this is the log price, and well maybe those are outliers.

51
00:03:29,880 --> 00:03:31,690
You see they are sort of separated

52
00:03:31,690 --> 00:03:35,310
from the main distribution, and then what about these?

53
00:03:35,310 --> 00:03:37,830
Maybe those are outliers too over here on the left.

54
00:03:40,320 --> 00:03:44,000
Scatter plots are really useful, but as with

55
00:03:44,000 --> 00:03:46,840
any data exploration make sure you look at multiple views.

56
00:03:48,000 --> 00:03:52,330
So here we have a scatter plot that should be familiar by now.

57
00:03:52,330 --> 00:03:54,420
We have log price on the vertical axis and

58
00:03:54,420 --> 00:03:56,970
cities miles per gallon on the horizontal axis.

59
00:03:58,220 --> 00:03:59,570
And you can notice some things for

60
00:03:59,570 --> 00:04:03,220
example, these seem like fairly obvious outliers.

61
00:04:04,420 --> 00:04:06,264
Everything lines up like this,

62
00:04:06,264 --> 00:04:09,826
except these guys are way over here at very high fuel economy.

63
00:04:09,826 --> 00:04:11,097
Now it could be that those

64
00:04:11,097 --> 00:04:13,822
automobiles are just exceptionally efficient, and

65
00:04:13,822 --> 00:04:15,649
that might be an interesting case.

66
00:04:17,310 --> 00:04:20,570
And then we&#39;ve got these, you see the main bulk of the data,

67
00:04:20,570 --> 00:04:23,850
if you look at these overlapping dots, it has a trend.

68
00:04:23,850 --> 00:04:25,470
It&#39;s trending down here, but

69
00:04:25,470 --> 00:04:29,130
all of a sudden you&#39;ve got these guys up here.

70
00:04:29,130 --> 00:04:30,070
So what about that?

71
00:04:31,340 --> 00:04:34,150
And then we&#39;ve got this group of three that seems

72
00:04:34,150 --> 00:04:36,290
a little out of place maybe, maybe not.

73
00:04:39,010 --> 00:04:42,150
So if you&#39;ve identified some outliers and like I said,

74
00:04:42,150 --> 00:04:44,330
you need to really look at multiple views,

75
00:04:44,330 --> 00:04:47,560
don&#39;t just go for one or two views here.

76
00:04:49,220 --> 00:04:51,010
What are some treatments?

77
00:04:51,010 --> 00:04:56,280
So censoring, which simply means perhaps removing

78
00:04:56,280 --> 00:05:01,220
the row that contains the outlier or the suspected error.

79
00:05:01,220 --> 00:05:06,040
Trimming, so trimming is a process whereby you say,

80
00:05:06,040 --> 00:05:11,440
well I know this instrument reads on a scale of 0 to 100.

81
00:05:11,440 --> 00:05:14,580
And it&#39;ll never read over 100 or less than 0.

82
00:05:14,580 --> 00:05:17,380
So you might trim values that are beyond that range

83
00:05:17,380 --> 00:05:20,447
back to either 0 if they&#39;re outliers on the low side or

84
00:05:20,447 --> 00:05:21,861
100 on the high side.

85
00:05:21,861 --> 00:05:25,659
I could interpolate, I could say, especially if it&#39;s a time

86
00:05:25,659 --> 00:05:29,430
series or something that has data that has some order.

87
00:05:29,430 --> 00:05:33,140
I could say, well let&#39;s just maybe linearly or spline

88
00:05:33,140 --> 00:05:40,190
interpolation or something along there to come up with that.

89
00:05:40,190 --> 00:05:44,040
I could have a substitute value, I could say maybe zero.

90
00:05:44,040 --> 00:05:46,690
Maybe if the values are just way off,

91
00:05:46,690 --> 00:05:49,540
I&#39;m just gonna say it&#39;s zero, and that might be good for

92
00:05:49,540 --> 00:05:53,290
what I&#39;m doing, and then there&#39;s tools to do it.

93
00:05:53,290 --> 00:05:56,062
So there&#39;s the clip values module in Azure Machine

94
00:05:56,062 --> 00:05:59,671
Learning, which has quite a bit of flexibility, although it also

95
00:05:59,671 --> 00:06:02,326
has some limitations as you&#39;ll see in the demo.

96
00:06:05,998 --> 00:06:08,832
We can do it with R or if you are using Python,

97
00:06:08,832 --> 00:06:12,600
if you are on that track, you can do it with Python.

98
00:06:12,600 --> 00:06:13,310
And R and

99
00:06:13,310 --> 00:06:16,010
Python have the advantage that you can do very complex

100
00:06:16,010 --> 00:06:20,080
filtering with relatively simple expressions in those languages.

101
00:06:20,080 --> 00:06:21,600
So let&#39;s look at an example of that.

102
00:06:22,950 --> 00:06:28,450
So R, the general case is my final data frame is,

103
00:06:28,450 --> 00:06:34,590
the data frame with some filter expression for the rows.

104
00:06:34,590 --> 00:06:40,260
Notice the comma here, so I&#39;m filtering typically on rows and

105
00:06:40,260 --> 00:06:42,330
here&#39;s an example of that.

106
00:06:42,330 --> 00:06:48,320
Using dplyr, I have frame1 and then frame1 and

107
00:06:48,320 --> 00:06:51,700
I chain it into a whole series of filter expressions here.

108
00:06:53,670 --> 00:06:56,572
I can also do that like I said, it was data frame notation as I

109
00:06:56,572 --> 00:06:58,735
showed up here, either works really well.

110
00:07:02,173 --> 00:07:05,061
With Python again you do DataFrame =

111
00:07:05,061 --> 00:07:08,150
DataFrame[filter_expression].

112
00:07:08,150 --> 00:07:08,942
Here&#39;s an example of that.

113
00:07:14,854 --> 00:07:18,178
So we&#39;ve talked about how to find outliers,

114
00:07:18,178 --> 00:07:21,390
possible treatments of outliers.

115
00:07:21,390 --> 00:07:24,830
And some practical tools that we have in our bag here that

116
00:07:24,830 --> 00:07:28,550
we can use to take care of outliers.

117
00:07:28,550 --> 00:07:31,281
So I hope that gives you some food for thought on how you&#39;re

118
00:07:31,281 --> 00:07:33,748
gonna approach dealing with outliers in your work.

