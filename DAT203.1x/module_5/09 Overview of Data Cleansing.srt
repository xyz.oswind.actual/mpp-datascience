0
00:00:05,291 --> 00:00:10,113
So in these next few sequences, I&#39;m gonna talk about various

1
00:00:10,113 --> 00:00:14,843
data cleaning and transformation processes that, again,

2
00:00:14,843 --> 00:00:19,571
as data scientists, we do this over and over again, because

3
00:00:19,571 --> 00:00:24,140
every data set needs some clean up, some reformulating.

4
00:00:25,305 --> 00:00:29,001
So what are we gonna talk about here in these next four

5
00:00:29,001 --> 00:00:29,935
sequences?

6
00:00:29,935 --> 00:00:33,770
So first off, I&#39;m gonna talk about just the overall process

7
00:00:33,770 --> 00:00:36,711
of how you can think about data preparations.

8
00:00:36,711 --> 00:00:38,060
It&#39;s a very iterative process.

9
00:00:38,060 --> 00:00:43,410
There isn&#39;t a linear path through it in the real world.

10
00:00:43,410 --> 00:00:47,470
One of the issues we hit a lot, our missing and

11
00:00:47,470 --> 00:00:50,230
repeated values, and so

12
00:00:50,230 --> 00:00:53,715
we&#39;ll talk about identifying those and treating them.

13
00:00:53,715 --> 00:00:59,080
Outliers and errors in our data are perennial problem for

14
00:00:59,080 --> 00:01:02,830
data scientist, so we&#39;ll spend a little time on that.

15
00:01:02,830 --> 00:01:04,500
And finally the issue of scaling,

16
00:01:04,500 --> 00:01:06,530
scaling numeric features so that

17
00:01:07,710 --> 00:01:11,960
when we go to build a machine learning model, it actually

18
00:01:11,960 --> 00:01:16,570
works the way we want it to using the features correctly.

19
00:01:19,650 --> 00:01:22,310
So data cleaning and

20
00:01:22,310 --> 00:01:26,380
transformation is just, often, we just call it data munging

21
00:01:26,380 --> 00:01:29,740
when we&#39;re talking between ourselves as data scientists.

22
00:01:29,740 --> 00:01:35,150
There&#39;s no defined limit on what that can mean.

23
00:01:35,150 --> 00:01:40,170
It just depends on the dataset you have and what&#39;s wrong with

24
00:01:40,170 --> 00:01:43,170
it that you may need to clean up or do something about.

25
00:01:44,440 --> 00:01:48,571
So, in fact, I think in my life I&#39;ve never been handed a nice

26
00:01:48,571 --> 00:01:51,736
clean dataset that just worked out of the box.

27
00:01:51,736 --> 00:01:56,592
So I&#39;ve always wound up doing some data munging on any data

28
00:01:56,592 --> 00:01:57,988
I&#39;ve taken on.

29
00:01:57,988 --> 00:02:01,573
And it&#39;s often my most time-consuming part

30
00:02:01,573 --> 00:02:02,900
of the process.

31
00:02:02,900 --> 00:02:05,010
I&#39;ve already mentioned this before and

32
00:02:05,010 --> 00:02:08,658
there&#39;s all sorts of stats out there about data scientists

33
00:02:08,657 --> 00:02:14,490
doing 80-year 90% of their time on these data munging tasks.

34
00:02:14,490 --> 00:02:17,950
And I don&#39;t think that&#39;s gonna change very soon.

35
00:02:17,950 --> 00:02:19,860
And it&#39;s an iterative process.

36
00:02:19,860 --> 00:02:21,960
You never get it right the first time.

37
00:02:21,960 --> 00:02:23,960
Plan on going back and forth and

38
00:02:23,960 --> 00:02:25,590
testing what you&#39;re doing a little bit.

39
00:02:27,170 --> 00:02:29,780
And visualizations,

40
00:02:29,780 --> 00:02:32,510
we&#39;ve already talked about visualization a lot.

41
00:02:32,510 --> 00:02:35,460
Visualization is your friend in these cases because

42
00:02:35,460 --> 00:02:38,280
if you can see some relationships from the data

43
00:02:38,280 --> 00:02:41,760
that just don&#39;t look right, that&#39;s usually a good

44
00:02:41,760 --> 00:02:44,040
cue that you&#39;ve got some more data munging to do.

45
00:02:45,290 --> 00:02:50,760
And the more data munging you can do as

46
00:02:50,760 --> 00:02:54,850
you get to the modeling stage, the machine learning models,

47
00:02:54,850 --> 00:02:58,160
you&#39;ve headed off those problems, if you can.

48
00:02:58,160 --> 00:03:01,410
But some may slip through, so even at the modeling stage you

49
00:03:01,410 --> 00:03:05,610
may see some strange behavior that just isn&#39;t right and

50
00:03:05,610 --> 00:03:09,390
you may need to go back and do some more data munging.

51
00:03:10,570 --> 00:03:13,810
So you&#39;ve already seen this diagram.

52
00:03:13,810 --> 00:03:16,195
This is an old idea.

53
00:03:16,195 --> 00:03:21,030
I&#39;m not saying this is how we always do it.

54
00:03:21,030 --> 00:03:29,200
But just gives you some general setting for

55
00:03:29,200 --> 00:03:33,920
how this works, and so from that point of view, it&#39;s a bit useful

56
00:03:33,920 --> 00:03:36,610
to think about that you need to have business understanding,

57
00:03:36,610 --> 00:03:39,870
data understanding, which we&#39;ve talked about a lot in

58
00:03:39,870 --> 00:03:44,740
the visualization and data exploration module,

59
00:03:44,740 --> 00:03:49,780
data preparation which is the main focus of this module.

60
00:03:49,780 --> 00:03:52,530
Modeling, which we&#39;ll get to, evaluation,

61
00:03:52,530 --> 00:03:56,600
deployment, so notice that every one of these stages,

62
00:03:56,600 --> 00:04:00,350
you probably could draw an arrow from any of these as feedback

63
00:04:00,350 --> 00:04:03,960
and you&#39;re going back and forth between them.

64
00:04:03,960 --> 00:04:08,500
But notice that right in the middle here is data preparation

65
00:04:08,500 --> 00:04:12,420
and that&#39;s just a key thing that we have to do.

66
00:04:12,420 --> 00:04:16,308
So anyway, I hope that sets a little bit of context around

67
00:04:16,308 --> 00:04:19,873
what may seem like a rather mundane discussion, but

68
00:04:19,873 --> 00:04:23,845
actually is really important if you wanna have successful

69
00:04:23,845 --> 00:04:25,970
modeling as a data scientist.

