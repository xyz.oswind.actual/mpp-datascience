0
00:00:04,825 --> 00:00:06,250
Hi and welcome.

1
00:00:06,250 --> 00:00:09,120
In this sequence, we&#39;re going to talk about the very

2
00:00:09,120 --> 00:00:10,810
important subject of metadata.

3
00:00:10,810 --> 00:00:13,085
And I should say it&#39;s a very important and

4
00:00:13,085 --> 00:00:14,584
often neglected subject.

5
00:00:14,584 --> 00:00:17,383
Because it seems sorta maybe nebulous or

6
00:00:17,383 --> 00:00:21,590
almost peripheral to what we&#39;re doing as data scientists.

7
00:00:21,590 --> 00:00:25,700
But really, having a bit of a grasp on certain important

8
00:00:25,700 --> 00:00:27,960
aspects of metadata is really gonna help you.

9
00:00:28,960 --> 00:00:31,530
So in this chapter we&#39;re gonna talk about data types.

10
00:00:32,689 --> 00:00:36,569
We&#39;re gonna talk about continuous and discreet values.

11
00:00:36,569 --> 00:00:39,360
We&#39;re gonna talk about categorical variables.

12
00:00:40,620 --> 00:00:44,754
We&#39;re gonna talk a little bit about the Azure Machine Learning

13
00:00:44,754 --> 00:00:47,726
tools available for working with metadata.

14
00:00:47,726 --> 00:00:53,608
And keep in mind, there&#39;s analogous tools in R,

15
00:00:53,608 --> 00:00:58,650
sometimes in dplyr, sometimes in base R,

16
00:00:58,650 --> 00:01:01,750
and in Python and Pandas.

17
00:01:01,750 --> 00:01:05,094
And finally, we&#39;re gonna talk about another allied topic,

18
00:01:05,093 --> 00:01:08,319
which is quantization of categorical variables briefly.

19
00:01:08,319 --> 00:01:12,609
So, to remind you, Azure Machine Learning,

20
00:01:12,609 --> 00:01:15,139
and all analytic systems,

21
00:01:15,139 --> 00:01:19,879
have some set number of data types that you can use.

22
00:01:19,879 --> 00:01:23,671
For example, one numeric type might be floating point.

23
00:01:23,671 --> 00:01:26,490
Another might be integer.

24
00:01:26,490 --> 00:01:29,392
Boolean is for logicals.

25
00:01:29,392 --> 00:01:32,717
String for character data.

26
00:01:32,717 --> 00:01:37,787
Categorical, which I&#39;ll talk about in a minute.

27
00:01:37,787 --> 00:01:42,918
Date-time, which it could be the year,

28
00:01:42,918 --> 00:01:46,876
the month, the day, the hour,

29
00:01:46,876 --> 00:01:51,138
minute, second, millisecond.

30
00:01:51,138 --> 00:01:55,160
And that&#39;s one of the issues with some metadata like that.

31
00:01:55,160 --> 00:01:59,408
In different parts of the world there&#39;s different conventions

32
00:01:59,408 --> 00:02:02,484
for representing exactly the same date time.

33
00:02:02,484 --> 00:02:06,009
So be aware of that, you may need to do some conversions.

34
00:02:06,009 --> 00:02:10,493
Time-spans are differences in time from

35
00:02:10,493 --> 00:02:14,858
a start to an end, or some other period.

36
00:02:14,858 --> 00:02:19,563
And they can be anything from a few microsecond to years,

37
00:02:19,563 --> 00:02:24,268
but there&#39;s still a time-span as opposed to an absolute

38
00:02:24,268 --> 00:02:25,455
date-time.

39
00:02:25,455 --> 00:02:29,454
And then there&#39;s many, many possible specialized data types,

40
00:02:29,454 --> 00:02:32,949
such as in Azure ML, there&#39;s an image types supported.

41
00:02:32,949 --> 00:02:34,820
But there&#39;s lots of other special types.

42
00:02:36,370 --> 00:02:40,743
And keep in mind, and that&#39;s the purpose of bringing this up

43
00:02:40,743 --> 00:02:43,327
here, that data type is metadata.

44
00:02:43,327 --> 00:02:47,367
So, anytime you have a column of data in a table of any sort, and

45
00:02:47,367 --> 00:02:51,257
it has a certain type, that&#39;s metadata about that column,

46
00:02:51,257 --> 00:02:53,730
as is, say, the name of that column.

47
00:02:53,730 --> 00:02:54,780
That&#39;s also metadata.

48
00:02:56,770 --> 00:02:59,800
So, let&#39;s talk about continuous versus discrete variables.

49
00:03:01,470 --> 00:03:05,330
So, a continuous variable can take on any value,

50
00:03:05,330 --> 00:03:07,460
so, within the resolution.

51
00:03:07,460 --> 00:03:09,540
So, for example, temperature.

52
00:03:09,540 --> 00:03:12,850
If I have a thermometer that reads to the nearest tenth of

53
00:03:12,850 --> 00:03:16,820
degrees centigrade, it&#39;s basically a continuous

54
00:03:16,820 --> 00:03:20,830
measurement from whatever

55
00:03:20,830 --> 00:03:23,990
the range of measurements that that thermometer will handle.

56
00:03:25,300 --> 00:03:28,230
Although, it&#39;s a little bit quantized in the sense that,

57
00:03:28,230 --> 00:03:30,760
again, I said it&#39;s only good to the nearest tenth

58
00:03:30,760 --> 00:03:33,080
of a degree centigrade.

59
00:03:33,080 --> 00:03:36,908
Distance, similarly, is a continuous variable.

60
00:03:36,908 --> 00:03:42,850
Distance can be anything from nanometers to millions and

61
00:03:42,850 --> 00:03:46,380
millions of kilometers.

62
00:03:46,380 --> 00:03:48,874
Weight, similarly, is a continuous variable.

63
00:03:48,874 --> 00:03:53,774
There&#39;s no reason a weight of an object has to be a discreet

64
00:03:53,774 --> 00:03:54,480
value.

65
00:03:54,480 --> 00:03:57,974
But there are discreet variables that have fixed values.

66
00:03:57,974 --> 00:04:01,491
For example, maybe the number of people walking

67
00:04:01,491 --> 00:04:05,961
through a door into our store, that&#39;s a discreet variable.

68
00:04:05,961 --> 00:04:08,680
It has to have an integer number of people.

69
00:04:08,680 --> 00:04:12,560
You&#39;re not gonna have 1.3 people walk into your store.

70
00:04:12,560 --> 00:04:14,920
It&#39;s either gonna be one or two.

71
00:04:14,920 --> 00:04:19,045
The number of wheels on a vehicle, it&#39;s a countable thing.

72
00:04:19,045 --> 00:04:21,485
And so therefore, it has a discreet value.

73
00:04:21,485 --> 00:04:24,167
And knowing whether something&#39;s continuous and

74
00:04:24,167 --> 00:04:26,920
discreet is often an important piece of metadata.

75
00:04:28,790 --> 00:04:31,420
Categorical variables are important and

76
00:04:31,420 --> 00:04:34,540
sometimes confusing bit of metadata.

77
00:04:34,540 --> 00:04:36,860
So the categories themselves are the metadata.

78
00:04:36,860 --> 00:04:41,460
So say I have small, medium, large, are my categories.

79
00:04:41,460 --> 00:04:42,940
That&#39;s the metadata.

80
00:04:42,940 --> 00:04:47,364
The fact that certain rows have small and many other rows might

81
00:04:47,364 --> 00:04:52,128
have medium, and maybe only one row has large, that&#39;s the data,

82
00:04:52,128 --> 00:04:56,066
but the metadata tells me what those categories can b.

83
00:04:56,066 --> 00:05:00,086
And there&#39;s a problem too if you have too many categories that

84
00:05:00,086 --> 00:05:01,407
leads to problems.

85
00:05:01,407 --> 00:05:06,245
For example, I just gave a example where large only had one

86
00:05:06,245 --> 00:05:08,331
member in that data set.

87
00:05:08,331 --> 00:05:11,693
[INAUDIBLE], there&#39;s not a lot of analysis you can do if you

88
00:05:11,693 --> 00:05:14,162
only have one example of that category, or

89
00:05:14,162 --> 00:05:17,830
maybe you have no examples, maybe you have empty categories.

90
00:05:17,830 --> 00:05:20,790
So, that&#39;s a problem itself.

91
00:05:20,790 --> 00:05:25,080
And also, when you think about machine learning modelling or

92
00:05:25,080 --> 00:05:29,400
any analytics model, having a large number of dimensions,

93
00:05:29,400 --> 00:05:32,580
the categories equal dimensions is gonna cause problems.

94
00:05:32,580 --> 00:05:34,980
We&#39;ll talk about that more later, but

95
00:05:34,980 --> 00:05:38,160
just keep in mind that too many categories,

96
00:05:38,160 --> 00:05:40,950
too fine a grain of category, is a problem.

97
00:05:42,680 --> 00:05:46,430
And so, we often need to combine categories to reduce their

98
00:05:46,430 --> 00:05:49,290
numbers and try to group like categories.

99
00:05:49,290 --> 00:05:51,690
You&#39;ve already seen that in some of the examples we&#39;ve done with

100
00:05:51,690 --> 00:05:54,680
the automotive data where we&#39;ve grouped

101
00:05:54,680 --> 00:05:58,280
cars with similar number of cylinders together.

102
00:05:58,280 --> 00:06:01,980
Let&#39;s talk about a practical tool for dealing with metadata,

103
00:06:01,980 --> 00:06:03,350
and it&#39;s actually quite a nice tool.

104
00:06:03,350 --> 00:06:05,590
The Azure Machine Learning Metadata Editor.

105
00:06:07,200 --> 00:06:13,244
So, it deals with certain metadata types.

106
00:06:13,244 --> 00:06:16,300
Data types, you can change data types.

107
00:06:16,300 --> 00:06:19,643
And you can change categories, like do the grouping of

108
00:06:19,643 --> 00:06:21,975
categories that I just talked about.

109
00:06:21,975 --> 00:06:26,067
There&#39;s certain special field types, like it can be a feature

110
00:06:26,067 --> 00:06:29,060
or a label, which apply to the columns.

111
00:06:29,060 --> 00:06:32,230
And you can say, rename the columns.

112
00:06:32,230 --> 00:06:36,270
So that&#39;s another metadata manipulation you can do.

113
00:06:36,270 --> 00:06:37,419
So these are typical.

114
00:06:37,419 --> 00:06:41,228
I bring this up not only because it&#39;s something you&#39;re gonna be

115
00:06:41,228 --> 00:06:43,485
doing in Azure Machine Learning, but

116
00:06:43,485 --> 00:06:47,364
also because these are typical metadata manipulations that data

117
00:06:47,364 --> 00:06:50,906
scientists do regardless of the environment they&#39;re in.

118
00:06:50,906 --> 00:06:54,436
And regardless of what environment in you&#39;re gonna need

119
00:06:54,436 --> 00:06:55,759
some kind of editor or

120
00:06:55,759 --> 00:06:58,710
some kind of tool to manipulate your metadata.

121
00:07:00,240 --> 00:07:02,560
So finally, let me talk about an allied subject,

122
00:07:02,560 --> 00:07:04,860
which is quantizing continuous variables.

123
00:07:04,860 --> 00:07:07,890
You may say, well, why would I bother?

124
00:07:07,890 --> 00:07:13,970
Well, first off, you may really want a categorical feature.

125
00:07:13,970 --> 00:07:18,220
There are things that are measured continuously

126
00:07:18,220 --> 00:07:22,860
that might not really work well as continuous variables,

127
00:07:22,860 --> 00:07:26,250
maybe really better off as a categorical.

128
00:07:26,250 --> 00:07:29,575
So, some examples might be small, medium, large.

129
00:07:29,575 --> 00:07:32,423
So to be a little more specific,

130
00:07:32,423 --> 00:07:36,856
say we&#39;re trying to build a marketing campaign.

131
00:07:36,856 --> 00:07:40,755
And we have income of a lot of people.

132
00:07:40,755 --> 00:07:44,791
And it varies widely from people who make very little to people

133
00:07:44,791 --> 00:07:46,741
who are fabulously wealthy.

134
00:07:46,741 --> 00:07:50,690
But we actually don&#39;t care maybe that much whether someone makes

135
00:07:50,690 --> 00:07:53,520
10,000 or 20,000.

136
00:07:53,520 --> 00:07:57,003
We care that they&#39;re below 25,000.

137
00:07:57,003 --> 00:08:00,751
We might care that they&#39;re between 25 and 50,000.

138
00:08:00,751 --> 00:08:04,728
Or 50,000 and 100, 100 and 250,000, or

139
00:08:04,728 --> 00:08:06,795
greater than 250,000.

140
00:08:06,795 --> 00:08:11,721
So those are categories that might be more meaningful for

141
00:08:11,721 --> 00:08:14,697
analysis, and say charting, and

142
00:08:14,697 --> 00:08:19,750
even machine learning, than that continuous variable.

143
00:08:19,750 --> 00:08:25,080
Just to wrap this up, I hope I&#39;ve given you a flavor for

144
00:08:25,080 --> 00:08:29,670
why you should put some energy and some thought into

145
00:08:29,670 --> 00:08:34,570
thinking about and not just taken for granted your metadata.

146
00:08:34,570 --> 00:08:37,800
It&#39;s an important part of your overall data set.

147
00:08:37,799 --> 00:08:41,735
And as a data scientist, if you spend that extra time and

148
00:08:41,735 --> 00:08:45,086
think about and properly set up your metadata,

149
00:08:45,086 --> 00:08:48,296
your life is gonna be just a whole lot easier.

