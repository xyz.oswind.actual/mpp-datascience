0
00:00:00,000 --> 00:00:03,570
So in previous sequences we&#39;ve looked at

1
00:00:03,570 --> 00:00:07,200
using the dplyr verbs we&#39;ve used

2
00:00:07,200 --> 00:00:10,380
them to filter data frames to manipulate

3
00:00:10,380 --> 00:00:13,710
rows and columns to do computations on

4
00:00:13,710 --> 00:00:15,360
data frames but we&#39;ve always been using

5
00:00:15,360 --> 00:00:18,900
a single verb at a time but in many

6
00:00:18,900 --> 00:00:20,880
cases you actually need multiple

7
00:00:20,880 --> 00:00:24,180
transformation selections whatever on a

8
00:00:24,180 --> 00:00:26,670
data frame to get down to the final

9
00:00:26,670 --> 00:00:29,519
result that you actually require - and

10
00:00:29,519 --> 00:00:32,700
dplyr has this very powerful method for

11
00:00:32,700 --> 00:00:35,250
chaining verbs you can create very long

12
00:00:35,250 --> 00:00:38,280
chains of verbs to do exactly the

13
00:00:38,280 --> 00:00:40,890
operations you want to get complex and

14
00:00:40,890 --> 00:00:43,010
results

15
00:00:43,010 --> 00:00:44,719
in my screen here i have a summary of

16
00:00:44,719 --> 00:00:47,030
what that syntax is so I have my result

17
00:00:47,030 --> 00:00:49,820
equals whatever my initial data frame

18
00:00:49,820 --> 00:00:52,729
name is and then I chain that with this

19
00:00:52,729 --> 00:00:56,180
percent arrow percent operator into the

20
00:00:56,180 --> 00:00:58,280
first verb with whatever arguments that

21
00:00:58,280 --> 00:01:01,549
requires the output of that first verb

22
00:01:01,549 --> 00:01:04,519
is then chained into the second verb

23
00:01:04,519 --> 00:01:06,409
with whatever arguments and i can add a

24
00:01:06,409 --> 00:01:09,350
third fourth fifth however many argument

25
00:01:09,350 --> 00:01:12,500
however many verbs I need to get to this

26
00:01:12,500 --> 00:01:14,060
final result

27
00:01:14,060 --> 00:01:18,050
just make sure that at each step

28
00:01:18,050 --> 00:01:20,179
here whenever you chain one verb into

29
00:01:20,179 --> 00:01:24,170
another that the output schema of this

30
00:01:24,170 --> 00:01:26,840
verb is compatible with the input

31
00:01:26,840 --> 00:01:28,399
expected by the next verb or you&#39;re

32
00:01:28,399 --> 00:01:31,090
going to have some strange problems

33
00:01:31,090 --> 00:01:35,020
so let&#39;s have a look at

34
00:01:35,020 --> 00:01:39,880
at some of the chain examples so let&#39;s

35
00:01:39,880 --> 00:01:42,400
start with the initial

36
00:01:42,400 --> 00:01:45,010
auto.price data set we have and

37
00:01:45,010 --> 00:01:47,740
here&#39;s again my chain operator and i&#39;m

38
00:01:47,740 --> 00:01:52,430
going to apply a filter so that

39
00:01:52,430 --> 00:01:55,640
I only get cars made by Audi now

40
00:01:55,640 --> 00:01:57,320
remember the first time we applied this

41
00:01:57,320 --> 00:01:59,720
filter i had the name

42
00:01:59,720 --> 00:02:02,540
of the data frame in there as the first

43
00:02:02,540 --> 00:02:05,540
argument but when I chain that argument

44
00:02:05,540 --> 00:02:07,970
is implied by whatever i&#39;m getting

45
00:02:07,970 --> 00:02:09,950
through the chain operator in this case

46
00:02:09,949 --> 00:02:12,980
the full initial data frame so then that

47
00:02:12,980 --> 00:02:16,610
subset that&#39;s just Audis goes through

48
00:02:16,610 --> 00:02:18,590
another chain operator and now i can

49
00:02:18,590 --> 00:02:22,070
select columns drive wheels wheel base

50
00:02:22,070 --> 00:02:25,070
curb weight etc so let me run that for

51
00:02:25,070 --> 00:02:28,069
you and you&#39;ll see what we get

52
00:02:28,069 --> 00:02:30,670
and

53
00:02:30,670 --> 00:02:33,250
you see it&#39;s this it&#39;s essentially what

54
00:02:33,250 --> 00:02:36,310
we had before when we did this as two

55
00:02:36,310 --> 00:02:38,830
separate steps with two separate with

56
00:02:38,830 --> 00:02:42,660
the two separate verbs here

57
00:02:42,750 --> 00:02:45,330
so it&#39;s just those six out automobiles

58
00:02:45,330 --> 00:02:48,670
and just the columns we selected

59
00:02:48,670 --> 00:02:50,560
now when you&#39;re chaining you can also

60
00:02:50,560 --> 00:02:54,190
include the dplyr groupby now you

61
00:02:54,190 --> 00:02:58,960
can use the groupby verb to group

62
00:02:58,960 --> 00:03:03,790
a data frame anyway but i find it

63
00:03:03,790 --> 00:03:07,600
particularly useful in a verb chain so I

64
00:03:07,600 --> 00:03:11,170
have again auto.price i&#39;m going to

65
00:03:11,170 --> 00:03:15,910
group it by the drive wheels of the car

66
00:03:15,910 --> 00:03:19,170
i&#39;m going to create a summary

67
00:03:19,170 --> 00:03:20,760
and I&#39;m this case I&#39;m going to have the

68
00:03:20,760 --> 00:03:24,840
count the mean price standard deviation

69
00:03:24,840 --> 00:03:27,900
the max and the min price so i&#39;m going to

70
00:03:27,900 --> 00:03:31,620
create a table for cars grouped

71
00:03:31,620 --> 00:03:34,739
by the type of Drive Wheels that car has

72
00:03:34,739 --> 00:03:36,959
so it&#39;s just two verbs groupby and

73
00:03:36,959 --> 00:03:39,140
summarize

74
00:03:39,140 --> 00:03:41,690
and there you have it so here&#39;s my drive

75
00:03:41,690 --> 00:03:44,180
wheel types the three drive wheel types

76
00:03:44,180 --> 00:03:47,150
and the statistics i computed with

77
00:03:47,150 --> 00:03:52,010
summarize grouped by those drive wheel

78
00:03:52,010 --> 00:03:54,980
times so let me just show you one last

79
00:03:54,980 --> 00:03:59,300
example here so with a few more verbs so

80
00:03:59,300 --> 00:04:02,140
first off I&#39;m going to filter

81
00:04:02,140 --> 00:04:04,680
just to have Toyotas

82
00:04:04,680 --> 00:04:06,569
and I&#39;m going to group that by body

83
00:04:06,569 --> 00:04:09,269
style and again i&#39;m going to summarize

84
00:04:09,269 --> 00:04:11,760
what&#39;s going to give a similar result

85
00:04:11,760 --> 00:04:15,010
but a different grouping

86
00:04:15,010 --> 00:04:17,730
just by body style

87
00:04:17,730 --> 00:04:21,240
and you notice in this case there&#39;s a

88
00:04:21,240 --> 00:04:25,320
missing value for convertibles weight

89
00:04:25,320 --> 00:04:27,150
in fact they&#39;re all missing because it

90
00:04:27,150 --> 00:04:29,850
turns out so so we do wind up with an NA

91
00:04:29,850 --> 00:04:33,120
but  you

92
00:04:33,120 --> 00:04:37,070
can have several different

93
00:04:38,510 --> 00:04:41,960
classes of

94
00:04:41,960 --> 00:04:44,870
excuse me several different categorical

95
00:04:44,870 --> 00:04:49,669
columns named in the groupby so you

96
00:04:49,669 --> 00:04:51,650
could get more complex groupings than

97
00:04:51,650 --> 00:04:55,069
what i&#39;m showing you so that gives you

98
00:04:55,069 --> 00:04:58,220
some insight into how you can create

99
00:04:58,220 --> 00:05:02,270
very complex powerful transformations

100
00:05:02,270 --> 00:05:04,370
and selections of your data frames using

101
00:05:04,370 --> 00:05:07,580
this concept of chaining dplyr verbs

102
00:05:07,580 --> 00:05:10,729
and some examples using groupby in

103
00:05:10,729 --> 00:05:13,130
those chains in this case only we&#39;ve

104
00:05:13,130 --> 00:05:15,080
only done it to compute some simple

105
00:05:15,080 --> 00:05:16,729
summary statistics but you can do much

106
00:05:16,729 --> 00:05:18,680
more sophisticated things when you need

107
00:05:18,680 --> 00:05:20,830
to

