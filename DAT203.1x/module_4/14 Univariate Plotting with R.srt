0
00:00:00,000 --> 00:00:03,330
Hello and welcome in the sequence i&#39;m

1
00:00:03,330 --> 00:00:06,509
going to show you how we as data

2
00:00:06,509 --> 00:00:09,030
scientists use different chart types to

3
00:00:09,030 --> 00:00:10,740
explore the relationships and data

4
00:00:10,740 --> 00:00:14,940
specifically we&#39;re going to use R and

5
00:00:14,940 --> 00:00:18,720
the R ggplot2 package to create those

6
00:00:18,720 --> 00:00:22,289
plots and i&#39;m using ggplot2 because it&#39;s

7
00:00:22,289 --> 00:00:25,710
a very powerful package that has a lot

8
00:00:25,710 --> 00:00:27,599
of nice features you can use both for

9
00:00:27,599 --> 00:00:30,840
exploration and for presentation of your

10
00:00:30,840 --> 00:00:33,579
charts

11
00:00:33,579 --> 00:00:36,129
so specifically in this first video

12
00:00:36,129 --> 00:00:38,050
we&#39;re just going to look at univariate

13
00:00:38,050 --> 00:00:40,570
plots plots for a single dimension of

14
00:00:40,570 --> 00:00:42,129
the data set where we look at one

15
00:00:42,129 --> 00:00:45,519
dimension of the time and those plots

16
00:00:45,519 --> 00:00:50,949
include bar plots histograms box blocks

17
00:00:50,949 --> 00:00:53,500
kernel density estimation plots and

18
00:00:53,500 --> 00:00:56,799
violin plots so we&#39;ll get started here

19
00:00:56,799 --> 00:01:01,119
first I&#39;m going to load the data set and

20
00:01:01,119 --> 00:01:03,969
we&#39;re not going to spend much time on

21
00:01:03,969 --> 00:01:06,009
this part of it but basically we have to

22
00:01:06,009 --> 00:01:08,950
convert some of the columns that show up

23
00:01:08,950 --> 00:01:12,250
as character to numeric and then we have

24
00:01:12,250 --> 00:01:15,549
to remove some missing values

25
00:01:15,549 --> 00:01:17,679
we&#39;re just going to discard rows that

26
00:01:17,679 --> 00:01:20,060
have missing values

27
00:01:20,060 --> 00:01:22,850
we run that for you

28
00:01:22,850 --> 00:01:25,700
and you can see there were some missing

29
00:01:25,700 --> 00:01:28,400
values here and we can get a quick look

30
00:01:28,400 --> 00:01:31,970
at what&#39;s in this data set here and you

31
00:01:31,970 --> 00:01:35,080
see we have 26

32
00:01:35,220 --> 00:01:39,690
variables in this data set and we&#39;re

33
00:01:39,690 --> 00:01:41,550
just going to focus on a few for the

34
00:01:41,550 --> 00:01:43,740
purpose of this lesson but the important

35
00:01:43,740 --> 00:01:45,510
thing is notice some are integer some are

36
00:01:45,510 --> 00:01:47,400
character some are numeric which means

37
00:01:47,400 --> 00:01:51,290
basically floating point

38
00:01:52,960 --> 00:01:55,330
so let&#39;s get started with our first plot

39
00:01:55,330 --> 00:01:58,810
type which is a bar plot now to run

40
00:01:58,810 --> 00:02:02,409
ggplot2 you of course have to load it in

41
00:02:02,409 --> 00:02:04,060
this case i&#39;m using the R require

42
00:02:04,060 --> 00:02:07,630
function and i&#39;m also using repr the

43
00:02:07,630 --> 00:02:11,200
R repertoire package so that i can set

44
00:02:11,200 --> 00:02:15,310
the width and height of my plot. repr

45
00:02:15,310 --> 00:02:18,850
has many plotting utilities

46
00:02:18,850 --> 00:02:20,860
which you may want to look at but at

47
00:02:20,860 --> 00:02:22,780
this point we&#39;re just using this one

48
00:02:22,780 --> 00:02:27,400
simple one and so what&#39;s the syntax here

49
00:02:27,400 --> 00:02:31,000
for ggplot2 so I&#39;ve loaded the package

50
00:02:31,000 --> 00:02:35,560
and i have ggplot is my first function i

51
00:02:35,560 --> 00:02:38,300
call from the package

52
00:02:38,300 --> 00:02:40,460
have the name of this data frame we just

53
00:02:40,460 --> 00:02:42,910
loaded and looked at

54
00:02:42,910 --> 00:02:46,990
and I have this aes function and i&#39;m

55
00:02:46,990 --> 00:02:49,300
only since i&#39;m doing a univariate plot

56
00:02:49,300 --> 00:02:51,970
i&#39;m only going to have one  variable

57
00:02:51,970 --> 00:02:54,760
so I&#39;m just saying body.style you&#39;ll

58
00:02:54,760 --> 00:02:56,290
see what happens when we get to to

59
00:02:56,290 --> 00:03:00,370
multivariate plots in a while and then I

60
00:03:00,370 --> 00:03:02,890
have to specify some attributes that

61
00:03:02,890 --> 00:03:04,960
plot and in this case we&#39;re going to

62
00:03:04,960 --> 00:03:06,910
make a bar plot so I&#39;m doing geom_bar

63
00:03:06,910 --> 00:03:09,820
and notice there&#39;s this

64
00:03:09,820 --> 00:03:12,490
chaining operator this which is a plus

65
00:03:12,490 --> 00:03:15,520
sign and ggplot2 so so basically the way

66
00:03:15,520 --> 00:03:17,200
you read this is i&#39;m going to use

67
00:03:17,200 --> 00:03:20,290
ggplot2 plot something from this auto.price

68
00:03:20,290 --> 00:03:22,750
data frame in this case it&#39;s

69
00:03:22,750 --> 00:03:25,569
the body.style

70
00:03:25,569 --> 00:03:29,739
variable or feature and i&#39;m going to

71
00:03:29,739 --> 00:03:34,389
make the plot attribute a bar plot so

72
00:03:34,389 --> 00:03:37,280
let me run this for you

73
00:03:37,280 --> 00:03:40,550
and there we have as advertised a bar

74
00:03:40,550 --> 00:03:43,100
plot so we have body.style across

75
00:03:43,100 --> 00:03:48,319
the horizontal axis and the counts of

76
00:03:48,319 --> 00:03:50,780
the number of cars with those body

77
00:03:50,780 --> 00:03:54,170
styles on our vertical axis and so right

78
00:03:54,170 --> 00:03:56,060
away we can see sedans are the most

79
00:03:56,060 --> 00:03:59,450
common hatchback so the next wagons it&#39;s

80
00:03:59,450 --> 00:04:00,650
actually get hard you know if you have

81
00:04:00,650 --> 00:04:02,360
like convertibles and hardtops it i

82
00:04:02,360 --> 00:04:04,160
guess it&#39;s clear here that hard tops are

83
00:04:04,160 --> 00:04:05,900
more common than convertibles in this

84
00:04:05,900 --> 00:04:08,510
dataset but in many cases especially if

85
00:04:08,510 --> 00:04:10,040
you have a lot of categories that&#39;s

86
00:04:10,040 --> 00:04:12,440
difficult so sometimes it&#39;s a really

87
00:04:12,440 --> 00:04:16,519
good idea to order your data set so

88
00:04:16,519 --> 00:04:18,709
notice I&#39;m doing something different

89
00:04:18,709 --> 00:04:22,250
here is slightly gnarly R syntax but

90
00:04:22,250 --> 00:04:24,680
i&#39;m using this reorder function on body

91
00:04:24,680 --> 00:04:30,020
style and without going into the gory

92
00:04:30,020 --> 00:04:33,620
details here basically i had i have an

93
00:04:33,620 --> 00:04:35,840
anonymous function so i&#39;m going to order

94
00:04:35,840 --> 00:04:39,080
them based on when i say -length so it&#39;s

95
00:04:39,080 --> 00:04:41,090
going to go from the most common to the

96
00:04:41,090 --> 00:04:45,080
least common again i have geom_bar and

97
00:04:45,080 --> 00:04:46,400
i&#39;m going to do something else if you

98
00:04:46,400 --> 00:04:49,850
notice here if I had a lot of categories

99
00:04:49,850 --> 00:04:53,090
of this data these labels would overlap

100
00:04:53,090 --> 00:04:55,280
each other so I&#39;m going to to add a

101
00:04:55,280 --> 00:04:57,979
theme so I&#39;m adding another attribute to

102
00:04:57,979 --> 00:05:00,050
this plot with another chaining operator

103
00:05:00,050 --> 00:05:03,080
and it&#39;s just that my text on the x-axis

104
00:05:03,080 --> 00:05:05,510
is going to be angled at 90 degrees

105
00:05:05,510 --> 00:05:07,400
ok

106
00:05:07,400 --> 00:05:11,030
and there you have it that plot and you

107
00:05:11,030 --> 00:05:13,340
can see my text is indeed angled at 90

108
00:05:13,340 --> 00:05:17,270
degrees you can see the counts and now

109
00:05:17,270 --> 00:05:19,820
it&#39;s ordered from the most frequent

110
00:05:19,820 --> 00:05:25,100
sedans which have maybe 90 or close to

111
00:05:25,100 --> 00:05:27,800
90 sedans then hatchback etc. so

112
00:05:27,800 --> 00:05:29,660
there&#39;s no ambiguity in a plot like this

113
00:05:29,660 --> 00:05:34,990
which are the most frequent categories

114
00:05:35,350 --> 00:05:38,830
we can also do a histogram so a

115
00:05:38,830 --> 00:05:42,640
histogram is like a bar plot but it

116
00:05:42,640 --> 00:05:47,470
spins a continuous variable the bar plot

117
00:05:47,470 --> 00:05:49,630
was for a categorical variable like a

118
00:05:49,630 --> 00:05:51,880
car is either a hatchback or sedan or

119
00:05:51,880 --> 00:05:54,250
something like that it but if we look at

120
00:05:54,250 --> 00:05:57,330
something like price of the car

121
00:05:57,330 --> 00:05:58,949
that&#39;s a numeric variable - it&#39;s

122
00:05:58,949 --> 00:06:01,439
continuous a car can have effectively

123
00:06:01,439 --> 00:06:05,669
any price probably greater than zero up

124
00:06:05,669 --> 00:06:07,409
to some extremely expensive cars of

125
00:06:07,409 --> 00:06:11,969
course nowadays so we bin by some

126
00:06:11,969 --> 00:06:15,780
the Min and Max of each bin and then

127
00:06:15,780 --> 00:06:17,849
display the counts in those bins so

128
00:06:17,849 --> 00:06:18,750
that&#39;s the difference between a

129
00:06:18,750 --> 00:06:22,620
histogram and a bar plot and here we

130
00:06:22,620 --> 00:06:25,199
have the histogram of price and we can

131
00:06:25,199 --> 00:06:28,169
right away learn some things about the

132
00:06:28,169 --> 00:06:30,840
prices of automobiles notice that the

133
00:06:30,840 --> 00:06:32,820
most frequent

134
00:06:32,820 --> 00:06:36,240
price is in this bin here which is

135
00:06:36,240 --> 00:06:39,050
probably around i&#39;m guessing about

136
00:06:39,050 --> 00:06:43,310
eight thousand dollars or so in fact

137
00:06:43,310 --> 00:06:45,620
that the distribution of cars is quite

138
00:06:45,620 --> 00:06:49,340
skewed we don&#39;t have any cars that are

139
00:06:49,340 --> 00:06:51,770
maybe less than a five thousand dollars

140
00:06:51,770 --> 00:06:56,090
and we have just a few cars out here

141
00:06:56,090 --> 00:06:58,669
maybe I think three cars it looks like

142
00:06:58,669 --> 00:07:01,610
that are over 40 thousand dollars and

143
00:07:01,610 --> 00:07:03,770
then we have this long tail of somewhat

144
00:07:03,770 --> 00:07:06,860
you know high priced car so most cars

145
00:07:06,860 --> 00:07:10,280
are on the low end of the price scale in

146
00:07:10,280 --> 00:07:12,430
this sample

147
00:07:12,430 --> 00:07:16,180
so another useful plot that data

148
00:07:16,180 --> 00:07:19,450
scientists use quite often is a box plot

149
00:07:19,450 --> 00:07:24,550
and so to go and look so it&#39;s another

150
00:07:24,550 --> 00:07:28,180
way to look at distributions of a

151
00:07:28,180 --> 00:07:32,470
univariate feature so we&#39;re going to

152
00:07:32,470 --> 00:07:33,970
have again

153
00:07:33,970 --> 00:07:37,270
auto.prices as our data frame price as our Y

154
00:07:37,270 --> 00:07:38,740
variable so it&#39;s going to be on the

155
00:07:38,740 --> 00:07:41,350
vertical axis I have to put something on

156
00:07:41,350 --> 00:07:43,150
the x-axis I&#39;m just putting a

157
00:07:43,150 --> 00:07:46,450
essentially a nova value a factor that

158
00:07:46,450 --> 00:07:49,690
just has a value 0 and then it&#39;s this

159
00:07:49,690 --> 00:07:53,230
geom_boxplot is the attribute

160
00:07:53,230 --> 00:07:56,280
to create a boxplot

161
00:07:56,680 --> 00:07:59,680
and here&#39;s my box plot of price so

162
00:07:59,680 --> 00:08:03,120
similar to the histogram

163
00:08:03,120 --> 00:08:06,360
it&#39;s showing the skewed distribution of

164
00:08:06,360 --> 00:08:08,220
the prices so how do you read this thing

165
00:08:08,220 --> 00:08:13,680
so this is the median price here which

166
00:08:13,680 --> 00:08:15,690
turns out to be just over ten thousand

167
00:08:15,690 --> 00:08:19,169
dollars is dark black line the lower

168
00:08:19,169 --> 00:08:21,840
quartile or the first lower quartile

169
00:08:21,840 --> 00:08:26,970
below the median is this part of the box

170
00:08:26,970 --> 00:08:30,030
the upper first quartile above the

171
00:08:30,030 --> 00:08:33,620
median is this box

172
00:08:33,690 --> 00:08:39,210
in the lower most quartile is shown by

173
00:08:39,210 --> 00:08:40,800
what we call the whisker this little

174
00:08:40,799 --> 00:08:45,840
lying down here but look at the above so

175
00:08:45,840 --> 00:08:48,120
the whisker

176
00:08:48,120 --> 00:08:50,940
can be extended by one-and-a-half up to

177
00:08:50,940 --> 00:08:53,100
one-and-a-half times this interquartile

178
00:08:53,100 --> 00:08:55,160
range okay

179
00:08:55,160 --> 00:08:58,519
but if we have data that are outliers

180
00:08:58,519 --> 00:09:01,100
effectively that they&#39;re beyond well

181
00:09:01,100 --> 00:09:03,560
this one and a half times the

182
00:09:03,560 --> 00:09:06,130
interquartile range from

183
00:09:06,130 --> 00:09:10,060
the first upper quartile then we have

184
00:09:10,060 --> 00:09:11,830
outliers so we just show those is some

185
00:09:11,830 --> 00:09:15,130
symbol in ggplot2 there they&#39;re showing

186
00:09:15,130 --> 00:09:18,010
as dots so these are very expensive cars

187
00:09:18,010 --> 00:09:20,649
that we already saw in the histogram

188
00:09:20,649 --> 00:09:26,050
and what we can do with a boxplot that&#39;s

189
00:09:26,050 --> 00:09:27,970
a cool thing and you can do this if you

190
00:09:27,970 --> 00:09:30,699
have a lot of different categories is to

191
00:09:30,699 --> 00:09:33,279
effectively a group by you create a set

192
00:09:33,279 --> 00:09:36,970
of boxplots group by some other

193
00:09:36,970 --> 00:09:39,220
continue categorical variable in the

194
00:09:39,220 --> 00:09:41,769
data set so in this case I&#39;m going to

195
00:09:41,769 --> 00:09:47,259
make my X a factor of fuel type and

196
00:09:47,259 --> 00:09:49,269
again Y is price and I&#39;ve got geom_boxplot

197
00:09:49,269 --> 00:09:51,129
and I&#39;m doing something else

198
00:09:51,129 --> 00:09:52,660
here I&#39;m adding some more attributes

199
00:09:52,660 --> 00:09:54,670
with this chaining operator i&#39;m adding

200
00:09:54,670 --> 00:09:59,379
an X label for fuel type and an overall

201
00:09:59,379 --> 00:10:02,350
title price by fuel type so that&#39;s how

202
00:10:02,350 --> 00:10:04,329
you keep building up your plot to get

203
00:10:04,329 --> 00:10:07,420
more a more meaningful more useful plot

204
00:10:07,420 --> 00:10:09,790
and also a better plot to present and

205
00:10:09,790 --> 00:10:13,089
share with your colleagues notice on

206
00:10:13,089 --> 00:10:16,000
this plot here the x-axis is marked as

207
00:10:16,000 --> 00:10:18,129
factor 0 well someone who doesn&#39;t know a

208
00:10:18,129 --> 00:10:19,899
lot about ggplot2 is going to look at

209
00:10:19,899 --> 00:10:20,740
that go

210
00:10:20,740 --> 00:10:22,509
what do you mean by

211
00:10:22,509 --> 00:10:24,730
that so let&#39;s do this

212
00:10:24,730 --> 00:10:30,009
ok so now the big picture item here is

213
00:10:30,009 --> 00:10:33,040
there to fuel types it turns out

214
00:10:33,040 --> 00:10:35,259
autos can have either diesel engines or

215
00:10:35,259 --> 00:10:38,560
gasoline engines and you can see the

216
00:10:38,560 --> 00:10:41,920
median for diesel engine cars is a bit

217
00:10:41,920 --> 00:10:44,259
more than the median for gasoline engine

218
00:10:44,259 --> 00:10:47,170
cars also the distribution is a little

219
00:10:47,170 --> 00:10:50,769
less you&#39;d you see the median is those

220
00:10:50,769 --> 00:10:53,199
boxes are a little closer to the same

221
00:10:53,199 --> 00:10:56,290
with the upper the first upper quartile

222
00:10:56,290 --> 00:10:58,809
in the first lower quartile whereas as

223
00:10:58,809 --> 00:11:00,910
we look before these are quite skewed

224
00:11:00,910 --> 00:11:03,910
and the outliers interestingly so all

225
00:11:03,910 --> 00:11:06,759
are very expensive cars except maybe one

226
00:11:06,759 --> 00:11:10,120
here are all gasoline cars so that&#39;s

227
00:11:10,120 --> 00:11:13,089
kind of interesting and notice because I

228
00:11:13,089 --> 00:11:16,240
added these extra plot attributes i have

229
00:11:16,240 --> 00:11:18,699
a nice label on my overall plot or title

230
00:11:18,699 --> 00:11:22,329
price by fuel type and I have fuel type

231
00:11:22,329 --> 00:11:25,709
labeled as my x-axis

232
00:11:25,920 --> 00:11:29,670
so how else can we look at a single

233
00:11:29,670 --> 00:11:33,779
variable well similar to a histogram is

234
00:11:33,779 --> 00:11:35,970
a kernel density estimation plot

235
00:11:35,970 --> 00:11:40,589
basically we compute a nonlinear curve

236
00:11:40,589 --> 00:11:45,540
that fills in a smooth version of the

237
00:11:45,540 --> 00:11:49,790
histogram to let me run that for you

238
00:11:50,120 --> 00:11:52,790
and notice that the syntax is exactly

239
00:11:52,790 --> 00:11:54,380
the same as if i was doing a histogram

240
00:11:54,380 --> 00:11:57,770
except it&#39;s geom_density and so

241
00:11:57,770 --> 00:12:01,649
this gives us very similar

242
00:12:01,649 --> 00:12:03,869
view to what we had with the histogram

243
00:12:03,869 --> 00:12:05,399
we have price on the horizontal axis

244
00:12:05,399 --> 00:12:09,029
density notice the densities in these

245
00:12:09,029 --> 00:12:12,209
small units because density if you did

246
00:12:12,209 --> 00:12:15,660
the integral over this density it has to

247
00:12:15,660 --> 00:12:18,300
add up to 1 so that&#39;s just a

248
00:12:18,300 --> 00:12:21,329
normalization convention but again we

249
00:12:21,329 --> 00:12:24,779
notice that the mode the most frequent

250
00:12:24,779 --> 00:12:27,449
price of cars seems to be you know like

251
00:12:27,449 --> 00:12:29,519
under maybe around eight thousand

252
00:12:29,519 --> 00:12:30,990
dollars and then they just cut off

253
00:12:30,990 --> 00:12:33,540
there&#39;s no cars below about five

254
00:12:33,540 --> 00:12:36,660
thousand dollars and we have this long

255
00:12:36,660 --> 00:12:39,749
tail out here with those few very

256
00:12:39,749 --> 00:12:41,399
expensive cars and you can see there&#39;s a

257
00:12:41,399 --> 00:12:43,829
little bump of them for some reason out

258
00:12:43,829 --> 00:12:45,270
here

259
00:12:45,270 --> 00:12:46,860
but you can do something else you can

260
00:12:46,860 --> 00:12:50,790
also add attributes to ggplot.plot so

261
00:12:50,790 --> 00:12:53,670
all I&#39;m doing is I&#39;m taking this same

262
00:12:53,670 --> 00:12:55,830
line of code here and i&#39;m adding this

263
00:12:55,830 --> 00:12:59,160
adjust = 1/5 so my smoothing

264
00:12:59,160 --> 00:13:04,710
operator is now one fifth the x range instead of I

265
00:13:04,710 --> 00:13:08,000
think the default is one-half

266
00:13:08,010 --> 00:13:10,200
and what do I get well I get a much more

267
00:13:10,200 --> 00:13:13,170
bumpy plot because i&#39;m using a much

268
00:13:13,170 --> 00:13:16,470
shorter span of my smoother

269
00:13:16,470 --> 00:13:18,870
and the question is you know what tells

270
00:13:18,870 --> 00:13:22,560
you more you want this smooth version or

271
00:13:22,560 --> 00:13:24,339
do you want this

272
00:13:24,339 --> 00:13:26,350
bumpy version that obviously has more

273
00:13:26,350 --> 00:13:27,639
detail

274
00:13:27,639 --> 00:13:32,270
in this particular case i would say

275
00:13:32,270 --> 00:13:34,460
I like the smooth version because i

276
00:13:34,460 --> 00:13:37,190
think this bumpy version is basically

277
00:13:37,190 --> 00:13:40,850
just showing me noise an individual cars

278
00:13:40,850 --> 00:13:43,370
show up as little bumps here and that&#39;s

279
00:13:43,370 --> 00:13:45,230
not the idea of getting an overall

280
00:13:45,230 --> 00:13:50,120
notion of the distribution of car prices

281
00:13:50,120 --> 00:13:52,760
is just telling you about some

282
00:13:52,760 --> 00:13:54,980
noise in your particular sample when you

283
00:13:54,980 --> 00:13:56,720
use such a short smoothing operator but

284
00:13:56,720 --> 00:13:58,850
you can pick smoothing operators

285
00:13:58,850 --> 00:14:01,070
you know between these two extremes to

286
00:14:01,070 --> 00:14:03,290
and see how that goes

287
00:14:03,290 --> 00:14:05,240
so we have one last plot i&#39;d like to

288
00:14:05,240 --> 00:14:06,950
show you are going to look at violin

289
00:14:06,950 --> 00:14:12,170
plots. Violin plots are a new sort

290
00:14:12,170 --> 00:14:14,510
of plot and they take some of the

291
00:14:14,510 --> 00:14:18,320
properties of a box plot and of a kernel

292
00:14:18,320 --> 00:14:20,210
density estimation plot both of which

293
00:14:20,210 --> 00:14:23,450
we&#39;ve just looked at and so in this

294
00:14:23,450 --> 00:14:26,210
particular case again I&#39;m going to from

295
00:14:26,210 --> 00:14:29,360
the auto price data frame i&#39;m going to

296
00:14:29,360 --> 00:14:32,300
separate my data by the fuel type diesel

297
00:14:32,300 --> 00:14:33,890
and gasoline we&#39;re going to look at

298
00:14:33,890 --> 00:14:37,370
price i&#39;m going to use this

299
00:14:37,370 --> 00:14:39,779
geom_violin

300
00:14:39,779 --> 00:14:42,300
and the other attributes remain the same

301
00:14:42,300 --> 00:14:47,069
that I&#39;ve my label for the x-axis and my

302
00:14:47,069 --> 00:14:47,879
title

303
00:14:47,879 --> 00:14:51,329
ok so let me run this

304
00:14:51,329 --> 00:14:54,179
and now you see some violin plots and

305
00:14:54,179 --> 00:14:56,759
you can see they are indeed segregated

306
00:14:56,759 --> 00:14:58,829
the data segregated or more stratified

307
00:14:58,829 --> 00:15:03,360
by diesel and gasoline cars and you can

308
00:15:03,360 --> 00:15:06,239
see what is going on here this is like a

309
00:15:06,239 --> 00:15:09,779
kernel density estimation plot

310
00:15:09,779 --> 00:15:12,740
and it&#39;s a mirror image

311
00:15:13,100 --> 00:15:16,540
each type of car

312
00:15:16,540 --> 00:15:18,580
and when we look at this we can compare

313
00:15:18,580 --> 00:15:21,580
gasoline cars to diesel cars we can see

314
00:15:21,580 --> 00:15:23,440
their price the density of their price

315
00:15:23,440 --> 00:15:26,860
so what we see is that gasoline cars are

316
00:15:26,860 --> 00:15:31,270
indeed bunched up around this fairly low

317
00:15:31,270 --> 00:15:34,810
price but also have this long

318
00:15:34,810 --> 00:15:37,600
tail of quite expensive cars there&#39;s

319
00:15:37,600 --> 00:15:39,160
diesel cars are in a narrower range

320
00:15:39,160 --> 00:15:43,150
they&#39;re a little bit bunched toward the the

321
00:15:43,150 --> 00:15:45,730
low end of the price but not as much as

322
00:15:45,730 --> 00:15:49,660
gasoline cars and they have some higher

323
00:15:49,660 --> 00:15:53,110
priced cars but again not this long tail

324
00:15:53,110 --> 00:15:55,660
like we have for the gasoline cars so by

325
00:15:55,660 --> 00:15:57,790
doing this violin plot we actually can

326
00:15:57,790 --> 00:16:00,040
show quite a bit of information about

327
00:16:00,040 --> 00:16:03,340
the price of cars versus the fuel types

328
00:16:03,340 --> 00:16:05,650
of their engines and you can imagine if

329
00:16:05,650 --> 00:16:08,560
we had many more categories in the

330
00:16:08,560 --> 00:16:12,400
feature that we are trying to group the

331
00:16:12,400 --> 00:16:15,100
data by you could lay these all side by

332
00:16:15,100 --> 00:16:17,530
side by side here and you&#39;d learn quite

333
00:16:17,530 --> 00:16:22,590
a lot about those relationships

334
00:16:22,780 --> 00:16:25,780
so that I hope gives you an overview of

335
00:16:25,780 --> 00:16:28,930
first off how to make some really nice

336
00:16:28,930 --> 00:16:31,690
plots using our and ggplot2 but also

337
00:16:31,690 --> 00:16:35,380
what do we a data scientists do with

338
00:16:35,380 --> 00:16:36,850
those plots to understand the

339
00:16:36,850 --> 00:16:40,110
relationships in our data

