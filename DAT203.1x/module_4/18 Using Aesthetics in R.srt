0
00:00:00,000 --> 00:00:03,179
In the previous sequences we looked at

1
00:00:03,179 --> 00:00:05,400
one-dimensional plots and

2
00:00:05,400 --> 00:00:09,599
two-dimensional plots but this is a

3
00:00:09,599 --> 00:00:12,809
fundamental problem in visualization of

4
00:00:12,809 --> 00:00:15,719
data that we&#39;re always looking at a

5
00:00:15,719 --> 00:00:17,520
two-dimensional surface where we make

6
00:00:17,520 --> 00:00:19,650
our plot it&#39;s a generally our computer

7
00:00:19,650 --> 00:00:21,060
screen it could be on a piece of paper

8
00:00:21,060 --> 00:00:24,390
but we&#39;re stuck with two dimensions and

9
00:00:24,390 --> 00:00:27,000
so question is how do you how do you see

10
00:00:27,000 --> 00:00:29,580
more dimensions in the data set when

11
00:00:29,580 --> 00:00:31,230
you&#39;re stuck with a two-dimensional

12
00:00:31,230 --> 00:00:34,469
projection and one answer is to use what

13
00:00:34,469 --> 00:00:37,079
we call aesthetics and aesthetics are

14
00:00:37,079 --> 00:00:41,399
things like color transparency shape etc

15
00:00:41,399 --> 00:00:45,840
and so let&#39;s explore how we can add new

16
00:00:45,840 --> 00:00:47,760
dimensions a new understanding of the

17
00:00:47,760 --> 00:00:49,620
relationships in our data using it

18
00:00:49,620 --> 00:00:56,740
aesthetics so i have on my screen here

19
00:00:56,740 --> 00:00:59,140
plot that&#39;s very similar to what we

20
00:00:59,140 --> 00:01:02,710
already looked at so we&#39;re going

21
00:01:02,710 --> 00:01:08,530
to use ggplot again and the auto price

22
00:01:08,530 --> 00:01:10,510
data and the city miles per gallon vs

23
00:01:10,510 --> 00:01:16,000
price but then in my geom_point add another

24
00:01:16,000 --> 00:01:19,900
aes function and its color equals

25
00:01:19,900 --> 00:01:22,680
then fuel type where I make

26
00:01:22,680 --> 00:01:24,750
a factor out of fuel types of those two

27
00:01:24,750 --> 00:01:27,900
levels of fuel type recall gasoline and

28
00:01:27,900 --> 00:01:31,440
diesel and then my plot labels and title

29
00:01:31,440 --> 00:01:35,420
alright let me run that

30
00:01:36,120 --> 00:01:39,900
and so what you can see is gasoline cars

31
00:01:39,900 --> 00:01:43,380
are showing in this kind of blue and

32
00:01:43,380 --> 00:01:46,140
diesel cars or blue-green and diesel

33
00:01:46,140 --> 00:01:48,810
cars are shown in red and now certain

34
00:01:48,810 --> 00:01:50,610
things make more sense

35
00:01:50,610 --> 00:01:53,370
remember we had these cars that seem to

36
00:01:53,370 --> 00:01:56,790
have different behavior from the rest

37
00:01:56,790 --> 00:01:59,580
that were lying up above the general

38
00:01:59,580 --> 00:02:01,590
trend and it turns out they&#39;re almost

39
00:02:01,590 --> 00:02:04,440
all diesel cars so we can conclude

40
00:02:04,440 --> 00:02:06,690
actually just by the simple plot that

41
00:02:06,690 --> 00:02:10,020
for a given level of fuel efficiency

42
00:02:10,020 --> 00:02:13,440
performance a diesel car costs us more

43
00:02:13,440 --> 00:02:16,200
than a gasoline car

44
00:02:16,200 --> 00:02:18,660
let me just make a general comment here

45
00:02:18,660 --> 00:02:21,660
that when you&#39;re using colors aesthetic

46
00:02:21,660 --> 00:02:23,280
notice there&#39;s only two colors on this

47
00:02:23,280 --> 00:02:24,269
plot

48
00:02:24,269 --> 00:02:27,840
but if you had a factor with a great

49
00:02:27,840 --> 00:02:30,300
many levels you could wind up with a

50
00:02:30,300 --> 00:02:32,880
dozen colors on a plot in its becomes

51
00:02:32,880 --> 00:02:34,920
very very difficult for people to see

52
00:02:34,920 --> 00:02:36,630
the relationships you&#39;re going to have

53
00:02:36,630 --> 00:02:38,280
that problem but your audience is really

54
00:02:38,280 --> 00:02:39,599
gonna have that problem so don&#39;t overdo

55
00:02:39,599 --> 00:02:39,959
it

56
00:02:39,959 --> 00:02:43,890
make sure you limit how many colors you

57
00:02:43,890 --> 00:02:47,580
use on a plot we also recall had this

58
00:02:47,580 --> 00:02:50,910
issue of over plotting so you know

59
00:02:50,910 --> 00:02:53,610
there&#39;s a lot of dots here and a lot of

60
00:02:53,610 --> 00:02:57,510
dots their arm we use that 2d kernel

61
00:02:57,510 --> 00:02:59,849
density estimation plot to contour that

62
00:02:59,849 --> 00:03:02,549
gave us some idea of the density there&#39;s

63
00:03:02,549 --> 00:03:04,440
another way we can do this with point

64
00:03:04,440 --> 00:03:09,180
transparency so all this code is the

65
00:03:09,180 --> 00:03:13,440
same except they added one more argument

66
00:03:13,440 --> 00:03:16,500
to geom_point which is this

67
00:03:16,500 --> 00:03:19,140
alpha equals 0.3 so alpha is a

68
00:03:19,140 --> 00:03:22,829
transparency coefficient alpha equals

69
00:03:22,829 --> 00:03:24,870
one is perfectly opaque that&#39;s the

70
00:03:24,870 --> 00:03:26,280
default that&#39;s what we&#39;re seeing here

71
00:03:26,280 --> 00:03:29,489
alpha equals 0 is perfectly transparent

72
00:03:29,489 --> 00:03:32,100
we wouldn&#39;t even see the dot but with

73
00:03:32,100 --> 00:03:34,950
.3 it&#39;s about thirty percent so

74
00:03:34,950 --> 00:03:38,010
about 70-percent show through and let me

75
00:03:38,010 --> 00:03:39,720
run that for you and you&#39;ll see what

76
00:03:39,720 --> 00:03:43,489
that does for us

77
00:03:44,050 --> 00:03:46,840
so you can see places where we just have

78
00:03:46,840 --> 00:03:50,470
a single car which is quite a bit out

79
00:03:50,470 --> 00:03:53,770
here we just have this kind of you can

80
00:03:53,770 --> 00:03:56,350
still see the dot ok but it&#39;s it&#39;s

81
00:03:56,350 --> 00:03:58,500
fairly

82
00:03:58,500 --> 00:04:01,650
you can see through it it&#39;s not very

83
00:04:01,650 --> 00:04:04,260
distinct and it looks like maybe we have

84
00:04:04,260 --> 00:04:06,300
two cars lying here because notice how

85
00:04:06,300 --> 00:04:09,840
much denser that are intense that color

86
00:04:09,840 --> 00:04:12,540
red is then up here but we get down to

87
00:04:12,540 --> 00:04:15,150
these high density points we really get

88
00:04:15,150 --> 00:04:17,370
a better feel for that there&#39;s really

89
00:04:17,370 --> 00:04:19,620
are a lot of points lying on top of each

90
00:04:19,620 --> 00:04:23,220
other because of this see-through we saw

91
00:04:23,220 --> 00:04:24,900
that of course with the 2D plot but

92
00:04:24,900 --> 00:04:27,030
this is a 2d kernel density estimation

93
00:04:27,030 --> 00:04:30,030
plot sorry but this is another way to

94
00:04:30,030 --> 00:04:32,760
show that without cluttering up the plot

95
00:04:32,760 --> 00:04:35,160
with a lot of contour lines which may or

96
00:04:35,160 --> 00:04:38,770
may not help you

97
00:04:38,770 --> 00:04:42,250
so another thing we can do is marker

98
00:04:42,250 --> 00:04:45,430
size so up till now we&#39;ve been just

99
00:04:45,430 --> 00:04:48,159
using a point a dot and we&#39;ve just been

100
00:04:48,159 --> 00:04:50,889
accepting the default size dot that

101
00:04:50,889 --> 00:04:55,479
ggplot2 gives us but we can control that

102
00:04:55,479 --> 00:04:58,509
and we can do it in a certain way here

103
00:04:58,509 --> 00:05:02,199
where it says size equals engine.size so

104
00:05:02,199 --> 00:05:06,449
engine.size is another variable in this

105
00:05:06,449 --> 00:05:09,229
a data frame

106
00:05:09,639 --> 00:05:11,650
and we&#39;re going to keep our color scheme

107
00:05:11,650 --> 00:05:14,139
where we&#39;re going to have different

108
00:05:14,139 --> 00:05:16,360
colors for diesel and gasoline cars

109
00:05:16,360 --> 00:05:18,400
we&#39;re going to keep our transparency but

110
00:05:18,400 --> 00:05:22,889
we&#39;re going to make the marker size

111
00:05:23,800 --> 00:05:27,930
proportional to the engine size

112
00:05:28,550 --> 00:05:31,850
and we get a plot like this and see some

113
00:05:31,850 --> 00:05:34,400
of these very very fuel-efficient cars

114
00:05:34,400 --> 00:05:38,660
have pretty tiny engines are mid-range

115
00:05:38,660 --> 00:05:41,750
cars seem to have middle-sized engines

116
00:05:41,750 --> 00:05:44,360
and then these very expensive low fuel

117
00:05:44,360 --> 00:05:46,790
efficiency cars notice there&#39;s some some

118
00:05:46,790 --> 00:05:48,350
of these have actually the biggest

119
00:05:48,350 --> 00:05:52,640
engines are the biggest dots there&#39;s

120
00:05:52,640 --> 00:05:55,160
kind of a problem with this display

121
00:05:55,160 --> 00:05:57,560
which is we&#39;re going to the diameter

122
00:05:57,560 --> 00:06:01,760
radius of these points is equal to the

123
00:06:01,760 --> 00:06:05,540
size of the engine in our human

124
00:06:05,540 --> 00:06:08,450
perception were more sensitive to area

125
00:06:08,450 --> 00:06:10,670
than size so i can make that an area

126
00:06:10,670 --> 00:06:14,720
plot so I create a new column engine

127
00:06:14,720 --> 00:06:16,960
size too

128
00:06:16,960 --> 00:06:20,259
and it&#39;s just the square of engine size

129
00:06:20,259 --> 00:06:23,740
so now you see my size and size equals

130
00:06:23,740 --> 00:06:26,139
engine size 2 which is engine size

131
00:06:26,139 --> 00:06:30,120
squared so everything else is the same

132
00:06:32,670 --> 00:06:34,890
and you get a little bit you know bigger

133
00:06:34,890 --> 00:06:40,740
change its more proportionate

134
00:06:40,740 --> 00:06:43,349
or the areas now proportionate to the

135
00:06:43,349 --> 00:06:45,960
size of the engine not the diameter

136
00:06:45,960 --> 00:06:47,880
radius of the point so we get a little

137
00:06:47,880 --> 00:06:50,130
bit you can see there&#39;s some smaller

138
00:06:50,130 --> 00:06:52,500
some really small engines down here in

139
00:06:52,500 --> 00:06:55,440
these cheap cars it&#39;s it&#39;s more apparent

140
00:06:55,440 --> 00:06:59,490
and the really big expensive cars

141
00:06:59,490 --> 00:07:01,889
apparently have these really big engines

142
00:07:01,889 --> 00:07:05,240
which is not too surprising

143
00:07:07,150 --> 00:07:09,550
so we have some other aesthetics we can

144
00:07:09,550 --> 00:07:11,860
try there&#39;s marker shape so up till now

145
00:07:11,860 --> 00:07:14,290
we&#39;ve just been using around dot which

146
00:07:14,290 --> 00:07:17,590
is the default in ggplot2 but we could

147
00:07:17,590 --> 00:07:20,020
use the marker shape to show something

148
00:07:20,020 --> 00:07:23,050
in this case again all this code is the

149
00:07:23,050 --> 00:07:24,970
same as what you just saw

150
00:07:24,970 --> 00:07:27,580
but i&#39;ve added this after size this one

151
00:07:27,580 --> 00:07:32,140
new attribute which is the factor of

152
00:07:32,140 --> 00:07:33,610
aspiration there&#39;s two types of

153
00:07:33,610 --> 00:07:35,290
aspiration here there&#39;s either turbo

154
00:07:35,290 --> 00:07:38,170
engines or standard engines like that

155
00:07:38,170 --> 00:07:41,170
which I assume are fuel injected engines so

156
00:07:41,170 --> 00:07:45,240
let me run this plot for you

157
00:07:47,800 --> 00:07:53,200
and so now we see triangles

158
00:07:53,200 --> 00:07:56,810
so again and diesel are red, gas are

159
00:07:56,810 --> 00:07:59,930
so this Blue/green, engine size now we

160
00:07:59,930 --> 00:08:02,690
have standard aspiration cars are a dot

161
00:08:02,690 --> 00:08:05,900
turbo cars are this triangle and you see

162
00:08:05,900 --> 00:08:10,330
a lot of our diesel cars are

163
00:08:10,330 --> 00:08:12,970
turbo cars maybe that you know that

164
00:08:12,970 --> 00:08:14,680
could account for why they seem to be

165
00:08:14,680 --> 00:08:19,060
more expensive for a given fuel economy

166
00:08:19,060 --> 00:08:21,639
than these gasoline cars - there is a few turbo

167
00:08:21,639 --> 00:08:25,509
gasoline cars scattered in here you see

168
00:08:25,509 --> 00:08:28,569
a few triangles but in these very highly

169
00:08:28,569 --> 00:08:30,639
fuel-efficient cars notice there&#39;s no

170
00:08:30,639 --> 00:08:32,790
turbo

171
00:08:32,789 --> 00:08:36,060
cars like the highest fuel efficiency

172
00:08:36,059 --> 00:08:38,730
turbo is this one diesel here with a

173
00:08:38,730 --> 00:08:40,530
pretty small engine notice the marker

174
00:08:40,530 --> 00:08:44,070
size is really small so we&#39;ve learned a

175
00:08:44,070 --> 00:08:46,920
lot and and look at how many dimensions

176
00:08:46,920 --> 00:08:48,540
are already on this plot so we have

177
00:08:48,540 --> 00:08:50,580
price and miles per gallon that&#39;s too

178
00:08:50,580 --> 00:08:55,920
and we have fuel type 3 engine side for

179
00:08:55,920 --> 00:08:58,830
and aspiration five so we&#39;ve got a five

180
00:08:58,830 --> 00:09:02,160
dimensions of data that we&#39;re projecting

181
00:09:02,160 --> 00:09:05,970
onto a two-dimensional surface here just

182
00:09:05,970 --> 00:09:08,780
by using these

183
00:09:09,230 --> 00:09:13,190
aesthetics we also have a 6th not quite

184
00:09:13,190 --> 00:09:16,310
dimension but we get this idea of

185
00:09:16,310 --> 00:09:18,770
density of how many cars are clustered

186
00:09:18,770 --> 00:09:20,240
in some of these areas with the

187
00:09:20,240 --> 00:09:23,570
transparency

188
00:09:23,570 --> 00:09:29,090
so that&#39;s an overview of some plot

189
00:09:29,090 --> 00:09:31,640
aesthetics that allow us to project more

190
00:09:31,640 --> 00:09:33,680
dimensions on to our two-dimensional

191
00:09:33,680 --> 00:09:36,290
plot surface there&#39;s also a lot of plot

192
00:09:36,290 --> 00:09:38,120
specific aesthetics we&#39;ve looked at a

193
00:09:38,120 --> 00:09:41,060
couple we for the violin plot and the

194
00:09:41,060 --> 00:09:45,410
boxplot we added an X variable which was

195
00:09:45,410 --> 00:09:47,720
in that case fuel type but it could be

196
00:09:47,720 --> 00:09:50,930
any categorical variable so that&#39;s a

197
00:09:50,930 --> 00:09:53,360
specific aesthetic to box and violin

198
00:09:53,360 --> 00:09:58,190
plots we looked at changing the span of

199
00:09:58,190 --> 00:10:01,400
the kernel density estimator for our

200
00:10:01,400 --> 00:10:03,350
kernel density estimation plot that&#39;s a

201
00:10:03,350 --> 00:10:05,630
different aesthetic and let me just show

202
00:10:05,630 --> 00:10:09,710
you one other example here and that is

203
00:10:09,710 --> 00:10:13,550
histogram binning. So up till now we&#39;ve

204
00:10:13,550 --> 00:10:16,040
been accepting the default histogram

205
00:10:16,040 --> 00:10:21,800
binning but we can change that and so

206
00:10:21,800 --> 00:10:24,230
I&#39;m going to use this grid extra package

207
00:10:24,230 --> 00:10:26,600
to display two plots side-by-side i&#39;ll

208
00:10:26,600 --> 00:10:27,890
show you how that works

209
00:10:27,890 --> 00:10:30,470
but i compute two different bin

210
00:10:30,470 --> 00:10:34,340
widths which is just the max of the auto

211
00:10:34,340 --> 00:10:36,410
price minus the min of the auto price /

212
00:10:36,410 --> 00:10:41,000
either 20 or 50, you I&#39;m gonna have, you know 20 bins or 50

213
00:10:41,000 --> 00:10:46,760
bins and i use this grid extra package

214
00:10:46,760 --> 00:10:49,490
and I plot i create the two histograms

215
00:10:49,490 --> 00:10:52,970
with binwidth in geom_histogram

216
00:10:52,970 --> 00:10:54,740
I&#39;ve set been with either have

217
00:10:54,740 --> 00:10:57,260
bin width one or bin width two everything

218
00:10:57,260 --> 00:11:00,440
else is the same here but I don&#39;t plot

219
00:11:00,440 --> 00:11:04,640
those i assign those plot objects to two

220
00:11:04,640 --> 00:11:06,950
variables p1 p2 and then i use grid or

221
00:11:06,950 --> 00:11:08,660
.arrrange which is from that gridExtra

222
00:11:08,660 --> 00:11:12,350
package to put p1 p2 next to each

223
00:11:12,350 --> 00:11:14,720
other because I&#39;m saying,  or one

224
00:11:14,720 --> 00:11:16,790
above another, I&#39;m saying nrow equals

225
00:11:16,790 --> 00:11:21,280
2, so I&#39;m going to have two rows of plots

226
00:11:22,070 --> 00:11:24,950
and there you have it so my 20 bin

227
00:11:24,950 --> 00:11:28,070
histogram it&#39;s a little coarser I think

228
00:11:28,070 --> 00:11:30,110
the default we looked at before was 30

229
00:11:30,110 --> 00:11:32,180
it&#39;s a little coarser than that default

230
00:11:32,180 --> 00:11:35,480
but gives us you know the same basic

231
00:11:35,480 --> 00:11:38,990
idea of some very expensive cars the

232
00:11:38,990 --> 00:11:42,350
bulk of our cars tending toward this low

233
00:11:42,350 --> 00:11:46,070
price end of our range here if I go with

234
00:11:46,070 --> 00:11:49,310
60 bins again it&#39;s similar to what

235
00:11:49,310 --> 00:11:52,370
happened when i made the span of the

236
00:11:52,370 --> 00:11:54,290
kernel density estimation plot shorter

237
00:11:54,290 --> 00:11:56,960
it gets very bumpy you&#39;re seeing each

238
00:11:56,960 --> 00:11:59,860
individual car here

239
00:11:59,860 --> 00:12:03,100
Falls you know very narrow ranges of

240
00:12:03,100 --> 00:12:05,649
these bins so the question is again

241
00:12:05,649 --> 00:12:07,990
which do you prefer do you prefer

242
00:12:07,990 --> 00:12:11,380
something more aggregated smoother like

243
00:12:11,380 --> 00:12:14,079
this with 20 mins or something like this

244
00:12:14,079 --> 00:12:17,110
60 bins that shows a lot of detail in my

245
00:12:17,110 --> 00:12:21,400
it to my view right now the 60 bins

246
00:12:21,400 --> 00:12:23,770
it&#39;s just showing some noise and it&#39;s

247
00:12:23,770 --> 00:12:27,010
just the random chance of where the

248
00:12:27,010 --> 00:12:29,740
prices of the cars, but

249
00:12:29,740 --> 00:12:33,760
20 could be too smooth so maybe

250
00:12:33,760 --> 00:12:36,220
the right answer might be 30 or 40 you

251
00:12:36,220 --> 00:12:37,930
can experiment with these things and see

252
00:12:37,930 --> 00:12:41,200
what shows you what you want to know

253
00:12:41,200 --> 00:12:44,770
so in this sequence then we&#39;ve looked at

254
00:12:44,770 --> 00:12:47,860
some powerful methods to use plot

255
00:12:47,860 --> 00:12:51,130
aesthetics to break out of the problem

256
00:12:51,130 --> 00:12:53,620
of just having a two-dimensional surface

257
00:12:53,620 --> 00:12:56,140
to look to visualize the projection of

258
00:12:56,140 --> 00:12:58,810
our data on and we&#39;ve gotten up to

259
00:12:58,810 --> 00:13:01,510
five dimensions just using the simple

260
00:13:01,510 --> 00:13:04,440
plot aesthetics

