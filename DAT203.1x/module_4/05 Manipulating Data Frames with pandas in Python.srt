0
00:00:00,000 --> 00:00:03,689
So in the previous demo i introduced some

1
00:00:03,689 --> 00:00:06,210
basic properties of how you can work

2
00:00:06,210 --> 00:00:09,059
with pandas data frames in this video

3
00:00:09,059 --> 00:00:11,099
I&#39;m going to extend that and we&#39;re going

4
00:00:11,099 --> 00:00:13,980
to look at how you can do some other

5
00:00:13,980 --> 00:00:17,910
manipulations of pandas data frames so

6
00:00:17,910 --> 00:00:21,150
in the notebook of my screen here I have

7
00:00:21,150 --> 00:00:23,400
some code we&#39;ll run to do

8
00:00:23,400 --> 00:00:25,710
various subsets of the data frame so

9
00:00:25,710 --> 00:00:26,820
we&#39;re in this case we&#39;re going to take

10
00:00:26,820 --> 00:00:30,300
slices of the rows in the data frame and

11
00:00:30,300 --> 00:00:34,170
that the syntax for taking a slice and

12
00:00:34,170 --> 00:00:36,570
the same syntax actually works for

13
00:00:36,570 --> 00:00:39,840
columns as well as with the data frame

14
00:00:39,840 --> 00:00:42,210
we have the square bracket operator we

15
00:00:42,210 --> 00:00:44,920
have the start

16
00:00:44,920 --> 00:00:47,800
remember zero-based then a colon in the

17
00:00:47,800 --> 00:00:51,250
end another colon in the stride but

18
00:00:51,250 --> 00:00:52,930
there&#39;s some defaults this the default

19
00:00:52,930 --> 00:00:56,920
for start is 0 the default for

20
00:00:56,920 --> 00:00:59,559
end is the number of rows and the

21
00:00:59,559 --> 00:01:02,049
default for sorry the number of rows

22
00:01:02,049 --> 00:01:04,059
minus 1 remember because it&#39;s zero-based

23
00:01:04,059 --> 00:01:08,289
and the stride is one you can change any

24
00:01:08,289 --> 00:01:10,899
of those so for example if I rely

25
00:01:10,899 --> 00:01:16,869
on those defaults for start and stride

26
00:01:16,869 --> 00:01:20,289
I can just get the first five rows of my

27
00:01:20,289 --> 00:01:22,960
auto_prices data frame this way just

28
00:01:22,960 --> 00:01:25,330
colon 5

29
00:01:25,330 --> 00:01:26,800
and there they are and you can see the

30
00:01:26,800 --> 00:01:31,420
index 0 1 2 3 4 and you see all I got

31
00:01:31,420 --> 00:01:32,740
all my columns because they didn&#39;t

32
00:01:32,740 --> 00:01:34,750
subset any columns i just took the first

33
00:01:34,750 --> 00:01:36,760
five rows

34
00:01:36,760 --> 00:01:42,640
ok I can also do an arbitrary slice like

35
00:01:42,640 --> 00:01:44,460
this

36
00:01:44,460 --> 00:01:48,530
so I&#39;m going to get five

37
00:01:48,530 --> 00:01:52,430
rows here between 9 and 14 which are

38
00:01:52,430 --> 00:01:57,630
the 10th row to the 15th row

39
00:01:57,630 --> 00:01:59,700
with the zero-based indexing so you can

40
00:01:59,700 --> 00:02:05,659
see get 10 11 12 13 14 are my indices

41
00:02:05,950 --> 00:02:08,610
So notice how this works so

42
00:02:08,610 --> 00:02:10,850
I get

43
00:02:10,860 --> 00:02:13,650
10, you know i started with 9 as my

44
00:02:13,650 --> 00:02:17,580
start but I actually see 10 11 12 13 14

45
00:02:17,580 --> 00:02:20,550
so you have to be cognoscente of that how

46
00:02:20,550 --> 00:02:24,750
the indices are used to count this span

47
00:02:24,750 --> 00:02:26,790
from start to end when you take slices

48
00:02:26,790 --> 00:02:28,740
of a pandas dataframe it can be a little

49
00:02:28,740 --> 00:02:30,060
confusing you&#39;re gonna have to

50
00:02:30,060 --> 00:02:31,890
experiment with it

51
00:02:31,890 --> 00:02:34,260
there&#39;s one other thing i want to show

52
00:02:34,260 --> 00:02:38,670
you about slices just in this part and

53
00:02:38,670 --> 00:02:41,190
that&#39;s there&#39;s also this iloc method

54
00:02:41,190 --> 00:02:44,640
which means index-based location that&#39;s

55
00:02:44,640 --> 00:02:47,790
what iloc is an abbreviation for and I

56
00:02:47,790 --> 00:02:51,270
can use the same indus index ranges that

57
00:02:51,270 --> 00:02:58,900
I used hear of 9 to 14 to do this

58
00:02:58,900 --> 00:03:00,879
and i get the same result so in this

59
00:03:00,879 --> 00:03:02,739
case there&#39;s no difference but as we go

60
00:03:02,739 --> 00:03:05,650
through these demos you&#39;ll see there are

61
00:03:05,650 --> 00:03:09,909
differences and this is a good time to

62
00:03:09,909 --> 00:03:14,950
discuss something that&#39;s been changing

63
00:03:14,950 --> 00:03:16,569
and is relatively new with pandas

64
00:03:16,569 --> 00:03:18,099
dataframe is I think a lot of people are

65
00:03:18,099 --> 00:03:19,780
finding it a bit confusing

66
00:03:19,780 --> 00:03:22,359
there&#39;s a loc method and iloc method

67
00:03:22,359 --> 00:03:25,150
and ix method so what&#39;s the difference

68
00:03:25,150 --> 00:03:30,280
so loc is location in its location

69
00:03:30,280 --> 00:03:35,670
based on labels and

70
00:03:35,940 --> 00:03:37,750
iloc

71
00:03:37,750 --> 00:03:38,780
is location

72
00:03:38,780 --> 00:03:41,780
but it&#39;s based on indices

73
00:03:41,780 --> 00:03:44,270
so you have your choice of using labels

74
00:03:44,270 --> 00:03:47,000
are indices but sometimes you want to

75
00:03:47,000 --> 00:03:50,209
mix those and there&#39;s this is ix method

76
00:03:50,209 --> 00:03:53,120
which lets you mix you know maybe four

77
00:03:53,120 --> 00:03:56,209
rows you use indices for columns you use

78
00:03:56,209 --> 00:03:59,239
labels or something like that

79
00:03:59,239 --> 00:04:02,330
be careful because especially the rows

80
00:04:02,330 --> 00:04:06,230
the index in the label the defaults are

81
00:04:06,230 --> 00:04:07,760
essentially the same but they don&#39;t

82
00:04:07,760 --> 00:04:10,430
always have to be if you&#39;ve somehow

83
00:04:10,430 --> 00:04:12,680
changed your

84
00:04:12,680 --> 00:04:16,910
labels so so when you use ix just really

85
00:04:16,910 --> 00:04:18,560
be careful if there&#39;s a lot of pitfalls

86
00:04:18,560 --> 00:04:22,389
with it and

87
00:04:22,389 --> 00:04:26,409
so we can summarize for example for

88
00:04:26,409 --> 00:04:28,300
iloc

89
00:04:28,300 --> 00:04:31,270
here&#39;s again this in the square bracket

90
00:04:31,270 --> 00:04:36,129
operator you have the start row, end row

91
00:04:36,129 --> 00:04:40,980
the row stride then a comma

92
00:04:40,980 --> 00:04:43,440
and then the column start, column end

93
00:04:43,440 --> 00:04:48,000
column stride - notice the colons separate

94
00:04:48,000 --> 00:04:52,110
start end row as they always do so it

95
00:04:52,110 --> 00:04:53,910
gives you quite a bit of flexibility to

96
00:04:53,910 --> 00:04:57,090
subset exactly what you want out of your

97
00:04:57,090 --> 00:04:59,760
data frame but obviously some care is

98
00:04:59,760 --> 00:05:01,440
required

99
00:05:01,440 --> 00:05:04,080
so let&#39;s look at some other approaches

100
00:05:04,080 --> 00:05:09,060
so there&#39;s also this sample method and

101
00:05:09,060 --> 00:05:11,880
that&#39;s for random sampling so so if I

102
00:05:11,880 --> 00:05:13,860
just in and we do this a lot of data

103
00:05:13,860 --> 00:05:16,740
science we need a random sample from a

104
00:05:16,740 --> 00:05:20,970
data frame for testing something or or

105
00:05:20,970 --> 00:05:22,350
training a model or something like this

106
00:05:22,350 --> 00:05:24,750
in this case i&#39;m just i&#39;m using n equals

107
00:05:24,750 --> 00:05:27,690
five now if I wanted a a fraction i

108
00:05:27,690 --> 00:05:30,450
could go frac equals point five and i

109
00:05:30,450 --> 00:05:34,650
get a random sample that had half the

110
00:05:34,650 --> 00:05:37,950
rows in it so let me just run that we&#39;re

111
00:05:37,950 --> 00:05:40,680
just going to do this for five and you

112
00:05:40,680 --> 00:05:45,840
can see I got rows 18, 46, 141, 170, and 162

113
00:05:45,840 --> 00:05:48,000
notice that they&#39;re not necessarily in

114
00:05:48,000 --> 00:05:49,920
ascending order either is just they are

115
00:05:49,920 --> 00:05:53,420
truly randomly sampled

116
00:05:54,810 --> 00:05:55,790
now we talked about

117
00:05:55,790 --> 00:05:58,380
different ways to

118
00:05:58,380 --> 00:06:01,080
select rows to take slices let&#39;s talk

119
00:06:01,080 --> 00:06:04,200
about columns so if I just want one

120
00:06:04,200 --> 00:06:07,350
column out of my DF data frame remember

121
00:06:07,350 --> 00:06:08,670
that was the one where we had already

122
00:06:08,670 --> 00:06:13,040
subset it for just audi cars

123
00:06:13,169 --> 00:06:14,550
I&#39;m just I just want to look at the

124
00:06:14,550 --> 00:06:18,449
price and so now I get these for the six

125
00:06:18,449 --> 00:06:21,749
cars i get their price and notice this

126
00:06:21,749 --> 00:06:24,629
is now a series whenever you select a

127
00:06:24,629 --> 00:06:27,210
single column from a data frame you wind

128
00:06:27,210 --> 00:06:29,099
up with the pandas series that confuses

129
00:06:29,099 --> 00:06:31,650
people because the series have different

130
00:06:31,650 --> 00:06:33,930
attributes and different methods then

131
00:06:33,930 --> 00:06:36,270
the data frames but what if i want a

132
00:06:36,270 --> 00:06:40,830
whole bunch of columns by name or by

133
00:06:40,830 --> 00:06:43,199
label out of that data frame so here I&#39;m

134
00:06:43,199 --> 00:06:45,860
going to take five

135
00:06:46,400 --> 00:06:48,650
columns and notice i have the square

136
00:06:48,650 --> 00:06:53,270
bracket operator on the outside and then

137
00:06:53,270 --> 00:06:56,330
I this square bracket is for a Python

138
00:06:56,330 --> 00:06:58,520
list of my five name so don&#39;t get

139
00:06:58,520 --> 00:07:00,080
confused why there&#39;s bracket bracket

140
00:07:00,080 --> 00:07:02,300
they mean different things the one is

141
00:07:02,300 --> 00:07:04,789
the subsetting operator the outer

142
00:07:04,789 --> 00:07:07,400
brackets the inner brackets is just for

143
00:07:07,400 --> 00:07:11,600
a Python list so let me run that and

144
00:07:11,600 --> 00:07:14,090
there we have it for our five or six

145
00:07:14,090 --> 00:07:18,080
audi cars we&#39;ve got those five

146
00:07:18,080 --> 00:07:21,190
columns

147
00:07:21,920 --> 00:07:26,030
so we talked about  loc and loc

148
00:07:26,030 --> 00:07:29,000
already but so here&#39;s a way to use loc

149
00:07:29,000 --> 00:07:33,240
to get just the column names

150
00:07:33,240 --> 00:07:36,360
and you can see I just give it a list of

151
00:07:36,360 --> 00:07:39,419
column names but I need this colon and

152
00:07:39,419 --> 00:07:42,810
the comma to get all the rows so this is

153
00:07:42,810 --> 00:07:44,759
essentially doing the same thing I just

154
00:07:44,759 --> 00:07:52,540
did with this syntax but using loc

155
00:07:52,540 --> 00:07:54,440
oops sorry

156
00:07:54,440 --> 00:07:56,420
we run that and look at the different

157
00:07:56,420 --> 00:07:58,400
result the results the same which it

158
00:07:58,400 --> 00:08:01,990
should be so that&#39;s not a surprise

159
00:08:02,160 --> 00:08:03,960
but and what if i just wanted to drop

160
00:08:03,960 --> 00:08:07,350
like three columns out of it in this

161
00:08:07,350 --> 00:08:11,280
case we&#39;ve got about twenty six columns

162
00:08:11,280 --> 00:08:13,620
but you could have hundreds of columns

163
00:08:13,620 --> 00:08:15,180
in your data frame and you only want to

164
00:08:15,180 --> 00:08:17,370
drop a few and you don&#39;t want to use

165
00:08:17,370 --> 00:08:19,020
this kind of notation we have to write

166
00:08:19,020 --> 00:08:21,660
the whole list out for every column you

167
00:08:21,660 --> 00:08:24,990
want to keep you can use the drop method

168
00:08:24,990 --> 00:08:28,170
and provided with a list of column

169
00:08:28,170 --> 00:08:31,050
labels now you have to say in this case

170
00:08:31,050 --> 00:08:33,000
i&#39;m dropping column so I have to say axis

171
00:08:33,000 --> 00:08:36,060
equals one but i can also if I had

172
00:08:36,059 --> 00:08:39,450
labels on my rows which I do

173
00:08:39,450 --> 00:08:42,120
by default i could use axis equals 0

174
00:08:42,120 --> 00:08:45,420
and have a list of row name so drop drop

175
00:08:45,420 --> 00:08:48,030
work on rows or columns depending on

176
00:08:48,030 --> 00:08:50,940
what how you specify axis so make sure

177
00:08:50,940 --> 00:08:55,560
you get the axis correct its 0 for rows, 1

178
00:08:55,560 --> 00:08:58,280
for columns

179
00:08:59,699 --> 00:09:02,549
and you can see I&#39;ve got the 23 columns

180
00:09:02,549 --> 00:09:05,449
that should be left

181
00:09:06,110 --> 00:09:09,019
and I no longer have symbolizing

182
00:09:09,019 --> 00:09:11,930
normalized losses which are the first

183
00:09:11,930 --> 00:09:14,600
two columns, or engine size which is down

184
00:09:14,600 --> 00:09:18,370
here all right

185
00:09:18,860 --> 00:09:20,720
so one last thing I&#39;d like to show you

186
00:09:20,720 --> 00:09:24,050
in this sequence is sorting and there&#39;s

187
00:09:24,050 --> 00:09:26,510
a new method in pandas called sort_values

188
00:09:26,510 --> 00:09:28,970
the old sort method is

189
00:09:28,970 --> 00:09:31,070
now deprecated if you&#39;re at all familiar

190
00:09:31,070 --> 00:09:32,810
with it if you&#39;re not don&#39;t worry about

191
00:09:32,810 --> 00:09:33,500
it

192
00:09:33,500 --> 00:09:35,630
and in this case I&#39;m going to sort by

193
00:09:35,630 --> 00:09:38,570
rows so I have to say axis equals 0 now

194
00:09:38,570 --> 00:09:40,280
I could be sorting columns and and I

195
00:09:40,280 --> 00:09:43,250
have to have access equals one and I&#39;m

196
00:09:43,250 --> 00:09:45,770
just going to sort first by drive wheels

197
00:09:45,770 --> 00:09:50,300
and secondly by price so in your list of

198
00:09:50,300 --> 00:09:54,110
labels it&#39;s always goes sorting first

199
00:09:54,110 --> 00:09:56,960
second if you had a third fourth etc so

200
00:09:56,960 --> 00:09:58,970
that&#39;s how the sort works it&#39;s pretty

201
00:09:58,970 --> 00:10:01,700
simple so you see it did work

202
00:10:01,700 --> 00:10:04,700
and the default is ascending so

203
00:10:04,700 --> 00:10:06,830
you see I have my four wheel drive car

204
00:10:06,830 --> 00:10:09,530
and then all my front wheel drive car so

205
00:10:09,530 --> 00:10:12,320
it&#39;s just an alphabetical order and then

206
00:10:12,320 --> 00:10:13,460
by price

207
00:10:13,460 --> 00:10:14,870
well there&#39;s only one four

208
00:10:14,870 --> 00:10:17,570
wheel drive car and then you see by

209
00:10:17,570 --> 00:10:21,460
price in ascending order here

210
00:10:21,850 --> 00:10:24,310
so those are some basic operations you

211
00:10:24,310 --> 00:10:26,980
can perform to work with rows and

212
00:10:26,980 --> 00:10:30,449
columns in a pandas dataframe

