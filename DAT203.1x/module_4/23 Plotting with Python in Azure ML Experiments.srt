0
00:00:01,240 --> 00:00:03,010
So we&#39;ve looked at a number of the ways

1
00:00:03,010 --> 00:00:06,460
you can explore multi-dimensional data

2
00:00:06,460 --> 00:00:09,460
using Python tools and and how you gain

3
00:00:09,460 --> 00:00:12,639
insight from that exploration of a data

4
00:00:12,639 --> 00:00:13,929
set

5
00:00:13,929 --> 00:00:15,999
let me just show you one short thing

6
00:00:15,999 --> 00:00:17,769
here in this sequence which is how do

7
00:00:17,769 --> 00:00:19,840
you create plots when you&#39;re working

8
00:00:19,840 --> 00:00:22,240
with Azure machine learning and that

9
00:00:22,240 --> 00:00:23,679
there&#39;s a lot of reasons for

10
00:00:23,679 --> 00:00:26,500
investigating the results of your model

11
00:00:26,500 --> 00:00:29,199
or displaying attributes of your data

12
00:00:29,199 --> 00:00:32,169
set where you might want to do that so

13
00:00:32,168 --> 00:00:35,170
my screen here I have some basic code to

14
00:00:35,170 --> 00:00:38,540
do just that so

15
00:00:38,540 --> 00:00:42,500
I have the usual where I&#39;d define the

16
00:00:42,500 --> 00:00:47,180
azureml_main main function and i am

17
00:00:47,180 --> 00:00:51,950
reading a data frame from the first

18
00:00:51,950 --> 00:00:54,620
input port and then I&#39;m just going to

19
00:00:54,620 --> 00:00:58,989
divide that data frame into two

20
00:00:59,450 --> 00:01:01,490
by type of aspiration whether its

21
00:01:01,490 --> 00:01:03,920
standard or turbo

22
00:01:03,920 --> 00:01:07,729
then I import matplotlib as pyplot

23
00:01:07,729 --> 00:01:11,509
and the usual stuff to define my figure make

24
00:01:11,509 --> 00:01:14,360
my two scatter plots

25
00:01:14,360 --> 00:01:18,140
set my title as usual so here&#39;s the only

26
00:01:18,140 --> 00:01:20,060
thing that&#39;s different

27
00:01:20,060 --> 00:01:21,500
when i&#39;m working in Azure machine

28
00:01:21,500 --> 00:01:23,060
learning

29
00:01:23,060 --> 00:01:28,370
I need to do this fig.savefig and I

30
00:01:28,370 --> 00:01:30,890
need to give it a unique name and it&#39;s

31
00:01:30,890 --> 00:01:33,710
going to be a PNG file so it&#39;s the PNG

32
00:01:33,710 --> 00:01:35,540
file that i&#39;m going to see displayed at

33
00:01:35,540 --> 00:01:37,520
the end so just keep that in mind you

34
00:01:37,520 --> 00:01:39,860
need to save your figure that you&#39;ve

35
00:01:39,860 --> 00:01:42,560
just created you know is you define the

36
00:01:42,560 --> 00:01:45,530
figure here and on its size and whatever

37
00:01:45,530 --> 00:01:47,570
other attributes you need and then you

38
00:01:47,570 --> 00:01:50,900
save that figure down here with with the

39
00:01:50,900 --> 00:01:52,759
savefig method

40
00:01:52,759 --> 00:01:56,479
alright so let me select all that code

41
00:01:56,479 --> 00:02:01,210
and copy it over and I have a

42
00:02:01,210 --> 00:02:05,680
experiment i started here where I&#39;ve got

43
00:02:05,680 --> 00:02:07,780
my automobile price data i&#39;m removing

44
00:02:07,780 --> 00:02:09,880
the missing date in this case using the

45
00:02:09,880 --> 00:02:12,130
clean missing data module I could do

46
00:02:12,130 --> 00:02:13,840
that with my Python code of course but

47
00:02:13,840 --> 00:02:16,270
in this case it&#39;s just easier to do it

48
00:02:16,270 --> 00:02:19,569
with the built-in AML module and let me

49
00:02:19,569 --> 00:02:23,860
get an execute python script module on

50
00:02:23,860 --> 00:02:26,860
the canvas here for you and I just

51
00:02:26,860 --> 00:02:30,489
connect that and i&#39;m going to use Python

52
00:02:30,489 --> 00:02:34,050
3.5 here

53
00:02:34,360 --> 00:02:36,640
I&#39;m just going to replace all that

54
00:02:36,640 --> 00:02:39,820
example code with the code i just showed

55
00:02:39,820 --> 00:02:41,440
you

56
00:02:41,440 --> 00:02:44,190
save this

57
00:02:45,760 --> 00:02:48,090
run it

58
00:02:50,790 --> 00:02:54,150
and there its run and all you have to do

59
00:02:54,150 --> 00:02:56,189
them to see the result is look at this

60
00:02:56,189 --> 00:02:58,890
Python device port it&#39;s that port on

61
00:02:58,890 --> 00:03:01,560
the lower right so just click on that

62
00:03:01,560 --> 00:03:04,910
click on visualize

63
00:03:04,910 --> 00:03:08,150
and there we have it our plot which

64
00:03:08,150 --> 00:03:11,450
shows the length of the auto and the

65
00:03:11,450 --> 00:03:12,980
price of the auto on the vertical axis

66
00:03:12,980 --> 00:03:17,450
and standard aspiration cars in blue and

67
00:03:17,450 --> 00:03:23,460
turbo aspiration cars in red

68
00:03:23,460 --> 00:03:26,610
and that&#39;s we don&#39;t what we see actually

69
00:03:26,610 --> 00:03:30,000
here is our most expensive cars are all

70
00:03:30,000 --> 00:03:33,150
standard aspiration and the turbo car

71
00:03:33,150 --> 00:03:34,560
seemed to have a slightly different

72
00:03:34,560 --> 00:03:37,470
curve up here from again being

73
00:03:37,470 --> 00:03:41,010
somewhat more expensive generally for a

74
00:03:41,010 --> 00:03:43,530
given length than the standard

75
00:03:43,530 --> 00:03:47,640
aspiration cars so there you have it

76
00:03:47,640 --> 00:03:50,910
how simple it is to take any plotting

77
00:03:50,910 --> 00:03:54,090
code you have in Python and just with

78
00:03:54,090 --> 00:03:57,660
the addition of one method call you can

79
00:03:57,660 --> 00:04:00,270
add as many plots as you want just make

80
00:04:00,270 --> 00:04:02,670
sure every time you have a plot you give

81
00:04:02,670 --> 00:04:07,350
it a unique name for that PNG file and

82
00:04:07,350 --> 00:04:09,210
you&#39;ll see them all in your output at

83
00:04:09,210 --> 00:04:13,310
that Python device port

