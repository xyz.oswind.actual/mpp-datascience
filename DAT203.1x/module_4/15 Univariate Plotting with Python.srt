0
00:00:00,000 --> 00:00:03,570
So this is the first sequence of our

1
00:00:03,570 --> 00:00:07,080
lessons of how to use Python plotting

2
00:00:07,080 --> 00:00:08,760
methods for data exploration and

3
00:00:08,760 --> 00:00:10,769
visualization in this particular

4
00:00:10,769 --> 00:00:12,599
sequence we&#39;re going to focus on

5
00:00:12,599 --> 00:00:15,570
univariate plot types and a little bit

6
00:00:15,570 --> 00:00:19,260
talk about how you use Python methods

7
00:00:19,260 --> 00:00:21,510
for plotting but we&#39;re really going to

8
00:00:21,510 --> 00:00:25,289
focus on those univariate plot types so

9
00:00:25,289 --> 00:00:28,380
we&#39;ve already talked about this

10
00:00:28,380 --> 00:00:31,410
automobile price data set and how we

11
00:00:31,410 --> 00:00:34,680
clean it so I&#39;m not going to dwell on

12
00:00:34,680 --> 00:00:38,219
that again I&#39;ll just run that and load

13
00:00:38,219 --> 00:00:39,960
it and we&#39;ll just look at the head of

14
00:00:39,960 --> 00:00:41,960
the data frame

15
00:00:41,960 --> 00:00:44,960
and again by now this should be

16
00:00:44,960 --> 00:00:47,210
somewhat familiar to you we have this

17
00:00:47,210 --> 00:00:52,190
data set with195

18
00:00:52,190 --> 00:00:54,320
automobiles once you clean the missing

19
00:00:54,320 --> 00:00:57,170
values out and various attributes of the

20
00:00:57,170 --> 00:00:59,300
automobile and what we&#39;re interested in

21
00:00:59,300 --> 00:01:02,690
is understanding how we can model the

22
00:01:02,690 --> 00:01:05,239
price of the automobile

23
00:01:05,239 --> 00:01:08,240
which of these many variables tell us

24
00:01:08,240 --> 00:01:10,130
something interesting about price and

25
00:01:10,130 --> 00:01:13,820
how do these many variables or features

26
00:01:13,820 --> 00:01:17,450
relate to price but initially we&#39;re

27
00:01:17,450 --> 00:01:20,240
going to look at just univariate plot

28
00:01:20,240 --> 00:01:22,130
type so things like bar plots histograms

29
00:01:22,130 --> 00:01:25,100
boxplots kernel density

30
00:01:25,100 --> 00:01:28,550
estimation plots and violin plots will

31
00:01:28,550 --> 00:01:32,270
pick up 2D plots to variable plots in

32
00:01:32,270 --> 00:01:35,360
the next sequence so let&#39;s talk about

33
00:01:35,360 --> 00:01:38,600
bar plots so the first thing you have to

34
00:01:38,600 --> 00:01:41,659
do in Python to compute a barplot is to

35
00:01:41,659 --> 00:01:45,930
compute the counts so

36
00:01:45,930 --> 00:01:48,720
from my pandas dataframe of auto_prices

37
00:01:48,720 --> 00:01:51,600
I just want say the make

38
00:01:51,600 --> 00:01:54,300
and we&#39;ll look at the make as

39
00:01:54,300 --> 00:01:56,850
manufacturer and we&#39;ll just get the key

40
00:01:56,850 --> 00:01:59,900
vector of counts. And there you

41
00:02:00,790 --> 00:02:03,970
have it by manufacturer notice the list

42
00:02:03,970 --> 00:02:07,840
is ordered from the most frequent to the

43
00:02:07,840 --> 00:02:10,290
least frequent

44
00:02:10,840 --> 00:02:14,500
and since we haven&#39;t looked at a python

45
00:02:14,500 --> 00:02:16,209
plot i&#39;m just going to give you a few

46
00:02:16,209 --> 00:02:18,489
highlights so first off if you&#39;re

47
00:02:18,489 --> 00:02:20,440
working in a Jupyter notebook make sure

48
00:02:20,440 --> 00:02:22,269
you have this what&#39;s called an

49
00:02:22,269 --> 00:02:25,390
ipython magic command which is %matplotlib inline

50
00:02:25,390 --> 00:02:28,060
if you don&#39;t have that and

51
00:02:28,060 --> 00:02:29,860
and notice there&#39;s a % there which

52
00:02:29,860 --> 00:02:32,799
denotes it is a magic command if you

53
00:02:32,799 --> 00:02:34,780
don&#39;t have that if that it&#39;s basically

54
00:02:34,780 --> 00:02:37,330
an environment variable that tells the

55
00:02:37,330 --> 00:02:39,519
Jupyter notebook or the ipython you&#39;re

56
00:02:39,519 --> 00:02:42,819
using in Jupyter to display the plot in

57
00:02:42,819 --> 00:02:45,519
line after the code, which is what you

58
00:02:45,519 --> 00:02:47,170
would think should be the default but it

59
00:02:47,170 --> 00:02:49,540
actually isn&#39;t so we&#39;re going to import

60
00:02:49,540 --> 00:02:54,940
matplotlib.pyploy as plot we&#39;re

61
00:02:54,940 --> 00:02:56,799
going to define a figure with a figure

62
00:02:56,799 --> 00:02:58,540
size

63
00:02:58,540 --> 00:03:00,760
matplotlib figure we&#39;re going to

64
00:03:00,760 --> 00:03:04,810
define an axis on that figure so it&#39;s a

65
00:03:04,810 --> 00:03:07,989
set of axes and we just have one in this

66
00:03:07,989 --> 00:03:13,000
case and we get count.plots.bar

67
00:03:13,000 --> 00:03:16,840
on that axis and we can give it so then

68
00:03:16,840 --> 00:03:19,390
we can use methods on axis like the

69
00:03:19,390 --> 00:03:22,870
title the xlabel the ylabel so

70
00:03:22,870 --> 00:03:24,519
that&#39;s kind of the basic recipe for

71
00:03:24,519 --> 00:03:28,299
making a simple Python plot using matplotlib

72
00:03:28,299 --> 00:03:30,910
not too hard

73
00:03:30,910 --> 00:03:33,810
few steps

74
00:03:35,920 --> 00:03:40,209
all right and here&#39;s our result so we

75
00:03:40,209 --> 00:03:43,209
have the make of the automobile on this

76
00:03:43,209 --> 00:03:46,840
x-axis you see going from and then the

77
00:03:46,840 --> 00:03:50,590
number of autos on the y-axis and

78
00:03:50,590 --> 00:03:55,690
because our counts were ordered this bar

79
00:03:55,690 --> 00:03:58,330
plot is nicely ordered and generally

80
00:03:58,330 --> 00:04:00,550
when you&#39;re doing a bar plot it&#39;s good

81
00:04:00,550 --> 00:04:02,590
to have some order whether it&#39;s from

82
00:04:02,590 --> 00:04:04,660
most frequent to least or the other way

83
00:04:04,660 --> 00:04:07,870
around because it if you just had these

84
00:04:07,870 --> 00:04:10,600
randomly here you know you have to

85
00:04:10,600 --> 00:04:12,700
really search like okay maybe the first

86
00:04:12,700 --> 00:04:15,400
one and the second one would be obvious

87
00:04:15,400 --> 00:04:17,530
but could you really tell that there&#39;s a

88
00:04:17,529 --> 00:04:19,600
few more that maybe one more you know

89
00:04:19,600 --> 00:04:21,940
the same number it looks like maybe of

90
00:04:21,940 --> 00:04:24,220
mazdas Mitsubishis and Hondas

91
00:04:24,220 --> 00:04:26,560
here and then the next most frequent

92
00:04:26,560 --> 00:04:30,479
being Volkswagens and

93
00:04:30,560 --> 00:04:33,770
etc so so ordering it really helps

94
00:04:33,770 --> 00:04:37,040
you and your audience if you&#39;re

95
00:04:37,040 --> 00:04:39,380
presenting this to your colleagues or

96
00:04:39,380 --> 00:04:40,820
something like that

97
00:04:40,820 --> 00:04:44,300
understand what are the most common at

98
00:04:44,300 --> 00:04:45,740
least common so that&#39;s the whole point

99
00:04:45,740 --> 00:04:47,330
of a bar plot is just to look at

100
00:04:47,330 --> 00:04:50,270
frequency

101
00:04:50,270 --> 00:04:55,880
so another related plot is histogram and

102
00:04:55,880 --> 00:04:59,210
a histogram is like, now a bar plot was

103
00:04:59,210 --> 00:05:02,150
for categorical variables a car is made

104
00:05:02,150 --> 00:05:04,580
by Toyota or it&#39;s made by some other

105
00:05:04,580 --> 00:05:08,000
manufacturer like Nissan, Mazda it is not

106
00:05:08,000 --> 00:05:10,730
made by eight different manufacturers

107
00:05:10,730 --> 00:05:13,340
or at least the name badge on it doesn&#39;t

108
00:05:13,340 --> 00:05:17,750
have eight different names on it so

109
00:05:17,750 --> 00:05:22,010
again following that same recipe and

110
00:05:22,010 --> 00:05:24,320
this time let&#39;s look at engine sizes. So

111
00:05:24,320 --> 00:05:26,870
I&#39;ve got auto.prices i&#39;m selecting using

112
00:05:26,870 --> 00:05:28,520
the square bracket operator just ended

113
00:05:28,520 --> 00:05:31,820
in size and I&#39;ve got plot.hist on my

114
00:05:31,820 --> 00:05:35,600
axis and again just title xlabel ylabel

115
00:05:35,600 --> 00:05:37,550
so it&#39;s the same recipe i just

116
00:05:37,550 --> 00:05:40,060
showed you

117
00:05:41,569 --> 00:05:43,789
but the histogram is a little different

118
00:05:43,789 --> 00:05:45,979
because engine size is a numeric it&#39;s a

119
00:05:45,979 --> 00:05:48,819
continuous variable

120
00:05:49,009 --> 00:05:52,939
and so we&#39;ve binned it and we&#39;re using

121
00:05:52,939 --> 00:05:54,650
just the default bin size you can

122
00:05:54,650 --> 00:05:57,559
specify then widths and things like that

123
00:05:57,559 --> 00:05:59,449
but for now we&#39;re we&#39;re not doing that

124
00:05:59,449 --> 00:06:02,960
so we can see that the most frequent

125
00:06:02,960 --> 00:06:06,830
auto engine size seems to fall around a

126
00:06:06,830 --> 00:06:10,720
hundred these are in cubic inches

127
00:06:10,720 --> 00:06:13,360
and there&#39;s a few cars with really large

128
00:06:13,360 --> 00:06:16,780
engine sizes out beyond 300 and there&#39;s

129
00:06:16,780 --> 00:06:19,300
some really you know maybe I think

130
00:06:19,300 --> 00:06:21,940
that&#39;s around 80 probably less than 80

131
00:06:21,940 --> 00:06:24,070
cubic inches is just a few cars again

132
00:06:24,070 --> 00:06:26,590
with very small engine sizes and

133
00:06:26,590 --> 00:06:30,490
somewhat asymmetric distribution so

134
00:06:30,490 --> 00:06:31,960
we&#39;ve learned a lot just by looking at

135
00:06:31,960 --> 00:06:34,270
the histogram of these auto engine sizes

136
00:06:34,270 --> 00:06:37,210
about the shape of the

137
00:06:37,210 --> 00:06:39,670
distribution the dispersion the fact

138
00:06:39,670 --> 00:06:41,830
that we have these sort of extreme cases

139
00:06:41,830 --> 00:06:43,960
of some cars with really large engines

140
00:06:43,960 --> 00:06:46,000
and some cars with really small engines

141
00:06:46,000 --> 00:06:48,310
and the majority of cars tend to be

142
00:06:48,310 --> 00:06:52,420
toward the smaller end so another way to

143
00:06:52,420 --> 00:06:55,920
look at this sort of

144
00:06:55,920 --> 00:06:57,750
distributional information is with a

145
00:06:57,750 --> 00:07:00,210
boxplot

146
00:07:00,210 --> 00:07:04,530
but now there&#39;s no point or very little

147
00:07:04,530 --> 00:07:07,530
point in just using a single box plot

148
00:07:07,530 --> 00:07:10,199
that&#39;s the same information that you can

149
00:07:10,199 --> 00:07:11,819
get from a histogram the histogram will

150
00:07:11,819 --> 00:07:13,979
almost invariably give you a little more

151
00:07:13,979 --> 00:07:16,949
detail but there&#39;s something else we can

152
00:07:16,949 --> 00:07:19,710
do again following that same recipe of

153
00:07:19,710 --> 00:07:23,639
using the matplotlib pyplot plot

154
00:07:23,639 --> 00:07:27,240
methods i&#39;m going to subselect now

155
00:07:27,240 --> 00:07:31,139
engine size and fuel type so you see I have

156
00:07:31,139 --> 00:07:33,870
another column selected and I&#39;m going

157
00:07:33,870 --> 00:07:35,880
to box plot and then I&#39;ve got this by

158
00:07:35,880 --> 00:07:41,160
argument by fuel type on those axes so

159
00:07:41,160 --> 00:07:42,900
basically i&#39;m going to do a group buy

160
00:07:42,900 --> 00:07:45,870
fuel type

161
00:07:45,870 --> 00:07:48,480
they let me run this

162
00:07:48,480 --> 00:07:51,810
and you can see I&#39;ve got fuel type on

163
00:07:51,810 --> 00:07:56,280
the x-axis and engine size on this y

164
00:07:56,280 --> 00:07:59,940
vertical axis and i have diesel and gas

165
00:07:59,940 --> 00:08:03,930
cars and so it looks like gas cars have

166
00:08:03,930 --> 00:08:07,050
a wider range of engine sizes and you

167
00:08:07,050 --> 00:08:09,570
can see some all those cars with the

168
00:08:09,570 --> 00:08:13,470
really big engines out there you know

169
00:08:13,470 --> 00:08:17,040
beyond 200 or even 300 cubic inches were all

170
00:08:17,040 --> 00:08:20,010
gas cars, those cars with really tiny

171
00:08:20,010 --> 00:08:24,750
engines were all gas cars, diesel cars

172
00:08:24,750 --> 00:08:27,030
tend to be in a smaller range so you see

173
00:08:27,030 --> 00:08:29,160
the advantage of a box plot like this

174
00:08:29,160 --> 00:08:31,440
and you could do this with many

175
00:08:31,440 --> 00:08:33,390
categories of data and we&#39;ll be doing

176
00:08:33,390 --> 00:08:35,640
that as we go through this course you

177
00:08:35,640 --> 00:08:38,100
can quickly by i compare and say yeah

178
00:08:38,100 --> 00:08:41,430
diesel cars have a tighter grouping than

179
00:08:41,429 --> 00:08:44,340
gas cars their median tends to be a

180
00:08:44,340 --> 00:08:46,590
little larger engine size but there&#39;s a

181
00:08:46,590 --> 00:08:48,990
lot of ... but the inner quartile

182
00:08:48,990 --> 00:08:51,990
range here overlap substantially so it&#39;s

183
00:08:51,990 --> 00:08:54,510
not really probably a huge significant

184
00:08:54,510 --> 00:08:56,700
difference it&#39;s just the range of gas

185
00:08:56,700 --> 00:08:59,560
cars is much wider

186
00:08:59,560 --> 00:09:04,430
so an alternative to a histogram

187
00:09:04,430 --> 00:09:08,690
is a kernel density plot and when we&#39;re

188
00:09:08,690 --> 00:09:10,910
doing kernel density plot i&#39;m going to

189
00:09:10,910 --> 00:09:13,370
use another python package that&#39;s fairly

190
00:09:13,370 --> 00:09:19,010
new and has really become a

191
00:09:19,010 --> 00:09:22,899
commonly-used thing

192
00:09:23,660 --> 00:09:26,000
and it&#39;s the Seaborn package so I&#39;m just

193
00:09:26,000 --> 00:09:30,650
going to import seaborn as SNS and you

194
00:09:30,650 --> 00:09:33,290
have to set up a grid to plot a seaborne

195
00:09:33,290 --> 00:09:38,270
plot so I&#39;ve got sns.set_style and

196
00:09:38,270 --> 00:09:39,740
i&#39;m just going to use the most generic

197
00:09:39,740 --> 00:09:42,410
grid really is called white grid so I&#39;m

198
00:09:42,410 --> 00:09:45,140
just going to do that and I can do

199
00:09:45,140 --> 00:09:48,710
sns.kde for kernel density estimation plot

200
00:09:48,710 --> 00:09:52,640
of my engine size see how that works

201
00:09:52,640 --> 00:09:55,310
rather than doing and this is an

202
00:09:55,310 --> 00:09:58,420
alternative to a histogram

203
00:09:59,230 --> 00:10:01,030
let me scroll down so you guys can see

204
00:10:01,030 --> 00:10:02,980
that

205
00:10:02,980 --> 00:10:07,340
and we have essentially the density

206
00:10:07,340 --> 00:10:09,860
on this vertical axis and the engine

207
00:10:09,860 --> 00:10:12,920
size on the horizontal axis very similar

208
00:10:12,920 --> 00:10:14,600
conclusions you would draw from this

209
00:10:14,600 --> 00:10:16,160
from the histogram that are most

210
00:10:16,160 --> 00:10:17,779
frequent

211
00:10:17,779 --> 00:10:19,999
engine size is somewhere around a

212
00:10:19,999 --> 00:10:22,519
hundred cubic inches in this case the

213
00:10:22,519 --> 00:10:24,649
density plot saying a little bit more

214
00:10:24,649 --> 00:10:27,230
we&#39;ve got a few cars with really small

215
00:10:27,230 --> 00:10:31,100
engines in this set of outlier somewhere

216
00:10:31,100 --> 00:10:33,649
out here around 300 so it&#39;s just a

217
00:10:33,649 --> 00:10:36,559
different way of looking at density and

218
00:10:36,559 --> 00:10:39,470
in fact you can overlay this on a

219
00:10:39,470 --> 00:10:41,870
histogram as well to get two views

220
00:10:41,870 --> 00:10:44,559
in one

221
00:10:46,140 --> 00:10:49,470
so i can do a little bit fancier plot

222
00:10:49,470 --> 00:10:51,330
there so i just use the very generic so

223
00:10:51,330 --> 00:10:54,480
i can mix matplotlib and seaborn

224
00:10:54,480 --> 00:10:56,550
and just wanted to give you an example

225
00:10:56,550 --> 00:10:58,320
that so first off i can set the figure

226
00:10:58,320 --> 00:11:00,060
size notice i just took the default

227
00:11:00,060 --> 00:11:04,020
figure siz here i can again define

228
00:11:04,020 --> 00:11:07,410
axes I can create my plot but this time

229
00:11:07,410 --> 00:11:10,710
i say what axes I want to do that on and

230
00:11:10,710 --> 00:11:13,200
then i can give it titles and labels

231
00:11:13,200 --> 00:11:16,320
notice this plot is actually not a very

232
00:11:16,320 --> 00:11:19,260
good plot if I showed this to a group of

233
00:11:19,260 --> 00:11:23,970
my bosses say or my clients it you know

234
00:11:23,970 --> 00:11:25,620
other than this hint here that this is

235
00:11:25,620 --> 00:11:28,770
engine size it&#39;s like well okay but but

236
00:11:28,770 --> 00:11:31,230
what&#39;s this vertical axis what&#39;s what&#39;s

237
00:11:31,230 --> 00:11:34,230
the title i mean people cannot, your

238
00:11:34,230 --> 00:11:36,240
audience cannot interpret your plot

239
00:11:36,240 --> 00:11:39,090
unless you have proper labels on it so

240
00:11:39,090 --> 00:11:43,440
so combining matplotliib

241
00:11:43,440 --> 00:11:45,570
and seaborn gives us a lot

242
00:11:45,570 --> 00:11:47,920
more control of that

243
00:11:47,920 --> 00:11:50,170
and you see we get a plot that&#39;s a more

244
00:11:50,170 --> 00:11:53,019
proper plot so it says KDE plot of

245
00:11:53,019 --> 00:11:54,760
auto engine size so we know what it is

246
00:11:54,760 --> 00:11:56,860
we&#39;ve got density labeled properly on

247
00:11:56,860 --> 00:12:00,190
the vertical axis engine size labeled

248
00:12:00,190 --> 00:12:03,279
properly on the horizontal axis so

249
00:12:03,279 --> 00:12:06,760
there&#39;s one other plot we can use for

250
00:12:06,760 --> 00:12:09,570
getting

251
00:12:10,950 --> 00:12:15,060
pardon me for looking at densities and

252
00:12:15,060 --> 00:12:17,280
it&#39;s similar to the box plot itself it&#39;s

253
00:12:17,280 --> 00:12:19,620
a newer plot type violin plots have

254
00:12:19,620 --> 00:12:22,860
only existed for maybe a few decades

255
00:12:22,860 --> 00:12:25,830
boxplots go way back to work of two key

256
00:12:25,830 --> 00:12:28,020
in the early nineteen sixties so they&#39;ve

257
00:12:28,020 --> 00:12:29,340
been around for more than half a century

258
00:12:29,340 --> 00:12:32,430
and and there&#39;s certain things about

259
00:12:32,430 --> 00:12:34,380
violin plots that&#39;ll that are more

260
00:12:34,380 --> 00:12:37,260
attractive and so let me just write this

261
00:12:37,260 --> 00:12:39,330
the recipe here is the same as what you

262
00:12:39,330 --> 00:12:42,250
just saw using Seaborn

263
00:12:42,250 --> 00:12:44,080
so let me just run it and we&#39;ll cut to

264
00:12:44,080 --> 00:12:45,970
the chase

265
00:12:45,970 --> 00:12:50,620
so I&#39;ve divided it you see I did my my

266
00:12:50,620 --> 00:12:53,589
x was fuel type my y was engine size

267
00:12:53,589 --> 00:12:56,319
and so I have gas engines and diesel engines

268
00:12:56,319 --> 00:12:58,509
recall that we looked at box plots of

269
00:12:58,509 --> 00:13:00,879
this before with the smaller range of

270
00:13:00,879 --> 00:13:03,879
diesel engines in the wider range of gas

271
00:13:03,879 --> 00:13:06,370
engines but now the KDE the kernel

272
00:13:06,370 --> 00:13:10,240
density estimate defines this shape and

273
00:13:10,240 --> 00:13:11,740
you can see it does kinda look some of

274
00:13:11,740 --> 00:13:13,689
these do kinda look like a violin with

275
00:13:13,689 --> 00:13:14,439
it

276
00:13:14,439 --> 00:13:16,990
not always but but but kind of so that&#39;s

277
00:13:16,990 --> 00:13:18,879
where they got their name and their

278
00:13:18,879 --> 00:13:21,639
giving you the interquartile range the

279
00:13:21,639 --> 00:13:23,319
median with the white dot and the

280
00:13:23,319 --> 00:13:25,360
whiskers that you would see on a box

281
00:13:25,360 --> 00:13:28,509
plot in here just for free so in some

282
00:13:28,509 --> 00:13:30,970
ways the way they the Seaborn package

283
00:13:30,970 --> 00:13:33,970
does this combines the best of what you

284
00:13:33,970 --> 00:13:36,879
learn from a boxplot about interquartile

285
00:13:36,879 --> 00:13:41,170
range median whiskers outliers with a

286
00:13:41,170 --> 00:13:43,060
kernel density estimation plot so it&#39;s a

287
00:13:43,060 --> 00:13:47,290
very attractive way to convey quite a

288
00:13:47,290 --> 00:13:51,910
bit of information about density of

289
00:13:51,910 --> 00:13:54,819
various variables and again we could

290
00:13:54,819 --> 00:13:57,670
subdivide this we could do this right

291
00:13:57,670 --> 00:13:59,110
now we&#39;re just doing it by fuel type but

292
00:13:59,110 --> 00:14:00,879
we could do it by any categorical

293
00:14:00,879 --> 00:14:03,009
variable body style or manufacturer

294
00:14:03,009 --> 00:14:04,779
whatever it is you&#39;re you&#39;re interested

295
00:14:04,779 --> 00:14:09,170
in to get a good look at things

296
00:14:09,170 --> 00:14:13,100
so that&#39;s a quick overview of both some

297
00:14:13,100 --> 00:14:16,490
plot methods using python and more

298
00:14:16,490 --> 00:14:20,209
importantly some plot types what what

299
00:14:20,209 --> 00:14:22,040
they&#39;re good for and how we interpret

300
00:14:22,040 --> 00:14:24,459
them

