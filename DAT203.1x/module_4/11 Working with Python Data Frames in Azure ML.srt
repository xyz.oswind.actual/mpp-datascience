0
00:00:03,740 --> 00:00:09,440
Hi so in Azure machine learning you can

1
00:00:09,440 --> 00:00:13,190
run python scripts in what&#39;s called the

2
00:00:13,190 --> 00:00:17,720
Execute Python module and you you take

3
00:00:17,720 --> 00:00:20,960
an Azure machine learning table and read

4
00:00:20,960 --> 00:00:24,920
it into that module and it becomes a

5
00:00:24,920 --> 00:00:28,369
pandas dataframe so let&#39;s look at my

6
00:00:28,369 --> 00:00:30,710
screen to illustrate what i mean by that

7
00:00:30,710 --> 00:00:32,689
so here i have a very simple experiment

8
00:00:32,689 --> 00:00:35,570
with a data set and it&#39;s feeding into

9
00:00:35,570 --> 00:00:39,079
this Execute Python script module

10
00:00:39,079 --> 00:00:41,690
and when i run my experiment that data

11
00:00:41,690 --> 00:00:43,579
frame, that excuse me that Azure

12
00:00:43,579 --> 00:00:46,309
machine learning table is input from the

13
00:00:46,309 --> 00:00:49,129
data set into the execute python script

14
00:00:49,129 --> 00:00:51,220
module

15
00:00:51,220 --> 00:00:54,040
and it becomes a pandas dataframe that i

16
00:00:54,040 --> 00:00:56,650
can then do whatever manipulation i want

17
00:00:56,650 --> 00:00:59,380
on that so let&#39;s drill down a little bit

18
00:00:59,380 --> 00:01:01,600
on what&#39;s going on with that execute

19
00:01:01,600 --> 00:01:05,110
python script module so this rectangle

20
00:01:05,110 --> 00:01:08,790
here represents that module

21
00:01:09,420 --> 00:01:12,659
I have my two Azure machine learning

22
00:01:12,659 --> 00:01:16,080
tables going into the port 1 and port

23
00:01:16,080 --> 00:01:17,820
two - they&#39;re called data set one and dataset

24
00:01:17,820 --> 00:01:21,990
two and all i have to do when i define my

25
00:01:21,990 --> 00:01:25,049
main function, my azureml_main

26
00:01:25,049 --> 00:01:28,649
function, is just give it one or two

27
00:01:28,649 --> 00:01:33,149
I can do one or two arguments here and

28
00:01:33,149 --> 00:01:35,580
just call them any name I want have called

29
00:01:35,580 --> 00:01:37,770
them frame 1 and frame to in this case

30
00:01:37,770 --> 00:01:42,060
so now i have two panda data frames as

31
00:01:42,060 --> 00:01:45,960
the input arguments to my python script

32
00:01:45,960 --> 00:01:49,979
whatever that is. I can also from a zip

33
00:01:49,979 --> 00:01:52,229
file port here i can have a zip file and

34
00:01:52,229 --> 00:01:54,119
maybe i have some custom module I&#39;ve

35
00:01:54,119 --> 00:01:56,490
built and I want to import that and use

36
00:01:56,490 --> 00:01:59,640
it in my script so this the usual

37
00:01:59,640 --> 00:02:02,520
thing you do if you&#39;re working at the

38
00:02:02,520 --> 00:02:06,149
command line just import my module as

39
00:02:06,149 --> 00:02:08,069
you give it some convenient name like I

40
00:02:08,068 --> 00:02:10,440
called it mm in this case for my module

41
00:02:10,440 --> 00:02:13,680
so that i can use you know m.[whatever]

42
00:02:13,680 --> 00:02:16,380
you know functions or methods I

43
00:02:16,380 --> 00:02:19,530
have in that module

44
00:02:19,530 --> 00:02:22,709
I can also print to this Python

45
00:02:22,709 --> 00:02:26,790
deviceport here so for debugging or or

46
00:02:26,790 --> 00:02:28,140
just getting some additional information

47
00:02:28,140 --> 00:02:30,330
out like here I&#39;m just printing hello

48
00:02:30,330 --> 00:02:33,510
world to that device port and I&#39;ll see

49
00:02:33,510 --> 00:02:37,530
that printed output in the output.log

50
00:02:37,530 --> 00:02:41,980
file which I can read from

51
00:02:41,980 --> 00:02:44,620
and then finally say just want to return

52
00:02:44,620 --> 00:02:48,459
frame one from this little python script

53
00:02:48,459 --> 00:02:51,659
so I return

54
00:02:51,680 --> 00:02:54,290
a pandas data frame and it becomes an

55
00:02:54,290 --> 00:02:56,629
Azure machine learning table now make

56
00:02:56,629 --> 00:02:58,790
sure when you return what you return is

57
00:02:58,790 --> 00:03:01,790
in fact a pandas dataframe if you try to

58
00:03:01,790 --> 00:03:05,150
return a list or any other Python object

59
00:03:05,150 --> 00:03:08,359
type you&#39;re going to get you know

60
00:03:08,359 --> 00:03:10,730
sometimes very strange error messages

61
00:03:10,730 --> 00:03:14,090
and the reason is that that port

62
00:03:14,090 --> 00:03:16,400
only knows how to deal with pandas

63
00:03:16,400 --> 00:03:18,409
dataframes, so just make sure you

64
00:03:18,409 --> 00:03:21,730
keep that in mind

65
00:03:21,960 --> 00:03:25,200
so I hope that little introduction tells

66
00:03:25,200 --> 00:03:27,360
you what you need to know to take

67
00:03:27,360 --> 00:03:31,380
otherwise essentially ordinary Python

68
00:03:31,380 --> 00:03:35,160
code of any sort and the few extra

69
00:03:35,160 --> 00:03:37,470
things you need to do so that you set it

70
00:03:37,470 --> 00:03:40,230
up and run it successfully within Azure

71
00:03:40,230 --> 00:03:42,630
machine learning in an Execute Python

72
00:03:42,630 --> 00:03:46,560
script module. On my screen here i have a

73
00:03:46,560 --> 00:03:48,510
very simple experiment i have this

74
00:03:48,510 --> 00:03:53,850
automobiles price data set and I have an

75
00:03:53,850 --> 00:03:57,800
Execute Python script module

76
00:03:57,810 --> 00:03:59,760
click on this and you&#39;ll see a little

77
00:03:59,760 --> 00:04:03,980
code editor window appears here

78
00:04:03,980 --> 00:04:05,959
and so what&#39;s that what what is this

79
00:04:05,959 --> 00:04:08,720
code doing the first line I just define

80
00:04:08,720 --> 00:04:11,810
a main function this azureml_main

81
00:04:11,810 --> 00:04:15,530
and since i only have one input to

82
00:04:15,530 --> 00:04:18,229
results Dataset1, I only have one

83
00:04:18,228 --> 00:04:21,799
argument here which I&#39;m calling autos if

84
00:04:21,798 --> 00:04:24,470
I had a second input from somewhere to

85
00:04:24,470 --> 00:04:27,080
results Dataset2 I&#39;d have to have a comma

86
00:04:27,080 --> 00:04:32,060
some other name and that would be my

87
00:04:32,060 --> 00:04:34,610
second argument to this main function

88
00:04:34,610 --> 00:04:37,669
and whatever that those argument names

89
00:04:37,669 --> 00:04:43,700
are turn out to be pandas data frames

90
00:04:43,700 --> 00:04:45,320
typical things that I would do with the

91
00:04:45,320 --> 00:04:48,230
pandas dataframe like here i&#39;m going to

92
00:04:48,230 --> 00:04:50,930
subset auto so I only say only want the

93
00:04:50,930 --> 00:04:56,249
height and width of the cars in that so

94
00:04:56,249 --> 00:04:58,799
recall the outer square brackets are the

95
00:04:58,799 --> 00:05:03,359
subset operator for pandas and the inner

96
00:05:03,359 --> 00:05:05,549
square brackets to find the list of

97
00:05:05,549 --> 00:05:09,749
those column names and so I get this new

98
00:05:09,749 --> 00:05:13,439
pandas dataframe called out and it only

99
00:05:13,439 --> 00:05:15,299
has the two columns presumably but say

100
00:05:15,299 --> 00:05:17,579
I was having some problem with this

101
00:05:17,579 --> 00:05:19,139
and I wanted to get a little debug

102
00:05:19,139 --> 00:05:20,879
output I want to have a look into some

103
00:05:20,879 --> 00:05:24,790
result here i can use this

104
00:05:24,790 --> 00:05:27,130
print and i&#39;m just going to print out

105
00:05:27,130 --> 00:05:29,980
.head and so we&#39;ll see where that

106
00:05:29,980 --> 00:05:32,020
goes

107
00:05:32,020 --> 00:05:34,210
and then it&#39;s very important you have to

108
00:05:34,210 --> 00:05:36,759
always as I said I think before you

109
00:05:36,759 --> 00:05:39,729
always have to return a pandas dataframe

110
00:05:39,729 --> 00:05:45,409
from an execute python script

111
00:05:45,409 --> 00:05:48,739
so that&#39;s all there is to that code i&#39;m

112
00:05:48,739 --> 00:05:52,050
going to save my experiment

113
00:05:52,050 --> 00:05:53,520
I&#39;m going to run it and i&#39;ll show you

114
00:05:53,520 --> 00:05:55,849
the result

115
00:05:55,849 --> 00:06:00,379
ok the result is run so first off let&#39;s

116
00:06:00,379 --> 00:06:03,229
look at that printed output and we&#39;ll

117
00:06:03,229 --> 00:06:07,459
visualize this Python sorry this to

118
00:06:07,459 --> 00:06:11,869
python device output - just visualize that

119
00:06:11,869 --> 00:06:14,270
and I scroll down a little bit here

120
00:06:14,270 --> 00:06:17,389
there&#39;s some messages and here&#39;s that

121
00:06:17,389 --> 00:06:20,389
head of that data frame with the height

122
00:06:20,389 --> 00:06:23,569
and the width and some values so exactly

123
00:06:23,569 --> 00:06:25,069
what I expected from that print

124
00:06:25,069 --> 00:06:27,669
statement; and remember I

125
00:06:28,020 --> 00:06:30,810
returned the full data frame that full

126
00:06:30,810 --> 00:06:34,760
data frame i called out and here it is

127
00:06:34,760 --> 00:06:37,670
two columns I&#39;d sub selected

128
00:06:37,670 --> 00:06:40,580
and those are as an Azure ML table so i

129
00:06:40,580 --> 00:06:43,640
could then use those in other modules to

130
00:06:43,640 --> 00:06:46,449
continue building my experiment

131
00:06:46,449 --> 00:06:48,789
so those are just some simple you know

132
00:06:48,789 --> 00:06:51,639
basic things you need to know to take

133
00:06:51,639 --> 00:06:56,229
any ordinary python script and run it in

134
00:06:56,229 --> 00:06:58,990
Azure ML within the execute python

135
00:06:58,990 --> 00:07:01,860
script module.

