0
00:00:01,220 --> 00:00:04,580
Hi and welcome. In this series of lessons

1
00:00:04,580 --> 00:00:05,840
we&#39;re going to talk about one of my

2
00:00:05,840 --> 00:00:08,330
personal favorite topics in data science

3
00:00:08,330 --> 00:00:12,050
which is visualization and exploratory

4
00:00:12,050 --> 00:00:15,019
data analysis that is how do we use

5
00:00:15,019 --> 00:00:18,320
visualization techniques primarily we

6
00:00:18,320 --> 00:00:20,240
get a new data set we don&#39;t know

7
00:00:20,240 --> 00:00:21,919
anything much about it

8
00:00:21,919 --> 00:00:23,689
how do we start understanding the

9
00:00:23,689 --> 00:00:25,999
relationships in that data set trying to

10
00:00:25,999 --> 00:00:27,619
figure out you know what&#39;s up what&#39;s

11
00:00:27,619 --> 00:00:31,429
down what sideways and and you often

12
00:00:31,429 --> 00:00:33,170
wind up spending a lot of time on

13
00:00:33,170 --> 00:00:37,400
visualization and you go back and forth

14
00:00:37,400 --> 00:00:39,980
you&#39;ll feel to some visualization you&#39;ll

15
00:00:39,980 --> 00:00:41,750
try some analysis things don&#39;t quite

16
00:00:41,750 --> 00:00:44,270
work the way you think they should and

17
00:00:44,270 --> 00:00:46,310
you go back and you do some more

18
00:00:46,310 --> 00:00:48,590
in-depth visualization and you realize

19
00:00:48,590 --> 00:00:51,350
hi miss this relationship the first time

20
00:00:51,350 --> 00:00:54,230
and you try to fix your problems etc so

21
00:00:54,230 --> 00:00:56,720
very iterative process there&#39;s no linear

22
00:00:56,720 --> 00:00:59,120
path through really understanding a data

23
00:00:59,120 --> 00:01:01,079
set

24
00:01:01,079 --> 00:01:04,229
so let me just give you an overview of

25
00:01:04,229 --> 00:01:08,460
this prop process we call it exploratory

26
00:01:08,460 --> 00:01:11,189
data analysis and I think I hope as we

27
00:01:11,189 --> 00:01:13,259
go through these lessons you&#39;ll

28
00:01:13,259 --> 00:01:15,720
understand why it&#39;s called exploratory

29
00:01:15,720 --> 00:01:17,159
because we&#39;re really searching for

30
00:01:17,159 --> 00:01:20,759
what&#39;s going on in the data set

31
00:01:20,759 --> 00:01:24,899
we&#39;re going to use either the R ggplot2

32
00:01:24,899 --> 00:01:29,189
package or Python pandas plot and matplotlib

33
00:01:29,189 --> 00:01:33,270
packages for plotting so

34
00:01:33,270 --> 00:01:35,490
there&#39;s two different tracks as with all

35
00:01:35,490 --> 00:01:37,790
our sets of lessons

36
00:01:37,790 --> 00:01:39,830
and let&#39;s talk about what i mean by

37
00:01:39,830 --> 00:01:43,270
exploratory data analysis

38
00:01:43,450 --> 00:01:46,539
so obviously we&#39;re exploring data and

39
00:01:46,539 --> 00:01:48,490
we&#39;re using primarily visualization

40
00:01:48,490 --> 00:01:52,600
techniques as I&#39;ve already said we are

41
00:01:52,600 --> 00:01:54,310
doing that so we understand the

42
00:01:54,310 --> 00:01:55,929
relationships in the data when you have

43
00:01:55,929 --> 00:02:00,700
complex data and there are many

44
00:02:00,700 --> 00:02:03,939
variables that may or may not relate to

45
00:02:03,939 --> 00:02:06,369
each other may not relate to the label

46
00:02:06,369 --> 00:02:08,140
the thing we&#39;re trying to predict with

47
00:02:08,139 --> 00:02:10,780
our same machine learning model you

48
00:02:10,780 --> 00:02:13,319
don&#39;t know

49
00:02:13,970 --> 00:02:17,330
where to start sometimes and you need to

50
00:02:17,330 --> 00:02:19,610
poke around and understand and explore

51
00:02:19,610 --> 00:02:22,430
those relationships we do that by

52
00:02:22,430 --> 00:02:24,590
creating multiple views of the data we

53
00:02:24,590 --> 00:02:26,990
use different chart types and different

54
00:02:26,990 --> 00:02:30,380
variables with on even the same chart

55
00:02:30,380 --> 00:02:33,740
type and we we just try different things

56
00:02:33,740 --> 00:02:38,330
and so kind of the advice is try a lot

57
00:02:38,330 --> 00:02:40,910
of things fail fast

58
00:02:40,910 --> 00:02:43,700
keep going have fun

59
00:02:43,700 --> 00:02:45,590
we&#39;re going to look at in some of these

60
00:02:45,590 --> 00:02:47,870
lessons how do we go beyond two

61
00:02:47,870 --> 00:02:51,850
dimensions in plotting and so

62
00:02:51,850 --> 00:02:55,390
plotting of any visualization is

63
00:02:55,390 --> 00:02:57,370
inherently doesn&#39;t matter whether you&#39;re

64
00:02:57,370 --> 00:02:59,710
drawing it out paper and pencil or you

65
00:02:59,710 --> 00:03:01,540
working on a computer screen it&#39;s a

66
00:03:01,540 --> 00:03:04,240
two-dimensional surface and yet we&#39;re

67
00:03:04,240 --> 00:03:05,890
often dealing with very high-dimensional

68
00:03:05,890 --> 00:03:08,020
data so we can start using something

69
00:03:08,020 --> 00:03:10,480
called plot aesthetics like colors

70
00:03:10,480 --> 00:03:14,380
shapes etc arm to look at more

71
00:03:14,380 --> 00:03:17,860
dimensions and another powerful method

72
00:03:17,860 --> 00:03:21,160
conditioning or faceting where we

73
00:03:21,160 --> 00:03:24,130
grouped data and subdivide the data and

74
00:03:24,130 --> 00:03:27,730
project different plots on a grid of

75
00:03:27,730 --> 00:03:31,270
values of some other variables so we can

76
00:03:31,270 --> 00:03:34,690
get more variables / plotted on our

77
00:03:34,690 --> 00:03:38,790
limit of our 2d data set

78
00:03:40,060 --> 00:03:42,130
and we can do this i&#39;ve talked about

79
00:03:42,130 --> 00:03:45,340
exploratory data analysis but it at the

80
00:03:45,340 --> 00:03:47,680
end when you&#39;re done you can use it to

81
00:03:47,680 --> 00:03:49,959
explore sources a model error in some

82
00:03:49,959 --> 00:03:52,180
cases and also of course good

83
00:03:52,180 --> 00:03:54,370
visualization good graphics are a great

84
00:03:54,370 --> 00:03:55,959
way if you&#39;re a networking data

85
00:03:55,959 --> 00:03:58,690
scientist you want to present results to

86
00:03:58,690 --> 00:04:00,819
your colleagues and your bosses or

87
00:04:00,819 --> 00:04:04,000
whatever having a good visualization is

88
00:04:04,000 --> 00:04:05,890
going to be worth a lot more than trying

89
00:04:05,890 --> 00:04:07,180
to get everybody to look at some

90
00:04:07,180 --> 00:04:12,340
god-awful table of numbers so let me

91
00:04:12,340 --> 00:04:13,510
just talk a little bit about the history

92
00:04:13,510 --> 00:04:15,970
of exploratory data analysis where this

93
00:04:15,970 --> 00:04:17,829
term come from

94
00:04:17,829 --> 00:04:20,889
well it was coined in a seminal book by

95
00:04:20,889 --> 00:04:24,400
john w two key who is a professor at

96
00:04:24,400 --> 00:04:26,560
Princeton he was actually I&#39;m still

97
00:04:26,560 --> 00:04:28,270
around when I was a graduate student I

98
00:04:28,270 --> 00:04:31,720
was very lucky I felt to know him and in

99
00:04:31,720 --> 00:04:35,290
1977 based on already almost two decades

100
00:04:35,290 --> 00:04:38,080
of research he had done he published a

101
00:04:38,080 --> 00:04:39,639
book and the title of the book was

102
00:04:39,639 --> 00:04:43,030
exploratory data analysis and that

103
00:04:43,030 --> 00:04:45,010
really opened the eyes of people in the

104
00:04:45,010 --> 00:04:47,169
data analysis and statistics world to

105
00:04:47,169 --> 00:04:49,810
the idea that it isn&#39;t just about math

106
00:04:49,810 --> 00:04:52,249
isn&#39;t just about numbers

107
00:04:52,249 --> 00:04:54,289
there&#39;s a whole visual component where

108
00:04:54,289 --> 00:04:56,149
you can learn a lot by using

109
00:04:56,149 --> 00:04:59,869
visualization the other seminal work

110
00:04:59,869 --> 00:05:03,289
we&#39;re going to rely on heavily in

111
00:05:03,289 --> 00:05:07,030
these lessons was

112
00:05:07,030 --> 00:05:09,460
work by Bill cleveland in his book again

113
00:05:09,460 --> 00:05:11,440
based on more than 20 years of his

114
00:05:11,440 --> 00:05:13,990
research at AT&amp;T Bell Labs back in the

115
00:05:13,990 --> 00:05:16,300
days when that existed he published this

116
00:05:16,300 --> 00:05:19,000
book unique yeah simply called

117
00:05:19,000 --> 00:05:21,250
visualizing data but what Cleveland was

118
00:05:21,250 --> 00:05:22,930
really looking at was different ways

119
00:05:22,930 --> 00:05:24,640
like using plot aesthetics using

120
00:05:24,640 --> 00:05:27,190
conditioning or fascinating that we can

121
00:05:27,190 --> 00:05:30,490
overcome this limitation i was talking

122
00:05:30,490 --> 00:05:33,610
about of the two-dimensional surface how

123
00:05:33,610 --> 00:05:37,790
do we look at more dimensions at once

124
00:05:37,790 --> 00:05:39,980
and I mentioned this business of

125
00:05:39,980 --> 00:05:41,900
multiple views and I&#39;ve talked about it

126
00:05:41,900 --> 00:05:45,260
but why do I keep your dwelling on it

127
00:05:45,260 --> 00:05:47,300
because there&#39;s never a simple

128
00:05:47,300 --> 00:05:49,640
relationships in real-world data it&#39;s

129
00:05:49,640 --> 00:05:52,160
always complex it&#39;s always multifaceted

130
00:05:52,160 --> 00:05:55,250
and you have to look at those multiple

131
00:05:55,250 --> 00:05:56,840
views or you&#39;re just not going to know

132
00:05:56,840 --> 00:06:00,080
you know what&#39;s important what relates

133
00:06:00,080 --> 00:06:02,690
to what what&#39;s sometimes variables are

134
00:06:02,690 --> 00:06:04,310
simply redundant with each other and you

135
00:06:04,310 --> 00:06:07,710
can see that very clearly and apply

136
00:06:07,710 --> 00:06:10,290
so that&#39;s it i hope gets you some

137
00:06:10,290 --> 00:06:12,510
insight into why this important i hope

138
00:06:12,510 --> 00:06:15,150
you&#39;re excited about this and and and

139
00:06:15,150 --> 00:06:17,400
when you work on doing visualizations on

140
00:06:17,400 --> 00:06:18,720
your own i hope you have fun with it

141
00:06:18,720 --> 00:06:21,630
it&#39;s a bit of a chase it&#39;s a bit of an

142
00:06:21,630 --> 00:06:23,730
open-ended process but there&#39;s a lot of

143
00:06:23,730 --> 00:06:28,250
cool stuff you can learn by doing

