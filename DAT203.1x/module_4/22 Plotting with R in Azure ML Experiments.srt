0
00:00:02,420 --> 00:00:04,400
So we&#39;ve looked at a number of ways to

1
00:00:04,400 --> 00:00:07,310
create very informative charts using R

2
00:00:07,310 --> 00:00:09,590
in this short sequence I&#39;d like to show

3
00:00:09,590 --> 00:00:12,800
you how to create those same plots with

4
00:00:12,800 --> 00:00:15,769
R in Azure machine learning, which

5
00:00:15,769 --> 00:00:17,840
is a very useful thing to do when you&#39;re

6
00:00:17,840 --> 00:00:19,699
working with data and Azure machine

7
00:00:19,699 --> 00:00:21,380
learning

8
00:00:21,380 --> 00:00:25,339
so on my screen here I have some simple

9
00:00:25,339 --> 00:00:28,669
R code that&#39;s going to do just that

10
00:00:28,669 --> 00:00:31,849
so as usual in Azure machine learning I

11
00:00:31,849 --> 00:00:34,489
get my data frame in this case from

12
00:00:34,489 --> 00:00:39,440
input port one I do some cleanup

13
00:00:39,440 --> 00:00:43,489
on that data which is not different than

14
00:00:43,489 --> 00:00:44,839
what we&#39;ve done before it&#39;s just the

15
00:00:44,839 --> 00:00:47,750
same and then again there&#39;s not much i&#39;m

16
00:00:47,750 --> 00:00:52,610
doing very different here so I make sure

17
00:00:52,610 --> 00:00:57,229
ggplot2 is loaded the library I create a

18
00:00:57,229 --> 00:01:00,559
title you know just a string I use

19
00:01:00,559 --> 00:01:03,619
ggplot the name of that data frame that

20
00:01:03,619 --> 00:01:06,110
i&#39;ve loaded and cleaned up and i&#39;m going

21
00:01:06,110 --> 00:01:09,229
to plot on the x-axis length the y-axis

22
00:01:09,229 --> 00:01:11,930
price i&#39;m going to make a point plot and

23
00:01:11,930 --> 00:01:14,060
i&#39;m going to make the color in this case

24
00:01:14,060 --> 00:01:17,540
a factor of aspiration so turbo or

25
00:01:17,540 --> 00:01:21,620
standard and i&#39;ll add that title

26
00:01:21,620 --> 00:01:23,630
so let me i&#39;m just going to cut and

27
00:01:23,630 --> 00:01:27,320
paste all that code I&#39;m going to come

28
00:01:27,320 --> 00:01:29,330
over here and I&#39;ve started an experiment

29
00:01:29,330 --> 00:01:30,950
in Azure machine learning where I have

30
00:01:30,950 --> 00:01:34,250
that automobile price data so let me

31
00:01:34,250 --> 00:01:38,060
find the execute R script here I&#39;m

32
00:01:38,060 --> 00:01:39,590
just going to drag it onto the canvas

33
00:01:39,590 --> 00:01:44,470
and connect that to my port 1

34
00:01:45,270 --> 00:01:47,640
click here and i can pick my R version

35
00:01:47,640 --> 00:01:49,710
i&#39;m just going to stick with the CRAN

36
00:01:49,710 --> 00:01:52,320
I don&#39;t really need a random seed for

37
00:01:52,320 --> 00:01:55,259
this code and then I&#39;m just going to

38
00:01:55,259 --> 00:01:59,909
replace all that example code with the

39
00:01:59,909 --> 00:02:02,680
code i just showed you

40
00:02:02,680 --> 00:02:04,930
and I&#39;ll just save that for good measure

41
00:02:04,930 --> 00:02:10,920
and i&#39;m going to run my experiment.

42
00:02:15,770 --> 00:02:17,090
There  my experiment has run

43
00:02:17,090 --> 00:02:18,710
successfully as i can tell from the

44
00:02:18,710 --> 00:02:22,340
green check marks and I go to this port

45
00:02:22,340 --> 00:02:24,530
here this port is called the R device

46
00:02:24,530 --> 00:02:26,840
port so that&#39;s where my printed and

47
00:02:26,840 --> 00:02:31,220
plotted output appears and I click on it

48
00:02:31,220 --> 00:02:34,900
and I go to visualize

49
00:02:35,330 --> 00:02:37,100
and there I have my plot so I have the

50
00:02:37,100 --> 00:02:40,250
length of my cars on the horizontal axis

51
00:02:40,250 --> 00:02:43,280
the price on the vertical you can see my

52
00:02:43,280 --> 00:02:45,710
two colors here of standard and turbo

53
00:02:45,710 --> 00:02:49,310
aspiration so it looks like longer cars

54
00:02:49,310 --> 00:02:52,040
generally are more expensive and the

55
00:02:52,040 --> 00:02:56,210
most expensive long length cars seem to

56
00:02:56,210 --> 00:02:58,880
mostly be turbo there&#39;s a few standard

57
00:02:58,880 --> 00:03:01,960
cars up there too

58
00:03:02,730 --> 00:03:04,620
so that&#39;s all there is to it it&#39;s very

59
00:03:04,620 --> 00:03:08,010
easy to create really informative useful

60
00:03:08,010 --> 00:03:10,409
plots using R in Azure machine

61
00:03:10,409 --> 00:03:12,739
learning

