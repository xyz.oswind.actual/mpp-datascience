0
00:00:01,050 --> 00:00:04,170
so we&#39;ve just looked at ways to project

1
00:00:04,170 --> 00:00:07,830
multiple dimensions of a data set onto

2
00:00:07,830 --> 00:00:11,370
our two-dimensional plot surface using

3
00:00:11,370 --> 00:00:14,400
aesthetics, let&#39;s look at some other ways to

4
00:00:14,400 --> 00:00:15,240
do that we&#39;re going to look at

5
00:00:15,240 --> 00:00:17,249
scatterplot matrices and something

6
00:00:17,249 --> 00:00:18,960
another very powerful method called

7
00:00:18,960 --> 00:00:21,679
faceting

8
00:00:22,300 --> 00:00:24,100
so

9
00:00:24,100 --> 00:00:28,000
pairwise scatter plots are a way to

10
00:00:28,000 --> 00:00:30,420
put

11
00:00:30,420 --> 00:00:33,120
a number of variables side by side by

12
00:00:33,120 --> 00:00:36,060
side and look at their

13
00:00:36,060 --> 00:00:39,540
relationships and there&#39;s a plot

14
00:00:39,540 --> 00:00:42,079
formula here

15
00:00:42,079 --> 00:00:44,239
and so in R you&#39;ll see these formulas

16
00:00:44,239 --> 00:00:46,820
quite a bit but in this case the formula

17
00:00:46,820 --> 00:00:50,960
of which variables you want to use in

18
00:00:50,960 --> 00:00:52,760
your scatterplot matrix is just this

19
00:00:52,760 --> 00:00:57,289
this squiggly line and then a list

20
00:00:57,289 --> 00:00:59,570
separated by plus signs of whatever

21
00:00:59,570 --> 00:01:01,369
variables you want to project into the

22
00:01:01,369 --> 00:01:04,030
scatterplot matrix

23
00:01:04,030 --> 00:01:06,700
and you can do some other things so

24
00:01:06,700 --> 00:01:09,310
we&#39;re going to go set our width and

25
00:01:09,310 --> 00:01:11,979
height i&#39;m going to use the scatterplot

26
00:01:11,979 --> 00:01:15,850
matrix function from the car package

27
00:01:15,850 --> 00:01:18,729
because it adds some nice attributes to

28
00:01:18,729 --> 00:01:19,990
the plot which you&#39;ll see in a minute

29
00:01:19,990 --> 00:01:21,940
and we&#39;re going to look at wheelbase

30
00:01:21,940 --> 00:01:24,820
curb weight engine size horsepower city

31
00:01:24,820 --> 00:01:27,070
miles per gallon price so we&#39;ve got the

32
00:01:27,070 --> 00:01:30,250
six numeric variables and we&#39;re going to

33
00:01:30,250 --> 00:01:34,440
create our matrix from that

34
00:01:37,390 --> 00:01:41,320
so when you look at this first you see

35
00:01:41,320 --> 00:01:45,159
on the diagonal we have wheelbase curb

36
00:01:45,159 --> 00:01:48,340
weights engine size except those are six

37
00:01:48,340 --> 00:01:52,119
variables that we specified with our

38
00:01:52,119 --> 00:01:54,970
formula in each one has a kernel density

39
00:01:54,970 --> 00:01:58,030
estimation plot associated with it and

40
00:01:58,030 --> 00:02:00,280
so right away you can look just down the

41
00:02:00,280 --> 00:02:02,590
diagonal of the scatterplot matrix and

42
00:02:02,590 --> 00:02:06,909
see a wheelbase is kind of clumped at

43
00:02:06,909 --> 00:02:09,100
the low end with this long tail so

44
00:02:09,100 --> 00:02:13,600
that&#39;s telling us that you know that a

45
00:02:13,600 --> 00:02:16,120
large number of cars are fairly short

46
00:02:16,120 --> 00:02:21,730
and only a few cars are fairly long curb

47
00:02:21,730 --> 00:02:24,130
weight again has seemed to have two

48
00:02:24,130 --> 00:02:26,560
humps but again toward the low end

49
00:02:26,560 --> 00:02:29,050
engine size is very much skewed toward

50
00:02:29,050 --> 00:02:31,330
the low end look at this it goes to our

51
00:02:31,330 --> 00:02:34,600
modem really low and then there&#39;s just

52
00:02:34,600 --> 00:02:38,380
this tale of large engine cars and that

53
00:02:38,380 --> 00:02:40,600
looks very similar to horsepower not too

54
00:02:40,600 --> 00:02:44,019
surprisingly city miles per gallon is a

55
00:02:44,019 --> 00:02:46,360
little more symmetric but also does have

56
00:02:46,360 --> 00:02:50,580
this tale toward the the high end

57
00:02:50,580 --> 00:02:52,080
and price we&#39;ve looked at that

58
00:02:52,080 --> 00:02:55,530
extensively before so then we also have

59
00:02:55,530 --> 00:02:58,320
all possible pairwise combinations of

60
00:02:58,320 --> 00:03:01,470
like wheelbase and curb weight so in

61
00:03:01,470 --> 00:03:03,990
both projections so in this case we have

62
00:03:03,990 --> 00:03:07,410
wheelbase on the horizontal axis curb

63
00:03:07,410 --> 00:03:09,150
weight on the vertical axis or if you

64
00:03:09,150 --> 00:03:11,070
prefer curb weight on the horizontal

65
00:03:11,070 --> 00:03:12,810
axis and wheelbase on the vertical axis

66
00:03:12,810 --> 00:03:16,560
etc. You see we&#39;ve also got these regression

67
00:03:16,560 --> 00:03:19,140
lines here there&#39;s a green line it&#39;s a

68
00:03:19,140 --> 00:03:21,030
little hard to see in some cases but

69
00:03:21,030 --> 00:03:25,260
that&#39;s the linear regression line and

70
00:03:25,260 --> 00:03:27,370
then this

71
00:03:27,370 --> 00:03:31,870
red line and it&#39;s error bars the

72
00:03:31,870 --> 00:03:34,659
confidence intervals or error bars is a

73
00:03:34,659 --> 00:03:37,750
nonlinear regression of those data so

74
00:03:37,750 --> 00:03:40,510
some things like like wheelbase vs curb

75
00:03:40,510 --> 00:03:42,970
weight seems to have a pretty straight

76
00:03:42,970 --> 00:03:44,379
line relationship that&#39;s not too

77
00:03:44,379 --> 00:03:47,860
surprising longer cars are heavier etc

78
00:03:47,860 --> 00:03:50,049
and you see a similar linear

79
00:03:50,049 --> 00:03:52,090
relationship between engine size and

80
00:03:52,090 --> 00:03:55,510
horsepower but you get into things like

81
00:03:55,510 --> 00:03:58,450
this like horsepower versus City miles

82
00:03:58,450 --> 00:04:00,129
per gallon which again you can look at

83
00:04:00,129 --> 00:04:03,069
either orientation of that plot there&#39;s

84
00:04:03,069 --> 00:04:06,620
definitely a curved relationship that

85
00:04:06,620 --> 00:04:10,040
low miles per gallon cars do have the

86
00:04:10,040 --> 00:04:11,900
highest horsepower but it&#39;s not a simple

87
00:04:11,900 --> 00:04:14,540
linear relationship

88
00:04:14,540 --> 00:04:16,970
and similar here&#39;s the relationship we

89
00:04:16,970 --> 00:04:18,620
already saw with miles per gallon and

90
00:04:18,620 --> 00:04:21,259
price and you can keep studying this

91
00:04:21,259 --> 00:04:22,699
plot you can see there&#39;s a lot of

92
00:04:22,699 --> 00:04:24,380
information on this plot some

93
00:04:24,380 --> 00:04:27,139
relationships are fairly clear like

94
00:04:27,139 --> 00:04:29,300
horsepower versus City miles per gallon

95
00:04:29,300 --> 00:04:33,210
and others

96
00:04:33,210 --> 00:04:34,650
like

97
00:04:34,650 --> 00:04:37,380
curb weight versus price are a little

98
00:04:37,380 --> 00:04:39,600
more complicated we seem to have a group

99
00:04:39,600 --> 00:04:41,250
going one way and a group going the

100
00:04:41,250 --> 00:04:44,430
other and and maybe wheelbase vs prices

101
00:04:44,430 --> 00:04:47,210
similar thing

102
00:04:47,310 --> 00:04:49,050
where it&#39;s just kinda all over the place

103
00:04:49,050 --> 00:04:51,780
so maybe so tells you which variables

104
00:04:51,780 --> 00:04:53,400
have a strong relationship in which

105
00:04:53,400 --> 00:04:56,520
don&#39;t and notice i limited how many

106
00:04:56,520 --> 00:04:59,310
dimensions i put you-- you can overwhelm

107
00:04:59,310 --> 00:05:02,520
yourself by putting too many variables

108
00:05:02,520 --> 00:05:04,950
on a scatterplot matrix you better off

109
00:05:04,950 --> 00:05:07,170
doing a series of them with a limited

110
00:05:07,170 --> 00:05:09,090
number of variables than trying to cram

111
00:05:09,090 --> 00:05:11,610
everything into one and in bruising your

112
00:05:11,610 --> 00:05:15,120
brain so basically this is a

113
00:05:15,120 --> 00:05:18,270
six-dimensional plot here we&#39;ve got six

114
00:05:18,270 --> 00:05:20,820
variables that we&#39;ve plotted on our 2d

115
00:05:20,820 --> 00:05:22,410
surface but there&#39;s another way to do

116
00:05:22,410 --> 00:05:25,170
this which is what we call facet plots

117
00:05:25,170 --> 00:05:27,240
and we&#39;ve already looked at an idea

118
00:05:27,240 --> 00:05:30,330
behind this with like the box plots and

119
00:05:30,330 --> 00:05:32,790
the violin plots where we had a variable

120
00:05:32,790 --> 00:05:35,190
a group by variable that we condition

121
00:05:35,190 --> 00:05:38,940
those plots by and so fast plots extend

122
00:05:38,940 --> 00:05:43,530
that idea to two-dimensional grids in an

123
00:05:43,530 --> 00:05:46,290
are we model we use again that modeling

124
00:05:46,290 --> 00:05:49,620
language we have the role variables with

125
00:05:49,620 --> 00:05:51,660
the squiggly line by the column variable

126
00:05:51,660 --> 00:05:55,800
so so it tells you you can have one or

127
00:05:55,800 --> 00:05:57,750
more row variables one or more common

128
00:05:57,750 --> 00:06:00,030
column variables if you just want to

129
00:06:00,030 --> 00:06:05,820
make a row - several rows so stacked

130
00:06:05,820 --> 00:06:08,850
plots one on top of the other you can go

131
00:06:08,850 --> 00:06:12,810
row variables model to dot see there&#39;s a

132
00:06:12,810 --> 00:06:15,060
dot there or you can do the columns

133
00:06:15,060 --> 00:06:17,340
where you just have your plots

134
00:06:17,340 --> 00:06:19,590
on a grid with just columns in one row

135
00:06:19,590 --> 00:06:23,190
again its dot squiggly line column

136
00:06:23,190 --> 00:06:25,470
variables and you can do multiple you

137
00:06:25,470 --> 00:06:27,570
can do row variable 1, row variable 2,

138
00:06:27,570 --> 00:06:31,230
etc. by column variable 1,

139
00:06:31,230 --> 00:06:33,270
column variable 2, although I&#39;ll warn

140
00:06:33,270 --> 00:06:36,030
you don&#39;t go too far with this sometimes

141
00:06:36,030 --> 00:06:39,030
you wind up with plots that are really

142
00:06:39,030 --> 00:06:40,740
hard for you and especially your

143
00:06:40,740 --> 00:06:42,810
audience to understand you also can

144
00:06:42,810 --> 00:06:44,940
overdo this to the point where you wind

145
00:06:44,940 --> 00:06:47,970
up with a lot of your plots on your grid

146
00:06:47,970 --> 00:06:49,950
which have no data in them and that&#39;s

147
00:06:49,950 --> 00:06:51,960
not very useful

148
00:06:51,960 --> 00:06:55,259
so let&#39;s look at a specific example here

149
00:06:55,259 --> 00:06:57,150
we&#39;re going to look at some

150
00:06:57,150 --> 00:07:02,069
histograms so and i&#39;m i&#39;m setting a bin

151
00:07:02,069 --> 00:07:06,780
width to 30 and again this is the same

152
00:07:06,780 --> 00:07:08,880
as we did for any other histogram we do

153
00:07:08,880 --> 00:07:12,360
auto.price aes price because we&#39;re

154
00:07:12,360 --> 00:07:13,470
just gonna look at the histogram of

155
00:07:13,470 --> 00:07:17,190
price but in my end in my geom_histogram

156
00:07:17,190 --> 00:07:19,259
I&#39;m gonna set that bin width but then i

157
00:07:19,259 --> 00:07:21,780
have this new attribute of the plot

158
00:07:21,780 --> 00:07:24,300
called facet_grid and i&#39;m going

159
00:07:24,300 --> 00:07:26,880
to do .drive-wheels so I&#39;m just going

160
00:07:26,880 --> 00:07:30,330
to put by the drive wheel type of

161
00:07:30,330 --> 00:07:34,110
the car as the columns of the grid and

162
00:07:34,110 --> 00:07:35,880
I&#39;m just gonna have one row with my grid

163
00:07:35,880 --> 00:07:37,800
at this point so let&#39;s go ahead and run

164
00:07:37,800 --> 00:07:39,080
that

165
00:07:39,080 --> 00:07:42,710
and you see i have a histogram

166
00:07:42,710 --> 00:07:45,020
separated or grouped by four wheel drive

167
00:07:45,020 --> 00:07:46,819
cars, front-wheel-drive cars, rear-wheel

168
00:07:46,819 --> 00:07:49,129
drive cars and we can learn a few things

169
00:07:49,129 --> 00:07:52,280
right away just from this plot so

170
00:07:52,280 --> 00:07:54,650
there&#39;s not very many four wheel drive

171
00:07:54,650 --> 00:07:57,259
cars they tend to be toward the low end

172
00:07:57,259 --> 00:07:59,160
of price

173
00:07:59,160 --> 00:08:01,110
my front wheel drive cars seem quite

174
00:08:01,110 --> 00:08:03,480
numerous notice the vertical scale in

175
00:08:03,480 --> 00:08:05,280
the horizontal scales by default are the

176
00:08:05,280 --> 00:08:07,440
same on these so you can directly

177
00:08:07,440 --> 00:08:10,920
compare these facet grid plots and you

178
00:08:10,920 --> 00:08:13,140
see a lot of our front-wheel-drive cars

179
00:08:13,140 --> 00:08:15,180
are clustered at this low end of the

180
00:08:15,180 --> 00:08:17,880
price so that&#39;s interesting and our rear

181
00:08:17,880 --> 00:08:20,070
wheel drive cars are kind of spread out

182
00:08:20,070 --> 00:08:23,070
all over and all the high-price cars

183
00:08:23,070 --> 00:08:25,920
tend to be rear-wheel-drive cars so

184
00:08:25,920 --> 00:08:27,990
we&#39;ve just by using this one simple

185
00:08:27,990 --> 00:08:30,060
facet variable drive wheels

186
00:08:30,060 --> 00:08:32,280
we&#39;ve actually learned quite a bit new

187
00:08:32,280 --> 00:08:36,890
about the price of automobiles

188
00:08:37,740 --> 00:08:39,870
now I can extend this to a

189
00:08:39,870 --> 00:08:43,380
two-dimensional grid so this is the same

190
00:08:43,380 --> 00:08:45,990
plot i just did except i&#39;m changing the

191
00:08:45,990 --> 00:08:49,050
grid variables i&#39;m going to do fuel type

192
00:08:49,050 --> 00:08:53,660
by aspiration ok

193
00:08:53,660 --> 00:08:55,970
let me run that for you

194
00:08:55,970 --> 00:08:59,329
and now i have a two-by-two grid and I

195
00:08:59,329 --> 00:09:03,860
have standard diesel cars turbo diesel

196
00:09:03,860 --> 00:09:06,560
cars you see these little what we call

197
00:09:06,560 --> 00:09:10,399
tiles tell me how to read what&#39;s in the

198
00:09:10,399 --> 00:09:12,920
rows and I have standard gas cars and

199
00:09:12,920 --> 00:09:16,490
turbo gas cars so not too many diesel

200
00:09:16,490 --> 00:09:19,220
cars overall and my standard diesel cars

201
00:09:19,220 --> 00:09:22,790
tend to be pretty cheap my turbo diesel

202
00:09:22,790 --> 00:09:27,050
cars are spread out over a wide range my

203
00:09:27,050 --> 00:09:29,360
turbo gas cars tend to be in this kind

204
00:09:29,360 --> 00:09:32,329
of mid-range toward the low and i have a

205
00:09:32,329 --> 00:09:37,670
lot of standard gas cars

206
00:09:37,670 --> 00:09:40,250
so again we have this big bunch of

207
00:09:40,250 --> 00:09:44,420
low-cost cars that are the standard gas

208
00:09:44,420 --> 00:09:47,480
cars but also it looks like all my

209
00:09:47,480 --> 00:09:49,790
really high priced cars there&#39;s only one

210
00:09:49,790 --> 00:09:53,270
other car which is a turbo diesel over

211
00:09:53,270 --> 00:09:55,850
thirty thousand dollars all the others

212
00:09:55,850 --> 00:09:58,640
are standard gas and the bulk of these

213
00:09:58,640 --> 00:10:02,060
low-cost cars are also standard gas so

214
00:10:02,060 --> 00:10:04,220
why the simple two-by-two grid we began

215
00:10:04,220 --> 00:10:07,990
learned quite a bit about auto pricing

216
00:10:08,019 --> 00:10:10,269
let&#39;s try a scatterplot so this is the

217
00:10:10,269 --> 00:10:12,100
same scatterplot we were looking at

218
00:10:12,100 --> 00:10:15,850
before where we do City miles per gallon

219
00:10:15,850 --> 00:10:19,329
on the horizontal axis price on the

220
00:10:19,329 --> 00:10:21,939
vertical or use the same aesthetics of

221
00:10:21,939 --> 00:10:24,050
color

222
00:10:24,050 --> 00:10:28,760
buy fuel type size of the engine squared

223
00:10:28,760 --> 00:10:31,610
is the size shape is the aspiration

224
00:10:31,610 --> 00:10:34,499
whether its standard or turbo

225
00:10:34,499 --> 00:10:35,969
but then I&#39;m going to add this facet

226
00:10:35,969 --> 00:10:39,269
grid i&#39;m going to do body style by drive

227
00:10:39,269 --> 00:10:40,199
wheels

228
00:10:40,199 --> 00:10:42,299
ok so body style on the vertical axis

229
00:10:42,299 --> 00:10:46,370
drive wheels on the horizontal axis

230
00:10:46,370 --> 00:10:48,320
we&#39;re going to grind up with a grid of

231
00:10:48,320 --> 00:10:51,670
12 plots here

232
00:10:57,649 --> 00:11:01,610
and excuse me not 12 plots more than

233
00:11:01,610 --> 00:11:04,339
12 and you can see that some of these

234
00:11:04,339 --> 00:11:06,350
this is kind of what I was warning you

235
00:11:06,350 --> 00:11:10,100
some of these grids are plots on the

236
00:11:10,100 --> 00:11:11,600
grid aren&#39;t that useful

237
00:11:11,600 --> 00:11:13,910
there&#39;s no four-wheel drive convertibles

238
00:11:13,910 --> 00:11:16,699
no four-wheel drive hardtops only one

239
00:11:16,699 --> 00:11:19,279
four-wheel-drive hatchback a few

240
00:11:19,279 --> 00:11:21,230
four-wheel-drive sedans and a few

241
00:11:21,230 --> 00:11:23,809
four-wheel-drive wagon so that you know

242
00:11:23,809 --> 00:11:26,600
is that useful or not it&#39;s not clear but

243
00:11:26,600 --> 00:11:29,230
we look at some of these other

244
00:11:29,230 --> 00:11:31,690
combinations here especially comparing

245
00:11:31,690 --> 00:11:33,070
front-wheel drive to rear-wheel-drive

246
00:11:33,070 --> 00:11:37,980
you know some very

247
00:11:38,790 --> 00:11:41,970
expensive convertibles here at low fuel

248
00:11:41,970 --> 00:11:44,220
efficiency and looks like they have the

249
00:11:44,220 --> 00:11:47,190
big engines etc the hardtop so now we

250
00:11:47,190 --> 00:11:50,130
can see that this really big engine high

251
00:11:50,130 --> 00:11:52,860
priced low efficiency car is considered

252
00:11:52,860 --> 00:11:56,490
a hardtop if we look at hatchbacks which

253
00:11:56,490 --> 00:11:59,760
were recall fairly numerous the

254
00:11:59,760 --> 00:12:01,590
rear-wheel-drive hatchbacks have a

255
00:12:01,590 --> 00:12:03,930
distinctly different behavior than the

256
00:12:03,930 --> 00:12:05,940
front wheel drive hatchback so those very

257
00:12:05,940 --> 00:12:08,790
fuel-efficient cars and a lot of our low

258
00:12:08,790 --> 00:12:11,580
price cars in the in the hatchback

259
00:12:11,580 --> 00:12:13,790
category

260
00:12:13,790 --> 00:12:16,940
have front-wheel drive and really

261
00:12:16,940 --> 00:12:19,100
generally more expensive for the fuel

262
00:12:19,100 --> 00:12:22,400
efficiency hatchbacks in the

263
00:12:22,400 --> 00:12:24,170
rear-wheel-drive category in a similar

264
00:12:24,170 --> 00:12:26,390
relationship for the sedan you can see

265
00:12:26,390 --> 00:12:29,780
front-wheel-drive sedans tend to stick

266
00:12:29,780 --> 00:12:32,720
to the low-end a price here and and now

267
00:12:32,720 --> 00:12:35,240
we see some of those really big engine

268
00:12:35,240 --> 00:12:39,080
low fuel efficiency expensive cars that

269
00:12:39,080 --> 00:12:41,780
are gasoline engines and and standard

270
00:12:41,780 --> 00:12:44,450
aspiration are all rear-wheel-drive

271
00:12:44,450 --> 00:12:47,500
sedans

272
00:12:48,720 --> 00:12:54,060
so using these combinations of scatter

273
00:12:54,060 --> 00:12:56,819
plot matrices and fascinating so we

274
00:12:56,819 --> 00:12:59,009
create a grid of plots we&#39;ve been able

275
00:12:59,009 --> 00:13:02,160
to expand the number of dimensions by a

276
00:13:02,160 --> 00:13:04,769
couple in this case we went up to seven

277
00:13:04,769 --> 00:13:06,990
dimensions with that last plot i showed

278
00:13:06,990 --> 00:13:08,550
you and we&#39;ve learned a lot of new

279
00:13:08,550 --> 00:13:13,220
things about the pricing of automobiles

