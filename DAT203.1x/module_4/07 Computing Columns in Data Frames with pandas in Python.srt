0
00:00:00,000 --> 00:00:03,899
So I&#39;ve talked about various

1
00:00:03,899 --> 00:00:06,629
manipulations we can do on the rows and

2
00:00:06,629 --> 00:00:09,809
columns of pandas dataframe let&#39;s talk

3
00:00:09,809 --> 00:00:11,580
about something we&#39;re a little more

4
00:00:11,580 --> 00:00:13,740
powerful where we can actually do some

5
00:00:13,740 --> 00:00:17,279
computing on the values in a pandas

6
00:00:17,279 --> 00:00:18,930
dataframe which is a very common thing

7
00:00:18,930 --> 00:00:20,460
you&#39;re going to need to do is a data

8
00:00:20,460 --> 00:00:22,320
scientist

9
00:00:22,320 --> 00:00:25,470
so my screen here I have some examples

10
00:00:25,470 --> 00:00:28,830
of what we&#39;re going to compute here in

11
00:00:28,830 --> 00:00:32,940
this example is what is the curb weight

12
00:00:32,940 --> 00:00:35,309
of the auto in kilograms so currently

13
00:00:35,309 --> 00:00:37,770
the curb weight in the data frame we

14
00:00:37,770 --> 00:00:41,370
load it is has the weight in pounds and

15
00:00:41,370 --> 00:00:44,950
so we can use the divide method

16
00:00:44,950 --> 00:00:47,890
to divide by the number of kilograms in

17
00:00:47,890 --> 00:00:50,110
a pound and we&#39;re going to call that

18
00:00:50,110 --> 00:00:53,110
notice we&#39;re creating a new column of

19
00:00:53,110 --> 00:00:56,230
the data frame using the loc method here

20
00:00:56,230 --> 00:00:59,710
curb weight in kilograms now

21
00:00:59,710 --> 00:01:02,500
the reason I&#39;m using loc is I&#39;m

22
00:01:02,500 --> 00:01:05,740
trying not to create a copy of the df_2

23
00:01:05,740 --> 00:01:10,090
data frame as you see

24
00:01:10,090 --> 00:01:12,250
there&#39;s some issues with this new

25
00:01:12,250 --> 00:01:15,790
release of pandas and sometimes

26
00:01:15,790 --> 00:01:17,890
we&#39;ll see the warning about that

27
00:01:17,890 --> 00:01:20,259
regardless of whether you&#39;re doing it

28
00:01:20,259 --> 00:01:22,680
right or not

29
00:01:23,700 --> 00:01:28,710
so there&#39;s the warning but you can see

30
00:01:28,710 --> 00:01:32,909
that here&#39;s my original curb weight

31
00:01:32,909 --> 00:01:36,840
which is in pounds and here is my new

32
00:01:36,840 --> 00:01:40,350
column curb weight in kilograms so I&#39;ve

33
00:01:40,350 --> 00:01:41,609
applied this

34
00:01:41,609 --> 00:01:44,030
oops sorry

35
00:01:44,030 --> 00:01:48,909
applied this divide operator here too

36
00:01:48,970 --> 00:01:53,520
get the weight of the cars in kilograms

37
00:01:53,980 --> 00:01:57,630
what if i want to do something

38
00:01:57,850 --> 00:02:00,189
on a series of columns so I&#39;ve got the

39
00:02:00,189 --> 00:02:02,650
some of these numeric columns

40
00:02:02,650 --> 00:02:04,990
wheelbase, curb weight, horsepower, price,

41
00:02:04,990 --> 00:02:06,940
and curb weight in kilograms. I want to

42
00:02:06,940 --> 00:02:11,170
take the log of all those columns

43
00:02:11,170 --> 00:02:14,739
ok so again i am going to follow the

44
00:02:14,739 --> 00:02:17,380
same basic recipe i just showed you

45
00:02:17,380 --> 00:02:20,200
we&#39;ve got the data frame name the loc

46
00:02:20,200 --> 00:02:23,080
method the location method and of course

47
00:02:23,080 --> 00:02:26,080
we want all rows so that&#39;s why

48
00:02:26,080 --> 00:02:28,270
there&#39;s just a colon comma there then

49
00:02:28,270 --> 00:02:32,800
this list of columns and again the name

50
00:02:32,800 --> 00:02:34,480
of the data frame and the same list of

51
00:02:34,480 --> 00:02:37,209
columns from subsetting for that and

52
00:02:37,209 --> 00:02:40,060
then i use this applymap method.

53
00:02:40,060 --> 00:02:44,590
applymap is going to apply whatever function

54
00:02:44,590 --> 00:02:48,640
i have here element wise to each column

55
00:02:48,640 --> 00:02:52,299
so each value in each column gets the

56
00:02:52,299 --> 00:02:54,790
natural log function from the math

57
00:02:54,790 --> 00:02:56,380
library applied

58
00:02:56,380 --> 00:03:00,000
ok so let&#39;s have a look at that

59
00:03:00,610 --> 00:03:03,230
alright so

60
00:03:03,230 --> 00:03:04,940
if you look at the values we had for

61
00:03:04,940 --> 00:03:07,190
like wheelbase, curb weight, horsepower,

62
00:03:07,190 --> 00:03:09,409
price, curb weight, you can see that

63
00:03:09,409 --> 00:03:11,959
indeed we still have the drive wheels

64
00:03:11,959 --> 00:03:14,120
and we do indeed seem to have the

65
00:03:14,120 --> 00:03:17,450
natural logarithm in those columns and

66
00:03:17,450 --> 00:03:19,400
notice there&#39;s no copy worn this time

67
00:03:19,400 --> 00:03:21,530
because it&#39;s the second time we&#39;ve used

68
00:03:21,530 --> 00:03:26,590
the loc method so strangely enough.

69
00:03:27,010 --> 00:03:28,870
There are several of these methods that

70
00:03:28,870 --> 00:03:31,780
you can use and it&#39;s a little confusing

71
00:03:31,780 --> 00:03:35,500
if you&#39;re new to this so we just

72
00:03:35,500 --> 00:03:39,280
used appl map which I told

73
00:03:39,280 --> 00:03:40,640
you

74
00:03:40,640 --> 00:03:43,010
iterate over the columns of a data frame

75
00:03:43,010 --> 00:03:45,590
and computes a result for each value of

76
00:03:45,590 --> 00:03:48,970
the column so its element by element

77
00:03:48,970 --> 00:03:51,250
apply on the other hand iterates over

78
00:03:51,250 --> 00:03:54,010
the columns and computes the aggregate

79
00:03:54,010 --> 00:03:57,520
value for each column and finally

80
00:03:57,520 --> 00:04:01,510
there&#39;s map which is like applymap but

81
00:04:01,510 --> 00:04:03,700
remember i told you that the methods are

82
00:04:03,700 --> 00:04:06,430
not always the same for series as for

83
00:04:06,430 --> 00:04:08,800
data frames so map is the same

84
00:04:08,800 --> 00:04:10,810
effectively as applymap but it&#39;s for

85
00:04:10,810 --> 00:04:13,240
series so it&#39;ll take it will apply

86
00:04:13,240 --> 00:04:17,739
whatever function you specified to each

87
00:04:17,738 --> 00:04:19,690
element of your series so you can

88
00:04:19,690 --> 00:04:22,740
transform a series that way

89
00:04:22,740 --> 00:04:25,860
let&#39;s have a look at using apply so

90
00:04:25,860 --> 00:04:28,349
recall apply is going to create computed

91
00:04:28,349 --> 00:04:31,199
aggregate for each column so we&#39;re using

92
00:04:31,199 --> 00:04:34,889
these for numeric columns here in this

93
00:04:34,889 --> 00:04:39,180
list in this Python list and here&#39;s

94
00:04:39,180 --> 00:04:42,240
apply and I&#39;ve got this lambda here

95
00:04:42,240 --> 00:04:45,210
which is just summing the value in those

96
00:04:45,210 --> 00:04:47,729
columns on when i run this code i&#39;m just

97
00:04:47,729 --> 00:04:49,680
going to get the sum, the aggregate sum,

98
00:04:49,680 --> 00:04:53,250
of the values in each of those four

99
00:04:53,250 --> 00:04:56,970
columns indeed that&#39;s what I get and I

100
00:04:56,970 --> 00:05:00,270
told you the last one of these operators

101
00:05:00,270 --> 00:05:03,690
is map and that&#39;s for a series and if i

102
00:05:03,690 --> 00:05:08,040
select just the price column from

103
00:05:08,040 --> 00:05:11,040
auto_prices i&#39;ve got a series

104
00:05:11,040 --> 00:05:13,890
because I now just have one column i&#39;m

105
00:05:13,890 --> 00:05:17,610
going to use map with the

106
00:05:17,610 --> 00:05:20,130
natural log function from the math

107
00:05:20,130 --> 00:05:22,680
library and i&#39;m just going to

108
00:05:22,680 --> 00:05:25,890
look at the first 10 elements of my

109
00:05:25,890 --> 00:05:27,630
series here so we don&#39;t print out all

110
00:05:27,630 --> 00:05:30,430
almost 200 of them

111
00:05:30,430 --> 00:05:33,610
and there we have it so so that&#39;s now

112
00:05:33,610 --> 00:05:37,840
the log price of those automobiles as a

113
00:05:37,840 --> 00:05:42,070
series rather than a data frame so

114
00:05:42,070 --> 00:05:45,280
that&#39;s some methods you can use to

115
00:05:45,280 --> 00:05:47,860
create conceivably quite powerful

116
00:05:47,860 --> 00:05:50,080
computations that you can perform on the

117
00:05:50,080 --> 00:05:54,600
rows and columns of a pandas dataframe

