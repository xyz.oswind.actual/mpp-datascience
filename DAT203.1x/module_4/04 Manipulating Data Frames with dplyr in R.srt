0
00:00:00,000 --> 00:00:04,110
So in the last sequence i introduced the

1
00:00:04,110 --> 00:00:06,779
idea of the dplyr verbs and we did

2
00:00:06,779 --> 00:00:10,440
a simple filter on the rows of on our

3
00:00:10,440 --> 00:00:14,130
data frame with that one dplyr verb so

4
00:00:14,130 --> 00:00:15,599
let&#39;s look at some other dplyr

5
00:00:15,599 --> 00:00:17,340
verbs that we use for basic

6
00:00:17,340 --> 00:00:21,240
manipulations on rows and columns of our

7
00:00:21,240 --> 00:00:24,020
data frames

8
00:00:24,560 --> 00:00:26,330
and on my screen here i have a first

9
00:00:26,330 --> 00:00:29,900
example so there&#39;s this slice verb and

10
00:00:29,900 --> 00:00:31,369
it does just what it sounds like it

11
00:00:31,369 --> 00:00:34,340
takes a slice of rows out of a data

12
00:00:34,340 --> 00:00:37,280
frame so here I&#39;ve got auto.price

13
00:00:37,280 --> 00:00:39,440
as my full data frame and i&#39;m just going

14
00:00:39,440 --> 00:00:43,730
to take rows 20 to 30 out of that data frame

15
00:00:43,730 --> 00:00:46,070
let me run it for you

16
00:00:46,070 --> 00:00:48,379
and there you have it so we have in fact

17
00:00:48,379 --> 00:00:52,000
11 rows

18
00:00:52,040 --> 00:00:53,630
and you can see we&#39;ve got all the

19
00:00:53,630 --> 00:00:55,430
columns because we didn&#39;t select sub

20
00:00:55,430 --> 00:00:58,310
select any columns so what&#39;s another

21
00:00:58,310 --> 00:01:00,380
thing i can do? well I might want to

22
00:01:00,380 --> 00:01:03,560
random sample a dataframe them as data

23
00:01:03,560 --> 00:01:06,770
scientists we do this very often because

24
00:01:06,770 --> 00:01:09,920
we needed for training a model,

25
00:01:09,920 --> 00:01:15,350
testing a model, etc so so we need to do

26
00:01:15,350 --> 00:01:17,090
a sample so i&#39;m using the sample_frac

27
00:01:17,090 --> 00:01:20,840
which takes a fraction of

28
00:01:20,840 --> 00:01:21,890
the rows

29
00:01:21,890 --> 00:01:23,960
so here&#39;s my data frame the one

30
00:01:23,960 --> 00:01:25,940
I&#39;ve just taken slice and i&#39;m going to

31
00:01:25,940 --> 00:01:29,180
take half (.5)  of those there&#39;s

32
00:01:29,180 --> 00:01:32,490
also a sample_in

33
00:01:32,490 --> 00:01:34,740
function if you wanted to take a fixed

34
00:01:34,740 --> 00:01:38,369
number like six or 25 or whatever it is

35
00:01:38,369 --> 00:01:42,290
so let me just run that for you

36
00:01:42,380 --> 00:01:45,350
and you can see that the row numbers

37
00:01:45,350 --> 00:01:48,469
here are randomly sampled they&#39;re in

38
00:01:48,469 --> 00:01:51,470
no particular order and so I have

39
00:01:51,470 --> 00:01:55,189
five which is as close to half as you

40
00:01:55,189 --> 00:02:00,350
can get of 11 randomly sampled rows

41
00:02:00,350 --> 00:02:02,420
ok so that&#39;s a couple of operations we

42
00:02:02,420 --> 00:02:07,539
can do on rows we can slice rows, we can

43
00:02:07,880 --> 00:02:11,760
sample rows. Let&#39;s

44
00:02:11,760 --> 00:02:15,060
look at the select verb

45
00:02:15,060 --> 00:02:17,610
we&#39;re going to do it on a data frame our

46
00:02:17,610 --> 00:02:20,040
subset and we&#39;re going to select just

47
00:02:20,040 --> 00:02:24,060
certain columns and notice again i just

48
00:02:24,060 --> 00:02:25,950
write out the names of those columns I

49
00:02:25,950 --> 00:02:28,319
don&#39;t have to put them in quotes or

50
00:02:28,319 --> 00:02:29,880
anything like that so we&#39;ve got drive

51
00:02:29,880 --> 00:02:33,120
wheels, wheel base, curb weight, horsepower,

52
00:02:33,120 --> 00:02:35,780
and price

53
00:02:36,690 --> 00:02:40,580
and so for those six

54
00:02:40,580 --> 00:02:42,530
Audi&#39;s recall the six

55
00:02:42,530 --> 00:02:46,880
Audi automobiles i just have those five

56
00:02:46,880 --> 00:02:51,250
columns and I&#39;ve done that with select

57
00:02:52,390 --> 00:02:54,770
and so now

58
00:02:54,770 --> 00:02:57,380
they&#39;re more row-based operation is

59
00:02:57,380 --> 00:02:58,910
something called a range which is

60
00:02:58,910 --> 00:03:01,670
basically a sort, but the word &quot;sort&quot;

61
00:03:01,670 --> 00:03:05,060
was taken so when Hadley Wickham wrote

62
00:03:05,060 --> 00:03:08,240
the dplyr package he picked a range

63
00:03:08,240 --> 00:03:12,680
as a sort, so whichever rows i put, or

64
00:03:12,680 --> 00:03:15,230
sorry column names i put here, the rows

65
00:03:15,230 --> 00:03:17,510
will be sorted in that order so first

66
00:03:17,510 --> 00:03:19,670
we&#39;re going to sort by drive wheels in

67
00:03:19,670 --> 00:03:21,620
ascending order is the default, and then

68
00:03:21,620 --> 00:03:23,480
we&#39;re going to sort by price in

69
00:03:23,480 --> 00:03:26,140
ascending order

70
00:03:26,980 --> 00:03:29,770
so let me do that for you

71
00:03:29,770 --> 00:03:33,280
and you can see ascending order is

72
00:03:33,280 --> 00:03:36,580
alphabetical order so 4 and then

73
00:03:36,580 --> 00:03:40,180
front-wheel drive FWD and you can see

74
00:03:40,180 --> 00:03:42,190
well there&#39;s only one four-wheel drive

75
00:03:42,190 --> 00:03:43,510
but then for the front-wheel-drive

76
00:03:43,510 --> 00:03:46,960
Audi&#39;s we see the price sorted in

77
00:03:46,960 --> 00:03:49,230
ascending order

78
00:03:49,230 --> 00:03:53,340
so those are some fundamental row and

79
00:03:53,340 --> 00:03:55,290
column manipulations you can do very

80
00:03:55,290 --> 00:04:01,730
easily using the dplyr verbs

