0
00:00:01,340 --> 00:00:04,700
Hi so having discussed a little bit

1
00:00:04,700 --> 00:00:08,269
about data frames in the abstract let&#39;s

2
00:00:08,269 --> 00:00:11,480
get down to trying some things with the

3
00:00:11,480 --> 00:00:14,690
Python pandas package so we&#39;ll see some

4
00:00:14,690 --> 00:00:19,130
actual behavior of data frames in with

5
00:00:19,130 --> 00:00:21,140
python

6
00:00:21,140 --> 00:00:25,130
so just to remind everybody of what

7
00:00:25,130 --> 00:00:28,400
we&#39;re talking about here i have just

8
00:00:28,400 --> 00:00:31,099
some simple quick demos we&#39;re going to

9
00:00:31,099 --> 00:00:36,460
start with this in this notebook so

10
00:00:36,710 --> 00:00:40,940
if you recall pandas can have two fundamental

11
00:00:40,940 --> 00:00:44,180
data structures there&#39;s a series and a

12
00:00:44,180 --> 00:00:46,600
data frame

13
00:00:46,600 --> 00:00:51,129
ok so a series is a one-dimensional

14
00:00:51,129 --> 00:00:52,930
array so I just have this list of

15
00:00:52,930 --> 00:00:55,899
numbers and i&#39;m going to make it into a

16
00:00:55,899 --> 00:00:58,930
pandas series and print it out and

17
00:00:58,930 --> 00:01:00,789
there you have it just my six numbers

18
00:01:00,789 --> 00:01:03,620
you see the

19
00:01:03,620 --> 00:01:06,740
indices of for those six numbers or

20
00:01:06,740 --> 00:01:09,820
labels here

21
00:01:09,970 --> 00:01:11,260
there&#39;s a lot more you can say about

22
00:01:11,260 --> 00:01:13,120
series but that&#39;s all we&#39;re going to say

23
00:01:13,120 --> 00:01:14,110
for now

24
00:01:14,110 --> 00:01:17,830
likewise the data frame i have three

25
00:01:17,830 --> 00:01:21,970
columns i&#39;m going to make X Y and

26
00:01:21,970 --> 00:01:25,990
strings and I&#39;m doing this there are

27
00:01:25,990 --> 00:01:28,390
various ways to create a data frame for

28
00:01:28,390 --> 00:01:31,180
pandas i&#39;m doing this with a dictionary

29
00:01:31,180 --> 00:01:33,640
in this case so the the dictionary

30
00:01:33,640 --> 00:01:36,710
element

31
00:01:36,710 --> 00:01:38,540
identifier is going to become the column

32
00:01:38,540 --> 00:01:40,180
name

33
00:01:40,180 --> 00:01:42,300
so let me run that

34
00:01:42,300 --> 00:01:45,780
and we have strings which is a string

35
00:01:45,780 --> 00:01:48,090
column it just says string 1, string 2,

36
00:01:48,090 --> 00:01:51,210
string 3. I have x and y which are my

37
00:01:51,210 --> 00:01:56,040
numeric columns x and y you see one with

38
00:01:56,040 --> 00:02:00,000
their values i have the column label

39
00:02:00,000 --> 00:02:05,520
strings, X, Y and I have the indices of the

40
00:02:05,520 --> 00:02:08,670
rows you know zero-based of course in

41
00:02:08,669 --> 00:02:10,980
Python so 0, 1, 2...

42
00:02:10,979 --> 00:02:14,670
so that&#39;s just a quick reminder of what

43
00:02:14,670 --> 00:02:17,730
we&#39;re talking about for series which are

44
00:02:17,730 --> 00:02:20,040
one-dimensional, and data frames which

45
00:02:20,040 --> 00:02:23,790
are two-dimensional tables so we&#39;re

46
00:02:23,790 --> 00:02:27,440
going to load a data set here

47
00:02:28,210 --> 00:02:32,580
and I&#39;ve cut and pasted in the auto

48
00:02:32,760 --> 00:02:35,159
generated code from when i created this

49
00:02:35,159 --> 00:02:38,489
notebook in Azure machine learning, and

50
00:02:38,489 --> 00:02:41,870
I&#39;ve changed the default name

51
00:02:41,870 --> 00:02:45,110
of the pandas dataframe that I get to

52
00:02:45,110 --> 00:02:51,379
auto_prices and we have to

53
00:02:51,379 --> 00:02:53,989
remove some missing values so how do you

54
00:02:53,989 --> 00:02:58,129
do that in pandas first off we care

55
00:02:58,129 --> 00:03:01,610
about the missing values in this case in

56
00:03:01,610 --> 00:03:04,640
these numeric columns but these

57
00:03:04,640 --> 00:03:08,819
numeric columns actually show up as

58
00:03:08,819 --> 00:03:12,299
strings because the way this data set

59
00:03:12,299 --> 00:03:14,249
was originally created the missing values

60
00:03:14,249 --> 00:03:17,760
were shown as a text string - just a

61
00:03:17,760 --> 00:03:20,730
question mark so we go through

62
00:03:20,730 --> 00:03:23,609
we&#39;ve got this data frame

63
00:03:23,609 --> 00:03:28,560
name and we look for this logical case

64
00:03:28,560 --> 00:03:33,689
where that column has a question mark in

65
00:03:33,689 --> 00:03:34,019
it

66
00:03:34,019 --> 00:03:36,959
whichever rows of that are and we put in

67
00:03:36,959 --> 00:03:39,870
a Numpy NaN which is a missing value

68
00:03:39,870 --> 00:03:44,129
and then we can take that transformed data

69
00:03:44,129 --> 00:03:46,050
frame

70
00:03:46,050 --> 00:03:50,150
and we can

71
00:03:50,150 --> 00:03:52,630
use the drop

72
00:03:52,630 --> 00:03:56,360
a method here okay

73
00:03:56,360 --> 00:03:58,730
and then the only thing left to do is to

74
00:03:58,730 --> 00:04:02,570
iterate over those columns and make sure

75
00:04:02,570 --> 00:04:06,020
they&#39;re coerced to numeric values so let me

76
00:04:06,020 --> 00:04:08,530
run that

77
00:04:09,020 --> 00:04:13,250
ok so let&#39;s see how we did

78
00:04:13,250 --> 00:04:14,810
so we&#39;re going to do some quick

79
00:04:14,810 --> 00:04:16,790
exploration of this data frame just to

80
00:04:16,790 --> 00:04:19,370
get a feel for what&#39;s going on here and

81
00:04:19,370 --> 00:04:21,830
with pandas data frames and

82
00:04:21,829 --> 00:04:24,200
series you can do data frame dot

83
00:04:24,200 --> 00:04:26,330
attributes if you know the attribute

84
00:04:26,330 --> 00:04:29,630
name you can just type the name of the

85
00:04:29,630 --> 00:04:31,700
data frame dot whatever that attribute

86
00:04:31,700 --> 00:04:33,800
name is so in this case i want to just

87
00:04:33,800 --> 00:04:35,930
know the column names and the attribute

88
00:04:35,930 --> 00:04:38,090
name for column names is not

89
00:04:38,090 --> 00:04:41,510
surprisingly columns so I run that and I

90
00:04:41,510 --> 00:04:44,330
see my first column is named symbolizing

91
00:04:44,330 --> 00:04:47,030
and I have a whole bunch of columns and

92
00:04:47,030 --> 00:04:49,940
my last column is called price so those

93
00:04:49,940 --> 00:04:54,030
are the column names of my data frame

94
00:04:54,030 --> 00:04:55,950
I can also apply methods to a dataframe

95
00:04:55,950 --> 00:04:59,550
so again it&#39;s like its data frame dot

96
00:04:59,550 --> 00:05:02,370
and then whatever my method name is and

97
00:05:02,370 --> 00:05:05,160
then any arguments that that method

98
00:05:05,160 --> 00:05:09,240
requires so in this case I have

99
00:05:09,240 --> 00:05:12,450
auto_prices.info, Info is a

100
00:05:12,450 --> 00:05:14,940
way to get a sort of a summary insight

101
00:05:14,940 --> 00:05:17,010
of the contents of that data frame - the

102
00:05:17,010 --> 00:05:19,500
structure of that data frame. I&#39;m

103
00:05:19,500 --> 00:05:21,000
not using any of the arguments in this

104
00:05:21,000 --> 00:05:24,750
case so i just have blanks in my params

105
00:05:24,750 --> 00:05:28,190
let me run that for you guys

106
00:05:28,190 --> 00:05:32,710
and what do I have select this first

107
00:05:32,710 --> 00:05:35,290
column here is called symbolizing have

108
00:05:35,290 --> 00:05:38,350
195 non-null values and

109
00:05:38,350 --> 00:05:42,580
there a 64 int -  a long int.

110
00:05:42,580 --> 00:05:46,689
if i look at say price likewise have

111
00:05:46,689 --> 00:05:50,050
195 values that are non

112
00:05:50,050 --> 00:05:53,650
null and they&#39;re also an int64 but if i

113
00:05:53,650 --> 00:05:57,300
look at something like a

114
00:05:57,300 --> 00:06:00,479
the bore of the engine again have

115
00:06:00,479 --> 00:06:03,180
195 values, they&#39;re non

116
00:06:03,180 --> 00:06:04,969
null,

117
00:06:04,969 --> 00:06:07,070
and that&#39;s a float

118
00:06:07,070 --> 00:06:09,100
and

119
00:06:09,100 --> 00:06:11,210
engine type

120
00:06:11,210 --> 00:06:14,330
95 non-null values is an object that

121
00:06:14,330 --> 00:06:17,919
basically means it&#39;s a string value

122
00:06:18,020 --> 00:06:20,419
so let me just do one more thing here we

123
00:06:20,419 --> 00:06:22,340
looked at this already but i just want

124
00:06:22,340 --> 00:06:25,310
to emphasize how you filter with pandas

125
00:06:25,310 --> 00:06:29,210
so you can do the data frames name and

126
00:06:29,210 --> 00:06:31,849
then the square bracket operator and

127
00:06:31,849 --> 00:06:34,940
some any logical expression so in this

128
00:06:34,940 --> 00:06:40,129
case let&#39;s find the about the cars out

129
00:06:40,129 --> 00:06:41,629
of that data set of a hundred

130
00:06:41,629 --> 00:06:44,060
ninety-five notice there&#39;s a hundred

131
00:06:44,060 --> 00:06:47,210
ninety five entries here that were made

132
00:06:47,210 --> 00:06:50,930
by Audi we&#39;re Audi is the make so I have

133
00:06:50,930 --> 00:06:53,750
auto_prices.make = &#39;audi&#39;

134
00:06:53,750 --> 00:06:57,139
and i&#39;ll call that new data frame

135
00:06:57,139 --> 00:06:58,940
DF

136
00:06:58,940 --> 00:07:01,880
let me run that and there we have it so

137
00:07:01,880 --> 00:07:03,950
there looks like there&#39;s five or six

138
00:07:03,950 --> 00:07:08,450
automobiles made by Audi in this data

139
00:07:08,450 --> 00:07:10,250
set and you can see the different

140
00:07:10,250 --> 00:07:13,160
columns for that frame which is a subset

141
00:07:13,160 --> 00:07:17,660
of the full auto_prices data

142
00:07:17,660 --> 00:07:19,630
set

143
00:07:19,630 --> 00:07:22,150
so that&#39;s a quick introduction to some

144
00:07:22,150 --> 00:07:24,790
very basic properties of how you work

145
00:07:24,790 --> 00:07:28,590
with pandas data frames

