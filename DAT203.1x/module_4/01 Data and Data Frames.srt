0
00:00:04,963 --> 00:00:08,840
Hi and welcome to the module on data exploration and

1
00:00:08,840 --> 00:00:10,230
visualization.

2
00:00:10,230 --> 00:00:14,600
So in this module we&#39;re going to look at visualization techniques

3
00:00:14,600 --> 00:00:18,770
you can use to understand the relationships in a dataset.

4
00:00:18,770 --> 00:00:21,630
And when you get a new dataset and you don&#39;t know much about

5
00:00:21,630 --> 00:00:26,010
it, one of the first things you should do is start exploring,

6
00:00:26,010 --> 00:00:30,550
exploring visually, maybe with some summary statistics,

7
00:00:30,550 --> 00:00:33,460
that we&#39;ve already talked about, things like that.

8
00:00:33,460 --> 00:00:37,510
So, what are we gonna cover in this module is just that,

9
00:00:37,510 --> 00:00:40,510
exploratory data analysis.

10
00:00:40,510 --> 00:00:41,240
How does that work?

11
00:00:41,240 --> 00:00:42,750
How do you do it?

12
00:00:42,750 --> 00:00:45,260
What do you get out of it?

13
00:00:45,260 --> 00:00:49,480
So, let&#39;s just talk a little bit about data frames here.

14
00:00:49,480 --> 00:00:54,130
So, data frames are very ubiquitous in R.

15
00:00:55,550 --> 00:00:58,470
R is built around data frames.

16
00:00:58,470 --> 00:01:02,640
We&#39;re going to talk In several, in a few chapters

17
00:01:02,640 --> 00:01:06,620
extensively about the Python Panda package which introduce us

18
00:01:06,620 --> 00:01:10,910
a very powerful data frame capability into Python.

19
00:01:10,910 --> 00:01:15,905
And there&#39;s data tables in Azure ML.

20
00:01:15,905 --> 00:01:16,523
Well, let me

21
00:01:16,523 --> 00:01:19,890
just make some general comments about data frame.

22
00:01:19,890 --> 00:01:23,400
So, as I said, they&#39;re available in R and in Python&#39;s Pandas.

23
00:01:24,470 --> 00:01:27,840
They&#39;re easy to map to and from Azure machine learning tables,

24
00:01:27,840 --> 00:01:29,050
you&#39;ve already been doing that.

25
00:01:29,050 --> 00:01:30,300
You&#39;ve seen it in our demos.

26
00:01:30,300 --> 00:01:31,450
You&#39;ve done it in your labs.

27
00:01:32,620 --> 00:01:38,470
They&#39;re rectangular tables so that means they can&#39;t

28
00:01:38,470 --> 00:01:41,590
have ragged ends, the column lengths have to be the same.

29
00:01:42,650 --> 00:01:49,018
And further each column has to be of one type, But R or

30
00:01:49,018 --> 00:01:54,733
Python Pandas gives you lots of ways to subset by rows and

31
00:01:54,733 --> 00:01:56,850
by columns.

32
00:01:56,850 --> 00:01:59,900
There&#39;s also ways in Azure ML which we&#39;ll get to as well.

33
00:02:01,290 --> 00:02:04,740
And you can either just subset by say,

34
00:02:04,740 --> 00:02:08,560
names or you can do lots of logical filtering.

35
00:02:08,560 --> 00:02:11,630
Pretty much any kind of filter you can conceive of

36
00:02:11,630 --> 00:02:16,210
the logic of you can create in R or Python Pandas.

37
00:02:18,320 --> 00:02:23,930
So in Azure ML, we have the following setup,

38
00:02:23,930 --> 00:02:26,340
so say our data set and execute,

39
00:02:26,340 --> 00:02:30,100
whatever script this could be execute R or execute Python.

40
00:02:30,100 --> 00:02:31,180
So you&#39;ve already seen this.

41
00:02:31,180 --> 00:02:35,270
So you get a data set that comes down, this is an Azure machine

42
00:02:35,270 --> 00:02:38,300
learning table that&#39;s output from the data set.

43
00:02:38,300 --> 00:02:41,545
And then it shows up as a data frame for R or for

44
00:02:41,545 --> 00:02:42,772
Python Pandas.

45
00:02:42,772 --> 00:02:45,780
All right, so with these basic simple facts about data

46
00:02:45,780 --> 00:02:46,700
frames in mind.

47
00:02:46,700 --> 00:02:49,670
And a little bit of background on how to debug if

48
00:02:49,670 --> 00:02:51,600
things go wrong.

49
00:02:51,600 --> 00:02:56,276
It&#39;s time to dig in either to the R E-plyer package

50
00:02:56,276 --> 00:02:58,895
the Python Pandas package.

