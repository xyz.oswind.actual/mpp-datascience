0
00:00:03,750 --> 00:00:07,799
Hi in the sequence i&#39;m going to talk

1
00:00:07,799 --> 00:00:11,460
about how we work with data frames in

2
00:00:11,460 --> 00:00:14,580
R from within the Azure

3
00:00:14,580 --> 00:00:17,730
machine learning Execute R script

4
00:00:17,730 --> 00:00:21,119
module so conceptually you can see on my

5
00:00:21,119 --> 00:00:23,789
screen here what I&#39;m talking about we

6
00:00:23,789 --> 00:00:27,960
have a data set input of some sort and

7
00:00:27,960 --> 00:00:29,699
we have an Execute R script in this

8
00:00:29,699 --> 00:00:33,989
little box down here and when we run our

9
00:00:33,989 --> 00:00:37,079
experiment a data table, an Azure

10
00:00:37,079 --> 00:00:42,030
data table comes down into that Execute

11
00:00:42,030 --> 00:00:44,070
R script

12
00:00:44,070 --> 00:00:48,390
and becomes a data frame so let&#39;s drill

13
00:00:48,390 --> 00:00:51,180
down on that and talk about the details

14
00:00:51,180 --> 00:00:56,129
so this rectangle now is my Execute R

15
00:00:56,129 --> 00:00:58,460
script

16
00:00:58,460 --> 00:01:01,100
I can have a first data frame and the

17
00:01:01,100 --> 00:01:03,680
second data frame and you see what I&#39;ve

18
00:01:03,680 --> 00:01:05,600
done is those data frames from those

19
00:01:05,600 --> 00:01:08,450
two input ports the Dataset1 and Dataset2

20
00:01:08,450 --> 00:01:12,619
input ports are read with this

21
00:01:12,619 --> 00:01:15,770
maml.mapInputPort function and I just give

22
00:01:15,770 --> 00:01:18,259
it the single argument 1 to show its

23
00:01:18,259 --> 00:01:21,710
port one or 2 to show its port two and

24
00:01:21,710 --> 00:01:24,110
then i have two data frames that I can

25
00:01:24,110 --> 00:01:25,940
work within my experiment: frame1 and

26
00:01:25,940 --> 00:01:28,240
frame2

27
00:01:28,240 --> 00:01:30,939
now we can also read in something from a

28
00:01:30,939 --> 00:01:34,570
zip file here so we have this zip input

29
00:01:34,570 --> 00:01:38,350
port and say I have an R script

30
00:01:38,350 --> 00:01:40,899
called myScript.R so it&#39;s a

31
00:01:40,899 --> 00:01:44,049
little R file in that zip file so i

32
00:01:44,049 --> 00:01:46,509
can just source it and notice i have to

33
00:01:46,509 --> 00:01:50,649
do is src/ so it&#39;s in the src directory

34
00:01:50,649 --> 00:01:53,320
then whatever the name is I&#39;ve given it

35
00:01:53,320 --> 00:01:57,100
myScript.R so the source is the same

36
00:01:57,100 --> 00:01:58,570
if you&#39;ve worked at the command

37
00:01:58,570 --> 00:02:01,209
line with R that you&#39;re used to

38
00:02:01,209 --> 00:02:05,100
just remember it&#39;s in this src

39
00:02:05,360 --> 00:02:08,619
directory

40
00:02:08,619 --> 00:02:10,629
so if  I&#39;m doing some debugging or something

41
00:02:10,628 --> 00:02:14,319
i can print stuff like here I&#39;m just

42
00:02:14,319 --> 00:02:16,989
printing hello world to this R device

43
00:02:16,989 --> 00:02:19,989
port so that shows up in the log file

44
00:02:19,989 --> 00:02:24,159
the output.log file and finally if I

45
00:02:24,159 --> 00:02:26,170
want to output a dataframe i use this

46
00:02:26,170 --> 00:02:30,220
maml.mapOutputPort and then i have my

47
00:02:30,220 --> 00:02:32,200
frame1, so I&#39;m just going to

48
00:02:32,200 --> 00:02:34,870
output frame1 notice that I have to have

49
00:02:34,870 --> 00:02:38,260
the name &quot;frame1&quot; in quotes or you get

50
00:02:38,260 --> 00:02:40,540
a very strange error message that may or

51
00:02:40,540 --> 00:02:42,340
may not make sense to you so just

52
00:02:42,340 --> 00:02:45,099
remember the quotes when you use the maml.mapOutputPort

53
00:02:45,099 --> 00:02:48,250
function and you can

54
00:02:48,250 --> 00:02:50,230
see the result is I now have an Azure

55
00:02:50,230 --> 00:02:53,769
machine learning table as an output so

56
00:02:53,769 --> 00:02:58,060
two tables in, one table out and so

57
00:02:58,060 --> 00:03:00,849
hopefully that gives you everything you

58
00:03:00,849 --> 00:03:05,140
need to know to successfully build R

59
00:03:05,140 --> 00:03:08,019
scripts that are standard R scripts in

60
00:03:08,019 --> 00:03:10,239
every other respect except you have to

61
00:03:10,239 --> 00:03:11,980
do these few little things and keep them

62
00:03:11,980 --> 00:03:14,530
in mind so they run correctly in the

63
00:03:14,530 --> 00:03:18,940
Azure ml Execute R Script module. So I

64
00:03:18,940 --> 00:03:22,060
have on my screen here a really

65
00:03:22,060 --> 00:03:24,879
simple experiment so it just has this

66
00:03:24,879 --> 00:03:28,780
automobile price data and single Execute

67
00:03:28,780 --> 00:03:32,410
R Script module and it let me click on

68
00:03:32,410 --> 00:03:34,660
that and I&#39;ll just walk you through this

69
00:03:34,660 --> 00:03:36,480
code

70
00:03:36,480 --> 00:03:41,250
so first off we have the maml.mapInputPort

71
00:03:41,250 --> 00:03:44,640
with the argument 1 and

72
00:03:44,640 --> 00:03:47,190
that&#39;s because you see I&#39;m only using

73
00:03:47,190 --> 00:03:50,280
the Dataset1 input port

74
00:03:50,280 --> 00:03:52,709
if I had something on this Dataset2

75
00:03:52,709 --> 00:03:55,799
input port another data table coming in

76
00:03:55,799 --> 00:03:59,250
I&#39;d need another maml.InputPort

77
00:03:59,250 --> 00:04:03,420
function with an argument of 2 in

78
00:04:03,420 --> 00:04:05,760
this case I assign the output of that

79
00:04:05,760 --> 00:04:07,829
function to a name

80
00:04:07,829 --> 00:04:10,319
autos and autos now becomes a data

81
00:04:10,319 --> 00:04:13,200
frame within my R script so that&#39;s all

82
00:04:13,200 --> 00:04:15,960
you have to do to read

83
00:04:15,960 --> 00:04:19,500
an Azure data table and end up with

84
00:04:19,500 --> 00:04:23,310
an R data frame and I can do typical R

85
00:04:23,310 --> 00:04:26,580
data frame operations on autos now like

86
00:04:26,580 --> 00:04:29,250
here i&#39;m just going to subset say I just

87
00:04:29,250 --> 00:04:31,020
want the height and the width of these

88
00:04:31,020 --> 00:04:35,340
cars for some analysis i&#39;m doing so so

89
00:04:35,340 --> 00:04:38,610
just this vector height and width the

90
00:04:38,610 --> 00:04:41,310
names you know the usual R subsetting

91
00:04:41,310 --> 00:04:46,250
operator here with the square brackets

92
00:04:46,440 --> 00:04:48,960
and I&#39;m gonna assign that to a new data

93
00:04:48,960 --> 00:04:51,540
frame name called out but say i was

94
00:04:51,540 --> 00:04:54,030
having some problem with this code and I

95
00:04:54,030 --> 00:04:55,860
wanted to look look at some intermediate

96
00:04:55,860 --> 00:04:58,110
results so i could add a print statement

97
00:04:58,110 --> 00:05:00,090
here in this case I&#39;m just going to use

98
00:05:00,090 --> 00:05:03,090
str to look at the properties of the

99
00:05:03,090 --> 00:05:07,290
out data frame but i can also then use

100
00:05:07,290 --> 00:05:11,610
the maml.mapOutputPort function and

101
00:05:11,610 --> 00:05:16,080
output out to this results dataset port

102
00:05:16,080 --> 00:05:19,650
and again recall as i mentioned before I

103
00:05:19,650 --> 00:05:22,920
put that name out in quotes

104
00:05:22,920 --> 00:05:24,240
that&#39;s very important that you remember

105
00:05:24,240 --> 00:05:28,810
to do that so let me just save

106
00:05:28,810 --> 00:05:33,210
and run this experiment

107
00:05:33,310 --> 00:05:36,910
good my experiment is run and I&#39;ll click

108
00:05:36,910 --> 00:05:39,490
at the output of this R device port

109
00:05:39,490 --> 00:05:42,460
which is where my printed output go and

110
00:05:42,460 --> 00:05:44,230
there you see it here&#39;s the data frame

111
00:05:44,230 --> 00:05:47,500
the 205 observations and just the two

112
00:05:47,500 --> 00:05:50,320
variables after I subset height and

113
00:05:50,320 --> 00:05:53,750
width ok; but I could

114
00:05:53,750 --> 00:05:56,420
print any other output there I want it

115
00:05:56,420 --> 00:05:59,750
and I&#39;ve returned to data frame using

116
00:05:59,750 --> 00:06:03,110
that maml.mapOutputPort function and

117
00:06:03,110 --> 00:06:05,570
there there my 205 rows and the two

118
00:06:05,570 --> 00:06:08,160
columns I sub selected

119
00:06:08,160 --> 00:06:10,200
so that&#39;s really all all there is to it

120
00:06:10,200 --> 00:06:12,390
that&#39;s all the bits you need to know to

121
00:06:12,390 --> 00:06:16,890
take any R code you have essentially

122
00:06:16,890 --> 00:06:20,670
and set it up so it can run successfully

123
00:06:20,670 --> 00:06:23,550
within an Execute R Script module

124
00:06:23,550 --> 00:06:26,510
within Azure ML.

