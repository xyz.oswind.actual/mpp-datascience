0
00:00:00,000 --> 00:00:05,910
In the previous sequence I focused on a

1
00:00:05,910 --> 00:00:09,570
single variable plots like box plots, bar

2
00:00:09,570 --> 00:00:13,559
plots, histograms, violin plots. Let&#39;s look

3
00:00:13,559 --> 00:00:17,880
at 2D plots now, plots where we have

4
00:00:17,880 --> 00:00:20,070
two variables that were plotting one

5
00:00:20,070 --> 00:00:22,699
against the other

6
00:00:23,030 --> 00:00:26,120
so the most basic 2d plot is one

7
00:00:26,120 --> 00:00:28,190
probably everybody&#39;s familiar with is

8
00:00:28,190 --> 00:00:32,840
called a scatter plot and we can use the

9
00:00:32,840 --> 00:00:38,510
pandas plot method kind equals scatter

10
00:00:38,510 --> 00:00:41,270
we&#39;re just going to look at City miles

11
00:00:41,270 --> 00:00:45,320
per gallon vs price so we&#39;re going to

12
00:00:45,320 --> 00:00:47,390
compute a plot here with very little

13
00:00:47,390 --> 00:00:51,379
code just take the defaults and there

14
00:00:51,379 --> 00:00:54,410
you go city miles per gallon price we

15
00:00:54,410 --> 00:00:56,560
have the dots

16
00:00:56,560 --> 00:00:59,110
and and and we see this nice curve

17
00:00:59,110 --> 00:01:03,310
relationship so so we know that it isn&#39;t

18
00:01:03,310 --> 00:01:05,230
a straight line relationship between

19
00:01:05,230 --> 00:01:07,030
price and city miles per gallon its kind

20
00:01:07,030 --> 00:01:09,220
of this curvy thing we also see this

21
00:01:09,220 --> 00:01:11,200
kind of these outliers these groups here

22
00:01:11,200 --> 00:01:13,780
like like here of these very

23
00:01:13,780 --> 00:01:17,020
fuel-efficient low-cost cars and these

24
00:01:17,020 --> 00:01:19,990
very fuel inefficient high-class car so

25
00:01:19,990 --> 00:01:21,070
that&#39;s kind of interesting and then

26
00:01:21,070 --> 00:01:23,470
we&#39;ve got some other groups that seem to

27
00:01:23,470 --> 00:01:27,040
be away from this same this main clump

28
00:01:27,040 --> 00:01:29,800
of cars here and we&#39;ve got these sort of

29
00:01:29,800 --> 00:01:33,010
concentrations of cars at certain price

30
00:01:33,010 --> 00:01:36,720
and fuel economy ranges

31
00:01:36,990 --> 00:01:39,090
so it&#39;s quite a bit we&#39;ve learned just

32
00:01:39,090 --> 00:01:41,700
by making this simple plot and if we

33
00:01:41,700 --> 00:01:44,549
want a little more control over it we

34
00:01:44,549 --> 00:01:47,399
can come we can use map plot live more

35
00:01:47,399 --> 00:01:48,659
directly

36
00:01:48,659 --> 00:01:51,149
this recipe is very similar to what i

37
00:01:51,149 --> 00:01:53,310
showed you in the previous sequence for

38
00:01:53,310 --> 00:01:56,310
univariate plots we create a figure

39
00:01:56,310 --> 00:02:00,940
sighs we create an axis we

40
00:02:00,940 --> 00:02:04,690
again use the Python pandas plot method

41
00:02:04,690 --> 00:02:07,240
kind equal scatter but now I&#39;ve given it

42
00:02:07,240 --> 00:02:11,560
the it inherits the axis attributes and

43
00:02:11,560 --> 00:02:15,819
I can set titles and labels

44
00:02:15,819 --> 00:02:18,670
by using methods on that axis attribute

45
00:02:18,670 --> 00:02:22,670
so let me run that for you

46
00:02:22,670 --> 00:02:25,370
it&#39;s a very similar plot again we see

47
00:02:25,370 --> 00:02:27,440
that curve relationship we see though

48
00:02:27,440 --> 00:02:33,230
the outliers of down here these highly

49
00:02:33,230 --> 00:02:34,940
fuel-efficient cars in these very

50
00:02:34,940 --> 00:02:37,370
expensive cars up here but I have some

51
00:02:37,370 --> 00:02:41,090
proper label so this is more of a plot i

52
00:02:41,090 --> 00:02:43,069
could use for a presentation because i

53
00:02:43,069 --> 00:02:45,650
have proper labels my audience will know

54
00:02:45,650 --> 00:02:48,410
what they&#39;re looking for what they&#39;re

55
00:02:48,410 --> 00:02:50,800
looking at

56
00:02:50,910 --> 00:02:53,790
so in the previous sequence i talked

57
00:02:53,790 --> 00:02:55,820
about

58
00:02:56,020 --> 00:02:58,480
single dimension kernel density

59
00:02:58,480 --> 00:03:00,940
estimation plots we saw that just as a

60
00:03:00,940 --> 00:03:02,860
kernel density estimation plot we also

61
00:03:02,860 --> 00:03:05,980
saw that in the violin plots but we can

62
00:03:05,980 --> 00:03:09,100
do a 2d kernel density estimation plot

63
00:03:09,100 --> 00:03:12,340
and we do that with the Seaborn package

64
00:03:12,340 --> 00:03:17,050
which we already discussed and a recipe

65
00:03:17,050 --> 00:03:18,610
isn&#39;t too different from what we&#39;ve

66
00:03:18,610 --> 00:03:21,340
already done there we set a figure set

67
00:03:21,340 --> 00:03:24,610
an axis set us to grid style just using

68
00:03:24,610 --> 00:03:27,820
that generic white grid and but notice

69
00:03:27,820 --> 00:03:31,060
when I do KDE plot this time I give

70
00:03:31,060 --> 00:03:33,660
it two

71
00:03:34,700 --> 00:03:37,970
variables; not one we used one in the

72
00:03:37,970 --> 00:03:41,120
last sequence obviously it

73
00:03:41,120 --> 00:03:44,540
inherits that axis, and I&#39;m using a

74
00:03:44,540 --> 00:03:46,550
color map here cmap

75
00:03:46,550 --> 00:03:48,440
just to make it look

76
00:03:48,440 --> 00:03:53,239
pretty, and i&#39;m also going to overlay on

77
00:03:53,239 --> 00:03:55,519
that same axis you see I could lay

78
00:03:55,519 --> 00:03:59,239
multiple plots on a given set of axes a

79
00:03:59,239 --> 00:04:03,270
scatterplot, so a Pandas scatterplot

80
00:04:03,270 --> 00:04:06,630
okay and I&#39;ve got my title and label so

81
00:04:06,630 --> 00:04:08,280
I know what my plot is when i presented

82
00:04:08,280 --> 00:04:12,290
to my colleagues and my bosses

83
00:04:12,810 --> 00:04:15,300
and there we have it so this is actually

84
00:04:15,300 --> 00:04:18,269
quite an interesting plot as we saw with

85
00:04:18,269 --> 00:04:20,700
the scatterplot we have miles per gallon

86
00:04:20,700 --> 00:04:23,310
on the horizontal axis price on the

87
00:04:23,310 --> 00:04:26,700
vertical axis and we have these contours

88
00:04:26,700 --> 00:04:31,260
of density so as I suspected and I

89
00:04:31,260 --> 00:04:34,500
mentioned we have certain ranges of

90
00:04:34,500 --> 00:04:37,380
price

91
00:04:37,380 --> 00:04:40,500
and miles per gallon were cars tend to

92
00:04:40,500 --> 00:04:43,260
group and you see we have a pretty

93
00:04:43,260 --> 00:04:45,240
strong grouping here

94
00:04:45,240 --> 00:04:49,020
no maybe in this a little less than you

95
00:04:49,020 --> 00:04:53,130
know maybe 27 to 30 miles per gallon in

96
00:04:53,130 --> 00:04:55,440
this price a little less than ten

97
00:04:55,440 --> 00:04:57,180
thousand dollars seems to be a really

98
00:04:57,180 --> 00:05:00,000
big peak of cars there&#39;s another little

99
00:05:00,000 --> 00:05:03,600
peek of somewhat more expensive less

100
00:05:03,600 --> 00:05:07,440
fuel-efficient cars outside of outliers

101
00:05:07,440 --> 00:05:09,660
here and then others that just are

102
00:05:09,660 --> 00:05:11,100
really outliers they didn&#39;t get

103
00:05:11,100 --> 00:05:14,700
contoured at all and some other car so

104
00:05:14,700 --> 00:05:17,310
we understand as we go through exploring

105
00:05:17,310 --> 00:05:18,780
this data set

106
00:05:18,780 --> 00:05:20,700
what these outliers are white why are

107
00:05:20,700 --> 00:05:22,920
they behaving different from the bulk of

108
00:05:22,920 --> 00:05:25,140
these cars and having something like

109
00:05:25,140 --> 00:05:27,840
this kernel density 2d kernel density

110
00:05:27,840 --> 00:05:31,860
estimate plot with the dots on it from

111
00:05:31,860 --> 00:05:34,170
the dot from the scatterplot really

112
00:05:34,170 --> 00:05:37,680
helps bring out at least for me my eye

113
00:05:37,680 --> 00:05:41,370
is drawn to the unusual cases and also

114
00:05:41,370 --> 00:05:45,750
where the centroid of everything lies so

115
00:05:45,750 --> 00:05:49,129
one last

116
00:05:49,129 --> 00:05:54,619
very common two-dimensional

117
00:05:54,619 --> 00:05:58,789
plot is a line plot and i&#39;m just

118
00:05:58,789 --> 00:06:01,459
going to create a data frame here so i&#39;m

119
00:06:01,459 --> 00:06:05,089
going to create a list of numbers one to

120
00:06:05,089 --> 00:06:08,029
a hundred and i&#39;m going to create a data

121
00:06:08,029 --> 00:06:10,040
frame

122
00:06:10,040 --> 00:06:16,030
from that and its square so this is just

123
00:06:19,350 --> 00:06:22,680
computing the square of that X

124
00:06:22,680 --> 00:06:26,509
for the range of a hundred

125
00:06:31,160 --> 00:06:34,950
and so let&#39;s make

126
00:06:34,950 --> 00:06:38,670
a line plot of that and again the the

127
00:06:38,670 --> 00:06:42,330
recipe is very similar notice though

128
00:06:42,330 --> 00:06:45,240
when i use the pandas method i don&#39;t

129
00:06:45,240 --> 00:06:47,100
actually have to say kind equals

130
00:06:47,100 --> 00:06:49,200
something the default happens to be line

131
00:06:49,200 --> 00:06:53,010
plots for better for worse and I give it

132
00:06:53,010 --> 00:06:54,960
my labels and all that so the same basic

133
00:06:54,960 --> 00:06:58,160
recipe we&#39;ve been using

134
00:06:59,020 --> 00:07:02,950
and there&#39;s my line plots i&#39;ve got x

135
00:07:02,950 --> 00:07:09,490
x squared on the vertical axis and so

136
00:07:09,490 --> 00:07:11,320
the line plot but it has some special

137
00:07:11,320 --> 00:07:13,180
properties which should be aware of

138
00:07:13,180 --> 00:07:16,050
notice

139
00:07:16,139 --> 00:07:18,870
x squared is of course monotonic with X

140
00:07:18,870 --> 00:07:21,659
it isn&#39;t like my scatter plot

141
00:07:21,659 --> 00:07:24,419
if i go back to my scatterplot you see I

142
00:07:24,419 --> 00:07:28,529
have many prices for each miles per

143
00:07:28,529 --> 00:07:30,360
gallon miles per gallon turn out to be

144
00:07:30,360 --> 00:07:33,180
quantized in an integer quantity

145
00:07:33,180 --> 00:07:35,999
so presumably to the nearest miles per

146
00:07:35,999 --> 00:07:36,870
gallon

147
00:07:36,870 --> 00:07:40,259
I couldn&#39;t if I tried to represent that

148
00:07:40,259 --> 00:07:42,090
with a line plot i have this line that

149
00:07:42,090 --> 00:07:43,620
was bouncing up and down and all over

150
00:07:43,620 --> 00:07:45,930
the place be absolute it wouldn&#39;t mean

151
00:07:45,930 --> 00:07:48,360
anything to anybody but if i have a

152
00:07:48,360 --> 00:07:51,029
monotonic relationship like this and and

153
00:07:51,029 --> 00:07:53,759
maybe a classic cases you know something

154
00:07:53,759 --> 00:07:55,349
where you have a simple mathematical

155
00:07:55,349 --> 00:07:57,629
relationship like this or a time series

156
00:07:57,629 --> 00:08:01,740
plot which things where there&#39;s there&#39;s

157
00:08:01,740 --> 00:08:04,469
reason to believe you have a monotonic

158
00:08:04,469 --> 00:08:07,800
relationship there is output of a model

159
00:08:07,800 --> 00:08:09,839
like a regression model that&#39;s where a

160
00:08:09,839 --> 00:08:13,189
line plot is really useful

161
00:08:14,120 --> 00:08:17,419
so in this sequence I&#39;ve discussed a

162
00:08:17,420 --> 00:08:20,150
number of two-dimensional plots

163
00:08:20,150 --> 00:08:23,980
scatterplots kernel density estimation

164
00:08:23,990 --> 00:08:26,870
plots and line plots all very widely

165
00:08:26,870 --> 00:08:30,400
used plot types

