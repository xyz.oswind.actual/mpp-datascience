0
00:00:00,000 --> 00:00:02,780
So we&#39;ve looked at

1
00:00:02,780 --> 00:00:05,390
row and column manipulations with panels

2
00:00:05,390 --> 00:00:07,729
data frames and how to compute on canvas

3
00:00:07,729 --> 00:00:09,470
data frames and there&#39;s one last topic I

4
00:00:09,470 --> 00:00:11,809
want to hit which is chaining of the

5
00:00:11,809 --> 00:00:14,629
methods and pandas so in many cases when

6
00:00:14,629 --> 00:00:16,309
you&#39;re doing some serious data munging

7
00:00:16,309 --> 00:00:18,410
you don&#39;t want just one method you need

8
00:00:18,410 --> 00:00:21,440
a whole series of methods to get the car

9
00:00:21,440 --> 00:00:23,930
the transformations and computations you

10
00:00:23,930 --> 00:00:26,630
need to get to the final result and

11
00:00:26,630 --> 00:00:28,310
pandas has a really powerful way of

12
00:00:28,310 --> 00:00:32,400
dealing with chains of these methods

13
00:00:32,400 --> 00:00:36,920
so my screen here I&#39;ve laid out

14
00:00:36,920 --> 00:00:39,559
our basic syntax so we have the name of

15
00:00:39,559 --> 00:00:41,690
our data frame dot whatever our first

16
00:00:41,690 --> 00:00:44,659
method is in its arguments dot whatever

17
00:00:44,659 --> 00:00:46,789
the second method is in its arguments

18
00:00:46,789 --> 00:00:50,300
dot etc etc so we can have essentially

19
00:00:50,300 --> 00:00:55,369
as long as chain as we need but with

20
00:00:55,369 --> 00:01:00,670
some restrictions obviously the schema

21
00:01:01,180 --> 00:01:02,770
the data frame needs to be compatible

22
00:01:02,770 --> 00:01:05,440
with whatever is required for this first

23
00:01:05,440 --> 00:01:09,220
method the schema produced by the

24
00:01:09,220 --> 00:01:11,320
results of that first method needs to be

25
00:01:11,320 --> 00:01:13,060
compatible with the input requirements

26
00:01:13,060 --> 00:01:16,180
of the second method etc so so sometimes

27
00:01:16,180 --> 00:01:20,050
you have to be on your toes and not not

28
00:01:20,050 --> 00:01:22,780
to have a product some sort of type

29
00:01:22,780 --> 00:01:26,740
conflict or data structure conflict so

30
00:01:26,740 --> 00:01:31,960
we&#39;ve done a number of let me start that

31
00:01:31,960 --> 00:01:34,170
over again

32
00:01:34,870 --> 00:01:38,490
so we&#39;ve applied a number of

33
00:01:38,490 --> 00:01:41,520
methods to this original auto underbar

34
00:01:41,520 --> 00:01:44,820
Price&#39;s data frame and let&#39;s go ahead

35
00:01:44,820 --> 00:01:47,090
and

36
00:01:47,090 --> 00:01:49,909
use a whole chain of these methods and

37
00:01:49,909 --> 00:01:52,520
and some new new ones too

38
00:01:52,520 --> 00:01:54,289
so first off we&#39;re going to subset that

39
00:01:54,289 --> 00:01:57,979
data frame using labels in first and

40
00:01:57,979 --> 00:02:01,310
we&#39;re going to only look for autos where

41
00:02:01,310 --> 00:02:04,039
the make is outie

42
00:02:04,039 --> 00:02:06,409
we&#39;re only going to take these four

43
00:02:06,409 --> 00:02:10,190
columns that are in that list so that&#39;s

44
00:02:10,190 --> 00:02:13,519
our first method is lock our second

45
00:02:13,519 --> 00:02:18,310
method is apply math and

46
00:02:18,310 --> 00:02:22,800
we want to take the log of those

47
00:02:22,980 --> 00:02:24,819
methods

48
00:02:24,819 --> 00:02:26,210
I&#39;m sorry the log

49
00:02:26,210 --> 00:02:29,960
values of each element in those columns

50
00:02:29,960 --> 00:02:31,700
we&#39;ve selected so that&#39;s why we&#39;re using

51
00:02:31,700 --> 00:02:34,130
the apply math methods that&#39;s our second

52
00:02:34,130 --> 00:02:38,160
method we want to sort

53
00:02:38,160 --> 00:02:41,400
the result so using the sort values

54
00:02:41,400 --> 00:02:43,020
method that&#39;s our third method by the

55
00:02:43,020 --> 00:02:46,650
price column and we&#39;re doing that on the

56
00:02:46,650 --> 00:02:50,280
road value so so so we&#39;ll wind up with

57
00:02:50,280 --> 00:02:53,640
an ordered list of price so let me run

58
00:02:53,640 --> 00:02:56,630
all that for you

59
00:02:57,160 --> 00:03:00,220
and there we have it so we have first

60
00:03:00,220 --> 00:03:03,730
off notice that we&#39;ve got the six outie

61
00:03:03,730 --> 00:03:05,569
cars

62
00:03:05,569 --> 00:03:09,319
we&#39;ve got the log of their some of their

63
00:03:09,319 --> 00:03:13,159
attributes here and it is indeed sorted

64
00:03:13,159 --> 00:03:16,850
in ascending order by price

65
00:03:16,850 --> 00:03:21,170
so now that we can build a chain we can

66
00:03:21,170 --> 00:03:24,300
include a group by method

67
00:03:24,300 --> 00:03:26,520
and.and group i can be you stand alone

68
00:03:26,520 --> 00:03:30,510
for sure but in practice i find that i

69
00:03:30,510 --> 00:03:32,610
usually use group by with some other

70
00:03:32,610 --> 00:03:34,470
method I don&#39;t just want the grouped

71
00:03:34,470 --> 00:03:38,250
data frame i want something of the group

72
00:03:38,250 --> 00:03:41,490
data frame so in this case I&#39;ve got auto

73
00:03:41,490 --> 00:03:44,160
prices and i&#39;m going to use body style

74
00:03:44,160 --> 00:03:46,590
number of cylinders and price so those

75
00:03:46,590 --> 00:03:50,100
are three columns i&#39;m selecting and i&#39;m

76
00:03:50,100 --> 00:03:53,160
just using the square bracket operator

77
00:03:53,160 --> 00:03:57,260
here I could use lock of course

78
00:03:58,080 --> 00:04:00,360
I&#39;m going to group by the first two of

79
00:04:00,360 --> 00:04:02,910
those columns so first I group by body

80
00:04:02,910 --> 00:04:05,640
style then secondarily I group by the

81
00:04:05,640 --> 00:04:08,400
number of cylinders and then i&#39;m going

82
00:04:08,400 --> 00:04:11,130
to apply account method so what I should

83
00:04:11,130 --> 00:04:13,710
get is account of how many cars there

84
00:04:13,710 --> 00:04:16,170
are by each body style and by the number

85
00:04:16,170 --> 00:04:19,859
of cylinders in the engine so that and i

86
00:04:19,858 --> 00:04:22,500
am using these two methods the group by

87
00:04:22,500 --> 00:04:26,240
an account to get there in that chain

88
00:04:26,240 --> 00:04:30,050
and there you have it so i have five

89
00:04:30,050 --> 00:04:33,949
bodystyles convertible hardtop etc and i

90
00:04:33,949 --> 00:04:35,960
have noticed their varying numbers of

91
00:04:35,960 --> 00:04:39,710
engines like convertibles calves 846

92
00:04:39,710 --> 00:04:44,509
whereas hardtops of 85 4 6 etcetera so

93
00:04:44,509 --> 00:04:48,380
that so if there are no counts in that

94
00:04:48,380 --> 00:04:50,750
combination of body style and number of

95
00:04:50,750 --> 00:04:53,000
cylinders it doesn&#39;t show up in my

96
00:04:53,000 --> 00:04:54,410
grouping which is nice you don&#39;t get a

97
00:04:54,410 --> 00:04:57,680
lot of blank values and then you can see

98
00:04:57,680 --> 00:04:59,810
how many prices basically there are

99
00:04:59,810 --> 00:05:04,069
which is the number of cars so 14 and

100
00:05:04,069 --> 00:05:06,020
you can see that there&#39;s an awful lot of

101
00:05:06,020 --> 00:05:07,940
say hatchbacks with four-cylinder

102
00:05:07,940 --> 00:05:09,919
engines an awful lot of sedans with

103
00:05:09,919 --> 00:05:13,550
four-cylinder engines in this data set

104
00:05:13,550 --> 00:05:15,590
and and maybe a fair number of wagons

105
00:05:15,590 --> 00:05:16,940
and everything else is kind of

106
00:05:16,940 --> 00:05:21,080
incidental small numbers that gives us

107
00:05:21,080 --> 00:05:22,460
some insight just from looking at

108
00:05:22,460 --> 00:05:25,490
Count&#39;s this is a way to get a summary

109
00:05:25,490 --> 00:05:27,949
of how frequent it&#39;s called a frequency

110
00:05:27,949 --> 00:05:29,659
table and gives you a summary of how

111
00:05:29,659 --> 00:05:32,210
frequent different combinations of

112
00:05:32,210 --> 00:05:35,600
categorical variables are

113
00:05:35,600 --> 00:05:38,600
and so let&#39;s look at one last example

114
00:05:38,600 --> 00:05:42,620
here so in this case I&#39;m going to do an

115
00:05:42,620 --> 00:05:46,280
aggregation by group and i&#39;m going to

116
00:05:46,280 --> 00:05:48,440
use a method a new method we looked at

117
00:05:48,440 --> 00:05:52,490
apply which does aggregations but you

118
00:05:52,490 --> 00:05:55,460
can also use the aggregate operator for

119
00:05:55,460 --> 00:05:57,200
some other function so in this case it&#39;s

120
00:05:57,200 --> 00:06:00,950
a numpy mean function and I&#39;m going to

121
00:06:00,950 --> 00:06:02,870
sort the value by prices so we&#39;re going

122
00:06:02,870 --> 00:06:05,360
to look at the mean prices of these

123
00:06:05,360 --> 00:06:09,230
automobiles grouped by body style so

124
00:06:09,230 --> 00:06:14,480
we&#39;ve got one two three four methods in

125
00:06:14,480 --> 00:06:17,050
our chain

126
00:06:17,310 --> 00:06:20,370
so we&#39;ve got by body style we&#39;ve got the

127
00:06:20,370 --> 00:06:23,340
mean of curb weight the mean a price etc

128
00:06:23,340 --> 00:06:26,730
and it&#39;s sort of notice it&#39;s sorted in

129
00:06:26,730 --> 00:06:30,540
ascending order by the mean price of the

130
00:06:30,540 --> 00:06:32,770
cars

131
00:06:32,770 --> 00:06:35,349
so that gives you some idea of some of

132
00:06:35,349 --> 00:06:37,210
the very powerful things you can do by

133
00:06:37,210 --> 00:06:42,129
chaining methods with pandas and also a

134
00:06:42,129 --> 00:06:44,530
few examples of say using the group by

135
00:06:44,530 --> 00:06:47,979
method in the chain so you can get

136
00:06:47,979 --> 00:06:50,139
various aggregations that are useful for

137
00:06:50,139 --> 00:06:53,460
summarizing your data

