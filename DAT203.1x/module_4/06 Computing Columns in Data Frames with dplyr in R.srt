0
00:00:00,000 --> 00:00:02,490
Now that you&#39;ve been introduced to some

1
00:00:02,490 --> 00:00:04,890
dplyr verbs and some basic row and

2
00:00:04,890 --> 00:00:07,140
column manipulations we can perform on

3
00:00:07,140 --> 00:00:10,320
the data frames with those verbs let&#39;s

4
00:00:10,320 --> 00:00:12,150
look at some other verbs that allow us

5
00:00:12,150 --> 00:00:15,690
to do kinda more interesting computing

6
00:00:15,690 --> 00:00:19,050
on the values in the rows and columns of

7
00:00:19,050 --> 00:00:21,220
an our data frame

8
00:00:21,220 --> 00:00:24,039
so the first case we&#39;re going to look at

9
00:00:24,039 --> 00:00:27,250
is computing a new column is very common

10
00:00:27,250 --> 00:00:29,770
that you need to do that that you want

11
00:00:29,770 --> 00:00:34,180
to compute one or more new columns in

12
00:00:34,180 --> 00:00:36,640
your data frame may be transforming the

13
00:00:36,640 --> 00:00:41,890
values in some way so the somewhat oddly

14
00:00:41,890 --> 00:00:45,879
named mutate function mutates the data

15
00:00:45,879 --> 00:00:49,900
frame by adding additional columns to it

16
00:00:49,900 --> 00:00:54,870
and so here&#39;s my original data frame df2

17
00:00:54,870 --> 00:00:58,800
and syntax then for mutate is

18
00:00:58,800 --> 00:01:02,610
I&#39;ve got say curb weight in kilograms so

19
00:01:02,610 --> 00:01:06,090
curbed weight kg equals the original

20
00:01:06,090 --> 00:01:09,540
curb weight column divided by the number

21
00:01:09,540 --> 00:01:13,290
of pounds in a kilogram so that I go

22
00:01:13,290 --> 00:01:17,100
from pounds to kilograms and maybe I

23
00:01:17,100 --> 00:01:19,080
want the ratio of weight to horsepower

24
00:01:19,080 --> 00:01:21,149
so weight.horsepower is the curb

25
00:01:21,149 --> 00:01:23,850
weight / the horsepower so give me the

26
00:01:23,850 --> 00:01:27,090
the number of pounds per horsepower in

27
00:01:27,090 --> 00:01:28,860
the car

28
00:01:28,860 --> 00:01:34,620
and i&#39;m just going to use select of curb

29
00:01:34,620 --> 00:01:37,530
weight curb weight in kilograms and weight.horsepower

30
00:01:37,530 --> 00:01:40,480
so we just see

31
00:01:40,480 --> 00:01:42,820
what&#39;s coming out there and we&#39;re just

32
00:01:42,820 --> 00:01:45,400
going to print that we&#39;re not assigning

33
00:01:45,400 --> 00:01:47,560
that to anything

34
00:01:47,560 --> 00:01:49,630
and so here&#39;s the original curb weight

35
00:01:49,630 --> 00:01:54,460
curb weight in kilograms and then that

36
00:01:54,460 --> 00:01:57,280
curb weight / the horsepower so it&#39;s a

37
00:01:57,280 --> 00:02:00,220
pounds per horsepower so that&#39;s a way

38
00:02:00,220 --> 00:02:03,369
with this mutate verb you can compute

39
00:02:03,369 --> 00:02:05,979
conceivably quite a few new

40
00:02:05,979 --> 00:02:09,160
columns in your data frame i can also

41
00:02:09,160 --> 00:02:11,739
use many times you want to do summaries

42
00:02:11,739 --> 00:02:13,840
or aggregations of the values in the

43
00:02:13,840 --> 00:02:16,360
data frame so i can use this summarize

44
00:02:16,360 --> 00:02:19,660
verb so let&#39;s stick with our result that

45
00:02:19,660 --> 00:02:22,299
we just computed here and so we&#39;re going

46
00:02:22,299 --> 00:02:24,910
to get the mean of the curb weight is

47
00:02:24,910 --> 00:02:27,610
mean of curb weight we&#39;re gonna get the

48
00:02:27,610 --> 00:02:29,260
standard deviation of curb weight to

49
00:02:29,260 --> 00:02:31,060
standard deviation of curb weight

50
00:02:31,060 --> 00:02:33,489
etc. the max and the min

51
00:02:33,489 --> 00:02:35,830
likewise so just some simple summary

52
00:02:35,830 --> 00:02:39,280
statistics that we can summarize what

53
00:02:39,280 --> 00:02:40,720
we&#39;ve just done here with these

54
00:02:40,720 --> 00:02:44,210
transformations on this result

55
00:02:44,210 --> 00:02:47,950
there&#39;s the the numbers

56
00:02:48,319 --> 00:02:51,469
but nothing too shocking about it but I

57
00:02:51,469 --> 00:02:53,659
hope you see you can because you can

58
00:02:53,659 --> 00:02:57,230
specify what those operations are you

59
00:02:57,230 --> 00:02:58,310
could do something much more

60
00:02:58,310 --> 00:03:00,260
sophisticated with the what I&#39;m doing in

61
00:03:00,260 --> 00:03:04,280
this demo. A final verb I&#39;d like to

62
00:03:04,280 --> 00:03:07,310
cover in this sequence is count so

63
00:03:07,310 --> 00:03:09,260
count does just that it gives you count

64
00:03:09,260 --> 00:03:11,870
it creates what we call in statistics a

65
00:03:11,870 --> 00:03:15,409
frequency table so we&#39;re going back to

66
00:03:15,409 --> 00:03:18,049
our original auto.price data frame

67
00:03:18,049 --> 00:03:21,980
and I want the counts by body style and

68
00:03:21,980 --> 00:03:24,260
number of cylinders in the engine of the

69
00:03:24,260 --> 00:03:25,760
car

70
00:03:25,760 --> 00:03:27,379
so those are my two arguments and if i

71
00:03:27,379 --> 00:03:29,659
added other categorical variables i

72
00:03:29,659 --> 00:03:31,489
could get this even more subdivided but

73
00:03:31,489 --> 00:03:33,799
let&#39;s just make a two-level frequency

74
00:03:33,799 --> 00:03:38,060
table here with this and so I can see

75
00:03:38,060 --> 00:03:41,329
for my convertibles there&#39;s 8,4,6 engines

76
00:03:41,329 --> 00:03:43,909
there&#39;s not too many convertibles but if

77
00:03:43,909 --> 00:03:47,510
i look at like hatchbacks I&#39;ve got 4, 6,

78
00:03:47,510 --> 00:03:50,329
and 3-cylinder engines is only one

79
00:03:50,329 --> 00:03:52,609
three-cylinder engine there&#39;s quite a

80
00:03:52,609 --> 00:03:56,220
few four-cylinder hatchbacks

81
00:03:56,220 --> 00:03:58,320
likewise there&#39;s quite a few in the

82
00:03:58,320 --> 00:04:01,560
sedan&#39;s I&#39;ve got a few eights a few fives

83
00:04:01,560 --> 00:04:05,400
quite a few fours and if you sit and you

84
00:04:05,400 --> 00:04:07,080
know it&#39;s actually the largest number

85
00:04:07,080 --> 00:04:10,200
and then a few sixes etc so that&#39;s my

86
00:04:10,200 --> 00:04:12,210
frequency table I&#39;ve created with this

87
00:04:12,210 --> 00:04:14,410
count function

88
00:04:14,410 --> 00:04:18,730
so those are some simple

89
00:04:18,730 --> 00:04:24,430
ideas of how you compute on in this case

90
00:04:24,430 --> 00:04:26,950
mostly columns but you can also do some

91
00:04:26,950 --> 00:04:30,040
operations on rows with data frames

92
00:04:30,040 --> 00:04:32,110
using these very powerful dplyr

93
00:04:32,110 --> 00:04:34,740
verbs

