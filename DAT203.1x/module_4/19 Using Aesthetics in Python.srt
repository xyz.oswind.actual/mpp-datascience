0
00:00:00,000 --> 00:00:03,330
Hi and welcome back in the last

1
00:00:03,330 --> 00:00:07,649
sequences we looked at plots for single

2
00:00:07,649 --> 00:00:10,349
variables and for pairs of variables 2d

3
00:00:10,349 --> 00:00:12,090
plots

4
00:00:12,090 --> 00:00:16,890
but in every case we were fixed to a 2d

5
00:00:16,890 --> 00:00:19,260
surface of the screen of the computer

6
00:00:19,260 --> 00:00:21,720
which is what you&#39;re always dealing with

7
00:00:21,720 --> 00:00:22,590
you always

8
00:00:22,590 --> 00:00:26,070
viewing whatever projection you are you

9
00:00:26,070 --> 00:00:28,289
have in your visualization on a 2d

10
00:00:28,289 --> 00:00:31,619
surface there&#39;s no way around it but we

11
00:00:31,619 --> 00:00:34,260
can use plot aesthetics to introduce

12
00:00:34,260 --> 00:00:37,590
additional dimensions to that 2d surface

13
00:00:37,590 --> 00:00:40,350
we can also use plot aesthetics to

14
00:00:40,350 --> 00:00:43,050
highlight certain relationships we want

15
00:00:43,050 --> 00:00:45,030
to draw the attentions of our audience

16
00:00:45,030 --> 00:00:47,130
to or even our own attention if we&#39;re

17
00:00:47,130 --> 00:00:48,840
exploring a new data set that we don&#39;t

18
00:00:48,840 --> 00:00:51,070
really understand

19
00:00:51,070 --> 00:00:54,310
so what are some typical aesthetics that

20
00:00:54,310 --> 00:00:56,320
we&#39;re going to cover here are color,

21
00:00:56,320 --> 00:01:01,989
transparency, size, marker shape, and then

22
00:01:01,989 --> 00:01:04,000
at the end i&#39;m going to show you a few

23
00:01:04,000 --> 00:01:07,180
aesthetics we can use their specific to

24
00:01:07,180 --> 00:01:10,030
certain plot types so let&#39;s just start

25
00:01:10,030 --> 00:01:14,470
with color and

26
00:01:14,470 --> 00:01:17,170
we&#39;re going to use

27
00:01:24,150 --> 00:01:27,600
we&#39;re going to use the Seaborn what&#39;s

28
00:01:27,600 --> 00:01:31,070
called lmplot

29
00:01:31,090 --> 00:01:33,130
and so it&#39;s a lot it&#39;s a scatter plot

30
00:01:33,130 --> 00:01:35,469
but it implies that there&#39;s a linear

31
00:01:35,469 --> 00:01:37,750
model plot it but if you don&#39;t want to

32
00:01:37,750 --> 00:01:40,509
see that you can do fit_reg = false

33
00:01:40,509 --> 00:01:43,149
so when I don&#39;t want to

34
00:01:43,149 --> 00:01:47,110
show that line plot in this case i just

35
00:01:47,110 --> 00:01:49,689
want to see the scatterplot and I have

36
00:01:49,689 --> 00:01:52,030
city miles per gallon vs price which is

37
00:01:52,030 --> 00:01:53,740
the scatterplot we looked at the

38
00:01:53,740 --> 00:01:56,380
last sequence but i&#39;m going to change

39
00:01:56,380 --> 00:02:00,250
the hue that is the color by fuel type

40
00:02:00,250 --> 00:02:02,859
and I&#39;ve picked this palette called set2

41
00:02:02,859 --> 00:02:07,720
now when you&#39;re designing a plot like

42
00:02:07,720 --> 00:02:09,910
this that you&#39;re going to use colors

43
00:02:09,910 --> 00:02:12,069
there&#39;s a couple of words of warning

44
00:02:12,069 --> 00:02:15,519
first off a fair number of your audience

45
00:02:15,519 --> 00:02:17,680
especially men maybe red-green

46
00:02:17,680 --> 00:02:19,720
colorblind so you want to pick palettes

47
00:02:19,720 --> 00:02:23,230
that try to avoid reds and greens the

48
00:02:23,230 --> 00:02:27,220
other is, fuel type in this case there&#39;s

49
00:02:27,220 --> 00:02:29,379
only two fuel types as gasoline and

50
00:02:29,379 --> 00:02:31,060
diesel, so we&#39;re gonna have two

51
00:02:31,060 --> 00:02:33,370
colors on the plot and that&#39;s fine if

52
00:02:33,370 --> 00:02:35,380
you pick some categorical variable that

53
00:02:35,380 --> 00:02:38,799
has 20 different levels and you have 20

54
00:02:38,799 --> 00:02:40,450
different colors people are going to

55
00:02:40,450 --> 00:02:43,269
have a really hard time telling one

56
00:02:43,269 --> 00:02:45,940
shade of blue from another one shade of

57
00:02:45,940 --> 00:02:48,190
orange from a shade of red or something

58
00:02:48,190 --> 00:02:49,989
like that it becomes kind of meaningless

59
00:02:49,989 --> 00:02:53,230
so don&#39;t go overboard with using colors

60
00:02:53,230 --> 00:02:55,980
as an aesthetic

61
00:02:57,000 --> 00:03:00,640
and there i&#39;ve run that plot

62
00:03:00,640 --> 00:03:05,770
and I have gas and diesel

63
00:03:05,770 --> 00:03:07,780
gas in green

64
00:03:07,780 --> 00:03:09,790
diesel in red - exactly what i said not

65
00:03:09,790 --> 00:03:12,970
to do but but it for someone who&#39;s

66
00:03:12,970 --> 00:03:14,380
colorblind this will show up as

67
00:03:14,380 --> 00:03:17,110
different shades of grey presumably so

68
00:03:17,110 --> 00:03:19,420
some of these outliers we looked at now

69
00:03:19,420 --> 00:03:22,240
we can see that were away from this main

70
00:03:22,240 --> 00:03:24,400
grouping of cars tend to be the diesel

71
00:03:24,400 --> 00:03:27,010
engines so that&#39;s interesting so right

72
00:03:27,010 --> 00:03:29,860
away by adding one simple plot aesthetic

73
00:03:29,860 --> 00:03:33,700
we have learned something about the

74
00:03:33,700 --> 00:03:36,760
prices and fuel efficiencies of cars

75
00:03:36,760 --> 00:03:41,680
that in general it looks like a diesel

76
00:03:41,680 --> 00:03:43,900
car for a given fuel efficiency cost

77
00:03:43,900 --> 00:03:45,860
more

78
00:03:45,860 --> 00:03:47,250
now

79
00:03:47,250 --> 00:03:50,100
I can also do this with somewhat more

80
00:03:50,100 --> 00:03:52,380
control using matplotlib but a lot

81
00:03:52,380 --> 00:03:56,160
more code i have to create my groups

82
00:03:56,160 --> 00:04:00,930
manually I have to plot the two groups

83
00:04:00,930 --> 00:04:04,260
separately on the same set of axes but i

84
00:04:04,260 --> 00:04:08,580
can then include titles i can define my

85
00:04:08,580 --> 00:04:12,300
patches my red and a blue patch and i

86
00:04:12,300 --> 00:04:14,310
can put a legend on so there&#39;s lots of

87
00:04:14,310 --> 00:04:16,530
things I can do that I don&#39;t have that

88
00:04:16,529 --> 00:04:19,739
control here with seaborn whether

89
00:04:19,738 --> 00:04:21,600
that&#39;s worth it to or not you&#39;ll have to

90
00:04:21,600 --> 00:04:23,820
decide but let me just run that you&#39;ll

91
00:04:23,820 --> 00:04:25,980
see the difference you see I&#39;ve got a

92
00:04:25,980 --> 00:04:28,200
little bit more control over size I&#39;ve

93
00:04:28,200 --> 00:04:30,180
got my axis labels I&#39;ve got a proper

94
00:04:30,180 --> 00:04:33,930
title I&#39;ve actually got a easier to read

95
00:04:33,930 --> 00:04:36,120
legend in some ways with diesel and gas

96
00:04:36,120 --> 00:04:37,890
and I could control what those colors

97
00:04:37,890 --> 00:04:39,169
were

98
00:04:39,169 --> 00:04:41,509
now it&#39;s red and blue not red and green

99
00:04:41,509 --> 00:04:43,669
which I already told you was a problem

100
00:04:43,669 --> 00:04:45,499
the conclusion is the same however that

101
00:04:45,499 --> 00:04:49,039
for a given level of fuel efficiency

102
00:04:49,039 --> 00:04:53,449
we&#39;re going to pay more for a diesel car

103
00:04:53,449 --> 00:04:55,669
so what&#39;s another plot aesthetic

104
00:04:55,669 --> 00:04:59,809
transparency is another powerful plot

105
00:04:59,809 --> 00:05:01,879
aesthetic and the reason is if you look

106
00:05:01,879 --> 00:05:04,279
at this plot we&#39;ve already seen that

107
00:05:04,279 --> 00:05:06,889
with the scatter plots and with the 2d

108
00:05:06,889 --> 00:05:10,219
density kernel density estimation plots

109
00:05:10,219 --> 00:05:13,639
there&#39;s a lot of dots lying one on top

110
00:05:13,639 --> 00:05:15,620
of each other in some of these regions

111
00:05:15,620 --> 00:05:17,930
because there&#39;s just a lot of cars in

112
00:05:17,930 --> 00:05:21,469
certain price and fuel efficiency ranges

113
00:05:21,469 --> 00:05:23,270
and it&#39;s kind of hard to tell in fact

114
00:05:23,270 --> 00:05:25,249
that may be impossible to tell is that

115
00:05:25,249 --> 00:05:28,069
two dots on top of each other four dots

116
00:05:28,069 --> 00:05:30,560
or eight dot sight you really cannot see

117
00:05:30,560 --> 00:05:33,169
it so you can use something called

118
00:05:33,169 --> 00:05:38,479
transparency and so that&#39;s specified in

119
00:05:38,479 --> 00:05:43,759
like matplotlib.pyplot

120
00:05:43,759 --> 00:05:47,270
plots as a parameter alpha

121
00:05:47,270 --> 00:05:49,069
in fact that&#39;s very ubiquitous across

122
00:05:49,069 --> 00:05:51,589
many plotting packages not just in

123
00:05:51,589 --> 00:05:56,419
Python so an alpha of 1 is perfectly

124
00:05:56,419 --> 00:05:59,719
opaque so it can&#39;t has no so see-through

125
00:05:59,719 --> 00:06:02,330
an alpha of 0 is perfectly transparent

126
00:06:02,330 --> 00:06:05,120
you won&#39;t see that point at all so it&#39;s

127
00:06:05,120 --> 00:06:06,050
kind of useless

128
00:06:06,050 --> 00:06:08,389
generally I find values like

129
00:06:08,389 --> 00:06:10,610
.2 or .3 are good if I have

130
00:06:10,610 --> 00:06:13,009
us you know a reasonable amount over

131
00:06:13,009 --> 00:06:16,689
plotting not a huge amount

132
00:06:16,689 --> 00:06:18,999
I&#39;m gonna go with .3 here otherwise

133
00:06:18,999 --> 00:06:21,849
the recipe is the same it&#39;s like i said

134
00:06:21,849 --> 00:06:22,989
this code is a little complicated

135
00:06:22,989 --> 00:06:25,629
because I&#39;m having to do some of these

136
00:06:25,629 --> 00:06:30,269
operations as primitives

137
00:06:31,360 --> 00:06:33,219
so it&#39;s essentially the same plot i

138
00:06:33,219 --> 00:06:35,900
showed you here

139
00:06:35,900 --> 00:06:39,590
but with this transparency reduced to .3

140
00:06:39,590 --> 00:06:41,990
so the see-through is much

141
00:06:41,990 --> 00:06:45,110
higher and now i start to see yeah there

142
00:06:45,110 --> 00:06:47,180
really are clusters of cars where

143
00:06:47,180 --> 00:06:50,030
there&#39;s a lot of points lying on top of

144
00:06:50,030 --> 00:06:52,220
each other and put certain places here

145
00:06:52,220 --> 00:06:54,259
just a lot of and they&#39;re all gasoline

146
00:06:54,259 --> 00:06:57,740
cars and you can see a single you know

147
00:06:57,740 --> 00:06:59,570
the single cars that are out here in

148
00:06:59,570 --> 00:07:02,060
these outliers I can still see the point

149
00:07:02,060 --> 00:07:03,650
because they&#39;re the transparency was set

150
00:07:03,650 --> 00:07:05,570
the .3 if i set it to like .05

151
00:07:05,570 --> 00:07:07,880
i might not even be able to see those

152
00:07:07,880 --> 00:07:08,810
points

153
00:07:08,810 --> 00:07:10,310
it looks like there&#39;s a couple diesel

154
00:07:10,310 --> 00:07:12,500
cars at that price and fuel efficiency

155
00:07:12,500 --> 00:07:16,190
see that&#39;s a a brighter red than these

156
00:07:16,190 --> 00:07:21,620
others just a little bit so so it

157
00:07:21,620 --> 00:07:24,440
confirmed some of the things we&#39;ve been

158
00:07:24,440 --> 00:07:26,360
inferring about this relationship

159
00:07:26,360 --> 00:07:28,849
between the price and the fuel

160
00:07:28,849 --> 00:07:31,699
efficiency of these automobiles just by

161
00:07:31,699 --> 00:07:34,190
using in fact in this case two plot

162
00:07:34,190 --> 00:07:37,099
aesthetics we&#39;ve got transparency so we

163
00:07:37,099 --> 00:07:39,320
can see the groupings better and we&#39;re

164
00:07:39,320 --> 00:07:40,789
using color so we can tell the

165
00:07:40,789 --> 00:07:42,349
difference between gasoline cars and

166
00:07:42,349 --> 00:07:45,190
diesel cars

167
00:07:45,720 --> 00:07:48,150
so all right let&#39;s add yet another

168
00:07:48,150 --> 00:07:51,690
aesthetic in this case is marker size

169
00:07:51,690 --> 00:07:54,600
see we&#39;ve been using just a single dot

170
00:07:54,600 --> 00:07:57,290
size here

171
00:07:57,930 --> 00:08:03,320
can do something else I can go in here

172
00:08:03,910 --> 00:08:05,430
and

173
00:08:05,430 --> 00:08:08,280
there&#39;s this argument to my pandas

174
00:08:08,280 --> 00:08:10,920
plotting method

175
00:08:10,920 --> 00:08:15,450
of s = 0.5 x auto.price

176
00:08:15,450 --> 00:08:18,240
engine size so I&#39;m just taking the

177
00:08:18,240 --> 00:08:21,150
engine size column from my original data

178
00:08:21,150 --> 00:08:23,310
frame multiplying by a half just for

179
00:08:23,310 --> 00:08:25,740
some scaling which I didn&#39;t want my dots

180
00:08:25,740 --> 00:08:29,340
to be too big so now the dot size will

181
00:08:29,340 --> 00:08:31,890
tell us something about engine size and

182
00:08:31,890 --> 00:08:36,200
all the other aesthetics are the same

183
00:08:37,130 --> 00:08:42,630
all right here we go so we can see

184
00:08:42,630 --> 00:08:44,310
some relationship there&#39;s

185
00:08:44,310 --> 00:08:47,340
quite a few cars with big dots big

186
00:08:47,340 --> 00:08:49,500
gasoline engines in these high-priced

187
00:08:49,500 --> 00:08:53,340
cars not too surprisingly the highly

188
00:08:53,340 --> 00:08:55,680
fuel-efficient gasoline cars have much

189
00:08:55,680 --> 00:08:58,260
smaller dots than these very expensive

190
00:08:58,260 --> 00:09:01,800
gas guzzlers less clear what the

191
00:09:01,800 --> 00:09:03,900
relationship is in diesel, they&#39;re sort of

192
00:09:03,900 --> 00:09:06,120
scattered all over and when you get into

193
00:09:06,120 --> 00:09:08,610
this area here again it&#39;s less clear

194
00:09:08,610 --> 00:09:11,070
they&#39;re mostly mid-sized engines I

195
00:09:11,070 --> 00:09:13,200
suppose you could say but it&#39;s not

196
00:09:13,200 --> 00:09:17,730
entirely clear and notice I&#39;ve

197
00:09:17,730 --> 00:09:20,910
changed my title here so it says with

198
00:09:20,910 --> 00:09:23,390
engine size

199
00:09:23,390 --> 00:09:25,880
but let me do something else i&#39;m going

200
00:09:25,880 --> 00:09:26,900
to do

201
00:09:26,900 --> 00:09:30,770
engine size square so i&#39;m using a

202
00:09:30,770 --> 00:09:32,720
slightly different scale factor so it&#39;s

203
00:09:32,720 --> 00:09:36,050
auto price prices engine size times auto

204
00:09:36,050 --> 00:09:39,590
price engine size etc so why am i doing

205
00:09:39,590 --> 00:09:43,400
that so in this case the radius or

206
00:09:43,400 --> 00:09:46,740
diameter of the point

207
00:09:46,740 --> 00:09:48,810
that i&#39;m plotting is proportional to the

208
00:09:48,810 --> 00:09:51,240
engine size but there&#39;s been a lot of

209
00:09:51,240 --> 00:09:53,220
studies on this and the human eye tends

210
00:09:53,220 --> 00:09:56,910
to pick up area more than size of

211
00:09:56,910 --> 00:10:00,550
markers like this okay

212
00:10:00,550 --> 00:10:04,000
so let me run this so now we&#39;re

213
00:10:04,000 --> 00:10:06,940
so now you can see the area of some of

214
00:10:06,940 --> 00:10:11,170
these gas-guzzling expensive cars they

215
00:10:11,170 --> 00:10:13,240
have really big engine sizes you see

216
00:10:13,240 --> 00:10:16,810
more variation of the area, which

217
00:10:16,810 --> 00:10:18,700
proportional the engine size of the

218
00:10:18,700 --> 00:10:23,440
diesel cars and these little tiny

219
00:10:23,440 --> 00:10:25,600
engines for these highly fuel-efficient

220
00:10:25,600 --> 00:10:28,090
cars it&#39;s just much more apparent

221
00:10:28,090 --> 00:10:30,040
although I may need to increase my

222
00:10:30,040 --> 00:10:32,470
transparency a bit because you see I&#39;m

223
00:10:32,470 --> 00:10:34,390
getting... because of my bigger marker

224
00:10:34,390 --> 00:10:36,550
size or change the scaling of my marker

225
00:10:36,550 --> 00:10:39,610
size and I&#39;m getting so much overlap

226
00:10:39,610 --> 00:10:41,020
here it&#39;s a little hard to see I&#39;m

227
00:10:41,020 --> 00:10:46,350
having a bit of a jam-up so

228
00:10:47,510 --> 00:10:49,760
another aesthetic the final aesthetic

229
00:10:49,760 --> 00:10:51,410
we&#39;re going to look at for scatterplots

230
00:10:51,410 --> 00:10:54,720
is marker shape

231
00:10:54,720 --> 00:10:56,670
and so I

232
00:10:56,670 --> 00:10:58,800
have this again and in my pandas

233
00:10:58,800 --> 00:11:03,060
plotting method plot I&#39;ve got

234
00:11:03,060 --> 00:11:06,980
marker=mk and I&#39;ve had to

235
00:11:06,980 --> 00:11:13,040
specify lists of, turbo, you know whether

236
00:11:13,040 --> 00:11:15,250
I have

237
00:11:16,360 --> 00:11:22,870
one in diesel and gas and etc so it&#39;s a

238
00:11:22,870 --> 00:11:24,670
it&#39;s a fair amount of a little bit

239
00:11:24,670 --> 00:11:26,290
tricky code that i had to build these

240
00:11:26,290 --> 00:11:28,600
lists of the possible combinations and

241
00:11:28,600 --> 00:11:31,480
the colors and and marker types i want

242
00:11:31,480 --> 00:11:34,600
to use see you might want to study that

243
00:11:34,600 --> 00:11:37,269
code and you&#39;ll see what the pattern is

244
00:11:37,269 --> 00:11:39,399
especially when you see what the plot is

245
00:11:39,399 --> 00:11:41,860
but the basic recipes the same it&#39;s just

246
00:11:41,860 --> 00:11:44,410
a little bit tedious to set up you know

247
00:11:44,410 --> 00:11:47,829
which color and which marker goes with

248
00:11:47,829 --> 00:11:50,800
which type of aspiration whether the

249
00:11:50,800 --> 00:11:54,850
which is turbo or standard corporation

250
00:11:54,850 --> 00:11:58,500
and then the fuel type

251
00:11:59,350 --> 00:12:02,540
so standard

252
00:12:02,540 --> 00:12:05,570
carburation vehicles have a plus sign

253
00:12:05,570 --> 00:12:09,290
turbo have a circle the size of the

254
00:12:09,290 --> 00:12:13,090
marker is proportional to the square

255
00:12:14,230 --> 00:12:18,570
of the engine size

256
00:12:19,520 --> 00:12:22,070
so now we start to see that first off

257
00:12:22,070 --> 00:12:24,800
most of our diesels are turbo there&#39;s

258
00:12:24,800 --> 00:12:28,910
only a few non turbo diesels these were

259
00:12:28,910 --> 00:12:34,880
the red pluses are these cheaper fairly

260
00:12:34,880 --> 00:12:36,650
fuel-efficient cars which was a strong

261
00:12:36,650 --> 00:12:40,910
cluster are all gasoline standard cars

262
00:12:40,910 --> 00:12:43,850
this other cluster we had up here are

263
00:12:43,850 --> 00:12:47,600
all almost all gasoline turbo cars so

264
00:12:47,600 --> 00:12:49,280
maybe that accounts for why they&#39;re more

265
00:12:49,280 --> 00:12:53,240
expensive so so by using essentially

266
00:12:53,240 --> 00:12:55,730
multiple aesthetics now we&#39;ve learned a

267
00:12:55,730 --> 00:12:58,100
lot about this car so how many plot

268
00:12:58,100 --> 00:13:00,830
dimensions do we actually have here we

269
00:13:00,830 --> 00:13:03,590
have price and city miles per gallon

270
00:13:03,590 --> 00:13:04,910
which is where we started so that&#39;s two

271
00:13:04,910 --> 00:13:07,790
plot dimensions we have marker size

272
00:13:07,790 --> 00:13:11,600
that&#39;s three dimensions we have color

273
00:13:11,600 --> 00:13:15,650
which is four dimensions we have shape

274
00:13:15,650 --> 00:13:17,780
which is five dimensions so basically we&#39;re

275
00:13:17,780 --> 00:13:19,550
showing

276
00:13:19,550 --> 00:13:22,560
five dimensions

277
00:13:22,560 --> 00:13:24,270
on a two-dimensional projection

278
00:13:24,270 --> 00:13:27,210
and we&#39;re highlighting certain

279
00:13:27,210 --> 00:13:30,090
aspects of these relationships in terms

280
00:13:30,090 --> 00:13:34,380
of where different types of cars lie so

281
00:13:34,380 --> 00:13:37,710
so just by using a few plot aesthetics

282
00:13:37,710 --> 00:13:41,460
in a bit of code we&#39;ve brought out a lot

283
00:13:41,460 --> 00:13:44,130
more of the dimensionality of this data

284
00:13:44,130 --> 00:13:48,060
set so we look at a couple of different

285
00:13:48,060 --> 00:13:52,110
plot specific aesthetics so we can have

286
00:13:52,110 --> 00:13:54,930
the number of bins and a histogram and

287
00:13:54,930 --> 00:13:58,800
this code again follows the i&#39;m using

288
00:13:58,800 --> 00:14:01,529
two different sets of axes here and I

289
00:14:01,529 --> 00:14:05,400
column is actually an axis array - axis 0

290
00:14:05,400 --> 00:14:08,740
and axis 1

291
00:14:08,740 --> 00:14:11,500
and we&#39;re going to do histogram of

292
00:14:11,500 --> 00:14:14,459
engine size

293
00:14:15,590 --> 00:14:18,800
ok and you seethe trick is

294
00:14:18,800 --> 00:14:22,670
you do plot.subplots and I&#39;ve got a

295
00:14:22,670 --> 00:14:26,930
1 by 2 array of axes

296
00:14:26,930 --> 00:14:30,110
for my figure and my axes so that&#39;s the

297
00:14:30,110 --> 00:14:32,660
only new coding trick here to get this

298
00:14:32,660 --> 00:14:35,510
and i just did that so you can see the

299
00:14:35,510 --> 00:14:40,210
difference between bins=40

300
00:14:40,400 --> 00:14:47,180
and the default,

301
00:14:50,310 --> 00:14:53,130
so here&#39;s the default with just the 10

302
00:14:53,130 --> 00:14:56,860
bins that&#39;s the default and here&#39;s

303
00:14:56,860 --> 00:15:00,160
with 40 bin so the question is you know

304
00:15:00,160 --> 00:15:02,050
which one is better well it depends what

305
00:15:02,050 --> 00:15:05,980
you&#39;re trying to do with 10 bins there&#39;s

306
00:15:05,980 --> 00:15:09,860
it&#39;s pretty smooth right

307
00:15:09,860 --> 00:15:12,260
but I&#39;ve lost a lot of details, with 40

308
00:15:12,260 --> 00:15:13,910
mins i may have gone overboard you see

309
00:15:13,910 --> 00:15:15,709
it&#39;s quite jagged it&#39;s hard to tell

310
00:15:15,709 --> 00:15:18,110
where the kind of the mode of that

311
00:15:18,110 --> 00:15:21,230
distribution is so maybe with 40 bins

312
00:15:21,230 --> 00:15:23,390
I&#39;ve introduced too much noise so i

313
00:15:23,390 --> 00:15:24,920
might want to keep this is a case where

314
00:15:24,920 --> 00:15:26,720
I need to keep iterating i might want to

315
00:15:26,720 --> 00:15:29,300
try 20 bins and see if that&#39;s a more

316
00:15:29,300 --> 00:15:33,260
satisfactory plot. So another possible

317
00:15:33,260 --> 00:15:36,380
aesthetic with we looked at violin plots

318
00:15:36,380 --> 00:15:40,940
previously and i can add a hue for

319
00:15:40,940 --> 00:15:43,160
aspiration so we&#39;ve already separated

320
00:15:43,160 --> 00:15:46,399
the violin plots by fuel type but i&#39;m

321
00:15:46,399 --> 00:15:49,040
going to use a hue of aspiration so i&#39;m

322
00:15:49,040 --> 00:15:51,980
going to put a third dimension into my

323
00:15:51,980 --> 00:15:55,670
violin plot so I have price on the

324
00:15:55,670 --> 00:15:59,089
vertical axis I have gas and diesel cars

325
00:15:59,089 --> 00:16:01,970
on this horizontal axis and then by

326
00:16:01,970 --> 00:16:05,630
color i have standard or turbo and I can

327
00:16:05,630 --> 00:16:08,450
see there&#39;s generally the distributions

328
00:16:08,450 --> 00:16:10,430
of turbo cars are shifted up a little

329
00:16:10,430 --> 00:16:13,360
bit so for both gas and diesel

330
00:16:13,360 --> 00:16:15,579
just the ranges and things like that I

331
00:16:15,579 --> 00:16:17,649
don&#39;t draw any new conclusions but i can

332
00:16:17,649 --> 00:16:18,910
see you gotta pay a little more

333
00:16:18,910 --> 00:16:21,670
generally if you want to turbo cars

334
00:16:21,670 --> 00:16:26,040
opposed to a standard car and

335
00:16:26,140 --> 00:16:29,350
I can do a similar thing with hue on a

336
00:16:29,350 --> 00:16:32,080
box plot in this case i&#39;m using body

337
00:16:32,080 --> 00:16:34,540
style so just to give you an idea we

338
00:16:34,540 --> 00:16:40,450
we&#39;ve got, well  unfortunately my legend

339
00:16:40,450 --> 00:16:43,480
kind of laid down there but for gas

340
00:16:43,480 --> 00:16:45,939
and diesel cars

341
00:16:45,939 --> 00:16:48,939
I can see for example hardtop is this

342
00:16:48,939 --> 00:16:52,599
group is a wide range of gasoline cars

343
00:16:52,599 --> 00:16:55,479
in that brown has a pretty skewed

344
00:16:55,479 --> 00:16:57,129
distribution because the medians way

345
00:16:57,129 --> 00:17:00,609
down there convertibles has this fairly

346
00:17:00,609 --> 00:17:03,220
elongated distribution again a little

347
00:17:03,220 --> 00:17:06,909
skewed etc so you could study this is

348
00:17:06,909 --> 00:17:08,500
quite a bit of information I could also

349
00:17:08,500 --> 00:17:11,829
go back and and do a violin plot with

350
00:17:11,829 --> 00:17:14,529
the hue of body style and try to maybe

351
00:17:14,529 --> 00:17:18,280
better understand those relationships

352
00:17:18,280 --> 00:17:23,410
so we&#39;ve used aesthetics to both

353
00:17:23,410 --> 00:17:25,540
highlight relationships in the data and

354
00:17:25,540 --> 00:17:28,600
project additional dimensions on to our

355
00:17:28,600 --> 00:17:31,000
two-dimensional screen that we&#39;re stuck

356
00:17:31,000 --> 00:17:34,650
within the visualization world

