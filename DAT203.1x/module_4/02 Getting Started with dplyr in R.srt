0
00:00:00,000 --> 00:00:03,149
So in the previous sequence i talked

1
00:00:03,149 --> 00:00:05,490
about data frames kind of the abstract

2
00:00:05,490 --> 00:00:08,429
about what what the date what those data

3
00:00:08,429 --> 00:00:10,260
structures are and a little bit about

4
00:00:10,260 --> 00:00:13,230
how we use them and things like that but

5
00:00:13,230 --> 00:00:15,059
let&#39;s bring this down to earth now and

6
00:00:15,059 --> 00:00:17,609
talk specifically about our data frames

7
00:00:17,609 --> 00:00:20,580
and how we use the very powerful R

8
00:00:20,580 --> 00:00:22,949
dplyr package to do manipulations on

9
00:00:22,949 --> 00:00:26,580
data frames so i have on my screen here

10
00:00:26,580 --> 00:00:29,070
a notebook that I&#39;ve started in Azure

11
00:00:29,070 --> 00:00:32,790
machine learning and we&#39;re going to work

12
00:00:32,790 --> 00:00:36,329
with this automobile price data (raw).csv

13
00:00:36,329 --> 00:00:39,960
data so when I started this notebook

14
00:00:39,960 --> 00:00:43,290
most of this code was auto-generated for

15
00:00:43,290 --> 00:00:47,070
me by a AML except I did one thing here i

16
00:00:47,070 --> 00:00:49,289
gave my data frame a more meaningful

17
00:00:49,289 --> 00:00:53,720
name auto.price instead of just dat

18
00:00:53,720 --> 00:00:56,090
I have this function to clean the data

19
00:00:56,090 --> 00:00:58,430
and there&#39;s two problems with these data

20
00:00:58,430 --> 00:01:00,530
because of missing values which are

21
00:01:00,530 --> 00:01:04,940
coded as a string ? some columns which

22
00:01:04,940 --> 00:01:07,490
like these with the column names which

23
00:01:07,490 --> 00:01:10,340
should be coded as numeric aren&#39;t they

24
00:01:10,340 --> 00:01:13,040
come up as strings so first off I run l

25
00:01:13,040 --> 00:01:16,490
apply over the the list of these columns

26
00:01:16,490 --> 00:01:20,060
and i use the as.numeric function to

27
00:01:20,060 --> 00:01:22,700
coerce them to numeric

28
00:01:22,700 --> 00:01:26,869
and then I need to remove rows where

29
00:01:26,869 --> 00:01:29,420
there&#39;s missing values so I&#39;m just going

30
00:01:29,420 --> 00:01:32,240
to chuck those rows out and i use the

31
00:01:32,240 --> 00:01:36,289
complete.cases function to do that so

32
00:01:36,289 --> 00:01:39,740
complete.cases if there&#39;s any missing

33
00:01:39,740 --> 00:01:42,140
values in a row

34
00:01:42,140 --> 00:01:46,250
it returns false if there are because

35
00:01:46,250 --> 00:01:47,720
it&#39;s not a complete case there&#39;s missing

36
00:01:47,720 --> 00:01:49,790
values if it is a complete case no

37
00:01:49,790 --> 00:01:51,890
missing values in that row it returns

38
00:01:51,890 --> 00:01:56,090
true so by applying this operator and

39
00:01:56,090 --> 00:01:58,670
returning that amount or that

40
00:01:58,670 --> 00:02:03,410
result I get just the rows without any

41
00:02:03,410 --> 00:02:07,690
missing values so let me

42
00:02:07,690 --> 00:02:10,030
run that for you

43
00:02:10,030 --> 00:02:11,620
and you can see when i coerced to

44
00:02:11,620 --> 00:02:14,380
numeric is a few NAs for sure

45
00:02:14,380 --> 00:02:18,580
and let&#39;s use the R str function just

46
00:02:18,580 --> 00:02:20,620
to get a quick look at what&#39;s in this

47
00:02:20,620 --> 00:02:24,880
data frame and you can see we&#39;ve got

48
00:02:24,880 --> 00:02:27,940
you know a number of you know fuel type

49
00:02:27,940 --> 00:02:29,950
is a character and you can see a few

50
00:02:29,950 --> 00:02:32,440
samples of that aspiration is character

51
00:02:32,440 --> 00:02:36,070
and then we&#39;ve got things like

52
00:02:36,070 --> 00:02:39,730
horsepower which is numeric

53
00:02:39,730 --> 00:02:42,790
and city miles per gallon which turns

54
00:02:42,790 --> 00:02:45,250
out to be an integer in this case or the

55
00:02:45,250 --> 00:02:47,110
price of the automobile in numeric so

56
00:02:47,110 --> 00:02:50,530
there&#39;s 26 columns of several different

57
00:02:50,530 --> 00:02:52,569
types

58
00:02:52,569 --> 00:02:56,079
so let&#39;s do some manipulation of this

59
00:02:56,079 --> 00:02:59,680
just to get started with dplyr so

60
00:02:59,680 --> 00:03:04,629
the basic syntax of dplyr is a

61
00:03:04,629 --> 00:03:06,579
package of what are called verbs and

62
00:03:06,579 --> 00:03:09,909
they&#39;re called verbs because they take

63
00:03:09,909 --> 00:03:14,200
an action they do something they operate

64
00:03:14,200 --> 00:03:19,060
on the data frame so if I pick a verb

65
00:03:19,060 --> 00:03:20,980
and they&#39;re usually something you know

66
00:03:20,980 --> 00:03:23,950
with a descriptive name like summarize

67
00:03:23,950 --> 00:03:26,439
or filter something like that so

68
00:03:26,439 --> 00:03:28,959
whatever my verb name is given a data

69
00:03:28,959 --> 00:03:31,540
frame as the first argument invariably

70
00:03:31,540 --> 00:03:33,609
and then there may be other optional

71
00:03:33,609 --> 00:03:37,269
arguments and that gives me my result so

72
00:03:37,269 --> 00:03:40,180
it&#39;s a fairly simple very consistent

73
00:03:40,180 --> 00:03:42,219
syntax for these dplyr verbs which

74
00:03:42,219 --> 00:03:43,510
is one of the really nice things about

75
00:03:43,510 --> 00:03:45,420
the package

76
00:03:45,420 --> 00:03:48,360
so in this case I&#39;m going to use the

77
00:03:48,360 --> 00:03:51,850
dplyr filter verb

78
00:03:51,850 --> 00:03:54,620
and i just want to find

79
00:03:54,620 --> 00:03:57,530
for the auto.price data frame i just

80
00:03:57,530 --> 00:04:00,490
want to find cases where

81
00:04:00,490 --> 00:04:04,330
the make the manufacturer is Audi now

82
00:04:04,330 --> 00:04:06,940
notice I don&#39;t have to use any square

83
00:04:06,940 --> 00:04:08,980
brackets or dollar signs or any of the

84
00:04:08,980 --> 00:04:11,110
usual our notation with this because

85
00:04:11,110 --> 00:04:15,670
they&#39;ve given the dplyr  filter verb

86
00:04:15,670 --> 00:04:20,170
the name of the data frame it knows

87
00:04:20,170 --> 00:04:22,930
there&#39;s a column called make and that i

88
00:04:22,930 --> 00:04:25,720
want only the cases where make is equal

89
00:04:25,720 --> 00:04:28,930
to Audi so let me run that for you

90
00:04:28,930 --> 00:04:30,400
then we&#39;re loading the package and

91
00:04:30,400 --> 00:04:33,370
operating the filter and well we have

92
00:04:33,370 --> 00:04:37,400
six automobiles

93
00:04:37,400 --> 00:04:39,430
that

94
00:04:39,430 --> 00:04:44,160
all have a make of Audi

95
00:04:44,729 --> 00:04:49,740
so that&#39;s the quick introduction to dplyr

96
00:04:49,740 --> 00:04:53,490
and how you used dplyr verbs on

97
00:04:53,490 --> 00:04:57,199
our data frames

