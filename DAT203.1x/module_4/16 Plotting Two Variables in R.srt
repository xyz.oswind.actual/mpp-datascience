0
00:00:01,480 --> 00:00:04,720
Hi welcome back in the last sequence we

1
00:00:04,720 --> 00:00:07,690
talked about univariate plots or plots

2
00:00:07,690 --> 00:00:10,990
where we took one attribute one feature

3
00:00:10,990 --> 00:00:13,449
of the data set and looked at its

4
00:00:13,449 --> 00:00:16,960
relationships in this sequence we&#39;re

5
00:00:16,960 --> 00:00:19,060
going to look at two dimensional plots

6
00:00:19,060 --> 00:00:21,130
that is where we have an x and y

7
00:00:21,130 --> 00:00:23,050
variable and we can look at the

8
00:00:23,050 --> 00:00:26,660
relationships between two variables

9
00:00:26,660 --> 00:00:30,830
so my screen here i have the code to

10
00:00:30,830 --> 00:00:33,470
create the most obvious of these sorts

11
00:00:33,470 --> 00:00:36,559
of plots a 2d scatterplot so again with

12
00:00:36,559 --> 00:00:40,520
ggplot i have the name of my data frame

13
00:00:40,520 --> 00:00:46,040
in using aes i define

14
00:00:46,040 --> 00:00:49,340
my x-axis my horizontal axis city

15
00:00:49,340 --> 00:00:52,220
miles per gallon and my vertical axis is

16
00:00:52,220 --> 00:00:54,830
price and i&#39;m just going to make a point

17
00:00:54,830 --> 00:00:57,440
plot so point plot is a plot where

18
00:00:57,440 --> 00:01:01,520
there&#39;s just one point for every car at

19
00:01:01,520 --> 00:01:03,590
whatever it miles per gallon and

20
00:01:03,590 --> 00:01:09,050
prices and then I&#39;ve added some X labels

21
00:01:09,050 --> 00:01:12,560
a Y label, and an overall plot title

22
00:01:12,560 --> 00:01:16,960
ok let me run this and we&#39;ll look at it

23
00:01:18,250 --> 00:01:23,680
and you can see my title and prices on

24
00:01:23,680 --> 00:01:26,290
the vertical axis and city miles per

25
00:01:26,290 --> 00:01:29,080
gallon on the horizontal axis and you

26
00:01:29,080 --> 00:01:30,940
start to see some interesting things in

27
00:01:30,940 --> 00:01:33,310
this plot right you first off see that

28
00:01:33,310 --> 00:01:36,010
this kind of this may be curvy

29
00:01:36,010 --> 00:01:38,590
relationship doesn&#39;t look like there&#39;s a

30
00:01:38,590 --> 00:01:40,540
really great straight line fit to these

31
00:01:40,540 --> 00:01:42,430
data

32
00:01:42,430 --> 00:01:44,710
we&#39;ve got this cluster of outliers which

33
00:01:44,710 --> 00:01:47,080
are these expensive cars that we&#39;ve

34
00:01:47,080 --> 00:01:49,690
already noted and notice that they also

35
00:01:49,690 --> 00:01:52,660
all have generally pretty low fuel

36
00:01:52,660 --> 00:01:58,270
efficiency we also have this cluster of

37
00:01:58,270 --> 00:02:00,970
lower price car see there&#39;s a lot of

38
00:02:00,970 --> 00:02:02,950
dots there you actually it&#39;s even hard

39
00:02:02,950 --> 00:02:05,350
to tell how many dots there are kind of

40
00:02:05,350 --> 00:02:07,960
in the middle range of fuel efficiency

41
00:02:07,960 --> 00:02:10,720
and then we have these extremely

42
00:02:10,720 --> 00:02:12,940
fuel-efficient cars that are also at the

43
00:02:12,940 --> 00:02:16,440
low price range and we have this

44
00:02:16,440 --> 00:02:19,110
other cluster of cars and then we have a

45
00:02:19,110 --> 00:02:21,690
bunch of cars that seem to have somewhat

46
00:02:21,690 --> 00:02:25,040
different behavior up here

47
00:02:25,990 --> 00:02:27,940
so we can get a little bit more

48
00:02:27,940 --> 00:02:31,180
information on this by using a 2d kernel

49
00:02:31,180 --> 00:02:33,670
density estimation plot we already

50
00:02:33,670 --> 00:02:36,280
looked at a 1d kernel density estimation

51
00:02:36,280 --> 00:02:38,470
plot where we looked at just the price

52
00:02:38,470 --> 00:02:40,840
and then we did a similar thing with

53
00:02:40,840 --> 00:02:45,190
those violin plots where we segregated

54
00:02:45,190 --> 00:02:49,000
price or or group the price by fuel type

55
00:02:49,000 --> 00:02:51,430
but this gives us a way to look at a 2d

56
00:02:51,430 --> 00:02:55,570
contour so I&#39;m going to add to plot

57
00:02:55,570 --> 00:02:57,190
attributes here so I&#39;m going to keep my

58
00:02:57,190 --> 00:03:00,760
points on the plot but i&#39;m going to add

59
00:03:00,760 --> 00:03:04,650
this geom_density2d

60
00:03:04,660 --> 00:03:08,830
so just added one new call here and

61
00:03:08,830 --> 00:03:11,640
we&#39;ll see what that does

62
00:03:11,760 --> 00:03:13,439
and it&#39;s actually kind of interesting

63
00:03:13,439 --> 00:03:17,250
you see again same horizontal and

64
00:03:17,250 --> 00:03:19,680
vertical axis here but as I said

65
00:03:19,680 --> 00:03:22,860
remember we had this notion that there

66
00:03:22,860 --> 00:03:24,870
was a high density of cars in this

67
00:03:24,870 --> 00:03:28,170
certain area around 20 maybe 25 miles

68
00:03:28,170 --> 00:03:31,200
per gallon and a price of maybe eight or

69
00:03:31,200 --> 00:03:33,930
nine thousand dollars and indeed you can

70
00:03:33,930 --> 00:03:36,209
see this is like a topographic map so

71
00:03:36,209 --> 00:03:38,069
there&#39;s a big hill here is a big bump

72
00:03:38,069 --> 00:03:40,409
and we have this really high density of

73
00:03:40,409 --> 00:03:44,340
cars in this particular region and we

74
00:03:44,340 --> 00:03:47,360
also have another kind of

75
00:03:47,360 --> 00:03:51,880
little hump on our Hill here and then

76
00:03:51,880 --> 00:03:53,980
this cluster of cars and then some true

77
00:03:53,980 --> 00:03:57,040
outliers these cars are outside even the

78
00:03:57,040 --> 00:04:00,640
lowest value of the contour so that

79
00:04:00,640 --> 00:04:03,370
gives us some considerable more

80
00:04:03,370 --> 00:04:06,250
information that there&#39;s just a lot of

81
00:04:06,250 --> 00:04:08,980
cars in this region and maybe in this

82
00:04:08,980 --> 00:04:11,980
region and then a lot of cars that seem

83
00:04:11,980 --> 00:04:14,080
to have special cases for some reason we

84
00:04:14,080 --> 00:04:15,930
don&#39;t yet know

85
00:04:15,930 --> 00:04:20,070
so another plot that&#39;s a 2d plot that&#39;s

86
00:04:20,070 --> 00:04:22,860
widely used is a line plot now a line

87
00:04:22,860 --> 00:04:27,060
plot you use whenever you have ordered

88
00:04:27,060 --> 00:04:30,270
data for example a time series or some

89
00:04:30,270 --> 00:04:32,190
sort of be plotting a function or

90
00:04:32,190 --> 00:04:34,020
something where there&#39;s a monotonic

91
00:04:34,020 --> 00:04:35,970
relationship between one variable and

92
00:04:35,970 --> 00:04:38,160
another you couldn&#39;t do that with like

93
00:04:38,160 --> 00:04:40,650
the auto price data versus City miles

94
00:04:40,650 --> 00:04:42,090
per gallon you just get this crazy

95
00:04:42,090 --> 00:04:44,610
squiggly line but in this case i&#39;ve just

96
00:04:44,610 --> 00:04:48,090
created on my screen here

97
00:04:48,090 --> 00:04:51,180
a data set which I with an X variable

98
00:04:51,180 --> 00:04:54,000
which is just one to a hundred and a

99
00:04:54,000 --> 00:04:54,660
what

100
00:04:54,660 --> 00:04:57,120
and then a Y variable which is just x

101
00:04:57,120 --> 00:04:59,630
squared let me

102
00:04:59,630 --> 00:05:01,280
create that so we just have this data

103
00:05:01,280 --> 00:05:06,260
frame DF and i&#39;m just going to plot that

104
00:05:06,260 --> 00:05:10,100
data frame the X versus Y with a line

105
00:05:10,100 --> 00:05:13,990
plot and I&#39;ve given the title

106
00:05:14,260 --> 00:05:17,680
and not too surprisingly my day my plot

107
00:05:17,680 --> 00:05:19,540
looks like half a parabola which is

108
00:05:19,540 --> 00:05:23,710
exactly what it is and you can see as x

109
00:05:23,710 --> 00:05:26,320
increases this way Y increases its

110
00:05:26,320 --> 00:05:31,030
square of X this way on the vertical and

111
00:05:31,030 --> 00:05:33,490
so that&#39;s what i mean by a monotonic so

112
00:05:33,490 --> 00:05:39,450
X Y is increasing fairly steadily

113
00:05:39,600 --> 00:05:41,430
as x increases you can do that if it was

114
00:05:41,430 --> 00:05:43,830
going up and down like a sine function

115
00:05:43,830 --> 00:05:45,750
or anything like that but it it has to

116
00:05:45,750 --> 00:05:46,890
have kind of this one-to-one

117
00:05:46,890 --> 00:05:50,160
relationship why cannot be multivalued

118
00:05:50,160 --> 00:05:57,200
when versus X to get a nice line plot

119
00:05:58,960 --> 00:06:03,400
so those are some 2d plots you can use

120
00:06:03,400 --> 00:06:05,050
to explore the relationships in your

121
00:06:05,050 --> 00:06:09,400
data set and as you can see they in some

122
00:06:09,400 --> 00:06:11,889
cases and a lot more understanding than

123
00:06:11,889 --> 00:06:14,349
maybe some of the 1d plots we tried

124
00:06:14,349 --> 00:06:17,130
earlier

