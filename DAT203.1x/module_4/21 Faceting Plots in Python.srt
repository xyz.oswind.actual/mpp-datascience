0
00:00:00,000 --> 00:00:04,350
so we&#39;ve looked at single variable plots

1
00:00:04,350 --> 00:00:08,490
two variable plots and then in the last

2
00:00:08,490 --> 00:00:11,429
sequence we used a variety of aesthetics

3
00:00:11,429 --> 00:00:16,710
to include multiple dimensions on to our

4
00:00:16,710 --> 00:00:18,510
two-dimensional plot surface that we&#39;re

5
00:00:18,510 --> 00:00:20,820
always stuck with in the visualization

6
00:00:20,820 --> 00:00:23,880
world so we use like color and marker

7
00:00:23,880 --> 00:00:26,099
shape and marker size and things like

8
00:00:26,099 --> 00:00:29,160
that so but there&#39;s other ways we can

9
00:00:29,160 --> 00:00:31,710
look at multiple dimensions we can use

10
00:00:31,710 --> 00:00:35,160
multi-axis plots scatterplot matrices

11
00:00:35,160 --> 00:00:37,649
or we can use a method, a very powerful

12
00:00:37,649 --> 00:00:40,500
method, called faceting that was

13
00:00:40,500 --> 00:00:42,660
introduced by Bill cleveland in that

14
00:00:42,660 --> 00:00:44,820
book i referred to way back at the

15
00:00:44,820 --> 00:00:52,230
beginning of this module so

16
00:00:52,230 --> 00:00:54,420
here&#39;s an example of

17
00:00:54,420 --> 00:00:58,500
pairwise scatterplots so we&#39;re going to

18
00:00:58,500 --> 00:01:01,289
create a number of scatter plots in a

19
00:01:01,289 --> 00:01:03,660
scatterplot matrix we can do that

20
00:01:03,660 --> 00:01:06,930
pairwise we&#39;ve got length curb weight

21
00:01:06,930 --> 00:01:09,000
engine size horsepower city miles per

22
00:01:09,000 --> 00:01:12,000
gallon price so those are numeric

23
00:01:12,000 --> 00:01:14,280
variables that we can scatter plot we

24
00:01:14,280 --> 00:01:17,070
also have fuel type here which is a

25
00:01:17,070 --> 00:01:19,170
categorical variable remember

26
00:01:19,170 --> 00:01:23,490
our cars can be either diesel gasoline

27
00:01:23,490 --> 00:01:28,770
and seaborn has a really powerful pair

28
00:01:28,770 --> 00:01:33,180
plot method so i&#39;m going to use that and

29
00:01:33,180 --> 00:01:34,710
i&#39;m going to of course i have to specify

30
00:01:34,710 --> 00:01:38,130
the subset of my data frame i&#39;m going to

31
00:01:38,130 --> 00:01:44,520
make the color or hue my fuel type i&#39;m

32
00:01:44,520 --> 00:01:47,700
going to use that a color palette that

33
00:01:47,700 --> 00:01:50,430
I&#39;ve selected on the diagonal of this

34
00:01:50,430 --> 00:01:52,380
matrix and include kernel density

35
00:01:52,380 --> 00:01:55,979
estimation plots and on the upper part

36
00:01:55,979 --> 00:01:58,710
of this matrix i&#39;m going to have

37
00:01:58,710 --> 00:02:01,590
2d kernel density estimation plots using

38
00:02:01,590 --> 00:02:06,509
a this color map ok so let me run that

39
00:02:06,509 --> 00:02:09,590
it&#39;s going to take a little while

40
00:02:11,510 --> 00:02:17,989
alright so now we have our scatterplot

41
00:02:17,989 --> 00:02:20,269
matrix so first let&#39;s look at the most

42
00:02:20,269 --> 00:02:23,209
basic part so you see the labels here

43
00:02:23,209 --> 00:02:27,769
like length curb weight engine size

44
00:02:27,769 --> 00:02:30,769
horsepower city miles per gallon price

45
00:02:30,769 --> 00:02:33,530
and then on the other dimension exactly

46
00:02:33,530 --> 00:02:35,420
the same length curb weight engine size

47
00:02:35,420 --> 00:02:37,370
horsepower city miles per gallon and

48
00:02:37,370 --> 00:02:39,709
price so i can look at the relationship

49
00:02:39,709 --> 00:02:44,450
say between price and length here so it

50
00:02:44,450 --> 00:02:48,590
looks like long cars it&#39;s kind of a

51
00:02:48,590 --> 00:02:50,900
curvy relationship but long cars are

52
00:02:50,900 --> 00:02:55,909
generally more expensive than short cars

53
00:02:55,909 --> 00:02:58,370
and we already looked at say curb weight

54
00:02:58,370 --> 00:03:00,170
heavy cars tend to be more expensive

55
00:03:00,170 --> 00:03:03,109
cars with big engines tend to be more

56
00:03:03,109 --> 00:03:05,239
expensive cars with bigger horsepower

57
00:03:05,239 --> 00:03:07,280
tend to be expensive but what if i want

58
00:03:07,280 --> 00:03:09,079
to say what&#39;s the relationship between

59
00:03:09,079 --> 00:03:12,290
engine size and horsepower so get engine

60
00:03:12,290 --> 00:03:14,030
size then i go over here and I find

61
00:03:14,030 --> 00:03:17,989
horsepower and there it is so not

62
00:03:17,989 --> 00:03:20,030
surprisingly bigger engines have more

63
00:03:20,030 --> 00:03:22,370
horsepower and I have all possible

64
00:03:22,370 --> 00:03:24,650
pairwise that&#39;s why it&#39;s called a

65
00:03:24,650 --> 00:03:26,389
pairwise scatterplot all possible

66
00:03:26,389 --> 00:03:29,209
pairwise combinations of those variables

67
00:03:29,209 --> 00:03:32,569
now also you can see I&#39;ve got the

68
00:03:32,569 --> 00:03:37,879
diesel in red and the gas in green so

69
00:03:37,879 --> 00:03:40,160
you get some idea like like here with

70
00:03:40,160 --> 00:03:41,840
City miles per gallon and engine size

71
00:03:41,840 --> 00:03:46,970
that as we already saw that it you

72
00:03:46,970 --> 00:03:52,699
get higher fuel efficiency for a given

73
00:03:52,699 --> 00:03:57,019
engine size with a diesel car than a

74
00:03:57,019 --> 00:04:05,480
gasoline car but you have to pay if we

75
00:04:05,480 --> 00:04:11,419
go back up here to price and we find

76
00:04:11,419 --> 00:04:12,919
city miles per gallon we have to pay

77
00:04:12,919 --> 00:04:15,739
more to get that fuel efficiency that&#39;s

78
00:04:15,739 --> 00:04:17,419
something we already saw with our diesel

79
00:04:17,418 --> 00:04:20,419
cars now I&#39;ve got the kernel density

80
00:04:20,418 --> 00:04:23,419
estimates the 2d kernel density

81
00:04:23,419 --> 00:04:24,680
estimates here

82
00:04:24,680 --> 00:04:27,590
on this upper part of the matrix there

83
00:04:27,590 --> 00:04:30,860
take a little more study but for example

84
00:04:30,860 --> 00:04:33,440
price and city miles per gallon you

85
00:04:33,440 --> 00:04:35,120
again it&#39;s not too surprising we&#39;ve

86
00:04:35,120 --> 00:04:37,190
we&#39;ve looked at this extensively that

87
00:04:37,190 --> 00:04:39,350
plot is simply notice that the axes are

88
00:04:39,350 --> 00:04:41,300
reversed where we have price on the

89
00:04:41,300 --> 00:04:43,639
horizontal and city miles per gallon on

90
00:04:43,639 --> 00:04:47,150
the vertical and all these others so you

91
00:04:47,150 --> 00:04:49,370
can see and they kind of highlight

92
00:04:49,370 --> 00:04:51,020
some of the outliers that&#39;s the main

93
00:04:51,020 --> 00:04:54,410
reason to do this and then down the

94
00:04:54,410 --> 00:04:58,009
diagonal here I&#39;ve got for gasoline in

95
00:04:58,009 --> 00:05:02,030
green and red in red

96
00:05:02,030 --> 00:05:04,099
I&#39;m sorry for gasoline in green and

97
00:05:04,099 --> 00:05:05,479
diesel in red

98
00:05:05,479 --> 00:05:09,590
I&#39;ve got the 1d kernel density estimates

99
00:05:09,590 --> 00:05:16,820
so 2d  kerd 1d for each variable so price

100
00:05:16,820 --> 00:05:18,919
city miles per gallon horsepower center

101
00:05:18,919 --> 00:05:21,289
so you can spend a lot of time studying

102
00:05:21,289 --> 00:05:23,900
the scatterplot matrices also you can

103
00:05:23,900 --> 00:05:27,199
see why I limited how many variables i&#39;m

104
00:05:27,199 --> 00:05:28,970
looking at this is already a little bit

105
00:05:28,970 --> 00:05:32,720
mind-boggling in you might in with a

106
00:05:32,720 --> 00:05:35,090
large number of numeric variables you

107
00:05:35,090 --> 00:05:37,550
probably wind up looking at a set of

108
00:05:37,550 --> 00:05:40,400
possibly overlapping scatterplot

109
00:05:40,400 --> 00:05:43,909
matrices so you just don&#39;t go crazy but

110
00:05:43,909 --> 00:05:46,159
let&#39;s talk about another powerful method

111
00:05:46,159 --> 00:05:48,470
which is fascinating and this is another

112
00:05:48,470 --> 00:05:52,280
area where seaborn package really has

113
00:05:52,280 --> 00:05:56,360
some nice capability so we create what

114
00:05:56,360 --> 00:05:59,930
we call a facet grid for this auto

115
00:05:59,930 --> 00:06:01,699
prices data frame and i&#39;m only going to

116
00:06:01,699 --> 00:06:05,659
do a column by drive wheels

117
00:06:05,659 --> 00:06:09,199
ok so i&#39;m going to divide the columns

118
00:06:09,199 --> 00:06:12,590
but so it&#39;s like a group by basically on

119
00:06:12,590 --> 00:06:16,849
drive wheels and i&#39;m just going to do a

120
00:06:16,849 --> 00:06:21,949
kernel density plot what they call a

121
00:06:21,949 --> 00:06:25,759
density plot grouped by drive wheel so

122
00:06:25,759 --> 00:06:26,900
let&#39;s have a look at what that really

123
00:06:26,900 --> 00:06:30,650
means so the density plot for seaborn

124
00:06:30,650 --> 00:06:33,260
gives me both a histogram and kernel

125
00:06:33,260 --> 00:06:35,860
density estimation plot laid one on an

126
00:06:35,860 --> 00:06:37,389
top of the other which is really nice

127
00:06:37,389 --> 00:06:41,439
and so you can see kind of aspects of

128
00:06:41,439 --> 00:06:43,090
different things depending on which of

129
00:06:43,090 --> 00:06:46,330
these plots are using so here&#39;s for

130
00:06:46,330 --> 00:06:48,699
rear-wheel-drive front-wheel drive and

131
00:06:48,699 --> 00:06:50,110
four-wheel drive which are the only two

132
00:06:50,110 --> 00:06:54,430
possible drive wheel combinations we can

133
00:06:54,430 --> 00:06:56,530
see that rear wheel drive in and well

134
00:06:56,530 --> 00:06:58,150
it&#39;s a little hard to read unfortunately

135
00:06:58,150 --> 00:07:03,699
but rear-wheel-drive cars clearly have a

136
00:07:03,699 --> 00:07:06,550
wider range of price and it looks like

137
00:07:06,550 --> 00:07:08,319
front-wheel drive and four wheel drive

138
00:07:08,319 --> 00:07:11,409
cars are pretty overlapped there&#39;s also

139
00:07:11,409 --> 00:07:15,490
general skew toward lower-priced cars

140
00:07:15,490 --> 00:07:19,300
for really any of these types of Drive

141
00:07:19,300 --> 00:07:21,789
Wheels although there&#39;s this almost

142
00:07:21,789 --> 00:07:24,099
bimodal behavior with rear-wheel-drive

143
00:07:24,099 --> 00:07:28,389
cars so this helps you

144
00:07:28,389 --> 00:07:30,250
so the idea is fascinating is because

145
00:07:30,250 --> 00:07:33,819
you have grouped all cars by what type

146
00:07:33,819 --> 00:07:35,560
of drive wheel they have and then put

147
00:07:35,560 --> 00:07:38,710
these plots side by side you can

148
00:07:38,710 --> 00:07:42,580
start seeing different aspects of the

149
00:07:42,580 --> 00:07:44,409
price and how it corresponds to the

150
00:07:44,409 --> 00:07:47,770
drive wheels now I can do something else

151
00:07:47,770 --> 00:07:50,860
here i can do this plot i can do a

152
00:07:50,860 --> 00:07:54,819
distribution plot and I can use a

153
00:07:54,819 --> 00:07:56,889
different facet grid here in this case

154
00:07:56,889 --> 00:07:58,060
I&#39;m going to use fuel type and

155
00:07:58,060 --> 00:08:02,529
aspiration so i have a columns being

156
00:08:02,529 --> 00:08:04,659
fuel type rose being aspiration so i

157
00:08:04,659 --> 00:08:07,060
have 2 faceting variables are to group by

158
00:08:07,060 --> 00:08:11,020
variables here and there are two values

159
00:08:11,020 --> 00:08:12,430
of each of that so I get this nice

160
00:08:12,430 --> 00:08:15,339
little square grid and I can see for

161
00:08:15,339 --> 00:08:17,919
aspiration equal standard fuel type

162
00:08:17,919 --> 00:08:21,129
equals gas or aspiration equals turbo

163
00:08:21,129 --> 00:08:26,199
fuel type equals gas and then my turbo

164
00:08:26,199 --> 00:08:29,409
diesel and standard diesel cars and I

165
00:08:29,409 --> 00:08:32,469
can see the price and see you know for

166
00:08:32,469 --> 00:08:35,289
example standard aspiration gas cars

167
00:08:35,289 --> 00:08:37,839
have this pretty wide range almost a

168
00:08:37,839 --> 00:08:41,110
bimodal range with some really expensive

169
00:08:41,110 --> 00:08:46,149
cars there is also partly because

170
00:08:46,149 --> 00:08:48,850
there&#39;s very few standard

171
00:08:48,850 --> 00:08:50,800
diesel cars we already saw it in

172
00:08:50,800 --> 00:08:53,019
the scatterplot its kind of bumpy it&#39;s

173
00:08:53,019 --> 00:08:56,440
there&#39;s sort of two modes maybe whereas

174
00:08:56,440 --> 00:08:59,079
the the turbo diesel cars are sort of

175
00:08:59,079 --> 00:09:01,480
spread out and price here over a

176
00:09:01,480 --> 00:09:03,519
reasonable range that mostly overlaps

177
00:09:03,519 --> 00:09:06,519
turbo gas cars, although as we saw it&#39;s a

178
00:09:06,519 --> 00:09:09,190
little more expensive and i can do the

179
00:09:09,190 --> 00:09:15,370
same thing with scatter plots so i can

180
00:09:15,370 --> 00:09:17,769
again create a facet grid I&#39;m gonna do

181
00:09:17,769 --> 00:09:20,589
it by drive wheels on the columns and

182
00:09:20,589 --> 00:09:23,649
body styles and i&#39;m going to do a reg

183
00:09:23,649 --> 00:09:25,690
plot and i&#39;m not going to show the

184
00:09:25,690 --> 00:09:27,759
regression line i could that might be

185
00:09:27,759 --> 00:09:30,670
useful but i&#39;m not in this case so city

186
00:09:30,670 --> 00:09:32,649
miles per gallon vs price which is a

187
00:09:32,649 --> 00:09:39,069
plot we&#39;ve looked at a lot but

188
00:09:39,069 --> 00:09:41,680
now it&#39;s faceted by drive wheels so

189
00:09:41,680 --> 00:09:47,319
we&#39;ve got a front wheel drive rear wheel

190
00:09:47,319 --> 00:09:50,889
drive four-wheel drive and then our body

191
00:09:50,889 --> 00:09:54,430
styles like convertibles hatchback

192
00:09:54,430 --> 00:09:58,720
sedans wagons and hard tops and notice

193
00:09:58,720 --> 00:10:01,839
some of these group elements on the grid

194
00:10:01,839 --> 00:10:04,180
there are no cars there are no

195
00:10:04,180 --> 00:10:06,699
four-wheel drive hard tops for example

196
00:10:06,699 --> 00:10:08,800
so there&#39;s no point there&#39;s only one

197
00:10:08,800 --> 00:10:17,110
rear-wheel-drive hardtop and so they&#39;re

198
00:10:17,110 --> 00:10:19,480
they&#39;re almost all hardtops our front

199
00:10:19,480 --> 00:10:22,630
wheel drive but you can also see it&#39;s a

200
00:10:22,630 --> 00:10:26,800
pretty steep curve of miles per gallon

201
00:10:26,800 --> 00:10:30,189
vs price for hardtops it&#39;s a really

202
00:10:30,189 --> 00:10:36,490
shallow curve for wagons with

203
00:10:36,490 --> 00:10:44,110
front-wheel drive I and sedans again

204
00:10:44,110 --> 00:10:46,660
with front-wheel drive have this kind of

205
00:10:46,660 --> 00:10:49,060
curvy relationship where sedans with

206
00:10:49,060 --> 00:10:52,029
full rear-wheel-drive have a pretty

207
00:10:52,029 --> 00:10:54,220
steep curve and there&#39;s only a few

208
00:10:54,220 --> 00:10:57,610
four-wheel-drive sedans etc so again

209
00:10:57,610 --> 00:11:00,610
take some studying to really bring this

210
00:11:00,610 --> 00:11:02,380
out but the ideas we&#39;ve now group

211
00:11:02,380 --> 00:11:04,450
by two different variables and we&#39;re

212
00:11:04,450 --> 00:11:06,430
seeing different aspects different

213
00:11:06,430 --> 00:11:09,100
facets of this data set which is why

214
00:11:09,100 --> 00:11:12,580
this is called a facet grid let me just

215
00:11:12,580 --> 00:11:15,910
show you one other example here so

216
00:11:15,910 --> 00:11:17,890
remember we spent a lot of time looking

217
00:11:17,890 --> 00:11:20,680
at plot aesthetics so let&#39;s introduce a

218
00:11:20,680 --> 00:11:23,410
few.. one aesthetic into this grid

219
00:11:23,410 --> 00:11:25,630
which is simply we&#39;re going to use hue

220
00:11:25,630 --> 00:11:27,340
for fuel type

221
00:11:27,340 --> 00:11:32,590
ok so we have aspiration and price are

222
00:11:32,590 --> 00:11:35,230
sorry aspiration and body style as are

223
00:11:35,230 --> 00:11:38,200
faceting variables are sorry drive

224
00:11:38,200 --> 00:11:40,270
wheels and body styles are faceting

225
00:11:40,270 --> 00:11:44,950
variables and fuel type as a aesthetic

226
00:11:44,950 --> 00:11:47,560
so that&#39;s three dimensions and then city

227
00:11:47,560 --> 00:11:49,240
miles per gallon vs price we&#39;re going to

228
00:11:49,240 --> 00:11:52,390
have a total of five dimensions on this

229
00:11:52,390 --> 00:11:58,840
plot and you can see pretty clearly

230
00:11:58,840 --> 00:12:02,620
where the turbo cars always lie at

231
00:12:02,620 --> 00:12:09,700
higher prices and city miles per gallon

232
00:12:09,700 --> 00:12:12,790
you know depends whether you again let

233
00:12:12,790 --> 00:12:14,530
steep relationship there&#39;s just that one

234
00:12:14,530 --> 00:12:22,300
diesel car so rear-wheel-drive are sorry

235
00:12:22,300 --> 00:12:26,890
front-wheel-drive sedans have some

236
00:12:26,890 --> 00:12:29,230
diesel cars mostly at the more

237
00:12:29,230 --> 00:12:33,670
fuel-efficient range down here whereas

238
00:12:33,670 --> 00:12:36,640
for sedans with rear-wheel drive it&#39;s

239
00:12:36,640 --> 00:12:40,360
it&#39;s more it&#39;s steeper relationship

240
00:12:40,360 --> 00:12:42,190
again with the diesel cars always

241
00:12:42,190 --> 00:12:45,310
costing more forgiving fuel efficiency

242
00:12:45,310 --> 00:12:48,400
etc so that&#39;s five dimensions on the

243
00:12:48,400 --> 00:12:50,080
plot you can keep going further with

244
00:12:50,080 --> 00:12:52,750
other aesthetics and other faceting

245
00:12:52,750 --> 00:12:56,020
variables but you&#39;ll find that you reach

246
00:12:56,020 --> 00:12:58,180
a point of diminishing return with this

247
00:12:58,180 --> 00:13:01,540
that you you don&#39;t want to use too

248
00:13:01,540 --> 00:13:04,420
complicated a set of plot parameters or

249
00:13:04,420 --> 00:13:07,570
your your your mind as a hard time in

250
00:13:07,570 --> 00:13:09,190
your audience&#39;s mind is a hard time

251
00:13:09,190 --> 00:13:10,960
wrapping their heads around what&#39;s going

252
00:13:10,960 --> 00:13:15,970
so we&#39;ve looked at now another

253
00:13:15,970 --> 00:13:19,300
method besides aesthetics for looking at

254
00:13:19,300 --> 00:13:20,800
more than two dimensions on our

255
00:13:20,800 --> 00:13:23,350
two-dimensional computer screen that is

256
00:13:23,350 --> 00:13:25,810
multi-axis plots are the scatterplot

257
00:13:25,810 --> 00:13:28,269
matrix and this very powerful method of

258
00:13:28,269 --> 00:13:30,910
faceting which is a group

259
00:13:30,910 --> 00:13:35,889
by approach on a grid of facet variables

260
00:13:35,889 --> 00:13:39,189
for different types of plots but if we

261
00:13:39,189 --> 00:13:40,750
only looked at histograms and scatter

262
00:13:40,750 --> 00:13:42,819
plots but it can be any kind of plot

263
00:13:42,819 --> 00:13:46,500
really that you can facet

