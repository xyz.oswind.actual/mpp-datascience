0
00:00:04,712 --> 00:00:06,480
Welcome to introduction to Data Science.

1
00:00:06,480 --> 00:00:08,050
My name&#39;s Cynthia Rudin.

2
00:00:08,050 --> 00:00:09,930
&gt;&gt; And I&#39;m Steve Elston.

3
00:00:09,930 --> 00:00:11,632
&gt;&gt; Now, data science is really hot right now,

4
00:00:11,632 --> 00:00:14,592
and you probably know that if you&#39;ve come this far already.

5
00:00:14,592 --> 00:00:16,060
So data&#39;s coming, right now, from many new sources.

6
00:00:16,059 --> 00:00:18,070
It&#39;s coming from social media.

7
00:00:18,070 --> 00:00:21,620
It&#39;s coming form transportation, from cellular phones,

8
00:00:21,620 --> 00:00:23,030
from advertising.

9
00:00:23,030 --> 00:00:25,420
Almost every company&#39;s collecting it now.

10
00:00:25,420 --> 00:00:28,030
And they know these data are valuable, so

11
00:00:28,030 --> 00:00:29,390
what are they gonna do with it?

12
00:00:29,390 --> 00:00:31,760
I mean, it&#39;s not just gonna sit there, it&#39;s valuable.

13
00:00:31,760 --> 00:00:34,260
And so these companies are gonna hire people to work with it.

14
00:00:34,260 --> 00:00:36,610
They&#39;re gonna hire people to glean insight from it.

15
00:00:36,610 --> 00:00:38,500
They&#39;re gonna build models with it.

16
00:00:38,500 --> 00:00:39,790
And that&#39;s gonna be problematic

17
00:00:39,790 --> 00:00:42,430
because there&#39;s a massive shortage of data scientists.

18
00:00:42,430 --> 00:00:44,571
It takes a while to train someone to do this job.

19
00:00:44,571 --> 00:00:46,990
And we aren&#39;t training people fast enough.

20
00:00:46,990 --> 00:00:49,440
And even if we did, it&#39;s not clear that the people who are

21
00:00:49,440 --> 00:00:52,820
trained as data scientists have a solid grasp of the basics.

22
00:00:52,820 --> 00:00:54,820
Manipulating and cleaning data is only the beginning.

23
00:00:54,820 --> 00:00:55,878
But it&#39;s not enough.

24
00:00:55,878 --> 00:00:58,010
You need to be able to turn data into knowledge.

25
00:00:59,240 --> 00:01:01,845
Also, being a data scientist is really fun.

26
00:01:01,845 --> 00:01:04,508
A data scientist can work in almost company doing anything

27
00:01:04,507 --> 00:01:07,348
from international affairs to charity to corporate work.

28
00:01:07,348 --> 00:01:10,127
Whatever you wanna do, the world is your oyster right now when

29
00:01:10,127 --> 00:01:11,847
you have the skills to work with data.

30
00:01:11,847 --> 00:01:12,449
So Steve and

31
00:01:12,449 --> 00:01:15,047
I are looking forward to teaching you this course.

32
00:01:15,047 --> 00:01:18,520
So let us introduce ourselves first of all.

33
00:01:18,520 --> 00:01:19,910
So I&#39;m Cynthia Rudin.

34
00:01:19,910 --> 00:01:21,930
I&#39;m an associate professor of computer science and

35
00:01:21,930 --> 00:01:23,790
electrical engineering at Duke University.

36
00:01:23,790 --> 00:01:26,040
And also an associate professor of statistic at MIT.

37
00:01:27,050 --> 00:01:29,630
My expertise is in machine learning and data mining.

38
00:01:29,630 --> 00:01:31,988
My lab is called the Prediction Analysis Lab.

39
00:01:31,988 --> 00:01:35,270
My PhD is from Princeton University.

40
00:01:35,270 --> 00:01:38,100
And, a lot of my work is in applied data science and

41
00:01:38,100 --> 00:01:39,440
machine learning.

42
00:01:39,440 --> 00:01:42,290
In particular, I work very often with the electric power

43
00:01:42,290 --> 00:01:45,290
industry, healthcare, and in computational criminology.

44
00:01:48,530 --> 00:01:49,778
&gt;&gt; And I&#39;m Steve Elston.

45
00:01:49,778 --> 00:01:55,500
And I&#39;m co-founder and principal consultant at Quantia Analytics,

46
00:01:55,500 --> 00:01:58,000
a data science consultancy here in Seattle.

47
00:01:59,800 --> 00:02:04,470
And I have several decades of experience in several industries

48
00:02:04,470 --> 00:02:07,580
with predictive analytics and machine learning.

49
00:02:07,580 --> 00:02:11,330
I&#39;ve been a long term R (S/SPLUS), Python user and

50
00:02:11,330 --> 00:02:11,940
developer.

51
00:02:11,940 --> 00:02:16,710
I started using S when it was a Bell Labs research project.

52
00:02:16,710 --> 00:02:20,244
I was involved with the company that commercialize S.

53
00:02:20,244 --> 00:02:22,940
And have long ago made the transition to R.

54
00:02:25,040 --> 00:02:29,420
I&#39;m an advisor to Microsoft on the Azure Machine Learning and

55
00:02:29,420 --> 00:02:31,560
also on some analytics.

56
00:02:33,240 --> 00:02:37,580
And I&#39;ve worked in a variety of industries over the years.

57
00:02:37,580 --> 00:02:42,143
Some examples being payment fraud prevention, telecoms,

58
00:02:42,143 --> 00:02:46,885
capital markets, such as risk, particularly risk models and

59
00:02:46,885 --> 00:02:49,123
logistics, forecasting and

60
00:02:49,123 --> 00:02:52,000
industrial applications like that.

61
00:02:53,100 --> 00:02:54,580
And I have a PhD degree in

62
00:02:54,580 --> 00:02:56,420
geophysics from Princeton University.

63
00:02:58,593 --> 00:03:02,400
&gt;&gt; Now we&#39;ve put a definition of data science up on the screen.

64
00:03:02,400 --> 00:03:06,020
And when you&#39;re doing data science, right, your goal is to

65
00:03:06,020 --> 00:03:09,059
analyze data in a way that you can extract knowledge and

66
00:03:09,059 --> 00:03:12,190
insight from that data,

67
00:03:12,190 --> 00:03:15,440
which allows you to take action in a principle data driven way.

68
00:03:19,313 --> 00:03:23,993
&gt;&gt; So on this slide I&#39;m showing what the whole goal of data

69
00:03:23,993 --> 00:03:25,120
science is.

70
00:03:25,120 --> 00:03:28,310
It&#39;s to take data, transform it into decisions,

71
00:03:28,310 --> 00:03:29,670
which become actions.

72
00:03:29,670 --> 00:03:32,170
And only by making decisions and

73
00:03:32,170 --> 00:03:34,860
taking actions do you derive value.

74
00:03:34,860 --> 00:03:37,579
So on the little graphic here on the left,

75
00:03:37,579 --> 00:03:41,244
I have what&#39;s basically our raw input, which is data.

76
00:03:41,244 --> 00:03:44,988
And over toward the right, our decisions that we

77
00:03:44,988 --> 00:03:49,020
want to create from doing something with that data.

78
00:03:49,020 --> 00:03:52,800
Historically that was often done retrospectively.

79
00:03:52,800 --> 00:03:55,440
A lot of business intelligence,

80
00:03:55,440 --> 00:03:58,270
BI type of analysis is still this way.

81
00:03:58,270 --> 00:03:59,180
You look backwards,

82
00:03:59,180 --> 00:04:02,780
you say, well what happened, where are we kinda thing?

83
00:04:02,780 --> 00:04:05,660
And it&#39;s a very manual process to come to a decision.

84
00:04:07,920 --> 00:04:10,300
Related to that is things like for

85
00:04:10,300 --> 00:04:13,190
example what engineers do at root cause analysis.

86
00:04:13,190 --> 00:04:16,120
You read about this in the paper often, say if there&#39;s been

87
00:04:16,120 --> 00:04:19,470
a transportation accident or something like that.

88
00:04:19,470 --> 00:04:20,695
But again, it&#39;s backward looking.

89
00:04:20,695 --> 00:04:23,454
They looking at what broke, what went wrong,

90
00:04:23,454 --> 00:04:26,090
what can be done to improve it in the future.

91
00:04:26,090 --> 00:04:29,250
So they come to decisions but by backward looking analysis.

92
00:04:30,290 --> 00:04:30,930
In this course,

93
00:04:30,930 --> 00:04:34,555
we&#39;re gonna more think about predictive analytics.

94
00:04:34,555 --> 00:04:39,130
And we&#39;re gonna talk about ways to forecast what

95
00:04:39,130 --> 00:04:40,830
is going to happen.

96
00:04:40,830 --> 00:04:45,012
So, how do we use the history to learn?

97
00:04:45,012 --> 00:04:49,200
And how do you transform that into a decision?

98
00:04:49,200 --> 00:04:52,110
And then ultimately what we really wanna do is,

99
00:04:52,110 --> 00:04:55,192
this top line, we wanna know what to do.

100
00:04:55,192 --> 00:04:58,760
We wanna support decisions, but in many cases,

101
00:04:58,760 --> 00:05:04,750
we can now go directly from a decision to an action.

102
00:05:04,750 --> 00:05:09,394
Say with a real time streaming system or something like that.

103
00:05:09,394 --> 00:05:12,214
So, another way to look at this is there&#39;s several types

104
00:05:12,214 --> 00:05:13,570
of analytics.

105
00:05:13,570 --> 00:05:17,380
Retrospective analytics, which is like

106
00:05:17,380 --> 00:05:21,660
business intelligence is generally where that&#39;s at.

107
00:05:23,470 --> 00:05:24,550
Predictive analytics,

108
00:05:24,550 --> 00:05:27,800
which we&#39;re gonna be focusing a lot on in this course.

109
00:05:29,670 --> 00:05:34,268
Real time analytics, which really is just taking

110
00:05:34,268 --> 00:05:39,413
predictive analytics into a more real-time setting,

111
00:05:39,413 --> 00:05:44,124
where you can go from a new input value to an action.

112
00:05:44,124 --> 00:05:51,640
And more recently, say intelligent apps.

113
00:05:51,640 --> 00:05:55,222
Cortana being on your smartphone being an example of that, where

114
00:05:55,222 --> 00:05:59,350
in real-time you&#39;re interacting with a machine learning system.

115
00:06:01,930 --> 00:06:06,210
So, as data scientist we&#39;re obsessed with datas.

116
00:06:06,210 --> 00:06:09,380
You might imagine, because data is our raw material.

117
00:06:09,380 --> 00:06:10,305
It&#39;s our fuel.

118
00:06:10,305 --> 00:06:14,320
And without data, we really don&#39;t do anything.

119
00:06:14,320 --> 00:06:18,085
So, we spend a lot of time trying to find interesting

120
00:06:18,085 --> 00:06:22,030
sources of data, trying to figure out what&#39;s there.

121
00:06:22,030 --> 00:06:26,843
Often, we have to figure out where the gaps are and

122
00:06:26,843 --> 00:06:28,910
go acquire new data.

123
00:06:30,610 --> 00:06:34,834
Once we have our data it&#39;s invariably never in the form we

124
00:06:34,834 --> 00:06:37,390
want it in, for various reasons.

125
00:06:37,390 --> 00:06:41,235
And so we spend a lot of time cleaning, transforming,

126
00:06:41,235 --> 00:06:42,665
integrating data.

127
00:06:42,665 --> 00:06:44,970
And we&#39;re gonna talk about that in this course.

128
00:06:46,050 --> 00:06:49,610
We need to understand once we have the data kinda organized.

129
00:06:49,610 --> 00:06:50,920
What&#39;s the relationship?

130
00:06:50,920 --> 00:06:53,360
What matters?

131
00:06:53,360 --> 00:06:55,830
What&#39;s superfluous is in this data?

132
00:06:55,830 --> 00:07:00,889
And again, we&#39;ll talk about that extensively in this course.

133
00:07:00,889 --> 00:07:02,698
We need to present our results,

134
00:07:02,698 --> 00:07:05,380
because data science is never done in a vacuum.

135
00:07:05,380 --> 00:07:07,450
You&#39;re part of a larger organization.

136
00:07:07,450 --> 00:07:11,513
You&#39;re presenting results to management to help them make

137
00:07:11,513 --> 00:07:12,412
decisions.

138
00:07:12,412 --> 00:07:16,287
You&#39;re showing how you&#39;ve improved, say, a real-time

139
00:07:16,287 --> 00:07:20,580
process like preventing payment fraud or something like that.

140
00:07:20,580 --> 00:07:24,300
So, there&#39;s an art to presenting your results in a way that

141
00:07:24,300 --> 00:07:26,280
the rest of your organization can understand.

142
00:07:26,280 --> 00:07:28,250
And we&#39;ll talk a little bit about that when we

143
00:07:28,250 --> 00:07:29,670
talk about visualization.

144
00:07:30,850 --> 00:07:34,234
And the final bottom line really is, to get

145
00:07:34,234 --> 00:07:39,036
back to the main point here, is delivering value from data.

146
00:07:39,036 --> 00:07:42,963
So if we don&#39;t deliver value from the data, if we don&#39;t turn

147
00:07:42,963 --> 00:07:46,582
raw data into a valuable intellectual property asset,

148
00:07:46,582 --> 00:07:49,995
we&#39;re not really doing our job as data scientists.

149
00:07:49,995 --> 00:07:56,550
So, why would you wanna do this particular course?

150
00:07:56,550 --> 00:08:01,380
So, first of all, we&#39;re going to focus on some of the basics

151
00:08:01,380 --> 00:08:04,970
skill you need to convincingly explain your evidence.

152
00:08:04,970 --> 00:08:09,640
Data sciences and evidence base discipline,

153
00:08:09,640 --> 00:08:11,420
because we&#39;re focusing on data and

154
00:08:11,420 --> 00:08:13,292
the corrected interpretation of data.

155
00:08:13,292 --> 00:08:17,783
Primarily Cynthia&#39;s going to explain a lot

156
00:08:17,783 --> 00:08:21,552
about probability and statistics.

157
00:08:21,552 --> 00:08:24,659
But in a way that helps you solidify that story, helps you

158
00:08:24,659 --> 00:08:28,170
explain what you&#39;re doing and understand what you&#39;re doing.

159
00:08:29,250 --> 00:08:31,940
So the focus is gonna be on the core ideas.

160
00:08:31,940 --> 00:08:35,459
It&#39;s not gonna be a long theory lecture, so

161
00:08:35,458 --> 00:08:37,559
don&#39;t worry about that.

162
00:08:37,558 --> 00:08:41,102
You&#39;re gonna develop skills in working with and exploring data,

163
00:08:41,102 --> 00:08:43,050
which I already alluded to.

164
00:08:43,049 --> 00:08:46,940
Very important to understand what your data are and

165
00:08:46,940 --> 00:08:49,840
how they relate before you get to far.

166
00:08:49,840 --> 00:08:52,623
And we&#39;re gonna provide some case studies and

167
00:08:52,623 --> 00:08:56,334
some experience for you through the labs with programming and

168
00:08:56,334 --> 00:08:59,560
using state-of-the-art data science software.

169
00:09:01,370 --> 00:09:07,012
And I&#39;m also gonna show this in demos as well.

170
00:09:07,012 --> 00:09:10,885
And we&#39;re gonna focus on Azure ML, Python and

171
00:09:10,885 --> 00:09:13,871
R, and you have your choice there.

172
00:09:13,871 --> 00:09:15,960
So how do you get the most from this course?

173
00:09:15,960 --> 00:09:19,408
If you&#39;re gonna invest the time, what should you do?

174
00:09:19,408 --> 00:09:22,120
For each module there&#39;s lectures,

175
00:09:22,120 --> 00:09:24,620
which you watch online.

176
00:09:24,620 --> 00:09:26,850
There&#39;s demos, which I&#39;ll present to you.

177
00:09:26,850 --> 00:09:29,650
And there&#39;s labs, which you can do on your own.

178
00:09:31,786 --> 00:09:34,642
And the labs are there to reinforce the key concepts that

179
00:09:34,642 --> 00:09:37,210
we&#39;re presenting in the lectures and the demos.

180
00:09:39,600 --> 00:09:42,090
And you&#39;ll use Azure Machine Learning.

181
00:09:42,090 --> 00:09:45,530
And you can use either R or Python to perform the lab.

182
00:09:45,530 --> 00:09:50,289
So, I suggest you think about, do you wanna use R or Python?

183
00:09:50,289 --> 00:09:54,875
Every lab has exactly the same material covered in

184
00:09:54,875 --> 00:09:56,830
both languages.

185
00:09:56,830 --> 00:09:59,720
If you&#39;re extremely ambitious, you&#39;re certainly

186
00:09:59,720 --> 00:10:02,600
welcome to do both, but it&#39;s far from required.

187
00:10:02,600 --> 00:10:05,164
And we look forward to having you in this course, and

188
00:10:05,164 --> 00:10:06,632
we hope you got a lot out of it.

