0
00:00:04,965 --> 00:00:08,764
So today we&#39;re going to talk about the data science process.

1
00:00:08,764 --> 00:00:12,592
And in particular we&#39;ll give historical notes on the KDD

2
00:00:12,592 --> 00:00:14,993
process, the CRISP-DM process.

3
00:00:14,993 --> 00:00:16,426
Big Data and Data Science and

4
00:00:16,426 --> 00:00:19,422
their relationships to Data Mining and Machine Learning.

5
00:00:19,422 --> 00:00:21,328
And I&#39;ll go through an example of the knowledge

6
00:00:21,328 --> 00:00:22,190
discovery process.

7
00:00:25,234 --> 00:00:27,210
Okay, let&#39;s start with some historical notes.

8
00:00:28,250 --> 00:00:30,960
So the term Big Data was coined by these two

9
00:00:30,960 --> 00:00:31,732
astronomers in 1997.

10
00:00:31,732 --> 00:00:36,170
So it&#39;s an old term that got a bit hyped up a few years ago

11
00:00:36,170 --> 00:00:38,860
after the CCC Whitepapers on big data came out.

12
00:00:40,040 --> 00:00:44,170
Now, I&#39;m just showing you here the CCC BIg Data Pipeline from

13
00:00:44,170 --> 00:00:45,130
the white paper in 2012.

14
00:00:45,130 --> 00:00:50,220
It&#39;s nice, it&#39;s a nice effort to formalize the scientific

15
00:00:50,220 --> 00:00:55,000
process that goes into discovering knowledge from data.

16
00:00:55,000 --> 00:00:56,280
It&#39;s not just data mining,

17
00:00:56,280 --> 00:00:59,480
data mining, there&#39;s a lot more steps to it.

18
00:00:59,480 --> 00:01:01,611
In fact data mining is actually in here,

19
00:01:01,611 --> 00:01:04,062
it&#39;s actually in the fourth step out of five.

20
00:01:04,062 --> 00:01:07,564
And the major steps in the analysis of big data are shown

21
00:01:07,564 --> 00:01:09,099
in this flow at the top.

22
00:01:09,099 --> 00:01:12,554
But then these are big data needs that make these tasks up

23
00:01:12,554 --> 00:01:13,810
here challenging.

24
00:01:15,200 --> 00:01:17,763
So let me go through the five steps.

25
00:01:17,763 --> 00:01:21,107
So perhaps first you have to through a process of actually

26
00:01:21,107 --> 00:01:22,368
obtaining the data.

27
00:01:22,368 --> 00:01:25,210
And perhaps that involves recording how users behave on

28
00:01:25,210 --> 00:01:27,031
different websites or something.

29
00:01:27,031 --> 00:01:29,980
So you have to write a script to collect the data properly.

30
00:01:31,500 --> 00:01:33,590
Then the second step is extraction or

31
00:01:33,590 --> 00:01:34,844
cleaning or annotation.

32
00:01:34,844 --> 00:01:37,340
Where you try to reduce the noise a bit, and

33
00:01:37,340 --> 00:01:39,480
get rid of the data you don&#39;t directly need.

34
00:01:39,480 --> 00:01:41,730
Or if you&#39;re working with free text that&#39;s easy to annotate,

35
00:01:41,730 --> 00:01:44,220
you might be able to turn it into a structured table.

36
00:01:45,690 --> 00:01:46,620
So basically at this step,

37
00:01:46,620 --> 00:01:49,360
you&#39;re turning the data from what is potentially a pile of

38
00:01:49,360 --> 00:01:51,910
rubbish into something you might actually be able to work with.

39
00:01:53,590 --> 00:01:57,790
Now this third step, which is integration, aggregation, and

40
00:01:57,790 --> 00:01:58,645
representation.

41
00:01:58,645 --> 00:02:02,881
You&#39;re gonna set up the data in a way that&#39;s directly conducive

42
00:02:02,881 --> 00:02:03,987
to data mining.

43
00:02:03,987 --> 00:02:06,854
So all the text documents are gonna be linked properly to

44
00:02:06,854 --> 00:02:08,263
other structured tables.

45
00:02:08,263 --> 00:02:10,887
And you&#39;ve got one central database where all that

46
00:02:10,887 --> 00:02:12,970
information is stored nice and neatly.

47
00:02:15,450 --> 00:02:17,660
And then this is the analysis and modeling step,

48
00:02:17,660 --> 00:02:19,520
which is where all of the great machine learning and

49
00:02:19,520 --> 00:02:20,590
data mining takes place.

50
00:02:20,590 --> 00:02:24,844
And then the interpretation step is where at the end you see what

51
00:02:24,844 --> 00:02:27,661
you&#39;ve got and you really interpret it.

52
00:02:27,661 --> 00:02:30,451
Okay so this is the big data pipeline.

53
00:02:30,451 --> 00:02:35,093
And now I&#39;m gonna show you another separate big

54
00:02:35,093 --> 00:02:39,620
data pipeline constructed by a separate group

55
00:02:39,620 --> 00:02:42,531
of people at a separate time.

56
00:02:42,531 --> 00:02:46,062
And the second group of people, these are also people trying to

57
00:02:46,062 --> 00:02:49,466
formalize the process of knowledge discovery, you ready?

58
00:02:51,678 --> 00:02:55,302
Okay, so this is what&#39;s called the KDD Process,

59
00:02:55,302 --> 00:02:57,980
Knowledge Discovery in Databases,

60
00:02:57,980 --> 00:03:01,110
which was published way back in 1996.

61
00:03:01,110 --> 00:03:05,293
And it has these five steps, so selection, preprocessing,

62
00:03:05,293 --> 00:03:08,835
transformation, data mining, interpretation,

63
00:03:08,835 --> 00:03:10,450
does it look familiar?

64
00:03:10,450 --> 00:03:14,328
It should, because these are the same five steps that appeared on

65
00:03:14,328 --> 00:03:15,604
the previous slide.

66
00:03:15,604 --> 00:03:17,074
The same exact steps, so

67
00:03:17,074 --> 00:03:20,225
you&#39;re probably not surprised, you say, so what,

68
00:03:20,225 --> 00:03:23,527
two groups of people they come up with the same diagram.

69
00:03:23,527 --> 00:03:26,654
But, look, this diagram&#39;s from 1996.

70
00:03:26,654 --> 00:03:30,953
And an interesting piece of this whole story is that the CCC

71
00:03:30,953 --> 00:03:32,707
people, in 2012,

72
00:03:32,707 --> 00:03:37,110
they actually didn&#39;t cite the original KDD paper.

73
00:03:37,110 --> 00:03:39,860
So, it&#39;s actually possible that they

74
00:03:39,860 --> 00:03:41,700
didn&#39;t know about each other.

75
00:03:41,700 --> 00:03:44,840
Two completely separate groups of people coming together and

76
00:03:44,840 --> 00:03:50,770
producing exactly the same thing, several years apart.

77
00:03:50,770 --> 00:03:53,280
Perhaps there&#39;s something fundamental about this process.

78
00:03:53,280 --> 00:03:55,650
Maybe it&#39;s like an important number like pi or

79
00:03:55,650 --> 00:03:57,290
the golden ratio or something.

80
00:03:57,290 --> 00:03:59,970
Maybe this is a universal process that maybe

81
00:03:59,970 --> 00:04:02,460
anyone who works with data will discover.

82
00:04:02,460 --> 00:04:04,230
Anyway, so this is the KDD process.

83
00:04:06,840 --> 00:04:09,920
Now, it seems so abstract, but there might actually be

84
00:04:09,920 --> 00:04:12,420
something fundamental about this.

85
00:04:12,420 --> 00:04:15,690
By the way, reading both of these articles is really

86
00:04:15,690 --> 00:04:16,800
inspiring.

87
00:04:16,800 --> 00:04:22,028
One thing I wanna point out, again, is that in both cases,

88
00:04:22,028 --> 00:04:26,732
data mining is only a part of this process, one part,

89
00:04:26,732 --> 00:04:28,000
not all of it.

90
00:04:28,000 --> 00:04:32,760
Granted, it&#39;s like the climax of a story, but like any story,

91
00:04:32,760 --> 00:04:37,007
the set up of the story really makes the whole thing work.

92
00:04:37,007 --> 00:04:41,058
Now, KDD wasn&#39;t the only game in town before the CCC white

93
00:04:41,058 --> 00:04:41,726
papers.

94
00:04:41,726 --> 00:04:45,230
So this CRISP-DM, the CRoss Industry Standard Process for

95
00:04:45,230 --> 00:04:46,060
Data Mining.

96
00:04:46,060 --> 00:04:48,829
And these guys were really serious about formalizing

97
00:04:48,829 --> 00:04:50,680
the knowledge discovery process.

98
00:04:50,680 --> 00:04:54,860
They wrote a 77 page manual on how to do this stuff.

99
00:04:54,860 --> 00:04:58,180
And I actually like CRISP-DM&#39;s process the best.

100
00:04:58,180 --> 00:05:00,750
Because it includes business understanding and

101
00:05:00,750 --> 00:05:03,450
data understanding, which I think are important.

102
00:05:03,450 --> 00:05:06,455
And I also love all of these backward arrows.

103
00:05:06,455 --> 00:05:10,490
That show how many iterations you realistically do

104
00:05:10,490 --> 00:05:13,040
within this process on a regular basis.

105
00:05:13,040 --> 00:05:16,222
So anyway this is the process that I tend to think of when I

106
00:05:16,222 --> 00:05:18,271
think of the data science process.

107
00:05:20,485 --> 00:05:24,351
So just to summarize, the stages are basically the same.

108
00:05:24,351 --> 00:05:25,477
No matter who invents or

109
00:05:25,477 --> 00:05:28,013
reinvents this knowledge discovery, data mining,

110
00:05:28,013 --> 00:05:30,380
big data or data science process.

111
00:05:30,380 --> 00:05:35,330
You may not always need all the stages, and

112
00:05:35,330 --> 00:05:36,880
data science is an iterative process.

113
00:05:36,880 --> 00:05:40,150
There is backward arrows on most of those process diagrams.

114
00:05:40,150 --> 00:05:46,128
And those backward arrows are really part of the process,

115
00:05:46,128 --> 00:05:49,788
you never end up doing it just once.

116
00:05:49,788 --> 00:05:53,150
Let&#39;s go through an example of the knowledge discovery process.

117
00:05:53,150 --> 00:05:57,909
And in particular I&#39;m gonna walk you through an example that I

118
00:05:57,909 --> 00:05:58,830
worked on.

119
00:05:58,830 --> 00:06:01,499
Which is the process of predicting

120
00:06:01,499 --> 00:06:03,896
power failures in Manhattan.

121
00:06:03,896 --> 00:06:07,311
So, the motivation of this example is that in New York city

122
00:06:07,311 --> 00:06:09,901
the peak demand for electricity is rising.

123
00:06:09,901 --> 00:06:12,696
But the infrastructure that&#39;s supporting New York actually

124
00:06:12,696 --> 00:06:16,300
dates back to the 1880s from the time of Thomas Edison.

125
00:06:16,300 --> 00:06:19,293
And power failures occur in New York fairly often,

126
00:06:19,293 --> 00:06:21,172
enough to do statistics on, and

127
00:06:21,172 --> 00:06:24,174
they&#39;re actually somewhat expensive to repair.

128
00:06:24,174 --> 00:06:28,614
So our goal was to figure out how to prioritize which manholes

129
00:06:28,614 --> 00:06:30,539
were gonna be inspected.

130
00:06:30,539 --> 00:06:34,939
In order to optimally reduce the number of manhole events in

131
00:06:34,939 --> 00:06:35,910
the future.

132
00:06:35,910 --> 00:06:38,781
And a manhole event is a either a fire or an explosion, or

133
00:06:38,781 --> 00:06:39,827
a smoking manhole.

134
00:06:42,636 --> 00:06:44,212
And this is a real problem,

135
00:06:44,212 --> 00:06:46,550
this is something I actually worked on.

136
00:06:47,930 --> 00:06:50,510
So let&#39;s go through the stages in the knowledge discovery

137
00:06:50,510 --> 00:06:51,610
process.

138
00:06:51,610 --> 00:06:54,819
And, of course, so they&#39;re opportunity assessment and

139
00:06:54,819 --> 00:06:58,890
business understanding, tjis is CRISP-DM&#39;s process.

140
00:06:58,890 --> 00:07:01,380
Data understanding and data acquisition, preparation,

141
00:07:01,380 --> 00:07:04,400
including model building and policy construction.

142
00:07:04,400 --> 00:07:06,150
And if you want the 77 page description,

143
00:07:06,150 --> 00:07:08,360
you&#39;re welcome to look at the CRISP-DM manual.

144
00:07:09,670 --> 00:07:14,904
Okay, so evaluation and then model deployment.

145
00:07:14,904 --> 00:07:19,095
So what do you really want to accomplish in opportunity

146
00:07:19,095 --> 00:07:23,662
assessment and business understanding, the first step.

147
00:07:23,662 --> 00:07:25,640
What do you really want to accomplish?

148
00:07:25,640 --> 00:07:28,050
What are your constraints, what are your risks?

149
00:07:28,050 --> 00:07:30,960
And how are you gonna evaluate the quality of the results?

150
00:07:30,960 --> 00:07:34,210
And these goals are concrete, we can form concrete assessments of

151
00:07:34,210 --> 00:07:36,086
how well we did with these tasks.

152
00:07:36,086 --> 00:07:40,530
So for manhole events, our goal was just

153
00:07:40,530 --> 00:07:44,560
generally predict fires and explosions before they occur.

154
00:07:44,560 --> 00:07:46,919
And so we tried to make that more precise.

155
00:07:48,520 --> 00:07:52,190
So the first goal was to assess predictive accuracy for

156
00:07:52,190 --> 00:07:55,380
predicting manhole events in the year before they happen.

157
00:07:55,380 --> 00:07:58,164
So we specified a particular timeframe, and

158
00:07:58,164 --> 00:08:00,440
we defined what a manhole event was.

159
00:08:02,220 --> 00:08:05,150
And then the second goal was to create a cost-benefit analysis

160
00:08:05,150 --> 00:08:07,940
for the inspection policies.

161
00:08:07,940 --> 00:08:11,180
These new inspection policies that take into account the cost

162
00:08:11,180 --> 00:08:14,190
of doing the inspection, but also the cost of a fire.

163
00:08:14,190 --> 00:08:16,970
So these trade off with each other cuz if you do more

164
00:08:16,970 --> 00:08:19,010
inspections you have less fires.

165
00:08:19,010 --> 00:08:22,640
And if you do less inspections you have more fires.

166
00:08:22,640 --> 00:08:25,390
And that will help you optimally determine how often

167
00:08:25,390 --> 00:08:26,780
the manholes need to be inspected.

168
00:08:28,500 --> 00:08:32,580
So this is the second step, data understanding and

169
00:08:32,580 --> 00:08:33,390
data acquisition.

170
00:08:33,390 --> 00:08:37,624
Now the data that we had were trouble tickets.

171
00:08:37,624 --> 00:08:40,465
Which are free text documents that were typed by

172
00:08:40,465 --> 00:08:42,819
the dispatchers in the power company.

173
00:08:42,818 --> 00:08:46,191
While they were directing the action of

174
00:08:46,191 --> 00:08:48,754
what to do about the problem.

175
00:08:48,754 --> 00:08:51,961
And they were taping these all in while this was going on.

176
00:08:54,147 --> 00:08:57,708
We had also a lot of records of information about the manholes.

177
00:08:57,708 --> 00:09:03,140
Where they were located, and what type of cover they had, and

178
00:09:03,140 --> 00:09:05,400
other basic information about manholes.

179
00:09:05,400 --> 00:09:08,020
Whether it was a manhole proper or a service box.

180
00:09:09,430 --> 00:09:12,160
We also had a lot of records of information about underground

181
00:09:12,160 --> 00:09:13,220
cables.

182
00:09:13,220 --> 00:09:15,875
And one major challenge in all of this was

183
00:09:15,875 --> 00:09:19,556
that the cable data was not matched to the manhole data.

184
00:09:19,556 --> 00:09:23,851
We had to do it ourselves, and so what happened was when we

185
00:09:23,851 --> 00:09:28,512
tried to match up which cables was match with which manholes.

186
00:09:28,512 --> 00:09:31,731
We actually lost about half the cable records in the process.

187
00:09:31,731 --> 00:09:34,732
So we had to do a lot of data cleaning to try to make that

188
00:09:34,732 --> 00:09:36,860
do that data integration properly.

189
00:09:36,860 --> 00:09:41,460
And then we also had electrical shock information tables,

190
00:09:42,470 --> 00:09:44,590
and information about, extra information,

191
00:09:44,590 --> 00:09:45,990
about these serious events.

192
00:09:45,990 --> 00:09:49,311
There was a separate table that had that information.

193
00:09:49,311 --> 00:09:52,407
We had inspection reports and also vented cover data.

194
00:09:52,407 --> 00:09:56,477
Which manholes had special vented covers and

195
00:09:56,477 --> 00:10:00,040
which ones had solid covers?

196
00:10:00,040 --> 00:10:02,768
The inspection report data was interesting because

197
00:10:02,768 --> 00:10:05,129
the inspection program was relatively new.

198
00:10:05,129 --> 00:10:09,553
They weren&#39;t inspecting manholes up until almost the start of

199
00:10:09,553 --> 00:10:12,484
when we started working on this project.

200
00:10:12,484 --> 00:10:16,277
So this was brand new data that we were working with, and

201
00:10:16,277 --> 00:10:18,790
its quality was evolving over time.

202
00:10:21,070 --> 00:10:25,668
Now you probably have heard of the four Vs of big data.

203
00:10:25,668 --> 00:10:33,390
So velocity, variety and volume and veracity.

204
00:10:33,390 --> 00:10:35,430
So two of those, variety and

205
00:10:35,430 --> 00:10:39,120
veracity, those are challenges that we had to face.

206
00:10:39,120 --> 00:10:41,170
There was a huge variety of different data,

207
00:10:41,170 --> 00:10:45,130
it was in all different formats and veracity also was an issue.

208
00:10:45,130 --> 00:10:48,680
So veracity means, how trustworthy is the data?

209
00:10:48,680 --> 00:10:51,430
And we had different data with different levels of trust

210
00:10:51,430 --> 00:10:54,240
worthiness that we had to be very careful about how exactly

211
00:10:54,240 --> 00:10:54,940
we used the data.

212
00:10:54,940 --> 00:10:57,100
And made sure that we depended on the right data, and

213
00:10:57,100 --> 00:10:58,540
didn&#39;t depend too much on the wrong data.

214
00:11:00,760 --> 00:11:02,820
So that just boils down to the question,

215
00:11:02,820 --> 00:11:05,630
well how do you know what data that you should trust?

216
00:11:08,140 --> 00:11:10,685
So the next step in the process is data cleaning and

217
00:11:10,685 --> 00:11:11,347
transformation.

218
00:11:12,390 --> 00:11:15,520
And obviously sometimes that is actually 99% of the work.

219
00:11:15,520 --> 00:11:17,880
The machine learning gets to take all of the credit, but

220
00:11:17,880 --> 00:11:20,770
actually the data cleaning is a huge amount of work.

221
00:11:22,450 --> 00:11:25,729
Before this effort, you might have a giant pile of garbage,

222
00:11:25,729 --> 00:11:28,060
and you&#39;re supposed to turn it into gold.

223
00:11:29,100 --> 00:11:32,320
So for us we had to turn all of the free text

224
00:11:32,320 --> 00:11:33,600
into structured information.

225
00:11:33,600 --> 00:11:36,253
So we had trouble tickets, and we had to turn each

226
00:11:36,253 --> 00:11:39,644
trouble ticket into a vector that looks something like this.

227
00:11:39,644 --> 00:11:40,847
So it’s a collection of,

228
00:11:40,847 --> 00:11:42,543
a vector is a collection of numbers,

229
00:11:42,543 --> 00:11:45,240
and we had to turn it into something that looked like this.

230
00:11:45,240 --> 00:11:46,950
So was the manhole,

231
00:11:46,950 --> 00:11:49,570
was the trouble ticket representing a serious event or

232
00:11:49,570 --> 00:11:52,760
less serious event or was it just not an event?

233
00:11:52,760 --> 00:11:55,560
What year, month, day was it in?

234
00:11:55,560 --> 00:11:57,630
What manholes were involved in it?

235
00:11:57,630 --> 00:11:59,866
And all this other kind of information.

236
00:11:59,866 --> 00:12:05,280
And then again, [LAUGH] as I mentioned,

237
00:12:05,280 --> 00:12:08,210
trying to integrate the tables is particularly difficult.

238
00:12:09,580 --> 00:12:13,700
And so, just to try to join the manholes to what

239
00:12:13,700 --> 00:12:17,236
cables they fit into, we just lost half the cable records.

240
00:12:17,236 --> 00:12:20,110
And so we had to do a lot of work trying to

241
00:12:20,110 --> 00:12:24,590
make sure the integrity of that table join was good.

242
00:12:27,500 --> 00:12:28,090
So I like to

243
00:12:28,090 --> 00:12:31,680
think of the knowledge discovery process sort of like alchemy.

244
00:12:31,680 --> 00:12:34,960
Alchemy is where you try to turn lead into gold,

245
00:12:34,960 --> 00:12:38,000
except that alchemy doesn&#39;t work, but data often does.

246
00:12:39,160 --> 00:12:41,970
In business understanding, it&#39;s like,

247
00:12:41,970 --> 00:12:45,020
am I really going to get gold out of this?

248
00:12:45,020 --> 00:12:47,160
What constitutes gold here?

249
00:12:47,160 --> 00:12:48,870
And, data understanding is sort of like,

250
00:12:49,980 --> 00:12:51,960
what are my raw materials that I&#39;m going to collect?

251
00:12:53,220 --> 00:12:56,374
And then data preparation over here, that&#39;s an important step.

252
00:12:56,374 --> 00:12:59,255
That&#39;s where you purify all the raw ingredients and

253
00:12:59,255 --> 00:13:01,951
you get rid of all the junk and the contaminants.

254
00:13:01,951 --> 00:13:06,245
And then modeling, the modeling step, that&#39;s where you actually

255
00:13:06,245 --> 00:13:09,875
do the transformation of the raw ingredients into gold.

256
00:13:09,875 --> 00:13:12,681
And that&#39;s why people are obsessed with it, and

257
00:13:12,681 --> 00:13:16,260
that&#39;s why other people think it&#39;s magic.

258
00:13:16,260 --> 00:13:19,280
It really, as long as you did the data processing right,

259
00:13:19,280 --> 00:13:21,280
this stuff is generally pretty easy.

260
00:13:21,280 --> 00:13:23,494
And it does work like magic, but

261
00:13:23,494 --> 00:13:26,368
you&#39;ll see it&#39;s definitely not magic.

262
00:13:26,368 --> 00:13:30,459
Now for the evaluations step, that&#39;s where appraiser comes and

263
00:13:30,459 --> 00:13:32,551
tells you how much gold is worth.

264
00:13:32,551 --> 00:13:36,373
And then of course deployment is where the whole village comes to

265
00:13:36,373 --> 00:13:37,303
buy your gold.

266
00:13:37,303 --> 00:13:42,113
Okay, back to the modelling step.

267
00:13:42,113 --> 00:13:47,071
Now, predictive modeling means machine learning or

268
00:13:47,071 --> 00:13:49,740
statistical modeling.

269
00:13:49,740 --> 00:13:53,170
It doesn&#39;t necessarily mean prediction in the future

270
00:13:53,170 --> 00:13:54,505
time wise.

271
00:13:54,505 --> 00:13:58,855
It could just mean prediction of a new circumstance that you

272
00:13:58,855 --> 00:14:00,598
haven&#39;t seen before.

273
00:14:00,598 --> 00:14:04,252
Now if your goal is to answer yes or no questions,

274
00:14:04,252 --> 00:14:06,583
then this is classification.

275
00:14:06,583 --> 00:14:09,838
So for instance if you&#39;re asking questions like,

276
00:14:09,838 --> 00:14:13,330
will a manhole explode next year yes or no?

277
00:14:13,330 --> 00:14:14,898
This is a classification problem.

278
00:14:14,898 --> 00:14:20,020
If you wanna predict the numerical value,

279
00:14:20,020 --> 00:14:20,990
this is regression.

280
00:14:23,098 --> 00:14:26,530
If you wanna predict how many bicycles are going to be

281
00:14:26,530 --> 00:14:29,090
rented next year, that&#39;s a regression problem.

282
00:14:30,660 --> 00:14:34,936
And then, if you want to group observations into similar

283
00:14:34,936 --> 00:14:39,402
looking groups, that&#39;s actually a clustering problem.

284
00:14:39,402 --> 00:14:42,733
If you wanna recommend someone an item, like a book or

285
00:14:42,733 --> 00:14:46,444
a movie or a product based on ratings from other customers.

286
00:14:46,444 --> 00:14:48,370
Then this is a recommendation system.

287
00:14:49,950 --> 00:14:52,160
And I should mention to you that there are many,

288
00:14:52,160 --> 00:14:54,900
many other machine learning problems out there.

289
00:14:54,900 --> 00:14:57,252
I&#39;ve just told you four important categories of machine

290
00:14:57,252 --> 00:14:59,702
learning problems, but there are many, many other ones.

291
00:15:03,667 --> 00:15:05,852
Okay, so policy construction,

292
00:15:05,852 --> 00:15:09,136
how&#39;s your model gonna be used to change policy?

293
00:15:09,136 --> 00:15:12,124
So for manholes, how should we recommend changing

294
00:15:12,124 --> 00:15:14,634
the inspection policy based on our model?

295
00:15:14,634 --> 00:15:16,910
How can we construct something that&#39;s actionable?

296
00:15:18,480 --> 00:15:20,900
So for example let&#39;s consider using social media and

297
00:15:20,900 --> 00:15:22,510
customer purchase data.

298
00:15:22,510 --> 00:15:25,850
To determine customer participation, if Starbucks,

299
00:15:25,850 --> 00:15:28,330
say, moves into a new city.

300
00:15:28,330 --> 00:15:29,460
After the model&#39;s created,

301
00:15:29,460 --> 00:15:32,860
how do you do that optimization where the shops are located?

302
00:15:32,860 --> 00:15:34,940
How big they should be, and

303
00:15:34,940 --> 00:15:36,830
where the warehouses are located?

304
00:15:36,830 --> 00:15:38,330
That&#39;s an optimization problem.

305
00:15:38,330 --> 00:15:40,530
So, this is a policy construction problem.

306
00:15:43,240 --> 00:15:47,040
To model building that part the last that&#39;s predictive,

307
00:15:47,040 --> 00:15:49,680
policy construction is prescriptive.

308
00:15:49,680 --> 00:15:52,216
So policy construction usually involves solving some

309
00:15:52,216 --> 00:15:54,041
complicated optimization problems.

310
00:15:56,345 --> 00:15:57,613
Evaluation, so

311
00:15:57,613 --> 00:16:01,780
how do you measure the quality of the results?

312
00:16:01,780 --> 00:16:05,495
Evaluation can be difficult if the data don&#39;t actually provide

313
00:16:05,495 --> 00:16:06,396
ground truth.

314
00:16:06,396 --> 00:16:08,578
But luckily for the manhole problem,

315
00:16:08,578 --> 00:16:11,247
we were working with engineers at Con Edison.

316
00:16:11,247 --> 00:16:15,303
And what they did was they withheld a lot of high quality

317
00:16:15,303 --> 00:16:16,369
recent data.

318
00:16:16,369 --> 00:16:20,993
So they could use it to test our predictive models to see whether

319
00:16:20,993 --> 00:16:23,969
we had predicted events that happened.

320
00:16:23,969 --> 00:16:27,455
Between when the model was built and when they actually happened.

321
00:16:27,455 --> 00:16:29,477
And that&#39;s how they knew that they could trust us,

322
00:16:29,477 --> 00:16:30,532
cuz this was a blind test.

323
00:16:30,532 --> 00:16:36,110
So deployment is the last step,

324
00:16:36,110 --> 00:16:39,650
this is like, hey, I turned lead into gold.

325
00:16:42,390 --> 00:16:47,710
And I will warn you that getting a working proof of concept

326
00:16:47,710 --> 00:16:51,619
deployed, it actually stops, I would say 95% of projects.

327
00:16:51,619 --> 00:16:54,280
Cuz it&#39;s like, hey I turned lead into gold, and

328
00:16:54,280 --> 00:16:56,650
the reaction is, well that&#39;s nice.

329
00:16:58,500 --> 00:17:02,300
You need to actually get it implemented.

330
00:17:02,300 --> 00:17:05,019
So my suggestion is just don&#39;t even bother

331
00:17:05,019 --> 00:17:08,569
doing it in the first place if nobody plans to deploy it,

332
00:17:08,569 --> 00:17:10,997
unless of course it&#39;s actually fun.

333
00:17:10,997 --> 00:17:17,420
So you should keep a realistic timeline in mind.

334
00:17:17,420 --> 00:17:20,343
And then I think also you should add several months to

335
00:17:20,343 --> 00:17:21,274
that timeline,

336
00:17:21,273 --> 00:17:24,872
because the goals might change, the data itself might change.

337
00:17:24,872 --> 00:17:28,173
The data might change as the reaction to your new policy.

338
00:17:28,173 --> 00:17:30,816
You might observe something that you didn&#39;t expect,

339
00:17:30,816 --> 00:17:32,490
you put it in and so on and so forth.

340
00:17:35,270 --> 00:17:36,470
And while the model is deployed,

341
00:17:36,470 --> 00:17:39,040
it will need to be updated and improved.

342
00:17:39,040 --> 00:17:41,030
Just keep that in mind when you&#39;re doing this,

343
00:17:41,030 --> 00:17:42,910
it&#39;s not a one shot thing.

344
00:17:42,910 --> 00:17:46,129
You&#39;re always gonna need to go and follow those backward arrows

345
00:17:46,129 --> 00:17:49,178
back to the beginning and update things and improve things.

346
00:17:52,030 --> 00:17:53,246
So as I mentioned,

347
00:17:53,246 --> 00:17:56,292
knowledge discovery is an iterative process.

348
00:17:56,292 --> 00:17:58,928
You gotta do things over and over again.

349
00:17:58,928 --> 00:18:02,873
And it might be helpful of keep pieces of code around to have

350
00:18:02,873 --> 00:18:04,490
for the whole process.

351
00:18:04,490 --> 00:18:06,542
So that you can run things through the process

352
00:18:06,542 --> 00:18:07,730
without having to look for

353
00:18:07,730 --> 00:18:10,280
little bit of codes that you wrote a couple of years ago.

354
00:18:12,010 --> 00:18:14,550
Now at least if you have this outline it helps you to figure

355
00:18:14,550 --> 00:18:16,990
out what to charge people for

356
00:18:16,990 --> 00:18:19,260
your services if you are a contractor or something.

357
00:18:19,260 --> 00:18:22,270
It is not like you can charge them for doing some modelling.

358
00:18:22,270 --> 00:18:23,770
You actually have to charge them for

359
00:18:23,770 --> 00:18:26,360
all parts of this knowledge discovery process.

360
00:18:26,360 --> 00:18:29,260
And the iterations that you&#39;re gonna do to continue to fix

361
00:18:29,260 --> 00:18:32,450
up the system and keep it updated and get it deployed.

362
00:18:32,450 --> 00:18:35,140
So a proposal to work

363
00:18:35,140 --> 00:18:39,810
might actually include each of these steps in the process

364
00:18:39,810 --> 00:18:41,890
within the proposal to actually do the work.

365
00:18:41,890 --> 00:18:45,260
All right, so let&#39;s summarize.

366
00:18:45,260 --> 00:18:48,150
There have been several attempts to

367
00:18:48,150 --> 00:18:51,320
make the process of discovering knowledge scientific.

368
00:18:52,370 --> 00:18:55,250
And those attempts are called the KDD process,

369
00:18:55,250 --> 00:18:58,670
the CRISP-DM process, and the CCC Big Data Pipeline.

370
00:19:00,390 --> 00:19:02,800
They all have very similar steps, and

371
00:19:02,800 --> 00:19:05,460
data mining is only one of those steps.

372
00:19:05,460 --> 00:19:07,508
However, it is an important one.

