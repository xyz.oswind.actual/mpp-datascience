0
00:00:04,737 --> 00:00:06,610
Hi, and welcome back.

1
00:00:06,610 --> 00:00:07,310
In this sequence,

2
00:00:07,310 --> 00:00:11,210
I&#39;m gonna talk specifically about Azure Machine Learning.

3
00:00:11,210 --> 00:00:14,250
So, first off, you may wonder, why bother?

4
00:00:14,250 --> 00:00:18,420
What&#39;s the point of learning this particular tool?

5
00:00:18,420 --> 00:00:21,850
Well, one point is, it&#39;s easy to use.

6
00:00:21,850 --> 00:00:25,370
And that&#39;s very nice.

7
00:00:25,370 --> 00:00:27,980
You can build some pretty sophisticated models with

8
00:00:27,980 --> 00:00:30,860
no programming, although albeit you need some

9
00:00:30,860 --> 00:00:33,120
data science knowledge so you know what you&#39;re doing.

10
00:00:34,280 --> 00:00:37,650
You can quickly deploy your production solutions as web

11
00:00:37,650 --> 00:00:41,610
services and this is really cool because in other environments

12
00:00:41,610 --> 00:00:46,120
I&#39;ve worked on over the years, we get the solution ready,

13
00:00:46,120 --> 00:00:50,040
it&#39;s all tested out and then you wait, whether it&#39;s days, weeks,

14
00:00:50,040 --> 00:00:53,420
months, before any users even see what you&#39;ve done.

15
00:00:53,420 --> 00:00:55,340
So this way,

16
00:00:55,340 --> 00:00:59,000
it&#39;s deployable literally in a matter of minutes.

17
00:01:01,140 --> 00:01:04,990
You run your models on a highly scalable cloud environment and

18
00:01:04,989 --> 00:01:08,770
so when you are ready to deploy you&#39;ve got that scalability

19
00:01:08,770 --> 00:01:10,530
of the Azure Cloud behind you.

20
00:01:11,570 --> 00:01:14,440
And the Azure Cloud is also quite a secure environment for

21
00:01:14,440 --> 00:01:15,910
your data and your code.

22
00:01:15,910 --> 00:01:21,090
So, that&#39;s something again that could take quite a lot of effort

23
00:01:21,090 --> 00:01:23,695
to achieve and you may not need,

24
00:01:23,695 --> 00:01:29,000
in fact you won&#39;t need to do it with Azure Machine Learning.

25
00:01:29,000 --> 00:01:32,905
And it has a really nice array of powerful efficient

26
00:01:32,905 --> 00:01:35,740
built-in algorithms, machine learning algorithms.

27
00:01:35,740 --> 00:01:38,320
So there’s some pretty nice stuff there.

28
00:01:38,320 --> 00:01:41,480
And to me one of the best features is it&#39;s

29
00:01:41,480 --> 00:01:45,250
fully extensible with SQL or with Python or with R.

30
00:01:45,250 --> 00:01:47,570
So if you&#39;re comfortable with any of those languages or

31
00:01:47,570 --> 00:01:50,750
all of those languages you have lots of avenues to

32
00:01:53,090 --> 00:01:58,460
extend what&#39;s going on here for your special particular problem.

33
00:01:58,460 --> 00:02:02,970
And finally, there&#39;s integration with Jupyter Notebooks.

34
00:02:02,970 --> 00:02:05,630
So Jupyter Notebooks now have R and

35
00:02:05,630 --> 00:02:09,730
Python kernels and the Jupyter Notebooks are integrated with

36
00:02:09,729 --> 00:02:10,840
Azure Machine Learning.

37
00:02:10,840 --> 00:02:14,437
So if you wanna do some work on a result or

38
00:02:14,437 --> 00:02:19,560
an intermediate step or something in the notebook, and

39
00:02:19,560 --> 00:02:24,574
then you get in the notebook, the text in the R or Python

40
00:02:24,574 --> 00:02:29,920
code that you use to do that analysis, you&#39;re good to go.

41
00:02:29,920 --> 00:02:31,440
And that&#39;s really a nice feature.

42
00:02:32,780 --> 00:02:36,394
If you haven&#39;t already done so, I&#39;d encourage you to establish

43
00:02:36,394 --> 00:02:39,180
your free tier Azure Machine Learning account.

44
00:02:39,180 --> 00:02:43,050
You can follow along with me in these demos with that account.

45
00:02:43,050 --> 00:02:45,730
And one of the nice things about the free tier account,

46
00:02:45,730 --> 00:02:47,270
is you get unlimited time,

47
00:02:47,270 --> 00:02:50,020
it&#39;s not a 30 day kind of thing or whatever.

48
00:02:50,020 --> 00:02:53,580
You can use it for a long time, albeit at a restricted priority.

49
00:02:53,580 --> 00:02:55,830
So things may run a little slow,

50
00:02:55,830 --> 00:03:01,230
but you can do it and keep experimenting,

51
00:03:01,230 --> 00:03:04,520
keep learning about data science and machine learning.

52
00:03:04,520 --> 00:03:08,130
If you need the full performance like if you&#39;re actually gonna

53
00:03:08,130 --> 00:03:11,280
deploy a solution to your colleagues, you really should be

54
00:03:11,280 --> 00:03:14,310
using a paid account because that&#39;s what it&#39;s for,

55
00:03:14,310 --> 00:03:16,080
it&#39;s for getting performance.

56
00:03:16,080 --> 00:03:17,870
It&#39;s getting the scalability etc.

57
00:03:19,310 --> 00:03:22,530
So, let&#39;s talk about Azure Machine Learning Studio, so

58
00:03:22,530 --> 00:03:26,520
the studio is the desktop tool we use to

59
00:03:26,520 --> 00:03:29,980
build our work flows in Azure Machine Learning.

60
00:03:31,680 --> 00:03:34,110
And I have a project here.

61
00:03:34,110 --> 00:03:35,930
So along this side here on the left,

62
00:03:35,930 --> 00:03:39,720
we have projects, experiments, web services, notebooks,

63
00:03:39,720 --> 00:03:42,660
so that&#39;s your Jupyter notebooks I was talking about.

64
00:03:42,660 --> 00:03:45,570
Data sets, trained models, when you build some models

65
00:03:45,570 --> 00:03:48,310
you can save them as trained models, and some settings.

66
00:03:48,310 --> 00:03:49,210
But.

67
00:03:49,210 --> 00:03:52,590
And you see I have a project created already.

68
00:03:52,590 --> 00:03:55,560
So I went to New and created a project.

69
00:03:56,650 --> 00:04:00,650
But let me create a new experiment here.

70
00:04:00,650 --> 00:04:01,440
So I&#39;m gonna go to New and

71
00:04:01,440 --> 00:04:05,950
you see here I could create a new project as I said, or

72
00:04:05,950 --> 00:04:07,919
get a new data set which I&#39;ll do in a minute.

73
00:04:08,970 --> 00:04:11,490
But I&#39;m going to create a new experiment here.

74
00:04:11,490 --> 00:04:13,670
And I&#39;m just gonna do a blank experiment.

75
00:04:15,920 --> 00:04:19,986
And I&#39;m gonna call it, and since this is our first demo,

76
00:04:19,986 --> 00:04:21,941
I&#39;m gonna call it Demo 1.

77
00:04:21,940 --> 00:04:23,464
All right.

78
00:04:23,464 --> 00:04:26,630
So now, I need some data for my experiment.

79
00:04:26,630 --> 00:04:28,070
So how do I do that?

80
00:04:28,070 --> 00:04:30,790
Now I have a datafile that&#39;s

81
00:04:30,790 --> 00:04:34,330
on my desktop machine here that I&#39;m using.

82
00:04:35,440 --> 00:04:37,430
So I wanna upload that, and

83
00:04:37,430 --> 00:04:40,710
I want that to become the dataset I&#39;m gonna use.

84
00:04:40,710 --> 00:04:45,630
So I do plus again to get a new dataset, and then yes,

85
00:04:45,630 --> 00:04:50,250
it&#39;s on a local file, and I&#39;m gonna browse to that.

86
00:04:51,460 --> 00:04:56,670
And let me get to my Documents &gt; Data Science Essentials,

87
00:04:56,670 --> 00:04:58,200
and here&#39;s our lab files.

88
00:04:59,500 --> 00:05:06,900
And I go down and I find this called Credit-Scoring-Clean.csv.

89
00:05:06,900 --> 00:05:10,990
So, I&#39;ve cleaned up this dataset for the purpose of this

90
00:05:10,990 --> 00:05:16,080
demonstration, so we can just jump right in and do

91
00:05:16,080 --> 00:05:20,160
some stuff with it without a lot of data munging, at this point.

92
00:05:20,160 --> 00:05:22,144
We&#39;ll get to that in this course but

93
00:05:22,144 --> 00:05:24,790
right now we&#39;re just gonna use a nice, clean,

94
00:05:24,790 --> 00:05:27,851
pristine dataset that&#39;s already been pre-staged.

95
00:05:27,851 --> 00:05:34,528
And you can see it&#39;s a credits scoring clean is the name.

96
00:05:34,528 --> 00:05:39,083
It&#39;s a generic CSV file with a header yes, and I&#39;ve given it

97
00:05:39,083 --> 00:05:43,292
just a little description, bank credit scoring data.

98
00:05:43,292 --> 00:05:47,143
And if I click that checkbox it&#39;s gonna upload, you see,

99
00:05:47,143 --> 00:05:49,499
down here in this lower right here,

100
00:05:49,499 --> 00:05:53,130
it shows that it&#39;s uploading or over here on the left.

101
00:05:54,610 --> 00:05:55,560
We&#39;ll just let that go.

102
00:05:57,160 --> 00:05:57,940
Okay.

103
00:05:57,940 --> 00:06:00,671
So, you see I have a green checkbox over here, and

104
00:06:00,671 --> 00:06:03,857
I have an OK over here, so it looks like this thing uploaded

105
00:06:03,857 --> 00:06:05,501
correctly, which is great.

106
00:06:08,913 --> 00:06:12,400
And so now I&#39;m gonna go and find it.

107
00:06:12,400 --> 00:06:14,927
So, in this panel here, this pane,

108
00:06:14,927 --> 00:06:18,527
this is where all my modules and data are organised.

109
00:06:18,527 --> 00:06:22,198
So you can see, save dataset, so that&#39;s where we&#39;re gonna find

110
00:06:22,198 --> 00:06:25,290
this, but there is trained models, transforms etc.

111
00:06:25,290 --> 00:06:29,285
Data input and output, feature selection, machine learning,

112
00:06:29,285 --> 00:06:35,960
Python are statistics functions text analytics.

113
00:06:35,960 --> 00:06:38,830
So we&#39;re gonna go through quite a few in the course of these

114
00:06:38,830 --> 00:06:42,410
different, what&#39;s under these tabs, like say for

115
00:06:42,410 --> 00:06:46,530
machine learning and then you find initialize model.

116
00:06:46,530 --> 00:06:49,611
And here we have anomaly detection, classification,

117
00:06:49,611 --> 00:06:51,520
clustering, regression, etc.

118
00:06:51,520 --> 00:06:54,939
So let me find my dataset.

119
00:07:06,786 --> 00:07:07,453
There it is,

120
00:07:07,453 --> 00:07:09,770
Credit-Scoring-Clean under My Dataset.

121
00:07:09,770 --> 00:07:13,676
So I&#39;m just gonna, and what you do to use this,

122
00:07:13,676 --> 00:07:18,507
is you just drag it onto the canvas, and there you have it.

123
00:07:18,507 --> 00:07:24,460
So, at this point now we have a experiment

124
00:07:24,460 --> 00:07:28,370
that we&#39;re starting to build in the middle here called Demo 1,

125
00:07:28,370 --> 00:07:31,740
and so far it has one thing in the canvas,

126
00:07:31,740 --> 00:07:33,685
one module, which is just the dataset.

127
00:07:34,960 --> 00:07:36,220
All right. So there you have it.

128
00:07:36,220 --> 00:07:39,760
We have a start of our experiment,

129
00:07:39,760 --> 00:07:44,860
we have our dataset, and in the next two sequences we&#39;re going

130
00:07:44,860 --> 00:07:48,500
to continue to add modules to that experiment.

131
00:07:48,500 --> 00:07:52,323
Build a workflow for a machine learning model that we&#39;re gonna

132
00:07:52,323 --> 00:07:54,565
use that data that we just uploaded.

