0
00:00:04,544 --> 00:00:06,170
Hi and welcome.

1
00:00:06,170 --> 00:00:10,480
In this demo I wanna show you a very cool feature of

2
00:00:10,480 --> 00:00:14,500
Azure Machine Learning, which is the ability to work with your

3
00:00:14,500 --> 00:00:20,960
data from your experiment in a Jupyter Notebook using Python.

4
00:00:23,230 --> 00:00:27,090
So I have here on my desktop a very simple experiment where I

5
00:00:27,090 --> 00:00:28,680
have some data and

6
00:00:28,680 --> 00:00:32,180
I&#39;ve done a few manipulations using a module.

7
00:00:32,180 --> 00:00:37,250
Some python, some r, some SQL, nothing too fancy.

8
00:00:38,330 --> 00:00:39,810
And if I go to this data,

9
00:00:39,810 --> 00:00:44,610
which happens to be a .csv file and click on it.

10
00:00:44,610 --> 00:00:52,200
You see I have options to open a Python 2 or Python 3 notebook.

11
00:00:52,200 --> 00:00:54,800
So let me just go ahead and open a Python 2 notebook

12
00:00:56,140 --> 00:00:58,950
since in this course we&#39;re working in Python 2, and

13
00:00:58,950 --> 00:01:01,080
there, I&#39;ve got the notebook.

14
00:01:03,370 --> 00:01:07,150
But what if I wanted to look at an intermediate result,

15
00:01:07,150 --> 00:01:11,630
say the end result of all these transforms, and

16
00:01:11,630 --> 00:01:12,900
I wanted to work with that.

17
00:01:12,900 --> 00:01:15,130
So I go down here to Open Notebooks and

18
00:01:15,130 --> 00:01:16,250
oops it&#39;s all grayed out.

19
00:01:16,250 --> 00:01:22,602
And the reason is this is not in CSV format, but I just look for

20
00:01:22,602 --> 00:01:27,708
the module called convert to CSV, and I just drag it on

21
00:01:27,708 --> 00:01:32,946
to my canvass and connected it up and I&#39;m gonna run it.

22
00:01:32,946 --> 00:01:36,974
Okay, so now I click on that,

23
00:01:36,974 --> 00:01:41,170
and I can start notebook.

24
00:01:41,170 --> 00:01:44,115
So let me start a Python 2 notebook,

25
00:01:44,115 --> 00:01:46,491
I&#39;ll show you what happens.

26
00:01:48,697 --> 00:01:53,685
All right first of all, you can see there’s a data frame

27
00:01:53,685 --> 00:01:56,990
that’s created here called frame.

28
00:01:58,230 --> 00:02:02,530
And it&#39;s by a method called two data frame for this DS.

29
00:02:02,530 --> 00:02:06,288
And DS is essentially a GUID, everything you need to know to

30
00:02:06,288 --> 00:02:11,550
get to your session, get to your data in Azure Machine Learning.

31
00:02:12,660 --> 00:02:14,139
And here&#39;s frame right here.

32
00:02:17,280 --> 00:02:20,090
And so, let me go to this menu.

33
00:02:20,090 --> 00:02:22,340
And I&#39;m gonna run those cells.

34
00:02:22,340 --> 00:02:23,450
You see there&#39;s two cells.

35
00:02:23,450 --> 00:02:27,730
This cell and the cell that just says frame.

36
00:02:27,730 --> 00:02:28,770
So I&#39;m gonna run all.

37
00:02:28,770 --> 00:02:33,910
And it run pretty quickly.

38
00:02:35,040 --> 00:02:39,090
And you can see I&#39;ve got what would normally happen in any

39
00:02:39,090 --> 00:02:40,890
interactive Python session.

40
00:02:42,090 --> 00:02:43,801
I&#39;ve got the summary of the data frame here.

41
00:02:47,433 --> 00:02:49,477
And so I&#39;m gonna go to Insert and

42
00:02:49,477 --> 00:02:51,760
I&#39;m gonna put another cell below.

43
00:02:51,760 --> 00:02:53,470
So now I have another cell where I can run

44
00:02:53,470 --> 00:02:54,995
some other Python code.

45
00:02:54,995 --> 00:02:58,275
So let&#39;s do

46
00:02:58,275 --> 00:03:03,820
frame.dtypes and then I

47
00:03:03,820 --> 00:03:07,120
just click on this little arrow that says run the selected cell.

48
00:03:08,850 --> 00:03:09,900
And there I have the result,

49
00:03:11,110 --> 00:03:15,210
which is the names of my columns and their types.

50
00:03:15,210 --> 00:03:17,120
And I have another cell where I could type some

51
00:03:17,120 --> 00:03:18,990
more Python code.

52
00:03:18,990 --> 00:03:23,590
So one last bit here I can go up here and

53
00:03:23,590 --> 00:03:26,970
click on that funny machine generated name and

54
00:03:26,970 --> 00:03:30,240
give it some name that I can remember when I maybe go back

55
00:03:30,240 --> 00:03:32,399
to this notebook and work on it in the future.

56
00:03:35,110 --> 00:03:40,510
That&#39;s all you need to know to, from any point in the work flow

57
00:03:40,510 --> 00:03:44,570
within an Azure machine learning experiment, to take the data,

58
00:03:44,570 --> 00:03:49,170
put it into a Jupiter notebook and work with it with Python.

