0
00:00:04,923 --> 00:00:06,460
Hi and welcome back.

1
00:00:06,460 --> 00:00:09,260
So the last thing we need to do here to finish this

2
00:00:09,260 --> 00:00:12,610
experiment we&#39;ve been working on in the previous sequences,

3
00:00:12,610 --> 00:00:14,900
is to add a machine learning model.

4
00:00:14,900 --> 00:00:18,110
And in this case, we&#39;re gonna add a classification model.

5
00:00:18,110 --> 00:00:20,980
So the classification model we&#39;re gonna try to predict,

6
00:00:20,980 --> 00:00:24,870
whether a bank customer is likely to have good or

7
00:00:24,870 --> 00:00:25,660
bad credit.

8
00:00:26,960 --> 00:00:29,610
So how do we actually turn that into

9
00:00:29,610 --> 00:00:31,100
a machine learning workflow?

10
00:00:32,280 --> 00:00:34,970
Well we start with some input data, which we&#39;ve already

11
00:00:34,970 --> 00:00:39,040
uploaded in a previous demo, we do some data transformation.

12
00:00:39,040 --> 00:00:46,580
So in our case, we eliminated 9 of our 21 columns.

13
00:00:46,580 --> 00:00:50,350
So 9 of 20 features were filtered out, and

14
00:00:50,350 --> 00:00:51,590
that&#39;s our data transformation.

15
00:00:51,590 --> 00:00:55,970
So recall, we used RPython SQL to do that.

16
00:00:55,970 --> 00:01:00,140
So the next thing I have to do, is I have to split the data, and

17
00:01:00,140 --> 00:01:01,910
you&#39;ll see why that is in a minute.

18
00:01:01,910 --> 00:01:06,288
So I&#39;m gonna split the data, and I&#39;m gonna use one split of that

19
00:01:06,288 --> 00:01:09,790
data, one portion of that data to train the model.

20
00:01:09,790 --> 00:01:13,150
I&#39;m holding another portion back, okay?

21
00:01:13,150 --> 00:01:15,850
So maybe I&#39;m using 70% to train the model and

22
00:01:15,850 --> 00:01:19,930
I&#39;m holding back 30% that I&#39;m not gonna let the model training

23
00:01:19,930 --> 00:01:21,000
algorithm know about.

24
00:01:22,150 --> 00:01:24,290
And then I have to give some model definition.

25
00:01:24,290 --> 00:01:27,590
We&#39;re gonna use a very simple linear model and, or sorry,

26
00:01:27,590 --> 00:01:29,080
tree model in this case.

27
00:01:30,610 --> 00:01:32,960
But we still have to give it a few model parameters and things.

28
00:01:32,960 --> 00:01:34,360
That&#39;s not the point of this demo,

29
00:01:34,360 --> 00:01:37,630
we&#39;re not gonna dwell on that, but we do have to do it.

30
00:01:38,890 --> 00:01:40,810
Now, we&#39;re gonna score that model, so

31
00:01:40,810 --> 00:01:42,020
we&#39;ve computed the model.

32
00:01:42,020 --> 00:01:46,250
So the output of that model and that bit of data,

33
00:01:46,250 --> 00:01:50,633
say that 30% that we held back, it used to score the data.

34
00:01:50,633 --> 00:01:56,722
So this way we compute labels.

35
00:01:56,722 --> 00:02:00,850
We know what the real labels are, but we compute labels

36
00:02:00,850 --> 00:02:05,600
from our model based on that 30% of features.

37
00:02:05,600 --> 00:02:08,900
And now we can compare because we actually, in our training

38
00:02:08,900 --> 00:02:11,880
data up here, we actually knew what those labels were, so

39
00:02:11,880 --> 00:02:14,440
we can evaluate that module.

40
00:02:14,440 --> 00:02:20,579
We can see, yeah, how good did we do of real features,

41
00:02:20,579 --> 00:02:27,264
I mean, real labels values versus computed label values?

42
00:02:27,264 --> 00:02:30,251
So lets go to the demo and try to make that work.

43
00:02:32,134 --> 00:02:33,965
Okay, so as I indicated,

44
00:02:33,965 --> 00:02:37,309
the first thing we need to do is split that data.

45
00:02:37,309 --> 00:02:40,181
So lets find the split module,

46
00:02:40,181 --> 00:02:44,829
and there it is, and drag it on to the canvas here.

47
00:02:44,829 --> 00:02:48,888
And I&#39;m going to connect the output of my filter data to

48
00:02:48,888 --> 00:02:51,016
the input of the split line.

49
00:02:51,016 --> 00:02:55,668
Mostly gonna just move that up so you can see what&#39;s going on.

50
00:02:55,668 --> 00:02:59,645
And I&#39;m gonna use a 70, 30 split, and

51
00:02:59,645 --> 00:03:03,734
basically what the split data module does,

52
00:03:03,734 --> 00:03:09,852
is it randomly samples, The rows,

53
00:03:09,852 --> 00:03:16,629
and decides which 70% are going to be the cases for training.

54
00:03:16,629 --> 00:03:20,013
So I&#39;ve set that at 0.7, and I&#39;ve just given it a random

55
00:03:20,013 --> 00:03:23,546
seed, and I&#39;m splitting the rows, I&#39;m splitting by rows.

56
00:03:23,546 --> 00:03:27,650
So then I need to train a model and there it is,

57
00:03:27,650 --> 00:03:29,594
Train Model module.

58
00:03:29,594 --> 00:03:34,576
And I&#39;m gonna drag that in here, and I&#39;m gonna connect it up.

59
00:03:34,576 --> 00:03:37,046
So notice results dataset1,

60
00:03:37,046 --> 00:03:39,801
which is the output of this split,

61
00:03:39,801 --> 00:03:44,744
is connected to the dataset that I&#39;m gonna use for training.

62
00:03:44,744 --> 00:03:49,234
And now I need to define the model that I&#39;m gonna train, so

63
00:03:49,234 --> 00:03:55,461
I&#39;m gonna use, Something called a decision forest.

64
00:03:55,461 --> 00:04:00,411
And it&#39;s not so important what that is at this point, But

65
00:04:00,411 --> 00:04:05,397
we have a two class decision forest, okay?

66
00:04:05,397 --> 00:04:10,230
You put this over here, and so that&#39;s my untrained model.

67
00:04:10,230 --> 00:04:13,590
Output is connected to the untrained model

68
00:04:13,590 --> 00:04:17,660
input of trained model, and let me set a few parameters here.

69
00:04:20,250 --> 00:04:22,300
So again, it&#39;s not so important what these are,

70
00:04:22,300 --> 00:04:27,070
but we&#39;re gonna create 50 models that are gonna vote.

71
00:04:27,070 --> 00:04:30,270
Basically that&#39;s how it works, they&#39;re gonna vote.

72
00:04:30,270 --> 00:04:33,310
And I&#39;m gonna limit that to 32, and

73
00:04:33,310 --> 00:04:35,883
I&#39;m going to limit the number.

74
00:04:35,883 --> 00:04:39,964
So with tree models you don&#39;t want them to what&#39;s

75
00:04:39,964 --> 00:04:42,297
called overparameterize,

76
00:04:42,297 --> 00:04:46,775
and again we&#39;ll talk about this extensively as we go.

77
00:04:46,775 --> 00:04:49,205
But I&#39;m just doing some things here to limit that.

78
00:04:49,205 --> 00:04:53,366
And so the last thing I have to do, is I have to tell this train

79
00:04:53,366 --> 00:04:57,800
model module what&#39;s the label, what am I training, I guess.

80
00:04:57,800 --> 00:05:02,682
So let&#39;s give it a column name, and it&#39;s called CreditStatus,

81
00:05:02,682 --> 00:05:05,345
recall that was that label column,

82
00:05:05,345 --> 00:05:07,763
this way you just check the box.

83
00:05:07,763 --> 00:05:10,669
So I&#39;ve used this column selector,

84
00:05:10,669 --> 00:05:14,794
I&#39;ve included my column name, CreditStatus, and

85
00:05:14,794 --> 00:05:17,618
then I check the little check here.

86
00:05:19,309 --> 00:05:21,734
Now I need to score this model, and

87
00:05:21,734 --> 00:05:24,587
I hope you see how quick I&#39;m doing this.

88
00:05:24,587 --> 00:05:27,964
I mean, I&#39;m literally building a machine learning model right in

89
00:05:27,964 --> 00:05:28,998
front of your eyes.

90
00:05:28,998 --> 00:05:33,689
And the trained model goes into that trained model input of

91
00:05:33,689 --> 00:05:35,705
the score model module.

92
00:05:35,705 --> 00:05:38,653
And now that results dataset2, so

93
00:05:38,653 --> 00:05:43,967
that&#39;s that 30% of data I held back, it&#39;s connected here.

94
00:05:43,967 --> 00:05:46,825
There are no parameters here, except for one,

95
00:05:46,825 --> 00:05:49,047
I pin the score column to the output.

96
00:05:49,047 --> 00:05:52,561
So that means I&#39;ll see the actual label and

97
00:05:52,561 --> 00:05:54,127
the scored label.

98
00:05:54,127 --> 00:06:02,417
And the last thing I need is an evaluate model module, okay?

99
00:06:02,417 --> 00:06:04,747
And you can actually compare two models here, but

100
00:06:04,747 --> 00:06:05,868
I&#39;m not gonna do that.

101
00:06:05,868 --> 00:06:08,968
I&#39;m just gonna take my scored data set and plug it in there.

102
00:06:10,557 --> 00:06:13,553
And that&#39;s all I need to do, so

103
00:06:13,553 --> 00:06:17,484
we now have the work flow I talked about.

104
00:06:17,484 --> 00:06:21,423
Let me just zoom out a bit so that you can see that.

105
00:06:21,423 --> 00:06:24,385
So we have our data, we have our data transformations,

106
00:06:24,385 --> 00:06:25,965
basically our data munging.

107
00:06:25,965 --> 00:06:30,501
We split the data, we define a model, we train that model we&#39;ve

108
00:06:30,501 --> 00:06:34,549
defined, we score that model, we evaluate that model.

109
00:06:34,549 --> 00:06:39,510
So I&#39;m gonna save my work here, And

110
00:06:39,510 --> 00:06:41,222
them I&#39;m gonna run that model.

111
00:06:42,824 --> 00:06:45,955
All right, so it looks like the model ran successfully.

112
00:06:45,955 --> 00:06:50,876
You see all my new modules now have green check boxes.

113
00:06:50,876 --> 00:06:54,053
And there&#39;s a finished running green checkbox up here, so my

114
00:06:54,053 --> 00:06:57,367
model&#39;s run successfully, let&#39;s have a look at the results.

115
00:06:57,367 --> 00:07:00,037
So the first thing we can do is

116
00:07:00,037 --> 00:07:03,945
just look at the raw scored results here.

117
00:07:03,945 --> 00:07:09,620
So I&#39;m gonna visualize those, and let me scroll over,

118
00:07:09,620 --> 00:07:15,314
and I have the CreditStatus, which is my actual label.

119
00:07:15,314 --> 00:07:18,843
So that&#39;s the true label, and then I have my scored label,

120
00:07:18,843 --> 00:07:20,690
that&#39;s my forecasted label.

121
00:07:20,690 --> 00:07:24,168
And you can see, for example my first case,

122
00:07:24,168 --> 00:07:28,050
the actual label was 1, my scored label was 1.

123
00:07:28,050 --> 00:07:33,723
The second case, my actual label was 1, my scored label was 1.

124
00:07:33,723 --> 00:07:37,065
The third case, my actual label was 0.

125
00:07:37,065 --> 00:07:40,011
So that was a bad credit customer, and

126
00:07:40,011 --> 00:07:42,042
the scored label is zero.

127
00:07:42,042 --> 00:07:47,387
I&#39;m doing pretty well, but oops, I get down here, and I have 0,

128
00:07:47,387 --> 00:07:53,124
0, 0, which were scored as 1, 1, 1, so there&#39;s three errors.

129
00:07:53,124 --> 00:07:55,032
And then I have another one correct,

130
00:07:55,032 --> 00:07:57,688
then I have another error, then another correct.

131
00:07:57,688 --> 00:08:01,899
So you kinda get the idea that it&#39;s working, but

132
00:08:01,899 --> 00:08:03,550
maybe not always.

133
00:08:03,550 --> 00:08:07,170
But that&#39;s very subjective, very qualitative.

134
00:08:07,170 --> 00:08:09,730
So we can get a little more quantitative here if we look at

135
00:08:09,730 --> 00:08:12,470
this evaluate model module.

136
00:08:12,470 --> 00:08:14,659
Let me visualize that output, and

137
00:08:14,659 --> 00:08:17,614
we&#39;re not gonna go through the details here.

138
00:08:17,614 --> 00:08:21,504
I&#39;m just gonna point to one statistic at this point,

139
00:08:21,504 --> 00:08:22,632
the accuracy.

140
00:08:22,632 --> 00:08:25,600
So accuracy is total correct cases,

141
00:08:25,600 --> 00:08:29,237
so where we scored it either correctly as 1 or

142
00:08:29,237 --> 00:08:32,507
correctly as 0, divided by all cases.

143
00:08:32,506 --> 00:08:36,840
And so it looks like we got 74% of those correct.

144
00:08:36,840 --> 00:08:39,458
So for a first model, where we haven&#39;t put

145
00:08:39,457 --> 00:08:42,893
a lot of energy into it, that&#39;s pretty good actually.

146
00:08:42,893 --> 00:08:47,093
And also, you see that I&#39;ve done this right before your eyes in

147
00:08:47,093 --> 00:08:50,668
a matter of minutes, using Azure Machine Learning.

148
00:08:50,668 --> 00:08:55,184
So I hope that in this little sequence now you&#39;ve gotten

149
00:08:55,184 --> 00:08:59,413
a feel for using the Azure Machine Learning Studio,

150
00:08:59,413 --> 00:09:04,122
how you put the modules together to build workflows in your

151
00:09:04,122 --> 00:09:08,446
experiment, and even what the general workflow is to

152
00:09:08,446 --> 00:09:11,167
build a machine learning model.

153
00:09:11,167 --> 00:09:15,323
And we&#39;ll get into a lot more detail on understanding data,

154
00:09:15,323 --> 00:09:18,105
cleaning data, etc., as we go along.

155
00:09:18,105 --> 00:09:20,903
So I look forward to showing you all that.

156
00:09:20,903 --> 00:09:24,004
And I think it&#39;s gonna be really interesting and

157
00:09:24,004 --> 00:09:25,223
exciting for you.

