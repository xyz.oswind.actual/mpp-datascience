0
00:00:04,879 --> 00:00:07,030
Hi, and welcome back.

1
00:00:07,030 --> 00:00:11,070
So previously in this module, Cynthia has been discussing

2
00:00:11,070 --> 00:00:14,530
the process of how we do data science and

3
00:00:14,530 --> 00:00:19,740
various aspects of the different stages we go through,

4
00:00:19,740 --> 00:00:21,680
when working a data science problem.

5
00:00:21,680 --> 00:00:23,590
So, in this sequence and

6
00:00:23,590 --> 00:00:26,680
the next few sequences I&#39;m gonna take you through some practical

7
00:00:26,680 --> 00:00:31,450
aspect of the technology we use to actually do data science.

8
00:00:31,450 --> 00:00:35,050
So, in this module I&#39;m gonna start out with

9
00:00:35,050 --> 00:00:40,100
just giving you a broad overview of data science technology and

10
00:00:40,100 --> 00:00:43,030
including the technologies we&#39;ll use in this course.

11
00:00:44,690 --> 00:00:47,120
That&#39;s going to lead us to a tour of

12
00:00:47,120 --> 00:00:48,850
Azure Machine Learning Studio.

13
00:00:49,930 --> 00:00:53,040
And I&#39;ll explain to you how to build what we&#39;re gonna call

14
00:00:53,040 --> 00:00:55,130
an Azure Machine Learning Experiment.

15
00:00:55,130 --> 00:01:00,090
That&#39;s how you actually do build a machine learning model in

16
00:01:00,090 --> 00:01:02,100
Azure Machine Learning.

17
00:01:02,100 --> 00:01:06,380
I&#39;m gonna talk a little bit about the mechanics of how

18
00:01:06,380 --> 00:01:09,270
data flows so you can understand how the experiment works,

19
00:01:09,270 --> 00:01:12,240
how the tables work between modules and

20
00:01:12,240 --> 00:01:15,160
what some of the data types are you can use.

21
00:01:15,160 --> 00:01:19,950
We&#39;re gonna discuss, just briefly here R and Python and

22
00:01:19,950 --> 00:01:22,660
how that works within Azure Machine Learning.

23
00:01:22,660 --> 00:01:26,890
I&#39;ll spend quite a bit more time in this course discussing R and

24
00:01:26,890 --> 00:01:33,570
Python, both as standalone tools and in Azure Machine Learning.

25
00:01:33,570 --> 00:01:36,980
But for the moment, we&#39;re just gonna focus on, for

26
00:01:36,980 --> 00:01:39,600
the purpose of this introduction,

27
00:01:39,600 --> 00:01:41,780
just using them in Azure Machine Learning okay?

28
00:01:43,420 --> 00:01:46,230
We&#39;re also gonna talk just briefly about how to use SQL in

29
00:01:46,230 --> 00:01:47,570
Azure Machine Learning,

30
00:01:47,570 --> 00:01:50,590
and we&#39;re not gonna do a lot with SQL in this course,

31
00:01:50,590 --> 00:01:53,620
there are other courses available for that.

32
00:01:54,710 --> 00:01:58,160
And in the end, I&#39;m going to work a complete

33
00:01:58,160 --> 00:02:01,690
machine learning example and the point there is so you get

34
00:02:01,690 --> 00:02:05,060
an idea of how the data flow works in Azure Machine Learning.

35
00:02:06,270 --> 00:02:09,730
There&#39;s lots of details about models and evaluation,

36
00:02:09,729 --> 00:02:10,350
things like that.

37
00:02:10,350 --> 00:02:13,260
That&#39;s all coming, but for now I just want you to

38
00:02:13,260 --> 00:02:15,060
understand how the data flow works.

39
00:02:16,560 --> 00:02:21,791
So let&#39;s talk about the tools we use in Data Science.

40
00:02:21,791 --> 00:02:26,323
Open source tools are very important to us, and R and

41
00:02:26,323 --> 00:02:31,270
Python are probably the most widely used of those tools.

42
00:02:31,270 --> 00:02:36,220
I&#39;d say half data scientists or half the projects

43
00:02:36,220 --> 00:02:40,300
I encounter are using R, maybe the other half Python.

44
00:02:40,300 --> 00:02:45,340
And in general I&#39;d say for

45
00:02:45,340 --> 00:02:50,210
R and Python they really do predominate,

46
00:02:50,210 --> 00:02:53,880
but there&#39;s some tools like Azure Machine Learning that

47
00:02:53,880 --> 00:02:56,580
we&#39;re gonna spend a lot of time on in this course, and

48
00:02:56,580 --> 00:02:59,670
part of the reason is they&#39;re very easy to use.

49
00:02:59,670 --> 00:03:01,840
They don&#39;t require a lot of programming.

50
00:03:01,840 --> 00:03:05,230
They do require that you understand data science,

51
00:03:05,230 --> 00:03:08,400
process, the models, and evaluation.

52
00:03:08,400 --> 00:03:12,070
So this isn&#39;t something you should just hand over to

53
00:03:12,070 --> 00:03:16,200
a colleague who doesn&#39;t know anything about analytics or

54
00:03:16,200 --> 00:03:18,340
data science, but they&#39;re easy to use.

55
00:03:18,340 --> 00:03:20,310
The other thing about Azure Machine Learning that I

56
00:03:20,310 --> 00:03:23,180
really like is the models are easy to deploy and

57
00:03:23,180 --> 00:03:25,220
we&#39;ll get to that in this course.

58
00:03:25,220 --> 00:03:28,520
And by easy deploy, I mean it&#39;s almost like a one click

59
00:03:28,520 --> 00:03:31,570
deployment and your colleagues can be using

60
00:03:31,570 --> 00:03:34,160
the machine learning model you&#39;ve built.

61
00:03:34,160 --> 00:03:36,580
And there&#39;s really not many environments out there that

62
00:03:36,580 --> 00:03:40,610
are that easy to deploy where you don&#39;t have to go through

63
00:03:40,610 --> 00:03:45,310
weeks, months, whatever of some massive deployment undertaking.

64
00:03:46,510 --> 00:03:51,130
And realize that the tools we&#39;re talking about are always

65
00:03:51,130 --> 00:03:53,200
part of a bigger data science stack.

66
00:03:53,200 --> 00:03:56,068
There&#39;s always data sources, there&#39;s data management,

67
00:03:56,068 --> 00:04:00,240
whether it&#39;s databases or many other tools.

68
00:04:02,180 --> 00:04:04,870
As you&#39;re going through this course, even though we&#39;re

69
00:04:04,870 --> 00:04:09,040
working on just the data science and machine learning specific

70
00:04:09,040 --> 00:04:13,150
parts of the stack, realize you&#39;re part of a bigger

71
00:04:13,150 --> 00:04:15,270
picture when you&#39;re doing real world data science.

72
00:04:16,440 --> 00:04:20,720
So why talk about these Open-Source Tools?

73
00:04:21,959 --> 00:04:25,730
R and Python, as I&#39;ve already said, are really widely used.

74
00:04:25,730 --> 00:04:27,135
So that&#39;s one good reason to do it.

75
00:04:27,135 --> 00:04:30,450
The chances are if you&#39;re trying to work in a team of other data

76
00:04:30,450 --> 00:04:33,490
scientists, they&#39;re going to know R or Python, or

77
00:04:33,490 --> 00:04:36,510
many people will know both.

78
00:04:36,510 --> 00:04:41,984
They&#39;re highly interactive, so you can test models,

79
00:04:41,984 --> 00:04:48,278
you can explore data, you can share that with your colleagues.

80
00:04:48,278 --> 00:04:51,452
They have that good visualizations, both packages,

81
00:04:51,452 --> 00:04:53,177
so when you get a data set, and

82
00:04:53,177 --> 00:04:56,490
you&#39;re just trying to figure out what&#39;s going on here?

83
00:04:56,490 --> 00:04:57,930
What do we do with it?

84
00:04:57,930 --> 00:05:00,490
How do we even get started on this thing?

85
00:05:02,070 --> 00:05:04,080
Visualization is really helpful.

86
00:05:04,080 --> 00:05:07,000
And also when you have things that don&#39;t seem to quite

87
00:05:07,000 --> 00:05:10,190
work with your models, the visualization can often

88
00:05:10,190 --> 00:05:13,070
bail you out and you can look at what&#39;s going wrong.

89
00:05:14,530 --> 00:05:19,210
Both R and Python have vast and constantly growing packages of

90
00:05:20,210 --> 00:05:23,500
machine learning algorithms, utilities for cleaning data,

91
00:05:23,500 --> 00:05:27,550
for creating new and better visualizations.

92
00:05:28,650 --> 00:05:32,120
And they both have excellent development environments.

93
00:05:32,120 --> 00:05:36,190
In this course, we&#39;re going to use Spyder for

94
00:05:36,190 --> 00:05:39,240
those of you who wanna use Python.

95
00:05:39,240 --> 00:05:42,260
We&#39;re going to use RStudio with R Open for

96
00:05:42,260 --> 00:05:46,330
those of you who wanna use R.

97
00:05:46,330 --> 00:05:49,060
But there&#39;s other development environments that are worth

98
00:05:49,060 --> 00:05:49,880
thinking about.

99
00:05:49,880 --> 00:05:55,020
For example, Visual Studio has plugins for these tools.

100
00:05:55,020 --> 00:05:57,030
And if you were doing a project where, say,

101
00:05:57,030 --> 00:06:00,020
you also needed to do some C# code or

102
00:06:00,020 --> 00:06:04,340
something like that or C++, having an integrated environment

103
00:06:04,340 --> 00:06:07,538
that&#39;s multi-language like that could be quite a nice thing.

104
00:06:07,538 --> 00:06:12,560
And finally, both languages R and

105
00:06:12,560 --> 00:06:17,340
Python are now supported by kernels in Jupyter notebooks.

106
00:06:17,340 --> 00:06:19,290
We&#39;re not gonna do a lot with that in the course,

107
00:06:19,290 --> 00:06:20,860
but be aware of that.

108
00:06:20,860 --> 00:06:23,280
You might want to explore that at some point on your own.

109
00:06:23,280 --> 00:06:29,420
Jupyter gives you a way to create, basically an interactive

110
00:06:29,420 --> 00:06:32,950
log of your analysis that you can communicate with your

111
00:06:32,950 --> 00:06:36,760
data science colleagues and they can reproduce what you did.

112
00:06:36,760 --> 00:06:38,860
You can all talk about it.

113
00:06:38,860 --> 00:06:39,820
Things like that.

114
00:06:39,820 --> 00:06:41,050
So that&#39;s actually very cool.

115
00:06:43,810 --> 00:06:46,230
So, you may be thinking to yourself,

116
00:06:46,230 --> 00:06:48,710
gee should I do the R or should I do the Python?

117
00:06:48,710 --> 00:06:51,270
You know, which way should I go on this thing?

118
00:06:51,270 --> 00:06:54,810
And honestly I don&#39;t think you&#39;re gonna go wrong

119
00:06:54,810 --> 00:06:59,280
either way, they&#39;re both widely used, they&#39;re both great.

120
00:06:59,280 --> 00:07:01,960
They&#39;re both powerful open-source tools.

121
00:07:03,130 --> 00:07:07,290
And as all things in the real world neither is perfect,

122
00:07:08,920 --> 00:07:11,420
you know, one is better at one thing one is better at another.

123
00:07:12,610 --> 00:07:16,410
So in my considered opinion, some people may dispute this,

124
00:07:16,410 --> 00:07:19,440
I find that Python just as a language

125
00:07:19,440 --> 00:07:21,330
is somewhat more systematic.

126
00:07:23,330 --> 00:07:26,180
Better structured in a way.

127
00:07:26,180 --> 00:07:28,610
R tends to be a little more ad hoc.

128
00:07:28,610 --> 00:07:32,170
Also Python generally is a little faster.

129
00:07:32,170 --> 00:07:36,470
For certain things it&#39;s a lot faster like say text analytics.

130
00:07:36,470 --> 00:07:39,090
For other things like certain graphics operations it

131
00:07:39,090 --> 00:07:40,350
actually may be slower.

132
00:07:40,350 --> 00:07:45,090
But on average my experience is it&#39;s a little faster.

133
00:07:45,090 --> 00:07:50,090
On the other hand, R has a wider range of packages,

134
00:07:50,090 --> 00:07:54,990
I think there&#39;s over 6,000 now, for doing specialized analytics.

135
00:07:54,990 --> 00:07:56,990
So if you&#39;re working in some area and

136
00:07:56,990 --> 00:08:02,350
you need certain algorithms to tackle a specific problem.

137
00:08:03,410 --> 00:08:06,270
There&#39;s a slightly better chance you&#39;ll find exactly what you&#39;re

138
00:08:06,270 --> 00:08:08,730
looking for, that somebody&#39;s already done, and

139
00:08:08,730 --> 00:08:13,660
put in, what&#39;s called, CRAN, which is the R Open source

140
00:08:13,660 --> 00:08:18,010
Archive Repository for these packages, ahead of you.

141
00:08:18,010 --> 00:08:21,170
So, that&#39;s something to keep in mind.

142
00:08:21,170 --> 00:08:22,790
The other thing I like about R is,

143
00:08:22,790 --> 00:08:25,300
the graphics, I find them easier to use.

144
00:08:25,300 --> 00:08:27,860
I find them less verbose to create even a fairly

145
00:08:27,860 --> 00:08:29,550
sophisticated plot.

146
00:08:29,550 --> 00:08:32,300
And part of that is because, and I&#39;ll talk about this a lot

147
00:08:32,299 --> 00:08:36,150
in this class, there&#39;s the GG Plot 2 package, and

148
00:08:36,150 --> 00:08:38,690
it introduces a grammar of graphics.

149
00:08:38,690 --> 00:08:42,620
There&#39;s a systematic way to make even complicated many

150
00:08:42,620 --> 00:08:46,530
layer plots with all sorts of attributes.

151
00:08:46,530 --> 00:08:50,600
You can do all that in python, there&#39;s actually no reason you

152
00:08:50,600 --> 00:08:53,660
can&#39;t do everything the same in both languages.

153
00:08:53,660 --> 00:08:57,575
You might wind up writing a little more code, that&#39;s all.

154
00:08:57,575 --> 00:09:03,090
So, let me talk then about this stack.

155
00:09:03,090 --> 00:09:06,840
I mentioned that you need to keep in mind that you&#39;re doing

156
00:09:06,840 --> 00:09:10,730
your work in a context of a bigger world project,

157
00:09:10,730 --> 00:09:13,800
in a real data science project that&#39;s part of a business.

158
00:09:13,800 --> 00:09:16,980
And by business I don&#39;t necessarily just mean

159
00:09:16,980 --> 00:09:17,880
a commercial business.

160
00:09:17,880 --> 00:09:19,120
It could be an NGO.

161
00:09:19,120 --> 00:09:22,500
It could be an academic research organization.

162
00:09:22,500 --> 00:09:26,060
It could be lots of organizations, but it&#39;s somehow,

163
00:09:26,060 --> 00:09:28,360
it&#39;s a business in the sense that it has goals and

164
00:09:28,360 --> 00:09:29,999
it has to produce certain things.

165
00:09:31,080 --> 00:09:34,640
And so, on the slide that I&#39;m showing here,

166
00:09:35,980 --> 00:09:39,240
I&#39;m just gonna use Cortana Intelligence Suite.

167
00:09:39,240 --> 00:09:42,520
And it&#39;s a good canonical example of the entire stack.

168
00:09:42,520 --> 00:09:45,290
And I think going through the slide will give you that

169
00:09:45,290 --> 00:09:47,830
context of where everything fits in.

170
00:09:47,830 --> 00:09:50,880
Across the bottom here you can see we start with data.

171
00:09:50,880 --> 00:09:54,030
That&#39;s always are input as data scientists.

172
00:09:54,030 --> 00:09:56,780
We apply some kind of intelligence, typically machine

173
00:09:56,780 --> 00:09:59,220
learning or some kind of predictive analytics.

174
00:10:00,470 --> 00:10:02,940
And what we really want out of this thing is action.

175
00:10:02,940 --> 00:10:05,930
We want something that somebody can get some value from,

176
00:10:05,930 --> 00:10:07,720
do something with.

177
00:10:07,720 --> 00:10:09,720
Let&#39;s break that down in a little more detail.

178
00:10:10,920 --> 00:10:15,490
Starting out we have data sources.

179
00:10:15,490 --> 00:10:17,700
They could be databases.

180
00:10:17,700 --> 00:10:22,780
It could be feeds from a capital market&#39;s pricing.

181
00:10:22,780 --> 00:10:24,320
It could be lots of things.

182
00:10:24,320 --> 00:10:26,650
Or maybe it&#39;s people&#39;s phone apps.

183
00:10:26,650 --> 00:10:31,390
It could be sensors, Internet of Things type data.

184
00:10:31,390 --> 00:10:34,760
It could be locations of trucks or

185
00:10:34,760 --> 00:10:36,650
how many people walk through a door.

186
00:10:36,650 --> 00:10:39,810
All sorts of things where our data comes from.

187
00:10:39,810 --> 00:10:43,730
So just as vast as you can imagine,

188
00:10:43,730 --> 00:10:46,490
how many places it can come from.

189
00:10:46,490 --> 00:10:48,990
Now when you get that much data coming

190
00:10:48,990 --> 00:10:51,830
probably in reality from many sources, you need to think

191
00:10:51,830 --> 00:10:54,920
about the information management part of your stack.

192
00:10:54,920 --> 00:10:57,720
And like in Cortana Intelligence Suite, there&#39;s something

193
00:10:57,720 --> 00:11:01,210
called data factory that helps manage all the moving parts.

194
00:11:01,210 --> 00:11:05,010
There&#39;s a data catalog and this thing called the event hub,

195
00:11:05,010 --> 00:11:08,780
which is where like real time data, like Internet of

196
00:11:08,780 --> 00:11:13,800
Things data can be managed, and buffered, and things like that.

197
00:11:13,800 --> 00:11:16,280
And you need to put it somewhere

198
00:11:16,280 --> 00:11:18,900
if you&#39;re gonna model data if you&#39;re gonna test models.

199
00:11:19,960 --> 00:11:23,280
You&#39;re not gonna do it on the real time feed for example,

200
00:11:23,280 --> 00:11:26,110
if you&#39;re building a trading model or if you&#39;re trying to

201
00:11:26,110 --> 00:11:30,180
build a model to predict when aircraft engines might fail.

202
00:11:30,180 --> 00:11:32,270
You&#39;re gonna archive that data.

203
00:11:32,270 --> 00:11:33,320
It may be large amounts.

204
00:11:33,320 --> 00:11:36,820
So there&#39;s things like a data lake store, for example, or

205
00:11:36,820 --> 00:11:41,700
maybe a SQL data warehouse as we have in the Cortana suite.

206
00:11:41,700 --> 00:11:45,840
And don&#39;t discount the fact it could be something just like

207
00:11:45,840 --> 00:11:48,850
SQL server, one of its many flavors there.

208
00:11:50,080 --> 00:11:54,660
Now we get to the layer in the stack that&#39;s really where we&#39;re

209
00:11:54,660 --> 00:11:58,150
really going to focus and that&#39;s machine learning and analytic.

210
00:11:58,150 --> 00:12:00,882
So at the top there&#39;s Azure Machine Learning,

211
00:12:00,882 --> 00:12:04,376
which we&#39;re gonna, as I said, spend quite a bit of time on.

212
00:12:04,376 --> 00:12:09,098
There&#39;s also Data Lake analytics which is done through

213
00:12:09,098 --> 00:12:13,216
a special flavor of SQL, and then like Hadoop and

214
00:12:13,216 --> 00:12:16,336
Spark you can use R and Python there.

215
00:12:16,336 --> 00:12:21,089
And Stream Analytics which has its own kind of languages or

216
00:12:21,089 --> 00:12:25,360
a Python API as well, and we have to output that now.

217
00:12:25,360 --> 00:12:26,650
We need to do something with it.

218
00:12:26,650 --> 00:12:27,950
I said there had to be action.

219
00:12:27,950 --> 00:12:31,130
So you can provide intelligence to something.

220
00:12:31,130 --> 00:12:32,760
Maybe its a bot framework.

221
00:12:32,760 --> 00:12:34,330
Maybe it&#39;s a smart phone.

222
00:12:35,830 --> 00:12:38,550
Personal assistant or something like that.

223
00:12:38,550 --> 00:12:42,300
Or possibly it&#39;s a dashboard or some kind of visualization that

224
00:12:42,300 --> 00:12:45,550
you&#39;re gonna present to the bosses at your company or

225
00:12:45,550 --> 00:12:48,390
something like that, so something like Power BI.

226
00:12:49,430 --> 00:12:53,730
And finally, we have the consumers of our results.

227
00:12:53,730 --> 00:12:55,250
This is where the actions actually up.

228
00:12:55,250 --> 00:13:00,080
It could be people who get a new AHA insight from our analytics.

229
00:13:00,080 --> 00:13:03,810
It could be apps in a phone or on the web or a bot, that does

230
00:13:03,810 --> 00:13:07,500
something cool, and interesting, and helpful to people.

231
00:13:07,500 --> 00:13:10,650
Or it could be an automated system that says, gee,

232
00:13:10,650 --> 00:13:14,240
that machine in our factory is getting too hot.

233
00:13:14,240 --> 00:13:17,130
We need to start cycling it down, or something like that.

234
00:13:17,130 --> 00:13:24,700
So, lots of possibilities here for this stack.

235
00:13:24,700 --> 00:13:27,169
And I hope you keep it in context then,

236
00:13:27,169 --> 00:13:28,867
where our data science and

237
00:13:28,867 --> 00:13:32,816
machine learning slice fits into that much bigger picture.

