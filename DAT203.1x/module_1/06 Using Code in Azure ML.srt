0
00:00:04,819 --> 00:00:05,701
Hi, and welcome back.

1
00:00:05,701 --> 00:00:09,136
So to continue building our experiment here,

2
00:00:09,136 --> 00:00:13,892
our first experiment, we&#39;re going to add some custom Python,

3
00:00:13,892 --> 00:00:17,254
R, and SQL code in modules to that workflow.

4
00:00:17,254 --> 00:00:21,082
So let&#39;s talk about developing R, and

5
00:00:21,082 --> 00:00:25,959
testing R in Python, in Azure Machine Learning.

6
00:00:25,959 --> 00:00:29,605
So one thing to keep in mind, is when you&#39;re developing

7
00:00:29,605 --> 00:00:32,631
custom modules, Azure Machine Learning is,

8
00:00:32,631 --> 00:00:36,604
just think about it kind of as your production environment.

9
00:00:36,604 --> 00:00:41,423
It&#39;s really not the best place to interactively

10
00:00:41,423 --> 00:00:44,133
develop R Python SQL code.

11
00:00:44,133 --> 00:00:47,896
I encourage you to use, for R and Python especially,

12
00:00:47,896 --> 00:00:48,822
I use an IDE.

13
00:00:48,822 --> 00:00:53,044
Could be R Studio for R, Spider for Python,

14
00:00:53,044 --> 00:00:57,611
VisualStudio is another good choice for both,

15
00:00:57,611 --> 00:01:00,933
depending on what you&#39;re doing.

16
00:01:00,933 --> 00:01:05,335
If you need to test, say in a desktop environment with an IDE,

17
00:01:05,334 --> 00:01:09,315
if you&#39;re working with large data sets that Azure ML can

18
00:01:09,315 --> 00:01:13,972
handle with it&#39;s large memory, that maybe your desktop machine

19
00:01:13,972 --> 00:01:18,291
is choking on, there&#39;s ways to subset that data down just to

20
00:01:18,291 --> 00:01:22,357
test the code, and then download the .csv file onto your

21
00:01:22,357 --> 00:01:27,127
desktop environment from your Azure Machine Learning workflow.

22
00:01:27,127 --> 00:01:31,117
And when you&#39;re in the IDE, the reason I&#39;m encouraging to do

23
00:01:31,117 --> 00:01:33,802
that, is you have a powerful editor, and

24
00:01:33,802 --> 00:01:36,565
a debugger, and all those nice features,

25
00:01:36,565 --> 00:01:40,633
which you&#39;re not gonna find in a production environment, but

26
00:01:40,633 --> 00:01:43,579
you will find in a development environment.

27
00:01:43,579 --> 00:01:47,407
And then when you&#39;re done, cut and paste the code into your

28
00:01:47,407 --> 00:01:50,241
Execute R or Execute Python Script module,

29
00:01:50,241 --> 00:01:53,457
which I&#39;ll show you how to do that in a minute, and

30
00:01:53,457 --> 00:01:56,153
then you test in Azure Machine Learning.

31
00:01:56,153 --> 00:01:57,731
Or another alternative,

32
00:01:57,731 --> 00:02:01,388
if you happen to be already keen on using Jupyter notebooks,

33
00:02:01,388 --> 00:02:04,272
you can do the same thing in Jupyter notebooks.

34
00:02:04,272 --> 00:02:07,380
You can test the code there, and then cut and

35
00:02:07,380 --> 00:02:10,744
paste it into Azure Machine Learning modules.

36
00:02:10,744 --> 00:02:13,069
So different ways to go here, but

37
00:02:13,069 --> 00:02:15,403
what you&#39;re comfortable with.

38
00:02:15,403 --> 00:02:17,887
My personal preference is to use an IDE, but

39
00:02:17,887 --> 00:02:19,980
that may not be right for everybody.

40
00:02:19,980 --> 00:02:23,469
So let&#39;s just talk about SQL for a minute here.

41
00:02:23,469 --> 00:02:26,166
I&#39;m not gonna go into that a lot, but

42
00:02:26,166 --> 00:02:30,690
I thought it was useful just to show you very basically what was

43
00:02:30,690 --> 00:02:34,348
gonna happen here, and it&#39;s used in two places.

44
00:02:34,348 --> 00:02:40,006
SQL is used to read tables from Azure SQL which is the SQL

45
00:02:40,006 --> 00:02:44,931
database, from the SQL data warehouse, etc.

46
00:02:44,931 --> 00:02:46,806
And there&#39;s also something called

47
00:02:46,806 --> 00:02:48,872
the Apply SQL Transformation module.

48
00:02:48,872 --> 00:02:53,200
And that lets you run a flavor of SQL called SQLite,

49
00:02:53,200 --> 00:02:56,030
just in line in your experiment.

50
00:02:56,030 --> 00:02:59,224
So if you&#39;re very comfortable with SQL and you like to filter

51
00:02:59,224 --> 00:03:01,916
your data, and you think in terms of SQL statements

52
00:03:01,916 --> 00:03:04,751
to filter data, that might be a really nice way to go.

53
00:03:04,751 --> 00:03:08,107
So let&#39;s jump to Spyder here, and

54
00:03:08,107 --> 00:03:13,679
I have in the left-hand side here, a little bit of code.

55
00:03:13,679 --> 00:03:17,363
This is very simple, I&#39;m just gonna read one pandas DataFrame

56
00:03:17,363 --> 00:03:18,900
that I call creditframe.

57
00:03:18,900 --> 00:03:21,166
So I don&#39;t have a second argument here,

58
00:03:21,166 --> 00:03:23,172
but I&#39;m only gonna have one input.

59
00:03:23,172 --> 00:03:27,171
I&#39;m going to create a list of two columns I want to remove

60
00:03:27,171 --> 00:03:30,915
from that credit data set, called SexAndStatus and

61
00:03:30,915 --> 00:03:32,885
OtherDetorsGuarantors.

62
00:03:32,885 --> 00:03:36,310
Turns out those don&#39;t actually help our machine learning model.

63
00:03:36,310 --> 00:03:39,302
I&#39;ve just determined that by experimentation,

64
00:03:39,302 --> 00:03:41,302
which we&#39;ll talk about as we go.

65
00:03:41,302 --> 00:03:45,111
But for now, just to precise it to say they&#39;re not important.

66
00:03:45,111 --> 00:03:48,005
So there&#39;s a pandas method called drop.

67
00:03:48,005 --> 00:03:52,256
So creditframe.drop, then the drop_cols list axis = 1,

68
00:03:52,256 --> 00:03:54,393
means I&#39;m dropping columns.

69
00:03:54,393 --> 00:03:58,705
And I&#39;m gonna do it in place, so that I don&#39;t need to

70
00:03:58,705 --> 00:04:03,410
allocate more memory, and I&#39;m just gonna return that.

71
00:04:03,410 --> 00:04:07,277
Okay, so let me copy that, And

72
00:04:07,277 --> 00:04:11,309
I&#39;m gonna go to my experiment here, and I will search for

73
00:04:11,309 --> 00:04:16,916
what&#39;s called the Execute, Python Script module.

74
00:04:19,526 --> 00:04:21,697
And I&#39;m just gonna connect those like that.

75
00:04:21,697 --> 00:04:26,121
So you see I&#39;ve connected the output of my dataset

76
00:04:26,121 --> 00:04:28,870
to this Dataset1 input port.

77
00:04:28,870 --> 00:04:32,084
Notice there&#39;s these nice tool tips, so I know that that&#39;s

78
00:04:32,084 --> 00:04:35,752
the Dataset1 input port, I know that&#39;s the Dataset2 input port.

79
00:04:35,752 --> 00:04:38,611
I know that&#39;s the Results dataset output,

80
00:04:38,611 --> 00:04:42,241
where my results that I end up in a pandas DataFrame, and

81
00:04:42,241 --> 00:04:45,808
return from that Python function, are gonna show up.

82
00:04:45,808 --> 00:04:51,475
So I&#39;m just gonna do a select all, and I&#39;m gonna copy, okay.

83
00:04:53,190 --> 00:04:57,289
So I&#39;m just gonna keep building my experiment here, so

84
00:04:57,289 --> 00:04:59,214
I&#39;m gonna go to RStudio.

85
00:04:59,214 --> 00:05:05,794
And much as I discussed now, I&#39;m gonna create something,

86
00:05:05,794 --> 00:05:10,020
a R data frame called credit.frame.

87
00:05:10,020 --> 00:05:12,548
Here&#39;s this function with its argument 1, so

88
00:05:12,548 --> 00:05:14,768
I&#39;m gonna read from the Dataset1 port.

89
00:05:14,768 --> 00:05:18,305
And again, I&#39;m gonna drop some more columns, that I know from

90
00:05:18,305 --> 00:05:21,846
experience, don&#39;t really help our machine learning model,

91
00:05:21,846 --> 00:05:23,140
these three columns.

92
00:05:23,140 --> 00:05:29,084
And I&#39;m going to, this is a little bit cryptic R.

93
00:05:29,084 --> 00:05:34,180
But basically, if the name of that column isn&#39;t in the list of

94
00:05:34,180 --> 00:05:39,673
drop columns, then it&#39;s going to be put into out.frame, okay?

95
00:05:39,673 --> 00:05:42,058
Sorry, that&#39;s a little bit cryptic, but

96
00:05:42,058 --> 00:05:44,120
hopefully you can understand that.

97
00:05:44,120 --> 00:05:49,574
So, I&#39;m gonna copy that, and we&#39;ll go back to our model.

98
00:05:49,574 --> 00:05:52,351
And now, here I&#39;ve got an Execute R Script.

99
00:05:55,066 --> 00:05:58,238
Okay, and again, I&#39;m gonna select all and

100
00:05:58,238 --> 00:06:00,908
I&#39;m just gonna paste that in there.

101
00:06:00,908 --> 00:06:03,898
And finally, I&#39;ve got some SQL here, and

102
00:06:03,898 --> 00:06:06,567
I&#39;m just doing a simple select here.

103
00:06:06,567 --> 00:06:11,330
So, I&#39;m gonna select the columns I want from t1 and

104
00:06:11,330 --> 00:06:15,234
I&#39;ll explain what t1 is here in a second.

105
00:06:15,234 --> 00:06:17,995
Let me go back here.

106
00:06:21,166 --> 00:06:24,078
And I&#39;m gonna search for that, and there it is,

107
00:06:24,078 --> 00:06:25,793
Apply SQL Transformation.

108
00:06:28,627 --> 00:06:31,598
And again, I&#39;m gonna take the Results Dataset output and

109
00:06:31,598 --> 00:06:32,636
connect it to this.

110
00:06:32,636 --> 00:06:37,270
It&#39;s called Table1, which then becomes t1, so

111
00:06:37,270 --> 00:06:39,756
I have Table2, Table3.

112
00:06:39,756 --> 00:06:42,508
So that&#39;s one nice thing about the Apply SQL Transform.

113
00:06:42,508 --> 00:06:45,410
And if you&#39;re trying to do something that involves more

114
00:06:45,410 --> 00:06:48,014
than two tables, that may be your go-to module.

115
00:06:48,014 --> 00:06:51,199
You see the Execute R Script, Execute Python Script can

116
00:06:51,199 --> 00:06:53,928
handle up to two tables, this one can do three.

117
00:06:53,928 --> 00:06:56,720
So there may be circumstances where that helps you.

118
00:06:56,720 --> 00:07:00,167
So let me copy that, and I&#39;ll paste that.

119
00:07:00,167 --> 00:07:03,241
And notice this little viewer here,

120
00:07:03,241 --> 00:07:05,825
so I can see my whole workflow.

121
00:07:05,825 --> 00:07:08,773
As my workflow gets long, that can become useful.

122
00:07:08,773 --> 00:07:14,644
So now I&#39;m gonna SAVE this, And I&#39;m gonna RUN it.

123
00:07:18,159 --> 00:07:20,916
There, so the experiment has run successfully, so

124
00:07:20,916 --> 00:07:22,427
I know that in a couple ways.

125
00:07:22,427 --> 00:07:25,367
Notice that all my modules have a green check mark.

126
00:07:25,367 --> 00:07:29,652
If they didn&#39;t run, they&#39;d have a white x in a red circle,

127
00:07:29,652 --> 00:07:31,985
which would indicate an error.

128
00:07:31,985 --> 00:07:35,492
And as I indicated, there&#39;s this, if you click here and

129
00:07:35,492 --> 00:07:39,290
go down here, you can view the output log, if you wanna look at

130
00:07:39,290 --> 00:07:42,381
the error message or the error log, for example.

131
00:07:42,381 --> 00:07:43,186
And also,

132
00:07:43,186 --> 00:07:47,850
it says Finished running up here at the top with a green check.

133
00:07:47,850 --> 00:07:50,621
So let&#39;s have a look at what actually happened here.

134
00:07:50,621 --> 00:07:54,607
The output of these Azure tables you can visualize.

135
00:07:54,607 --> 00:08:00,505
So I click there and I click Visualize, And

136
00:08:00,505 --> 00:08:05,225
this is what I started with, I had 21 columns, 950 rows.

137
00:08:05,225 --> 00:08:08,268
And you can see the column names that I got from the header of

138
00:08:08,268 --> 00:08:08,898
that file.

139
00:08:12,087 --> 00:08:16,384
And over here is something called CreditStatus,

140
00:08:16,384 --> 00:08:20,897
which is either 0 or 1, but has two unique values.

141
00:08:20,897 --> 00:08:23,480
So that tells us whether it&#39;s a good or

142
00:08:23,480 --> 00:08:25,351
bad customer of this bank.

143
00:08:25,351 --> 00:08:28,906
And these features are some what cryptic,

144
00:08:28,906 --> 00:08:31,579
like CheckingAccountStatus.

145
00:08:31,579 --> 00:08:35,531
You see the feature names are things like A11, A14,

146
00:08:35,530 --> 00:08:38,670
etc., A13, but those means something.

147
00:08:38,669 --> 00:08:42,030
But pay attention the fact mostly,

148
00:08:42,030 --> 00:08:45,284
that there&#39;s 21 columns here.

149
00:08:45,284 --> 00:08:50,709
So recall that our Python Script, we remove two columns.

150
00:08:50,709 --> 00:08:55,355
So with luck, we&#39;ll have 19 columns here, and indeed we do.

151
00:08:59,825 --> 00:09:05,259
And again, we removed three columns with the R Script.

152
00:09:05,259 --> 00:09:09,949
And I&#39;ll Visualize that, again we have 17 columns.

153
00:09:13,252 --> 00:09:16,560
And then at the end, we applied our SQL, and

154
00:09:16,560 --> 00:09:20,595
we should end up with 12 columns, and indeed we do.

155
00:09:20,595 --> 00:09:24,863
So we have 11 columns that contain data that help us

156
00:09:24,863 --> 00:09:29,422
predict whether our customer of our bank is gonna be good

157
00:09:29,422 --> 00:09:30,105
or bad.

158
00:09:30,105 --> 00:09:31,744
We call those features, so

159
00:09:31,744 --> 00:09:34,961
that&#39;s a term you should get used to in data science.

160
00:09:34,961 --> 00:09:38,567
A feature is some data that you use in the model to

161
00:09:38,567 --> 00:09:42,984
try to predict the result, the thing you want to predict,

162
00:09:42,984 --> 00:09:45,074
which is called the label.

163
00:09:45,074 --> 00:09:49,134
So in this case, CreditStatus is what we call our label.

164
00:09:49,134 --> 00:09:53,892
And we can look at some things,

165
00:09:53,892 --> 00:09:57,187
like we can graph it.

166
00:09:57,187 --> 00:10:01,732
So we have these two values of CreditStatus,

167
00:10:01,732 --> 00:10:06,373
0 and 1, For good and bad customers.

168
00:10:06,373 --> 00:10:10,315
And if we say how does that look versus duration, so

169
00:10:10,315 --> 00:10:14,350
Duration is how long the count was outstanding, and

170
00:10:14,350 --> 00:10:17,206
you see these sort of a dot plot here.

171
00:10:17,206 --> 00:10:19,156
We&#39;ll talk about how to interpret this later.

172
00:10:19,156 --> 00:10:23,259
I just wanted you to see how you can kind of explore your data at

173
00:10:23,259 --> 00:10:26,819
sort of a top level, using Azure Machine Learning.

174
00:10:29,258 --> 00:10:31,077
So now we have our data prepared, and

175
00:10:31,077 --> 00:10:33,481
we&#39;re ready to build a Machine Learning model,

176
00:10:33,481 --> 00:10:35,898
which what we&#39;re gonna do in the next sequence.

