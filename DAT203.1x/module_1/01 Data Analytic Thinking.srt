0
00:00:05,047 --> 00:00:06,520
Hi, welcome back.

1
00:00:06,520 --> 00:00:09,470
In this sequence we&#39;re going to talk about data analytic

2
00:00:09,470 --> 00:00:13,490
thinking which is a process whereby

3
00:00:13,490 --> 00:00:17,800
organizations think about their data assets and use those assets

4
00:00:17,800 --> 00:00:21,780
to transform how they do whatever it is they do.

5
00:00:21,780 --> 00:00:25,100
Whether it&#39;s an NGO or a commercial business or

6
00:00:25,100 --> 00:00:27,080
a hospital, whatever.

7
00:00:27,080 --> 00:00:31,100
So what are the aspects of data analytic thinking?

8
00:00:32,299 --> 00:00:35,520
So kinda the goal is

9
00:00:35,520 --> 00:00:40,470
replace intuition with data driven analytical decisions.

10
00:00:40,470 --> 00:00:43,066
So In many years past and centuries past,

11
00:00:43,066 --> 00:00:44,698
managers would just say,

12
00:00:44,698 --> 00:00:48,482
well, I&#39;ve been in this business a long time and I kinda know

13
00:00:48,482 --> 00:00:52,560
that if we do this and that it&#39;ll probably work out.

14
00:00:52,560 --> 00:00:55,250
But a lot of it was just intuition driven.

15
00:00:55,250 --> 00:00:57,350
And now with data science and

16
00:00:57,350 --> 00:01:02,880
data analytic thinking you can actually look at forecasts and

17
00:01:02,880 --> 00:01:05,790
some machine, maybe the output of machine learning models or

18
00:01:05,790 --> 00:01:12,170
something like that and get some substantiation for that point.

19
00:01:12,170 --> 00:01:14,840
And what this means is now

20
00:01:14,840 --> 00:01:19,210
organizations that often have considerable of data

21
00:01:19,210 --> 00:01:21,550
are suddenly realizing that&#39;s a valuable asset.

22
00:01:21,550 --> 00:01:22,910
That if they pull that together,

23
00:01:22,910 --> 00:01:26,580
if they organize it correctly, analyze it correctly and

24
00:01:26,580 --> 00:01:30,620
provide the machine learning capabilities to extract,

25
00:01:30,620 --> 00:01:33,530
to take that knowledge and turn it into decisions and

26
00:01:33,530 --> 00:01:37,040
actions, that can move their business tremendously.

27
00:01:38,180 --> 00:01:41,980
And part of that trend is to increase the pace of actions.

28
00:01:41,980 --> 00:01:45,340
Many what used to be manual decision processes,

29
00:01:45,340 --> 00:01:51,740
manual analysis is now being moved with predictive analytics

30
00:01:51,740 --> 00:01:55,585
to support it or even with prescriptive analytics where

31
00:01:55,585 --> 00:02:00,600
we&#39;re moving directly to the action like credit card fraud

32
00:02:00,600 --> 00:02:03,510
prevention for example where a real time decision is made.

33
00:02:04,630 --> 00:02:08,710
So what&#39;s an example of data analytic thinking?

34
00:02:09,910 --> 00:02:13,900
So one in particular that I think is really interesting is

35
00:02:13,900 --> 00:02:15,815
in some areas of medical treatment.

36
00:02:15,815 --> 00:02:20,970
Formerly, patients only showed up at a doctor

37
00:02:20,970 --> 00:02:23,710
when they felt some symptoms and then the doctor would try to

38
00:02:23,710 --> 00:02:26,620
understand what those symptoms were and possibly

39
00:02:26,620 --> 00:02:29,570
confirm with other doctors and specialists and try to come up

40
00:02:29,570 --> 00:02:33,050
with the diagnosis and then they would take action to cure

41
00:02:33,050 --> 00:02:37,840
that patient of whatever had gone wrong with them they hoped.

42
00:02:37,840 --> 00:02:43,460
But with in a data analytic way you can take a lot

43
00:02:43,460 --> 00:02:47,310
of information about a patient, whether it&#39;s lifestyle choices,

44
00:02:49,500 --> 00:02:53,490
tests, blood tests, physiology tests,

45
00:02:53,490 --> 00:02:56,930
things like that and look at the patients that are higher risk.

46
00:02:58,070 --> 00:03:01,900
And then what we really wanna do is before that patient

47
00:03:01,900 --> 00:03:04,830
actually gets sick, actually has a problem, we wanna take some

48
00:03:04,830 --> 00:03:09,230
action to help them change what they&#39;re doing.

49
00:03:09,230 --> 00:03:12,620
Maybe it&#39;s medication, maybe it&#39;s some exercise, whatever it

50
00:03:12,620 --> 00:03:16,610
is but we&#39;re taking preventative action based on the analysis of

51
00:03:16,610 --> 00:03:19,780
data over many patience, to help our highest risk patience.

52
00:03:20,800 --> 00:03:24,320
And the same approach is being used also in industrial areas,

53
00:03:24,320 --> 00:03:28,400
for example there&#39;s an area now called preventive maintenance,

54
00:03:28,400 --> 00:03:31,950
for example aircraft engine company&#39;s now,

55
00:03:31,950 --> 00:03:35,130
a big part of their value ad isn&#39;t building an engine

56
00:03:35,130 --> 00:03:38,060
an Turning it over to an aircraft manufacturer.

57
00:03:38,060 --> 00:03:40,770
They actually build real time applications that collect

58
00:03:40,770 --> 00:03:42,780
data on those engines and

59
00:03:42,780 --> 00:03:46,950
can tell the airline operating that aircraft hey, you need to

60
00:03:46,950 --> 00:03:50,380
replace this part because it&#39;s operating abnormally.

61
00:03:50,380 --> 00:03:52,580
So you don&#39;t have to wait for the plane to break down,

62
00:03:52,580 --> 00:03:56,580
and that has huge benefits in terms of lost time for

63
00:03:56,580 --> 00:04:02,480
the aircraft which is an enormous time-sink for

64
00:04:02,480 --> 00:04:05,310
the airline and a lot of lost dollars if they have to cancel

65
00:04:05,310 --> 00:04:08,010
flights because of a disabled aircraft or something like that.

66
00:04:08,010 --> 00:04:09,920
So you try to get ahead of that.

67
00:04:09,920 --> 00:04:14,250
Similar things is an application by one of the major elevator

68
00:04:14,250 --> 00:04:15,780
companies where they do the same thing.

69
00:04:15,780 --> 00:04:18,130
Elevators are mechanically very complex and

70
00:04:18,130 --> 00:04:19,500
can have a lot of breakdowns but

71
00:04:19,500 --> 00:04:24,160
now they&#39;re trying to get ahead of that with this predictive

72
00:04:24,160 --> 00:04:27,180
maintenance as opposed to preventative maintenance.

73
00:04:27,180 --> 00:04:32,080
So another example is detection of risky payment accounts.

74
00:04:32,080 --> 00:04:35,460
This could be from fraud or people who run up big

75
00:04:35,460 --> 00:04:38,890
credit card bills and just can&#39;t pay or something like that.

76
00:04:38,890 --> 00:04:43,790
So I&#39;ve been involved in this on and off for over two decades and

77
00:04:43,790 --> 00:04:47,770
in the early days we collected data, and a lot of it

78
00:04:47,770 --> 00:04:52,050
was to support manual analysis and manual decisions.

79
00:04:52,050 --> 00:04:54,710
We could provide some statistical

80
00:04:54,710 --> 00:04:57,630
analysis that would say highlight risky cases, but

81
00:04:57,630 --> 00:05:00,660
it still went through a manual analyst in those days.

82
00:05:02,770 --> 00:05:05,630
The industry moved on, even in the 90&#39;s we started

83
00:05:05,630 --> 00:05:09,310
doing real time decision using machine learning.

84
00:05:09,310 --> 00:05:12,960
Although probably we didn&#39;t call it machine learning at the time,

85
00:05:12,960 --> 00:05:14,430
that wasn&#39;t a buzzword yet.

86
00:05:15,600 --> 00:05:18,935
But where we would take transaction history and then

87
00:05:18,935 --> 00:05:23,720
real time transactions and try to look for suspicious patterns.

88
00:05:23,720 --> 00:05:26,860
And now the industry&#39;s moved onto something called anomaly

89
00:05:26,860 --> 00:05:31,540
detection where you&#39;re looking for wider, large scale

90
00:05:31,540 --> 00:05:35,850
anomalies that maybe across many accounts, which maybe indicative

91
00:05:35,850 --> 00:05:40,688
of a fraud scheme that&#39;s being perpetrated on a large scale.

92
00:05:40,688 --> 00:05:49,190
Another example is for example, bicycle rental demand.

93
00:05:49,190 --> 00:05:54,720
So imagine you&#39;re the manager of a bike sharing service,

94
00:05:54,720 --> 00:05:56,470
which a lot of large cities have now,

95
00:05:56,470 --> 00:05:59,770
where people can come check out a bike, use it for

96
00:05:59,770 --> 00:06:02,230
a while, say to get to work or back to home or

97
00:06:02,230 --> 00:06:05,270
just to go to the grocery store, whatever or just have fun.

98
00:06:06,960 --> 00:06:09,840
While there&#39;s many locations where the bikes can be picked up

99
00:06:09,840 --> 00:06:15,010
and dropped off and if you have too many bikes at a location or

100
00:06:15,010 --> 00:06:17,810
not enough bikes at another location that sort of

101
00:06:17,810 --> 00:06:21,550
overstocking and under stocking is costly.

102
00:06:21,550 --> 00:06:24,050
It basically means you&#39;re not serving your customers.

103
00:06:24,050 --> 00:06:26,270
It also means you could be losing customers for

104
00:06:26,270 --> 00:06:28,720
your system because they wind up, they show up,

105
00:06:28,720 --> 00:06:30,290
there&#39;s no bikes so what are they gonna do?

106
00:06:30,290 --> 00:06:32,640
They&#39;re gonna now have to walk of take a bus or

107
00:06:32,640 --> 00:06:34,700
something else so they&#39;re not gonna be happy.

108
00:06:36,940 --> 00:06:39,490
Manual forecasting is difficult because there&#39;s a lot of

109
00:06:39,490 --> 00:06:40,030
variables.

110
00:06:40,030 --> 00:06:43,750
Everything from weather to time of day to holidays,

111
00:06:43,750 --> 00:06:47,020
all sorts of things and it tends to be fairly inaccurate.

112
00:06:47,020 --> 00:06:51,680
So generally With manual forecasting in a lot of areas

113
00:06:51,680 --> 00:06:54,590
the only alternative was to overstock

114
00:06:54,590 --> 00:06:58,460
which you can imagine that has a high capital cost, for example,

115
00:06:58,460 --> 00:07:00,840
just having a lot more bikes than you ever really need.

116
00:07:01,840 --> 00:07:05,880
But using machine learning with a forecasting model

117
00:07:05,880 --> 00:07:10,070
you can come up with something that&#39;s much more accurate so

118
00:07:10,070 --> 00:07:12,150
you&#39;re not way over or way under.

119
00:07:13,950 --> 00:07:18,500
And that sort of forecasting is widely used, it&#39;s used for say,

120
00:07:18,500 --> 00:07:22,240
in power generation where you have power plants that may take

121
00:07:22,240 --> 00:07:26,940
a day or two to come into production or to shut down.

122
00:07:26,940 --> 00:07:30,500
You need to forecast ahead, how many boilers for

123
00:07:30,500 --> 00:07:34,100
example you need running to meet power demand so

124
00:07:34,100 --> 00:07:38,230
that you&#39;re not burning a lit of excess fuel, nor

125
00:07:38,230 --> 00:07:42,230
do you want to have power cuts because you didn&#39;t

126
00:07:42,230 --> 00:07:45,080
bring enough boilers online when you needed to.

127
00:07:47,030 --> 00:07:49,920
&gt;&gt; All right, so I&#39;m gonna tell you about a couple of examples

128
00:07:49,920 --> 00:07:52,370
from the prediction analysis lab, which is my lab.

129
00:07:52,370 --> 00:07:55,910
And the first project is with the Cambridge Police Department.

130
00:07:55,910 --> 00:07:57,210
And my collaborators are Tong Wong,

131
00:07:57,210 --> 00:07:59,900
who is a PhD student in my lab.

132
00:07:59,900 --> 00:08:00,920
And Lieutenant Dan Wagner,

133
00:08:00,920 --> 00:08:03,620
and Rich Deveri from the Cambridge Police Department.

134
00:08:03,620 --> 00:08:06,060
Now the goal of the project is to

135
00:08:07,350 --> 00:08:11,020
figure out which crimes form a series.

136
00:08:11,020 --> 00:08:13,190
In other words figure out which crimes were committed by

137
00:08:13,190 --> 00:08:15,720
the same individual or group of individuals.

138
00:08:16,870 --> 00:08:20,590
Now this is a critical problem in the field of crime analysis

139
00:08:20,590 --> 00:08:23,890
and crime analyst actually sit there everyday searching through

140
00:08:23,890 --> 00:08:26,690
loads and loads of data trying to find patterns in it

141
00:08:26,690 --> 00:08:29,160
manually just doing database queries.

142
00:08:29,160 --> 00:08:31,960
And this is a really, really difficult thing to do.

143
00:08:31,960 --> 00:08:32,990
So we figured,

144
00:08:32,990 --> 00:08:36,760
and also less than 14% of house breaks are solved nationwide.

145
00:08:36,760 --> 00:08:39,730
That&#39;s a very, very difficult problem to solve.

146
00:08:39,730 --> 00:08:42,690
And we figured that we could

147
00:08:42,690 --> 00:08:45,910
try to create automated tools to do what the police do manually.

148
00:08:45,910 --> 00:08:49,520
If we could create a tool, then it would might shave a good

149
00:08:49,520 --> 00:08:52,250
chunk of time off their work and it would allow them to find

150
00:08:52,250 --> 00:08:54,170
patterns that they might not have found otherwise.

151
00:08:54,170 --> 00:08:54,830
I mean,

152
00:08:54,830 --> 00:08:58,370
the police, they really can&#39;t do anything about a pattern unless

153
00:08:58,370 --> 00:08:59,110
they know about it.

154
00:09:00,460 --> 00:09:01,320
Now we have access to

155
00:09:01,320 --> 00:09:03,920
the full database of housebreaks in Cambridge.

156
00:09:03,920 --> 00:09:06,480
And in particular we had detailed data about several

157
00:09:06,480 --> 00:09:09,970
thousand crimes that happened all the way back to 1997.

158
00:09:09,970 --> 00:09:10,630
And here

159
00:09:10,630 --> 00:09:13,430
is an example of the data that we have about each crime.

160
00:09:13,430 --> 00:09:18,540
And the fact that they keep a really detailed database like

161
00:09:18,540 --> 00:09:23,910
this, it really illustrates the fact that crime

162
00:09:23,910 --> 00:09:26,450
series detection is an important problem for these guys.

163
00:09:26,450 --> 00:09:28,110
I mean, you don&#39;t keep a hand-annotated

164
00:09:28,110 --> 00:09:30,930
database like this, with this level of detail,

165
00:09:30,930 --> 00:09:32,420
unless the problem is really important.

166
00:09:32,420 --> 00:09:36,310
Now as it turns out

167
00:09:36,310 --> 00:09:40,090
the crime series detection problem is exactly what we call

168
00:09:40,090 --> 00:09:43,320
a subspace clustering problem in machine learning.

169
00:09:43,320 --> 00:09:47,460
And as it turns out, what the police call a crime series

170
00:09:47,460 --> 00:09:50,050
we would call a cluster of crimes.

171
00:09:50,050 --> 00:09:53,470
And what they call the modus operandi of the criminals,

172
00:09:53,470 --> 00:09:56,330
we would call that a subspace, meaning a subset

173
00:09:56,330 --> 00:09:58,170
of the features that we need to pay attention to.

174
00:09:58,170 --> 00:10:00,660
Now I don&#39;t wanna get into all the technical

175
00:10:00,660 --> 00:10:04,000
details about how this works but it&#39;s not really too different.

176
00:10:04,000 --> 00:10:07,570
Than the k means algorithm that we go over in this course.

177
00:10:07,570 --> 00:10:09,740
So what I wanna do is to give you an example of one of

178
00:10:09,740 --> 00:10:12,930
the crime series we found with our machine learning algorithm.

179
00:10:14,580 --> 00:10:18,760
Okay, so this story revolves around ten crimes that happened,

180
00:10:18,760 --> 00:10:22,010
that we isolated from the database and

181
00:10:22,010 --> 00:10:26,290
they happened around the end of 2006 and the beginning of 2007.

182
00:10:26,290 --> 00:10:29,200
And on the bottom left here is a map.

183
00:10:29,200 --> 00:10:31,800
It&#39;s a map of where all the crimes happened.

184
00:10:31,800 --> 00:10:34,850
And over here are just the numbers one through ten

185
00:10:34,850 --> 00:10:36,700
representing the ten crimes.

186
00:10:36,700 --> 00:10:39,910
And there&#39;s a line between each pair of crimes that are sort of

187
00:10:39,910 --> 00:10:41,210
similar in time and space.

188
00:10:41,210 --> 00:10:43,550
And actually constructing this graph is complicated but

189
00:10:43,550 --> 00:10:44,360
I won&#39;t get into it.

190
00:10:44,360 --> 00:10:47,690
So you should just think of each line here as being

191
00:10:47,690 --> 00:10:50,830
these two crimes are similar in time and space.

192
00:10:50,830 --> 00:10:52,530
There&#39;s a lot of information in this table, so

193
00:10:52,530 --> 00:10:53,920
I&#39;m gonna zoom into the table so

194
00:10:53,920 --> 00:10:57,402
you can see it a little bit better.

195
00:10:57,402 --> 00:10:59,340
Okay, so here are the ten crimes,

196
00:10:59,340 --> 00:11:01,950
here are the dates they occurred, location of entry,

197
00:11:01,950 --> 00:11:03,800
front door, basement door, whatever.

198
00:11:03,800 --> 00:11:07,730
Means of entry, did they pry it open, shove it open.

199
00:11:07,730 --> 00:11:10,580
The type of premises which are usually apartments but

200
00:11:10,580 --> 00:11:11,740
I guess we&#39;re missing some data here.

201
00:11:13,360 --> 00:11:17,150
And then whether or not the apartments were ransacked.

202
00:11:17,150 --> 00:11:19,980
Whether the resident was present during the break in.

203
00:11:19,980 --> 00:11:22,800
It&#39;s pretty unusual to be present during a break in but

204
00:11:22,800 --> 00:11:25,070
here it happened three times.

205
00:11:25,070 --> 00:11:27,800
The time of the day, day of the week.

206
00:11:27,800 --> 00:11:29,990
If their suspect information and victim information.

207
00:11:29,990 --> 00:11:33,160
Now the first thing that I want you to look carefully at

208
00:11:33,160 --> 00:11:34,230
are the dates here,

209
00:11:35,350 --> 00:11:37,680
like do you notice anything interesting about these dates?

210
00:11:39,270 --> 00:11:42,900
So they were happily committing crimes in november and december

211
00:11:42,900 --> 00:11:46,410
and then all of a sudden it looks like they went on

212
00:11:46,410 --> 00:11:48,740
Christmas vacation or something, and then came back and

213
00:11:48,740 --> 00:11:52,865
started committing crimes again in February and March.

214
00:11:52,865 --> 00:11:54,370
Okay, so

215
00:11:54,370 --> 00:11:58,780
this is actually the reason why the Cambridge Police Department

216
00:11:58,780 --> 00:12:02,860
actually had written these down as two separate crime series.

217
00:12:02,860 --> 00:12:06,540
But we were able to convince them that this was a single

218
00:12:06,540 --> 00:12:07,230
crime series.

219
00:12:09,270 --> 00:12:13,490
Now, so you might have thought before that there is just a weak

220
00:12:13,490 --> 00:12:15,440
connection between the first five crimes and

221
00:12:15,440 --> 00:12:18,220
crimes six through ten because there is only this

222
00:12:18,220 --> 00:12:20,330
weak connection between crimes five and six.

223
00:12:21,430 --> 00:12:23,660
But remember these lines are just representing time and

224
00:12:23,660 --> 00:12:25,210
space, right?

225
00:12:25,210 --> 00:12:28,750
It turns out, this connection is actually very strong.

226
00:12:28,750 --> 00:12:32,310
It&#39;s much stronger than the police department actually

227
00:12:32,310 --> 00:12:33,200
originally thought.

228
00:12:34,300 --> 00:12:37,130
And one way to see this is visually to look at the map

229
00:12:37,130 --> 00:12:40,040
because crimes one through five are down here.

230
00:12:40,040 --> 00:12:43,075
And then they went on vacation and then came back and

231
00:12:43,075 --> 00:12:45,670
committed the sixth crime very close to where they committed

232
00:12:45,670 --> 00:12:48,260
the first five crimes and then they moved north.

233
00:12:49,820 --> 00:12:51,990
And so one way these crimes are linked geographically is

234
00:12:51,990 --> 00:12:55,360
through this sixth crime, but not only that crime six and

235
00:12:55,360 --> 00:13:00,080
seven share some important factors with crime five.

236
00:13:01,420 --> 00:13:04,600
Zooming in again, you can see for instance that crimes five,

237
00:13:04,600 --> 00:13:08,840
six, and seven here are the only ones where the resident

238
00:13:08,840 --> 00:13:10,940
was present during the break in.

239
00:13:10,940 --> 00:13:11,780
And it&#39;s pretty rare for

240
00:13:11,780 --> 00:13:13,680
criminals to come while residents are present, so

241
00:13:13,680 --> 00:13:16,460
having three crimes that are close to each other that share

242
00:13:16,460 --> 00:13:18,130
this particular factor is important.

243
00:13:19,800 --> 00:13:22,095
Also it seems that these particular criminals often like

244
00:13:22,095 --> 00:13:23,399
to come through the front door.

245
00:13:25,077 --> 00:13:26,774
And they like to come on weekdays,

246
00:13:26,774 --> 00:13:29,465
as well during the work week, usually around midday or

247
00:13:29,465 --> 00:13:31,970
early afternoon and they don&#39;t like to ransack.

248
00:13:31,970 --> 00:13:34,830
And there are more similarities between them as well that

249
00:13:34,830 --> 00:13:38,130
are a bit more subtle that I don&#39;t necessarily

250
00:13:38,130 --> 00:13:39,000
have on the slides.

251
00:13:40,020 --> 00:13:42,660
So the connection between a crime is somehow stronger than

252
00:13:42,660 --> 00:13:43,580
it looks or

253
00:13:43,580 --> 00:13:45,910
initially which is why we were able to convinced the police

254
00:13:45,910 --> 00:13:49,410
that this crimes are actually part of the same series.

255
00:13:49,410 --> 00:13:53,432
And when I showed this to my collaborator, Lieutenant Wagner,

256
00:13:53,432 --> 00:13:57,010
he said I said I wonder what made these guys move up north.

257
00:13:57,010 --> 00:13:59,030
I mean they had a good thing going down here.

258
00:13:59,030 --> 00:14:01,930
And I said to him well,

259
00:14:01,930 --> 00:14:04,310
weren&#39;t they spotted during one of the crimes?

260
00:14:05,540 --> 00:14:07,820
Wasn&#39;t there some suspect information?

261
00:14:09,335 --> 00:14:13,350
And which crime was it that had that suspect information?

262
00:14:13,350 --> 00:14:17,790
Maybe that scared them and caused them to move north.

263
00:14:17,790 --> 00:14:20,340
And sure enough and when we look back down,

264
00:14:20,340 --> 00:14:25,090
it was crime six, it was the only crime where

265
00:14:25,090 --> 00:14:27,380
there was suspect information recorded.

266
00:14:27,380 --> 00:14:29,510
So what I think happened is that the criminals where happily

267
00:14:29,510 --> 00:14:31,040
committing crimes down here and

268
00:14:31,040 --> 00:14:33,480
then they took their vacation and then they came back and

269
00:14:33,480 --> 00:14:35,770
committed crime 6 and then they were spotted.

270
00:14:35,770 --> 00:14:37,680
And thought maybe it would be better to move out of the area

271
00:14:37,680 --> 00:14:40,460
for awhile and went north to commit crimes seven through ten.

272
00:14:40,460 --> 00:14:44,230
And this is an example of a pattern in data that&#39;s very

273
00:14:44,230 --> 00:14:47,480
subtle and it&#39;s difficult to detect by crime analysts.

274
00:14:47,480 --> 00:14:49,580
And in our case they didn&#39;t detect it.

275
00:14:49,580 --> 00:14:50,610
This particular series,

276
00:14:50,610 --> 00:14:53,350
they didn&#39;t detect it correctly in the first place but

277
00:14:53,350 --> 00:14:54,170
now they know.

278
00:14:56,820 --> 00:14:59,640
So let me talk about a second project which is

279
00:14:59,640 --> 00:15:02,540
a project on the digital clock drawing test.

280
00:15:02,540 --> 00:15:04,970
And this project is a collaboration between

281
00:15:04,970 --> 00:15:08,780
Lahey Clinic and MIT and here are my collaborators.

282
00:15:08,780 --> 00:15:11,470
So they created the Digital Clock Drawing Test and

283
00:15:11,470 --> 00:15:14,430
they&#39;ve been collecting data and analyzing data For

284
00:15:14,430 --> 00:15:16,310
more than a decade.

285
00:15:16,310 --> 00:15:19,640
Now the goal of the project is cognitive assessment and

286
00:15:19,640 --> 00:15:22,100
early detection of diseases like Alzheimer&#39;s disease,

287
00:15:22,100 --> 00:15:24,280
Dementia and Parkinson&#39;s disease.

288
00:15:24,280 --> 00:15:27,160
And the test relies on subtle information collected

289
00:15:27,160 --> 00:15:29,470
while the patients draw clocks and

290
00:15:29,470 --> 00:15:31,670
the subtle information has diagnostic value.

291
00:15:33,370 --> 00:15:34,770
Okay, so here are the instructions

292
00:15:34,770 --> 00:15:35,920
that the patients received.

293
00:15:35,920 --> 00:15:38,310
The first set of instruction is,

294
00:15:38,310 --> 00:15:40,850
draw a clock showing 10 after 11.

295
00:15:40,850 --> 00:15:44,960
So the patients draw their clock using this digital pen and

296
00:15:44,960 --> 00:15:47,110
then for the second part of the test,

297
00:15:47,110 --> 00:15:50,440
they have to copy a clock that&#39;s given to them and the whole time

298
00:15:50,440 --> 00:15:53,060
they&#39;re completing this test, the digital pen that they&#39;re

299
00:15:53,060 --> 00:15:56,460
holding is recording every subtle movement.

300
00:15:56,460 --> 00:15:59,500
And then we look at that data to detect whether or

301
00:15:59,500 --> 00:16:01,310
not they have a cognitive impairment.

302
00:16:03,900 --> 00:16:07,760
So we&#39;re using machine learning to score their tests.

303
00:16:10,075 --> 00:16:12,880
Well, machine learning is really paying off here because

304
00:16:12,880 --> 00:16:16,990
the new machine learning based method is it shown to have a far

305
00:16:16,990 --> 00:16:21,590
higher accuracy in screening for such things as memory problems.

306
00:16:21,590 --> 00:16:24,750
Like Alzheimer&#39;s disease, and mild cognitive impairment

307
00:16:25,910 --> 00:16:29,420
than is available with existing testing techniques.

308
00:16:29,420 --> 00:16:33,180
So here, an AUC of 0.5 is actually random guessing,

309
00:16:33,180 --> 00:16:35,640
and an AUC of one is perfect.

310
00:16:35,640 --> 00:16:38,470
And so whereas that in the literature they&#39;re reporting

311
00:16:38,470 --> 00:16:41,990
this range of AUCs the machine learning techniques are all

312
00:16:41,990 --> 00:16:42,610
the way up here.

313
00:16:44,050 --> 00:16:46,540
Okay so the machine learning is really paying off.

314
00:16:46,540 --> 00:16:49,100
And if you stick around in this course you&#39;ll find out what AUC

315
00:16:49,100 --> 00:16:50,860
actually means.

316
00:16:50,860 --> 00:16:53,991
But in any case you wouldn&#39;t think that data science can

317
00:16:53,991 --> 00:16:56,856
actually help you diagnose cognitive diseases but

318
00:16:56,856 --> 00:16:57,866
actually it can.

319
00:17:01,290 --> 00:17:05,310
&gt;&gt; All right, so let me wrap this up.

320
00:17:05,310 --> 00:17:06,840
What&#39;s the point of all of these?

321
00:17:06,839 --> 00:17:10,480
And it&#39;s all about data analytic thinking where

322
00:17:10,480 --> 00:17:14,510
in all these examples we&#39;ve presented, we&#39;ve replaced

323
00:17:14,510 --> 00:17:18,380
some process that used to be done perhaps by intuition or

324
00:17:18,380 --> 00:17:21,640
retrospectively with data driven analytics.

325
00:17:23,710 --> 00:17:27,670
In data, for example, the police data that was allotted data.

326
00:17:27,670 --> 00:17:30,070
They&#39;re obviously they&#39;ve been collecting it for

327
00:17:30,070 --> 00:17:35,090
over 20 years and they were manually analyzing it.

328
00:17:35,090 --> 00:17:36,810
But by using machine learning methods,

329
00:17:36,810 --> 00:17:40,020
they were able to transform it into a much more valuable asset.

330
00:17:40,020 --> 00:17:44,660
This is happening In all sorts of organizations and industries.

331
00:17:44,660 --> 00:17:47,100
And also there&#39;s this whole

332
00:17:48,420 --> 00:17:52,410
push toward increasing the pace of action to go from

333
00:17:52,410 --> 00:17:56,710
new data inputs to an actual action very quickly.

334
00:17:57,850 --> 00:17:59,843
&gt;&gt; So we&#39;re looking forward to teaching you this course.

