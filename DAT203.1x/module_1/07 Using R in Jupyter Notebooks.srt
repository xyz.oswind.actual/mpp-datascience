0
00:00:04,937 --> 00:00:08,667
Hi, in this demo, I wanna show you a very cool

1
00:00:08,667 --> 00:00:11,960
feature of Azure Machine Learning.

2
00:00:11,960 --> 00:00:17,460
That is, your ability to open and work with data sets

3
00:00:17,460 --> 00:00:21,400
using a Jupiter notebook with an R kernel.

4
00:00:23,550 --> 00:00:27,400
So on my screen here I have a really simple experiment I put

5
00:00:27,400 --> 00:00:33,300
together that has a data set, as I&#39;ve selected some columns.

6
00:00:33,300 --> 00:00:38,860
It has some little R and Python and SQL to do some things.

7
00:00:38,860 --> 00:00:44,379
And I could go just to this CSV file here and

8
00:00:44,379 --> 00:00:48,180
I could open a notebook in R.

9
00:00:48,180 --> 00:00:49,485
Here I&#39;ll just show you.

10
00:00:52,012 --> 00:00:54,504
But what I actually wanna show you,

11
00:00:54,504 --> 00:00:57,826
what if want some result and I wanna take that and

12
00:00:57,826 --> 00:01:00,667
do some interactive work in a notebook?

13
00:01:00,667 --> 00:01:05,070
So, notice I can&#39;t open a notebook here.

14
00:01:05,069 --> 00:01:09,170
It&#39;s just grayed out, all the notebooks.

15
00:01:09,170 --> 00:01:13,665
And the reason is you need to have a CSV

16
00:01:13,665 --> 00:01:17,291
output to talk to a notebook.

17
00:01:17,291 --> 00:01:21,613
But I can just add anywhere I want in my experiment path

18
00:01:21,613 --> 00:01:25,668
really, a convert to CSV and i&#39;ll just run that.

19
00:01:25,668 --> 00:01:30,095
And now, look,

20
00:01:30,095 --> 00:01:36,110
I can open notebooks.

21
00:01:36,110 --> 00:01:39,265
So let me open an R notebook, which is the point here.

22
00:01:43,406 --> 00:01:46,416
And there&#39;s a couple cells that have been generated for

23
00:01:46,416 --> 00:01:47,510
me automatically.

24
00:01:47,510 --> 00:01:49,930
This first cell here

25
00:01:53,090 --> 00:01:57,500
creates a data frame called dat, and it has all

26
00:01:57,500 --> 00:02:02,010
the information needed to get to that data from my experiment.

27
00:02:03,790 --> 00:02:09,348
And it gives me right away a head function with that.

28
00:02:09,348 --> 00:02:12,750
So, let me go ahead and

29
00:02:12,750 --> 00:02:18,540
run all those cells, and you can see it&#39;s running.

30
00:02:18,540 --> 00:02:19,430
Oops, stopped running.

31
00:02:19,430 --> 00:02:22,720
But that little thing&#39;s turning there, tells you that.

32
00:02:22,720 --> 00:02:26,850
So there, I got the head, and

33
00:02:26,850 --> 00:02:31,312
I can add another cell here, and

34
00:02:31,312 --> 00:02:35,290
let me look at the str of dat.

35
00:02:36,500 --> 00:02:39,950
And then I can just click this little arrow to run that cell.

36
00:02:41,530 --> 00:02:45,140
In there I see what&#39;s in the columns, I see the names,

37
00:02:45,140 --> 00:02:49,930
the type, and some little, first few values.

38
00:02:49,930 --> 00:02:53,510
And I have another cell that I can type some more R code and

39
00:02:53,510 --> 00:02:54,615
run interactively.

40
00:02:54,615 --> 00:02:58,450
So one last thing, if I want to save that,

41
00:02:58,450 --> 00:03:02,850
it&#39;s gonna be saved in my Azure Machine Learning workspace.

42
00:03:02,850 --> 00:03:09,830
And if I want to give it a name, just click on that.

43
00:03:09,830 --> 00:03:12,648
I can give this a name that is more memorable than this

44
00:03:12,648 --> 00:03:14,198
machine-generated name.

45
00:03:16,846 --> 00:03:20,762
So that&#39;s just how easy it is to launch a notebook and

46
00:03:20,762 --> 00:03:24,856
work with a notebook in R from Azure Machine Learning.

