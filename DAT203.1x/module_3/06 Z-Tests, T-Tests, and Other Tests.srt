0
00:00:05,191 --> 00:00:07,520
Let&#39;s talk about some other types of hypothesis test.

1
00:00:07,520 --> 00:00:11,390
So, let&#39;s say that my lemonade dispenser is suppose to produce

2
00:00:11,390 --> 00:00:15,450
10 units of lemonade and I wanna do a hypothesis test to

3
00:00:15,450 --> 00:00:17,730
determine whether the machine is broken.

4
00:00:17,730 --> 00:00:20,290
Meaning it dispenses too much or too little,

5
00:00:20,290 --> 00:00:22,490
now I can do a hypothesis test and I can check on that.

6
00:00:22,490 --> 00:00:26,060
Now see the null hypothesis obviously its that the machine

7
00:00:26,060 --> 00:00:30,830
is performing according to specifications which is that

8
00:00:30,830 --> 00:00:34,940
the amount of lemonade is normal with the mean of 10 units or

9
00:00:34,940 --> 00:00:35,550
something.

10
00:00:35,550 --> 00:00:39,470
But the standard deviation of 3 units or something like that.

11
00:00:39,470 --> 00:00:43,340
And the alternative is that the mean is not equal to 10 units

12
00:00:43,340 --> 00:00:45,780
and then you can perform a hypothesis test

13
00:00:45,780 --> 00:00:48,400
by calculating probabilities using the normal distribution,

14
00:00:48,400 --> 00:00:50,980
and you don&#39;t have to do this yourself.

15
00:00:50,980 --> 00:00:52,810
You just need to know what the test is called.

16
00:00:52,810 --> 00:00:55,355
So if you can call the right function on the computer

17
00:00:55,355 --> 00:01:00,200
then it&#39;ll do the whole test for you.

18
00:01:00,200 --> 00:01:04,250
Now if the test uses the normal distribution with a known

19
00:01:04,250 --> 00:01:08,210
mean and variance, it is called a z-test.

20
00:01:08,210 --> 00:01:11,220
Now doing a z-test on a computer is really easy.

21
00:01:13,190 --> 00:01:17,820
So the inputs to the test are the data, the list of numbers.

22
00:01:17,820 --> 00:01:20,250
Your null hypothesis which is that the mean of

23
00:01:20,250 --> 00:01:25,510
the distribution that the data come from is 10 and

24
00:01:25,510 --> 00:01:26,545
standard deviation 3.

25
00:01:28,200 --> 00:01:30,280
And then the alternative hypothesis is the thing you

26
00:01:30,280 --> 00:01:34,640
wanna prove and you have to tell it right tail or left or both.

27
00:01:34,640 --> 00:01:37,951
The right tail or left is just whether the alternative is that

28
00:01:37,951 --> 00:01:42,450
views less than 10, or greater than 10, or not equal to 10.

29
00:01:42,450 --> 00:01:46,780
And then the significance level you put in is

30
00:01:46,780 --> 00:01:48,805
pretty much always 0.05.

31
00:01:48,805 --> 00:01:51,630
And you just feed this into the computer.

32
00:01:51,630 --> 00:01:56,320
You tell it, please computer do a z-test and the outputs

33
00:01:56,320 --> 00:02:00,260
are a yes or no, should you reject the null hypothesis?

34
00:02:00,260 --> 00:02:02,653
You get a p-value if you rejected.

35
00:02:02,653 --> 00:02:07,883
Obviously the p-value is below 0.05 and then, sometimes

36
00:02:07,883 --> 00:02:12,160
you might get a confidence interval or a z-score.

37
00:02:12,160 --> 00:02:17,513
Now as you&#39;ve noticed, you have to know sigma to do a z-test.

38
00:02:17,513 --> 00:02:19,489
And that&#39;s great if you know sigma or

39
00:02:19,489 --> 00:02:22,630
if you can estimate it pretty wellbBut what if you don&#39;t?

40
00:02:23,860 --> 00:02:26,687
Then you do what&#39;s called a t-test.

41
00:02:26,687 --> 00:02:31,900
Now, t-tests are useful when you have small samples from

42
00:02:31,900 --> 00:02:35,000
a normal distribution and you do not know the variants of it.

43
00:02:37,720 --> 00:02:40,630
So, if you know the variance, you can do a z-test,

44
00:02:40,630 --> 00:02:45,750
but if you don&#39;t know it, you know you can do this t-test.

45
00:02:45,750 --> 00:02:47,980
Okay and so thanks to William Sealy Gosset,

46
00:02:47,980 --> 00:02:48,790
none of us are stuck.

47
00:02:48,790 --> 00:02:51,230
So William Sealy Gosset is the person who invented the t-test

48
00:02:51,230 --> 00:02:53,920
and Gosset was a really interesting guy.

49
00:02:53,920 --> 00:02:55,140
He was working for

50
00:02:55,140 --> 00:02:58,070
the Guinness Brewing Company who wouldn&#39;t let him publish

51
00:02:58,070 --> 00:03:00,200
anything because they didn&#39;t wanna release trade secrets.

52
00:03:00,200 --> 00:03:03,785
So he published under the pen name of student, so

53
00:03:03,785 --> 00:03:06,152
this guy invented the t-test.

54
00:03:06,152 --> 00:03:08,791
And this is from when the data comes from a normal distribution

55
00:03:08,791 --> 00:03:11,580
but you don&#39;t know the variance of the distribution.

56
00:03:11,580 --> 00:03:14,690
And so, the null hypothesis ends up

57
00:03:14,690 --> 00:03:17,810
looking like this distribution that I have on the screen.

58
00:03:17,810 --> 00:03:20,650
And the t distribution is not quite the same as the normal

59
00:03:20,650 --> 00:03:23,700
distribution but it looks very, very similar to it.

60
00:03:23,700 --> 00:03:26,210
It&#39;s just that the tails are a little bit wider

61
00:03:26,210 --> 00:03:27,480
because there&#39;s more uncertainty,

62
00:03:27,480 --> 00:03:29,720
and that is because you don&#39;t know the variants.

63
00:03:31,230 --> 00:03:34,570
There&#39;s actually a whole family of t distributions, there&#39;s

64
00:03:34,570 --> 00:03:39,720
a distribution for each possible size of your data, your sample.

65
00:03:39,720 --> 00:03:43,260
As the data gets larger the t distributions get closer and

66
00:03:43,260 --> 00:03:45,840
closer to the normal distribution.

67
00:03:45,840 --> 00:03:48,910
And this one here is if you have five data points.

68
00:03:48,910 --> 00:03:51,830
And this one is for when you have 20 data points, and

69
00:03:51,830 --> 00:03:54,760
by this point the distribution looks more and more normal.

70
00:03:54,760 --> 00:03:56,970
Okay, and

71
00:03:56,970 --> 00:03:59,160
this one is actually the normal distribution by the way.

72
00:03:59,160 --> 00:04:01,530
You can&#39;t actually tell the difference, I mean I&#39;m just

73
00:04:01,530 --> 00:04:04,170
showing you a whole bunch of bumps that all look the same.

74
00:04:04,170 --> 00:04:08,570
And as it turns out, the sample standard deviation converges

75
00:04:08,570 --> 00:04:10,480
quickly to the true standard deviation.

76
00:04:10,480 --> 00:04:13,530
So, once you have above the 30 data points in here.

77
00:04:13,530 --> 00:04:16,780
You can&#39;t actually tell apart the t distribution from

78
00:04:16,779 --> 00:04:17,770
the normal distribution.

79
00:04:17,769 --> 00:04:20,830
So, you can just pretend that you know that

80
00:04:20,829 --> 00:04:22,410
true standard deviation at that point.

81
00:04:23,710 --> 00:04:24,480
At that point,

82
00:04:24,480 --> 00:04:26,660
the t-test becomes essentially the same thing as the z test.

83
00:04:27,900 --> 00:04:29,940
You could just use either one of them and

84
00:04:29,940 --> 00:04:30,890
you&#39;ll get the same answer.

85
00:04:32,260 --> 00:04:35,100
Now this is the actually formula for the t distribution.

86
00:04:35,100 --> 00:04:37,040
You never need to know this formula.

87
00:04:37,040 --> 00:04:39,570
All you need to know is that there is such a thing as a t

88
00:04:39,570 --> 00:04:40,270
distribution.

89
00:04:40,270 --> 00:04:42,910
So you can happily do as many t-tests as you want and

90
00:04:42,910 --> 00:04:45,420
you never need to see this formula again.

91
00:04:47,310 --> 00:04:50,500
Okay, so how do you conduct a single sample t-test?

92
00:04:50,500 --> 00:04:51,640
You don&#39;t have to give it sigma.

93
00:04:52,660 --> 00:04:53,730
That&#39;s not assumed to be known.

94
00:04:53,730 --> 00:04:59,120
You give it your data, the pile of numbers, your null

95
00:04:59,120 --> 00:05:03,715
hypothesis, which here is the mean of true distribution is 10.

96
00:05:05,740 --> 00:05:08,370
You give the thing you wanna prove.

97
00:05:08,370 --> 00:05:12,082
So, which tail do you care about or both, and

98
00:05:12,082 --> 00:05:16,987
then the significance level, which as usual is 0.05.

99
00:05:16,987 --> 00:05:19,243
And what it returns to you is whether or

100
00:05:19,243 --> 00:05:21,790
not the test rejects the null hypothesis.

101
00:05:22,840 --> 00:05:27,307
The p-value and the confidence interval perhaps.

102
00:05:27,307 --> 00:05:29,770
Okay, so let&#39;s go over when to use these tests.

103
00:05:29,770 --> 00:05:35,870
We talked about the single sample z-test,

104
00:05:35,870 --> 00:05:40,000
and you can use that either when the data are already

105
00:05:40,000 --> 00:05:44,800
normal with the standard deviation known.

106
00:05:46,320 --> 00:05:50,320
But you could also use it when n is greater than or equal to 30.

107
00:05:50,320 --> 00:05:53,430
Doesn&#39;t matter what the distribution of the data

108
00:05:53,430 --> 00:05:56,910
are as long they&#39;re independent, because then the central limit

109
00:05:56,910 --> 00:06:00,420
theorem kicks in and things are approximately normal.

110
00:06:01,690 --> 00:06:06,980
And then in that case The sample standard deviation is very,

111
00:06:06,980 --> 00:06:09,720
very close to the true standard deviation.

112
00:06:09,720 --> 00:06:15,220
And so you can use the z-test and plug in the sample standard

113
00:06:15,220 --> 00:06:20,175
deviation as if it were the true standard deviation.

114
00:06:20,175 --> 00:06:22,124
So that&#39;s when you can use the z-test,

115
00:06:22,124 --> 00:06:23,910
either of these two circumstances.

116
00:06:23,910 --> 00:06:29,030
So first, when the data are known to be normal, sigma known,

117
00:06:29,030 --> 00:06:34,580
or when the number of samples is really large and in that case,

118
00:06:34,580 --> 00:06:36,910
the central limit theorem says everything is normal.

119
00:06:36,910 --> 00:06:38,830
So you can use the z-test.

120
00:06:38,830 --> 00:06:42,660
And then the other test we talked about was

121
00:06:42,660 --> 00:06:44,870
the single sample t-test.

122
00:06:44,870 --> 00:06:48,070
And so here you&#39;d use it only for n less than 30, was or

123
00:06:48,070 --> 00:06:51,200
equal to 30, because if you&#39;re using it above or equal 30,

124
00:06:51,200 --> 00:06:55,790
it&#39;s the same as z-test, so doesn&#39;t matter.

125
00:06:55,790 --> 00:06:59,170
And also for the t-test, the data have to be normal.

126
00:07:00,720 --> 00:07:02,200
They have to be normal.

127
00:07:03,940 --> 00:07:07,539
Now there are many other tests that I don&#39;t have time to

128
00:07:07,539 --> 00:07:08,171
go over.

129
00:07:08,171 --> 00:07:11,798
But what I can do is kinda summarize them for you,

130
00:07:11,798 --> 00:07:15,530
at least point you in a direction of where to look.

131
00:07:16,800 --> 00:07:18,930
So let&#39;s say that you have a bunch of data and

132
00:07:18,930 --> 00:07:20,910
you don&#39;t know what distribution they come from.

133
00:07:22,240 --> 00:07:24,800
You can actually do tests on the median.

134
00:07:24,800 --> 00:07:25,700
You can test

135
00:07:27,380 --> 00:07:29,900
that whether the median equals this specific value.

136
00:07:30,930 --> 00:07:33,890
And the names of those tests are the sign test and

137
00:07:33,890 --> 00:07:35,350
the signed rank test.

138
00:07:35,350 --> 00:07:39,492
These are what are called non-parametric hypothesis tests.

139
00:07:41,900 --> 00:07:43,430
And there are a bunch of tests on the variants.

140
00:07:43,430 --> 00:07:46,120
You could test whether the variance of the distribution is

141
00:07:46,120 --> 00:07:47,780
greater or smaller than something or

142
00:07:47,780 --> 00:07:51,140
other, and that chi-squared test on the variants.

143
00:07:52,970 --> 00:07:54,510
And then if you have binary data,

144
00:07:54,510 --> 00:07:56,800
you can do tests on proportion.

145
00:07:56,800 --> 00:08:00,910
So you could say, I want to test the hypothesis that

146
00:08:00,910 --> 00:08:05,260
the proportion

147
00:08:05,260 --> 00:08:09,360
of one&#39;s in my population is above a certain value.

148
00:08:09,360 --> 00:08:12,870
And those test use some binomial,

149
00:08:12,870 --> 00:08:14,250
uses binomial distribution or

150
00:08:14,250 --> 00:08:15,986
the normal distribution if you have enough data.

151
00:08:15,986 --> 00:08:21,970
Okay, so essentially the idea is you look at your

152
00:08:21,970 --> 00:08:27,020
data and you figure out which hypothesis test you need and

153
00:08:27,020 --> 00:08:29,830
what the assumptions are, and

154
00:08:29,830 --> 00:08:31,820
the options are pretty much always the same.

155
00:08:31,820 --> 00:08:34,800
You send in the data, you tell whether to do a right tailed

156
00:08:34,799 --> 00:08:37,610
test, a left tailed test or both, depending on the thing

157
00:08:37,610 --> 00:08:40,630
you&#39;re trying to prove, your alternative hypothesis.

158
00:08:40,630 --> 00:08:42,890
You tell it what the null hypothesis is and

159
00:08:42,890 --> 00:08:45,850
you tell it the significance level, and off it goes and

160
00:08:45,850 --> 00:08:46,610
does the test for you.

161
00:08:47,750 --> 00:08:51,370
And there are also tests when you have two samples.

162
00:08:51,370 --> 00:08:54,360
Now if you have, now sample one and sample two.

163
00:08:54,360 --> 00:08:57,770
Let&#39;s say you want to determine whether the population,

164
00:08:57,770 --> 00:09:01,630
the sample one comes from as a larger mean

165
00:09:01,630 --> 00:09:03,640
than the population in sample two comes from.

166
00:09:05,310 --> 00:09:07,540
Now, you can do that as well.

167
00:09:07,540 --> 00:09:11,590
So, what you would do is send into the computer both samples.

168
00:09:11,590 --> 00:09:13,500
You tell it which test to conduct, and

169
00:09:13,500 --> 00:09:15,430
here we can do a t-test.

170
00:09:15,430 --> 00:09:18,940
You tell it the null hypothesis, which is that the means of

171
00:09:18,940 --> 00:09:24,390
the two populations are the same and

172
00:09:24,390 --> 00:09:27,330
then you can tell it what you want it to prove.

173
00:09:27,330 --> 00:09:29,780
Do you want it to prove that the first

174
00:09:29,780 --> 00:09:33,360
population mean is bigger than the second one, or

175
00:09:33,360 --> 00:09:36,630
vice versa, or that the population means are different?

176
00:09:37,730 --> 00:09:39,140
And you give it a significance level.

177
00:09:40,350 --> 00:09:45,480
So all this stuff goes into the computer, we hit Go and

178
00:09:45,480 --> 00:09:50,204
the outputs are whether the test rejects or not and the p-value,

179
00:09:50,204 --> 00:09:51,860
and sometimes the confidence interval.

180
00:09:54,090 --> 00:09:57,380
Now, there are actually a few different t-tests you can

181
00:09:57,380 --> 00:10:00,180
conduct for the two samples setting and

182
00:10:00,180 --> 00:10:02,910
I think it&#39;s worthwhile to just go through them with you.

183
00:10:02,910 --> 00:10:06,930
Because it actually, you can actually get really messed up if

184
00:10:06,930 --> 00:10:09,750
you don&#39;t know which test you&#39;re conducting.

185
00:10:09,750 --> 00:10:12,650
And the first question you have to ask yourself when you have

186
00:10:12,650 --> 00:10:16,820
two samples is are they independent, or are they paired?

187
00:10:19,310 --> 00:10:20,450
Now independent

188
00:10:21,920 --> 00:10:25,200
samples are samples that were collected completely separately.

189
00:10:26,440 --> 00:10:30,310
Now a tell tale sign that you have independent random samples

190
00:10:30,310 --> 00:10:32,710
is when the number of observations

191
00:10:32,710 --> 00:10:35,290
in the two samples are different.

192
00:10:35,290 --> 00:10:38,400
Okay so for instance I could grab a bunch of random smokers,

193
00:10:38,400 --> 00:10:39,960
maybe 100 of them.

194
00:10:39,960 --> 00:10:43,770
And I could grab 200 random nonsmokers and

195
00:10:43,770 --> 00:10:45,300
those would be independent samples.

196
00:10:46,750 --> 00:10:50,930
And for paired samples, an experiment has to be conducted

197
00:10:50,930 --> 00:10:55,750
in a way that pairs exist between the first sample and

198
00:10:55,750 --> 00:10:57,240
the second sample.

199
00:10:57,240 --> 00:11:01,510
So the first observation of one sample naturally

200
00:11:01,510 --> 00:11:04,000
has some connection to the observation of the second set.

201
00:11:05,550 --> 00:11:08,480
For instance, maybe we&#39;re pairing each smoker with

202
00:11:08,480 --> 00:11:13,493
a nonsmoker, but maybe the first smoker

203
00:11:13,493 --> 00:11:18,060
is actually, maybe the nonsmoker is actually just

204
00:11:19,470 --> 00:11:23,250
the smoker who quit, so it&#39;s actually the same person.

205
00:11:23,250 --> 00:11:25,890
In that case there&#39;s a natural pairing between the smokers and

206
00:11:25,890 --> 00:11:28,330
the non smokers.

207
00:11:28,330 --> 00:11:31,210
Now if your data are paired, you are in really good shape

208
00:11:31,210 --> 00:11:33,890
because you can conduct a matched pairs t-test.

209
00:11:33,890 --> 00:11:35,620
So let me show you that one.

210
00:11:35,620 --> 00:11:38,600
And I&#39;m going to discuss independent samples in a minute

211
00:11:38,600 --> 00:11:40,110
because that one gets a little more complicated.

212
00:11:41,260 --> 00:11:44,410
Okay, so matched pairs t-test, simple, simple.

213
00:11:44,410 --> 00:11:46,410
You put in to the computer the two samples,

214
00:11:46,410 --> 00:11:50,800
now remember this first guy, he&#39;s paired with the second guy.

215
00:11:50,800 --> 00:11:53,390
They&#39;re somehow united like a before and

216
00:11:53,390 --> 00:11:55,180
after, something like that.

217
00:11:56,830 --> 00:12:00,710
And then you tell it to do a matched pairs t-test, you can

218
00:12:00,710 --> 00:12:03,920
give it your null hypothesis which is that the two means

219
00:12:03,920 --> 00:12:04,560
are the same.

220
00:12:06,440 --> 00:12:08,930
And then you tell it which you want to prove, with

221
00:12:10,350 --> 00:12:13,720
the significance level as usual, and the outputs are all again.

222
00:12:13,720 --> 00:12:16,070
Always usual, does the test reject?

223
00:12:16,070 --> 00:12:20,333
And you get a p-value and maybe a confidence interval.

224
00:12:20,333 --> 00:12:23,076
Now if your samples are independent,

225
00:12:23,076 --> 00:12:25,484
you have another choice to make.

226
00:12:25,484 --> 00:12:30,235
That determines what test to conduct, because there

227
00:12:30,235 --> 00:12:35,340
are multiple independent sample hypothesis t-tests.

228
00:12:35,340 --> 00:12:40,280
Now, the key assumption is whether you can assume variances

229
00:12:40,280 --> 00:12:41,890
of the two populations are equal.

230
00:12:41,890 --> 00:12:46,420
So can you assume this even if you don&#39;t know the variances,

231
00:12:46,420 --> 00:12:47,110
that&#39;s the tricky bit.

232
00:12:49,540 --> 00:12:50,340
Because there are these,

233
00:12:50,340 --> 00:12:52,330
there are two different tests that you would conduct.

234
00:12:54,510 --> 00:12:59,930
So, the way you do it, the calculations

235
00:12:59,930 --> 00:13:02,330
are actually different if you choose equal variances or

236
00:13:02,330 --> 00:13:04,140
if you choose unequal variances.

237
00:13:04,140 --> 00:13:08,650
So be aware, if you choose equal variances in this test,

238
00:13:08,650 --> 00:13:11,070
you&#39;re making a stronger assumption.

239
00:13:11,070 --> 00:13:13,430
So the test is going to be more powerful.

240
00:13:13,430 --> 00:13:15,660
But if you choose unequal variances,

241
00:13:15,660 --> 00:13:17,330
you&#39;re making a less strong assumption.

242
00:13:17,330 --> 00:13:20,170
So your test won&#39;t be as powerful, but

243
00:13:20,170 --> 00:13:22,590
at least in that case you&#39;re not making untrue assumptions and

244
00:13:22,590 --> 00:13:24,396
it&#39;s more likely that the result will hold.

245
00:13:24,396 --> 00:13:32,700
So I usually use unequal variances.

246
00:13:32,700 --> 00:13:35,270
Okay, so I don&#39;t assume equal variances.

247
00:13:35,270 --> 00:13:37,008
You put all the stuff in and

248
00:13:37,008 --> 00:13:40,491
this is what you get out as usual, that&#39;s the test.

