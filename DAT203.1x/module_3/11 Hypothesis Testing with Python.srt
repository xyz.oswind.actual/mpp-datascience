0
00:00:01,090 --> 00:00:04,120
Hi and welcome so Cynthia has been

1
00:00:04,120 --> 00:00:07,630
discussing two important concepts and

2
00:00:07,630 --> 00:00:09,700
statistics which is confidence intervals

3
00:00:09,700 --> 00:00:13,450
and hypothesis testing and in this demo

4
00:00:13,450 --> 00:00:16,449
I&#39;m going to use some tools from Python

5
00:00:16,449 --> 00:00:20,230
to pull those two concepts together so

6
00:00:20,230 --> 00:00:22,869
you can see how statisticians we

7
00:00:22,869 --> 00:00:26,619
actually use those concepts to determine

8
00:00:26,619 --> 00:00:31,090
whether say two populations are the same

9
00:00:31,090 --> 00:00:33,430
or not and i&#39;m going to use a very

10
00:00:33,430 --> 00:00:35,410
famous data set

11
00:00:35,410 --> 00:00:38,770
Francis Galton who invented regression

12
00:00:38,770 --> 00:00:41,710
published this a paper on this data set

13
00:00:41,710 --> 00:00:44,649
in 1886 it had to do with the heights of

14
00:00:44,649 --> 00:00:47,739
parents and their adult children in 19th

15
00:00:47,739 --> 00:00:50,379
century London he was using this to

16
00:00:50,379 --> 00:00:51,850
introduce the whole concept of

17
00:00:51,850 --> 00:00:53,860
regression at the time we&#39;re going to do

18
00:00:53,860 --> 00:00:55,149
something slightly different we&#39;re going

19
00:00:55,149 --> 00:00:57,520
to look at some hypothesis test we can

20
00:00:57,520 --> 00:00:59,739
use with this very famous data set of

21
00:00:59,739 --> 00:01:02,760
Dalton&#39;s

22
00:01:03,410 --> 00:01:05,480
so on my screen here i have this

23
00:01:05,480 --> 00:01:08,330
notebook and i&#39;m just going to run the

24
00:01:08,330 --> 00:01:11,150
first sale here which just loads the

25
00:01:11,150 --> 00:01:12,790
data

26
00:01:12,790 --> 00:01:15,560
and we&#39;ll look at the

27
00:01:15,560 --> 00:01:19,790
first few rows of the data so what do we

28
00:01:19,790 --> 00:01:22,159
have here we have the case which is just

29
00:01:22,159 --> 00:01:24,829
a number galten gave it the family so

30
00:01:24,829 --> 00:01:27,710
these people came from unique families

31
00:01:27,710 --> 00:01:29,360
so golden gave them unique family

32
00:01:29,360 --> 00:01:32,299
numbers the height of the father of the

33
00:01:32,299 --> 00:01:34,189
family and inches the height of the

34
00:01:34,189 --> 00:01:37,520
mother and family and inches the average

35
00:01:37,520 --> 00:01:39,619
height of the parents the number of

36
00:01:39,619 --> 00:01:42,200
children that that family had and then

37
00:01:42,200 --> 00:01:44,630
the child number the gender of the child

38
00:01:44,630 --> 00:01:47,720
and the height of that adult child

39
00:01:47,720 --> 00:01:49,070
ok

40
00:01:49,070 --> 00:01:51,590
and let&#39;s have a look at the size of

41
00:01:51,590 --> 00:01:53,270
that data set

42
00:01:53,270 --> 00:01:56,630
you see there were 934 cases that were

43
00:01:56,630 --> 00:01:58,880
golden apparently went out sometime in

44
00:01:58,880 --> 00:02:00,979
the early eighteen eighties and measured

45
00:02:00,979 --> 00:02:03,979
these people so to get a better feel for

46
00:02:03,979 --> 00:02:06,590
that data set i&#39;m going to create some

47
00:02:06,590 --> 00:02:09,350
histograms and I&#39;m going to do it for a

48
00:02:09,350 --> 00:02:12,980
case here we&#39;re going to look at the

49
00:02:12,980 --> 00:02:14,660
gender which is the gender of the child

50
00:02:14,660 --> 00:02:16,310
is male

51
00:02:16,310 --> 00:02:17,630
so we&#39;re going to compare and we&#39;re

52
00:02:17,630 --> 00:02:21,380
going to call that new data frame sons

53
00:02:21,380 --> 00:02:24,250
and

54
00:02:24,250 --> 00:02:26,500
we&#39;re gonna make a histogram of sons

55
00:02:26,500 --> 00:02:28,120
we&#39;re going to compare the child height

56
00:02:28,120 --> 00:02:30,280
which is the height of the Sun to the

57
00:02:30,280 --> 00:02:33,450
height of his mother

58
00:02:33,450 --> 00:02:36,200
so here we go

59
00:02:36,950 --> 00:02:39,350
and oh and i put

60
00:02:39,350 --> 00:02:43,100
line at the mean so you can see here&#39;s

61
00:02:43,100 --> 00:02:46,000
the heights of the mothers

62
00:02:46,000 --> 00:02:50,640
and the heights of the sons

63
00:02:50,690 --> 00:02:52,610
and there&#39;s quite a difference there&#39;s

64
00:02:52,610 --> 00:02:53,870
quite a bit of overlap in these

65
00:02:53,870 --> 00:02:57,860
distributions as you can see but the

66
00:02:57,860 --> 00:03:00,530
means are quite distinct and the

67
00:03:00,530 --> 00:03:03,890
question is if we look at the confidence

68
00:03:03,890 --> 00:03:06,320
intervals and perform a hypothesis test

69
00:03:06,320 --> 00:03:09,830
on this data are these two populations

70
00:03:09,830 --> 00:03:11,780
the height of the mothers and the

71
00:03:11,780 --> 00:03:14,450
heights of there&#39;s adult sons actually

72
00:03:14,450 --> 00:03:18,050
different at some significance level

73
00:03:18,050 --> 00:03:20,600
ok we&#39;re going to get a different case

74
00:03:20,600 --> 00:03:22,820
here which is we&#39;re going to call this

75
00:03:22,820 --> 00:03:25,610
daughter so it&#39;s the same idea except

76
00:03:25,610 --> 00:03:27,020
this time we&#39;re going to look at we&#39;re

77
00:03:27,020 --> 00:03:29,300
going to compare the daughters the

78
00:03:29,300 --> 00:03:30,740
height of the adult daughters to the

79
00:03:30,740 --> 00:03:33,830
height of their adult i&#39;m sorry the

80
00:03:33,830 --> 00:03:36,560
height of the adult daughters to the

81
00:03:36,560 --> 00:03:39,250
height of their mothers

82
00:03:39,250 --> 00:03:41,890
and as you can imagine those

83
00:03:41,890 --> 00:03:44,230
distributions look a lot more similar

84
00:03:44,230 --> 00:03:48,880
there&#39;s a lot more overlap in the

85
00:03:48,880 --> 00:03:50,890
daughters and the means are virtually

86
00:03:50,890 --> 00:03:54,520
the same but again is that

87
00:03:54,520 --> 00:03:57,040
small difference significant at some

88
00:03:57,040 --> 00:03:59,080
confidence interval or not your

89
00:03:59,080 --> 00:04:03,190
confidence level or not so to resolve

90
00:04:03,190 --> 00:04:05,710
that we&#39;re going to use the t-test we&#39;re

91
00:04:05,710 --> 00:04:09,130
going to use the t-test at the five

92
00:04:09,130 --> 00:04:14,380
percent or 0.5 confidence level and so

93
00:04:14,380 --> 00:04:16,270
we&#39;re going to and we&#39;re going to do a

94
00:04:16,269 --> 00:04:20,980
two-sided t-test here and we&#39;ll print

95
00:04:20,980 --> 00:04:23,470
out some other statistics like the

96
00:04:23,470 --> 00:04:25,150
degrees of freedom the difference of the

97
00:04:25,150 --> 00:04:28,390
means the t-statistic itself the p value

98
00:04:28,390 --> 00:04:31,150
and the confidence interval and they

99
00:04:31,150 --> 00:04:32,680
were going to plot those we&#39;re going to

100
00:04:32,680 --> 00:04:34,570
make histograms but we&#39;re going to show

101
00:04:34,570 --> 00:04:36,910
the confidence interval on those

102
00:04:36,910 --> 00:04:39,520
histograms and that&#39;s what all this code

103
00:04:39,520 --> 00:04:43,840
is about so first we&#39;ll do this between

104
00:04:43,840 --> 00:04:46,450
between the sun&#39;s the height of the

105
00:04:46,450 --> 00:04:48,430
adult sons and the height of their

106
00:04:48,430 --> 00:04:49,480
mothers

107
00:04:49,480 --> 00:04:53,340
so let me run that for you

108
00:04:56,639 --> 00:05:00,360
ok so first let&#39;s look at the statistics

109
00:05:00,360 --> 00:05:03,840
so we had you know there&#39;s a large

110
00:05:03,840 --> 00:05:05,849
number of degrees of freedom here over

111
00:05:05,849 --> 00:05:10,949
900 because we had about 400 + + 20 or

112
00:05:10,949 --> 00:05:14,200
440 pairs of

113
00:05:14,200 --> 00:05:17,009
mothers and sons

114
00:05:17,009 --> 00:05:19,900
the difference is about

115
00:05:19,900 --> 00:05:23,979
five inches you can see there&#39;s the

116
00:05:23,979 --> 00:05:26,560
difference in the means they&#39;re the t

117
00:05:26,560 --> 00:05:30,910
statistic is fairly large it&#39;s 39 and a

118
00:05:30,910 --> 00:05:32,270
half

119
00:05:32,270 --> 00:05:34,160
the p-value is quite small and

120
00:05:34,160 --> 00:05:35,900
effectively at zero if you look at

121
00:05:35,900 --> 00:05:37,430
something that&#39;s 10 to the minus a

122
00:05:37,430 --> 00:05:39,530
hundred and fifty-three but that&#39;s just

123
00:05:39,530 --> 00:05:40,730
a minute

124
00:05:40,730 --> 00:05:43,460
computational anomaly it&#39;s it&#39;s

125
00:05:43,460 --> 00:05:46,670
effectively a p a very low p value and

126
00:05:46,670 --> 00:05:48,320
we can see the upper and lower

127
00:05:48,320 --> 00:05:52,729
confidence interval around

128
00:05:52,729 --> 00:05:56,809
basically the height of the mother so

129
00:05:56,809 --> 00:05:58,129
we&#39;re comparing the mother to the child

130
00:05:58,129 --> 00:06:00,319
here so you need to sort of pay

131
00:06:00,319 --> 00:06:03,319
attention to which cut which the

132
00:06:03,319 --> 00:06:05,659
confidence interval around which of the

133
00:06:05,659 --> 00:06:08,029
means you&#39;re talking about and so

134
00:06:08,029 --> 00:06:09,740
graphically we can see there&#39;s quite a

135
00:06:09,740 --> 00:06:11,809
difference here here&#39;s our child height

136
00:06:11,809 --> 00:06:13,819
so this is the height of the adult sons

137
00:06:13,819 --> 00:06:16,789
cystogram here&#39;s the histogram of the

138
00:06:16,789 --> 00:06:19,490
height of the mothers and in those

139
00:06:19,490 --> 00:06:21,589
dotted lines around the mean that&#39;s the

140
00:06:21,589 --> 00:06:23,029
confidence and that&#39;s our ninety-five

141
00:06:23,029 --> 00:06:25,370
percent confidence interval so this mean

142
00:06:25,370 --> 00:06:28,069
is way outside that confidence interval

143
00:06:28,069 --> 00:06:30,770
is just no doubt about it so yes we can

144
00:06:30,770 --> 00:06:32,330
say based on all this

145
00:06:32,330 --> 00:06:34,669
that Suns are significantly different in

146
00:06:34,669 --> 00:06:39,229
height and their mothers but we can do

147
00:06:39,229 --> 00:06:42,050
the same thing with that mothers

148
00:06:42,050 --> 00:06:44,180
comparing the mothers to their daughters

149
00:06:44,180 --> 00:06:47,560
so let me run that for you

150
00:06:47,560 --> 00:06:49,750
and we get slightly fewer degrees of

151
00:06:49,750 --> 00:06:52,300
freedom um because the means are so

152
00:06:52,300 --> 00:06:55,980
close and in its

153
00:06:55,980 --> 00:07:00,420
the difference is also only point 044

154
00:07:00,420 --> 00:07:04,380
4.45 very small difference our

155
00:07:04,380 --> 00:07:06,660
t-statistic now is less than one its

156
00:07:06,660 --> 00:07:10,320
point 35 and our p-value is almost won

157
00:07:10,320 --> 00:07:13,020
its points73 so it&#39;s getting very close

158
00:07:13,020 --> 00:07:15,300
to one and our confidence interval

159
00:07:15,300 --> 00:07:19,290
overlap 0 so right there for that

160
00:07:19,290 --> 00:07:21,150
difference that should tell us something

161
00:07:21,150 --> 00:07:23,520
that&#39;s that&#39;s a bit odd

162
00:07:23,520 --> 00:07:27,870
so if we plot those histograms again we

163
00:07:27,870 --> 00:07:32,550
see the means for the adult daughters

164
00:07:32,550 --> 00:07:34,560
and the mothers and the confidence

165
00:07:34,560 --> 00:07:37,080
interval and clearly that mean is well

166
00:07:37,080 --> 00:07:38,340
within that ninety-five percent

167
00:07:38,340 --> 00:07:43,730
confidence interval so we need to reject

168
00:07:44,350 --> 00:07:45,880
sorry we need to accept that null

169
00:07:45,880 --> 00:07:49,100
hypothesis that

170
00:07:49,100 --> 00:07:52,680
from the

171
00:07:52,680 --> 00:07:53,880
others are the same as the heights of

172
00:07:53,880 --> 00:07:55,740
the mother&#39;s own dear i said something

173
00:07:55,740 --> 00:07:58,460
wrong here

174
00:08:00,240 --> 00:08:02,699
oh no that&#39;s right i guess we can cut

175
00:08:02,699 --> 00:08:05,970
that last bit out but that but that&#39;s ok

176
00:08:05,970 --> 00:08:09,350
so let me let me just do a wrap-up

177
00:08:09,350 --> 00:08:12,560
so I hope this demo gave you some idea

178
00:08:12,560 --> 00:08:16,310
of how in practice we use the concepts

179
00:08:16,310 --> 00:08:18,680
of confidence intervals and hypothesis

180
00:08:18,680 --> 00:08:20,420
test specifically in this case the t

181
00:08:20,420 --> 00:08:23,600
test to determine if

182
00:08:23,600 --> 00:08:28,280
two samples have significantly different

183
00:08:28,280 --> 00:08:30,820
means

