0
00:00:01,110 --> 00:00:03,540
Hi and welcome so Cynthia has been

1
00:00:03,540 --> 00:00:06,660
talking about two concepts we use quite

2
00:00:06,660 --> 00:00:09,240
often in statistics hypothesis testing

3
00:00:09,240 --> 00:00:11,339
and confidence intervals and in this

4
00:00:11,339 --> 00:00:13,589
demo I&#39;m going to try to pull those two

5
00:00:13,589 --> 00:00:15,419
concepts together to show you a

6
00:00:15,419 --> 00:00:18,570
practical example and the example we&#39;re

7
00:00:18,570 --> 00:00:21,240
going to look at is comparing the

8
00:00:21,240 --> 00:00:24,150
heights of adult children to the heights

9
00:00:24,150 --> 00:00:26,670
of their parents and the data set we&#39;re

10
00:00:26,670 --> 00:00:28,560
going to use is actually of historical

11
00:00:28,560 --> 00:00:31,410
significance in statistics Francis

12
00:00:31,410 --> 00:00:33,210
Galton who invented the regression

13
00:00:33,210 --> 00:00:35,040
method published his original paper and

14
00:00:35,040 --> 00:00:38,730
1886 and these data were what Frank

15
00:00:38,730 --> 00:00:42,120
gotten used to in that paper

16
00:00:42,120 --> 00:00:44,190
we&#39;re not going to look at the exact

17
00:00:44,190 --> 00:00:46,140
problem Dalton looked at we&#39;re going to

18
00:00:46,140 --> 00:00:47,700
look at a different one which is

19
00:00:47,700 --> 00:00:52,890
hypothesis testing on those data we can

20
00:00:52,890 --> 00:00:56,149
load the data set

21
00:00:56,940 --> 00:01:00,059
and then we&#39;re going to just look at the

22
00:01:00,059 --> 00:01:02,280
first few rows of that data set and just

23
00:01:02,280 --> 00:01:04,860
talk about what is in these columns so

24
00:01:04,860 --> 00:01:07,200
the first column is a case number that&#39;s

25
00:01:07,200 --> 00:01:08,970
just a sequential number that golden

26
00:01:08,970 --> 00:01:12,659
gave these data family data and finally

27
00:01:12,659 --> 00:01:15,450
number so gotten like these first four

28
00:01:15,450 --> 00:01:18,810
children all come from the same family

29
00:01:18,810 --> 00:01:20,130
family one and then there&#39;s some

30
00:01:20,130 --> 00:01:23,369
children from family to etc then we had

31
00:01:23,369 --> 00:01:25,380
the height of their father and inches

32
00:01:25,380 --> 00:01:28,500
the height of their mother and inches

33
00:01:28,500 --> 00:01:31,500
the average height of the parents the

34
00:01:31,500 --> 00:01:35,010
number of children in that family

35
00:01:35,010 --> 00:01:39,120
a number assigned to each child their

36
00:01:39,120 --> 00:01:41,310
gender whether they were male or female

37
00:01:41,310 --> 00:01:44,130
and the height of that adult child

38
00:01:44,130 --> 00:01:46,410
alright and let&#39;s just look at the

39
00:01:46,410 --> 00:01:49,080
dimensions of that data frame

40
00:01:49,080 --> 00:01:53,850
and you can see the golden had 934 cases

41
00:01:53,850 --> 00:01:57,030
where he went out in 19th century london

42
00:01:57,030 --> 00:01:58,830
and actually measured these people in

43
00:01:58,830 --> 00:02:00,620
these families

44
00:02:00,620 --> 00:02:02,990
so

45
00:02:02,990 --> 00:02:06,079
first off let&#39;s make a histogram and

46
00:02:06,079 --> 00:02:09,140
look at these data visually but we&#39;re

47
00:02:09,139 --> 00:02:10,700
going to look at a subset of these data

48
00:02:10,699 --> 00:02:13,099
so in this line of code here I&#39;m

49
00:02:13,099 --> 00:02:14,870
subsetting the data so we&#39;re only going

50
00:02:14,870 --> 00:02:18,319
to have the sun&#39;s basically the male

51
00:02:18,319 --> 00:02:20,340
children

52
00:02:20,340 --> 00:02:23,360
and then we&#39;re going to make a histogram

53
00:02:23,360 --> 00:02:26,030
of the height of those sons and

54
00:02:26,030 --> 00:02:29,770
histogram of the height of their mothers

55
00:02:29,770 --> 00:02:33,360
that&#39;s what all this code is doing here

56
00:02:35,440 --> 00:02:40,210
and there we have it so you can see the

57
00:02:40,210 --> 00:02:42,010
child heights that&#39;s the height of the

58
00:02:42,010 --> 00:02:45,130
adult sons and the mothers height

59
00:02:45,130 --> 00:02:48,100
obviously the height of the mother that

60
00:02:48,100 --> 00:02:51,650
son and here&#39;s the mean

61
00:02:51,650 --> 00:02:53,569
of the mother site and the mean of the

62
00:02:53,569 --> 00:02:54,829
sunlight they look quite different

63
00:02:54,829 --> 00:02:57,079
there&#39;s a fair amount of overlap between

64
00:02:57,079 --> 00:02:59,959
those two distributions but the question

65
00:02:59,959 --> 00:03:02,750
we need to look at is is that overlap

66
00:03:02,750 --> 00:03:05,420
actually significant if we consider the

67
00:03:05,420 --> 00:03:08,480
confidence intervals and perform a

68
00:03:08,480 --> 00:03:11,370
hypothesis test on that

69
00:03:11,370 --> 00:03:12,330
and we&#39;re going to look at one other

70
00:03:12,330 --> 00:03:14,159
case here so i&#39;m going to subset this

71
00:03:14,159 --> 00:03:18,210
date again so that we have

72
00:03:18,210 --> 00:03:20,820
the gender now the child is female so

73
00:03:20,820 --> 00:03:22,140
we&#39;re going to compare the height of

74
00:03:22,140 --> 00:03:25,140
daughters to the height of their mothers

75
00:03:25,140 --> 00:03:29,130
let me run that code

76
00:03:29,130 --> 00:03:30,610
and

77
00:03:30,610 --> 00:03:32,590
have it so here&#39;s the heights of the

78
00:03:32,590 --> 00:03:34,120
daughters Instagram of heights the

79
00:03:34,120 --> 00:03:35,320
daughters in the histogram of the

80
00:03:35,320 --> 00:03:38,590
heights of their mothers and those means

81
00:03:38,590 --> 00:03:40,180
are pretty close but again the question

82
00:03:40,180 --> 00:03:43,000
is is that small different still

83
00:03:43,000 --> 00:03:44,780
significant

84
00:03:44,780 --> 00:03:48,040
so

85
00:03:49,080 --> 00:03:51,750
down and a perform a t-test on this and

86
00:03:51,750 --> 00:03:53,520
we&#39;re going to look at the t-test we&#39;re

87
00:03:53,520 --> 00:03:54,690
going to look at the confidence interval

88
00:03:54,690 --> 00:04:03,530
around that mean one of those means and

89
00:04:03,690 --> 00:04:07,440
all this code does here so we&#39;re doing

90
00:04:07,440 --> 00:04:12,480
the two-sided t-test and let me just run

91
00:04:12,480 --> 00:04:14,690
it

92
00:04:18,829 --> 00:04:22,520
and there you have it so here&#39;s this

93
00:04:22,520 --> 00:04:23,990
first histogram is the heights of the

94
00:04:23,990 --> 00:04:27,020
mothers with the mean height of the

95
00:04:27,020 --> 00:04:30,199
mother and the mean height of the suns

96
00:04:30,199 --> 00:04:33,139
and this dotted line here is the

97
00:04:33,139 --> 00:04:36,050
confidence interval around the height of

98
00:04:36,050 --> 00:04:38,330
the mother and you can see that that

99
00:04:38,330 --> 00:04:40,819
difference between this height mean of

100
00:04:40,819 --> 00:04:43,009
this site and the mean of that height is

101
00:04:43,009 --> 00:04:44,780
quite different and it&#39;s well outside

102
00:04:44,780 --> 00:04:46,550
that ninety-five percent confidence

103
00:04:46,550 --> 00:04:49,280
interval we can also look at the output

104
00:04:49,280 --> 00:04:50,990
from this to set what&#39;s called the

105
00:04:50,990 --> 00:04:54,919
Walshes two-sample t-test and we get a

106
00:04:54,919 --> 00:04:57,379
t-statistic that&#39;s fairly large it&#39;s got

107
00:04:57,379 --> 00:04:58,759
a magnitude of about thirty

108
00:04:58,759 --> 00:05:01,639
two-and-a-half pretty large degrees of

109
00:05:01,639 --> 00:05:04,520
freedom that&#39;s essentially a little less

110
00:05:04,520 --> 00:05:07,550
than twice the number of mother-son

111
00:05:07,550 --> 00:05:11,090
pairs our p-value is pretty tiny 10 to

112
00:05:11,090 --> 00:05:13,669
the minus 16 is basically approaching 0

113
00:05:13,669 --> 00:05:16,099
here and here&#39;s the confidence interval

114
00:05:16,099 --> 00:05:18,949
and that gonna be a little careful it&#39;s

115
00:05:18,949 --> 00:05:21,500
the confidence interval around the first

116
00:05:21,500 --> 00:05:26,659
mean the mean of the mother so and

117
00:05:26,659 --> 00:05:28,460
that&#39;s how we got those Dottie lines

118
00:05:28,460 --> 00:05:30,220
there

119
00:05:30,220 --> 00:05:32,500
so one last thing to do here is we have

120
00:05:32,500 --> 00:05:35,440
to look at testing this other

121
00:05:35,440 --> 00:05:37,210
relationship which is between the mother

122
00:05:37,210 --> 00:05:39,730
and the adult daughters see if those

123
00:05:39,730 --> 00:05:42,540
Heights are significantly different

124
00:05:42,540 --> 00:05:45,129
alright

125
00:05:45,129 --> 00:05:48,669
so somewhat smaller degrees of freedom

126
00:05:48,669 --> 00:05:50,289
there&#39;s there&#39;s fewer and the sample is

127
00:05:50,289 --> 00:05:53,739
also the means are close together so now

128
00:05:53,739 --> 00:05:56,319
the p-value is quite large in this case

129
00:05:56,319 --> 00:05:58,719
points77 so it&#39;s getting pretty close to

130
00:05:58,719 --> 00:06:01,539
one RT statistic on the other hand is

131
00:06:01,539 --> 00:06:05,080
extremely small it&#39;s about point 3 so

132
00:06:05,080 --> 00:06:08,409
and here&#39;s our confidence interval and

133
00:06:08,409 --> 00:06:10,300
notice that overlap 0 which is

134
00:06:10,300 --> 00:06:12,210
interesting

135
00:06:12,210 --> 00:06:14,580
and when we plot so that&#39;s the

136
00:06:14,580 --> 00:06:16,500
confidence interval the difference and

137
00:06:16,500 --> 00:06:20,009
so here&#39;s that confidence interval

138
00:06:20,009 --> 00:06:24,360
plotted on the histogram upper and lower

139
00:06:24,360 --> 00:06:27,870
and that mean of the daughters height is

140
00:06:27,870 --> 00:06:29,729
pretty clearly within that ninety-five

141
00:06:29,729 --> 00:06:32,009
percent confidence interval so what&#39;s

142
00:06:32,009 --> 00:06:35,849
the conclusion here so we have to accept

143
00:06:35,849 --> 00:06:40,460
the null hypothesis in this case that

144
00:06:40,460 --> 00:06:43,280
mothers and their adult daughters have

145
00:06:43,280 --> 00:06:46,639
this the same height on average we can

146
00:06:46,639 --> 00:06:50,360
reject that null hypothesis in this

147
00:06:50,360 --> 00:06:54,050
first case where so we can reject the

148
00:06:54,050 --> 00:06:55,849
null hypothesis that the height of the

149
00:06:55,849 --> 00:06:58,460
Sun and the height of the mother are

150
00:06:58,460 --> 00:07:02,410
effectively no different

151
00:07:03,210 --> 00:07:07,259
so I hope that&#39;s helped you understand

152
00:07:07,259 --> 00:07:09,389
how we apply these concepts of the

153
00:07:09,389 --> 00:07:11,729
confidence interval and the hypothesis

154
00:07:11,729 --> 00:07:15,090
test to a data set where you&#39;re actually

155
00:07:15,090 --> 00:07:17,430
trying to compare whether two

156
00:07:17,430 --> 00:07:20,430
populations have some significant

157
00:07:20,430 --> 00:07:22,940
difference

