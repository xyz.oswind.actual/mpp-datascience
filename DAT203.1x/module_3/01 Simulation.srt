0
00:00:04,769 --> 00:00:06,330
Let&#39;s talk about simulation.

1
00:00:07,650 --> 00:00:11,800
So what happens if you have a random variable

2
00:00:11,800 --> 00:00:14,250
with a very complicated distribution?

3
00:00:14,250 --> 00:00:16,740
Simulation is actually for when you have this.

4
00:00:16,740 --> 00:00:18,180
When you have a distribution that&#39;s so

5
00:00:18,180 --> 00:00:20,540
complicated that you can&#39;t even write it down exactly.

6
00:00:20,540 --> 00:00:23,910
You can write down what the random process is

7
00:00:23,910 --> 00:00:27,490
that generates the data, but you can&#39;t write down its PDF.

8
00:00:28,790 --> 00:00:32,150
In that case, how would you compute probabilities

9
00:00:32,150 --> 00:00:34,750
like the probability that your random variable is below

10
00:00:34,750 --> 00:00:35,580
a specific value?

11
00:00:36,670 --> 00:00:39,250
Let me give you an example to illustrates simulation, so

12
00:00:39,250 --> 00:00:40,760
maybe a few of you already knew this, but

13
00:00:40,760 --> 00:00:43,630
I run a lemonade stand during the spring and summer months.

14
00:00:43,630 --> 00:00:46,730
It&#39;s a chain of lemonade stands that

15
00:00:46,730 --> 00:00:49,415
operates throughout Boston Common, and

16
00:00:49,415 --> 00:00:53,100
it goes a little bit into the fall throughout the summer.

17
00:00:53,100 --> 00:00:56,330
It&#39;s called Cynthia&#39;s lemonade, and we sell only lemonade, and

18
00:00:56,330 --> 00:00:57,080
it&#39;s really, really good.

19
00:00:58,400 --> 00:01:03,134
And here&#39;s a picture of one of my regular customers here.

20
00:01:03,134 --> 00:01:04,690
It&#39;s a nine year old boy.

21
00:01:04,690 --> 00:01:08,510
He visited pretty much every day in July and August last year.

22
00:01:08,510 --> 00:01:10,850
And you can see him here happily displaying his lemonade.

23
00:01:11,900 --> 00:01:17,160
And now, in general my profit is a random variable,

24
00:01:17,160 --> 00:01:21,240
and it depends on the number of customers I get that day,

25
00:01:22,920 --> 00:01:24,190
the weather, and

26
00:01:24,190 --> 00:01:27,510
then whatever tips the customer decides to leave for us.

27
00:01:27,510 --> 00:01:31,519
And I&#39;m gonna tell you exactly how the profit depends

28
00:01:31,519 --> 00:01:33,665
on those things in a minute.

29
00:01:33,665 --> 00:01:36,180
Okay, so here&#39;s the question I wanna ask.

30
00:01:36,180 --> 00:01:39,850
Which is what is the probability that my profit on a particular

31
00:01:39,850 --> 00:01:45,040
day is gonna fall below a value x, on a day sometime in July or

32
00:01:45,040 --> 00:01:45,800
August next year?

33
00:01:47,050 --> 00:01:49,100
Now in order to answer this question,

34
00:01:49,100 --> 00:01:52,070
I&#39;m going to tell you exactly how my profit is calculated.

35
00:01:54,350 --> 00:01:57,300
It&#39;s actually the number of customers

36
00:01:59,190 --> 00:02:03,990
times what they pay us, right, the profit that I make per cup

37
00:02:03,990 --> 00:02:05,930
plus the tip that the customer gives us.

38
00:02:07,880 --> 00:02:10,438
So here&#39;s how I&#39;m gonna model the profit.

39
00:02:10,437 --> 00:02:14,973
I will say that the number of customers comes from a normal

40
00:02:14,973 --> 00:02:19,698
distribution, with mean, 600 customers per day, and

41
00:02:19,698 --> 00:02:22,079
standard deviation, 30.

42
00:02:22,079 --> 00:02:23,330
Why did I choose normal?

43
00:02:24,360 --> 00:02:27,370
Because customers are independent, and

44
00:02:27,370 --> 00:02:31,498
when you add up a bunch of independent random variables,

45
00:02:31,498 --> 00:02:36,228
the central limit theorem kicks in and everything looks normal.

46
00:02:36,228 --> 00:02:39,343
Okay, now for the profit per cup,

47
00:02:39,343 --> 00:02:42,040
that depends on the weather.

48
00:02:43,120 --> 00:02:47,800
Because if the weather is hot, if the weather is hot,

49
00:02:47,800 --> 00:02:53,430
then I can charge 5 whole bucks for a lemonade.

50
00:02:53,430 --> 00:02:54,640
That&#39;s Boston Common for you.

51
00:02:55,820 --> 00:03:00,678
But if the weather is bad, I can only justify charging $3.50.

52
00:03:00,678 --> 00:03:03,405
Okay, and so these are the probabilities that I get,

53
00:03:03,405 --> 00:03:05,900
good weather, medium weather and bad weather.

54
00:03:09,000 --> 00:03:13,610
Now for the tips, the customers give us whatever they want.

55
00:03:13,610 --> 00:03:15,610
But usually it&#39;s the easiest for

56
00:03:15,610 --> 00:03:17,720
them to give us whatever change they have.

57
00:03:17,720 --> 00:03:18,840
So they might give us nothing.

58
00:03:19,860 --> 00:03:21,560
And that happens half the time.

59
00:03:21,560 --> 00:03:23,240
They give us nothing.

60
00:03:23,240 --> 00:03:27,270
Or they give us a quarter 20% of the time.

61
00:03:27,270 --> 00:03:30,610
Or a buck or 2 bucks, okay.

62
00:03:30,610 --> 00:03:33,170
And half the time they don&#39;t give us any tip, which is okay,

63
00:03:33,170 --> 00:03:35,249
considering how much we&#39;re charging for the lemonade.

64
00:03:37,130 --> 00:03:39,068
So here&#39;s how I&#39;m gonna simulate that on my computer.

65
00:03:39,068 --> 00:03:43,476
For each day, I&#39;m gonna simulate the number of customers from

66
00:03:43,476 --> 00:03:45,223
a normal distribution,

67
00:03:45,223 --> 00:03:48,817
centered at 600 with standard deviation 30.

68
00:03:48,817 --> 00:03:53,126
And then I&#39;ll simulate the profit per cup as a discrete

69
00:03:53,126 --> 00:03:57,255
random variable depending on the weather that day.

70
00:03:57,255 --> 00:04:00,577
And I showed you the random variable&#39;s distribution

71
00:04:00,577 --> 00:04:02,070
on the previous slide.

72
00:04:02,070 --> 00:04:03,338
And then for each customer,

73
00:04:03,338 --> 00:04:05,750
I&#39;ll simulate the tip they&#39;re gonna give me.

74
00:04:07,050 --> 00:04:10,034
Which again I showed you the distribution on the previous

75
00:04:10,034 --> 00:04:11,350
slide, and that&#39;s it.

76
00:04:12,490 --> 00:04:15,170
In using this code, I can simulate as many days as I like.

77
00:04:16,329 --> 00:04:20,010
And then I can use the simulation results to actually

78
00:04:20,010 --> 00:04:24,940
create a histogram to show what the distribution of my profits

79
00:04:24,940 --> 00:04:26,600
are gonna look like.

80
00:04:26,600 --> 00:04:27,450
Now let me show that to you.

81
00:04:29,410 --> 00:04:30,080
And here it is.

82
00:04:31,940 --> 00:04:35,640
Okay, but why does it look like this?

83
00:04:35,640 --> 00:04:37,190
Where would all these bumps have come from?

84
00:04:38,190 --> 00:04:39,970
Why would my profits look like this?

85
00:04:41,500 --> 00:04:42,780
Let me dissect it a bit more.

86
00:04:43,940 --> 00:04:45,948
But take a look at the simulation one more time.

87
00:04:48,218 --> 00:04:52,590
We have customers times profit per cup plus tip.

88
00:04:54,000 --> 00:04:56,750
So what would happen if we removed the tips?

89
00:04:56,750 --> 00:04:57,990
I should think that the tips kinda

90
00:04:57,990 --> 00:05:01,220
complicate the distribution, so let&#39;s just remove them for now.

91
00:05:01,220 --> 00:05:03,060
Okay, so there it is.

92
00:05:03,060 --> 00:05:07,065
So I&#39;m gonna redo the simulation but

93
00:05:07,065 --> 00:05:10,690
without the tips, so here goes.

94
00:05:10,690 --> 00:05:12,840
So now there&#39;s three bumps.

95
00:05:12,840 --> 00:05:13,750
But why three bumps?

96
00:05:14,940 --> 00:05:15,600
I&#39;ll give you a hint.

97
00:05:16,670 --> 00:05:21,803
If the lemonade is 5 bucks and 600 people purchase it,

98
00:05:21,803 --> 00:05:25,066
that&#39;s exactly 3,000 bucks.

99
00:05:26,648 --> 00:05:27,326
Okay?

100
00:05:29,772 --> 00:05:34,945
So now, if we charge 4 bucks, then we&#39;re here.

101
00:05:34,945 --> 00:05:37,440
And for 3.50, we&#39;re here.

102
00:05:37,440 --> 00:05:38,570
So these three bumps are for

103
00:05:38,570 --> 00:05:41,060
three different prices of lemonade that are possible.

104
00:05:42,790 --> 00:05:44,912
Now in the next slide I&#39;m just gonna model the tips.

105
00:05:47,594 --> 00:05:49,227
And the tips are here.

106
00:05:49,227 --> 00:05:50,080
Look at them. See,

107
00:05:50,080 --> 00:05:52,104
half the time they&#39;re zero, but

108
00:05:52,104 --> 00:05:55,879
when they aren&#39;t zero they just add bumps at various places.

109
00:05:57,170 --> 00:06:00,529
And it&#39;ll create sort of ripple bumps throughout our histogram.

110
00:06:00,529 --> 00:06:02,866
And there won&#39;t just be four bumps, right?

111
00:06:02,866 --> 00:06:05,180
There&#39;ll be four bumps for each of the three prices of lemonade,

112
00:06:05,180 --> 00:06:07,110
so there&#39;ll be 12 bumps.

113
00:06:07,110 --> 00:06:08,940
And some of the bumps are gonna be kind of small and

114
00:06:08,940 --> 00:06:10,660
will get schmeared out.

115
00:06:10,660 --> 00:06:11,950
But let&#39;s see it all together again.

116
00:06:14,140 --> 00:06:15,194
And there it is.

117
00:06:17,745 --> 00:06:21,110
Okay, so now you know where it comes from.

118
00:06:21,110 --> 00:06:24,562
The two bumps here are kinda smooshed because of the tips and

119
00:06:24,562 --> 00:06:28,370
then this small bump out here, that&#39;s also because of the tips.

120
00:06:29,790 --> 00:06:32,990
And now because I have this great simulation of

121
00:06:32,990 --> 00:06:35,120
the probability density function,

122
00:06:35,120 --> 00:06:37,970
I can estimate probabilities really easily.

123
00:06:37,970 --> 00:06:42,679
Like if I wanna know how often I&#39;ll get less than 3,000 profit,

124
00:06:42,679 --> 00:06:47,389
that&#39;s just a fraction of times I&#39;m below 3000, so I just count

125
00:06:47,389 --> 00:06:51,876
the percentage of days when I&#39;ll get less than 3000 profit.

126
00:06:51,876 --> 00:06:54,747
And if I wanna know the probability to get between

127
00:06:54,747 --> 00:06:59,087
2,500 and 3,500 profit, I just count the percent of days when I

128
00:06:59,087 --> 00:07:01,986
get between those two values in the simulation.

129
00:07:01,986 --> 00:07:08,100
Now, most software can simulate draws

130
00:07:08,100 --> 00:07:10,470
from the normal distribution, Bernoulli distribution,

131
00:07:10,470 --> 00:07:13,650
binomial, uniform and poisson distribution.

132
00:07:13,650 --> 00:07:15,400
It&#39;s actually really simple.

133
00:07:15,400 --> 00:07:18,883
You just say, I would like to use you to draw me

134
00:07:18,883 --> 00:07:22,109
a random number with this distribution.

135
00:07:22,109 --> 00:07:24,912
But what about a discrete distribution?

136
00:07:24,912 --> 00:07:27,727
How do you do that?

137
00:07:27,727 --> 00:07:30,050
Let me show you a nice very simple way to do that.

138
00:07:32,910 --> 00:07:34,150
So what you do is this.

139
00:07:34,150 --> 00:07:36,724
And I&#39;m gonna do an example to demonstrate it.

140
00:07:36,724 --> 00:07:39,994
But let&#39;s say that I want a sample from the distribution of

141
00:07:39,994 --> 00:07:42,063
tips, which is this distribution,

142
00:07:42,063 --> 00:07:44,080
with these probabilities up here.

143
00:07:46,090 --> 00:07:50,100
So what I&#39;ll do is, I&#39;ll draw a single uniform random number

144
00:07:50,100 --> 00:07:51,150
between zero and one.

145
00:07:52,890 --> 00:07:55,660
And then I&#39;ll divide up the interval

146
00:07:55,660 --> 00:07:57,630
according to these probabilities.

147
00:07:58,910 --> 00:08:03,464
Now if my random number is less than 0.5, that happens,

148
00:08:03,464 --> 00:08:08,485
right, this is half the time, obviously, then I assign No tip.

149
00:08:08,485 --> 00:08:11,564
Okay, so if my random number that I choose is below 0.5,

150
00:08:11,564 --> 00:08:12,370
assign No tip.

151
00:08:13,450 --> 00:08:16,579
If the value is between 0.5 and

152
00:08:16,579 --> 00:08:21,497
0.7, which happens with a 20% probability,

153
00:08:21,497 --> 00:08:25,871
I assign a tip of $0.25, and that&#39;s it.

154
00:08:25,871 --> 00:08:28,591
That&#39;s how you do it.

155
00:08:28,591 --> 00:08:31,686
The simulation is useful for answering questions like, what

156
00:08:31,686 --> 00:08:34,601
happens when you have a random variable with a complicated

157
00:08:34,601 --> 00:08:38,470
distribution, and how do you compute probabilities from it?

158
00:08:38,470 --> 00:08:41,000
These are for distributions that are so complicated that you

159
00:08:41,000 --> 00:08:43,510
can&#39;t actually write down the PDF, but you kind of

160
00:08:43,510 --> 00:08:46,975
understand the random process by which the data are generated.

161
00:08:46,975 --> 00:08:50,599
And then, in that case, you just simulate from the distribution,

162
00:08:50,599 --> 00:08:53,206
and then you can at least see what it looks like and

163
00:08:53,206 --> 00:08:55,450
you can estimate probabilities from it.

