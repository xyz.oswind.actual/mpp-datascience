0
00:00:04,651 --> 00:00:07,660
So let&#39;s talk about type I and type II errors.

1
00:00:07,660 --> 00:00:12,990
Now a type I error is made when a test rejects

2
00:00:12,990 --> 00:00:16,060
the null hypothesis in favor of the alternative hypothesis,

3
00:00:17,150 --> 00:00:20,100
when actually the null hypothesis was true.

4
00:00:20,100 --> 00:00:21,880
This is also called a false positive.

5
00:00:23,350 --> 00:00:27,980
So for instance, maybe the person doesn&#39;t have the disease

6
00:00:27,980 --> 00:00:31,811
but the test for the disease comes back positive.

7
00:00:31,811 --> 00:00:34,370
So that&#39;s a type I error.

8
00:00:34,370 --> 00:00:37,920
And a type II error is made when the test fails to reject

9
00:00:37,920 --> 00:00:41,500
the null hypothesis when the alternative was actually true.

10
00:00:41,500 --> 00:00:43,861
And this is also called a false negative.

11
00:00:43,861 --> 00:00:46,467
And this is the case, for instance where the person

12
00:00:46,467 --> 00:00:50,490
actually has the disease, but the test comes back negative.

13
00:00:50,490 --> 00:00:53,170
And we can write this in a little table like this.

14
00:00:53,170 --> 00:00:55,060
Though if the null hypothesis is true,

15
00:00:55,060 --> 00:00:58,100
then we don&#39;t reject it, well, we were correct.

16
00:00:59,230 --> 00:01:02,009
And if the null hypothesis is false, and

17
00:01:02,009 --> 00:01:05,131
we reject it, well that&#39;s correct as well.

18
00:01:05,131 --> 00:01:07,861
But if the null hypothesis is true and we reject it,

19
00:01:07,861 --> 00:01:09,900
that&#39;s a type I error.

20
00:01:09,900 --> 00:01:11,070
And then the type II error&#39;s over

21
00:01:11,070 --> 00:01:13,770
here where the null hypothesis is false and we didn&#39;t reject.

22
00:01:14,850 --> 00:01:16,420
Now the probability of a type I error,

23
00:01:16,420 --> 00:01:19,740
that&#39;s actually called the alpha-risk.

24
00:01:19,740 --> 00:01:23,370
That&#39;s the probability you make this kind of error.

25
00:01:24,730 --> 00:01:28,510
And then the probability that you make a type II error

26
00:01:28,510 --> 00:01:30,980
is called the beta-risk.

27
00:01:30,980 --> 00:01:34,904
And I can use the definitions of type I and

28
00:01:34,904 --> 00:01:40,461
type II errors to write down the alpha and the beta risks.

29
00:01:40,461 --> 00:01:44,341
So the alpha risk is the probability of a type I error.

30
00:01:44,341 --> 00:01:48,174
And a type I error is when you reject the null hypothesis when

31
00:01:48,174 --> 00:01:49,640
it&#39;s actually true.

32
00:01:51,370 --> 00:01:56,332
And then the beta risk is the probability of a type II error,

33
00:01:56,332 --> 00:02:00,998
which is the probability that you fail to reject the null

34
00:02:00,998 --> 00:02:04,490
hypothesis when it is actually not true.

