0
00:00:04,673 --> 00:00:07,309
I think, the best way to learn hypothesis testing is to just do

1
00:00:07,309 --> 00:00:08,230
a bunch of examples.

2
00:00:08,230 --> 00:00:09,920
So, I&#39;ve prepared a bunch of examples for you.

3
00:00:09,920 --> 00:00:12,640
I&#39;m gonna give you an example, and

4
00:00:12,640 --> 00:00:17,330
then we&#39;re gonna figure out what the best test is to use, ready?

5
00:00:17,330 --> 00:00:21,030
I claim the amount of toothpaste in a tube is normal,

6
00:00:21,030 --> 00:00:25,460
with a mean of 10 units, and you say the mean is not 10.

7
00:00:25,460 --> 00:00:28,440
And so here are the measurements from each tube of toothpaste.

8
00:00:29,520 --> 00:00:33,340
And there are 36 points here, what test do I use?

9
00:00:34,372 --> 00:00:37,780
I bought a bunch of containers of toothpaste, namely 36 of

10
00:00:37,780 --> 00:00:41,340
them, and I measure how many units of toothpaste there were.

11
00:00:41,340 --> 00:00:44,830
Now I&#39;m gonna use a single sample t-test or

12
00:00:44,830 --> 00:00:47,660
a z-test, because again they are the same,

13
00:00:47,660 --> 00:00:50,780
because there are more than 30 points here.

14
00:00:51,850 --> 00:00:54,580
And I&#39;m gonna use a 2-sided test, because

15
00:00:54,580 --> 00:00:57,890
the alternative hypothesis, the thing that I wanna prove,

16
00:00:57,890 --> 00:01:01,110
is that the mean is not equal to 10.

17
00:01:01,110 --> 00:01:04,670
And then as it turns out we can&#39;t reject the null hypothesis

18
00:01:04,670 --> 00:01:06,810
for this particular case.

19
00:01:06,810 --> 00:01:11,370
Okay, so what if I do the same test,

20
00:01:11,370 --> 00:01:14,270
except that I only have few measurements.

21
00:01:14,270 --> 00:01:16,410
I can assume normality, that&#39;s okay,

22
00:01:16,410 --> 00:01:18,668
because it&#39;s the amount of toothpaste.

23
00:01:18,668 --> 00:01:23,790
So the machine&#39;s operate in a way that

24
00:01:23,790 --> 00:01:26,050
you can assume normal errors, so that&#39;s fine.

25
00:01:27,580 --> 00:01:30,830
Well, I&#39;m gonna use a single sample t-test.

26
00:01:30,830 --> 00:01:34,145
I have to use a t-test here because I have less than

27
00:01:34,145 --> 00:01:38,600
30 measurements, and I don&#39;t know the standard deviation.

28
00:01:39,770 --> 00:01:42,570
And again this will be a two sided test.

29
00:01:42,570 --> 00:01:47,550
Because you&#39;re claiming the mean is not 10 units and

30
00:01:47,550 --> 00:01:49,920
that&#39;s what the alternative hypothesis is.

31
00:01:49,920 --> 00:01:50,430
And so again,

32
00:01:50,430 --> 00:01:53,550
it turns out I can&#39;t reject the null hypothesis.

33
00:01:53,550 --> 00:01:54,680
Okay, here&#39;s another example.

34
00:01:56,530 --> 00:01:59,440
The Worldwide Importers Trucking Company claims they generally

35
00:01:59,440 --> 00:02:01,790
have more trucks operating daily than

36
00:02:01,790 --> 00:02:03,670
the Northwind Traders Trucking Company.

37
00:02:05,550 --> 00:02:08,610
So here is Worldwide&#39;s daily number of trucks,

38
00:02:08,610 --> 00:02:13,980
give you maybe a month of trucks.

39
00:02:15,930 --> 00:02:18,150
And then here&#39;s Northwind&#39;s daily number of trucks.

40
00:02:20,240 --> 00:02:24,700
Now clearly these samples are independent,

41
00:02:24,700 --> 00:02:27,290
since we have more measurements from one company than the other.

42
00:02:29,620 --> 00:02:34,010
So, we decided to use a t-test for 2 independent samples.

43
00:02:34,010 --> 00:02:36,560
I decided to not assume the variances were equal,

44
00:02:36,560 --> 00:02:39,960
and used the test for unequal variances.

45
00:02:39,960 --> 00:02:45,280
And then I&#39;m gonna do a right tailed test because I want to

46
00:02:48,590 --> 00:02:53,430
prove that Worldwide has more trucks than Northwind.

47
00:02:53,430 --> 00:02:57,229
Here also the test turned out that we couldn&#39;t reject the null

48
00:02:57,229 --> 00:02:59,433
hypothesis, just a coincidence.

49
00:02:59,433 --> 00:03:00,981
More practice,

50
00:03:00,981 --> 00:03:06,404
I claim that more than half of all women in Louisiana vote.

51
00:03:06,404 --> 00:03:10,665
So I did look at the data from a survey of women voters,

52
00:03:10,665 --> 00:03:13,808
I had 40 women and 30 of them voted.

53
00:03:13,808 --> 00:03:17,131
So what I can do is I can use the binomial distribution to

54
00:03:17,131 --> 00:03:18,736
calculate the p-value.

55
00:03:18,736 --> 00:03:22,760
The p-value again is the probability that out of n women,

56
00:03:22,760 --> 00:03:27,000
there&#39;s 40, that 30 or more of them voted.

57
00:03:27,000 --> 00:03:29,450
And you already know how to calculate those probabilities,

58
00:03:29,450 --> 00:03:32,860
because we went over the binomial distribution.

59
00:03:35,340 --> 00:03:39,138
And again, if the p-value is below the level of significance

60
00:03:39,138 --> 00:03:43,447
alpha, which is 0.05 here, then I can reject the null hypothesis

61
00:03:43,447 --> 00:03:45,315
in favor of the alternative.

62
00:03:45,315 --> 00:03:49,433
ThePhone-Company.com claims that customers purchase more during

63
00:03:49,433 --> 00:03:50,762
the day than at night.

64
00:03:50,762 --> 00:03:55,385
So they look at a whole bunch of customers, like Dan and

65
00:03:55,385 --> 00:03:58,006
Joanne and Consuelo and so on.

66
00:03:58,006 --> 00:04:00,043
And we look at how many purchases they made during

67
00:04:00,043 --> 00:04:02,040
the day and how many at night.

68
00:04:02,040 --> 00:04:05,982
So, how might we go about proving their claim?

69
00:04:05,982 --> 00:04:09,298
Well, clearly these are matched pairs.

70
00:04:09,298 --> 00:04:11,168
Because the daytime measurement and

71
00:04:11,168 --> 00:04:14,305
the nighttime measurement are paired naturally because they

72
00:04:14,305 --> 00:04:16,020
both belong to the same person.

73
00:04:16,019 --> 00:04:19,710
So, we&#39;re gonna use a t-test for paired samples.

74
00:04:19,709 --> 00:04:22,663
And we&#39;ll make it right tailed because we&#39;re trying to prove

75
00:04:22,663 --> 00:04:25,521
that customers purchase more during the day than at night.

76
00:04:25,521 --> 00:04:28,546
Now, I&#39;m a little bit concerned about using the t-test here.

77
00:04:28,546 --> 00:04:31,066
Because I&#39;m not sure that I agree with the normality

78
00:04:31,066 --> 00:04:33,986
assumption, because I think that some people purchase more

79
00:04:33,986 --> 00:04:34,970
than other people.

80
00:04:34,970 --> 00:04:38,020
And there&#39;s not necessarily a central limit theorem to kick in

81
00:04:38,020 --> 00:04:39,310
here because we&#39;re not adding them up,

82
00:04:39,310 --> 00:04:41,770
we&#39;re considering them each separately.

83
00:04:41,770 --> 00:04:43,900
And there can be groups of customers that behave very

84
00:04:43,900 --> 00:04:44,430
differently.

85
00:04:45,780 --> 00:04:49,260
So in my own self I might prefer a sign test,

86
00:04:49,260 --> 00:04:53,808
which is a different test that assumes very little about

87
00:04:53,808 --> 00:04:55,565
the distributions.

88
00:04:55,565 --> 00:04:58,151
And if I did that, maybe I wouldn&#39;t get as much power as I

89
00:04:58,151 --> 00:04:59,371
would out of the t-test.

90
00:04:59,371 --> 00:05:02,926
But at least I wouldn&#39;t have made an untrue assumption,

91
00:05:02,926 --> 00:05:05,602
which might lead to an untrue conclusion.

92
00:05:05,602 --> 00:05:09,236
So I hope you kind of get the idea that each of these tests

93
00:05:09,236 --> 00:05:12,791
has some assumptions that come with it that you have to

94
00:05:12,791 --> 00:05:16,030
be okay with in order to believe the conclusion.

95
00:05:16,030 --> 00:05:18,290
Okay, so

96
00:05:18,290 --> 00:05:22,370
First Up consultants, they claim that their pirate marketing

97
00:05:22,370 --> 00:05:26,540
campaign works better then their diamond marketing campaign.

98
00:05:28,920 --> 00:05:31,840
So they keep track of their sales on days with the pirate

99
00:05:31,840 --> 00:05:34,620
campaign and sales on days with the diamond campaign.

100
00:05:34,620 --> 00:05:37,060
You&#39;ve got all of these numbers here.

101
00:05:37,060 --> 00:05:40,440
Now are these independent samples or

102
00:05:40,440 --> 00:05:41,849
are they paired samples?

103
00:05:43,610 --> 00:05:45,301
And the answer is they&#39;re independent.

104
00:05:45,301 --> 00:05:50,394
Because the day where they showed the pirate

105
00:05:50,394 --> 00:05:55,218
campaign over here and got sales of 30.

106
00:05:55,218 --> 00:06:00,117
That was a totally different day than the day they got

107
00:06:00,117 --> 00:06:03,242
12 with the diamond campaign.

108
00:06:03,242 --> 00:06:06,045
So we&#39;re gonna use the t-test for independent samples.

109
00:06:06,045 --> 00:06:08,205
And again it&#39;s going to the right tailed,

110
00:06:08,205 --> 00:06:11,265
cuz I&#39;m trying to show that the pirate campaign works better

111
00:06:11,265 --> 00:06:12,716
than the diamond campaign.

