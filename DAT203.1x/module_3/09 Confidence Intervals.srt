0
00:00:05,209 --> 00:00:07,111
So let&#39;s talk about confidence intervals.

1
00:00:07,111 --> 00:00:10,716
Let&#39;s say we&#39;re trying to figure out what is the average amount

2
00:00:10,716 --> 00:00:12,730
of coffee in a mug?

3
00:00:12,730 --> 00:00:15,910
Now the coffee machine can&#39;t pour exactly the same amount of

4
00:00:15,910 --> 00:00:16,760
coffee each time.

5
00:00:16,760 --> 00:00:20,120
So there&#39;s some distribution of how much coffee goes into a mug.

6
00:00:21,290 --> 00:00:22,430
And I think it&#39;s fair to

7
00:00:22,430 --> 00:00:25,550
guess that the distribution is approximately normal.

8
00:00:25,550 --> 00:00:27,140
Maybe that&#39;s not fair, I&#39;m not sure,

9
00:00:27,140 --> 00:00:30,400
but it seems that a cup of coffee is a sum of lots of

10
00:00:30,400 --> 00:00:32,780
independent little drops of coffee.

11
00:00:32,780 --> 00:00:34,650
So the central limit theorem might kick in and

12
00:00:34,650 --> 00:00:38,510
say that the total amount of coffee is approximately normal.

13
00:00:38,510 --> 00:00:39,520
So if you buy that,

14
00:00:39,520 --> 00:00:42,950
then the amount of coffee in a mug is normally distributed.

15
00:00:44,340 --> 00:00:49,150
So, Steve Elston helps me out by drinking 100 cups of coffee and

16
00:00:49,150 --> 00:00:53,000
I wrote down exactly how much coffee was in each cup.

17
00:00:54,180 --> 00:00:57,356
And now, if I take the average of these numbers,

18
00:00:57,356 --> 00:01:00,155
how close is it to the population average?

19
00:01:02,993 --> 00:01:06,030
So here&#39;s the average of the 100 cups.

20
00:01:06,030 --> 00:01:09,280
It&#39;s called x bar because it&#39;s the average of the sample.

21
00:01:10,720 --> 00:01:14,130
And now, I&#39;m not allowed to know the true distribution, right?

22
00:01:14,130 --> 00:01:15,590
I don&#39;t know that.

23
00:01:15,590 --> 00:01:17,670
I never can see that.

24
00:01:17,670 --> 00:01:18,940
But I know that it&#39;s normal.

25
00:01:21,380 --> 00:01:22,845
So it could be here,

26
00:01:22,845 --> 00:01:26,482
this could be true distribution with its mean mu.

27
00:01:28,685 --> 00:01:33,479
Or it could be here, or here, or here and

28
00:01:33,479 --> 00:01:38,352
I&#39;ll never know which one it is.

29
00:01:38,352 --> 00:01:42,099
What I&#39;d really like to know is how far away x bar is from

30
00:01:42,099 --> 00:01:43,892
true mu, whatever it is,

31
00:01:43,892 --> 00:01:47,572
that we&#39;re hoping to find out the x bar is close to mu.

32
00:01:51,100 --> 00:01:53,859
And what I&#39;d would really like to do is be able to draw

33
00:01:53,859 --> 00:01:56,110
an interval around x bar.

34
00:01:56,110 --> 00:02:01,021
And be able to say with some degree of certainty that mu is

35
00:02:01,021 --> 00:02:06,038
close to x bar, that mu is in here with high probability,

36
00:02:06,038 --> 00:02:08,771
with probability 1- alpha.

37
00:02:12,002 --> 00:02:14,624
Now, I can tell you exactly what that interval is, but

38
00:02:14,624 --> 00:02:17,080
it has a bunch of symbols in it that you don&#39;t know.

39
00:02:18,080 --> 00:02:19,830
Let me explain all these symbols, and

40
00:02:19,830 --> 00:02:21,912
then I&#39;m going to get back to this picture.

41
00:02:21,912 --> 00:02:25,071
It&#39;s gonna be the same picture, except that next time I show you

42
00:02:25,071 --> 00:02:28,011
this, you&#39;ll understand what all of these symbols mean.

43
00:02:31,173 --> 00:02:34,562
Okay, so let&#39;s start with the standard normal,

44
00:02:34,562 --> 00:02:37,260
the mean 0 and standard deviation 1.

45
00:02:37,260 --> 00:02:38,918
And I&#39;m gonna ask you some questions about this

46
00:02:38,918 --> 00:02:39,505
distribution.

47
00:02:42,515 --> 00:02:46,569
I want to know, if I draw a point from the distribution,

48
00:02:46,569 --> 00:02:50,192
what is the probability that it will be more than 1

49
00:02:50,192 --> 00:02:53,750
standard deviation above the mean?

50
00:02:53,750 --> 00:02:55,840
Now, remember, probabilities are areas here.

51
00:02:57,660 --> 00:02:59,030
Okay, so what do you think the answer is?

52
00:02:59,030 --> 00:03:01,859
What do you think the probability is to get a value

53
00:03:01,859 --> 00:03:04,069
that&#39;s in here when I draw randomly?

54
00:03:06,701 --> 00:03:13,378
And the answer turns out to be about 15.87%, okay?

55
00:03:13,378 --> 00:03:17,748
There&#39;s a 15.87% chance that you&#39;d draw a value that&#39;s more

56
00:03:17,748 --> 00:03:20,550
than 1 standard deviation above the mean.

57
00:03:21,890 --> 00:03:26,334
Unfortunately there is no formula that people can write

58
00:03:26,334 --> 00:03:30,407
down to get you from this 1 to this 15%, right?

59
00:03:30,407 --> 00:03:32,716
The problem is that there&#39;s no analytical form for

60
00:03:32,716 --> 00:03:35,500
the area under the normal distribution.

61
00:03:35,500 --> 00:03:39,150
Now, remember, since probabilities are integrals, and

62
00:03:39,150 --> 00:03:42,970
integrals are sometimes not able to be computed analytically.

63
00:03:42,970 --> 00:03:47,242
And this is a case where we can&#39;t compute the integral using

64
00:03:47,242 --> 00:03:48,100
a formula.

65
00:03:48,100 --> 00:03:51,230
And that&#39;s okay, cuz we have a look up table on the computer.

66
00:03:51,230 --> 00:03:54,320
Any software you use will have a command that gets

67
00:03:54,320 --> 00:03:55,759
you between these two numbers.

68
00:03:57,000 --> 00:04:02,363
So, if you know that this probability is 15%,

69
00:04:02,363 --> 00:04:06,610
then the computer can tell you that this corresponds to 1

70
00:04:06,610 --> 00:04:08,100
standard deviation above the mean.

71
00:04:10,630 --> 00:04:12,600
And it works for any alpha and

72
00:04:12,600 --> 00:04:15,580
Z alpha and this is the usual notation.

73
00:04:15,580 --> 00:04:18,740
Alpha&#39;s the area, that is probability.

74
00:04:20,089 --> 00:04:22,100
And Z offers the corresponding Z score.

75
00:04:23,100 --> 00:04:27,330
Z alpha is the number of standard deviations above or

76
00:04:27,330 --> 00:04:32,500
below the mean, so that this probability is alpha.

77
00:04:36,580 --> 00:04:39,024
So if you give me Z alpha, whatever the heck it is.

78
00:04:41,141 --> 00:04:44,137
Then there&#39;s only one possible alpha corresponding to it, and

79
00:04:44,137 --> 00:04:46,143
that&#39;s in the computer&#39;s look up table.

80
00:04:48,782 --> 00:04:51,706
There&#39;s a one to one correspondence which means if

81
00:04:51,706 --> 00:04:54,630
you know alpha, you know Z alpha, and vice versa.

82
00:04:55,640 --> 00:04:58,330
For any alpha you give me, I can give you Z alpha.

83
00:04:58,330 --> 00:05:00,484
For any Z alpha you give me, I can give you alpha.

84
00:05:03,811 --> 00:05:06,270
Okay, so I hope you get the point.

85
00:05:06,270 --> 00:05:07,462
If you have the Z score,

86
00:05:07,462 --> 00:05:09,498
you have the probability to be above it.

87
00:05:12,407 --> 00:05:15,727
And now there&#39;s this cool little trick that people use for

88
00:05:15,727 --> 00:05:17,070
notation.

89
00:05:17,070 --> 00:05:20,124
So you see the standard normal has mean 0.

90
00:05:20,124 --> 00:05:22,241
So if you know alpha and Z alpha,

91
00:05:22,241 --> 00:05:25,390
you automatically know the answer to this one.

92
00:05:27,510 --> 00:05:31,400
If that&#39;s alpha, what is the Z that corresponds to this?

93
00:05:32,660 --> 00:05:37,007
And because the standard normal is symmetric,

94
00:05:37,007 --> 00:05:40,164
the answer is actually -Z alpha.

95
00:05:44,189 --> 00:05:46,741
Sometimes people like to ask questions about whether

96
00:05:46,741 --> 00:05:48,490
something is unusual.

97
00:05:48,490 --> 00:05:49,330
Meaning it could be in

98
00:05:49,330 --> 00:05:52,200
one tail of the distribution or the other.

99
00:05:52,200 --> 00:05:56,867
In that case they usually say that things

100
00:05:56,867 --> 00:06:00,030
are okay 1-a of the time.

101
00:06:00,030 --> 00:06:02,993
And then the probability of being unusual is alpha,

102
00:06:02,993 --> 00:06:06,170
and that probability gets split between the two tails.

103
00:06:07,410 --> 00:06:12,453
So again, you&#39;re this many standard deviations

104
00:06:12,453 --> 00:06:17,263
above the mean with probability, alpha/2.

105
00:06:21,278 --> 00:06:24,380
Okay, so I think we&#39;re ready to go back to confidence intervals.

106
00:06:24,380 --> 00:06:25,470
You wanna know

107
00:06:25,470 --> 00:06:26,930
what&#39;s the average amount of coffee in a cup.

108
00:06:26,930 --> 00:06:28,830
You make n measurements.

109
00:06:28,830 --> 00:06:33,586
The sample average of which is 1.07 cups.

110
00:06:33,586 --> 00:06:37,702
So what is the confidence interval around x bar for

111
00:06:37,702 --> 00:06:39,962
the true population mean?

112
00:06:42,527 --> 00:06:43,815
And now I can show you the answer,

113
00:06:43,815 --> 00:06:45,250
because now you&#39;ll understand it.

114
00:06:45,250 --> 00:06:46,965
So here&#39;s the formula.

115
00:06:46,965 --> 00:06:49,640
You have data coming from a normal distribution.

116
00:06:49,640 --> 00:06:51,720
Let&#39;s say you know the standard deviation sigma,

117
00:06:51,720 --> 00:06:52,630
so that&#39;s known.

118
00:06:52,630 --> 00:06:54,830
And you have n observations.

119
00:06:56,050 --> 00:06:57,035
N for us is 100.

120
00:06:58,200 --> 00:07:00,188
Now with probability alpha,

121
00:07:00,188 --> 00:07:05,380
the true population mean mu is within this interval right here.

122
00:07:05,380 --> 00:07:09,646
So it&#39;s x bar + this thing and x bar- this thing and

123
00:07:09,646 --> 00:07:13,427
now you know what all the terms in there mean.

124
00:07:13,427 --> 00:07:15,620
So n is 100 cups of coffee.

125
00:07:15,620 --> 00:07:18,427
You assume you have sigma for this particular problem.

126
00:07:18,427 --> 00:07:21,792
And then, given alpha, you know what z alpha/2 means.

127
00:07:21,792 --> 00:07:25,906
Now, let me write down the formula directly.

128
00:07:29,227 --> 00:07:31,740
Okay, I can say it in a different way here.

129
00:07:34,070 --> 00:07:39,900
With probability 1- alpha, mu is in this interval here.

130
00:07:41,260 --> 00:07:43,082
And this is your first formula for

131
00:07:43,082 --> 00:07:45,044
computing a confidence interval.

132
00:07:48,768 --> 00:07:52,710
Now, confidence intervals come in two other flavors called one

133
00:07:52,710 --> 00:07:53,240
sided.

134
00:07:53,240 --> 00:07:56,500
And what I just showed was a two sided confidence interval.

135
00:07:58,840 --> 00:08:02,800
So, here, we just want to know that mu is not too big.

136
00:08:04,470 --> 00:08:09,150
So whatever x bar is, we just want an upper bound for

137
00:08:09,150 --> 00:08:10,430
mu, okay.

138
00:08:11,880 --> 00:08:17,310
So we would say, with probability 1- alpha, mu

139
00:08:17,310 --> 00:08:21,500
is less than x bar + this thing that you know how to compute.

140
00:08:21,500 --> 00:08:26,000
And this is called upper one sided because it provides

141
00:08:26,000 --> 00:08:27,904
an upper bound for mu.

142
00:08:31,327 --> 00:08:35,272
And then this one&#39;s lower one sided, where we want to know

143
00:08:35,272 --> 00:08:38,747
that mu is greater than something, which is that.

144
00:08:41,251 --> 00:08:42,980
And so here it is.

145
00:08:42,980 --> 00:08:44,240
So with probability 1- alpha,

146
00:08:44,240 --> 00:08:47,158
mu is greater than x bar- that thing.

147
00:08:47,158 --> 00:08:52,080
Okay, so here&#39;s a summary of the three formulas for

148
00:08:52,080 --> 00:08:53,430
the confidence interval.

149
00:08:53,430 --> 00:08:56,612
This is for the 2-sided confidence interval,

150
00:08:56,612 --> 00:09:00,270
where you&#39;ve alpha over 2 on each side,

151
00:09:00,270 --> 00:09:03,710
and then you have the lower 1-sided confidence interval and

152
00:09:03,710 --> 00:09:05,998
the upper 1-sided one.

153
00:09:05,998 --> 00:09:09,020
And now I hope at least some of you are saying to yourselves,

154
00:09:09,020 --> 00:09:11,810
but all this is useless since I don&#39;t actually know

155
00:09:11,810 --> 00:09:13,170
the true standard deviation,

156
00:09:13,170 --> 00:09:15,810
sigma, I can&#39;t plug anything into this formula.

157
00:09:16,930 --> 00:09:17,710
Well that would be true.

158
00:09:18,930 --> 00:09:23,540
But the good news is that the sample standard deviation s

159
00:09:23,540 --> 00:09:26,760
converges very quickly to the true standard deviation sigma.

160
00:09:27,760 --> 00:09:30,680
So as long as you have enough data, it&#39;s pretty safe to plug

161
00:09:30,680 --> 00:09:33,820
in the sample standard deviation s instead of that sigma.

162
00:09:35,920 --> 00:09:37,420
Now remember, s you can calculate,

163
00:09:37,420 --> 00:09:38,460
whereas sigma you can&#39;t.

164
00:09:40,340 --> 00:09:44,762
And the other piece of good news is that there is a way to change

165
00:09:44,762 --> 00:09:48,060
this confidence interval just a little bit so

166
00:09:48,060 --> 00:09:52,516
that it has an s in it and not a sigma using the T distribution.

167
00:09:54,083 --> 00:09:57,325
Now there are lots of other kinds of confidence intervals

168
00:09:57,325 --> 00:09:59,888
and I&#39;m not sure it&#39;s the best use of time for

169
00:09:59,888 --> 00:10:03,340
me to go through all of the different variations.

170
00:10:03,340 --> 00:10:05,775
But, at least you know the foundations and

171
00:10:05,775 --> 00:10:07,607
hopefully you can go from here.

172
00:10:11,148 --> 00:10:14,820
Let&#39;s put that formula for the two sided confidence interval

173
00:10:14,820 --> 00:10:16,450
for the mean back up there.

174
00:10:17,540 --> 00:10:19,720
Okay, so why do you care as data scientists?

175
00:10:20,860 --> 00:10:21,980
Because what you can do for

176
00:10:21,980 --> 00:10:25,520
people now is quantify the uncertainty.

177
00:10:25,520 --> 00:10:29,810
If you tell someone that you&#39;re gonna, next year

178
00:10:29,810 --> 00:10:33,370
you&#39;re gonna earn 300,000 bucks, are they going to believe you?

179
00:10:33,370 --> 00:10:34,699
Probably not, right?

180
00:10:34,699 --> 00:10:38,727
The chance that you&#39;ll make exactly 300,000 bucks to

181
00:10:38,727 --> 00:10:40,980
the penny is almost exactly zero.

182
00:10:40,980 --> 00:10:42,910
But how much will you make?

183
00:10:45,750 --> 00:10:51,320
If they say you&#39;ll make $300k plus or minus $200k?

184
00:10:51,320 --> 00:10:54,460
Well that&#39;s not a very certain estimate, is it?

185
00:10:54,460 --> 00:10:58,870
But if they say you&#39;ll make $300,000 + 20,

186
00:10:58,870 --> 00:11:00,990
then this is a totally different story.

187
00:11:00,990 --> 00:11:01,624
All right,

188
00:11:01,624 --> 00:11:04,742
it&#39;s much more certain that you&#39;ll make about 300k.

189
00:11:04,742 --> 00:11:07,940
Okay, so, the bottom line, confidence intervals,

190
00:11:07,940 --> 00:11:11,778
they quantify uncertainty about a parameter of the population.

