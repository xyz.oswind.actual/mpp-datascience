0
00:00:04,897 --> 00:00:07,485
Now hypothesis testing is obviously essential topic in

1
00:00:07,485 --> 00:00:08,976
statistics.

2
00:00:08,976 --> 00:00:12,795
Hypothesis tests, they&#39;re used as a sort of data

3
00:00:12,795 --> 00:00:14,890
centered technique to answer very specific yes or

4
00:00:14,890 --> 00:00:17,720
no questions, kind of beyond a reasonable doubt.

5
00:00:17,720 --> 00:00:20,802
Now there&#39;s been a lot of controversy over hypothesis

6
00:00:20,802 --> 00:00:23,750
tests lately because a psychology journal actually

7
00:00:23,750 --> 00:00:24,762
banned p values.

8
00:00:24,762 --> 00:00:26,645
And that happened recently.

9
00:00:26,645 --> 00:00:28,638
And then there was a response from

10
00:00:28,638 --> 00:00:32,129
the American Statistical Association kind of explaining

11
00:00:32,128 --> 00:00:35,332
what p values are and explaining what they&#39;re not and

12
00:00:35,332 --> 00:00:38,150
warning people sort of not to mess it up [LAUGH].

13
00:00:38,150 --> 00:00:40,790
And whether or not it&#39;s a good idea,

14
00:00:40,790 --> 00:00:44,320
there are multi-million dollar decisions being made based on p

15
00:00:44,320 --> 00:00:46,930
values every single day, like what drugs are safe for

16
00:00:46,930 --> 00:00:50,610
you to take, that depends somewhat on p values.

17
00:00:50,610 --> 00:00:53,123
There are lawsuits every day about the interpretation of

18
00:00:53,123 --> 00:00:54,590
these p values.

19
00:00:54,590 --> 00:00:58,690
So, let&#39;s discuss it in a way that not only can you use it in

20
00:00:58,690 --> 00:01:01,410
your toolbox but you can know whether someone&#39;s using it

21
00:01:01,410 --> 00:01:04,140
incorrectly in their toolbox.

22
00:01:04,140 --> 00:01:05,878
Let&#39;s start with an example.

23
00:01:05,878 --> 00:01:08,636
Your friend likes to go a casino that you think might do some

24
00:01:08,636 --> 00:01:09,930
shady deals.

25
00:01:09,930 --> 00:01:11,740
In fact you would not be surprised

26
00:01:11,740 --> 00:01:14,960
if they have interesting ways of cheating you out of money.

27
00:01:14,960 --> 00:01:17,099
Now a regular casino has an edge, but

28
00:01:17,099 --> 00:01:19,385
this one possibly has a negative edge.

29
00:01:19,385 --> 00:01:23,260
Now you know that if you roll the roulette wheel and

30
00:01:23,260 --> 00:01:28,057
you bet on black, you should have a 47% chance of winning if

31
00:01:28,057 --> 00:01:30,096
the wheel is fair, okay.

32
00:01:30,096 --> 00:01:34,137
So there are 18 bins that are black out of 38.

33
00:01:34,137 --> 00:01:35,788
So you go there with your friend.

34
00:01:35,788 --> 00:01:40,088
And she bets on black 20 times, and she wins 4 games, not so

35
00:01:40,088 --> 00:01:40,600
good.

36
00:01:41,860 --> 00:01:43,275
And now she wants to go and play more games.

37
00:01:43,275 --> 00:01:45,375
But you tap her on the shoulder and say,

38
00:01:45,375 --> 00:01:47,545
hey, can I talk to you just for a second?

39
00:01:47,545 --> 00:01:48,246
Your friend is so

40
00:01:48,246 --> 00:01:50,305
intent on playing that she doesn&#39;t wanna quit.

41
00:01:50,305 --> 00:01:53,253
But after some more annoying tapping on her shoulder she

42
00:01:53,253 --> 00:01:56,074
finally relents, and you whisper something to her,

43
00:01:56,074 --> 00:01:57,154
takes two seconds.

44
00:01:57,154 --> 00:01:58,362
She turns around again, and

45
00:01:58,362 --> 00:02:00,583
sure enough she walks away and doesn&#39;t play again.

46
00:02:00,583 --> 00:02:02,160
So what did you say to her?

47
00:02:04,150 --> 00:02:09,239
You said if the wheel was fair, then probability to get a result

48
00:02:09,239 --> 00:02:13,860
as bad as she did, to win 4 or less times, that’s 1%.

49
00:02:13,860 --> 00:02:15,050
And now you know for

50
00:02:15,050 --> 00:02:18,853
yourself that what you&#39;ve told her is actually a p-value.

51
00:02:18,853 --> 00:02:21,662
And you don&#39;t need to explain to her the the basics of hypothesis

52
00:02:21,662 --> 00:02:23,586
testing for her to figure this out, right,

53
00:02:23,586 --> 00:02:26,449
that since this probability is so low, there&#39;s not a reasonable

54
00:02:26,449 --> 00:02:29,220
chance that this roulette wheel is fair.

55
00:02:29,220 --> 00:02:31,720
So she concludes that she&#39;s gonna reject her hypothesis that

56
00:02:31,720 --> 00:02:34,690
the wheel is fair and go play some other game.

57
00:02:34,690 --> 00:02:37,313
And knowing this casino these games are probably unfair too,

58
00:02:37,313 --> 00:02:39,683
but at least she has you along to provide the statistical

59
00:02:39,683 --> 00:02:40,201
analysis.

60
00:02:40,201 --> 00:02:41,960
Okay, so what&#39;s going on here?

61
00:02:43,880 --> 00:02:45,410
Now this is the binomial distribution.

62
00:02:46,440 --> 00:02:50,580
If the wheel was fair, which is called our null hypothesis, then

63
00:02:50,580 --> 00:02:53,420
this is the distribution of wins we would get after 20 games.

64
00:02:55,000 --> 00:02:57,798
Now, since the chance to win one game is 47%,

65
00:02:57,798 --> 00:03:01,357
we&#39;d generally win between sort of 8 and 14 games, okay?

66
00:03:01,357 --> 00:03:03,961
Now, the probability of 4 wins is really small.

67
00:03:03,961 --> 00:03:07,755
And even if you add up the probability of getting 4 wins,

68
00:03:07,755 --> 00:03:12,140
3 wins, 2 wins, 1 win or 0 wins, it&#39;s still less than 1%.

69
00:03:12,140 --> 00:03:13,371
So there&#39;s something wrong, right.

70
00:03:13,371 --> 00:03:15,517
The chance that this is a fair roulette game,

71
00:03:15,517 --> 00:03:17,006
at this point is pretty small.

72
00:03:17,006 --> 00:03:21,510
And the p-value, by the way, this is the probability to

73
00:03:21,510 --> 00:03:25,010
observe something as extreme or more extreme than what you

74
00:03:25,010 --> 00:03:28,470
actually got, assuming that the null hypothesis is true.

75
00:03:30,000 --> 00:03:31,163
Okay, refresh,

76
00:03:31,163 --> 00:03:34,428
what you said to your friend got the point across.

77
00:03:34,428 --> 00:03:38,056
But let&#39;s say that you had time to not only whisper one sentence

78
00:03:38,056 --> 00:03:40,670
in her ear, but you had a few minutes to do it.

79
00:03:40,670 --> 00:03:42,943
And then you could explain things a bit more thoroughly.

80
00:03:42,943 --> 00:03:47,377
And you tell her, [SOUND] I&#39;m gonna set up a hypothesis test.

81
00:03:47,377 --> 00:03:51,199
The null hypothesis is that everything is fair,

82
00:03:51,199 --> 00:03:53,480
as it should be.

83
00:03:53,480 --> 00:03:56,120
The roulette wheel is gonna give you a 0.47 chance

84
00:03:56,120 --> 00:03:57,380
of winning if you bet on black.

85
00:03:59,620 --> 00:04:02,886
Now, in general, the null hypothesis is that everything is

86
00:04:02,886 --> 00:04:06,095
behaving as the status quo as you would expect it to behave.

87
00:04:06,095 --> 00:04:07,974
Now you get that terminology?

88
00:04:07,974 --> 00:04:11,811
A null hypothesis is the claim of no difference, okay,

89
00:04:11,811 --> 00:04:16,056
the claim that everything is as you&#39;d expect it to be if things

90
00:04:16,055 --> 00:04:16,890
were fair.

91
00:04:17,930 --> 00:04:20,457
And now here the null hypothesis is that the casino

92
00:04:20,457 --> 00:04:23,420
is playing fair and that the probability to win is 47%.

93
00:04:23,420 --> 00:04:27,827
And now the alternative hypothesis is the claim we&#39;re

94
00:04:27,827 --> 00:04:29,271
trying to prove.

95
00:04:29,271 --> 00:04:34,305
So here we&#39;re trying to prove that the casino is not playing

96
00:04:34,305 --> 00:04:39,366
fairly, that the probability to win is below that 47%.

97
00:04:39,366 --> 00:04:43,564
Now in this case the p-value provides a lot of evidence in

98
00:04:43,564 --> 00:04:46,457
favor of the alternative hypothesis.

99
00:04:48,818 --> 00:04:50,248
Okay, so here I have the definition.

100
00:04:50,248 --> 00:04:53,070
So the p-value is the probability to observe something

101
00:04:53,070 --> 00:04:57,430
as extreme or more extreme than what you observed.

102
00:04:59,250 --> 00:05:00,396
And when I say extreme,

103
00:05:00,396 --> 00:05:02,755
I mean extreme relative to the null hypothesis.

104
00:05:02,755 --> 00:05:04,813
So I&#39;ll just put that there explicitly.

105
00:05:06,850 --> 00:05:07,737
Okay, now,

106
00:05:07,737 --> 00:05:12,181
clearly 1% was a small enough p-value that your friend decided

107
00:05:12,181 --> 00:05:15,896
to reject the null hypothesis that the wheel is fair and

108
00:05:15,896 --> 00:05:19,720
accept the alternative hypothesis that it was rigged.

109
00:05:20,970 --> 00:05:22,420
But at what point would that p-value

110
00:05:22,420 --> 00:05:24,100
be low enough that she should do that?

111
00:05:25,150 --> 00:05:28,350
Maybe if things weren&#39;t as bad and she won a few more times,

112
00:05:28,350 --> 00:05:31,614
then she might have believed that the results could actually

113
00:05:31,614 --> 00:05:32,899
come from a fair wheel.

114
00:05:32,899 --> 00:05:35,720
Okay, so let&#39;s take a look at these probabilities here.

115
00:05:35,720 --> 00:05:38,900
So at what point do we decide that things are really unfair,

116
00:05:38,900 --> 00:05:41,500
just worse than just bad luck?

117
00:05:41,500 --> 00:05:44,090
So for instance if you had whispered in her ear that

118
00:05:44,090 --> 00:05:48,550
the probability to observe something that extreme was 19%,

119
00:05:48,550 --> 00:05:50,890
would she have gotten up and never come back?

120
00:05:50,890 --> 00:05:53,450
Probably not, cuz at that point,

121
00:05:53,450 --> 00:05:55,850
she might still have the sense that the wheel might be fair.

122
00:05:59,720 --> 00:06:02,360
Okay, what about if she has 6 wins?

123
00:06:02,360 --> 00:06:05,357
Right, she might still think the wheel is fair at 6 wins.

124
00:06:05,357 --> 00:06:08,689
Okay, there&#39;s still a 9% chance to get a result as

125
00:06:08,689 --> 00:06:10,290
extreme as what she got.

126
00:06:11,310 --> 00:06:14,270
Now at some point there has to be a threshold below which you

127
00:06:14,270 --> 00:06:16,220
would declare the wheel unfair.

128
00:06:18,600 --> 00:06:22,435
And that threshold&#39;s called alpha, and usually alpha is 5%.

129
00:06:22,435 --> 00:06:25,880
Now I have a definition up here for the significance level.

130
00:06:25,880 --> 00:06:29,947
So the significance level alpha is if the p-value&#39;s below alpha,

131
00:06:29,947 --> 00:06:31,884
we reject the null hypothesis.

132
00:06:31,884 --> 00:06:35,499
So it&#39;s the point below which we would reject the null

133
00:06:35,499 --> 00:06:36,470
hypothesis.

134
00:06:36,470 --> 00:06:40,167
So it&#39;s the point where we decide that the observations we

135
00:06:40,167 --> 00:06:43,568
got are no longer in the 95% realm of possibility.

136
00:06:43,568 --> 00:06:45,990
So we don&#39;t believe the null hypothesis and

137
00:06:45,990 --> 00:06:48,550
instead accept the alternative hypothesis.

138
00:06:50,480 --> 00:06:54,190
Now, by the way, there is nothing special about 0.05,

139
00:06:54,190 --> 00:06:56,920
it&#39;s just what everyone uses, okay.

140
00:06:56,920 --> 00:06:57,700
But really you could use

141
00:06:57,700 --> 00:06:59,160
whatever you think is the right level there.

142
00:07:01,430 --> 00:07:04,791
Right, so now that you have all the basic definitions,

143
00:07:04,791 --> 00:07:06,803
let&#39;s start over at the casino.

144
00:07:06,803 --> 00:07:08,578
Okay, before you go into the casino,

145
00:07:08,578 --> 00:07:10,700
you do a little prep work.

146
00:07:10,700 --> 00:07:13,510
You say to your friend, I think the wheel is rigged.

147
00:07:13,510 --> 00:07:15,750
I&#39;m gonna set up a hypothesis test.

148
00:07:15,750 --> 00:07:19,163
Here&#39;s my null and alternative hypotheses.

149
00:07:19,163 --> 00:07:21,772
Okay, let&#39;s say that you&#39;ve rolled 20 times,

150
00:07:21,772 --> 00:07:23,531
if the result is really unlikely,

151
00:07:23,531 --> 00:07:25,970
I&#39;ll whisper in your ear and tell you about it.

152
00:07:27,460 --> 00:07:30,280
By the way how extreme do things need to be for

153
00:07:30,280 --> 00:07:31,310
me to whisper in your ear?

154
00:07:33,380 --> 00:07:35,981
Then she says, 5%.

155
00:07:35,981 --> 00:07:39,281
She says, tell you what, if I observe that it&#39;s not in the 95%

156
00:07:39,281 --> 00:07:42,250
realm of possibility, you can interrupt my fun.

157
00:07:42,250 --> 00:07:45,330
So she gives you the significance level of 0.05, and

158
00:07:45,330 --> 00:07:47,480
once that&#39;s all set up, then she plays.

159
00:07:49,520 --> 00:07:51,610
And then she sees the probability to get something

160
00:07:51,610 --> 00:07:52,230
as extreme or

161
00:07:52,230 --> 00:07:55,890
more extreme than what she got under the null hypothesis.

162
00:07:55,890 --> 00:07:59,542
And realizing it&#39;s below the significance level, she decides

163
00:07:59,542 --> 00:08:02,449
that this is not within the realm of possibility and

164
00:08:02,449 --> 00:08:03,740
goes somewhere else.

165
00:08:03,740 --> 00:08:06,056
Now in this case the outcome was really unlikely, but

166
00:08:06,056 --> 00:08:07,970
things could have turned out differently.

167
00:08:09,160 --> 00:08:11,300
Let&#39;s say she had won six times.

168
00:08:11,300 --> 00:08:12,560
Then things would have been different

169
00:08:12,560 --> 00:08:16,554
because the probability to get six or fewer wins is 9%.

170
00:08:16,554 --> 00:08:19,990
And since that&#39;s not below the significance level,

171
00:08:19,990 --> 00:08:21,390
we wouldn&#39;t have stopped the game.

172
00:08:22,620 --> 00:08:25,570
This however does not mean that the wheel is fair,

173
00:08:25,570 --> 00:08:28,620
not by any means, but it means we don&#39;t have enough

174
00:08:28,620 --> 00:08:31,720
evidence to conclude that the wheel is unfair.

175
00:08:33,360 --> 00:08:36,854
Now in general for hypothesis tests if you don&#39;t reject

176
00:08:36,854 --> 00:08:40,793
the null hypothesis, you cannot accept the null hypothesis or

177
00:08:40,793 --> 00:08:42,295
any other hypothesis.

178
00:08:44,949 --> 00:08:46,319
So I hope you get the idea.

179
00:08:46,319 --> 00:08:48,157
So let&#39;s go over the general outline for

180
00:08:48,157 --> 00:08:49,730
how to conduct a hypothesis test.

181
00:08:50,860 --> 00:08:52,810
First you define the null hypothesis and

182
00:08:52,810 --> 00:08:54,890
the alternative hypothesis.

183
00:08:54,890 --> 00:08:57,860
The null hypothesis is the hypothesis of no change, and

184
00:08:57,860 --> 00:08:59,450
the alternative is the thing you wanna prove.

185
00:09:01,730 --> 00:09:03,363
And here we were looking for

186
00:09:03,363 --> 00:09:06,558
the test to give us an unusually small number of wins,

187
00:09:06,558 --> 00:09:09,338
which means the probability is below 47%.

188
00:09:09,338 --> 00:09:13,480
So, we&#39;re looking at that tail of the distribution.

189
00:09:15,770 --> 00:09:18,565
But we could look at the alternative hypothesis as being

190
00:09:18,565 --> 00:09:21,298
the wheel makes us win too often to be a standard wheel,

191
00:09:21,298 --> 00:09:22,784
in which case we would look for

192
00:09:22,784 --> 00:09:25,656
an unusual high number of wins, say, 15 or 16 wins.

193
00:09:28,952 --> 00:09:32,582
Or we could say that it&#39;s unusual to get either too small

194
00:09:32,582 --> 00:09:36,057
or too high wins, in which case we have an alternative

195
00:09:36,057 --> 00:09:38,760
hypothesis that looks like this, okay,

196
00:09:38,760 --> 00:09:43,113
where the alternative hypothesis is that p is not equal to 47%.

197
00:09:45,641 --> 00:09:47,380
Now the whole procedure can be repeated for

198
00:09:47,380 --> 00:09:49,670
many different kinds of tests.

199
00:09:49,670 --> 00:09:52,512
And here we&#39;re working on a single sample test using

200
00:09:52,512 --> 00:09:55,679
the binomial distribution as the null distribution, but

201
00:09:55,679 --> 00:09:58,535
you can create tests for other types of hypotheses.

202
00:09:58,535 --> 00:10:01,000
You wanna kinda follow this outline though.

203
00:10:01,000 --> 00:10:02,962
First you define the null hypothesis,

204
00:10:02,962 --> 00:10:05,166
which defines probability distribution.

205
00:10:05,166 --> 00:10:07,664
And then you define the alternative hypothesis which

206
00:10:07,664 --> 00:10:10,274
tells you what tail of the distribution you&#39;re gonna be

207
00:10:10,274 --> 00:10:12,450
looking at, or you could look at both of them.

208
00:10:15,680 --> 00:10:18,290
And then you define the significance level which tells

209
00:10:18,290 --> 00:10:20,340
you what decision you&#39;re gonna make after you see your data.

210
00:10:21,590 --> 00:10:23,970
Now the significance level tells you what is in the realm

211
00:10:23,970 --> 00:10:25,530
of the usual.

212
00:10:25,530 --> 00:10:28,614
And most of the time I choose a 0.05 significance level just so

213
00:10:28,614 --> 00:10:31,586
nobody asks me why I chose something different than 0.05,

214
00:10:31,586 --> 00:10:34,184
but there&#39;s nothing really special about 0.05.

215
00:10:36,824 --> 00:10:38,414
Then you calculate the p-value and

216
00:10:38,414 --> 00:10:40,380
reject if the p-value&#39;s less than alpha.

217
00:10:41,460 --> 00:10:42,200
Otherwise you don&#39;t reject,

218
00:10:42,200 --> 00:10:45,128
and then you don&#39;t accept any hypothesis, right.

219
00:10:46,140 --> 00:10:48,010
If you don&#39;t reject the null hypothesis,

220
00:10:48,010 --> 00:10:51,470
this does not mean that the null hypothesis is true.

221
00:10:51,470 --> 00:10:54,958
In our case, if we have 6 successes out of 20, right,

222
00:10:54,958 --> 00:10:58,228
with a p-value of 9%, we might not be able to say for

223
00:10:58,228 --> 00:11:00,210
sure that the wheel&#39;s unfair.

224
00:11:00,210 --> 00:11:02,660
But certainly the wheel could definitely be unfair.

225
00:11:02,660 --> 00:11:04,663
We just didn&#39;t get enough evidence to prove it.

226
00:11:06,922 --> 00:11:12,685
So to summarize, does the data contradict the null hypothesis

227
00:11:12,685 --> 00:11:17,480
beyond a reasonable doubt if you assume it&#39;s true?

228
00:11:17,480 --> 00:11:21,841
If yes, then you accept the alternative hypothesis and

229
00:11:21,841 --> 00:11:26,665
reject the null hypothesis, and if not, the null hypothesis

230
00:11:26,665 --> 00:11:30,682
can&#39;t be ruled out as an explanation for the data.

231
00:11:30,682 --> 00:11:35,002
In that case, we can&#39;t accept any hypothesis.

232
00:11:35,002 --> 00:11:39,196
And the analogy is the notion of a person being presumed innocent

233
00:11:39,196 --> 00:11:41,070
until being proven guilty.

234
00:11:42,090 --> 00:11:44,301
So the null hypothesis is that they&#39;re not guilty, and

235
00:11:44,301 --> 00:11:45,955
the alternative is that they are guilty.

236
00:11:45,955 --> 00:11:50,630
So the question is, does the data contradict H0?

237
00:11:50,630 --> 00:11:52,580
If yes, then rule as guilty.

238
00:11:52,580 --> 00:11:55,000
If not, it doesn&#39;t mean the person is innocent, but

239
00:11:55,000 --> 00:11:57,680
evidence is insufficient to establish guilt.

240
00:11:57,680 --> 00:12:01,290
It gives the person the benefit of the doubt.

