0
00:00:01,070 --> 00:00:04,370
Hi and welcome so Cynthia has been

1
00:00:04,370 --> 00:00:07,580
discussing the I concept of simulation

2
00:00:07,580 --> 00:00:10,580
and she used that example of the

3
00:00:10,580 --> 00:00:13,940
lemonade stand profitability simulation

4
00:00:13,940 --> 00:00:16,490
and in this demo I&#39;m going to show you

5
00:00:16,490 --> 00:00:19,939
the R code we use to actually run that

6
00:00:19,939 --> 00:00:22,220
simulation and how it how things fit

7
00:00:22,220 --> 00:00:23,990
together and practice and how you can

8
00:00:23,990 --> 00:00:27,020
create simulations

9
00:00:27,020 --> 00:00:29,840
so i have on my screen here this

10
00:00:29,840 --> 00:00:34,040
notebook which are creating simulations

11
00:00:34,040 --> 00:00:35,870
and I have some functions here to

12
00:00:35,870 --> 00:00:38,510
compute some confidence intervals which

13
00:00:38,510 --> 00:00:39,890
we&#39;re going to use to compare

14
00:00:39,890 --> 00:00:43,460
distributions I&#39;m have a summary

15
00:00:43,460 --> 00:00:47,180
function will be using to completion

16
00:00:47,180 --> 00:00:50,540
histograms and some of those summary

17
00:00:50,540 --> 00:00:51,920
statistics

18
00:00:51,920 --> 00:00:54,200
alright so that&#39;s just setting ourselves

19
00:00:54,200 --> 00:00:58,580
up so we can look at what we&#39;re doing so

20
00:00:58,580 --> 00:01:00,739
the first bit is when you&#39;re building a

21
00:01:00,739 --> 00:01:03,290
simulation you need to create lots of

22
00:01:03,290 --> 00:01:07,239
realizations of

23
00:01:07,650 --> 00:01:11,790
of a distribution and so you might say

24
00:01:11,790 --> 00:01:13,590
well like how many just how many

25
00:01:13,590 --> 00:01:15,900
realizations should i create and how do

26
00:01:15,900 --> 00:01:17,070
I know that

27
00:01:17,070 --> 00:01:18,780
so this function here we&#39;re going to

28
00:01:18,780 --> 00:01:20,520
create realizations of a normal

29
00:01:20,520 --> 00:01:22,820
distribution

30
00:01:23,620 --> 00:01:27,409
let me just load that function

31
00:01:27,409 --> 00:01:28,789
and we&#39;re going to look at some of those

32
00:01:28,789 --> 00:01:31,399
summary statistics for it and we&#39;re

33
00:01:31,399 --> 00:01:34,100
going to do that for you see there&#39;s

34
00:01:34,100 --> 00:01:36,829
this vector i&#39;m creating it called numbs

35
00:01:36,829 --> 00:01:40,219
of a hundred a thousand ten thousand a

36
00:01:40,219 --> 00:01:45,219
hundred thousand realizations ok

37
00:01:46,440 --> 00:01:50,250
so here&#39;s our first and we&#39;ll just look

38
00:01:50,250 --> 00:01:52,080
at like the mean and the median so the

39
00:01:52,080 --> 00:01:54,300
median is a little less than six a

40
00:01:54,300 --> 00:01:58,110
little over 600 the means low over 600

41
00:01:58,110 --> 00:01:59,970
here&#39;s the histogram so the mean and the

42
00:01:59,970 --> 00:02:01,740
median look about right because we had a

43
00:02:01,740 --> 00:02:06,300
a mean of 600 here

44
00:02:06,300 --> 00:02:08,190
but it doesn&#39;t really look like a

45
00:02:08,190 --> 00:02:10,140
bell-shaped curve it&#39;s pretty raggedy

46
00:02:10,139 --> 00:02:12,150
because we only have a hundred

47
00:02:12,150 --> 00:02:14,910
realizations so now we&#39;re going to do a

48
00:02:14,910 --> 00:02:17,640
thousand and you can see the mean and

49
00:02:17,640 --> 00:02:19,410
the median or little maybe a little bit

50
00:02:19,410 --> 00:02:23,100
closer but that not really actually ones

51
00:02:23,100 --> 00:02:25,230
further one&#39;s closer to 600 which is

52
00:02:25,230 --> 00:02:28,260
what their ideal should be but the curve

53
00:02:28,260 --> 00:02:30,570
is the histogram is now looking a little

54
00:02:30,570 --> 00:02:34,520
bit more like a bell-shaped curve

55
00:02:35,280 --> 00:02:39,650
so for 10,000 values

56
00:02:39,810 --> 00:02:42,660
spot on the median is sort of still

57
00:02:42,660 --> 00:02:46,700
wiggling around a little bit

58
00:02:46,770 --> 00:02:48,720
and that really is starting to look like

59
00:02:48,720 --> 00:02:51,120
a bell-shaped curve but it&#39;s it&#39;s kind

60
00:02:51,120 --> 00:02:52,860
of a hairy bell-shaped curve it&#39;s got

61
00:02:52,860 --> 00:02:56,220
these little spikes and and tails and

62
00:02:56,220 --> 00:02:59,790
things so then finally for 10,000 cases

63
00:02:59,790 --> 00:03:04,170
mean and median are spot-on and that

64
00:03:04,170 --> 00:03:05,490
really does look like a bell-shaped

65
00:03:05,490 --> 00:03:08,160
curve installed just a little ragged but

66
00:03:08,160 --> 00:03:11,190
it you know that definitely is is

67
00:03:11,190 --> 00:03:13,740
passable more than passable fact

68
00:03:13,740 --> 00:03:16,980
probably even at the 10,000 cases is all

69
00:03:16,980 --> 00:03:20,670
we need it now we can do the same thing

70
00:03:20,670 --> 00:03:23,610
for a poisson distribution so keep this

71
00:03:23,610 --> 00:03:27,420
distribution in mind here where we had

72
00:03:27,420 --> 00:03:30,360
the 10,000 cases and I have this

73
00:03:30,360 --> 00:03:32,970
function that creates the realizations

74
00:03:32,970 --> 00:03:35,640
for poisson distribution in the same

75
00:03:35,640 --> 00:03:38,710
summary statistics

76
00:03:38,710 --> 00:03:41,950
and so will do 10,000 cases and again

77
00:03:41,950 --> 00:03:44,710
the mean and the median or spot-on

78
00:03:44,710 --> 00:03:47,020
now that the shape is all funny because

79
00:03:47,020 --> 00:03:49,180
just the way the histogram bins but you

80
00:03:49,180 --> 00:03:52,240
can see it&#39;s it&#39;s a nice smooth almost

81
00:03:52,240 --> 00:03:56,860
bell-shaped curve are so if these

82
00:03:56,860 --> 00:04:01,260
because we have

83
00:04:01,410 --> 00:04:03,940
600

84
00:04:03,940 --> 00:04:06,730
expected arrival rate the difference

85
00:04:06,730 --> 00:04:08,740
between a normal distribution of poisson

86
00:04:08,740 --> 00:04:10,660
distribution at that point is pretty

87
00:04:10,660 --> 00:04:11,830
minimal

88
00:04:11,830 --> 00:04:13,630
even though technically you would think

89
00:04:13,630 --> 00:04:15,520
Poisson would be the right distribution

90
00:04:15,520 --> 00:04:19,890
to use for arrivals

91
00:04:20,048 --> 00:04:24,879
so let&#39;s move on here let&#39;s get to our

92
00:04:24,879 --> 00:04:26,259
lemonade stand

93
00:04:26,259 --> 00:04:29,710
so first off we had this distribution of

94
00:04:29,710 --> 00:04:34,449
profits and i&#39;m just using in our if if

95
00:04:34,449 --> 00:04:39,280
else statement here so we had so we

96
00:04:39,280 --> 00:04:42,970
compute realizations of a uniform random

97
00:04:42,970 --> 00:04:46,690
number and if that random number for

98
00:04:46,690 --> 00:04:49,240
each case is less than point 3 we say

99
00:04:49,240 --> 00:04:52,960
our our prophet was five if it&#39;s less

100
00:04:52,960 --> 00:04:54,639
than point six so that&#39;s forty percent

101
00:04:54,639 --> 00:04:56,349
that&#39;s 30-person another thirty percent

102
00:04:56,349 --> 00:04:57,759
of the time the difference between point

103
00:04:57,759 --> 00:05:01,360
three and point six it&#39;s 3.5 and for the

104
00:05:01,360 --> 00:05:03,699
remainder of cases it&#39;s for which is

105
00:05:03,699 --> 00:05:07,569
what Cynthia gave us when she discussed

106
00:05:07,569 --> 00:05:10,229
this problem

107
00:05:10,320 --> 00:05:13,170
and let&#39;s run again a hundred thousand

108
00:05:13,170 --> 00:05:16,740
realizations of that simulation and

109
00:05:16,740 --> 00:05:19,260
we&#39;ll look at this breakdown of these

110
00:05:19,260 --> 00:05:22,980
profits so our median profit turns out

111
00:05:22,980 --> 00:05:26,010
to be 4r mean profits a little higher at

112
00:05:26,010 --> 00:05:29,850
4.15 roughly and here&#39;s what the

113
00:05:29,850 --> 00:05:33,740
histogram looks like it&#39;s it&#39;s

114
00:05:33,740 --> 00:05:35,210
it kind of when you think about it it&#39;s

115
00:05:35,210 --> 00:05:36,620
exactly what we thought thirty percent

116
00:05:36,620 --> 00:05:38,750
of the time we had our prophet was

117
00:05:38,750 --> 00:05:40,970
three-and-a-half dollars thirty percent

118
00:05:40,970 --> 00:05:43,880
of the time our prophet was five dollars

119
00:05:43,880 --> 00:05:45,710
and the remaining forty percent of the

120
00:05:45,710 --> 00:05:48,770
time our prophet was four dollars so it

121
00:05:48,770 --> 00:05:50,419
looks like that&#39;s all working so we can

122
00:05:50,419 --> 00:05:53,419
compute profits we had a similar thing

123
00:05:53,419 --> 00:05:54,889
with the tips only it was a little more

124
00:05:54,889 --> 00:05:57,680
complicated i&#39;m not going to go through

125
00:05:57,680 --> 00:05:59,539
that just in the sake of the demo you

126
00:05:59,539 --> 00:06:02,030
since you already explained it was we

127
00:06:02,030 --> 00:06:04,069
have three possible to are four possible

128
00:06:04,069 --> 00:06:06,919
tips that are customer can leave so

129
00:06:06,919 --> 00:06:09,560
let&#39;s look at the summary

130
00:06:09,560 --> 00:06:12,050
of that will begin to 10,000

131
00:06:12,050 --> 00:06:16,130
realizations of those profits

132
00:06:16,130 --> 00:06:19,340
I&#39;m sorry of those tips and we see half

133
00:06:19,340 --> 00:06:23,690
the time our customer leaves us no tip

134
00:06:23,690 --> 00:06:26,510
twenty percent of the time the customer

135
00:06:26,510 --> 00:06:29,840
leaves us at twenty-five cents twenty

136
00:06:29,840 --> 00:06:31,520
percent of the time our customer leaves

137
00:06:31,520 --> 00:06:35,630
us a dollar and ten percent of the time

138
00:06:35,630 --> 00:06:38,030
our customer leaves us a whopping two

139
00:06:38,030 --> 00:06:40,790
dollars as a tip obviously a very happy

140
00:06:40,790 --> 00:06:42,260
customer

141
00:06:42,260 --> 00:06:50,280
so now we need to pull this together and

142
00:06:50,280 --> 00:06:54,030
have this code here where we go through

143
00:06:54,030 --> 00:06:58,910
the test steps in for and we&#39;re going to

144
00:06:59,780 --> 00:07:02,300
so first off what do we do so we compute

145
00:07:02,300 --> 00:07:04,700
the expected number of custom we are the

146
00:07:04,700 --> 00:07:07,040
distribution of customer rival so the

147
00:07:07,040 --> 00:07:10,760
realization of customer rivals over some

148
00:07:10,760 --> 00:07:14,150
number of days and for example the

149
00:07:14,150 --> 00:07:15,919
normal distribution is just what you saw

150
00:07:15,919 --> 00:07:18,080
before and we&#39;re just going to print

151
00:07:18,080 --> 00:07:22,520
that summary again and plot it then

152
00:07:22,520 --> 00:07:23,660
we&#39;re going to look again at the

153
00:07:23,660 --> 00:07:26,740
distribution of profits

154
00:07:27,780 --> 00:07:31,410
and the total profit is just making this

155
00:07:31,410 --> 00:07:33,780
assumption because the Prophet depends

156
00:07:33,780 --> 00:07:38,460
on the weather so that every customer

157
00:07:38,460 --> 00:07:40,620
who arrives on that day it&#39;s just going

158
00:07:40,620 --> 00:07:43,500
to be the same profit that because we

159
00:07:43,500 --> 00:07:45,630
can raise our price in hot weather but

160
00:07:45,630 --> 00:07:47,790
we have to lower price and cold weather

161
00:07:47,790 --> 00:07:54,110
so here&#39;s our total is just that

162
00:07:54,990 --> 00:07:57,580
that product in

163
00:07:57,580 --> 00:08:00,909
and we got our tips again we&#39;re gonna

164
00:08:00,909 --> 00:08:02,560
make the similar assumption for tips

165
00:08:02,560 --> 00:08:04,569
maybe not quite a realistic but that

166
00:08:04,569 --> 00:08:06,759
every customer on a given day leaves us

167
00:08:06,759 --> 00:08:09,400
the same tip maybe they&#39;re happier on

168
00:08:09,400 --> 00:08:12,280
certain days and not in others and our

169
00:08:12,280 --> 00:08:14,800
total to what i&#39;m calling total take is

170
00:08:14,800 --> 00:08:16,990
obviously just the sum of profits plus

171
00:08:16,990 --> 00:08:19,659
tips and we&#39;ll we&#39;ll print that so let

172
00:08:19,659 --> 00:08:22,080
me run that whole thing here

173
00:08:22,080 --> 00:08:25,520
first I&#39;m going to do that

174
00:08:26,500 --> 00:08:28,360
that whole thing and we&#39;ll look at what

175
00:08:28,360 --> 00:08:31,170
happened

176
00:08:31,520 --> 00:08:33,680
alright so here&#39;s the distribution of

177
00:08:33,679 --> 00:08:35,539
customer rivals per day that&#39;s no

178
00:08:35,539 --> 00:08:37,159
surprise we already looked at that

179
00:08:37,159 --> 00:08:39,919
earlier in this demo

180
00:08:39,919 --> 00:08:42,620
here&#39;s our distribution of profits per

181
00:08:42,620 --> 00:08:45,399
arrival

182
00:08:45,680 --> 00:08:49,760
and that&#39;s again no surprise there but

183
00:08:49,760 --> 00:08:51,680
now if you take the product of those two

184
00:08:51,680 --> 00:08:53,120
things you get this very humpy

185
00:08:53,120 --> 00:08:54,710
distribution because we have three

186
00:08:54,710 --> 00:08:57,800
possible profit levels depending on what

187
00:08:57,800 --> 00:09:00,260
day and we have this distribution of

188
00:09:00,260 --> 00:09:04,610
arrivals we get this three humped

189
00:09:04,610 --> 00:09:07,010
distribution and that would be something

190
00:09:07,010 --> 00:09:10,459
that would be at least tedious to deal

191
00:09:10,459 --> 00:09:14,209
with by analytical methods and obviously

192
00:09:14,209 --> 00:09:16,940
we&#39;re doing pretty well with simulation

193
00:09:16,940 --> 00:09:18,980
methods and we can see that are our

194
00:09:18,980 --> 00:09:22,399
median profit is about 2,400 and our

195
00:09:22,399 --> 00:09:25,339
mean profit is about 2490 or something

196
00:09:25,339 --> 00:09:29,089
like that so so so we get some summary

197
00:09:29,089 --> 00:09:33,080
statistics and that distribution here&#39;s

198
00:09:33,080 --> 00:09:35,180
again our tips

199
00:09:35,180 --> 00:09:36,950
no surprise there we looked at that

200
00:09:36,950 --> 00:09:40,820
before now total tips per day now 0 x

201
00:09:40,820 --> 00:09:43,700
any number of arrivals is 0 so we just

202
00:09:43,700 --> 00:09:46,130
get that spike there and then we have

203
00:09:46,130 --> 00:09:49,460
these other distributions depending on

204
00:09:49,460 --> 00:09:52,190
how many customers are arriving the day

205
00:09:52,190 --> 00:09:55,490
they&#39;re giving us whichever tip and we

206
00:09:55,490 --> 00:09:58,130
add that all together because our total

207
00:09:58,130 --> 00:10:00,530
profit is our tips plus our profit per

208
00:10:00,530 --> 00:10:05,860
cup and we now have a distribution with

209
00:10:05,889 --> 00:10:10,029
almost one two three four five Peaks

210
00:10:10,029 --> 00:10:13,480
very complicated thing but because we&#39;re

211
00:10:13,480 --> 00:10:15,549
using a simulation method we generate

212
00:10:15,549 --> 00:10:17,709
these realizations we can look at

213
00:10:17,709 --> 00:10:19,629
summary statistics about those

214
00:10:19,629 --> 00:10:23,410
realizations and we actually know a lot

215
00:10:23,410 --> 00:10:25,899
that we might be able to use for kit

216
00:10:25,899 --> 00:10:28,359
forecasting the profitability of our

217
00:10:28,359 --> 00:10:29,799
Lemonade Stand we could look at

218
00:10:29,799 --> 00:10:32,679
confidence intervals for example you

219
00:10:32,679 --> 00:10:36,129
know the best half of days would be you

220
00:10:36,129 --> 00:10:38,079
know what was the profit above the meat

221
00:10:38,079 --> 00:10:42,249
the median or something like that so so

222
00:10:42,249 --> 00:10:44,079
we&#39;ve we&#39;ve taken a fairly complicated

223
00:10:44,079 --> 00:10:46,869
problem and with not that much code

224
00:10:46,869 --> 00:10:51,929
created quite an insightful simulation

