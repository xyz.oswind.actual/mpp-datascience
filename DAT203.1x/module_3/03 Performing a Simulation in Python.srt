0
00:00:00,000 --> 00:00:05,160
Hi and welcome so Cynthia was discussing

1
00:00:05,160 --> 00:00:08,519
this concept of simulation and the theory

2
00:00:08,519 --> 00:00:11,610
of simulation and she was also talking

3
00:00:11,610 --> 00:00:14,700
specifically about this simulation of

4
00:00:14,700 --> 00:00:18,390
the lemonade stand profitability and in

5
00:00:18,390 --> 00:00:21,270
this video I&#39;m going to show you code in

6
00:00:21,270 --> 00:00:24,600
Python that we use to create some of

7
00:00:24,600 --> 00:00:26,279
those examples you saw in Cynthia&#39;s

8
00:00:26,279 --> 00:00:30,750
lecture so now you&#39;ll see hands on how

9
00:00:30,750 --> 00:00:33,899
we actually build a simulation and what

10
00:00:33,899 --> 00:00:36,530
goes into it

11
00:00:36,530 --> 00:00:41,590
so if we switch to my screen here

12
00:00:43,100 --> 00:00:44,730
have this

13
00:00:44,730 --> 00:00:47,160
code here that&#39;s going to create a

14
00:00:47,160 --> 00:00:49,020
summary is going to print some summary

15
00:00:49,020 --> 00:00:51,960
statistics and plots and histograms of

16
00:00:51,960 --> 00:00:56,059
our distributions

17
00:00:58,410 --> 00:01:02,430
I have a code here that computes

18
00:01:02,430 --> 00:01:05,460
realizations of from a normal

19
00:01:05,459 --> 00:01:10,380
distribution so we&#39;re using the numpy

20
00:01:10,380 --> 00:01:12,750
random library

21
00:01:12,750 --> 00:01:15,990
normal distribution here and it&#39;s just

22
00:01:15,990 --> 00:01:18,780
mean of mean and standard deviation and

23
00:01:18,780 --> 00:01:21,090
number of realizations and then the

24
00:01:21,090 --> 00:01:23,640
summary statistics okay nothing too

25
00:01:23,640 --> 00:01:27,180
exotic there and we&#39;re going to do this

26
00:01:27,180 --> 00:01:33,750
for 100 1000 10000 in 100,000

27
00:01:33,750 --> 00:01:35,100
realizations of the normal distribution

28
00:01:35,100 --> 00:01:38,039
and we&#39;re going to compare those summary

29
00:01:38,039 --> 00:01:40,880
statistics

30
00:01:41,930 --> 00:01:44,870
all right here we go so here&#39;s our first

31
00:01:44,870 --> 00:01:47,420
realization of a normal distribution

32
00:01:47,420 --> 00:01:50,620
with a hundred

33
00:01:50,840 --> 00:01:54,380
realizations and so it doesn&#39;t really

34
00:01:54,380 --> 00:01:56,840
look like much of a bell-shaped curve

35
00:01:56,840 --> 00:01:59,060
here right it&#39;s it&#39;s it&#39;s pretty jaggedy

36
00:01:59,060 --> 00:02:03,320
it&#39;s it&#39;s kind of discontinuous

37
00:02:03,320 --> 00:02:05,700
but if you look the mean

38
00:02:05,700 --> 00:02:10,649
is 600 and the 50-percent quantile which

39
00:02:10,649 --> 00:02:14,730
is the median is 601 so are some of our

40
00:02:14,730 --> 00:02:18,000
summary statistics aren&#39;t too bad but it

41
00:02:18,000 --> 00:02:20,250
clearly doesn&#39;t look like a very

42
00:02:20,250 --> 00:02:23,040
convincing normal distribution so now

43
00:02:23,040 --> 00:02:25,019
for a thousand it&#39;s starting to look

44
00:02:25,019 --> 00:02:27,330
like a bell-shaped curve it&#39;s pretty

45
00:02:27,330 --> 00:02:32,880
raggedy are mean and our median or maybe

46
00:02:32,880 --> 00:02:35,160
a little closer mean is is definitely

47
00:02:35,160 --> 00:02:37,080
closer median kind of wobbling around

48
00:02:37,080 --> 00:02:39,680
maybe a little bit

49
00:02:39,680 --> 00:02:41,689
now we&#39;re doing this for ten thousand

50
00:02:41,689 --> 00:02:44,659
values and that definitely looks like a

51
00:02:44,659 --> 00:02:46,519
bell-shaped curve but a bell-shaped

52
00:02:46,519 --> 00:02:50,160
curve with a little bit of hair on it

53
00:02:50,160 --> 00:02:53,340
and our mean is pretty much spot on our

54
00:02:53,340 --> 00:02:55,410
median is pretty much spot on at this

55
00:02:55,410 --> 00:02:56,760
point and now we&#39;re going to do a

56
00:02:56,760 --> 00:02:59,940
hundred thousand realizations and that&#39;s

57
00:02:59,940 --> 00:03:02,850
getting to be a pretty smooth you know

58
00:03:02,850 --> 00:03:04,920
within the limits of thinning histograms

59
00:03:04,920 --> 00:03:06,720
and all that a pretty smooth normal

60
00:03:06,720 --> 00:03:09,720
distribution with the mean absolutely

61
00:03:09,720 --> 00:03:12,660
spot-on and the median absolutely

62
00:03:12,660 --> 00:03:15,090
spot-on to like three decimal places are

63
00:03:15,090 --> 00:03:18,120
almost four decimal places so that gives

64
00:03:18,120 --> 00:03:20,070
you some idea of what we mean by

65
00:03:20,070 --> 00:03:23,850
simulating a distribution and

66
00:03:23,850 --> 00:03:27,540
realizations but we can also compare

67
00:03:27,540 --> 00:03:30,900
that to a poisson distribution so in

68
00:03:30,900 --> 00:03:32,970
many cases when you have an arrival rate

69
00:03:32,970 --> 00:03:35,160
like number of people per day arriving

70
00:03:35,160 --> 00:03:36,840
in our Lemonade Stand you might wish to

71
00:03:36,840 --> 00:03:40,500
use a plus on distribution and let&#39;s

72
00:03:40,500 --> 00:03:43,590
just compare that to the normal we just

73
00:03:43,590 --> 00:03:45,360
didn&#39;t do it for the hundred thousand

74
00:03:45,360 --> 00:03:47,620
cases

75
00:03:47,620 --> 00:03:51,700
and there&#39;s a slight bit of oddness hear

76
00:03:51,700 --> 00:03:53,739
that you have to let your I get past

77
00:03:53,739 --> 00:03:57,400
because of the way the histogram bins it

78
00:03:57,400 --> 00:04:01,000
looks there&#39;s these sort of cuts but but

79
00:04:01,000 --> 00:04:02,650
ignore that problem for a minute

80
00:04:02,650 --> 00:04:04,930
otherwise this looks like a pretty nice

81
00:04:04,930 --> 00:04:07,190
bell-shaped curve

82
00:04:07,190 --> 00:04:11,030
and as before are mean and median are

83
00:04:11,030 --> 00:04:12,530
spot-on

84
00:04:12,530 --> 00:04:15,440
so what&#39;s the point the point is that

85
00:04:15,440 --> 00:04:17,989
when you get to high arrival rates like

86
00:04:17,988 --> 00:04:20,090
in this case are expected arrival rate

87
00:04:20,089 --> 00:04:23,930
is 600 customers per day the normal

88
00:04:23,930 --> 00:04:25,910
distribution is just about as good an

89
00:04:25,910 --> 00:04:27,980
approximation as the poisson

90
00:04:27,980 --> 00:04:30,130
distribution

91
00:04:31,330 --> 00:04:34,810
so let&#39;s look at some of our specifics

92
00:04:34,810 --> 00:04:37,639
of this

93
00:04:37,639 --> 00:04:39,379
Lemonade Stand example so we have to

94
00:04:39,379 --> 00:04:43,520
generate profits so this somewhat long

95
00:04:43,520 --> 00:04:49,370
drawn-out state function here or sorry

96
00:04:49,370 --> 00:04:51,469
list comprehension here

97
00:04:51,469 --> 00:04:55,550
recall that is how we&#39;re going to

98
00:04:55,550 --> 00:04:58,099
compute the profits on a day and

99
00:04:58,099 --> 00:05:01,249
remember the the profits on a day depend

100
00:05:01,249 --> 00:05:04,340
on the weather so all profits for all

101
00:05:04,340 --> 00:05:06,349
cups of of lemonade are going to be the

102
00:05:06,349 --> 00:05:07,969
same on a certain day and they but they

103
00:05:07,969 --> 00:05:11,750
can be three and a half four or five and

104
00:05:11,750 --> 00:05:15,020
so we some simulate a uniform

105
00:05:15,020 --> 00:05:16,550
distribution we&#39;ve been doing you know

106
00:05:16,550 --> 00:05:18,919
puts on a normal before this is now a

107
00:05:18,919 --> 00:05:20,840
uniform distribution on the interval 0

108
00:05:20,840 --> 00:05:24,110
to 1 and if the uniform distribution is

109
00:05:24,110 --> 00:05:27,469
less than three-tenths we say our profit

110
00:05:27,469 --> 00:05:30,830
that day is five if it&#39;s less than six

111
00:05:30,830 --> 00:05:32,930
tenths the else so that&#39;s basically

112
00:05:32,930 --> 00:05:36,499
another three tenths it&#39;s 3.5 and for

113
00:05:36,499 --> 00:05:40,889
all of their cases it&#39;s for ok

114
00:05:40,889 --> 00:05:42,900
so let me

115
00:05:42,900 --> 00:05:45,900
if that function loaded here and we&#39;ll

116
00:05:45,900 --> 00:05:50,840
just do 10,000 realizations of that

117
00:05:53,139 --> 00:05:54,999
and it&#39;s just what we expected here&#39;s

118
00:05:54,999 --> 00:05:57,310
the histogram so thirty percent of the

119
00:05:57,310 --> 00:05:58,449
time we have a profit of

120
00:05:58,449 --> 00:06:00,310
three-and-a-half thirty percent of the

121
00:06:00,310 --> 00:06:03,370
time we have a profit of five and the

122
00:06:03,370 --> 00:06:04,930
remaining forty percent of the time we

123
00:06:04,930 --> 00:06:06,950
have a profit of four

124
00:06:06,950 --> 00:06:08,900
and we have to do a similar thing with

125
00:06:08,900 --> 00:06:10,250
the tips remember there were four

126
00:06:10,250 --> 00:06:12,860
possible tips that a customer could

127
00:06:12,860 --> 00:06:15,250
leave

128
00:06:18,050 --> 00:06:20,060
it works basically the same as what i

129
00:06:20,060 --> 00:06:22,400
just showed you and we&#39;ll go ahead and

130
00:06:22,400 --> 00:06:25,159
compute 10,000 realizations of the

131
00:06:25,159 --> 00:06:28,300
distribution of tips

132
00:06:28,300 --> 00:06:32,440
and so as we should have we have half

133
00:06:32,440 --> 00:06:34,240
the time our customer leaves us no tip

134
00:06:34,240 --> 00:06:36,729
twenty percent of the time our customer

135
00:06:36,729 --> 00:06:39,670
leaves us a 25 cents

136
00:06:39,670 --> 00:06:41,560
twenty percent of the time the customer

137
00:06:41,560 --> 00:06:44,320
leaves us a dollar and ten percent of

138
00:06:44,320 --> 00:06:45,760
the time the customer leaves us this

139
00:06:45,760 --> 00:06:49,540
whopping two dollars okay and our mean

140
00:06:49,540 --> 00:06:53,290
tip is about four-and-a-half and our

141
00:06:53,290 --> 00:06:57,930
median tip is about is 25 cents

142
00:06:58,790 --> 00:07:01,700
so let&#39;s look pull this all together and

143
00:07:01,700 --> 00:07:03,590
simulate the total profits of our

144
00:07:03,590 --> 00:07:05,810
Lemonade Stand so what are we doing here

145
00:07:05,810 --> 00:07:07,070
so first off we have to simulate

146
00:07:07,070 --> 00:07:09,080
arrivals and we&#39;re just going to use a

147
00:07:09,080 --> 00:07:10,880
normal distribution since we decided

148
00:07:10,880 --> 00:07:13,640
that&#39;s a pretty good approximation here

149
00:07:13,640 --> 00:07:15,200
and we&#39;ll we&#39;ll print some summary

150
00:07:15,200 --> 00:07:17,660
statistics of these we&#39;re going to

151
00:07:17,660 --> 00:07:21,260
compute our profits distribution of

152
00:07:21,260 --> 00:07:25,160
profits in our total profit because we

153
00:07:25,160 --> 00:07:28,280
said that the profit for the each couple

154
00:07:28,280 --> 00:07:30,680
lemonade berries with the price were

155
00:07:30,680 --> 00:07:32,360
charging which varies with the weather

156
00:07:32,360 --> 00:07:35,180
so all customers arriving on a given day

157
00:07:35,180 --> 00:07:37,580
pay the same and our prophet is the same

158
00:07:37,580 --> 00:07:40,430
so we can simply say total profit is

159
00:07:40,430 --> 00:07:42,560
just the arrivals on that day times the

160
00:07:42,560 --> 00:07:44,540
profit for whatever we&#39;re able to charge

161
00:07:44,540 --> 00:07:46,850
on that particular day we&#39;re doing the

162
00:07:46,850 --> 00:07:49,940
same approximation here on the tips

163
00:07:49,940 --> 00:07:52,700
maybe it&#39;s not quite so realistic but

164
00:07:52,700 --> 00:07:54,740
we&#39;re just saying on a given day all

165
00:07:54,740 --> 00:07:56,210
customers are going to give us the same

166
00:07:56,210 --> 00:07:59,330
tips and then our total take for the day

167
00:07:59,330 --> 00:08:01,880
is our profit on selling the lemonade

168
00:08:01,880 --> 00:08:03,230
plus our tips

169
00:08:03,230 --> 00:08:06,710
that&#39;s all there is to it so let me run

170
00:08:06,710 --> 00:08:10,420
this and we&#39;ll look at the results

171
00:08:14,540 --> 00:08:17,160
on the right things will go better

172
00:08:17,160 --> 00:08:20,220
alright so here&#39;s our rivals of

173
00:08:20,220 --> 00:08:22,230
customers per day with an expected value

174
00:08:22,230 --> 00:08:25,080
600 no surprise there we already looked

175
00:08:25,080 --> 00:08:28,710
at that distribution quite a lot

176
00:08:28,710 --> 00:08:30,540
likewise we looked at this distribution

177
00:08:30,540 --> 00:08:32,520
of profits for arrival

178
00:08:32,520 --> 00:08:36,510
no surprise there but so on a given day

179
00:08:36,510 --> 00:08:39,690
whatever our prophet is we have this

180
00:08:39,690 --> 00:08:42,120
distribution of customer arrivals

181
00:08:42,120 --> 00:08:46,790
here&#39;s what our profits look like

182
00:08:46,830 --> 00:08:48,930
the realizations of profits across

183
00:08:48,930 --> 00:08:52,710
10,000 days hundred thousand days notice

184
00:08:52,710 --> 00:08:56,040
we now have a3 hunt distribution here so

185
00:08:56,040 --> 00:08:58,680
this would be a little bit difficult to

186
00:08:58,680 --> 00:09:03,150
take on analytically at least messy and

187
00:09:03,150 --> 00:09:05,490
yet with a simulation we easily get to

188
00:09:05,490 --> 00:09:08,460
this and we can visualize it and we can

189
00:09:08,460 --> 00:09:12,620
look at some summary statistics about it

190
00:09:12,760 --> 00:09:15,640
like we can say on 75 percent of the

191
00:09:15,640 --> 00:09:17,470
days we&#39;re going to have a profit of at

192
00:09:17,470 --> 00:09:20,710
least 2859 so it&#39;s probably somewhere

193
00:09:20,710 --> 00:09:22,510
out here

194
00:09:22,510 --> 00:09:26,230
etc so then here&#39;s our tip distribution

195
00:09:26,230 --> 00:09:30,360
looks the same as what we expect it

196
00:09:30,360 --> 00:09:32,519
and again we have customer rivals on the

197
00:09:32,519 --> 00:09:36,269
day where we get different tips and you

198
00:09:36,269 --> 00:09:40,050
see this funny set of four humps now 0

199
00:09:40,050 --> 00:09:41,760
times whatever the number of arrivals

200
00:09:41,760 --> 00:09:43,589
are on the day when the customers are in

201
00:09:43,589 --> 00:09:45,810
a bad mood and don&#39;t give us any tips is

202
00:09:45,810 --> 00:09:47,579
still zero so that&#39;s why we just get a

203
00:09:47,579 --> 00:09:50,519
spike there at zero and we add those

204
00:09:50,519 --> 00:09:53,220
together the tips and the prophets and

205
00:09:53,220 --> 00:09:55,290
we get this pretty complicated

206
00:09:55,290 --> 00:09:57,660
distribution which arguably has 5 humps

207
00:09:57,660 --> 00:10:00,000
there&#39;s a little hump here big hump

208
00:10:00,000 --> 00:10:03,660
another big hump hump hump

209
00:10:03,660 --> 00:10:07,800
so this would this is pretty complicated

210
00:10:07,800 --> 00:10:10,200
but yet through these few lines of code

211
00:10:10,200 --> 00:10:12,779
in this simulation we get there

212
00:10:12,779 --> 00:10:15,450
and finally you can see our mean profit

213
00:10:15,450 --> 00:10:20,579
per day is about 2758 our median profit

214
00:10:20,579 --> 00:10:25,860
is about is 2665 our best days

215
00:10:25,860 --> 00:10:27,360
twenty-five percent of our best days

216
00:10:27,360 --> 00:10:32,860
we&#39;re going to make 3102 as our profit

217
00:10:32,860 --> 00:10:36,010
so I hope that gives you some idea of

218
00:10:36,010 --> 00:10:40,210
how you can fit together fairly

219
00:10:40,210 --> 00:10:42,940
complicated scenario that might be hard

220
00:10:42,940 --> 00:10:44,800
to deal with analytically and yet using

221
00:10:44,800 --> 00:10:47,740
simulation you get very clear and

222
00:10:47,740 --> 00:10:52,090
insightful insight into the behavior of

223
00:10:52,090 --> 00:10:54,190
this system in this case our

224
00:10:54,190 --> 00:10:58,140
profitability of our lemonade stand

