0
00:00:05,009 --> 00:00:07,618
Okay so now that you think you know something about hypothesis

1
00:00:07,618 --> 00:00:09,030
testing, it&#39;s time for a quiz.

2
00:00:10,240 --> 00:00:12,671
So this is a quiz on misconceptions about hypothesis

3
00:00:12,671 --> 00:00:15,436
testing that&#39;s based on the document that cannot recently

4
00:00:15,436 --> 00:00:17,608
from the American Statistical Association.

5
00:00:20,542 --> 00:00:25,607
True or false, p-values indicate how incompatible

6
00:00:25,607 --> 00:00:29,893
data are with a specified model of the world.

7
00:00:33,480 --> 00:00:34,580
True or false?

8
00:00:36,430 --> 00:00:39,830
And of course the answer is true.

9
00:00:41,120 --> 00:00:44,755
It&#39;s how incompatible your data are with the null hypothesis.

10
00:00:47,502 --> 00:00:49,908
Next question.

11
00:00:49,908 --> 00:00:54,391
A p-value measures the probability that the null

12
00:00:54,391 --> 00:00:56,318
hypothesis is true.

13
00:00:58,906 --> 00:01:00,531
True or false?

14
00:01:02,890 --> 00:01:07,642
No way, p-value measures the probability to observe

15
00:01:07,642 --> 00:01:09,707
something as extreme or

16
00:01:09,707 --> 00:01:15,580
more extreme than what you did under the null hypothesis.

17
00:01:15,580 --> 00:01:20,284
I&#39;ll repeat that again, p-value measures the probability that

18
00:01:20,284 --> 00:01:22,972
the probability to observe something

19
00:01:22,972 --> 00:01:26,920
as extreme as what you did under the null hypothesis does

20
00:01:26,920 --> 00:01:31,389
not measure the probability that the null hypothesis is true.

21
00:01:35,333 --> 00:01:36,991
True or false?

22
00:01:36,991 --> 00:01:41,913
P-value measures the probability that the data were produced by

23
00:01:41,913 --> 00:01:43,621
random chance alone.

24
00:01:49,486 --> 00:01:51,028
And the answer is clearly false.

25
00:01:51,028 --> 00:01:54,256
I have no idea what it means, random chance alone.

26
00:01:54,256 --> 00:01:55,202
Right, that&#39;s not it.

27
00:01:55,202 --> 00:01:57,780
The p-value has got to be thought of with respect

28
00:01:57,780 --> 00:02:01,740
to the null hypothesis, which is a probability distribution.

29
00:02:02,750 --> 00:02:04,980
Random chance alone to me means nothing.

30
00:02:08,158 --> 00:02:12,945
True or false, a p-value below 0.05 is sufficient to base

31
00:02:12,945 --> 00:02:16,739
scientific conclusions or business decisions or

32
00:02:16,739 --> 00:02:18,287
policy decisions.

33
00:02:21,589 --> 00:02:27,047
And in general, the answer to that is false.

34
00:02:27,047 --> 00:02:30,365
There&#39;s nothing special about that 0.05, right?

35
00:02:30,365 --> 00:02:32,387
It depends how the study is done and

36
00:02:32,387 --> 00:02:36,168
whether there&#39;s enough other evidence to make that decision.

37
00:02:36,168 --> 00:02:38,361
The p-value is a piece of evidence.

38
00:02:38,361 --> 00:02:41,566
Assuming that somebody didn&#39;t manufacture the data to get

39
00:02:41,566 --> 00:02:44,010
the p-value they wanted, which people do.

40
00:02:44,010 --> 00:02:47,108
Obviously, this is totally unethical, but

41
00:02:47,108 --> 00:02:48,579
people do it anyway.

42
00:02:48,579 --> 00:02:51,157
So, anyway, the p-value is one piece of evidence.

43
00:02:54,611 --> 00:02:56,339
True or false?

44
00:02:56,339 --> 00:03:00,582
A p-value measures how big an effect is.

45
00:03:00,582 --> 00:03:04,909
A small p-value means a large effect.

46
00:03:07,773 --> 00:03:09,208
What do you think?

47
00:03:10,552 --> 00:03:12,618
No way.

48
00:03:12,618 --> 00:03:17,670
Okay, so here&#39;s an example, n=10 million.

49
00:03:17,670 --> 00:03:19,800
I have 10 million students.

50
00:03:19,800 --> 00:03:21,580
Say they all go to a tutoring session.

51
00:03:22,830 --> 00:03:28,139
The tutoring session improved their score

52
00:03:28,139 --> 00:03:33,153
over the population mean by only say five

53
00:03:33,153 --> 00:03:38,475
hundredths of a point, five hundredths.

54
00:03:38,475 --> 00:03:43,311
Let&#39;s say that the population mean is 80 and

55
00:03:43,311 --> 00:03:48,767
after they go to the tutoring session, the the score

56
00:03:48,767 --> 00:03:54,974
of the students becomes only slightly, slightly higher.

57
00:03:54,974 --> 00:03:59,031
And I could calculate the p-value for testing whether

58
00:03:59,031 --> 00:04:03,000
the tutoring session improves their score above 0,

59
00:04:03,000 --> 00:04:06,810
and I might get that the p-value is 0.0001.

60
00:04:06,810 --> 00:04:09,100
So highly significant.

61
00:04:09,100 --> 00:04:12,115
I&#39;m really sure that the tutoring sessions improved

62
00:04:12,115 --> 00:04:12,920
their score.

63
00:04:12,920 --> 00:04:15,543
But whoop dee do, yeah, it improved their score, and

64
00:04:15,543 --> 00:04:16,693
I&#39;m really sure of it.

65
00:04:16,692 --> 00:04:19,111
But it only improved the score by a tiny little itty

66
00:04:19,111 --> 00:04:19,750
bitty drop.

67
00:04:20,810 --> 00:04:22,880
Okay, so don&#39;t get fooled by this one.

68
00:04:22,880 --> 00:04:27,641
The p-value is not the size of the effect, no way, no how,

69
00:04:27,641 --> 00:04:28,315
false.

70
00:04:32,034 --> 00:04:36,930
True or false, a p-value tells you how important a result is.

71
00:04:38,590 --> 00:04:42,580
And the answer to this one should be obvious.

72
00:04:42,580 --> 00:04:45,508
If it doesn&#39;t tell you how big the effect is,

73
00:04:45,508 --> 00:04:48,908
then how can it tell you how important the result is?

74
00:04:52,455 --> 00:04:56,529
The p-value is the first line of defense against being fooled by

75
00:04:56,529 --> 00:04:58,330
random chance.

76
00:04:58,330 --> 00:05:00,184
They&#39;re very helpful and

77
00:05:00,184 --> 00:05:02,800
I suggest you use them with caution.

