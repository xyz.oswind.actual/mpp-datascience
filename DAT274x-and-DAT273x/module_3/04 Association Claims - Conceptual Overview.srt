0
00:00:01,670 --> 00:00:04,705
>> Now, I want to transition and talk about

1
00:00:04,705 --> 00:00:07,390
a slightly more sophisticated research claim.

2
00:00:07,390 --> 00:00:09,820
Now, this is one of the most common research claims

3
00:00:09,820 --> 00:00:11,110
we can possibly make.

4
00:00:11,110 --> 00:00:13,600
It's probably the vast majority of research claims you're

5
00:00:13,600 --> 00:00:16,360
going to encounter either as a consumer of research,

6
00:00:16,360 --> 00:00:17,890
say reading the news or even

7
00:00:17,890 --> 00:00:20,200
paying attention to the world of science,

8
00:00:20,200 --> 00:00:23,895
or just simply processing and analyzing data.

9
00:00:23,895 --> 00:00:26,300
This is called the association claim.

10
00:00:26,300 --> 00:00:28,270
The association claim is very

11
00:00:28,270 --> 00:00:30,700
simple and it is by far the most common.

12
00:00:30,700 --> 00:00:32,380
You are looking at links between

13
00:00:32,380 --> 00:00:34,295
two or more variables and you're just

14
00:00:34,295 --> 00:00:36,775
describing some relationship between them.

15
00:00:36,775 --> 00:00:38,770
That's really all that it is.

16
00:00:38,770 --> 00:00:40,915
Imagine the situation on the left here.

17
00:00:40,915 --> 00:00:42,370
We could be considering

18
00:00:42,370 --> 00:00:46,470
the engagement levels of employees and their performance.

19
00:00:46,470 --> 00:00:48,040
I've already used this example

20
00:00:48,040 --> 00:00:49,820
with you but we're repeating again,

21
00:00:49,820 --> 00:00:51,430
at least in this sample,

22
00:00:51,430 --> 00:00:52,750
the more engaged employees

23
00:00:52,750 --> 00:00:54,420
tend to be the higher performing one.

24
00:00:54,420 --> 00:00:56,140
That's pretty much all you

25
00:00:56,140 --> 00:00:58,270
can say with an association claim.

26
00:00:58,270 --> 00:01:01,010
As one goes up the other tends to change too.

27
00:01:01,010 --> 00:01:03,870
They covary, they correlate.

28
00:01:03,870 --> 00:01:05,200
For that reason, we often

29
00:01:05,200 --> 00:01:06,895
call this correlational research.

30
00:01:06,895 --> 00:01:09,350
We're showing the links between variables.

31
00:01:09,350 --> 00:01:12,100
Now, this can be a very useful tool.

32
00:01:12,100 --> 00:01:13,510
After all, if I know

33
00:01:13,510 --> 00:01:14,920
the more engaged employees

34
00:01:14,920 --> 00:01:16,360
tend to be the higher performing,

35
00:01:16,360 --> 00:01:18,820
that suggests to me I might want

36
00:01:18,820 --> 00:01:21,850
to consider helping my employees be engaged.

37
00:01:21,850 --> 00:01:25,170
However, there are some cautionary notes we should make.

38
00:01:25,170 --> 00:01:28,685
Yes, we're finding a link between two or more variables,

39
00:01:28,685 --> 00:01:30,930
but it's not showing causation.

40
00:01:30,930 --> 00:01:32,570
Of course, there is

41
00:01:32,570 --> 00:01:34,360
a common mantra, you may have heard it,

42
00:01:34,360 --> 00:01:36,795
correlation is not causation

43
00:01:36,795 --> 00:01:39,410
or correlation does not equal causation,

44
00:01:39,410 --> 00:01:43,005
but it bears repeating in this case.

45
00:01:43,005 --> 00:01:44,940
I don't actually know

46
00:01:44,940 --> 00:01:47,025
that engagement is driving performance.

47
00:01:47,025 --> 00:01:49,260
In fact, there are multiple reasons why

48
00:01:49,260 --> 00:01:52,230
engagement and performance might be related here.

49
00:01:52,230 --> 00:01:55,250
On one hand, engagement could be driving performance,

50
00:01:55,250 --> 00:01:59,240
true, but I guess I don't really know which comes first.

51
00:01:59,240 --> 00:02:01,695
In fact, isn't it entirely possible that

52
00:02:01,695 --> 00:02:03,030
high performance leads to

53
00:02:03,030 --> 00:02:06,320
more engagement? Think about it.

54
00:02:06,320 --> 00:02:08,600
If an employee is performing well,

55
00:02:08,600 --> 00:02:10,580
they're feeling really satisfied in their jobs,

56
00:02:10,580 --> 00:02:12,040
they're feeling really good about that,

57
00:02:12,040 --> 00:02:13,490
that's a positive thing in

58
00:02:13,490 --> 00:02:16,310
their life that might make it easier for them to engage.

59
00:02:16,310 --> 00:02:17,950
So if that's the case,

60
00:02:17,950 --> 00:02:19,490
helping people engage more might

61
00:02:19,490 --> 00:02:20,820
do nothing for performance.

62
00:02:20,820 --> 00:02:22,610
I could invest a considerable amount of

63
00:02:22,610 --> 00:02:25,290
money in helping people be more engaged.

64
00:02:25,290 --> 00:02:27,360
Obviously, that sounds great, don't

65
00:02:27,360 --> 00:02:28,640
we all want to be engaged?

66
00:02:28,640 --> 00:02:32,695
But from a performance perspective that might not help.

67
00:02:32,695 --> 00:02:34,850
So just seeing this link,

68
00:02:34,850 --> 00:02:37,430
I should be really careful about which I assume is

69
00:02:37,430 --> 00:02:40,325
the cause and which I assume is the effect.

70
00:02:40,325 --> 00:02:42,980
Just because I've put engagement on the bottom of

71
00:02:42,980 --> 00:02:45,985
my graph does not mean that it caused the other one.

72
00:02:45,985 --> 00:02:47,970
That's not even the half of it,

73
00:02:47,970 --> 00:02:49,640
there's a third possibility.

74
00:02:49,640 --> 00:02:52,340
We sometimes call this the third variable problem,

75
00:02:52,340 --> 00:02:54,350
and it's an even worse consideration.

76
00:02:54,350 --> 00:02:57,225
What if these are both caused by something entirely

77
00:02:57,225 --> 00:03:00,540
unrelated to the variables that I have on here,

78
00:03:00,540 --> 00:03:02,080
something conceptually different,

79
00:03:02,080 --> 00:03:04,475
something that drives both of them?

80
00:03:04,475 --> 00:03:06,530
For example, what if

81
00:03:06,530 --> 00:03:08,680
this is entirely an effect of salary?

82
00:03:08,680 --> 00:03:10,880
What if I find that higher salary employees

83
00:03:10,880 --> 00:03:12,985
are both more engaged and perform better?

84
00:03:12,985 --> 00:03:14,340
Or maybe it's not salary,

85
00:03:14,340 --> 00:03:16,100
maybe it's workplace conditions or

86
00:03:16,100 --> 00:03:18,650
some aspect of the work itself?

87
00:03:18,650 --> 00:03:21,200
If that's the case, this relationship

88
00:03:21,200 --> 00:03:22,879
is not causal at all,

89
00:03:22,879 --> 00:03:24,260
it is purely an artifact

90
00:03:24,260 --> 00:03:26,150
of something else that I didn't measure.

91
00:03:26,150 --> 00:03:29,665
This finding is actually really common.

92
00:03:29,665 --> 00:03:31,570
Just because two things are correlated,

93
00:03:31,570 --> 00:03:33,635
we often assume one is causing the other.

94
00:03:33,635 --> 00:03:34,940
But when you think of all of

95
00:03:34,940 --> 00:03:37,525
the possible cause-effect possibilities,

96
00:03:37,525 --> 00:03:40,130
there's probably an unending number of

97
00:03:40,130 --> 00:03:42,645
things that could be causing either of these variables.

98
00:03:42,645 --> 00:03:45,320
So, I don't really know what's causing what.

99
00:03:45,320 --> 00:03:48,630
So, association claims are great for describing

100
00:03:48,630 --> 00:03:50,220
trends but you shouldn't draw

101
00:03:50,220 --> 00:03:52,005
a causal conclusion from them.

102
00:03:52,005 --> 00:03:53,520
In fact, although it might be

103
00:03:53,520 --> 00:03:55,500
tempting to draw a causal conclusion from them,

104
00:03:55,500 --> 00:03:57,900
I would suggest if it's really important you

105
00:03:57,900 --> 00:04:01,045
consider doing another research study.

106
00:04:01,045 --> 00:04:02,805
For instance, maybe we could try

107
00:04:02,805 --> 00:04:05,220
experimentally trying to improve

108
00:04:05,220 --> 00:04:07,030
engagement and see if performance changes.

109
00:04:07,030 --> 00:04:09,950
Then we're not doing causal association claims anymore,

110
00:04:09,950 --> 00:04:11,750
they we're trying to go onto a different kind of

111
00:04:11,750 --> 00:04:13,920
claim which we'll get to in a future lesson.

112
00:04:13,920 --> 00:04:17,360
So association claims, links between variables,

113
00:04:17,360 --> 00:04:20,385
describes trends but does not describe causation,

114
00:04:20,385 --> 00:04:22,935
and you should take a very big note of caution away from

115
00:04:22,935 --> 00:04:24,870
any association claim if

116
00:04:24,870 --> 00:04:28,120
someone tries to make a causal conclusion.

