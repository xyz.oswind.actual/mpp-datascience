0
00:00:01,820 --> 00:00:04,120
>> In this lesson I want to give you

1
00:00:04,120 --> 00:00:06,100
some cautionary notes and help

2
00:00:06,100 --> 00:00:09,755
you to properly make sense of frequency claims.

3
00:00:09,755 --> 00:00:11,995
So, we've previously said that

4
00:00:11,995 --> 00:00:14,340
we have some data on coffee consumption,

5
00:00:14,340 --> 00:00:15,700
and we find that the average

6
00:00:15,700 --> 00:00:17,710
depending on which you use, median or mean,

7
00:00:17,710 --> 00:00:19,900
it's about one, one and a third cups of

8
00:00:19,900 --> 00:00:22,440
coffee per day for this sample.

9
00:00:22,440 --> 00:00:24,865
So, how do we make sense of that?

10
00:00:24,865 --> 00:00:28,135
There are a couple of cautionary note I want to give you.

11
00:00:28,135 --> 00:00:31,120
The first, as I see this a lot is

12
00:00:31,120 --> 00:00:32,620
people want to slice and dice

13
00:00:32,620 --> 00:00:34,240
this into a bunch of subgroups.

14
00:00:34,240 --> 00:00:35,950
So Bin might say things like, "Well,

15
00:00:35,950 --> 00:00:38,170
20 percent of people consume three or more cups of

16
00:00:38,170 --> 00:00:40,680
coffee per day." That's fine.

17
00:00:40,680 --> 00:00:42,880
In fact, they even suggest doing that,

18
00:00:42,880 --> 00:00:44,350
but you want to be really,

19
00:00:44,350 --> 00:00:45,970
really careful that you label

20
00:00:45,970 --> 00:00:48,350
that as sample-specific number.

21
00:00:48,350 --> 00:00:50,060
Because think about it, that's

22
00:00:50,060 --> 00:00:52,375
20 percent in your sample, right?

23
00:00:52,375 --> 00:00:55,450
But you're now basing this on a subsample of your data.

24
00:00:55,450 --> 00:00:57,070
You don't have all of

25
00:00:57,070 --> 00:00:59,465
your participants in that 20 percent,

26
00:00:59,465 --> 00:01:00,870
maybe you only have 100 people,

27
00:01:00,870 --> 00:01:01,990
that's only 20 people.

28
00:01:01,990 --> 00:01:05,045
That is a smaller sample of people than you started with.

29
00:01:05,045 --> 00:01:07,030
As you think about power concerns

30
00:01:07,030 --> 00:01:08,620
for the overall study, well,

31
00:01:08,620 --> 00:01:10,780
the minute you start subdividing it

32
00:01:10,780 --> 00:01:13,225
your power concerns become much more pronounced.

33
00:01:13,225 --> 00:01:14,920
So I just want to caution

34
00:01:14,920 --> 00:01:17,115
you that if you're going to subdivide it,

35
00:01:17,115 --> 00:01:19,180
you put a few asterisks

36
00:01:19,180 --> 00:01:21,330
of caution like that on that result.

37
00:01:21,330 --> 00:01:22,990
20 percent of people in

38
00:01:22,990 --> 00:01:27,860
the sample consume three or more cups of coffee per day.

39
00:01:28,930 --> 00:01:32,665
What would that be in the population? We don't know.

40
00:01:32,665 --> 00:01:34,620
We could put our confidence interval

41
00:01:34,620 --> 00:01:35,920
around any of these numbers,

42
00:01:35,920 --> 00:01:37,210
be at that 20 percent,

43
00:01:37,210 --> 00:01:40,240
be it on the average, and that might be a good option.

44
00:01:40,240 --> 00:01:42,390
See, beware the false certainty

45
00:01:42,390 --> 00:01:43,700
of working with the sample.

46
00:01:43,700 --> 00:01:45,840
There's a reason I focused with you so

47
00:01:45,840 --> 00:01:48,360
much on the dangers and pitfalls of

48
00:01:48,360 --> 00:01:53,190
using samples because samples are just guesses.

49
00:01:53,190 --> 00:01:56,770
The population mean is not 1.35 cups of coffee.

50
00:01:56,770 --> 00:01:59,655
There is a mean in the population, and we don't have it.

51
00:01:59,655 --> 00:02:01,830
We are estimating that mean using

52
00:02:01,830 --> 00:02:04,470
our sample mean which is great and fine and good,

53
00:02:04,470 --> 00:02:07,350
but I actually don't know what the population mean is.

54
00:02:07,350 --> 00:02:10,275
So, whether it's around a percentage in some bin

55
00:02:10,275 --> 00:02:11,995
or space of this histogram

56
00:02:11,995 --> 00:02:13,739
or whether it's just around the average,

57
00:02:13,739 --> 00:02:16,035
I recommend putting a confidence interval.

58
00:02:16,035 --> 00:02:18,400
For instance, if my 95 percent

59
00:02:18,400 --> 00:02:20,405
confidence interval was between,

60
00:02:20,405 --> 00:02:22,785
about 1.3 and 1.4,

61
00:02:22,785 --> 00:02:25,405
there abouts, "Wow, you know what?

62
00:02:25,405 --> 00:02:27,645
I feel pretty confident in my result.

63
00:02:27,645 --> 00:02:30,740
I could be 95 percent confident the true mean is

64
00:02:30,740 --> 00:02:33,870
somewhere between 1.27 and 1.42.

65
00:02:33,870 --> 00:02:35,610
Great. I trust my result,

66
00:02:35,610 --> 00:02:36,960
I know that my guess is

67
00:02:36,960 --> 00:02:39,215
somewhere in the neighborhood of the right answer."

68
00:02:39,215 --> 00:02:40,830
On the other hand, if I had

69
00:02:40,830 --> 00:02:43,705
a really wide confidence interval, say,

70
00:02:43,705 --> 00:02:45,270
somewhere between half a cup of

71
00:02:45,270 --> 00:02:48,095
coffee and two and a quarter cups of coffee,

72
00:02:48,095 --> 00:02:50,340
well, that's a really big range.

73
00:02:50,340 --> 00:02:52,750
The first is based on a large sample,

74
00:02:52,750 --> 00:02:55,785
high precision, high power, good estimate.

75
00:02:55,785 --> 00:02:58,040
Second is based on a small sample,

76
00:02:58,040 --> 00:03:00,450
low precision, low power,

77
00:03:00,450 --> 00:03:02,915
and so the estimate is less good.

78
00:03:02,915 --> 00:03:05,100
So, think about that. Think about that.

79
00:03:05,100 --> 00:03:06,630
Think about putting a confidence

80
00:03:06,630 --> 00:03:07,990
interval around your result.

81
00:03:07,990 --> 00:03:09,360
If you told me

82
00:03:09,360 --> 00:03:12,185
the bottom confidence interval, I'm going to tell you,

83
00:03:12,185 --> 00:03:15,030
"Great, I know the average coffee consumption

84
00:03:15,030 --> 00:03:17,295
is somewhere between a half and two."

85
00:03:17,295 --> 00:03:19,140
If I'm forecasting or making

86
00:03:19,140 --> 00:03:21,860
decisions based on that coffee count,

87
00:03:21,860 --> 00:03:23,640
I don't trust that very much.

88
00:03:23,640 --> 00:03:26,610
In fact, I might rather redo the study

89
00:03:26,610 --> 00:03:28,050
with a larger sample to get

90
00:03:28,050 --> 00:03:29,850
that tight confidence interval.

91
00:03:29,850 --> 00:03:32,790
So, beware the false certainty that comes from that.

92
00:03:32,790 --> 00:03:35,040
Think about this situation. What have I told you?

93
00:03:35,040 --> 00:03:37,440
I'm 95 percent confident the number of

94
00:03:37,440 --> 00:03:38,640
cups of coffee consumed is

95
00:03:38,640 --> 00:03:40,935
somewhere between zero and five.

96
00:03:40,935 --> 00:03:44,165
I could've told you that without doing the study.

97
00:03:44,165 --> 00:03:45,830
So these are some things that you might think

98
00:03:45,830 --> 00:03:47,975
about, some cautionary notes.

99
00:03:47,975 --> 00:03:50,325
A couple other cautionary notes for you.

100
00:03:50,325 --> 00:03:54,045
There is something researchers call external validity.

101
00:03:54,045 --> 00:03:56,120
That is the degree to which the result can actually

102
00:03:56,120 --> 00:03:58,975
generalize to the world outside of your study.

103
00:03:58,975 --> 00:04:01,870
We aren't just doing research for the heck of it,

104
00:04:01,870 --> 00:04:04,140
we want to be able to generalize our result

105
00:04:04,140 --> 00:04:07,785
up to the population to the outside world.

106
00:04:07,785 --> 00:04:10,060
If we want to do that, it's important that

107
00:04:10,060 --> 00:04:12,730
our sample be highly representative of the outside world.

108
00:04:12,730 --> 00:04:13,990
So, think about where you're

109
00:04:13,990 --> 00:04:15,420
getting your participants from.

110
00:04:15,420 --> 00:04:16,510
Are you getting them from

111
00:04:16,510 --> 00:04:18,455
an online social networking site?

112
00:04:18,455 --> 00:04:20,535
Okay, maybe that's fine,

113
00:04:20,535 --> 00:04:23,320
but is that online social networking site representative

114
00:04:23,320 --> 00:04:26,675
of the population you want to describe?

115
00:04:26,675 --> 00:04:28,600
Is your sample even representative of

116
00:04:28,600 --> 00:04:30,400
people on that networking site,

117
00:04:30,400 --> 00:04:33,420
or are you relying on people to select into the study?

118
00:04:33,420 --> 00:04:35,500
We might need to think about

119
00:04:35,500 --> 00:04:37,900
these things and take them into consideration

120
00:04:37,900 --> 00:04:41,500
because you can very accurately describe a population of

121
00:04:41,500 --> 00:04:43,540
people who accept a free survey offer

122
00:04:43,540 --> 00:04:45,490
which might not be anybody you care about,

123
00:04:45,490 --> 00:04:47,650
that might not be a population you want to describe.

124
00:04:47,650 --> 00:04:48,925
So you should really think

125
00:04:48,925 --> 00:04:50,820
about where you're getting your data.

126
00:04:50,820 --> 00:04:52,360
The best data is

127
00:04:52,360 --> 00:04:54,785
and I hate to say this, but mandatory data,

128
00:04:54,785 --> 00:04:57,340
data that you're going to have on everybody or that would

129
00:04:57,340 --> 00:04:59,005
accurately describe the entire

130
00:04:59,005 --> 00:05:00,665
population you want to describe.

131
00:05:00,665 --> 00:05:03,160
So for instance, you might consider having

132
00:05:03,160 --> 00:05:06,160
a survey question put in a product registration form

133
00:05:06,160 --> 00:05:08,410
if you're wanting the survey registered product owner

134
00:05:08,410 --> 00:05:10,145
or something like that where you're going to be

135
00:05:10,145 --> 00:05:12,700
able to get data on everybody or at least a

136
00:05:12,700 --> 00:05:14,680
representative group of the customers

137
00:05:14,680 --> 00:05:16,340
or people you're interested in knowing about.

138
00:05:16,340 --> 00:05:19,150
So, just keep this in mind that you're going to be

139
00:05:19,150 --> 00:05:22,780
estimating whatever the population your sample is.

140
00:05:22,780 --> 00:05:26,360
So, just keep in mind where your data come from.

141
00:05:26,360 --> 00:05:27,880
If you're approaching people at

142
00:05:27,880 --> 00:05:29,965
random in a grocery store,

143
00:05:29,965 --> 00:05:31,300
doing a little intercept study,

144
00:05:31,300 --> 00:05:32,150
where you intercept people,

145
00:05:32,150 --> 00:05:33,040
you give them a little survey,

146
00:05:33,040 --> 00:05:35,050
that might actually be more representative

147
00:05:35,050 --> 00:05:37,060
than an online survey because at least you

148
00:05:37,060 --> 00:05:38,800
can make sure you are approaching

149
00:05:38,800 --> 00:05:41,735
people from different characteristics and demographics,

150
00:05:41,735 --> 00:05:43,990
assuming that you've got a good enough offer that

151
00:05:43,990 --> 00:05:46,120
people don't generally shoot you down.

152
00:05:46,120 --> 00:05:47,560
I will say this isn't always

153
00:05:47,560 --> 00:05:49,420
a problem depending on what you're studying.

154
00:05:49,420 --> 00:05:50,190
If you're studying something

155
00:05:50,190 --> 00:05:51,760
that's universal, for instance,

156
00:05:51,760 --> 00:05:53,230
a researcher might want to know what

157
00:05:53,230 --> 00:05:54,990
people's memory capacity is,

158
00:05:54,990 --> 00:05:57,895
probably doesn't differ depending on who you ask.

159
00:05:57,895 --> 00:05:59,650
People's memory capacity might

160
00:05:59,650 --> 00:06:01,420
be more fixed by their biology.

161
00:06:01,420 --> 00:06:03,730
If you're asking and assessing the effect

162
00:06:03,730 --> 00:06:06,365
of coffee on people's physiology for instance,

163
00:06:06,365 --> 00:06:10,350
probably, generally consistent across subpopulations,

164
00:06:10,350 --> 00:06:12,620
so then we don't know that without studying it.

165
00:06:12,620 --> 00:06:14,665
But still, probably a safe bet.

166
00:06:14,665 --> 00:06:16,450
But depending on what you're doing,

167
00:06:16,450 --> 00:06:17,755
if the thing you're studying

168
00:06:17,755 --> 00:06:20,560
varies considerably across populations you

169
00:06:20,560 --> 00:06:22,359
really need to pay attention

170
00:06:22,359 --> 00:06:24,520
to where you're getting your data from because you

171
00:06:24,520 --> 00:06:27,460
may get a very accurate answer about a Nish Group that

172
00:06:27,460 --> 00:06:29,020
doesn't have any relationship to

173
00:06:29,020 --> 00:06:30,845
the people you want to be able to describe.

174
00:06:30,845 --> 00:06:33,050
If I want to sell coffee I should

175
00:06:33,050 --> 00:06:35,320
make sure I survey coffee consumption habits,

176
00:06:35,320 --> 00:06:37,900
but I should also make sure I'm getting the people

177
00:06:37,900 --> 00:06:40,760
who I want to sell coffee to to be my sample.

178
00:06:40,760 --> 00:06:44,520
If I don't have that, then I've wasted my time.

