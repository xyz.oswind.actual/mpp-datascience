0
00:00:00,000 --> 00:00:02,280
>> So now you've done

1
00:00:02,280 --> 00:00:03,990
your data analysis and you have

2
00:00:03,990 --> 00:00:06,615
your association claim. Can you make of it?

3
00:00:06,615 --> 00:00:08,490
What kinds of information can we

4
00:00:08,490 --> 00:00:11,030
extract from that association claim?

5
00:00:11,030 --> 00:00:12,525
Well, there really isn't

6
00:00:12,525 --> 00:00:15,150
a lot that you need to know at this point because we've

7
00:00:15,150 --> 00:00:17,340
already talked about a number of the pieces but I

8
00:00:17,340 --> 00:00:19,950
want to highlight a couple of important themes.

9
00:00:19,950 --> 00:00:21,895
The first is again any time

10
00:00:21,895 --> 00:00:24,145
two variables are related, any time,

11
00:00:24,145 --> 00:00:26,190
whether it's group differences that you've

12
00:00:26,190 --> 00:00:28,570
seen, whether it's trendlines,

13
00:00:28,570 --> 00:00:30,995
whether it's percentage differences and crosstabs

14
00:00:30,995 --> 00:00:33,555
or a contentious table like we've just seen,

15
00:00:33,555 --> 00:00:35,550
any of those, okay, any of those,

16
00:00:35,550 --> 00:00:38,620
there's three reasons that association might exist.

17
00:00:38,620 --> 00:00:41,220
You need to carefully think through and I would even

18
00:00:41,220 --> 00:00:45,240
suggest point this out to anybody looking at the data.

19
00:00:45,240 --> 00:00:47,200
Reason number one, your X

20
00:00:47,200 --> 00:00:49,815
variable could cause your Y variable.

21
00:00:49,815 --> 00:00:52,110
Typically, I named my variables X

22
00:00:52,110 --> 00:00:54,060
and Y. I don't name them, they're not pets.

23
00:00:54,060 --> 00:00:55,340
But do you know I like to refer to

24
00:00:55,340 --> 00:00:57,050
one variable as the X variable.

25
00:00:57,050 --> 00:01:00,300
I think of it in my mind as the predictor or the cause,

26
00:01:00,300 --> 00:01:03,200
I've got the other variable I call it the Y variable,

27
00:01:03,200 --> 00:01:04,430
I think of it as the effect or

28
00:01:04,430 --> 00:01:06,815
the outcome, and that's good.

29
00:01:06,815 --> 00:01:09,170
In my brain I probably think about it

30
00:01:09,170 --> 00:01:12,050
this way that X is causing Y, probably.

31
00:01:12,050 --> 00:01:15,050
But that is really only one possibility.

32
00:01:15,050 --> 00:01:16,465
As we've talked about before,

33
00:01:16,465 --> 00:01:19,040
it is true that X could cause Y but

34
00:01:19,040 --> 00:01:21,725
there is nothing in my data that tells me that.

35
00:01:21,725 --> 00:01:23,630
In fact, Y could be causing X.

36
00:01:23,630 --> 00:01:24,920
It's entirely possible that it

37
00:01:24,920 --> 00:01:26,690
goes in the opposite direction.

38
00:01:26,690 --> 00:01:28,670
The variable that you thought of as

39
00:01:28,670 --> 00:01:30,820
the cause might actually be the effect.

40
00:01:30,820 --> 00:01:34,190
If I'm just looking at correlations or trends in my data,

41
00:01:34,190 --> 00:01:35,420
there's nothing that's going to help

42
00:01:35,420 --> 00:01:37,070
me make that decision.

43
00:01:37,070 --> 00:01:39,080
That's really on you,

44
00:01:39,080 --> 00:01:41,010
the researcher or the data analyst,

45
00:01:41,010 --> 00:01:43,640
the data scientist to interpret.

46
00:01:43,640 --> 00:01:45,700
Now you see again

47
00:01:45,700 --> 00:01:47,480
in the first module of this course why I

48
00:01:47,480 --> 00:01:48,975
push so hard for you to have

49
00:01:48,975 --> 00:01:50,870
good theory and good reasons.

50
00:01:50,870 --> 00:01:52,415
You really need to think through

51
00:01:52,415 --> 00:01:54,320
why you are looking at what you're looking

52
00:01:54,320 --> 00:01:56,030
for because if we have

53
00:01:56,030 --> 00:01:58,610
good reason to think that one is causing the other,

54
00:01:58,610 --> 00:02:00,650
it's a little bit of a safer bet to go with

55
00:02:00,650 --> 00:02:03,605
that than if we were just testing randomly.

56
00:02:03,605 --> 00:02:06,205
So again, the research is part of a process.

57
00:02:06,205 --> 00:02:07,550
The data analysis is part of

58
00:02:07,550 --> 00:02:09,875
a process and the data analyst,

59
00:02:09,875 --> 00:02:12,120
the person making the conclusions,

60
00:02:12,120 --> 00:02:15,365
they are the person who most needs to know that theory.

61
00:02:15,365 --> 00:02:17,300
Then again this is why you want to study

62
00:02:17,300 --> 00:02:18,695
research methods if you're doing

63
00:02:18,695 --> 00:02:21,300
any kind of analytics or data science work.

64
00:02:21,300 --> 00:02:24,425
Okay, so X could cause Y and Y could cause X.

65
00:02:24,425 --> 00:02:26,260
But again, we could have a third variable.

66
00:02:26,260 --> 00:02:28,255
There could be some other cause

67
00:02:28,255 --> 00:02:30,190
that leads to both of them.

68
00:02:30,190 --> 00:02:32,810
In this case, the link between X and Y could

69
00:02:32,810 --> 00:02:35,065
be entirely an artifact of something else,

70
00:02:35,065 --> 00:02:37,130
something that you didn't think to mention,

71
00:02:37,130 --> 00:02:38,405
something you didn't even think to

72
00:02:38,405 --> 00:02:40,770
include in the dataset.

73
00:02:40,770 --> 00:02:43,670
This is a problem.

74
00:02:43,670 --> 00:02:47,170
Partially because it's ruining

75
00:02:47,170 --> 00:02:48,940
your interpretation but it's also

76
00:02:48,940 --> 00:02:50,750
probably because it's the most common.

77
00:02:50,750 --> 00:02:52,180
What are the odds that of

78
00:02:52,180 --> 00:02:54,400
all the variables in the universe that you

79
00:02:54,400 --> 00:02:56,290
happen to include the two that

80
00:02:56,290 --> 00:02:58,925
cause each other in your sample?

81
00:02:58,925 --> 00:03:00,680
And when you go to analyze the data,

82
00:03:00,680 --> 00:03:02,740
there's nothing else that's causing either of

83
00:03:02,740 --> 00:03:04,150
those two things that

84
00:03:04,150 --> 00:03:06,205
could cause them to correlate to each other.

85
00:03:06,205 --> 00:03:08,125
Yeah. I'm going to guess the

86
00:03:08,125 --> 00:03:10,275
causal variable might be omitted.

87
00:03:10,275 --> 00:03:12,640
So, you have to have good theory.

88
00:03:12,640 --> 00:03:14,650
You have a good reason for looking at what you're

89
00:03:14,650 --> 00:03:17,215
looking for but you also need some humility.

90
00:03:17,215 --> 00:03:19,840
When you're analyzing correlated variables,

91
00:03:19,840 --> 00:03:21,850
you really need to take with a grain of

92
00:03:21,850 --> 00:03:24,615
salt any kind of calls or claim you might want to make.

93
00:03:24,615 --> 00:03:26,890
It's easy for us to assume we

94
00:03:26,890 --> 00:03:28,930
have the cause and effect relationship in

95
00:03:28,930 --> 00:03:30,940
our dataset because that's what's in front

96
00:03:30,940 --> 00:03:33,795
of us but that doesn't mean that's reality.

97
00:03:33,795 --> 00:03:36,325
Let's talk about some other caveats.

98
00:03:36,325 --> 00:03:38,980
Sometimes relationships are not linear.

99
00:03:38,980 --> 00:03:41,865
This is one of my favorite examples.

100
00:03:41,865 --> 00:03:46,180
You can look at people in a store and imagine you

101
00:03:46,180 --> 00:03:48,055
walk into a retail establishment

102
00:03:48,055 --> 00:03:50,500
and the staff comes up to you and says, "Hi.

103
00:03:50,500 --> 00:03:51,950
Can I help you find something?

104
00:03:51,950 --> 00:03:53,060
Is there anything I can do to help?"

105
00:03:53,060 --> 00:03:54,940
You think, "Oh. That sounds nice." All right.

106
00:03:54,940 --> 00:03:56,830
Then the next person asks you and you're like, "Okay.

107
00:03:56,830 --> 00:03:58,390
I kind of just want to do my shopping."

108
00:03:58,390 --> 00:04:00,440
Then the next person asks you and you think, "Wow!

109
00:04:00,440 --> 00:04:02,475
I'm being badgered at the store."

110
00:04:02,475 --> 00:04:04,750
Indeed, we might imagine that there is

111
00:04:04,750 --> 00:04:06,430
not a straight line relationship.

112
00:04:06,430 --> 00:04:09,585
If people are too eager to help you or not eager enough,

113
00:04:09,585 --> 00:04:11,580
either of those is unsatisfactory.

114
00:04:11,580 --> 00:04:13,480
Maybe there's a sweet spot for

115
00:04:13,480 --> 00:04:17,020
helpfulness that is both helpful but not annoying.

116
00:04:17,020 --> 00:04:19,440
We can do similar kinds of mental exercise.

117
00:04:19,440 --> 00:04:21,825
We can think about people's liking of music.

118
00:04:21,825 --> 00:04:23,715
A song plays on the radio,

119
00:04:23,715 --> 00:04:25,735
okay new song, you hear it a few times.

120
00:04:25,735 --> 00:04:26,870
Okay, it's kind of catchy, you're

121
00:04:26,870 --> 00:04:28,240
getting to like that song.

122
00:04:28,240 --> 00:04:29,630
You've heard the song enough times.

123
00:04:29,630 --> 00:04:30,940
Good. Keeps playing.

124
00:04:30,940 --> 00:04:32,310
Eventually, you get sick of it.

125
00:04:32,310 --> 00:04:34,810
There are plenty of relationships like this.

126
00:04:34,810 --> 00:04:36,125
Another great example;

127
00:04:36,125 --> 00:04:37,955
who tends to use the healthcare system?

128
00:04:37,955 --> 00:04:39,725
Young people and old people.

129
00:04:39,725 --> 00:04:41,920
People in the middle, not so much.

130
00:04:41,920 --> 00:04:43,480
So there are lots of

131
00:04:43,480 --> 00:04:45,760
relationships that when you stop to think about them,

132
00:04:45,760 --> 00:04:48,210
again have good theory and good hypotheses.

133
00:04:48,210 --> 00:04:50,160
We can't just run data science models.

134
00:04:50,160 --> 00:04:51,580
We have to actually think about

135
00:04:51,580 --> 00:04:54,310
things are not straight lines.

136
00:04:54,310 --> 00:04:55,960
In this case, if I were to actually

137
00:04:55,960 --> 00:04:58,985
run a straight line regression,

138
00:04:58,985 --> 00:05:01,300
I might not see anything.

139
00:05:01,300 --> 00:05:03,995
In fact, you could imagine in this data set here,

140
00:05:03,995 --> 00:05:08,040
I could fit a horizontal line through there but

141
00:05:08,040 --> 00:05:12,340
really truthfully this is a curvilinear relationship.

142
00:05:12,340 --> 00:05:13,470
So you need to think

143
00:05:13,470 --> 00:05:15,110
about that when you're running your models.

144
00:05:15,110 --> 00:05:17,250
Of course, again a lot of this

145
00:05:17,250 --> 00:05:19,695
comes down to theory; subject matter expertise.

146
00:05:19,695 --> 00:05:21,945
You need to actually think to look for this.

147
00:05:21,945 --> 00:05:23,325
It would be a mistake to test

148
00:05:23,325 --> 00:05:25,790
non-linear models in every analysis.

149
00:05:25,790 --> 00:05:27,030
After all, now we're going to come

150
00:05:27,030 --> 00:05:28,540
across a lot of false positives.

151
00:05:28,540 --> 00:05:31,895
So we don't do that either. You need good theory.

152
00:05:31,895 --> 00:05:34,500
Another caveat. What if

153
00:05:34,500 --> 00:05:36,275
we don't have access to all the data?

154
00:05:36,275 --> 00:05:38,040
Imagine, for example,

155
00:05:38,040 --> 00:05:40,530
that the customer satisfaction ratings from

156
00:05:40,530 --> 00:05:42,810
the previous slide were actually from

157
00:05:42,810 --> 00:05:45,390
people who left little complaint cards in a box.

158
00:05:45,390 --> 00:05:47,490
After all, let's think about it for a moment.

159
00:05:47,490 --> 00:05:50,070
How do you get customer satisfaction data?

160
00:05:50,070 --> 00:05:52,245
Often it's because people leave a complaint.

161
00:05:52,245 --> 00:05:54,090
So people are on their way out the store,

162
00:05:54,090 --> 00:05:55,470
they've got a little commment card.

163
00:05:55,470 --> 00:05:56,820
Sometimes they tell you if they're really

164
00:05:56,820 --> 00:05:58,155
satisfied but let's be honest.

165
00:05:58,155 --> 00:05:59,715
Often the people who fill those out

166
00:05:59,715 --> 00:06:01,740
are the unsatisfied customers.

167
00:06:01,740 --> 00:06:03,290
So this is actually the same data.

168
00:06:03,290 --> 00:06:05,125
I just chop the top off of the graph here.

169
00:06:05,125 --> 00:06:08,545
Here's the full graph. Let's just chop the top off.

170
00:06:08,545 --> 00:06:11,090
We don't have access to the middle part of the data.

171
00:06:11,090 --> 00:06:13,290
So if I fit a regression through this,

172
00:06:13,290 --> 00:06:14,960
actually the straight line fits

173
00:06:14,960 --> 00:06:16,605
well and it tells me there's

174
00:06:16,605 --> 00:06:20,470
no relationship between helpfulness and satisfaction.

175
00:06:20,470 --> 00:06:23,020
Imagine this problem. In fact,

176
00:06:23,020 --> 00:06:26,195
the entire reason those scores are low is due to

177
00:06:26,195 --> 00:06:28,365
the eagerness of the staff and

178
00:06:28,365 --> 00:06:31,530
yet I can't see it because I don't have all the data.

179
00:06:31,530 --> 00:06:34,435
There are plenty of examples like this.

180
00:06:34,435 --> 00:06:37,515
When you restrict the range of a variable,

181
00:06:37,515 --> 00:06:39,760
when you only have some of the data,

182
00:06:39,760 --> 00:06:42,300
the relationships can change a little bit.

183
00:06:42,300 --> 00:06:44,125
That's something to keep in mind.

184
00:06:44,125 --> 00:06:46,555
So think about where your data is coming from.

185
00:06:46,555 --> 00:06:47,950
There's nothing in your data

186
00:06:47,950 --> 00:06:48,910
that are going to tell you that

187
00:06:48,910 --> 00:06:51,390
these things are problems.

188
00:06:51,390 --> 00:06:53,530
You need to be aware of them because you've taken

189
00:06:53,530 --> 00:06:56,130
some research methods and statistics courses.

190
00:06:56,130 --> 00:06:57,790
You need to be aware of those so that you

191
00:06:57,790 --> 00:06:59,655
think to ask about these problems;

192
00:06:59,655 --> 00:07:01,015
how were the data sampled?

193
00:07:01,015 --> 00:07:02,485
Who are the participants?

194
00:07:02,485 --> 00:07:03,795
What was the source of the data?

195
00:07:03,795 --> 00:07:06,940
Is there any reason why I might not have access to

196
00:07:06,940 --> 00:07:08,620
all the data I would need to really

197
00:07:08,620 --> 00:07:11,280
tell the story of this relationship?

198
00:07:11,280 --> 00:07:14,020
Those are all questions that our research methods train

199
00:07:14,020 --> 00:07:15,700
data scientists will ask and

200
00:07:15,700 --> 00:07:18,175
will help you make better conclusions.

201
00:07:18,175 --> 00:07:20,470
Another caveat; we could

202
00:07:20,470 --> 00:07:23,170
consider a number of related problems.

203
00:07:23,170 --> 00:07:25,205
This is what's known as Anscombe's quartet.

204
00:07:25,205 --> 00:07:27,340
So these are four different variables that actually

205
00:07:27,340 --> 00:07:30,045
have the same correlation or for different graphs rather.

206
00:07:30,045 --> 00:07:33,200
The top situation shows a trend,

207
00:07:33,200 --> 00:07:35,075
an upward trend between two variables.

208
00:07:35,075 --> 00:07:37,110
Has a certain correlation to it.

209
00:07:37,110 --> 00:07:39,360
Relationship number two, you see

210
00:07:39,360 --> 00:07:41,680
actually has a very systematic relationship;

211
00:07:41,680 --> 00:07:43,650
as X increases,

212
00:07:43,650 --> 00:07:46,210
Y is tending to go up and increase as well.

213
00:07:46,210 --> 00:07:48,775
It's actually a stronger relationship

214
00:07:48,775 --> 00:07:50,440
but you wouldn't know that if you

215
00:07:50,440 --> 00:07:52,870
didn't bother to think that it

216
00:07:52,870 --> 00:07:53,980
might be a problem

217
00:07:53,980 --> 00:07:55,690
because if you were just running correlations,

218
00:07:55,690 --> 00:07:59,380
it has the exact same correlation as the top graph.

219
00:07:59,380 --> 00:08:01,930
Even worse, let's look at the third panel.

220
00:08:01,930 --> 00:08:04,735
Now here we have almost a perfect 1 to 1 relationship

221
00:08:04,735 --> 00:08:07,510
for every single point except one.

222
00:08:07,510 --> 00:08:09,070
And that one point is enough to

223
00:08:09,070 --> 00:08:11,260
throw off the whole analysis.

224
00:08:11,260 --> 00:08:13,170
The fourth example,

225
00:08:13,170 --> 00:08:14,950
we have a bunch of points at one level and

226
00:08:14,950 --> 00:08:16,180
then one little point off in

227
00:08:16,180 --> 00:08:19,015
the distance and we see here again the same correlation.

228
00:08:19,015 --> 00:08:20,905
So these are four different patterns

229
00:08:20,905 --> 00:08:22,910
that all have the same correlation.

230
00:08:22,910 --> 00:08:25,465
So again, some things to think about.

231
00:08:25,465 --> 00:08:26,950
Think through the reason for

232
00:08:26,950 --> 00:08:29,800
the prediction and you can immediately see that

233
00:08:29,800 --> 00:08:31,480
we might need to go looking in

234
00:08:31,480 --> 00:08:32,830
more depth than just a straight

235
00:08:32,830 --> 00:08:34,545
correlation or regression.

236
00:08:34,545 --> 00:08:38,155
Or graph the data, draw a plot.

237
00:08:38,155 --> 00:08:39,730
Some of these trends can be easily

238
00:08:39,730 --> 00:08:41,365
seen with data visualization.

239
00:08:41,365 --> 00:08:43,090
So don't forget data visualization

240
00:08:43,090 --> 00:08:44,875
is one of your best tools.

241
00:08:44,875 --> 00:08:47,280
So some take homes for the association claim.

242
00:08:47,280 --> 00:08:49,640
We are looking at a link between variables.

243
00:08:49,640 --> 00:08:52,240
We cannot claim causality because it

244
00:08:52,240 --> 00:08:55,285
could go really be caused by any of three things.

245
00:08:55,285 --> 00:08:57,310
The last point I will

246
00:08:57,310 --> 00:08:58,690
make is that always

247
00:08:58,690 --> 00:09:00,420
be aware of where your data came from.

248
00:09:00,420 --> 00:09:02,470
Remember, we're just estimating

249
00:09:02,470 --> 00:09:04,105
these relationships with a sample.

250
00:09:04,105 --> 00:09:06,295
Depending on who your population is,

251
00:09:06,295 --> 00:09:08,440
your data may miss the mark.

252
00:09:08,440 --> 00:09:10,165
You need that same external validity.

253
00:09:10,165 --> 00:09:11,860
So the same thing I said to you about

254
00:09:11,860 --> 00:09:13,885
the frequency claim, I'll say it here.

255
00:09:13,885 --> 00:09:15,625
You are going to accurately describe

256
00:09:15,625 --> 00:09:17,320
whoever your participants are.

257
00:09:17,320 --> 00:09:19,120
If that's people who fill out comment cards,

258
00:09:19,120 --> 00:09:21,675
that's not all the customers in your store.

259
00:09:21,675 --> 00:09:23,320
So you need to think about how you

260
00:09:23,320 --> 00:09:24,875
get access to participants.

261
00:09:24,875 --> 00:09:27,805
Ideally, you want data on everybody or at least

262
00:09:27,805 --> 00:09:30,830
not to systematically leave out certain groups of people.

263
00:09:30,830 --> 00:09:32,620
That will help you make sure you're making

264
00:09:32,620 --> 00:09:36,190
good conclusions from the data that you have access to.

