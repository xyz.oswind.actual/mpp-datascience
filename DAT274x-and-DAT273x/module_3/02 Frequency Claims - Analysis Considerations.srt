0
00:00:01,850 --> 00:00:04,820
>> In this lesson, I want to build on what we've been

1
00:00:04,820 --> 00:00:07,480
talking about with frequency claims.

2
00:00:07,480 --> 00:00:08,630
So, in the previous lesson,

3
00:00:08,630 --> 00:00:10,730
we talked about how you might visualize or

4
00:00:10,730 --> 00:00:13,865
describe what a single variable is doing.

5
00:00:13,865 --> 00:00:15,290
We want to report the level of

6
00:00:15,290 --> 00:00:17,240
coffee consumption among a thousand people.

7
00:00:17,240 --> 00:00:19,690
We talked about visualizing it with a graph,

8
00:00:19,690 --> 00:00:21,050
or a histogram, we might

9
00:00:21,050 --> 00:00:23,285
consider making a table or something like that.

10
00:00:23,285 --> 00:00:24,650
In this lesson, I want to talk

11
00:00:24,650 --> 00:00:25,910
about ways you might analyze

12
00:00:25,910 --> 00:00:28,850
that with some sort of a statistical analysis tool.

13
00:00:28,850 --> 00:00:31,640
In this video, I'm going to suggest we're

14
00:00:31,640 --> 00:00:34,595
really just going to stick with a tried and true method.

15
00:00:34,595 --> 00:00:35,980
The average or the mean.

16
00:00:35,980 --> 00:00:39,490
The average or the mean something

17
00:00:39,490 --> 00:00:43,285
we've all learned in some way or form in our lives.

18
00:00:43,285 --> 00:00:44,800
You take all of the scores,

19
00:00:44,800 --> 00:00:46,615
you add them up and divide by their number.

20
00:00:46,615 --> 00:00:48,620
This is just the standard average.

21
00:00:48,620 --> 00:00:50,860
But it is an incredibly useful tool because it

22
00:00:50,860 --> 00:00:53,440
summarizes what the data tends to do.

23
00:00:53,440 --> 00:00:55,435
You see here on this histogram

24
00:00:55,435 --> 00:00:58,270
that the mean is right at about one and a third cup of

25
00:00:58,270 --> 00:01:00,670
coffee and you think to yourself that seems to be

26
00:01:00,670 --> 00:01:03,535
a pretty good description of what's going on here.

27
00:01:03,535 --> 00:01:05,305
A lot of people are consuming one

28
00:01:05,305 --> 00:01:07,000
but there's a good number of people that are

29
00:01:07,000 --> 00:01:08,590
consuming more than one and

30
00:01:08,590 --> 00:01:11,005
the mean gives you a compromise estimate.

31
00:01:11,005 --> 00:01:13,075
It captures both the one

32
00:01:13,075 --> 00:01:15,040
and the more than one and it weights that for you,

33
00:01:15,040 --> 00:01:18,435
it gives you a nice good center of this distribution.

34
00:01:18,435 --> 00:01:20,435
So, if I had to guess

35
00:01:20,435 --> 00:01:23,080
what a person's coffee consumption is,

36
00:01:23,080 --> 00:01:25,255
one and a third is a great guess.

37
00:01:25,255 --> 00:01:26,740
I would like to point out though,

38
00:01:26,740 --> 00:01:29,545
that it is not the only option for you.

39
00:01:29,545 --> 00:01:31,300
For instance, we could actually compute

40
00:01:31,300 --> 00:01:33,620
the median which is another form of average.

41
00:01:33,620 --> 00:01:36,520
Another measure of the center of the data.

42
00:01:36,520 --> 00:01:37,660
You see here that it's

43
00:01:37,660 --> 00:01:39,895
actually giving you a different result.

44
00:01:39,895 --> 00:01:41,325
In this case, the median is

45
00:01:41,325 --> 00:01:44,160
one but the mean is one and a third.

46
00:01:44,160 --> 00:01:46,020
So, wait a minute.

47
00:01:46,020 --> 00:01:48,090
What is the data doing?

48
00:01:48,090 --> 00:01:50,920
Is the best description of this data one cup of

49
00:01:50,920 --> 00:01:52,810
coffee or is the best description of

50
00:01:52,810 --> 00:01:54,800
this data one and a third cup of coffee?

51
00:01:54,800 --> 00:01:56,260
You could imagine that being

52
00:01:56,260 --> 00:01:58,105
an important or meaningful difference.

53
00:01:58,105 --> 00:01:59,560
If I'm wanting to make forecasts or

54
00:01:59,560 --> 00:02:02,740
predictions one cup of coffee per person

55
00:02:02,740 --> 00:02:05,080
per day can be quite different than one

56
00:02:05,080 --> 00:02:07,615
and a third cups of coffee per person per day.

57
00:02:07,615 --> 00:02:10,270
Those could be important and add up.

58
00:02:10,270 --> 00:02:13,270
So, we really want to think for a moment about which is

59
00:02:13,270 --> 00:02:16,880
the best tool to use in different situations.

60
00:02:16,880 --> 00:02:19,495
Now, in this case, we have something called count

61
00:02:19,495 --> 00:02:22,420
data and this is something to pay attention to.

62
00:02:22,420 --> 00:02:25,930
This is not a symmetrical bell-curved distribution.

63
00:02:25,930 --> 00:02:29,545
In fact, you can't have less than zero.

64
00:02:29,545 --> 00:02:32,200
The reason this thing is angled off to

65
00:02:32,200 --> 00:02:36,415
the side is that we have a lower bound zero.

66
00:02:36,415 --> 00:02:38,245
I can't have negative cups of coffee.

67
00:02:38,245 --> 00:02:40,270
I can't owe you coffee for the day.

68
00:02:40,270 --> 00:02:41,860
So, as a result of that,

69
00:02:41,860 --> 00:02:44,620
I can only go down to zero but I can

70
00:02:44,620 --> 00:02:47,625
go up an infinitely large amount.

71
00:02:47,625 --> 00:02:49,210
Well, in theory, infinite,

72
00:02:49,210 --> 00:02:51,350
I mean at some point you've overdosed.

73
00:02:51,350 --> 00:02:52,600
But for the most part,

74
00:02:52,600 --> 00:02:54,325
I could go up an infinite amount.

75
00:02:54,325 --> 00:02:56,150
So, as a result of that, this distribution

76
00:02:56,150 --> 00:02:57,520
is what's called skewed.

77
00:02:57,520 --> 00:03:01,960
It can go up and then it sharply goes out in a long tail.

78
00:03:01,960 --> 00:03:04,810
That long tail is really the problem.

79
00:03:04,810 --> 00:03:06,730
You see when you average numbers,

80
00:03:06,730 --> 00:03:09,590
extreme scores can have some big influence.

81
00:03:09,590 --> 00:03:12,165
You can think of those long scores as

82
00:03:12,165 --> 00:03:15,620
pulling the mean up toward that extreme end.

83
00:03:15,620 --> 00:03:17,350
They're just taking that average and they're

84
00:03:17,350 --> 00:03:20,015
pulling it out toward that long tail.

85
00:03:20,015 --> 00:03:22,990
So, that mean is being biased by those extreme scores.

86
00:03:22,990 --> 00:03:24,640
True, it's a very small number

87
00:03:24,640 --> 00:03:26,420
of extreme scores in the 5,6,7 range,

88
00:03:26,420 --> 00:03:28,910
but they're dragging that mean up a little bit.

89
00:03:28,910 --> 00:03:32,500
So, the general rule is if you've got skewed data,

90
00:03:32,500 --> 00:03:35,275
the mean might not be a good option for you.

91
00:03:35,275 --> 00:03:37,280
In fact, we can measure skewness.

92
00:03:37,280 --> 00:03:39,110
So, there is a tool called Skewness.

93
00:03:39,110 --> 00:03:40,670
It is a statistic we can calculate.

94
00:03:40,670 --> 00:03:43,835
It can be calculated in virtually any data analysis tool.

95
00:03:43,835 --> 00:03:46,950
In general, Skewness works like this.

96
00:03:46,950 --> 00:03:48,820
If you have a skewness statistic of zero that

97
00:03:48,820 --> 00:03:51,130
means the data is nice and distributed.

98
00:03:51,130 --> 00:03:53,290
I would recommend using the mean in that case.

99
00:03:53,290 --> 00:03:57,445
The mean is a beautifully nice democratic statistic.

100
00:03:57,445 --> 00:03:59,240
Every score gets a vote.

101
00:03:59,240 --> 00:04:00,640
So, it tends to be very

102
00:04:00,640 --> 00:04:03,085
stable from one sample to the next.

103
00:04:03,085 --> 00:04:05,010
It's a highly reliable tool.

104
00:04:05,010 --> 00:04:07,955
But if you've got skewed to the left or to the right,

105
00:04:07,955 --> 00:04:10,480
that long tail pointing to the left,

106
00:04:10,480 --> 00:04:12,935
we would say that's negative skew or skew left.

107
00:04:12,935 --> 00:04:14,650
If you've got maybe that long tail

108
00:04:14,650 --> 00:04:16,210
pointing to the right like we have here,

109
00:04:16,210 --> 00:04:17,370
we would say that skew right.

110
00:04:17,370 --> 00:04:19,070
Then you have a non-zero number.

111
00:04:19,070 --> 00:04:22,090
Then you begin to think maybe you've got a problem.

112
00:04:22,090 --> 00:04:24,655
My general suggestion is when you start getting

113
00:04:24,655 --> 00:04:28,140
Skewness levels of up to about one to two,

114
00:04:28,140 --> 00:04:31,515
you might think about using the median instead.

115
00:04:31,515 --> 00:04:33,700
In this case, it's on the fence.

116
00:04:33,700 --> 00:04:35,350
Different data analysts would

117
00:04:35,350 --> 00:04:38,535
potentially argue or disagree about which to use here.

118
00:04:38,535 --> 00:04:40,870
But the mean and median are generally close.

119
00:04:40,870 --> 00:04:43,685
I might generally report the median, in this case,

120
00:04:43,685 --> 00:04:47,245
because I do tend to have some skew right.

121
00:04:47,245 --> 00:04:49,815
So, in this case, and in many cases,

122
00:04:49,815 --> 00:04:51,640
it also doesn't hurt to report both.

123
00:04:51,640 --> 00:04:52,960
But I would definitely

124
00:04:52,960 --> 00:04:55,270
advocate letting people know when they look

125
00:04:55,270 --> 00:04:58,975
at that statistic that it is a skewed distribution.

126
00:04:58,975 --> 00:05:01,900
Yes, so it is one of the third cups of coffee but people

127
00:05:01,900 --> 00:05:05,435
are bounded at zero and it's worth pointing that out.

128
00:05:05,435 --> 00:05:08,350
Another thing I'll point out here is it doesn't hurt to

129
00:05:08,350 --> 00:05:11,080
pair this with something like a standard deviation.

130
00:05:11,080 --> 00:05:12,850
A standard deviation tells you

131
00:05:12,850 --> 00:05:15,605
on average how close are people to the mean.

132
00:05:15,605 --> 00:05:20,065
So, if I say the average coffee count is one and a third,

133
00:05:20,065 --> 00:05:21,700
it might also be worth saying, yeah,

134
00:05:21,700 --> 00:05:23,770
plus or minus about one.

135
00:05:23,770 --> 00:05:26,620
We see here the standard deviation is 1.16.

136
00:05:26,620 --> 00:05:29,170
That might add a little bit more information

137
00:05:29,170 --> 00:05:31,655
to your frequency claim,

138
00:05:31,655 --> 00:05:33,490
and, in fact, this is often what we do.

139
00:05:33,490 --> 00:05:36,220
We typically give a mean or median plus or minus

140
00:05:36,220 --> 00:05:38,095
a standard deviation to let you know

141
00:05:38,095 --> 00:05:40,580
about how far scores are from average.

142
00:05:40,580 --> 00:05:43,235
I can quickly eyeball this and say, okay,

143
00:05:43,235 --> 00:05:46,360
most people are going to be within

144
00:05:46,360 --> 00:05:48,910
about one to three cups of coffee just by

145
00:05:48,910 --> 00:05:52,660
knowing my mean or median and my standard deviation.

