0
00:00:00,000 --> 00:00:05,020
>> So we've designed our experiment.

1
00:00:05,020 --> 00:00:06,680
We have a between subjects design,

2
00:00:06,680 --> 00:00:08,735
we randomly assigned people to two groups,

3
00:00:08,735 --> 00:00:10,230
and now we want to know,

4
00:00:10,230 --> 00:00:11,810
did this intervention work?

5
00:00:11,810 --> 00:00:14,690
Did I improve people's performance by manipulating

6
00:00:14,690 --> 00:00:16,160
their engagement or whatever it

7
00:00:16,160 --> 00:00:17,700
is that I'm trying to study?

8
00:00:17,700 --> 00:00:20,180
How do we do the analysis on this?

9
00:00:20,180 --> 00:00:21,890
As we've discussed before,

10
00:00:21,890 --> 00:00:23,660
there are many different analysis techniques

11
00:00:23,660 --> 00:00:25,220
you can use for comparing groups.

12
00:00:25,220 --> 00:00:27,530
You could use something called a t-test

13
00:00:27,530 --> 00:00:29,985
to see whether the means of the two groups are different.

14
00:00:29,985 --> 00:00:32,400
Do the people in your engagement group,

15
00:00:32,400 --> 00:00:34,975
do they have a higher average performance in

16
00:00:34,975 --> 00:00:36,940
the people who did not do the engagement treatment?

17
00:00:36,940 --> 00:00:38,075
So that's one option.

18
00:00:38,075 --> 00:00:39,730
You can do something called an ANOVA,

19
00:00:39,730 --> 00:00:41,170
an analysis of variance

20
00:00:41,170 --> 00:00:42,965
if you've got several groups to compare them.

21
00:00:42,965 --> 00:00:44,600
Or you can use regression,

22
00:00:44,600 --> 00:00:46,100
as we talked about.

23
00:00:46,100 --> 00:00:47,330
The thing I want to emphasize here is

24
00:00:47,330 --> 00:00:49,630
actually in a between groups experiment,

25
00:00:49,630 --> 00:00:51,470
the data analysis is exactly the

26
00:00:51,470 --> 00:00:53,540
same as it was in an association claim.

27
00:00:53,540 --> 00:00:56,690
So if you think about the only difference between

28
00:00:56,690 --> 00:01:00,500
an experiment and an association claim,

29
00:01:00,500 --> 00:01:02,725
a causal claim and an association claim

30
00:01:02,725 --> 00:01:04,180
is the interpretation.

31
00:01:04,180 --> 00:01:06,845
The data analysis technique is exactly the same.

32
00:01:06,845 --> 00:01:09,910
You can use a regression here to see as people,

33
00:01:09,910 --> 00:01:11,995
as the group changes,

34
00:01:11,995 --> 00:01:14,360
do we see a corresponding change

35
00:01:14,360 --> 00:01:15,860
in their performance or whatever

36
00:01:15,860 --> 00:01:17,335
variables we're trying to measure.

37
00:01:17,335 --> 00:01:19,175
So, although we often have

38
00:01:19,175 --> 00:01:20,840
kind of a set of analysis we use for

39
00:01:20,840 --> 00:01:22,820
causal claims and a set of analysis that

40
00:01:22,820 --> 00:01:24,890
we use for association claims,

41
00:01:24,890 --> 00:01:26,645
you can actually use either for either.

42
00:01:26,645 --> 00:01:29,705
What changes is the interpretation.

43
00:01:29,705 --> 00:01:32,090
If you're doing within subjects design,

44
00:01:32,090 --> 00:01:34,180
which I suggested is maybe a little bit more efficient,

45
00:01:34,180 --> 00:01:36,900
a little better, it does change a little bit,

46
00:01:36,900 --> 00:01:39,330
so I do want to talk briefly about this.

47
00:01:39,330 --> 00:01:41,920
Remember in the Within subjects design,

48
00:01:41,920 --> 00:01:43,625
you have people who start in one group

49
00:01:43,625 --> 00:01:45,440
and you change the group that they're in.

50
00:01:45,440 --> 00:01:46,640
So you actually have them

51
00:01:46,640 --> 00:01:48,995
swap groups partway through the study.

52
00:01:48,995 --> 00:01:51,650
This is a fantastically good design more efficient.

53
00:01:51,650 --> 00:01:52,880
We talked about that,

54
00:01:52,880 --> 00:01:54,410
but the data analysis does change.

55
00:01:54,410 --> 00:01:55,985
So now what you're interested in

56
00:01:55,985 --> 00:01:58,150
is whether people's scores change.

57
00:01:58,150 --> 00:02:01,115
So there are some data analysis tools for this.

58
00:02:01,115 --> 00:02:03,710
Historically, we would have something called Within

59
00:02:03,710 --> 00:02:06,660
subjects t-test or a paired sample's t-test,

60
00:02:06,660 --> 00:02:08,785
sometimes called a Dependent samples t-test.

61
00:02:08,785 --> 00:02:11,600
Gosh, they have a lot of names for these tools.

62
00:02:11,600 --> 00:02:13,610
They had analysis of various options

63
00:02:13,610 --> 00:02:14,980
called a Repeated Measures ANOVA.

64
00:02:14,980 --> 00:02:16,240
That would be another option.

65
00:02:16,240 --> 00:02:18,020
Today, however, we typically do

66
00:02:18,020 --> 00:02:19,880
something called a Mixed Effects Model,

67
00:02:19,880 --> 00:02:21,500
and we're going to get into

68
00:02:21,500 --> 00:02:23,440
a little bit of that in the labs.

69
00:02:23,440 --> 00:02:25,520
But what you need to know essentially is

70
00:02:25,520 --> 00:02:27,740
we're simply looking at the average change.

71
00:02:27,740 --> 00:02:30,680
When I go from group one to group two, what is my change?

72
00:02:30,680 --> 00:02:31,835
Is it a positive changes?

73
00:02:31,835 --> 00:02:32,965
Is it a negative change?

74
00:02:32,965 --> 00:02:34,520
That's really what we're analyzing,

75
00:02:34,520 --> 00:02:36,530
is the change, the amount of change.

76
00:02:36,530 --> 00:02:38,090
So that's something to keep in mind.

77
00:02:38,090 --> 00:02:40,415
It is a slightly different analysis.

78
00:02:40,415 --> 00:02:41,900
In a Between subjects design,

79
00:02:41,900 --> 00:02:43,925
I'm just asking, are the means different?

80
00:02:43,925 --> 00:02:46,290
In a Within subjects design I'm asking,

81
00:02:46,290 --> 00:02:50,060
is the amount of change positive, negative, or neutral?

82
00:02:50,060 --> 00:02:52,325
So it is a slightly different analysis.

83
00:02:52,325 --> 00:02:53,840
The Within subjects design is

84
00:02:53,840 --> 00:02:56,450
objectively better in most cases because you're

85
00:02:56,450 --> 00:02:58,310
getting a lot more data

86
00:02:58,310 --> 00:03:01,195
out of the same participants and is more efficient.

87
00:03:01,195 --> 00:03:03,055
Last thing I will mention,

88
00:03:03,055 --> 00:03:05,670
a lot of these analysis techniques I just talked about,

89
00:03:05,670 --> 00:03:07,550
t-tests and ANOVAs, they're really

90
00:03:07,550 --> 00:03:09,400
there to give you those key values.

91
00:03:09,400 --> 00:03:11,505
We want to reject the null hypothesis.

92
00:03:11,505 --> 00:03:13,455
But please, please, please, remember,

93
00:03:13,455 --> 00:03:16,325
p is less than point 0.05 is not the whole story.

94
00:03:16,325 --> 00:03:19,090
You need to estimate the size of the effect.

95
00:03:19,090 --> 00:03:21,250
So be sure to calculate those slopes,

96
00:03:21,250 --> 00:03:23,440
be sure to calculate those CO/SD values,

97
00:03:23,440 --> 00:03:25,310
so you can quantify the size of

98
00:03:25,310 --> 00:03:27,695
the effect not just the significance.

99
00:03:27,695 --> 00:03:29,180
We definitely want to be able to

100
00:03:29,180 --> 00:03:30,650
reject the null hypothesis.

101
00:03:30,650 --> 00:03:32,570
You need statistical significance,

102
00:03:32,570 --> 00:03:35,355
but it is not the end of the story, but the beginning.

103
00:03:35,355 --> 00:03:37,425
Still with these analysis techniques,

104
00:03:37,425 --> 00:03:38,990
you have a good way of saying,

105
00:03:38,990 --> 00:03:40,790
"I did an intervention and it

106
00:03:40,790 --> 00:03:43,145
was greater than a chance effect.

107
00:03:43,145 --> 00:03:45,190
I have a real effect of my intervention,

108
00:03:45,190 --> 00:03:47,050
now I'm going to describe to you how big it is."

109
00:03:47,050 --> 00:03:48,830
And really that's the goal on research.

110
00:03:48,830 --> 00:03:50,150
We can now estimate with

111
00:03:50,150 --> 00:03:54,050
confidence the impact of our effects and interventions.

