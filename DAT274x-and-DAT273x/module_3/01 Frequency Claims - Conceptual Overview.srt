0
00:00:00,770 --> 00:00:03,150
>> In this module, we're going to talk

1
00:00:03,150 --> 00:00:04,710
about something a little different.

2
00:00:04,710 --> 00:00:06,390
We're going to talk about the different kinds of

3
00:00:06,390 --> 00:00:08,930
claims that you can make with research.

4
00:00:08,930 --> 00:00:11,040
Now this applies both to the kinds

5
00:00:11,040 --> 00:00:13,110
of statistical modeling we'll do,

6
00:00:13,110 --> 00:00:15,315
but also the kinds of research designs and

7
00:00:15,315 --> 00:00:17,880
ideas that we can express with research.

8
00:00:17,880 --> 00:00:19,920
So I want to talk through three different kinds of

9
00:00:19,920 --> 00:00:22,630
stories or analyses that you might do.

10
00:00:22,630 --> 00:00:25,560
I'm going to group them into three different dimensions,

11
00:00:25,560 --> 00:00:26,580
and we're going to start with

12
00:00:26,580 --> 00:00:29,750
a really basic one called the "Frequency Claim."

13
00:00:29,750 --> 00:00:32,010
So let's go ahead and take a look at

14
00:00:32,010 --> 00:00:35,695
a Data situation you might have for a Frequency Claim.

15
00:00:35,695 --> 00:00:37,850
So here we go on the screen.

16
00:00:37,850 --> 00:00:40,325
I have data from 1,000 people

17
00:00:40,325 --> 00:00:43,475
on how many coffee beverages they consume in a day.

18
00:00:43,475 --> 00:00:45,800
This is the starting point

19
00:00:45,800 --> 00:00:47,720
for almost any research or data analysis.

20
00:00:47,720 --> 00:00:49,740
You want to understand coffee consumption,

21
00:00:49,740 --> 00:00:51,020
so you get a number of

22
00:00:51,020 --> 00:00:53,535
people to give you their coffee counts.

23
00:00:53,535 --> 00:00:54,815
And here you see we have

24
00:00:54,815 --> 00:00:58,550
a loose disorganized amount of data.

25
00:00:58,550 --> 00:01:00,455
This is a very basic problem,

26
00:01:00,455 --> 00:01:01,640
but it's still one that we need

27
00:01:01,640 --> 00:01:03,000
to overcome with research,

28
00:01:03,000 --> 00:01:06,410
that is, how much coffee are people drinking?

29
00:01:06,410 --> 00:01:08,890
Here, you can eyeball this.

30
00:01:08,890 --> 00:01:10,160
I see a bunch of ones,

31
00:01:10,160 --> 00:01:11,810
so it seems like maybe some people

32
00:01:11,810 --> 00:01:13,740
are drinking one cup a day.

33
00:01:13,740 --> 00:01:15,870
You see a bunch of zeroes, some people drink none.

34
00:01:15,870 --> 00:01:17,570
I'm seeing some twos, and threes, and fours,

35
00:01:17,570 --> 00:01:21,580
so I'm going to estimate somewhere around 1 to 4.

36
00:01:21,580 --> 00:01:23,690
However, I'm just going to point

37
00:01:23,690 --> 00:01:25,370
out this is an eyeball estimate.

38
00:01:25,370 --> 00:01:26,585
I don't have any kind of

39
00:01:26,585 --> 00:01:30,020
statistical or data science model

40
00:01:30,020 --> 00:01:31,655
to make sense of this for me.

41
00:01:31,655 --> 00:01:33,880
This is what a Frequency Claim is going to be.

42
00:01:33,880 --> 00:01:36,020
We're going to just simply make sense of and

43
00:01:36,020 --> 00:01:39,260
report the level of my variable.

44
00:01:39,260 --> 00:01:43,540
You need to take the temperature of your variable.

45
00:01:43,540 --> 00:01:45,815
So, a common way of doing this is with a graph.

46
00:01:45,815 --> 00:01:47,600
So let's look at a graph here.

47
00:01:47,600 --> 00:01:48,930
This is a Histogram.

48
00:01:48,930 --> 00:01:50,770
Now, you are familiar with this

49
00:01:50,770 --> 00:01:53,595
obviously at this point in your training,

50
00:01:53,595 --> 00:01:55,280
but we can still review it.

51
00:01:55,280 --> 00:01:58,090
So a Histogram is going to show you on the x-axis,

52
00:01:58,090 --> 00:02:00,860
the different values that we see in the data.

53
00:02:00,860 --> 00:02:03,040
So here we see there's some zero, some ones,

54
00:02:03,040 --> 00:02:04,385
some two, some threes, and fours,

55
00:02:04,385 --> 00:02:05,870
and a few higher numbers.

56
00:02:05,870 --> 00:02:07,880
Immediately we see that

57
00:02:07,880 --> 00:02:10,160
the most common score is going to be one,

58
00:02:10,160 --> 00:02:11,800
and I can see that because on

59
00:02:11,800 --> 00:02:15,810
the y-axis represents the number of those scores.

60
00:02:15,810 --> 00:02:17,705
So I can easily see here,

61
00:02:17,705 --> 00:02:20,375
one is the most common number.

62
00:02:20,375 --> 00:02:23,170
You can also get a sense of the distribution,

63
00:02:23,170 --> 00:02:25,640
that's somewhere between 0 and 2

64
00:02:25,640 --> 00:02:28,055
is probably the vast majority of cases.

65
00:02:28,055 --> 00:02:30,380
And we have some more things

66
00:02:30,380 --> 00:02:31,970
we could do with some further analyses,

67
00:02:31,970 --> 00:02:33,890
but this gives you a good rough

68
00:02:33,890 --> 00:02:36,890
eyeball impression of the result.

69
00:02:36,890 --> 00:02:39,335
And this is what a Frequency Claim is going to do.

70
00:02:39,335 --> 00:02:41,900
It's going to be measuring the level of a variable.

71
00:02:41,900 --> 00:02:44,720
Now, you can slice and dice this a little bit more.

72
00:02:44,720 --> 00:02:47,590
You can do something like give percentages.

73
00:02:47,590 --> 00:02:48,955
You could say for instance,

74
00:02:48,955 --> 00:02:50,570
70 percent of customers have

75
00:02:50,570 --> 00:02:52,190
positive ratings of my restaurant,

76
00:02:52,190 --> 00:02:54,540
or you can give percentages under this.

77
00:02:54,540 --> 00:02:56,885
I could say what percentage of people

78
00:02:56,885 --> 00:02:59,825
report to or higher, something like that.

79
00:02:59,825 --> 00:03:02,780
What percentage of people report a non-zero answer?

80
00:03:02,780 --> 00:03:04,675
It's certainly possible to do that.

81
00:03:04,675 --> 00:03:06,770
But importantly, a Frequency Claim

82
00:03:06,770 --> 00:03:08,690
is going to be trying to report the level

83
00:03:08,690 --> 00:03:10,430
of a variable with some kind of

84
00:03:10,430 --> 00:03:12,855
summary, statistic or tool.

85
00:03:12,855 --> 00:03:14,885
I really loved the data visualization.

86
00:03:14,885 --> 00:03:17,520
In fact I advocate always start there.

87
00:03:17,520 --> 00:03:21,730
Before you calculate a number, just plot it,

88
00:03:21,730 --> 00:03:25,190
just give yourself that little extra graphic,

89
00:03:25,190 --> 00:03:27,200
because the graphic will often tell you

90
00:03:27,200 --> 00:03:29,870
much more than any summary number.

91
00:03:29,870 --> 00:03:31,790
Couple of things I would say though if

92
00:03:31,790 --> 00:03:33,705
you're going to be doing Frequency Claims,

93
00:03:33,705 --> 00:03:35,825
you're not really comparing on other qualities.

94
00:03:35,825 --> 00:03:37,545
If you want to compare, say,

95
00:03:37,545 --> 00:03:42,410
how full time versus part time people consume coffee,

96
00:03:42,410 --> 00:03:44,360
then you're really doing a different kind of analysis.

97
00:03:44,360 --> 00:03:47,755
So, you're really just looking at one variable at a time,

98
00:03:47,755 --> 00:03:50,500
and looking at a Histogram is a great place to start.

99
00:03:50,500 --> 00:03:52,100
If you're doing Categorical Data,

100
00:03:52,100 --> 00:03:54,790
you're looking at things like eye color,

101
00:03:54,790 --> 00:03:56,650
or job status, or something like that.

102
00:03:56,650 --> 00:03:57,680
That's not numeric.

103
00:03:57,680 --> 00:03:59,140
You can do the same kind of thing.

104
00:03:59,140 --> 00:04:00,950
You can make a Bar Graph that would show

105
00:04:00,950 --> 00:04:03,725
the number of people who are part time versus full time.

106
00:04:03,725 --> 00:04:05,215
Something along those lines.

107
00:04:05,215 --> 00:04:08,165
Those kind of visualizations are great places to start.

108
00:04:08,165 --> 00:04:10,795
That would be the essence of a Frequency Claim.

109
00:04:10,795 --> 00:04:12,410
What is the level or what are

110
00:04:12,410 --> 00:04:14,870
the most common levels of a variable.

111
00:04:14,870 --> 00:04:18,140
A great place to start any data analysis.

