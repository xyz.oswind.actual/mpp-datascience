0
00:00:01,400 --> 00:00:04,200
>> All right. So, now let's think

1
00:00:04,200 --> 00:00:06,490
about what we actually want to know.

2
00:00:06,490 --> 00:00:09,060
Remember all the way back at the beginning of our course,

3
00:00:09,060 --> 00:00:11,280
we said there are really two kinds of research with

4
00:00:11,280 --> 00:00:13,865
two kinds of goals: Basic research,

5
00:00:13,865 --> 00:00:16,605
you want to understand what things are doing.

6
00:00:16,605 --> 00:00:18,335
You want to know what's causing what,

7
00:00:18,335 --> 00:00:20,070
you want to know how this thing

8
00:00:20,070 --> 00:00:21,720
that you're studying works.

9
00:00:21,720 --> 00:00:23,500
Second kind of research,

10
00:00:23,500 --> 00:00:25,915
you want to know if I were to make some sort of

11
00:00:25,915 --> 00:00:28,370
change or a policy implementation,

12
00:00:28,370 --> 00:00:30,180
develop some kind of product,

13
00:00:30,180 --> 00:00:31,380
scale something out to

14
00:00:31,380 --> 00:00:33,075
all my locations or whatever it is,

15
00:00:33,075 --> 00:00:34,620
what would happen, if I were to do

16
00:00:34,620 --> 00:00:36,980
something what would be the effect of it.

17
00:00:36,980 --> 00:00:39,390
Really, both of these things are about

18
00:00:39,390 --> 00:00:41,860
anticipating future decisions, right?

19
00:00:41,860 --> 00:00:43,380
If I act in the world,

20
00:00:43,380 --> 00:00:45,120
I want to know what the result is.

21
00:00:45,120 --> 00:00:47,910
I need to know what causes what.

22
00:00:47,910 --> 00:00:49,740
I'm not really satisfied with

23
00:00:49,740 --> 00:00:52,560
predicting something, "Association claim".

24
00:00:52,560 --> 00:00:55,870
I want to know what's causing what.

25
00:00:55,870 --> 00:00:58,265
I need to be able to make a Cause effect claim.

26
00:00:58,265 --> 00:01:00,745
And that's really our third research goal,

27
00:01:00,745 --> 00:01:01,815
is a "Causal claim".

28
00:01:01,815 --> 00:01:04,015
Let's talk about Causal claims.

29
00:01:04,015 --> 00:01:07,060
How do we know if one thing causes another?

30
00:01:07,060 --> 00:01:10,715
Well, we kind of have three criteria we can use.

31
00:01:10,715 --> 00:01:12,625
If we meet all of these criteria,

32
00:01:12,625 --> 00:01:14,855
we could actually make a Cause effect claim.

33
00:01:14,855 --> 00:01:17,185
If we can't meet all of these criteria,

34
00:01:17,185 --> 00:01:19,745
then a Cause effect claim is not really warranted.

35
00:01:19,745 --> 00:01:21,110
So, let's talk through these.

36
00:01:21,110 --> 00:01:22,905
The first is covariance,

37
00:01:22,905 --> 00:01:25,270
which sounds technical but

38
00:01:25,270 --> 00:01:27,640
actually it just means there is a relationship.

39
00:01:27,640 --> 00:01:29,660
Covariance means that as one thing varies,

40
00:01:29,660 --> 00:01:30,790
the other thing varies with it.

41
00:01:30,790 --> 00:01:33,525
It covaries, it correlates.

42
00:01:33,525 --> 00:01:37,345
Covariance is really just an Association claim.

43
00:01:37,345 --> 00:01:39,130
It's saying that I know that there is some sort of

44
00:01:39,130 --> 00:01:41,260
link or relationship between two variables.

45
00:01:41,260 --> 00:01:42,480
No surprise there.

46
00:01:42,480 --> 00:01:45,220
The second one is a little bit trickier.

47
00:01:45,220 --> 00:01:47,710
We call it Temporal precedence but it basically

48
00:01:47,710 --> 00:01:50,510
means that I know the cause comes before the effect.

49
00:01:50,510 --> 00:01:52,690
I don't know about you but the universe in which

50
00:01:52,690 --> 00:01:54,880
I live causes come before effects.

51
00:01:54,880 --> 00:01:58,140
So, remember how we said before it's sometimes unclear,

52
00:01:58,140 --> 00:02:01,400
does X cause Y or does Y cause X? Which comes first?

53
00:02:01,400 --> 00:02:04,495
Is it engaged employees lead to better performance,

54
00:02:04,495 --> 00:02:05,890
or does better performance lead

55
00:02:05,890 --> 00:02:07,970
to more engaged employees?

56
00:02:07,970 --> 00:02:10,510
In a typical co-relational analysis,

57
00:02:10,510 --> 00:02:12,430
I don't have that information but in

58
00:02:12,430 --> 00:02:14,380
fact I need it to make a causal effect.

59
00:02:14,380 --> 00:02:17,230
After all if I see one thing came first and it

60
00:02:17,230 --> 00:02:18,640
predicts the other thing that came

61
00:02:18,640 --> 00:02:21,930
later that tells me which direction it's going.

62
00:02:21,930 --> 00:02:24,100
The third criteria is that

63
00:02:24,100 --> 00:02:26,710
we've eliminated alternative explanations.

64
00:02:26,710 --> 00:02:29,500
For instance, I would need to know that it

65
00:02:29,500 --> 00:02:33,085
was engagement that's causing

66
00:02:33,085 --> 00:02:35,820
performance and not something related to engagement.

67
00:02:35,820 --> 00:02:37,870
Or I might want to know that it's performance that's

68
00:02:37,870 --> 00:02:38,890
causing engagement and

69
00:02:38,890 --> 00:02:40,510
not something related to performance.

70
00:02:40,510 --> 00:02:41,890
Really, I'm saying we need to get

71
00:02:41,890 --> 00:02:43,560
rid of those third variables,

72
00:02:43,560 --> 00:02:45,355
those other things that might be

73
00:02:45,355 --> 00:02:47,845
correlated or are tracking with those variables.

74
00:02:47,845 --> 00:02:49,840
I need to eliminate those explanations

75
00:02:49,840 --> 00:02:51,640
so that I know that it's the variable in

76
00:02:51,640 --> 00:02:53,440
my model that's actually doing

77
00:02:53,440 --> 00:02:56,030
the causing and not something related to it.

78
00:02:56,030 --> 00:02:58,315
This is actually sounds like

79
00:02:58,315 --> 00:03:01,660
an impossibly tall tale and in fact, it is.

80
00:03:01,660 --> 00:03:03,370
If you're dealing with just data

81
00:03:03,370 --> 00:03:05,140
that you've collected in most situations,

82
00:03:05,140 --> 00:03:06,760
it is almost if not

83
00:03:06,760 --> 00:03:09,655
actually impossible to make causal claims.

84
00:03:09,655 --> 00:03:12,160
It is possible for instance,

85
00:03:12,160 --> 00:03:13,825
if I have early data

86
00:03:13,825 --> 00:03:16,010
and later data and I can show the early data predicts

87
00:03:16,010 --> 00:03:18,700
the later data and I can

88
00:03:18,700 --> 00:03:20,410
measure and statistically control

89
00:03:20,410 --> 00:03:22,160
for every possible explanation,

90
00:03:22,160 --> 00:03:23,765
I can maybe get close.

91
00:03:23,765 --> 00:03:26,815
But really, honestly, I'm not really going to get there.

92
00:03:26,815 --> 00:03:28,490
If I want to meet these criteria,

93
00:03:28,490 --> 00:03:29,905
I have to do something different.

94
00:03:29,905 --> 00:03:31,540
I have to be more active.

95
00:03:31,540 --> 00:03:33,250
So, let's talk about that.

96
00:03:33,250 --> 00:03:36,285
We're going to do something called an Experiment.

97
00:03:36,285 --> 00:03:39,425
And an Experiment is a special kind of research tool.

98
00:03:39,425 --> 00:03:42,500
I've been referring to this some as an AB test,

99
00:03:42,500 --> 00:03:45,045
and you may have heard of an AB test before.

100
00:03:45,045 --> 00:03:46,775
An Experiment is simply

101
00:03:46,775 --> 00:03:48,980
a situation in which you create or

102
00:03:48,980 --> 00:03:51,400
are actively involved in

103
00:03:51,400 --> 00:03:52,710
the reality of your participants.

104
00:03:52,710 --> 00:03:55,670
You intervene in the world and see the effect.

105
00:03:55,670 --> 00:03:57,380
You know what the cause is and

106
00:03:57,380 --> 00:03:59,080
what the effect is because you did it.

107
00:03:59,080 --> 00:04:01,910
You were the one who imposed the cause

108
00:04:01,910 --> 00:04:04,880
on your participants and you saw the effect.

109
00:04:04,880 --> 00:04:06,815
So, let's look at how this might work.

110
00:04:06,815 --> 00:04:08,450
Here's some lovely participants,

111
00:04:08,450 --> 00:04:10,270
I've recruited them to be in my study.

112
00:04:10,270 --> 00:04:13,760
I want to see what the effect of

113
00:04:13,760 --> 00:04:15,395
some intervention or treatment

114
00:04:15,395 --> 00:04:17,920
or thing that I'm studying is.

115
00:04:17,920 --> 00:04:19,880
Maybe I have an employee

116
00:04:19,880 --> 00:04:22,220
engagement improvement program and I want to

117
00:04:22,220 --> 00:04:23,990
see if putting them in

118
00:04:23,990 --> 00:04:27,095
this program makes them higher performance.

119
00:04:27,095 --> 00:04:28,745
This is testing that theory

120
00:04:28,745 --> 00:04:31,060
that engagement leads to performance.

121
00:04:31,060 --> 00:04:32,875
So, I intervene.

122
00:04:32,875 --> 00:04:34,955
I increase their engagement,

123
00:04:34,955 --> 00:04:36,745
see if performance changes.

124
00:04:36,745 --> 00:04:38,795
To do this, I'm going to create two groups.

125
00:04:38,795 --> 00:04:40,310
One group is going to be

126
00:04:40,310 --> 00:04:43,160
the engagement group, say group 1.

127
00:04:43,160 --> 00:04:44,660
These people are going to undergo

128
00:04:44,660 --> 00:04:46,430
that engagement improvement exercise.

129
00:04:46,430 --> 00:04:48,710
I'm going to try to improve their engagement and see

130
00:04:48,710 --> 00:04:51,410
if their performance improves.

131
00:04:51,410 --> 00:04:53,430
Group 2 is not going to do that.

132
00:04:53,430 --> 00:04:54,965
They are my comparison group.

133
00:04:54,965 --> 00:04:56,975
They are my control group.

134
00:04:56,975 --> 00:04:58,880
Because I have given

135
00:04:58,880 --> 00:05:00,620
this treatment to one group and not the

136
00:05:00,620 --> 00:05:04,189
other and I'm going to assign them at random to groups,

137
00:05:04,189 --> 00:05:06,670
I can draw a cause effect conclusions. Let's see that.

138
00:05:06,670 --> 00:05:08,120
So, first off I randomly

139
00:05:08,120 --> 00:05:10,040
put people into groups, flipped a coin.

140
00:05:10,040 --> 00:05:11,990
Every person had an equal chance

141
00:05:11,990 --> 00:05:13,070
of being in either group.

142
00:05:13,070 --> 00:05:16,835
So, Group 1 has participants one and four,

143
00:05:16,835 --> 00:05:18,755
Group 2 as the middle two participants.

144
00:05:18,755 --> 00:05:20,185
This was done at random.

145
00:05:20,185 --> 00:05:22,829
Because this was done at random I'm confident

146
00:05:22,829 --> 00:05:24,950
that all of the features

147
00:05:24,950 --> 00:05:27,095
of my participants was distributed evenly.

148
00:05:27,095 --> 00:05:30,600
So, for instance, they are going to be,

149
00:05:30,600 --> 00:05:32,785
the sexes will be evenly distributed,

150
00:05:32,785 --> 00:05:34,930
racial background will be evenly distributed,

151
00:05:34,930 --> 00:05:37,240
work history will be evenly distributed,

152
00:05:37,240 --> 00:05:39,380
personality traits will be evenly distributed.

153
00:05:39,380 --> 00:05:40,935
Right, if this is done randomly there's

154
00:05:40,935 --> 00:05:43,910
no systematic difference between my groups.

155
00:05:43,910 --> 00:05:46,595
In fact, the only systematic difference

156
00:05:46,595 --> 00:05:48,810
between my groups is the one that I caused,

157
00:05:48,810 --> 00:05:50,695
and that's the engagement treatment.

158
00:05:50,695 --> 00:05:52,250
So, if I then see

159
00:05:52,250 --> 00:05:55,840
a result that performance is better in group 1,

160
00:05:55,840 --> 00:05:57,140
well now I know the only

161
00:05:57,140 --> 00:05:59,035
systematic difference was the engagement.

162
00:05:59,035 --> 00:06:01,490
So, I can be confident that's what caused it.

163
00:06:01,490 --> 00:06:04,925
In fact, it meets all three criteria from before.

164
00:06:04,925 --> 00:06:06,455
That's really great. Let's look.

165
00:06:06,455 --> 00:06:08,800
If I see Group 1 scores higher on performance,

166
00:06:08,800 --> 00:06:11,080
there is your co-variance.

167
00:06:11,080 --> 00:06:14,270
Group 1 more performance than Group 2.

168
00:06:14,270 --> 00:06:16,800
So, that's the relationship, right?

169
00:06:16,800 --> 00:06:18,740
Or the difference between groups score.

170
00:06:18,740 --> 00:06:20,605
So, I've met the first criteria.

171
00:06:20,605 --> 00:06:23,610
Temporal precedents which came first.

172
00:06:23,610 --> 00:06:27,345
I know the engagement came first because I did it.

173
00:06:27,345 --> 00:06:28,725
I intervened.

174
00:06:28,725 --> 00:06:30,870
I went in there and I ran them and I

175
00:06:30,870 --> 00:06:33,720
tinkered systematically with their engagement.

176
00:06:33,720 --> 00:06:36,210
I know that came first and I know the performance

177
00:06:36,210 --> 00:06:38,550
came later because I did them in time.

178
00:06:38,550 --> 00:06:40,380
So, I did the engagement intervention

179
00:06:40,380 --> 00:06:43,005
first and then some period of time later,

180
00:06:43,005 --> 00:06:44,750
I measured their performance.

181
00:06:44,750 --> 00:06:48,045
So, I know which one came first because I did it.

182
00:06:48,045 --> 00:06:51,160
What about alternative explanations eliminated?

183
00:06:51,160 --> 00:06:52,830
Well, in fact that once covered too.

184
00:06:52,830 --> 00:06:55,100
I put people randomly into groups.

185
00:06:55,100 --> 00:06:57,600
So, every possible competing explanation,

186
00:06:57,600 --> 00:07:00,990
whether it's work history or attitude or whatever is

187
00:07:00,990 --> 00:07:02,610
going to be evenly distributed across

188
00:07:02,610 --> 00:07:05,295
the two groups as long as they're sufficiently large.

189
00:07:05,295 --> 00:07:08,070
This is a beautiful research design

190
00:07:08,070 --> 00:07:11,735
because I can meet all three criteria for causation.

191
00:07:11,735 --> 00:07:15,840
I can know that indeed it was the employee engagement

192
00:07:15,840 --> 00:07:20,340
that led to an increase in performance because I did it.

193
00:07:20,340 --> 00:07:22,380
This is caught up between subjects experiment

194
00:07:22,380 --> 00:07:24,310
and it is a beautiful design.

195
00:07:24,310 --> 00:07:26,190
It is a good workhorse.

196
00:07:26,190 --> 00:07:28,440
I will briefly mention another type of

197
00:07:28,440 --> 00:07:30,990
design called The within subjects design.

198
00:07:30,990 --> 00:07:33,520
The within subjects design is very similar,

199
00:07:33,520 --> 00:07:35,185
but in The within subjects design,

200
00:07:35,185 --> 00:07:36,540
I basically start you in

201
00:07:36,540 --> 00:07:38,980
a random group and I swap you halfway through.

202
00:07:38,980 --> 00:07:41,355
It's essentially the same design

203
00:07:41,355 --> 00:07:43,335
I just measure each person twice.

204
00:07:43,335 --> 00:07:44,830
I just reuse them.

205
00:07:44,830 --> 00:07:46,840
Lets think about what that might mean.

206
00:07:46,840 --> 00:07:50,335
So, I actually get all four people in group 1.

207
00:07:50,335 --> 00:07:52,050
Some of these people started out in group 1,

208
00:07:52,050 --> 00:07:53,610
some started out in Group 2 but I

209
00:07:53,610 --> 00:07:55,795
get everybody in that program.

210
00:07:55,795 --> 00:07:58,650
Then I also get everybody in the other group,

211
00:07:58,650 --> 00:08:00,060
because I measure each person twice.

212
00:08:00,060 --> 00:08:01,465
So, what would this look like?

213
00:08:01,465 --> 00:08:03,545
Well in our research example,

214
00:08:03,545 --> 00:08:05,880
this would mean half the people start out in

215
00:08:05,880 --> 00:08:08,190
the comparison group and I measure

216
00:08:08,190 --> 00:08:10,830
their performance and then I put them in

217
00:08:10,830 --> 00:08:12,000
the engagement group and I see

218
00:08:12,000 --> 00:08:13,970
if their performance increases.

219
00:08:13,970 --> 00:08:16,200
So, I'm now looking within each person,

220
00:08:16,200 --> 00:08:18,190
"Hey when I put you into

221
00:08:18,190 --> 00:08:22,165
that engagement group, what happens to you?"

222
00:08:22,165 --> 00:08:23,550
This is really efficient.

223
00:08:23,550 --> 00:08:24,610
I've measured you twice.

224
00:08:24,610 --> 00:08:26,635
I get twice as much data out.

225
00:08:26,635 --> 00:08:29,420
What about people who start off in the engagement group?

226
00:08:29,420 --> 00:08:30,500
They're really engaged.

227
00:08:30,500 --> 00:08:31,560
Now I take them out of it.

228
00:08:31,560 --> 00:08:32,575
Their engagement drops.

229
00:08:32,575 --> 00:08:34,420
Does their performance drop too?

230
00:08:34,420 --> 00:08:37,130
It's just a more efficient design because now

231
00:08:37,130 --> 00:08:38,480
I'm not just comparing the people

232
00:08:38,480 --> 00:08:40,010
in group 1 against people in group 2,

233
00:08:40,010 --> 00:08:41,585
I'm actually comparing you in

234
00:08:41,585 --> 00:08:43,380
one group against you in the other group.

235
00:08:43,380 --> 00:08:46,050
Who has a better comparison for you than you?

236
00:08:46,050 --> 00:08:47,630
It is a much more efficient design.

237
00:08:47,630 --> 00:08:49,980
Also, you get two observations per person,

238
00:08:49,980 --> 00:08:51,330
you get twice as much data,

239
00:08:51,330 --> 00:08:53,800
twice as much bang for your buck.

