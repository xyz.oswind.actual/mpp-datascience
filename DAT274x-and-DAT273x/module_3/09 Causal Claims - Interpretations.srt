0
00:00:00,000 --> 00:00:03,545
>> We've done our experiment.

1
00:00:03,545 --> 00:00:06,410
We've got our data. We're ready to make our causal claim.

2
00:00:06,410 --> 00:00:08,660
Are there any considerations we need to think

3
00:00:08,660 --> 00:00:10,790
about when interpreting our results,

4
00:00:10,790 --> 00:00:12,170
or some things that maybe we

5
00:00:12,170 --> 00:00:13,610
should think about when interpreting

6
00:00:13,610 --> 00:00:16,970
a study whether it was done well or poorly?

7
00:00:16,970 --> 00:00:20,110
Yes, there are. There's two that I want to emphasize.

8
00:00:20,110 --> 00:00:22,470
The first is simply the same when we've been

9
00:00:22,470 --> 00:00:24,700
talking about before, external validity.

10
00:00:24,700 --> 00:00:26,030
Who are your participants?

11
00:00:26,030 --> 00:00:27,885
Do your participants represent

12
00:00:27,885 --> 00:00:29,730
the people you want to understand?

13
00:00:29,730 --> 00:00:32,640
For instance, if I'm wanting to know whether

14
00:00:32,640 --> 00:00:36,365
increasing engagement results in increased performance,

15
00:00:36,365 --> 00:00:39,950
that's an excellent chance for me to run an experiment,

16
00:00:39,950 --> 00:00:42,000
try and engagement improvement program,

17
00:00:42,000 --> 00:00:43,500
see if performance improves.

18
00:00:43,500 --> 00:00:45,180
But I need to think about

19
00:00:45,180 --> 00:00:46,915
whether the participants that I've

20
00:00:46,915 --> 00:00:48,900
studied in that intervention

21
00:00:48,900 --> 00:00:50,980
are the people I want to apply this to.

22
00:00:50,980 --> 00:00:52,620
Just because it works with one group

23
00:00:52,620 --> 00:00:54,420
does not mean it will work everywhere.

24
00:00:54,420 --> 00:00:55,950
Just because it works in one industry

25
00:00:55,950 --> 00:00:57,285
doesn't mean it works everywhere.

26
00:00:57,285 --> 00:00:59,940
If I'm working in tech and I see that works,

27
00:00:59,940 --> 00:01:01,530
that might not actually apply,

28
00:01:01,530 --> 00:01:02,630
say, in the service sector.

29
00:01:02,630 --> 00:01:05,010
So depending on the industry that you're

30
00:01:05,010 --> 00:01:08,120
working in or the people who are your participants,

31
00:01:08,120 --> 00:01:10,740
you might do a lot of work to run

32
00:01:10,740 --> 00:01:12,120
an experiment only to find

33
00:01:12,120 --> 00:01:13,650
that the results don't generalize.

34
00:01:13,650 --> 00:01:16,160
So, just as I have been saying all along,

35
00:01:16,160 --> 00:01:18,270
recruit the people for your study

36
00:01:18,270 --> 00:01:21,365
who represent the people you want to make claims about.

37
00:01:21,365 --> 00:01:23,880
That is a central theme in all

38
00:01:23,880 --> 00:01:27,045
of this module and I think it bears repeating again.

39
00:01:27,045 --> 00:01:30,360
Another related issue that hasn't really

40
00:01:30,360 --> 00:01:33,560
come up before but it definitely comes up now is,

41
00:01:33,560 --> 00:01:36,600
does the intervention that you've done,

42
00:01:36,600 --> 00:01:37,990
the experiment that you've done,

43
00:01:37,990 --> 00:01:39,455
the AB test that you've done,

44
00:01:39,455 --> 00:01:41,010
does that actually seem

45
00:01:41,010 --> 00:01:43,505
realistic enough to apply generally?

46
00:01:43,505 --> 00:01:45,425
Let me give you an example.

47
00:01:45,425 --> 00:01:48,625
Say, we do our engagement improvement module,

48
00:01:48,625 --> 00:01:50,630
we improve people's engagement,

49
00:01:50,630 --> 00:01:53,160
does that actually represent the kinds

50
00:01:53,160 --> 00:01:55,890
of engagement effects I might see?

51
00:01:55,890 --> 00:01:57,840
Maybe I have to really really really

52
00:01:57,840 --> 00:02:00,100
really really engage people at

53
00:02:00,100 --> 00:02:01,500
exceedingly high level to get

54
00:02:01,500 --> 00:02:04,785
any effect. That's really great.

55
00:02:04,785 --> 00:02:06,000
I can know that improving

56
00:02:06,000 --> 00:02:08,220
engagement gives me some performance,

57
00:02:08,220 --> 00:02:10,320
but it's possible that it might require

58
00:02:10,320 --> 00:02:13,050
so much engagement that it's just not realistic,

59
00:02:13,050 --> 00:02:15,765
that's not realistic of the kinds of things

60
00:02:15,765 --> 00:02:17,595
that I would see in the real world

61
00:02:17,595 --> 00:02:18,960
in terms of engagement levels.

62
00:02:18,960 --> 00:02:21,480
Sure it might be great as an improvement program,

63
00:02:21,480 --> 00:02:23,190
but it might not accurately describe

64
00:02:23,190 --> 00:02:26,465
the engagement levels we see in the wild, so to speak.

65
00:02:26,465 --> 00:02:28,230
So, just something to think about,

66
00:02:28,230 --> 00:02:29,665
if you've got an experiment, sometimes

67
00:02:29,665 --> 00:02:31,320
they can get a little artificial.

68
00:02:31,320 --> 00:02:32,790
You want to make sure that what you're doing

69
00:02:32,790 --> 00:02:33,870
is realistic enough to

70
00:02:33,870 --> 00:02:37,155
the real world that your results are meaningful.

71
00:02:37,155 --> 00:02:39,830
If it's too contrived or it's too artificial,

72
00:02:39,830 --> 00:02:41,030
you may waste your time.

73
00:02:41,030 --> 00:02:43,170
And if your participants don't represent the people you

74
00:02:43,170 --> 00:02:45,475
want to actually be able to understand,

75
00:02:45,475 --> 00:02:47,810
you might be wasting your time and money.

76
00:02:47,810 --> 00:02:49,585
So, things to think about.

77
00:02:49,585 --> 00:02:51,765
A related issue that comes up

78
00:02:51,765 --> 00:02:53,660
with experiments is called the Confound.

79
00:02:53,660 --> 00:02:55,110
And not like confound it,

80
00:02:55,110 --> 00:02:56,895
not like frustration confound,

81
00:02:56,895 --> 00:02:59,450
but a statistical confound, the design confound.

82
00:02:59,450 --> 00:03:01,855
So what is a Confound? A Confound

83
00:03:01,855 --> 00:03:03,330
simply put is an accidental

84
00:03:03,330 --> 00:03:04,995
difference between your groups.

85
00:03:04,995 --> 00:03:06,930
Remember the reason we do

86
00:03:06,930 --> 00:03:08,920
an experiment is that you've got two groups;

87
00:03:08,920 --> 00:03:10,490
group one and two, maybe more,

88
00:03:10,490 --> 00:03:12,450
they're identical in every way

89
00:03:12,450 --> 00:03:14,420
except the thing you manipulate,

90
00:03:14,420 --> 00:03:15,560
the thing that you're testing,

91
00:03:15,560 --> 00:03:18,085
whether it's an engagement program or whatever.

92
00:03:18,085 --> 00:03:20,505
Let's use a logo design example.

93
00:03:20,505 --> 00:03:22,010
You've created two logos;

94
00:03:22,010 --> 00:03:23,130
logo A and logo B,

95
00:03:23,130 --> 00:03:25,150
and you want to compare them.

96
00:03:25,150 --> 00:03:28,065
What if just by happenstance,

97
00:03:28,065 --> 00:03:30,925
logo A is in color and logo B is in grayscale?

98
00:03:30,925 --> 00:03:33,210
That is a difference between those groups and it

99
00:03:33,210 --> 00:03:35,415
is a difference that you did not want to test.

100
00:03:35,415 --> 00:03:38,040
You're not interested in testing color versus grayscale,

101
00:03:38,040 --> 00:03:42,185
you're interested in testing design A and design B.

102
00:03:42,185 --> 00:03:43,970
This is what's called a Confound.

103
00:03:43,970 --> 00:03:45,335
It is an accidental difference.

104
00:03:45,335 --> 00:03:46,840
It's not the thing you want to study,

105
00:03:46,840 --> 00:03:49,295
but it is now a difference between the two groups.

106
00:03:49,295 --> 00:03:50,870
So how do you know if you see

107
00:03:50,870 --> 00:03:52,580
a difference what is due to?

108
00:03:52,580 --> 00:03:54,845
There's now two differences between your groups.

109
00:03:54,845 --> 00:03:57,140
So maybe people will prefer a logo A.

110
00:03:57,140 --> 00:03:58,465
Not because it's better,

111
00:03:58,465 --> 00:04:00,800
but because it's in color.

112
00:04:00,800 --> 00:04:03,595
I get really nervous about

113
00:04:03,595 --> 00:04:06,685
any systematic differences between my treatments.

114
00:04:06,685 --> 00:04:08,110
Think about this, are they

115
00:04:08,110 --> 00:04:09,700
run on different groups of people?

116
00:04:09,700 --> 00:04:12,520
Are they run at different times of day?

117
00:04:12,520 --> 00:04:14,745
Are the instructions different?

118
00:04:14,745 --> 00:04:17,940
Even subtle differences in wording can affect people.

119
00:04:17,940 --> 00:04:19,925
So these are some things to think about.

120
00:04:19,925 --> 00:04:23,965
Your groups should really be identical in every way.

121
00:04:23,965 --> 00:04:27,085
Think like scripted, like use a script.

122
00:04:27,085 --> 00:04:28,685
Keep the language the same.

123
00:04:28,685 --> 00:04:29,930
Keep instructions the same.

124
00:04:29,930 --> 00:04:31,960
Keep the time and date the same. The only difference

125
00:04:31,960 --> 00:04:33,220
should be the thing you want to test.

126
00:04:33,220 --> 00:04:34,360
If you adhere to that

127
00:04:34,360 --> 00:04:37,750
strict scientifically rigorous level

128
00:04:37,750 --> 00:04:39,120
of intervention effectiveness,

129
00:04:39,120 --> 00:04:42,615
you will be able to draw great cause effect conclusions.

130
00:04:42,615 --> 00:04:44,020
You will be able to anticipate

131
00:04:44,020 --> 00:04:46,000
the effects of your future interventions.

132
00:04:46,000 --> 00:04:49,480
This is really how we got science as it is today.

133
00:04:49,480 --> 00:04:52,300
We have phenomenally great innovations

134
00:04:52,300 --> 00:04:53,990
in this modern era.

135
00:04:53,990 --> 00:04:55,750
Thanks to a history of people being

136
00:04:55,750 --> 00:04:57,835
rigorous with their experiments.

137
00:04:57,835 --> 00:04:59,120
And you can do it too.

138
00:04:59,120 --> 00:05:01,750
It doesn't take a lot to run a rigorous experiment,

139
00:05:01,750 --> 00:05:03,640
but we do need to think through and keep

140
00:05:03,640 --> 00:05:06,100
our groups the same so we avoid those confounds.

141
00:05:06,100 --> 00:05:07,330
With that, we can draw

142
00:05:07,330 --> 00:05:10,320
some amazing cause effect conclusions.

