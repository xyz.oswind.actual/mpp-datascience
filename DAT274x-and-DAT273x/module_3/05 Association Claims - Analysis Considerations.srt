0
00:00:01,490 --> 00:00:03,690
>> Okay. So, you have

1
00:00:03,690 --> 00:00:05,670
your data and now you're ready to make

2
00:00:05,670 --> 00:00:07,290
sense and present some sort of

3
00:00:07,290 --> 00:00:09,670
association claim. How do you do it?

4
00:00:09,670 --> 00:00:11,460
I want to suggest to you that there are

5
00:00:11,460 --> 00:00:14,170
a number of different data analysis options.

6
00:00:14,170 --> 00:00:17,160
Therefore this can become kind of confusing.

7
00:00:17,160 --> 00:00:18,600
So, I want to present kind of

8
00:00:18,600 --> 00:00:19,620
a road map for

9
00:00:19,620 --> 00:00:21,930
the different data analyses you might pursue.

10
00:00:21,930 --> 00:00:23,730
We don't really have the time to teach

11
00:00:23,730 --> 00:00:25,530
each of these analyses in detail the way you

12
00:00:25,530 --> 00:00:27,090
would in a stat's course but at

13
00:00:27,090 --> 00:00:29,010
least you should have a good menu

14
00:00:29,010 --> 00:00:30,480
of options to choose from and

15
00:00:30,480 --> 00:00:32,640
certainly you could look up the option that you need,

16
00:00:32,640 --> 00:00:34,850
or study them more in a future course.

17
00:00:34,850 --> 00:00:38,305
So, let's get into it. The first situation

18
00:00:38,305 --> 00:00:40,030
probably the most common is in which you

19
00:00:40,030 --> 00:00:42,305
have two different continuous variables.

20
00:00:42,305 --> 00:00:45,790
Continuous meaning scores can take a range of values,

21
00:00:45,790 --> 00:00:47,080
for instance, engagement and

22
00:00:47,080 --> 00:00:49,360
performance as we've already talked about.

23
00:00:49,360 --> 00:00:50,710
In this case, I have people who have

24
00:00:50,710 --> 00:00:53,410
a range of different engagement levels and a range of

25
00:00:53,410 --> 00:00:55,754
different performance levels and I can directly

26
00:00:55,754 --> 00:00:59,560
correlate them in an analysis like you see on the screen.

27
00:00:59,560 --> 00:01:00,940
One option would be to simply

28
00:01:00,940 --> 00:01:02,830
compute a correlation coefficient.

29
00:01:02,830 --> 00:01:04,385
These are useful tools,

30
00:01:04,385 --> 00:01:05,920
they range from zero to one,

31
00:01:05,920 --> 00:01:07,435
they can be positive or negative

32
00:01:07,435 --> 00:01:10,415
nicely summarizes the strength of relationship.

33
00:01:10,415 --> 00:01:12,220
Another option might be to do

34
00:01:12,220 --> 00:01:14,140
a regression in which we actually find

35
00:01:14,140 --> 00:01:15,370
the trend line and plot it on

36
00:01:15,370 --> 00:01:18,165
the screen such as what you just see here.

37
00:01:18,165 --> 00:01:20,690
I really would advocate for

38
00:01:20,690 --> 00:01:22,670
regression analysis in most cases,

39
00:01:22,670 --> 00:01:24,280
the reason why is you can

40
00:01:24,280 --> 00:01:27,550
directly plot it visually so that people who

41
00:01:27,550 --> 00:01:30,100
are looking at your results who might not necessarily

42
00:01:30,100 --> 00:01:32,410
understand the data science and statistical models

43
00:01:32,410 --> 00:01:33,610
that you're going to run,

44
00:01:33,610 --> 00:01:35,275
can at least visualize it.

45
00:01:35,275 --> 00:01:37,180
Here I see that as engagement goes up,

46
00:01:37,180 --> 00:01:39,600
there is a corresponding change in performance but

47
00:01:39,600 --> 00:01:43,180
engagement looks it's relatively weak.

48
00:01:43,180 --> 00:01:46,180
You could also define an exact relationship for

49
00:01:46,180 --> 00:01:47,375
every one point of

50
00:01:47,375 --> 00:01:50,760
engagement I go up so much performance.

51
00:01:50,760 --> 00:01:53,380
I can do that by just describing the slope of the line.

52
00:01:53,380 --> 00:01:57,950
So, regression is an excellent tool in this situation.

53
00:01:58,010 --> 00:02:01,745
What is this? We are comparing two different groups here.

54
00:02:01,745 --> 00:02:03,290
You might think to yourself this

55
00:02:03,290 --> 00:02:04,835
isn't an association claim.

56
00:02:04,835 --> 00:02:07,500
We're looking at the difference between two groups,

57
00:02:07,500 --> 00:02:09,890
but actually no this is exactly the same,

58
00:02:09,890 --> 00:02:11,330
because indeed I have not

59
00:02:11,330 --> 00:02:13,000
manipulated either of these variables.

60
00:02:13,000 --> 00:02:14,170
I've just measured them,

61
00:02:14,170 --> 00:02:16,650
I measured how much experience people have,

62
00:02:16,650 --> 00:02:18,170
I've got two discrete groups now,

63
00:02:18,170 --> 00:02:20,390
it's not continuous it's just two groups,

64
00:02:20,390 --> 00:02:21,590
but I did measure that

65
00:02:21,590 --> 00:02:24,240
variable and I measured their satisfaction.

66
00:02:24,240 --> 00:02:27,455
So, although the graph looks different it is in fact

67
00:02:27,455 --> 00:02:29,359
exactly the same situation

68
00:02:29,359 --> 00:02:31,370
as we saw in the previous slide,

69
00:02:31,370 --> 00:02:34,145
as one variable increases in this case

70
00:02:34,145 --> 00:02:35,870
experience the other variable

71
00:02:35,870 --> 00:02:37,800
is also increasing satisfaction.

72
00:02:37,800 --> 00:02:39,860
So, although we might describe

73
00:02:39,860 --> 00:02:41,840
this as a test of difference we might

74
00:02:41,840 --> 00:02:46,235
use a tool like t-test or an ANOVA to analyze this data.

75
00:02:46,235 --> 00:02:48,250
We could also use a regression.

76
00:02:48,250 --> 00:02:50,375
It's really the same situation,

77
00:02:50,375 --> 00:02:52,200
I'm looking as one variable changes

78
00:02:52,200 --> 00:02:54,640
the other one tends to change as well.

79
00:02:54,640 --> 00:02:57,360
Now, the nice thing is is if you like doing regressions,

80
00:02:57,360 --> 00:02:59,150
and in fact if you've learned regression as one of

81
00:02:59,150 --> 00:03:00,800
your main statistical and data

82
00:03:00,800 --> 00:03:02,705
science tools you're in good shape here.

83
00:03:02,705 --> 00:03:05,285
You can actually do the exact same analysis

84
00:03:05,285 --> 00:03:07,915
with this data as you can on the previous slide.

85
00:03:07,915 --> 00:03:10,115
All you do, you look at whether

86
00:03:10,115 --> 00:03:12,340
a one unit increase in experience in

87
00:03:12,340 --> 00:03:14,200
this case going from none to some

88
00:03:14,200 --> 00:03:16,490
predicts a change in satisfaction.

89
00:03:16,490 --> 00:03:19,250
So, it is exactly the same analysis.

90
00:03:19,250 --> 00:03:21,710
There are a lot of options for analyzing this kind of

91
00:03:21,710 --> 00:03:24,455
data but regression is a tried and true technique.

92
00:03:24,455 --> 00:03:27,020
I do want to point out to you though that this is

93
00:03:27,020 --> 00:03:28,940
the same situation and people do often

94
00:03:28,940 --> 00:03:31,020
get confused because one thing looks like it's a trend,

95
00:03:31,020 --> 00:03:32,030
another one looks like it's

96
00:03:32,030 --> 00:03:34,100
differences but if you can wrap your mind

97
00:03:34,100 --> 00:03:35,150
around this it is actually

98
00:03:35,150 --> 00:03:38,360
the exact same claim just a different data type.

99
00:03:39,660 --> 00:03:42,485
Here we have a third option,

100
00:03:42,485 --> 00:03:44,305
here we now have data where you have

101
00:03:44,305 --> 00:03:46,780
categorical data in two different dimensions.

102
00:03:46,780 --> 00:03:48,300
So, now I'm looking at whether people

103
00:03:48,300 --> 00:03:50,515
have some or no experience.

104
00:03:50,515 --> 00:03:53,430
Let's just say with my online course topic.

105
00:03:53,430 --> 00:03:55,665
I'm teaching you some online course

106
00:03:55,665 --> 00:03:58,310
and people have some experience or no experience.

107
00:03:58,310 --> 00:04:00,435
I want to know if that differs by coast.

108
00:04:00,435 --> 00:04:02,670
Maybe I think people on the East Coast might have

109
00:04:02,670 --> 00:04:03,840
different amounts of experience

110
00:04:03,840 --> 00:04:05,845
than people on the West Coast.

111
00:04:05,845 --> 00:04:08,900
But, here now I've got outcomes,

112
00:04:08,900 --> 00:04:10,470
my outcome is categorical

113
00:04:10,470 --> 00:04:12,785
and my predictor is categorical.

114
00:04:12,785 --> 00:04:14,160
Both variables or categories,

115
00:04:14,160 --> 00:04:16,230
I don't have any continuous variables here.

116
00:04:16,230 --> 00:04:18,030
Nothing is measured in numbers,

117
00:04:18,030 --> 00:04:19,290
I have people who have none or some,

118
00:04:19,290 --> 00:04:21,120
and I have East Coast and West Coast there are

119
00:04:21,120 --> 00:04:22,980
no numbers to this data analysis.

120
00:04:22,980 --> 00:04:26,160
So, how do we analyze this and is this really the same?

121
00:04:26,160 --> 00:04:27,930
In fact it actually is

122
00:04:27,930 --> 00:04:30,200
the exact same situation once again.

123
00:04:30,200 --> 00:04:31,920
We are looking at whether

124
00:04:31,920 --> 00:04:34,010
a systematic change in one variable,

125
00:04:34,010 --> 00:04:35,849
say East Coast or West Coast,

126
00:04:35,849 --> 00:04:38,850
corresponds to a systematic change in the other variable,

127
00:04:38,850 --> 00:04:40,665
say the number of people with experience.

128
00:04:40,665 --> 00:04:42,480
So, it's the same question as

129
00:04:42,480 --> 00:04:45,060
one increases the other tends to change.

130
00:04:45,060 --> 00:04:47,710
The analysis in this case is a little different,

131
00:04:47,710 --> 00:04:49,705
you would not be using a regression here,

132
00:04:49,705 --> 00:04:51,000
we would be actually be doing what's

133
00:04:51,000 --> 00:04:52,440
called a contingency table

134
00:04:52,440 --> 00:04:55,280
analysis or Chi-squared test.

135
00:04:55,280 --> 00:04:57,810
I simply put, I could calculate some percentages.

136
00:04:57,810 --> 00:04:59,330
Let's look at the East Coasters.

137
00:04:59,330 --> 00:05:01,380
Okay. The East Coast I see that

138
00:05:01,380 --> 00:05:05,100
only about 34 percent of my people have no experience,

139
00:05:05,100 --> 00:05:07,290
like 30 out of that row

140
00:05:07,290 --> 00:05:09,270
total for the East Coasters 30 percent of

141
00:05:09,270 --> 00:05:11,655
people sorry 34 percent have

142
00:05:11,655 --> 00:05:14,360
no experience for the East Coast.

143
00:05:14,360 --> 00:05:15,630
For the West Coast,

144
00:05:15,630 --> 00:05:17,090
in fact it's a little different.

145
00:05:17,090 --> 00:05:18,690
In the West Coast only 16 percent

146
00:05:18,690 --> 00:05:20,195
of people lack experience.

147
00:05:20,195 --> 00:05:21,660
So, depending on the Coast we

148
00:05:21,660 --> 00:05:23,155
see some different percentages,

149
00:05:23,155 --> 00:05:25,240
34 percent and 16 percent.

150
00:05:25,240 --> 00:05:27,275
Are those different? Well, we can

151
00:05:27,275 --> 00:05:29,405
easily do a chi-square test to find out.

152
00:05:29,405 --> 00:05:31,290
The chi-square test can be done in

153
00:05:31,290 --> 00:05:33,700
nearly any data analysis tool.

154
00:05:33,700 --> 00:05:36,340
Going through the math in the equations beyond this are

155
00:05:36,340 --> 00:05:38,655
a little outside of the scope of this lesson,

156
00:05:38,655 --> 00:05:40,650
but, the analysis can be

157
00:05:40,650 --> 00:05:42,975
easily done for you in virtually any software.

158
00:05:42,975 --> 00:05:46,425
Simply put there is a P value to it, it's 0.2,

159
00:05:46,425 --> 00:05:48,060
that is only a two percent risk I could have

160
00:05:48,060 --> 00:05:50,360
gotten a difference this big just by chance.

161
00:05:50,360 --> 00:05:51,490
So, I'm going to in further

162
00:05:51,490 --> 00:05:53,085
really is a Coastal difference here.

163
00:05:53,085 --> 00:05:54,600
That's really interesting to know.

164
00:05:54,600 --> 00:05:56,010
Well, there is a difference and

165
00:05:56,010 --> 00:05:58,585
experience by Coast, that's neat.

166
00:05:58,585 --> 00:06:00,315
You certainly could present

167
00:06:00,315 --> 00:06:01,710
these two different percentages,

168
00:06:01,710 --> 00:06:03,150
in fact percentages are

169
00:06:03,150 --> 00:06:05,220
a common way of describing these kinds of

170
00:06:05,220 --> 00:06:07,230
relationships but you could also

171
00:06:07,230 --> 00:06:08,610
actually translate this into

172
00:06:08,610 --> 00:06:10,625
something like a correlation.

173
00:06:10,625 --> 00:06:12,540
If you take your chi-squared divide by

174
00:06:12,540 --> 00:06:14,400
sample size and square root,

175
00:06:14,400 --> 00:06:15,550
which you can play with in

176
00:06:15,550 --> 00:06:17,025
the lab allow we don't have to do it right here,

177
00:06:17,025 --> 00:06:18,230
but you just put some numbers in

178
00:06:18,230 --> 00:06:20,110
right our chi-square is 5.36,

179
00:06:20,110 --> 00:06:23,370
we are 162 people which I know because I counted them up,

180
00:06:23,370 --> 00:06:25,645
it gives me a value of about 0.18.

181
00:06:25,645 --> 00:06:28,080
This is an under use statistic but it essentially

182
00:06:28,080 --> 00:06:30,420
allows you to present this result,

183
00:06:30,420 --> 00:06:32,995
the same way you would at any other correlation result,

184
00:06:32,995 --> 00:06:36,190
it ranges from zero to one and is a useful tool.

185
00:06:36,190 --> 00:06:39,390
So, here we go, three different kinds of analyses,

186
00:06:39,390 --> 00:06:42,165
one with cross tabs or contingency tables,

187
00:06:42,165 --> 00:06:43,620
one with group differences,

188
00:06:43,620 --> 00:06:45,000
and one with trend lines

189
00:06:45,000 --> 00:06:46,665
but in fact they're all the same.

190
00:06:46,665 --> 00:06:49,260
We're describing relationships between variables.

191
00:06:49,260 --> 00:06:51,030
We're saying that as one changes we

192
00:06:51,030 --> 00:06:53,970
tend to see a change in the other.

