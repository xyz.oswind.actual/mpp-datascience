0
00:00:00,930 --> 00:00:03,825
>> Hello and welcome to this lesson,

1
00:00:03,825 --> 00:00:05,090
where we're going to talk about

2
00:00:05,090 --> 00:00:07,060
the psychology of providing data.

3
00:00:07,060 --> 00:00:08,690
Now, I want you to imagine for

4
00:00:08,690 --> 00:00:10,250
a moment that you are a part of

5
00:00:10,250 --> 00:00:12,260
a research team or you're designing

6
00:00:12,260 --> 00:00:14,510
a study and you're now ready to give out a survey,

7
00:00:14,510 --> 00:00:16,590
you're ready to deploy a survey to a lot of people.

8
00:00:16,590 --> 00:00:19,505
We want that survey to give us valid answers.

9
00:00:19,505 --> 00:00:21,180
We've already got our question framed,

10
00:00:21,180 --> 00:00:22,434
we already have a theory,

11
00:00:22,434 --> 00:00:23,600
we already have our measures

12
00:00:23,600 --> 00:00:25,140
picked out for the most part,

13
00:00:25,140 --> 00:00:26,660
but you have a chance to look

14
00:00:26,660 --> 00:00:28,760
over it and I want you to think about

15
00:00:28,760 --> 00:00:30,350
some of the issues that might come up

16
00:00:30,350 --> 00:00:32,165
when people are actually given this

17
00:00:32,165 --> 00:00:34,730
survey.The reason we need to stop and

18
00:00:34,730 --> 00:00:37,410
think about this before we deploy is that,

19
00:00:37,410 --> 00:00:39,650
often when we're writing surveys we just

20
00:00:39,650 --> 00:00:42,660
assume we can ask people a question and we get an answer.

21
00:00:42,660 --> 00:00:44,370
And the reason we assume that is

22
00:00:44,370 --> 00:00:45,840
that is our frame of reference.

23
00:00:45,840 --> 00:00:49,675
I have a question, I get an answer, seems simple.

24
00:00:49,675 --> 00:00:51,870
The problem is that's not how people work.

25
00:00:51,870 --> 00:00:54,990
So, you might ask people why is it that you do what you

26
00:00:54,990 --> 00:00:56,550
do and they may give you

27
00:00:56,550 --> 00:00:59,045
an answer and it may be completely wrong.

28
00:00:59,045 --> 00:01:01,620
So, we really just can't trust that people

29
00:01:01,620 --> 00:01:04,220
are going to be robotic givers of information.

30
00:01:04,220 --> 00:01:06,060
We need to actually stop and think from

31
00:01:06,060 --> 00:01:08,100
a participant's perspective how

32
00:01:08,100 --> 00:01:10,590
that survey is going to get answered.

33
00:01:10,590 --> 00:01:12,780
The vast majority of surveys I

34
00:01:12,780 --> 00:01:14,760
see given out in any sort of

35
00:01:14,760 --> 00:01:17,520
business context violate one or more

36
00:01:17,520 --> 00:01:19,620
of the rules I'm about to teach you.

37
00:01:19,620 --> 00:01:22,949
That is a problem because when that data comes back,

38
00:01:22,949 --> 00:01:25,850
it gives untrustworthy conclusions.

39
00:01:25,850 --> 00:01:27,285
The last thing we want

40
00:01:27,285 --> 00:01:29,160
is for us to stop trusting our data.

41
00:01:29,160 --> 00:01:30,600
Because if we don't trust our data,

42
00:01:30,600 --> 00:01:32,390
we're going to ignore it and then we're going to make

43
00:01:32,390 --> 00:01:35,000
really bad decisions at least some of the time.

44
00:01:35,000 --> 00:01:37,290
What we want is trustworthy data so that when we

45
00:01:37,290 --> 00:01:39,700
go to make decisions we make good decisions.

46
00:01:39,700 --> 00:01:41,400
So, we need to stop and

47
00:01:41,400 --> 00:01:44,130
think before the data collection starts.

48
00:01:44,130 --> 00:01:47,455
How can I get valid answers to my questions?

49
00:01:47,455 --> 00:01:50,280
The simple answer is that people are bias

50
00:01:50,280 --> 00:01:51,840
and they are highly influenced

51
00:01:51,840 --> 00:01:53,475
by the way we ask questions.

52
00:01:53,475 --> 00:01:55,325
They're influenced by design decisions,

53
00:01:55,325 --> 00:01:56,550
they're influenced by the order

54
00:01:56,550 --> 00:01:57,900
in which we ask questions.

55
00:01:57,900 --> 00:01:59,550
People will get strung along by

56
00:01:59,550 --> 00:02:00,990
your survey and they may give you

57
00:02:00,990 --> 00:02:02,650
answers that they think you want to hear.

58
00:02:02,650 --> 00:02:03,780
So, if we understand

59
00:02:03,780 --> 00:02:05,970
the psychology of filling out a survey,

60
00:02:05,970 --> 00:02:08,775
we can actually design good surveys.

61
00:02:08,775 --> 00:02:12,205
So, the first and foremost rule of

62
00:02:12,205 --> 00:02:13,540
survey psychology is that

63
00:02:13,540 --> 00:02:15,940
people want to manage their impressions.

64
00:02:15,940 --> 00:02:17,545
They want to look good to you

65
00:02:17,545 --> 00:02:19,885
and they want to look good to themselves.

66
00:02:19,885 --> 00:02:22,260
If I'm filling out a survey about for

67
00:02:22,260 --> 00:02:24,795
example "My attitude towards green products",

68
00:02:24,795 --> 00:02:26,545
I might immediately think,

69
00:02:26,545 --> 00:02:28,590
well the person giving me

70
00:02:28,590 --> 00:02:30,930
this survey is selling a green product.

71
00:02:30,930 --> 00:02:33,810
So, of course they believe it's a good thing to do.

72
00:02:33,810 --> 00:02:36,465
I don't want to say I don't like the environment.

73
00:02:36,465 --> 00:02:38,310
I don't want to express those attitudes to

74
00:02:38,310 --> 00:02:40,870
you because I think you would not like that.

75
00:02:40,870 --> 00:02:44,490
You might judge me and I would never want to be judged.

76
00:02:44,490 --> 00:02:47,070
So, the first rule of writing

77
00:02:47,070 --> 00:02:48,570
a survey is always going to

78
00:02:48,570 --> 00:02:50,805
be that you need to sound neutral.

79
00:02:50,805 --> 00:02:53,815
If you sound like you have a perspective in that survey,

80
00:02:53,815 --> 00:02:56,060
that is my brand, people are

81
00:02:56,060 --> 00:02:58,230
immediately going to say, "Yeah."

82
00:02:58,230 --> 00:03:01,940
But they're not going to tell you that it's kind of

83
00:03:01,940 --> 00:03:04,250
fake or worse they may

84
00:03:04,250 --> 00:03:06,935
actually want to seem good to themselves.

85
00:03:06,935 --> 00:03:10,205
They may have their own ideas about how

86
00:03:10,205 --> 00:03:11,810
they should be and they might not want

87
00:03:11,810 --> 00:03:14,260
to answer those questions honestly.

88
00:03:14,260 --> 00:03:16,160
If you ask people how often do they

89
00:03:16,160 --> 00:03:18,550
recycle they might say all the time.

90
00:03:18,550 --> 00:03:20,710
Not knowing that they're really

91
00:03:20,710 --> 00:03:22,565
telling you how they think they are but

92
00:03:22,565 --> 00:03:24,400
really they don't recycle all the time in fact they

93
00:03:24,400 --> 00:03:25,810
probably don't recycle nearly

94
00:03:25,810 --> 00:03:27,065
as much as they think they do.

95
00:03:27,065 --> 00:03:29,500
Why? Because people tell you how they want to

96
00:03:29,500 --> 00:03:32,020
see themselves not how they actually are.

97
00:03:32,020 --> 00:03:33,910
So, those are some things to think about.

98
00:03:33,910 --> 00:03:35,860
When we're asking questions you never want them

99
00:03:35,860 --> 00:03:37,890
to seem loaded in any way.

100
00:03:37,890 --> 00:03:39,280
You never want them to imply

101
00:03:39,280 --> 00:03:40,780
that there's a right answer or

102
00:03:40,780 --> 00:03:42,560
a valid answer or that there's

103
00:03:42,560 --> 00:03:44,635
something you're looking for.

104
00:03:44,635 --> 00:03:46,450
You just want to take the temperature

105
00:03:46,450 --> 00:03:47,900
of whatever it is that you're measuring.

106
00:03:47,900 --> 00:03:50,110
So, although it might seem a little dry,

107
00:03:50,110 --> 00:03:52,210
sounding objective is definitely

108
00:03:52,210 --> 00:03:55,165
better than sounding like you have a perspective.

109
00:03:55,165 --> 00:03:58,530
This leads to something called a demand characteristic.

110
00:03:58,530 --> 00:04:00,030
And a demand characteristic is

111
00:04:00,030 --> 00:04:03,465
where there is some piece of that study that

112
00:04:03,465 --> 00:04:05,430
suggests there is a right answer and it

113
00:04:05,430 --> 00:04:07,380
is next to impossible to

114
00:04:07,380 --> 00:04:09,675
actually participate in a study

115
00:04:09,675 --> 00:04:12,205
where this is a problem and give a valid answer.

116
00:04:12,205 --> 00:04:14,810
Imagine that you're testing a new brand logo out.

117
00:04:14,810 --> 00:04:16,440
So, your company has developed

118
00:04:16,440 --> 00:04:19,180
a new logo for a product line, you want to test it out.

119
00:04:19,180 --> 00:04:22,910
So, you go ahead and you show participants the logos,

120
00:04:22,910 --> 00:04:25,395
say here's the new logo and it's clear

121
00:04:25,395 --> 00:04:28,030
that the company that made the logo is giving it to you.

122
00:04:28,030 --> 00:04:29,695
So I know this is their new logo

123
00:04:29,695 --> 00:04:31,665
and they say, is it great?

124
00:04:31,665 --> 00:04:34,450
And you are going to have a hard time saying, ''No,

125
00:04:34,450 --> 00:04:36,050
this logo is not great,

126
00:04:36,050 --> 00:04:37,530
I don't like this logo much at all.''

127
00:04:37,530 --> 00:04:39,080
You can have a hard time saying that

128
00:04:39,080 --> 00:04:40,840
because it's very clear that

129
00:04:40,840 --> 00:04:44,705
there is an expectation that I respond positively.

130
00:04:44,705 --> 00:04:46,775
Think about social situations.

131
00:04:46,775 --> 00:04:48,815
Your survey is a social interaction.

132
00:04:48,815 --> 00:04:50,760
If you were talking to this person one on one,

133
00:04:50,760 --> 00:04:52,545
they're going to want to be polite to you,

134
00:04:52,545 --> 00:04:54,980
they're going to want to let

135
00:04:54,980 --> 00:04:56,910
you down easy if they don't like you.

136
00:04:56,910 --> 00:04:58,310
Like they're not going to be

137
00:04:58,310 --> 00:05:00,590
honest with you about those things.

138
00:05:00,590 --> 00:05:02,360
It's much better in fact if you

139
00:05:02,360 --> 00:05:04,610
don't look like the company that made the logo.

140
00:05:04,610 --> 00:05:06,200
It's much better if you kind of

141
00:05:06,200 --> 00:05:07,880
disguise that a little bit,

142
00:05:07,880 --> 00:05:09,240
"Hey, here's a graphic.

143
00:05:09,240 --> 00:05:10,100
We're really interested in

144
00:05:10,100 --> 00:05:11,230
your perceptions of the graphic."

145
00:05:11,230 --> 00:05:12,410
Don't call it a logo.

146
00:05:12,410 --> 00:05:13,745
Just call it a graphic.

147
00:05:13,745 --> 00:05:15,980
Ask people their opinions without

148
00:05:15,980 --> 00:05:18,410
having some implied objective.

149
00:05:18,410 --> 00:05:21,295
"Oh, we want to know your perceptions of this logo.

150
00:05:21,295 --> 00:05:22,870
Hear some negative adjectives,

151
00:05:22,870 --> 00:05:24,110
hear some positive adjectives".

152
00:05:24,110 --> 00:05:25,885
Tell us how much it matches those.

153
00:05:25,885 --> 00:05:28,040
The more you sound objective,

154
00:05:28,040 --> 00:05:29,555
like you have less of a perspective,

155
00:05:29,555 --> 00:05:31,510
you won't have these demand characteristics.

156
00:05:31,510 --> 00:05:32,885
You could immediately think about

157
00:05:32,885 --> 00:05:34,400
the disasters that could have been

158
00:05:34,400 --> 00:05:38,730
averted simply by adhering to this rule.

159
00:05:38,790 --> 00:05:41,840
Related to this, in general,

160
00:05:41,840 --> 00:05:43,715
people are really good at

161
00:05:43,715 --> 00:05:46,010
telling what answer they think you're looking for.

162
00:05:46,010 --> 00:05:48,860
They've got really good detectors for this.

163
00:05:48,860 --> 00:05:50,960
If there's any hint

164
00:05:50,960 --> 00:05:53,170
they know what it is you're going for,

165
00:05:53,170 --> 00:05:55,260
they'll try to support you in

166
00:05:55,260 --> 00:05:57,455
that or sometimes they'll try to sabotage you.

167
00:05:57,455 --> 00:05:59,680
Researchers acknowledge both tendencies.

168
00:05:59,680 --> 00:06:02,330
So, sometimes we call the good participant effect.

169
00:06:02,330 --> 00:06:04,580
Maybe you're AB testing things,

170
00:06:04,580 --> 00:06:07,340
maybe you've got a grayscale logo and a color logo.

171
00:06:07,340 --> 00:06:09,230
And if people see both they might

172
00:06:09,230 --> 00:06:11,360
immediately know they're trying to

173
00:06:11,360 --> 00:06:13,505
figure out if the color logo is better

174
00:06:13,505 --> 00:06:16,790
than the grayscale logo and they might play along,

175
00:06:16,790 --> 00:06:17,914
be a good participant

176
00:06:17,914 --> 00:06:19,760
because they think that's what you're trying to do.

177
00:06:19,760 --> 00:06:21,830
I therefore might be careful not to

178
00:06:21,830 --> 00:06:24,930
show a grayscale logo next to a color logo and say,

179
00:06:24,930 --> 00:06:26,225
"Which do you like better?"

180
00:06:26,225 --> 00:06:28,910
I might be careful not to do that or at least if I do,

181
00:06:28,910 --> 00:06:30,620
I might try to disguise it a little bit,

182
00:06:30,620 --> 00:06:32,560
put in a few other logos for instance,

183
00:06:32,560 --> 00:06:34,190
in the mix so that it's

184
00:06:34,190 --> 00:06:36,020
not immediately apparent that I want to

185
00:06:36,020 --> 00:06:37,370
know if you like this one better

186
00:06:37,370 --> 00:06:38,900
than that one because that's going to

187
00:06:38,900 --> 00:06:42,080
help reduce that demand characteristic.

188
00:06:42,080 --> 00:06:44,060
Also keep in mind some participants

189
00:06:44,060 --> 00:06:45,230
won't want to help you,

190
00:06:45,230 --> 00:06:46,565
they will like to sabotage you.

191
00:06:46,565 --> 00:06:48,240
There are trolls out there.

192
00:06:48,240 --> 00:06:50,615
So, we really want to be careful.

193
00:06:50,615 --> 00:06:52,275
If you sound objective,

194
00:06:52,275 --> 00:06:54,730
you will avoid the problem.

