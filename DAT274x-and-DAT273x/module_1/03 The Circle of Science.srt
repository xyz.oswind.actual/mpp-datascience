0
00:00:00,000 --> 00:00:03,550
>> Hello. In this lesson,

1
00:00:03,550 --> 00:00:05,770
I want to talk to you about the research process in

2
00:00:05,770 --> 00:00:07,570
a little bit more detail in what I

3
00:00:07,570 --> 00:00:09,775
call the circle of science.

4
00:00:09,775 --> 00:00:11,470
The circle of science is a nice,

5
00:00:11,470 --> 00:00:13,660
useful way to visualize and think about

6
00:00:13,660 --> 00:00:15,469
the entire research process

7
00:00:15,469 --> 00:00:17,805
from initial question to final insights.

8
00:00:17,805 --> 00:00:19,270
And in this way, we can think about it as

9
00:00:19,270 --> 00:00:21,015
a problem solving process.

10
00:00:21,015 --> 00:00:24,020
I'm going to apply this to an actual research example.

11
00:00:24,020 --> 00:00:27,160
So, I want you to imagine for the moment that

12
00:00:27,160 --> 00:00:30,884
you work for a marketing company that has a product,

13
00:00:30,884 --> 00:00:33,265
specifically some green products

14
00:00:33,265 --> 00:00:34,750
that are not selling very well.

15
00:00:34,750 --> 00:00:36,100
So, this company has developed

16
00:00:36,100 --> 00:00:38,880
some green products that are environmentally friendly,

17
00:00:38,880 --> 00:00:41,339
they've got all these benefits and advantages,

18
00:00:41,339 --> 00:00:43,310
or initial focus groups suggested,

19
00:00:43,310 --> 00:00:45,050
"Yes, people really like these things.

20
00:00:45,050 --> 00:00:47,060
They like supporting the environment,

21
00:00:47,060 --> 00:00:48,250
they feel good, they feel really

22
00:00:48,250 --> 00:00:50,135
green when they buy them."

23
00:00:50,135 --> 00:00:51,190
We've deployed them to

24
00:00:51,190 --> 00:00:52,540
stores and they're just not selling,

25
00:00:52,540 --> 00:00:53,770
so we want to understand

26
00:00:53,770 --> 00:00:56,320
why it is that these things aren't selling.

27
00:00:56,320 --> 00:00:58,840
More generally, we're finding this across the board,

28
00:00:58,840 --> 00:01:00,310
"Our green products are just not selling,

29
00:01:00,310 --> 00:01:03,210
so we want to understand what's going on."

30
00:01:03,210 --> 00:01:04,420
So, the first thing we're going to

31
00:01:04,420 --> 00:01:06,250
do is we're going to develop a theory.

32
00:01:06,250 --> 00:01:07,570
We're going to try to develop

33
00:01:07,570 --> 00:01:10,815
some ideas as to what we think is going on.

34
00:01:10,815 --> 00:01:12,475
I think this bears repeating.

35
00:01:12,475 --> 00:01:14,530
You really want to spend some time here.

36
00:01:14,530 --> 00:01:17,070
You don't want to jump straight into a survey or

37
00:01:17,070 --> 00:01:19,510
some data collection processes without actually

38
00:01:19,510 --> 00:01:22,195
stopping to think about why this is happening.

39
00:01:22,195 --> 00:01:25,180
So for instance, we might go out and read.

40
00:01:25,180 --> 00:01:27,655
There's really a lot to be said about reading.

41
00:01:27,655 --> 00:01:29,980
People have said a lot about the topics

42
00:01:29,980 --> 00:01:32,930
that we're inevitably thinking and caring about already,

43
00:01:32,930 --> 00:01:34,570
so I like to start there.

44
00:01:34,570 --> 00:01:35,935
There's also something to be said for

45
00:01:35,935 --> 00:01:37,780
balancing ideas off people.

46
00:01:37,780 --> 00:01:40,555
Inevitably, my spouse always has

47
00:01:40,555 --> 00:01:43,470
the killer idea for whatever it is that I'm working on.

48
00:01:43,470 --> 00:01:45,560
Now, she's not my area,

49
00:01:45,560 --> 00:01:47,120
but she gets people.

50
00:01:47,120 --> 00:01:48,820
And really, when you think about it,

51
00:01:48,820 --> 00:01:50,530
most of what we're doing involves

52
00:01:50,530 --> 00:01:52,540
people in one way she'll perform.

53
00:01:52,540 --> 00:01:54,670
So maybe, my theory is that

54
00:01:54,670 --> 00:01:57,900
people in our initial focus groups were saying, "Yes.

55
00:01:57,900 --> 00:01:59,675
Rah! Rah! I like green products.

56
00:01:59,675 --> 00:02:02,145
Yes. But when we get into the moment,

57
00:02:02,145 --> 00:02:04,845
those green products are a little bit more costly.

58
00:02:04,845 --> 00:02:06,160
There's other ways we can feel good

59
00:02:06,160 --> 00:02:07,780
about ourselves than buying a green product."

60
00:02:07,780 --> 00:02:09,970
So, what we've got is

61
00:02:09,970 --> 00:02:12,630
a theory now that people want to feel green,

62
00:02:12,630 --> 00:02:14,680
they just don't want the costs of being green.

63
00:02:14,680 --> 00:02:17,750
So, let's go ahead and now test that theory.

64
00:02:17,750 --> 00:02:20,480
The next stage is to develop a hypothesis.

65
00:02:20,480 --> 00:02:22,940
So, my hypothesis might go something like this,

66
00:02:22,940 --> 00:02:24,890
people are going to

67
00:02:24,890 --> 00:02:27,230
express positive attitudes toward green products,

68
00:02:27,230 --> 00:02:31,790
but if I give them an array of possible product choices,

69
00:02:31,790 --> 00:02:33,200
they're going to pick the familiar brands.

70
00:02:33,200 --> 00:02:34,700
And the reason I think they're going to do

71
00:02:34,700 --> 00:02:37,040
that is because they don't need to

72
00:02:37,040 --> 00:02:39,890
feel green from buying the products

73
00:02:39,890 --> 00:02:41,120
because they feel green from

74
00:02:41,120 --> 00:02:42,890
expressing those positive attitudes.

75
00:02:42,890 --> 00:02:43,910
So, I'm going to give them

76
00:02:43,910 --> 00:02:46,880
both an attitude survey and maybe an array of products.

77
00:02:46,880 --> 00:02:49,670
If I see this in my study at the same time,

78
00:02:49,670 --> 00:02:51,635
I'm going to be fairly confident that

79
00:02:51,635 --> 00:02:54,020
this is matching what I'm seeing in the real world,

80
00:02:54,020 --> 00:02:55,670
that these positive attitudes people are

81
00:02:55,670 --> 00:02:57,350
expressing just aren't predicting

82
00:02:57,350 --> 00:02:58,640
the choices that they're making.

83
00:02:58,640 --> 00:03:00,050
And if that's the case, I can set

84
00:03:00,050 --> 00:03:02,180
aside that attitude information,

85
00:03:02,180 --> 00:03:03,470
and just trust that

86
00:03:03,470 --> 00:03:06,205
these sales numbers are what they're going to be.

87
00:03:06,205 --> 00:03:08,960
So from there, I can proceed to data collection.

88
00:03:08,960 --> 00:03:10,550
Now, this is where it's really,

89
00:03:10,550 --> 00:03:12,715
really important to think about

90
00:03:12,715 --> 00:03:14,105
how it is you want to collect

91
00:03:14,105 --> 00:03:15,825
the data that you're collecting.

92
00:03:15,825 --> 00:03:18,230
Often, we just think you can throw

93
00:03:18,230 --> 00:03:20,285
a little attitude survey together and it's fine.

94
00:03:20,285 --> 00:03:21,740
But actually, research on

95
00:03:21,740 --> 00:03:23,600
measurement shows that's just not the case.

96
00:03:23,600 --> 00:03:25,820
A lot of measures are very badly worded,

97
00:03:25,820 --> 00:03:28,400
and a lot of people don't have the insight into

98
00:03:28,400 --> 00:03:30,540
their mental processes that

99
00:03:30,540 --> 00:03:32,230
you might think that they have.

100
00:03:32,230 --> 00:03:34,790
So, it would actually be worth our time

101
00:03:34,790 --> 00:03:37,320
to find well-developed measures.

102
00:03:37,320 --> 00:03:39,950
If I want to give people a series of products to

103
00:03:39,950 --> 00:03:43,040
buy and measure their actual preferences,

104
00:03:43,040 --> 00:03:45,230
that might be more valuable than

105
00:03:45,230 --> 00:03:46,280
repeating the mistakes of

106
00:03:46,280 --> 00:03:48,260
the past and just asking people,

107
00:03:48,260 --> 00:03:49,760
"How do you feel about green products?"

108
00:03:49,760 --> 00:03:52,515
Because people might not know, or worse,

109
00:03:52,515 --> 00:03:54,655
they might tell you what they think is true,

110
00:03:54,655 --> 00:03:56,210
"I'm really green", but they might

111
00:03:56,210 --> 00:03:57,950
not actually tell you what's really true.

112
00:03:57,950 --> 00:03:59,515
I don't want the cost of those things.

113
00:03:59,515 --> 00:04:01,580
And since we have some theories about that though,

114
00:04:01,580 --> 00:04:03,320
we can include questions

115
00:04:03,320 --> 00:04:05,050
more deliberately related to that.

116
00:04:05,050 --> 00:04:06,740
So, I could give an attitude measure,

117
00:04:06,740 --> 00:04:08,480
and I'm pretty sure people are going to sound really

118
00:04:08,480 --> 00:04:10,740
positive on it because that's what my theory says.

119
00:04:10,740 --> 00:04:13,010
I can give people a series of green choices,

120
00:04:13,010 --> 00:04:14,630
and I'm pretty sure people won't make

121
00:04:14,630 --> 00:04:17,000
the green choices because that's what my theory says,

122
00:04:17,000 --> 00:04:19,070
but I can also include some measures

123
00:04:19,070 --> 00:04:22,595
directly related to my theory and hypothesis.

124
00:04:22,595 --> 00:04:24,200
People don't want the costs.

125
00:04:24,200 --> 00:04:26,990
So, for instance maybe I try an AB test.

126
00:04:26,990 --> 00:04:29,705
I present two different versions of product choices.

127
00:04:29,705 --> 00:04:31,310
In some choices, I present

128
00:04:31,310 --> 00:04:34,430
a green product and maybe a traditional familiar brand,

129
00:04:34,430 --> 00:04:35,750
and I make them the same price.

130
00:04:35,750 --> 00:04:37,860
In other versions, I make them different prices

131
00:04:37,860 --> 00:04:40,595
to see if I can move those levers around a little bit.

132
00:04:40,595 --> 00:04:42,740
Inevitably, because I've taken

133
00:04:42,740 --> 00:04:45,775
the time to put into my theory development,

134
00:04:45,775 --> 00:04:50,090
I now have a set of data that's going to be diagnostic.

135
00:04:50,090 --> 00:04:51,760
Whatever this data tells me,

136
00:04:51,760 --> 00:04:53,815
is going to be useful for understanding my theory.

137
00:04:53,815 --> 00:04:55,090
Maybe the prices don't move

138
00:04:55,090 --> 00:04:56,785
people around, but now I know that.

139
00:04:56,785 --> 00:04:58,970
Maybe people just want the familiar brand regardless,

140
00:04:58,970 --> 00:05:00,110
but now I know that.

141
00:05:00,110 --> 00:05:02,500
Maybe the attitudes that people express on

142
00:05:02,500 --> 00:05:04,015
the survey don't correlate at

143
00:05:04,015 --> 00:05:05,560
all with the choices they're going to make,

144
00:05:05,560 --> 00:05:07,465
and maybe that's good for me to know

145
00:05:07,465 --> 00:05:08,920
regardless because that's information

146
00:05:08,920 --> 00:05:10,980
I didn't have before.

147
00:05:10,980 --> 00:05:13,110
Now, that we've got the data,

148
00:05:13,110 --> 00:05:15,855
we're going to go ahead and do our data analysis.

149
00:05:15,855 --> 00:05:16,830
This is where we're going to get into

150
00:05:16,830 --> 00:05:18,245
the data science models.

151
00:05:18,245 --> 00:05:19,500
In a research process,

152
00:05:19,500 --> 00:05:21,635
especially where you're dealing with a sample of data,

153
00:05:21,635 --> 00:05:22,800
and this bears repeating because we

154
00:05:22,800 --> 00:05:24,120
only have a sample I don't

155
00:05:24,120 --> 00:05:25,770
have necessarily because I'm

156
00:05:25,770 --> 00:05:27,240
going out and collecting my own data,

157
00:05:27,240 --> 00:05:29,130
I don't necessarily have tens of thousands or

158
00:05:29,130 --> 00:05:30,600
hundreds of thousands of records

159
00:05:30,600 --> 00:05:32,190
that I can pull out of some database.

160
00:05:32,190 --> 00:05:34,890
I might only have 100 or 200 people who have

161
00:05:34,890 --> 00:05:38,370
participated in my experiment or survey.

162
00:05:38,370 --> 00:05:39,870
So, the first thing I'm going to

163
00:05:39,870 --> 00:05:41,520
do is descriptive statistics.

164
00:05:41,520 --> 00:05:42,960
I'm going to go ahead and look at

165
00:05:42,960 --> 00:05:44,655
my sample and I'm going to ask,

166
00:05:44,655 --> 00:05:46,620
"Are my predictions supported in the sample?

167
00:05:46,620 --> 00:05:50,160
What pattern of results do I see in my sample?"

168
00:05:50,160 --> 00:05:51,960
From there, I'm going to go ahead

169
00:05:51,960 --> 00:05:53,760
and do inferential statistics.

170
00:05:53,760 --> 00:05:55,725
Now, these are very important statistics

171
00:05:55,725 --> 00:05:58,140
because in my sample,

172
00:05:58,140 --> 00:06:00,380
the results might actually be the result of my sample.

173
00:06:00,380 --> 00:06:02,250
They may be a fluke, they might be a coincidence,

174
00:06:02,250 --> 00:06:04,709
they might just be due to random chance,

175
00:06:04,709 --> 00:06:06,930
and I need to do something called inference.

176
00:06:06,930 --> 00:06:08,520
I need to be able to say that

177
00:06:08,520 --> 00:06:10,230
those sample results are strong enough,

178
00:06:10,230 --> 00:06:13,020
that they probably didn't happen by chance.

179
00:06:13,020 --> 00:06:14,760
Now, this is obviously something we

180
00:06:14,760 --> 00:06:16,540
do in data science already,.

181
00:06:16,540 --> 00:06:18,580
But if we're only focused on prediction,

182
00:06:18,580 --> 00:06:21,145
we may be a little bit less concerned about inference,

183
00:06:21,145 --> 00:06:22,350
but in the research process,

184
00:06:22,350 --> 00:06:24,045
we care a lot about this.

185
00:06:24,045 --> 00:06:26,160
I'm often working with smaller samples,

186
00:06:26,160 --> 00:06:28,855
and when I say small, I mean a few hundred people.

187
00:06:28,855 --> 00:06:31,905
Because of that, I really need to safeguard

188
00:06:31,905 --> 00:06:35,010
against a fluke of my sample being responsible.

189
00:06:35,010 --> 00:06:39,255
So, we've got tools for this statistical hypothesis test.

190
00:06:39,255 --> 00:06:41,770
Imagine, you go ahead and get the data,

191
00:06:41,770 --> 00:06:43,170
and imagine the data goes ahead

192
00:06:43,170 --> 00:06:45,665
and comes back to you just as we were expecting,

193
00:06:45,665 --> 00:06:47,850
which is always great, right? sounds great.

194
00:06:47,850 --> 00:06:49,875
The data come back as expected

195
00:06:49,875 --> 00:06:51,940
and indeed we find that people sound "Rah!

196
00:06:51,940 --> 00:06:54,300
Rah! Rah!" on the attitude surveys but when you

197
00:06:54,300 --> 00:06:56,820
give them some arrays of products to choose from,

198
00:06:56,820 --> 00:06:58,905
they don't really pick the green ones.

199
00:06:58,905 --> 00:07:01,200
And in fact, we find that their attitudes are not

200
00:07:01,200 --> 00:07:03,855
correlating at all with the green products.

201
00:07:03,855 --> 00:07:05,970
And when you give them some surveys

202
00:07:05,970 --> 00:07:08,030
regardless of what the pricing is,

203
00:07:08,030 --> 00:07:09,925
they just want the familiar product.

204
00:07:09,925 --> 00:07:11,160
see now I know that now,

205
00:07:11,160 --> 00:07:13,300
I can begin to draw some conclusions.

206
00:07:13,300 --> 00:07:15,030
And this stage of the research process,

207
00:07:15,030 --> 00:07:16,500
it's really important to think about

208
00:07:16,500 --> 00:07:19,080
all the information we have both from our study

209
00:07:19,080 --> 00:07:21,420
and from previous research that we might have

210
00:07:21,420 --> 00:07:24,010
run to stitch those pieces together.

211
00:07:24,010 --> 00:07:25,250
I might begin to think,

212
00:07:25,250 --> 00:07:26,760
"You know what, pricing

213
00:07:26,760 --> 00:07:28,160
doesn't seem to have much of an effect?"

214
00:07:28,160 --> 00:07:29,570
even when I priced them the same,

215
00:07:29,570 --> 00:07:31,290
people don't go for the green products.

216
00:07:31,290 --> 00:07:33,090
People say they want to be green,

217
00:07:33,090 --> 00:07:35,220
but their actions don't back that up.

218
00:07:35,220 --> 00:07:36,840
In fact, every time I give

219
00:07:36,840 --> 00:07:38,685
them product arrays to choose from,

220
00:07:38,685 --> 00:07:40,920
they simply pick the familiar product.

221
00:07:40,920 --> 00:07:43,200
Lo and behold, now I have

222
00:07:43,200 --> 00:07:46,240
a conclusion, people want familiar.

223
00:07:46,240 --> 00:07:48,930
So, my green product ideas that were initially

224
00:07:48,930 --> 00:07:50,640
supported might just not be

225
00:07:50,640 --> 00:07:53,115
a good idea with this customer base.

226
00:07:53,115 --> 00:07:55,470
And here, we can see the value of collecting data,

227
00:07:55,470 --> 00:07:59,310
because if I just had initial insights

228
00:07:59,310 --> 00:08:01,140
into this based on

229
00:08:01,140 --> 00:08:03,355
existing data or focus groups I might miss that,

230
00:08:03,355 --> 00:08:04,875
I might make a misstep,

231
00:08:04,875 --> 00:08:06,770
I might make a bad decision.

232
00:08:06,770 --> 00:08:09,180
And if I were to just give a survey that was not

233
00:08:09,180 --> 00:08:11,920
informed by a theory and just ask people or attitudes,

234
00:08:11,920 --> 00:08:14,955
I might compound that and make that worse.

235
00:08:14,955 --> 00:08:17,880
However, because I've got some data on this maybe

236
00:08:17,880 --> 00:08:20,645
I included some questions about familiarity in my survey.

237
00:08:20,645 --> 00:08:21,960
I thought I hadn't thought of that.

238
00:08:21,960 --> 00:08:24,330
I can begin to think about some ways I might try to

239
00:08:24,330 --> 00:08:27,200
market these products that would actually be effective.

240
00:08:27,200 --> 00:08:29,070
For instance, maybe I toned down

241
00:08:29,070 --> 00:08:30,990
the innovativeness of these products

242
00:08:30,990 --> 00:08:32,670
and I sell them in ways that might be

243
00:08:32,670 --> 00:08:34,965
familiar because that's what people are going for.

244
00:08:34,965 --> 00:08:37,500
And so, we can immediately see the benefit of

245
00:08:37,500 --> 00:08:38,880
thinking through your study

246
00:08:38,880 --> 00:08:40,410
before you put the measures together.

247
00:08:40,410 --> 00:08:43,085
Inevitably, you'll have more information

248
00:08:43,085 --> 00:08:45,790
and you'll be able to make good choices and decisions,

249
00:08:45,790 --> 00:08:48,630
and that is the circle of science.

