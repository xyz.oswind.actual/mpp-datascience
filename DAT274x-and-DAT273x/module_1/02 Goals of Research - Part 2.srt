0
00:00:00,320 --> 00:00:02,670
>> So, in this next lesson,

1
00:00:02,670 --> 00:00:03,700
I want to talk to you about two

2
00:00:03,700 --> 00:00:05,230
different kinds of research and

3
00:00:05,230 --> 00:00:06,760
ways that we might use those research

4
00:00:06,760 --> 00:00:08,195
to make good decisions.

5
00:00:08,195 --> 00:00:09,640
First kind of research I want to talk

6
00:00:09,640 --> 00:00:11,320
about is called basic research.

7
00:00:11,320 --> 00:00:13,240
Now, this is probably akin to

8
00:00:13,240 --> 00:00:15,535
what you're used to when you think of analytics.

9
00:00:15,535 --> 00:00:17,290
Researchers have some sort of question,

10
00:00:17,290 --> 00:00:19,140
they get some data, and they try to answer it.

11
00:00:19,140 --> 00:00:20,830
But I want to suggest to you that

12
00:00:20,830 --> 00:00:22,540
there's more to it than just that,

13
00:00:22,540 --> 00:00:25,070
especially around this idea of prediction.

14
00:00:25,070 --> 00:00:26,980
Now, often when we're doing analytics,

15
00:00:26,980 --> 00:00:29,290
we're interested not so much in why something works,

16
00:00:29,290 --> 00:00:30,970
but simply that it does work.

17
00:00:30,970 --> 00:00:32,590
So, for instance, if I

18
00:00:32,590 --> 00:00:34,300
know something about customers and I

19
00:00:34,300 --> 00:00:35,920
know that customers who have

20
00:00:35,920 --> 00:00:38,100
some certain attribute tend to buy a certain product,

21
00:00:38,100 --> 00:00:39,705
I don't much care why they do it,

22
00:00:39,705 --> 00:00:41,510
I just know that they do do it.

23
00:00:41,510 --> 00:00:44,185
That's certainly useful for making a lot of decisions.

24
00:00:44,185 --> 00:00:47,110
However, it has one small problem

25
00:00:47,110 --> 00:00:49,885
in that it doesn't allow us to make new moves.

26
00:00:49,885 --> 00:00:51,210
It works under that context,

27
00:00:51,210 --> 00:00:52,690
but if I want to change things around it,

28
00:00:52,690 --> 00:00:55,130
I don't really understand why the pieces are working.

29
00:00:55,130 --> 00:00:56,590
So, if we have

30
00:00:56,590 --> 00:01:00,710
a more detailed process of deep understanding,

31
00:01:00,710 --> 00:01:02,390
we're able to make better decisions.

32
00:01:02,390 --> 00:01:04,120
That's what basic research is.

33
00:01:04,120 --> 00:01:08,140
Basic research is about understanding your topic.

34
00:01:08,140 --> 00:01:09,880
So, we have four levels at

35
00:01:09,880 --> 00:01:11,740
which you can conduct basic research.

36
00:01:11,740 --> 00:01:13,620
The first level we call description.

37
00:01:13,620 --> 00:01:15,460
So, in description, we may

38
00:01:15,460 --> 00:01:17,410
not have much of an idea about how something works,

39
00:01:17,410 --> 00:01:18,970
but we're going to go collect data on it

40
00:01:18,970 --> 00:01:21,175
and try to probe it and understand it.

41
00:01:21,175 --> 00:01:23,145
If I'm wanting to understand, say,

42
00:01:23,145 --> 00:01:25,990
why customers prefer a certain kind

43
00:01:25,990 --> 00:01:27,040
of product over another,

44
00:01:27,040 --> 00:01:28,330
I might start by just collecting

45
00:01:28,330 --> 00:01:29,790
initial data on their preferences.

46
00:01:29,790 --> 00:01:31,050
What is it that they buy?

47
00:01:31,050 --> 00:01:34,420
Why do they report buying those things?

48
00:01:34,420 --> 00:01:37,549
From there, I'm going to go through my research process,

49
00:01:37,549 --> 00:01:39,050
and I'm going to develop theories or ideas.

50
00:01:39,050 --> 00:01:40,730
So, at this stage now,

51
00:01:40,730 --> 00:01:42,530
I'm going to have the ability to explain

52
00:01:42,530 --> 00:01:44,240
what my customers are doing,

53
00:01:44,240 --> 00:01:46,220
and I might start to take those explanations

54
00:01:46,220 --> 00:01:48,750
and use them to predict behavior.

55
00:01:48,750 --> 00:01:51,225
So, I might have a theory that

56
00:01:51,225 --> 00:01:52,820
a certain kind of product packaging

57
00:01:52,820 --> 00:01:54,590
attracts a certain kind of customer,

58
00:01:54,590 --> 00:01:56,805
and I might start collecting data on them.

59
00:01:56,805 --> 00:01:59,480
From there, though, we're going to achieve

60
00:01:59,480 --> 00:02:02,150
what we consider the highest level of understanding,

61
00:02:02,150 --> 00:02:03,530
and that is control.

62
00:02:03,530 --> 00:02:05,510
Control is where I can actually

63
00:02:05,510 --> 00:02:07,565
act in the world in a new way,

64
00:02:07,565 --> 00:02:08,850
a way that's not tested yet.

65
00:02:08,850 --> 00:02:09,920
But because I have

66
00:02:09,920 --> 00:02:11,700
a good understanding of how this works,

67
00:02:11,700 --> 00:02:13,745
I know that when I do that,

68
00:02:13,745 --> 00:02:16,130
I'm going to have a predictable outcome.

69
00:02:16,130 --> 00:02:18,350
So, for instance, I might know

70
00:02:18,350 --> 00:02:21,645
that if I were to re-brand my logo,

71
00:02:21,645 --> 00:02:22,940
it's going to have this effect.

72
00:02:22,940 --> 00:02:25,310
I don't necessarily always need to run research

73
00:02:25,310 --> 00:02:27,690
to test that because I understand how it works.

74
00:02:27,690 --> 00:02:29,690
So, there's not need to always test

75
00:02:29,690 --> 00:02:32,465
everything because you've got a good theory or idea,

76
00:02:32,465 --> 00:02:34,440
and that's really the goal.

77
00:02:34,440 --> 00:02:37,280
The goal is that you can plan next moves,

78
00:02:37,280 --> 00:02:38,885
the goal is that you don't need to test

79
00:02:38,885 --> 00:02:41,750
every little thing because you understand how it works.

80
00:02:41,750 --> 00:02:43,775
So, this is basic research.

81
00:02:43,775 --> 00:02:45,680
We're really trying to develop

82
00:02:45,680 --> 00:02:47,950
ideas about how the world works,

83
00:02:47,950 --> 00:02:50,030
and because we have those ideas and we've

84
00:02:50,030 --> 00:02:52,370
tested them upfront, we can trust them,

85
00:02:52,370 --> 00:02:54,650
we can lean on them when we

86
00:02:54,650 --> 00:02:57,820
are in a moment where we actually need them.

87
00:02:57,820 --> 00:03:00,615
In contrast to this is applied research.

88
00:03:00,615 --> 00:03:04,060
So, applied research is much more practical hands-on to

89
00:03:04,060 --> 00:03:06,040
a specific policy or

90
00:03:06,040 --> 00:03:08,300
idea or product that you might want to test.

91
00:03:08,300 --> 00:03:10,550
Maybe there is a new product and you want to test it

92
00:03:10,550 --> 00:03:12,980
out before you deploy it at scale,

93
00:03:12,980 --> 00:03:14,690
or maybe there's a new policy that you

94
00:03:14,690 --> 00:03:16,610
want to put into place in your business.

95
00:03:16,610 --> 00:03:18,710
But before you deploy a company wide,

96
00:03:18,710 --> 00:03:19,810
you might want to test it out at

97
00:03:19,810 --> 00:03:21,530
a few stores or locations.

98
00:03:21,530 --> 00:03:23,980
Now, we don't always do this.

99
00:03:23,980 --> 00:03:28,280
In fact, often, decisions are made by experienced,

100
00:03:28,280 --> 00:03:31,415
well-informed, and wise individuals, and that's great.

101
00:03:31,415 --> 00:03:33,305
We often rely on our intuition.

102
00:03:33,305 --> 00:03:35,900
But how often does intuition lead us astray?

103
00:03:35,900 --> 00:03:37,490
How often do we

104
00:03:37,490 --> 00:03:39,770
think we know what something is going to do,

105
00:03:39,770 --> 00:03:41,040
and then when we go to do it,

106
00:03:41,040 --> 00:03:42,435
we realize that we're wrong?

107
00:03:42,435 --> 00:03:44,090
Reality has a way of

108
00:03:44,090 --> 00:03:45,800
punching us in the shoulder a little bit,

109
00:03:45,800 --> 00:03:49,130
and not always treating us the way that we want it to go.

110
00:03:49,130 --> 00:03:53,705
So, it's worth testing things before you deploy them.

111
00:03:53,705 --> 00:03:57,875
This is not so much a goal of deeper understanding.

112
00:03:57,875 --> 00:03:59,440
We think we know how this stuff works.

113
00:03:59,440 --> 00:04:02,405
That's why we've made the decisions to get where we are.

114
00:04:02,405 --> 00:04:04,320
But now that I've got some product or

115
00:04:04,320 --> 00:04:06,320
practice or idea or assumption,

116
00:04:06,320 --> 00:04:10,310
whatever it is, I want to test it before I lean on it.

117
00:04:10,310 --> 00:04:11,895
These can be smaller scale,

118
00:04:11,895 --> 00:04:13,110
they might be shorter,

119
00:04:13,110 --> 00:04:16,070
they might just be a five minute online survey to

120
00:04:16,070 --> 00:04:17,440
test or verify that

121
00:04:17,440 --> 00:04:20,030
some assumption that you think is true is really true.

122
00:04:20,030 --> 00:04:21,410
Maybe there is a new coffee cup

123
00:04:21,410 --> 00:04:22,550
that your store wants to test,

124
00:04:22,550 --> 00:04:24,410
but before you deploy it to all the stores,

125
00:04:24,410 --> 00:04:26,180
you test it out with a sample

126
00:04:26,180 --> 00:04:28,250
of about 100 customers, just to be sure.

127
00:04:28,250 --> 00:04:30,185
This allows you to be right.

128
00:04:30,185 --> 00:04:33,185
This allows you to stand on firm ground and know

129
00:04:33,185 --> 00:04:34,520
before you leap that

130
00:04:34,520 --> 00:04:37,400
there's sure footing for you to land on.

