0
00:00:00,530 --> 00:00:03,520
>> And welcome back. In this lesson,

1
00:00:03,520 --> 00:00:05,150
I want to build on what we've talked

2
00:00:05,150 --> 00:00:07,070
about with our circle of science and

3
00:00:07,070 --> 00:00:09,200
talk about how we actually wrangle

4
00:00:09,200 --> 00:00:12,115
a question down to something that's manageable.

5
00:00:12,115 --> 00:00:13,940
Now, in our previous lesson,

6
00:00:13,940 --> 00:00:15,590
I gave you an example of a company

7
00:00:15,590 --> 00:00:17,750
trying to develop a green product,

8
00:00:17,750 --> 00:00:19,460
and we began to see over

9
00:00:19,460 --> 00:00:22,160
the process of developing ideas and testing

10
00:00:22,160 --> 00:00:24,590
them that there might in fact be a lot of things

11
00:00:24,590 --> 00:00:27,080
we need to look at in any one study.

12
00:00:27,080 --> 00:00:29,330
We want insights, we need to know what rocks

13
00:00:29,330 --> 00:00:31,675
to look under and what questions to ask.

14
00:00:31,675 --> 00:00:34,760
And you can begin to think about how that might result in

15
00:00:34,760 --> 00:00:36,500
projects that get very large and very

16
00:00:36,500 --> 00:00:38,725
bloated and very unmanageable.

17
00:00:38,725 --> 00:00:41,600
Now, I've worked with a lot of organizations developing

18
00:00:41,600 --> 00:00:44,485
surveys and studies around questions like this.

19
00:00:44,485 --> 00:00:46,100
And one of the things that always

20
00:00:46,100 --> 00:00:47,840
happens is that everybody who

21
00:00:47,840 --> 00:00:51,320
has input into the study wants to add their piece,

22
00:00:51,320 --> 00:00:53,240
which completely makes sense,

23
00:00:53,240 --> 00:00:56,270
because everybody has different value brought

24
00:00:56,270 --> 00:00:57,950
into a team and everybody's got

25
00:00:57,950 --> 00:01:00,015
some questions that they might want to ask.

26
00:01:00,015 --> 00:01:02,510
However, if we want to answer

27
00:01:02,510 --> 00:01:05,255
our one key learning objective really well,

28
00:01:05,255 --> 00:01:06,980
why aren't people buying my products?

29
00:01:06,980 --> 00:01:10,730
for instance, then we really need to focus in and

30
00:01:10,730 --> 00:01:12,620
if we've got lots of different questions we might

31
00:01:12,620 --> 00:01:15,140
want to consider doing lots of different surveys.

32
00:01:15,140 --> 00:01:18,080
The reason this is a problem is that,

33
00:01:18,080 --> 00:01:21,250
we often have to attack a question in depth.

34
00:01:21,250 --> 00:01:22,550
So, for instance, as we

35
00:01:22,550 --> 00:01:24,230
talked about in the previous lesson,

36
00:01:24,230 --> 00:01:25,460
if we want to know why people aren't

37
00:01:25,460 --> 00:01:26,720
buying my green products,

38
00:01:26,720 --> 00:01:29,210
I might need to ask questions about familiarity,

39
00:01:29,210 --> 00:01:31,000
about comfort, about price.

40
00:01:31,000 --> 00:01:33,260
I might want to try A/B testing

41
00:01:33,260 --> 00:01:35,030
different prices on customers to

42
00:01:35,030 --> 00:01:37,340
see even hypothetically within a survey.

43
00:01:37,340 --> 00:01:39,280
I can start to take a lot of time.

44
00:01:39,280 --> 00:01:42,400
And so, if we try to go broad,

45
00:01:42,400 --> 00:01:44,600
we may inevitably not go deep enough to

46
00:01:44,600 --> 00:01:47,480
actually answer our question validly.

47
00:01:47,480 --> 00:01:49,050
And when you think about that,

48
00:01:49,050 --> 00:01:50,800
that could be a really big problem,

49
00:01:50,800 --> 00:01:53,285
because if we asked something very shallowly,

50
00:01:53,285 --> 00:01:55,850
we will think we've got an answer and it won't be valid.

51
00:01:55,850 --> 00:01:57,170
So now we have both run

52
00:01:57,170 --> 00:01:59,570
a project and that project is not valid.

53
00:01:59,570 --> 00:02:01,570
We're going to make a bad business decisions,

54
00:02:01,570 --> 00:02:03,380
bad organizational decisions and we're going to

55
00:02:03,380 --> 00:02:05,540
actually be worse than we started.

56
00:02:05,540 --> 00:02:07,040
So, in this lesson,

57
00:02:07,040 --> 00:02:08,840
I want to talk about a process

58
00:02:08,840 --> 00:02:11,990
for narrowing your question down.

59
00:02:12,590 --> 00:02:15,060
The first procedure that I use is

60
00:02:15,060 --> 00:02:17,000
called the "Clarifying Interview."

61
00:02:17,000 --> 00:02:19,650
Inevitably, whenever somebody has

62
00:02:19,650 --> 00:02:21,920
a study they want to do or project they want to do,

63
00:02:21,920 --> 00:02:24,190
there is a question behind the question.

64
00:02:24,190 --> 00:02:26,585
So they might come to you by saying,

65
00:02:26,585 --> 00:02:28,110
why is it that

66
00:02:28,110 --> 00:02:29,820
this marketing campaign isn't

67
00:02:29,820 --> 00:02:31,785
giving the returns that we are expecting?

68
00:02:31,785 --> 00:02:33,295
And you might immediately think,

69
00:02:33,295 --> 00:02:35,330
"Okay, that sounds great.

70
00:02:35,330 --> 00:02:36,900
We can design the entire project around

71
00:02:36,900 --> 00:02:38,634
testing a marketing campaign,

72
00:02:38,634 --> 00:02:40,310
except the real problem is

73
00:02:40,310 --> 00:02:42,180
the people aren't buying the green product."

74
00:02:42,180 --> 00:02:44,085
And so what I like to do is I like to

75
00:02:44,085 --> 00:02:46,605
just stop and talk with stakeholders.

76
00:02:46,605 --> 00:02:48,540
If I'm designing a study or

77
00:02:48,540 --> 00:02:50,910
even if I'm just analyzing data from a study,

78
00:02:50,910 --> 00:02:53,490
I want to interview those people and ask,

79
00:02:53,490 --> 00:02:56,340
"Okay, what are the problems that we're trying to solve?

80
00:02:56,340 --> 00:02:58,725
What are the underlying questions that we have?"

81
00:02:58,725 --> 00:03:00,140
Usually, and this is

82
00:03:00,140 --> 00:03:02,365
the same thing that happens with a medical doctor,

83
00:03:02,365 --> 00:03:04,460
you come to the doctor, you're complaining of one thing,

84
00:03:04,460 --> 00:03:06,570
but over the process of talking through it,

85
00:03:06,570 --> 00:03:09,509
usually over the process of that conversation,

86
00:03:09,509 --> 00:03:11,610
the "Oh, but" moment happens,

87
00:03:11,610 --> 00:03:14,025
and it's that moment that's really important

88
00:03:14,025 --> 00:03:17,080
when the real issue comes down to it,

89
00:03:17,080 --> 00:03:18,270
that you're now seeing

90
00:03:18,270 --> 00:03:20,395
the real question that people have.

91
00:03:20,395 --> 00:03:23,330
If you do this clarifying interview

92
00:03:23,330 --> 00:03:24,870
with stakeholders, with boards,

93
00:03:24,870 --> 00:03:27,090
with committees, whoever is designing a study,

94
00:03:27,090 --> 00:03:29,600
essentially, you can boil it down to one question.

95
00:03:29,600 --> 00:03:31,370
And that's usually not the question that they

96
00:03:31,370 --> 00:03:33,680
think they want you to help them answer.

97
00:03:33,680 --> 00:03:35,995
If we can imagine the project around that,

98
00:03:35,995 --> 00:03:38,725
you'll have a much better project.

99
00:03:38,725 --> 00:03:40,980
I want to therefore

100
00:03:40,980 --> 00:03:43,410
caution you against the overstuffed survey.

101
00:03:43,410 --> 00:03:47,100
If people are having a lot of different questions,

102
00:03:47,100 --> 00:03:50,100
it's usually a sign that a study is set to fail.

103
00:03:50,100 --> 00:03:53,125
You really want one question.

104
00:03:53,125 --> 00:03:56,060
So, I'm going to say during this clarifying interview,

105
00:03:56,060 --> 00:03:59,590
try to bring it down to one question.

106
00:03:59,590 --> 00:04:02,365
That can have a couple of learning objectives to it.

107
00:04:02,365 --> 00:04:05,310
Maybe we think this product isn't selling,

108
00:04:05,310 --> 00:04:07,180
so we want to both understand why

109
00:04:07,180 --> 00:04:08,230
the product's not selling and

110
00:04:08,230 --> 00:04:09,355
how we could sell it better.

111
00:04:09,355 --> 00:04:10,990
But that's still just one question.

112
00:04:10,990 --> 00:04:13,210
Why is this product not selling? What's the problem?

113
00:04:13,210 --> 00:04:15,880
I'm not going to immediately assess

114
00:04:15,880 --> 00:04:17,410
attitudes toward this entire

115
00:04:17,410 --> 00:04:19,135
portfolio of related products.

116
00:04:19,135 --> 00:04:21,585
I'm going to keep things focused.

117
00:04:21,585 --> 00:04:23,320
Once we have one objective,

118
00:04:23,320 --> 00:04:25,450
I can develop several lines of what I

119
00:04:25,450 --> 00:04:27,730
call attack around those objectives.

120
00:04:27,730 --> 00:04:29,304
I might start developing

121
00:04:29,304 --> 00:04:32,410
a specific survey questions to get at key pieces of that,

122
00:04:32,410 --> 00:04:34,745
but I'm going to keep them focused.

123
00:04:34,745 --> 00:04:36,910
So, in my green product example,

124
00:04:36,910 --> 00:04:38,425
why is this product not selling?

125
00:04:38,425 --> 00:04:41,230
I might bring five or six reasons why that product's not

126
00:04:41,230 --> 00:04:42,540
selling and I might develop

127
00:04:42,540 --> 00:04:45,115
questions specifically around those.

128
00:04:45,115 --> 00:04:46,600
Finally, I have a few minutes for

129
00:04:46,600 --> 00:04:47,780
participants and my survey.

130
00:04:47,780 --> 00:04:49,495
I don't want to waste time assessing

131
00:04:49,495 --> 00:04:51,370
things that are tangential to that.

132
00:04:51,370 --> 00:04:53,760
I want to keep it focused.

133
00:04:53,760 --> 00:04:56,350
And that leads to my four step process.

134
00:04:56,350 --> 00:04:58,185
I call this the Research Foxtrot,

135
00:04:58,185 --> 00:04:59,740
because in a foxtrot, you

136
00:04:59,740 --> 00:05:01,600
step forward to the side and back.

137
00:05:01,600 --> 00:05:04,025
So you're doing this maneuver where,

138
00:05:04,025 --> 00:05:05,500
I think that's how a foxtrot works,

139
00:05:05,500 --> 00:05:08,120
but you're doing this dance maneuver where you have

140
00:05:08,120 --> 00:05:10,885
to make a move and then shift sideways.

141
00:05:10,885 --> 00:05:12,610
And that's really what we usually have to

142
00:05:12,610 --> 00:05:14,855
do when we've got a project going.

143
00:05:14,855 --> 00:05:18,215
So, the four steps I want you to practice are;

144
00:05:18,215 --> 00:05:20,370
one, look at the proposed design.

145
00:05:20,370 --> 00:05:23,070
Usually, when there's a study that's being proposed,

146
00:05:23,070 --> 00:05:24,130
people have already sketched out

147
00:05:24,130 --> 00:05:25,555
how they think it's going to work.

148
00:05:25,555 --> 00:05:27,250
Usually, is based around

149
00:05:27,250 --> 00:05:29,080
something that is not really the main objective.

150
00:05:29,080 --> 00:05:31,045
So, I want you to look at that proposed design.

151
00:05:31,045 --> 00:05:32,035
Step back.

152
00:05:32,035 --> 00:05:33,310
What are the assumptions people are

153
00:05:33,310 --> 00:05:35,270
making about this design?

154
00:05:35,270 --> 00:05:37,255
Maybe they're wanting to know why a product's

155
00:05:37,255 --> 00:05:38,435
not selling, like a green product.

156
00:05:38,435 --> 00:05:40,110
Okay, great, but what are the assumptions?

157
00:05:40,110 --> 00:05:41,730
Oh, they're assuming that

158
00:05:41,730 --> 00:05:43,885
the problem is price. Okay, great!

159
00:05:43,885 --> 00:05:45,120
That's actually not part of

160
00:05:45,120 --> 00:05:46,470
the survey because they didn't

161
00:05:46,470 --> 00:05:49,135
think to stop and clarify that, but you will.

162
00:05:49,135 --> 00:05:51,240
So now, we're going to formalize that.

163
00:05:51,240 --> 00:05:52,950
We're going to come up with a little theory

164
00:05:52,950 --> 00:05:54,880
about price using our circle of science.

165
00:05:54,880 --> 00:05:56,460
We're going to say, "Okay, the problem

166
00:05:56,460 --> 00:05:58,790
is people aren't buying green products because of price."

167
00:05:58,790 --> 00:06:01,435
So now we can actually develop lines of attack around.

168
00:06:01,435 --> 00:06:03,630
Let's come up with two different pricing schemes and

169
00:06:03,630 --> 00:06:06,260
see if one is more effective than the other.

170
00:06:06,260 --> 00:06:08,065
We're going to do this several times.

171
00:06:08,065 --> 00:06:09,660
Maybe we think the problem's familiarity.

172
00:06:09,660 --> 00:06:11,380
People want the familiar products.

173
00:06:11,380 --> 00:06:13,810
They are confident by the products they've used before.

174
00:06:13,810 --> 00:06:15,840
Okay, let's ask questions about that.

175
00:06:15,840 --> 00:06:17,885
We can develop the study around those.

176
00:06:17,885 --> 00:06:19,670
So, there's four steps.

177
00:06:19,670 --> 00:06:21,335
First, look at the design.

178
00:06:21,335 --> 00:06:24,155
Find the underlying question behind the question.

179
00:06:24,155 --> 00:06:26,755
Two, develop a theory around that.

180
00:06:26,755 --> 00:06:28,020
Three, come up with

181
00:06:28,020 --> 00:06:30,460
several different ways of testing that theory.

182
00:06:30,460 --> 00:06:32,730
Different questions, different ways of approaching

183
00:06:32,730 --> 00:06:36,435
the topic and then you've got your survey design for you.

184
00:06:36,435 --> 00:06:39,630
This is a much more effective way than shooting broad.

185
00:06:39,630 --> 00:06:42,750
We're going to go deep on one or two specific lines of

186
00:06:42,750 --> 00:06:44,190
questioning and you're going to have

187
00:06:44,190 --> 00:06:46,870
a much better project.

