0
00:00:00,560 --> 00:00:03,460
>> Hello. Welcome to our research methods

1
00:00:03,460 --> 00:00:04,865
for data science course.

2
00:00:04,865 --> 00:00:06,490
I'm excited to talk to you today

3
00:00:06,490 --> 00:00:09,450
about the difference between research and data science.

4
00:00:09,450 --> 00:00:10,975
In this first video,

5
00:00:10,975 --> 00:00:14,290
I really want to walk through how research is bigger

6
00:00:14,290 --> 00:00:16,000
than but inclusive of what you

7
00:00:16,000 --> 00:00:18,115
might do with data science work.

8
00:00:18,115 --> 00:00:20,110
First thing I want to mention is that

9
00:00:20,110 --> 00:00:23,125
research is often a misunderstood process.

10
00:00:23,125 --> 00:00:24,460
People think about research and they

11
00:00:24,460 --> 00:00:26,185
think what is that? How is that?

12
00:00:26,185 --> 00:00:28,630
Isn't that something that academics do?

13
00:00:28,630 --> 00:00:30,610
But I actually want to make the case to you that

14
00:00:30,610 --> 00:00:33,235
research is part of the analytics process all the time.

15
00:00:33,235 --> 00:00:34,800
It's just a little bit larger.

16
00:00:34,800 --> 00:00:37,360
In this course, I want to teach you

17
00:00:37,360 --> 00:00:39,130
that larger process so that you are more

18
00:00:39,130 --> 00:00:41,875
effective in the analytics work that you might do.

19
00:00:41,875 --> 00:00:45,340
Three things are really important to highlight here.

20
00:00:45,340 --> 00:00:46,960
So the first is that research

21
00:00:46,960 --> 00:00:49,730
includes a broader process of questioning development.

22
00:00:49,730 --> 00:00:51,220
Now in a lot of analytics work,

23
00:00:51,220 --> 00:00:53,230
you might be receiving

24
00:00:53,230 --> 00:00:55,990
data and being given a question and told to answer it.

25
00:00:55,990 --> 00:00:58,030
The research process involves developing

26
00:00:58,030 --> 00:01:00,985
the question actively with analytics in mind.

27
00:01:00,985 --> 00:01:02,440
Now in an ideal world,

28
00:01:02,440 --> 00:01:04,480
the person doing the data analyses would

29
00:01:04,480 --> 00:01:06,960
have input into that process and in this course,

30
00:01:06,960 --> 00:01:09,670
I really want you to get a chance to help

31
00:01:09,670 --> 00:01:13,390
learn to develop questions with analyses in mind.

32
00:01:13,390 --> 00:01:15,430
Another way that research differs from

33
00:01:15,430 --> 00:01:18,160
straight data analytics is that you're actually

34
00:01:18,160 --> 00:01:20,560
going out and collecting data for

35
00:01:20,560 --> 00:01:23,245
the question that you want to answer.

36
00:01:23,245 --> 00:01:24,859
So, for example,

37
00:01:24,859 --> 00:01:26,950
if you are being given an existing dataset,

38
00:01:26,950 --> 00:01:29,680
you might not have control over what variables were

39
00:01:29,680 --> 00:01:31,075
measured or what content

40
00:01:31,075 --> 00:01:33,295
makes it into that final data analysis.

41
00:01:33,295 --> 00:01:36,310
But if you're designing the project from soup to nuts,

42
00:01:36,310 --> 00:01:38,230
you can go out and collect the data that you

43
00:01:38,230 --> 00:01:40,170
need to answer the question.

44
00:01:40,170 --> 00:01:41,950
This is probably the biggest difference between

45
00:01:41,950 --> 00:01:44,155
research and straight analytics.

46
00:01:44,155 --> 00:01:45,460
But it is one of the things that makes

47
00:01:45,460 --> 00:01:47,485
research so incredibly powerful.

48
00:01:47,485 --> 00:01:49,780
After all, if you can include the variables that

49
00:01:49,780 --> 00:01:52,320
you actually need to answer the question,

50
00:01:52,320 --> 00:01:55,405
you can get better answers and better insights.

51
00:01:55,405 --> 00:01:57,070
So really then research becomes

52
00:01:57,070 --> 00:02:01,190
this dynamic iterative process of problem solving.

53
00:02:01,190 --> 00:02:02,860
You're able to go out and frame questions.

54
00:02:02,860 --> 00:02:04,420
You're able to go out and collect

55
00:02:04,420 --> 00:02:06,445
the data that you need to answer those questions,

56
00:02:06,445 --> 00:02:07,750
then as that data comes back,

57
00:02:07,750 --> 00:02:08,950
you might think about what

58
00:02:08,950 --> 00:02:10,635
questions are still unanswered.

59
00:02:10,635 --> 00:02:12,460
So it is just a broader process,

60
00:02:12,460 --> 00:02:14,050
and it's a process that you can be

61
00:02:14,050 --> 00:02:16,210
involved in from start to finish.

62
00:02:16,210 --> 00:02:18,530
If you are involved in that process from start to finish,

63
00:02:18,530 --> 00:02:20,080
you're going to be more effective at

64
00:02:20,080 --> 00:02:22,365
doing the analytics work that you do.

65
00:02:22,365 --> 00:02:24,180
So I want to talk through what

66
00:02:24,180 --> 00:02:26,130
that process looks like here,

67
00:02:26,130 --> 00:02:28,170
and I'm going to use an example that may or

68
00:02:28,170 --> 00:02:30,645
may not have some autobiographical truth to it.

69
00:02:30,645 --> 00:02:33,810
Imagine you're downstairs and you look up and you

70
00:02:33,810 --> 00:02:35,430
see there might be some water

71
00:02:35,430 --> 00:02:37,630
leaking from your ceiling, what do you do?

72
00:02:37,630 --> 00:02:39,450
Really this is the scientific

73
00:02:39,450 --> 00:02:41,160
process that you're going to be doing and

74
00:02:41,160 --> 00:02:42,840
any research project and structure

75
00:02:42,840 --> 00:02:44,730
by simply framing a question.

76
00:02:44,730 --> 00:02:47,375
So you need to think what question do I need answered?

77
00:02:47,375 --> 00:02:49,560
I obviously have a problem but what question is there?

78
00:02:49,560 --> 00:02:51,030
So we might think the answer to

79
00:02:51,030 --> 00:02:54,215
that question is where's the water coming from?

80
00:02:54,215 --> 00:02:57,185
At that point, we can begin to form a theory.

81
00:02:57,185 --> 00:02:59,190
Okay. Well, let's think about this.

82
00:02:59,190 --> 00:03:00,990
The bathroom is upstairs,

83
00:03:00,990 --> 00:03:02,310
and there is a shower stall

84
00:03:02,310 --> 00:03:04,100
there and that shower stall is running.

85
00:03:04,100 --> 00:03:06,820
So maybe we have a water leak case.

86
00:03:06,820 --> 00:03:09,180
So now we think about I've got a theory,

87
00:03:09,180 --> 00:03:10,190
and from that theory,

88
00:03:10,190 --> 00:03:12,865
I might start to think about some hypotheses.

89
00:03:12,865 --> 00:03:14,460
Now hypotheses are not

90
00:03:14,460 --> 00:03:16,750
just something for scientists wearing lab coats to do.

91
00:03:16,750 --> 00:03:19,064
It's something that data analysts

92
00:03:19,064 --> 00:03:21,150
and researchers are going to do all the time.

93
00:03:21,150 --> 00:03:23,625
A hypothesis is just a testable statement.

94
00:03:23,625 --> 00:03:25,830
So we might think, "Well,

95
00:03:25,830 --> 00:03:28,455
if the water is leaking from the pipes,

96
00:03:28,455 --> 00:03:30,690
then the water might be

97
00:03:30,690 --> 00:03:33,170
leaking regardless of whether the shower is turned on,

98
00:03:33,170 --> 00:03:38,035
or if the water is leaking because there is a bad seal,

99
00:03:38,035 --> 00:03:40,635
then if I were to pour water on the tub,

100
00:03:40,635 --> 00:03:42,105
without the water running,

101
00:03:42,105 --> 00:03:43,350
I might still see drips."

102
00:03:43,350 --> 00:03:45,510
Things like that. So we might make a list of

103
00:03:45,510 --> 00:03:47,310
those hypotheses that come from

104
00:03:47,310 --> 00:03:49,850
our theory that there's some sort of shower leak.

105
00:03:49,850 --> 00:03:51,510
From there, we're going to design

106
00:03:51,510 --> 00:03:52,920
an experiment and we're going to go out and

107
00:03:52,920 --> 00:03:56,565
test our hypotheses with data that we collect.

108
00:03:56,565 --> 00:03:58,380
This is one of the biggest advantages of

109
00:03:58,380 --> 00:04:00,075
the research process because

110
00:04:00,075 --> 00:04:02,010
rather than relying on existing data,

111
00:04:02,010 --> 00:04:03,735
we may go and collect our own.

112
00:04:03,735 --> 00:04:06,045
So if you want to test whether

113
00:04:06,045 --> 00:04:09,180
there is a water leak due to a bad seal,

114
00:04:09,180 --> 00:04:10,770
you might go get a bucket of water and

115
00:04:10,770 --> 00:04:12,630
start splashing it around your tub and

116
00:04:12,630 --> 00:04:14,500
see what causes drips

117
00:04:14,500 --> 00:04:16,350
and through a process of doing this,

118
00:04:16,350 --> 00:04:19,335
you'll begin to get some answers to your questions.

119
00:04:19,335 --> 00:04:22,615
From there, after we run several different analyses,

120
00:04:22,615 --> 00:04:24,690
possibly several different experiments,

121
00:04:24,690 --> 00:04:26,460
we're going to compile our data

122
00:04:26,460 --> 00:04:28,625
together and draw some conclusions.

123
00:04:28,625 --> 00:04:31,440
Okay. So the drips only happen when

124
00:04:31,440 --> 00:04:34,810
we've poured water on the tub itself,

125
00:04:34,810 --> 00:04:36,570
and that happens regardless

126
00:04:36,570 --> 00:04:37,920
of whether or not the water's running.

127
00:04:37,920 --> 00:04:41,170
So that makes me pretty sure I need a new bathtub.

128
00:04:41,240 --> 00:04:44,180
From there, we can draw our final conclusions.

129
00:04:44,180 --> 00:04:46,070
Our final conclusions might be

130
00:04:46,070 --> 00:04:49,250
anything that you would want to report up to higher ups.

131
00:04:49,250 --> 00:04:50,605
So this could be, for instance,

132
00:04:50,605 --> 00:04:53,510
that a product line might need to be discontinued.

133
00:04:53,510 --> 00:04:56,140
This might be that you need a new bathtub.

134
00:04:56,140 --> 00:04:59,515
So, what are my goals for you in this course?

135
00:04:59,515 --> 00:05:01,970
The first is that even if you're playing

136
00:05:01,970 --> 00:05:03,300
just to do straight data science

137
00:05:03,300 --> 00:05:04,290
or straight data analytics,

138
00:05:04,290 --> 00:05:05,730
I want you to be able to make

139
00:05:05,730 --> 00:05:07,845
sense of data out of context.

140
00:05:07,845 --> 00:05:09,240
This often happens.

141
00:05:09,240 --> 00:05:10,470
You're given a dataset.

142
00:05:10,470 --> 00:05:13,350
That data doesn't have a lot of context to it and you are

143
00:05:13,350 --> 00:05:17,200
tasked being the person to draw insights from it.

144
00:05:17,200 --> 00:05:18,480
Well, if you understand

145
00:05:18,480 --> 00:05:21,390
the entire question to conclusion process,

146
00:05:21,390 --> 00:05:24,330
you can begin to be more effective using that data.

147
00:05:24,330 --> 00:05:25,800
Also along with that,

148
00:05:25,800 --> 00:05:27,900
you can prevent what I call data misuse.

149
00:05:27,900 --> 00:05:30,240
Simply put, not all data are created equal;

150
00:05:30,240 --> 00:05:31,300
some data are good,

151
00:05:31,300 --> 00:05:32,445
some data are bad,

152
00:05:32,445 --> 00:05:34,555
some data come from poor measures,

153
00:05:34,555 --> 00:05:36,555
some data come from really excellent measures.

154
00:05:36,555 --> 00:05:38,980
If you can spot bad data,

155
00:05:38,980 --> 00:05:41,910
you can prevent bad conclusions.

156
00:05:41,910 --> 00:05:44,430
Last but not least, sometimes there

157
00:05:44,430 --> 00:05:46,755
is great data available to answer your question.

158
00:05:46,755 --> 00:05:48,315
Sometimes there is not.

159
00:05:48,315 --> 00:05:50,600
If you understand your toolkit,

160
00:05:50,600 --> 00:05:52,140
you can begin to know when to

161
00:05:52,140 --> 00:05:53,630
recommend collecting more data.

162
00:05:53,630 --> 00:05:55,200
There are many cases where I'm

163
00:05:55,200 --> 00:05:57,285
given data and I suddenly say,

164
00:05:57,285 --> 00:05:59,410
"There's no need to collect more data," but

165
00:05:59,410 --> 00:06:01,970
often I think to myself it might be useful

166
00:06:01,970 --> 00:06:03,980
for me to run an AB test or to go out and

167
00:06:03,980 --> 00:06:06,110
collect some surveys from customers or

168
00:06:06,110 --> 00:06:09,050
potential clients to further

169
00:06:09,050 --> 00:06:11,105
support the theories or questions that I have.

170
00:06:11,105 --> 00:06:12,830
So when you know what your options are,

171
00:06:12,830 --> 00:06:15,920
you can begin to know when you want to use which tool.

172
00:06:15,920 --> 00:06:18,050
So really we're expanding the toolkit

173
00:06:18,050 --> 00:06:21,195
that you have for research and data science.

174
00:06:21,195 --> 00:06:23,000
Then, of course, if you want to design

175
00:06:23,000 --> 00:06:25,050
your own research, you can.

