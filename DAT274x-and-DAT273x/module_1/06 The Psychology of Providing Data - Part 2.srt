0
00:00:01,520 --> 00:00:03,790
>> Welcome. In this lesson,

1
00:00:03,790 --> 00:00:05,420
I'm going to talk a little bit more about

2
00:00:05,420 --> 00:00:07,460
the psychology of providing data,

3
00:00:07,460 --> 00:00:09,410
specifically people filling out surveys or

4
00:00:09,410 --> 00:00:11,860
participating in studies and experiments.

5
00:00:11,860 --> 00:00:13,360
But in this video, I want to talk

6
00:00:13,360 --> 00:00:15,530
not about demand characteristics,

7
00:00:15,530 --> 00:00:16,960
ways we lead people along.

8
00:00:16,960 --> 00:00:18,140
I want to talk to you about ways

9
00:00:18,140 --> 00:00:19,550
people deceive themselves,

10
00:00:19,550 --> 00:00:22,165
and you by giving you bad information.

11
00:00:22,165 --> 00:00:24,650
Specifically, I want to talk about this issue of

12
00:00:24,650 --> 00:00:27,005
people not knowing answers to questions,

13
00:00:27,005 --> 00:00:28,345
and when they don't know them,

14
00:00:28,345 --> 00:00:30,170
they essentially make things up.

15
00:00:30,170 --> 00:00:32,835
This has been demonstrated time and time again

16
00:00:32,835 --> 00:00:34,235
in the psychology literature

17
00:00:34,235 --> 00:00:35,660
and the marketing literature,

18
00:00:35,660 --> 00:00:37,790
and there are some phenomenally interesting examples

19
00:00:37,790 --> 00:00:39,785
that should give you caution when writing surveys.

20
00:00:39,785 --> 00:00:42,530
I want to just talk through a couple of these examples,

21
00:00:42,530 --> 00:00:43,850
and then you might think about how we might

22
00:00:43,850 --> 00:00:45,920
apply this to writing survey questions.

23
00:00:45,920 --> 00:00:47,370
So, the first example, this was

24
00:00:47,370 --> 00:00:48,979
demonstrated in the 1970's

25
00:00:48,979 --> 00:00:52,260
by some psychologists who were trying to study actually,

26
00:00:52,260 --> 00:00:54,870
oddly the effect of perfume on choices.

27
00:00:54,870 --> 00:00:56,480
So, they had actually a series of

28
00:00:56,480 --> 00:00:58,300
scarves lined up in an array,

29
00:00:58,300 --> 00:01:00,650
and they put some perfume on one of them,

30
00:01:00,650 --> 00:01:02,120
and their thought was that people

31
00:01:02,120 --> 00:01:04,030
would pick the perfumed scarf.

32
00:01:04,030 --> 00:01:05,480
That was kind of the scarf they

33
00:01:05,480 --> 00:01:07,145
were expecting people to pick.

34
00:01:07,145 --> 00:01:08,400
That didn't work at all.

35
00:01:08,400 --> 00:01:09,560
But what they did find,

36
00:01:09,560 --> 00:01:10,610
this was sort of one of

37
00:01:10,610 --> 00:01:12,890
those accidental discoveries was that,

38
00:01:12,890 --> 00:01:14,675
people kept picking the scarf

39
00:01:14,675 --> 00:01:16,100
on the right side of the display,

40
00:01:16,100 --> 00:01:17,260
so they've got a scarf to pick.

41
00:01:17,260 --> 00:01:18,230
They're going to pick a scarf out of

42
00:01:18,230 --> 00:01:20,240
this array of scarves,

43
00:01:20,240 --> 00:01:21,260
and they kept picking the ones on

44
00:01:21,260 --> 00:01:22,400
the right side of the array.

45
00:01:22,400 --> 00:01:23,810
So, the question the researchers

46
00:01:23,810 --> 00:01:24,895
were thinking was, "Why is this?"

47
00:01:24,895 --> 00:01:26,540
Well, what was really interesting was

48
00:01:26,540 --> 00:01:28,510
they would randomize the order of the scarves,

49
00:01:28,510 --> 00:01:30,910
people would still keep picking the scarf on the right.

50
00:01:30,910 --> 00:01:32,810
So, they didn't think no matter what the scarf was,

51
00:01:32,810 --> 00:01:34,065
it wasn't the perfume one,

52
00:01:34,065 --> 00:01:36,185
in fact, it was the order that seemed to matter.

53
00:01:36,185 --> 00:01:38,285
People were picking the one on the right.

54
00:01:38,285 --> 00:01:41,960
Over a series of studies, they actually found that this

55
00:01:41,960 --> 00:01:46,025
occurs in people who read from left to right.

56
00:01:46,025 --> 00:01:47,780
You would actually see the opposite effect for

57
00:01:47,780 --> 00:01:49,550
people who read right to left,

58
00:01:49,550 --> 00:01:51,785
turns out we tend to

59
00:01:51,785 --> 00:01:54,320
look at products in the order in which we read,

60
00:01:54,320 --> 00:01:56,570
and we tend to pick the ones we looked at last.

61
00:01:56,570 --> 00:01:57,905
So, this actually makes sense.

62
00:01:57,905 --> 00:01:59,750
You give people an array of scarves,

63
00:01:59,750 --> 00:02:00,530
they look at them,

64
00:02:00,530 --> 00:02:01,890
they look through them from left to right.

65
00:02:01,890 --> 00:02:03,660
They're kind of focusing on the ones on the right,

66
00:02:03,660 --> 00:02:05,120
and they pick one of those.

67
00:02:05,120 --> 00:02:07,665
What's really, really interesting is,

68
00:02:07,665 --> 00:02:09,560
this is one of those rare situations where we

69
00:02:09,560 --> 00:02:11,900
actually do know why people are doing what they're doing.

70
00:02:11,900 --> 00:02:13,190
It's the order; people are

71
00:02:13,190 --> 00:02:15,250
biased by the order. We know that.

72
00:02:15,250 --> 00:02:17,460
But what happens when you survey them?

73
00:02:17,460 --> 00:02:18,790
So, they did this.

74
00:02:18,790 --> 00:02:19,900
At the end of these studies,

75
00:02:19,900 --> 00:02:21,560
they would survey the participants,

76
00:02:21,560 --> 00:02:23,110
and they would ask them, "Why

77
00:02:23,110 --> 00:02:24,950
is it that you picked that scarf?"

78
00:02:24,950 --> 00:02:27,440
And they would do things, we call this confabulate.

79
00:02:27,440 --> 00:02:29,780
They would actually make up an answer,

80
00:02:29,780 --> 00:02:31,135
because they didn't know either.

81
00:02:31,135 --> 00:02:32,900
So, when they don't know,

82
00:02:32,900 --> 00:02:34,675
their mind just invents a reason,

83
00:02:34,675 --> 00:02:36,800
and this might be what shows up in

84
00:02:36,800 --> 00:02:39,385
your online survey if you're not really careful.

85
00:02:39,385 --> 00:02:40,850
In this example, they would come up

86
00:02:40,850 --> 00:02:42,650
with all sorts of reasons why they

87
00:02:42,650 --> 00:02:44,240
picked the scarf that were very

88
00:02:44,240 --> 00:02:46,055
interesting, but completely wrong.

89
00:02:46,055 --> 00:02:47,480
They might say things like, "Well,

90
00:02:47,480 --> 00:02:51,740
that is a fabric that I grew up

91
00:02:51,740 --> 00:02:53,870
with at home," or "This is similar

92
00:02:53,870 --> 00:02:56,230
to a scarf that my grandmother has."

93
00:02:56,230 --> 00:02:58,805
So, if you were to ask that question in a survey,

94
00:02:58,805 --> 00:03:02,145
"Why did you pick our product," any why question really,

95
00:03:02,145 --> 00:03:03,910
people just don't know the answer,

96
00:03:03,910 --> 00:03:05,335
and they might make things up,

97
00:03:05,335 --> 00:03:06,880
and it gets worse.

98
00:03:06,880 --> 00:03:08,750
For example, in a related study,

99
00:03:08,750 --> 00:03:11,620
they actually gave people ocean words.

100
00:03:11,620 --> 00:03:12,280
They were trying to see if

101
00:03:12,280 --> 00:03:13,930
they could mislead participants,

102
00:03:13,930 --> 00:03:17,010
so you might say things like moon, ocean,

103
00:03:17,010 --> 00:03:18,700
waves, and then they asked

104
00:03:18,700 --> 00:03:20,615
people to think of a laundry detergent,

105
00:03:20,615 --> 00:03:22,690
and immediately, people came up

106
00:03:22,690 --> 00:03:25,460
with a certain ocean themed laundry detergent,

107
00:03:25,460 --> 00:03:27,500
and we would ask people, "Okay,

108
00:03:27,500 --> 00:03:30,120
why is that the laundry detergent you thought of today?"

109
00:03:30,120 --> 00:03:32,175
And they would immediately say things like,

110
00:03:32,175 --> 00:03:34,285
"It's the one I use at home."

111
00:03:34,285 --> 00:03:37,635
Now, this is really important for a number of reasons.

112
00:03:37,635 --> 00:03:40,290
It basically suggests that the answers people give

113
00:03:40,290 --> 00:03:43,560
could be given very confidently and very wrongly,

114
00:03:43,560 --> 00:03:46,200
and the reason why they're doing it is they don't have

115
00:03:46,200 --> 00:03:47,580
the mental insights into

116
00:03:47,580 --> 00:03:49,155
why they're doing what they're doing.

117
00:03:49,155 --> 00:03:51,820
So, we really want to avoid why questions,

118
00:03:51,820 --> 00:03:55,240
or more generally, we want to avoid complex questions.

119
00:03:55,240 --> 00:03:58,745
Think about how you might answer the following questions,

120
00:03:58,745 --> 00:04:00,300
"How do you feel about ice cream

121
00:04:00,300 --> 00:04:01,850
on Tuesdays versus Wednesdays?"

122
00:04:01,850 --> 00:04:04,485
Wait for a moment.

123
00:04:04,485 --> 00:04:05,840
How do you feel about ice cream

124
00:04:05,840 --> 00:04:07,630
on Tuesdays versus Wednesdays?

125
00:04:07,630 --> 00:04:09,390
If I were to ask this question, by the way,

126
00:04:09,390 --> 00:04:10,670
this is not unlike some of

127
00:04:10,670 --> 00:04:12,600
the questions I see in product surveys.

128
00:04:12,600 --> 00:04:15,060
If I were to be asked this question,

129
00:04:15,060 --> 00:04:16,695
I would have to stop and think about it.

130
00:04:16,695 --> 00:04:18,240
How do I feel about ice cream

131
00:04:18,240 --> 00:04:19,920
on Tuesdays versus Wednesdays?

132
00:04:19,920 --> 00:04:21,750
I have to stop and invent an answer.

133
00:04:21,750 --> 00:04:23,340
I mean, I might invent an answer

134
00:04:23,340 --> 00:04:25,005
about how I feel about ice cream in general.

135
00:04:25,005 --> 00:04:26,900
I might start thinking about my schedule.

136
00:04:26,900 --> 00:04:29,835
But questions that require complex thought,

137
00:04:29,835 --> 00:04:32,055
the harder the thought, people don't like to think.

138
00:04:32,055 --> 00:04:34,120
They're just going to invent an answer,

139
00:04:34,120 --> 00:04:37,260
and they might invent a very wrong answer.

140
00:04:37,260 --> 00:04:39,830
So here's the rule,

141
00:04:39,830 --> 00:04:41,715
when you are writing survey questions,

142
00:04:41,715 --> 00:04:43,650
always go for simple,

143
00:04:43,650 --> 00:04:45,120
and go for knowable.

144
00:04:45,120 --> 00:04:47,700
If a question that you're asking is not something that

145
00:04:47,700 --> 00:04:50,670
people can kind of gut know the answer to,

146
00:04:50,670 --> 00:04:53,160
it's better not to ask it as a question.

147
00:04:53,160 --> 00:04:54,370
Its better not to ask,

148
00:04:54,370 --> 00:04:56,005
"Why did you pick this product?"

149
00:04:56,005 --> 00:04:59,775
Instead, it's much better to measure the reasons

150
00:04:59,775 --> 00:05:01,740
why someone might buy a product and see if

151
00:05:01,740 --> 00:05:04,840
those correlate in your data analysis.

