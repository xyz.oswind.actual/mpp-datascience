0
00:00:00,000 --> 00:00:03,315
>> Welcome back.

1
00:00:03,315 --> 00:00:05,130
In this lesson I want to talk about

2
00:00:05,130 --> 00:00:07,930
a third piece of the psychology of providing data.

3
00:00:07,930 --> 00:00:11,320
Specifically, I want to talk about Placebo Effects.

4
00:00:11,320 --> 00:00:12,810
Now, we've probably all heard

5
00:00:12,810 --> 00:00:15,040
about placebos from medical studies.

6
00:00:15,040 --> 00:00:18,760
Doctors give a drug and they give a fake drug or

7
00:00:18,760 --> 00:00:20,280
sugar pill to people

8
00:00:20,280 --> 00:00:22,350
when they want to see if the drug is working.

9
00:00:22,350 --> 00:00:23,760
Why do we do this?

10
00:00:23,760 --> 00:00:25,500
Because it turns out if I give some

11
00:00:25,500 --> 00:00:27,980
people a drug and some people nothing,

12
00:00:27,980 --> 00:00:31,525
the people who get the drug expect to get better

13
00:00:31,525 --> 00:00:35,610
and that effect of belief might be really strong.

14
00:00:35,610 --> 00:00:38,310
This can happen a lot in our surveys and

15
00:00:38,310 --> 00:00:39,540
studies and experiments that we

16
00:00:39,540 --> 00:00:40,890
might run in organizations,

17
00:00:40,890 --> 00:00:42,390
in businesses, government agencies,

18
00:00:42,390 --> 00:00:43,695
wherever we're doing our work.

19
00:00:43,695 --> 00:00:46,260
In fact, if our participants have

20
00:00:46,260 --> 00:00:48,390
expectations they might be influenced

21
00:00:48,390 --> 00:00:50,610
by those expectations and we need to manage them.

22
00:00:50,610 --> 00:00:52,770
So, the first thing that we need

23
00:00:52,770 --> 00:00:55,095
to know is that beliefs matter.

24
00:00:55,095 --> 00:00:56,155
Whatever it is that your

25
00:00:56,155 --> 00:00:57,750
participants believe about what you're

26
00:00:57,750 --> 00:00:59,580
doing can influence them and

27
00:00:59,580 --> 00:01:01,770
it can have a fairly strong expectation.

28
00:01:01,770 --> 00:01:04,230
This is really interesting because this can

29
00:01:04,230 --> 00:01:06,900
happen in all sorts of ways even outside of studies.

30
00:01:06,900 --> 00:01:08,610
There have been some research that suggests

31
00:01:08,610 --> 00:01:10,440
that pricing actually has

32
00:01:10,440 --> 00:01:14,325
a strong effect on how people think about products.

33
00:01:14,325 --> 00:01:15,630
For instance, they were

34
00:01:15,630 --> 00:01:17,460
actually able to do a series of studies

35
00:01:17,460 --> 00:01:19,080
some consumer researchers where they

36
00:01:19,080 --> 00:01:21,380
actually gave participants an energy drink.

37
00:01:21,380 --> 00:01:23,310
Now, in one condition the energy drink

38
00:01:23,310 --> 00:01:24,890
was priced a little bit higher,

39
00:01:24,890 --> 00:01:26,250
in one condition the energy drink

40
00:01:26,250 --> 00:01:28,055
was priced a little bit lower.

41
00:01:28,055 --> 00:01:31,015
Now usually, we think a lower price sounds good.

42
00:01:31,015 --> 00:01:32,410
It's going to sell more as

43
00:01:32,410 --> 00:01:33,970
long as we can make money off the product,

44
00:01:33,970 --> 00:01:36,205
in fact that lower price might actually have

45
00:01:36,205 --> 00:01:38,020
a benefit for the company

46
00:01:38,020 --> 00:01:39,370
because we're going to sell more products.

47
00:01:39,370 --> 00:01:42,010
But what they found was that actually

48
00:01:42,010 --> 00:01:45,485
people's beliefs about the product matched the pricing.

49
00:01:45,485 --> 00:01:47,990
So, when the energy drink was more expensive,

50
00:01:47,990 --> 00:01:51,130
people actually expected it to be a higher quality drink.

51
00:01:51,130 --> 00:01:52,930
They actually reported more satisfaction and

52
00:01:52,930 --> 00:01:56,230
more effectiveness out of the drink which is amazing,

53
00:01:56,230 --> 00:01:58,060
but also suggests that the signals

54
00:01:58,060 --> 00:02:00,330
we're giving to participants,

55
00:02:00,330 --> 00:02:01,685
to customers, to people,

56
00:02:01,685 --> 00:02:05,535
have a strong influence over what they think.

57
00:02:05,535 --> 00:02:07,565
So, we need to be really careful

58
00:02:07,565 --> 00:02:08,810
when we're doing any kind of

59
00:02:08,810 --> 00:02:12,115
study or design to manage expectations well.

60
00:02:12,115 --> 00:02:14,540
If our participants expect a certain thing,

61
00:02:14,540 --> 00:02:18,125
they may not only report that to you deceptively,

62
00:02:18,125 --> 00:02:20,320
they may actually come to believe it.

63
00:02:20,320 --> 00:02:22,490
There's a famous guitar manufacturer that

64
00:02:22,490 --> 00:02:24,800
had a similar example.

65
00:02:24,800 --> 00:02:26,420
They had a certain series of

66
00:02:26,420 --> 00:02:29,150
guitar was sold as a lower end guitar,

67
00:02:29,150 --> 00:02:31,430
it was less expensive, they had a harder time selling it.

68
00:02:31,430 --> 00:02:33,320
They actually raised the price and

69
00:02:33,320 --> 00:02:35,630
immediately people began to see it as

70
00:02:35,630 --> 00:02:37,550
a luxury guitar and it started to

71
00:02:37,550 --> 00:02:41,025
both command a high price and it increased demand.

72
00:02:41,025 --> 00:02:43,990
So, people's expectations matter a lot,

73
00:02:43,990 --> 00:02:45,220
and we can really begin to

74
00:02:45,220 --> 00:02:46,930
think about managing expectations

75
00:02:46,930 --> 00:02:49,870
within a study or survey or in general,

76
00:02:49,870 --> 00:02:52,385
in our business making decisions.

77
00:02:52,385 --> 00:02:55,679
Related to this are two different biases

78
00:02:55,679 --> 00:02:56,910
that people can have that

79
00:02:56,910 --> 00:02:58,315
give them different expectations.

80
00:02:58,315 --> 00:03:00,060
I want you to think about yourself being

81
00:03:00,060 --> 00:03:02,410
observed in this example.

82
00:03:02,410 --> 00:03:03,585
Nobody likes to be watched

83
00:03:03,585 --> 00:03:04,680
but imagine if you had somebody

84
00:03:04,680 --> 00:03:07,680
standing over you at work all day.

85
00:03:07,680 --> 00:03:11,550
People act differently when they're being observed. I do.

86
00:03:11,550 --> 00:03:13,860
If I know that you're paying close attention to me,

87
00:03:13,860 --> 00:03:15,400
I'm going to act differently,

88
00:03:15,400 --> 00:03:17,780
I'm going to try to present my best self to you,

89
00:03:17,780 --> 00:03:22,485
I'm not going to be as honest or authentic.

90
00:03:22,485 --> 00:03:24,270
There's not really a way to avoid

91
00:03:24,270 --> 00:03:26,150
this when people are filling out surveys.

92
00:03:26,150 --> 00:03:27,830
People know they're being observed,

93
00:03:27,830 --> 00:03:29,340
but there are certainly some things

94
00:03:29,340 --> 00:03:30,900
we can do to lessen that.

95
00:03:30,900 --> 00:03:34,185
So for example, I might want to avoid

96
00:03:34,185 --> 00:03:35,910
some big flashy logos on

97
00:03:35,910 --> 00:03:38,940
my survey screen because that might communicate that,

98
00:03:38,940 --> 00:03:41,720
"Oh this company or this agency is watching me."

99
00:03:41,720 --> 00:03:44,270
I might want to try to make it look as

100
00:03:44,270 --> 00:03:47,865
plain or bland as possible as long as people fill it out.

101
00:03:47,865 --> 00:03:49,860
We don't really want to brand our surveys

102
00:03:49,860 --> 00:03:51,750
too strongly for that reason because people may

103
00:03:51,750 --> 00:03:54,690
actually feel like that company is watching them and I

104
00:03:54,690 --> 00:03:56,190
want to also caution that you

105
00:03:56,190 --> 00:03:57,755
actually have the same bias,

106
00:03:57,755 --> 00:03:59,350
you the data scientist.

107
00:03:59,350 --> 00:04:01,435
We tend to see what we want to see.

108
00:04:01,435 --> 00:04:05,750
So, not only will we see this when we are analyzing data,

109
00:04:05,750 --> 00:04:07,020
but we may see this when we're actually

110
00:04:07,020 --> 00:04:08,340
recording observations.

111
00:04:08,340 --> 00:04:09,660
This can happen a lot in

112
00:04:09,660 --> 00:04:10,890
focus groups if somebody is

113
00:04:10,890 --> 00:04:12,300
running a focus group and they

114
00:04:12,300 --> 00:04:16,460
have a certain set of expectations or biases or beliefs.

115
00:04:16,460 --> 00:04:20,435
A good focus group administrator will actually push,

116
00:04:20,435 --> 00:04:21,690
will be their own devil's advocate,

117
00:04:21,690 --> 00:04:24,330
will push back on that because it is very easy to

118
00:04:24,330 --> 00:04:27,620
see what we think we expect to see.

119
00:04:27,620 --> 00:04:30,400
The solution to this by the way is just called blinding.

120
00:04:30,400 --> 00:04:32,170
So, in general especially

121
00:04:32,170 --> 00:04:34,030
if we're doing some sort of an experiment or

122
00:04:34,030 --> 00:04:36,040
AB test which we'll be talking about at

123
00:04:36,040 --> 00:04:38,310
great depth later on in this course,

124
00:04:38,310 --> 00:04:40,090
but you want to just essentially

125
00:04:40,090 --> 00:04:42,280
keep everybody as naive as possible.

126
00:04:42,280 --> 00:04:45,520
If participants are participating in a study,

127
00:04:45,520 --> 00:04:47,300
maybe there's a couple of different conditions,

128
00:04:47,300 --> 00:04:50,180
you don't want them to know what the conditions are.

129
00:04:50,180 --> 00:04:51,340
As much as possible,

130
00:04:51,340 --> 00:04:52,600
you don't want the participants to

131
00:04:52,600 --> 00:04:54,060
know what the question is.

132
00:04:54,060 --> 00:04:56,290
As much as possible, you don't want the participant to

133
00:04:56,290 --> 00:04:58,570
know what the hypotheses are,

134
00:04:58,570 --> 00:05:00,220
you really want to keep them as naive as

135
00:05:00,220 --> 00:05:02,590
possible so that they're not going to

136
00:05:02,590 --> 00:05:07,090
give you AB susceptible to like a demand characteristic,

137
00:05:07,090 --> 00:05:08,740
but also they're just not going to

138
00:05:08,740 --> 00:05:11,065
be let along by their own beliefs.

139
00:05:11,065 --> 00:05:13,115
They're not going to mislead themselves.

140
00:05:13,115 --> 00:05:15,340
Related to this, if we are interacting with

141
00:05:15,340 --> 00:05:17,640
people in any way when we are collecting data,

142
00:05:17,640 --> 00:05:19,795
maybe you've got an intercept study.

143
00:05:19,795 --> 00:05:21,820
You have a research assistant walk up to

144
00:05:21,820 --> 00:05:23,755
people in a store, approach them and say,

145
00:05:23,755 --> 00:05:26,150
"Hi, I've got a little five question survey

146
00:05:26,150 --> 00:05:28,220
and I'm going to give you a gift card if you answer it."

147
00:05:28,220 --> 00:05:29,830
That's great, that's a great way

148
00:05:29,830 --> 00:05:31,165
to get data from customers.

149
00:05:31,165 --> 00:05:34,450
But we should also be really careful that those people

150
00:05:34,450 --> 00:05:35,530
are also a little bit

151
00:05:35,530 --> 00:05:37,840
naive to the questions that are being asked.

152
00:05:37,840 --> 00:05:39,520
They may know the survey questions,

153
00:05:39,520 --> 00:05:40,960
but we don't want them to be

154
00:05:40,960 --> 00:05:43,965
accidentally leading people along.

155
00:05:43,965 --> 00:05:46,650
We've seen this a lot in research.

156
00:05:46,650 --> 00:05:50,020
There are some classic studies that suggested in

157
00:05:50,020 --> 00:05:51,640
the early 2000s that if

158
00:05:51,640 --> 00:05:54,970
you expose people to words about the elderly,

159
00:05:54,970 --> 00:05:57,290
so have people just read some words about

160
00:05:57,290 --> 00:06:01,130
old and senior and related to that,

161
00:06:01,130 --> 00:06:02,650
words about the elderly.

162
00:06:02,650 --> 00:06:05,050
They thought that after seeing

163
00:06:05,050 --> 00:06:07,960
these words people got into a kind of a mindset

164
00:06:07,960 --> 00:06:09,370
around that that they would actually

165
00:06:09,370 --> 00:06:10,780
be impacted by that and

166
00:06:10,780 --> 00:06:12,130
they had measured how

167
00:06:12,130 --> 00:06:13,950
quickly people walked away from the surveys.

168
00:06:13,950 --> 00:06:15,460
This was a study where they actually

169
00:06:15,460 --> 00:06:16,990
had people in person filling

170
00:06:16,990 --> 00:06:19,030
out a survey with all this elderly content

171
00:06:19,030 --> 00:06:20,320
in it and they had people with a

172
00:06:20,320 --> 00:06:22,510
stopwatch watching how quickly

173
00:06:22,510 --> 00:06:24,920
they walked away from the survey.

174
00:06:24,920 --> 00:06:28,180
Not surprisingly, now we know

175
00:06:28,180 --> 00:06:32,430
people did appear to walk away more slowly.

176
00:06:32,430 --> 00:06:33,820
We thought okay, "Well,

177
00:06:33,820 --> 00:06:35,290
we've got this effect, wow,

178
00:06:35,290 --> 00:06:38,360
being exposed to elderly words biases our walking speed?

179
00:06:38,360 --> 00:06:39,850
That's really impressive.

180
00:06:39,850 --> 00:06:42,760
But it turns out that's really not the case because

181
00:06:42,760 --> 00:06:45,760
what we find is when we double-blinded the study,

182
00:06:45,760 --> 00:06:48,050
this was done a few years back now.

183
00:06:48,050 --> 00:06:49,540
But when we double-blinded the study,

184
00:06:49,540 --> 00:06:51,130
when we didn't tell participants what we were

185
00:06:51,130 --> 00:06:52,600
doing and we didn't tell

186
00:06:52,600 --> 00:06:54,530
the people with the stopwatch what we were doing,

187
00:06:54,530 --> 00:06:56,575
and in fact we see no effect.

188
00:06:56,575 --> 00:06:59,230
Turns out that this entire effect

189
00:06:59,230 --> 00:07:00,790
unfortunately was caused by

190
00:07:00,790 --> 00:07:03,055
some biases with people with the stopwatch.

191
00:07:03,055 --> 00:07:04,900
I know you saw the elderly words,

192
00:07:04,900 --> 00:07:08,245
I'm kind of excited to find support for my predictions.

193
00:07:08,245 --> 00:07:10,990
I'm less objective with my measurement.

194
00:07:10,990 --> 00:07:13,375
We can immediately see how that could sabotage

195
00:07:13,375 --> 00:07:16,060
not only one piece of data collection,

196
00:07:16,060 --> 00:07:18,805
but indeed an entire business decision

197
00:07:18,805 --> 00:07:21,160
that might be made on the basis of that data.

198
00:07:21,160 --> 00:07:23,460
So really, the solution is simple,

199
00:07:23,460 --> 00:07:26,170
keep the people collecting the data naive,

200
00:07:26,170 --> 00:07:28,360
keep the participants naive as much as

201
00:07:28,360 --> 00:07:29,710
possible so that we can get

202
00:07:29,710 --> 00:07:32,720
an objective answer to our questions.

