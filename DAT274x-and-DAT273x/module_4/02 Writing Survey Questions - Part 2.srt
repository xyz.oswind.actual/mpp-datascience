0
00:00:00,890 --> 00:00:03,480
>> So, now, I want to talk about

1
00:00:03,480 --> 00:00:06,675
some things not to do when writing survey questions.

2
00:00:06,675 --> 00:00:09,705
There are a lot of bad survey questions out there.

3
00:00:09,705 --> 00:00:10,980
I'm really sorry to say.

4
00:00:10,980 --> 00:00:12,555
It's just very common

5
00:00:12,555 --> 00:00:14,400
and there are some simple things we can

6
00:00:14,400 --> 00:00:17,910
do to avoid getting bad answers from our participants.

7
00:00:17,910 --> 00:00:21,040
So, let's talk about a few things not to do.

8
00:00:21,040 --> 00:00:24,450
Number one, do not ask a leading question.

9
00:00:24,450 --> 00:00:26,895
Imagine you got the following question.

10
00:00:26,895 --> 00:00:29,990
Do you agree that Seattle is the best place to live ever?

11
00:00:29,990 --> 00:00:32,460
How would you respond?

12
00:00:32,460 --> 00:00:34,230
If you're like me, I would feel

13
00:00:34,230 --> 00:00:35,850
some sort of social pressure.

14
00:00:35,850 --> 00:00:37,470
I would feel some sort of social pressure

15
00:00:37,470 --> 00:00:39,080
to agree with the question.

16
00:00:39,080 --> 00:00:40,780
The question seems to think that

17
00:00:40,780 --> 00:00:42,660
it is in fact a good place to live.

18
00:00:42,660 --> 00:00:46,410
So, if I feel pressure to respond a certain way,

19
00:00:46,410 --> 00:00:48,595
no, I'm not being objective or honest.

20
00:00:48,595 --> 00:00:50,340
And indeed this is a problem,

21
00:00:50,340 --> 00:00:52,710
it goes all the way back to the psychology of

22
00:00:52,710 --> 00:00:55,395
providing data that we spoke about earlier.

23
00:00:55,395 --> 00:00:57,870
Simply put; participants can tell

24
00:00:57,870 --> 00:01:00,180
when you have an opinion and they're typically

25
00:01:00,180 --> 00:01:02,455
going to either go along with your opinion or

26
00:01:02,455 --> 00:01:04,500
some particularly contrary participants will

27
00:01:04,500 --> 00:01:06,810
push back against your opinion regardless,

28
00:01:06,810 --> 00:01:08,320
they're not giving you their opinion

29
00:01:08,320 --> 00:01:09,700
and that's what you want.

30
00:01:09,700 --> 00:01:13,755
So, in general, we want to avoid having any sort of

31
00:01:13,755 --> 00:01:16,470
implied right answer or

32
00:01:16,470 --> 00:01:18,610
perspective when asking a question.

33
00:01:18,610 --> 00:01:20,790
Now, I love marketers,

34
00:01:20,790 --> 00:01:23,340
but I will say when you are paid to

35
00:01:23,340 --> 00:01:24,780
promote something it's very

36
00:01:24,780 --> 00:01:26,745
hard to turn that instinct off.

37
00:01:26,745 --> 00:01:28,680
In fact, depending on what we're doing,

38
00:01:28,680 --> 00:01:31,020
we're often asking survey questions because

39
00:01:31,020 --> 00:01:33,435
we do have an answer that we want to find.

40
00:01:33,435 --> 00:01:36,120
I want to find that my product is like,

41
00:01:36,120 --> 00:01:38,820
I want to find that this new coffee cup I'm

42
00:01:38,820 --> 00:01:42,150
trying out actually works well for my customers.

43
00:01:42,150 --> 00:01:43,560
Whatever it is that I'm doing I

44
00:01:43,560 --> 00:01:46,130
probably have something I'm hoping to find,

45
00:01:46,130 --> 00:01:48,060
but I can't ask the question in

46
00:01:48,060 --> 00:01:50,490
any way that might lead people to that.

47
00:01:50,490 --> 00:01:53,680
I'm going to get a wrong answer or an invalid answer.

48
00:01:53,680 --> 00:01:55,430
Now, some people have pushed back up

49
00:01:55,430 --> 00:01:56,725
me on this and said, "Well,

50
00:01:56,725 --> 00:01:59,815
but don't we want to find that answer?

51
00:01:59,815 --> 00:02:01,060
Isn't that the goal?"

52
00:02:01,060 --> 00:02:02,580
And I say, "Yes,

53
00:02:02,580 --> 00:02:04,015
it is the goal,

54
00:02:04,015 --> 00:02:06,800
but why pay to run

55
00:02:06,800 --> 00:02:09,230
a research study if you're not

56
00:02:09,230 --> 00:02:11,930
going to get an accurate temperature."

57
00:02:11,930 --> 00:02:13,250
If we're going to get people to

58
00:02:13,250 --> 00:02:15,569
say they like living in Seattle,

59
00:02:15,569 --> 00:02:19,685
I want them to do it on their terms, not mine.

60
00:02:19,685 --> 00:02:24,285
Number two, a double-barreled question.

61
00:02:24,285 --> 00:02:26,980
Double-barreled questions are very common.

62
00:02:26,980 --> 00:02:28,860
We often just don't think about it.

63
00:02:28,860 --> 00:02:31,020
We often have more than one thing

64
00:02:31,020 --> 00:02:32,590
in mind when we ask the questions.

65
00:02:32,590 --> 00:02:33,995
We might ask a question like this;

66
00:02:33,995 --> 00:02:35,490
"Would you be happy if you could

67
00:02:35,490 --> 00:02:37,675
live in Seattle and work in tech?"

68
00:02:37,675 --> 00:02:40,485
Probably, this is actually not one question, but two.

69
00:02:40,485 --> 00:02:42,800
I'm asking if you'd be happy living in Seattle and

70
00:02:42,800 --> 00:02:46,865
I'm asking if you would be happy working in tech.

71
00:02:46,865 --> 00:02:50,170
This question if your participants are logical can

72
00:02:50,170 --> 00:02:53,560
only be answered yes if both are true at once.

73
00:02:53,560 --> 00:02:54,460
Because I'm saying would you be

74
00:02:54,460 --> 00:02:55,360
happy if you could live in

75
00:02:55,360 --> 00:02:57,310
Seattle and work in tech.

76
00:02:57,310 --> 00:02:58,569
So if your participants

77
00:02:58,569 --> 00:02:59,950
are paying attention to the question,

78
00:02:59,950 --> 00:03:01,780
which they might not be by the way,

79
00:03:01,780 --> 00:03:03,670
but if they are paying attention to the question,

80
00:03:03,670 --> 00:03:05,020
someone who would be happy living in

81
00:03:05,020 --> 00:03:06,445
Atlanta and working in tech,

82
00:03:06,445 --> 00:03:08,170
is going to say no.

83
00:03:08,170 --> 00:03:11,025
Similarly, someone who would be happy living in Seattle

84
00:03:11,025 --> 00:03:14,265
and working in say education,

85
00:03:14,265 --> 00:03:16,205
would also saying no.

86
00:03:16,205 --> 00:03:18,050
So, it's a problem, unless

87
00:03:18,050 --> 00:03:19,250
you're really only interested in

88
00:03:19,250 --> 00:03:22,345
the intersection of these two things, it is a problem.

89
00:03:22,345 --> 00:03:23,540
However, if you're interested in

90
00:03:23,540 --> 00:03:25,400
the intersection of those two things,

91
00:03:25,400 --> 00:03:28,780
why not ask both questions and then look at the overlap.

92
00:03:28,780 --> 00:03:29,930
You're going to get more information

93
00:03:29,930 --> 00:03:30,950
that way and you avoid

94
00:03:30,950 --> 00:03:33,555
the problem of someone misunderstanding the question.

95
00:03:33,555 --> 00:03:36,320
Because misunderstandings are going to happen here,

96
00:03:36,320 --> 00:03:38,500
there are going to be people who read this as,

97
00:03:38,500 --> 00:03:40,040
"Would you be happy if you could live in

98
00:03:40,040 --> 00:03:42,499
Seattle or work in tech."

99
00:03:42,499 --> 00:03:44,075
See there are people who would answer

100
00:03:44,075 --> 00:03:45,680
yes if only one of those are

101
00:03:45,680 --> 00:03:48,980
true and that's not actually what the question says,

102
00:03:48,980 --> 00:03:51,065
but people will misread the question.

103
00:03:51,065 --> 00:03:52,925
It's an ambiguous question.

104
00:03:52,925 --> 00:03:55,460
It is clear if you're a logician,

105
00:03:55,460 --> 00:03:57,330
but a lot of people will misinterpret it.

106
00:03:57,330 --> 00:03:58,920
It's just not a good question,

107
00:03:58,920 --> 00:04:02,225
better to ask the questions individually that you want

108
00:04:02,225 --> 00:04:06,485
and then look in data analysis to find the story.

109
00:04:06,485 --> 00:04:09,895
Number three, a double negative,

110
00:04:09,895 --> 00:04:13,970
long wording or any kind of confusing language.

111
00:04:13,970 --> 00:04:16,225
Imagine the following question;

112
00:04:16,225 --> 00:04:18,230
people who do not drive with

113
00:04:18,230 --> 00:04:20,675
a suspended license should never be punished.

114
00:04:20,675 --> 00:04:23,125
Do you agree or disagree with this statement?

115
00:04:23,125 --> 00:04:25,710
No, actually do you agree or disagree with it?

116
00:04:25,710 --> 00:04:27,990
Feel free to pause the video if you need.

117
00:04:27,990 --> 00:04:30,105
I'm guessing you do need because that's

118
00:04:30,105 --> 00:04:32,190
actually a really confusing question.

119
00:04:32,190 --> 00:04:34,470
Okay. People who do not drive

120
00:04:34,470 --> 00:04:37,095
with a suspended license should never be punished.

121
00:04:37,095 --> 00:04:38,310
So, I'm actually asking

122
00:04:38,310 --> 00:04:39,360
your opinions about whether people

123
00:04:39,360 --> 00:04:42,110
who do drive with, wait, what?

124
00:04:42,110 --> 00:04:44,080
This requires a lot of

125
00:04:44,080 --> 00:04:46,330
mental effort to answer and quite frankly,

126
00:04:46,330 --> 00:04:48,100
your participants probably don't

127
00:04:48,100 --> 00:04:49,765
want to give you their mental effort.

128
00:04:49,765 --> 00:04:52,540
You're probably not going to get a valid answer to this.

129
00:04:52,540 --> 00:04:56,860
What's going to happen is people will SKIM your question.

130
00:04:56,860 --> 00:04:58,780
So, they will see the question,

131
00:04:58,780 --> 00:05:00,640
they'll give you sort of a gut response not

132
00:05:00,640 --> 00:05:02,770
based on the actual logical meaning,

133
00:05:02,770 --> 00:05:04,240
but just based on their impression

134
00:05:04,240 --> 00:05:06,100
of the words in the question.

135
00:05:06,100 --> 00:05:08,200
I know it's probably hard for us to

136
00:05:08,200 --> 00:05:10,060
imagine this because when we write a survey,

137
00:05:10,060 --> 00:05:12,940
we spend a lot of time thinking about each question.

138
00:05:12,940 --> 00:05:15,640
I've been on teams that spend months going back and

139
00:05:15,640 --> 00:05:18,540
forth over which question we put in which survey,

140
00:05:18,540 --> 00:05:19,750
but the reality is

141
00:05:19,750 --> 00:05:22,465
your participants will not read them closely.

142
00:05:22,465 --> 00:05:25,510
They will go through each question and

143
00:05:25,510 --> 00:05:27,040
get kind of a gut sense of

144
00:05:27,040 --> 00:05:28,790
the meaning and they will SKIM.

145
00:05:28,790 --> 00:05:30,940
So if your question cannot be skimmed,

146
00:05:30,940 --> 00:05:32,795
it's probably not a good question.

147
00:05:32,795 --> 00:05:33,980
So, there we go;

148
00:05:33,980 --> 00:05:36,695
three things to avoid when writing survey questions.

149
00:05:36,695 --> 00:05:39,580
If you can avoid these you can get valid answers to

150
00:05:39,580 --> 00:05:41,230
your questions and hopefully

151
00:05:41,230 --> 00:05:43,800
have good insights from your data.

