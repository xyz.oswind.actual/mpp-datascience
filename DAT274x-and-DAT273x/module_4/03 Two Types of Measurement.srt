0
00:00:00,000 --> 00:00:03,445
>> So, now we

1
00:00:03,445 --> 00:00:04,875
want to switch gears and talk about

2
00:00:04,875 --> 00:00:06,875
two different kinds of measurement.

3
00:00:06,875 --> 00:00:08,960
If we have to collect data on things or if

4
00:00:08,960 --> 00:00:11,010
we have to analyze data on things,

5
00:00:11,010 --> 00:00:13,260
it's worth taking a moment to consider that

6
00:00:13,260 --> 00:00:16,115
your data can be of two different types.

7
00:00:16,115 --> 00:00:18,240
So, the first type is what

8
00:00:18,240 --> 00:00:20,340
we probably assume that we have.

9
00:00:20,340 --> 00:00:22,710
We call this manifest measurement.

10
00:00:22,710 --> 00:00:26,430
Simply put, it's something that we can measure directly.

11
00:00:26,430 --> 00:00:28,680
If I want to know how well a product sells,

12
00:00:28,680 --> 00:00:32,610
I don't really have to do much fancy question writing,

13
00:00:32,610 --> 00:00:34,050
I don't really need to brainstorm

14
00:00:34,050 --> 00:00:35,940
something particularly complicated,

15
00:00:35,940 --> 00:00:38,350
I can literally just look at how well it sells.

16
00:00:38,350 --> 00:00:40,350
I can look at the sales counts or

17
00:00:40,350 --> 00:00:42,450
various other metrics associated with that.

18
00:00:42,450 --> 00:00:44,610
I can get the answer to the question

19
00:00:44,610 --> 00:00:46,755
directly without doing much

20
00:00:46,755 --> 00:00:48,815
that is fancy or sophisticated.

21
00:00:48,815 --> 00:00:51,930
Similarly, if I want to know how tall a person is,

22
00:00:51,930 --> 00:00:54,285
it's not that complicated to measure their height.

23
00:00:54,285 --> 00:00:56,715
Yes indeed, I do actually have to measure it.

24
00:00:56,715 --> 00:00:58,220
But the answer is there for me.

25
00:00:58,220 --> 00:01:01,275
There is nothing hidden or secretive about this.

26
00:01:01,275 --> 00:01:02,620
This answer exists.

27
00:01:02,620 --> 00:01:05,015
I simply need to go out and get it.

28
00:01:05,015 --> 00:01:06,900
When you get datasets,

29
00:01:06,900 --> 00:01:09,050
especially databases in organizations,

30
00:01:09,050 --> 00:01:10,730
this is often what you have.

31
00:01:10,730 --> 00:01:13,530
Very often, our corporate databases are filled

32
00:01:13,530 --> 00:01:16,270
with manifestly measured variables,

33
00:01:16,270 --> 00:01:17,765
and these are great.

34
00:01:17,765 --> 00:01:21,370
They are often the kinds of things that we want to know.

35
00:01:21,370 --> 00:01:24,290
Often but not always,

36
00:01:24,290 --> 00:01:25,920
there are a good percentage of

37
00:01:25,920 --> 00:01:27,360
the time when we actually want

38
00:01:27,360 --> 00:01:30,370
to know something that you can't measure directly.

39
00:01:30,370 --> 00:01:31,770
If that's the case, we

40
00:01:31,770 --> 00:01:34,350
actually can't use a manifest measure.

41
00:01:34,350 --> 00:01:38,540
Worse, we often want to rely on existing data.

42
00:01:38,540 --> 00:01:40,740
If that's the case, we might use

43
00:01:40,740 --> 00:01:42,630
a manifest measure as a way

44
00:01:42,630 --> 00:01:44,790
of getting at something indirectly,

45
00:01:44,790 --> 00:01:46,230
something that we really should be

46
00:01:46,230 --> 00:01:48,470
measuring intentionally.

47
00:01:48,470 --> 00:01:49,965
That's what I want to talk about next.

48
00:01:49,965 --> 00:01:52,380
Something that you cannot measure directly,

49
00:01:52,380 --> 00:01:54,425
something you would have to intentionally measure,

50
00:01:54,425 --> 00:01:57,070
and something that could be very important to measure.

51
00:01:57,070 --> 00:01:59,440
We call it a latent measure.

52
00:01:59,440 --> 00:02:01,880
Latent measures are very important.

53
00:02:01,880 --> 00:02:04,970
Simply put, it's anything you cannot measure directly.

54
00:02:04,970 --> 00:02:08,900
This pretty much means anything mental or psychological.

55
00:02:08,900 --> 00:02:11,295
What do your customers think of your brand?

56
00:02:11,295 --> 00:02:14,840
You can't actually measure what they think of your brand.

57
00:02:14,840 --> 00:02:16,400
How intelligent are your customers?

58
00:02:16,400 --> 00:02:18,470
How experienced are your customers?

59
00:02:18,470 --> 00:02:20,280
Anything about customers really,

60
00:02:20,280 --> 00:02:21,895
in fact, when you think about it,

61
00:02:21,895 --> 00:02:23,270
anything about people other

62
00:02:23,270 --> 00:02:25,239
than their physical characteristics,

63
00:02:25,239 --> 00:02:27,770
probably going to be a manifest measure.

64
00:02:27,770 --> 00:02:30,050
These are things we have to infer.

65
00:02:30,050 --> 00:02:33,450
If you have to write a survey question about it,

66
00:02:33,450 --> 00:02:35,430
it's probably a latent measure.

67
00:02:35,430 --> 00:02:37,100
For instance, if I'm asking,

68
00:02:37,100 --> 00:02:38,909
"How much do you love the city of Seattle?"

69
00:02:38,909 --> 00:02:41,885
I don't actually measure your feelings.

70
00:02:41,885 --> 00:02:45,130
I'm measuring what you're saying about your feelings.

71
00:02:45,130 --> 00:02:49,470
That's going to be influenced by the feelings you have,

72
00:02:49,470 --> 00:02:50,700
but it's also going to be

73
00:02:50,700 --> 00:02:52,920
influenced by the way I ask the question,

74
00:02:52,920 --> 00:02:54,020
the phrasing of the question,

75
00:02:54,020 --> 00:02:56,835
the order of the question, other stuff.

76
00:02:56,835 --> 00:02:58,675
Similarly, if I'm saying,

77
00:02:58,675 --> 00:03:00,915
"How do you feel about my brand?"

78
00:03:00,915 --> 00:03:03,265
I could ask all sorts of questions.

79
00:03:03,265 --> 00:03:04,600
Describe my brand?

80
00:03:04,600 --> 00:03:05,650
Is it friendly?

81
00:03:05,650 --> 00:03:07,390
Is it enjoyable? Is it pleasant?

82
00:03:07,390 --> 00:03:10,060
Whatever adjectives or feeling words I want.

83
00:03:10,060 --> 00:03:11,485
Does this brand make you happy?

84
00:03:11,485 --> 00:03:15,115
Et cetera. But I'm not actually measuring your feelings.

85
00:03:15,115 --> 00:03:17,530
I'm not taking your emotions out of your body,

86
00:03:17,530 --> 00:03:19,895
putting them on a scale, and weighing them.

87
00:03:19,895 --> 00:03:21,545
I just can't do that.

88
00:03:21,545 --> 00:03:23,905
So, I have to infer

89
00:03:23,905 --> 00:03:27,885
your sentiment from those we call the indicators.

90
00:03:27,885 --> 00:03:29,920
So, if that's the case then,

91
00:03:29,920 --> 00:03:32,470
there's actually some thought we have to put into

92
00:03:32,470 --> 00:03:33,820
that that goes a little deeper

93
00:03:33,820 --> 00:03:35,655
than what we've done so far.

94
00:03:35,655 --> 00:03:37,485
When we think about it,

95
00:03:37,485 --> 00:03:40,630
I can assume there is some sort of

96
00:03:40,630 --> 00:03:42,160
sentiment or feeling that you

97
00:03:42,160 --> 00:03:44,740
have toward my brand, or Seattle,

98
00:03:44,740 --> 00:03:45,970
or maybe some attitude,

99
00:03:45,970 --> 00:03:48,400
there's some underlying psychological thing

100
00:03:48,400 --> 00:03:50,140
that I want to know about you,

101
00:03:50,140 --> 00:03:51,790
but I can't measure it directly.

102
00:03:51,790 --> 00:03:53,470
I infer that that is

103
00:03:53,470 --> 00:03:56,160
driving you to answer question one on my survey,

104
00:03:56,160 --> 00:03:57,550
question two on my survey,

105
00:03:57,550 --> 00:03:59,430
question three on my survey a

106
00:03:59,430 --> 00:04:01,740
certain way. That makes sense.

107
00:04:01,740 --> 00:04:04,120
Right? If somebody feels positively toward your brand,

108
00:04:04,120 --> 00:04:05,335
they're going to answer

109
00:04:05,335 --> 00:04:07,855
positively on questions that asks,

110
00:04:07,855 --> 00:04:09,665
"How do you feel about my brand?

111
00:04:09,665 --> 00:04:11,235
Does this brand make you feel happy?

112
00:04:11,235 --> 00:04:12,260
Do you feel this brand is

113
00:04:12,260 --> 00:04:13,970
trustworthy?" That kind of thing.

114
00:04:13,970 --> 00:04:16,410
But I think we should be very

115
00:04:16,410 --> 00:04:18,700
explicit about the other issue here.

116
00:04:18,700 --> 00:04:20,445
That is, there's other stuff

117
00:04:20,445 --> 00:04:22,365
also acting on those questions.

118
00:04:22,365 --> 00:04:24,360
There's other stuff that's going to come

119
00:04:24,360 --> 00:04:26,670
in and drive your answers.

120
00:04:26,670 --> 00:04:29,955
For instance, of the three questions I just asked,

121
00:04:29,955 --> 00:04:32,310
they're all positive questions.

122
00:04:32,310 --> 00:04:35,280
So, how do we know if you answer positively on

123
00:04:35,280 --> 00:04:38,550
them that is because you feel positively about my brand.

124
00:04:38,550 --> 00:04:40,230
How do we know it's not just you're

125
00:04:40,230 --> 00:04:41,885
an enthusiastic person.

126
00:04:41,885 --> 00:04:44,545
I could ask, "How do you feel about squirrels?"

127
00:04:44,545 --> 00:04:47,300
You'd be gung ho excited about it, right?

128
00:04:47,300 --> 00:04:50,460
How do I know that I don't just capture

129
00:04:50,460 --> 00:04:52,410
some other thing that's

130
00:04:52,410 --> 00:04:54,570
going to go along with those questions?

131
00:04:54,570 --> 00:04:56,260
So, I think it's worth pointing this

132
00:04:56,260 --> 00:04:57,925
out and making this really explicit.

133
00:04:57,925 --> 00:04:59,875
Every survey measure,

134
00:04:59,875 --> 00:05:02,200
it's going to have variation due to the thing you

135
00:05:02,200 --> 00:05:05,350
want to measure in those questions but it's

136
00:05:05,350 --> 00:05:07,105
also going to have what we call

137
00:05:07,105 --> 00:05:09,125
"error" variation or other junk.

138
00:05:09,125 --> 00:05:10,505
Measurement error.

139
00:05:10,505 --> 00:05:13,255
Things that I don't want to measure.

140
00:05:13,255 --> 00:05:15,850
So, how do we have a good survey then?

141
00:05:15,850 --> 00:05:17,160
How do I actually measure

142
00:05:17,160 --> 00:05:19,925
your feelings or sentiment about something?

143
00:05:19,925 --> 00:05:21,690
You might say, well, you know what?

144
00:05:21,690 --> 00:05:23,030
Let's just not you survey questions.

145
00:05:23,030 --> 00:05:24,850
Let's use natural language processing,

146
00:05:24,850 --> 00:05:27,280
or open-ended questions, or other things like that.

147
00:05:27,280 --> 00:05:29,900
But, in fact, that also has the same problem.

148
00:05:29,900 --> 00:05:31,100
Any time you measure

149
00:05:31,100 --> 00:05:33,380
something and use it as an indicator or

150
00:05:33,380 --> 00:05:36,980
a guess about something mental or underlying,

151
00:05:36,980 --> 00:05:38,075
you've got this problem.

152
00:05:38,075 --> 00:05:42,675
Every measurement method has error.

153
00:05:42,675 --> 00:05:45,130
Unless you can measure it directly like height or weight,

154
00:05:45,130 --> 00:05:47,220
even then your scale is probably not perfect.

155
00:05:47,220 --> 00:05:48,900
So, how do we define or

156
00:05:48,900 --> 00:05:51,065
describe what a good measure looks like?

157
00:05:51,065 --> 00:05:55,530
A good measure has two simple principles that it follows.

158
00:05:55,530 --> 00:05:59,475
First, the indicators should reflect the construct,

159
00:05:59,475 --> 00:06:01,815
the thing you're measuring and little else.

160
00:06:01,815 --> 00:06:05,965
Second, when I aggregate them into a composite,

161
00:06:05,965 --> 00:06:07,710
I'm going to capture

162
00:06:07,710 --> 00:06:09,955
the construct or the thing you're measuring well.

163
00:06:09,955 --> 00:06:12,730
So, again, if I'm writing questions one, two,

164
00:06:12,730 --> 00:06:14,475
and three, those questions

165
00:06:14,475 --> 00:06:16,500
shouldn't be measuring other stuff.

166
00:06:16,500 --> 00:06:18,540
I should be really careful with my wording that

167
00:06:18,540 --> 00:06:20,130
those wordings are not going to pick

168
00:06:20,130 --> 00:06:21,925
up on other content accidentally.

169
00:06:21,925 --> 00:06:24,540
Principal one, they measure the sentiment,

170
00:06:24,540 --> 00:06:27,280
they measure the construct, and little else.

171
00:06:28,180 --> 00:06:31,180
Indicator two, I should also make

172
00:06:31,180 --> 00:06:33,190
sure that these constructs,

173
00:06:33,190 --> 00:06:35,950
these questions capture the construct well.

174
00:06:35,950 --> 00:06:38,155
I'm not missing out on important stuff.

175
00:06:38,155 --> 00:06:41,300
If there's key pieces of sentiment that I'm leaving out,

176
00:06:41,300 --> 00:06:43,865
if I needed a question four and I didn't put it in there,

177
00:06:43,865 --> 00:06:46,805
then I've also failed in my job.

178
00:06:46,805 --> 00:06:49,270
So, two things that we can try to maximize.

179
00:06:49,270 --> 00:06:51,220
We'll talk in the coming lessons about how to do that.

180
00:06:51,220 --> 00:06:53,650
First, that my questions should really be

181
00:06:53,650 --> 00:06:55,510
measuring or capturing the thing

182
00:06:55,510 --> 00:06:56,805
I want to measure and nothing else.

183
00:06:56,805 --> 00:06:59,050
I want to avoid weird quirks

184
00:06:59,050 --> 00:07:01,060
or idiosyncrasies in my questions.

185
00:07:01,060 --> 00:07:02,860
I want to avoid making my questions so

186
00:07:02,860 --> 00:07:04,300
specific that they're really

187
00:07:04,300 --> 00:07:06,445
going to pick up on something else.

188
00:07:06,445 --> 00:07:09,880
Number two, I want to make sure my question base is broad

189
00:07:09,880 --> 00:07:11,800
enough and I'm probably going to need to ask

190
00:07:11,800 --> 00:07:13,920
a few questions then if it's really important,

191
00:07:13,920 --> 00:07:15,160
that I'm not leaving out

192
00:07:15,160 --> 00:07:17,590
important variation in the thing I'm measuring.

193
00:07:17,590 --> 00:07:20,170
So, two principles that can maximize

194
00:07:20,170 --> 00:07:22,360
the validity of our measure and we are going to

195
00:07:22,360 --> 00:07:25,550
go ahead and look at that in the coming videos.

