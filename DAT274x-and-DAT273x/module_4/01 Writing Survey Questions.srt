0
00:00:01,520 --> 00:00:03,590
>> In this module,

1
00:00:03,590 --> 00:00:05,780
I want to shift gears a little bit and talk about

2
00:00:05,780 --> 00:00:08,820
how we collect valid data.

3
00:00:08,820 --> 00:00:10,510
Now, in many cases,

4
00:00:10,510 --> 00:00:11,960
there is existing data that

5
00:00:11,960 --> 00:00:13,595
can help us answer our questions.

6
00:00:13,595 --> 00:00:16,790
Our organization might have a lot of the information

7
00:00:16,790 --> 00:00:17,810
we might need to

8
00:00:17,810 --> 00:00:20,345
assess the answers to our questions already.

9
00:00:20,345 --> 00:00:22,730
But in many cases, it's actually worth it

10
00:00:22,730 --> 00:00:25,020
to go out and collect new data,

11
00:00:25,020 --> 00:00:27,780
as we've talked about in the research process already.

12
00:00:27,780 --> 00:00:29,745
If that data doesn't exist,

13
00:00:29,745 --> 00:00:31,790
you might accidentally just base

14
00:00:31,790 --> 00:00:33,945
your conclusions on what's available,

15
00:00:33,945 --> 00:00:35,565
not the right data.

16
00:00:35,565 --> 00:00:37,530
So, it's good to have existing data,

17
00:00:37,530 --> 00:00:39,040
but you want the right data.

18
00:00:39,040 --> 00:00:41,930
So, let's just say we need to collect some new data.

19
00:00:41,930 --> 00:00:43,335
How do you know if it's valid?

20
00:00:43,335 --> 00:00:45,110
How do you know if you have the right data?

21
00:00:45,110 --> 00:00:46,910
How do you know if that data is going to

22
00:00:46,910 --> 00:00:49,900
legitimately and accurately answer your questions?

23
00:00:49,900 --> 00:00:51,545
So, we're going to start in this lesson

24
00:00:51,545 --> 00:00:53,295
by talking about surveys.

25
00:00:53,295 --> 00:00:55,225
Surveys are one of the most common ways we

26
00:00:55,225 --> 00:00:57,500
get information from people in bulk.

27
00:00:57,500 --> 00:01:00,265
It lets us build large data sets of people's thoughts,

28
00:01:00,265 --> 00:01:01,909
opinions, preferences,

29
00:01:01,909 --> 00:01:04,115
and all kinds of information about them.

30
00:01:04,115 --> 00:01:05,510
But you might not be

31
00:01:05,510 --> 00:01:07,215
surprised to know by now that there's

32
00:01:07,215 --> 00:01:10,250
a good way and a bad way to write a survey question,

33
00:01:10,250 --> 00:01:12,505
and it might also not surprise you by now to know that

34
00:01:12,505 --> 00:01:15,860
many survey questions are probably not very valid.

35
00:01:15,860 --> 00:01:17,900
So, let's take a chance to talk

36
00:01:17,900 --> 00:01:20,495
through how to write a valid survey question.

37
00:01:20,495 --> 00:01:21,890
To do this, I'm going to

38
00:01:21,890 --> 00:01:23,535
start off with a little exercise.

39
00:01:23,535 --> 00:01:25,130
So, imagine you wanted to

40
00:01:25,130 --> 00:01:27,440
ask how much people love a city.

41
00:01:27,440 --> 00:01:28,680
Let's just say Seattle.

42
00:01:28,680 --> 00:01:30,470
So, I want you to actually

43
00:01:30,470 --> 00:01:33,270
pause the video and think for a moment.

44
00:01:33,270 --> 00:01:35,060
Just give yourself a few minutes and

45
00:01:35,060 --> 00:01:37,450
brainstorm all the ways you could ask,

46
00:01:37,450 --> 00:01:39,590
and I don't just mean ways of phrasing,

47
00:01:39,590 --> 00:01:41,315
ways you might approach the question.

48
00:01:41,315 --> 00:01:43,790
Do you ask people a yes or no question?

49
00:01:43,790 --> 00:01:45,555
Do you have people write out a paragraph?

50
00:01:45,555 --> 00:01:47,780
What do you do? So, I want you to pause video

51
00:01:47,780 --> 00:01:51,780
now and stop and brainstorm for about three minutes.

52
00:01:52,220 --> 00:01:54,460
You're still watching? I'm going to

53
00:01:54,460 --> 00:01:56,185
assume you've done this exercise.

54
00:01:56,185 --> 00:01:58,570
Let's take a journey through

55
00:01:58,570 --> 00:02:02,590
a number of survey options and see how we might do it.

56
00:02:02,590 --> 00:02:05,550
One strategy you may have come

57
00:02:05,550 --> 00:02:07,875
up with was what we call the open-ended question.

58
00:02:07,875 --> 00:02:10,060
This would literally just be a text box,

59
00:02:10,060 --> 00:02:12,275
or if you're handing out a sheet of paper,

60
00:02:12,275 --> 00:02:15,655
verbal answers people give or write out.

61
00:02:15,655 --> 00:02:18,115
We could ask people, what do you think of Seattle?

62
00:02:18,115 --> 00:02:20,040
Taking the temperature,

63
00:02:20,040 --> 00:02:22,560
we get a lot of text with this data.

64
00:02:22,560 --> 00:02:24,025
So, what are we going to do with it?

65
00:02:24,025 --> 00:02:26,185
That's a new important and interesting question.

66
00:02:26,185 --> 00:02:28,540
In reality, we often end up with

67
00:02:28,540 --> 00:02:31,660
large bodies of text and need to find ways to analyze it.

68
00:02:31,660 --> 00:02:33,940
Now, using modern machine learning methods,

69
00:02:33,940 --> 00:02:35,560
there are some really cool things

70
00:02:35,560 --> 00:02:37,280
that we can do with this data,

71
00:02:37,280 --> 00:02:39,370
things like sentiment analysis,

72
00:02:39,370 --> 00:02:42,550
analyzing the patterns of words that are used,

73
00:02:42,550 --> 00:02:45,275
and natural language processing or NLP.

74
00:02:45,275 --> 00:02:47,550
These are really great methods,

75
00:02:47,550 --> 00:02:51,700
but they can be a little laborious and time-consuming,

76
00:02:51,700 --> 00:02:55,145
and they might not get you the answer you directly want,

77
00:02:55,145 --> 00:02:57,415
which is, do people love Seattle?

78
00:02:57,415 --> 00:02:59,825
So, it might be a useful tool,

79
00:02:59,825 --> 00:03:02,375
but this might not be the only tool that you want.

80
00:03:02,375 --> 00:03:04,365
The analytic methods can be cumbersome.

81
00:03:04,365 --> 00:03:05,735
They can be complicated,

82
00:03:05,735 --> 00:03:07,420
and if you are doing it without

83
00:03:07,420 --> 00:03:09,970
the advantage of a machine learning algorithm,

84
00:03:09,970 --> 00:03:11,750
you would have to end up reading and

85
00:03:11,750 --> 00:03:14,885
coding many responses along these lines.

86
00:03:14,885 --> 00:03:16,780
Now, I've done that kind of work before,

87
00:03:16,780 --> 00:03:18,810
and it can be very time-consuming.

88
00:03:18,810 --> 00:03:21,790
Imagine getting five or six hundred paragraphs,

89
00:03:21,790 --> 00:03:24,020
having to read for themes and code them.

90
00:03:24,020 --> 00:03:27,770
It is very rich as a source of data,

91
00:03:27,770 --> 00:03:29,600
but it might not give you the short,

92
00:03:29,600 --> 00:03:31,835
direct answer to the question that you want.

93
00:03:31,835 --> 00:03:34,845
So, let's consider another option.

94
00:03:34,845 --> 00:03:37,270
If we've got these pros and cons,

95
00:03:37,270 --> 00:03:39,100
are there ways we can maybe minimize

96
00:03:39,100 --> 00:03:42,365
the cons and still maximize the pros?

97
00:03:42,365 --> 00:03:45,655
The complete opposite of that

98
00:03:45,655 --> 00:03:48,575
is a forced-choice format question.

99
00:03:48,575 --> 00:03:51,120
We could simply give people a yes or no question,

100
00:03:51,120 --> 00:03:53,460
do you like Seattle or do you love Seattle,

101
00:03:53,460 --> 00:03:56,050
and let people pick yes or no.

102
00:03:56,050 --> 00:03:57,750
This is not a bad option.

103
00:03:57,750 --> 00:03:59,140
In fact, you've probably filled

104
00:03:59,140 --> 00:04:02,220
survey questions out like this before.

105
00:04:02,220 --> 00:04:04,445
How would we analyze it?

106
00:04:04,445 --> 00:04:06,700
Nicely, there's not a whole lot

107
00:04:06,700 --> 00:04:08,545
of machine learning that we need to do.

108
00:04:08,545 --> 00:04:11,465
There's not a lot of data analysis that we need to do.

109
00:04:11,465 --> 00:04:13,530
Analysis is pretty much going to

110
00:04:13,530 --> 00:04:15,250
just be finding the percentage of

111
00:04:15,250 --> 00:04:17,080
people who say yes and comparing it

112
00:04:17,080 --> 00:04:19,070
to the percentage of people who say no.

113
00:04:19,070 --> 00:04:21,070
If we're interested in predicting this,

114
00:04:21,070 --> 00:04:23,040
we can predict a yes or no question with

115
00:04:23,040 --> 00:04:24,290
a logistic regression or a

116
00:04:24,290 --> 00:04:27,495
classifier-based machine learning algorithm,

117
00:04:27,495 --> 00:04:29,350
lots of neat data science tools

118
00:04:29,350 --> 00:04:30,840
that we're not getting into yet.

119
00:04:30,840 --> 00:04:33,245
But at a basic level,

120
00:04:33,245 --> 00:04:34,290
we can just count them,

121
00:04:34,290 --> 00:04:36,310
count the yeses and count the nos.

122
00:04:36,310 --> 00:04:39,640
This has actually the exact opposite pattern

123
00:04:39,640 --> 00:04:42,050
of pros and cons from the open-ended question.

124
00:04:42,050 --> 00:04:44,810
Now, we have very simple data,

125
00:04:44,810 --> 00:04:46,850
whereas before, we had very complicated data.

126
00:04:46,850 --> 00:04:48,520
This is very simple. So, this is

127
00:04:48,520 --> 00:04:52,105
a very fast easy way to get an answer.

128
00:04:52,105 --> 00:04:54,070
Of course, what's simple for you is

129
00:04:54,070 --> 00:04:55,820
also simple for the participant.

130
00:04:55,820 --> 00:04:57,490
You're not asking people to fill

131
00:04:57,490 --> 00:04:59,500
out long paragraphs of text.

132
00:04:59,500 --> 00:05:01,210
You're not asking people to spend

133
00:05:01,210 --> 00:05:03,260
a lot of time and effort thinking.

134
00:05:03,260 --> 00:05:05,770
You want the answer, you get it.

135
00:05:05,770 --> 00:05:08,885
The con is exactly the same as the pro.

136
00:05:08,885 --> 00:05:10,270
It's simple. So, now,

137
00:05:10,270 --> 00:05:13,140
you've lost some of the rich source of information.

138
00:05:13,140 --> 00:05:16,360
Whereas, that long text response would give you

139
00:05:16,360 --> 00:05:19,915
something that would give a rich source of information,

140
00:05:19,915 --> 00:05:21,450
not as much as you might get from, say,

141
00:05:21,450 --> 00:05:23,680
a focus group, but a rich source of information.

142
00:05:23,680 --> 00:05:24,870
Here, you're really only going

143
00:05:24,870 --> 00:05:26,240
to get the answer to your question,

144
00:05:26,240 --> 00:05:27,700
and you're going to get it quickly.

145
00:05:27,700 --> 00:05:29,410
Assuming, of course, you've asked

146
00:05:29,410 --> 00:05:31,435
a question that people know the answer to.

147
00:05:31,435 --> 00:05:34,090
I will say you do need to stop and think when writing

148
00:05:34,090 --> 00:05:37,190
your questions about the psychology of providing data.

149
00:05:37,190 --> 00:05:39,265
Remember, if you ask people question,

150
00:05:39,265 --> 00:05:41,540
"Why do you do something?", and they don't know,

151
00:05:41,540 --> 00:05:43,115
they may invent an answer,

152
00:05:43,115 --> 00:05:45,295
an answer that's not truthful or real,

153
00:05:45,295 --> 00:05:47,705
just because they need to give you an answer.

154
00:05:47,705 --> 00:05:50,240
If it's a question, like do you like Seattle,

155
00:05:50,240 --> 00:05:51,310
or love Seattle, or something that

156
00:05:51,310 --> 00:05:53,050
people have probably thought about before,

157
00:05:53,050 --> 00:05:55,330
something that would be easily on hand,

158
00:05:55,330 --> 00:05:56,740
probably going to be able to get

159
00:05:56,740 --> 00:05:59,270
some kind of a valid answer.

160
00:06:01,070 --> 00:06:03,630
Is there a middle ground?

161
00:06:03,630 --> 00:06:05,780
Yes, and this middle ground is something

162
00:06:05,780 --> 00:06:07,955
you've probably seen a lot before.

163
00:06:07,955 --> 00:06:09,970
We call it Likert scale.

164
00:06:09,970 --> 00:06:11,285
A Likert scale gives you

165
00:06:11,285 --> 00:06:13,390
many different options that you can pick.

166
00:06:13,390 --> 00:06:15,980
So, for instance, maybe we're asking people,

167
00:06:15,980 --> 00:06:17,974
do you believe the trainings

168
00:06:17,974 --> 00:06:19,930
at your organization are effective?

169
00:06:19,930 --> 00:06:21,620
We trying to imagine the organization where

170
00:06:21,620 --> 00:06:23,600
you work gives out different kinds of trainings,

171
00:06:23,600 --> 00:06:26,440
and we want to know how effective we think they are.

172
00:06:26,440 --> 00:06:29,170
So, you could ask people to write out answers,

173
00:06:29,170 --> 00:06:30,905
and that's going to be laborious

174
00:06:30,905 --> 00:06:33,845
and more work than it's worth to get the answer.

175
00:06:33,845 --> 00:06:36,590
You could ask people just a yes or no question,

176
00:06:36,590 --> 00:06:39,110
but now you're really missing the degree

177
00:06:39,110 --> 00:06:42,185
of middle ground of gray zone there.

178
00:06:42,185 --> 00:06:45,470
A Likert scale takes the best of both worlds.

179
00:06:45,470 --> 00:06:47,250
So, when you give people five options,

180
00:06:47,250 --> 00:06:50,000
not effective at all, slightly effective,

181
00:06:50,000 --> 00:06:52,515
moderately effective, very effective,

182
00:06:52,515 --> 00:06:53,730
and extremely effective,

183
00:06:53,730 --> 00:06:55,760
these are scored on a one to five scale.

184
00:06:55,760 --> 00:06:58,330
So you can quickly calculate an average,

185
00:06:58,330 --> 00:06:59,480
or you could ask maybe

186
00:06:59,480 --> 00:07:02,165
several such questions that we'll talk about soon,

187
00:07:02,165 --> 00:07:06,710
and aggregate them together to get a more nuanced score.

188
00:07:06,710 --> 00:07:08,510
But now you've got something that you can

189
00:07:08,510 --> 00:07:10,790
use in analytic models.

190
00:07:10,790 --> 00:07:11,840
You've got something that you can

191
00:07:11,840 --> 00:07:13,850
correlate or use in a regression,

192
00:07:13,850 --> 00:07:15,290
especially if you have several questions.

193
00:07:15,290 --> 00:07:18,315
So you get more nuanced than just one through five.

194
00:07:18,315 --> 00:07:20,915
But now you've got something that is quick.

195
00:07:20,915 --> 00:07:22,520
It does not take your participants a lot

196
00:07:22,520 --> 00:07:24,340
of time and effort to think through,

197
00:07:24,340 --> 00:07:26,870
but you've also got something that

198
00:07:26,870 --> 00:07:28,100
you can analyze with more

199
00:07:28,100 --> 00:07:29,870
nuanced than a simple yes or no.

200
00:07:29,870 --> 00:07:32,070
We like the Likert scale.

201
00:07:32,070 --> 00:07:34,490
You shouldn't think for a moment about

202
00:07:34,490 --> 00:07:36,715
how many of these you ask people to fill out,

203
00:07:36,715 --> 00:07:38,330
and quite frankly, how

204
00:07:38,330 --> 00:07:40,715
interested your participants are in your survey.

205
00:07:40,715 --> 00:07:43,110
If they're answering a lot of questions,

206
00:07:43,110 --> 00:07:45,200
even Likert scales can get tedious.

207
00:07:45,200 --> 00:07:47,270
If people are volunteering and only

208
00:07:47,270 --> 00:07:49,730
willing to give you about 30 seconds of your time,

209
00:07:49,730 --> 00:07:51,570
this will take more time.

210
00:07:51,570 --> 00:07:54,525
But if you've got people for a little while,

211
00:07:54,525 --> 00:07:57,780
this is a nice, happy middle ground.

