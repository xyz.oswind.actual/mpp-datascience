0
00:00:00,920 --> 00:00:03,060
>> In the previous lesson,

1
00:00:03,060 --> 00:00:05,865
we talked about two things you need for good measure.

2
00:00:05,865 --> 00:00:07,020
When you're measuring something

3
00:00:07,020 --> 00:00:07,890
that you need to capture with

4
00:00:07,890 --> 00:00:09,120
a survey or measuring

5
00:00:09,120 --> 00:00:10,835
something that you can't measure directly.

6
00:00:10,835 --> 00:00:13,140
We said those two things were,

7
00:00:13,140 --> 00:00:15,780
that you need to measure the construct.

8
00:00:15,780 --> 00:00:18,495
The thing you want to measure and little else.

9
00:00:18,495 --> 00:00:20,700
You also want to make sure you're broadly

10
00:00:20,700 --> 00:00:23,340
covering the topic that you need.

11
00:00:23,340 --> 00:00:26,829
These two principles translate

12
00:00:26,829 --> 00:00:30,375
into some broader concepts now that I want to talk about,

13
00:00:30,375 --> 00:00:31,630
that you want your measures to be

14
00:00:31,630 --> 00:00:34,470
both reliable and valid.

15
00:00:34,470 --> 00:00:36,550
So, before we get into how you do,

16
00:00:36,550 --> 00:00:38,100
that there are some things we can look for.

17
00:00:38,100 --> 00:00:40,630
In fact, there are even some tests we can run and

18
00:00:40,630 --> 00:00:43,240
some statistical procedures we can do on our measures

19
00:00:43,240 --> 00:00:46,120
to make sure we have good measures,

20
00:00:46,120 --> 00:00:48,680
ways to test these principles.

21
00:00:48,680 --> 00:00:51,960
I want to talk for a few moments about

22
00:00:51,960 --> 00:00:53,580
the concepts of what it means to be

23
00:00:53,580 --> 00:00:55,545
reliable and what it means to be valid.

24
00:00:55,545 --> 00:00:57,775
So, let's go through them.

25
00:00:57,775 --> 00:00:59,855
First reliability,

26
00:00:59,855 --> 00:01:02,880
when we say something reliable in a measurement context,

27
00:01:02,880 --> 00:01:04,170
we're really asking whether

28
00:01:04,170 --> 00:01:06,075
our measurement is consistent.

29
00:01:06,075 --> 00:01:08,390
Now, this could be a few different things,

30
00:01:08,390 --> 00:01:10,380
but in general if I'm asking you

31
00:01:10,380 --> 00:01:12,510
several different questions about sentiment,

32
00:01:12,510 --> 00:01:15,150
I'm hopeful that if I'm measuring sentiment you're

33
00:01:15,150 --> 00:01:18,255
kind of answering similarly on all of them.

34
00:01:18,255 --> 00:01:20,800
If I'm asking "Does my brand seem trustworthy,

35
00:01:20,800 --> 00:01:22,205
does it seem positive,

36
00:01:22,205 --> 00:01:25,105
does it seem friendly, et cetera?"

37
00:01:25,105 --> 00:01:27,930
Then really, I'm kind of assuming that

38
00:01:27,930 --> 00:01:29,070
those are just measuring your

39
00:01:29,070 --> 00:01:30,690
overall feelings toward the brand,

40
00:01:30,690 --> 00:01:31,950
I'm kind of assuming you're going to

41
00:01:31,950 --> 00:01:34,100
answer similarly on all of them.

42
00:01:34,100 --> 00:01:35,815
If you're not, if you're answering

43
00:01:35,815 --> 00:01:37,500
higher on some and lower on

44
00:01:37,500 --> 00:01:39,210
others then I don't

45
00:01:39,210 --> 00:01:41,730
actually have a reliable measure of sentiment.

46
00:01:41,730 --> 00:01:44,040
I might actually have three separate measures

47
00:01:44,040 --> 00:01:46,230
or say trustworthiness and so on.

48
00:01:46,230 --> 00:01:48,660
But if you're answering very similarly on all of

49
00:01:48,660 --> 00:01:51,090
them then I'm consistently measuring something,

50
00:01:51,090 --> 00:01:54,090
I've really reliably measuring one thing,

51
00:01:54,090 --> 00:01:55,390
and that's something to keep in mind and

52
00:01:55,390 --> 00:01:58,130
something we're going actually need to test.

53
00:01:58,590 --> 00:02:02,370
Reliably measuring one thing is not enough.

54
00:02:02,370 --> 00:02:03,940
We also need to make sure that we are

55
00:02:03,940 --> 00:02:06,235
reliably measuring the correct thing.

56
00:02:06,235 --> 00:02:08,410
For instance, I could ask you

57
00:02:08,410 --> 00:02:11,800
several questions about your sentiment

58
00:02:11,800 --> 00:02:14,830
toward a brand and as I mentioned before,

59
00:02:14,830 --> 00:02:17,085
accidentally we measuring the wrong thing,

60
00:02:17,085 --> 00:02:19,195
your level of personal enthusiasm.

61
00:02:19,195 --> 00:02:21,070
If I say does this brand feel

62
00:02:21,070 --> 00:02:24,370
friendly and happy and exciting and trustworthy,

63
00:02:24,370 --> 00:02:27,470
you might answer positively to all of those not because

64
00:02:27,470 --> 00:02:30,170
you feel positively toward the brand or product,

65
00:02:30,170 --> 00:02:32,965
but because you're enthusiastic like me.

66
00:02:32,965 --> 00:02:35,410
So, if that's the case,

67
00:02:35,410 --> 00:02:37,460
you don't have a valid measurement of sentiment,

68
00:02:37,460 --> 00:02:39,350
you just have a measure of enthusiasm.

69
00:02:39,350 --> 00:02:40,745
So, there are some things we might

70
00:02:40,745 --> 00:02:42,410
need to consider doing to

71
00:02:42,410 --> 00:02:43,700
assure ourselves that what we're

72
00:02:43,700 --> 00:02:46,710
measuring is the correct thing.

73
00:02:47,930 --> 00:02:50,910
I want to point out that you cannot

74
00:02:50,910 --> 00:02:53,835
actually get validity without reliability.

75
00:02:53,835 --> 00:02:56,520
You can't measure the right thing unless

76
00:02:56,520 --> 00:02:59,160
you're first measuring something consistently.

77
00:02:59,160 --> 00:03:00,780
So, this is really going to be

78
00:03:00,780 --> 00:03:03,060
a two-step process and I'm going to advocate

79
00:03:03,060 --> 00:03:05,175
that you always stop and

80
00:03:05,175 --> 00:03:08,160
assess the reliability and validity of your measures.

81
00:03:08,160 --> 00:03:09,600
If you're collecting data,

82
00:03:09,600 --> 00:03:13,125
do not just jump straight into the analysis of that data.

83
00:03:13,125 --> 00:03:15,715
I know it's really exciting to create

84
00:03:15,715 --> 00:03:19,500
an enthusiasm score or a sentiment score or whatever,

85
00:03:19,500 --> 00:03:22,165
and run your data science models right away.

86
00:03:22,165 --> 00:03:25,449
But unless I can assure myself that I've reliably

87
00:03:25,449 --> 00:03:26,920
measured something and I've

88
00:03:26,920 --> 00:03:28,660
reliably measured the right thing,

89
00:03:28,660 --> 00:03:30,640
validity, I really have

90
00:03:30,640 --> 00:03:34,220
no business doing analysis on that data.

91
00:03:34,220 --> 00:03:39,365
So, to recap if I'm doing this with a scale,

92
00:03:39,365 --> 00:03:41,620
I might be asking myself "Am I

93
00:03:41,620 --> 00:03:44,285
consistently getting the correct weight?"

94
00:03:44,285 --> 00:03:48,015
Validity, I'm asking "Is that the correct weight?"

95
00:03:48,015 --> 00:03:50,770
Whatever it is that I am doing I can apply

96
00:03:50,770 --> 00:03:54,970
these two principles consistency and correctness.

97
00:03:54,970 --> 00:03:58,870
Similarly, if I've got a questionnaire, reliability,

98
00:03:58,870 --> 00:04:00,700
is really asking whether

99
00:04:00,700 --> 00:04:03,550
I'm consistently measuring one thing.

100
00:04:03,550 --> 00:04:05,590
I would expect my questions to be

101
00:04:05,590 --> 00:04:08,365
highly correlated with each other, right?

102
00:04:08,365 --> 00:04:09,730
If I've got three things and they're

103
00:04:09,730 --> 00:04:11,510
really just don't measure sentiment,

104
00:04:11,510 --> 00:04:14,080
they should correlate highly with each other.

105
00:04:14,080 --> 00:04:17,710
So, we could think of reliability in this case as

106
00:04:17,710 --> 00:04:19,840
some sort of indicator of

107
00:04:19,840 --> 00:04:22,940
how correlated my questions are.

108
00:04:24,030 --> 00:04:27,910
I can also then create a composite index

109
00:04:27,910 --> 00:04:29,560
and correlate that with other things

110
00:04:29,560 --> 00:04:30,880
to see if it's the right thing.

111
00:04:30,880 --> 00:04:32,740
And that would be a way of getting a validity.

112
00:04:32,740 --> 00:04:34,690
We're going to explore both of these

113
00:04:34,690 --> 00:04:36,640
in depth in the next couple of lessons,

114
00:04:36,640 --> 00:04:38,440
but I think it's worth taking a moment to

115
00:04:38,440 --> 00:04:40,360
make sure we feel really comfortable

116
00:04:40,360 --> 00:04:42,170
with these two steps

117
00:04:42,170 --> 00:04:44,685
of assessing the quality of a measure.

118
00:04:44,685 --> 00:04:46,755
Is it consistently measuring something?

119
00:04:46,755 --> 00:04:48,280
Reliability. And is it

120
00:04:48,280 --> 00:04:50,330
consistently measuring the right thing?

121
00:04:50,330 --> 00:04:52,000
Validity. So, let's take

122
00:04:52,000 --> 00:04:54,800
a look at those in the next couple lessons.

