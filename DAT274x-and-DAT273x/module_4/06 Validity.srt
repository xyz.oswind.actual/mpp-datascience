0
00:00:00,000 --> 00:00:02,840
>> So now that we know

1
00:00:02,840 --> 00:00:06,115
we've consistently measured something,

2
00:00:06,115 --> 00:00:08,270
reliability, now we need to make sure that

3
00:00:08,270 --> 00:00:11,175
we've measured the right thing, validity.

4
00:00:11,175 --> 00:00:13,800
How do we do that? How do we be sure that

5
00:00:13,800 --> 00:00:17,135
the thing that we've got is the thing that we want?

6
00:00:17,135 --> 00:00:20,625
Well one thing that we can do is called face validity.

7
00:00:20,625 --> 00:00:21,890
This is subjective.

8
00:00:21,890 --> 00:00:23,190
This is simply put,

9
00:00:23,190 --> 00:00:24,750
putting your questions through

10
00:00:24,750 --> 00:00:26,445
what I like to call the smell test.

11
00:00:26,445 --> 00:00:28,620
Does it seem right to you?

12
00:00:28,620 --> 00:00:30,360
You're allowed to do this.

13
00:00:30,360 --> 00:00:31,680
You don't have to have

14
00:00:31,680 --> 00:00:33,810
a statistical or data based answer

15
00:00:33,810 --> 00:00:35,280
to the question of validity.

16
00:00:35,280 --> 00:00:39,060
It is okay for you to use your subject matter expertise.

17
00:00:39,060 --> 00:00:40,590
Does it seem valid?

18
00:00:40,590 --> 00:00:42,660
Of course we call it face validity because we're

19
00:00:42,660 --> 00:00:45,780
saying on the face of it does it seem valid.

20
00:00:45,780 --> 00:00:48,105
So, just taking that

21
00:00:48,105 --> 00:00:51,315
at the face level or at surface level.

22
00:00:51,315 --> 00:00:54,150
I will point out there are plenty of things that are face

23
00:00:54,150 --> 00:00:57,000
valid that are totally not valid at all.

24
00:00:57,000 --> 00:00:58,440
In fact we've talked about these

25
00:00:58,440 --> 00:01:00,360
earlier if you asked people why they do

26
00:01:00,360 --> 00:01:02,415
things that seems like a face valid question

27
00:01:02,415 --> 00:01:05,160
but people have no idea why they do things.

28
00:01:05,160 --> 00:01:07,140
Similarly you might be asking

29
00:01:07,140 --> 00:01:09,660
questions about how someone feels about

30
00:01:09,660 --> 00:01:11,910
a brand and you might accidentally also be

31
00:01:11,910 --> 00:01:15,000
measuring their just level of agreement or enthusiasm.

32
00:01:15,000 --> 00:01:16,620
So things that are faced valid

33
00:01:16,620 --> 00:01:19,220
aren't necessarily actually valid.

34
00:01:19,220 --> 00:01:22,085
But it's something. It's a good starting point.

35
00:01:22,085 --> 00:01:24,860
In fact I would say most things that you're measuring

36
00:01:24,860 --> 00:01:27,865
probably should have some face validity to them.

37
00:01:27,865 --> 00:01:30,200
Yes, I suppose it's possible that

38
00:01:30,200 --> 00:01:32,450
there is some strange combination of questions

39
00:01:32,450 --> 00:01:34,550
we could put together that make no sense but

40
00:01:34,550 --> 00:01:37,640
somehow miraculously measure the thing you want.

41
00:01:37,640 --> 00:01:40,630
But I'm guessing that's not a path we want

42
00:01:40,630 --> 00:01:43,710
to try to run down when writing our research.

43
00:01:43,710 --> 00:01:46,215
Number two, content validity.

44
00:01:46,215 --> 00:01:48,570
This is also subjective and it is

45
00:01:48,570 --> 00:01:52,205
another question that you should ask of your questions.

46
00:01:52,205 --> 00:01:54,390
Whereas face validity just said does this

47
00:01:54,390 --> 00:01:56,705
seem valid at its face level?

48
00:01:56,705 --> 00:01:59,880
Content validity is a more detailed content analysis

49
00:01:59,880 --> 00:02:01,350
of the questions and you can

50
00:02:01,350 --> 00:02:03,570
ask this just as the face validity

51
00:02:03,570 --> 00:02:06,240
before you collect the data. So you should.

52
00:02:06,240 --> 00:02:10,050
So, what we're asking specifically is are

53
00:02:10,050 --> 00:02:12,360
the indicators typically questions although

54
00:02:12,360 --> 00:02:14,880
you could use non questions as well.

55
00:02:14,880 --> 00:02:17,675
You could take other indicators, indices,

56
00:02:17,675 --> 00:02:19,170
measures that you have and combine

57
00:02:19,170 --> 00:02:21,875
them but typically survey questions.

58
00:02:21,875 --> 00:02:25,515
Are they appropriately broad or narrow?

59
00:02:25,515 --> 00:02:28,710
For example, if I'm testing your knowledge

60
00:02:28,710 --> 00:02:32,160
of data science research methods for example,

61
00:02:32,160 --> 00:02:34,650
you would be mightily irritated with me if I

62
00:02:34,650 --> 00:02:36,150
only asked questions about

63
00:02:36,150 --> 00:02:37,965
a certain module in this course.

64
00:02:37,965 --> 00:02:40,460
After all, you took the entire course.

65
00:02:40,460 --> 00:02:42,300
It would be far too narrow to only

66
00:02:42,300 --> 00:02:44,535
ask questions about a specific piece of that.

67
00:02:44,535 --> 00:02:46,005
I would have left out

68
00:02:46,005 --> 00:02:47,370
important variance or

69
00:02:47,370 --> 00:02:49,320
important variation in your knowledge.

70
00:02:49,320 --> 00:02:51,325
There's important pieces of that that

71
00:02:51,325 --> 00:02:53,220
I've just completely ignored and

72
00:02:53,220 --> 00:02:54,960
because of that I don't actually

73
00:02:54,960 --> 00:02:58,185
have a valid measure of your knowledge.

74
00:02:58,185 --> 00:03:01,530
Similarly, you would also be irritated with me if I

75
00:03:01,530 --> 00:03:03,360
included content that didn't

76
00:03:03,360 --> 00:03:04,995
belong in this course at all.

77
00:03:04,995 --> 00:03:06,510
I could ask you all sorts of

78
00:03:06,510 --> 00:03:08,670
questions that are completely unrelated to

79
00:03:08,670 --> 00:03:10,650
the content of the course and

80
00:03:10,650 --> 00:03:12,820
that would not be content valid either.

81
00:03:12,820 --> 00:03:14,280
So you really have to assess

82
00:03:14,280 --> 00:03:15,960
the inclusion criteria and

83
00:03:15,960 --> 00:03:18,120
the exclusion criteria for your questions.

84
00:03:18,120 --> 00:03:21,000
You need to make sure that what's in there needs to be

85
00:03:21,000 --> 00:03:22,830
there and that also

86
00:03:22,830 --> 00:03:25,335
everything that needs to be there is in there.

87
00:03:25,335 --> 00:03:29,055
These are things we can assess before we collect data.

88
00:03:29,055 --> 00:03:31,950
So there's really no excuse for not doing this.

89
00:03:31,950 --> 00:03:35,630
However, often we really just do face validity.

90
00:03:35,630 --> 00:03:37,410
Very often when committees or

91
00:03:37,410 --> 00:03:39,700
groups or organizations put surveys together,

92
00:03:39,700 --> 00:03:42,915
they don't stop to ask whether it's content valid.

93
00:03:42,915 --> 00:03:44,760
They just say I have these questions in

94
00:03:44,760 --> 00:03:46,875
mind and they seemed important.

95
00:03:46,875 --> 00:03:49,635
But we can actually take an extra step

96
00:03:49,635 --> 00:03:51,105
and assess the content validity

97
00:03:51,105 --> 00:03:53,190
before we collect the data.

98
00:03:53,190 --> 00:03:57,075
Of course we will collect our data and at that point

99
00:03:57,075 --> 00:04:00,930
really we should do some data analysis if we can.

100
00:04:00,930 --> 00:04:02,700
This is called criterion

101
00:04:02,700 --> 00:04:05,070
validity and criterion validity simply

102
00:04:05,070 --> 00:04:07,170
looks at the pattern of correlations

103
00:04:07,170 --> 00:04:10,290
between your measure and other trustworthy measures.

104
00:04:10,290 --> 00:04:13,430
This is like the smell test again,

105
00:04:13,430 --> 00:04:15,675
but now you're doing it with correlations.

106
00:04:15,675 --> 00:04:17,640
For instance, if you've got a measure

107
00:04:17,640 --> 00:04:19,440
of brand sentiment does it

108
00:04:19,440 --> 00:04:21,160
correlate with other things that

109
00:04:21,160 --> 00:04:23,765
brand sentiment should correlate with?

110
00:04:23,765 --> 00:04:24,925
Things that you know

111
00:04:24,925 --> 00:04:27,765
brand sentiment has correlated with in the past.

112
00:04:27,765 --> 00:04:30,210
Maybe you come up with a set of questions to

113
00:04:30,210 --> 00:04:32,400
measure brand sentiment and you try it out

114
00:04:32,400 --> 00:04:34,260
a couple times and you find that

115
00:04:34,260 --> 00:04:36,955
a certain set of questions works really well,

116
00:04:36,955 --> 00:04:38,790
tends to always predict sales,

117
00:04:38,790 --> 00:04:40,440
tends to always predict

118
00:04:40,440 --> 00:04:42,765
whether someone becomes a repeat customer.

119
00:04:42,765 --> 00:04:44,760
Now you've locked those questions in.

120
00:04:44,760 --> 00:04:46,150
You know that they're

121
00:04:46,150 --> 00:04:48,760
criterion invalid because they correlate,

122
00:04:48,760 --> 00:04:51,530
they predict important things.

123
00:04:51,530 --> 00:04:54,670
So because of that now we have

124
00:04:54,670 --> 00:04:56,350
an external way of

125
00:04:56,350 --> 00:04:57,790
assessing that the thing that I

126
00:04:57,790 --> 00:04:59,495
thought was valid, face valid.

127
00:04:59,495 --> 00:05:02,425
The thing that had the right content in it,

128
00:05:02,425 --> 00:05:05,095
content valid, actually is valid.

129
00:05:05,095 --> 00:05:08,770
I've got this external check and I will just

130
00:05:08,770 --> 00:05:14,720
say you can still use the smell test here subjectively,

131
00:05:14,720 --> 00:05:17,709
in that sometimes a low correlation

132
00:05:17,709 --> 00:05:20,170
might not be a problem so you don't

133
00:05:20,170 --> 00:05:22,570
have to throw things out if the correlation surprise

134
00:05:22,570 --> 00:05:25,690
you but these should factor into your decision making.

135
00:05:25,690 --> 00:05:28,540
It is a common misconception that

136
00:05:28,540 --> 00:05:30,190
people's attitudes or sentiment will

137
00:05:30,190 --> 00:05:32,150
correlate strongly with their behavior.

138
00:05:32,150 --> 00:05:33,895
It just often doesn't.

139
00:05:33,895 --> 00:05:35,290
Attitudes rarely

140
00:05:35,290 --> 00:05:37,475
correlate strongly with people's behaviors.

141
00:05:37,475 --> 00:05:40,450
People just don't do what they think a lot.

142
00:05:40,450 --> 00:05:42,460
That's not a problem with your attitude measure.

143
00:05:42,460 --> 00:05:43,900
That's a problem with psychology,

144
00:05:43,900 --> 00:05:45,310
that's a problem with people.

145
00:05:45,310 --> 00:05:48,550
So some subject matter expertise is helpful here but

146
00:05:48,550 --> 00:05:50,230
you really also want to make sure

147
00:05:50,230 --> 00:05:52,695
that you're using your own expertise.

148
00:05:52,695 --> 00:05:56,285
So, in summary, test your measures.

149
00:05:56,285 --> 00:05:58,745
If you can test your measures just do it.

150
00:05:58,745 --> 00:06:00,875
If you can get previous data

151
00:06:00,875 --> 00:06:02,960
or even maybe consider running

152
00:06:02,960 --> 00:06:04,790
a few surveys or comparisons

153
00:06:04,790 --> 00:06:07,120
on a set of measures you might want to use a lot,

154
00:06:07,120 --> 00:06:08,290
go ahead and do that.

155
00:06:08,290 --> 00:06:11,255
Validate those measures ensure that they are reliable.

156
00:06:11,255 --> 00:06:12,680
It doesn't have to be in

157
00:06:12,680 --> 00:06:14,900
the study where you really want to use them as

158
00:06:14,900 --> 00:06:16,550
long as you know that measure has been put

159
00:06:16,550 --> 00:06:18,635
through a pretty rigorous test.

160
00:06:18,635 --> 00:06:20,225
Now you've got a set of questions

161
00:06:20,225 --> 00:06:21,410
or indicators that you can

162
00:06:21,410 --> 00:06:25,550
reliably use in any context to answer your questions.

163
00:06:25,550 --> 00:06:27,710
People don't often do this,

164
00:06:27,710 --> 00:06:30,110
they often just look at whether something makes sense to

165
00:06:30,110 --> 00:06:33,440
them and they start doing all kinds of analytics on it.

166
00:06:33,440 --> 00:06:35,240
It's just not a good way to do

167
00:06:35,240 --> 00:06:36,470
your research because you're going

168
00:06:36,470 --> 00:06:37,925
to make bad conclusions.

169
00:06:37,925 --> 00:06:39,410
Inevitably the questions you

170
00:06:39,410 --> 00:06:41,315
thought were consistently measuring something

171
00:06:41,315 --> 00:06:43,400
aren't or inevitably you end up

172
00:06:43,400 --> 00:06:46,400
measuring the wrong thing without meaning to.

173
00:06:46,400 --> 00:06:48,080
The better we can make our measurement,

174
00:06:48,080 --> 00:06:50,360
the better we can make our insights because It is

175
00:06:50,360 --> 00:06:51,695
the only link between

176
00:06:51,695 --> 00:06:54,300
us and the things that we want to understand.

177
00:06:54,300 --> 00:06:57,910
So, if you can test your measures, just do it.

