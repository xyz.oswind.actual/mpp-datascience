0
00:00:01,220 --> 00:00:03,780
>> So now I want to take a few moments,

1
00:00:03,780 --> 00:00:07,050
just kind of meditate on the concept of reliability.

2
00:00:07,050 --> 00:00:09,060
How do we know if our measure is reliable?

3
00:00:09,060 --> 00:00:12,330
What are some things we could do with our data to assess

4
00:00:12,330 --> 00:00:14,820
this based on the information

5
00:00:14,820 --> 00:00:16,925
that we bothered to collect?

6
00:00:16,925 --> 00:00:20,250
I said before, reliability is about consistency.

7
00:00:20,250 --> 00:00:21,900
In fact, there are actually

8
00:00:21,900 --> 00:00:23,520
a few different kinds of

9
00:00:23,520 --> 00:00:25,770
consistency that might be worth considering.

10
00:00:25,770 --> 00:00:28,185
So, let's talk through these in turn.

11
00:00:28,185 --> 00:00:33,085
Number one, we call it Test-retest reliability.

12
00:00:33,085 --> 00:00:36,020
This is relevant or applicable if you're

13
00:00:36,020 --> 00:00:39,170
measuring something that should be consistent over time.

14
00:00:39,170 --> 00:00:40,820
For instance, if you're

15
00:00:40,820 --> 00:00:44,090
measuring a customer's personality,

16
00:00:44,090 --> 00:00:46,910
maybe I think that people who are extroverted might be

17
00:00:46,910 --> 00:00:50,565
really attracted to this particular brand or product.

18
00:00:50,565 --> 00:00:52,685
Then my measure of extroversion's

19
00:00:52,685 --> 00:00:53,810
personality is really pretty

20
00:00:53,810 --> 00:00:57,430
stable shouldn't change much over time.

21
00:00:57,430 --> 00:01:01,270
Number two, if I'm observing behavior,

22
00:01:01,270 --> 00:01:04,840
maybe I've got an intercept study or I'm people

23
00:01:04,840 --> 00:01:09,150
watching and I'm observing people what they do,

24
00:01:09,150 --> 00:01:10,890
I might have somebody recording

25
00:01:10,890 --> 00:01:12,760
or taking notes on what they're doing.

26
00:01:12,760 --> 00:01:15,190
I might want to make sure that my raters,

27
00:01:15,190 --> 00:01:16,420
the people who are doing the note

28
00:01:16,420 --> 00:01:18,710
taking, have consistent observations.

29
00:01:18,710 --> 00:01:21,915
We call it Interrater reliability.

30
00:01:21,915 --> 00:01:24,580
Third, we might want to look

31
00:01:24,580 --> 00:01:27,070
within a set of questions and ensure that

32
00:01:27,070 --> 00:01:28,900
they are consistently getting

33
00:01:28,900 --> 00:01:30,730
similar responses if we're

34
00:01:30,730 --> 00:01:32,710
going to create composite index,

35
00:01:32,710 --> 00:01:33,910
like asking three or

36
00:01:33,910 --> 00:01:35,545
four questions about brand sentiment.

37
00:01:35,545 --> 00:01:37,150
So there are different kinds of

38
00:01:37,150 --> 00:01:38,530
consistency that might be

39
00:01:38,530 --> 00:01:40,345
relevant in different situations,

40
00:01:40,345 --> 00:01:43,145
so let's take a moment and talk about these.

41
00:01:43,145 --> 00:01:45,880
Test-retest, definitely something you would need

42
00:01:45,880 --> 00:01:47,170
if you were studying something that

43
00:01:47,170 --> 00:01:49,090
supposed to be stable over time,

44
00:01:49,090 --> 00:01:52,100
but not really something you need to worry about if

45
00:01:52,100 --> 00:01:53,660
you're not studying something

46
00:01:53,660 --> 00:01:55,415
that needs to be stable over time.

47
00:01:55,415 --> 00:01:57,110
There are a lot of measures that

48
00:01:57,110 --> 00:01:59,390
fluctuate within the same person over time.

49
00:01:59,390 --> 00:02:01,970
Well, that's not a problem unless you're studying

50
00:02:01,970 --> 00:02:04,765
something that needs to be or should be stable.

51
00:02:04,765 --> 00:02:07,075
Then you maybe got a bad measure.

52
00:02:07,075 --> 00:02:09,640
Interrater, really only an

53
00:02:09,640 --> 00:02:13,355
issue if you're observing someone's actions.

54
00:02:13,355 --> 00:02:16,820
Maybe this is something where you have people write

55
00:02:16,820 --> 00:02:20,305
out text responses and you want to code those for themes.

56
00:02:20,305 --> 00:02:25,350
So, does this text box mention parking issues?

57
00:02:25,350 --> 00:02:29,150
Or does this text box mention annoyances

58
00:02:29,150 --> 00:02:32,285
about the colors in the app that I just watched?

59
00:02:32,285 --> 00:02:35,090
If you're doing content analysis like that.

60
00:02:35,090 --> 00:02:37,490
We typically would suggest having

61
00:02:37,490 --> 00:02:40,440
two people do the content analysis.

62
00:02:40,440 --> 00:02:43,935
What you can do is look at the percent agreement.

63
00:02:43,935 --> 00:02:46,260
Do they agree 50 percent of the time?

64
00:02:46,260 --> 00:02:47,525
Not very good.

65
00:02:47,525 --> 00:02:50,230
Do they agree 80 percent of the time? That's better.

66
00:02:50,230 --> 00:02:53,700
So you can assess the reliability of your rater.

67
00:02:53,700 --> 00:02:56,075
You can make Interrater reliability

68
00:02:56,075 --> 00:02:58,930
better simply by giving clear instructions.

69
00:02:58,930 --> 00:03:01,460
Tell people or coach them how to read

70
00:03:01,460 --> 00:03:02,795
a certain situation or

71
00:03:02,795 --> 00:03:06,200
what criteria you would use for marking say,

72
00:03:06,200 --> 00:03:08,000
a participant as looking stressed or

73
00:03:08,000 --> 00:03:10,970
anxious or whatever your rating or coding.

74
00:03:10,970 --> 00:03:13,050
Clear directions will help with that.

75
00:03:13,050 --> 00:03:15,200
Of course, if we're rating survey questions,

76
00:03:15,200 --> 00:03:17,690
then we care the most about is internal validity.

77
00:03:17,690 --> 00:03:19,850
That is looking at whether your questions are

78
00:03:19,850 --> 00:03:23,230
consistently getting at the same topic.

