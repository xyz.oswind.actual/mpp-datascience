0
00:00:01,590 --> 00:00:05,100
>> In this module I want to shift gears now and

1
00:00:05,100 --> 00:00:07,890
look at a menu of research design options.

2
00:00:07,890 --> 00:00:09,825
We've talked about measurement,

3
00:00:09,825 --> 00:00:11,890
we talked about data analysis,

4
00:00:11,890 --> 00:00:13,750
we've talked about the research process,

5
00:00:13,750 --> 00:00:16,440
but we haven't gotten a chance to put all of the pieces

6
00:00:16,440 --> 00:00:18,240
together and see ourselves

7
00:00:18,240 --> 00:00:20,230
answering some questions with data.

8
00:00:20,230 --> 00:00:22,650
So, in this module I want to walk through

9
00:00:22,650 --> 00:00:26,070
a few correlational and experimental research designs,

10
00:00:26,070 --> 00:00:28,530
and see how we can use all of the pieces

11
00:00:28,530 --> 00:00:32,020
of this course to tell stories with data.

12
00:00:32,020 --> 00:00:33,540
I want to start off by talking

13
00:00:33,540 --> 00:00:36,390
about bi-variant correlational research.

14
00:00:36,390 --> 00:00:39,570
This is when we've got a set of variables and we want to

15
00:00:39,570 --> 00:00:42,660
look at the associations or relationships between them.

16
00:00:42,660 --> 00:00:44,380
Now usually,

17
00:00:44,380 --> 00:00:46,920
there's one or two variables that you're

18
00:00:46,920 --> 00:00:49,800
really interested in that you want to understand,

19
00:00:49,800 --> 00:00:51,840
and you might be really looking at how a bunch of

20
00:00:51,840 --> 00:00:54,855
different variables correlate with those outcomes.

21
00:00:54,855 --> 00:00:56,730
However, we might not have

22
00:00:56,730 --> 00:00:58,230
a sophisticated theory or

23
00:00:58,230 --> 00:01:00,090
statistical model that we're testing,

24
00:01:00,090 --> 00:01:01,980
we might just be interested in the pattern of

25
00:01:01,980 --> 00:01:03,945
correlations and that is a fine place to start.

26
00:01:03,945 --> 00:01:05,700
It can be a little bit more exploratory,

27
00:01:05,700 --> 00:01:07,005
so there are some things we need to

28
00:01:07,005 --> 00:01:09,420
take into consideration, we'll do that.

29
00:01:09,420 --> 00:01:11,220
But there's a good place to start.

30
00:01:11,220 --> 00:01:12,690
In fact, whenever I'm doing

31
00:01:12,690 --> 00:01:14,370
more sophisticated data modeling,

32
00:01:14,370 --> 00:01:17,160
I usually try to start here as well because I

33
00:01:17,160 --> 00:01:20,230
can map out the associations among my variables.

34
00:01:20,230 --> 00:01:23,690
So, for this lesson I want to give you an example of

35
00:01:23,690 --> 00:01:25,230
dataset and we're actually going to

36
00:01:25,230 --> 00:01:27,330
look at this dataset in the lab.

37
00:01:27,330 --> 00:01:30,400
This dataset looks at correlates of national happiness.

38
00:01:30,400 --> 00:01:33,910
Got data from 155 different geographic regions,

39
00:01:33,910 --> 00:01:35,705
the data are from 2017,

40
00:01:35,705 --> 00:01:37,590
and we'll actually be able to look at what

41
00:01:37,590 --> 00:01:40,825
predicts or correlates with national happiness.

42
00:01:40,825 --> 00:01:43,090
Here, I've got an example.

43
00:01:43,090 --> 00:01:45,260
You might be wondering if

44
00:01:45,260 --> 00:01:48,170
generosity is related to happiness.

45
00:01:48,170 --> 00:01:50,345
Stop and think about that for a moment.

46
00:01:50,345 --> 00:01:52,670
More generous people might be more happy,

47
00:01:52,670 --> 00:01:54,230
maybe we think being generous is

48
00:01:54,230 --> 00:01:56,665
uplifting for the soul perhaps.

49
00:01:56,665 --> 00:01:59,080
So, maybe we would be interested in looking at that.

50
00:01:59,080 --> 00:02:00,110
So, we've got some data on

51
00:02:00,110 --> 00:02:02,420
the generosity of different regions,

52
00:02:02,420 --> 00:02:03,860
I've plotted that on the x-axis

53
00:02:03,860 --> 00:02:05,630
here and then I've got data

54
00:02:05,630 --> 00:02:09,645
on the happiness of those regions plotted on the y-axis.

55
00:02:09,645 --> 00:02:11,950
We see here, there is

56
00:02:11,950 --> 00:02:15,680
a not quite statistically significant association.

57
00:02:15,680 --> 00:02:17,630
I can't really reject

58
00:02:17,630 --> 00:02:20,020
the null hypothesis that there's nothing here.

59
00:02:20,020 --> 00:02:21,890
There was a p of 0.054.

60
00:02:21,890 --> 00:02:24,545
This is always frustrating when this happens.

61
00:02:24,545 --> 00:02:25,730
This tells me there's about

62
00:02:25,730 --> 00:02:28,040
a five to six percent risk that I could have

63
00:02:28,040 --> 00:02:30,650
gotten this even if there was nothing there.

64
00:02:30,650 --> 00:02:32,290
This could be just due to change.

65
00:02:32,290 --> 00:02:33,920
So, I'm not really able to draw

66
00:02:33,920 --> 00:02:37,180
the conclusion that generosity and happiness are related,

67
00:02:37,180 --> 00:02:39,905
but I'm thinking that they might be.

68
00:02:39,905 --> 00:02:42,540
This is what we do when we

69
00:02:42,540 --> 00:02:44,545
do bi-variant correlational analysis.

70
00:02:44,545 --> 00:02:46,720
I take a pair of variables and I try to

71
00:02:46,720 --> 00:02:49,675
tell the story of those variables.

72
00:02:49,675 --> 00:02:52,990
I will often focus and maybe even try

73
00:02:52,990 --> 00:02:56,495
to plot out the ones that I'm most interested in.

74
00:02:56,495 --> 00:02:59,830
Dang, it would have been so neat

75
00:02:59,830 --> 00:03:03,555
if these two things had been significantly correlated.

76
00:03:03,555 --> 00:03:05,650
They're not, so I need to take into

77
00:03:05,650 --> 00:03:09,015
consideration what a non-significant finding means.

78
00:03:09,015 --> 00:03:10,725
In this case,

79
00:03:10,725 --> 00:03:13,180
nonsignificant doesn't mean there's nothing there,

80
00:03:13,180 --> 00:03:14,470
it just means I don't have

81
00:03:14,470 --> 00:03:17,020
strong enough evidence to say there is a link.

82
00:03:17,020 --> 00:03:19,750
Bummer. Of course, often,

83
00:03:19,750 --> 00:03:21,820
we will not just look at one pair of variables,

84
00:03:21,820 --> 00:03:24,130
but we will look at many pairs of variables.

85
00:03:24,130 --> 00:03:26,530
So, on the next slide I actually want to show

86
00:03:26,530 --> 00:03:28,920
you a correlation map or grid.

87
00:03:28,920 --> 00:03:33,395
Let's take a look. Here, we have a correlation grid.

88
00:03:33,395 --> 00:03:35,735
Now, this is done visually.

89
00:03:35,735 --> 00:03:38,250
So, we see that on the x-axis and on

90
00:03:38,250 --> 00:03:40,895
the y-axis are a set of variables,

91
00:03:40,895 --> 00:03:42,880
and here, I've got many different variables.

92
00:03:42,880 --> 00:03:45,970
I've got generosity, the freedom score for that area,

93
00:03:45,970 --> 00:03:47,430
people's trust in government,

94
00:03:47,430 --> 00:03:49,180
the degree to which that area is

95
00:03:49,180 --> 00:03:51,090
classified as a dystopia,

96
00:03:51,090 --> 00:03:52,600
that sounds very negative,

97
00:03:52,600 --> 00:03:55,140
family values, the happiness score,

98
00:03:55,140 --> 00:03:57,160
gross domestic product, all sorts of different things

99
00:03:57,160 --> 00:04:00,390
about a region that we might be interested in looking at.

100
00:04:00,390 --> 00:04:01,990
This is a grid,

101
00:04:01,990 --> 00:04:03,430
so I can look at the correlation

102
00:04:03,430 --> 00:04:05,575
between any pairs of variables.

103
00:04:05,575 --> 00:04:08,080
For instance, I see or hear that

104
00:04:08,080 --> 00:04:12,610
happiness is correlating with GDP at 0.81.

105
00:04:12,610 --> 00:04:14,560
If you look at the column or row for

106
00:04:14,560 --> 00:04:17,680
happiness and the column or row for GDP,

107
00:04:17,680 --> 00:04:20,180
the intersection of those two is 0.81.

108
00:04:20,180 --> 00:04:22,060
So, this shows me the correlations

109
00:04:22,060 --> 00:04:24,425
between every possible pair of variables.

110
00:04:24,425 --> 00:04:25,810
Now, this information is

111
00:04:25,810 --> 00:04:27,635
repeated twice because it's a grid.

112
00:04:27,635 --> 00:04:29,320
On the lower diagonal I have

113
00:04:29,320 --> 00:04:31,600
the number of the correlation.

114
00:04:31,600 --> 00:04:32,860
For instance, I find

115
00:04:32,860 --> 00:04:35,710
that happy countries tend to be more free.

116
00:04:35,710 --> 00:04:39,010
You see the column for freedom in the row for happiness,

117
00:04:39,010 --> 00:04:41,495
they intersect at point 0.57.

118
00:04:41,495 --> 00:04:44,170
But I also have this represented

119
00:04:44,170 --> 00:04:46,635
visually up top with those circles.

120
00:04:46,635 --> 00:04:48,050
So, the bigger the circle,

121
00:04:48,050 --> 00:04:49,525
the stronger the correlation,

122
00:04:49,525 --> 00:04:51,120
and then if it's blue,

123
00:04:51,120 --> 00:04:52,600
it is a positive correlation,

124
00:04:52,600 --> 00:04:54,585
if it's red, it is a negative correlation.

125
00:04:54,585 --> 00:04:56,920
So, these are all positively correlated variables.

126
00:04:56,920 --> 00:04:58,570
This is just a nice easy way

127
00:04:58,570 --> 00:04:59,815
to map out your correlations.

128
00:04:59,815 --> 00:05:02,050
We immediately see the big correlations

129
00:05:02,050 --> 00:05:03,570
with those big blue circles.

130
00:05:03,570 --> 00:05:06,240
Family and happiness tend to correlate highly.

131
00:05:06,240 --> 00:05:08,940
Family and GDP,and happiness, in fact,

132
00:05:08,940 --> 00:05:11,620
they're all correlating highly, family, GDP, happiness,

133
00:05:11,620 --> 00:05:13,060
life expectancy, this is

134
00:05:13,060 --> 00:05:14,980
a cluster of correlated variables,

135
00:05:14,980 --> 00:05:16,340
and I can quickly see that

136
00:05:16,340 --> 00:05:18,860
those things are correlating together,

137
00:05:18,860 --> 00:05:21,085
and that helps me map out or

138
00:05:21,085 --> 00:05:24,115
understand the relationships that are going on here.

139
00:05:24,115 --> 00:05:26,545
Now you don't have to do this,

140
00:05:26,545 --> 00:05:29,020
but I did actually go ahead and put

141
00:05:29,020 --> 00:05:33,145
a little X over the nonsignificant correlations.

142
00:05:33,145 --> 00:05:35,530
Those are ones where the correlation was not big

143
00:05:35,530 --> 00:05:37,985
enough to reject the null hypothesis.

144
00:05:37,985 --> 00:05:39,320
Remember, the null hypothesis

145
00:05:39,320 --> 00:05:40,670
says that there's nothing there,

146
00:05:40,670 --> 00:05:42,985
it's just all a coincidence in your sample.

147
00:05:42,985 --> 00:05:45,690
So, I can't really say much about those.

148
00:05:45,690 --> 00:05:49,555
So, this is a correlation, greater correlation matrix.

149
00:05:49,555 --> 00:05:51,225
This is actually sometimes called a

150
00:05:51,225 --> 00:05:53,735
corollary gram or a correlation plot,

151
00:05:53,735 --> 00:05:55,910
and I will actually walk you through

152
00:05:55,910 --> 00:05:59,035
production of this plot in the lab.

153
00:05:59,035 --> 00:06:02,860
Let's take a moment though and consider a few things we

154
00:06:02,860 --> 00:06:07,345
should be cognizant of when looking at a plot like this.

155
00:06:07,345 --> 00:06:10,525
The first is that we are analyzing many correlations.

156
00:06:10,525 --> 00:06:14,880
Remember that when we use statistical significance tests,

157
00:06:14,880 --> 00:06:19,030
we are saying that I would only get a result this big,

158
00:06:19,030 --> 00:06:22,420
five percent of the time if there was nothing there.

159
00:06:22,420 --> 00:06:23,910
So in other words,

160
00:06:23,910 --> 00:06:26,820
I've got about a five percent false positive risk.

161
00:06:26,820 --> 00:06:29,710
Well, how many correlations do I have up here?

162
00:06:29,710 --> 00:06:31,645
In fact, I've got quite a lot.

163
00:06:31,645 --> 00:06:34,110
So, because I'm doing a lot of analysis,

164
00:06:34,110 --> 00:06:35,860
the possibility that I would run

165
00:06:35,860 --> 00:06:37,705
into a false positive gross,

166
00:06:37,705 --> 00:06:39,309
and in fact, it grows exponentially

167
00:06:39,309 --> 00:06:40,810
the more correlations I test.

168
00:06:40,810 --> 00:06:42,290
So, please keep that in mind.

169
00:06:42,290 --> 00:06:43,390
If you find something

170
00:06:43,390 --> 00:06:46,240
surprising and you're testing a lot of them,

171
00:06:46,240 --> 00:06:48,020
you might consider trying to replicate it.

172
00:06:48,020 --> 00:06:50,350
Maybe set half your data aside before

173
00:06:50,350 --> 00:06:53,585
you do the analysis if you've got enough data to spare.

174
00:06:53,585 --> 00:06:55,960
Another consideration is power.

175
00:06:55,960 --> 00:06:58,550
Remember, to reject the null hypothesis

176
00:06:58,550 --> 00:07:00,025
you have to have power.

177
00:07:00,025 --> 00:07:02,260
As we learned early in the course,

178
00:07:02,260 --> 00:07:05,055
you can actually run a power analysis based on

179
00:07:05,055 --> 00:07:08,115
the sample size of 155 regions,

180
00:07:08,115 --> 00:07:10,110
I know that I have power to detect

181
00:07:10,110 --> 00:07:12,620
correlations as low as 0.22.

182
00:07:12,620 --> 00:07:15,240
So, if there are correlations that are weaker than that,

183
00:07:15,240 --> 00:07:17,670
they might still be real correlations,

184
00:07:17,670 --> 00:07:20,240
I just might not have the power to detect them.

185
00:07:20,240 --> 00:07:22,870
So, if I've got a nonsignificant finding

186
00:07:22,870 --> 00:07:25,050
that doesn't necessarily mean it's zero,

187
00:07:25,050 --> 00:07:26,220
it might just be under

188
00:07:26,220 --> 00:07:28,940
the threshold of what I have the ability to detect.

189
00:07:28,940 --> 00:07:30,705
So, all these X's up here,

190
00:07:30,705 --> 00:07:32,620
they don't mean there's nothing there,

191
00:07:32,620 --> 00:07:34,905
it just means I don't have the ability

192
00:07:34,905 --> 00:07:37,530
to detect correlations that small,

193
00:07:37,530 --> 00:07:39,870
and it is indeed possible that there's nothing there.

194
00:07:39,870 --> 00:07:41,880
I just can't say a whole lot about them.

195
00:07:41,880 --> 00:07:43,890
So, those are some things to keep in mind

196
00:07:43,890 --> 00:07:47,050
when you're analyzing a correlation matrix.

