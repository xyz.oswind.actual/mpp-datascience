0
00:00:01,750 --> 00:00:03,775
>> Now, I want to talk

1
00:00:03,775 --> 00:00:07,055
about Multivariate Correlational research.

2
00:00:07,055 --> 00:00:08,290
In the previous lesson,

3
00:00:08,290 --> 00:00:10,370
we talked about Bivariate Correlational

4
00:00:10,370 --> 00:00:11,380
research when we looked at

5
00:00:11,380 --> 00:00:14,680
pairs of variables that might be of interest to us.

6
00:00:14,680 --> 00:00:16,390
For example, we could look at whether

7
00:00:16,390 --> 00:00:19,450
more grateful regions tend to be happier,

8
00:00:19,450 --> 00:00:21,070
or in fact, we could look at a grid of

9
00:00:21,070 --> 00:00:23,830
every possible correlation. That's exploratory.

10
00:00:23,830 --> 00:00:26,955
We tested a lot of things at once,

11
00:00:26,955 --> 00:00:28,780
so we had to be a little concerned

12
00:00:28,780 --> 00:00:31,315
about some false positives that might show up.

13
00:00:31,315 --> 00:00:34,800
But what if we had a more sophisticated question?

14
00:00:34,800 --> 00:00:36,540
A more sophisticated model.

15
00:00:36,540 --> 00:00:39,510
What if I had a more sophisticated theory that said,

16
00:00:39,510 --> 00:00:43,780
"I think that this one thing is causing that thing,

17
00:00:43,780 --> 00:00:45,710
and I really want to be able to test that?"

18
00:00:45,710 --> 00:00:47,260
We know that we can't

19
00:00:47,260 --> 00:00:49,285
test causation with correlational data.

20
00:00:49,285 --> 00:00:51,660
We know that by now. We do know that by now,

21
00:00:51,660 --> 00:00:52,930
right? We know that by now.

22
00:00:52,930 --> 00:00:55,210
So, that's okay, but we

23
00:00:55,210 --> 00:00:57,740
can look for evidence consistent with it,

24
00:00:57,740 --> 00:00:59,185
and that is what we're going to do

25
00:00:59,185 --> 00:01:02,225
with multivariate correlational research.

26
00:01:02,225 --> 00:01:04,300
Multivariate Correlational research involves

27
00:01:04,300 --> 00:01:06,100
identifying a target outcome,

28
00:01:06,100 --> 00:01:08,035
something that you want to predict.

29
00:01:08,035 --> 00:01:10,810
You're then going to find a number of predictors,

30
00:01:10,810 --> 00:01:12,580
and establish what we would say is

31
00:01:12,580 --> 00:01:15,165
the unique role of each predictor.

32
00:01:15,165 --> 00:01:18,160
So, for example, maybe I'm

33
00:01:18,160 --> 00:01:19,900
interested in predicting whether

34
00:01:19,900 --> 00:01:21,170
people are engaged at work,

35
00:01:21,170 --> 00:01:22,720
employee engagement, and I have a set

36
00:01:22,720 --> 00:01:24,875
of predictors that I want to look at.

37
00:01:24,875 --> 00:01:27,385
I might be interested in which predictors

38
00:01:27,385 --> 00:01:30,010
actually seem to matter for engagement,

39
00:01:30,010 --> 00:01:32,350
and I might look at the effect of each of

40
00:01:32,350 --> 00:01:34,810
those controlling for the others.

41
00:01:34,810 --> 00:01:36,270
So, I might look, for instance,

42
00:01:36,270 --> 00:01:37,760
at how stressed-out people are,

43
00:01:37,760 --> 00:01:39,415
how many hours that they work,

44
00:01:39,415 --> 00:01:41,020
and I might be interested in kind of

45
00:01:41,020 --> 00:01:42,910
the unique contribution of

46
00:01:42,910 --> 00:01:44,590
each of those things to engagement,

47
00:01:44,590 --> 00:01:46,330
that might be interesting to me.

48
00:01:46,330 --> 00:01:48,520
I can do that statistically if I have

49
00:01:48,520 --> 00:01:50,380
data on all of these variables,

50
00:01:50,380 --> 00:01:53,320
I can run some, what we call regression analysis,

51
00:01:53,320 --> 00:01:54,655
with a multiple regression.

52
00:01:54,655 --> 00:01:55,930
One of many different kinds of

53
00:01:55,930 --> 00:01:58,120
regression models to try to look at that,

54
00:01:58,120 --> 00:01:59,440
and I can look at each

55
00:01:59,440 --> 00:02:02,465
variable controlling for the others.

56
00:02:02,465 --> 00:02:05,620
However, I think it bears repeating,

57
00:02:05,620 --> 00:02:09,160
you cannot show causation from correlational data.

58
00:02:09,160 --> 00:02:12,525
So, we're finding evidence consistent with our theory.

59
00:02:12,525 --> 00:02:15,195
But we're not really testing our theory,

60
00:02:15,195 --> 00:02:17,160
we want to know, for instance,

61
00:02:17,160 --> 00:02:18,730
what factors impact engagement,

62
00:02:18,730 --> 00:02:20,450
we really do need to actually

63
00:02:20,450 --> 00:02:22,915
test it out by experimenting.

64
00:02:22,915 --> 00:02:24,070
Put together some sort of

65
00:02:24,070 --> 00:02:26,690
program that moves around the causes,

66
00:02:26,690 --> 00:02:28,990
and see if engagement follows.

67
00:02:28,990 --> 00:02:32,410
That's expensive and it's time-consuming,

68
00:02:32,410 --> 00:02:34,825
and it's effortful, and if I've got data,

69
00:02:34,825 --> 00:02:35,920
why don't we start with that?

70
00:02:35,920 --> 00:02:38,180
So often, we start off with some correlational data.

71
00:02:38,180 --> 00:02:41,440
So, let's take a look at how this works.

72
00:02:41,440 --> 00:02:43,785
I'm going to use these circles

73
00:02:43,785 --> 00:02:46,310
as metaphors for correlation.

74
00:02:46,310 --> 00:02:49,020
Overlapping circles are correlated variables,

75
00:02:49,020 --> 00:02:52,019
and more overlapping circles are more correlated,

76
00:02:52,019 --> 00:02:54,900
less overlapping circles are less correlated.

77
00:02:54,900 --> 00:02:56,565
So, here I've got employee engagement.

78
00:02:56,565 --> 00:02:57,930
I want to predict it,

79
00:02:57,930 --> 00:02:59,850
and say I find for instance that

80
00:02:59,850 --> 00:03:02,565
stressed-out employees tend to be less engaged.

81
00:03:02,565 --> 00:03:05,520
These two things are modestly correlated.

82
00:03:05,520 --> 00:03:07,860
In a Bivariate Correlational world

83
00:03:07,860 --> 00:03:09,460
like what we did in the previous lesson,

84
00:03:09,460 --> 00:03:11,040
that's as far as I would go,

85
00:03:11,040 --> 00:03:13,065
but now we're doing multivariate,

86
00:03:13,065 --> 00:03:16,665
so I could actually consider the fact that actually hours

87
00:03:16,665 --> 00:03:18,750
worked and stress and engagement

88
00:03:18,750 --> 00:03:21,120
are also all correlated with each other.

89
00:03:21,120 --> 00:03:22,500
You see that hours worked

90
00:03:22,500 --> 00:03:24,615
correlates with engagement a little bit,

91
00:03:24,615 --> 00:03:27,615
and hours and stress correlate a little bit.

92
00:03:27,615 --> 00:03:30,690
Not perfectly, you see here actually that the people who

93
00:03:30,690 --> 00:03:34,070
work more hours are maybe a little bit more stressed,

94
00:03:34,070 --> 00:03:36,225
but it's not necessarily a huge relationship.

95
00:03:36,225 --> 00:03:38,205
They're not the same information,

96
00:03:38,205 --> 00:03:42,360
they're partially redundant, they're overlapping ideas.

97
00:03:42,360 --> 00:03:44,675
All of these things overlap a little bit.

98
00:03:44,675 --> 00:03:49,010
So, if we want to understand engagement,

99
00:03:49,010 --> 00:03:52,065
which variable gets credit?

100
00:03:52,065 --> 00:03:53,940
So, what we're going to do is

101
00:03:53,940 --> 00:03:55,530
we can actually statistically

102
00:03:55,530 --> 00:03:58,915
look at the association between each predictor,

103
00:03:58,915 --> 00:04:00,970
stress, and hours, controlling

104
00:04:00,970 --> 00:04:02,515
for the others. So, let's try it.

105
00:04:02,515 --> 00:04:06,180
Let's statistically remove the effect of hours worked.

106
00:04:06,180 --> 00:04:07,920
This would be if you were doing

107
00:04:07,920 --> 00:04:10,030
a multiple regression analysis,

108
00:04:10,030 --> 00:04:12,420
you would look at what we would say the slope for stress,

109
00:04:12,420 --> 00:04:14,550
and we're going to get a chance to do that in

110
00:04:14,550 --> 00:04:16,965
the lab that goes with this lesson.

111
00:04:16,965 --> 00:04:20,220
But if you were to take the stress variable and use it to

112
00:04:20,220 --> 00:04:23,570
predict engagement with the effective hours removed,

113
00:04:23,570 --> 00:04:26,190
conceptually, it's something like this.

114
00:04:26,190 --> 00:04:27,960
I can statistically carve out

115
00:04:27,960 --> 00:04:31,360
any variance that's due to hours,

116
00:04:31,360 --> 00:04:33,480
and I can remove it.

117
00:04:33,480 --> 00:04:36,090
Now, I have kind of gutted

118
00:04:36,090 --> 00:04:37,840
my stress variable a little bit,

119
00:04:37,840 --> 00:04:40,005
and we might need to worry about that.

120
00:04:40,005 --> 00:04:41,830
It's called tolerance, and we can

121
00:04:41,830 --> 00:04:43,760
look at that statistically.

122
00:04:43,760 --> 00:04:47,400
But the big take

123
00:04:47,400 --> 00:04:50,995
home here is that even after I remove hours,

124
00:04:50,995 --> 00:04:52,980
I still see overlap

125
00:04:52,980 --> 00:04:55,460
between what's remaining of stress and engagement.

126
00:04:55,460 --> 00:04:57,540
Stress even with hours removed,

127
00:04:57,540 --> 00:05:00,105
is still correlating with engagement.

128
00:05:00,105 --> 00:05:03,090
That tells me that whatever that association is,

129
00:05:03,090 --> 00:05:05,100
it's not just due to the hours worked.

130
00:05:05,100 --> 00:05:08,160
There is something important about stress in

131
00:05:08,160 --> 00:05:11,370
addition to hours that is predicting engagement.

132
00:05:11,370 --> 00:05:13,500
So, that is useful information.

133
00:05:13,500 --> 00:05:16,900
I might consider removing some other correlates too.

134
00:05:16,900 --> 00:05:19,370
There might be some other things related to stress

135
00:05:19,370 --> 00:05:22,170
like for instance responsibility level,

136
00:05:22,170 --> 00:05:25,440
or things like that I might consider also removing.

137
00:05:25,440 --> 00:05:26,980
But at the end of the day,

138
00:05:26,980 --> 00:05:30,330
if I remove enough variables

139
00:05:30,330 --> 00:05:31,590
if I control for enough things,

140
00:05:31,590 --> 00:05:33,930
and I still see an association that does

141
00:05:33,930 --> 00:05:38,550
suggest or is consistent with my causal theory,

142
00:05:38,550 --> 00:05:40,950
certainly does not show my causal theory and it,

143
00:05:40,950 --> 00:05:43,520
in fact, doesn't even tell me which way it goes.

144
00:05:43,520 --> 00:05:45,575
As we talked earlier in this course,

145
00:05:45,575 --> 00:05:47,010
could even go backwards,

146
00:05:47,010 --> 00:05:49,395
it could be that engagement leads to stress for instance.

147
00:05:49,395 --> 00:05:51,110
So, I don't really know which way it goes.

148
00:05:51,110 --> 00:05:52,890
But I can remove the effect

149
00:05:52,890 --> 00:05:55,740
of other correlates or predictors,

150
00:05:55,740 --> 00:05:57,270
and see if my relationship

151
00:05:57,270 --> 00:06:00,165
still survives, and that's important.

152
00:06:00,165 --> 00:06:02,460
I can flip it around by the way.

153
00:06:02,460 --> 00:06:04,230
I can remove the effect of

154
00:06:04,230 --> 00:06:05,610
stress and look at

155
00:06:05,610 --> 00:06:08,050
whether hours correlates with engagement,

156
00:06:08,050 --> 00:06:10,120
and that would be in a regression looking

157
00:06:10,120 --> 00:06:12,300
at the slope for hours,

158
00:06:12,300 --> 00:06:14,165
and I see here that even after I

159
00:06:14,165 --> 00:06:16,950
removed the effect of stress,

160
00:06:16,950 --> 00:06:19,710
there is maybe a tiny little bit

161
00:06:19,710 --> 00:06:21,640
of overlap between hours and engagement,

162
00:06:21,640 --> 00:06:23,100
but it's really not there,

163
00:06:23,100 --> 00:06:24,900
it's pretty close to zero.

164
00:06:24,900 --> 00:06:26,455
The percentage of my variable that's

165
00:06:26,455 --> 00:06:28,340
overlapping is almost nothing,

166
00:06:28,340 --> 00:06:30,240
and that's really important for me to

167
00:06:30,240 --> 00:06:31,830
know because if I were just looking

168
00:06:31,830 --> 00:06:33,450
at the bivariate correlations,

169
00:06:33,450 --> 00:06:35,855
and just looking at the overall overlap,

170
00:06:35,855 --> 00:06:37,610
it's hard to say for overall overlap.

171
00:06:37,610 --> 00:06:40,110
If I'm just looking at the overall overlap,

172
00:06:40,110 --> 00:06:43,350
I might mistakenly think that hours worked might

173
00:06:43,350 --> 00:06:45,839
actually affect people's engagement

174
00:06:45,839 --> 00:06:47,230
in some sort of direct way,

175
00:06:47,230 --> 00:06:49,680
but when I remove the effect of stress,

176
00:06:49,680 --> 00:06:52,200
I see that that's just not the case.

177
00:06:52,200 --> 00:06:55,385
There might be some effective hours on engagement,

178
00:06:55,385 --> 00:06:56,580
but if it's happening,

179
00:06:56,580 --> 00:06:58,070
it's happening via stress.

180
00:06:58,070 --> 00:07:00,240
Stress is the most important variable

181
00:07:00,240 --> 00:07:02,045
now that I should be considering,

182
00:07:02,045 --> 00:07:04,750
and if stress is the most important variable, well,

183
00:07:04,750 --> 00:07:06,760
then if I were to try to intervene,

184
00:07:06,760 --> 00:07:08,505
that's what I would want to work on.

185
00:07:08,505 --> 00:07:12,150
Again, I don't know cause-effect relationships here,

186
00:07:12,150 --> 00:07:14,760
but it is consistent with a causal theory.

187
00:07:14,760 --> 00:07:17,040
I can tell some initial stories with

188
00:07:17,040 --> 00:07:20,010
just a few analyses before I

189
00:07:20,010 --> 00:07:22,980
say go and try to implement a stress reduction program,

190
00:07:22,980 --> 00:07:26,175
to see if my employees become more engaged for instance.

191
00:07:26,175 --> 00:07:28,560
There's another metaphor we could use for

192
00:07:28,560 --> 00:07:30,380
this other than the Venn diagrams.

193
00:07:30,380 --> 00:07:31,620
I do like the Venn diagrams,

194
00:07:31,620 --> 00:07:33,220
but there is another metaphor we can use.

195
00:07:33,220 --> 00:07:35,070
This is also what is

196
00:07:35,070 --> 00:07:37,000
happening when we do a regression analysis,

197
00:07:37,000 --> 00:07:39,150
we do some statistical control.

198
00:07:39,150 --> 00:07:42,465
For the unique role of stress,

199
00:07:42,465 --> 00:07:44,090
what we're essentially doing is we are

200
00:07:44,090 --> 00:07:46,310
adjusting everyone's stress scores

201
00:07:46,310 --> 00:07:48,440
to what they would have

202
00:07:48,440 --> 00:07:51,175
been if everyone worked the same hours.

203
00:07:51,175 --> 00:07:53,600
When I'm removing the effect of hours,

204
00:07:53,600 --> 00:07:55,399
what I'm really doing is I'm adjusting

205
00:07:55,399 --> 00:07:57,970
everyone's stress scores just a little bit,

206
00:07:57,970 --> 00:07:59,059
and I am correlating

207
00:07:59,059 --> 00:08:02,540
those adjusted stress scores with the outcome.

208
00:08:02,540 --> 00:08:03,875
So, when I say I'm

209
00:08:03,875 --> 00:08:05,970
removing the effective hours, I mean it.

210
00:08:05,970 --> 00:08:07,505
I'm actually mapping out

211
00:08:07,505 --> 00:08:09,870
how much hours and stress are correlated,

212
00:08:09,870 --> 00:08:13,415
I'm actually adjusting the stress scores so that

213
00:08:13,415 --> 00:08:15,440
they are what they would have

214
00:08:15,440 --> 00:08:17,870
been if everyone worked the same hours,

215
00:08:17,870 --> 00:08:20,120
and then I'm seeing if that predicts my outcome.

216
00:08:20,120 --> 00:08:22,190
So, and in fact,

217
00:08:22,190 --> 00:08:23,885
we saw in our little diagram here,

218
00:08:23,885 --> 00:08:26,000
that indeed that was the case after I adjusted

219
00:08:26,000 --> 00:08:27,590
everyone's stress scores to

220
00:08:27,590 --> 00:08:29,240
remove the effective hours worked,

221
00:08:29,240 --> 00:08:30,515
I'm still seeing overlap.

222
00:08:30,515 --> 00:08:33,520
They're still correlated. This is all

223
00:08:33,520 --> 00:08:35,080
happening automatically when you

224
00:08:35,080 --> 00:08:37,255
run a multiple regression analysis.

225
00:08:37,255 --> 00:08:39,370
It's not something that you have to do.

226
00:08:39,370 --> 00:08:41,680
in fact, it's not even a separate step,

227
00:08:41,680 --> 00:08:44,110
it's just how the multiple regression works,

228
00:08:44,110 --> 00:08:46,765
but it helps you to understand what it's doing.

229
00:08:46,765 --> 00:08:50,425
You're actually looking at adjusted stress scores

230
00:08:50,425 --> 00:08:52,930
to reflect what they would be if

231
00:08:52,930 --> 00:08:55,875
hours were held constant statistically.

232
00:08:55,875 --> 00:08:58,375
Works the same way for the other variable.

233
00:08:58,375 --> 00:09:01,510
If I wanted to look at the effective hours on engagement,

234
00:09:01,510 --> 00:09:03,385
I could adjust everyone's hours

235
00:09:03,385 --> 00:09:05,530
to a of adjusted the score,

236
00:09:05,530 --> 00:09:07,045
to what they would have been

237
00:09:07,045 --> 00:09:09,220
if everyone had the same stress level.

238
00:09:09,220 --> 00:09:11,120
Then I could see if those two correlate,

239
00:09:11,120 --> 00:09:13,880
and indeed they do not really correlate now.

240
00:09:13,880 --> 00:09:15,490
I've adjusted everyone's hours

241
00:09:15,490 --> 00:09:17,020
to what I would think your hours would

242
00:09:17,020 --> 00:09:21,275
be if I were to somehow hold stress levels constant.

243
00:09:21,275 --> 00:09:23,770
Here we see hours are not overlapping.

244
00:09:23,770 --> 00:09:26,440
So, it's just another metaphor for what

245
00:09:26,440 --> 00:09:27,775
the analysis is doing

246
00:09:27,775 --> 00:09:29,655
when you're controlling for something.

247
00:09:29,655 --> 00:09:31,480
You can immediately imagine

248
00:09:31,480 --> 00:09:34,450
situations where this doesn't make a whole lot of sense.

249
00:09:34,450 --> 00:09:38,470
Maybe hours and stress are so inexorably linked at

250
00:09:38,470 --> 00:09:40,870
your organization that it doesn't make sense to look

251
00:09:40,870 --> 00:09:43,585
at stress-free hours worked.

252
00:09:43,585 --> 00:09:45,190
I still think it kind of does,

253
00:09:45,190 --> 00:09:47,260
it's kind of useful for me to know hours doesn't

254
00:09:47,260 --> 00:09:50,250
really matter for engagement outside of stress,

255
00:09:50,250 --> 00:09:52,180
but something to keep in

256
00:09:52,180 --> 00:09:54,160
mind when you're interpreting the results.

257
00:09:54,160 --> 00:09:56,650
One last caveat I'll put on here is that

258
00:09:56,650 --> 00:09:59,440
statistical control is only as good as your measurement.

259
00:09:59,440 --> 00:10:00,760
If you control for something that

260
00:10:00,760 --> 00:10:02,350
you did not measure well,

261
00:10:02,350 --> 00:10:04,450
you haven't actually controlled for it, or worse,

262
00:10:04,450 --> 00:10:05,590
you've controlled for something

263
00:10:05,590 --> 00:10:06,880
you measured accidentally.

264
00:10:06,880 --> 00:10:09,520
So, make sure you're testing good measures before

265
00:10:09,520 --> 00:10:13,350
you go ahead and do a regression analysis.

