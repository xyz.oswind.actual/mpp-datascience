0
00:00:01,670 --> 00:00:04,320
>> In a previous lesson, I taught you

1
00:00:04,320 --> 00:00:06,620
the advantages of a within subjects design.

2
00:00:06,620 --> 00:00:08,250
Put people in one group to

3
00:00:08,250 --> 00:00:10,670
start and then instead of being done with them,

4
00:00:10,670 --> 00:00:12,660
swap them over to another group.

5
00:00:12,660 --> 00:00:14,070
Look at how much their scores

6
00:00:14,070 --> 00:00:16,205
change when they change groups.

7
00:00:16,205 --> 00:00:18,060
Instead of just comparing two different groups

8
00:00:18,060 --> 00:00:19,110
against each other,

9
00:00:19,110 --> 00:00:21,510
I can track people over time and see how much

10
00:00:21,510 --> 00:00:24,285
the different groups affect each individual person.

11
00:00:24,285 --> 00:00:27,150
It is a much more powerful and efficient design.

12
00:00:27,150 --> 00:00:29,790
Can you do that in a factorial space?

13
00:00:29,790 --> 00:00:32,105
Yes, absolutely.

14
00:00:32,105 --> 00:00:33,810
That's what we're going to do here.

15
00:00:33,810 --> 00:00:35,680
It is a within subjects design

16
00:00:35,680 --> 00:00:38,545
applied to the factorial experiment.

17
00:00:38,545 --> 00:00:40,680
This isn't actually a new design.

18
00:00:40,680 --> 00:00:42,130
To be honest, there's really not

19
00:00:42,130 --> 00:00:43,860
anything new in this lesson.

20
00:00:43,860 --> 00:00:46,635
I just want to point out the fact that you can do it.

21
00:00:46,635 --> 00:00:49,180
Remember, when you do a within subjects design,

22
00:00:49,180 --> 00:00:51,535
you get a power advantage.

23
00:00:51,535 --> 00:00:52,900
I'm comparing you at

24
00:00:52,900 --> 00:00:55,030
one point against you at another point.

25
00:00:55,030 --> 00:00:56,710
You're the same person.

26
00:00:56,710 --> 00:00:59,815
There's a lot of cleanliness to that data.

27
00:00:59,815 --> 00:01:01,260
It's not noisy.

28
00:01:01,260 --> 00:01:04,235
Effects stand out very clean.

29
00:01:04,235 --> 00:01:07,390
Because of that, I have a lot of power.

30
00:01:07,390 --> 00:01:09,725
I can get away with smaller samples.

31
00:01:09,725 --> 00:01:12,330
So, we like doing that.

32
00:01:12,330 --> 00:01:14,740
Any variable that you do are repeated

33
00:01:14,740 --> 00:01:16,600
measures on or within subjects on,

34
00:01:16,600 --> 00:01:19,190
you get that power advantage including,

35
00:01:19,190 --> 00:01:22,240
by the way, any interaction that you study.

36
00:01:22,240 --> 00:01:25,430
So if I'm looking at whether logo A and B differ,

37
00:01:25,430 --> 00:01:27,080
and whether that differ is depending

38
00:01:27,080 --> 00:01:29,855
on color, grayscale or color,

39
00:01:29,855 --> 00:01:31,420
if I'm doing the interaction

40
00:01:31,420 --> 00:01:34,090
between color status and logos,

41
00:01:34,090 --> 00:01:37,150
I can actually get the power advantage to

42
00:01:37,150 --> 00:01:40,595
both the within subjects variable and the interaction.

43
00:01:40,595 --> 00:01:43,290
So it's a very neat design.

44
00:01:43,290 --> 00:01:45,500
Now, if that sounds a little abstract to you,

45
00:01:45,500 --> 00:01:47,100
let's make it really concrete and see

46
00:01:47,100 --> 00:01:49,725
what this looks like in practice.

47
00:01:49,725 --> 00:01:53,250
I can make either a variable within subjects.

48
00:01:53,250 --> 00:01:57,710
So let's go ahead and take a look at this example, again.

49
00:01:57,710 --> 00:02:01,960
In this case, I've made one variable between subjects,

50
00:02:01,960 --> 00:02:04,635
and that is color.

51
00:02:04,635 --> 00:02:06,745
So at the beginning of this study,

52
00:02:06,745 --> 00:02:09,535
when you sign up or join the study,

53
00:02:09,535 --> 00:02:11,910
I flip a coin and I randomly put you

54
00:02:11,910 --> 00:02:14,640
in either the grayscale column,

55
00:02:14,640 --> 00:02:16,075
that is the left side,

56
00:02:16,075 --> 00:02:17,940
or the color column,

57
00:02:17,940 --> 00:02:20,085
that is the right side, and you see that right there.

58
00:02:20,085 --> 00:02:21,720
So you see that I've got

59
00:02:21,720 --> 00:02:23,605
those participants in the left column.

60
00:02:23,605 --> 00:02:26,055
They stay in the left column the whole time.

61
00:02:26,055 --> 00:02:27,645
I've randomly assigned them to

62
00:02:27,645 --> 00:02:30,320
be looking at the grayscale only.

63
00:02:30,320 --> 00:02:32,130
The people on the right, I've randomly

64
00:02:32,130 --> 00:02:34,035
assigned them to look at the color logo only.

65
00:02:34,035 --> 00:02:35,940
So colors are between subjects variable.

66
00:02:35,940 --> 00:02:39,330
You are in either the color or the grayscale condition.

67
00:02:39,330 --> 00:02:41,310
So that's the same as it was before.

68
00:02:41,310 --> 00:02:43,525
I haven't changed anything there.

69
00:02:43,525 --> 00:02:45,390
The only thing that I've changed

70
00:02:45,390 --> 00:02:47,450
from the previous lesson was,

71
00:02:47,450 --> 00:02:50,190
I've now made logo within subjects.

72
00:02:50,190 --> 00:02:54,450
People see both logo A and logo B.

73
00:02:54,450 --> 00:02:56,100
So if you look in that left column,

74
00:02:56,100 --> 00:02:59,010
I have those two participants there and they see

75
00:02:59,010 --> 00:03:03,245
both logo A grayscale and logo B grayscale.

76
00:03:03,245 --> 00:03:07,400
So I get that within subjects benefit for the logo.

77
00:03:07,400 --> 00:03:09,555
So I have a really high power test.

78
00:03:09,555 --> 00:03:11,710
Is logo A different than logo B?

79
00:03:11,710 --> 00:03:13,560
Do these two logos differ from each other?

80
00:03:13,560 --> 00:03:15,330
I've got a really high powered test of

81
00:03:15,330 --> 00:03:17,645
that comparison now. Same on the right.

82
00:03:17,645 --> 00:03:19,510
Those two participants there on

83
00:03:19,510 --> 00:03:21,630
the right viewing color A

84
00:03:21,630 --> 00:03:24,010
are the same people who then see color B.

85
00:03:24,010 --> 00:03:25,890
So I've repeated measures.

86
00:03:25,890 --> 00:03:31,495
I'm going within participants with respect to logo.

87
00:03:31,495 --> 00:03:33,455
However, I'm doing color between.

88
00:03:33,455 --> 00:03:36,900
You only see either grey or color. Makes sense?

89
00:03:36,900 --> 00:03:39,315
So I've got one of these two variables is

90
00:03:39,315 --> 00:03:42,165
within subjects and I have a high power test for that.

91
00:03:42,165 --> 00:03:45,650
I'm probably more interested in logo A versus logo B.

92
00:03:45,650 --> 00:03:47,905
So I'm probably going to make that the within subjects

93
00:03:47,905 --> 00:03:48,990
variable so that I get

94
00:03:48,990 --> 00:03:52,360
the most power on my most important question.

95
00:03:52,670 --> 00:03:55,180
But I could do it the other way, as well.

96
00:03:55,180 --> 00:03:58,680
In this example, I've made the logo between subjects now.

97
00:03:58,680 --> 00:04:01,240
So in this example, you see here,

98
00:04:01,240 --> 00:04:04,135
I randomly assign you to see either logo A,

99
00:04:04,135 --> 00:04:05,320
so that top row, it's

100
00:04:05,320 --> 00:04:06,625
the same participants the whole row.

101
00:04:06,625 --> 00:04:07,940
I've put you in the top row,

102
00:04:07,940 --> 00:04:09,020
you stay in the top row.

103
00:04:09,020 --> 00:04:13,510
You can see either logo A or logo B on the bottom.

104
00:04:13,510 --> 00:04:15,180
I assign you to see logo B,

105
00:04:15,180 --> 00:04:16,250
you stay in logo B.

106
00:04:16,250 --> 00:04:18,970
So in this case, logo is now between

107
00:04:18,970 --> 00:04:20,500
subjects but you see

108
00:04:20,500 --> 00:04:23,885
both the grayscale and the color versions of it.

109
00:04:23,885 --> 00:04:26,945
So in this case, color is within subjects.

110
00:04:26,945 --> 00:04:28,530
The people seeing logo A,

111
00:04:28,530 --> 00:04:31,515
see both grey and color and I can actually compare

112
00:04:31,515 --> 00:04:34,500
whether grayscale or color matters with

113
00:04:34,500 --> 00:04:35,640
high power because I'm

114
00:04:35,640 --> 00:04:38,010
repeating measures on that dimension.

115
00:04:38,010 --> 00:04:40,650
Let's use a different version of it.

116
00:04:40,650 --> 00:04:42,570
I would suggest making

117
00:04:42,570 --> 00:04:46,050
the most important variable within subjects variable.

118
00:04:46,050 --> 00:04:48,590
If you care the most about A versus B,

119
00:04:48,590 --> 00:04:51,990
then make that the within subjects variable.

120
00:04:51,990 --> 00:04:56,700
If you're most interested in color versus grayscale,

121
00:04:56,700 --> 00:04:58,780
make that the within subjects variable.

122
00:04:58,780 --> 00:05:01,665
You get a lot of statistical power on that test.

123
00:05:01,665 --> 00:05:04,255
So you're going to get the benefit of that.

124
00:05:04,255 --> 00:05:07,410
Or, if you feel like having participants look at

125
00:05:07,410 --> 00:05:10,960
four combinations, just do both.

126
00:05:10,960 --> 00:05:14,790
Use everybody in every condition, easy.

127
00:05:14,790 --> 00:05:17,130
You don't have to worry about random assignment.

128
00:05:17,130 --> 00:05:20,850
Everybody just is given one of these four permutations.

129
00:05:20,850 --> 00:05:23,160
They see all four. You randomize the order,

130
00:05:23,160 --> 00:05:25,410
and now I can look at either the effect of

131
00:05:25,410 --> 00:05:31,625
color or the effect of logo with high power.

132
00:05:31,625 --> 00:05:35,455
So, why do we like this design?

133
00:05:35,455 --> 00:05:37,420
Well, you have a lot better power

134
00:05:37,420 --> 00:05:38,910
for whatever the within subjects

135
00:05:38,910 --> 00:05:43,000
variable is and any interaction involving it.

136
00:05:43,000 --> 00:05:45,250
If you are going ahead and

137
00:05:45,250 --> 00:05:47,740
doing both factors within subjects,

138
00:05:47,740 --> 00:05:49,780
you have incredibly high power for

139
00:05:49,780 --> 00:05:52,890
every comparison you might be interested in making.

140
00:05:52,890 --> 00:05:56,215
You also have the advantage of smaller sample size.

141
00:05:56,215 --> 00:05:59,930
Remember, when you have a within subjects design,

142
00:05:59,930 --> 00:06:02,515
you're repeating your participants.

143
00:06:02,515 --> 00:06:05,300
So you get many times more data

144
00:06:05,300 --> 00:06:07,180
for every person you have.

145
00:06:07,180 --> 00:06:08,550
Because you have higher power,

146
00:06:08,550 --> 00:06:10,970
you can also get the added bonus if

147
00:06:10,970 --> 00:06:13,430
you don't need as many observations anyway,

148
00:06:13,430 --> 00:06:15,210
because you have a high powered design.

149
00:06:15,210 --> 00:06:18,485
This, truly, lets you run a study more efficiently.

150
00:06:18,485 --> 00:06:20,890
It is a more efficient design.

151
00:06:20,890 --> 00:06:23,960
However, beware issues that

152
00:06:23,960 --> 00:06:25,940
can come up with repeated measurement.

153
00:06:25,940 --> 00:06:28,330
By the time people have seen three or four logos,

154
00:06:28,330 --> 00:06:30,940
they might not be responding validly anymore.

155
00:06:30,940 --> 00:06:32,480
In fact, if it's an online survey,

156
00:06:32,480 --> 00:06:35,720
they might just start clicking through it haphazardly.

157
00:06:35,720 --> 00:06:39,060
So be aware of that. Also, beware of duration creep.

158
00:06:39,060 --> 00:06:42,020
Asking people to do a study four times is actually

159
00:06:42,020 --> 00:06:43,430
four times more annoying than

160
00:06:43,430 --> 00:06:45,020
asking them to do it one time.

161
00:06:45,020 --> 00:06:46,850
So you might consider just doing

162
00:06:46,850 --> 00:06:50,090
one factor within subjects if you're worried about that.

163
00:06:50,090 --> 00:06:51,680
Either way, this is

164
00:06:51,680 --> 00:06:55,025
a useful way to test the answer to your question.

165
00:06:55,025 --> 00:06:58,355
It combines all the advantages of a factorial design

166
00:06:58,355 --> 00:07:00,620
and it combines all the advantages

167
00:07:00,620 --> 00:07:02,815
of within subjects designs.

168
00:07:02,815 --> 00:07:05,880
You do need to worry about the bloat of

169
00:07:05,880 --> 00:07:07,350
your study but that is something that

170
00:07:07,350 --> 00:07:09,325
can be solved in the moment.

171
00:07:09,325 --> 00:07:12,405
It is an effective and efficient way of

172
00:07:12,405 --> 00:07:13,770
answering your most important

173
00:07:13,770 --> 00:07:16,330
questions with an experiment.

