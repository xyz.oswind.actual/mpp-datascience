0
00:00:01,670 --> 00:00:04,625
>> Sometimes we don't want to or

1
00:00:04,625 --> 00:00:07,020
can't do a between groups experiment.

2
00:00:07,020 --> 00:00:09,020
Sometimes it's just not efficient.

3
00:00:09,020 --> 00:00:11,210
Sometimes we don't have enough people to do it.

4
00:00:11,210 --> 00:00:15,000
Sometimes we want to do another experimental design.

5
00:00:15,000 --> 00:00:16,820
Well, we have you covered

6
00:00:16,820 --> 00:00:20,350
within subjects or within groups experiment.

7
00:00:20,350 --> 00:00:23,480
In within subjects or within groups experiment,

8
00:00:23,480 --> 00:00:25,235
participants are put in

9
00:00:25,235 --> 00:00:27,905
every condition in a random order.

10
00:00:27,905 --> 00:00:31,260
We actually repeat the measurement.

11
00:00:31,260 --> 00:00:33,530
We reuse our participants.

12
00:00:33,530 --> 00:00:36,120
We get more data per person.

13
00:00:36,120 --> 00:00:38,050
It is a really neat design.

14
00:00:38,050 --> 00:00:40,140
I want to show you how it works.

15
00:00:40,140 --> 00:00:42,290
In this design, I'm not so much interested in

16
00:00:42,290 --> 00:00:44,465
comparing group A versus group B,

17
00:00:44,465 --> 00:00:46,460
so much as tracking people as they

18
00:00:46,460 --> 00:00:49,160
change conditions to see how much they change.

19
00:00:49,160 --> 00:00:52,129
For instance, if you put me in an experiment

20
00:00:52,129 --> 00:00:55,370
in which you gave me a stress reduction intervention,

21
00:00:55,370 --> 00:00:57,285
and you saw how engaged I was at work,

22
00:00:57,285 --> 00:00:58,610
and then you took that away,

23
00:00:58,610 --> 00:01:01,490
and you put me in a highly stressful situation

24
00:01:01,490 --> 00:01:03,440
and my engagement decreased,

25
00:01:03,440 --> 00:01:04,700
you could be confident that

26
00:01:04,700 --> 00:01:06,060
it was due to those treatments.

27
00:01:06,060 --> 00:01:08,520
But you're not comparing me against another group,

28
00:01:08,520 --> 00:01:10,310
you're comparing me against me.

29
00:01:10,310 --> 00:01:13,520
You're tracking me over time to see how much I changed.

30
00:01:13,520 --> 00:01:15,890
So, I want to talk about the advantages of

31
00:01:15,890 --> 00:01:18,530
this design because it is a really neat design,

32
00:01:18,530 --> 00:01:20,375
but there are a few caveats we should think

33
00:01:20,375 --> 00:01:22,455
of before we try to implement it.

34
00:01:22,455 --> 00:01:25,870
If we use the diagram that we showed before, here,

35
00:01:25,870 --> 00:01:28,140
we've got our four participants and indeed,

36
00:01:28,140 --> 00:01:30,455
now they are in both groups.

37
00:01:30,455 --> 00:01:32,000
So, when you run this design,

38
00:01:32,000 --> 00:01:35,145
you put people in a group at random to start.

39
00:01:35,145 --> 00:01:36,330
You run the study.

40
00:01:36,330 --> 00:01:38,210
You collect their data,

41
00:01:38,210 --> 00:01:40,065
and instead of being done,

42
00:01:40,065 --> 00:01:41,630
you swap them over.

43
00:01:41,630 --> 00:01:44,270
The people who were in Group 1 go to Group 2,

44
00:01:44,270 --> 00:01:47,650
the people who were in Group 2, go to Group 1.

45
00:01:47,650 --> 00:01:50,060
So, for the start of this experiment,

46
00:01:50,060 --> 00:01:52,925
it's actually identical to the AB test.

47
00:01:52,925 --> 00:01:55,230
Everybody is in a random group to start.

48
00:01:55,230 --> 00:01:57,290
In theory, you could just end the study

49
00:01:57,290 --> 00:02:00,215
right there and have your between groups experiment.

50
00:02:00,215 --> 00:02:03,260
What we do differently in this design is,

51
00:02:03,260 --> 00:02:05,025
we just repeat the study.

52
00:02:05,025 --> 00:02:06,890
We swap you to the other condition.

53
00:02:06,890 --> 00:02:08,870
So now, instead of having one data point

54
00:02:08,870 --> 00:02:12,200
per participant, we have two.

55
00:02:12,210 --> 00:02:15,950
Let's imagine that we were doing some logo testing,

56
00:02:15,950 --> 00:02:20,105
and we were interested in showing you two different logos

57
00:02:20,105 --> 00:02:21,995
and assessing your opinions

58
00:02:21,995 --> 00:02:24,575
of either of them or in both of them, in fact.

59
00:02:24,575 --> 00:02:27,170
So, we're going to show you one logo at random to start.

60
00:02:27,170 --> 00:02:29,870
You're going to give us some feedback on that brand logo,

61
00:02:29,870 --> 00:02:32,330
maybe it's a rebranding or redesign,

62
00:02:32,330 --> 00:02:33,710
and then we're going to switch you to the

63
00:02:33,710 --> 00:02:35,185
other logo and repeat.

64
00:02:35,185 --> 00:02:37,370
So, let's talk about how this might work.

65
00:02:37,370 --> 00:02:38,930
What the data might look like,

66
00:02:38,930 --> 00:02:40,670
and then maybe some caveats or

67
00:02:40,670 --> 00:02:44,490
cautionary points that we might have about this design.

68
00:02:44,490 --> 00:02:48,295
This is a screenshot of what the data might look like.

69
00:02:48,295 --> 00:02:51,040
I have say, 10 different participants here.

70
00:02:51,040 --> 00:02:52,690
I've got 10 different rows,

71
00:02:52,690 --> 00:02:56,340
and I have their positivity ratings for the old logo.

72
00:02:56,340 --> 00:02:58,810
Maybe I've got a logo that's in current use,

73
00:02:58,810 --> 00:03:02,250
and then I've got positivity ratings of a new logo,

74
00:03:02,250 --> 00:03:05,500
the new logo that I want to test out.

75
00:03:05,500 --> 00:03:07,920
What we're going to do in this design,

76
00:03:07,920 --> 00:03:10,120
and this is done automatically for

77
00:03:10,120 --> 00:03:13,230
us in the data analysis we're going to do.

78
00:03:13,230 --> 00:03:14,710
But just to kind of illustrate it,

79
00:03:14,710 --> 00:03:17,650
I created a separate column to show the change.

80
00:03:17,650 --> 00:03:20,380
That change, again, would be handled for

81
00:03:20,380 --> 00:03:23,750
us by the regression analysis or by the Unova,

82
00:03:23,750 --> 00:03:26,030
so you don't have to calculate it manually.

83
00:03:26,030 --> 00:03:27,190
But you see here,

84
00:03:27,190 --> 00:03:30,215
the first participant, they actually decreased a point.

85
00:03:30,215 --> 00:03:31,810
They liked the old logo better by

86
00:03:31,810 --> 00:03:33,610
a point or to put it differently,

87
00:03:33,610 --> 00:03:36,515
they liked the new logo by one point less,

88
00:03:36,515 --> 00:03:38,420
so that's interesting to know.

89
00:03:38,420 --> 00:03:42,240
I'm comparing participant 1 against participant 1,

90
00:03:42,240 --> 00:03:45,340
and I'll immediately see an advantage here.

91
00:03:45,340 --> 00:03:47,075
Who is a better comparison for

92
00:03:47,075 --> 00:03:49,355
participant 1 than they themselves?

93
00:03:49,355 --> 00:03:51,100
Nothing about them is changed from

94
00:03:51,100 --> 00:03:53,845
the old logo to the new logo other than the logo.

95
00:03:53,845 --> 00:03:59,235
So, this is kind of a more intense, more precise design.

96
00:03:59,235 --> 00:04:00,880
I'm not relying on comparing

97
00:04:00,880 --> 00:04:03,240
two different groups against each other.

98
00:04:03,240 --> 00:04:07,160
I can immediately see eyeballing this change column,

99
00:04:07,160 --> 00:04:09,710
that in fact, people tend to have a negative shift.

100
00:04:09,710 --> 00:04:12,550
When I go from the old logo to the new logo,

101
00:04:12,550 --> 00:04:14,550
liking decreases, and that

102
00:04:14,550 --> 00:04:17,145
is not good news for the new logo.

103
00:04:17,145 --> 00:04:19,120
I will add, in general,

104
00:04:19,120 --> 00:04:20,230
people don't like change,

105
00:04:20,230 --> 00:04:22,510
so it's also possible this is an artifact of that,

106
00:04:22,510 --> 00:04:25,060
but that's subject matter expertise.

107
00:04:25,060 --> 00:04:26,980
Yes, people really don't like rebranding,

108
00:04:26,980 --> 00:04:29,425
especially loyal customers, tends to wear off though.

109
00:04:29,425 --> 00:04:31,510
However, in this case,

110
00:04:31,510 --> 00:04:32,900
I do want to point out that,

111
00:04:32,900 --> 00:04:34,180
in fact, people would have

112
00:04:34,180 --> 00:04:36,580
started with one logo at random.

113
00:04:36,580 --> 00:04:38,930
So, I don't have this information up there.

114
00:04:38,930 --> 00:04:40,120
But you should remember,

115
00:04:40,120 --> 00:04:41,920
I don't always start you on

116
00:04:41,920 --> 00:04:44,260
the old logo and swap you to the new one.

117
00:04:44,260 --> 00:04:45,400
I start some people on

118
00:04:45,400 --> 00:04:47,275
the new logo and swap to the old one.

119
00:04:47,275 --> 00:04:48,850
I don't have a column indicating

120
00:04:48,850 --> 00:04:50,470
which order you completed them in,

121
00:04:50,470 --> 00:04:53,550
but because I do that for the whole sample at random,

122
00:04:53,550 --> 00:04:56,180
they should even out in the end.

123
00:04:56,180 --> 00:05:00,655
I went ahead and I histogrammed those change scores.

124
00:05:00,655 --> 00:05:03,530
This is a larger sample,

125
00:05:03,530 --> 00:05:06,605
but here we can see kind of a similar example.

126
00:05:06,605 --> 00:05:09,820
If I were to collect more participants and plot the data,

127
00:05:09,820 --> 00:05:13,830
I see scores tend to be negative going from old to new.

128
00:05:13,830 --> 00:05:15,850
I've arranged all of them,

129
00:05:15,850 --> 00:05:17,330
so they're scored as old to new.

130
00:05:17,330 --> 00:05:18,840
So, even the people who started new and

131
00:05:18,840 --> 00:05:20,710
went to old, I've reversed them,

132
00:05:20,710 --> 00:05:24,935
so that I can get a consistent scoring for everybody.

133
00:05:24,935 --> 00:05:26,785
We see here on average,

134
00:05:26,785 --> 00:05:28,270
people are decreasing in

135
00:05:28,270 --> 00:05:31,930
their liking for the new one relative to the old one.

136
00:05:31,930 --> 00:05:34,220
This is important information.

137
00:05:34,220 --> 00:05:36,130
Important information for me to know people are

138
00:05:36,130 --> 00:05:38,360
not as happy about this new logo.

139
00:05:38,360 --> 00:05:41,260
Again, the interpretation of that is up to you.

140
00:05:41,260 --> 00:05:42,620
Maybe you think, "You know what?

141
00:05:42,620 --> 00:05:43,780
It's just a new logo,

142
00:05:43,780 --> 00:05:45,110
people will get used to it.

143
00:05:45,110 --> 00:05:47,440
But I'm still going to potentially take

144
00:05:47,440 --> 00:05:52,060
a hit when I deploy it and I should be prepared for that.

145
00:05:52,060 --> 00:05:55,295
Why is this randomized order important?

146
00:05:55,295 --> 00:05:57,045
I've mentioned a moment ago

147
00:05:57,045 --> 00:05:59,020
that we kind of want to even it out.

148
00:05:59,020 --> 00:06:00,180
We start half the people with

149
00:06:00,180 --> 00:06:01,680
the new logo switch to the old.

150
00:06:01,680 --> 00:06:02,760
So, have the people with the

151
00:06:02,760 --> 00:06:04,530
old logo and switch to the new.

152
00:06:04,530 --> 00:06:06,360
That is important because I don't want

153
00:06:06,360 --> 00:06:08,760
either logo to come first.

154
00:06:08,760 --> 00:06:12,810
Maybe people just like whichever logo they see first.

155
00:06:12,810 --> 00:06:14,670
Maybe there's a mental comparison

156
00:06:14,670 --> 00:06:16,020
effect that after I've seen one,

157
00:06:16,020 --> 00:06:18,550
I'm kind of comparing the other to it.

158
00:06:18,550 --> 00:06:23,590
So, it's important that over all duration of my study,

159
00:06:23,590 --> 00:06:25,810
that neither one systematically comes first.

160
00:06:25,810 --> 00:06:27,285
It allows me to have

161
00:06:27,285 --> 00:06:32,545
a more bias-free estimate of change. That is important.

162
00:06:32,545 --> 00:06:35,710
We could imagine easily that if we always showed

163
00:06:35,710 --> 00:06:38,400
the old logo first and then showed the new one,

164
00:06:38,400 --> 00:06:41,710
now, I've accidentally created a confound.

165
00:06:41,710 --> 00:06:43,750
The new logo is both new

166
00:06:43,750 --> 00:06:47,465
and directly being compared to the old one.

167
00:06:47,465 --> 00:06:49,595
Whereas, if I showed the old one first,

168
00:06:49,595 --> 00:06:51,540
you're not comparing it against the new one.

169
00:06:51,540 --> 00:06:53,710
That would bias my study results.

170
00:06:53,710 --> 00:06:57,510
So, we do want to show them in a random order.

171
00:06:57,510 --> 00:07:00,000
So, there are some benefits

172
00:07:00,000 --> 00:07:02,355
and some negatives about this design.

173
00:07:02,355 --> 00:07:07,665
It has a highly powerful design.

174
00:07:07,665 --> 00:07:09,710
It is great for power.

175
00:07:09,710 --> 00:07:12,990
So, you remember power is all

176
00:07:12,990 --> 00:07:15,600
about effect size relative

177
00:07:15,600 --> 00:07:17,330
to what you would expect by chance.

178
00:07:17,330 --> 00:07:19,565
The more effect I have,

179
00:07:19,565 --> 00:07:22,415
the less kind of noise in the data.

180
00:07:22,415 --> 00:07:23,830
My effect is kind of stand out.

181
00:07:23,830 --> 00:07:26,130
I'm going to get a significant result. This is great.

182
00:07:26,130 --> 00:07:28,110
Well, if I'm comparing you against

183
00:07:28,110 --> 00:07:30,615
you in different situations,

184
00:07:30,615 --> 00:07:32,415
there's not a whole lot of noise in that data.

185
00:07:32,415 --> 00:07:34,740
This is really clean, really pure data.

186
00:07:34,740 --> 00:07:37,905
I do not need big samples to test these questions.

187
00:07:37,905 --> 00:07:40,080
I can get away with much smaller samples.

188
00:07:40,080 --> 00:07:42,750
It makes them cost efficient in many situations

189
00:07:42,750 --> 00:07:45,545
even when I have to pay my participants.

190
00:07:45,545 --> 00:07:47,690
I'm paying them more, they are in my study longer,

191
00:07:47,690 --> 00:07:49,170
but I may be able to get away with

192
00:07:49,170 --> 00:07:51,810
a sample that is half or four times,

193
00:07:51,810 --> 00:07:56,150
or even 10 times smaller depending on the situation.

194
00:07:56,150 --> 00:07:58,995
We can look at some of that with our power analysis.

195
00:07:58,995 --> 00:08:01,185
There are a few drawbacks though.

196
00:08:01,185 --> 00:08:03,800
One is a demand characteristic.

197
00:08:03,800 --> 00:08:05,320
If I've shown you one logo,

198
00:08:05,320 --> 00:08:07,210
and then I immediately show you the new logo,

199
00:08:07,210 --> 00:08:09,505
you may guess what I'm trying to do.

200
00:08:09,505 --> 00:08:11,870
You might guess that I'm testing a new logo,

201
00:08:11,870 --> 00:08:14,200
and you might guess that I'm wanting you to like it,

202
00:08:14,200 --> 00:08:16,275
and that might bias you.

203
00:08:16,275 --> 00:08:18,740
So, I might want to consider doing some sleight of

204
00:08:18,740 --> 00:08:21,620
hand when I am designing my survey.

205
00:08:21,620 --> 00:08:23,840
There might be some practice defects,

206
00:08:23,840 --> 00:08:25,055
you've seen the questions.

207
00:08:25,055 --> 00:08:26,960
You're familiar with the measures that I'm giving you,

208
00:08:26,960 --> 00:08:28,780
and so you might answer differently.

209
00:08:28,780 --> 00:08:31,195
Of course, the solution to that,

210
00:08:31,195 --> 00:08:32,390
show them in a random order,

211
00:08:32,390 --> 00:08:34,900
so neither one systematically comes first.

212
00:08:34,900 --> 00:08:37,495
Lastly, we have that carryover effect

213
00:08:37,495 --> 00:08:38,760
that we talked about earlier,

214
00:08:38,760 --> 00:08:40,210
and we will repeat it again.

215
00:08:40,210 --> 00:08:42,680
Sometimes after being measured once,

216
00:08:42,680 --> 00:08:44,260
you're mentally biased and you

217
00:08:44,260 --> 00:08:47,200
cannot be validly measure it again.

218
00:08:47,200 --> 00:08:50,570
It's possible in this situation,

219
00:08:50,570 --> 00:08:52,530
that after you've seen one logo,

220
00:08:52,530 --> 00:08:54,310
that's your point of comparison.

221
00:08:54,310 --> 00:08:57,850
The next logo can't really be objectively viewed anymore.

222
00:08:57,850 --> 00:08:59,690
That might actually be the case,

223
00:08:59,690 --> 00:09:01,520
and I therefore might advocate not

224
00:09:01,520 --> 00:09:04,575
comparing to logos back to back like this,

225
00:09:04,575 --> 00:09:07,780
unless we have some sort of a wash out period.

226
00:09:07,780 --> 00:09:10,260
Now, there's lots of easy ways we could do that.

227
00:09:10,260 --> 00:09:11,830
For instance, maybe I put

228
00:09:11,830 --> 00:09:15,310
a few other logos for other things in between them.

229
00:09:15,310 --> 00:09:17,840
So, you're not mentally comparing them anymore.

230
00:09:17,840 --> 00:09:20,230
Now, I can objectively measure you the second

231
00:09:20,230 --> 00:09:22,660
time after measuring you the first time.

232
00:09:22,660 --> 00:09:26,145
That's a good strategy in this within design,

233
00:09:26,145 --> 00:09:28,135
add some kind of filler content.

234
00:09:28,135 --> 00:09:30,010
Still take advantage of the power,

235
00:09:30,010 --> 00:09:32,470
but now it's not directly obvious to people what you're

236
00:09:32,470 --> 00:09:35,520
doing and you can get more objective measures,

237
00:09:35,520 --> 00:09:37,725
a really neat research design,

238
00:09:37,725 --> 00:09:40,775
often cost efficient, often doable.

239
00:09:40,775 --> 00:09:42,340
It might take a little bit of creativity

240
00:09:42,340 --> 00:09:43,510
to do it validly,

241
00:09:43,510 --> 00:09:45,730
but it can save time and money and give

242
00:09:45,730 --> 00:09:49,640
much clearer answers than a between subjects design.

