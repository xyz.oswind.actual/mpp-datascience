0
00:00:00,000 --> 00:00:03,335
>> Well, we've reached the end.

1
00:00:03,335 --> 00:00:06,305
Thank you for participating in our online course.

2
00:00:06,305 --> 00:00:09,200
You have done a lot in this course and I think it's worth

3
00:00:09,200 --> 00:00:10,280
taking a moment just to

4
00:00:10,280 --> 00:00:12,220
reflect on what all we've learned.

5
00:00:12,220 --> 00:00:14,955
You've learned the entire research process,

6
00:00:14,955 --> 00:00:16,670
a decision making process,

7
00:00:16,670 --> 00:00:19,695
from start to finish, asking questions,

8
00:00:19,695 --> 00:00:23,705
collecting data, getting insights, and telling stories.

9
00:00:23,705 --> 00:00:26,180
It is a broad process and it is

10
00:00:26,180 --> 00:00:27,800
a process that has a lot of

11
00:00:27,800 --> 00:00:29,960
skills that are often left out.

12
00:00:29,960 --> 00:00:31,640
You're working on your way

13
00:00:31,640 --> 00:00:33,945
toward becoming a data scientist,

14
00:00:33,945 --> 00:00:37,365
but now you have the ability to put data in context,

15
00:00:37,365 --> 00:00:39,040
to tell stories with data,

16
00:00:39,040 --> 00:00:42,760
to spot good research and do good research,

17
00:00:42,760 --> 00:00:45,425
and spot bad research along the way.

18
00:00:45,425 --> 00:00:47,855
I wish you the best in your journey.

19
00:00:47,855 --> 00:00:49,680
I hope this course has been helpful,

20
00:00:49,680 --> 00:00:51,970
these lessons and these labs,

21
00:00:51,970 --> 00:00:54,770
have benefited you as you grow in your journey.

22
00:00:54,770 --> 00:00:57,110
I wish you all the best.

