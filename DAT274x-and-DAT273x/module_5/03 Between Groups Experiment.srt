0
00:00:01,070 --> 00:00:03,570
>> Now, we're going to switch gears and

1
00:00:03,570 --> 00:00:05,265
talk about the experiment.

2
00:00:05,265 --> 00:00:07,290
We talked about the experiment previously when

3
00:00:07,290 --> 00:00:09,390
we talked about causal claims.

4
00:00:09,390 --> 00:00:11,130
There are certain ways in which I can

5
00:00:11,130 --> 00:00:13,425
know one thing is causing another.

6
00:00:13,425 --> 00:00:16,170
As a researcher, I need to get in there and I need

7
00:00:16,170 --> 00:00:19,565
to intervene,and then I look at the effect.

8
00:00:19,565 --> 00:00:21,360
I know what's the cause and what's

9
00:00:21,360 --> 00:00:23,910
the effect because I'm the one who intervenes,

10
00:00:23,910 --> 00:00:25,740
so I am the one who did it.

11
00:00:25,740 --> 00:00:27,060
If you go back and review

12
00:00:27,060 --> 00:00:29,190
your notes from our earlier module,

13
00:00:29,190 --> 00:00:30,390
you'll see it satisfies

14
00:00:30,390 --> 00:00:34,555
our criteria for knowing causality, which is great.

15
00:00:34,555 --> 00:00:36,360
A few things I just want to review

16
00:00:36,360 --> 00:00:37,500
with you because we're going to get into

17
00:00:37,500 --> 00:00:40,455
a few more sophisticated experimental designs,

18
00:00:40,455 --> 00:00:43,130
this is sometimes known as an "A/B" test.

19
00:00:43,130 --> 00:00:45,030
It's something where, we've done

20
00:00:45,030 --> 00:00:47,070
this a few times now in our examples,

21
00:00:47,070 --> 00:00:49,345
but we randomly assigned people into

22
00:00:49,345 --> 00:00:51,840
two or more groups and we look at the effects.

23
00:00:51,840 --> 00:00:53,590
So, when I say, I'm getting

24
00:00:53,590 --> 00:00:55,725
in there and I'm manipulating something,

25
00:00:55,725 --> 00:00:57,480
what I'm doing is, I'm creating

26
00:00:57,480 --> 00:01:01,280
alternate realities in which I put people randomly.

27
00:01:01,280 --> 00:01:03,300
Some people see reality A,

28
00:01:03,300 --> 00:01:04,960
some people see reality B,

29
00:01:04,960 --> 00:01:07,485
and I look at the differences between them.

30
00:01:07,485 --> 00:01:09,565
Because I randomly put people into groups,

31
00:01:09,565 --> 00:01:11,940
I know that there's really no other difference between

32
00:01:11,940 --> 00:01:15,120
those groups other than the differences that I created,

33
00:01:15,120 --> 00:01:16,740
and that can allow me to be

34
00:01:16,740 --> 00:01:19,245
sure of my cause-effect conclusion.

35
00:01:19,245 --> 00:01:20,730
This is something that

36
00:01:20,730 --> 00:01:22,260
we've talked about several times now,

37
00:01:22,260 --> 00:01:23,880
but the statistical analysis would be

38
00:01:23,880 --> 00:01:25,650
to simply compare the averages.

39
00:01:25,650 --> 00:01:28,200
So, I might look at the average level of

40
00:01:28,200 --> 00:01:31,980
employee engagement in intervention group where I try to

41
00:01:31,980 --> 00:01:34,230
reduce your stress and the average level of

42
00:01:34,230 --> 00:01:36,595
employee engagement in a group where I have not done

43
00:01:36,595 --> 00:01:41,965
that and I see if improving stress improves engagement.

44
00:01:41,965 --> 00:01:43,740
So, again this is something we've discussed,

45
00:01:43,740 --> 00:01:45,030
but it bears repeating

46
00:01:45,030 --> 00:01:46,740
because these kinds of designs can get a

47
00:01:46,740 --> 00:01:48,300
little tricky especially when

48
00:01:48,300 --> 00:01:50,255
we make them more sophisticated.

49
00:01:50,255 --> 00:01:51,930
So, just to tie this back to

50
00:01:51,930 --> 00:01:53,700
some of the examples we used earlier,

51
00:01:53,700 --> 00:01:56,910
you can visualize the results from

52
00:01:56,910 --> 00:02:00,720
an "A/B" test or a between subjects experiment like this,

53
00:02:00,720 --> 00:02:01,985
where you're just plotting

54
00:02:01,985 --> 00:02:05,700
the average outcome level by the groups.

55
00:02:05,700 --> 00:02:07,500
So, in this example we are

56
00:02:07,500 --> 00:02:09,735
looking at the satisfaction with an online course.

57
00:02:09,735 --> 00:02:10,775
This was from an earlier

58
00:02:10,775 --> 00:02:12,690
module and we see that people who have

59
00:02:12,690 --> 00:02:14,655
no experience with the subject matter

60
00:02:14,655 --> 00:02:16,500
have a lower satisfaction score,

61
00:02:16,500 --> 00:02:19,040
that line on the left.

62
00:02:19,040 --> 00:02:21,600
They have a higher satisfaction level if

63
00:02:21,600 --> 00:02:25,030
they have some experience with the subject matter.

64
00:02:25,750 --> 00:02:29,255
I'm going to use diagrams like this to illustrate

65
00:02:29,255 --> 00:02:30,980
these experiments and I just want

66
00:02:30,980 --> 00:02:33,155
to repeat what I said before.

67
00:02:33,155 --> 00:02:35,280
I have a sample of people here.

68
00:02:35,280 --> 00:02:36,650
We see four people who might

69
00:02:36,650 --> 00:02:38,390
participate in my study and I

70
00:02:38,390 --> 00:02:41,760
distribute them randomly to the two groups.

71
00:02:41,760 --> 00:02:43,250
So, this is very, very,

72
00:02:43,250 --> 00:02:44,600
very important because this

73
00:02:44,600 --> 00:02:47,180
safeguards my cause-effect conclusion.

74
00:02:47,180 --> 00:02:48,840
If I see a difference,

75
00:02:48,840 --> 00:02:50,825
I know that my group membership

76
00:02:50,825 --> 00:02:52,820
co-varies with my outcome,

77
00:02:52,820 --> 00:02:54,685
one criteria for causation.

78
00:02:54,685 --> 00:02:56,985
If I see a difference in my groups,

79
00:02:56,985 --> 00:02:59,120
I know which is the cause and which is

80
00:02:59,120 --> 00:03:01,250
the effect because I'm the one who did it,

81
00:03:01,250 --> 00:03:04,505
temporal precedence criteria two for causation.

82
00:03:04,505 --> 00:03:06,360
I intervened first, I saw the effect

83
00:03:06,360 --> 00:03:09,080
later unless my participants can time travel,

84
00:03:09,080 --> 00:03:11,535
I'm fairly confident in what caused what.

85
00:03:11,535 --> 00:03:13,280
Then, there's no other

86
00:03:13,280 --> 00:03:15,460
competing explanation for the difference.

87
00:03:15,460 --> 00:03:18,650
Why? Because I put people into groups randomly so

88
00:03:18,650 --> 00:03:22,230
every feature of my participants is distributed evenly.

89
00:03:22,230 --> 00:03:24,260
I should have an equal proportion

90
00:03:24,260 --> 00:03:26,090
of males and females in my two groups.

91
00:03:26,090 --> 00:03:28,715
An equal number of older or younger individuals.

92
00:03:28,715 --> 00:03:30,170
People with different backgrounds.

93
00:03:30,170 --> 00:03:32,650
People with different opinions and personalities.

94
00:03:32,650 --> 00:03:34,320
Because I'm doing it randomly,

95
00:03:34,320 --> 00:03:37,235
they should be evenly distributed among the two groups.

96
00:03:37,235 --> 00:03:39,680
So, there's really, truly, honestly,

97
00:03:39,680 --> 00:03:42,535
no other good explanation for any differences I

98
00:03:42,535 --> 00:03:44,020
see as long as it's greater

99
00:03:44,020 --> 00:03:46,460
than chance, statistically significant.

100
00:03:46,460 --> 00:03:49,535
It is a beautiful design. It is simple.

101
00:03:49,535 --> 00:03:51,170
It is usually easy to do.

102
00:03:51,170 --> 00:03:53,660
I can expand this design and make it more flexible.

103
00:03:53,660 --> 00:03:55,400
I can add more groups.

104
00:03:55,400 --> 00:03:58,505
There is only really a couple of problems with it,

105
00:03:58,505 --> 00:04:01,040
it is weak in terms of power.

106
00:04:01,040 --> 00:04:02,240
I'm comparing different groups

107
00:04:02,240 --> 00:04:03,275
of people against each other.

108
00:04:03,275 --> 00:04:05,295
I'll show you a solution to this shortly.

109
00:04:05,295 --> 00:04:06,935
It's just a little bit inefficient.

110
00:04:06,935 --> 00:04:09,305
I only get one observation per person.

111
00:04:09,305 --> 00:04:11,880
Still, it's almost always easy to do,

112
00:04:11,880 --> 00:04:13,370
depending on what it is you're doing.

113
00:04:13,370 --> 00:04:15,530
If you're manipulating things on a website, for instance,

114
00:04:15,530 --> 00:04:17,090
you can get a large number of

115
00:04:17,090 --> 00:04:18,860
people easily compensate for

116
00:04:18,860 --> 00:04:20,840
that weak power and you can usually get

117
00:04:20,840 --> 00:04:24,530
insights pretty quickly with the between subjects design.

