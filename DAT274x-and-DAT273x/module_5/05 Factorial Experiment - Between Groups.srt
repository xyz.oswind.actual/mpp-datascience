0
00:00:00,000 --> 00:00:05,050
>> If you're going to take the time to do an experiment,

1
00:00:05,050 --> 00:00:07,000
why manipulate one thing?

2
00:00:07,000 --> 00:00:10,625
Why not A/B test on several dimensions at once?

3
00:00:10,625 --> 00:00:13,630
This is the factorial experiment.

4
00:00:13,630 --> 00:00:15,230
In a factorial experiment,

5
00:00:15,230 --> 00:00:18,025
you randomly assign people to two or more conditions

6
00:00:18,025 --> 00:00:20,910
on one dimension and then while you're at it,

7
00:00:20,910 --> 00:00:23,595
you go ahead and do it again on another dimension.

8
00:00:23,595 --> 00:00:26,020
As a result, we can actually get

9
00:00:26,020 --> 00:00:27,700
many different permutations or

10
00:00:27,700 --> 00:00:31,630
combinations and assess many interesting questions.

11
00:00:31,630 --> 00:00:34,630
This is something we just often don't think to do,

12
00:00:34,630 --> 00:00:36,474
but we could in fact envision

13
00:00:36,474 --> 00:00:38,785
testing a lot of really interesting things.

14
00:00:38,785 --> 00:00:41,770
For example, say I randomly assign you

15
00:00:41,770 --> 00:00:44,690
to Z one of two brand logos.

16
00:00:44,690 --> 00:00:47,230
So, I've got logo A and logo B.

17
00:00:47,230 --> 00:00:48,670
Maybe I'm doing this between

18
00:00:48,670 --> 00:00:50,320
subjects because I'm a little

19
00:00:50,320 --> 00:00:52,135
worried about carryover effects

20
00:00:52,135 --> 00:00:53,580
in or within subjects design,

21
00:00:53,580 --> 00:00:54,745
so I'm going to randomly assign you to see

22
00:00:54,745 --> 00:00:56,410
either one or the other.

23
00:00:56,410 --> 00:00:58,310
It's not as efficient of a design as

24
00:00:58,310 --> 00:01:00,700
a within subjects design but okay,

25
00:01:00,700 --> 00:01:03,130
I'm willing to do it because I'm a

26
00:01:03,130 --> 00:01:05,955
little bit more confident in the validity of my results.

27
00:01:05,955 --> 00:01:09,365
Thanks. Now while we're at it,

28
00:01:09,365 --> 00:01:12,220
I might also be interested in knowing whether people

29
00:01:12,220 --> 00:01:15,210
like a color logo or a grayscale logo.

30
00:01:15,210 --> 00:01:17,530
That might be something I'm interested in testing.

31
00:01:17,530 --> 00:01:20,410
Maybe my marketing contents will include

32
00:01:20,410 --> 00:01:21,905
color or grayscale versions

33
00:01:21,905 --> 00:01:23,610
and I might want to test both.

34
00:01:23,610 --> 00:01:25,060
Maybe I might also have

35
00:01:25,060 --> 00:01:27,250
some theory that one logo might do better in

36
00:01:27,250 --> 00:01:29,230
color and one logo might struggle

37
00:01:29,230 --> 00:01:31,330
with grayscale and if that's the case,

38
00:01:31,330 --> 00:01:35,000
I might want to test both of those at once.

39
00:01:35,000 --> 00:01:38,970
So, really, I can randomly assign people to

40
00:01:38,970 --> 00:01:42,540
a logo and randomly assign them to a color status,

41
00:01:42,540 --> 00:01:44,790
and that creates now not

42
00:01:44,790 --> 00:01:50,130
two but four combinations that we're assigning people to.

43
00:01:50,130 --> 00:01:53,500
This really will have some benefits and some drawbacks.

44
00:01:53,500 --> 00:01:55,290
Let's examine the design.

45
00:01:55,290 --> 00:01:57,700
So, in the design here,

46
00:01:57,700 --> 00:01:59,805
I have now four combinations.

47
00:01:59,805 --> 00:02:01,795
So, if I've got four participants,

48
00:02:01,795 --> 00:02:04,270
I've now only got one person per

49
00:02:04,270 --> 00:02:07,390
group and you can immediately see the drawback.

50
00:02:07,390 --> 00:02:10,285
I'm assigning people to groups and I've got now

51
00:02:10,285 --> 00:02:12,520
a growing number of groups and

52
00:02:12,520 --> 00:02:14,930
I've got relatively few people per group.

53
00:02:14,930 --> 00:02:17,505
If I want to do more on nuanced comparisons,

54
00:02:17,505 --> 00:02:20,530
for instance look at color just among logo A,

55
00:02:20,530 --> 00:02:21,850
I'm comparing one person

56
00:02:21,850 --> 00:02:23,805
against one person. That's not enough.

57
00:02:23,805 --> 00:02:26,500
You could end up slicing and dicing your sample to

58
00:02:26,500 --> 00:02:29,515
the point that it is too small to do a lot with.

59
00:02:29,515 --> 00:02:32,380
However, overall, I can do

60
00:02:32,380 --> 00:02:35,620
some pretty cool comparisons

61
00:02:35,620 --> 00:02:38,240
depending on how I analyze my data,

62
00:02:38,240 --> 00:02:39,805
if I'm using a regression framework

63
00:02:39,805 --> 00:02:41,640
or an analysis of variance,

64
00:02:41,640 --> 00:02:43,950
assuming that our outcome is normally distributed,

65
00:02:43,950 --> 00:02:46,085
but we'll talk about that in the labs.

66
00:02:46,085 --> 00:02:49,390
I have three questions that I can answer.

67
00:02:49,390 --> 00:02:51,560
So, a factorial design will pretty much always

68
00:02:51,560 --> 00:02:53,780
allow you to answer three questions.

69
00:02:53,780 --> 00:02:55,105
So, let's walk through them.

70
00:02:55,105 --> 00:03:00,600
First, I can look at whether logo A differs from logo B.

71
00:03:00,600 --> 00:03:02,960
When I do this, I basically just

72
00:03:02,960 --> 00:03:05,580
collapse the color in grayscale logos together.

73
00:03:05,580 --> 00:03:09,140
I've got now two groups of two that I am comparing.

74
00:03:09,140 --> 00:03:11,315
I can take the average of the top row

75
00:03:11,315 --> 00:03:14,330
and compare it against the average of the bottom row.

76
00:03:14,330 --> 00:03:16,760
Within the statistical analysis,

77
00:03:16,760 --> 00:03:18,770
this can be done for you automatically.

78
00:03:18,770 --> 00:03:19,980
There doesn't need to be

79
00:03:19,980 --> 00:03:22,835
a separate data analysis that you are going to do.

80
00:03:22,835 --> 00:03:25,080
Although depending on how you analyze your data,

81
00:03:25,080 --> 00:03:27,565
there might be different options for you.

82
00:03:27,565 --> 00:03:30,940
You might say, "Is it valid to combine them?"

83
00:03:30,940 --> 00:03:33,720
But actually in the real world, it probably is.

84
00:03:33,720 --> 00:03:37,705
In fact, it might be more valid than doing it otherwise.

85
00:03:37,705 --> 00:03:39,390
Think about it. If you were to

86
00:03:39,390 --> 00:03:41,090
deploy logo A in the real world,

87
00:03:41,090 --> 00:03:43,530
you might deploy both color and grayscale versions of it,

88
00:03:43,530 --> 00:03:45,270
and having both in your data

89
00:03:45,270 --> 00:03:47,070
is a little bit more realistic

90
00:03:47,070 --> 00:03:50,895
of a test of logo A than just having one or the other.

91
00:03:50,895 --> 00:03:53,130
If we hadn't done the factorial design.

92
00:03:53,130 --> 00:03:55,095
So, thanks factorial.

93
00:03:55,095 --> 00:03:58,870
Similarly logo B, I may have a grayscale and

94
00:03:58,870 --> 00:04:01,280
a color version of it and having both of those to

95
00:04:01,280 --> 00:04:04,085
play gives me a more realistic test of logo B.

96
00:04:04,085 --> 00:04:05,560
So, in many cases,

97
00:04:05,560 --> 00:04:07,380
factorial designs are more

98
00:04:07,380 --> 00:04:10,140
realistic and better even though they take

99
00:04:10,140 --> 00:04:12,595
a little bit more work to set up because you're

100
00:04:12,595 --> 00:04:16,580
testing more use cases in the real world.

101
00:04:16,740 --> 00:04:19,160
Now, of course, since I could

102
00:04:19,160 --> 00:04:20,660
compare the rows against each other,

103
00:04:20,660 --> 00:04:23,315
I could also compare the columns against each other.

104
00:04:23,315 --> 00:04:25,940
I could see if people have on average a preference

105
00:04:25,940 --> 00:04:29,150
for the color logo over the grayscale logo.

106
00:04:29,150 --> 00:04:30,860
This might not have even

107
00:04:30,860 --> 00:04:32,530
been the intention of your design,

108
00:04:32,530 --> 00:04:35,100
maybe you just wanted to field both versions.

109
00:04:35,100 --> 00:04:36,780
But in a factorial experiment,

110
00:04:36,780 --> 00:04:38,270
you have permission to look at

111
00:04:38,270 --> 00:04:40,625
this and therefore you probably should.

112
00:04:40,625 --> 00:04:43,550
It's actually a pretty neat thing you can do.

113
00:04:43,550 --> 00:04:46,590
You can actually test now across different logos,

114
00:04:46,590 --> 00:04:50,395
does colors seem to get a better response than grayscale?

115
00:04:50,395 --> 00:04:54,490
So, you now have another dimension

116
00:04:54,490 --> 00:04:56,445
on which you can compare your variables.

117
00:04:56,445 --> 00:04:59,100
In this case, I would just completely ignore logo.

118
00:04:59,100 --> 00:05:01,135
I would calculate a column average

119
00:05:01,135 --> 00:05:03,400
on the left and a column average on the right,

120
00:05:03,400 --> 00:05:05,470
and I would see if they significantly differ.

121
00:05:05,470 --> 00:05:07,300
Again, this would probably be

122
00:05:07,300 --> 00:05:09,540
done for you within your data analysis.

123
00:05:09,540 --> 00:05:10,600
You probably don't have to do

124
00:05:10,600 --> 00:05:12,310
anything separate to do this.

125
00:05:12,310 --> 00:05:14,230
Most analysis techniques for this kind of

126
00:05:14,230 --> 00:05:17,320
design will include this automatically.

127
00:05:17,320 --> 00:05:20,849
Finally, we can do the most interesting comparison

128
00:05:20,849 --> 00:05:22,690
and that is called the interaction.

129
00:05:22,690 --> 00:05:24,465
Interactions are often poorly

130
00:05:24,465 --> 00:05:26,399
misunderstood, but essentially,

131
00:05:26,399 --> 00:05:28,410
I'm looking at whether the effect of

132
00:05:28,410 --> 00:05:30,510
one variable depends or

133
00:05:30,510 --> 00:05:34,185
differs when broken out on the other variable.

134
00:05:34,185 --> 00:05:36,000
Let me say what I mean by that.

135
00:05:36,000 --> 00:05:39,390
For example, I could look at weather color versus

136
00:05:39,390 --> 00:05:44,290
grayscale matters differently for logo A than logo B.

137
00:05:44,290 --> 00:05:46,200
So, I'm looking at whether

138
00:05:46,200 --> 00:05:47,550
the pattern of one variable

139
00:05:47,550 --> 00:05:49,185
differs depending on the other.

140
00:05:49,185 --> 00:05:51,360
Maybe I think logo A is going

141
00:05:51,360 --> 00:05:53,400
to do well regardless color or grayscale,

142
00:05:53,400 --> 00:05:55,965
but logo B doesn't work well in grayscale.

143
00:05:55,965 --> 00:05:58,800
The interaction, if it's significant would support that.

144
00:05:58,800 --> 00:06:00,585
It would tell me

145
00:06:00,585 --> 00:06:03,720
that color matters differently

146
00:06:03,720 --> 00:06:05,690
for one logo than the other.

147
00:06:05,690 --> 00:06:07,290
Rather than just doing

148
00:06:07,290 --> 00:06:09,180
a couple different comparisons and saying,

149
00:06:09,180 --> 00:06:11,650
"Well, it looks bigger for a logo A than logo B."

150
00:06:11,650 --> 00:06:14,580
I can actually get a significant test for that.

151
00:06:14,580 --> 00:06:16,310
I can actually say,

152
00:06:16,310 --> 00:06:18,250
"P less than point 05."

153
00:06:18,250 --> 00:06:19,620
That the effect of color

154
00:06:19,620 --> 00:06:22,690
differs for one logo than the other.

155
00:06:22,690 --> 00:06:23,760
So, I actually have

156
00:06:23,760 --> 00:06:26,670
a statistical hypothesis test rather than just

157
00:06:26,670 --> 00:06:29,430
eyeballing one study comparing

158
00:06:29,430 --> 00:06:31,650
color for one logo versus another.

159
00:06:31,650 --> 00:06:35,080
It lets you test with more confidence that prediction,

160
00:06:35,080 --> 00:06:38,450
and I think that that is very valuable to have.

161
00:06:38,450 --> 00:06:41,650
So, here we go, three questions that you can

162
00:06:41,650 --> 00:06:45,065
answer in a factorial experiment.

163
00:06:45,065 --> 00:06:47,465
Does my first factor matter?

164
00:06:47,465 --> 00:06:49,865
Does logo A differ from logo B?

165
00:06:49,865 --> 00:06:51,600
Does my second factor matter?

166
00:06:51,600 --> 00:06:54,465
Does color differ from grayscale?

167
00:06:54,465 --> 00:06:56,120
Then, do they interact?

168
00:06:56,120 --> 00:06:57,490
Does the effect of one really

169
00:06:57,490 --> 00:06:59,040
differ depending on the other?

170
00:06:59,040 --> 00:07:02,679
Usually, the most interesting or nuanced prediction,

171
00:07:02,679 --> 00:07:04,810
but often one that we're interested in.

172
00:07:04,810 --> 00:07:06,740
Last but not least, I will say,

173
00:07:06,740 --> 00:07:08,250
why stop at two?

174
00:07:08,250 --> 00:07:11,515
Why not also look at print versus digital?

175
00:07:11,515 --> 00:07:15,850
Why not look at phone browser versus desktop?

176
00:07:15,850 --> 00:07:19,295
Why not manipulate many different things?

177
00:07:19,295 --> 00:07:21,700
You indeed can do that.

178
00:07:21,700 --> 00:07:23,185
Depending on what you're doing,

179
00:07:23,185 --> 00:07:26,890
you could get a very unwieldy and complicated design,

180
00:07:26,890 --> 00:07:28,330
that is fine.

181
00:07:28,330 --> 00:07:30,055
I will say however,

182
00:07:30,055 --> 00:07:32,290
that the more things you manipulate,

183
00:07:32,290 --> 00:07:34,690
the more comparisons you do.

184
00:07:34,690 --> 00:07:37,120
Remember, every comparison will have

185
00:07:37,120 --> 00:07:39,515
a five percent false positive rate,

186
00:07:39,515 --> 00:07:40,915
so do keep that in mind.

187
00:07:40,915 --> 00:07:44,890
If you're doing a very exponentially large design

188
00:07:44,890 --> 00:07:47,410
and you're running 20 different things,

189
00:07:47,410 --> 00:07:48,820
you might start to stumble across

190
00:07:48,820 --> 00:07:50,260
some false positives because

191
00:07:50,260 --> 00:07:51,910
remember when there's no effect,

192
00:07:51,910 --> 00:07:54,180
they still happen five percent of the time.

193
00:07:54,180 --> 00:07:56,020
So, just keep that in mind

194
00:07:56,020 --> 00:07:57,840
when you're doing your analysis.

195
00:07:57,840 --> 00:07:59,530
Again, I am a big fan of

196
00:07:59,530 --> 00:08:01,630
collecting more data than you need and

197
00:08:01,630 --> 00:08:03,520
setting aside half the data to see if

198
00:08:03,520 --> 00:08:05,595
it works on the first batch and the second batch.

199
00:08:05,595 --> 00:08:07,610
Cross validate your findings,

200
00:08:07,610 --> 00:08:10,105
at least if you've got enough data to do it validly.

201
00:08:10,105 --> 00:08:12,160
Don't do it if that would hurt your power.

202
00:08:12,160 --> 00:08:13,540
But, this is a

203
00:08:13,540 --> 00:08:16,135
fantastically helpful design that can answer

204
00:08:16,135 --> 00:08:19,755
a lot more useful and real-world relevant questions

205
00:08:19,755 --> 00:08:23,620
than a simple non-factorial experiment.

