0
00:00:01,160 --> 00:00:03,720
>> Welcome back. In this lesson,

1
00:00:03,720 --> 00:00:06,150
I want to explore what happens to

2
00:00:06,150 --> 00:00:09,495
our research conclusions when we have weak power.

3
00:00:09,495 --> 00:00:11,190
Now previously, I explained

4
00:00:11,190 --> 00:00:12,570
to you that if you have weak power,

5
00:00:12,570 --> 00:00:15,165
you have little ability to find things.

6
00:00:15,165 --> 00:00:18,615
But, in fact, there are some deeper more big problems

7
00:00:18,615 --> 00:00:20,160
that result from having weak power.

8
00:00:20,160 --> 00:00:21,690
I want to illustrate this to you,

9
00:00:21,690 --> 00:00:23,955
so you understand the importance of

10
00:00:23,955 --> 00:00:25,320
knowing what your power

11
00:00:25,320 --> 00:00:27,465
is when you make a research claim.

12
00:00:27,465 --> 00:00:30,195
I want you to imagine the following situation.

13
00:00:30,195 --> 00:00:31,995
We have two logos for our brand,

14
00:00:31,995 --> 00:00:33,550
an old one and a new one.

15
00:00:33,550 --> 00:00:35,670
We want to compare them and see if people

16
00:00:35,670 --> 00:00:38,010
have different impressions of the two of them.

17
00:00:38,010 --> 00:00:39,830
Are you to imagine we do that study,

18
00:00:39,830 --> 00:00:40,980
we do that survey,

19
00:00:40,980 --> 00:00:44,310
we do that test, and we find no significant difference.

20
00:00:44,310 --> 00:00:47,225
We cannot reject the null hypothesis.

21
00:00:47,225 --> 00:00:50,490
We can't say anything much more than that other

22
00:00:50,490 --> 00:00:51,840
than the difference was not

23
00:00:51,840 --> 00:00:53,840
greater than what you expect by chance.

24
00:00:53,840 --> 00:00:57,790
So, we conclude that the logos must be similar.

25
00:00:57,790 --> 00:01:00,025
That's one possible outcome.

26
00:01:00,025 --> 00:01:01,260
Another possible outcome is,

27
00:01:01,260 --> 00:01:03,030
we conclude there is a difference.

28
00:01:03,030 --> 00:01:04,540
In other case, I want you to

29
00:01:04,540 --> 00:01:06,415
know how to interpret those findings.

30
00:01:06,415 --> 00:01:08,915
Why should I actually interpret or

31
00:01:08,915 --> 00:01:11,605
what meaning should I draw from those findings.

32
00:01:11,605 --> 00:01:14,455
So obviously, if we say there's no difference,

33
00:01:14,455 --> 00:01:16,350
hopefully there's no difference, right?

34
00:01:16,350 --> 00:01:18,565
When we're saying that we were correct.

35
00:01:18,565 --> 00:01:20,035
We made a correct decision.

36
00:01:20,035 --> 00:01:22,310
We said, "Nope. Not significant.

37
00:01:22,310 --> 00:01:24,100
Must be the same." So, we

38
00:01:24,100 --> 00:01:26,040
would want that to be the right decision.

39
00:01:26,040 --> 00:01:28,240
Similarly, if we said there is a difference,

40
00:01:28,240 --> 00:01:30,135
we would want hopefully to be correct there.

41
00:01:30,135 --> 00:01:32,385
We would want there to be a real difference.

42
00:01:32,385 --> 00:01:35,080
But, in either situation,

43
00:01:35,080 --> 00:01:36,945
we could imagine we made a wrong decision.

44
00:01:36,945 --> 00:01:38,650
Sometimes we could say there is

45
00:01:38,650 --> 00:01:40,720
no difference when there is. We could miss it.

46
00:01:40,720 --> 00:01:42,490
In other situations, we might

47
00:01:42,490 --> 00:01:44,155
say there is a difference when there is not.

48
00:01:44,155 --> 00:01:46,580
We can have a false positive or a false alarm.

49
00:01:46,580 --> 00:01:48,445
I want to talk about these possibilities

50
00:01:48,445 --> 00:01:49,705
because these are very important

51
00:01:49,705 --> 00:01:51,115
when you're making sense of research

52
00:01:51,115 --> 00:01:52,965
and any data science claim is real.

53
00:01:52,965 --> 00:01:54,160
So, let's talk about the

54
00:01:54,160 --> 00:01:55,785
two kinds of errors you could make.

55
00:01:55,785 --> 00:01:58,960
The first we call a false positive or a type 1 error.

56
00:01:58,960 --> 00:02:00,890
Simply put, this is when you say,

57
00:02:00,890 --> 00:02:03,805
there is an "effect" or a relationship and there is not,

58
00:02:03,805 --> 00:02:05,895
you're wrong. These happen.

59
00:02:05,895 --> 00:02:08,470
In fact, they happened about five percent of the time.

60
00:02:08,470 --> 00:02:10,080
Why five percent of the time?

61
00:02:10,080 --> 00:02:12,835
Well, that's what the P-value says, right?

62
00:02:12,835 --> 00:02:15,600
P is 0.05 means if there's nothing there,

63
00:02:15,600 --> 00:02:17,440
I could still get this result five percent

64
00:02:17,440 --> 00:02:19,355
of the time. That's what it means.

65
00:02:19,355 --> 00:02:21,115
So, if I'm getting

66
00:02:21,115 --> 00:02:23,380
significant P-values, I'm saying, "You know what,

67
00:02:23,380 --> 00:02:24,630
this could only happen

68
00:02:24,630 --> 00:02:28,025
five percent of the time that I'm wrong.

69
00:02:28,025 --> 00:02:29,970
So, there's the rule, when you're

70
00:02:29,970 --> 00:02:32,260
wrong, there's nothing there,

71
00:02:32,260 --> 00:02:33,430
you're going to get

72
00:02:33,430 --> 00:02:36,510
a false positive five percent of those situations.

73
00:02:36,510 --> 00:02:38,295
Should keep that in mind.

74
00:02:38,295 --> 00:02:41,050
If you're wrong, there's still a five percent risk,

75
00:02:41,050 --> 00:02:43,370
it's going to come up significant anyway.

76
00:02:43,370 --> 00:02:45,100
Right? You see the problem.

77
00:02:45,100 --> 00:02:47,250
Significant doesn't mean true.

78
00:02:47,250 --> 00:02:50,170
So we've got to worry about these false positives.

79
00:02:50,170 --> 00:02:51,640
When a significant effect comes up,

80
00:02:51,640 --> 00:02:53,880
you should wonder, is this one of those five percent?

81
00:02:53,880 --> 00:02:55,750
That's a false positive, or is this real?

82
00:02:55,750 --> 00:02:57,520
Significant isn't proof.

83
00:02:57,520 --> 00:03:01,000
We can also have a false negative.

84
00:03:01,000 --> 00:03:02,500
We call this a type 2 error.

85
00:03:02,500 --> 00:03:03,880
This would be the situation

86
00:03:03,880 --> 00:03:06,020
where you don't find anything.

87
00:03:06,020 --> 00:03:08,515
You say, "Nope. There's no relationship here.

88
00:03:08,515 --> 00:03:09,880
It's not significant.

89
00:03:09,880 --> 00:03:14,115
My P-value is 0.07, not significant."

90
00:03:14,115 --> 00:03:16,550
Conclude there's no relationship. But you know what?

91
00:03:16,550 --> 00:03:19,205
You missed it. This sometimes is also known as a miss.

92
00:03:19,205 --> 00:03:22,405
This will happen about 20 percent of the time,

93
00:03:22,405 --> 00:03:24,305
if you've got 80 percent power.

94
00:03:24,305 --> 00:03:26,440
Reasoning here should make sense.

95
00:03:26,440 --> 00:03:27,850
Power is the percentage of the time

96
00:03:27,850 --> 00:03:29,415
you will find something.

97
00:03:29,415 --> 00:03:31,090
So, whatever's left is

98
00:03:31,090 --> 00:03:32,980
the amount of time you'll miss something.

99
00:03:32,980 --> 00:03:34,560
If you have 80 percent power,

100
00:03:34,560 --> 00:03:35,755
you'll miss the "effect"

101
00:03:35,755 --> 00:03:37,345
the other 20 percent of the time.

102
00:03:37,345 --> 00:03:39,165
If you've got 90 percent power,

103
00:03:39,165 --> 00:03:40,240
you'll miss the "effect"

104
00:03:40,240 --> 00:03:41,970
the other 10 percent of the time.

105
00:03:41,970 --> 00:03:43,780
So, whatever 100 percent

106
00:03:43,780 --> 00:03:45,485
minus your power is that'll be this.

107
00:03:45,485 --> 00:03:47,125
This is also a big deal.

108
00:03:47,125 --> 00:03:49,200
Again, you want to minimize this, right?

109
00:03:49,200 --> 00:03:50,760
If you're running a study,

110
00:03:50,760 --> 00:03:52,900
you want every possible chance of

111
00:03:52,900 --> 00:03:56,075
actually finding an "effect" assuming it's there.

112
00:03:56,075 --> 00:03:59,200
So, let's go ahead and look at

113
00:03:59,200 --> 00:04:01,030
some different situations that can

114
00:04:01,030 --> 00:04:04,110
happen with this combination of two things.

115
00:04:04,110 --> 00:04:05,980
I want to show you two universes.

116
00:04:05,980 --> 00:04:07,180
The first universe is

117
00:04:07,180 --> 00:04:09,480
the universe in which we want to live in,

118
00:04:09,480 --> 00:04:10,560
and I'll walk through this.

119
00:04:10,560 --> 00:04:12,280
Let's imagine you're a researcher

120
00:04:12,280 --> 00:04:14,280
and you're going to be doing 100 tests.

121
00:04:14,280 --> 00:04:16,590
By the way, this doesn't have to be a research situation.

122
00:04:16,590 --> 00:04:19,390
This could be any analytics or data science situation,

123
00:04:19,390 --> 00:04:20,910
and what you're doing hypothesis testing.

124
00:04:20,910 --> 00:04:22,595
Lets imagine you're doing a hundred of these.

125
00:04:22,595 --> 00:04:26,185
Okay. Let's stop for a second and think,

126
00:04:26,185 --> 00:04:27,820
what percentage of the time is

127
00:04:27,820 --> 00:04:29,410
there really some true effect.

128
00:04:29,410 --> 00:04:30,700
I'm going to go ahead and say,

129
00:04:30,700 --> 00:04:33,495
let's imagine that we were right,

130
00:04:33,495 --> 00:04:35,740
about 30 percent of the time.

131
00:04:35,740 --> 00:04:37,660
That might seem low,

132
00:04:37,660 --> 00:04:41,005
but in reality, it's actually probably pretty accurate.

133
00:04:41,005 --> 00:04:43,815
After all, if we were right most of the time,

134
00:04:43,815 --> 00:04:45,785
why are we busy running research, right?

135
00:04:45,785 --> 00:04:47,290
If I've got good insight and

136
00:04:47,290 --> 00:04:49,730
intuition about what's going on in the universe,

137
00:04:49,730 --> 00:04:52,445
I don't need to be running research.

138
00:04:52,445 --> 00:04:54,730
We are often running research studies to test

139
00:04:54,730 --> 00:04:56,145
things that we think might be

140
00:04:56,145 --> 00:04:57,830
true, but need verification.

141
00:04:57,830 --> 00:04:59,950
So, let's just assume that there is

142
00:04:59,950 --> 00:05:03,295
some effect there about 30 percent of the time.

143
00:05:03,295 --> 00:05:04,920
So, of these hundred tests,

144
00:05:04,920 --> 00:05:06,940
there is about 30 real effects

145
00:05:06,940 --> 00:05:10,280
and 70 times in which there is nothing.

146
00:05:10,280 --> 00:05:12,430
So, our job is to correctly

147
00:05:12,430 --> 00:05:14,575
label the 30 effect significant.

148
00:05:14,575 --> 00:05:16,695
The 70 non-effects,

149
00:05:16,695 --> 00:05:19,060
not significant, right? That's what we want to do.

150
00:05:19,060 --> 00:05:21,370
For those 30 effect's, I want to say, Yep, significant.

151
00:05:21,370 --> 00:05:23,100
For those 70 non-effects,

152
00:05:23,100 --> 00:05:24,670
I want to say, not significant.

153
00:05:24,670 --> 00:05:27,065
Right? I want to make the right choices.

154
00:05:27,065 --> 00:05:29,655
So, let's see how our tests do that.

155
00:05:29,655 --> 00:05:32,780
Okay. So, let's look at the 70 non-effects.

156
00:05:32,780 --> 00:05:35,595
Well, we get false positives.

157
00:05:35,595 --> 00:05:37,535
We set about five percent of the time.

158
00:05:37,535 --> 00:05:39,395
Five percent of the time when there's nothing there,

159
00:05:39,395 --> 00:05:40,610
we're going to get a false alarm.

160
00:05:40,610 --> 00:05:42,080
So, let's take five percent of those,

161
00:05:42,080 --> 00:05:44,820
that gives us about three and a half false positives.

162
00:05:44,820 --> 00:05:46,625
That's not too bad, right?

163
00:05:46,625 --> 00:05:48,885
Three and a half false positives out of 100 tests,

164
00:05:48,885 --> 00:05:50,230
sure, not a big deal.

165
00:05:50,230 --> 00:05:56,040
In fact, that also means about the other 65.5 times,

166
00:05:56,040 --> 00:05:57,550
we would make the correct decision.

167
00:05:57,550 --> 00:05:59,120
We would say, "Yeah.

168
00:05:59,120 --> 00:06:00,835
Nope. No effect there."

169
00:06:00,835 --> 00:06:02,030
So, we're making the correct decision

170
00:06:02,030 --> 00:06:04,210
95 percent of the time over here.

171
00:06:04,210 --> 00:06:05,650
That's good. This is good.

172
00:06:05,650 --> 00:06:08,225
I'm making good decisions 95 percent of the time.

173
00:06:08,225 --> 00:06:10,280
It's because I used that P is less than 0.05

174
00:06:10,280 --> 00:06:13,265
rule that keeps my False Positive rate at five percent.

175
00:06:13,265 --> 00:06:15,160
We say, thank you to our P-values for

176
00:06:15,160 --> 00:06:17,200
doing that work for us or our confidence intervals.

177
00:06:17,200 --> 00:06:22,350
Let's also go look at those 30 effects that are present.

178
00:06:22,350 --> 00:06:24,770
For those 30 effects that are present,

179
00:06:24,770 --> 00:06:27,860
I want to actually declare all of those significant.

180
00:06:27,860 --> 00:06:30,120
I don't have a 100 percent power though.

181
00:06:30,120 --> 00:06:32,375
Usually, I only have about 80 percent.

182
00:06:32,375 --> 00:06:34,280
I don't have enough sample size to

183
00:06:34,280 --> 00:06:36,045
always catch those 30 effects.

184
00:06:36,045 --> 00:06:38,395
So, if I've got about 80 percent power, lets say,

185
00:06:38,395 --> 00:06:42,930
I would find them about 24 of those 30. About 80 percent.

186
00:06:42,930 --> 00:06:45,220
So, for those 30 effects that are present,

187
00:06:45,220 --> 00:06:47,270
I'm going to get 24 true positives.

188
00:06:47,270 --> 00:06:50,240
Good decision, and I'm going to miss about six of them.

189
00:06:50,240 --> 00:06:52,380
So this is this research situation.

190
00:06:52,380 --> 00:06:54,310
This is a good world to live in.

191
00:06:54,310 --> 00:06:55,985
I'll just point a couple of things out.

192
00:06:55,985 --> 00:06:58,475
If you look at just the significant findings,

193
00:06:58,475 --> 00:07:01,025
I'm getting about three and a half False Positives.

194
00:07:01,025 --> 00:07:02,850
But 24 true positives

195
00:07:02,850 --> 00:07:07,210
right here and there, this is really good.

196
00:07:07,300 --> 00:07:10,550
Imagine for a moment that we look through

197
00:07:10,550 --> 00:07:12,620
all these hundred findings and we

198
00:07:12,620 --> 00:07:15,220
just filter out the nonsignificant ones,

199
00:07:15,220 --> 00:07:17,060
so we're just looking at these significant ones.

200
00:07:17,060 --> 00:07:21,525
The vast majority is significant results are true, right?

201
00:07:21,525 --> 00:07:23,270
or correct. I've got 24 of

202
00:07:23,270 --> 00:07:24,430
these significant effects and

203
00:07:24,430 --> 00:07:26,040
their real effects and I found them.

204
00:07:26,040 --> 00:07:28,480
I only got three and a half false positives.

205
00:07:28,480 --> 00:07:30,660
I will point out that three and a half though,

206
00:07:30,660 --> 00:07:33,710
it is greater than five percent, right?

207
00:07:33,710 --> 00:07:35,675
It's a decent number.

208
00:07:35,675 --> 00:07:37,600
It's not a huge proportion.

209
00:07:37,600 --> 00:07:38,660
So, if I'm looking at

210
00:07:38,660 --> 00:07:39,980
the significant findings in

211
00:07:39,980 --> 00:07:41,980
this universe that I'm showing on the screen here,

212
00:07:41,980 --> 00:07:44,395
I'm correct most of the time.

213
00:07:44,395 --> 00:07:48,490
But now I want to show you a different universe.

214
00:07:48,490 --> 00:07:50,310
The universe I'm about to show you,

215
00:07:50,310 --> 00:07:52,290
is unfortunately perhaps a

216
00:07:52,290 --> 00:07:54,610
little bit more commonplace than we would want to think.

217
00:07:54,610 --> 00:07:56,400
So, I'm only going to change two things,

218
00:07:56,400 --> 00:07:59,550
but it's going to dramatically change the outcome.

219
00:07:59,550 --> 00:08:01,860
So first off, same situation,

220
00:08:01,860 --> 00:08:05,125
researcher or data scientist runs 100 tests,

221
00:08:05,125 --> 00:08:07,540
100 models, 100 analyses.

222
00:08:07,540 --> 00:08:10,170
But this data scientist and

223
00:08:10,170 --> 00:08:12,000
researcher is engaging in

224
00:08:12,000 --> 00:08:13,905
a questionable research practice.

225
00:08:13,905 --> 00:08:16,410
They are not thinking through

226
00:08:16,410 --> 00:08:19,110
the steps I talked about in module one of this course.

227
00:08:19,110 --> 00:08:20,790
They are not developing a theory.

228
00:08:20,790 --> 00:08:22,665
They're not testing a hypothesis.

229
00:08:22,665 --> 00:08:23,905
They haven't thought about it.

230
00:08:23,905 --> 00:08:26,680
They're just going to test everything.

231
00:08:26,680 --> 00:08:29,080
So let's imagine now,

232
00:08:29,080 --> 00:08:30,985
if you test every possible thing,

233
00:08:30,985 --> 00:08:32,610
you're going to be wrong most of the time.

234
00:08:32,610 --> 00:08:34,455
Let's imagine you get 10 percent

235
00:08:34,455 --> 00:08:36,905
of the time there's really something there.

236
00:08:36,905 --> 00:08:38,880
In this universe, I've only got

237
00:08:38,880 --> 00:08:41,765
10 effects present and 90 effects absent.

238
00:08:41,765 --> 00:08:43,590
This is the result of going fishing for

239
00:08:43,590 --> 00:08:45,345
findings of not doing

240
00:08:45,345 --> 00:08:47,070
the research work that I told you we should

241
00:08:47,070 --> 00:08:49,745
do early in that research process.

242
00:08:49,745 --> 00:08:53,355
This is going to set us up for failure in a big way.

243
00:08:53,355 --> 00:08:56,560
Because now we've got more non-effects.

244
00:08:56,560 --> 00:08:58,365
So, if I take five percent of those,

245
00:08:58,365 --> 00:09:00,165
I now have four and a half false positives.

246
00:09:00,165 --> 00:09:02,310
That's no good. I've actually increased

247
00:09:02,310 --> 00:09:06,560
my false positives just because I'm wrong more often.

248
00:09:06,640 --> 00:09:08,905
of those 10 effects present,

249
00:09:08,905 --> 00:09:11,420
let's imagine now I didn't do a power analysis either.

250
00:09:11,420 --> 00:09:12,430
Well, you really have not been

251
00:09:12,430 --> 00:09:13,840
paying attention in my course.

252
00:09:13,840 --> 00:09:17,115
So now, we've got only 30 percent power.

253
00:09:17,115 --> 00:09:18,610
You say, that's really low.

254
00:09:18,610 --> 00:09:21,195
That's true, but it's actually not that unrealistic.

255
00:09:21,195 --> 00:09:23,890
In a number of statistical analyses,

256
00:09:23,890 --> 00:09:26,054
even a lot of published research,

257
00:09:26,054 --> 00:09:27,850
there is low power because people have not

258
00:09:27,850 --> 00:09:29,810
paid good attention to power over the years.

259
00:09:29,810 --> 00:09:31,270
So, I only have

260
00:09:31,270 --> 00:09:34,210
30 percent ability to find those 10 effects.

261
00:09:34,210 --> 00:09:36,150
Meaning, I'm going to catch three of them.

262
00:09:36,150 --> 00:09:39,290
The other seven, sorry I missed them. I have low power.

263
00:09:39,290 --> 00:09:42,140
Should I use a bigger sample size.

264
00:09:42,140 --> 00:09:44,710
Now immediately, we see the problem.

265
00:09:44,710 --> 00:09:48,025
There are more false significant findings than true ones.

266
00:09:48,025 --> 00:09:49,990
Look at just the Significant findings I've got

267
00:09:49,990 --> 00:09:53,955
about three true ones and four and a half false ones.

268
00:09:53,955 --> 00:09:56,690
Imagine the problem here.

269
00:09:56,690 --> 00:09:58,170
You're running 100 tests.

270
00:09:58,170 --> 00:10:00,140
You're wanting to make business decisions.

271
00:10:00,140 --> 00:10:01,910
You look at the significant results.

272
00:10:01,910 --> 00:10:04,040
You find about seven of them,

273
00:10:04,040 --> 00:10:07,320
and over half of them are wrong.

274
00:10:07,320 --> 00:10:10,150
Wow. Over half of them are wrong.

275
00:10:10,150 --> 00:10:11,900
You'd be better off not doing

276
00:10:11,900 --> 00:10:14,105
research in this case, right?

277
00:10:14,105 --> 00:10:15,310
You can flip a coin and make

278
00:10:15,310 --> 00:10:18,575
a good decision with better accuracy.

279
00:10:18,575 --> 00:10:20,580
So, what went wrong here.

280
00:10:20,580 --> 00:10:21,825
Well, two things.

281
00:10:21,825 --> 00:10:23,360
The first thing that went wrong,

282
00:10:23,360 --> 00:10:24,585
we're fishing for findings.

283
00:10:24,585 --> 00:10:26,350
We're testing everything possible.

284
00:10:26,350 --> 00:10:28,965
When you do that, you're just wrong more often.

285
00:10:28,965 --> 00:10:30,854
So, you're going to be false positive

286
00:10:30,854 --> 00:10:32,640
more than true positive.

287
00:10:32,640 --> 00:10:36,695
The second thing that went wrong, you had low power.

288
00:10:36,695 --> 00:10:38,410
So when there were true effects

289
00:10:38,410 --> 00:10:40,425
even though they were few and far between,

290
00:10:40,425 --> 00:10:41,985
you couldn't even find those.

291
00:10:41,985 --> 00:10:46,465
So, we've now resulted in very few true positives.

292
00:10:46,465 --> 00:10:48,935
To draw valid conclusions,

293
00:10:48,935 --> 00:10:52,375
you need to consider and optimize your power.

294
00:10:52,375 --> 00:10:53,860
Power is very important.

295
00:10:53,860 --> 00:10:56,400
If you don't have power, you can't have true positives.

296
00:10:56,400 --> 00:10:59,020
But false positives will still happen and you'll get

297
00:10:59,020 --> 00:11:00,190
in a world where things are

298
00:11:00,190 --> 00:11:01,780
wrong more often than they're right.

299
00:11:01,780 --> 00:11:03,040
You don't want that.

300
00:11:03,040 --> 00:11:04,930
Statistical significance will not

301
00:11:04,930 --> 00:11:07,000
save you in that situation.

302
00:11:07,000 --> 00:11:11,230
You should also be skeptical of very unlikely effects.

303
00:11:11,230 --> 00:11:13,130
If I told you, "You know what,

304
00:11:13,130 --> 00:11:14,915
the sun exploded this morning.

305
00:11:14,915 --> 00:11:16,100
My study told me so."

306
00:11:16,100 --> 00:11:18,775
PS lesson 0.5. You should say,

307
00:11:18,775 --> 00:11:20,420
"Yeah, but I don't think it did."

308
00:11:20,420 --> 00:11:21,920
It's okay to be skeptical.

309
00:11:21,920 --> 00:11:23,610
False Positives do happen and you're

310
00:11:23,610 --> 00:11:27,120
allowed to think critically about those findings.

311
00:11:27,120 --> 00:11:29,670
You should also beware of

312
00:11:29,670 --> 00:11:32,385
filtering out the nonsignificant findings.

313
00:11:32,385 --> 00:11:34,650
On the previous screen we

314
00:11:34,650 --> 00:11:36,060
saw that there were

315
00:11:36,060 --> 00:11:38,345
more False Significant findings than True ones,

316
00:11:38,345 --> 00:11:39,660
but there were also a lot of

317
00:11:39,660 --> 00:11:41,520
nonsignificant findings that could

318
00:11:41,520 --> 00:11:42,945
have drowned those out.

319
00:11:42,945 --> 00:11:44,750
Often we ignore those though.

320
00:11:44,750 --> 00:11:45,820
When we can't reject them,

321
00:11:45,820 --> 00:11:47,400
no we just throw the result away.

322
00:11:47,400 --> 00:11:50,190
Don't do that, report all the results.

323
00:11:50,190 --> 00:11:52,380
If you don't want to report all the results in

324
00:11:52,380 --> 00:11:54,150
a PowerPoint slide or a report,

325
00:11:54,150 --> 00:11:57,090
that's fine, but have them in some supplement.

326
00:11:57,090 --> 00:12:00,045
People should know all the nonsignificant things

327
00:12:00,045 --> 00:12:02,120
as much as what is significant.

328
00:12:02,120 --> 00:12:04,635
The last thing I'll say is just

329
00:12:04,635 --> 00:12:07,980
treat any statistical finding with a grain of salt.

330
00:12:07,980 --> 00:12:10,005
Just take it a little bit skeptically.

331
00:12:10,005 --> 00:12:11,830
If something seems a little uncertain to you,

332
00:12:11,830 --> 00:12:13,470
the P-value is not super

333
00:12:13,470 --> 00:12:15,530
convincing and seems a little odd,

334
00:12:15,530 --> 00:12:17,840
there's nothing wrong with replicating it,

335
00:12:17,840 --> 00:12:19,615
especially if data collection is cheap.

336
00:12:19,615 --> 00:12:22,890
If you don't have access to the ability to replicate it,

337
00:12:22,890 --> 00:12:24,875
set half the data aside to start,

338
00:12:24,875 --> 00:12:26,550
run the analysis on the first half,

339
00:12:26,550 --> 00:12:29,120
see if it replicates in the second half.

340
00:12:29,120 --> 00:12:32,830
After all, you avoid false significant findings that way.

341
00:12:32,830 --> 00:12:34,710
If it didn't replicate, okay,

342
00:12:34,710 --> 00:12:36,150
must have been a False Positive.

343
00:12:36,150 --> 00:12:39,110
These are some tools you can use to safeguard yourself.

344
00:12:39,110 --> 00:12:41,000
There are also some tools you can use to

345
00:12:41,000 --> 00:12:43,335
avoid being misled by bad research.

346
00:12:43,335 --> 00:12:45,070
You will see bad research both in

347
00:12:45,070 --> 00:12:47,120
your organizations and unfortunately,

348
00:12:47,120 --> 00:12:50,795
you will see bad research in the public and on the news.

349
00:12:50,795 --> 00:12:55,260
So, now you have some tools to sniff-out bad research.

350
00:12:55,260 --> 00:12:57,880
You want power, and you want

351
00:12:57,880 --> 00:13:00,325
good rationale for your findings.

352
00:13:00,325 --> 00:13:01,750
If you don't have those two things,

353
00:13:01,750 --> 00:13:04,030
you deserve to be skeptical.

