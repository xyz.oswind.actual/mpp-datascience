0
00:00:01,280 --> 00:00:03,860
>> Welcome back. In this lesson,

1
00:00:03,860 --> 00:00:06,950
I want to talk about how we reject or discredit

2
00:00:06,950 --> 00:00:08,750
the null hypothesis so that we can

3
00:00:08,750 --> 00:00:10,990
trust the findings that come out of our samples.

4
00:00:10,990 --> 00:00:12,290
So, I want to briefly

5
00:00:12,290 --> 00:00:13,800
review what the null hypothesis says,

6
00:00:13,800 --> 00:00:15,185
and then we can talk about

7
00:00:15,185 --> 00:00:17,410
how we reject it or discredit it.

8
00:00:17,410 --> 00:00:19,025
So, first off, let's review.

9
00:00:19,025 --> 00:00:21,110
The null hypothesis says the effect is

10
00:00:21,110 --> 00:00:24,085
absent in whatever the population you're studying.

11
00:00:24,085 --> 00:00:26,240
So, the result that you find in your sample,

12
00:00:26,240 --> 00:00:27,650
that was just due to random chance,

13
00:00:27,650 --> 00:00:29,020
that was just a fluke.

14
00:00:29,020 --> 00:00:30,800
We really want to be able to discredit

15
00:00:30,800 --> 00:00:32,370
or reject this null hypothesis.

16
00:00:32,370 --> 00:00:33,590
This null hypothesis is very

17
00:00:33,590 --> 00:00:35,955
skeptical of anything you find in your sample,

18
00:00:35,955 --> 00:00:37,785
and before we can trust our findings,

19
00:00:37,785 --> 00:00:39,140
we need to make sure we can actually

20
00:00:39,140 --> 00:00:41,290
reject this possibility.

21
00:00:41,850 --> 00:00:44,715
We might also want to know if we can say more.

22
00:00:44,715 --> 00:00:46,400
Rejecting the idea that the results are due

23
00:00:46,400 --> 00:00:48,275
to chance, sounds good.

24
00:00:48,275 --> 00:00:49,490
We might also consider whether

25
00:00:49,490 --> 00:00:50,915
there's anything else we can say.

26
00:00:50,915 --> 00:00:52,370
The results are not due to chance?

27
00:00:52,370 --> 00:00:53,450
That's great. Is there anything

28
00:00:53,450 --> 00:00:54,780
else my study can tell me?

29
00:00:54,780 --> 00:00:56,810
We're going to be able to do part of that,

30
00:00:56,810 --> 00:00:59,090
the first part of that was something called a p-value,

31
00:00:59,090 --> 00:01:01,770
and we're going to talk about that in this lesson.

32
00:01:01,770 --> 00:01:04,840
So, before we can understand what a p-value

33
00:01:04,840 --> 00:01:07,195
is because they tend to be very misunderstood.

34
00:01:07,195 --> 00:01:10,540
We have to understand some basics of probability.

35
00:01:10,540 --> 00:01:13,525
This is what's called a conditional probability.

36
00:01:13,525 --> 00:01:14,710
We don't need to talk about

37
00:01:14,710 --> 00:01:15,940
a lot of the math behind this,

38
00:01:15,940 --> 00:01:18,625
but we do need to understand this one basic concept.

39
00:01:18,625 --> 00:01:20,770
This is basically asking what is

40
00:01:20,770 --> 00:01:23,025
the probability that A is true,

41
00:01:23,025 --> 00:01:24,700
given that I know that B is true?

42
00:01:24,700 --> 00:01:26,530
So, for instance, maybe I

43
00:01:26,530 --> 00:01:28,695
want to know what is the probability

44
00:01:28,695 --> 00:01:31,340
that somebody is driving

45
00:01:31,340 --> 00:01:34,040
fast given that I know that their car is red.

46
00:01:34,040 --> 00:01:35,880
Now, it's not necessarily the case

47
00:01:35,880 --> 00:01:37,970
that someone who drives a red car drives faster.

48
00:01:37,970 --> 00:01:41,030
But in general, if you told me that a car is red,

49
00:01:41,030 --> 00:01:42,560
I might make a different guess

50
00:01:42,560 --> 00:01:44,030
about the speed of the car,

51
00:01:44,030 --> 00:01:45,575
a different probability estimate

52
00:01:45,575 --> 00:01:46,865
for the speed of the car.

53
00:01:46,865 --> 00:01:50,300
If you told me that the car is a go-cart for instance,

54
00:01:50,300 --> 00:01:53,870
if the second half that says car equals go-cart,

55
00:01:53,870 --> 00:01:55,760
I'm going to guess that the probability of

56
00:01:55,760 --> 00:01:57,650
it being a fast car is very low.

57
00:01:57,650 --> 00:02:02,495
But if you told me the car is a high-speed racer,

58
00:02:02,495 --> 00:02:04,160
well, in that case, I'm going to estimate

59
00:02:04,160 --> 00:02:06,940
a high probability that speed is fast.

60
00:02:06,940 --> 00:02:09,110
So, it's a statement of probability.

61
00:02:09,110 --> 00:02:11,530
What is the probability that I'm driving a fast car?

62
00:02:11,530 --> 00:02:14,690
But it's informed by some piece of information.

63
00:02:14,690 --> 00:02:16,195
I know that B is true.

64
00:02:16,195 --> 00:02:17,805
You've given me that fact,

65
00:02:17,805 --> 00:02:19,725
and now that I know that B is true,

66
00:02:19,725 --> 00:02:21,605
I'm going to maybe give a different estimate

67
00:02:21,605 --> 00:02:25,385
for the probability of A, if that makes sense.

68
00:02:25,385 --> 00:02:28,290
So, let's apply this now.

69
00:02:28,430 --> 00:02:31,065
This is what a p-value is.

70
00:02:31,065 --> 00:02:32,285
So, let's talk through this.

71
00:02:32,285 --> 00:02:35,390
A p-value says what is the probability of getting

72
00:02:35,390 --> 00:02:37,720
some result in your sample

73
00:02:37,720 --> 00:02:40,605
if the null hypothesis was actually true?

74
00:02:40,605 --> 00:02:44,560
So, if you told me that the results were due to chance,

75
00:02:44,560 --> 00:02:47,440
how often could I get this result anyway?

76
00:02:47,440 --> 00:02:49,550
What is the probability of getting a result this

77
00:02:49,550 --> 00:02:52,690
big if it was really due to chance?

78
00:02:52,690 --> 00:02:54,735
This is a way of testing the null hypothesis.

79
00:02:54,735 --> 00:02:56,880
I'm kind of playing devil's advocate for a moment.

80
00:02:56,880 --> 00:02:58,970
Okay. The results are due to chance.

81
00:02:58,970 --> 00:03:02,450
Great. How often could I get this result anyway?

82
00:03:02,450 --> 00:03:04,765
So, if I've got a large p-value,

83
00:03:04,765 --> 00:03:06,740
that's saying if the results were due to chance,

84
00:03:06,740 --> 00:03:09,185
I could still get this data often.

85
00:03:09,185 --> 00:03:11,985
If your p-value is 50 percent,

86
00:03:11,985 --> 00:03:14,665
that would tell me I could get a result this big

87
00:03:14,665 --> 00:03:18,335
with the null hypothesis being true half of the time.

88
00:03:18,335 --> 00:03:21,085
Wait, you're telling me I could get a result this

89
00:03:21,085 --> 00:03:23,970
big half the time just by chance?

90
00:03:23,970 --> 00:03:27,165
I don't think so. I'm not going to trust that result.

91
00:03:27,165 --> 00:03:31,215
What if the p-value is one percent or half of a percent?

92
00:03:31,215 --> 00:03:34,530
Well, that tells me if the results were due to chance,

93
00:03:34,530 --> 00:03:35,620
I could only get in effect

94
00:03:35,620 --> 00:03:39,035
this big one percent or half of a percent of the time,

95
00:03:39,035 --> 00:03:41,580
maybe the null hypothesis isn't so true.

96
00:03:41,580 --> 00:03:44,710
You see, this is sort of a roundabout way of testing

97
00:03:44,710 --> 00:03:46,530
the null hypothesis because you can't actually

98
00:03:46,530 --> 00:03:48,555
test whether the null hypothesis is true.

99
00:03:48,555 --> 00:03:50,115
I kind of want to point that out here.

100
00:03:50,115 --> 00:03:52,530
We have no idea whether the null is true or not.

101
00:03:52,530 --> 00:03:55,785
But I can say if it were true, could I get this result?

102
00:03:55,785 --> 00:03:57,310
That's what we're asking here.

103
00:03:57,310 --> 00:03:59,885
So, here we're saying if the p-value is large,

104
00:03:59,885 --> 00:04:02,055
the result could easily be produced by chance.

105
00:04:02,055 --> 00:04:05,915
It would be a highly probable result if the null is true.

106
00:04:05,915 --> 00:04:07,830
If it's small, we're seeing the result

107
00:04:07,830 --> 00:04:09,660
is not easily produced by chance.

108
00:04:09,660 --> 00:04:11,339
It would be a very improbable

109
00:04:11,339 --> 00:04:13,435
result if the null were true.

110
00:04:13,435 --> 00:04:15,240
I would love to have

111
00:04:15,240 --> 00:04:17,310
a more direct way of testing the null hypothesis.

112
00:04:17,310 --> 00:04:19,440
I would love to just be able to calculate;

113
00:04:19,440 --> 00:04:21,090
what are the odds this is due to chance?

114
00:04:21,090 --> 00:04:22,545
We don't have a way of doing that.

115
00:04:22,545 --> 00:04:24,180
Or rather, we don't have a way of doing that

116
00:04:24,180 --> 00:04:26,100
with traditional classical statistics.

117
00:04:26,100 --> 00:04:28,675
So, this is kind of our best option.

118
00:04:28,675 --> 00:04:30,330
As long as we understand this,

119
00:04:30,330 --> 00:04:31,995
we can avoid misusing it.

120
00:04:31,995 --> 00:04:33,960
We'll talk about some of the implications of

121
00:04:33,960 --> 00:04:35,710
that later on in this course.

122
00:04:35,710 --> 00:04:37,080
If you don't understand

123
00:04:37,080 --> 00:04:38,860
this or you misinterpret a p-value,

124
00:04:38,860 --> 00:04:40,930
you can make some very bad business decisions.

125
00:04:40,930 --> 00:04:42,235
We'll talk about that.

126
00:04:42,235 --> 00:04:45,200
So, in a sense then,

127
00:04:45,200 --> 00:04:47,415
what we're really just doing is we're just asking how

128
00:04:47,415 --> 00:04:49,740
easily is the result produced by chance.

129
00:04:49,740 --> 00:04:52,260
What are the odds I could get this result by chance?

130
00:04:52,260 --> 00:04:55,755
How often could I get this result if the null is true?

131
00:04:55,755 --> 00:04:57,510
If that's very rare, I'm going to

132
00:04:57,510 --> 00:05:00,185
call it statistically significant.

133
00:05:00,185 --> 00:05:04,065
You may have heard this phrasing used before,

134
00:05:04,065 --> 00:05:06,235
"This is a statistically significant

135
00:05:06,235 --> 00:05:07,440
result", "My goodness!

136
00:05:07,440 --> 00:05:09,630
We found a statistically significant difference

137
00:05:09,630 --> 00:05:11,185
between this group and that group."

138
00:05:11,185 --> 00:05:13,830
But statistically significant, I just want to

139
00:05:13,830 --> 00:05:16,290
point this out just means a low p-value.

140
00:05:16,290 --> 00:05:18,105
It doesn't mean there's a real difference.

141
00:05:18,105 --> 00:05:20,370
We haven't disproved chance at all even.

142
00:05:20,370 --> 00:05:23,005
All significant means is you

143
00:05:23,005 --> 00:05:26,180
probably couldn't get this result by chance.

144
00:05:26,180 --> 00:05:29,130
That's not a very big statement to make.

145
00:05:29,130 --> 00:05:31,395
If I were to tell my spouse in the morning,

146
00:05:31,395 --> 00:05:34,255
"Hey honey, I love you greater than chance."

147
00:05:34,255 --> 00:05:37,895
She's going to say, [inaudible] or

148
00:05:37,895 --> 00:05:39,905
she might say something more than that.

149
00:05:39,905 --> 00:05:41,250
But regardless, it's not

150
00:05:41,250 --> 00:05:43,105
a very big informative thing to say.

151
00:05:43,105 --> 00:05:45,750
That's not a very bold claim to make.

152
00:05:45,750 --> 00:05:48,080
We should be able to reject the null hypothesis.

153
00:05:48,080 --> 00:05:51,820
We should be able to declare a finding significant.

154
00:05:51,820 --> 00:05:54,300
We should be able to say the results are probably

155
00:05:54,300 --> 00:05:57,020
not produced by chance very well.

156
00:05:57,020 --> 00:05:58,910
But it's not a lot to say.

157
00:05:58,910 --> 00:06:00,330
It's kind of a bare minimum.

158
00:06:00,330 --> 00:06:03,090
So, in research, our rule is

159
00:06:03,090 --> 00:06:06,850
usually that we will declare the results significant,

160
00:06:06,850 --> 00:06:10,020
we will reject the null hypothesis

161
00:06:10,020 --> 00:06:13,065
if p is less than 0.05.

162
00:06:13,065 --> 00:06:16,635
So, if five percent of the time

163
00:06:16,635 --> 00:06:17,735
I could get this result by

164
00:06:17,735 --> 00:06:20,215
chance, that's kind of my threshold.

165
00:06:20,215 --> 00:06:22,010
I won't really accept bigger than that.

166
00:06:22,010 --> 00:06:24,035
I'll accept less than that though.

167
00:06:24,035 --> 00:06:26,110
I will call it significant and I

168
00:06:26,110 --> 00:06:27,540
will reject the null hypothesis,

169
00:06:27,540 --> 00:06:30,370
but only if it's a five percent or less.

170
00:06:30,370 --> 00:06:31,735
So, let's see this in action.

171
00:06:31,735 --> 00:06:34,460
So here's our data we looked at earlier.

172
00:06:34,460 --> 00:06:36,140
So here, I've got my course

173
00:06:36,140 --> 00:06:37,750
satisfaction from people who have

174
00:06:37,750 --> 00:06:40,060
some experience on the right and people

175
00:06:40,060 --> 00:06:41,200
who don't have experience with

176
00:06:41,200 --> 00:06:43,075
my subject matter on the left.

177
00:06:43,075 --> 00:06:45,130
I had this difference and I said, "Wait,

178
00:06:45,130 --> 00:06:47,330
how do I know this difference isn't due to chance?"

179
00:06:47,330 --> 00:06:49,900
Well, I can calculate a p-value on this data,

180
00:06:49,900 --> 00:06:52,760
and here I see my p-value as 0.00001,

181
00:06:53,630 --> 00:06:56,875
which is much less than one percent.

182
00:06:56,875 --> 00:06:59,650
So as a decimal, you move the decimal over two spaces.

183
00:06:59,650 --> 00:07:01,060
It's still significantly less

184
00:07:01,060 --> 00:07:03,545
or considerably less than one percent.

185
00:07:03,545 --> 00:07:05,780
So what I'm saying with this p-value,

186
00:07:05,780 --> 00:07:08,700
I'm not saying I know for sure. This isn't a coincidence.

187
00:07:08,700 --> 00:07:11,790
I'm not saying I know the null hypothesis is wrong but

188
00:07:11,790 --> 00:07:13,170
I'm saying the null hypothesis

189
00:07:13,170 --> 00:07:15,070
could only produce this result,

190
00:07:15,070 --> 00:07:20,370
0.00001 proportion of the time

191
00:07:20,370 --> 00:07:23,190
or 0.001 percent of the time.

192
00:07:23,190 --> 00:07:27,215
I can only get an effect this big by chance that often.

193
00:07:27,215 --> 00:07:29,310
Since that's less than 0.05,

194
00:07:29,310 --> 00:07:31,050
I'm going to reject the null hypothesis.

195
00:07:31,050 --> 00:07:32,280
I'm going to say this result is

196
00:07:32,280 --> 00:07:33,660
not easily produced by chance.

197
00:07:33,660 --> 00:07:35,460
I can go on about my day.

198
00:07:35,460 --> 00:07:36,840
I can say, "Okay, you know what?

199
00:07:36,840 --> 00:07:38,520
I reject the null hypothesis.

200
00:07:38,520 --> 00:07:41,125
This is probably not due to chance." That's good.

201
00:07:41,125 --> 00:07:42,965
That's good that I can say that.

202
00:07:42,965 --> 00:07:45,380
Not the most informative thing I can say,

203
00:07:45,380 --> 00:07:46,630
but if we can't do this,

204
00:07:46,630 --> 00:07:49,340
we really can't do much of anything with these data.

205
00:07:49,340 --> 00:07:51,645
So, in conclusion, we can

206
00:07:51,645 --> 00:07:52,950
declare that chance is

207
00:07:52,950 --> 00:07:54,640
a poor explanation for the difference.

208
00:07:54,640 --> 00:07:57,220
We can reject the null hypothesis.

209
00:07:57,220 --> 00:07:59,390
You do that when your p-value is small.

210
00:07:59,390 --> 00:08:00,945
Because you're saying, I could

211
00:08:00,945 --> 00:08:02,670
only get a result this big when

212
00:08:02,670 --> 00:08:07,030
the null hypothesis is true, very, very rarely.

