0
00:00:01,280 --> 00:00:04,340
>> Welcome back. In this lesson,

1
00:00:04,340 --> 00:00:07,015
I want to talk a little bit about Effect Size.

2
00:00:07,015 --> 00:00:09,270
Now, when we're dealing with power,

3
00:00:09,270 --> 00:00:10,420
I already said in

4
00:00:10,420 --> 00:00:13,150
the previous lesson that we need to take and pay

5
00:00:13,150 --> 00:00:14,620
attention to the size of

6
00:00:14,620 --> 00:00:15,970
our sample and the size of

7
00:00:15,970 --> 00:00:17,350
the effect that we want to find.

8
00:00:17,350 --> 00:00:20,725
So, we need to know what I mean when I say effect size.

9
00:00:20,725 --> 00:00:22,870
What on earth am I talking about?

10
00:00:22,870 --> 00:00:24,205
There's two measures for

11
00:00:24,205 --> 00:00:26,140
effect size and we're going to be using these

12
00:00:26,140 --> 00:00:29,735
in our statistical analysis from our data.

13
00:00:29,735 --> 00:00:31,540
So, I want to talk through what those are

14
00:00:31,540 --> 00:00:33,745
and give you some benchmarks, so you understand them.

15
00:00:33,745 --> 00:00:35,950
The first is called Cohen's d.

16
00:00:35,950 --> 00:00:38,350
D by the way stands for difference.

17
00:00:38,350 --> 00:00:41,400
If I were comparing two groups, for example,

18
00:00:41,400 --> 00:00:43,580
like the online course evaluations

19
00:00:43,580 --> 00:00:46,225
that I was talking about in the previous lesson,

20
00:00:46,225 --> 00:00:49,620
I can quantify how big some differences in raw units.

21
00:00:49,620 --> 00:00:51,060
So, in our previous examples,

22
00:00:51,060 --> 00:00:53,970
we had said the group with some experience was

23
00:00:53,970 --> 00:00:55,560
about eight-tenths of a point more

24
00:00:55,560 --> 00:00:58,200
satisfied than the people with no experience.

25
00:00:58,200 --> 00:01:01,440
Often, that metric doesn't make a lot of sense to us,

26
00:01:01,440 --> 00:01:03,255
eight-tenths of a point. How big is that?

27
00:01:03,255 --> 00:01:05,420
Is that a big difference? Is that a small difference?

28
00:01:05,420 --> 00:01:08,220
I don't really know what the units of measure are here.

29
00:01:08,220 --> 00:01:10,670
This becomes a big problem when we're doing

30
00:01:10,670 --> 00:01:12,470
surveys in which you're having

31
00:01:12,470 --> 00:01:15,095
people rate something on a 1-10 scale.

32
00:01:15,095 --> 00:01:17,750
How likely would you be to refer us to

33
00:01:17,750 --> 00:01:20,315
a friend on a 1-10 scale?

34
00:01:20,315 --> 00:01:21,650
How big is a point? I don't

35
00:01:21,650 --> 00:01:23,765
really know how big that point is.

36
00:01:23,765 --> 00:01:27,810
So, if a unit of measure is meaningful, that's great,

37
00:01:27,810 --> 00:01:29,150
but we really would like to have kind of

38
00:01:29,150 --> 00:01:32,150
a standard rating scale for how big something is.

39
00:01:32,150 --> 00:01:33,470
That's really what we want here.

40
00:01:33,470 --> 00:01:35,205
Is just some sort of unit free measurement,

41
00:01:35,205 --> 00:01:36,880
and that's what Cohen's d gives you.

42
00:01:36,880 --> 00:01:39,400
Essentially, I won't get into the math here,

43
00:01:39,400 --> 00:01:40,820
but it is the size of

44
00:01:40,820 --> 00:01:42,350
the difference in standard deviations.

45
00:01:42,350 --> 00:01:46,130
So, we're keeping the same difference from our data,

46
00:01:46,130 --> 00:01:47,690
but we're just converting the unit to

47
00:01:47,690 --> 00:01:48,830
standard deviations where

48
00:01:48,830 --> 00:01:51,725
one standard deviation is one unit.

49
00:01:51,725 --> 00:01:53,900
You don't need to worry too much about

50
00:01:53,900 --> 00:01:55,370
the mathematical implications of this,

51
00:01:55,370 --> 00:01:56,750
but it does give us a kind of

52
00:01:56,750 --> 00:01:58,940
a standard rating scale for pretty

53
00:01:58,940 --> 00:02:01,950
much all studies looking at differences between groups.

54
00:02:01,950 --> 00:02:04,460
So, we have some cutoffs that were suggested

55
00:02:04,460 --> 00:02:06,775
to us by statistician Jay Cohen.

56
00:02:06,775 --> 00:02:09,600
He suggested Cohen's d from about

57
00:02:09,600 --> 00:02:12,220
0.2 will be considered so small,

58
00:02:12,220 --> 00:02:14,985
it's trivial, not superbly meaningful.

59
00:02:14,985 --> 00:02:17,730
Cohen's d of about 0.2 to 0.5 would be small,

60
00:02:17,730 --> 00:02:19,770
0.5 to 0.8 would be considered medium,

61
00:02:19,770 --> 00:02:21,885
and 0.8 and up is large.

62
00:02:21,885 --> 00:02:25,940
So, we're going to use Cohen's d as a way of quantifying

63
00:02:25,940 --> 00:02:27,410
how big our effects are when

64
00:02:27,410 --> 00:02:30,355
the units of measure aren't that meaningful.

65
00:02:30,355 --> 00:02:32,570
However, these might not

66
00:02:32,570 --> 00:02:34,580
be meaningful to you at the moment.

67
00:02:34,580 --> 00:02:35,720
So, I want to walk through

68
00:02:35,720 --> 00:02:38,115
a few examples that will benchmark them.

69
00:02:38,115 --> 00:02:40,605
Before I do that, I also just want to point out

70
00:02:40,605 --> 00:02:42,960
an alternative measure might be correlation.

71
00:02:42,960 --> 00:02:44,880
You are probably already familiar with

72
00:02:44,880 --> 00:02:47,130
the correlation coefficient, the r value.

73
00:02:47,130 --> 00:02:48,915
There are some cutoffs for those as well.

74
00:02:48,915 --> 00:02:50,880
Feel free to pause this video and

75
00:02:50,880 --> 00:02:53,175
write these down to have some notes on them.

76
00:02:53,175 --> 00:02:56,115
You can actually even go back and forth between them.

77
00:02:56,115 --> 00:02:57,710
Not that you really need to do this,

78
00:02:57,710 --> 00:02:59,610
but if you wanted to convert between r and

79
00:02:59,610 --> 00:03:02,130
d or vice versa, you can do that.

80
00:03:02,130 --> 00:03:04,289
They're basically giving you the same information

81
00:03:04,289 --> 00:03:06,780
where a lot of us have used correlations before.

82
00:03:06,780 --> 00:03:08,100
Correlation of zero means there is

83
00:03:08,100 --> 00:03:09,760
no relationship between things.

84
00:03:09,760 --> 00:03:11,100
Correlation of one means there is

85
00:03:11,100 --> 00:03:12,870
a perfect one to one relationship.

86
00:03:12,870 --> 00:03:14,520
So, it's just an alternative way

87
00:03:14,520 --> 00:03:16,050
of measuring how big an effect is.

88
00:03:16,050 --> 00:03:18,000
I'm not really going to focus on correlations right

89
00:03:18,000 --> 00:03:19,830
now because those are a little bit more intuitive,

90
00:03:19,830 --> 00:03:21,420
but I would like to give you some examples for

91
00:03:21,420 --> 00:03:23,225
those Cohen's d values.

92
00:03:23,225 --> 00:03:26,480
So, let's take a look at some examples.

93
00:03:26,480 --> 00:03:28,860
Height in inches. If we were to compare

94
00:03:28,860 --> 00:03:31,220
males and females on their height in inches,

95
00:03:31,220 --> 00:03:35,970
the effect size, Cohen's d comes out to be about 1.85.

96
00:03:35,970 --> 00:03:37,640
This would be a very large effect.

97
00:03:37,640 --> 00:03:39,420
Cohen said anything larger

98
00:03:39,420 --> 00:03:42,825
than Cohen's d about 0.8 is considered large.

99
00:03:42,825 --> 00:03:44,390
It's a very large effect and in fact,

100
00:03:44,390 --> 00:03:46,785
you probably can intuitively know that males

101
00:03:46,785 --> 00:03:49,240
biologically tend to be a little bit taller than females.

102
00:03:49,240 --> 00:03:50,390
That's not a surprise.

103
00:03:50,390 --> 00:03:51,690
It's not always true,

104
00:03:51,690 --> 00:03:53,060
but we know that it tends to be true.

105
00:03:53,060 --> 00:03:54,720
You can see it intuitively

106
00:03:54,720 --> 00:03:57,280
without doing any data analysis.

107
00:03:57,280 --> 00:03:59,160
Because this effect is so

108
00:03:59,160 --> 00:04:01,830
large as we said in our power analysis,

109
00:04:01,830 --> 00:04:03,995
you don't really need a big sample

110
00:04:03,995 --> 00:04:05,710
because you can have large effects

111
00:04:05,710 --> 00:04:09,410
with small samples or you can have small effects,

112
00:04:09,410 --> 00:04:10,440
but you would need large samples.

113
00:04:10,440 --> 00:04:11,880
Remember, you have to have one of those to

114
00:04:11,880 --> 00:04:13,855
be large to have good power.

115
00:04:13,855 --> 00:04:15,230
Since this effect is large,

116
00:04:15,230 --> 00:04:17,070
we don't actually need a very big sample.

117
00:04:17,070 --> 00:04:18,450
If you're studying something as

118
00:04:18,450 --> 00:04:20,260
big and pronounced as this,

119
00:04:20,260 --> 00:04:21,270
you can get away with

120
00:04:21,270 --> 00:04:25,320
about six people per group or 12 participants total.

121
00:04:25,320 --> 00:04:28,530
You could do this in a focus group even fairly easily.

122
00:04:28,530 --> 00:04:30,330
So, you might think to yourself that's too

123
00:04:30,330 --> 00:04:32,700
small of a sample to be trustworthy, but it's not.

124
00:04:32,700 --> 00:04:34,110
If you have an effect this big,

125
00:04:34,110 --> 00:04:36,150
you don't need a large sample and no need to spend

126
00:04:36,150 --> 00:04:38,635
the time or money researching that.

127
00:04:38,635 --> 00:04:40,690
That is actually why we like power analysis.

128
00:04:40,690 --> 00:04:42,300
You will find the optimal number of

129
00:04:42,300 --> 00:04:44,700
participants to answer your question

130
00:04:44,700 --> 00:04:46,770
validly without wasting resources.

131
00:04:46,770 --> 00:04:49,150
Let's try another one.

132
00:04:49,150 --> 00:04:51,720
This is also, this is by the way, this is not my data.

133
00:04:51,720 --> 00:04:54,090
This is data from some classic power research

134
00:04:54,090 --> 00:04:55,820
done in the field of psychology.

135
00:04:55,820 --> 00:04:57,420
So, if you ask people, they surveyed

136
00:04:57,420 --> 00:04:59,840
people about the number of shoe pairs owned.

137
00:04:59,840 --> 00:05:01,150
They compared males and females.

138
00:05:01,150 --> 00:05:03,380
They found that this effect size was a little smaller.

139
00:05:03,380 --> 00:05:05,360
Cohen's d about 1.07,

140
00:05:05,360 --> 00:05:06,940
so just another benchmark for you.

141
00:05:06,940 --> 00:05:08,410
Again, 0.8 is large,

142
00:05:08,410 --> 00:05:10,465
so this is still considered a large effect,

143
00:05:10,465 --> 00:05:12,325
but it's not quite as large.

144
00:05:12,325 --> 00:05:14,420
So, because it's a little smaller,

145
00:05:14,420 --> 00:05:16,850
we need to compensate with a slightly larger sample.

146
00:05:16,850 --> 00:05:18,160
So, what do we find?

147
00:05:18,160 --> 00:05:21,200
To get adequate power here for something about this size,

148
00:05:21,200 --> 00:05:24,495
you need about 15 participants per group or 30 people.

149
00:05:24,495 --> 00:05:26,780
So, it's still a reasonably small sample.

150
00:05:26,780 --> 00:05:27,805
You might think, "Wow.

151
00:05:27,805 --> 00:05:30,345
I didn't know I could get away with samples that small."

152
00:05:30,345 --> 00:05:31,970
Well, when you're studying large effects,

153
00:05:31,970 --> 00:05:33,870
you absolutely can and don't

154
00:05:33,870 --> 00:05:36,600
need to spend the resources on a larger sample.

155
00:05:36,600 --> 00:05:38,580
I hope these slides are useful

156
00:05:38,580 --> 00:05:39,975
for you because you can actually

157
00:05:39,975 --> 00:05:42,985
write these down and try to train your intuition,

158
00:05:42,985 --> 00:05:44,610
so you can identify when a sample size is

159
00:05:44,610 --> 00:05:46,550
adequate without running a power analysis.

160
00:05:46,550 --> 00:05:48,525
However, in the labs for this lesson,

161
00:05:48,525 --> 00:05:49,620
you'll get a chance to play with

162
00:05:49,620 --> 00:05:51,665
power analysis tools as well.

163
00:05:51,665 --> 00:05:53,820
What if we wanted a smaller effect?

164
00:05:53,820 --> 00:05:55,570
So, if you take on average the weight in

165
00:05:55,570 --> 00:05:57,480
pounds for males versus females,

166
00:05:57,480 --> 00:05:59,090
you see a much smaller difference.

167
00:05:59,090 --> 00:06:01,220
Hereoure Cohen's d is point 0.59.

168
00:06:01,220 --> 00:06:03,035
So, this would be in the moderate range.

169
00:06:03,035 --> 00:06:06,820
Remember, 0.5 to 0.8 is considered moderate or medium.

170
00:06:06,820 --> 00:06:08,720
Because this is an even smaller effect,

171
00:06:08,720 --> 00:06:10,160
I need an even bigger sample

172
00:06:10,160 --> 00:06:11,700
to be able to have good power.

173
00:06:11,700 --> 00:06:14,390
So, in this case, you would need about 46 participants

174
00:06:14,390 --> 00:06:16,495
per group or 92 people.

175
00:06:16,495 --> 00:06:18,020
Now, you could immediately

176
00:06:18,020 --> 00:06:19,890
imagine if you had a sample smaller than that,

177
00:06:19,890 --> 00:06:22,340
you might be in trouble because you need

178
00:06:22,340 --> 00:06:25,605
that much of a sample to be able to have good power.

179
00:06:25,605 --> 00:06:28,520
So, if I'm wanting to study a question that's this

180
00:06:28,520 --> 00:06:31,050
nuanced and I don't have enough participants,

181
00:06:31,050 --> 00:06:32,615
I'm going to be setting myself up to fail

182
00:06:32,615 --> 00:06:34,220
and that's not good because you always

183
00:06:34,220 --> 00:06:35,540
need a big enough sample to

184
00:06:35,540 --> 00:06:37,935
match the effect size that you're studying.

185
00:06:37,935 --> 00:06:40,400
What about we go something really small like

186
00:06:40,400 --> 00:06:43,265
the effect of dying

187
00:06:43,265 --> 00:06:46,625
from a smoking related illness on smoking status?

188
00:06:46,625 --> 00:06:48,680
This is a finding out a health research and we find

189
00:06:48,680 --> 00:06:50,670
the effect size is about 0.33.

190
00:06:50,670 --> 00:06:51,960
So, this is considered small.

191
00:06:51,960 --> 00:06:53,180
Remember, just because it's

192
00:06:53,180 --> 00:06:54,890
small doesn't mean it's unimportant.

193
00:06:54,890 --> 00:06:58,175
Health issues often are the result of many small effects.

194
00:06:58,175 --> 00:07:00,400
Each of those are very important,

195
00:07:00,400 --> 00:07:02,685
but the effect is small.

196
00:07:02,685 --> 00:07:05,015
Because this effect is smaller

197
00:07:05,015 --> 00:07:06,610
and I hope we're getting the trend right now,

198
00:07:06,610 --> 00:07:08,515
I need a big enough sample to compensate for that.

199
00:07:08,515 --> 00:07:12,740
So, now, I need 144 people per group or 288 people.

200
00:07:12,740 --> 00:07:15,340
Now, we begin to see it can be very easy to

201
00:07:15,340 --> 00:07:16,680
have low power here if I

202
00:07:16,680 --> 00:07:18,350
don't recruit a big enough sample.

203
00:07:18,350 --> 00:07:20,410
The other thing I want to point out is as

204
00:07:20,410 --> 00:07:22,810
the sample size are getting smaller and smaller,

205
00:07:22,810 --> 00:07:25,390
I don't just need a consistently larger sample.

206
00:07:25,390 --> 00:07:27,445
They need to get exponentially larger.

207
00:07:27,445 --> 00:07:31,495
The smaller the effect size,

208
00:07:31,495 --> 00:07:33,370
the bigger the sample size that's needed

209
00:07:33,370 --> 00:07:35,470
and that gets exponentially bigger.

210
00:07:35,470 --> 00:07:36,970
So, if we go to something very

211
00:07:36,970 --> 00:07:39,820
small like this is one of my favorite examples,

212
00:07:39,820 --> 00:07:41,240
you asked people the number of planets

213
00:07:41,240 --> 00:07:42,640
that they can correctly name,

214
00:07:42,640 --> 00:07:43,930
you compare people who say they

215
00:07:43,930 --> 00:07:46,395
prefer art, prefer science.

216
00:07:46,395 --> 00:07:49,010
This effect size is very, very small.

217
00:07:49,010 --> 00:07:51,130
We estimated at about 0.07.

218
00:07:51,130 --> 00:07:53,120
This would be in that trivial range,

219
00:07:53,120 --> 00:07:56,195
so small. It's almost unimportant.

220
00:07:56,195 --> 00:07:59,060
We see here we need an exponentially larger sample

221
00:07:59,060 --> 00:08:01,245
to detect and effect this small. There is an effect.

222
00:08:01,245 --> 00:08:02,765
There is a true difference.

223
00:08:02,765 --> 00:08:07,610
We would need about 7,338 participants to find it.

224
00:08:07,610 --> 00:08:11,910
So, what is the take home message?

225
00:08:11,910 --> 00:08:15,390
Small effects, large samples.

226
00:08:15,390 --> 00:08:17,200
You need one of those two to be large.

227
00:08:17,200 --> 00:08:19,530
If you don't have that, you're going to be in trouble.

228
00:08:19,530 --> 00:08:22,130
I hope these examples are helpful to help you benchmark,

229
00:08:22,130 --> 00:08:23,920
effect sizes and the

230
00:08:23,920 --> 00:08:26,075
sample sizes you would need to be able to find them.

231
00:08:26,075 --> 00:08:27,700
In the next video, though,

232
00:08:27,700 --> 00:08:29,770
we're going to talk about some more technical ways

233
00:08:29,770 --> 00:08:32,750
to identify an adequate sample size.

