0
00:00:00,580 --> 00:00:03,450
>> Welcome back. In this lesson,

1
00:00:03,450 --> 00:00:05,880
I want to talk about something that tends to

2
00:00:05,880 --> 00:00:06,990
confuse people who are

3
00:00:06,990 --> 00:00:09,050
learning Data Science and Statistics.

4
00:00:09,050 --> 00:00:10,790
It's called the Null Hypothesis.

5
00:00:10,790 --> 00:00:12,795
But it's really important and

6
00:00:12,795 --> 00:00:14,980
even more important than understanding the math,

7
00:00:14,980 --> 00:00:16,670
I want you to understand the concepts.

8
00:00:16,670 --> 00:00:18,050
So in this video, we're going to

9
00:00:18,050 --> 00:00:20,530
talk about the Null Hypothesis.

10
00:00:20,530 --> 00:00:23,520
The Null Hypothesis actually builds on

11
00:00:23,520 --> 00:00:26,190
what we were just talking about in the previous lesson.

12
00:00:26,190 --> 00:00:29,490
That is we're doing data analysis on samples,

13
00:00:29,490 --> 00:00:31,040
we're doing research on samples,

14
00:00:31,040 --> 00:00:32,640
and our samples are just estimates of

15
00:00:32,640 --> 00:00:34,380
what's going on in the population.

16
00:00:34,380 --> 00:00:35,580
I get 100 people,

17
00:00:35,580 --> 00:00:37,620
and I use that as a guess

18
00:00:37,620 --> 00:00:38,820
to make an inference about what's

19
00:00:38,820 --> 00:00:40,170
really going on in

20
00:00:40,170 --> 00:00:42,280
the larger population but it is just a guess,

21
00:00:42,280 --> 00:00:43,790
it is just an inference.

22
00:00:43,790 --> 00:00:46,770
We said, that these findings,

23
00:00:46,770 --> 00:00:48,480
these samples are kind of contaminated

24
00:00:48,480 --> 00:00:49,860
with random chance a little bit.

25
00:00:49,860 --> 00:00:52,980
So, what that's going to suggest to us

26
00:00:52,980 --> 00:00:54,575
or this possibility that we're going to

27
00:00:54,575 --> 00:00:56,295
get into with the Null Hypothesis,

28
00:00:56,295 --> 00:00:59,130
is that it can make false findings appear.

29
00:00:59,130 --> 00:01:01,990
So, I want to illustrate that for you and

30
00:01:01,990 --> 00:01:06,075
then talk about what we can do to combat that.

31
00:01:06,075 --> 00:01:10,410
So, here is a potential data situation.

32
00:01:10,410 --> 00:01:11,910
So, imagine you're teaching

33
00:01:11,910 --> 00:01:13,755
an online course for instance,

34
00:01:13,755 --> 00:01:15,900
and you are wanting to look at

35
00:01:15,900 --> 00:01:18,220
whether people are satisfied with the course.

36
00:01:18,220 --> 00:01:20,490
So, we've collected some data on how satisfied

37
00:01:20,490 --> 00:01:23,375
people are with your online course that you're running.

38
00:01:23,375 --> 00:01:26,430
Maybe you think that your course might

39
00:01:26,430 --> 00:01:27,690
be a little better for people

40
00:01:27,690 --> 00:01:29,100
who are a little bit more experienced.

41
00:01:29,100 --> 00:01:30,350
So, you want to break this out.

42
00:01:30,350 --> 00:01:31,410
You want to see if the people who

43
00:01:31,410 --> 00:01:32,715
have some experience are

44
00:01:32,715 --> 00:01:35,715
happier with your course than people who lack experience.

45
00:01:35,715 --> 00:01:37,180
So, you just break this out.

46
00:01:37,180 --> 00:01:39,325
So, here's a basic data visualization.

47
00:01:39,325 --> 00:01:41,035
We see here on the left side,

48
00:01:41,035 --> 00:01:44,490
our satisfaction points of people who have no experience.

49
00:01:44,490 --> 00:01:46,500
Then, people on the right are

50
00:01:46,500 --> 00:01:48,765
people who are a little bit more experienced learners,

51
00:01:48,765 --> 00:01:50,710
and if we look on the satisfaction scale,

52
00:01:50,710 --> 00:01:51,750
the people on the right here,

53
00:01:51,750 --> 00:01:53,600
do tend to score a little bit higher.

54
00:01:53,600 --> 00:01:54,920
That little box plot on there,

55
00:01:54,920 --> 00:01:56,890
kind of illustrates that relationship,

56
00:01:56,890 --> 00:01:59,310
seems like a pronounced difference.

57
00:01:59,310 --> 00:02:01,350
So, if you were doing this at

58
00:02:01,350 --> 00:02:04,045
your workplace or in your organization,

59
00:02:04,045 --> 00:02:05,340
you might think "Wow.

60
00:02:05,340 --> 00:02:11,050
It certainly looks like my course is better for people.

61
00:02:11,050 --> 00:02:12,250
They're more satisfied with

62
00:02:12,250 --> 00:02:14,635
my course, if they have experience."

63
00:02:14,635 --> 00:02:17,035
I might want to draw that conclusion.

64
00:02:17,035 --> 00:02:20,135
I want to say that's certainly suggested by the data,

65
00:02:20,135 --> 00:02:22,000
but the problem is, as we've just said,

66
00:02:22,000 --> 00:02:24,545
"We are working with a sample.

67
00:02:24,545 --> 00:02:26,655
Because of that, I need to know,

68
00:02:26,655 --> 00:02:30,365
is this a real effect of experience?

69
00:02:30,365 --> 00:02:31,680
If I were to run this again,

70
00:02:31,680 --> 00:02:33,180
get more people, would I see the same thing?

71
00:02:33,180 --> 00:02:36,190
Is this a real thing that we're finding with our study?

72
00:02:36,190 --> 00:02:39,705
Or, might this just be a coincidence?"

73
00:02:39,705 --> 00:02:42,000
The Null Hypothesis is going to say,

74
00:02:42,000 --> 00:02:43,190
"It's just a coincidence."

75
00:02:43,190 --> 00:02:47,340
That Null Hypothesis is going to say that in fact,

76
00:02:47,340 --> 00:02:50,025
you have stumbled across just randomness.

77
00:02:50,025 --> 00:02:52,005
You have a random coincidence

78
00:02:52,005 --> 00:02:53,640
due to sampling error because

79
00:02:53,640 --> 00:02:55,075
your samples are just estimates,

80
00:02:55,075 --> 00:02:57,220
they look different in your sample,

81
00:02:57,220 --> 00:03:00,135
but they're not really different in the population.

82
00:03:00,135 --> 00:03:02,785
If you understand the Null Hypothesis,

83
00:03:02,785 --> 00:03:06,569
you immediately realize that in fact,

84
00:03:06,569 --> 00:03:10,185
this possibility overshadows every finding

85
00:03:10,185 --> 00:03:12,825
based out of a sample ever done.

86
00:03:12,825 --> 00:03:14,790
Because what we're saying is,

87
00:03:14,790 --> 00:03:17,300
that your samples are estimates,

88
00:03:17,300 --> 00:03:19,160
those estimates could be wrong,

89
00:03:19,160 --> 00:03:21,865
and they could make differences appear out of nowhere.

90
00:03:21,865 --> 00:03:24,135
Differences that are not real.

91
00:03:24,135 --> 00:03:27,840
Imagine for example that our sample on

92
00:03:27,840 --> 00:03:29,340
the left side is an estimate of how

93
00:03:29,340 --> 00:03:31,250
satisfied the people with no experience are.

94
00:03:31,250 --> 00:03:32,775
But it's just an estimate.

95
00:03:32,775 --> 00:03:34,430
Imagine our estimate's just a little low.

96
00:03:34,430 --> 00:03:37,680
So like, bar is just down a little bit lower.

97
00:03:37,680 --> 00:03:39,255
Just due to random chance.

98
00:03:39,255 --> 00:03:42,900
Imagine the exact same situation happens on the right.

99
00:03:42,900 --> 00:03:44,850
Our estimate of how satisfied the people

100
00:03:44,850 --> 00:03:46,570
with some experience are,

101
00:03:46,570 --> 00:03:47,910
it's just a little overestimated,

102
00:03:47,910 --> 00:03:49,800
we just overestimate it, because we're estimating.

103
00:03:49,800 --> 00:03:51,930
So now, it looks like they're different,

104
00:03:51,930 --> 00:03:53,710
but in fact, they're not different.

105
00:03:53,710 --> 00:03:56,355
It's just due to this sampling error this random chance.

106
00:03:56,355 --> 00:03:59,360
We've created a difference in our sample that's not real.

107
00:03:59,360 --> 00:04:01,695
Just due to the fact that we're using samples.

108
00:04:01,695 --> 00:04:03,075
This is a problem.

109
00:04:03,075 --> 00:04:05,880
This is the problem of the Null Hypothesis.

110
00:04:05,880 --> 00:04:08,020
We see this too if we're looking at trends,

111
00:04:08,020 --> 00:04:10,710
so I've got a scatter plot here that shows,

112
00:04:10,710 --> 00:04:13,125
how engaged employees are on the x axis,

113
00:04:13,125 --> 00:04:14,970
and how they perform on the y axis,

114
00:04:14,970 --> 00:04:17,635
and it looks like in this sample of data,

115
00:04:17,635 --> 00:04:18,900
that there's an upward trend.

116
00:04:18,900 --> 00:04:20,220
The more engaged employees

117
00:04:20,220 --> 00:04:21,680
are performing better and that's

118
00:04:21,680 --> 00:04:25,070
good in theory, because "Hey,

119
00:04:25,070 --> 00:04:28,290
if making engaged employees makes for better employees,

120
00:04:28,290 --> 00:04:30,930
then we really want to keep them engaged at work,

121
00:04:30,930 --> 00:04:33,325
and we might make some business decisions based on that."

122
00:04:33,325 --> 00:04:36,635
But what if this is just a fluke of my sample?

123
00:04:36,635 --> 00:04:38,410
What if just due to random chance I

124
00:04:38,410 --> 00:04:40,055
happen to have some data points,

125
00:04:40,055 --> 00:04:41,330
it looks like there's a trend,

126
00:04:41,330 --> 00:04:43,360
but I just happen to randomly sample

127
00:04:43,360 --> 00:04:45,805
the data points for which the trend is going up.

128
00:04:45,805 --> 00:04:49,105
What if, I were to repeat this,

129
00:04:49,105 --> 00:04:51,150
and I found that there was no relationship,

130
00:04:51,150 --> 00:04:53,030
there's just a coincidence of the sample.

131
00:04:53,030 --> 00:04:54,515
Again, the sample is just a guess.

132
00:04:54,515 --> 00:04:56,750
It's entirely possible that trend line

133
00:04:56,750 --> 00:04:59,255
is actually flat in the population.

134
00:04:59,255 --> 00:05:01,295
I wouldn't want to make a business decision

135
00:05:01,295 --> 00:05:03,440
on the basis of a fluke.

136
00:05:03,440 --> 00:05:06,975
So, we have to really guard against this.

137
00:05:06,975 --> 00:05:09,280
So, the Null Hypothesis,

138
00:05:09,280 --> 00:05:13,060
states that the effect is absent in the population.

139
00:05:13,060 --> 00:05:15,710
That zero. Whatever the effect is that you're measuring,

140
00:05:15,710 --> 00:05:17,580
whatever the difference is that you're measuring,

141
00:05:17,580 --> 00:05:19,500
whatever the trend is that you're analyzing,

142
00:05:19,500 --> 00:05:20,590
the Null Hypothesis says,

143
00:05:20,590 --> 00:05:22,620
it's not really there, it's zero.

144
00:05:22,620 --> 00:05:25,580
So, why did you find something in your sample?

145
00:05:25,580 --> 00:05:28,040
It's just that random chance, that sampling error.

146
00:05:28,040 --> 00:05:32,240
That's a pretty big bold statement but it

147
00:05:32,240 --> 00:05:33,710
looms like a shadow over

148
00:05:33,710 --> 00:05:36,615
every survey-based or sample-based finding.

149
00:05:36,615 --> 00:05:39,030
So, in the example of this experiment,

150
00:05:39,030 --> 00:05:40,490
it's entirely possible that

151
00:05:40,490 --> 00:05:42,845
the averages are actually equal.

152
00:05:42,845 --> 00:05:44,570
So, this little Mu symbol here,

153
00:05:44,570 --> 00:05:46,595
this little Greek letter Mu says,

154
00:05:46,595 --> 00:05:47,690
"that's the average, so we

155
00:05:47,690 --> 00:05:48,810
could actually say that there's

156
00:05:48,810 --> 00:05:51,470
zero difference at the population level."

157
00:05:51,470 --> 00:05:53,560
That's what the Null Hypothesis would say.

158
00:05:53,560 --> 00:05:55,020
But, you don't really need to know

159
00:05:55,020 --> 00:05:56,780
the symbol equation to

160
00:05:56,780 --> 00:05:58,370
be successful in this research course,

161
00:05:58,370 --> 00:06:00,510
I'm not really concerned that you can write these,

162
00:06:00,510 --> 00:06:01,700
or know the Greek symbols

163
00:06:01,700 --> 00:06:03,210
for the models that we're doing.

164
00:06:03,210 --> 00:06:04,680
That's not really the goal here.

165
00:06:04,680 --> 00:06:06,770
But I want to illustrate the principle.

166
00:06:06,770 --> 00:06:08,720
That it is possible that the averages are actually

167
00:06:08,720 --> 00:06:10,850
identical in the population.

168
00:06:10,850 --> 00:06:12,410
They're just different in your sample because

169
00:06:12,410 --> 00:06:14,345
your sample is an estimate.

170
00:06:14,345 --> 00:06:17,125
Sometimes a biased estimate. Or an imperfect estimate.

171
00:06:17,125 --> 00:06:19,430
Now, of course, it's also

172
00:06:19,430 --> 00:06:21,710
possible that they are not different.

173
00:06:21,710 --> 00:06:23,869
We call this the alternative hypothesis,

174
00:06:23,869 --> 00:06:26,030
and the alternative hypothesis is what you

175
00:06:26,030 --> 00:06:28,490
actually are hoping to find usually,

176
00:06:28,490 --> 00:06:30,870
that in fact, there is some sort of difference

177
00:06:30,870 --> 00:06:33,855
that your study has picked up upon.

178
00:06:33,855 --> 00:06:36,410
So, the Null Hypothesis

179
00:06:36,410 --> 00:06:38,505
tells us our effects are due to chance.

180
00:06:38,505 --> 00:06:40,880
We would really like to know if that's the case.

181
00:06:40,880 --> 00:06:44,130
We would really like to know if we have real effects?

182
00:06:44,130 --> 00:06:46,615
Real differences? Real trends?

183
00:06:46,615 --> 00:06:48,100
The alternative hypothesis.

184
00:06:48,100 --> 00:06:50,345
We would like to know, if

185
00:06:50,345 --> 00:06:52,870
we have things due to random chance.

186
00:06:52,870 --> 00:06:54,105
That's the Null Hypothesis.

187
00:06:54,105 --> 00:06:56,980
We have to deal with that anytime we get a random sample,

188
00:06:56,980 --> 00:06:58,700
if we can reject the Null Hypothesis,

189
00:06:58,700 --> 00:06:59,825
if we can discredit it,

190
00:06:59,825 --> 00:07:01,040
we can go about our day,

191
00:07:01,040 --> 00:07:02,650
and we can draw good conclusions.

192
00:07:02,650 --> 00:07:04,740
If we can't reject the Null Hypothesis,

193
00:07:04,740 --> 00:07:06,440
then I'm really not going to take a lot of

194
00:07:06,440 --> 00:07:08,345
stock in those results. I'm going to be cautious.

195
00:07:08,345 --> 00:07:11,210
I might even set them aside because I really

196
00:07:11,210 --> 00:07:12,770
want to make sure I'm not basing

197
00:07:12,770 --> 00:07:15,350
my conclusions on a fluke.

