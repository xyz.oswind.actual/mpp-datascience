0
00:00:02,150 --> 00:00:05,160
>> Hello and welcome. In this lesson,

1
00:00:05,160 --> 00:00:07,920
I want to talk about different kinds of data.

2
00:00:07,920 --> 00:00:09,780
Specifically, I want to talk about

3
00:00:09,780 --> 00:00:12,540
sample data and population data.

4
00:00:12,540 --> 00:00:14,545
Now, population data.

5
00:00:14,545 --> 00:00:16,050
What is the population?

6
00:00:16,050 --> 00:00:18,360
The population is all of

7
00:00:18,360 --> 00:00:20,280
the people or observations or

8
00:00:20,280 --> 00:00:22,325
data points we might want to know about.

9
00:00:22,325 --> 00:00:23,945
So, for studying a product,

10
00:00:23,945 --> 00:00:26,355
this is all the people who could be

11
00:00:26,355 --> 00:00:29,865
buying our product or all of our potential customers.

12
00:00:29,865 --> 00:00:33,410
Usually, the population is what we want to know about.

13
00:00:33,410 --> 00:00:35,190
If I have population data,

14
00:00:35,190 --> 00:00:36,900
I'm in a really good spot.

15
00:00:36,900 --> 00:00:40,045
This is often what we imagine ourselves having.

16
00:00:40,045 --> 00:00:41,390
You download some data,

17
00:00:41,390 --> 00:00:42,635
you have some data set,

18
00:00:42,635 --> 00:00:45,090
and you're going to analyze the data and we immediately

19
00:00:45,090 --> 00:00:48,125
start running all sorts of data analyses.

20
00:00:48,125 --> 00:00:49,429
We're looking at averages,

21
00:00:49,429 --> 00:00:50,490
we're looking at trends,

22
00:00:50,490 --> 00:00:51,920
we're looking at relationships,

23
00:00:51,920 --> 00:00:53,310
we start to think about

24
00:00:53,310 --> 00:00:55,965
all of these great things we could do.

25
00:00:55,965 --> 00:00:59,060
This is how we often live our lives.

26
00:00:59,060 --> 00:01:00,870
We live our lives thinking as though

27
00:01:00,870 --> 00:01:03,060
we have this population data and so

28
00:01:03,060 --> 00:01:04,980
really we just need to poke around in

29
00:01:04,980 --> 00:01:07,340
our data a little bit and explore it.

30
00:01:07,340 --> 00:01:11,040
I cannot state this strongly enough,

31
00:01:11,040 --> 00:01:14,360
but that is actually not the world in which we live in.

32
00:01:14,360 --> 00:01:17,960
I'm sorry to say this, but usually,

33
00:01:17,960 --> 00:01:20,190
when you're doing work in research and

34
00:01:20,190 --> 00:01:23,265
often in any kind of analytics role,

35
00:01:23,265 --> 00:01:25,640
you don't have population data.

36
00:01:25,640 --> 00:01:28,080
I'm really sorry to have to break this to you,

37
00:01:28,080 --> 00:01:30,250
but we often only have sample data.

38
00:01:30,250 --> 00:01:32,140
Now, by sample data,

39
00:01:32,140 --> 00:01:34,350
I mean some of

40
00:01:34,350 --> 00:01:35,670
the observations we might want

41
00:01:35,670 --> 00:01:37,365
to make a generalization about.

42
00:01:37,365 --> 00:01:39,280
If you want to know about your customers,

43
00:01:39,280 --> 00:01:40,320
you don't have all the

44
00:01:40,320 --> 00:01:41,900
possible customers in your data set.

45
00:01:41,900 --> 00:01:44,115
Sorry, we only have some.

46
00:01:44,115 --> 00:01:46,260
If you want to know what

47
00:01:46,260 --> 00:01:48,340
all of the people in your target market think,

48
00:01:48,340 --> 00:01:50,890
you only have data on some of them.

49
00:01:50,890 --> 00:01:52,920
So, we have a problem.

50
00:01:52,920 --> 00:01:55,160
We want to draw inferences about everybody,

51
00:01:55,160 --> 00:01:56,970
but we only have some people.

52
00:01:56,970 --> 00:01:58,770
We have a sample and

53
00:01:58,770 --> 00:02:00,565
often this is what we're working with.

54
00:02:00,565 --> 00:02:02,425
This is a problem

55
00:02:02,425 --> 00:02:04,935
because and I want to state this really strongly,

56
00:02:04,935 --> 00:02:07,075
samples will only give you

57
00:02:07,075 --> 00:02:09,660
error-prone guesses at reality.

58
00:02:09,660 --> 00:02:11,025
They will not give you

59
00:02:11,025 --> 00:02:13,140
an accurate depiction of the real world.

60
00:02:13,140 --> 00:02:17,500
Now, I know that we don't have much else to go on,

61
00:02:17,500 --> 00:02:18,880
we're kind of stuck with samples,

62
00:02:18,880 --> 00:02:20,905
that's fine we can live with that.

63
00:02:20,905 --> 00:02:23,830
But I think we often forget that,

64
00:02:23,830 --> 00:02:26,580
we get lost in the sea of data that we analyze

65
00:02:26,580 --> 00:02:28,170
when we're doing data science work or

66
00:02:28,170 --> 00:02:29,800
research work, we get lost in this.

67
00:02:29,800 --> 00:02:31,410
We are used to just

68
00:02:31,410 --> 00:02:33,600
staring at our data from our sample and we

69
00:02:33,600 --> 00:02:36,150
forget this is just an error-prone guess

70
00:02:36,150 --> 00:02:38,370
or an estimate of what's really going on.

71
00:02:38,370 --> 00:02:40,740
So, I want to do a little demo with you

72
00:02:40,740 --> 00:02:43,860
that illustrates the problem of relying on samples.

73
00:02:43,860 --> 00:02:45,300
I want to humble you a little

74
00:02:45,300 --> 00:02:47,370
bit when you're looking at sample data.

75
00:02:47,370 --> 00:02:49,290
I want to humble you so that when you get into

76
00:02:49,290 --> 00:02:51,480
situations where you're analyzing sample data,

77
00:02:51,480 --> 00:02:52,980
you're not lost, you don't make

78
00:02:52,980 --> 00:02:55,075
bad decisions and bad conclusions.

79
00:02:55,075 --> 00:02:57,355
So, let's see what happens.

80
00:02:57,355 --> 00:02:59,210
Here's an illustration. I want you to

81
00:02:59,210 --> 00:03:00,680
imagine that we're trying

82
00:03:00,680 --> 00:03:03,080
to discover what percentage

83
00:03:03,080 --> 00:03:05,210
of customers are willing to try a new product.

84
00:03:05,210 --> 00:03:06,890
So, we're going to get samples of

85
00:03:06,890 --> 00:03:08,720
customers and we're going to

86
00:03:08,720 --> 00:03:10,970
ask them or test in some way,

87
00:03:10,970 --> 00:03:12,135
and maybe not a questionnaire

88
00:03:12,135 --> 00:03:13,440
if we don't trust the questionnaire,

89
00:03:13,440 --> 00:03:14,870
but we're going to assess in some way

90
00:03:14,870 --> 00:03:17,100
whether they're willing to try a new product.

91
00:03:17,100 --> 00:03:19,040
In this case, I'm going to actually give you

92
00:03:19,040 --> 00:03:21,020
the answer before we start.

93
00:03:21,020 --> 00:03:22,370
The answer is 30 percent.

94
00:03:22,370 --> 00:03:24,200
So, all we need to do is

95
00:03:24,200 --> 00:03:26,510
find 30 percent. That is our goal.

96
00:03:26,510 --> 00:03:29,295
Run a study or a survey or

97
00:03:29,295 --> 00:03:32,305
data science model or something and find 30 percent.

98
00:03:32,305 --> 00:03:35,310
If we can get 30 percent, we're good. Let's try this.

99
00:03:35,310 --> 00:03:37,385
What we're going to do is we're going to take

100
00:03:37,385 --> 00:03:41,510
some random samples of people.

101
00:03:41,510 --> 00:03:43,865
These people are all drawn

102
00:03:43,865 --> 00:03:47,120
from a population in which 30 percent is the answer.

103
00:03:47,120 --> 00:03:51,230
So, I just stuck my metaphorical hand in a bag and pulled

104
00:03:51,230 --> 00:03:54,020
out observations from a universe

105
00:03:54,020 --> 00:03:55,690
in which the answer is 30 percent,

106
00:03:55,690 --> 00:03:57,450
three out of every 10 people do

107
00:03:57,450 --> 00:03:59,945
actually want to try the new product,

108
00:03:59,945 --> 00:04:02,020
but if I get samples,

109
00:04:02,020 --> 00:04:03,410
will I actually get 30 percent?

110
00:04:03,410 --> 00:04:05,680
So, let's see what happens. These are samples of size 10.

111
00:04:05,680 --> 00:04:08,210
They've scored these one and zero by the way.

112
00:04:08,210 --> 00:04:10,550
So, a one means you're willing to try the product and a

113
00:04:10,550 --> 00:04:13,100
zero means you're not willing to try the product.

114
00:04:13,100 --> 00:04:16,630
So, let's see what happens. Here's the first sample.

115
00:04:16,630 --> 00:04:18,720
Okay, so I got 10 of them,

116
00:04:18,720 --> 00:04:20,770
I've drawn them, I just picked these at random.

117
00:04:20,770 --> 00:04:23,585
So, I got two out of 10.

118
00:04:23,585 --> 00:04:25,040
That's close to 30 percent.

119
00:04:25,040 --> 00:04:27,290
It's actually well, 20 percent.

120
00:04:27,290 --> 00:04:29,540
So, it's not quite 30 percent.

121
00:04:29,540 --> 00:04:31,300
This actually makes sense, right?

122
00:04:31,300 --> 00:04:33,950
Because my sample is just a guess, it's just an estimate.

123
00:04:33,950 --> 00:04:37,690
I've pulled out a few observations from the world.

124
00:04:37,690 --> 00:04:39,755
It's not going to be perfectly the same

125
00:04:39,755 --> 00:04:42,875
as reality of the real world that I'm sampling from.

126
00:04:42,875 --> 00:04:44,785
Let's pull out another sample.

127
00:04:44,785 --> 00:04:46,815
By the way I didn't doctor these in any way.

128
00:04:46,815 --> 00:04:49,220
These were actual samples that I collected.

129
00:04:49,220 --> 00:04:51,095
This one also gave me 20 percent.

130
00:04:51,095 --> 00:04:53,930
Pulled out another one, this time I got none.

131
00:04:53,930 --> 00:04:55,850
That should scare you a little bit.

132
00:04:55,850 --> 00:04:57,470
This is estimating zero percent.

133
00:04:57,470 --> 00:05:00,230
Each of these samples is trying to estimate 30 percent.

134
00:05:00,230 --> 00:05:01,340
But there are just estimates,

135
00:05:01,340 --> 00:05:04,325
they're not giving you the actual value. Let's try again.

136
00:05:04,325 --> 00:05:07,670
This one gave me 10 percent, did it again.

137
00:05:07,670 --> 00:05:10,380
Okay, this time I got over now 40 percent, right?

138
00:05:10,380 --> 00:05:12,020
You see what's happening every time we

139
00:05:12,020 --> 00:05:14,195
collect a sample, we are estimating.

140
00:05:14,195 --> 00:05:16,130
The thing I want to know and estimates are

141
00:05:16,130 --> 00:05:19,865
just that estimates, their error-prone guesses.

142
00:05:19,865 --> 00:05:21,830
Last one, this is true.

143
00:05:21,830 --> 00:05:23,335
I took what is that?

144
00:05:23,335 --> 00:05:24,650
This is the sixth one,

145
00:05:24,650 --> 00:05:26,720
I actually got 30 percent this time.

146
00:05:26,720 --> 00:05:29,435
So, this illustrates the process of sampling.

147
00:05:29,435 --> 00:05:31,530
When you are collecting samples,

148
00:05:31,530 --> 00:05:34,190
you have only an error-prone guess or

149
00:05:34,190 --> 00:05:35,750
an error-prone estimate of

150
00:05:35,750 --> 00:05:37,855
whatever it is that you want to understand.

151
00:05:37,855 --> 00:05:40,675
So, in this case, if I were given any one of these,

152
00:05:40,675 --> 00:05:42,140
if I were given the third sample,

153
00:05:42,140 --> 00:05:44,105
I might think nobody wants to try my product.

154
00:05:44,105 --> 00:05:46,790
If I were given the fifth sample up there,

155
00:05:46,790 --> 00:05:47,900
I might think 40 percent of

156
00:05:47,900 --> 00:05:49,295
people want to try my product.

157
00:05:49,295 --> 00:05:51,970
I don't actually have a good estimate here.

158
00:05:51,970 --> 00:05:53,720
The reason I don't have a good estimate is I only

159
00:05:53,720 --> 00:05:55,550
have 10 people in my sample, we'll get to that.

160
00:05:55,550 --> 00:05:57,290
But I want to humble you a little

161
00:05:57,290 --> 00:05:59,825
bit when you're relying on samples,

162
00:05:59,825 --> 00:06:01,660
samples are not populations.

163
00:06:01,660 --> 00:06:03,170
The data you get from a sample,

164
00:06:03,170 --> 00:06:06,905
are not the true answer you're seeking.

165
00:06:06,905 --> 00:06:10,130
It's just a guess. So, let's try simulating this.

166
00:06:10,130 --> 00:06:11,340
Now by the way, in the labs

167
00:06:11,340 --> 00:06:12,570
that go along with this lesson you'll get

168
00:06:12,570 --> 00:06:13,710
a chance to actually run some

169
00:06:13,710 --> 00:06:15,010
of these simulations yourself,

170
00:06:15,010 --> 00:06:16,640
should be some good fun.

171
00:06:16,640 --> 00:06:17,975
But let's just go ahead and simulate this.

172
00:06:17,975 --> 00:06:19,700
So, instead of doing six samples,

173
00:06:19,700 --> 00:06:21,870
I'm gonna run 10,000 of them and I'm just going to make

174
00:06:21,870 --> 00:06:24,685
a little histogram of the results, and here we go.

175
00:06:24,685 --> 00:06:26,730
So, you see here the bars here

176
00:06:26,730 --> 00:06:28,635
represent how often I get different results

177
00:06:28,635 --> 00:06:31,020
and you see here the most common answer

178
00:06:31,020 --> 00:06:32,545
is the bar right over 30 percent.

179
00:06:32,545 --> 00:06:34,090
So, that's good, right?

180
00:06:34,090 --> 00:06:35,460
These are my samples and

181
00:06:35,460 --> 00:06:37,535
the most common answer was 30 percent.

182
00:06:37,535 --> 00:06:39,920
So, if I do 10,000 samples,

183
00:06:39,920 --> 00:06:42,450
I do tend to get the right answer on average.

184
00:06:42,450 --> 00:06:44,800
But look how spread out the samples are.

185
00:06:44,800 --> 00:06:46,820
Some are going all the way down to zero,

186
00:06:46,820 --> 00:06:48,630
a few unlucky samples are

187
00:06:48,630 --> 00:06:50,570
actually hitting up above 60 percent,

188
00:06:50,570 --> 00:06:52,770
70 percent maybe even up to 80 percent.

189
00:06:52,770 --> 00:06:56,100
It's not happening very often, but it is happening,

190
00:06:56,100 --> 00:06:59,720
it is possible to get wildly inaccurate estimates.

191
00:06:59,720 --> 00:07:02,120
So, our samples are just estimates and

192
00:07:02,120 --> 00:07:05,000
sometimes they can be wildly inaccurate.

193
00:07:05,000 --> 00:07:07,220
Just in case you're wondering I did

194
00:07:07,220 --> 00:07:08,900
actually run some stats on this and I

195
00:07:08,900 --> 00:07:10,400
actually took the mean of

196
00:07:10,400 --> 00:07:11,690
this distribution and I am

197
00:07:11,690 --> 00:07:13,130
actually getting 30 percent on average.

198
00:07:13,130 --> 00:07:15,440
So, again on average, samples are trustworthy.

199
00:07:15,440 --> 00:07:19,425
It's just individual samples are not as trustworthy.

200
00:07:19,425 --> 00:07:22,515
We call this problem by the way a sampling error.

201
00:07:22,515 --> 00:07:24,520
You might think of this as just random chance.

202
00:07:24,520 --> 00:07:27,520
Any one sample is not trustworthy

203
00:07:27,520 --> 00:07:30,990
because it is contaminated with a sampling error.

204
00:07:30,990 --> 00:07:32,280
I'm envisioning your samples

205
00:07:32,280 --> 00:07:34,760
as zombie sets of data, right?

206
00:07:34,760 --> 00:07:36,240
They're infected with the disease

207
00:07:36,240 --> 00:07:37,760
of error, they're error-prone.

208
00:07:37,760 --> 00:07:39,780
When you get this one sample,

209
00:07:39,780 --> 00:07:42,905
you don't know how erroneous it is.

210
00:07:42,905 --> 00:07:44,730
But we do have one thing that will

211
00:07:44,730 --> 00:07:46,395
help us and that a sample size.

212
00:07:46,395 --> 00:07:48,650
So, this is samples of size 10.

213
00:07:48,650 --> 00:07:51,220
You pick 10 people at random from your workplace,

214
00:07:51,220 --> 00:07:53,190
they might not be very representative.

215
00:07:53,190 --> 00:07:54,930
Any one person with

216
00:07:54,930 --> 00:07:56,640
random flukes or weirdness

217
00:07:56,640 --> 00:07:58,635
could throw off the estimate quite a bit.

218
00:07:58,635 --> 00:08:01,620
What if we used a larger sample? So, let's try that.

219
00:08:01,620 --> 00:08:03,620
In the next slide here, okay.

220
00:08:03,620 --> 00:08:05,040
I've read on this, but I've run

221
00:08:05,040 --> 00:08:06,590
this with 100 people per sample,

222
00:08:06,590 --> 00:08:08,025
so still doing 10,000.

223
00:08:08,025 --> 00:08:10,900
You notice something, on the previous slide I'll go back,

224
00:08:10,900 --> 00:08:12,250
you see us spread out those scores

225
00:08:12,250 --> 00:08:13,475
are, they're spread out quite a bit.

226
00:08:13,475 --> 00:08:15,270
But here, they're much more

227
00:08:15,270 --> 00:08:17,310
tightly clustered around 30 percent.

228
00:08:17,310 --> 00:08:20,100
In fact, here if I run some stats on this,

229
00:08:20,100 --> 00:08:21,795
the average percent is still 30.

230
00:08:21,795 --> 00:08:23,970
But the standard deviation,

231
00:08:23,970 --> 00:08:26,380
the average degree to which my sample is off,

232
00:08:26,380 --> 00:08:28,590
is only about five percent.

233
00:08:28,590 --> 00:08:31,410
So, this is good. These samples are more trustworthy.

234
00:08:31,410 --> 00:08:34,230
Any one of these samples is likely to be on

235
00:08:34,230 --> 00:08:36,995
average within about five percent of the right answer.

236
00:08:36,995 --> 00:08:38,760
On the previous slide, bounce back,

237
00:08:38,760 --> 00:08:40,470
the average sample was

238
00:08:40,470 --> 00:08:43,230
about 14 percent from the right answer.

239
00:08:43,230 --> 00:08:45,240
I definitely want to be in

240
00:08:45,240 --> 00:08:48,430
this situation where I've got a sample size of 100.

241
00:08:48,430 --> 00:08:50,315
What's going on here?

242
00:08:50,315 --> 00:08:52,455
Well, a very simple principle.

243
00:08:52,455 --> 00:08:54,245
The larger my sample,

244
00:08:54,245 --> 00:08:55,910
the more trustworthy it is.

245
00:08:55,910 --> 00:08:58,750
So, we've reduced our sampling error.

246
00:08:58,750 --> 00:09:00,030
We've reduced the impact of

247
00:09:00,030 --> 00:09:02,370
random chance just by having a bigger sample.

248
00:09:02,370 --> 00:09:04,530
So, this is good news because it tells

249
00:09:04,530 --> 00:09:06,925
us there's something we can do as researchers,

250
00:09:06,925 --> 00:09:08,490
as data scientists to

251
00:09:08,490 --> 00:09:10,755
improve the trustworthiness of our samples.

252
00:09:10,755 --> 00:09:13,145
Yes they're contaminated with random chance,

253
00:09:13,145 --> 00:09:15,800
but if you can have a big enough sample,

254
00:09:15,800 --> 00:09:17,560
you can minimize that.

255
00:09:17,560 --> 00:09:22,140
Just for fun, I reran this with a sample size of 10,000.

256
00:09:22,140 --> 00:09:23,790
These are ridiculously large samples.

257
00:09:23,790 --> 00:09:25,290
You see almost every sample is

258
00:09:25,290 --> 00:09:27,525
hitting 30 percent or very close to it.

259
00:09:27,525 --> 00:09:30,210
So, here we've got an average of 30 percent.

260
00:09:30,210 --> 00:09:33,270
In this case, the average sample is,

261
00:09:33,270 --> 00:09:36,225
it's looking like it's within one percent

262
00:09:36,225 --> 00:09:38,975
actually about half of one percent from the right answer.

263
00:09:38,975 --> 00:09:41,190
So, clearly, if I want to

264
00:09:41,190 --> 00:09:43,930
get good estimates of whatever it is I'm doing,

265
00:09:43,930 --> 00:09:45,810
samples are estimates, but you

266
00:09:45,810 --> 00:09:47,800
can have good estimates and bad estimates.

267
00:09:47,800 --> 00:09:49,830
In this case, as long as you're sampling

268
00:09:49,830 --> 00:09:51,885
from the right location,

269
00:09:51,885 --> 00:09:53,550
what you really need for a good estimate is

270
00:09:53,550 --> 00:09:56,315
just a large enough sample. How large?

271
00:09:56,315 --> 00:09:58,350
Well, for any type of data,

272
00:09:58,350 --> 00:09:59,790
there is something we can calculate called

273
00:09:59,790 --> 00:10:01,690
standard error.

274
00:10:01,690 --> 00:10:03,720
For percentages, which is what we're doing it

275
00:10:03,720 --> 00:10:05,645
looks like this: I take the percentage,

276
00:10:05,645 --> 00:10:06,990
times one minus the percentage

277
00:10:06,990 --> 00:10:08,530
over the sample size and square root.

278
00:10:08,530 --> 00:10:10,875
You don't have to worry about that equation very much.

279
00:10:10,875 --> 00:10:12,090
But what you need to know

280
00:10:12,090 --> 00:10:13,705
is that when I run this equation,

281
00:10:13,705 --> 00:10:17,280
it gives me the average degree

282
00:10:17,280 --> 00:10:19,785
to which I'm off, about half a percent.

283
00:10:19,785 --> 00:10:22,320
This is a super cool tool.

284
00:10:22,320 --> 00:10:24,000
This will literally tell you

285
00:10:24,000 --> 00:10:26,230
how off I think my samples are.

286
00:10:26,230 --> 00:10:28,245
So, in this case, I run this

287
00:10:28,245 --> 00:10:30,150
knowing my sample size and I'm

288
00:10:30,150 --> 00:10:33,690
guessing I'm off by about half of a percent on average.

289
00:10:33,690 --> 00:10:35,590
So, I know that's a trustworthy sample.

290
00:10:35,590 --> 00:10:39,255
Standard error is a beautiful intervention of statistics

291
00:10:39,255 --> 00:10:42,655
because it lets us know how trustworthy our samples are.

292
00:10:42,655 --> 00:10:44,060
For different data types,

293
00:10:44,060 --> 00:10:45,735
the equation for standard error is different.

294
00:10:45,735 --> 00:10:47,160
To be honest, you don't really need

295
00:10:47,160 --> 00:10:48,560
to know these equations because they're

296
00:10:48,560 --> 00:10:50,700
built into the data science and research tools

297
00:10:50,700 --> 00:10:51,885
that we're going to use.

298
00:10:51,885 --> 00:10:53,160
But as a general principle,

299
00:10:53,160 --> 00:10:55,080
I want you to understand that larger samples

300
00:10:55,080 --> 00:10:57,630
are indeed more trustworthy.

301
00:10:57,630 --> 00:10:59,280
We're going to go forward with this and we're

302
00:10:59,280 --> 00:11:00,720
going to explore the impact that this

303
00:11:00,720 --> 00:11:04,570
will have on research decision making in the next lesson.

