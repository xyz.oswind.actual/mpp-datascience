0
00:00:01,010 --> 00:00:04,000
>> In this video, I want to get into

1
00:00:04,000 --> 00:00:05,590
the application of some

2
00:00:05,590 --> 00:00:06,720
of the lessons we've been learning.

3
00:00:06,720 --> 00:00:08,140
We've talked about power,

4
00:00:08,140 --> 00:00:09,835
we've talked about false positives,

5
00:00:09,835 --> 00:00:12,225
and we've talked about good research practice.

6
00:00:12,225 --> 00:00:15,130
I want to talk to you about some bad research practices

7
00:00:15,130 --> 00:00:17,890
or we might call them questionable research practices.

8
00:00:17,890 --> 00:00:19,660
These are known in the scientific community as

9
00:00:19,660 --> 00:00:23,785
QRPs that we might keep an eye out for.

10
00:00:23,785 --> 00:00:25,390
Keep in mind, you're not going to just be

11
00:00:25,390 --> 00:00:27,210
running data analyses and research,

12
00:00:27,210 --> 00:00:28,710
you're also going to be seeing it.

13
00:00:28,710 --> 00:00:31,840
You want to be good at spotting and sniffing

14
00:00:31,840 --> 00:00:33,880
out questionable claims as

15
00:00:33,880 --> 00:00:35,320
well as not making them yourself.

16
00:00:35,320 --> 00:00:38,290
So, I want to run through a menu of things not to

17
00:00:38,290 --> 00:00:39,520
do and things to be

18
00:00:39,520 --> 00:00:42,745
skeptical of when you see them. So, let's take a look.

19
00:00:42,745 --> 00:00:45,950
The first we call filtering for significance.

20
00:00:45,950 --> 00:00:48,625
What this is, you run a bunch of analyses,

21
00:00:48,625 --> 00:00:50,400
you select only the p is less than

22
00:00:50,400 --> 00:00:53,030
0.05 significant ones to share.

23
00:00:53,030 --> 00:00:55,880
This might seem intuitively natural.

24
00:00:55,880 --> 00:00:58,280
After all, we want to report our highlights.

25
00:00:58,280 --> 00:01:00,300
We want to report the big findings,

26
00:01:00,300 --> 00:01:02,340
the significant stuff to share.

27
00:01:02,340 --> 00:01:05,290
The problem is, as we've now seen,

28
00:01:05,290 --> 00:01:07,460
when you're wrong, you're still going to get

29
00:01:07,460 --> 00:01:10,270
significant findings about five percent of the time.

30
00:01:10,270 --> 00:01:12,750
Often, we run a lot of analyses.

31
00:01:12,750 --> 00:01:17,515
So, if you filter out all of the non-significant ones,

32
00:01:17,515 --> 00:01:18,920
you filter out some of the

33
00:01:18,920 --> 00:01:20,240
context that would help make it

34
00:01:20,240 --> 00:01:23,435
clear what's false positive and what's a true positive.

35
00:01:23,435 --> 00:01:25,520
For example, if we compare

36
00:01:25,520 --> 00:01:27,560
two different brand logos and we

37
00:01:27,560 --> 00:01:32,515
find that they do not differ on eight out of 10 measures,

38
00:01:32,515 --> 00:01:33,950
we should be a little skeptical of

39
00:01:33,950 --> 00:01:35,825
those two measures where they do differ.

40
00:01:35,825 --> 00:01:38,480
Because they're rarely differing.

41
00:01:38,480 --> 00:01:40,940
Just in a couple of places and it's not very big.

42
00:01:40,940 --> 00:01:43,390
However, if we filtered those out, and we said,

43
00:01:43,390 --> 00:01:46,155
"Hey, we found two differences between these logos."

44
00:01:46,155 --> 00:01:49,225
You don't tell me about the eight non-differences.

45
00:01:49,225 --> 00:01:51,000
It changes the context.

46
00:01:51,000 --> 00:01:51,760
It sounds like, wow,

47
00:01:51,760 --> 00:01:53,975
there are some pretty consistent differences

48
00:01:53,975 --> 00:01:56,785
when in reality, it's not consistent.

49
00:01:56,785 --> 00:01:59,870
So, keep the results in context.

50
00:01:59,870 --> 00:02:02,455
If there is non-significant results, share them.

51
00:02:02,455 --> 00:02:05,315
There's nothing wrong with making your results truthful.

52
00:02:05,315 --> 00:02:06,710
In fact, you cannot make

53
00:02:06,710 --> 00:02:09,790
good business decisions with bad intelligence.

54
00:02:09,790 --> 00:02:12,840
If you're ever making business decisions, you should ask,

55
00:02:12,840 --> 00:02:14,420
"Is there more information

56
00:02:14,420 --> 00:02:16,500
or evidence that's been filtered out?"

57
00:02:16,500 --> 00:02:19,865
Often, bad or negative information

58
00:02:19,865 --> 00:02:23,360
gets filtered out as you climb a hierarchy.

59
00:02:23,360 --> 00:02:25,310
The people at the top often

60
00:02:25,310 --> 00:02:27,440
don't get the bad news reported to them.

61
00:02:27,440 --> 00:02:29,450
That's really dangerous for

62
00:02:29,450 --> 00:02:31,200
executives and decision makers.

63
00:02:31,200 --> 00:02:33,110
Don't contribute to that problem.

64
00:02:33,110 --> 00:02:34,160
If you don't want to put it in

65
00:02:34,160 --> 00:02:35,840
a presentation or slide deck,

66
00:02:35,840 --> 00:02:38,080
put it in an appendix and call attention to it.

67
00:02:38,080 --> 00:02:39,470
Let people know, "Hey,

68
00:02:39,470 --> 00:02:40,580
there was a bunch of stuff that didn't

69
00:02:40,580 --> 00:02:41,630
work and we should take

70
00:02:41,630 --> 00:02:45,330
that information in context when we make our decisions."

71
00:02:45,330 --> 00:02:47,050
The next one is related.

72
00:02:47,050 --> 00:02:49,340
It's called p-Hacking. Simply put,

73
00:02:49,340 --> 00:02:51,290
it's running many different analyses

74
00:02:51,290 --> 00:02:53,805
to try to get p is less than 0.05.

75
00:02:53,805 --> 00:02:57,260
This is almost guaranteed to give you a false positive.

76
00:02:57,260 --> 00:02:59,314
After all, if I try

77
00:02:59,314 --> 00:03:02,180
running my analyses a bunch of different ways,

78
00:03:02,180 --> 00:03:03,490
every single time I do that,

79
00:03:03,490 --> 00:03:05,945
I've got a five percent risk of a false positive.

80
00:03:05,945 --> 00:03:08,470
Just give me 20 different ways of running the analyses.

81
00:03:08,470 --> 00:03:10,655
I can almost guarantee you I'll find something.

82
00:03:10,655 --> 00:03:13,550
The problem is, I am giving myself a lot of ways to

83
00:03:13,550 --> 00:03:17,225
stumble across that five percent false positive.

84
00:03:17,225 --> 00:03:19,790
This is really common when people

85
00:03:19,790 --> 00:03:22,325
feel motivated to return a significant result.

86
00:03:22,325 --> 00:03:24,740
You might feel incentivized to find something.

87
00:03:24,740 --> 00:03:27,780
But you know what, that's bad if it's not true.

88
00:03:27,780 --> 00:03:29,120
So, we really want to try to be

89
00:03:29,120 --> 00:03:31,400
truthful not just impressive.

90
00:03:31,400 --> 00:03:36,045
Be careful and skeptical of results that say,

91
00:03:36,045 --> 00:03:38,875
"We ran the analysis separate from men and women."

92
00:03:38,875 --> 00:03:40,850
Did they have a reason to do that or were they just

93
00:03:40,850 --> 00:03:42,980
fishing for something that would be significant?

94
00:03:42,980 --> 00:03:45,770
Maybe they suddenly try breaking out results by

95
00:03:45,770 --> 00:03:47,120
regions of the country and running

96
00:03:47,120 --> 00:03:48,785
the analysis separately on those.

97
00:03:48,785 --> 00:03:50,600
It's fine if that's planned,

98
00:03:50,600 --> 00:03:52,340
but you should be very skeptical of it

99
00:03:52,340 --> 00:03:54,200
when it's not because that may be

100
00:03:54,200 --> 00:03:55,880
a sign someone is fishing for

101
00:03:55,880 --> 00:03:58,585
something that's p is less than 0.05.

102
00:03:58,585 --> 00:04:00,710
We really need all that information and

103
00:04:00,710 --> 00:04:04,225
context to avoid that false positive conclusion.

104
00:04:04,225 --> 00:04:06,540
Next, HARKing.

105
00:04:06,540 --> 00:04:08,230
This is known as hypothesizing

106
00:04:08,230 --> 00:04:10,200
after the results are known or harking.

107
00:04:10,200 --> 00:04:13,300
Simply put it is overfitting.

108
00:04:13,300 --> 00:04:14,570
What you're doing is

109
00:04:14,570 --> 00:04:15,945
you're taking whatever the results are

110
00:04:15,945 --> 00:04:19,370
from your sample and pretending like you predicted those.

111
00:04:19,370 --> 00:04:22,160
This is really bad because essentially,

112
00:04:22,160 --> 00:04:25,715
you're taking an estimate and you want to sound good.

113
00:04:25,715 --> 00:04:27,065
People want to sound smart.

114
00:04:27,065 --> 00:04:29,420
It's very common for data

115
00:04:29,420 --> 00:04:30,860
scientists and researchers to want to

116
00:04:30,860 --> 00:04:32,630
sound smart and want to sound impressive.

117
00:04:32,630 --> 00:04:33,950
After all, our professional

118
00:04:33,950 --> 00:04:35,915
reputations are on the line here.

119
00:04:35,915 --> 00:04:38,885
But it's bad news if we're suddenly

120
00:04:38,885 --> 00:04:40,610
inventing theories to match

121
00:04:40,610 --> 00:04:43,370
our sample data because we only have samples.

122
00:04:43,370 --> 00:04:46,395
As we now know, samples are full of errors.

123
00:04:46,395 --> 00:04:49,670
So, I would rather hear what you

124
00:04:49,670 --> 00:04:51,530
thought and what your question

125
00:04:51,530 --> 00:04:53,520
was before you ran the analysis,

126
00:04:53,520 --> 00:04:55,990
and I would rather hear what the analysis actually said.

127
00:04:55,990 --> 00:05:01,155
If you change your prediction to match the result,

128
00:05:01,155 --> 00:05:03,330
A, that's dishonest, but, B,

129
00:05:03,330 --> 00:05:04,430
I can almost guarantee you,

130
00:05:04,430 --> 00:05:07,405
you're going to stumble across some false positives.

131
00:05:07,405 --> 00:05:09,700
Finally, optional stopping.

132
00:05:09,700 --> 00:05:12,350
This one is a big no, no.

133
00:05:12,350 --> 00:05:13,765
We should all get this is

134
00:05:13,765 --> 00:05:16,125
drilled into us at the beginning.

135
00:05:16,125 --> 00:05:18,620
Do not stop your data collection

136
00:05:18,620 --> 00:05:20,565
when p is less than 0.05.

137
00:05:20,565 --> 00:05:22,810
I've heard of some people

138
00:05:22,810 --> 00:05:27,240
checking their results after every data point comes in,

139
00:05:27,240 --> 00:05:29,625
running the test after every data point comes in.

140
00:05:29,625 --> 00:05:31,825
After all, we're running these studies,

141
00:05:31,825 --> 00:05:33,945
we're invested in these, we want to see how it goes.

142
00:05:33,945 --> 00:05:37,095
You're like a cook watching the pot boil

143
00:05:37,095 --> 00:05:40,845
or you're like a planter watching their plants grow.

144
00:05:40,845 --> 00:05:44,075
The problem is five percent of the time,

145
00:05:44,075 --> 00:05:45,680
you'll get that false positive.

146
00:05:45,680 --> 00:05:49,215
So, if you check the data 100 different times,

147
00:05:49,215 --> 00:05:51,960
plenty of the time, a good five percent of the time

148
00:05:51,960 --> 00:05:54,570
you're going to stray into that false positive territory.

149
00:05:54,570 --> 00:05:57,390
It's almost a guarantee.

150
00:05:57,390 --> 00:05:59,735
So, what I advise is,

151
00:05:59,735 --> 00:06:03,990
decide what sample size you need and stick to it.

152
00:06:03,990 --> 00:06:06,690
It's fine to glance while the data is collecting but

153
00:06:06,690 --> 00:06:09,230
do not stop when p is less than 0.05.

154
00:06:09,230 --> 00:06:10,770
You're giving yourself many shots at a

155
00:06:10,770 --> 00:06:12,900
false positive every time you check.

156
00:06:12,900 --> 00:06:14,380
So, if you need

157
00:06:14,380 --> 00:06:16,440
100 participants, collect 100 participants.

158
00:06:16,440 --> 00:06:18,160
If you need 500, collect 500.

159
00:06:18,160 --> 00:06:20,280
But stick to that because you're going to prevent

160
00:06:20,280 --> 00:06:24,155
yourself from being misled by your own p values.

161
00:06:24,155 --> 00:06:27,640
There you have it. A menu of things you shouldn't do.

162
00:06:27,640 --> 00:06:30,200
Things that make research claims questionable.

163
00:06:30,200 --> 00:06:31,540
Things that might let you fool

164
00:06:31,540 --> 00:06:33,660
yourself if you're analyzing data.

165
00:06:33,660 --> 00:06:36,450
If you can avoid these questionable research practices,

166
00:06:36,450 --> 00:06:38,640
and if you can spot them when you see them in use,

167
00:06:38,640 --> 00:06:41,355
you'll make much more sound decisions both in business

168
00:06:41,355 --> 00:06:45,220
and any context in which you're using research or data.

