0
00:00:01,010 --> 00:00:03,810
>> Welcome back. In this lesson,

1
00:00:03,810 --> 00:00:06,000
I want to talk to you about how to know when you've got

2
00:00:06,000 --> 00:00:08,715
a big enough sample to answer your question.

3
00:00:08,715 --> 00:00:11,340
I want you to consider the following situation.

4
00:00:11,340 --> 00:00:13,210
We've got this online course that we're teaching.

5
00:00:13,210 --> 00:00:15,930
We've been working with this example now.

6
00:00:15,930 --> 00:00:17,865
We've seen some difference between them.

7
00:00:17,865 --> 00:00:20,010
The people who have more experience in our course are a

8
00:00:20,010 --> 00:00:21,150
little bit more satisfied than

9
00:00:21,150 --> 00:00:22,860
the people who have less experience.

10
00:00:22,860 --> 00:00:24,770
But I want you to consider two situations.

11
00:00:24,770 --> 00:00:26,760
So the first situation is that

12
00:00:26,760 --> 00:00:29,325
this data is based on 20 people per group.

13
00:00:29,325 --> 00:00:31,595
So I've got 20 people with some experience,

14
00:00:31,595 --> 00:00:33,750
20 people with no experience,

15
00:00:33,750 --> 00:00:36,260
and they've given me different satisfaction ratings.

16
00:00:36,260 --> 00:00:37,780
You need to think about how much you would

17
00:00:37,780 --> 00:00:39,040
trust that finding.

18
00:00:39,040 --> 00:00:41,485
You see a difference, but it's only based on 40 people.

19
00:00:41,485 --> 00:00:44,520
How much would you trust that finding?

20
00:00:44,520 --> 00:00:48,645
Now I want you to contrast that with scenario two.

21
00:00:48,645 --> 00:00:51,030
You know have 4,000 people per

22
00:00:51,030 --> 00:00:53,220
group. Which would you trust more?

23
00:00:53,220 --> 00:00:55,520
If you see some difference between two groups,

24
00:00:55,520 --> 00:00:56,980
do you trust it more when it comes from

25
00:00:56,980 --> 00:01:00,930
40 people or 8,000 people?

26
00:01:01,020 --> 00:01:03,800
I'm guessing intuitively, you

27
00:01:03,800 --> 00:01:06,530
trust it more when it comes from the larger sample.

28
00:01:06,530 --> 00:01:09,200
Intuitively, you know that

29
00:01:09,200 --> 00:01:11,220
a larger sample is going to be more trustworthy.

30
00:01:11,220 --> 00:01:13,220
Intuitively, you know it's probably

31
00:01:13,220 --> 00:01:15,750
not due to chance if I've got 8,000 people.

32
00:01:15,750 --> 00:01:17,530
Intuitively, you've performed

33
00:01:17,530 --> 00:01:19,340
a P-value test in your head.

34
00:01:19,340 --> 00:01:21,775
How easily could I get this result by chance?

35
00:01:21,775 --> 00:01:24,155
With 8,000 people, not very easily.

36
00:01:24,155 --> 00:01:26,180
That's great. We want to know

37
00:01:26,180 --> 00:01:28,390
how we could do that mathematically.

38
00:01:28,390 --> 00:01:31,415
How can we find the optimal number of people

39
00:01:31,415 --> 00:01:34,800
to be able to have a confident answer to our question?

40
00:01:34,800 --> 00:01:37,160
The answer to that is something called Power.

41
00:01:37,160 --> 00:01:39,100
Power or Statistical Power

42
00:01:39,100 --> 00:01:41,565
is the ability to find something.

43
00:01:41,565 --> 00:01:44,030
Technically defined, it's how often you could

44
00:01:44,030 --> 00:01:46,660
actually reject the null.

45
00:01:46,660 --> 00:01:48,590
So, if you're conducting

46
00:01:48,590 --> 00:01:51,275
a research study and you have 80 percent power,

47
00:01:51,275 --> 00:01:52,820
what we're saying is, you've got

48
00:01:52,820 --> 00:01:54,200
an 80 percent shot at

49
00:01:54,200 --> 00:01:56,285
rejecting the null hypothesis there.

50
00:01:56,285 --> 00:01:59,390
That's good. You've set yourself up to be successful.

51
00:01:59,390 --> 00:02:01,750
Or, if you want 90 percent power,

52
00:02:01,750 --> 00:02:03,515
then you would actually be able to reject

53
00:02:03,515 --> 00:02:06,110
the null hypothesis 90 percent of the time.

54
00:02:06,110 --> 00:02:08,330
Remember, we need to reject the null hypothesis.

55
00:02:08,330 --> 00:02:10,640
I need to know that the results weren't due to chance.

56
00:02:10,640 --> 00:02:13,655
I need a big enough sample to be able to do that.

57
00:02:13,655 --> 00:02:16,225
I need enough power to be able to do that.

58
00:02:16,225 --> 00:02:18,755
But imagine the following situation.

59
00:02:18,755 --> 00:02:21,510
What if you've got a study with only 20 percent power?

60
00:02:21,510 --> 00:02:23,950
You think that wouldn't happen, but it actually does.

61
00:02:23,950 --> 00:02:25,385
It happens quite a bit.

62
00:02:25,385 --> 00:02:28,115
A study with 20 percent power would mean that,

63
00:02:28,115 --> 00:02:29,265
even if you were right,

64
00:02:29,265 --> 00:02:30,860
even if your theory was correct,

65
00:02:30,860 --> 00:02:33,305
even if there were real differences or trends,

66
00:02:33,305 --> 00:02:37,050
you would not notice them 80 percent of the time.

67
00:02:37,050 --> 00:02:38,390
You would walk away thinking,

68
00:02:38,390 --> 00:02:40,655
wow, there's no difference here.

69
00:02:40,655 --> 00:02:43,000
I can't even reject the null hypothesis.

70
00:02:43,000 --> 00:02:46,400
80 percent of the time failure rate

71
00:02:46,400 --> 00:02:47,430
is just not acceptable.

72
00:02:47,430 --> 00:02:49,080
You're setting yourself up to fail,

73
00:02:49,080 --> 00:02:51,530
but yet this happens all the time.

74
00:02:51,530 --> 00:02:53,960
This happens because people don't take the time

75
00:02:53,960 --> 00:02:56,510
ahead of time to calculate their power.

76
00:02:56,510 --> 00:02:59,000
This is a totally preventable situation.

77
00:02:59,000 --> 00:03:01,070
All we need to do is determine

78
00:03:01,070 --> 00:03:02,270
our power ahead of time and take

79
00:03:02,270 --> 00:03:03,725
some steps to have good power.

80
00:03:03,725 --> 00:03:05,090
That's what I want to teach you in

81
00:03:05,090 --> 00:03:07,580
this lesson and the next couple of lessons.

82
00:03:07,580 --> 00:03:09,760
So, what does power depend on?

83
00:03:09,760 --> 00:03:12,170
What do you need to have good power?

84
00:03:12,170 --> 00:03:14,825
Well, power depends really on two things.

85
00:03:14,825 --> 00:03:17,125
The first is effect size.

86
00:03:17,125 --> 00:03:19,215
How do you get a big effect?

87
00:03:19,215 --> 00:03:22,850
It's just a large difference or a steep trend in a graph.

88
00:03:22,850 --> 00:03:26,420
It is literally just a large magnitude effect.

89
00:03:26,420 --> 00:03:29,360
So, if you've got a large magnitude effect,

90
00:03:29,360 --> 00:03:30,905
it's easy to detect.

91
00:03:30,905 --> 00:03:33,400
Think about having a conversation with somebody.

92
00:03:33,400 --> 00:03:36,020
It's easier to hear them if they yell at you than

93
00:03:36,020 --> 00:03:38,695
if they whisper to you.

94
00:03:38,695 --> 00:03:41,345
You can hear large effects more easily.

95
00:03:41,345 --> 00:03:43,750
So that's pretty intuitive, right?

96
00:03:43,750 --> 00:03:45,170
We'll have low power if we've

97
00:03:45,170 --> 00:03:46,640
got a really nuanced finding.

98
00:03:46,640 --> 00:03:48,510
We're going to have high power if we've got

99
00:03:48,510 --> 00:03:51,440
a really large or pronounced finding.

100
00:03:51,440 --> 00:03:53,210
So what do you do then if you

101
00:03:53,210 --> 00:03:54,500
are studying something really nuance?

102
00:03:54,500 --> 00:03:55,550
What do you do if there's

103
00:03:55,550 --> 00:03:57,605
a really small but important trend

104
00:03:57,605 --> 00:03:59,280
you want to be able to detect?

105
00:03:59,280 --> 00:04:02,990
For example, many of us have heard to take

106
00:04:02,990 --> 00:04:04,560
aspirin if we're having

107
00:04:04,560 --> 00:04:06,530
a heart attack or a potential heart attack risk,

108
00:04:06,530 --> 00:04:07,580
but the effect of aspirin on

109
00:04:07,580 --> 00:04:09,200
heart attacks tends to be very small,

110
00:04:09,200 --> 00:04:10,775
yet it's very important.

111
00:04:10,775 --> 00:04:12,800
A lot of things are very small,

112
00:04:12,800 --> 00:04:13,835
but still very important.

113
00:04:13,835 --> 00:04:16,205
So what do we do if we want to study those things?

114
00:04:16,205 --> 00:04:17,780
Well, the answer is you just

115
00:04:17,780 --> 00:04:19,665
compensate with a large sample.

116
00:04:19,665 --> 00:04:23,675
A large sample has less random chance to it,

117
00:04:23,675 --> 00:04:25,445
so less sampling error.

118
00:04:25,445 --> 00:04:26,920
So it gives you more power.

119
00:04:26,920 --> 00:04:29,690
Your study is less influenced by random chance,

120
00:04:29,690 --> 00:04:32,915
and therefore you're better able to detect the effect.

121
00:04:32,915 --> 00:04:34,430
So this kind of makes sense.

122
00:04:34,430 --> 00:04:36,110
We just think about the smallest effect

123
00:04:36,110 --> 00:04:37,695
we want to be able to find,

124
00:04:37,695 --> 00:04:40,100
and then we just figure out what sample size I

125
00:04:40,100 --> 00:04:42,335
need to be able to find that effect.

126
00:04:42,335 --> 00:04:46,230
This gives us two common research situations.

127
00:04:46,300 --> 00:04:49,820
The first is where you walk in and you say,

128
00:04:49,820 --> 00:04:51,990
"I want to set my power to an acceptable level."

129
00:04:51,990 --> 00:04:54,710
Usually, most researchers will say 80 percent.

130
00:04:54,710 --> 00:04:56,210
I really am not

131
00:04:56,210 --> 00:04:58,595
satisfied with anything less than 80 percent power.

132
00:04:58,595 --> 00:05:01,025
I'm going to take the time and energy to run a study.

133
00:05:01,025 --> 00:05:03,535
I'm going to set power to 80 percent.

134
00:05:03,535 --> 00:05:08,405
Then, I identify the smallest effect that I care to find.

135
00:05:08,405 --> 00:05:11,090
I know that the more smaller nuanced effect

136
00:05:11,090 --> 00:05:12,230
I going to go looking,

137
00:05:12,230 --> 00:05:14,270
for the bigger sample I'm going to need.

138
00:05:14,270 --> 00:05:17,030
But essentially, if I tell my power tools,

139
00:05:17,030 --> 00:05:18,840
which you'll get a chance to play within the labs,

140
00:05:18,840 --> 00:05:20,760
if I tell my power tools,

141
00:05:20,760 --> 00:05:22,490
I want to be able to detect an effect as

142
00:05:22,490 --> 00:05:24,595
small as whatever it is,

143
00:05:24,595 --> 00:05:28,275
that's power tool will tell you the sample size needed.

144
00:05:28,275 --> 00:05:30,800
So, if I'm wanting to find what percentage of

145
00:05:30,800 --> 00:05:34,130
customers are interested in trying a product,

146
00:05:34,130 --> 00:05:35,450
I can actually enter the

147
00:05:35,450 --> 00:05:37,200
smallest percentage I care to be able to

148
00:05:37,200 --> 00:05:39,350
detect with 80 percent power

149
00:05:39,350 --> 00:05:41,810
and it will actually tell you the sample size needed.

150
00:05:41,810 --> 00:05:43,475
There is no guesswork involved.

151
00:05:43,475 --> 00:05:46,160
It will tell you that, which is fantastic.

152
00:05:46,160 --> 00:05:48,425
We could also imagine a situation

153
00:05:48,425 --> 00:05:50,340
kind of flipped around, where you say,

154
00:05:50,340 --> 00:05:54,130
"I need 80 percent power and my sample size is fixed.

155
00:05:54,130 --> 00:05:55,310
I don't really have control over this.

156
00:05:55,310 --> 00:05:57,010
I might have some preexisting data,

157
00:05:57,010 --> 00:05:59,255
with say 100 participants in it.

158
00:05:59,255 --> 00:06:01,820
Or I might have limited resources and only be able

159
00:06:01,820 --> 00:06:04,470
to detect or collect a sample of a certain size."

160
00:06:04,470 --> 00:06:06,440
That's fine too. That's just

161
00:06:06,440 --> 00:06:08,645
going to fix the smallest effect size.

162
00:06:08,645 --> 00:06:10,185
So, what this is going to do is

163
00:06:10,185 --> 00:06:12,535
your power tool will tell you, "Okay,

164
00:06:12,535 --> 00:06:14,195
with this sample and that power,

165
00:06:14,195 --> 00:06:16,310
you can only detect effects as small

166
00:06:16,310 --> 00:06:19,440
as a certain number."

167
00:06:19,440 --> 00:06:22,605
So really, if you know two of them,

168
00:06:22,605 --> 00:06:24,785
the power desired and

169
00:06:24,785 --> 00:06:27,435
either the sample size or effect size,

170
00:06:27,435 --> 00:06:29,250
the third is fixed for you.

171
00:06:29,250 --> 00:06:31,710
The power calculators will do this work for you.

172
00:06:31,710 --> 00:06:33,679
It's not really math you could do easily,

173
00:06:33,679 --> 00:06:35,545
but you do always need to attend

174
00:06:35,545 --> 00:06:37,625
the balance between these two forces.

175
00:06:37,625 --> 00:06:39,500
You're either going to be looking for,

176
00:06:39,500 --> 00:06:41,360
a large effect with a small sample

177
00:06:41,360 --> 00:06:43,450
or a small effect with a large sample.

178
00:06:43,450 --> 00:06:45,920
If you don't have one of those two things large,

179
00:06:45,920 --> 00:06:47,765
you're going to have weak power

180
00:06:47,765 --> 00:06:50,250
and that's going to set you up to fail.

