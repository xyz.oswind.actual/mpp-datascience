0
00:00:01,720 --> 00:00:04,150
>> Welcome back. In this lesson,

1
00:00:04,150 --> 00:00:07,900
I want to talk about a more technically precise way to

2
00:00:07,900 --> 00:00:10,060
estimate the sample size you need

3
00:00:10,060 --> 00:00:12,730
to have good power to answer your questions.

4
00:00:12,730 --> 00:00:15,280
Just to reiterate your intuition

5
00:00:15,280 --> 00:00:17,920
about sample size is probably wrong,

6
00:00:17,920 --> 00:00:19,750
to have enough participants,

7
00:00:19,750 --> 00:00:21,260
to have good power,

8
00:00:21,260 --> 00:00:24,370
you need exponentially larger samples

9
00:00:24,370 --> 00:00:26,195
to detect smaller effects.

10
00:00:26,195 --> 00:00:27,445
The smaller the effect,

11
00:00:27,445 --> 00:00:28,750
the larger the sample.

12
00:00:28,750 --> 00:00:30,870
But that relationship is not linear.

13
00:00:30,870 --> 00:00:32,855
Without some calculator or

14
00:00:32,855 --> 00:00:35,210
tool to tell you the sample size you need,

15
00:00:35,210 --> 00:00:36,910
you're going to have a hard time

16
00:00:36,910 --> 00:00:39,070
intuitively assessing that.

17
00:00:39,070 --> 00:00:41,050
So really, you're going to want to use

18
00:00:41,050 --> 00:00:44,550
some power calculator or power tool.

19
00:00:44,550 --> 00:00:45,970
So, in this lesson,

20
00:00:45,970 --> 00:00:48,355
I'm going to show you a couple of ways of doing that.

21
00:00:48,355 --> 00:00:50,500
The first way is with a power table.

22
00:00:50,500 --> 00:00:52,125
This is a way of eyeballing this.

23
00:00:52,125 --> 00:00:54,100
You can make power tables easily,

24
00:00:54,100 --> 00:00:56,110
in fact we'll be doing those in the labs.

25
00:00:56,110 --> 00:00:58,150
But in addition to that,

26
00:00:58,150 --> 00:01:00,640
you can also just use a power calculator.

27
00:01:00,640 --> 00:01:03,100
So, I'm going to walk through the logic of

28
00:01:03,100 --> 00:01:04,480
both of those and we'll start by showing

29
00:01:04,480 --> 00:01:06,130
you a power table.

30
00:01:06,130 --> 00:01:08,205
So, on the next screen here,

31
00:01:08,205 --> 00:01:09,730
I'm going to show you a power table and

32
00:01:09,730 --> 00:01:11,230
we'll be able to actually use it to

33
00:01:11,230 --> 00:01:13,060
determine the sample size

34
00:01:13,060 --> 00:01:16,010
needed to detect different kinds of effects.

35
00:01:16,010 --> 00:01:18,480
So here you see a chart,

36
00:01:18,480 --> 00:01:21,460
and this chart has many different colored lines on it.

37
00:01:21,460 --> 00:01:23,940
So on the x axis is the sample size.

38
00:01:23,940 --> 00:01:25,675
This is for comparing two groups.

39
00:01:25,675 --> 00:01:27,900
So this would be for example like

40
00:01:27,900 --> 00:01:29,770
the data science course or

41
00:01:29,770 --> 00:01:31,870
the online course that we were just showing you.

42
00:01:31,870 --> 00:01:33,765
We have two groups that we want to compare,

43
00:01:33,765 --> 00:01:35,020
we want to know if they're different.

44
00:01:35,020 --> 00:01:38,555
So, on the x-axis is the sample size per group,

45
00:01:38,555 --> 00:01:40,360
on the y-axis is the power.

46
00:01:40,360 --> 00:01:45,340
So you'll see there that if you had 60 people per group,

47
00:01:45,340 --> 00:01:47,800
about a third of the way over from the left,

48
00:01:47,800 --> 00:01:49,645
and then you scroll up,

49
00:01:49,645 --> 00:01:52,265
you see that to get 80 percent power,

50
00:01:52,265 --> 00:01:56,605
you need to be at that yellow line or that green line.

51
00:01:56,605 --> 00:01:59,230
These different lines represent different effects.

52
00:01:59,230 --> 00:02:01,180
So, for instance, that green line

53
00:02:01,180 --> 00:02:03,840
represents in effect size of.6.

54
00:02:03,840 --> 00:02:06,000
And you can easily see here that

55
00:02:06,000 --> 00:02:08,830
that effect size of.6 is going to be

56
00:02:08,830 --> 00:02:14,290
detectable when you have about 50 people per group.

57
00:02:14,290 --> 00:02:18,325
However, if you were having a different situation,

58
00:02:18,325 --> 00:02:20,140
say, you had an effect size of.4,

59
00:02:20,140 --> 00:02:21,375
that's that yellow line,

60
00:02:21,375 --> 00:02:23,985
to get that up to 80 percent power,

61
00:02:23,985 --> 00:02:25,270
you would actually need to move a

62
00:02:25,270 --> 00:02:26,410
little further to the right,

63
00:02:26,410 --> 00:02:30,570
have a sample size of about 110.

64
00:02:30,570 --> 00:02:33,790
So, this chart is actually really helpful.

65
00:02:33,790 --> 00:02:35,455
If you want to compare two groups,

66
00:02:35,455 --> 00:02:37,720
you need to get up to 80 percent power.

67
00:02:37,720 --> 00:02:38,960
Depending on your effect size,

68
00:02:38,960 --> 00:02:40,685
you can do that with a small sample,

69
00:02:40,685 --> 00:02:42,940
as you see for some of those colored lines.

70
00:02:42,940 --> 00:02:45,590
Or if you have a different effect size,

71
00:02:45,590 --> 00:02:47,540
if you're studying something really small and nuanced,

72
00:02:47,540 --> 00:02:48,740
you need a bigger sample.

73
00:02:48,740 --> 00:02:50,390
That would be one of those more

74
00:02:50,390 --> 00:02:52,840
orange or red colored line situations.

75
00:02:52,840 --> 00:02:55,130
So you can easily use a chart like this to

76
00:02:55,130 --> 00:02:57,995
find the sample size you need per group.

77
00:02:57,995 --> 00:03:00,650
This is also useful if you're reading research

78
00:03:00,650 --> 00:03:01,790
or if somebody has given you

79
00:03:01,790 --> 00:03:03,150
a finding and you want to know,

80
00:03:03,150 --> 00:03:05,095
"Hey, was their power any good?

81
00:03:05,095 --> 00:03:07,360
Was this study properly powered?"

82
00:03:07,360 --> 00:03:10,860
You can easily look at the effects size and ask "Well,

83
00:03:10,860 --> 00:03:14,120
did they have good power in this study?"

84
00:03:14,120 --> 00:03:17,435
If they didn't, that might raise some suspicions for you.

85
00:03:17,435 --> 00:03:19,630
So as we've seen,

86
00:03:19,630 --> 00:03:21,920
we have some charts and tools that we can

87
00:03:21,920 --> 00:03:24,455
use to estimate the sample size needed.

88
00:03:24,455 --> 00:03:26,000
We're going to get a chance to play with

89
00:03:26,000 --> 00:03:27,680
those charts more in the labs.

90
00:03:27,680 --> 00:03:30,395
We'll even have the ability to make them.

91
00:03:30,395 --> 00:03:31,940
However, it's useful to

92
00:03:31,940 --> 00:03:34,060
know that there are other ways to do it.

93
00:03:34,060 --> 00:03:37,430
For example, as long as we know that

94
00:03:37,430 --> 00:03:38,760
large effects are detectable with

95
00:03:38,760 --> 00:03:40,065
small samples and

96
00:03:40,065 --> 00:03:41,690
small effects are detect with large samples,

97
00:03:41,690 --> 00:03:44,170
there are some rules of thumb you can use.

98
00:03:44,170 --> 00:03:46,200
If you really want a rule of thumb,

99
00:03:46,200 --> 00:03:48,320
you don't want to do a power calculation.

100
00:03:48,320 --> 00:03:51,575
A general rule being, if you've got small samples,

101
00:03:51,575 --> 00:03:54,140
four to five hundred people is a pretty safe bet.

102
00:03:54,140 --> 00:03:56,000
It's very rare that you're going to study

103
00:03:56,000 --> 00:03:57,800
something that needs more than that.

104
00:03:57,800 --> 00:04:00,230
So, if you're getting an existing data set,

105
00:04:00,230 --> 00:04:02,040
you might consider just asking,

106
00:04:02,040 --> 00:04:04,530
"Do I have a minimum of four to five hundred people?"

107
00:04:04,530 --> 00:04:06,270
That might seem like a large sample,

108
00:04:06,270 --> 00:04:08,060
but many data sets you might be

109
00:04:08,060 --> 00:04:10,570
getting in a data science or analytics context,

110
00:04:10,570 --> 00:04:12,180
might be thousands, tens of thousands,

111
00:04:12,180 --> 00:04:13,695
or even millions of data points,

112
00:04:13,695 --> 00:04:16,280
which point power is really not a concern.

113
00:04:16,280 --> 00:04:18,260
This really becomes an issue when you start

114
00:04:18,260 --> 00:04:21,255
having somewhere under this threshold.

115
00:04:21,255 --> 00:04:23,210
At this point, you

116
00:04:23,210 --> 00:04:24,965
might consider using a power calculator.

117
00:04:24,965 --> 00:04:26,600
Now one caveat I will have,

118
00:04:26,600 --> 00:04:27,825
when you're comparing groups,

119
00:04:27,825 --> 00:04:29,840
all sample size estimating tools

120
00:04:29,840 --> 00:04:32,090
do tell you the size needed per group.

121
00:04:32,090 --> 00:04:33,610
So in that previous slide,

122
00:04:33,610 --> 00:04:35,420
that would be the number of people

123
00:04:35,420 --> 00:04:37,745
you need per group to do that comparison.

124
00:04:37,745 --> 00:04:39,650
Of course, I'm going to advocate for

125
00:04:39,650 --> 00:04:41,720
always just using a power calculator.

126
00:04:41,720 --> 00:04:44,080
There's plenty of these. There's packages

127
00:04:44,080 --> 00:04:45,650
for R in Python that will do this.

128
00:04:45,650 --> 00:04:47,850
There are free software tools you can do.

129
00:04:47,850 --> 00:04:49,310
There are even websites you can visit

130
00:04:49,310 --> 00:04:51,005
that will do these calculations for you.

131
00:04:51,005 --> 00:04:53,515
Simply put, you're going to enter your desired power,

132
00:04:53,515 --> 00:04:55,225
I recommend at least 80 percent,

133
00:04:55,225 --> 00:04:57,035
because you do not want to be set up to fail.

134
00:04:57,035 --> 00:04:58,925
They'll tell it the analysis you want to use

135
00:04:58,925 --> 00:05:00,990
and then you can enter in

136
00:05:00,990 --> 00:05:03,200
the smallest effect size you care about and it will

137
00:05:03,200 --> 00:05:06,570
tell you the sample size required.

138
00:05:06,760 --> 00:05:09,065
In conclusion,

139
00:05:09,065 --> 00:05:11,130
power is something you have to worry about.

140
00:05:11,130 --> 00:05:13,215
You should beware of small samples.

141
00:05:13,215 --> 00:05:14,819
For me I would advocate

142
00:05:14,819 --> 00:05:15,930
small samples anything less

143
00:05:15,930 --> 00:05:17,110
than about four or five hundred people,

144
00:05:17,110 --> 00:05:18,805
you should really worry about power.

145
00:05:18,805 --> 00:05:21,195
In those situations, check your power.

146
00:05:21,195 --> 00:05:24,000
Know what sample size

147
00:05:24,000 --> 00:05:25,830
you would need to make different kinds of claims.

148
00:05:25,830 --> 00:05:27,090
If you want to make claims about

149
00:05:27,090 --> 00:05:29,040
a very small or nuanced finding,

150
00:05:29,040 --> 00:05:31,290
you need a commensurately large sample

151
00:05:31,290 --> 00:05:32,765
to be able to detect it.

152
00:05:32,765 --> 00:05:34,830
If you don't have that large sample,

153
00:05:34,830 --> 00:05:36,420
I'm going to be suspicious about

154
00:05:36,420 --> 00:05:38,580
any claims you make and you should too.

155
00:05:38,580 --> 00:05:40,380
You should take that with a grain of salt.

156
00:05:40,380 --> 00:05:41,935
Because as we've seen now,

157
00:05:41,935 --> 00:05:44,325
several different times and several different ways.

158
00:05:44,325 --> 00:05:46,440
Well, if you've got a very large effect,

159
00:05:46,440 --> 00:05:47,920
you can get away with a small sample.

160
00:05:47,920 --> 00:05:50,860
But if you don't, you should be skeptical.

