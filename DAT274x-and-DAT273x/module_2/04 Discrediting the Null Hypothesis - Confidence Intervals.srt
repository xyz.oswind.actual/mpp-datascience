0
00:00:01,730 --> 00:00:04,420
>> Welcome back. In this lesson,

1
00:00:04,420 --> 00:00:05,800
I want to build on what we talked

2
00:00:05,800 --> 00:00:07,930
about rejecting the null hypothesis with

3
00:00:07,930 --> 00:00:09,460
P-values and give you

4
00:00:09,460 --> 00:00:13,310
a more informative and useful tool than the P-value.

5
00:00:13,310 --> 00:00:15,340
We said in the previous lesson

6
00:00:15,340 --> 00:00:16,550
that we can declare a result

7
00:00:16,550 --> 00:00:19,385
significant if the P-value is low.

8
00:00:19,385 --> 00:00:21,415
That is, you really couldn't get that result

9
00:00:21,415 --> 00:00:23,470
easily by chance or the result is,

10
00:00:23,470 --> 00:00:25,885
you could conclude probably not due to chance.

11
00:00:25,885 --> 00:00:28,010
That's good, that's great,

12
00:00:28,010 --> 00:00:29,760
but there's a slight problem with it,

13
00:00:29,760 --> 00:00:31,175
you're telling me what the result

14
00:00:31,175 --> 00:00:33,540
is not, not due to chance.

15
00:00:33,540 --> 00:00:35,680
You're not really telling me much about what

16
00:00:35,680 --> 00:00:38,080
you estimate the result is.

17
00:00:38,080 --> 00:00:41,835
I think we can say more and I think we should say more.

18
00:00:41,835 --> 00:00:44,005
P-values are a great place to start.

19
00:00:44,005 --> 00:00:46,070
We can discredit the no hypothesis.

20
00:00:46,070 --> 00:00:47,535
Chances are a poor explanation,

21
00:00:47,535 --> 00:00:48,880
low P-value, but I want to

22
00:00:48,880 --> 00:00:50,320
say a little bit more than that.

23
00:00:50,320 --> 00:00:51,910
I want to be able to give my estimate

24
00:00:51,910 --> 00:00:54,325
some degree of confidence or precision.

25
00:00:54,325 --> 00:00:57,745
We do that with a confidence interval.

26
00:00:57,745 --> 00:00:59,780
So, a confidence interval,

27
00:00:59,780 --> 00:01:02,070
we sometimes call it a 95% confidence

28
00:01:02,070 --> 00:01:04,670
interval or a 95% CI.

29
00:01:04,670 --> 00:01:07,265
This is a fantastic and useful tool

30
00:01:07,265 --> 00:01:09,500
because it doesn't tell you what the result is not,

31
00:01:09,500 --> 00:01:11,150
it tells you what the result is.

32
00:01:11,150 --> 00:01:13,265
So let's go ahead and look at this example data

33
00:01:13,265 --> 00:01:15,605
that I've been looking at here with core satisfaction.

34
00:01:15,605 --> 00:01:17,370
So in the sample here,

35
00:01:17,370 --> 00:01:19,070
I see a difference of

36
00:01:19,070 --> 00:01:21,560
about 0.84 points and if I back up here,

37
00:01:21,560 --> 00:01:23,440
you can see that on the screen here that

38
00:01:23,440 --> 00:01:26,630
the averages of the two groups

39
00:01:26,630 --> 00:01:30,400
is about a point apart, about 0.84 points.

40
00:01:30,620 --> 00:01:33,580
The null hypothesis would say there's no difference,

41
00:01:33,580 --> 00:01:34,730
it's just a coincidence.

42
00:01:34,730 --> 00:01:36,670
So, we can run it up P-value and

43
00:01:36,670 --> 00:01:38,790
test that in the population level.

44
00:01:38,790 --> 00:01:41,260
The null hypothesis would say there's no difference but

45
00:01:41,260 --> 00:01:43,980
my P-value would say no,

46
00:01:43,980 --> 00:01:45,130
it's significant and that's

47
00:01:45,130 --> 00:01:46,600
really all you can really say, right?

48
00:01:46,600 --> 00:01:50,000
With your P-value, you're saying difference is not zero,

49
00:01:50,000 --> 00:01:53,440
not due to chance. We can say more.

50
00:01:53,440 --> 00:01:55,810
What a confidence interval does is it

51
00:01:55,810 --> 00:01:58,255
actually gives you a range to estimate.

52
00:01:58,255 --> 00:02:01,380
So, my sample said it was 0.84.

53
00:02:01,380 --> 00:02:04,670
I know my sample is off because it's just due to chance.

54
00:02:04,670 --> 00:02:06,385
It's just a sample, it's just an estimate.

55
00:02:06,385 --> 00:02:09,260
But the confidence interval gives you a ranged estimate.

56
00:02:09,260 --> 00:02:10,960
So, I could say I am 95 percent

57
00:02:10,960 --> 00:02:13,030
confident that the true difference is

58
00:02:13,030 --> 00:02:17,445
somewhere between 0.48 and 1.21.

59
00:02:17,445 --> 00:02:19,515
Where did that range estimate come from?

60
00:02:19,515 --> 00:02:21,340
Well, I just take a few pieces of

61
00:02:21,340 --> 00:02:22,735
information from my sample

62
00:02:22,735 --> 00:02:24,130
and plug them into an equation.

63
00:02:24,130 --> 00:02:26,050
Those few pieces of information include

64
00:02:26,050 --> 00:02:28,300
things like the sample size, the bigger the sample,

65
00:02:28,300 --> 00:02:29,945
the more confident you are in the result,

66
00:02:29,945 --> 00:02:32,270
the more precise your window is,

67
00:02:32,270 --> 00:02:34,690
and a few other things along those lines.

68
00:02:34,690 --> 00:02:35,845
I'm not going to walk through

69
00:02:35,845 --> 00:02:37,270
the mathematical equation that

70
00:02:37,270 --> 00:02:39,880
generates the confidence interval and in your career,

71
00:02:39,880 --> 00:02:42,070
you will rarely need the actual equation,

72
00:02:42,070 --> 00:02:43,570
almost every data science and

73
00:02:43,570 --> 00:02:46,420
statistical analysis tool will calculate them for you.

74
00:02:46,420 --> 00:02:48,520
But what you should know is that this

75
00:02:48,520 --> 00:02:52,685
essentially answers your P-value question and then sum.

76
00:02:52,685 --> 00:02:55,530
After all, the null hypotheses says effect is absent,

77
00:02:55,530 --> 00:02:57,185
there is no difference, it's zero.

78
00:02:57,185 --> 00:02:58,620
With that confidence interval,

79
00:02:58,620 --> 00:03:01,060
you can say not only do I know it's not zero,

80
00:03:01,060 --> 00:03:04,490
I'm pretty confident it's between 0.48 and 1.21.

81
00:03:04,490 --> 00:03:08,200
I've got a window that both illustrates what I

82
00:03:08,200 --> 00:03:11,815
think the difference is and exemplifies my uncertainty.

83
00:03:11,815 --> 00:03:13,720
I can actually see how certain or

84
00:03:13,720 --> 00:03:17,120
confident I am in my sample estimate.

85
00:03:17,120 --> 00:03:20,740
So, whenever I do a study and I get some result,

86
00:03:20,740 --> 00:03:22,300
I like to give a confidence interval

87
00:03:22,300 --> 00:03:23,710
because it's going to tell me,

88
00:03:23,710 --> 00:03:25,600
"I'm pretty sure that the true value is

89
00:03:25,600 --> 00:03:28,090
somewhere between here and there."

90
00:03:28,090 --> 00:03:30,430
I can reject the null hypothesis if it's not zero,

91
00:03:30,430 --> 00:03:31,630
but I can say a whole lot

92
00:03:31,630 --> 00:03:33,710
more when I've got that ranged estimate.

93
00:03:33,710 --> 00:03:37,480
It also keeps us from having a mental bias where I look

94
00:03:37,480 --> 00:03:39,159
at that sample difference

95
00:03:39,159 --> 00:03:41,125
and I just automatically trust it.

96
00:03:41,125 --> 00:03:42,700
It's easy to accidentally

97
00:03:42,700 --> 00:03:44,335
forget you're working with the sample.

98
00:03:44,335 --> 00:03:47,220
It's easy to accidentally forget and think wow,

99
00:03:47,220 --> 00:03:49,115
that's a difference appointing for.

100
00:03:49,115 --> 00:03:51,300
No, you just got a sample.

101
00:03:51,300 --> 00:03:53,175
But, with a confidence interval,

102
00:03:53,175 --> 00:03:57,260
you can more easily see the estimate,

103
00:03:57,260 --> 00:03:58,925
you can actually pull from the data,

104
00:03:58,925 --> 00:04:00,445
somewhere between about a half a point

105
00:04:00,445 --> 00:04:02,560
and about one and a quarter point difference.

106
00:04:02,560 --> 00:04:04,700
That's still a reasonably large difference.

107
00:04:04,700 --> 00:04:05,830
It's a difference I might care

108
00:04:05,830 --> 00:04:08,550
about from its worst case to best case scenario.

109
00:04:08,550 --> 00:04:11,505
So, a more useful tool than the P-value.

110
00:04:11,505 --> 00:04:12,885
P-values are great,

111
00:04:12,885 --> 00:04:14,020
confidence intervals will give you

112
00:04:14,020 --> 00:04:16,260
that information and more.

