Starter Experiments
================

### Exercise 1:

[Lab 4 - Optimized Classification -
R](https://gallery.azure.ai/Experiment/DAT203-2x-Lab-4-Optimized-Classification-R-2)

[Lab 4 - Optimized Classification -
Python](https://gallery.azure.ai/Experiment/DAT203-2x-Lab-4-Optimized-Classification-Python-2)

### Exercise 2:

[Lab 4 - Regression -
R](https://gallery.azure.ai/Experiment/DAT203-2x-Lab-4-Regression-R-1)

[Lab 4 - Regression -
Python](https://gallery.azure.ai/Experiment/DAT203-2x-Lab4-Regression-Python-1)
