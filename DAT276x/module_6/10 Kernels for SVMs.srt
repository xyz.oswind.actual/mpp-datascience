0
00:00:05,080 --> 00:00:11,190
Now the major reason why support vector machines became so popular in the mid-nineties is because

1
00:00:11,190 --> 00:00:15,710
of a trick that I am about to tell you. And this trick allows support vector machines

2
00:00:15,710 --> 00:00:23,180
to effortlessly go from linear models to non-linear models by using only a slightly different

3
00:00:23,180 --> 00:00:28,189
optimization problem that the one we just saw. And this is called the kernel trick.

4
00:00:28,189 --> 00:00:32,980
The kernel trick allows the support vector machines to map all of the points to a high

5
00:00:32,980 --> 00:00:38,720
dimensional space, where the points are more easily separated. So here is the original

6
00:00:38,720 --> 00:00:43,680
space of features that you are working in ad you have this magic mapping phi that takes

7
00:00:43,680 --> 00:00:48,979
you a high dimensional space, where you can see perfectly separated positives and negatives.

8
00:00:48,979 --> 00:00:53,839
And when you map down to the original space, you have a non-linear boundary that you have

9
00:00:53,839 --> 00:00:59,790
got from the high dimensional space. And it perfectly separates the data. But this picture

10
00:00:59,790 --> 00:01:05,080
is actually a little bit misleading since you never actually see this function phi most

11
00:01:05,080 --> 00:01:09,200
of the time. So let’s do this with an example. Let’s

12
00:01:09,200 --> 00:01:14,800
say that we are trying to estimate the sales price of a house. So this is house I and these

13
00:01:14,800 --> 00:01:21,900
are things you might measure: the house’s list price. Early price estimate, the length

14
00:01:21,900 --> 00:01:27,550
of time it’s been in the market, the average price of a house in the neighborhood. So let’s

15
00:01:27,550 --> 00:01:30,790
just look at all these things but let’s just work with two of them for simplicity

16
00:01:30,790 --> 00:01:34,500
and I’ll tell you how to deal with a more general case later. So let’s just stick

17
00:01:34,500 --> 00:01:43,430
to two features. And now let me compute what’s called the inner product of the two observations

18
00:01:43,430 --> 00:01:49,370
called I and k. So the inner product is just multiplying the first two components together

19
00:01:49,370 --> 00:01:55,140
and then multiplying the second two components together and then adding them up. So inner

20
00:01:55,140 --> 00:02:00,580
products are a kind of way to think about distance in space. So here we are thinking

21
00:02:00,580 --> 00:02:08,060
about distances ion 2-dimensional space. So what would happen if we play this game where

22
00:02:08,060 --> 00:02:15,020
we map x to this new space. And then we will compute the inner product in that space instead.

23
00:02:15,020 --> 00:02:23,230
So instead of looking at this regular vector that we started with, you know the xi1 and

24
00:02:23,230 --> 00:02:30,290
xi2for house I, what we look at is xi12 and xi22 and we will also look at the two of them

25
00:02:30,290 --> 00:02:35,920
multiplied together. And that’s three numbers instead of two. So we are actually working

26
00:02:35,920 --> 00:02:42,090
in 3 dimensions instead of 2 dimensions. So let’s do the same thing now for house

27
00:02:42,090 --> 00:02:48,760
k. So both houses are represented by 3 numbers. And now we are really working in three dimensions.

28
00:02:48,760 --> 00:02:53,280
And now I can compute an inner product in a 3-dimensional space just by multiplying

29
00:02:53,280 --> 00:02:59,819
component-wise, multiplying this by that this by that and adding them all up. So now we

30
00:02:59,819 --> 00:03:04,849
have successfully computed inner products in 3-dimensions, when we started out only

31
00:03:04,849 --> 00:03:13,319
with 2-domensions. What we just did was actually to map things to a higher dimensional space.

32
00:03:13,319 --> 00:03:20,790
So you could decide on the map and anywhere in the optimization problem where there is

33
00:03:20,790 --> 00:03:28,959
a xi you could replace it with phi (xi) and you could solve the new optimization problem.

34
00:03:28,959 --> 00:03:33,069
And this is very simple. You could just call it a different sort of preprocessing if you’d

35
00:03:33,069 --> 00:03:38,680
like. You just turn the xs into phi (xs). So nothing fancy really happened here. But

36
00:03:38,680 --> 00:03:44,209
there is another way to do this that’s actually very clever. So it turns to that there is

37
00:03:44,209 --> 00:03:51,709
a special property of the support vector machine optimization problem in that it only depends

38
00:03:51,709 --> 00:04:00,569
on the xis through inner products. What I mean is that the only terms involving xi in

39
00:04:00,569 --> 00:04:06,980
the optimization problem, look like this: Inner Product (xi, xk). You will never see

40
00:04:06,980 --> 00:04:13,410
xi by itself or in other ways or in any other way in that optimization problem. The only

41
00:04:13,410 --> 00:04:17,940
place you see xi in the optimization problem are within inner products.

42
00:04:17,940 --> 00:04:23,379
I am going to put up the optimization problem that support vector machines itself. The one

43
00:04:23,379 --> 00:04:28,419
that we derived already is called the primal problem. This one that I am putting up right

44
00:04:28,419 --> 00:04:32,669
now is called the dual form of this problem and it is equivalent to the primal problem.

45
00:04:32,669 --> 00:04:38,819
And you see here, where do you see x? No matter ow closely you look at this formation, you

46
00:04:38,819 --> 00:04:44,909
are only going to see the xs I one place, which is right here inside this inner product.

47
00:04:44,909 --> 00:04:51,889
Here’s a way to cheat. Rather than actually writing down what phi is let’s just replace

48
00:04:51,889 --> 00:04:57,650
this inner product in the original space with an inner product in a different space. And

49
00:04:57,650 --> 00:05:02,270
then from this optimization problem’s perspective, you are in the new space, whether you even

50
00:05:02,270 --> 00:05:07,779
bother to compute phi or not at all is totally irrelevant. As long as you compute inner products

51
00:05:07,779 --> 00:05:12,900
in this space, you don’t need to compute phi. Wherever the formulation says to compute

52
00:05:12,900 --> 00:05:17,219
the inner product, you replace it with the inner product in the new space. And the cool

53
00:05:17,219 --> 00:05:21,569
thing is you never need to compute phi at all. You just replace the inner product in

54
00:05:21,569 --> 00:05:25,999
the old space with the inner product in the new space. And if you can compute this inner

55
00:05:25,999 --> 00:05:30,119
product in the new space without computing phi first, you are better off.

56
00:05:30,119 --> 00:05:35,809
Okay so how can I compute the inner product in the new space without computing the fuzz?

57
00:05:35,809 --> 00:05:43,419
Well you can. In fact, we will use our earlier example to demonstrate this. So we started

58
00:05:43,419 --> 00:05:49,539
out in a 2-dimensional space here and then we computed our inner product in a 3-dimensional

59
00:05:49,539 --> 00:05:54,610
space. And now you could well big deal! I can just compute my phi first and get my inner

60
00:05:54,610 --> 00:06:01,159
product. And that’s true, in this case. But in many other cases. You can’t compute

61
00:06:01,159 --> 00:06:08,349
phi at all but you can compute the kernel. You can compute this inner product here, without

62
00:06:08,349 --> 00:06:13,229
computing phi first that is the only way you can do it. And it doesn’t matter that if

63
00:06:13,229 --> 00:06:17,979
the map phi is infinite dimensional, it doesn’t matter if it is impossible to calculate it

64
00:06:17,979 --> 00:06:23,240
as long as you can calculate this thing called the kernel, between the two points, you can

65
00:06:23,240 --> 00:06:28,210
run the optimization formula. So with one click you can tell the support vector machine

66
00:06:28,210 --> 00:06:32,589
which kernel it is should use for its particular calculation and it just calculates kernels

67
00:06:32,589 --> 00:06:35,800
where it is supposed to calculate regular inner products, and whoosh you are in the

68
00:06:35,800 --> 00:06:41,210
new space. Pretty cool huh? Now this is called as I mentioned the kernel

69
00:06:41,210 --> 00:06:48,059
and one particularly useful kernel is the Gaussian kernel. Now the Gaussian distribution

70
00:06:48,059 --> 00:06:54,679
is the normal distribution, this s the case where I can’t exactly calculate phi in my

71
00:06:54,679 --> 00:06:59,869
computer because it is a whole function unless it is infinite dimensional there is an infinite

72
00:06:59,869 --> 00:07:04,729
number of values along this function. You need an infinite number of values to write

73
00:07:04,729 --> 00:07:09,710
out this function so of course you can’t do it on your computer. But you can actually

74
00:07:09,710 --> 00:07:19,749
write out the inner product which is right here. Now this thing I can compute and now

75
00:07:19,749 --> 00:07:27,029
I am doing support vector machine in infinite dimensional space. So when you are using Gaussian

76
00:07:27,029 --> 00:07:34,599
kernels, which are also called RBF kernels or radius basis function, in a way, you are

77
00:07:34,599 --> 00:07:39,869
essentially placing a normal shaped bump at each data point and weighting them and adding

78
00:07:39,869 --> 00:07:47,139
them up. And so there a lot of training observations, we have to add a lot of bumps together rand

79
00:07:47,139 --> 00:07:50,740
some of the bumps could be the right side up and some of them could be upside down,

80
00:07:50,740 --> 00:07:55,219
depending on the solution to the optimization problem.

81
00:07:55,219 --> 00:07:59,939
And when you go to compute the prediction for a new point, you end up computing a weighted

82
00:07:59,939 --> 00:08:05,770
sum of the bumps, at the new point, so wherever the new point is you add up all the bumps

83
00:08:05,770 --> 00:08:14,110
and that tells you wat the predicted value is value of f(x). The weights by the way,

84
00:08:14,110 --> 00:08:20,300
these alphas, they are determined by the solution of the support vector machine’s optimization

85
00:08:20,300 --> 00:08:31,849
problem. And as usual the decision boundary is where some of these bumps is 0. And that

86
00:08:31,849 --> 00:08:39,590
is how support vector machine are able to create beautiful non-linear decision boundaries.

87
00:08:39,590 --> 00:08:47,750
Now you have to be careful not to choose the bandwidth parameter too small. Otherwise it

88
00:08:47,750 --> 00:08:58,440
will overfit. So the bandwidth parameter is just the standard deviation of the Gaussian

89
00:08:58,440 --> 00:09:04,950
distr. If you make it too small the decision boundary will sort of snake its way around

90
00:09:04,950 --> 00:09:10,300
to try to make things separable and then it will overfit. Like over here, I think it is

91
00:09:10,300 --> 00:09:11,840
very badly overfitted.

92
00:09:11,840 --> 00:09:16,140
So the three most common kernels for support vector machines are exactly the ones I showed

93
00:09:16,140 --> 00:09:20,810
you. The linear kernel, which is the same as basically using no kernel and using the

94
00:09:20,810 --> 00:09:25,650
regular in your product, the polynomial kernel, it’s like the example I showed you in the

95
00:09:25,650 --> 00:09:29,260
beginning where you could square things and so on. And you could choose the degree of

96
00:09:29,260 --> 00:09:35,310
the polynomial. Obviously if you choose the degree to be too large, you could overfit

97
00:09:35,310 --> 00:09:41,210
again and cause the model to be very curvy. And then the Gaussian kern el. Like the example

98
00:09:41,210 --> 00:09:46,740
I showed you. There you chose the bandwidth parameter, and remember try not to choose

99
00:09:46,740 --> 00:09:51,780
it too small so you won’t overfit. Or you could do nested cross validation and two net

100
00:09:51,780 --> 00:09:54,650
parameter if you’d like.

101
00:09:54,650 --> 00:10:00,280
So let’s summarize by talking about some of the advantages of support vector machines.

102
00:10:00,280 --> 00:10:07,210
So support vector machined are theoretically very beautiful because of the kernel trick

103
00:10:07,210 --> 00:10:15,770
right? All the nonlinear stuff is precomputed even though when you compute the kernels right?

104
00:10:15,770 --> 00:10:21,490
So even though you are solving a regular mathematical problem, optimization problem to get a linear

105
00:10:21,490 --> 00:10:26,340
model, you can actually get something that is non-linear, which is really neat. It is

106
00:10:26,340 --> 00:10:31,950
based on quadratic programming so this is a type of optimization problem, and there

107
00:10:31,950 --> 00:10:38,400
these specialized solvers that I was telling you about that use that are like coordinate

108
00:10:38,400 --> 00:10:43,390
descent they are just two variable at a time.

109
00:10:43,390 --> 00:10:50,890
So quadratic programs, they are not the easiest program, optimization problems to solve. But

110
00:10:50,890 --> 00:10:55,580
we have these specialize solvers that can do it/ And another nice thing about them is

111
00:10:55,580 --> 00:11:00,500
anyone who runs it will theoretically get the same answer, which is not true for other

112
00:11:00,500 --> 00:11:05,970
methods like neural networks which we will talk about shortly, which is also based on

113
00:11:05,970 --> 00:11:11,940
optimization. And also, support vector machines can easily handle imbalanced data by reweighting

114
00:11:11,940 --> 00:11:19,800
the points. Support vector machines are generally very good, for imbalanced data.

115
00:11:19,800 --> 00:11:26,700
Now disadvantages of support vector machines, well, support vector machines right now is

116
00:11:26,700 --> 00:11:30,880
not among my first choice of things to recommend to people. For the implementations that my

117
00:11:30,880 --> 00:11:35,760
group has been using at least, support vector machine solvers can be on the slow side. And

118
00:11:35,760 --> 00:11:42,990
sometimes even after tuning the kernel parameters, performance can be a little lousy, most of

119
00:11:42,990 --> 00:11:47,260
the times it is not lousy but sometimes it is. And I can’t tell you in advance when

120
00:11:47,260 --> 00:11:54,610
it is going to happen. And when you are thinking about high dimensional dataset, it is not

121
00:11:54,610 --> 00:12:00,840
really clear with what’s going on. Support vector machine models are uninterruptible,

122
00:12:00,840 --> 00:12:07,040
they are not, you know they are not deigned to be interruptible, so they are to. I would

123
00:12:07,040 --> 00:12:11,260
warn you the tracer kernel can be slightly tricky because when you are using support

124
00:12:11,260 --> 00:12:14,400
vector machines, you have several traces you are going to have to make, whether you use

125
00:12:14,400 --> 00:12:18,950
a linear kernel, her you’re going to use a polynomial kernel what degree the polynomial

126
00:12:18,950 --> 00:12:24,640
should be or you could use a Gaussian kernel, what should be the bandwidth of the kernel

127
00:12:24,640 --> 00:12:32,410
parameter. So all these choices make it slightly difficult to tune the thing and get tot to

128
00:12:32,410 --> 00:12:33,279
work really the way you want it.

