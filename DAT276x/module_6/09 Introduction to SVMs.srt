0
00:00:05,190 --> 00:00:09,440
Let me tell you about support vector machines. Support vector machines are really principled,

1
00:00:09,440 --> 00:00:16,200
really elegant sort of machine learning methods, they arose directly out of statistical learning

2
00:00:16,200 --> 00:00:21,940
theory and they use optimization techniques so they are extremely principled and very

3
00:00:21,940 --> 00:00:28,480
elegant and then there&#39;s a check called the kernel check that I&#39;ll show you that allows

4
00:00:28,480 --> 00:00:36,039
you to create these very powerful nonlinear classifiers. Okay support vector machines

5
00:00:36,039 --> 00:00:41,249
started by addressing the question of what decision boundary to pick when all the points

6
00:00:41,249 --> 00:00:46,960
are correctly classified. Okay. So all the points are correctly classified and that decision

7
00:00:46,960 --> 00:00:53,499
boundary looks pretty good right there. But how did I know not to choose that one or this

8
00:00:53,499 --> 00:01:00,370
one. Those all look pretty plausible right? What about that one? This doesn’t seem like

9
00:01:00,370 --> 00:01:06,510
a particularly good idea because the line is so close to the points that it might not

10
00:01:06,510 --> 00:01:12,120
generalize well for new points. So even though all those training points are classified correctly

11
00:01:12,120 --> 00:01:19,760
maybe the test points won’t like that particular decision boundary. So the idea with the support

12
00:01:19,760 --> 00:01:28,360
vector machines is that we look at the margin, we look at how far the points are from that

13
00:01:28,360 --> 00:01:35,950
decision boundary and we want the minimum margin, the smallest distance from that decision

14
00:01:35,950 --> 00:01:42,790
boundary to be large. So the minimum margin is the distance between the decision boundary

15
00:01:42,790 --> 00:01:46,320
and the nearest training observation.

16
00:01:46,320 --> 00:01:49,780
Let&#39;s look at this whole problem from the margin’s perspective which I showed you

17
00:01:49,780 --> 00:01:54,690
earlier. So we’ll move all those point to the other side and this is just a good way

18
00:01:54,690 --> 00:02:02,760
of looking at the margins which are Y times F. And just so you didn&#39;t forget, the minimum

19
00:02:02,760 --> 00:02:09,159
margin is that that distance to the nearest, it is the smallest value of Y times F. Now,

20
00:02:09,158 --> 00:02:16,470
when we say we want the minimum margin to be large right to keep all the points away

21
00:02:16,470 --> 00:02:23,460
from the decision boundary, support vector machines make that very precise. In particular,

22
00:02:23,460 --> 00:02:30,690
they specify that all the points are farther than some number gamma from the decision boundary.

23
00:02:30,690 --> 00:02:39,470
And then when we say we went to margins to be large it means we want gamma to be as large

24
00:02:39,470 --> 00:02:48,590
as possible. I&#39;m making things precise here. We force the margin or point I which Y times

25
00:02:48,590 --> 00:02:56,200
Xi to be at least gamma. Okay that says all points are further than distance gamma from

26
00:02:56,200 --> 00:03:00,660
the decision boundary. And then this we are going to use as a set of constraints for the

27
00:03:00,660 --> 00:03:08,319
optimization problem where the goal is to maximize gamma and then this is this is precisely

28
00:03:08,319 --> 00:03:16,920
the optimization problem actually. To maximize gamma subject to the margins being at least

29
00:03:16,920 --> 00:03:23,740
gamma. So let’s put that up there. That’s the first step but if you think about this

30
00:03:23,740 --> 00:03:30,050
for a while you will figure out that this is an issue because gamma and f here don&#39;t

31
00:03:30,050 --> 00:03:36,099
actually mean anything there&#39;s a scaling issue that we need to deal with. If I just multiply

32
00:03:36,099 --> 00:03:41,400
by f by a million or some other big number, then all of sudden gamma is a million times

33
00:03:41,400 --> 00:03:49,450
bigger. And here&#39;s an example to show you that here you just multiply both sides by

34
00:03:49,450 --> 00:03:55,760
a big number you&#39;ll see that you could have gamma to be arbitrarily large just by multiplying

35
00:03:55,760 --> 00:04:01,819
f by some constant so not good. So how do we deal with this? We’re going to force

36
00:04:01,819 --> 00:04:11,209
gamma to be relative to the size of f. So however big f gets Gama is not going to be

37
00:04:11,209 --> 00:04:19,849
effective because it is not scaled by the size of an f. That&#39;s good but now we still

38
00:04:19,849 --> 00:04:26,900
have another problem. Now the gamma is meaningful but it is still arbitrary which f I pick because

39
00:04:26,900 --> 00:04:33,960
I can pick any f I want and you know it I&#39;ll get I&#39;ll just get this equation to hold no

40
00:04:33,960 --> 00:04:39,220
matter what scaling factor I use for f. I can multiply f with anything I want, and the

41
00:04:39,220 --> 00:04:44,680
size of f will get multiplied by the same thing. So we have to get rid of that arbitrariness

42
00:04:44,680 --> 00:04:50,020
about f. So we got to choose something. So what I’m going to do is choose the right

43
00:04:50,020 --> 00:04:56,620
hand side of this equation to be one, okay? So Gamma. Two f and f are equally good so

44
00:04:56,620 --> 00:05:04,730
we need to choose the scaling so I&#39;m going to choose this this thing to be one so now

45
00:05:04,730 --> 00:05:11,750
gamma is actually going to be just one over the size of f and now the problem is fixed.

46
00:05:11,750 --> 00:05:19,680
Okay, so remember back to the beginning, that gamma here, this gamma, this was lower bound

47
00:05:19,680 --> 00:05:24,400
on the margin. You might want to equate on you head now that this lower bound on the

48
00:05:24,400 --> 00:05:34,120
margin is actually now one over the size of f. So now it&#39;s fixed. So let’s just keep

49
00:05:34,120 --> 00:05:38,180
all the important bits on there and write down the optimization problem that we have

50
00:05:38,180 --> 00:05:48,310
so far as to maximize gamma such that this is greater than one and then gamma is just

51
00:05:48,310 --> 00:05:58,560
one over the size of f. And then the minimum margin, the minimum of Y times f, that’s

52
00:05:58,560 --> 00:06:03,570
always going to be 1 now by our choice. Okay so y time’s f, the smallest to can be is

53
00:06:03,570 --> 00:06:10,850
1. So let’s get rid of some unnecessary stuff here, I just move that over a bit. And

54
00:06:10,850 --> 00:06:34,139
then we can replace gamma by 1/size of f. Okay, margins are at least 1. Okay, so that’s

55
00:06:34,139 --> 00:06:41,380
good. And now what’s next? Well, I said nothing about what the function F(x) was for.

56
00:06:41,380 --> 00:06:50,490
So let’s do that. We are going to choose f to be a linear model. And the size of f,

57
00:06:50,490 --> 00:06:57,860
we’ll choose to be L2 norm so let’s plug all of this stuff and let’s see what happens.

58
00:06:57,860 --> 00:07:04,259
So I have replaced the size of f by L2 norm up there. Okay? So when you maximize 1 over

59
00:07:04,259 --> 00:07:09,570
something, it’s the same as minimizing that same thing. So I can get rid of that 1 over

60
00:07:09,570 --> 00:07:16,340
and change that max to a min, and it’s the same. Okay and there you go. You got the SVM

61
00:07:16,340 --> 00:07:23,509
mathematical programming formulation right there on the screen. That is step 3. Now this

62
00:07:23,509 --> 00:07:28,710
optimization problem is solved by standard techniques in convex optimization like Lagrange

63
00:07:28,710 --> 00:07:32,830
Multipliers and KKT conditions. Actually the machinery to deal with problems like this

64
00:07:32,830 --> 00:07:37,580
is totally standard, there is actually a well –established toolbox for optimization problems

65
00:07:37,580 --> 00:07:45,110
like this. Now there’s a really interesting property about the solution of 1 the support

66
00:07:45,110 --> 00:07:51,740
vector optimization problem, which is that only certain points actually end up in the

67
00:07:51,740 --> 00:07:57,639
final solution for creating these betas. So what that means is that even if you move all

68
00:07:57,639 --> 00:08:03,680
these points here, if you moved them all around the solution for the decision boundary actually

69
00:08:03,680 --> 00:08:10,340
won’t change at all. And the special points that create the final solution they are called

70
00:08:10,340 --> 00:08:15,750
support vectors. Ad they are the points that are the closed to the decision boundary. And

71
00:08:15,750 --> 00:08:19,600
this is a really nice property of the support vector machines. And it is also the reason

72
00:08:19,600 --> 00:08:24,069
why support vector machines are called support vector machines because you have these support

73
00:08:24,069 --> 00:08:30,280
vectors that are close to the decision boundary here. So every other point that is not a support

74
00:08:30,280 --> 00:08:34,130
vector, you can move it all around out here and it won’t change your final suction.

75
00:08:34,130 --> 00:08:39,080
As long as you don’t move it in here.

76
00:08:39,080 --> 00:08:43,430
Okay so everything we talked about so far was for the case where the points could be

77
00:08:43,429 --> 00:08:49,570
separated perfectly by the decision boundary. But what if we have some noise in the data?

78
00:08:49,570 --> 00:08:53,390
And that’s no longer the case that you can perfectly separate it. Like what if the data

79
00:08:53,390 --> 00:08:57,330
looked like this for example? Like you have got a whole bunch of pints in here and no

80
00:08:57,330 --> 00:09:05,500
way you will get them correctly classified it a linear decision boundary like that. So

81
00:09:05,500 --> 00:09:13,410
this formulation has got to change to accommodate the points that are not correctly classified.

82
00:09:13,410 --> 00:09:17,720
This is not going to work as it is. There won’t be any solutions that satisfy these

83
00:09:17,720 --> 00:09:25,970
constraints. The solution to the optimization problem is that there is no solution. Okay

84
00:09:25,970 --> 00:09:32,690
so we have to fix this problem. So let’s go back to the geometric picture and the margins

85
00:09:32,690 --> 00:09:38,209
perspectives. You should know the drill by now. You take the points and you map them

86
00:09:38,209 --> 00:09:48,740
to Y times F, like that. Now these points over here are wrong and these points are right.

87
00:09:48,740 --> 00:09:53,830
And these points are really wrong and these points are really right. Just like that. And

88
00:09:53,830 --> 00:10:00,730
we want larger loss over here and lower loss over here. And we will accomplish that with

89
00:10:00,730 --> 00:10:07,149
a support vector machine hinge loss. It looks like this. Straight down and totally flat

90
00:10:07,149 --> 00:10:12,190
so if you get a point right, it is right no matter where it is over here. There is a little

91
00:10:12,190 --> 00:10:18,220
overlap over here. So if you get a point right you could still suffer a little loss if it

92
00:10:18,220 --> 00:10:23,380
is close to the decision boundary but then further away you don’t suffer any loss.

93
00:10:23,380 --> 00:10:31,839
Okay, so how are we going to put that loss function into this formulation? well first

94
00:10:31,839 --> 00:10:36,510
of all, let’s get rid of these constraints. We know that they won’t; hold in the non-separable

95
00:10:36,510 --> 00:10:44,240
case anyway. And here’s what we will do. We will put in the SVM hinge loss right in

96
00:10:44,240 --> 00:10:51,740
here. Okay? So points that are correctly classified suffer no loss, and then points that are wrong,

97
00:10:51,740 --> 00:10:56,360
suffer a penalty that’s proportional to how far away from the decision boundary they

98
00:10:56,360 --> 00:11:04,290
are okay? So if you look at this slightly longer, you may realize that this formation

99
00:11:04,290 --> 00:11:10,769
looks slightly familiar. It ought to anyway because it is a special case of our generic

100
00:11:10,769 --> 00:11:17,440
machine learning method that minimizes a mix of loss and regularization. Right, the regularization

101
00:11:17,440 --> 00:11:26,260
is the L2 norm and the loss is the hinge loss. Right? Nice huh? We started out with margins,

102
00:11:26,260 --> 00:11:33,830
and ended up all the way back at regularized loss minimization. Now in practice, the optimization

103
00:11:33,830 --> 00:11:40,959
problem is solved using something very similar to coordinate descent, which is used in boosting.

104
00:11:40,959 --> 00:11:45,930
The difference is here, that you can’t adjust one variable at a time because then you end

105
00:11:45,930 --> 00:11:50,420
up violating constraints. So you end up adjusting two variables at a time and that gives you

106
00:11:50,420 --> 00:11:54,380
the extra flexibility to sort of accommodate those constraints.

