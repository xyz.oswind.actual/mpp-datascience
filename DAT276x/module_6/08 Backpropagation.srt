0
00:00:04,799 --> 00:00:08,940
Now the main machine learning algorithm used to train neural is called the back propagation

1
00:00:08,940 --> 00:00:18,619
algorithm. So as you recall from the last time, an artificial neuron gathers the output

2
00:00:18,619 --> 00:00:23,159
from other neurons feeding into it weighted by their connectivity weights. And then the

3
00:00:23,159 --> 00:00:28,199
sum of the weighted inputs goes through an activation function and becomes the outputs

4
00:00:28,199 --> 00:00:35,120
to other neurons. Not that’s exactly what we are going to start with. So for doing machine

5
00:00:35,120 --> 00:00:40,559
learning, we are going to use a particular activation function that is smooth, something

6
00:00:40,559 --> 00:00:46,840
like this, ok. Now for doing machine learning, we are not going to use a strict threshold,

7
00:00:46,840 --> 00:00:53,250
we are going to use a soft one because it is differentiable and this function is actually,

8
00:00:53,250 --> 00:00:57,640
this function here, that’s actually the same sigmoid that we used in logistic regression.

9
00:00:57,640 --> 00:01:01,460
Now if you remember the story, this is the function that was invented when they thought

10
00:01:01,460 --> 00:01:06,770
human populations were going to saturate a country. Anyway, so here is that sigmoid for

11
00:01:06,770 --> 00:01:12,490
you and we are going to use this as the activation function rather than that plain threshold.

12
00:01:12,490 --> 00:01:19,830
So here is the neuron. It adds up all the weighted input and then sends the sum through

13
00:01:19,830 --> 00:01:26,540
the sigmoid and gets an output. Ok so let’s say that this neuron, this guy here comes

14
00:01:26,540 --> 00:01:31,170
at the very end of the whole network, so the big network is here and this guys is neuron

15
00:01:31,170 --> 00:01:40,600
100 and there are 99 neurons in this network. And at the very end of the network, neuron

16
00:01:40,600 --> 00:01:45,140
100 it receives all the information from the other 99 neurons and it does its processing

17
00:01:45,140 --> 00:01:52,770
and it outputs O 100. So now how does the network learn? Well first we have to give

18
00:01:52,770 --> 00:01:57,820
it some feedback to learn from. So here let’s say we are doing some supervised regression

19
00:01:57,820 --> 00:02:05,060
and we can look at what the network was supposed to predict for the particular input that it

20
00:02:05,060 --> 00:02:10,250
was given. So let’s call that, this is the ground tree which is y and then the error

21
00:02:10,250 --> 00:02:14,420
which the network made is something like the distance between y and the network’s output

22
00:02:14,420 --> 00:02:24,950
squared. Ok so now that we have that information how are we going to learn from it? So in a

23
00:02:24,950 --> 00:02:30,880
brain, the synapses strengthen and weaken in order to learn. Let’s say that the same

24
00:02:30,880 --> 00:02:38,010
thing happens here, so how should we set these weights in order to reduce the error, in order

25
00:02:38,010 --> 00:02:42,569
to learn to reduce the error. This is why these weights are called connectivity weights,

26
00:02:42,569 --> 00:02:49,290
because it’s like in your brain the connectivity that changes as you learn. Ok so what we will

27
00:02:49,290 --> 00:02:53,990
do is we will just minimize this error with respect to these weights and that’s what

28
00:02:53,990 --> 00:03:02,400
the back propagation algorithm is for. So back propagation is an algorithm that trains

29
00:03:02,400 --> 00:03:09,490
the weights of a neural network. It requires us to propagate information backwards through

30
00:03:09,490 --> 00:03:15,670
the network and then forward and then backward and then forward and so on. So propagating

31
00:03:15,670 --> 00:03:20,459
backward, is actually going to be the same as using the chain rule for calculus and you

32
00:03:20,459 --> 00:03:30,599
will see that as we do it. Ok so let’s go. So let’s start with this picture and the

33
00:03:30,599 --> 00:03:35,430
sum we are going to call that net 100. So Net 100 is just the weighted sum of these

34
00:03:35,430 --> 00:03:40,270
outputs from the previous neurons. Now we are going to try to, let’s say that we are

35
00:03:40,270 --> 00:03:46,440
trying to adjust the connection strength W99,100, so our goal is to try to change that to help

36
00:03:46,440 --> 00:03:52,730
with learning. And we are going to adjust that to minimize this error. And so to do

37
00:03:52,730 --> 00:03:57,700
that we are going to take a derivative of this error with respect to this weight W99,100

38
00:03:57,700 --> 00:04:04,750
and then we can take steps along the gradient to try to reduce the error. Ok so now here

39
00:04:04,750 --> 00:04:09,000
is the gradient and I’m using the chain rule from calculus here. So it is the derivative

40
00:04:09,000 --> 00:04:13,420
of E with respect to the output, and then the derivative of the output with respect

41
00:04:13,420 --> 00:04:20,479
to the net and the derivative of the net with respect to the weight that we care about W99,100.

42
00:04:20,478 --> 00:04:26,499
And then in order to calculate at least one of these terms, I need to put up a factor

43
00:04:26,499 --> 00:04:35,610
on the screen which is the derivative of this activation function is actually this thing

44
00:04:35,610 --> 00:04:42,009
over here and this is a derivative you can easily work out for yourself. Ok so we are

45
00:04:42,009 --> 00:04:47,379
going to use this later on, it would be helpful if you could remember it well enough that

46
00:04:47,379 --> 00:04:52,169
you won’t be surprised to see it again, ok. So the derivative of phi is phi itself

47
00:04:52,169 --> 00:05:01,779
times 1 minus phi. Ok so let’s start with this derivative right

48
00:05:01,779 --> 00:05:09,409
here and let’s just do that computation on the side. And this one is easy because

49
00:05:09,409 --> 00:05:14,639
we have E as a function of O100, that’s just right there. So just take the derivative

50
00:05:14,639 --> 00:05:25,449
of E with respect to O100 and we get that. Ok. So I’m just copying it over there. On

51
00:05:25,449 --> 00:05:32,419
to the next term. So for this, we need the derivative of O100

52
00:05:32,419 --> 00:05:43,309
with respect to net100. Ok so we only need this information here which is that O100 is

53
00:05:43,309 --> 00:05:50,909
just phi of net100. So O100 is related to net100 just through this phi here. So now

54
00:05:50,909 --> 00:05:56,080
you are going to see why we are going to need the derivative of phi here ok. So hopefully

55
00:05:56,080 --> 00:06:02,969
you remembered from the other slide is that the derivative of phi is just phi itself times

56
00:06:02,969 --> 00:06:13,589
1 minus phi. So here again we use that O100 is just the same as phi of net100. Ok so we

57
00:06:13,589 --> 00:06:21,629
got that term and we only have... I just copied that over here and we just have this one term

58
00:06:21,629 --> 00:06:31,360
left. Ok so the derivative of net100 with respect to W99,100. Ok so this with respect

59
00:06:31,360 --> 00:06:38,400
to that. And this one is easy because net100 is just the sum of these weighted terms only

60
00:06:38,400 --> 00:06:49,400
one of which contains W99,100. Ok so there is that sum up there. This is the weighted

61
00:06:49,400 --> 00:06:56,430
sum and only that one term involves W99,100 and so the derivative is just the output of

62
00:06:56,430 --> 00:07:07,740
neuron 99. Ok so that’s the whole calculation. Let’s just put it there, nice and neat and

63
00:07:07,740 --> 00:07:16,330
now I’m going to fiddle with some notation per bit. So this thing over here, that guy,

64
00:07:16,330 --> 00:07:25,050
this which is the same as that, I’m going to call that delta for node 100. Ok so this

65
00:07:25,050 --> 00:07:32,339
term we are going to need later, and it depends only on node 100. Ok this term depends only

66
00:07:32,339 --> 00:07:39,339
on the node that we are working with. Ok so that’s going to be called delta 100 and

67
00:07:39,339 --> 00:07:47,349
we have finished the calculations for neuron 100. Ok so let’s keep going. Now I want

68
00:07:47,349 --> 00:07:53,029
you to think of the neurons in layers ok so something like this, and this is neuron 100.

69
00:07:53,029 --> 00:07:57,599
This is called a feed forward network because all the information flows in one direction.

70
00:07:57,599 --> 00:08:01,839
And so far we have just been working on the calculations for the final layer, but let’s

71
00:08:01,839 --> 00:08:10,110
go one layer in. Let’s work on these neurons. And just so you know, I labeled the neurons,

72
00:08:10,110 --> 00:08:14,559
I named the neurons based on what label they are in. So this one at the end is a 100 and

73
00:08:14,559 --> 00:08:18,460
for convenience I called these neurons the 90s and these neurons the 80s and these neurons

74
00:08:18,460 --> 00:08:22,210
the 70s and so on. Ok that was just for my own convenience so that I can figure out which

75
00:08:22,210 --> 00:08:26,330
node I was talking about all the time. If the nodes are just labeled I and I actually

76
00:08:26,330 --> 00:08:31,219
get confused and I can’t figure out which layer we are talking about. Ok so let me blow

77
00:08:31,219 --> 00:08:38,709
it up so that you can just see one neuron in the second last layer here. Ok so this

78
00:08:38,708 --> 00:08:44,490
is the neuron we are working on now, let’s say this is neuron 98. Ok let’s say neuron

79
00:08:44,490 --> 00:08:50,990
99 is up here, 97 is down here, 96, and this is neuron 100 that we already dealt with.

80
00:08:50,990 --> 00:09:00,399
Ok so let’s say that we want to update the connectivity weight here W87,98 which is the

81
00:09:00,399 --> 00:09:07,750
edge for neuron 87 and neuron 98. And now remember, we want to adjust all of these weights

82
00:09:07,750 --> 00:09:14,449
so that the error is the smallest possible, so I just picked one to work with and making

83
00:09:14,449 --> 00:09:19,819
that error the smallest possible, that’s what it means to learn in this framework.

84
00:09:19,819 --> 00:09:28,290
So let’s do it again, the chain rule is right there, we have to adjust this weight,

85
00:09:28,290 --> 00:09:34,420
so here is the chain rule, and we will again calculate all three terms starting with the

86
00:09:34,420 --> 00:09:45,630
last one. Ok so that only involves, from here to here. So net98 to W87,98. And then that

87
00:09:45,630 --> 00:09:51,410
one is easy because the net for neuron 98 is the weighted sum of all of the other neurons

88
00:09:51,410 --> 00:09:58,850
outputs in the previous layers including neuron 87. Again there is only one term that actually

89
00:09:58,850 --> 00:10:09,250
involves neuron 87 and so that term is this one. So the answer is O the output of 87.

90
00:10:09,250 --> 00:10:15,250
So on to the next term, and the next term that we’ll do is the middle one, here this

91
00:10:15,250 --> 00:10:19,899
middle one right there, ok. So we only need that part of the network that gets us from

92
00:10:19,899 --> 00:10:33,860
net98 to O98. Ok so net98, net98 to O98 here, right here, this part. So that’s why I’m

93
00:10:33,860 --> 00:10:41,990
just going to preserve that part of the screen. Ok so O98 is just phi of net 98. Ok so we

94
00:10:41,990 --> 00:10:47,269
are just going to take the derivative of phi with respect to its argument and then, and

95
00:10:47,269 --> 00:10:55,290
so what we end up with is just this right here, the derivative of phi with respect to

96
00:10:55,290 --> 00:11:02,009
its argument, again it’s phi times one minus phi, and we get this, right which is equal

97
00:11:02,009 --> 00:11:12,120
to this one. So this term is done. Ok last term. Now this, just involves the

98
00:11:12,120 --> 00:11:23,110
relationship between the error and O98, so we only need this part of the network here.

99
00:11:23,110 --> 00:11:30,379
Now for this one we use the chain rule again, ok so we are going to go this time through

100
00:11:30,379 --> 00:11:43,220
neuron 100. So here is this derivative right here, that guy, this is the one we computed

101
00:11:43,220 --> 00:11:48,759
earlier and we defined it to be delta 100, you can flip back a few slides if you don’t

102
00:11:48,759 --> 00:11:54,990
believe me, but this is exactly delta100. And since we already did the calculation for

103
00:11:54,990 --> 00:12:05,740
delta100 it’s all done and we know what delta100 is. Now for that last term, this

104
00:12:05,740 --> 00:12:12,930
is one where we used the definition of net100 and there is only one term in here which involves

105
00:12:12,930 --> 00:12:20,480
neuron 98 which is that one and we just wind up putting the answer, we just end up putting

106
00:12:20,480 --> 00:12:26,209
the answer right there ok. So this is only one term involving O98 and so the term that

107
00:12:26,209 --> 00:12:36,100
is left is W98, 100. Ok so now we know everything here. This term is delta100 times W98, 100

108
00:12:36,100 --> 00:12:41,970
and then we have this and that. So let’s put it all together here with this picture,

109
00:12:41,970 --> 00:12:50,800
ok. So that’s the three terms all together. And I’m just making it a little tidier,

110
00:12:50,800 --> 00:12:57,660
making it look pretty and we are finished with that one. We are finished with this derivative.

111
00:12:57,660 --> 00:13:02,629
And then we can do all of, we can do analogous calculation for all of the nodes in the 90s

112
00:13:02,629 --> 00:13:07,870
layer. I just picked one of them to work with neuron 98, but it’s the same calculation

113
00:13:07,870 --> 00:13:13,870
for all of the ones in the 90s layer. Ok so we managed to get through the first

114
00:13:13,870 --> 00:13:24,949
two layers, no let us do the third one. Ok so now we are in the 80s. I picked neuron,

115
00:13:24,949 --> 00:13:30,310
I picked weight 72, 87 to be the one that we are going to adjust, so this is the connectivity

116
00:13:30,310 --> 00:13:38,250
between neuron72 and neuron 87 which is right there. Ok so let’s as usual take the derivative

117
00:13:38,250 --> 00:13:41,930
of the error with respect to the weight we are interested in using the chain rule. And

118
00:13:41,930 --> 00:13:52,360
there it is. And this term here, same calculation. That one is just O72 again, just because of

119
00:13:52,360 --> 00:13:58,509
the linear relationship between net87 and W72, 87. So we’ve done that calculation

120
00:13:58,509 --> 00:14:06,720
twice already so that’s pretty easy. And now for the second term. Now this one involves

121
00:14:06,720 --> 00:14:12,089
only the definition of, so this is just this part of the network, so again this one only

122
00:14:12,089 --> 00:14:17,050
involves the definition of phi. So it doesn’t use this whole part of the network, so let’s

123
00:14:17,050 --> 00:14:24,209
get rid of that temporarily. Ok so there, this is the same as before, using the definition

124
00:14:24,209 --> 00:14:34,800
of phi and its derivative, which is over here, plug it all in. One term left, and this one

125
00:14:34,800 --> 00:14:44,430
involves everything, from the final error, all the way down to node 87. Ok the whole

126
00:14:44,430 --> 00:14:50,680
part of the network here is involved in that term. So this term is not as easy as it was

127
00:14:50,680 --> 00:15:01,879
before. Ok so how do we compute this thing? Now O87, it depends on everything that comes

128
00:15:01,879 --> 00:15:11,749
after it, all of these nodes. So the chain rule has to handle all of them. Ready? And

129
00:15:11,749 --> 00:15:15,639
there it is, this is the chain rule that includes all of the relevant downstream nodes, all

130
00:15:15,639 --> 00:15:25,709
of them. Ok so 99, 98, 97 so on. The whole 90s layer. And as it turns out a bunch of

131
00:15:25,709 --> 00:15:30,930
these terms that we had already computed while we were doing the 90s layer. All of these

132
00:15:30,930 --> 00:15:36,730
deltas are already computed just a little earlier in the calculation. So remember we,

133
00:15:36,730 --> 00:15:44,259
we just got a sum of deltas from the next layer times the connectivity, weights downstream

134
00:15:44,259 --> 00:15:51,240
one layer ok. So this term is just the sum of all the deltas that we computed earlier,

135
00:15:51,240 --> 00:15:57,519
in the next layer, right, times these connectivity weights downstream one layer.

136
00:15:57,519 --> 00:16:05,069
Ok so let’s take a step back, and try to write the computation in a more general way.

137
00:16:05,069 --> 00:16:09,360
So hopefully you understand the pattern well enough now that none of these is going to

138
00:16:09,360 --> 00:16:15,610
be a surprise. So let’s say we have a neuron, it’s called b who is connected to neuron

139
00:16:15,610 --> 00:16:24,180
a. And we are trying to adjust the weight Wa, b optimally. So here is the derivative

140
00:16:24,180 --> 00:16:32,949
of the error with respect to the Wa,b using the chain rule and one of these terms is always

141
00:16:32,949 --> 00:16:42,399
just the output of the earlier node. That will always happen. And then another term,

142
00:16:42,399 --> 00:16:50,230
this term over here, comes from the derivative of phi and it looks like this. Now you’ve

143
00:16:50,230 --> 00:16:55,680
seen that twice before or three times before now actually. And then the last term looks

144
00:16:55,680 --> 00:17:04,890
like this. It’s a sum of the deltas that are downstream, with the downstream weights.

145
00:17:04,890 --> 00:17:12,180
Now it’s important that these Ls are indices for downstream neurons. In order to compute

146
00:17:12,180 --> 00:17:19,310
this, we had to have already computed the delta Ls that go into this sum. So we had

147
00:17:19,310 --> 00:17:24,660
to work backward along the layer of the neural network in order to compute this. And if we

148
00:17:24,660 --> 00:17:31,790
did manage to compute this then we have the gradient, that’s the whole thing. Ok so

149
00:17:31,790 --> 00:17:39,600
what this implies is that we take our network, compute gradients with respect to the last

150
00:17:39,600 --> 00:17:46,300
layer, and then compute gradients for the second last layer, and then the third last

151
00:17:46,300 --> 00:17:53,300
layer, and as we compute these we use the deltas that we computed from the layer downstream

152
00:17:53,300 --> 00:18:05,510
until we get to the beginning. Ok so now we know how to compute the derivative

153
00:18:05,510 --> 00:18:09,950
of the error function with respect to the weights, for all the weights, we know how

154
00:18:09,950 --> 00:18:16,560
to do this. So let us do gradient descent, now that we have the gradient, we can do gradient

155
00:18:16,560 --> 00:18:23,640
descent. Ok so this is the formula for gradient descent. You take your weights and then send

156
00:18:23,640 --> 00:18:29,740
them... walk down the gradient by a step size of alpha where alpha here is the learning

157
00:18:29,740 --> 00:18:36,920
rate. So alpha is between 0 and 1 it’s called the learning rate. Now in standard Back propagation,

158
00:18:36,920 --> 00:18:40,900
too low of a learning rate makes the network learn very slowly. Too high a learning rate

159
00:18:40,900 --> 00:18:44,600
makes the weights an objective diverge, so there is no learning at all. So you could

160
00:18:44,600 --> 00:18:50,050
set this thing to somewhere like a half. In reality there are algorithms that adjust this

161
00:18:50,050 --> 00:18:54,760
thing dynamically, this guy. But in this case hopefully you get the idea which is that the

162
00:18:54,760 --> 00:19:02,540
weight changes depending on how steep the gradient is at that point. Ok so now we know

163
00:19:02,540 --> 00:19:09,530
how to propagate errors back through the network. And now once we made these weight updates,

164
00:19:09,530 --> 00:19:15,560
we have to update the calculation of the error. We know how to propagate the errors back through

165
00:19:15,560 --> 00:19:19,220
the network to get the gradient, but now we have to re compute these errors after we make

166
00:19:19,220 --> 00:19:26,300
these steps. And to do that we have to evaluate, we have to go forward through that network.

167
00:19:26,300 --> 00:19:34,140
Do you remember how you do it? Take the input and you just feed it through. You sum things

168
00:19:34,140 --> 00:19:42,070
up, you send it through the activation function, you calculate the outputs for that later and

169
00:19:42,070 --> 00:19:50,480
it goes to the next layer, the output of this layer goes as input to the next layer and

170
00:19:50,480 --> 00:19:57,080
then so on and we keep going. And that’s how we calculate the new error. All the way

171
00:19:57,080 --> 00:20:03,200
through the network until we calculate the new error. And that’s it.

172
00:20:03,200 --> 00:20:08,190
Ok so to do back propagation, you just repeat this whole process. You repeat going backward

173
00:20:08,190 --> 00:20:13,130
to calculate the gradient, adjusting the weights and you go forward to calculate the errors

174
00:20:13,130 --> 00:20:17,370
and you do it over and over again in order to learn and that’s the way you train a

175
00:20:17,370 --> 00:20:24,660
neural network. So let’s just summarize. The advantages

176
00:20:24,660 --> 00:20:32,510
of these neural networks are that they are highly expressive nonlinear models. They have

177
00:20:32,510 --> 00:20:38,800
advances in computer vision and speech that other machine learning methods have not achieved.

178
00:20:38,800 --> 00:20:43,630
And it’s because of their ability to capture latent structure within the hidden layers.

179
00:20:43,630 --> 00:20:49,630
Now the disadvantages of neural networks, are that they can get stuck in local optima

180
00:20:49,630 --> 00:20:55,200
because this whole procedure of backpropagation, there is no guarantee that it will end up

181
00:20:55,200 --> 00:21:02,970
with a globally minimal solution. So it could produce bad solutions, so you have to kind

182
00:21:02,970 --> 00:21:09,590
of tune things and set things up so that hopefully it won’t end up with a bad solution. The

183
00:21:09,590 --> 00:21:14,040
models are black box. There is a whole subfield of machine learning right now that tries to

184
00:21:14,040 --> 00:21:21,630
figure out what is going on inside of a neural network. The computations are completely opaque.

185
00:21:21,630 --> 00:21:24,900
And another disadvantage that’s actually quite a major disadvantage is that there are

186
00:21:24,900 --> 00:21:30,400
a lot of tuning parameters. For example, you have to determine the whole structure of the

187
00:21:30,400 --> 00:21:36,590
network before you even set the whole thing up and that could be a very intimidating process.

188
00:21:36,590 --> 00:21:40,140
But if you are one of those people who can work with these neural networks then you do

189
00:21:40,140 --> 00:21:41,450
things that nobody else can do.

