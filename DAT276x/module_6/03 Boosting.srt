0
00:00:05,259 --> 00:00:08,620
So now I am going to give you an introduction to boosting, which is one of the most powerful

1
00:00:08,620 --> 00:00:12,980
machine learning techniques out there. Now boosting started with the question of Michael

2
00:00:12,980 --> 00:00:19,840
Kearns which was &quot;Can you turn a weak learning algorithm, so an algorithm that is barely

3
00:00:19,840 --> 00:00:24,860
better than random guessing, into a strong learning algorithm, an algorithm whose error

4
00:00:24,860 --> 00:00:33,690
rate is arbitrarily close to 0? So Schapire and French who were working on

5
00:00:33,690 --> 00:00:39,710
this problem, they thought that maybe what we could do is take the weak learning algorithm,

6
00:00:39,710 --> 00:00:46,339
ask them to create a whole bunch of classifiers, and figure out how to combine all those classifiers.

7
00:00:46,339 --> 00:00:51,920
But how are we going to do that? Because the problem is we got to figure how to ask the

8
00:00:51,920 --> 00:00:56,769
learning algorithm to create a lot of classifiers. If we give the algorithm the same inputs each

9
00:00:56,769 --> 00:01:01,600
time, it’s going to produce the same answer. Not a lot of different classifiers. So we

10
00:01:01,600 --> 00:01:06,890
have to figure out a way to feed lots of different inputs into the weak learning algorithm but

11
00:01:06,890 --> 00:01:11,860
we have only one dataset. So Schapire and French’s answer was let’s just try reweight

12
00:01:11,860 --> 00:01:19,470
the data in many different ways and then we will feed this weighted datasets into the

13
00:01:19,470 --> 00:01:25,640
weak learning algorithm to create a weak classifier for each reweighted dataset. And then we will

14
00:01:25,640 --> 00:01:32,260
compute a weighted average of these weak classifiers. Now there were several boosting algorithms

15
00:01:32,260 --> 00:01:38,320
but the most successful one was called Adaboost. And I will talk about that one today. But

16
00:01:38,320 --> 00:01:43,340
first I want to talk about the application that made boosting really famous, and this

17
00:01:43,340 --> 00:01:48,450
is the famous application of Viola and Jones to computer vision. So Viola and Jones were

18
00:01:48,450 --> 00:01:54,630
interested in face detection. Given an image, they wanted to know where the face is in that

19
00:01:54,630 --> 00:02:03,479
image. So they came up with these very simple classifiers that looked something like this.

20
00:02:03,479 --> 00:02:10,470
So what the heck is that. How are these 4 weird shapes weak classifiers? So let’s

21
00:02:10,470 --> 00:02:15,069
just look at this one and see magic of what this does. So you’re supposed to take that

22
00:02:15,069 --> 00:02:21,599
little shape and move it around the image and subtract what’s in the white from what’s

23
00:02:21,599 --> 00:02:30,269
in the black area. So let’s give it a shot. This is Steve Elsden and I put my little detector

24
00:02:30,269 --> 00:02:36,599
somewhere on his face. And I subtract what’s in the white area form what’s in the black

25
00:02:36,599 --> 00:02:43,540
one and as it turns out the pixels looks basically the same in the white area and the black area.

26
00:02:43,540 --> 00:02:47,920
So basically this detector doesn’t detect anything because the black and white areas

27
00:02:47,920 --> 00:02:55,469
are very similar and so when we subtract them, we get 0. So okay. Now what?

28
00:02:55,469 --> 00:03:01,269
Let’s try again. I put the little detector on the bottom of Steve’s face and again

29
00:03:01,269 --> 00:03:05,540
the black and white area are very similar and the detector says whoop de doo I don’t

30
00:03:05,540 --> 00:03:12,829
see anything. But now we move the detector over Steve’s eyes and now what happens?

31
00:03:12,829 --> 00:03:18,249
The white area is really light colored whereas the black area covers his eyes, which is actually

32
00:03:18,249 --> 00:03:25,569
dark. So when we subtract the areas, you actually get a big number, so now the detector makes

33
00:03:25,569 --> 00:03:34,189
sense, it is an eye detector. The detector is actually looking for the difference between

34
00:03:34,189 --> 00:03:40,959
the dark eyes and the part of the face below that. And the second little detector there

35
00:03:40,959 --> 00:03:48,529
is actually looking for two black areas separated by a white area which is the person’s nose.

36
00:03:48,529 --> 00:03:54,369
Okay? So each of these little detectors by itself - not so useful. But you put them together

37
00:03:54,369 --> 00:03:59,260
- really useful. So now you can detect eyes and cheeks, you can detect the little space

38
00:03:59,260 --> 00:04:04,430
between the eyes and you can envision lots of little detectors trying to find parts of

39
00:04:04,430 --> 00:04:10,859
someone’s face. So when Viola and Jones they used in their paper they used hundreds

40
00:04:10,859 --> 00:04:15,059
of thousands of these weak classifiers of all different scales. Because, of course,

41
00:04:15,059 --> 00:04:21,790
faces can be large and or in an image they can be small. Now these are all weak classifiers

42
00:04:21,790 --> 00:04:26,810
since obviously none of them is actually better than random guessing but they are in fact

43
00:04:26,810 --> 00:04:32,240
better than random because if you combine them you can get something really really good.

44
00:04:32,240 --> 00:04:39,310
And then if things go well they are able to put these boxes around people’s faces. And

45
00:04:39,310 --> 00:04:45,540
now the question is now that we have all these detectors, how is it that we are able to combine

46
00:04:45,540 --> 00:04:51,380
these to make a good detector and that’s why I have toy explain the Adaboost algorithm

47
00:04:51,380 --> 00:04:51,820
to you.

