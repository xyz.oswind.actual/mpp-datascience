0
00:00:05,609 --> 00:00:10,340
Decision trees are some of the most popular machine learning algorithms used in industry,

1
00:00:10,340 --> 00:00:16,910
and they’re very useful just in general. So let’s talk about how they work. So why

2
00:00:16,910 --> 00:00:22,180
decision trees? I have kind of a love-hate relationship with decision trees, I’ll tell

3
00:00:22,180 --> 00:00:27,579
you the disadvantages later on. Let me first let you admire the nice things about them.

4
00:00:27,579 --> 00:00:32,360
They’re interpretable, they’re intuitive, they’re great for medical applications because

5
00:00:32,360 --> 00:00:37,250
they kind of mimic the way people like to reason and they’re based on logic. And they’re

6
00:00:37,250 --> 00:00:46,810
also powerful non-linear models. So I’m going to give you an example with a kind of

7
00:00:46,810 --> 00:00:51,329
an interesting data set to illustrate how these methods work. Okay so the problem is

8
00:00:51,329 --> 00:00:55,010
to predict whether a customer is going to wait for a table at a restaurant, or whether

9
00:00:55,010 --> 00:01:02,790
they’re going to leave. Now here are the features. So other options is true if there

10
00:01:02,790 --> 00:01:07,079
are other options nearby, right, so they might be more likely to leave if there are other

11
00:01:07,079 --> 00:01:15,940
restaurants nearby. Weekend, okay, so this feature is 1 if it’s Friday, Saturday, or

12
00:01:15,940 --> 00:01:21,500
Sunday. Area; does the restaurant have a nice bar or a waiting area to wait in? Does the

13
00:01:21,500 --> 00:01:27,850
customer have plans after dinner? The price of the restaurant, the precipitation if it’s

14
00:01:27,850 --> 00:01:36,520
raining or snowing, the genre, and the wait time of the restaurant, and whether it’s

15
00:01:36,520 --> 00:01:44,049
crowded and whether there are some other customers there. And here is the full dataset; so each

16
00:01:44,049 --> 00:01:50,040
row here in this data set is a customer, and then we have all the features for each customer

17
00:01:50,040 --> 00:01:54,840
in the table, and we have the label which is whether or not they stayed at the restaurant

18
00:01:54,840 --> 00:02:03,869
or left. Now there are indeed only twelve customers in this dataset, and here is an

19
00:02:03,869 --> 00:02:10,259
example of a decision tree built from this dataset. So the decision tree says, well,

20
00:02:10,258 --> 00:02:14,760
check if the restaurant’s well crowded. If the restaurant’s – if there are no

21
00:02:14,760 --> 00:02:19,730
customers in the restaurant, the customer is not going to wait for a table. If there’s

22
00:02:19,730 --> 00:02:24,540
no one in the restaurant, maybe there’s a reason why. Okay, if the restaurant has

23
00:02:24,540 --> 00:02:29,610
some customers, then yeah maybe it’s worthwhile to wait because hopefully you’ll get a table

24
00:02:29,610 --> 00:02:37,870
soon. If the restaurant is completely full, then the question is whether you have plans

25
00:02:37,870 --> 00:02:41,860
after you go to the restaurant. If you have plans and you need to eat dinner and go quick,

26
00:02:41,860 --> 00:02:47,349
then no, you’re not going to wait. If you don’t have plans, well, then it depends

27
00:02:47,349 --> 00:02:51,140
what genre the restaurant is. Like, you’re willing to wait for French, because that’s

28
00:02:51,140 --> 00:02:55,620
going to be really good; for some reason Mexican you don’t – you’re not interested in

29
00:02:55,620 --> 00:03:01,129
waiting; pizza, yeah you’ll wait for pizza because that’s going to be real fast. If

30
00:03:01,129 --> 00:03:07,510
it’s Thai, then, well, the question is whether it’s a weekend because if it’s not a weekend,

31
00:03:07,510 --> 00:03:11,959
you would not wait for Thai, but if it is a weekend, then you would indeed wait. Okay,

32
00:03:11,959 --> 00:03:16,080
so that’s the whole decision tree, and now you can see why people like these things so

33
00:03:16,080 --> 00:03:21,370
much. They’re super intuitive, it’s logic, like similar to the way that a human might

34
00:03:21,370 --> 00:03:27,709
reason about this problem. Okay, now here is the standard way to build

35
00:03:27,709 --> 00:03:36,860
a tree. You start at the very top, you grow the tree by splitting features one by one

36
00:03:36,860 --> 00:03:43,810
to create the tree, and to split, you have to look at how impure the node is, and I’ll

37
00:03:43,810 --> 00:03:54,150
tell you about that soon. Then you assign leaf nodes, a majority vote of the observations

38
00:03:54,150 --> 00:04:01,909
in that leaf. And then at the end, you go back and you prune the leaves to try to reduce

39
00:04:01,909 --> 00:04:12,450
over-fitting. Now, why is this a good way to build a tree? Build it from the top down,

40
00:04:12,450 --> 00:04:18,409
go back up and prune it. Now why is that a good way to build a tree? It’s not. It’s

41
00:04:18,409 --> 00:04:23,080
really what I call a hack, but it works really well, and I have to warn you that – C4.5

42
00:04:23,080 --> 00:04:29,490
and CART, they’re not elegant by any means that I can even deem elegant, right, C4.5

43
00:04:29,490 --> 00:04:34,550
and CART are the major decision tree algorithms right now. But the resulting trees can be

44
00:04:34,550 --> 00:04:39,520
very elegant and plus these are two of the top ten algorithms in data mining and these

45
00:04:39,520 --> 00:04:43,919
are decision tree algorithms. So it’s worth it for us to know what’s under the hood,

46
00:04:43,919 --> 00:04:50,509
even though, well, let’s just say it ain’t pretty. Okay, so let’s talk about how to

47
00:04:50,509 --> 00:04:55,009
construct a decision tree, and in particular, let’s talk about the splitting procedure.

48
00:04:55,009 --> 00:05:02,270
So let’s say that we have two choices of features to split on. Which one of these two

49
00:05:02,270 --> 00:05:06,749
gives the most information about whether the customer is going to wait? So I have all the

50
00:05:06,749 --> 00:05:12,409
customers, all twelve customers up here; there’s an equal number of wait and non-wait, like

51
00:05:12,409 --> 00:05:21,110
plus or minus, and then if we split in crowded… if you look at the restaurants where it’s

52
00:05:21,110 --> 00:05:26,939
not crowded or situations where it’s not crowded, then none of the – both times the

53
00:05:26,939 --> 00:05:36,149
customer left. And then if there restaurant is somewhat crowded, then all of the times

54
00:05:36,149 --> 00:05:47,419
in the data set people waited, okay, whereas for splitting on genre… okay, so you know,

55
00:05:47,419 --> 00:05:53,129
if you split on genre, you get an equal number of customers waiting and leaving for each

56
00:05:53,129 --> 00:05:58,309
possible type of restaurant. So it’s obvious here that, you know, genre tells us pretty

57
00:05:58,309 --> 00:06:04,520
much nothing by itself, okay? You have – here you have an equal number of customers waiting

58
00:06:04,520 --> 00:06:08,779
and not waiting in each of the four types, so clouded is clearly the answer; we would

59
00:06:08,779 --> 00:06:15,729
want to split on this one. So then after we split on crowded, we would then evaluate what

60
00:06:15,729 --> 00:06:21,969
to split on and then maybe we’d choose plans, okay so we’ve decided here that it’s best

61
00:06:21,969 --> 00:06:27,949
to split on nodes that give us more information about the label. Ah yet, I have not to find

62
00:06:27,949 --> 00:06:30,449
information, so let’s do that in the next clip.

