0
00:00:05,430 --> 00:00:10,160
So this lecture is about neural networks which have proved extremely valuable for our computer

1
00:00:10,160 --> 00:00:17,760
vision problems in particular. Now an artificial neural is totally different from a real neural

2
00:00:17,760 --> 00:00:23,220
network. The order of magnitudes is totally different. Like our brains are much bigger.

3
00:00:23,220 --> 00:00:28,530
There are a massive number of neurons in your brain and they care connected by tons of synapses,

4
00:00:28,530 --> 00:00:33,120
which are the connection between your neurons. And the signals that travel through your brain

5
00:00:33,120 --> 00:00:39,520
they are called spikes, and they are spikes of electrical potential. Now artificial networks

6
00:00:39,520 --> 00:00:43,440
are totally different. They are much smaller, they are not nearly as interconnected, and

7
00:00:43,440 --> 00:00:48,180
the spikes are totally different. Artificial neural networks, they calculate like calculators,

8
00:00:48,180 --> 00:00:53,940
we don’t. It is just totally different. But they were inspired by brains. But in any

9
00:00:53,940 --> 00:00:57,510
case they are great for computer vision in particular because they can handle really

10
00:00:57,510 --> 00:01:02,329
challenging substructures that other methods have difficulty with. We give them tons and

11
00:01:02,329 --> 00:01:06,719
tons of data and a problem in computer vision says that they can do things that other methods

12
00:01:06,719 --> 00:01:14,350
can’t. So this is me. I am artificial neuron. I just

13
00:01:14,350 --> 00:01:23,130
called it me. And I do a special kind of computation. So me sees as input, all of the other outputs

14
00:01:23,130 --> 00:01:30,080
of the other neurons that feed into it and it’s weighted by their connectivity. So

15
00:01:30,080 --> 00:01:36,900
this neuron me is very heavily connected to this guy over here and that guy over there.

16
00:01:36,900 --> 00:01:42,009
So what does me do with all of these inputs? It adds them all up. So I am going to put

17
00:01:42,009 --> 00:01:48,840
a little summation in there but what it actually means is this, where it adds up all the signals

18
00:01:48,840 --> 00:01:57,180
coming in and weighted by the strength of the connection. Okay so then what? This sum

19
00:01:57,180 --> 00:02:03,439
feeds into an activation function, which is usually something that goes between 0 and

20
00:02:03,439 --> 00:02:10,979
1. And it is supposed to look like a threshold function. So this is the activation function

21
00:02:10,979 --> 00:02:19,719
of the sum. Okay? So the value of this activation function is actually the output of the neuron

22
00:02:19,719 --> 00:02:27,950
and I am going to call it O of me. And O of me, once it is computed, it feeds into all

23
00:02:27,950 --> 00:02:34,029
of the neurons that it connects to. Weighted by the connection strength. And that’s what

24
00:02:34,029 --> 00:02:40,849
an artificial neuron is. That’s what computation it does. It’s not quite a real neuron. But

25
00:02:40,849 --> 00:02:47,859
it is definitely inspired by one. And now just for a few minutes, let’s say that the

26
00:02:47,859 --> 00:02:52,859
activation function is just a step function. Okay, so the step function is a threshold.

27
00:02:52,859 --> 00:02:57,169
So if you get enough electrical input from the other neurons, then this sum is large

28
00:02:57,169 --> 00:03:03,760
and then the neuron fires. So it spits out this higher value rather than the lower value

29
00:03:03,760 --> 00:03:09,349
if the sum is high enough. So what can this compute? How smart is this

30
00:03:09,349 --> 00:03:17,790
neuron? Well let’s try this out for a bunch of different inputs. Okay? So we are going

31
00:03:17,790 --> 00:03:24,150
to compute the output or a bunch of different choices for O1 and O2, which are the inputs

32
00:03:24,150 --> 00:03:29,159
into the neuron. So let’s say that they come from other neurons in the network. Each

33
00:03:29,159 --> 00:03:35,379
have a connectivity of 1. Okay the weights are 1. And then for free, we get an input

34
00:03:35,379 --> 00:03:43,889
of always -1, with a connection weight of 1.5 here. Okay so what if the inputs are 0

35
00:03:43,889 --> 00:03:52,989
for O1 and 0 for O2? Then what’s the output. It’s going to be zero plus zero times one

36
00:03:52,989 --> 00:04:03,889
plus zero times one again plus -1 times 1.5 which is -1.5. So the sum is -1.5. And that

37
00:04:03,889 --> 00:04:10,959
is below our threshold to fire which is zero. So our neuron does not fire the output is

38
00:04:10,959 --> 00:04:18,120
then 0. So what about if the inputs were 1 and 0.

39
00:04:18,120 --> 00:04:23,810
Then what’s the output. So it turns out that if you compute that weighted sum and

40
00:04:23,810 --> 00:04:30,199
if you subtract the free -1.5 from it, you still get something below zero, so the step

41
00:04:30,199 --> 00:04:38,490
function wont activate and the output again is zero. Okay what about 0 1? And that’s

42
00:04:38,490 --> 00:04:46,419
totally symmetric, so the result is exactly the same. But now let’s try 1 and 1 as inputs.

43
00:04:46,419 --> 00:04:57,550
In that case, the weighted sum is 1 times 1, plus 1 times 1, minus the free 1.5 and

44
00:04:57,550 --> 00:05:03,900
the answer is 0.5, And that is actually enough to trigger the activation function into firing

45
00:05:03,900 --> 00:05:13,509
because this is above zero. So this time the output is 1. Now cool. This table shows the

46
00:05:13,509 --> 00:05:21,979
output of the neuron as 1 if input 1 and input 2 are both 1. If either one of them is zero,

47
00:05:21,979 --> 00:05:27,770
then the output is zero; So this is exactly the computation of the and table function

48
00:05:27,770 --> 00:05:37,810
that computes whether both input1 and input 2 are one they are both 1 and then 0 otherwise.

49
00:05:37,810 --> 00:05:43,650
So in other words, this neuron computes the function AND. Now it is nice to know that

50
00:05:43,650 --> 00:05:49,199
we can compute some logic. That’s kind of fun. And in fact there are OR neurons. And

51
00:05:49,199 --> 00:05:53,889
there are NOT neurons too. So actually if you put a bunch of these neurons together,

52
00:05:53,889 --> 00:05:56,280
you can model all kinds of interesting logical statements.

53
00:05:56,280 --> 00:06:02,939
So I hope you get the idea of how you can do computation with artificial neural networks.

54
00:06:02,939 --> 00:06:08,129
Now in the next section we are gonna talk about how you would actually do learning with

55
00:06:08,129 --> 00:06:13,860
a neural network and in particular how you would adjust the connectivity waves using

56
00:06:13,860 --> 00:06:18,550
the back propagation algorithm in order to get the neural network to learn.

