0
00:00:01,790 --> 00:00:04,590
>> Hello and welcome. So, in

1
00:00:04,590 --> 00:00:06,990
this video I'm going to talk about the theory of

2
00:00:06,990 --> 00:00:11,990
Naive Bayes models specifically Naive Bayes classifiers.

3
00:00:11,990 --> 00:00:15,875
So, what do I mean by a Naive Bayes model?

4
00:00:15,875 --> 00:00:18,705
Well, first off, Naive Bayes models,

5
00:00:18,705 --> 00:00:20,080
like all Bayes models,

6
00:00:20,080 --> 00:00:24,370
use conditional probabilities to predict the label value.

7
00:00:24,370 --> 00:00:27,970
So, they're inherently probabilistic models,

8
00:00:27,970 --> 00:00:29,365
which is a little different from

9
00:00:29,365 --> 00:00:31,025
all the other machine learning models

10
00:00:31,025 --> 00:00:32,725
we talk about in this course.

11
00:00:32,725 --> 00:00:35,440
Just in how we talk about them,

12
00:00:35,440 --> 00:00:37,345
how we formulate them.

13
00:00:37,345 --> 00:00:39,010
But you often can use them

14
00:00:39,010 --> 00:00:41,870
interchangeably with other machine learning models.

15
00:00:41,870 --> 00:00:43,960
But we just think about this in

16
00:00:43,960 --> 00:00:46,760
terms of these conditional probabilities.

17
00:00:46,760 --> 00:00:48,915
So, what's the naive part?

18
00:00:48,915 --> 00:00:50,840
Why do we call it Naive Bayes, Well,

19
00:00:50,840 --> 00:00:53,095
the Naive assumption is that

20
00:00:53,095 --> 00:00:57,415
our features are statistically independent.

21
00:00:57,415 --> 00:01:00,360
It turns out that even in

22
00:01:00,360 --> 00:01:04,320
cases where that isn't quite true,

23
00:01:04,320 --> 00:01:06,765
it works out pretty well.

24
00:01:06,765 --> 00:01:09,870
Because of that there there's some really nice advantages

25
00:01:09,870 --> 00:01:12,480
of using these models.

26
00:01:12,480 --> 00:01:15,460
For example, they work well with

27
00:01:15,460 --> 00:01:17,775
a minimal amount of training data.

28
00:01:17,775 --> 00:01:19,880
If you have a fair number of features,

29
00:01:19,880 --> 00:01:21,770
fairly complex problem but yet

30
00:01:21,770 --> 00:01:24,850
you don't have a lot of cases to train on,

31
00:01:24,850 --> 00:01:26,130
often a Bayes model or

32
00:01:26,130 --> 00:01:28,445
a Naive Bayes model is a really good bet,

33
00:01:28,445 --> 00:01:30,690
because it has a nice way of

34
00:01:30,690 --> 00:01:33,080
maximizing the information you can extract,

35
00:01:33,080 --> 00:01:34,470
because of working directly

36
00:01:34,470 --> 00:01:36,420
with these conditional probabilities.

37
00:01:36,420 --> 00:01:39,520
You don't need to worry about a lot of regularization

38
00:01:39,520 --> 00:01:44,500
because Bayes models in general self-regularize.

39
00:01:44,500 --> 00:01:46,860
They tend to, because we're

40
00:01:46,860 --> 00:01:50,010
dealing with probabilities and

41
00:01:50,010 --> 00:01:52,460
probabilities have to be bounded between zero and

42
00:01:52,460 --> 00:01:56,105
one, they regularize themselves.

43
00:01:56,105 --> 00:01:58,515
They're very computationally

44
00:01:58,515 --> 00:02:01,250
efficient and highly scalable.

45
00:02:01,250 --> 00:02:05,965
They're used on very large scale problems as well.

46
00:02:05,965 --> 00:02:08,310
There's a number of good reasons why

47
00:02:08,310 --> 00:02:11,320
Naive Bayes models are useful to know about.

48
00:02:11,320 --> 00:02:14,410
So let's talk about conditional probabilities.

49
00:02:14,410 --> 00:02:16,160
You need to understand conditional probabilities to

50
00:02:16,160 --> 00:02:19,255
understand how the Naive Bayes model works.

51
00:02:19,255 --> 00:02:22,010
Conditional probability is the probability of

52
00:02:22,010 --> 00:02:24,690
some event given another event.

53
00:02:24,690 --> 00:02:26,480
Well okay. I mean, that sounds

54
00:02:26,480 --> 00:02:28,395
simple but it's also kind of abstract.

55
00:02:28,395 --> 00:02:31,180
Let's try to bring that down to earth here.

56
00:02:31,180 --> 00:02:34,790
Here I have a little box with a sample space S. That's

57
00:02:34,790 --> 00:02:37,130
just all the events that could possibly happen

58
00:02:37,130 --> 00:02:40,055
along maybe two different features,

59
00:02:40,055 --> 00:02:42,170
horizontal and vertical here.

60
00:02:42,170 --> 00:02:46,630
Say there is some subset in that space,

61
00:02:46,630 --> 00:02:48,020
which I'm going to call B.

62
00:02:48,020 --> 00:02:51,750
So, an event can occur within that subspace B,

63
00:02:51,750 --> 00:02:55,150
and the probability of B is just the area of

64
00:02:55,150 --> 00:02:57,850
that blue circle divided by

65
00:02:57,850 --> 00:03:00,630
the area of the rectangle of the whole sample space.

66
00:03:00,630 --> 00:03:03,230
In this case because I only have two dimensions, right?

67
00:03:03,230 --> 00:03:07,120
I could have another event occur A,

68
00:03:07,170 --> 00:03:11,365
and in this intersection here,

69
00:03:11,365 --> 00:03:14,100
of those two sets A and B,

70
00:03:14,100 --> 00:03:17,670
I could have some probability that if

71
00:03:17,670 --> 00:03:22,915
an event B occurs the event A could also occur, right?

72
00:03:22,915 --> 00:03:28,390
So, that's in that intersection of those two spaces.

73
00:03:28,390 --> 00:03:30,900
Okay, so how do we figure

74
00:03:30,900 --> 00:03:33,245
that out? What's going on there?

75
00:03:33,245 --> 00:03:35,610
So first off, just a little notation,

76
00:03:35,610 --> 00:03:38,740
we write that as a conditional probability,

77
00:03:38,740 --> 00:03:41,850
p of A given B, because,

78
00:03:41,850 --> 00:03:44,580
we've said the event B has happened and now we want

79
00:03:44,580 --> 00:03:47,450
to find the probability that the event A happens,

80
00:03:47,450 --> 00:03:50,670
given that we know that B has already happened, okay?

81
00:03:50,670 --> 00:03:54,000
So, you can say,

82
00:03:54,000 --> 00:03:56,520
probability of A given B equals,

83
00:03:56,520 --> 00:03:59,565
it clearly has to depend on the probability of B,

84
00:03:59,565 --> 00:04:04,125
because we've said B is a given times something.

85
00:04:04,125 --> 00:04:06,810
My question is, what's that something?

86
00:04:06,810 --> 00:04:12,805
Well, it's the intersection of A and B, okay?

87
00:04:12,805 --> 00:04:17,325
Because you can see B has definitely occurred,

88
00:04:17,325 --> 00:04:19,770
and then, what's the chance that A

89
00:04:19,770 --> 00:04:23,435
occurs given that B has definitely occurred?

90
00:04:23,435 --> 00:04:25,200
Which is that area

91
00:04:25,200 --> 00:04:27,330
of the overlap here where the question mark is.

92
00:04:27,330 --> 00:04:30,415
Well, that's the intersection of those two sets, A and B.

93
00:04:30,415 --> 00:04:33,595
All right? So that's simple enough.

94
00:04:33,595 --> 00:04:36,830
So let's try to bring this down to earth with

95
00:04:36,830 --> 00:04:40,290
a really silly simple example.

96
00:04:40,290 --> 00:04:43,545
Let's say, for an automobile I want to find out

97
00:04:43,545 --> 00:04:45,300
the chance that I'm going to have

98
00:04:45,300 --> 00:04:47,835
an accident because of brake failure.

99
00:04:47,835 --> 00:04:49,440
So my probability of

100
00:04:49,440 --> 00:04:51,640
accident given the condition of the brakes,

101
00:04:51,640 --> 00:04:53,130
and I'm going to write that again,

102
00:04:53,130 --> 00:04:56,770
p of A given B,

103
00:04:56,770 --> 00:05:01,455
and I can build a table and because,

104
00:05:01,455 --> 00:05:03,900
maybe I'm just going to categorize

105
00:05:03,900 --> 00:05:06,315
my brakes by three different possibilities.

106
00:05:06,315 --> 00:05:08,970
I'm going to say, "Okay the brakes are known to be good."

107
00:05:08,970 --> 00:05:12,195
So I have good brakes on this car so my probability of

108
00:05:12,195 --> 00:05:16,530
accident from brake problems is low.

109
00:05:16,530 --> 00:05:19,425
Not zero, because I could still have an accident

110
00:05:19,425 --> 00:05:23,770
even with the best brakes but it's low, hopefully.

111
00:05:23,770 --> 00:05:26,160
However, say my brakes need

112
00:05:26,160 --> 00:05:27,800
some kind of repair maintenance.

113
00:05:27,800 --> 00:05:30,210
They're not quite working as well as they should.

114
00:05:30,210 --> 00:05:32,520
Well, that increases my probability of

115
00:05:32,520 --> 00:05:35,430
having an accident become moderate.

116
00:05:35,430 --> 00:05:38,430
Let's say, the brakes have completely failed on

117
00:05:38,430 --> 00:05:40,980
this car and I'd be crazy to drive it,

118
00:05:40,980 --> 00:05:42,570
because my probability of having

119
00:05:42,570 --> 00:05:45,420
an accident given that the brakes do not work.

120
00:05:45,420 --> 00:05:49,840
So, my probability of A an accident,

121
00:05:49,840 --> 00:05:53,965
given B being brakes don't work, is very high.

122
00:05:53,965 --> 00:05:57,190
So, those are just some simple idea

123
00:05:57,190 --> 00:06:00,275
to help you wrap your head around conditional probability.

124
00:06:00,275 --> 00:06:04,630
So, a little bit more theory you need

125
00:06:04,630 --> 00:06:06,470
to understand this formulation is

126
00:06:06,470 --> 00:06:09,020
something called Bayes' theorem.

127
00:06:09,020 --> 00:06:11,680
So let's go back to that diagram

128
00:06:11,680 --> 00:06:14,330
and try to work that out in terms of Bayes' theorem.

129
00:06:14,330 --> 00:06:18,200
We're interested in this probability of A given B.

130
00:06:18,200 --> 00:06:22,160
We already said that the probability of A given B,

131
00:06:22,160 --> 00:06:24,110
is the probability of B times

132
00:06:24,110 --> 00:06:26,805
the intersection between B and A.

133
00:06:26,805 --> 00:06:28,580
Okay? Simple enough.

134
00:06:28,580 --> 00:06:32,375
But we could also have said

135
00:06:32,375 --> 00:06:37,010
the opposite is true and exactly equal, right?

136
00:06:37,010 --> 00:06:41,485
The probability of B given A is also the probability of

137
00:06:41,485 --> 00:06:47,900
A given that intersection of A and B, okay?

138
00:06:49,980 --> 00:06:54,870
So, we can set those equal to each other,

139
00:06:55,390 --> 00:07:01,220
and then we just simply divide here,

140
00:07:01,220 --> 00:07:03,295
divide through by the probability of B.

141
00:07:03,295 --> 00:07:05,595
We get probability B given A,

142
00:07:05,595 --> 00:07:09,360
is the probability of A given B times the probability A,

143
00:07:09,360 --> 00:07:11,620
divided by the probability B. I mean,

144
00:07:11,620 --> 00:07:13,700
it looks a little complicated but it's really

145
00:07:13,700 --> 00:07:15,890
just based on knowing that

146
00:07:15,890 --> 00:07:18,635
that probability of that event

147
00:07:18,635 --> 00:07:22,750
in that intersection has to be the same probability.

148
00:07:22,750 --> 00:07:24,650
Because it's the same set of events

149
00:07:24,650 --> 00:07:27,470
that both A and B have occurred, right?

150
00:07:27,470 --> 00:07:30,425
That's Bayes' theorem. That's all there is to it.

151
00:07:30,425 --> 00:07:33,015
So, it's not so mysterious.

152
00:07:33,015 --> 00:07:34,890
So how do we interpret

153
00:07:34,890 --> 00:07:38,870
this Bayes' theorem in

154
00:07:38,870 --> 00:07:41,800
terms we can use for machine learning?

155
00:07:41,800 --> 00:07:43,700
There is a specific way we can think about

156
00:07:43,700 --> 00:07:46,495
this that helps us with machine learning.

157
00:07:46,495 --> 00:07:48,520
Okay. So what does that mean?

158
00:07:48,520 --> 00:07:49,730
We say, the probability of

159
00:07:49,730 --> 00:07:52,940
some event given evidence equals

160
00:07:52,940 --> 00:07:55,520
the prior probability times the

161
00:07:55,520 --> 00:07:59,820
likelihood divided by the probability of that evidence.

162
00:07:59,820 --> 00:08:03,620
The likelihood is the probability.

163
00:08:03,620 --> 00:08:05,305
The evidence given the event,

164
00:08:05,305 --> 00:08:07,190
that is how likely was it that we'd

165
00:08:07,190 --> 00:08:09,390
observe those features given that,

166
00:08:09,390 --> 00:08:12,450
that event happened, so that's called the likelihood.

167
00:08:12,450 --> 00:08:14,880
And you can kind of see why it would be the likelihood,

168
00:08:14,880 --> 00:08:18,450
it's because there is that event could happen and

169
00:08:18,450 --> 00:08:20,110
with some other likelihood could

170
00:08:20,110 --> 00:08:22,170
have developed some other evidence,

171
00:08:22,170 --> 00:08:24,105
some other set of features.

172
00:08:24,105 --> 00:08:26,580
What we call the prior probability is

173
00:08:26,580 --> 00:08:30,230
the probability that event could have happened at all.

174
00:08:30,490 --> 00:08:35,260
So, let's now specifically try

175
00:08:35,260 --> 00:08:39,615
to formulate the Naive Bayes Classification Model.

176
00:08:39,615 --> 00:08:42,370
So, we're going to start with Bayes rule,

177
00:08:42,370 --> 00:08:44,630
and we're going to try to find

178
00:08:44,630 --> 00:08:50,535
the probability of some category or class C of k,

179
00:08:50,535 --> 00:08:53,730
given the evidence X, X,

180
00:08:53,730 --> 00:08:57,325
and so we write that in terms of Bayes rule

181
00:08:57,325 --> 00:09:00,060
as the probability of Ck.

182
00:09:00,060 --> 00:09:01,735
That is the category we're trying to

183
00:09:01,735 --> 00:09:04,030
predict in our classifier,

184
00:09:04,030 --> 00:09:07,540
given our features is the probability that

185
00:09:07,540 --> 00:09:12,375
that category occurs in the data.

186
00:09:12,375 --> 00:09:14,390
That may not be the same for

187
00:09:14,390 --> 00:09:17,805
different categories as we've already seen many times,

188
00:09:17,805 --> 00:09:23,600
times this likelihood that have giving that category,

189
00:09:23,600 --> 00:09:25,810
we would observe those features

190
00:09:25,810 --> 00:09:28,290
divided by the probability of those features.

191
00:09:28,290 --> 00:09:29,900
Well, it looks pretty hard to work with,

192
00:09:29,900 --> 00:09:32,400
but there's a lot of simplifications we're going to make

193
00:09:32,400 --> 00:09:33,910
here to get this down to

194
00:09:33,910 --> 00:09:36,765
a workable machine learning model.

195
00:09:36,765 --> 00:09:40,975
As I said, X is comprised of our features,

196
00:09:40,975 --> 00:09:43,870
which there's n features,

197
00:09:44,740 --> 00:09:48,855
and so let's start

198
00:09:48,855 --> 00:09:52,160
expanding this as a joint probability distribution.

199
00:09:52,160 --> 00:09:56,380
So, we've got the probability of this class and

200
00:09:56,380 --> 00:10:01,460
all the feature values X1 to Xn and it's just the same,

201
00:10:01,460 --> 00:10:03,850
and all I did was move

202
00:10:03,850 --> 00:10:06,510
this term to the end of the commas,

203
00:10:06,510 --> 00:10:08,550
so I haven't really changed anything there.

204
00:10:08,550 --> 00:10:11,040
I've just put it in a form that's a little

205
00:10:11,040 --> 00:10:12,350
easier to work with because I'm going

206
00:10:12,350 --> 00:10:13,945
to do something. I'm going to apply this.

207
00:10:13,945 --> 00:10:16,529
It's called the chain rule of probabilities,

208
00:10:16,529 --> 00:10:18,315
which is not to be confused with, say,

209
00:10:18,315 --> 00:10:19,630
the chain rule of calculus,

210
00:10:19,630 --> 00:10:21,685
but it's the chain rule of probabilities.

211
00:10:21,685 --> 00:10:24,335
So, that says I can rewrite

212
00:10:24,335 --> 00:10:29,060
this joint distribution as this.

213
00:10:29,060 --> 00:10:32,500
So, it's the conditional distribution of

214
00:10:32,500 --> 00:10:36,700
my features given that category, that event occurred,

215
00:10:36,700 --> 00:10:37,835
or that the category of

216
00:10:37,835 --> 00:10:42,430
Ck times the joint distribution of X2,

217
00:10:42,430 --> 00:10:49,020
X3 to Xn of Ck.

218
00:10:49,020 --> 00:10:50,510
I can keep expanding,

219
00:10:50,510 --> 00:10:52,930
so I've expanded once with the chain rule.

220
00:10:52,930 --> 00:10:55,600
I expand the second link on my chain.

221
00:10:55,600 --> 00:10:58,490
Now, I get the same term here,

222
00:10:58,490 --> 00:11:01,760
but now this one is the probability of X2, X3,

223
00:11:01,760 --> 00:11:05,490
et cetera to Xn and given that event,

224
00:11:05,490 --> 00:11:08,275
that Ck or that category k

225
00:11:08,275 --> 00:11:13,415
times the joint distribution of now X3 to Xn.

226
00:11:13,415 --> 00:11:17,325
Eventually, I get down to something like this,

227
00:11:17,325 --> 00:11:19,160
where I've reached the end of the chain.

228
00:11:19,160 --> 00:11:22,570
My last term is the probability of Xn,

229
00:11:22,570 --> 00:11:26,270
given that category Ck, okay?

230
00:11:29,320 --> 00:11:33,000
So, that's my product that I get from applying,

231
00:11:33,000 --> 00:11:35,320
recursively applying that chain rule

232
00:11:35,320 --> 00:11:37,930
to my joint distribution.

233
00:11:39,000 --> 00:11:41,835
But what happens? So, this

234
00:11:41,835 --> 00:11:44,895
is where this gets interesting.

235
00:11:44,895 --> 00:11:47,085
What happens if I say that

236
00:11:47,085 --> 00:11:53,440
all these features are statistically independent?

237
00:11:53,440 --> 00:11:58,420
Then the joint distribution just becomes this,

238
00:11:58,630 --> 00:12:01,850
and why was that? What happened here?

239
00:12:01,850 --> 00:12:06,220
So, this conditional probability of X1,

240
00:12:06,220 --> 00:12:08,745
X2, X3 to Xn given Ck.

241
00:12:08,745 --> 00:12:11,190
Well, if X1 doesn't depend on any

242
00:12:11,190 --> 00:12:13,645
of these other features,

243
00:12:13,645 --> 00:12:16,190
it's just going to be the probability of X1 given Ck.

244
00:12:16,190 --> 00:12:20,485
Likewise, for X2, X3 or Xn already,

245
00:12:20,485 --> 00:12:23,260
we had that from expanding by the chain rule.

246
00:12:23,260 --> 00:12:25,670
So, that is now saying because of

247
00:12:25,670 --> 00:12:28,225
statistical independence of our features,

248
00:12:28,225 --> 00:12:30,545
this whole big product that

249
00:12:30,545 --> 00:12:32,840
was kind of complicated to say the least,

250
00:12:32,840 --> 00:12:34,700
now becomes really something simple,

251
00:12:34,700 --> 00:12:39,000
it's just the product of these conditional probabilities.

252
00:12:39,000 --> 00:12:40,880
The conditional probabilities being

253
00:12:40,880 --> 00:12:42,450
the probability of observing

254
00:12:42,450 --> 00:12:47,490
a label or a future value given a label category.

255
00:12:48,300 --> 00:12:50,750
So, we can pull this together,

256
00:12:50,750 --> 00:12:54,360
the chain rule in Bayes theorem,

257
00:12:54,360 --> 00:12:57,245
and what we really want to know is this,

258
00:12:57,245 --> 00:13:01,075
the probability of a category

259
00:13:01,075 --> 00:13:05,930
of the label category Ck given the features we observed,

260
00:13:05,930 --> 00:13:07,875
and we get something like this.

261
00:13:07,875 --> 00:13:11,700
So, 1/Z, which we don't really need to worry about,

262
00:13:11,700 --> 00:13:14,340
the probability of that category

263
00:13:14,340 --> 00:13:18,290
occurring and the product of

264
00:13:18,290 --> 00:13:22,950
these conditional probabilities of

265
00:13:22,950 --> 00:13:27,235
observing the label values given that category.

266
00:13:27,235 --> 00:13:30,260
Z is a normalization, you get that from Bayes rule,

267
00:13:30,260 --> 00:13:32,170
but we actually don't care about it because

268
00:13:32,170 --> 00:13:34,395
the most likely case can just be written.

269
00:13:34,395 --> 00:13:37,230
Z is the same for all categories,

270
00:13:37,230 --> 00:13:39,100
so we can just ignore that,

271
00:13:39,100 --> 00:13:42,815
and just take the max of this bit here.,

272
00:13:42,815 --> 00:13:44,545
and that's our classifier.

273
00:13:44,545 --> 00:13:48,660
Whichever category Ck maximizes,

274
00:13:48,660 --> 00:13:52,920
this product is the most likely category

275
00:13:52,920 --> 00:13:54,805
according to Bayes rule,

276
00:13:54,805 --> 00:13:59,365
and given the assumption of independence of the feature,

277
00:13:59,365 --> 00:14:02,355
so that's the Naive Bayes Classifier.

278
00:14:02,355 --> 00:14:05,705
But there's some problems here, some pitfalls,

279
00:14:05,705 --> 00:14:09,630
the most common one is if you have very sparse data,

280
00:14:09,630 --> 00:14:11,470
which is often the case why maybe you're

281
00:14:11,470 --> 00:14:13,950
using a base classifier anyway.

282
00:14:13,950 --> 00:14:17,160
What happens to this product

283
00:14:17,160 --> 00:14:21,900
if you get a small probability?

284
00:14:21,900 --> 00:14:25,330
So, if you're multiplying

285
00:14:25,330 --> 00:14:27,175
a bunch of small numbers before,

286
00:14:27,175 --> 00:14:29,620
even in a computer that's got 64

287
00:14:29,620 --> 00:14:32,510
bit or 128 bit arithmetic,

288
00:14:32,510 --> 00:14:34,445
you're going to get underflow.

289
00:14:34,445 --> 00:14:35,910
It's inevitable, and this is

290
00:14:35,910 --> 00:14:38,095
the problem whenever you work with probabilities.

291
00:14:38,095 --> 00:14:40,205
In fact, whenever you work with probabilities,

292
00:14:40,205 --> 00:14:42,960
you actually want to work with the log probabilities.

293
00:14:42,960 --> 00:14:46,030
So, we can take the log here,

294
00:14:46,030 --> 00:14:52,130
and because the log of a product is the sum of the logs.

295
00:14:52,130 --> 00:14:56,510
Even if some of these terms are very close to zero,

296
00:14:56,510 --> 00:15:00,935
we can add them up and not have numerical underflow,

297
00:15:00,935 --> 00:15:03,250
so that's one trick.

298
00:15:04,460 --> 00:15:07,660
The other problem is what if one of

299
00:15:07,660 --> 00:15:10,380
these conditional probabilities is actually zero,

300
00:15:10,380 --> 00:15:14,150
then that product, the logarithm is not to find,

301
00:15:14,150 --> 00:15:16,460
that product blows up?

302
00:15:16,750 --> 00:15:19,305
It turns out Laplace,

303
00:15:19,305 --> 00:15:20,820
the famous French mathematician,

304
00:15:20,820 --> 00:15:22,420
hit this very problem

305
00:15:22,420 --> 00:15:24,420
200 years ago and

306
00:15:24,420 --> 00:15:26,990
fortunately came up with a simple solution,

307
00:15:26,990 --> 00:15:32,145
which is to simply smooth over the distributions,

308
00:15:32,145 --> 00:15:35,120
so that you can get close to zero,

309
00:15:35,120 --> 00:15:38,175
but never actually get a zero probability.

310
00:15:38,175 --> 00:15:42,625
And this so-called Laplace smoother fixes that problem

311
00:15:42,625 --> 00:15:45,295
in almost every statistical package

312
00:15:45,295 --> 00:15:47,770
where you use Naive Bayes models.

313
00:15:47,770 --> 00:15:50,740
There's some smoother parameter you

314
00:15:50,740 --> 00:15:53,880
can play with to set how much smoothing you might need,

315
00:15:53,880 --> 00:15:57,410
given the statistics of your particular dataset.

316
00:15:57,410 --> 00:16:03,720
So, in summary, Naive Bayes models really work well,

317
00:16:03,720 --> 00:16:06,890
despite the naive assumption of statistical independence.

318
00:16:06,890 --> 00:16:10,740
They're particularly effective in cases where you

319
00:16:10,740 --> 00:16:12,615
don't have a lot of training data compared

320
00:16:12,615 --> 00:16:15,790
to the dimensionality of the dataset,

321
00:16:16,100 --> 00:16:19,340
and the method is highly scalable.

322
00:16:19,340 --> 00:16:21,240
But, it does break down if

323
00:16:21,240 --> 00:16:22,950
you have strong correlations or

324
00:16:22,950 --> 00:16:26,925
some other covariance between your features,

325
00:16:26,925 --> 00:16:28,500
the model is going to break down,

326
00:16:28,500 --> 00:16:30,090
so keep that in mind.

327
00:16:30,090 --> 00:16:31,890
It works really great in some cases,

328
00:16:31,890 --> 00:16:35,200
but it's not a one size fits all.

