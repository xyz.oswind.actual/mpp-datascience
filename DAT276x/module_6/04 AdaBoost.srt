0
00:00:05,170 --> 00:00:10,389
Let’s go over the adaboost algorithm and show you an example. Now the way adaboost

1
00:00:10,389 --> 00:00:15,200
works is by reweighting the data over and over again so it gets a lot of different results

2
00:00:15,200 --> 00:00:20,360
out of the weak learning algorithm each time. So I am going to do the example but I want

3
00:00:20,360 --> 00:00:26,000
to show you what the pseudocode looks like first. So we start with each observation having

4
00:00:26,000 --> 00:00:35,019
equal weights, just 1/n and then we are going to do a loop. Inside the loop, we will train

5
00:00:35,019 --> 00:00:39,940
the weak learning algorithm, using the data weighted by the weights that we assigned.

6
00:00:39,940 --> 00:00:46,549
And this produce what we call the weak classifier. And then we are going to choose a coefficient

7
00:00:46,549 --> 00:00:51,320
which is just a number and the then we are going to update all of the weights in all

8
00:00:51,320 --> 00:00:57,129
of the observations. So they are not going to be equal in the next round. And here is

9
00:00:57,129 --> 00:01:05,540
the weight update stuff. And I will show you the intuition behind that soon. And then this

10
00:01:05,540 --> 00:01:10,479
guy over here which is part of the weight update stuff just to warn you this thing is

11
00:01:10,479 --> 00:01:17,259
either 1 or -1. It is 1 if the weak classifier got the observation right and it is -1 I it

12
00:01:17,259 --> 00:01:21,350
got it wrong. Okay so the weights are going to be different depending on whether the weak

13
00:01:21,350 --> 00:01:26,060
classifier got it correct or whether it was wrong. And then this Zt here is actually very

14
00:01:26,060 --> 00:01:29,829
boring. It’s just a normalization factor that just makes sure that all the weights

15
00:01:29,829 --> 00:01:39,700
add up to 1. And then in the end, after you have done this T times, you output the final

16
00:01:39,700 --> 00:01:49,200
classifier, which is the sign of the sum of the weighted sum of the weak classifiers that

17
00:01:49,200 --> 00:01:52,859
you got in our loop.

18
00:01:52,859 --> 00:01:58,439
So let’s do an example. Now our weak classifiers are only allowed to be lines that are either

19
00:01:58,439 --> 00:02:05,079
horizontal or vertical in this example. And all the observations start with equal weights.

20
00:02:05,079 --> 00:02:09,970
Okay. So I am going to run the learning algorithm, the weak learning algorithm, to get a weak

21
00:02:09,970 --> 00:02:15,390
classifier. And this is a pretty you now mediocre classifier. It classified these two points

22
00:02:15,390 --> 00:02:25,709
a positive. And it classified everything else as negative. Okay and which points were misclassified?

23
00:02:25,709 --> 00:02:30,569
These three appear to be misclassified. Now I have to choose the coefficient for this

24
00:02:30,569 --> 00:02:35,349
weak classifier, which is going to determine how much it contributes to the final combined

25
00:02:35,349 --> 00:02:42,980
classifier. And I will tell you how it get it officially soon. But I will tell you now

26
00:02:42,980 --> 00:02:48,700
this weight depends, this coefficient depends on how well our classifier performed on that

27
00:02:48,700 --> 00:02:55,709
round. So here the weak classifier missed three points that had reasonable weights.

28
00:02:55,709 --> 00:02:59,819
So won’t get a high coefficient, and the value of the coefficient of 0.41. That’s

29
00:02:59,819 --> 00:03:07,040
not a very high number. Now what we are going to do is increase the weights on the misclassified

30
00:03:07,040 --> 00:03:14,090
points. Okay so these three are misclassified and we will decrease the rates on the correctly

31
00:03:14,090 --> 00:03:19,769
classified points. So all these points were correctly classified. And see I am using the

32
00:03:19,769 --> 00:03:29,069
size of the plus or minus to illustrate the weights, - the d-weights. Okay now because

33
00:03:29,069 --> 00:03:34,980
the weights on those points are so high the weak learning algorithm basically has to get

34
00:03:34,980 --> 00:03:36,670
those three points right on the next round.

35
00:03:36,670 --> 00:03:43,870
Okay so let’s do the next round. Okay so yeah, it got these three points right this

36
00:03:43,870 --> 00:03:50,819
time. But it sacrificed these three negatives below it. See this time the coefficient is

37
00:03:50,819 --> 00:03:56,890
a little bit higher because it got these three positives right that high weight. And now

38
00:03:56,890 --> 00:04:01,750
again we are going to adjust the weights. And the weights on these misclassified negatives

39
00:04:01,750 --> 00:04:06,329
are going to go up and the weight son all the positives and these two negatives are

40
00:04:06,329 --> 00:04:17,820
going to go down. Okay and you can keep doing this as much as you want. So one more time.

41
00:04:17,820 --> 00:04:22,140
We will call the weak learning algorithm to give us a weak classifier. And now it got

42
00:04:22,140 --> 00:04:26,600
all the high weighted points correct so this classifier gets to contribute a lot to the

43
00:04:26,600 --> 00:04:32,130
final combined one because all these high weighted points got classified correctly.

44
00:04:32,130 --> 00:04:37,380
And then after three rounds of boosting, well, we are going to stop. And here is the final

45
00:04:37,380 --> 00:04:43,510
combined classifier, which is 0.42 times this guy + 0.66 times that guy and 0.93 times this

46
00:04:43,510 --> 00:04:51,890
guy. And if you put that all together, here is the final combined classifier and lo and

47
00:04:51,890 --> 00:04:59,290
behold, it classified all the points correctly. Isn’t that neat? So let, e tell you a little

48
00:04:59,290 --> 00:05:05,700
bit more about why it works. In particular, let’s talk about that mysterious alpha weight.

49
00:05:05,700 --> 00:05:12,600
The alpha looks at how good the classifier is at that round. In other words, it looks

50
00:05:12,600 --> 00:05:18,720
at the error that the classifier makes. So in our case, since the points are weighted,

51
00:05:18,720 --> 00:05:23,720
the error for the weak classifier round t is just the sum of weights of the misclassified

52
00:05:23,720 --> 00:05:30,310
points. So intuitively if the error is large we are going to want alpha to be small so

53
00:05:30,310 --> 00:05:36,530
that we don’t pay much attention to that classifier. And now here’s the formula for

54
00:05:36,530 --> 00:05:42,600
alpha. SO if I plot alpha there as a function of the error, you can see that as the error

55
00:05:42,600 --> 00:05:49,080
grows, the alpha is going to be smaller, which means we will pay less attention to the weak

56
00:05:49,080 --> 00:05:54,880
classifiers with larger error. Okay so that’s the remaining formula that I need to show

57
00:05:54,880 --> 00:06:01,780
you and so now hopefully, you understand the basics behind adaboost. Now all of that is

58
00:06:01,780 --> 00:06:07,430
under the hood. All you need to do is provide adaboost with the data, tell it which weak

59
00:06:07,430 --> 00:06:11,280
learning algorithm you want it to use, and how many rounds of boosting you want it to

60
00:06:11,280 --> 00:06:17,100
do. And everything is done in a way that you never need to program it. But you know how

61
00:06:17,100 --> 00:06:23,380
it works. So what I am going to do in the next segment is give you a different perspective

62
00:06:23,380 --> 00:06:25,740
on adaboost namely the coordinate descent perspective.

