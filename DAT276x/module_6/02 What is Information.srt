0
00:00:05,060 --> 00:00:11,600
So information is determined by observing the occurrence of an event. It’s actually

1
00:00:11,600 --> 00:00:18,199
the number of bits needed to encode the probability of the event. In other words if an event has

2
00:00:18,199 --> 00:00:28,550
probability p we need negative log 2 p bits. For instance a coin flip from a fair coin

3
00:00:28,550 --> 00:00:36,230
encodes 1 bit of information. The probability of success is one half so negative log of

4
00:00:36,230 --> 00:00:41,950
one half is actually 1 bit.

5
00:00:41,950 --> 00:00:46,380
Now for an even that has a probability of 1, we don’t need any bits. Because if you

6
00:00:46,380 --> 00:00:53,530
plug p is 1 in this thing –log (1) is 0 bits. Of course that makes total sense. Because

7
00:00:53,530 --> 00:01:00,310
even before we flip the coin already know the results. So we don’t need any information.

8
00:01:00,310 --> 00:01:07,369
Now if we had many events with probability p 1 to pm what is their mean information?

9
00:01:07,369 --> 00:01:12,229
Okay so I have written here expected information is. And you know what expectations because

10
00:01:12,229 --> 00:01:17,609
we covered it in the data science course. And this is just the definition of what an

11
00:01:17,609 --> 00:01:24,460
expectation is so that’s fine. So you just weight the information values by their probability

12
00:01:24,460 --> 00:01:29,200
of tier occurring the same way you compute any average And now I have just written here

13
00:01:29,200 --> 00:01:58,159
the definition and  the definition is actually like this. All I did was plug P in this formula and that’s

14
00:01:58,159 --> 00:02:05,630
it like this. So if probability is half and half the entropy is just 1. We did that because

15
00:02:05,630 --> 00:02:11,190
we know that the average information from a coin flip is just 1 bit of information.

16
00:02:11,190 --> 00:02:20,010
But if the probability are .099 and .01 actually the entropy is 0.008 bits. The entropy is

17
00:02:20,010 --> 00:02:25,069
much smaller. Any time we have a very low probability in there the entropy is going

18
00:02:25,069 --> 00:02:26,470
to be really low.

19
00:02:26,470 --> 00:02:33,379
So what to relates this to your sort of usual intuition about entropy. I think you know

20
00:02:33,379 --> 00:02:39,390
the idea of entropy from physics. If things are smoother out then the entropy is higher.

21
00:02:39,390 --> 00:02:44,390
Now for probability, higher entropies mean the probabilities are flatter they are closer

22
00:02:44,390 --> 00:02:49,730
to uniform. They are more spread out if we are having extremely rare events on the other

23
00:02:49,730 --> 00:02:53,260
hand, that would not be high entropy.

24
00:02:53,260 --> 00:02:58,890
So if the probability are flat here, the entropy is close to 1. That really makes sense with

25
00:02:58,890 --> 00:03:04,049
the intuition that entropy is larger when this are spread to and the entropy is less

26
00:03:04,049 --> 00:03:11,099
when things are not so spread out. Let’s actually look at the entropy of p and 1-p.

27
00:03:11,099 --> 00:03:18,370
So as we saw in the last couple of slides, this should be larger when p is close to i/2

28
00:03:18,370 --> 00:03:24,219
and should be smaller when p is close to 0 or close to 1. And check it out, that’s

29
00:03:24,219 --> 00:03:31,689
exactly what happens. You can see that one p is close to 0 or 1 than the entropy is small

30
00:03:31,689 --> 00:03:34,159
and when p is a half entropy is 1/

31
00:03:34,159 --> 00:03:40,900
Okay so no that u know what information is and u know that entropy is average information.

32
00:03:40,900 --> 00:03:47,819
Let’s find information gain. Which is the splitting criteria for decision trees. So

33
00:03:47,819 --> 00:03:51,420
let’s say that you are on a particular node on the tree and the rest of tree is up there

34
00:03:51,420 --> 00:03:57,749
somewhere and you look at what data comes into the branch of that tree. And that right

35
00:03:57,749 --> 00:04:03,890
the training data going to this branch, now the number of positives we have into the training

36
00:04:03,890 --> 00:04:07,609
data is here is that and the number of negatives is there.

37
00:04:07,609 --> 00:04:16,140
Now let’s say that we consider splitting on a variable called color. Okay? Color. So

38
00:04:16,140 --> 00:04:23,810
this color is called A, and it has values white, green, beige, and blue. And we can

39
00:04:23,810 --> 00:04:28,880
see that if we split on this variable we can count the number of positive and be negative

40
00:04:28,880 --> 00:04:37,560
so each color that falls into each of these branches. And we can write down the ratios

41
00:04:37,560 --> 00:04:43,440
of positives and negatives in each branch. So here in branch j right there, this is a

42
00:04:43,440 --> 00:04:51,220
fraction of observations that are positive and this is the fraction that are negative.

43
00:04:51,220 --> 00:04:57,990
We can look at them now remember that if we get probabilities like this, that means this

44
00:04:57,990 --> 00:05:02,740
branch is good it means that we pretty much know what the labels are going to be on the

45
00:05:02,740 --> 00:05:10,310
observation for the branch but this would be really bad because our cross wire in that

46
00:05:10,310 --> 00:05:15,240
case is going to be half right no matter whether we assign the label on the bridge to be positive

47
00:05:15,240 --> 00:05:20,750
or negative. Now if the entropy is smaller when we do this

48
00:05:20,750 --> 00:05:29,050
split and if don’t then it is a good split. Now we can define information gain.

49
00:05:29,050 --> 00:05:36,150
So information gain for a particular subset of data in a particular branch on variable

50
00:05:36,150 --> 00:05:44,610
A is the expected reduction in entropy due to branching on variable A. So it the original

51
00:05:44,610 --> 00:05:54,080
entropy before you branch minus the entropy after you branch. So you compute the entropy

52
00:05:54,080 --> 00:06:00,660
before you branch and then you compute the vaergae all the entropies after you branch

53
00:06:00,660 --> 00:06:08,520
and take the average of them. And the difference is information gain. So let’s do an example

54
00:06:08,520 --> 00:06:10,169
here.

55
00:06:10,169 --> 00:06:15,990
SO for instance if we split on the variable guests in a restaurant example. We started

56
00:06:15,990 --> 00:06:20,910
with half the data being positive and half the data being negative. So the entropy was

57
00:06:20,910 --> 00:06:28,580
1 and then when we split we computed the average entropy of the split and it is much lower

58
00:06:28,580 --> 00:06:36,300
right? Because the information gain over here is significant, so you can see that intuitively

59
00:06:36,300 --> 00:06:43,210
because each branching for guests is much better than random guessing. So two of the

60
00:06:43,210 --> 00:06:49,440
branches are perfect and this is not quite perfect but better than random, so the information

61
00:06:49,440 --> 00:06:58,159
gain is substantial there now we can split on genre and if you recall genre is actually

62
00:06:58,159 --> 00:07:06,560
genre is actually really lousy to split because at each node or each option of genre half

63
00:07:06,560 --> 00:07:09,659
the data ae positive and half the data are negative’

64
00:07:09,659 --> 00:07:16,569
So the entropy is 1 at each sub node here so there is no reduction in entropy at all

65
00:07:16,569 --> 00:07:24,669
we started with an entropy of 1 and the entropy when you split is 1. So 1-1 is 0. SO the information

66
00:07:24,669 --> 00:07:29,080
gain for splitting on genre is exactly 0. So let’s choose to split on guests instead

67
00:07:29,080 --> 00:07:33,220
as that works much better. Now the standard way to build a decision tree then just to

68
00:07:33,220 --> 00:07:41,250
summarize is to start at the top of the tree, you grow it by splitting features one by one

69
00:07:41,250 --> 00:07:47,690
and to split, you look at how impure the node. You look at the information gain and then

70
00:07:47,690 --> 00:07:54,379
once you decide to split to assign the label you just assign each leaf to the majority

71
00:07:54,379 --> 00:08:01,039
vote of the observations that fall into that leaf. And then at the end, you go back and

72
00:08:01,039 --> 00:08:05,830
you actual y rune the leaves to try to get rid of some of the extra eaves to try and

73
00:08:05,830 --> 00:08:07,419
reduce overfitting.

74
00:08:07,419 --> 00:08:13,270
Now what I spend most of my time on in this tis lecture was in telling you abbot the splitting

75
00:08:13,270 --> 00:08:21,419
criteria for the algorithm c4.5 which is one of the top 10 algorithms in data mining.

76
00:08:21,419 --> 00:08:26,759
CART is a different decision tree algorithm it uses a similar criteria called the Gini

77
00:08:26,759 --> 00:08:31,909
index. Once you have one splitting criteria you kind of get the idea they are all similar

78
00:08:31,909 --> 00:08:38,539
to information gain and they all have the same effect. Now there is also the matter

79
00:08:38,539 --> 00:08:45,300
of pruning. And I chose not to get too much detail on that since I went into so much detail

80
00:08:45,300 --> 00:08:51,640
on the splitting. SO basically the pruning involves eliminating the sub trees that gives

81
00:08:51,640 --> 00:08:56,180
the smallest bang for the buck. These are the parts of the tress that are more complicated

82
00:08:56,180 --> 00:09:00,350
which won’t reduce the errors very much. Now these are the ones you want to prune.

83
00:09:00,350 --> 00:09:06,380
Now there are decision trees for the problem of regression, what I was talking about all

84
00:09:06,380 --> 00:09:12,100
along has been classification. Noe for regression they use different criterial for splitting

85
00:09:12,100 --> 00:09:21,290
and pruning but they have the same benefits if the tress for regression as for classification.

86
00:09:21,290 --> 00:09:28,310
So lets review all of these benefits. Decision trees handle non linearity because they are

87
00:09:28,310 --> 00:09:36,310
logical models,] they are generally interpretable or at least CART is. C4.5 not always so interpretable

88
00:09:36,310 --> 00:09:44,390
but CART tends to produce for reliably interpretable models. Now decision tress are very greedy

89
00:09:44,390 --> 00:09:49,360
because they do everything from top down. SO they are very fast. So once you have picked

90
00:09:49,360 --> 00:09:54,740
out the first node you can go on to the second node and so on without worrying about what’s

91
00:09:54,740 --> 00:09:59,380
going on back at the first node because you have already done that. Also decision tress

92
00:09:59,380 --> 00:10:06,170
trees handle imbalanced data pretty easily by reweighting. That’s no problem you can

93
00:10:06,170 --> 00:10:12,920
always tell it to work within imbalanced data. The results are not always so great but at

94
00:10:12,920 --> 00:10:21,680
least there’s a way to handle it. Now let’s look about the disadvantages of decision trees.

95
00:10:21,680 --> 00:10:26,610
They tend not to be very interpretable without trading off accuracy So eve CART when you

96
00:10:26,610 --> 00:10:30,370
ask it to produce an interpretable model it won’t product something that s particularly

97
00:10:30,370 --> 00:10:38,730
accurate. Because since decision trees aren’t actually optimized or anything they are not

98
00:10:38,730 --> 00:10:46,690
really optimized for accuracy or interoperability. So trying to get them bot is somewhat difficult

99
00:10:46,690 --> 00:10:51,240
also since decision trees ae greedy methods they tend generally to be less accurate compared

100
00:10:51,240 --> 00:10:55,800
to other methods that are actually optimized for accuracy.

101
00:10:55,800 --> 00:11:05,779
And decision trees are heuristic, they not particularly elegant. They don’t truly optimized

102
00:11:05,779 --> 00:11:12,130
for anything so the algorithm is itself not particularly beautiful. And as I mentioned

103
00:11:12,130 --> 00:11:16,100
they tend to do pretty poorly when you have imbalanced data they tend to go from trivial

104
00:11:16,100 --> 00:11:20,230
models predicting all one class to trivial models predicting all the other class, which

105
00:11:20,230 --> 00:11:26,570
is not that great that seven when you adjust all the parameters. Anyway, as you can tell

106
00:11:26,570 --> 00:11:31,300
I really dislike decision trees. My research group has been working for years on alternatives

107
00:11:31,300 --> 00:11:37,899
to decision trees that are not greedy not heuristic and are nicely accurate. And maybe

108
00:11:37,899 --> 00:11:42,839
I will get to teach you about those methods when we get out of the introductory levels.

109
00:11:42,839 --> 00:11:48,130
In any case, use decision trees but beware.

110
00:11:48,130 --> 00:11:55,250
Now one major advantage of decision trees that I will talk about shortly is that you

111
00:11:55,250 --> 00:12:00,949
can boost them. In other words, you can combine whole bunch of not so accurate decision trees

112
00:12:00,949 --> 00:12:05,980
to produce a combined model that actually is very accurate.

