0
00:00:05,319 --> 00:00:09,860
Now we’re going to talk about decision forests. Decision forests are a type of ensemble method

1
00:00:09,860 --> 00:00:13,700
like boosting, where you combine a whole bunch of weak class forests to build something that’s

2
00:00:13,700 --> 00:00:18,910
really strong. Okay, so let’s say that you want to know whether you should get surgery,

3
00:00:18,910 --> 00:00:24,590
and you ask your doctor and she says that you should get it. So do you trust that? Right,

4
00:00:24,590 --> 00:00:28,560
even if the doctor is good, you might not trust that one doctor; you might want to,

5
00:00:28,560 --> 00:00:34,570
you know, get ten equally good doctors all trained at different places perhaps, and you

6
00:00:34,570 --> 00:00:38,390
would ask all of them if you should get surgery and let’s say eight of them say you should

7
00:00:38,390 --> 00:00:42,850
get it, well, that’s a majority vote over all those doctors that that might be much

8
00:00:42,850 --> 00:00:47,559
more trustworthy than simply just asking one doctor. And that’s the whole idea behind

9
00:00:47,559 --> 00:00:55,489
decision forests. Now decision forests are a complex and powerful

10
00:00:55,489 --> 00:01:01,090
prediction tool. It’s a black-box method, meaning that the models you get are not interpretable

11
00:01:01,090 --> 00:01:06,450
because they’re a combination of lots and lots of other models. And they use a similar

12
00:01:06,450 --> 00:01:11,530
idea to boosted decision trees; if you average many uncorrelated yet accurate models, that

13
00:01:11,530 --> 00:01:16,080
helps to reduce variance; it’s like asking lots of doctors for their opinions and then

14
00:01:16,080 --> 00:01:24,310
using the majority vote. So let’s go back to the example of customers waiting for a

15
00:01:24,310 --> 00:01:31,520
table at a restaurant. Now remember from the decision tree lecture that these are the different

16
00:01:31,520 --> 00:01:41,180
features in the example. So let’s say that we have three decision trees for the restaurant

17
00:01:41,180 --> 00:01:45,600
problem, and let’s say that these are computed on three different subsets of data, so they’re

18
00:01:45,600 --> 00:01:51,680
just three different trees, and then let’s say we have a new observation. Now the people

19
00:01:51,680 --> 00:01:56,040
are at a Mexican restaurant; it’s – you know, these are the features, it’s a full

20
00:01:56,040 --> 00:02:00,960
restaurant, it’s sort of medium expensive, there’s a 5 to 15 minute wait, the person

21
00:02:00,960 --> 00:02:07,350
doesn’t have any plans afterwards, and there are no other restaurants in the nearby area.

22
00:02:07,350 --> 00:02:10,910
So what do the decision trees think about this? Will the people wait or will they go

23
00:02:10,910 --> 00:02:19,799
somewhere else? Well, this decision tree says yeah, they’re going to wait, okay, because

24
00:02:19,799 --> 00:02:27,579
they don’t have any plans after the restaurant’s – or they don’t have any plans afterward,

25
00:02:27,579 --> 00:02:33,709
and it’s not that expensive so we’ll wait. And this decision tree says yes, because the

26
00:02:33,709 --> 00:02:39,880
5 – the wait time is only 5 to 15 minutes, so they’ll wait. But this decision tree

27
00:02:39,880 --> 00:02:46,769
says no, they’re not going to wait; in fact, they tend not to wait for Mexican restaurants

28
00:02:46,769 --> 00:02:53,040
on days when they’re crowded and even though there are no other options. Okay, so what

29
00:02:53,040 --> 00:02:56,419
we’re going to do is something super simple; just take the majority vote. We’re going

30
00:02:56,419 --> 00:03:03,690
to say that the answer is yes, they’ll wait. And so that’s what exactly the idea is behind

31
00:03:03,690 --> 00:03:07,400
decision forests; you create a bunch of trees from subsets of data, and then you take the

32
00:03:07,400 --> 00:03:15,400
majority vote. So let me be more precise: I need to define what a bootstrap sample is,

33
00:03:15,400 --> 00:03:23,479
okay, it is a sample of n points that are drawn with replacement at random from the

34
00:03:23,479 --> 00:03:28,590
training data. Okay, so you draw them one at a time, and you draw them with replacement,

35
00:03:28,590 --> 00:03:33,370
which means that sometimes you’re going to have some repeated points, and that’s

36
00:03:33,370 --> 00:03:40,680
okay. So here’s the algorithm: for each time,

37
00:03:40,680 --> 00:03:47,579
you draw a bootstrap sample of size n from the training data. Then you grow a decision

38
00:03:47,579 --> 00:03:58,310
tree that’s called treet, using this splitting and stopping procedure: you choose m features

39
00:03:58,310 --> 00:04:02,570
at random out of the p – so in other words you’re not actually considering all of the

40
00:04:02,570 --> 00:04:07,259
features even, you’re not considering all of the data, right, you have this bootstrap

41
00:04:07,259 --> 00:04:11,229
sample and then you’re not even considering all the features, you just have m of them

42
00:04:11,229 --> 00:04:18,680
at random out of the total of p. And then, you evaluate the splitting criteria on all

43
00:04:18,680 --> 00:04:22,680
of the features – all of the m features that you’re allowed to use, and you split

44
00:04:22,680 --> 00:04:33,630
on the best one. And then you keep doing that over and over again until the node has a very

45
00:04:33,630 --> 00:04:42,020
small number of observations in it. And then in that case, you stop splitting. And then

46
00:04:42,020 --> 00:04:48,240
at the end, you output all of the trees that you’ve constructed this way and you’ll

47
00:04:48,240 --> 00:04:54,940
notice that there’s no pruning and that there’s only splitting. And then to make

48
00:04:54,940 --> 00:05:00,620
a prediction on a new observation, you just use the majority vote of the trees that you’ve

49
00:05:00,620 --> 00:05:06,680
constructed on that new observation. Okay so let’s just compare with decision

50
00:05:06,680 --> 00:05:14,460
trees themselves. So decision trees use all the data whereas decision forests use bootstrap

51
00:05:14,460 --> 00:05:20,070
resamples to construct lots of decision trees, and decision trees are allowed to use all

52
00:05:20,070 --> 00:05:27,130
of the features at each split to consider for each split, whereas decision forests only

53
00:05:27,130 --> 00:05:33,530
consider m randomly chosen features for each split. And the whole effect to both of those

54
00:05:33,530 --> 00:05:40,610
things is to make the trees look kind of diverse, which hopefully reduces variance; it’s like

55
00:05:40,610 --> 00:05:47,780
trying to get doctors from different backgrounds to be your expert. Decision forests do not

56
00:05:47,780 --> 00:05:53,140
use pruning, and that helps make the trees fit more tightly to the data, and it reduces

57
00:05:53,140 --> 00:05:58,430
bias. And obviously, we don’t care about interpret ability here because this is a giant

58
00:05:58,430 --> 00:06:06,220
combination of complicated models. And then the majority vote of several trees is used

59
00:06:06,220 --> 00:06:12,770
to make predictions; it’s not just one tree. Okay, so what’s up with these choices? So

60
00:06:12,770 --> 00:06:17,580
the reason for the bootstrap samples and considering that just m features, that’s to make all

61
00:06:17,580 --> 00:06:21,150
the trees look different from each other. So if you have a lot of complicated yet diverse

62
00:06:21,150 --> 00:06:25,360
models and more than half of them agree on the prediction, then that’s what we want

63
00:06:25,360 --> 00:06:29,910
to use, and that’s it. That’s the full explanation of decision forests. It’s super

64
00:06:29,910 --> 00:06:36,590
simple, yet super powerful. So let’s take a step back for a second and

65
00:06:36,590 --> 00:06:41,380
talk about something that comes naturally with decision forests, which is a kind of

66
00:06:41,380 --> 00:06:46,270
notion of variable importance. It’s like a neat way to just measure how important a

67
00:06:46,270 --> 00:06:54,750
variable is. Okay, so we’re going to measure the importance of feature j, and here is the

68
00:06:54,750 --> 00:07:00,950
instructions on how to do it. We’re going to take the data that wasn’t used to construct

69
00:07:00,950 --> 00:07:11,590
a particular tree that we created at time t, and remember that the data that are used

70
00:07:11,590 --> 00:07:16,560
in this decision forest algorithm is not the full dataset, we’re only using a subset

71
00:07:16,560 --> 00:07:21,490
that’s chosen randomly. So there are going to be points that are not used to construct

72
00:07:21,490 --> 00:07:28,210
any given tree, okay? So we’ll call this data not used to construct the tree out-of-bag

73
00:07:28,210 --> 00:07:37,560
at time t. And then we’ll compute the error of that out-of-bag data on the tree that was

74
00:07:37,560 --> 00:07:45,260
constructed with the rest of the data. And now we’re going to do something really weird,

75
00:07:45,260 --> 00:07:55,740
which is to take the data and randomly permute only the jth feature values, okay, what the

76
00:07:55,740 --> 00:08:04,490
heck does that mean? So what I mean is you take the data and you just randomly permute

77
00:08:04,490 --> 00:08:10,510
the jth feature value, so you just reorder them at random. It seems really weird, huh?

78
00:08:10,510 --> 00:08:15,139
Right, each observation comes with its jth measurement, and now I’m randomly permuting

79
00:08:15,139 --> 00:08:19,919
them? Yes, that’s what I’m doing. The philosophy is that if the feature wasn’t

80
00:08:19,919 --> 00:08:25,210
important anyway, then even if I assign the feature values randomly, then the error shouldn’t

81
00:08:25,210 --> 00:08:31,180
really change very much. Okay, and you can see that they’ve been permuted here; they’re

82
00:08:31,180 --> 00:08:37,070
not what they’re supposed to be, and then we’ll call this dataset the out-of-bag data

83
00:08:37,070 --> 00:08:45,700
that’s permuted at time t. And then we’ll compute the error of this weird permuted out-of-bag

84
00:08:45,700 --> 00:08:58,470
data using the tree t – using that tree t. Okay so now we have a way to measure error

85
00:08:58,470 --> 00:09:03,920
with feature j intact, which is that guy, and with feature j all messed up, which is

86
00:09:03,920 --> 00:09:12,420
this guy. And then, we define the raw importance of feature j this way. It’s the average

87
00:09:12,420 --> 00:09:18,850
over trees of the difference between the true error and the permuted error, and that’s

88
00:09:18,850 --> 00:09:26,490
neat; that’s our new way of measuring variable importance. Now there is a version of decision

89
00:09:26,490 --> 00:09:31,670
forests for regression by the way. For regression, the procedure is pretty much the same as for

90
00:09:31,670 --> 00:09:39,520
classification, but there is one major difference, which is that the majority vote doesn’t

91
00:09:39,520 --> 00:09:43,070
make any sense in regression because each tree is going to predict a different value

92
00:09:43,070 --> 00:09:47,210
because now it’s all – everything’s real valued. So we average the votes rather

93
00:09:47,210 --> 00:09:55,290
than taking the majority. Okay so let’s wrap up decision forests.

94
00:09:55,290 --> 00:10:00,460
The advantages are that it’s a complex and powerful prediction tool, and it produces

95
00:10:00,460 --> 00:10:06,960
highly non-linear models. But the disadvantages, of course, is that it’s black-box, it’s

96
00:10:06,960 --> 00:10:13,020
not interpretable at all, and it does tend to overfit unless it’s tuned carefully.

97
00:10:13,020 --> 00:10:17,170
And the R package doesn’t necessarily have very intuitive tuning; I know several of my

98
00:10:17,170 --> 00:10:23,710
students have had difficulties with tuning this properly. It’s also pretty slow, because

99
00:10:23,710 --> 00:10:29,250
you have to calculate a lot of decision trees and average them all together. Okay, so I

100
00:10:29,250 --> 00:10:33,690
think decision forests are pretty neat, like, you grow a whole bunch of totally overfitted

101
00:10:33,690 --> 00:10:37,700
heuristic models and you average all their votes together and it works really well. How

102
00:10:37,700 --> 00:10:38,250
cool is that?

