0
00:00:04,980 --> 00:00:09,870
Now this section is optional, it’s just for fun. But I want to give you another perspective

1
00:00:09,870 --> 00:00:15,240
on adaboost. Because the framework of boosting is actually very very general, much more general

2
00:00:15,240 --> 00:00:21,560
than what I just presented, which was sort of a classical introduction to boosting. So

3
00:00:21,560 --> 00:00:26,160
I didn’t explain where the coefficients, the formula for the coefficients come from.

4
00:00:26,160 --> 00:00:29,759
And I kind of want you to understand that. Now in order to provide you with that extra

5
00:00:29,759 --> 00:00:37,030
explanation, I need to tell you what coordinate descent is. The idea with coordinate descent

6
00:00:37,030 --> 00:00:42,739
is that you must walk along the coordinates. For instance, if you are on a street, you

7
00:00:42,739 --> 00:00:47,890
can’t go diagonally to get somewhere. You have to go only in certain directions. So

8
00:00:47,890 --> 00:00:56,780
let’s say that you are at this blue dot over here and you want to get to over to here,

9
00:00:56,780 --> 00:01:02,309
to that spot there. You are not allowed to go diagonally. Right you can’t go this way,

10
00:01:02,309 --> 00:01:06,910
so you can only go you can only either go down that street like this way or you can

11
00:01:06,910 --> 00:01:13,970
go sideways. So perhaps you will go as far as you can in one direction but not overshoot,

12
00:01:13,970 --> 00:01:16,770
and then you will go in the other direction as far as you can without overshooting. And

13
00:01:16,770 --> 00:01:22,520
that’s the same idea behind coordinate descent. We are trying to minimize a function and you

14
00:01:22,520 --> 00:01:27,720
go in one direction and then you go in the other. So here’s the general outline for

15
00:01:27,720 --> 00:01:36,740
coordinate descent. So at each time, you chose a direction to go in, and it’s going to

16
00:01:36,740 --> 00:01:42,040
be called ht. and you choose how far to go in that direction, which is going to be called

17
00:01:42,040 --> 00:01:47,880
alpha t. And then you keep repeating this repeating this repeating this, until you get

18
00:01:47,880 --> 00:01:53,750
as close as you want to where you want to go. And in the very end you output your final

19
00:01:53,750 --> 00:01:59,450
position, which is like you went three units west and two units north, five units south

20
00:01:59,450 --> 00:02:06,770
ad whatever. Okay. So here the directions are actually the reclassifiers and the alphas

21
00:02:06,770 --> 00:02:13,010
are how far you went along each direction. Now it’s a bit abstract but you should think

22
00:02:13,010 --> 00:02:17,609
of the way classifiers as being directions in the spaces as you walk along. You think

23
00:02:17,609 --> 00:02:23,400
of h1 being one direction and h2 being another direction. And then each round of adaboost

24
00:02:23,400 --> 00:02:28,019
you take one direction. There’s your point and then you take one direction and then the

25
00:02:28,019 --> 00:02:28,709
other.

26
00:02:28,709 --> 00:02:33,680
So here’s adaboost again. And you might be able to see how it is walking down a hill

27
00:02:33,680 --> 00:02:38,859
like I showed you. So first when you train the weak learning algorithm, you are picking

28
00:02:38,859 --> 00:02:43,989
the most promising direction down the hill. That’s what the weak learning algorithm

29
00:02:43,989 --> 00:02:49,799
does for you. And when you choose that coefficient, you are deciding how far you want to walk

30
00:02:49,799 --> 00:02:57,120
along that direction and not overshoot. Now this weight update step, that actually just

31
00:02:57,120 --> 00:03:02,249
helps you pick the right direction in the next round. And then in the end when you combine

32
00:03:02,249 --> 00:03:07,760
the classifiers, it computes the total distance you have gone in all the directions and adds

33
00:03:07,760 --> 00:03:14,590
them up. Okay then the only question that remains is, what is the formula for the hill

34
00:03:14,590 --> 00:03:18,709
that adaboost goes down? Well, I‘ll show you that now.

35
00:03:18,709 --> 00:03:24,959
So let’s go back to this picture. Remember this? This is showing which points are misclassified

36
00:03:24,959 --> 00:03:31,659
and how badly. So the points over here are very badly misclassified and the post over

37
00:03:31,659 --> 00:03:35,930
here are sort of near the decision boundary. They are sort of sort of wrong and sort of

38
00:03:35,930 --> 00:03:42,730
correct. And we want to choose loss functions that heavily penalize misclassified points

39
00:03:42,730 --> 00:03:49,450
and don’t penalize the correct ones. And here is one such function that does that.

40
00:03:49,450 --> 00:03:56,159
This is called the exponential loss and it badly penalizes misclassified points. And

41
00:03:56,159 --> 00:04:04,099
this is the loss function that Adaboost uses. So here is adaboost’s total objective, which

42
00:04:04,099 --> 00:04:10,700
is sum of the losses of the individual observations. And adaboost doesn’t use a regularization

43
00:04:10,700 --> 00:04:16,739
term. So this Is adaboost’s full objective. And when you are running adaboost, you are

44
00:04:16,738 --> 00:04:23,670
actually doing coordinate descent on its objective function by walking along its weak classifiers

45
00:04:23,670 --> 00:04:27,540
going distance alpha t in each direction.

46
00:04:27,540 --> 00:04:31,600
So hopefully that gives a little bit more perspective on why adaboost works. It is just

47
00:04:31,600 --> 00:04:36,330
doing coordinate descent on the exponential loss. The weak learning algorithm tells you

48
00:04:36,330 --> 00:04:40,890
what direction to go in, which is the steepest one, and the coefficients are how far we want

49
00:04:40,890 --> 00:04:46,300
to move in that direction. And the weight update just makes the competition of the best

50
00:04:46,300 --> 00:04:53,290
direction easier for the next round. Now for the training stuff for the weak learning algorithm,

51
00:04:53,290 --> 00:04:58,470
you can use pretty much any machine learning algorithm as the weak learning algorithm,

52
00:04:58,470 --> 00:05:05,420
as long as its models are a bit better than random guessing, it should work. Now in particular

53
00:05:05,420 --> 00:05:11,040
decision trees make great weak learning algorithms. Boosted decision trees are possibly the most

54
00:05:11,040 --> 00:05:15,630
reliable classification methods out there right now. There was a study done by Carolina

55
00:05:15,630 --> 00:05:20,040
and Nicolas Kimeizel several years ago and they compared a very large variety of machine

56
00:05:20,040 --> 00:05:24,730
learning methods on a very large number of dataset and they declared boosted decision

57
00:05:24,730 --> 00:05:26,120
trees to be the best one.

58
00:05:26,120 --> 00:05:30,590
Now let me dwell a little bit on boosting and decision forests for a minute. They have

59
00:05:30,590 --> 00:05:35,550
very different sots of philosophies, even though what you end up with at the end is

60
00:05:35,550 --> 00:05:40,780
pretty similar which is a bunch of over fitted heuristic trees combined in some way/ so the

61
00:05:40,780 --> 00:05:48,780
philosophy behind decision forests is to average everything to reduce variance/ And the approach

62
00:05:48,780 --> 00:05:56,200
behind boosting is that it tries to minimize bias and make them a lot more accurate but

63
00:05:56,200 --> 00:05:59,700
ends up producing variance anyways because all the trees that generate tend to be diverse

64
00:05:59,700 --> 00:06:02,490
because of all the reweighting it does.

65
00:06:02,490 --> 00:06:09,440
So anyway I would say that these two algorithms, are two of the most powerful black box machine

66
00:06:09,440 --> 00:06:18,990
learning methods out there right now. So just a summary of boosting, its powerful,

67
00:06:18,990 --> 00:06:25,330
its non-linear, its black box, you can use it or any you know any dataset you want and

68
00:06:25,330 --> 00:06:29,430
you will get a model that maybe you won’t be able to understand it like a decision tree

69
00:06:29,430 --> 00:06:36,300
but you can use it for any dataset. It originated to reweight the hard to classify observations

70
00:06:36,300 --> 00:06:43,530
at each round. That’s how it started. Where the front end said let’s do this reweighting

71
00:06:43,530 --> 00:06:51,170
thing and combine all the classifiers we get out of it. It turns out it is coordinate descent

72
00:06:51,170 --> 00:06:56,120
on the exponential loss, which explains its good behavior, and it is currently rate at

73
00:06:56,120 --> 00:07:01,490
the top of the machine learning algorithms on empirical performance. It’s not the fastest

74
00:07:01,490 --> 00:07:03,909
algorithm in the world but it’s very reliable.

