0
00:00:00,000 --> 00:00:03,680
>> Hi and welcome.

1
00:00:03,680 --> 00:00:05,150
So Cynthia has been talking about

2
00:00:05,150 --> 00:00:07,740
the theory of clustering models,

3
00:00:07,740 --> 00:00:10,610
hierarchical clustering, K-means clustering.

4
00:00:10,610 --> 00:00:14,195
But there's this tricky problem with

5
00:00:14,195 --> 00:00:16,640
cluster models and actually with

6
00:00:16,640 --> 00:00:20,285
any unsupervised machine learning models,

7
00:00:20,285 --> 00:00:22,190
how do you evaluate it?

8
00:00:22,190 --> 00:00:23,630
There are no labels, right?

9
00:00:23,630 --> 00:00:25,285
In supervised machine learning,

10
00:00:25,285 --> 00:00:28,625
we went through a number of methods to

11
00:00:28,625 --> 00:00:31,490
evaluate say regression models or

12
00:00:31,490 --> 00:00:34,885
classification models because we had no labels.

13
00:00:34,885 --> 00:00:38,635
But in unsupervised machine learning, by definition,

14
00:00:38,635 --> 00:00:43,070
we don't have those labels that's why it's unsupervised.

15
00:00:43,070 --> 00:00:46,255
So we need to use a combination of

16
00:00:46,255 --> 00:00:49,935
approaches and it's going to take some judgment.

17
00:00:49,935 --> 00:00:52,820
But you're going to use a combination of

18
00:00:52,820 --> 00:00:55,580
visualization and summary statistics is what it comes

19
00:00:55,580 --> 00:00:59,330
down to and some probably domain knowledge.

20
00:00:59,330 --> 00:01:00,770
So visualization of

21
00:01:00,770 --> 00:01:02,930
the high dimensional space is where we typically

22
00:01:02,930 --> 00:01:04,850
apply clustering models or

23
00:01:04,850 --> 00:01:07,925
any unsupervised machine learning model is tough.

24
00:01:07,925 --> 00:01:10,010
But it still can be helpful

25
00:01:10,010 --> 00:01:11,930
if you can see clear separations of

26
00:01:11,930 --> 00:01:14,420
the clusters or some structure

27
00:01:14,420 --> 00:01:17,350
that is being highlighted by your model,

28
00:01:17,350 --> 00:01:18,720
you know you're getting somewhere.

29
00:01:18,720 --> 00:01:20,270
So it's really worth trying even

30
00:01:20,270 --> 00:01:23,090
though it can be tough at times.

31
00:01:23,090 --> 00:01:27,040
But keep in mind that

32
00:01:27,040 --> 00:01:31,195
an ideal cluster model has just two good characteristics.

33
00:01:31,195 --> 00:01:32,625
It should be compact.

34
00:01:32,625 --> 00:01:35,410
The individual clusters should be small,

35
00:01:35,410 --> 00:01:37,405
data within a cluster should be

36
00:01:37,405 --> 00:01:39,850
close to each other by whatever measure you're

37
00:01:39,850 --> 00:01:41,715
using in that space

38
00:01:41,715 --> 00:01:45,265
and they should be well separated from other clusters.

39
00:01:45,265 --> 00:01:48,189
If you have a lot of very diffuse clusters

40
00:01:48,189 --> 00:01:50,055
that have huge overlap,

41
00:01:50,055 --> 00:01:52,040
that's probably not such a great model.

42
00:01:52,040 --> 00:01:54,580
If you have compact clusters that seem to

43
00:01:54,580 --> 00:01:57,440
stand away from each other in some way,

44
00:01:57,440 --> 00:01:59,670
that's probably a good model.

45
00:01:59,670 --> 00:02:03,180
So let's talk about some specific methods.

46
00:02:03,180 --> 00:02:03,940
We are going to talk about

47
00:02:03,940 --> 00:02:05,830
three different summary statistics

48
00:02:05,830 --> 00:02:07,600
here that you can use.

49
00:02:07,600 --> 00:02:11,060
One is called the within cluster sum of squares.

50
00:02:11,060 --> 00:02:13,185
So it's kind of a long-term,

51
00:02:13,185 --> 00:02:17,290
a long name anyway for a simple idea.

52
00:02:17,290 --> 00:02:19,925
So, again it's back to the simple idea that

53
00:02:19,925 --> 00:02:23,555
good cluster model has small compact clusters.

54
00:02:23,555 --> 00:02:27,760
We need a way to measure compactness within

55
00:02:27,760 --> 00:02:29,520
the cluster using sum of

56
00:02:29,520 --> 00:02:32,770
squares and so we just write that out.

57
00:02:32,770 --> 00:02:34,510
I mean, we just look for

58
00:02:34,510 --> 00:02:37,030
a within cluster sum of squares is

59
00:02:37,030 --> 00:02:43,115
the minimum of this kind of array double sum here.

60
00:02:43,115 --> 00:02:47,300
Ci is the center of our cluster

61
00:02:48,350 --> 00:02:52,560
and Xj is the distance

62
00:02:52,560 --> 00:02:55,550
from Ci to the jth member of that cluster.

63
00:02:55,550 --> 00:02:58,805
So we sum over

64
00:02:58,805 --> 00:03:02,400
all i because we're looking for the right center,

65
00:03:02,400 --> 00:03:05,375
that's why we have this minimum.

66
00:03:05,375 --> 00:03:08,420
So we want to put this Xj

67
00:03:11,370 --> 00:03:16,270
in the right cluster with its center Ci.

68
00:03:16,270 --> 00:03:20,250
So that's what's going on with this outer sum.

69
00:03:20,250 --> 00:03:22,420
Then this inner some is just summing

70
00:03:22,420 --> 00:03:24,730
up over once we've determined

71
00:03:24,730 --> 00:03:27,940
the members the Xj's of

72
00:03:27,940 --> 00:03:29,755
that cluster with its center

73
00:03:29,755 --> 00:03:32,210
of the ith cluster with its center Ci,

74
00:03:32,210 --> 00:03:33,420
then we're just summing the

75
00:03:33,420 --> 00:03:36,340
Euclidean distance in this case.

76
00:03:36,960 --> 00:03:39,040
That's all there is to it. So that's

77
00:03:39,040 --> 00:03:42,395
the within cluster sum of squares.

78
00:03:42,395 --> 00:03:45,880
Now there's the between cluster sum of square.

79
00:03:45,880 --> 00:03:49,175
So good cluster model as I said should be well separated.

80
00:03:49,175 --> 00:03:52,825
That means are the between cluster sum of squares,

81
00:03:52,825 --> 00:03:54,705
a measure of how far apart

82
00:03:54,705 --> 00:03:57,660
the clusters are that should be large.

83
00:03:58,540 --> 00:04:03,325
Just axiomatically, we say the total sum of squares

84
00:04:03,325 --> 00:04:05,615
has to be the between cluster sum of

85
00:04:05,615 --> 00:04:08,620
squares plus the within cluster sum of squares.

86
00:04:08,620 --> 00:04:12,755
So we can compute the between cluster sum of squares just

87
00:04:12,755 --> 00:04:15,190
by subtracting the within cluster sum

88
00:04:15,190 --> 00:04:17,250
of squares from the total sum of squares,

89
00:04:17,250 --> 00:04:19,200
where the total sum of squares is just

90
00:04:19,200 --> 00:04:21,800
fortunately for us just a simple sum this time,

91
00:04:21,800 --> 00:04:23,360
it's not a double sum.

92
00:04:23,360 --> 00:04:28,580
It's just where mu is the mean,

93
00:04:28,580 --> 00:04:30,680
it's a mean vector because we're dealing with

94
00:04:30,680 --> 00:04:34,770
multiple dimensions minus all the values,

95
00:04:34,770 --> 00:04:37,480
all the i's in our total data sample.

96
00:04:37,480 --> 00:04:39,430
So that's the total sum of squares.

97
00:04:39,430 --> 00:04:41,625
It's just the variance basically.

98
00:04:41,625 --> 00:04:44,855
So that's the between cluster sum of squares is

99
00:04:44,855 --> 00:04:47,675
the remaining variance that

100
00:04:47,675 --> 00:04:52,640
isn't taken up by the within cluster sum of squares.

101
00:04:53,420 --> 00:04:56,860
Then there is a slightly more sophisticated method

102
00:04:56,860 --> 00:04:59,860
we can use called the silhouette coefficient.

103
00:04:59,860 --> 00:05:03,040
In the lab I hope it will become more

104
00:05:03,040 --> 00:05:04,570
clear why we call this a

105
00:05:04,570 --> 00:05:06,850
silhouette goldfish because you can build these,

106
00:05:06,850 --> 00:05:10,400
what are called silhouette plots for your cluster model.

107
00:05:10,400 --> 00:05:13,600
We define that as it's a

108
00:05:13,600 --> 00:05:16,930
little more calmly as the ai minus bi

109
00:05:16,930 --> 00:05:23,400
for the ith cluster divided by the max of ai, bi.

110
00:05:23,400 --> 00:05:25,175
So what are these?

111
00:05:25,175 --> 00:05:27,390
Ai is the average distance from

112
00:05:27,390 --> 00:05:31,760
point ai to other members of that cluster.

113
00:05:31,760 --> 00:05:34,210
So it's just a mean distance

114
00:05:34,210 --> 00:05:36,920
and we call it mean Euclidean distance.

115
00:05:36,920 --> 00:05:40,335
Well, it doesn't have to be Euclidean let me say that.

116
00:05:40,335 --> 00:05:42,840
Bi is the average distance from point

117
00:05:42,840 --> 00:05:45,530
i to members of the adjacent cluster.

118
00:05:45,530 --> 00:05:52,350
So this is a measure of within cluster compactness is ai,

119
00:05:52,350 --> 00:06:00,870
bi is a measure of between cluster separation.

120
00:06:00,870 --> 00:06:04,540
So, that leads to some properties here.

121
00:06:04,540 --> 00:06:08,770
The silhouette coefficient has to be greater than or

122
00:06:08,770 --> 00:06:10,645
equal to minus one less than or equal to

123
00:06:10,645 --> 00:06:13,460
one, because it's normalized.

124
00:06:13,460 --> 00:06:15,795
Go back there, yeah see,

125
00:06:15,795 --> 00:06:18,585
It's normalized by this Max.

126
00:06:18,585 --> 00:06:24,930
So it's never going to exceed minus one to one the range.

127
00:06:28,100 --> 00:06:33,340
What we want is for this ai measure that's

128
00:06:33,340 --> 00:06:37,030
our cluster compactness measure to be less

129
00:06:37,030 --> 00:06:41,620
than our between cluster separation measure,

130
00:06:41,620 --> 00:06:44,440
in which case our silhouette coefficient

131
00:06:44,440 --> 00:06:46,330
for that cluster is greater than zero.

132
00:06:46,330 --> 00:06:48,340
That's good. However, if we have

133
00:06:48,340 --> 00:06:50,770
very diffused clusters that aren't well separated,

134
00:06:50,770 --> 00:06:52,360
we could have a silhouette coefficient

135
00:06:52,360 --> 00:06:54,255
less than zero and that does happen,

136
00:06:54,255 --> 00:06:57,890
probably means you need to work harder on your model.

137
00:06:59,180 --> 00:07:01,410
In simple terms,

138
00:07:01,410 --> 00:07:04,750
the best model has a large silhouette coefficient.

139
00:07:04,750 --> 00:07:07,270
So, let me just summarize this

140
00:07:07,270 --> 00:07:09,790
what the ideal clustering model should have.

141
00:07:09,790 --> 00:07:11,860
As we've said is compact clusters,

142
00:07:11,860 --> 00:07:14,815
well-separated clusters. That's our goal.

143
00:07:14,815 --> 00:07:17,260
Then if we've hit that goal we've learned

144
00:07:17,260 --> 00:07:20,520
some really useful structure of that data.

145
00:07:20,520 --> 00:07:23,650
For K-means clustering, we can

146
00:07:23,650 --> 00:07:26,450
use within cluster sum of squares being small,

147
00:07:26,450 --> 00:07:28,525
means we have compact clusters.

148
00:07:28,525 --> 00:07:30,400
Between cluster sum of squares

149
00:07:30,400 --> 00:07:32,400
being large means we have well separated.

150
00:07:32,400 --> 00:07:35,260
In order say for K-means clustering models because

151
00:07:35,260 --> 00:07:38,530
remember that was all based on Euclidean norms.

152
00:07:38,530 --> 00:07:40,795
Now, there are special cases

153
00:07:40,795 --> 00:07:42,460
of hierarchical clustering where

154
00:07:42,460 --> 00:07:44,420
we use Euclidean norms

155
00:07:44,420 --> 00:07:46,675
but you also have the linkage functions.

156
00:07:46,675 --> 00:07:49,300
But for any distance metric,

157
00:07:49,300 --> 00:07:51,685
the silhouette coefficient works

158
00:07:51,685 --> 00:07:55,525
because I didn't say what metric I was using there.

159
00:07:55,525 --> 00:07:58,390
There's no sum of

160
00:07:58,390 --> 00:08:01,320
squares over Euclidean distance or anything like that.

161
00:08:01,320 --> 00:08:03,730
It could be that but it could be any metric.

162
00:08:03,730 --> 00:08:06,290
So we can use silhouette coefficient for

163
00:08:06,290 --> 00:08:07,955
hierarchical clustering models

164
00:08:07,955 --> 00:08:10,335
and K-means clustering models.

165
00:08:10,335 --> 00:08:11,990
But in any event, we want

166
00:08:11,990 --> 00:08:14,240
a large silhouette coefficient

167
00:08:14,240 --> 00:08:16,265
because we want that measure

168
00:08:16,265 --> 00:08:20,750
of within the cluster norm to be

169
00:08:20,750 --> 00:08:25,890
small and the between cluster measure to be large.

