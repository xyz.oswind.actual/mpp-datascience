0
00:00:05,339 --> 00:00:10,080
Let&#39;s talk about unsupervised learning and in particular we will focus on clustering.

1
00:00:10,080 --> 00:00:17,019
Now in the supervised setting we had ground truth labels for each observation and we were

2
00:00:17,019 --> 00:00:22,630
trying to label new observations. But in unsupervised learning, we just have observations. And you

3
00:00:22,630 --> 00:00:29,770
need to do something with them. Now clustering is a key unsupervised problem. In clustering,

4
00:00:29,770 --> 00:00:35,969
the goal is to group the points and the clusters that are somehow similar. Now let us say that

5
00:00:35,969 --> 00:00:41,020
each dot here represents a person represented by two features. So feature 1 and feature

6
00:00:41,020 --> 00:00:48,880
2. And I can see 5 clusters. Okay? And you want the algorithm to automatically be able

7
00:00:48,880 --> 00:00:55,460
to figure out what is in what cluster like these clusters there. And that can be pretty

8
00:00:55,460 --> 00:00:59,219
tricky, For instance how in the world are we going to train a learning algorithm to

9
00:00:59,219 --> 00:01:04,530
figure this one out. There are actually two clusters here, believe it or not. They are

10
00:01:04,530 --> 00:01:08,500
kind of tricky for the algorithm to find. In this lecture, we are going to show you

11
00:01:08,500 --> 00:01:14,079
two algorithms: one that is good for this type of data and another that is good for

12
00:01:14,079 --> 00:01:20,359
this type of data and neither algorithm can do well on the other type, interestingly enough.

13
00:01:20,359 --> 00:01:27,240
So there are a lot of applications for clustering. For instance, automatically grouping documents

14
00:01:27,240 --> 00:01:34,039
or web pages into topics like grouping news stories clustering products for instance Etsy

15
00:01:34,039 --> 00:01:39,509
does a lot of product clustering they had a great paper at KTT a couple of years ago

16
00:01:39,509 --> 00:01:45,159
on that. And the also clustering customer with a similar purchasing behavior, clustering

17
00:01:45,159 --> 00:01:50,759
medical patients, there is just a lot of clustering applications. It is a hot topic. So let’s

18
00:01:50,759 --> 00:01:57,969
talk about K-means which is probably the most widely used clustering method. So here are

19
00:01:57,969 --> 00:02:04,479
the steps for k-means and I am going to walk you through them with an example in a minute.

20
00:02:04,479 --> 00:02:12,260
So first you input into K-means the number of clusters you think the data has and then

21
00:02:12,260 --> 00:02:18,290
you randomly initialize the center of the clusters and then you assign all of the points

22
00:02:18,290 --> 00:02:24,879
to the closest cluster center. You change the cluster centers to be in the middle of

23
00:02:24,879 --> 00:02:29,269
the points that were assigned to it and then you repeat this until you are converged. Let

24
00:02:29,269 --> 00:02:35,480
us just walk this whole thing through with an example. Alright, so there is my data.

25
00:02:35,480 --> 00:02:39,980
And first I input the number of my clusters and I randomly initialized where the centers

26
00:02:39,980 --> 00:02:45,859
are. So these clusters centers are definitely not chosen well but that okay because they

27
00:02:45,859 --> 00:02:54,219
are going to move . And then you assign all points to the closest cluster center and this

28
00:02:54,219 --> 00:02:58,959
produces what’s called a Vernier partition where the space is divided into different

29
00:02:58,959 --> 00:03:07,189
pieces. based on who is assigned to what cluster center, and then you are going to change the

30
00:03:07,189 --> 00:03:13,370
cluster centers to be in the middle of the points that were assigned to it. And you repeat

31
00:03:13,370 --> 00:03:17,590
this until you are converged. Hopefully after a few iterations like this, you will get a

32
00:03:17,590 --> 00:03:23,640
lovely solution like this. I should warn you that this doesn’t always work. It depends

33
00:03:23,640 --> 00:03:27,640
on where you places those e initial seeds and sometimes people try several times with

34
00:03:27,640 --> 00:03:32,420
different initial seeds to see if they can get a better clustering. But that’s the

35
00:03:32,420 --> 00:03:41,739
general idea. Okay, ready? So let us let her rip. So this is a simulation that I wrote.

36
00:03:41,739 --> 00:03:49,379
So let us start with these data. And let us pop the initial cluster centers in there and

37
00:03:49,379 --> 00:03:56,280
lets us start iterating. So this is after one iteration, 2 and we will keep going there

38
00:03:56,280 --> 00:04:10,959
and that’s the final solution that I get after 15 or so iterations. Nice! So that’s

39
00:04:10,959 --> 00:04:15,499
the basic k-means and so in the next video I want to give you a little bit more intuition

40
00:04:15,499 --> 00:04:18,930
about what K-means I actually doing in terms of mathematical perception.

