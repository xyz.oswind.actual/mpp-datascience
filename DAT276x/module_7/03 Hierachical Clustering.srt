0
00:00:05,190 --> 00:00:08,770
Hierarchical agglomerative clustering is a different sort of clustering algorithm than

1
00:00:08,770 --> 00:00:14,940
K-means. So I hope you enjoy hearing about it. I like it because it shows you what a

2
00:00:14,940 --> 00:00:22,300
word of difference a choice of algorithm can make. Now it is a very very simple clustering

3
00:00:22,300 --> 00:00:27,070
algorithm, it just starts with each point in its own cluster and then it just repeatedly

4
00:00:27,070 --> 00:00:32,550
merges the clusters of the closest two points. And that’s it. That’s the whole algorithm.

5
00:00:32,549 --> 00:00:42,480
So here’s your dataset. And I just merged these two closest points. And I do that again.

6
00:00:42,480 --> 00:00:49,469
And I just keep doing it. And then I am good. If I go too far they will become one cluster.

7
00:00:49,469 --> 00:00:57,339
But that was the idea. So if we take the jelly rope dataset. Now remember K-means was awful

8
00:00:57,339 --> 00:01:02,749
in this dataset, just produced garbage. Now let’s try hierarchical agglomerative clustering.

9
00:01:02,749 --> 00:01:12,090
Aah perfect. A lovely result from hierarchical clustering. Okay now let’s try this dataset.

10
00:01:12,090 --> 00:01:18,600
This is the one where K--means did really well. It to almost all the points into one

11
00:01:18,600 --> 00:01:23,010
cluster and put all the outliers into their own clusters. See all the blue points, they

12
00:01:23,010 --> 00:01:30,140
are on one cluster. And here’s like the other clusters, see that red cluster there.

13
00:01:30,140 --> 00:01:36,560
Okay so what if we stopped it a little earlier before it merged everything into one cluster?

14
00:01:36,560 --> 00:01:44,990
No, not brilliant. It merged some of the clusters so fast, it merged this one so fast that we

15
00:01:44,990 --> 00:01:50,430
just really couldn’t just stop it. So this is just far from perfect. This algorithm is

16
00:01:50,430 --> 00:01:54,980
just not meant for this kind of overlapping spherical clusters. It does much better with

17
00:01:54,980 --> 00:01:59,770
these separated but twisty clusters, long skinny clusters right that’s not a problem

18
00:01:59,770 --> 00:02:05,210
for hierarchical agglomerative clustering. So it’s kind of neat right? K-means thought

19
00:02:05,210 --> 00:02:09,920
it was easy peasy. Just goes to show you that the choice of an algorithm can really make

20
00:02:09,919 --> 00:02:16,540
a difference. Now hierarchical agglomerative clustering produces these dendograms that

21
00:02:16,540 --> 00:02:20,920
tell you what clusters are being merged. So it will list all the observations here. And

22
00:02:20,920 --> 00:02:25,760
then it says you know in the first round, I merged these two. And then I merged these

23
00:02:25,760 --> 00:02:32,390
two and so on. So you can cut it off at any point. So here we stopped at 7 clusters these

24
00:02:32,390 --> 00:02:40,140
are the guys in one of the clusters to and so on. Or you can stop a bit higher up and

25
00:02:40,140 --> 00:02:45,680
get only 2 clusters. Whatever you want. Now I want to give you a warning about something

26
00:02:45,680 --> 00:02:51,690
you might see from some of the software packages. There is an output parameter called Number

27
00:02:51,690 --> 00:02:57,970
of points in each cluster, and sometimes if you add up the number of points on each cluster

28
00:02:57,970 --> 00:03:03,709
it could be actually less than the total number of points in the dataset. You might get confused.

29
00:03:03,709 --> 00:03:09,190
Now if the number of points assigned to clusters is less than the total number of data points

30
00:03:09,190 --> 00:03:15,490
available. Some data could not be assigned to a cluster. Now that’s never going to

31
00:03:15,490 --> 00:03:20,849
happen for a K-means. Because you just always assign each point to its nearest cluster center.

32
00:03:20,849 --> 00:03:24,310
But in hierarchical agglomerative clustering, you might have this problem as it might not

33
00:03:24,310 --> 00:03:30,770
have merged some of the points into clusters. Now the maximal distance to the cluster center

34
00:03:30,770 --> 00:03:38,090
is actually just the largest distance of any point away from its own the cluster center.

35
00:03:38,090 --> 00:03:43,959
So if this maximal distance is pretty large, lot larger, than the average distance to the

36
00:03:43,959 --> 00:03:48,440
points in the cluster center then it might mean that that cluster is particularly dispersed.

37
00:03:48,440 --> 00:03:55,050
Okay, let’s summarize. So I showed you two clustering methods. K-means and hierarchical

38
00:03:55,050 --> 00:03:58,950
agglomerative. Both of them are useful but with different

39
00:03:58,950 --> 00:04:04,020
sets of problems. K-means works really well for spherical data whereas hierarchical clustering

40
00:04:04,020 --> 00:04:11,129
is useful when the clusters are well-separated. The data can be on very complicated manifolds

41
00:04:11,129 --> 00:04:16,340
where they loop around each other. And that’s totally fine. Okay so data that are close

42
00:04:16,339 --> 00:04:21,449
together should be on the same cluster for this particular method. Now there are choices

43
00:04:21,449 --> 00:04:26,150
that you have to make. For k-means, you have to choose the number of clusters and so we

44
00:04:26,150 --> 00:04:31,470
suggest trying a few different ones. And then for hierarchical agglomerative, you just choose

45
00:04:31,470 --> 00:04:37,560
when to stop merging the cluster Now I should mention that the distance matcher for clustering

46
00:04:37,560 --> 00:04:44,020
is important and it can have a kind of a large impact on the quality of the solution. I just

47
00:04:44,020 --> 00:04:49,160
chose regular Euclidean distance, just regular distances. But I could have equally chose

48
00:04:49,160 --> 00:04:53,560
a different distance function. All right, I hope you enjoyed this lecture

49
00:04:53,560 --> 00:04:54,380
on clustering.

