0
00:00:05,319 --> 00:00:10,160
Now let’s talk about k-means from a more mathematical perspective. The input to k-means

1
00:00:10,160 --> 00:00:16,299
is your dataset and then the number of clusters that you are going to have and the output

2
00:00:16,299 --> 00:00:24,640
is just the centers of the clusters. And this cluster enters are chosen to try to minimize

3
00:00:24,640 --> 00:00:32,599
something or other and what is that IT starts with the distance between a point

4
00:00:32,598 --> 00:00:40,300
xi and a cluster center ck. Now we want to use the nearest cluster center the cluster

5
00:00:40,300 --> 00:00:47,489
center with the minimum distance to you. And then we want to sum all those distances up.

6
00:00:47,489 --> 00:00:55,039
Okay so we want the total distances to the nearest cluster center. And again we want

7
00:00:55,039 --> 00:01:05,059
to find we are trying- to find the cluster centers that minimize the total distance to

8
00:01:05,059 --> 00:01:14,080
the nearest cluster center. Okay so this thing here, this is a global objective. But you

9
00:01:14,080 --> 00:01:20,610
see K-means it actually has local moves. Its trying to optimize this but it can’t optimize

10
00:01:20,610 --> 00:01:25,030
the whole things at once like you know support factor machine or logistic regressionary thing,

11
00:01:25,030 --> 00:01:32,640
it would be really nice if we plain globally optimize this. Why the heck not, why can’t

12
00:01:32,640 --> 00:01:40,140
we just globally optimize the s for all our cluster centers. Well the problem is we can’t

13
00:01:40,140 --> 00:01:46,479
even figure what assignments to make for the clusters because you know we could try all

14
00:01:46,479 --> 00:01:52,280
possible assignments say of m point s to k cluster and this would be great if we had

15
00:01:52,280 --> 00:01:58,409
only 10 points and 4 clusters, Right because that would be only 34, 000 possibilities and

16
00:01:58,409 --> 00:02:03,009
we could easily run through all of the,. But if we have 19 points and 4 cluster well that’s

17
00:02:03,009 --> 00:02:08,060
10 to the 10th possibilities and that’s yucky and we can’t compute that many possibilities.

18
00:02:08,060 --> 00:02:14,100
So k-means is an approximation it cannot optimize this thing, it can’t because it can’t

19
00:02:14,100 --> 00:02:21,359
even figure out what the asignemnts are. Now let me take a look at the cost function again.

20
00:02:21,359 --> 00:02:28,290
Now I want to take a look at this cost function again. It is a sum over the points of the

21
00:02:28,290 --> 00:02:33,380
distance to nearest cluster center. It is just adding up all the points one by one.

22
00:02:33,380 --> 00:02:39,220
So I can count the points I can count the points this way I can just add sum them all

23
00:02:39,220 --> 00:02:43,989
up I can count from left to right or something. That’s one way to count them up, that’s

24
00:02:43,989 --> 00:02:49,040
why they are all turning red from left to right because yeah, counting them from the

25
00:02:49,040 --> 00:02:55,160
that direction. I could simply try to sum them up in a different order right so all

26
00:02:55,160 --> 00:02:59,380
I did here the sum didn’t change, I just routed it differently so I could something

27
00:02:59,380 --> 00:03:04,010
happen in different order. So here I am still summing over all the points but I am first

28
00:03:04,010 --> 00:03:10,280
summing over clusters and then summing over the points in each cluster. So the sum up

29
00:03:10,280 --> 00:03:14,660
over the cluster 1 s point s and then over cluster 2’s points. That is the only change

30
00:03:14,660 --> 00:03:15,620
I made there.

31
00:03:15,620 --> 00:03:21,650
Now let’s just try a slightly more general cost because I want to optimize who is assigned

32
00:03:21,650 --> 00:03:26,060
to what cluster. So I am going to put the Cluster of assignments here as well as the

33
00:03:26,060 --> 00:03:34,239
cluster of centers. Now cluster 1 is the all the points that are assigned to the first

34
00:03:34,239 --> 00:03:38,669
cluster and these are all the points assigned to the second cluster and so on and these

35
00:03:38,669 --> 00:03:44,799
are the cluster centers as before. And because I wrote the cost function like this, the cks,

36
00:03:44,799 --> 00:03:50,340
they are no longer forced to be the center of the clusters. We are going to make them

37
00:03:50,340 --> 00:03:54,570
to be the centers of the clusters but I am going to call them reference points for now.

38
00:03:54,570 --> 00:04:00,380
Because this function is actually so general they can allow any points to be assigned to

39
00:04:00,380 --> 00:04:04,609
any cluster whose reference point can literally be anywhere. It just doesn’t have to be

40
00:04:04,609 --> 00:04:11,180
in the center. This is the cause function of both the luster assignments to who is assigned

41
00:04:11,180 --> 00:04:15,250
to which center, and what we are going to use as reference points for the cluster we

42
00:04:15,250 --> 00:04:20,750
are going to change these things around independently of each and evaluate the costs.

43
00:04:20,750 --> 00:04:26,380
Let’s just look at these costs as a function of these cluster assignments only. Now if

44
00:04:26,380 --> 00:04:34,010
we fix these cks, fix these reference points, what’s the best way to assign these clusters

45
00:04:34,010 --> 00:04:42,660
assuming you fix the centers. And the answer is obvious. You just assign the points to

46
00:04:42,660 --> 00:04:50,700
the nearest cluster center, that’s exactly what K-means does. Or you just assign the

47
00:04:50,700 --> 00:04:56,560
points to the nearest cluster center. Okay so on the other hand what about assigning

48
00:04:56,560 --> 00:05:03,270
the reference points. If we have the cluster assignment set where should we put the ref

49
00:05:03,270 --> 00:05:13,690
points? And, of course, we would try to minimize the cost by putting those in the center to

50
00:05:13,690 --> 00:05:21,530
minimize the average distance to the reference points and that’s exactly what k-means does.

51
00:05:21,530 --> 00:05:25,350
So now you should have a much better understanding of what k-means is actually doing. I am going

52
00:05:25,350 --> 00:05:32,120
to put the cost up her just for reference and remember that it depends on both the cluster

53
00:05:32,120 --> 00:05:39,000
assignments and the cluster centers. Now let’s put it to the algorithm again until you have

54
00:05:39,000 --> 00:05:46,590
converged you just assign all points to the closed cluster center and that’s the same

55
00:05:46,590 --> 00:05:51,690
things as minimizing the cost by choosing the best assignments given the cluster centers

56
00:05:51,690 --> 00:05:58,060
and then for the next step you change the reference point that they are actually in

57
00:05:58,060 --> 00:06:05,610
the center of the points assigned to those clusters and this minimizes the cost of those

58
00:06:05,610 --> 00:06:11,960
reference points filling those cluster assignments. So k-means is sort of an alternating minimization

59
00:06:11,960 --> 00:06:15,420
scheme. First it is minimizing the coster with the cluster assignments and then minimizing

60
00:06:15,420 --> 00:06:20,630
the coster with the alternating minimization. So it is alternating between the two. It is

61
00:06:20,630 --> 00:06:27,570
an alternation minimization. So here is a question for you given that K–means’

62
00:06:27,570 --> 00:06:35,630
actual goal is to minimize these costs, does it actually achieve this goal? We know that

63
00:06:35,630 --> 00:06:43,090
the cost gets lower at each step of K-means because it always minimizing. But does it

64
00:06:43,090 --> 00:06:49,800
always converge to the globally optimal set of cluster assignment and reference points?

65
00:06:49,800 --> 00:06:57,880
And the answer is not always. You might actually need to restart it do multiple replicates

66
00:06:57,880 --> 00:07:01,780
because if we happen to start the reference point at so often a bad spot sometimes it

67
00:07:01,780 --> 00:07:07,400
won’t just go to the right place. And that’s why most k-means code allows you to do replicates.

68
00:07:07,400 --> 00:07:11,980
It can start with differently randomly chosen points to be at the center

69
00:07:11,980 --> 00:07:20,100
Now there’s something even worse than that, K-means goal might not be the right one. This

70
00:07:20,100 --> 00:07:25,860
cost function that might not be actually the thing you should optimize to get the best

71
00:07:25,860 --> 00:07:34,560
cost. So let me show you an example where this objective is just plain wrong. No matter

72
00:07:34,560 --> 00:07:42,860
how you set up the initial cluster centers you are going to get garbage. So there’s

73
00:07:42,860 --> 00:07:46,740
the initial cluster centers these two guys are on the top they are going to separate

74
00:07:46,740 --> 00:07:56,560
a bit, keep an eye on those two. There’s the first iteration and we will keep going.

75
00:07:56,560 --> 00:08:15,800
Beautiful huh? It is doing nothing. And then it calls itself conferred at this point. So

76
00:08:15,800 --> 00:08:20,750
you can find examples for k-means doesn’t optimize the objective function, here it actually

77
00:08:20,750 --> 00:08:24,050
did a good job of optimizing the objective it’s just that the objective was really

78
00:08:24,050 --> 00:08:29,870
lousy for this dataset. But anyway, it is a nifty little algorithm and it is very heavily

79
00:08:29,870 --> 00:08:31,750
used and now you know what it is doing.

80
00:08:31,750 --> 00:08:38,169
So let’s discuss a little more about the practical aspects of k-means. And in particular

81
00:08:38,169 --> 00:08:43,330
how you are going choose K because you have to put K in its input. There are a couple

82
00:08:43,330 --> 00:08:47,770
of options. There is no single right answer for this. One option is to look or a point

83
00:08:47,770 --> 00:08:53,770
of diminishing returns. As you increase the number of clusters, the cost function is going

84
00:08:53,770 --> 00:08:59,450
to go done and after a while it won’t go down very much and that’s probably where

85
00:08:59,450 --> 00:09:03,360
you want to stop. This is the point of diminishing returns. The more clusters you have over here

86
00:09:03,360 --> 00:09:09,270
is not lowering your cost very much. So you might want to stop there where the kink in

87
00:09:09,270 --> 00:09:16,730
the curve is. So option 1 look for the point of diminishing returns. Another option is

88
00:09:16,730 --> 00:09:22,830
to look at the ratio of the average distance to your sun cluster center v. the average

89
00:09:22,830 --> 00:09:29,589
distance to the other centers. So the average distance to the science center, you just compute

90
00:09:29,589 --> 00:09:36,339
the distance to the science center the closest one and you compare it to the average of the

91
00:09:36,339 --> 00:09:43,380
distance to the other centers. Now, you kind of want these numbers to look very different

92
00:09:43,380 --> 00:09:47,520
from each other. Ideally, you want to have nice tight clusters that are far say from

93
00:09:47,520 --> 00:09:54,540
other clusters. So if the first one is small and the second one is large that’s good.

94
00:09:54,540 --> 00:09:59,940
Now if you put so many cluster centers in there both of these things will decrease but

95
00:09:59,940 --> 00:10:07,370
you still want them to be as separate as possible. And to be honest, I don’t really like this??

96
00:10:07,370 --> 00:10:13,399
Its fine for two clusters and not really for more than that and so I actually think this

97
00:10:13,399 --> 00:10:21,310
average distance to other centers should be replaced with the average distance to the

98
00:10:21,310 --> 00:10:25,899
center that’s not yours but is the closest to the other center. Well I didn’t design

99
00:10:25,899 --> 00:10:30,460
the software and it’s not my fault there so anyway that’s my opinion on this metric.

100
00:10:30,460 --> 00:10:34,480
This one is not particularly good.

101
00:10:34,480 --> 00:10:41,860
At Metro they use the kink and the flaw. All right so K-means, just to summarize it, is

102
00:10:41,860 --> 00:10:46,360
a popular clustering algorithm. It is very computationally efficient. Because finding

103
00:10:46,360 --> 00:10:50,820
the mean of a bunch of points is easy and it is a single loop to find out what the cluster

104
00:10:50,820 --> 00:10:57,480
assignments are. It performs alternating minimization on a cost function. And I showed you exactly

105
00:10:57,480 --> 00:11:03,110
what that cost function was and what the alternating minimization scheme was. Now it doesn’t

106
00:11:03,110 --> 00:11:07,870
always fully minimize that cost function. So multiple replicates with different initial

107
00:11:07,870 --> 00:11:15,000
conditions different initial centers might be needed to find a good solution. And you

108
00:11:15,000 --> 00:11:19,339
can actually use the cost function itself to evaluate whether one replicate is better

109
00:11:19,339 --> 00:11:23,649
than the other because you will get a lower cost.

110
00:11:23,649 --> 00:11:30,310
And you can also use that cost function to help you choose that cluster by looking at

111
00:11:30,310 --> 00:11:37,180
that kink in the plot cost over the number of clusters.

112
00:11:37,180 --> 00:11:45,440
And as I showed you that example. It doesn’t really work for non-spherical clusters But

113
00:11:45,440 --> 00:11:47,960
it works very nicely when clusters ae actually spherical.

