0
00:00:01,520 --> 00:00:04,360
>> Hi and welcome. So, now,

1
00:00:04,360 --> 00:00:08,330
we're ready to build our first machine learning model.

2
00:00:08,330 --> 00:00:12,389
What we're going to do is use a KNN or K-Nearest

3
00:00:12,389 --> 00:00:17,110
Neighbors classifier to classify the species of flower.

4
00:00:17,110 --> 00:00:20,110
So, this is an iris flower dataset.

5
00:00:20,110 --> 00:00:21,460
It's a very famous data set in

6
00:00:21,460 --> 00:00:23,485
the history of statistics and machine learning.

7
00:00:23,485 --> 00:00:27,180
In fact, it goes all the way back to 1935.

8
00:00:27,480 --> 00:00:30,264
There are three species of flowers

9
00:00:30,264 --> 00:00:32,350
in this dataset and we'll have

10
00:00:32,350 --> 00:00:34,255
characteristics of the flowers which are

11
00:00:34,255 --> 00:00:36,070
their petal length and

12
00:00:36,070 --> 00:00:38,800
width and something called the sepal length and width,

13
00:00:38,800 --> 00:00:40,030
which is another part of

14
00:00:40,030 --> 00:00:42,300
the flower structures, the sepal.

15
00:00:42,300 --> 00:00:44,080
So, using those four measurements,

16
00:00:44,080 --> 00:00:45,670
we're going to see if we can

17
00:00:45,670 --> 00:00:48,850
classify the species of flowers.

18
00:00:49,900 --> 00:00:51,990
So, first thing I'm going to do

19
00:00:51,990 --> 00:00:53,190
is I'm going to load the data

20
00:00:53,190 --> 00:00:56,335
and we'll just look at the head of that data frame.

21
00:00:56,335 --> 00:01:00,280
You see I have the four measurements,

22
00:01:00,280 --> 00:01:03,725
these four feature columns, sepal length,

23
00:01:03,725 --> 00:01:05,600
sepal width, petal length,

24
00:01:05,600 --> 00:01:08,130
petal width, and their measurements in inches.

25
00:01:08,130 --> 00:01:10,345
Then, I have the species,

26
00:01:10,345 --> 00:01:13,750
the first five are all setosa.

27
00:01:14,000 --> 00:01:16,150
We can also look at some

28
00:01:16,150 --> 00:01:17,900
characteristics of that data frame.

29
00:01:17,900 --> 00:01:19,300
In fact, we see that

30
00:01:19,300 --> 00:01:22,945
our four features are all numeric, which is nice.

31
00:01:22,945 --> 00:01:26,590
Our species is a factor with three levels because there's

32
00:01:26,590 --> 00:01:30,720
three flower species we're trying to classify here.

33
00:01:30,720 --> 00:01:33,110
So, something we want to do right

34
00:01:33,110 --> 00:01:34,880
away before we get too far in

35
00:01:34,880 --> 00:01:37,535
any classification problem is to look at

36
00:01:37,535 --> 00:01:41,470
the frequency table of the label.

37
00:01:41,590 --> 00:01:43,650
In this case, you can see

38
00:01:43,650 --> 00:01:46,525
the three flower species: there's 50 setosa,

39
00:01:46,525 --> 00:01:49,630
50 versicolor, and 50 virginica.

40
00:01:49,630 --> 00:01:51,570
That's really great because that

41
00:01:51,570 --> 00:01:53,520
means we have what we call balanced

42
00:01:53,520 --> 00:01:58,365
cases or balanced classes for our classification problem.

43
00:01:58,365 --> 00:02:01,050
That makes a lot easier.

44
00:02:01,050 --> 00:02:03,670
We'll just start also by looking,

45
00:02:03,670 --> 00:02:05,765
just to have a look at the data.

46
00:02:05,765 --> 00:02:09,405
So, we can plot with ggplot2 here.

47
00:02:09,405 --> 00:02:13,225
We can plot sepal width versus sepal length,

48
00:02:13,225 --> 00:02:16,480
and petal width versus sepal length.

49
00:02:18,440 --> 00:02:20,575
Those are just two of

50
00:02:20,575 --> 00:02:24,040
several possible views of these data but we just want

51
00:02:24,040 --> 00:02:29,330
to see how well we can separate these classes.

52
00:02:29,330 --> 00:02:35,760
You see we have the legend over here.

53
00:02:35,760 --> 00:02:37,700
So, it looks like setosa is

54
00:02:37,700 --> 00:02:39,740
pretty well separated on this first flight.

55
00:02:39,740 --> 00:02:42,090
It got sepal width, sepal length,

56
00:02:42,090 --> 00:02:48,140
and versicolor, virginica have a fair degree of overlap.

57
00:02:48,140 --> 00:02:50,965
Now in this view,

58
00:02:50,965 --> 00:02:53,110
which is sepal length and petal width,

59
00:02:53,110 --> 00:02:55,195
against setosa's off by itself,

60
00:02:55,195 --> 00:02:57,120
there's a little bit better separation

61
00:02:57,120 --> 00:02:58,230
between versicolor and

62
00:02:58,230 --> 00:03:02,245
virginica but you still see some overlap here.

63
00:03:02,245 --> 00:03:05,540
So, we can't expect perfect results in a case like this.

64
00:03:05,540 --> 00:03:07,980
There's overlap given the features we have,

65
00:03:07,980 --> 00:03:10,705
which is almost always the case in machine learning.

66
00:03:10,705 --> 00:03:12,570
If these things were easy to separate,

67
00:03:12,570 --> 00:03:15,740
we wouldn't bother with fancy machine learning models.

68
00:03:15,740 --> 00:03:18,570
So, I'm going to prepare this data set.

69
00:03:18,570 --> 00:03:21,430
The first thing of course is I have to scale it,

70
00:03:21,430 --> 00:03:23,510
you always have to scale.

71
00:03:23,510 --> 00:03:29,010
So, I'm going to scale by using lapply to iterate over

72
00:03:29,010 --> 00:03:35,090
the four feature columns and apply the scale function.

73
00:03:35,090 --> 00:03:39,000
Then, just put those back into the data frame as scaled,

74
00:03:39,000 --> 00:03:41,440
and then we'll look at a summary.

75
00:03:41,440 --> 00:03:45,350
I'm also going to compute the standard deviation.

76
00:03:45,350 --> 00:03:47,940
So, we've done that.

77
00:03:47,940 --> 00:03:52,210
You can see from the summary that the mean is

78
00:03:52,210 --> 00:03:57,100
now zero for each of my four characteristics.

79
00:03:57,100 --> 00:04:00,470
Here's the standard deviation of

80
00:04:00,470 --> 00:04:05,160
those four features and they're all one.

81
00:04:05,160 --> 00:04:06,970
So, that's exactly what I want.

82
00:04:06,970 --> 00:04:09,575
So, now, my data is centered and scaled

83
00:04:09,575 --> 00:04:13,495
to zero mean standard deviation of one.

84
00:04:13,495 --> 00:04:15,590
So, now, I can get serious

85
00:04:15,590 --> 00:04:19,610
about building my machine learning model.

86
00:04:19,610 --> 00:04:21,710
But to evaluate my model,

87
00:04:21,710 --> 00:04:23,700
I need to independently

88
00:04:23,700 --> 00:04:26,950
sample a training and testing data set.

89
00:04:26,950 --> 00:04:29,650
So, I'm going to use from dplyr,

90
00:04:29,650 --> 00:04:34,085
the sample_frac function,

91
00:04:34,085 --> 00:04:37,250
and I'm going to make a training data

92
00:04:37,250 --> 00:04:39,330
set with 70 percent of the data,

93
00:04:39,330 --> 00:04:42,480
and 30 percent of the data is going to be

94
00:04:42,480 --> 00:04:47,040
used as my test data set.

95
00:04:47,040 --> 00:04:49,880
So, this way, I have independent sample.

96
00:04:49,880 --> 00:04:51,260
So, I'm not testing

97
00:04:51,260 --> 00:04:52,970
with my training data set which would give

98
00:04:52,970 --> 00:04:56,480
me biased evaluation of my model where I,

99
00:04:56,480 --> 00:04:59,150
in fact, might think my model is way better than it

100
00:04:59,150 --> 00:05:02,310
is even if it's just learning the training data.

101
00:05:02,310 --> 00:05:04,610
But of course, I want to test

102
00:05:04,610 --> 00:05:09,725
an independent data to get a more realistic evaluation.

103
00:05:09,725 --> 00:05:16,020
So, in the RKKNN library,

104
00:05:16,020 --> 00:05:21,225
there's the KKNN machine learning model

105
00:05:21,225 --> 00:05:22,620
for K-Nearest Neighbor,

106
00:05:22,620 --> 00:05:26,175
and I'm using the R modeling language here.

107
00:05:26,175 --> 00:05:29,135
If you're not familiar with it, it's fairly simple.

108
00:05:29,135 --> 00:05:31,880
You see species, and then there's

109
00:05:31,880 --> 00:05:34,210
the squiggly line or the tilde dot.

110
00:05:34,210 --> 00:05:36,965
So, that means I'm in a model species.

111
00:05:36,965 --> 00:05:39,190
So, species is modeled by.

112
00:05:39,190 --> 00:05:40,370
So, this little squiggly line,

113
00:05:40,370 --> 00:05:43,350
I say in English, is modeled by,

114
00:05:43,350 --> 00:05:45,800
species is modeled by dot,

115
00:05:45,800 --> 00:05:46,820
and dot just means

116
00:05:46,820 --> 00:05:49,090
all the other columns in that data frame,

117
00:05:49,090 --> 00:05:51,095
and there's only those four.

118
00:05:51,095 --> 00:05:55,085
My training data is train.iris,

119
00:05:55,085 --> 00:05:57,735
my test data is test.iris,

120
00:05:57,735 --> 00:06:02,495
and we're going to do a KNNK equals three model.

121
00:06:02,495 --> 00:06:05,270
That's why I call it knn.3,

122
00:06:05,270 --> 00:06:10,045
and we can print then the summary of that model object.

123
00:06:10,045 --> 00:06:13,620
That'll tell us some interesting things, we hope.

124
00:06:13,620 --> 00:06:16,315
All right. So, the first thing in the summary

125
00:06:16,315 --> 00:06:20,010
is the formula which I said was species

126
00:06:20,010 --> 00:06:26,750
modeled by dot and it gives me back the whole train.

127
00:06:26,750 --> 00:06:30,565
Now, I get this response table here.

128
00:06:30,565 --> 00:06:33,890
So, you can see for species setosa, versicolor,

129
00:06:33,890 --> 00:06:37,730
virginica, it gives me

130
00:06:37,730 --> 00:06:41,860
the probabilities based on this K-Nearest Neighbor.

131
00:06:41,860 --> 00:06:44,265
So, setosa as we already saw,

132
00:06:44,265 --> 00:06:46,720
was pretty well separated from its neighbors.

133
00:06:46,720 --> 00:06:50,960
So, those probabilities are all either one or zero.

134
00:06:51,320 --> 00:06:56,615
Say the first case of versicolor is 1.0,

135
00:06:56,615 --> 00:06:58,675
probability that it is versicolor.

136
00:06:58,675 --> 00:07:00,060
But this next one,

137
00:07:00,060 --> 00:07:02,330
there's like a nine percent probability it's really

138
00:07:02,330 --> 00:07:05,230
virginica because of the overlap.

139
00:07:05,230 --> 00:07:08,960
You see here, we actually classify this as

140
00:07:08,960 --> 00:07:11,935
virginica because the probability

141
00:07:11,935 --> 00:07:14,660
is slightly more than here.

142
00:07:14,660 --> 00:07:15,930
So, that's how this works

143
00:07:15,930 --> 00:07:18,070
for this multi-class classification.

144
00:07:18,070 --> 00:07:19,560
We look at these probabilities,

145
00:07:19,560 --> 00:07:23,960
we take the highest probability as our predicted class.

146
00:07:25,130 --> 00:07:30,235
So, we can actually add to our data frame a column

147
00:07:30,235 --> 00:07:32,630
called predicted by running

148
00:07:32,630 --> 00:07:35,815
the predict method over this model.

149
00:07:35,815 --> 00:07:39,875
I'm going to also add a column called correct,

150
00:07:39,875 --> 00:07:42,440
and correct is illogical.

151
00:07:42,440 --> 00:07:45,540
So, if the species which is

152
00:07:45,540 --> 00:07:51,135
the actual species is equal to the predicted,

153
00:07:51,135 --> 00:07:52,970
then that's going to be true, otherwise,

154
00:07:52,970 --> 00:07:54,110
it's going to be false.

155
00:07:54,110 --> 00:07:58,915
This little bit of

156
00:07:58,915 --> 00:08:00,460
arithmetic here just gives us

157
00:08:00,460 --> 00:08:03,220
the percent in terms of accuracy.

158
00:08:03,220 --> 00:08:04,480
So, we've got a little over

159
00:08:04,480 --> 00:08:06,740
95 percent accuracy, which is great.

160
00:08:06,740 --> 00:08:09,395
So, it means we have a few errors.

161
00:08:09,395 --> 00:08:11,930
We can do a plot,

162
00:08:11,930 --> 00:08:13,800
and try to look at those errors,

163
00:08:13,800 --> 00:08:16,075
and try to understand what's going on.

164
00:08:16,075 --> 00:08:19,290
So, I've used shape to show

165
00:08:19,290 --> 00:08:22,280
whether it's correct classification or not,

166
00:08:22,280 --> 00:08:26,460
and I'm using color for the predicted species.

167
00:08:26,460 --> 00:08:29,315
Keep in mind it's the predicted species.

168
00:08:29,315 --> 00:08:35,055
So, all the little upward triangles are correct.

169
00:08:35,055 --> 00:08:37,020
You see we got, in this first few of

170
00:08:37,020 --> 00:08:39,250
sepal width versus sepal length,

171
00:08:39,250 --> 00:08:41,000
we got all the virginicas,

172
00:08:41,000 --> 00:08:44,340
I'm sorry, all the setosas correct.

173
00:08:44,600 --> 00:08:51,355
But you can see that there is a circle for a virginica.

174
00:08:51,355 --> 00:08:54,800
I don't see a circle for, there's another one.

175
00:08:54,800 --> 00:08:57,385
So, there's a couple of errors there.

176
00:08:57,385 --> 00:09:00,900
You can see there's a class boundary running somewhere

177
00:09:00,900 --> 00:09:04,985
through here and that's why we have those errors.

178
00:09:04,985 --> 00:09:09,330
In another view, same issue.

179
00:09:09,330 --> 00:09:10,880
We've got our class boundary

180
00:09:10,880 --> 00:09:12,410
running somewhere through here,

181
00:09:12,410 --> 00:09:13,820
so we've got a couple of errors.

182
00:09:13,820 --> 00:09:15,890
That makes sense since we're

183
00:09:15,890 --> 00:09:22,110
doing K-Nearest Neighbor classification.

184
00:09:22,270 --> 00:09:25,850
Well, so, did you

185
00:09:25,850 --> 00:09:28,220
feel like you know a little bit about building and

186
00:09:28,220 --> 00:09:30,710
evaluating classifier models or

187
00:09:30,710 --> 00:09:34,590
machine learning models in general using R at this point?

