0
00:00:00,770 --> 00:00:04,270
>> In this video, I'm going to discuss

1
00:00:04,270 --> 00:00:06,934
some basics of Machine Learning in general,

2
00:00:06,934 --> 00:00:08,725
and we're also going to talk about

3
00:00:08,725 --> 00:00:13,290
the K- Nearest Neighbors Machine Learning algorithm.

4
00:00:13,290 --> 00:00:16,210
So, that's our first Machine Learning algorithm.

5
00:00:16,210 --> 00:00:19,780
So, let's talk about the basics of Machine Learning.

6
00:00:19,780 --> 00:00:21,610
So, what's our goal?

7
00:00:21,610 --> 00:00:24,240
Our goal is to train a model to

8
00:00:24,240 --> 00:00:27,250
produce useful response to the input.

9
00:00:27,250 --> 00:00:29,470
So, we have an input, we want a response.

10
00:00:29,470 --> 00:00:32,415
The response is typically a prediction

11
00:00:32,415 --> 00:00:37,965
of something that we want to know about those data.

12
00:00:37,965 --> 00:00:39,520
When we input something we

13
00:00:39,520 --> 00:00:42,235
get that response. That's our prediction.

14
00:00:42,235 --> 00:00:45,480
We can write in a very generic term as

15
00:00:45,480 --> 00:00:49,230
you've seen a formula like this where we say

16
00:00:49,230 --> 00:00:52,470
our Machine Learning algorithm which is a function F

17
00:00:52,470 --> 00:00:55,845
of X equals Y is our prediction.

18
00:00:55,845 --> 00:00:58,560
So X is our data cases

19
00:00:58,560 --> 00:01:01,125
that we have and Y is the prediction.

20
00:01:01,125 --> 00:01:03,600
If we know what those Y values

21
00:01:03,600 --> 00:01:05,945
are when we're training the model,

22
00:01:05,945 --> 00:01:08,285
we call those the labels,

23
00:01:08,285 --> 00:01:11,820
and if we'd have known labels where someone has gone

24
00:01:11,820 --> 00:01:13,680
through and marked the cases

25
00:01:13,680 --> 00:01:16,040
so we know what the right answer is,

26
00:01:16,040 --> 00:01:18,420
we call that Supervised Machine Learning because

27
00:01:18,420 --> 00:01:22,860
the learning is supervised by knowing the labels.

28
00:01:23,440 --> 00:01:25,970
In Supervise Machine Learning,

29
00:01:25,970 --> 00:01:30,190
the features which I've shown as X in my formula there,

30
00:01:30,190 --> 00:01:33,375
provide information to train the model.

31
00:01:33,375 --> 00:01:38,070
So those are one or more columns in say

32
00:01:38,070 --> 00:01:39,690
a data table that have

33
00:01:39,690 --> 00:01:42,060
that information and the labels

34
00:01:42,060 --> 00:01:45,045
are those correct answers as I've already said.

35
00:01:45,045 --> 00:01:48,490
Unsupervised Machine Learning, we only have features.

36
00:01:48,490 --> 00:01:50,560
We don't know the labels and

37
00:01:50,560 --> 00:01:52,770
so we have to use very different techniques,

38
00:01:52,770 --> 00:01:53,980
and we'll get to

39
00:01:53,980 --> 00:01:57,020
Unsupervised Learning later in the course.

40
00:01:57,020 --> 00:02:00,615
So, what's the process here?

41
00:02:00,615 --> 00:02:04,349
We'll talk about this other parts of the class as well,

42
00:02:04,349 --> 00:02:05,460
because it's important to keep in

43
00:02:05,460 --> 00:02:07,140
mind that Machine Learning isn't

44
00:02:07,140 --> 00:02:09,600
just a linear thing

45
00:02:09,600 --> 00:02:12,180
that you just follow a recipe and boom you're done.

46
00:02:12,180 --> 00:02:13,585
It's complicated.

47
00:02:13,585 --> 00:02:16,320
Things go wrong and it's more of

48
00:02:16,320 --> 00:02:20,100
a scientific process, an exploration process.

49
00:02:20,100 --> 00:02:21,270
So, the first thing is we have to

50
00:02:21,270 --> 00:02:23,205
acquire and ingest data.

51
00:02:23,205 --> 00:02:25,470
We have to explore those data.

52
00:02:25,470 --> 00:02:27,900
We have to understand the relationship between

53
00:02:27,900 --> 00:02:31,165
the features and the label or between different features.

54
00:02:31,165 --> 00:02:34,765
We'll get to that in another module of this class.

55
00:02:34,765 --> 00:02:37,525
We need to prepare the data so that they're not

56
00:02:37,525 --> 00:02:40,780
errors and other problems with the data.

57
00:02:40,780 --> 00:02:42,790
Another module in this class

58
00:02:42,790 --> 00:02:44,660
is going to address that of course,

59
00:02:44,660 --> 00:02:49,215
and that can provide cleaning the data,

60
00:02:49,215 --> 00:02:53,205
feature engineering and then splitting the data set.

61
00:02:53,205 --> 00:02:55,750
So we never want to train and evaluate or

62
00:02:55,750 --> 00:02:58,250
test the model on the same data.

63
00:02:58,250 --> 00:03:00,640
So, we're going to talk about methods later in

64
00:03:00,640 --> 00:03:03,100
the class to split the data.

65
00:03:03,100 --> 00:03:07,405
We need to then

66
00:03:07,405 --> 00:03:09,820
construct and evaluate a Machine Learning model,

67
00:03:09,820 --> 00:03:11,500
that's what we're leading up

68
00:03:11,500 --> 00:03:13,905
to with all this preparation,

69
00:03:13,905 --> 00:03:18,760
and maybe we're going to find out

70
00:03:18,760 --> 00:03:22,340
that evaluation doesn't show us the performance we

71
00:03:22,340 --> 00:03:26,330
want or are there some systematic bias or problem.

72
00:03:26,330 --> 00:03:28,175
Well, we may have to repeat

73
00:03:28,175 --> 00:03:30,590
many or all of the steps above,

74
00:03:30,590 --> 00:03:33,170
until we get where we want to go.

75
00:03:33,170 --> 00:03:36,760
So let's talk about our first Machine Learning algorithm

76
00:03:36,760 --> 00:03:38,020
in this class which is what

77
00:03:38,020 --> 00:03:42,130
we call the K-Nearest Neighbors Algorithm or KNN.

78
00:03:42,130 --> 00:03:44,020
It's a really simple method,

79
00:03:44,020 --> 00:03:46,045
it's been around for a very long time.

80
00:03:46,045 --> 00:03:48,840
In many cases, it's still very effective.

81
00:03:48,840 --> 00:03:51,970
It doesn't require any particular training.

82
00:03:51,970 --> 00:03:56,395
It's a little odd. It needs labels to know what

83
00:03:56,395 --> 00:04:01,170
the cases are but we don't train on those labels.

84
00:04:01,170 --> 00:04:02,950
It's just whatever the data are

85
00:04:02,950 --> 00:04:05,260
they determine the nearest neighbors.

86
00:04:05,260 --> 00:04:07,690
So, it's a little bit of an outlier in terms of

87
00:04:07,690 --> 00:04:09,350
other Machine Learning Algorithms

88
00:04:09,350 --> 00:04:11,770
that we'll deal with in this course.

89
00:04:11,770 --> 00:04:14,740
A new case. So, we have have

90
00:04:14,740 --> 00:04:17,660
some known cases, known label.

91
00:04:17,660 --> 00:04:19,635
We get a new case and we

92
00:04:19,635 --> 00:04:22,730
find out where it is in the feature space,

93
00:04:22,730 --> 00:04:24,910
and we look around and we

94
00:04:24,910 --> 00:04:27,690
find some near neighbors and we use

95
00:04:27,690 --> 00:04:30,070
the ones that are

96
00:04:30,070 --> 00:04:33,040
in the nearest proximity to our new case,

97
00:04:33,040 --> 00:04:38,610
to find out how to classify or predict that new case.

98
00:04:38,610 --> 00:04:41,090
We have to use a distance metric, right?

99
00:04:41,090 --> 00:04:43,190
So I'm going to tell you about proximity,

100
00:04:43,190 --> 00:04:45,650
where I need a distance metric.

101
00:04:45,650 --> 00:04:48,685
So, I need to measure distance from

102
00:04:48,685 --> 00:04:54,115
my new case to existing cases where I have marked labels.

103
00:04:54,115 --> 00:04:56,995
Most commonly, we use euclidean distance

104
00:04:56,995 --> 00:04:59,865
but other distances are used.

105
00:04:59,865 --> 00:05:02,230
We can also use weighted distance.

106
00:05:02,230 --> 00:05:07,965
We may care if not one case is close to another,

107
00:05:07,965 --> 00:05:09,820
we may want to weight that higher than

108
00:05:09,820 --> 00:05:12,290
one that's further away.

109
00:05:12,290 --> 00:05:17,060
K, the K in the K-Nearest Neighbors or KNN,

110
00:05:17,060 --> 00:05:19,130
defines the number of neighbors we're going to

111
00:05:19,130 --> 00:05:22,040
use to make that prediction.

112
00:05:22,040 --> 00:05:25,040
The basic algorithm then is really simple.

113
00:05:25,040 --> 00:05:26,305
Once we know how to compute

114
00:05:26,305 --> 00:05:29,850
the distance and weight or not weight the distance,

115
00:05:29,850 --> 00:05:32,600
we basically go for a majority vote,

116
00:05:32,600 --> 00:05:34,810
and that's the label that we are

117
00:05:34,810 --> 00:05:36,490
forecasting or predicting for

118
00:05:36,490 --> 00:05:39,075
the K-Nearest Neighbor's Algorithm.

119
00:05:39,075 --> 00:05:41,815
So, let's look at an example here.

120
00:05:41,815 --> 00:05:46,030
So, I have these data on my screen here,

121
00:05:46,030 --> 00:05:50,950
and you can see there's red triangles and blue dots.

122
00:05:51,210 --> 00:05:56,405
Let's try the K equals one case first, okay?

123
00:05:56,405 --> 00:05:59,060
I have see the question mark

124
00:05:59,060 --> 00:06:01,030
just appeared in the middle there,

125
00:06:01,030 --> 00:06:03,050
and I want to know how I'm going to

126
00:06:03,050 --> 00:06:07,850
classify that data case.

127
00:06:07,850 --> 00:06:09,405
So, I have my two features.

128
00:06:09,405 --> 00:06:12,230
You see the vertical and the horizontal axes.

129
00:06:12,230 --> 00:06:15,470
So, there's two features in this and I need to find

130
00:06:15,470 --> 00:06:17,900
the nearest neighbor and

131
00:06:17,900 --> 00:06:21,025
we'll just use euclidean distance in this case.

132
00:06:21,025 --> 00:06:23,045
So we draw a circle there,

133
00:06:23,045 --> 00:06:25,900
and we find that as the circle expands,

134
00:06:25,900 --> 00:06:30,375
here's the first case we hit is a red triangle.

135
00:06:30,375 --> 00:06:33,360
So, we classify this question mark now,

136
00:06:33,360 --> 00:06:36,890
we say, oh yeah that's a red triangle.

137
00:06:37,100 --> 00:06:40,515
But we could also say try K equals three.

138
00:06:40,515 --> 00:06:42,400
You can try different values of K and

139
00:06:42,400 --> 00:06:45,570
see what works best for your application.

140
00:06:45,570 --> 00:06:49,000
Now we get this bigger circle because we have to

141
00:06:49,000 --> 00:06:53,000
inscribe the first three data points we encounter,

142
00:06:53,000 --> 00:06:54,805
and it looks like in that case,

143
00:06:54,805 --> 00:06:57,230
the blue dots win.

144
00:06:57,230 --> 00:07:00,100
So, it's quite a different result we get here

145
00:07:00,100 --> 00:07:02,130
between K equals one and K equals three,

146
00:07:02,130 --> 00:07:03,390
and which is better,

147
00:07:03,390 --> 00:07:05,250
you'll have to evaluate and see

148
00:07:05,250 --> 00:07:08,190
and you'll get a chance to do that in the lab.

149
00:07:08,390 --> 00:07:11,610
But there's a problem with K- Nearest Neighbors,

150
00:07:11,610 --> 00:07:14,430
and this inpervades all Machine Learning

151
00:07:14,430 --> 00:07:16,125
but it's really evident here.

152
00:07:16,125 --> 00:07:19,650
It's an effective classifier when it works,

153
00:07:19,650 --> 00:07:21,270
but it suffers from something we call

154
00:07:21,270 --> 00:07:23,010
the curse of dimensionality.

155
00:07:23,010 --> 00:07:25,200
Let me just give you an example so you

156
00:07:25,200 --> 00:07:28,215
can wrap your heads around what this problem is.

157
00:07:28,215 --> 00:07:30,010
So in one dimension,

158
00:07:30,010 --> 00:07:32,395
say I just have one feature.

159
00:07:32,395 --> 00:07:35,090
So, one dimension, and I have 10 samples.

160
00:07:35,090 --> 00:07:37,745
Sort of pretty, maybe that's

161
00:07:37,745 --> 00:07:40,750
dense enough sampling of

162
00:07:40,750 --> 00:07:42,660
my known cases and I can

163
00:07:42,660 --> 00:07:44,590
get a pretty accurate classifier.

164
00:07:44,590 --> 00:07:47,280
But if I want that same density in

165
00:07:47,280 --> 00:07:49,050
two dimensions which is like the case

166
00:07:49,050 --> 00:07:51,110
I just showed you was two dimensional,

167
00:07:51,110 --> 00:07:53,520
I need 100 samples.

168
00:07:53,520 --> 00:07:55,580
If I have three dimensions,

169
00:07:55,580 --> 00:07:58,110
I need 1,000 samples,

170
00:07:58,110 --> 00:08:01,640
four dimensions I need 10,000 samples.

171
00:08:01,640 --> 00:08:03,380
So, if I had 10 features,

172
00:08:03,380 --> 00:08:08,040
I'd need tenth of a the tenth samples which is

173
00:08:08,040 --> 00:08:10,860
like 10 billion samples to

174
00:08:10,860 --> 00:08:14,010
get the density of my one dimensional case.

175
00:08:14,010 --> 00:08:17,520
This is what we call in Machine Learning the curse of

176
00:08:17,520 --> 00:08:20,240
dimensionality and it affects

177
00:08:20,240 --> 00:08:21,930
all Machine Learning as I said,

178
00:08:21,930 --> 00:08:23,730
but it's particularly a problem

179
00:08:23,730 --> 00:08:26,745
with algorithms like K- Nearest Neighbors.

180
00:08:26,745 --> 00:08:28,915
So, just keep that in mind

181
00:08:28,915 --> 00:08:32,650
when you're working with algorithms like that.

