0
00:00:05,430 --> 00:00:10,510
So now that you understand the basics of classification, let’s talk about loss functions, because

1
00:00:10,510 --> 00:00:15,209
that determines the major difference between a lot of machine learning methods. Okay so

2
00:00:15,209 --> 00:00:21,230
how do we measure classification error? Well, one very very simple way to do it is to use

3
00:00:21,230 --> 00:00:27,400
just a fraction of times our predictions are wrong. So just the fraction of times the sign

4
00:00:27,400 --> 00:00:34,120
of f(x) is not equal to the truth y, and I can write it like that, okay? The issue with

5
00:00:34,120 --> 00:00:40,600
this particular way of measuring classification error is that if you want to try to minimize

6
00:00:40,600 --> 00:00:45,399
this, you could run into a lot of trouble because it’s computationally hard to minimize

7
00:00:45,399 --> 00:00:50,210
this thing. So let’s give you the geometric picture here: the decision boundary is this

8
00:00:50,210 --> 00:00:57,629
line right here, and f being positive is here and f being negative is here, and the red

9
00:00:57,629 --> 00:01:02,030
points are all misclassified. And now what I’m going to do is something you might not

10
00:01:02,030 --> 00:01:06,490
be expecting which is that I’m going to move all the correctly classified points across

11
00:01:06,490 --> 00:01:10,940
the decision boundary to one side, and all the misclassified points to the other side.

12
00:01:10,940 --> 00:01:15,830
There they go, we’ll move all the misclassified points across the decision boundary and then

13
00:01:15,830 --> 00:01:20,280
all the correctly classified ones. Okay, so I’m glad I did that, now I have the ones

14
00:01:20,280 --> 00:01:24,180
we’ve got correct over here and the ones we got wrong over there. And now the labels

15
00:01:24,180 --> 00:01:29,740
on this plot are wrong because we changed it, so it’s actually something like that.

16
00:01:29,740 --> 00:01:38,610
Okay so over here, on the right either f is positive and y is also positive, so y-f is

17
00:01:38,610 --> 00:01:44,820
positive, or they’re both negative so the product is positive again. And then over here

18
00:01:44,820 --> 00:01:50,079
on the left, we have cases where the sign of f is different from y, so the product is

19
00:01:50,079 --> 00:01:57,509
negative. And then the ones I suffer a penalty for are all these guys over there.

20
00:01:57,509 --> 00:02:02,530
Okay so let’s keep that image in mind over here, and then I’ll put the labels up. Now

21
00:02:02,530 --> 00:02:09,500
this function tells us what kind of penalty we’re going to issue for being wrong. Okay

22
00:02:09,500 --> 00:02:18,540
so right now, if y times f is positive, it means we got it right and we lose 0 points.

23
00:02:18,540 --> 00:02:24,909
And if we get it wrong, the point is on the wrong side and so we lose one point. Now I’m

24
00:02:24,909 --> 00:02:29,260
just going to write this function another way which is like this, okay, so we lose one

25
00:02:29,260 --> 00:02:36,569
point if y-f is less than 0, and otherwise we lose no points. And this is the classic

26
00:02:36,569 --> 00:02:41,340
0-1 loss. It just tells you whether your classifier is right or wrong. And then this thing is

27
00:02:41,340 --> 00:02:46,169
called a loss function, and there are other loss functions too, and this one’s nice

28
00:02:46,169 --> 00:02:50,510
because – but it’s problematic because it’s not smooth, and we have issues with

29
00:02:50,510 --> 00:02:56,959
things that are not smooth in machine learning. So let’s try some more loss functions. So

30
00:02:56,959 --> 00:03:05,859
while we’re doing this, just keep in mind that these points over here are the ones that

31
00:03:05,859 --> 00:03:09,689
are very wrong, because they’re on the wrong side of the decision boundary, but they’re

32
00:03:09,689 --> 00:03:15,089
really far away from it too. And these points are wrong, but they’re not as bad; they’re

33
00:03:15,089 --> 00:03:18,680
on the wrong side of the decision boundary but they’re pretty close to it. And then

34
00:03:18,680 --> 00:03:22,329
we’ll say these points are sort of correct, and we’ll say these points are very correct.

35
00:03:22,329 --> 00:03:29,839
And what we’ll really like to have are loss functions that don’t penalize the very correct

36
00:03:29,839 --> 00:03:39,089
ones, but the penalty gets worse and worse as you go to the left. But maybe we can use

37
00:03:39,089 --> 00:03:44,969
some other loss function, something that – you know, maybe we get a small penalty for being

38
00:03:44,969 --> 00:03:49,339
sort of correct and then a bigger penalty for being sort of wrong and then a huge penalty

39
00:03:49,339 --> 00:03:57,180
for being very wrong. Something that looks like this would be a deal. So again, this

40
00:03:57,180 --> 00:04:05,479
is – the horizontal axis is y times f, and this red one is 1 if y disagrees with the

41
00:04:05,479 --> 00:04:10,089
sign of f, and then the other curves are different loss functions and they’re actually for

42
00:04:10,089 --> 00:04:15,249
different machine learning algorithms. And again, just keep in mind that on the right

43
00:04:15,249 --> 00:04:18,769
– these are points that are on the correct side of the decision boundary, they don’t

44
00:04:18,769 --> 00:04:22,780
suffer much penalty and on the left, these are points that are incorrectly classified

45
00:04:22,780 --> 00:04:30,240
and they suffer more penalty. This is the loss function that AdaBoost uses.

46
00:04:30,240 --> 00:04:35,650
AdaBoost is one of the machine learning methods that we’ll cover in the course. And this

47
00:04:35,650 --> 00:04:40,139
is the loss function that support vector machines use; it’s a line, and then it’s another

48
00:04:40,139 --> 00:04:47,599
flat line. And this is the loss function for logistic regression, and we’re going to

49
00:04:47,599 --> 00:04:52,900
cover all three of these. Now I’m going to write this idea about the loss functions

50
00:04:52,900 --> 00:05:02,909
in notation on the next slide. Okay so start here: the misclassification error is the fraction

51
00:05:02,909 --> 00:05:09,590
of times that the sign of f is not equal to the truth y that’s this. I can rewrite it

52
00:05:09,590 --> 00:05:19,409
this way, okay, the number of times y times f is less than 0. And then we’ll upper-bound

53
00:05:19,409 --> 00:05:27,969
this by these loss functions. Okay, so then what is a good way to try to reduce the misclassification

54
00:05:27,969 --> 00:05:34,520
error which is that guy? Well you could just try to minimize the average loss. So if you

55
00:05:34,520 --> 00:05:40,050
had a choice of functions f, you could try to choose f to minimize this thing, which

56
00:05:40,050 --> 00:05:46,189
hopefully would also minimize this but in a computationally easier way. So here’s

57
00:05:46,189 --> 00:05:50,919
your first try for a machine learning algorithm. Just choose the function f to minimize the

58
00:05:50,919 --> 00:05:56,849
average loss. And this seems like a good idea, right? Well it is, and that’s what most

59
00:05:56,849 --> 00:06:01,449
machine learning methods are based on, and how to do this minimization over models to

60
00:06:01,449 --> 00:06:06,539
get the best one, that involves some optimization techniques, which go on behind the scenes.

61
00:06:06,539 --> 00:06:10,699
But there’s one more thing I didn’t quite tell you, which is that we want to do more

62
00:06:10,699 --> 00:06:15,669
than have a low training error. We want to predict well on data that we haven’t seen

63
00:06:15,669 --> 00:06:21,360
before. We want to, you know, generalize the new points, and that’s why we need statistical

64
00:06:21,360 --> 00:06:28,050
learning theory, because this algorithm that I showed you – that’s not right, and you’ll

65
00:06:28,050 --> 00:06:34,550
see why. It’s pretty good, but it’s missing this key element that tells – that encourages

66
00:06:34,550 --> 00:06:39,560
the model to stay simple and not over-fit. So I’ll talk about statistical learning

67
00:06:39,560 --> 00:06:40,370
theory shortly.

