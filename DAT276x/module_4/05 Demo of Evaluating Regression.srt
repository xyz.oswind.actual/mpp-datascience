0
00:00:01,610 --> 00:00:04,275
>> Hello. Welcome back.

1
00:00:04,275 --> 00:00:06,165
In the previous video,

2
00:00:06,165 --> 00:00:09,120
I constructed a linear regression model

3
00:00:09,120 --> 00:00:10,540
for the auto price.

4
00:00:10,540 --> 00:00:15,135
I did that using that R modeling formula.

5
00:00:15,135 --> 00:00:17,595
Now, the question is how good is that model?

6
00:00:17,595 --> 00:00:19,635
So, I need to evaluate

7
00:00:19,635 --> 00:00:22,725
a regression model in R. In this demo,

8
00:00:22,725 --> 00:00:26,295
I'm going to show you some tools you can use to evaluate

9
00:00:26,295 --> 00:00:31,635
most any regression model you create with R. So,

10
00:00:31,635 --> 00:00:32,850
on my screen here,

11
00:00:32,850 --> 00:00:38,110
I have created a function which I call print_metrics.

12
00:00:39,240 --> 00:00:41,875
When you're evaluating regression models

13
00:00:41,875 --> 00:00:44,090
we tend to focus on the residuals,

14
00:00:44,090 --> 00:00:45,325
that is the errors.

15
00:00:45,325 --> 00:00:46,905
It's the difference between

16
00:00:46,905 --> 00:00:51,050
the label value and the score.

17
00:00:51,510 --> 00:00:54,510
Notice there can be a sign convention issue here,

18
00:00:54,510 --> 00:00:56,950
could be the score minus the label value.

19
00:00:56,950 --> 00:00:58,720
In this case, I'm doing label value

20
00:00:58,720 --> 00:01:01,720
minus the score but it doesn't really matter.

21
00:01:01,720 --> 00:01:05,770
I can compute the square of those residuals,

22
00:01:05,770 --> 00:01:11,335
and then I can compute things like R squared,

23
00:01:11,335 --> 00:01:13,350
and adjusted R squared,

24
00:01:13,350 --> 00:01:14,870
where I apply the adjustment,

25
00:01:14,870 --> 00:01:18,060
the degree of freedom adjustment.

26
00:01:18,550 --> 00:01:22,730
So, the rest of this is just printing

27
00:01:22,730 --> 00:01:27,190
my table that I want of summary statistics.

28
00:01:27,190 --> 00:01:32,300
So, for these numeric quantities like mean squared error,

29
00:01:32,300 --> 00:01:35,760
I wrap them in as.character,

30
00:01:35,760 --> 00:01:37,800
so that I get a character representation.

31
00:01:37,800 --> 00:01:39,810
But I also wrap them in round

32
00:01:39,810 --> 00:01:42,945
so I get the appropriate number of digits.

33
00:01:42,945 --> 00:01:45,900
You can see things like mean squared errors,

34
00:01:45,900 --> 00:01:48,840
the sum of those squared residuals

35
00:01:48,840 --> 00:01:51,805
divided by the number et cetera.

36
00:01:51,805 --> 00:01:54,870
So, very simple arithmetic here for the most part.

37
00:01:54,870 --> 00:01:58,995
Let me just run this for you and you'll see the result.

38
00:01:58,995 --> 00:02:03,715
We get some idea of how good or bad our model is here.

39
00:02:03,715 --> 00:02:07,280
We can see the mean squared error is pretty small about

40
00:02:07,280 --> 00:02:10,760
two percent or 0.2.

41
00:02:10,760 --> 00:02:13,880
So, remember we're modeling log price here.

42
00:02:13,880 --> 00:02:17,940
So, these numbers aren't as good as they look.

43
00:02:17,940 --> 00:02:21,550
Root mean square is about 0.15,

44
00:02:22,760 --> 00:02:29,205
mean absolute error is 0.1, et cetera.

45
00:02:29,205 --> 00:02:31,000
You see the R squared is about

46
00:02:31,000 --> 00:02:35,790
0.89 and the adjusted R squared is about 0.88.

47
00:02:35,790 --> 00:02:37,620
So, those are respectable values.

48
00:02:37,620 --> 00:02:40,075
So, it looks like this model is promising,

49
00:02:40,075 --> 00:02:42,190
but we need to drill further.

50
00:02:42,190 --> 00:02:45,110
Just because you have reasonable looking metrics

51
00:02:45,110 --> 00:02:48,495
doesn't mean the model really is all that great.

52
00:02:48,495 --> 00:02:51,275
So, this code here,

53
00:02:51,275 --> 00:02:54,120
I'm going to create

54
00:02:54,120 --> 00:02:58,170
a histogram of the residuals themselves.

55
00:02:59,150 --> 00:03:02,940
I'm going to use ggplot with

56
00:03:02,940 --> 00:03:04,580
the histogram method and I

57
00:03:04,580 --> 00:03:07,035
compute a bandwidth that I want.

58
00:03:07,035 --> 00:03:13,220
I also going to put a density estimate there.

59
00:03:13,220 --> 00:03:17,180
So, I have to do the Y equals dot dot dot density,

60
00:03:17,180 --> 00:03:19,990
and a yes here and here,

61
00:03:19,990 --> 00:03:24,620
and of course, give it labels and titles.

62
00:03:24,620 --> 00:03:27,800
So, let's run this and evaluate.

63
00:03:27,800 --> 00:03:29,720
Okay. So, that's our histogram

64
00:03:29,720 --> 00:03:33,570
of the residuals and you see zero.

65
00:03:33,570 --> 00:03:35,990
You want this zero centered and you want it to

66
00:03:35,990 --> 00:03:38,865
look fairly like a normal distribution.

67
00:03:38,865 --> 00:03:40,895
Well, this middle part,

68
00:03:40,895 --> 00:03:42,620
a little bit does.

69
00:03:42,620 --> 00:03:45,215
But, there's clearly one outlier here

70
00:03:45,215 --> 00:03:48,370
and maybe a little bit too much mass over here.

71
00:03:48,370 --> 00:03:49,750
So, it's a bit skewed.

72
00:03:49,750 --> 00:03:51,770
It's actually hard to say whether

73
00:03:51,770 --> 00:03:54,050
it's more right or left skewed,

74
00:03:54,050 --> 00:03:56,360
but it's definitely heavy tailed,

75
00:03:56,360 --> 00:03:59,840
which means that there's

76
00:03:59,840 --> 00:04:02,150
some large residuals so

77
00:04:02,150 --> 00:04:05,655
certain cases aren't well accounted for in this model,

78
00:04:05,655 --> 00:04:09,375
and that is probably indicative of a problem.

79
00:04:09,375 --> 00:04:14,160
Another way to evaluate how normal

80
00:04:14,160 --> 00:04:16,530
the residuals are of

81
00:04:16,530 --> 00:04:19,950
a regression model is to use a qq normal plot.

82
00:04:19,950 --> 00:04:26,580
So, again, I use ggplot and there's a geom_qq.

83
00:04:26,670 --> 00:04:28,930
I think, the best way to explain

84
00:04:28,930 --> 00:04:30,710
this one is I'll just make the plot,

85
00:04:30,710 --> 00:04:33,715
and then I'll show you the result and we'll discuss it.

86
00:04:33,715 --> 00:04:38,095
So, on the horizontal axis,

87
00:04:38,095 --> 00:04:41,030
we plot the quantiles of a standard normal distribution.

88
00:04:41,030 --> 00:04:42,500
So, that's what we want to compare to.

89
00:04:42,500 --> 00:04:46,290
We want the R residuals to

90
00:04:46,290 --> 00:04:49,275
be a standard normal distribution

91
00:04:49,275 --> 00:04:50,800
or a normal distribution.

92
00:04:50,800 --> 00:04:52,580
On the vertical, we plot

93
00:04:52,580 --> 00:04:55,300
the quantiles of the residuals themselves.

94
00:04:55,300 --> 00:04:59,620
If they were indeed normally distributed,

95
00:04:59,620 --> 00:05:02,025
they would all lie on a straight line

96
00:05:02,025 --> 00:05:04,050
because the quintiles would line up.

97
00:05:04,050 --> 00:05:07,480
Well, you can see there's a little bit of zigzag,

98
00:05:07,480 --> 00:05:09,400
a little bit of S-shape to this.

99
00:05:09,400 --> 00:05:13,030
So, clearly we've got some heavy tails,

100
00:05:13,030 --> 00:05:15,674
but it's not terrible,

101
00:05:15,674 --> 00:05:18,585
but it's not ideal either.

102
00:05:18,585 --> 00:05:22,860
There's one last really critical plot you

103
00:05:22,860 --> 00:05:26,820
need to make and that is a residual plot,

104
00:05:26,820 --> 00:05:28,065
it's called a residual plot.

105
00:05:28,065 --> 00:05:29,530
These others, of course, we show

106
00:05:29,530 --> 00:05:32,265
residuals but this is actually called the residual plot.

107
00:05:32,265 --> 00:05:40,655
So, you plot the residuals versus the predicted value.

108
00:05:40,655 --> 00:05:42,705
So, it's the residuals versus the predicted value.

109
00:05:42,705 --> 00:05:45,370
Predictive value on the x-axis,

110
00:05:45,370 --> 00:05:47,245
residuals on the y axis,

111
00:05:47,245 --> 00:05:50,675
and it's just a scatter plot.

112
00:05:50,675 --> 00:05:52,690
So, you use a geom_point and let

113
00:05:52,690 --> 00:05:55,975
me run this and I'll show you what happens.

114
00:05:55,975 --> 00:05:59,520
So, ideally there should be

115
00:05:59,520 --> 00:06:04,110
no structure in this dispersion.

116
00:06:04,110 --> 00:06:06,105
The dispersion of the residuals

117
00:06:06,105 --> 00:06:08,790
as you look at them with respect to predictive value.

118
00:06:08,790 --> 00:06:11,580
So, if you have a large number

119
00:06:11,580 --> 00:06:15,300
of samples that you're evaluating on,

120
00:06:15,300 --> 00:06:18,030
you'll just have something people have described it as

121
00:06:18,030 --> 00:06:20,810
being looking like a big fuzzy caterpillar, that is,

122
00:06:20,810 --> 00:06:23,340
there's no noticeable change in

123
00:06:23,340 --> 00:06:25,200
the variance or the structure going

124
00:06:25,200 --> 00:06:27,480
from one side of the plot to the other.

125
00:06:27,480 --> 00:06:29,050
I'm not sure that's the case here,

126
00:06:29,050 --> 00:06:32,040
we have a fairly tight set of residuals at

127
00:06:32,040 --> 00:06:33,810
the low end and you can

128
00:06:33,810 --> 00:06:35,580
see a bit more dispersion here and then.

129
00:06:35,580 --> 00:06:38,230
Here's those outliers we noticed.

130
00:06:38,230 --> 00:06:39,760
Maybe these guys too,

131
00:06:39,760 --> 00:06:40,830
but certainly this one,

132
00:06:40,830 --> 00:06:42,855
that's that low end outlier.

133
00:06:42,855 --> 00:06:47,530
So, there's not a perfect fit here by any means.

134
00:06:47,530 --> 00:06:51,530
Finally, remember we have

135
00:06:51,530 --> 00:06:54,840
been working with log of price so we can

136
00:06:54,840 --> 00:06:58,430
exponentiate the score and we

137
00:06:58,430 --> 00:07:04,180
can look at the residuals versus actual price.

138
00:07:05,080 --> 00:07:09,750
In this case, you see,

139
00:07:09,750 --> 00:07:11,680
there's really a lot of structure,

140
00:07:11,680 --> 00:07:13,120
so it really looks like a cone.

141
00:07:13,120 --> 00:07:15,390
So, it looks like low cost cars

142
00:07:15,390 --> 00:07:17,530
were doing a pretty good job of predicting their price,

143
00:07:17,530 --> 00:07:19,110
we have pretty small residuals,

144
00:07:19,110 --> 00:07:20,885
but once we get to

145
00:07:20,885 --> 00:07:24,695
higher price cars the residuals grow a lot.

146
00:07:24,695 --> 00:07:28,320
So, clearly R simple in your model is not

147
00:07:28,320 --> 00:07:33,705
ideal for predicting the price of low cost automobiles.

148
00:07:33,705 --> 00:07:35,945
So, in summary, those are

149
00:07:35,945 --> 00:07:39,370
the tools you have to evaluate a regression model.

150
00:07:39,370 --> 00:07:41,345
You have the summary statistics

151
00:07:41,345 --> 00:07:43,510
but you also have the histogram,

152
00:07:43,510 --> 00:07:44,720
the qq normal plot,

153
00:07:44,720 --> 00:07:47,000
and the residual plot which are very

154
00:07:47,000 --> 00:07:49,700
important to understand whether

155
00:07:49,700 --> 00:07:51,200
the summary statistics are

156
00:07:51,200 --> 00:07:52,880
really telling you a true story

157
00:07:52,880 --> 00:07:57,560
or hiding some problem where you actually,

158
00:07:57,560 --> 00:07:59,780
like in this case, where we seem to not be doing

159
00:07:59,780 --> 00:08:02,520
well modeling the expensive cars.

160
00:08:02,520 --> 00:08:04,150
You can only tell that through the plots,

161
00:08:04,150 --> 00:08:07,240
you can't tell that through the summary statistics.

