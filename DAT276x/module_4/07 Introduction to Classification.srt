0
00:00:05,069 --> 00:00:10,129
Classification is a core problem of machine learning. Now machine learning is a field

1
00:00:10,129 --> 00:00:14,049
that grew out of artificial intelligence within computer science, and the goal is to teach

2
00:00:14,049 --> 00:00:19,599
computers by example. Now if you want to teach the computer to recognize images of chairs,

3
00:00:19,599 --> 00:00:23,429
then we give the computer a whole bunch of chairs and tell it which ones are chairs and

4
00:00:23,429 --> 00:00:27,699
which ones are not, and then it’s supposed to learn to recognize chairs, even ones it

5
00:00:27,699 --> 00:00:32,930
hasn’t seen before. It’s not like we tell the computer how to recognize a chair. We

6
00:00:32,930 --> 00:00:36,510
don’t tell it a chair has four legs and a back and a flat surface to sit on and so

7
00:00:36,510 --> 00:00:42,300
on, we just give it a lot of examples. Now machine learning has close ties to statistics;

8
00:00:42,300 --> 00:00:46,719
in fact it’s hard to say what’s different about predictive statistics than machine learning.

9
00:00:46,719 --> 00:00:51,350
These fields are very closely linked right now. Now the problem I just told you about

10
00:00:51,350 --> 00:00:56,309
is a classification problem where we’re trying to identify chairs, and the way we

11
00:00:56,309 --> 00:01:02,699
set the problem up is that we have a training set of observations – in other words, like

12
00:01:02,699 --> 00:01:09,610
labeled images here – and we have a test set that we use only for evaluation. We use

13
00:01:09,610 --> 00:01:14,520
the training set to learn a model of what a chair is, and the test set are images that

14
00:01:14,520 --> 00:01:19,240
are not in the training set and we want to be able to make predictions on those as to

15
00:01:19,240 --> 00:01:25,140
whether or not each image is a chair. And it could be that some of the labels on the

16
00:01:25,140 --> 00:01:29,320
training set are noisy. In fact, that – you know, that could happen in fact one of these

17
00:01:29,320 --> 00:01:34,170
labels is noisy, right here. And that’s okay because as long as there isn’t too

18
00:01:34,170 --> 00:01:37,700
much noise, we should still be able to learn a model for a chair; it just won’t be able

19
00:01:37,700 --> 00:01:42,140
to classify perfectly and that happens. Some prediction problems are just much harder than

20
00:01:42,140 --> 00:01:46,119
others, but that’s okay because we just do the best we can from the training data,

21
00:01:46,119 --> 00:01:50,979
and in terms of the size of the training data: the more the merrier. We want as much data

22
00:01:50,979 --> 00:01:55,590
as we can to train these models. Now how do we represent an image of a chair or a flower

23
00:01:55,590 --> 00:02:04,649
or whatever in the training set? Now I just zoomed in on a little piece of a flower here,

24
00:02:04,649 --> 00:02:10,119
and you can see the pixels in the image, and we can represent each pixel in the image according

25
00:02:10,119 --> 00:02:17,410
to its RGB values, its red-green-blue values. So we get three numbers representing each

26
00:02:17,410 --> 00:02:23,980
pixel in the image. So you can represent the whole image as a collection of RGB values.

27
00:02:23,980 --> 00:02:29,959
So the image becomes this very large vector of numbers, and in general when doing machine

28
00:02:29,959 --> 00:02:34,519
learning, we need to represent each observation in the training and test sets as a vector

29
00:02:34,519 --> 00:02:40,500
of numbers, and the label is also represented by a number. Here are the labels minus 1,

30
00:02:40,500 --> 00:02:46,840
because the image is not a chair and the images of chairs would all get labels plus 1.

31
00:02:46,840 --> 00:02:51,820
Here’s another example: this is a problem that comes from New York City’s power company,

32
00:02:51,820 --> 00:02:56,440
where they want to predict which manholes are going to have a fire. So we would represent

33
00:02:56,440 --> 00:03:01,199
each manhole as a vector, and here are the components of the vector. Right the first

34
00:03:01,199 --> 00:03:06,130
component might be the number of serious events that the manhole had last year, like a fire

35
00:03:06,130 --> 00:03:12,139
or smoking manhole that’s very serious, or an explosion or something like that. And

36
00:03:12,139 --> 00:03:18,410
then maybe we would actually have a category for the number of serious events last year,

37
00:03:18,410 --> 00:03:23,590
so only three of these five events were very serious. The number of electrical cables in

38
00:03:23,590 --> 00:03:28,729
the manhole, the number of electrical cables that were installed before 1930, and so on

39
00:03:28,729 --> 00:03:34,250
and so forth. And you can make – you know in general, the first step is to figure out

40
00:03:34,250 --> 00:03:37,690
how to represent your data like this as a vector, and you can make this vector very

41
00:03:37,690 --> 00:03:42,919
large. You could include lots of factors if you like, that’s totally fine. Computationally,

42
00:03:42,919 --> 00:03:48,049
things are easier if you use fewer features, but then you risk leaving out information.

43
00:03:48,049 --> 00:03:51,579
So there’s a trade-off right there that you’re going to have to worry about, and

44
00:03:51,579 --> 00:03:55,510
we’ll talk more about that later. But in any case, you can’t do machine learning

45
00:03:55,510 --> 00:03:59,139
if you don’ have your data represented in the right way so that’s the first step.

46
00:03:59,139 --> 00:04:03,479
Now you think that manholes with more cables more recent serious events and so on would

47
00:04:03,479 --> 00:04:08,560
be more prone to explosions and fires in the near future, but what combination of them

48
00:04:08,560 --> 00:04:13,109
would give you the best predictor? How do you combine them together? You could add them

49
00:04:13,109 --> 00:04:17,370
all up, but that might not be the best thing. You could give them all weights and add them

50
00:04:17,370 --> 00:04:22,160
all up, but how do you know the weights? And that’s what machine learning does for you.

51
00:04:22,160 --> 00:04:28,510
It tells you what combinations to use to get the best predictors. But for the manhole problem,

52
00:04:28,510 --> 00:04:34,030
we want to use the data from the past to predict the future. So for instance, the future data

53
00:04:34,030 --> 00:04:41,560
might be from 2014 and before, and the label would be 1 if the manhole had an event in

54
00:04:41,560 --> 00:04:49,230
2015. So that’s our training set, and then for the test set, the feature data would be

55
00:04:49,230 --> 00:04:56,070
from 2015 and before and then we would try to predict what would happen in 2016. So just

56
00:04:56,070 --> 00:05:05,760
to be formal about it, we have each observation being represented by our set of features,

57
00:05:05,760 --> 00:05:09,690
and the features are also called predictors or covariant or explanatory variables or independent

58
00:05:09,690 --> 00:05:15,190
variables, whatever they are – you can choose whatever terminology you like. And then we

59
00:05:15,190 --> 00:05:22,220
have the labels, which are called y. Even more formally, we’re given a training set

60
00:05:22,220 --> 00:05:27,920
of feature label pairs xi yi, and there are n of them, and we want to create a classification

61
00:05:27,920 --> 00:05:36,450
model f that can predict a label y or a new x. Let’s take a simple example of – simple

62
00:05:36,450 --> 00:05:41,600
version of the manhole example, where we have only two features: the year the oldest cable

63
00:05:41,600 --> 00:05:47,190
was installed and the number of events that happened last year. So each observation can

64
00:05:47,190 --> 00:05:53,480
be represented as a point on a two-dimensional graph, which means I can plot the whole dataset.

65
00:05:53,480 --> 00:06:01,830
So something like this, where each point here is a manhole and I’ve labelled it with whether

66
00:06:01,830 --> 00:06:07,030
or not it had a serious event in the training set. So these are the manholes that didn’t

67
00:06:07,030 --> 00:06:11,730
have events, and these are the ones that did. And then I’m going to try to create a function

68
00:06:11,730 --> 00:06:20,430
here that’s going to divide the space into two pieces, where on one side of this – on

69
00:06:20,430 --> 00:06:26,240
one side over here of the decision boundary, I’m going to predict that there’s going

70
00:06:26,240 --> 00:06:29,840
to be an event, and on the other side of the decision boundary, I predict there will be

71
00:06:29,840 --> 00:06:36,430
no event. So this decision boundary is actually just the equation where the function is 0,

72
00:06:36,430 --> 00:06:39,990
and then where the function is positive we’ll predict positive and where the function is

73
00:06:39,990 --> 00:06:45,370
negative we’ll predict negative. And so this is going to be a function of these two

74
00:06:45,370 --> 00:06:51,650
variables, the oldest cable and then the number of events last year. And the same idea holds

75
00:06:51,650 --> 00:06:56,770
for the commuter vision problem that we discussed earlier. We’re trying to create this decision

76
00:06:56,770 --> 00:07:01,080
boundary that’s going to chop the space into two pieces, where on one side of the

77
00:07:01,080 --> 00:07:04,960
decision boundary we would predict positive, and then on the other side we’d predict

78
00:07:04,960 --> 00:07:10,580
negative. And the trick is, how do we create this decision boundary? How do we create this

79
00:07:10,580 --> 00:07:16,110
function f? Okay, so given our training data, we want to create our classification model

80
00:07:16,110 --> 00:07:20,840
f that can make predictions. The machine learning algorithm is going to create the function

81
00:07:20,840 --> 00:07:26,260
f for you, and no matter how complicated that function f is, the way to use it is not very

82
00:07:26,260 --> 00:07:32,020
complicated at all. The way to use it is just this: the predicted value of y for a new x

83
00:07:32,020 --> 00:07:36,450
that you haven’t seen before is just the sign of that function f.

84
00:07:36,450 --> 00:07:42,840
Classification is for yes or no questions. You can do a lot if you can answer yes or

85
00:07:42,840 --> 00:07:47,300
no questions. So for instance, think about like handwriting recognition. For each letter

86
00:07:47,300 --> 00:07:53,400
on a page, we’re going to evaluate whether it’s a letter A, a yes or no. And if you’re

87
00:07:53,400 --> 00:07:58,580
doing like spam detection, right, the spam detector on your computer has a machine learning

88
00:07:58,580 --> 00:08:04,050
algorithm in it. Each email that comes in has to be evaluated as to whether or not it’s

89
00:08:04,050 --> 00:08:11,800
spam. Credit defaults: right, whether or not you get a loan depends on whether the bank

90
00:08:11,800 --> 00:08:17,199
predicts that you’re going to default on your loan, yes or no. and in my lab, we do

91
00:08:17,199 --> 00:08:22,070
a lot of work on predicting medical outcomes. We want to know whether something will happen

92
00:08:22,070 --> 00:08:29,550
to a patient within a particular period of time. Here’s a list of common classification

93
00:08:29,550 --> 00:08:35,959
algorithms. Most likely, unless you’re interested in developing your own algorithms, you never

94
00:08:35,958 --> 00:08:39,950
need to program these yourself; they’re already programmed in by someone else. If

95
00:08:39,950 --> 00:08:44,870
you’re just going to be a consumer of these, you can use the code that’s already written.

96
00:08:44,870 --> 00:08:49,950
And all these are, you know – we’re going to cover a good chunk of these methods, and

97
00:08:49,950 --> 00:08:53,920
in order to use them effectively you’ve really got to know what you’re doing, otherwise

98
00:08:53,920 --> 00:08:59,260
you could really run into some issues. But if you can figure out how to use these, you’ve

99
00:08:59,260 --> 00:09:01,260
got a really powerful tool on your hands.

