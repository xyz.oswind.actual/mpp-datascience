0
00:00:05,420 --> 00:00:09,260
Well let’s give you a quick introduction to multiple linear regression. This is our

1
00:00:09,260 --> 00:00:15,650
usual regression setup with the features, the label, and the predicted label, f(x).

2
00:00:15,650 --> 00:00:21,050
Now I’m going to propose a very simple function that takes into account all the various factors

3
00:00:21,050 --> 00:00:26,430
and weights each of them by some amount, okay? Now this model is linear, meaning it’s just

4
00:00:26,430 --> 00:00:31,539
a weighted combination of the factors. But in reality, the model could be very complex.

5
00:00:31,539 --> 00:00:35,930
For instance, you could multiply some of the features together and use those as a new feature

6
00:00:35,930 --> 00:00:43,489
if you wanted to. In any case, where am I going to get these weights in reality, right?

7
00:00:43,489 --> 00:00:47,870
I could make them up, but that’s not really a good idea because it ignores all the data.

8
00:00:47,870 --> 00:00:54,110
So let’s learn them instead. So I’m going to stick to linear models like

9
00:00:54,110 --> 00:01:00,229
this for now. So the b’s are just these coefficients, which are 0 you know, 3, 10,

10
00:01:00,229 --> 00:01:05,650
100, whatever in the example. So, we’ll take our linear model and then the method

11
00:01:05,650 --> 00:01:11,539
of least squares is simply: choose the coefficients, the b’s, to minimize the sum of squares

12
00:01:11,539 --> 00:01:18,409
error. Okay, so the weights are learned from data? Right, how do we do this? Right, this

13
00:01:18,409 --> 00:01:23,049
is – this is an optimization problem that’s exactly the same as the one for simple linear

14
00:01:23,049 --> 00:01:28,929
regression, the only difference is that the function can take – has multiple terms in

15
00:01:28,929 --> 00:01:34,429
it. And this is called the method of least squares; once again, you don’t need to program

16
00:01:34,429 --> 00:01:39,060
this yourself. In fact, actually solving this problem is particularly easy for a computer

17
00:01:39,060 --> 00:01:43,710
since there’s an analytical solution to the minimization problem for this case. So

18
00:01:43,710 --> 00:01:46,740
all you need to do is give it the data and tell it to do least squares regression and

19
00:01:46,740 --> 00:01:51,399
boom, you’re done. You have the b’s, and you can use them in your predictive model

20
00:01:51,399 --> 00:01:55,749
f. You can be creative in the choice of features you’re going to use, you know, like I said,

21
00:01:55,749 --> 00:01:59,859
you can use polynomials, you can square the variables, cube the variables, you can multiply

22
00:01:59,859 --> 00:02:05,039
them together, you could create indicator variables – like this variable is 1 if the

23
00:02:05,039 --> 00:02:09,899
age of the person is above 60 or 0 otherwise. If you put too many variables in there the

24
00:02:09,899 --> 00:02:14,720
optimization problem gets harder, and you could run into recursive dimensionality issues,

25
00:02:14,720 --> 00:02:20,860
but you really should put what you think are all the potentially important factors in there.

26
00:02:20,860 --> 00:02:26,370
If you did put polynomials in there, it would allow the function to be kind of curvy and

27
00:02:26,370 --> 00:02:30,900
interesting, sort of like that with all the curves; although you can see already that

28
00:02:30,900 --> 00:02:34,549
if you put too many interesting features in there, you do run the risk of over-fitting.

29
00:02:34,549 --> 00:02:39,450
So we’re going to have to handle that a few lectures from now, but you really should

30
00:02:39,450 --> 00:02:43,290
put all the main features that you think are likely to be predictive.

