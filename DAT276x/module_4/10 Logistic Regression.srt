0
00:00:05,310 --> 00:00:09,410
I want to talk about a very basic method, in particular, it’s a very old algorithm.

1
00:00:09,410 --> 00:00:13,580
It dates back at least 50 years, but it really works. You won’t be disappointed in this

2
00:00:13,580 --> 00:00:18,070
algorithm. It’s simple, it’s fast and it often competes with the best machine learning

3
00:00:18,070 --> 00:00:24,949
methods, and it’s called logistic regression. Now, logistic regression uses this loss function

4
00:00:24,949 --> 00:00:31,560
over here that we talked about. You saw this before. It’s the dark blue curve over here.

5
00:00:31,560 --> 00:00:36,350
Now I want you to remember this function as best you can for a little while and so I’m

6
00:00:36,350 --> 00:00:42,930
going to put it in the corner over there. So logistic regression minimizes this; and

7
00:00:42,930 --> 00:00:47,900
there’s no regularization for vanilla logistic regression. It just minimizes this function,

8
00:00:47,900 --> 00:00:53,320
which is just the average loss on the training points. Now, we have to choose what kind of

9
00:00:53,320 --> 00:00:59,800
model f is going to be, and we’ll choose a linear model. So it’s a weighted sum of

10
00:00:59,800 --> 00:01:04,650
the features. For instance, if we’re trying to predict income, our model might look like

11
00:01:04,650 --> 00:01:10,539
three times the hours a person works, plus four times the years of experience and so

12
00:01:10,539 --> 00:01:20,829
on. So here, the first feature x1 is hours and beta one is 3 and so on. I can write it

13
00:01:20,829 --> 00:01:29,329
here in summation notation. So f is the sum of the weighted features, where the weights

14
00:01:29,329 --> 00:01:35,780
are called beta. And I also call the beta “coefficients”. So here, all I did was

15
00:01:35,780 --> 00:01:43,119
plug this form of f into that minimization problem for logistic regression. So now, it’s

16
00:01:43,119 --> 00:01:49,740
going to try to find the weights that minimize the sum of the training losses. And this is

17
00:01:49,740 --> 00:01:55,969
what logistic regression does; no more, no less. It just chooses the coefficients (those

18
00:01:55,969 --> 00:01:57,890
betas) to minimize this thing.

