0
00:00:01,550 --> 00:00:03,960
>> Hi and welcome.

1
00:00:03,960 --> 00:00:05,210
So in this video,

2
00:00:05,210 --> 00:00:07,055
I'm going to show you

3
00:00:07,055 --> 00:00:09,320
the basic workflow for

4
00:00:09,320 --> 00:00:11,400
creating a machine learning model with R,

5
00:00:11,400 --> 00:00:14,120
and we're going to do a simple linear regression model

6
00:00:14,120 --> 00:00:16,555
on the auto price data set.

7
00:00:16,555 --> 00:00:20,540
And the important thing to note here is that

8
00:00:20,540 --> 00:00:24,080
almost all machine learning models in R use

9
00:00:24,080 --> 00:00:28,365
a common way they specify the model itself.

10
00:00:28,365 --> 00:00:31,850
And so, once you understand that you actually know how to

11
00:00:31,850 --> 00:00:33,800
use quite a few models not

12
00:00:33,800 --> 00:00:37,180
just the linear regression I'm going to show you here.

13
00:00:37,960 --> 00:00:41,420
So we need to partition the data,

14
00:00:41,420 --> 00:00:42,915
we need to have

15
00:00:42,915 --> 00:00:46,755
training and test data

16
00:00:46,755 --> 00:00:49,270
that are statistically independent,

17
00:00:49,270 --> 00:00:51,240
and I need to do that so we don't

18
00:00:51,240 --> 00:00:55,380
have a biased evaluation of the model.

19
00:00:55,380 --> 00:00:58,470
If we evaluated just with training data of course we'd

20
00:00:58,470 --> 00:01:02,490
get a very optimistic view of how well our model works.

21
00:01:02,490 --> 00:01:05,820
So, to fix that problem we

22
00:01:05,820 --> 00:01:10,560
always sub sample the data into at least two if not

23
00:01:10,560 --> 00:01:14,565
three data sets and you can do that easily with this

24
00:01:14,565 --> 00:01:17,460
"Create Partition" or "Create

25
00:01:17,460 --> 00:01:20,435
Data Partition" function from caret.

26
00:01:20,435 --> 00:01:24,150
And it has just a few arguments.

27
00:01:24,150 --> 00:01:25,860
First off the data frame you want

28
00:01:25,860 --> 00:01:27,450
to partition and you need to give it

29
00:01:27,450 --> 00:01:29,340
a particular column name that it's going

30
00:01:29,340 --> 00:01:31,680
to do its sorting on,

31
00:01:31,680 --> 00:01:34,520
it really doesn't matter which one.

32
00:01:34,520 --> 00:01:39,160
You can create multiple partitions that

33
00:01:39,160 --> 00:01:41,970
is over sample the data

34
00:01:41,970 --> 00:01:43,650
but in this case I don't want to do that.

35
00:01:43,650 --> 00:01:45,990
I am going to partition once.

36
00:01:45,990 --> 00:01:48,910
And then you give it a probability that

37
00:01:48,910 --> 00:01:53,310
a given case or a given row is in the training data,

38
00:01:53,310 --> 00:01:55,390
and in this case I'm going to use three quarters of

39
00:01:55,390 --> 00:02:00,195
my training data for test,

40
00:02:00,195 --> 00:02:02,200
or three quarters of the total data for

41
00:02:02,200 --> 00:02:05,295
training and I don't want to list.

42
00:02:05,295 --> 00:02:06,980
And so, I create

43
00:02:06,980 --> 00:02:11,070
this partition object which is really just a logical,

44
00:02:11,070 --> 00:02:14,425
so I use it to

45
00:02:14,425 --> 00:02:16,150
extract the rows from

46
00:02:16,150 --> 00:02:18,490
my original auto prices data frame to get

47
00:02:18,490 --> 00:02:22,510
a training data and I use the minus which is like a

48
00:02:22,510 --> 00:02:24,310
logical not to get

49
00:02:24,310 --> 00:02:26,985
my test data and well look at the dimensions there.

50
00:02:26,985 --> 00:02:31,060
Let me run this code for you and you see I have out of

51
00:02:31,060 --> 00:02:34,150
my 195 cases for

52
00:02:34,150 --> 00:02:37,835
the Auto Data I have 147 that I'm going to train on,

53
00:02:37,835 --> 00:02:42,120
48 held back for evaluation. T.

54
00:02:42,120 --> 00:02:44,050
>> He next step is to scale,

55
00:02:44,050 --> 00:02:45,640
and in my model I'm only going to

56
00:02:45,640 --> 00:02:47,490
use three numeric features;

57
00:02:47,490 --> 00:02:50,645
curb weight, horsepower, and city miles per gallon.

58
00:02:50,645 --> 00:02:53,260
And again, caret helps us with this because

59
00:02:53,260 --> 00:02:56,725
there's this pre-process function

60
00:02:56,725 --> 00:03:04,555
and I give it the columns that I want to scale.

61
00:03:04,555 --> 00:03:07,810
And I always use the training data to scale and I want to

62
00:03:07,810 --> 00:03:11,720
train the scalar with the training data.

63
00:03:11,720 --> 00:03:15,470
And my method is center and scale so I'm

64
00:03:15,470 --> 00:03:18,520
going to do Z score normalization

65
00:03:18,520 --> 00:03:20,960
here which means it's going to have zero mean

66
00:03:20,960 --> 00:03:25,630
and unit standard deviation or unit variance.

67
00:03:25,630 --> 00:03:30,440
And so, I then once I have my scalar trained I

68
00:03:30,440 --> 00:03:31,880
apply a predict method to that

69
00:03:31,880 --> 00:03:35,170
scalar on both the training data and the test data.

70
00:03:35,170 --> 00:03:37,175
So this way the test data is scaled

71
00:03:37,175 --> 00:03:40,930
using the scalar I trained on the training data.

72
00:03:40,930 --> 00:03:42,590
And we'll just look at the head of

73
00:03:42,590 --> 00:03:45,275
those few columns we did.

74
00:03:45,275 --> 00:03:49,200
And you can see they all do look scaled.

75
00:03:49,240 --> 00:03:54,420
So now, let's build our linear regression model.

76
00:03:54,420 --> 00:04:01,005
And as I was saying at the introduction to this video,

77
00:04:01,005 --> 00:04:03,670
so I'm going to use the LM

78
00:04:03,670 --> 00:04:05,530
which is linear model or linear,

79
00:04:05,530 --> 00:04:09,520
it's a linear regression function in R. And I'm going

80
00:04:09,520 --> 00:04:13,730
to model log price on curb weight,

81
00:04:13,730 --> 00:04:17,330
horsepower, city miles per gallon, fuel type, aspiration,

82
00:04:17,330 --> 00:04:19,605
body style and drive wheels,

83
00:04:19,605 --> 00:04:24,185
and number of cylinders and you see my data.

84
00:04:24,185 --> 00:04:27,940
So I specify the data is the training data,

85
00:04:27,940 --> 00:04:30,870
my model type is LM,

86
00:04:30,870 --> 00:04:34,045
and this is the model formula,

87
00:04:34,045 --> 00:04:35,360
all of this, okay?

88
00:04:35,360 --> 00:04:40,070
So, most of machine learning models

89
00:04:40,070 --> 00:04:42,895
use this model formula language.

90
00:04:42,895 --> 00:04:47,140
And you notice the little squiggly line here and

91
00:04:47,140 --> 00:04:49,935
that squiggle or tilda

92
00:04:49,935 --> 00:04:53,630
is in English you would say modeled by.

93
00:04:53,630 --> 00:04:56,510
So I could say, log price is

94
00:04:56,510 --> 00:05:00,625
modeled by and then I have all my features curb way,

95
00:05:00,625 --> 00:05:02,950
horsepower, city miles per gallon, et cetera.

96
00:05:02,950 --> 00:05:05,955
>> So by specifying the model this way,

97
00:05:05,955 --> 00:05:10,770
I say exactly what my label column is,

98
00:05:10,770 --> 00:05:11,940
what I'm trying to predict,

99
00:05:11,940 --> 00:05:14,270
and what features I'm going to use to

100
00:05:14,270 --> 00:05:18,330
model that and predict that so let me run this.

101
00:05:20,200 --> 00:05:24,090
And then, you can create

102
00:05:24,090 --> 00:05:30,260
a summary for a linear model or any model object in R,

103
00:05:30,260 --> 00:05:31,790
there's a summary method

104
00:05:31,790 --> 00:05:33,960
usually and often a print method as well.

105
00:05:33,960 --> 00:05:37,845
In this case, I'm just going to extract the coefficients.

106
00:05:37,845 --> 00:05:42,855
And you see we have an intercept term,

107
00:05:42,855 --> 00:05:46,480
not too surprisingly, and then a model coefficient

108
00:05:46,480 --> 00:05:49,645
for each of those features I specified.

109
00:05:49,645 --> 00:05:54,565
So that's you see there's each feature

110
00:05:54,565 --> 00:06:01,070
that I specified here has a model coefficient here.

111
00:06:01,450 --> 00:06:04,095
So, that's really all there is to it.

112
00:06:04,095 --> 00:06:05,760
It's very easy to

113
00:06:05,760 --> 00:06:08,880
specify and I can change the specification very

114
00:06:08,880 --> 00:06:14,620
quickly for machine learning models with R.

