0
00:00:05,170 --> 00:00:10,210
So let’s start our unit on regression. First I’ll just do a recap and talk about simple

1
00:00:10,210 --> 00:00:15,260
linear regression, which is just one feature, multiple linear regression (which is many

2
00:00:15,260 --> 00:00:21,350
features), how to evaluate regression models, and we’ll do a case study. And then we’ll

3
00:00:21,350 --> 00:00:29,500
talk about outliers and the concept of leverage. Let’s start with a recap. Regression is

4
00:00:29,500 --> 00:00:33,550
for predicting real-valued outcome – things like “how many customers will arrive at

5
00:00:33,550 --> 00:00:37,070
our website next week?”, “how many tv’s will we sell next year?”, and “can we

6
00:00:37,070 --> 00:00:43,890
predict someone’s income from their click through information?”. So here’s our training

7
00:00:43,890 --> 00:00:51,260
data, the features are here and the labels, which are real-valued (in this case, it’s

8
00:00:51,260 --> 00:00:58,799
income), and then we also have the predictions, f(x), over here on the right. Now just looking

9
00:00:58,799 --> 00:01:05,070
at these two columns, we have to figure out how to evaluate the closeness of the truth,

10
00:01:05,069 --> 00:01:15,509
y, to what we predicted, which is f(x). Now here’s what I propose, which is y-f(x),

11
00:01:15,509 --> 00:01:23,509
and as we know, that is not a great idea. Because why didn’t I choose that one? If

12
00:01:23,509 --> 00:01:27,310
I choose either this one or that one it would be bad, because I’m looking at errors in

13
00:01:27,310 --> 00:01:32,939
only one direction and I want to penalize errors in either direction. So I could try

14
00:01:32,939 --> 00:01:41,130
to use this penalty here, which makes sure we count deviations in either direction. But

15
00:01:41,130 --> 00:01:48,259
that’s not actually what we’re going to use. We’re going to use the squared error.

16
00:01:48,259 --> 00:01:52,729
It’s just easier computationally and analytically, because, you know, it’s differentiable.

17
00:01:52,729 --> 00:01:57,270
But you should think of this as capturing errors in both directions, just like the absolute

18
00:01:57,270 --> 00:02:03,060
value I had on the previous slide. So same deal; penalize how far y is from f in either

19
00:02:03,060 --> 00:02:13,510
direction. So here’s a picture of it. So here we’d want to use f(x)-y and then here

20
00:02:13,510 --> 00:02:19,840
is a case where we’d want to use y-f(x), and so we could just use the absolute value

21
00:02:19,840 --> 00:02:23,239
to capture both of those things, but we’re not going to, we’re actually going to square

22
00:02:23,239 --> 00:02:29,900
it – get the squared error. Now the sum of squares error, this is, you know, if we

23
00:02:29,900 --> 00:02:34,310
add all these errors up, this is called the sum of squares error. And we’ll get back

24
00:02:34,310 --> 00:02:39,530
to that in a minute, and that is right here. But I want you to remember that this is a

25
00:02:39,530 --> 00:02:45,140
fundamental quality – quantity – in regression. And what I want to do now is talk about what

26
00:02:45,140 --> 00:02:50,129
the model, f, might look like. We’re going to choose f so that it’s a good model, meaning

27
00:02:50,129 --> 00:02:56,810
that it minimizes this sum of squares error. So let’s talk about simple linear regression.

28
00:02:56,810 --> 00:03:04,189
In simple linear regression, we have only one feature. So maybe we’re predicting the

29
00:03:04,189 --> 00:03:11,659
income based on a single feature, which is maybe the number of business week clicks the

30
00:03:11,659 --> 00:03:17,879
person makes. So now we have to figure out what our function f is going to look like.

31
00:03:17,879 --> 00:03:25,439
So here I’ve put on a very simple function f. It just gives everyone a baseline of $100000

32
00:03:25,439 --> 00:03:31,239
just for existing. And it estimates that for each click they make on the business week

33
00:03:31,239 --> 00:03:38,209
website, they’re $5000 richer. So this is kind of a silly model, since it predicts that

34
00:03:38,209 --> 00:03:41,920
anyone who spends all of their time on business week is a gazillionaire but hey, it’s just

35
00:03:41,920 --> 00:03:50,239
an example. But for our function that estimates y from x, we’re going to choose a model

36
00:03:50,239 --> 00:03:57,250
of this form. A baseline plus the multiplier for however many business week clicks we have

37
00:03:57,250 --> 00:04:07,180
(called b1) times the number of clicks, which is x1. So there’s the formula again. Before

38
00:04:07,180 --> 00:04:11,959
we start doing this, all we have is data. We don’t know this $100000 and we don’t

39
00:04:11,959 --> 00:04:18,620
know the $5000. I’ve just made those up. We have to estimate them by using data. Now

40
00:04:18,620 --> 00:04:27,920
remember, we want the sum of squares error to be small. So what we’re going to do is

41
00:04:27,920 --> 00:04:33,440
choose the b0 and the b1 to minimize the total error on the training set, and that is the

42
00:04:33,440 --> 00:04:41,700
procedure of simple linear regression – least squares regression. So let’s pretend I did

43
00:04:41,700 --> 00:04:47,980
this, and as it turned out, actually the model I had before wasn’t so good. When I fit

44
00:04:47,980 --> 00:04:55,670
it to the data, I got this model instead. And this new model fits pretty well on the

45
00:04:55,670 --> 00:05:01,420
data that I have in my training set, but how well does it perform out of sample? I didn’t

46
00:05:01,420 --> 00:05:07,240
tell you, but I actually left part of the data out for evaluation, and there it is.

47
00:05:07,240 --> 00:05:14,050
So let’s take a look at the errors, and they’re here – not too bad, looks like

48
00:05:14,050 --> 00:05:19,810
we did a pretty good job. So that is the procedure of simple linear regression – least squares

49
00:05:19,810 --> 00:05:24,960
regression for a single feature. You don’t need to solve the minimization problem yourself

50
00:05:24,960 --> 00:05:30,280
to find b0 and b1; the machine learning algorithm will do it for you, it’s all under the hood.

