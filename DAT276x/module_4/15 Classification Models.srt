0
00:00:01,730 --> 00:00:04,540
>> Hello. In this video,

1
00:00:04,540 --> 00:00:06,880
I'm going to show you a demo

2
00:00:06,880 --> 00:00:10,320
of how to build a classification model with R,

3
00:00:10,320 --> 00:00:13,680
and specific we're going to use logistic regression to

4
00:00:13,680 --> 00:00:15,010
classify good credit and

5
00:00:15,010 --> 00:00:18,450
bad credit customers from the German credit dataset.

6
00:00:18,450 --> 00:00:21,440
So the first thing you need to do when

7
00:00:21,440 --> 00:00:25,010
you're building a classification model

8
00:00:25,010 --> 00:00:27,830
with any package is to look at

9
00:00:27,830 --> 00:00:31,600
the label values and look at their frequencies.

10
00:00:31,600 --> 00:00:35,195
So you know what you're classifying and also you can see

11
00:00:35,195 --> 00:00:37,430
how much imbalance there is or there

12
00:00:37,430 --> 00:00:41,170
isn't in the values of the label,

13
00:00:41,170 --> 00:00:43,775
the classes that you're trying to classify.

14
00:00:43,775 --> 00:00:48,665
In this case, you can see there's significant imbalance.

15
00:00:48,665 --> 00:00:52,080
There's about 70 percent good customers,

16
00:00:52,080 --> 00:00:53,560
30 percent bad customers.

17
00:00:53,560 --> 00:00:55,595
So that's going to definitely affect

18
00:00:55,595 --> 00:00:58,160
anything we do with training this model.

19
00:00:58,160 --> 00:01:00,260
So we need to always keep that in mind.

20
00:01:00,260 --> 00:01:06,555
So the first thing we need to do again after that is,

21
00:01:06,555 --> 00:01:09,559
and this is true for any machine learning model,

22
00:01:09,559 --> 00:01:12,110
is we need to create our data partitions

23
00:01:12,110 --> 00:01:15,050
and I'll use the create data partition function

24
00:01:15,050 --> 00:01:18,980
from caret and I'm

25
00:01:18,980 --> 00:01:20,955
going to start with the credit data frame.

26
00:01:20,955 --> 00:01:23,535
I'm not going to over under sample.

27
00:01:23,535 --> 00:01:27,955
I'm going to use 70 percent of my data for

28
00:01:27,955 --> 00:01:30,250
training and the other 30 percent for

29
00:01:30,250 --> 00:01:32,930
testing so will be about 700 training cases,

30
00:01:32,930 --> 00:01:37,610
300 test cases and then I can use the partition object

31
00:01:37,610 --> 00:01:42,880
here to split into training and test datasets.

32
00:01:42,880 --> 00:01:46,070
As I advertised so about 700 here and

33
00:01:46,070 --> 00:01:49,835
about 300 cases for test.

34
00:01:49,835 --> 00:01:52,250
So next thing I have to do is

35
00:01:52,250 --> 00:01:56,325
scale and as I've mentioned in previous demos,

36
00:01:56,325 --> 00:02:01,885
we'll use this preprocess function from caret.

37
00:02:01,885 --> 00:02:06,560
We're operating on these four numeric columns

38
00:02:06,560 --> 00:02:08,865
that we're going to use in our model.

39
00:02:08,865 --> 00:02:12,280
So we preprocess training and

40
00:02:12,280 --> 00:02:14,910
again we're going to Z-score normalize this.

41
00:02:14,910 --> 00:02:17,080
So we center them and we scale them.

42
00:02:17,080 --> 00:02:19,750
So we're going to have mean zero,

43
00:02:19,750 --> 00:02:22,955
unit standard deviation or unit variance.

44
00:02:22,955 --> 00:02:25,475
We do this on the training data

45
00:02:25,475 --> 00:02:27,635
and then we do the predictions

46
00:02:27,635 --> 00:02:30,480
from that scaling object

47
00:02:30,480 --> 00:02:32,560
on the training data and the test data.

48
00:02:32,560 --> 00:02:34,760
So, we're using the same scaling

49
00:02:34,760 --> 00:02:36,380
for both and we'll just have

50
00:02:36,380 --> 00:02:39,020
a quick look and you see it does look

51
00:02:39,020 --> 00:02:42,770
like our scalar did the job we're hoping for.

52
00:02:43,280 --> 00:02:47,350
So, a basic logistic regression model

53
00:02:47,350 --> 00:02:49,890
in R can be created with

54
00:02:49,890 --> 00:02:52,990
the GLM package and I can make it a

55
00:02:52,990 --> 00:02:56,995
logistic regression by using the family equals binomial.

56
00:02:56,995 --> 00:02:59,890
So the response I'm expecting here is binomial

57
00:02:59,890 --> 00:03:03,165
because it's a two-class classification problem.

58
00:03:03,165 --> 00:03:05,890
Our customer either has good credit

59
00:03:05,890 --> 00:03:08,935
or bad credit and that's what our label is.

60
00:03:08,935 --> 00:03:11,770
Our data is the training data that we just

61
00:03:11,770 --> 00:03:14,950
split and here's our model formula.

62
00:03:14,950 --> 00:03:18,810
So I'm going to model bad credit that's

63
00:03:18,810 --> 00:03:22,095
our label column by

64
00:03:22,095 --> 00:03:24,680
and then loan duration months loan amount,

65
00:03:24,680 --> 00:03:26,225
payment percent income,

66
00:03:26,225 --> 00:03:28,650
age and years, checking account status,

67
00:03:28,650 --> 00:03:30,790
credit history purpose, gender status,

68
00:03:30,790 --> 00:03:32,440
time and residence and property.

69
00:03:32,440 --> 00:03:33,700
Well, that's a big mouthful,

70
00:03:33,700 --> 00:03:37,120
but you can see basically it's the label modeled by

71
00:03:37,120 --> 00:03:39,415
whatever features I have

72
00:03:39,415 --> 00:03:43,045
selected to try in building this model.

73
00:03:43,045 --> 00:03:47,645
That's just using the R model formula language.

74
00:03:47,645 --> 00:03:51,060
So it's the same as many other machine learning

75
00:03:51,060 --> 00:03:54,985
models in R. So let me run this it's very quick,

76
00:03:54,985 --> 00:03:57,770
logistic regression is computationally very efficient,

77
00:03:57,770 --> 00:04:00,700
and we'll print a summary.

78
00:04:00,700 --> 00:04:03,500
So I have a summary in a print method I can use.

79
00:04:03,500 --> 00:04:08,010
The summary shows me the model coefficients like

80
00:04:08,010 --> 00:04:13,945
the intercept loan duration in months et cetera.

81
00:04:13,945 --> 00:04:16,860
Notice the standard errors are fairly large,

82
00:04:16,860 --> 00:04:21,380
some of them are almost the same magnitude

83
00:04:21,380 --> 00:04:26,475
as the coefficient itself for say, loan amount and what.

84
00:04:26,475 --> 00:04:29,700
So this model may be significantly over fed.

85
00:04:29,700 --> 00:04:31,370
We're just doing a first model here,

86
00:04:31,370 --> 00:04:34,730
we're just taking a first guess at features that

87
00:04:34,730 --> 00:04:39,290
might be good based on the exploration of the data.

88
00:04:41,130 --> 00:04:45,465
So, we have to do something else now.

89
00:04:45,465 --> 00:04:48,430
So we've got the model object fit,

90
00:04:48,430 --> 00:04:50,770
we've seen the model coefficients and we think

91
00:04:50,770 --> 00:04:53,780
this model might be overfed but let's press on.

92
00:04:53,780 --> 00:04:55,570
So now, I take

93
00:04:55,570 --> 00:05:00,560
my model object and I have some new data equals tests,

94
00:05:00,560 --> 00:05:01,985
that's my test data,

95
00:05:01,985 --> 00:05:04,385
and I want to get the response.

96
00:05:04,385 --> 00:05:06,280
Now for a logistic regression model,

97
00:05:06,280 --> 00:05:08,560
the response is a probability

98
00:05:08,560 --> 00:05:10,620
based on those log likelihoods.

99
00:05:10,620 --> 00:05:15,340
So, I use the predict method on this model object and

100
00:05:15,340 --> 00:05:16,600
I'm just going to pin to

101
00:05:16,600 --> 00:05:20,720
the test data frame those probabilities,

102
00:05:20,720 --> 00:05:23,755
and we'll just print

103
00:05:23,755 --> 00:05:27,130
a few columns of our data frame, our test data frame.

104
00:05:27,130 --> 00:05:32,785
So, you have mostly bad credit

105
00:05:32,785 --> 00:05:34,640
and you see the probability,

106
00:05:34,640 --> 00:05:37,620
this is the probability that it is bad credit.

107
00:05:37,620 --> 00:05:41,905
So here's one where the probability is over a half.

108
00:05:41,905 --> 00:05:44,680
So we're going to use a threshold on that.

109
00:05:44,680 --> 00:05:46,860
So if we used a threshold of 0.5,

110
00:05:46,860 --> 00:05:50,125
this would result in an error in our score.

111
00:05:50,125 --> 00:05:53,900
Here's one where it's just a scooch under a half.

112
00:05:53,900 --> 00:05:57,810
Again, if you used a threshold value of 0.5,

113
00:05:57,810 --> 00:06:00,210
you would incorrectly score this.

114
00:06:00,210 --> 00:06:02,960
Most of these look pretty good though.

115
00:06:02,980 --> 00:06:05,970
So, now we have to score.

116
00:06:05,970 --> 00:06:08,890
So I showed you the probabilities which is what you

117
00:06:08,890 --> 00:06:13,200
get as the response here.

118
00:06:13,200 --> 00:06:15,395
Those are the probabilities.

119
00:06:15,395 --> 00:06:19,110
So what I have to do is apply some threshold to him

120
00:06:19,110 --> 00:06:22,420
because what I want is a binary good bad score.

121
00:06:22,420 --> 00:06:25,830
So, I simply use an if else here on

122
00:06:25,830 --> 00:06:29,320
the column and I create a new score.

123
00:06:29,320 --> 00:06:34,005
So on the probs column that I just created.

124
00:06:34,005 --> 00:06:35,730
If it's greater than the threshold,

125
00:06:35,730 --> 00:06:38,390
I say it's one which is bad credit.

126
00:06:38,390 --> 00:06:40,220
If it's less than the threshold,

127
00:06:40,220 --> 00:06:41,810
I say it's zero which is good credit.

128
00:06:41,810 --> 00:06:43,780
So we'll go ahead and run

129
00:06:43,780 --> 00:06:48,185
that and you can see as I predicted,

130
00:06:48,185 --> 00:06:50,930
we've got an error here, an error there,

131
00:06:50,930 --> 00:06:56,790
error and we got one right and another one right.

132
00:06:57,140 --> 00:07:01,050
So that's the basic workflow for

133
00:07:01,050 --> 00:07:02,640
creating a classifier model

134
00:07:02,640 --> 00:07:04,880
in R. You see it's very simple.

135
00:07:04,880 --> 00:07:06,870
You use the model formula to

136
00:07:06,870 --> 00:07:10,365
specify the model that you're trying to use.

137
00:07:10,365 --> 00:07:12,840
So we are trying our first model here

138
00:07:12,840 --> 00:07:15,270
and we'll expect to make

139
00:07:15,270 --> 00:07:18,650
many more models before we get a really good one.

140
00:07:19,290 --> 00:07:22,595
We split the data,

141
00:07:22,595 --> 00:07:25,155
scaled the data, trained the model,

142
00:07:25,155 --> 00:07:28,330
used the prediction method to complete

143
00:07:28,330 --> 00:07:31,580
probabilities and then created scores,

144
00:07:31,580 --> 00:07:36,030
label scores from those probabilities.

