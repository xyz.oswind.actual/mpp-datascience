0
00:00:05,390 --> 00:00:10,660
Let’s talk about another way to produce ROC curves. So ROC curves can be produced

1
00:00:10,660 --> 00:00:19,119
in two ways: the one that I just showed you, which is for a single, real valued classifier.

2
00:00:19,119 --> 00:00:25,740
In that case, the ROC curve evaluates the classifier. There’s another way to create

3
00:00:25,740 --> 00:00:32,850
an ROC curve, where you can use a single algorithm and sweep the imbalance parameter across the

4
00:00:32,850 --> 00:00:40,219
full range and trace out an ROC curve to evaluate the algorithm. So one is a property of a single

5
00:00:40,219 --> 00:00:44,149
classifier and the other is a property of a whole algorithm. So let’s go over this

6
00:00:44,149 --> 00:00:49,070
way again and I’ll repeat how to do that. So let’s say that we have our classifier

7
00:00:49,070 --> 00:00:55,910
– our function, f, and it’s increasing along this direction. And we could place a

8
00:00:55,910 --> 00:01:01,289
decision boundary anywhere we wanted along here. So let’s start from the top and sweep

9
00:01:01,289 --> 00:01:05,580
this thing down from top to bottom, and every time we move it, you record the true positive

10
00:01:05,580 --> 00:01:09,720
rate and the false positive rate. And then you plot all of those CPRs and FPRs on the

11
00:01:09,720 --> 00:01:15,020
scatter plot and that’s the ROC curve, so let’s do it. So we swing that whole thing

12
00:01:15,020 --> 00:01:19,950
that way and we record the true positive rate and false positive rate as we do it. Okay,

13
00:01:19,950 --> 00:01:25,780
and that traces out this whole curve. So that’s the first way to create ROC curves, and that’s

14
00:01:25,780 --> 00:01:31,479
for a single classification model. And now let’s talk about how to evaluate an algorithm.

15
00:01:31,479 --> 00:01:36,520
Do you remember the c parameter from the imbalanced learning section? That’s the one that allows

16
00:01:36,520 --> 00:01:42,409
you to weight the positives differently than the negatives. So here, I’m going to sweep

17
00:01:42,409 --> 00:01:49,799
through values of the c parameters and fit a machine learning model each time we adjust

18
00:01:49,799 --> 00:01:54,420
the c. And we’re going to start with c being tiny. So the classifier doesn’t care about

19
00:01:54,420 --> 00:01:58,880
the positives at all – it just cares about getting the negatives right. And guess what?

20
00:01:58,880 --> 00:02:04,619
It got all of them right because it just classified everything as negative. Then we adjust c and

21
00:02:04,619 --> 00:02:11,700
we get this one. And then we adjust c again and get that decision boundary and this one,

22
00:02:11,700 --> 00:02:15,950
and that one. And as you do this sweep, you get different models each time, but you also

23
00:02:15,950 --> 00:02:21,030
get a true positive rate and a false positive rate each time. So you could plot those values

24
00:02:21,030 --> 00:02:28,360
on a ROC curve. But this ROC curve evaluates the whole algorithm and not one algorithm.

25
00:02:28,360 --> 00:02:33,060
Now what’s the advantage of using one of these methods over the other? It’s not really

26
00:02:33,060 --> 00:02:37,810
like that – you can’t really say that. One of these two things is an evaluation measure

27
00:02:37,810 --> 00:02:42,849
for a single function and the other is a measure of quality for a whole algorithm. Usually

28
00:02:42,849 --> 00:02:48,879
an algorithm that’s optimized for a specific decision point can actually do better than

29
00:02:48,879 --> 00:02:55,300
an algorithm that is optimized for something else. So usually, you would expect to see

30
00:02:55,300 --> 00:03:01,750
the ROC curve for the whole algorithm, which is optimized at each point on this curve.

31
00:03:01,750 --> 00:03:07,860
You’d expect that to do better than just the single classifier, though every once in

32
00:03:07,860 --> 00:03:11,489
a while you do get a surprise and something weird happens. But in any case, this is the

33
00:03:11,489 --> 00:03:16,980
idea. If you use the algorithm and fiddle with the class weight parameter, you’re

34
00:03:16,980 --> 00:03:25,640
essentially optimizing for each point along that curve. Whereas, if you use a fixed classifier,

35
00:03:25,640 --> 00:03:30,920
you might be optimizing for one point on that curve, or for something else entirely, so

36
00:03:30,920 --> 00:03:35,650
you wouldn’t expect the ROC curve to be as good. Okay, so here ends the discussion

37
00:03:35,650 --> 00:03:37,510
of ways to produce ROC curves.

