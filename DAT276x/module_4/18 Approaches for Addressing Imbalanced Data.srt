0
00:00:00,000 --> 00:00:03,600
>> Hi and welcome.

1
00:00:03,600 --> 00:00:06,600
So in this video I'm going to talk about ways you can

2
00:00:06,600 --> 00:00:10,440
understand and deal with imbalanced data.

3
00:00:10,440 --> 00:00:14,280
So, our goal here is to find approaches to

4
00:00:14,280 --> 00:00:17,565
limit the imbalance bias

5
00:00:17,565 --> 00:00:19,230
in machine learning model training.

6
00:00:19,230 --> 00:00:21,720
So say you have a case where 90 percent of

7
00:00:21,720 --> 00:00:24,525
your cases are negative and 10 percent are positive.

8
00:00:24,525 --> 00:00:28,545
So there's some rare event maybe fraud or

9
00:00:28,545 --> 00:00:30,870
incorrect order numbers or

10
00:00:30,870 --> 00:00:33,860
something like that that you're looking for.

11
00:00:34,150 --> 00:00:37,490
You could build a really dumb classifier

12
00:00:37,490 --> 00:00:38,925
that isn't a classifier at all.

13
00:00:38,925 --> 00:00:40,970
It just says all the cases are

14
00:00:40,970 --> 00:00:43,700
the negative case and it would be 90 percent accurate.

15
00:00:43,700 --> 00:00:45,170
Of course it's also completely

16
00:00:45,170 --> 00:00:47,295
useless, so that's ridiculous.

17
00:00:47,295 --> 00:00:51,020
But if you train most machine learning models on

18
00:00:51,020 --> 00:00:54,785
a situation where you have 90 percent

19
00:00:54,785 --> 00:00:58,340
in one category and 10 percent in another or if

20
00:00:58,340 --> 00:01:00,530
it's multi-class classifier you

21
00:01:00,530 --> 00:01:02,315
could have other types of imbalance.

22
00:01:02,315 --> 00:01:05,270
You're going to have a strong bias toward getting

23
00:01:05,270 --> 00:01:08,810
the majority cases correct at the expense

24
00:01:08,810 --> 00:01:10,400
of the minority cases and it's often

25
00:01:10,400 --> 00:01:12,790
the minority cases you really care about.

26
00:01:12,790 --> 00:01:15,275
That is a strong bias

27
00:01:15,275 --> 00:01:17,675
that you really will probably want to avoid.

28
00:01:17,675 --> 00:01:19,600
So how can you do that?

29
00:01:19,600 --> 00:01:23,060
So our approach generally is

30
00:01:23,060 --> 00:01:26,080
to change the sampling of the dataset or the weighting

31
00:01:26,080 --> 00:01:29,330
of the dataset so that we can create a balance between

32
00:01:29,330 --> 00:01:31,250
the classes and we remove

33
00:01:31,250 --> 00:01:34,620
that bias in our training of machine learning model.

34
00:01:34,620 --> 00:01:36,530
There are a number

35
00:01:36,530 --> 00:01:38,000
of methods I'm going to talk about here.

36
00:01:38,000 --> 00:01:42,350
They all have advantages and disadvantages and you may

37
00:01:42,350 --> 00:01:44,540
just have to test them and see

38
00:01:44,540 --> 00:01:46,745
what works in your particular situation.

39
00:01:46,745 --> 00:01:48,775
There's no guarantees on this.

40
00:01:48,775 --> 00:01:52,550
Different problems have different statistical properties

41
00:01:52,550 --> 00:01:53,720
and it may be hard to know

42
00:01:53,720 --> 00:01:56,345
a priority which is the best approach.

43
00:01:56,345 --> 00:01:58,680
So let's go through some of these.

44
00:01:58,680 --> 00:02:03,730
So the first one is to under-sample the majority case.

45
00:02:03,730 --> 00:02:05,690
So, if I have

46
00:02:05,690 --> 00:02:11,030
60 percent of one category and 40 percent of the other I

47
00:02:11,030 --> 00:02:14,840
could under sample that majority case so that they

48
00:02:14,840 --> 00:02:16,850
have balanced but I've

49
00:02:16,850 --> 00:02:18,965
lost 20 percent of my training data.

50
00:02:18,965 --> 00:02:21,410
But that may be fine. That may actually give me

51
00:02:21,410 --> 00:02:23,510
a better less biased result than

52
00:02:23,510 --> 00:02:26,205
using all my training data. So how does that work?

53
00:02:26,205 --> 00:02:29,530
So you want to make sure your Bernoulli sample,

54
00:02:29,530 --> 00:02:30,950
which is random sample,

55
00:02:30,950 --> 00:02:34,220
the majority case to find that whatever percentage

56
00:02:34,220 --> 00:02:37,630
it is you're going to keep to get the cases imbalance.

57
00:02:37,630 --> 00:02:40,310
It's very important to randomly sample

58
00:02:40,310 --> 00:02:41,870
a Bernoulli sample and

59
00:02:41,870 --> 00:02:44,330
most statistical packages support this.

60
00:02:44,330 --> 00:02:50,470
If you just simply take the first n rows out of a table,

61
00:02:50,470 --> 00:02:51,650
there may be a bias,

62
00:02:51,650 --> 00:02:54,860
maybe those rows are organized

63
00:02:54,860 --> 00:02:59,080
by people's state where they live or something like that.

64
00:02:59,080 --> 00:03:01,010
You don't know. So you really

65
00:03:01,010 --> 00:03:03,140
need to make sure you random sample.

66
00:03:03,140 --> 00:03:04,880
That's true. It's really true of

67
00:03:04,880 --> 00:03:08,280
all sampling and statistical problems.

68
00:03:08,870 --> 00:03:12,170
By Bernoulli sampling or just sampling with

69
00:03:12,170 --> 00:03:15,560
some probability P that you're going to cake a case,

70
00:03:15,560 --> 00:03:18,625
you're minimizing that chance of bias.

71
00:03:18,625 --> 00:03:21,840
And you set the P, that probability,

72
00:03:21,840 --> 00:03:24,905
so that you get the desired number of cases back.

73
00:03:24,905 --> 00:03:27,730
If I had 10,000 of

74
00:03:27,730 --> 00:03:31,300
one category and 5,000 of another I would set P

75
00:03:31,300 --> 00:03:33,675
to 0.5 and under-sample

76
00:03:33,675 --> 00:03:37,570
that majority class so I get 5,000 and 5,000.

77
00:03:37,570 --> 00:03:40,955
Now I have a balanced training set.

78
00:03:40,955 --> 00:03:45,375
So the converse is maybe

79
00:03:45,375 --> 00:03:49,350
I can't afford to throw away that majority data.

80
00:03:49,350 --> 00:03:52,010
Maybe for whatever statistical reason,

81
00:03:52,010 --> 00:03:53,665
maybe there's just not enough of it or maybe

82
00:03:53,665 --> 00:03:55,985
there's too much variation.

83
00:03:55,985 --> 00:03:59,210
I might want to over-sample those minority cases.

84
00:03:59,210 --> 00:04:02,885
So by over-sampling, I'm giving them more weight.

85
00:04:02,885 --> 00:04:05,075
Right. So, say I

86
00:04:05,075 --> 00:04:07,850
over-sample by a factor of four so that now

87
00:04:07,850 --> 00:04:12,980
my minority cases each are represented four times.

88
00:04:12,980 --> 00:04:15,560
So they now have four times the way they

89
00:04:15,560 --> 00:04:19,310
originally did in the overall training data.

90
00:04:19,310 --> 00:04:23,435
Again we Bernoulli sample the minority cases.

91
00:04:23,435 --> 00:04:27,200
As I've said you have

92
00:04:27,200 --> 00:04:29,675
to do anything you

93
00:04:29,675 --> 00:04:31,280
can to limit bias when you're

94
00:04:31,280 --> 00:04:33,325
doing this kind of sampling.

95
00:04:33,325 --> 00:04:36,590
So, it's a little more complicated algorithm

96
00:04:36,590 --> 00:04:39,300
but not mindbogglingly complicated.

97
00:04:39,300 --> 00:04:40,955
So you just create a loop

98
00:04:40,955 --> 00:04:42,800
and you're going to go through that loop however

99
00:04:42,800 --> 00:04:44,875
many times you need and you're

100
00:04:44,875 --> 00:04:47,180
going to Bernoulli sample with

101
00:04:47,180 --> 00:04:49,490
probability P some number of

102
00:04:49,490 --> 00:04:53,625
samples out of those minority cases.

103
00:04:53,625 --> 00:04:55,600
Then you're going to concatenate

104
00:04:55,600 --> 00:04:58,835
that with the original sample.

105
00:04:58,835 --> 00:05:01,870
You're just going to keep going

106
00:05:01,870 --> 00:05:04,270
until you have the number of cases you need.

107
00:05:04,270 --> 00:05:07,780
There are different variations

108
00:05:07,780 --> 00:05:11,580
on that but that's the basic recipe for over-sampling.

109
00:05:11,580 --> 00:05:14,935
So another way to address this problem of

110
00:05:14,935 --> 00:05:17,030
imbalance is case weights

111
00:05:17,030 --> 00:05:19,710
and case weights are really nice.

112
00:05:20,160 --> 00:05:22,695
If your algorithm supports

113
00:05:22,695 --> 00:05:24,830
some things like linear models,

114
00:05:24,830 --> 00:05:31,300
support vector machine models, support case weights.

115
00:05:31,300 --> 00:05:34,950
The idea is really simple that you set

116
00:05:34,950 --> 00:05:37,500
the weight inversely proportional

117
00:05:37,500 --> 00:05:39,775
to the frequency of the classes.

118
00:05:39,775 --> 00:05:43,360
Say I have two-thirds in

119
00:05:43,360 --> 00:05:45,090
a majority class and one-third

120
00:05:45,090 --> 00:05:46,840
of my cases in a minority class.

121
00:05:46,840 --> 00:05:52,425
So now I give the minority class a weight of 0.66 and

122
00:05:52,425 --> 00:05:58,280
the majority case a weight of 0.34.

123
00:05:58,280 --> 00:06:00,090
So my weights now add up to

124
00:06:00,090 --> 00:06:04,890
1.66 but 0.34 and am over weighting

125
00:06:04,890 --> 00:06:07,650
those minority cases so that they have

126
00:06:07,650 --> 00:06:11,070
a balance with the majority cases

127
00:06:11,070 --> 00:06:14,385
and I don't bias my machine learning algorithm.

128
00:06:14,385 --> 00:06:17,490
There's more sophisticated methods I can use.

129
00:06:17,490 --> 00:06:19,050
We're not going to go into depth on this,

130
00:06:19,050 --> 00:06:21,750
but you can impute minority cases.

131
00:06:21,750 --> 00:06:24,270
There's various statistical methods.

132
00:06:24,270 --> 00:06:27,690
In most modern machine learning packages,

133
00:06:27,690 --> 00:06:30,675
you'll find options to use something called

134
00:06:30,675 --> 00:06:34,050
Synthetic Minority Over-sampling Technique or

135
00:06:34,050 --> 00:06:39,375
SMOTE and that's a powerful technique if it's available.

136
00:06:39,375 --> 00:06:44,585
But what I find in my practice is,

137
00:06:44,585 --> 00:06:46,530
I'm always very cautious of things

138
00:06:46,530 --> 00:06:48,810
like SMOTE or other imputation methods.

139
00:06:48,810 --> 00:06:50,460
Sometimes they work spectacularly

140
00:06:50,460 --> 00:06:52,860
well and I get amazing results.

141
00:06:52,860 --> 00:06:55,560
In other cases they just completely fail because

142
00:06:55,560 --> 00:06:59,975
the statistics of the problem aren't amendable to that.

143
00:06:59,975 --> 00:07:03,240
So, just to summarize,

144
00:07:03,240 --> 00:07:09,150
imbalanced training cases or imbalanced label cases are

145
00:07:09,150 --> 00:07:11,025
a real problem because they'll bias

146
00:07:11,025 --> 00:07:13,155
your machine learning model training

147
00:07:13,155 --> 00:07:15,020
often in a way you don't want.

148
00:07:15,020 --> 00:07:17,435
It's often exactly the wrong way.

149
00:07:17,435 --> 00:07:19,140
So we have to do something

150
00:07:19,140 --> 00:07:21,840
to come up with balanced cases.

151
00:07:21,840 --> 00:07:23,725
Our most common methods are

152
00:07:23,725 --> 00:07:26,270
to under-sample the majority case,

153
00:07:26,270 --> 00:07:28,780
over-sample the minority case,

154
00:07:28,780 --> 00:07:32,910
use weights if our algorithm adapts to it or in

155
00:07:32,910 --> 00:07:34,640
some cases maybe use

156
00:07:34,640 --> 00:07:38,790
some more sophisticated imputation method.

