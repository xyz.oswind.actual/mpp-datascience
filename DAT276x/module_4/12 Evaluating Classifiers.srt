0
00:00:05,140 --> 00:00:11,120
So let’s talk about how to evaluate a classifier. Now just following the example, we have our

1
00:00:11,120 --> 00:00:17,619
features, each observation being represented by a set of numbers, and each observation’s

2
00:00:17,619 --> 00:00:21,609
labelled and then the machine learning algorithm comes along and it gives a number to each

3
00:00:21,609 --> 00:00:25,919
observation, which is sort of what it thinks is going on, and then the number says how

4
00:00:25,919 --> 00:00:31,710
far away from the decision boundary the observation is, and also the sign of this function f is

5
00:00:31,710 --> 00:00:36,380
the predicted label. Now let’s put those in another column, so this is y hat, this

6
00:00:36,380 --> 00:00:42,309
is the sign of f; just tells you which side of the decision boundary that point is on.

7
00:00:42,309 --> 00:00:46,989
And if the classifier is right, then y hat agrees with y.

8
00:00:46,989 --> 00:00:51,550
Okay so let’s just look at these two columns for a few minutes. If the classifier is really

9
00:00:51,550 --> 00:00:58,879
good, these predicted labels often agree with the true labels. And let’s just put a few

10
00:00:58,879 --> 00:01:06,670
more examples in there just for fun. Now this is called a true positive, where the true

11
00:01:06,670 --> 00:01:13,530
label is plus one, and the predicted label is plus one. In a true negative, they’re

12
00:01:13,530 --> 00:01:20,640
both minus one. And then a false positive or a type one error is when you think that

13
00:01:20,640 --> 00:01:29,840
it’s positive, but it’s actually not. And then this is a false negative or a type

14
00:01:29,840 --> 00:01:36,000
2 error, where you think it’s negative, but it’s actually not. And then below it

15
00:01:36,000 --> 00:01:42,590
this is just another true negative, and below that is another false negative, and so on.

16
00:01:42,590 --> 00:01:53,299
Okay so the errors come in these two flavours. Now, how do we judge the quality of a classifier?

17
00:01:53,299 --> 00:01:59,899
And we construct a confusion matrix, and the confusion matrix has the true positives over

18
00:01:59,899 --> 00:02:05,090
on the upper left, and then the true negatives down on the lower right, and then the false

19
00:02:05,090 --> 00:02:08,070
positives up here and the false negatives down here so you can see that this classifier’s

20
00:02:08,070 --> 00:02:13,950
pretty good because most of the points are either true positives or true negatives, which

21
00:02:13,950 --> 00:02:23,319
is good and there’s not too many errors. Now if we don’t care about whether we have

22
00:02:23,319 --> 00:02:28,890
false positives or false negatives, if they’re both equally bad, then we can look at them

23
00:02:28,890 --> 00:02:34,489
as classification error. So this is just the fraction of points that are misclassified;

24
00:02:34,489 --> 00:02:41,239
it’s also called the classification error, and it’s also called the misclassification

25
00:02:41,239 --> 00:02:48,150
rate. And then I can write it this way, so this is the fraction of points for which the

26
00:02:48,150 --> 00:02:53,310
predicted label is not equal to the true label. So it just counts up the false positives and

27
00:02:53,310 --> 00:03:01,269
false negatives and divides by n. The true positive rate is simply the number of true

28
00:03:01,269 --> 00:03:11,140
positives divided by the total number of positives. So it’s this guy divided by the sum of these

29
00:03:11,140 --> 00:03:19,120
two here, so it’s the number of points whose true label is one, and whose predicted label

30
00:03:19,120 --> 00:03:25,150
is also one, and then divided by the number of points whose true label is one. It’s

31
00:03:25,150 --> 00:03:30,470
also called the sensitivity or the recall, and the true negative rate, or the specificity

32
00:03:30,470 --> 00:03:36,060
is defined this way: it’s actually just the number of true negatives divided by the

33
00:03:36,060 --> 00:03:44,580
total number of negatives, okay, so it’s the number of points who are negative – they’re

34
00:03:44,580 --> 00:03:49,439
truly negative, and they’re predicted to be negative, and then divided by the total

35
00:03:49,439 --> 00:03:55,180
number of negatives. So again, it’s this guy true negatives, divided by the sum of

36
00:03:55,180 --> 00:04:01,349
these two guys. And then the false positive rate looks like this: so it’s the number

37
00:04:01,349 --> 00:04:07,879
of false positives divided by the total number of negatives, and then there’s a few more

38
00:04:07,879 --> 00:04:13,370
metrics. There’s the precision, which is the true positives divided by the total number

39
00:04:13,370 --> 00:04:18,810
of predicted positives, so in other words it’s this one divided by the sum of these

40
00:04:18,810 --> 00:04:23,800
two. So the reason why I’m going through all these is because these metrics are all

41
00:04:23,800 --> 00:04:29,070
provided by pretty much any piece of software that you want to work with, and they’re

42
00:04:29,070 --> 00:04:34,860
quantities of interest that you hear about fairly often and here’s the F1 score – the

43
00:04:34,860 --> 00:04:40,720
F1 score is kind of neat. It’s a balance between precision and recall. So it’s two

44
00:04:40,720 --> 00:04:45,510
times precision times recall divided by precision plus recall. So if either the precision or

45
00:04:45,510 --> 00:04:52,890
the recall are bad, then the F1 score is bad. And so precision again uses those two quantities,

46
00:04:52,890 --> 00:05:00,790
and recall uses these two. But if you get a good F1 score, that generally means that

47
00:05:00,790 --> 00:05:06,450
your model – your model is good. F1 scores and precision and recall, these are all terms

48
00:05:06,450 --> 00:05:12,250
that are used very often in information retrieval, so things like evaluating search engines.

49
00:05:12,250 --> 00:05:19,030
And so here’s just some more detail about that. The precision at n for a search query

50
00:05:19,030 --> 00:05:26,420
is defined like this: so of the top n pages received by the search engine, how many were

51
00:05:26,420 --> 00:05:34,460
actually relevant to the query? And the way we can write that is the number of true positives

52
00:05:34,460 --> 00:05:44,020
out of those top n, divided by n – the number of pages received. And then the recall at

53
00:05:44,020 --> 00:05:51,940
n for a search query is the following: it says if there are n relevant webpages where

54
00:05:51,940 --> 00:05:58,690
n is the number of total positives – there are n relevant webpages what fraction of them

55
00:05:58,690 --> 00:06:05,020
did we get from our query? So that’s the number of true positives divided by the total

56
00:06:05,020 --> 00:06:10,340
number of positives. Which measure should you use? Now the machine

57
00:06:10,340 --> 00:06:15,710
learners often use accuracy – just plain accuracy or misclassification error because

58
00:06:15,710 --> 00:06:20,160
it’s just one number that you can directly compare across algorithms. You need a single

59
00:06:20,160 --> 00:06:24,570
measure of quality to compare algorithms. Once you have two measures of quality, you

60
00:06:24,570 --> 00:06:28,100
can’t directly make a comparison because what if one algorithm’s better according

61
00:06:28,100 --> 00:06:32,740
to one quality measure but not another one? Then you can’t compare them. But this only

62
00:06:32,740 --> 00:06:38,000
works – this one only works when errors for the positive class count equally to errors

63
00:06:38,000 --> 00:06:41,740
from the negative class, and this doesn’t work when the data are imbalanced, but anyway

64
00:06:41,740 --> 00:06:48,120
that’s what people do. So doctors, they often want to know like how many of the positives

65
00:06:48,120 --> 00:06:52,850
they got – they got right, and they want to know how many of the negatives they got

66
00:06:52,850 --> 00:06:56,460
right, so that makes sense that they want to look at both the true positive rate and

67
00:06:56,460 --> 00:07:01,250
the true negative rate. And if you’re in information retrieval, then you probably want

68
00:07:01,250 --> 00:07:06,590
to use precision and recall and F1 score, which is a combination of the two. So let’s

69
00:07:06,590 --> 00:07:09,600
say you’re – you know, you’re judging the quality of a search engine like Bing for

70
00:07:09,600 --> 00:07:15,280
instance. You might care about precision; again, precision is, you know, of the webpages

71
00:07:15,280 --> 00:07:18,940
that the engine returned, how many were relevant? That’s precision, and then recall is the

72
00:07:18,940 --> 00:07:23,380
fraction of the relevant webpages that the search engine returned, and then you use the

73
00:07:23,380 --> 00:07:27,110
F1 score so it’s a single measure, and then you can compare the quality of the different

74
00:07:27,110 --> 00:07:28,590
search engines in an easy way.

