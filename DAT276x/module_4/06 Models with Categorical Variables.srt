0
00:00:01,610 --> 00:00:04,550
>> Hello. So in this video,

1
00:00:04,550 --> 00:00:07,160
I'm going to talk about something that you may find a bit

2
00:00:07,160 --> 00:00:10,470
confusing when you build machine learning models in R,

3
00:00:10,470 --> 00:00:14,830
which is, how does R deal with categorical variables?

4
00:00:14,830 --> 00:00:17,690
You could expand this discussion to say,

5
00:00:17,690 --> 00:00:19,340
how does any machine

6
00:00:19,340 --> 00:00:23,400
learning algorithm deal with categorical variables?

7
00:00:23,400 --> 00:00:26,390
Machine learning algorithms work on numbers,

8
00:00:26,390 --> 00:00:28,265
they don't work on categories.

9
00:00:28,265 --> 00:00:30,860
They don't work on good customer,

10
00:00:30,860 --> 00:00:34,190
bad customer, male, female,

11
00:00:34,190 --> 00:00:36,840
whatever, they work on numbers.

12
00:00:36,840 --> 00:00:39,550
So, we have to convert

13
00:00:39,550 --> 00:00:43,260
categorical variables to what are called dummy variables.

14
00:00:43,260 --> 00:00:47,329
So, a dummy variable is a binary variable,

15
00:00:47,329 --> 00:00:48,840
and there's one dummy variable for

16
00:00:48,840 --> 00:00:51,000
each category in a categorical variable.

17
00:00:51,000 --> 00:00:56,015
Say we have four categories in our categorical variable,

18
00:00:56,015 --> 00:00:58,770
we need to expand

19
00:00:58,770 --> 00:01:01,450
that into four binary variables and say,

20
00:01:01,450 --> 00:01:06,630
it's something about body styles

21
00:01:06,630 --> 00:01:11,695
of automobiles like convertible, wagon, hatchback, sedan.

22
00:01:11,695 --> 00:01:15,180
It's sometimes called one hot encoding.

23
00:01:15,180 --> 00:01:19,060
So, the first one say, is convertible.

24
00:01:19,060 --> 00:01:20,689
It'll have a one

25
00:01:20,689 --> 00:01:25,005
if the car is a convertible and a zero elsewhere.

26
00:01:25,005 --> 00:01:27,790
But only one of those four variables

27
00:01:27,790 --> 00:01:29,420
for each case will have a one.

28
00:01:29,420 --> 00:01:31,569
That's why it's called one hot encoding,

29
00:01:31,569 --> 00:01:35,230
because you can't have

30
00:01:35,230 --> 00:01:36,895
a car that's both a convertible

31
00:01:36,895 --> 00:01:39,050
and a wagon or something like that,

32
00:01:39,050 --> 00:01:41,775
if they are unique categories.

33
00:01:41,775 --> 00:01:44,504
So, that's always happening

34
00:01:44,504 --> 00:01:46,940
whenever you create or do computation as a model.

35
00:01:46,940 --> 00:01:49,465
Sometimes you have to deal with this explicitly.

36
00:01:49,465 --> 00:01:51,880
Many times, you just see the results when

37
00:01:51,880 --> 00:01:54,100
you print model summaries and things like that.

38
00:01:54,100 --> 00:01:57,300
If you don't understand how that's working,

39
00:01:57,300 --> 00:01:58,500
it's a little confusing.

40
00:01:58,500 --> 00:02:01,220
So, let's look at one particular case and I hope

41
00:02:01,220 --> 00:02:05,560
that'll make it clear to you what's going on.

42
00:02:05,570 --> 00:02:08,610
So, we'll look at an example here.

43
00:02:08,610 --> 00:02:10,130
As I said, encode.

44
00:02:10,130 --> 00:02:13,175
I'm just going to load this German_Credit data.

45
00:02:13,175 --> 00:02:15,020
I'm going to remove columns like

46
00:02:15,020 --> 00:02:18,959
Customer_ID that aren't really features or label,

47
00:02:18,959 --> 00:02:21,375
and we'll look at the dimensionality.

48
00:02:21,375 --> 00:02:24,450
So, we're left with 21 columns,

49
00:02:24,450 --> 00:02:31,610
which are 20 features and are labeled bad_credit.

50
00:02:31,610 --> 00:02:34,810
But notice in the features, for example,

51
00:02:34,810 --> 00:02:38,205
checking account status is a factor with four levels.

52
00:02:38,205 --> 00:02:40,605
So as I said- and you see

53
00:02:40,605 --> 00:02:43,560
the first sample is

54
00:02:43,560 --> 00:02:47,185
something less than zero Deutsche Mark.

55
00:02:47,185 --> 00:02:51,995
So, you can't compute on a string like that as I said.

56
00:02:51,995 --> 00:02:54,260
So, we have to convert this to

57
00:02:54,260 --> 00:02:57,135
four binary dummy variables.

58
00:02:57,135 --> 00:02:59,570
For example, credit history likewise,

59
00:02:59,570 --> 00:03:00,780
would get converted to

60
00:03:00,780 --> 00:03:04,865
five dummy variables. How can we do that?

61
00:03:04,865 --> 00:03:06,890
So from the current package,

62
00:03:06,890 --> 00:03:09,810
there's this function called dummyVars.

63
00:03:09,810 --> 00:03:13,090
The way it works is, you have to say what your label is.

64
00:03:13,090 --> 00:03:14,695
In this case, it's bad_credit,

65
00:03:14,695 --> 00:03:17,055
and we're going to model that by "."

66
00:03:17,055 --> 00:03:20,495
So, that's all our features.

67
00:03:20,495 --> 00:03:23,550
What dummyVars is, it defines

68
00:03:23,550 --> 00:03:27,935
the recoding of the factors into dummy variables.

69
00:03:27,935 --> 00:03:30,620
Any numeric, like the integer,

70
00:03:30,620 --> 00:03:34,690
or float, columns are left alone.

71
00:03:34,690 --> 00:03:36,410
So they are not affected.

72
00:03:36,410 --> 00:03:40,190
So, you get this dummies object

73
00:03:40,190 --> 00:03:44,170
which is a training of the recoding.

74
00:03:44,170 --> 00:03:48,120
So, the dummies object here contains the recoding.

75
00:03:48,120 --> 00:03:50,745
Then I'm going to make a new data frame,

76
00:03:50,745 --> 00:03:54,595
because the predict method here for

77
00:03:54,595 --> 00:03:57,070
the dummy variables actually produces

78
00:03:57,070 --> 00:03:59,900
a matrix, not a data frame.

79
00:03:59,900 --> 00:04:05,235
So, I predict on dummies which is my trained object,

80
00:04:05,235 --> 00:04:07,045
and I give it new data credit

81
00:04:07,045 --> 00:04:08,690
which is just what I gave it.

82
00:04:08,690 --> 00:04:12,220
But you could give it training or test data.

83
00:04:12,220 --> 00:04:14,730
Then, I'm going to print the head again,

84
00:04:14,730 --> 00:04:17,625
and I'm going to print all the names.

85
00:04:17,625 --> 00:04:21,070
So, you can see how these features

86
00:04:21,070 --> 00:04:24,240
are expanded into dummy variables.

87
00:04:24,240 --> 00:04:26,770
Okay. So notice, here's

88
00:04:26,770 --> 00:04:28,200
our first dummy variable,

89
00:04:28,200 --> 00:04:30,900
checking_account_status zero DM.

90
00:04:30,900 --> 00:04:34,030
So, as I told you,

91
00:04:34,030 --> 00:04:37,085
there's one, two, three,

92
00:04:37,085 --> 00:04:40,595
four, that are checking account status something,

93
00:04:40,595 --> 00:04:45,695
and you see how R has expanded that name,

94
00:04:45,695 --> 00:04:51,125
so that now each category has its own column.

95
00:04:51,125 --> 00:04:53,520
This is why I was talking about the one hot encoding,

96
00:04:53,520 --> 00:04:55,560
like our first case here,

97
00:04:55,560 --> 00:04:59,805
has a checking account status between zero and 200 DM.

98
00:04:59,805 --> 00:05:03,080
Notice in the other three columns, there are all zeroes.

99
00:05:03,080 --> 00:05:06,400
Our second case has no checking account.

100
00:05:06,400 --> 00:05:09,580
Checking account status none et cetera.

101
00:05:09,580 --> 00:05:14,550
So then this third case is checking counts of zero.

102
00:05:14,550 --> 00:05:18,040
So, that's why it's called one hot encoding,

103
00:05:18,040 --> 00:05:19,440
and you expand the names.

104
00:05:19,440 --> 00:05:21,015
There's a lot of names,

105
00:05:21,015 --> 00:05:22,600
that's why I printed the names here.

106
00:05:22,600 --> 00:05:26,055
You can see things like foreign worker,

107
00:05:26,055 --> 00:05:27,540
no. Foreign worker, yes.

108
00:05:27,540 --> 00:05:32,840
So, there were two categories for foreign worker.

109
00:05:32,840 --> 00:05:37,750
So now, that expands into two dummy variables.

110
00:05:37,750 --> 00:05:42,710
So, that's the idea of dummy variables.

111
00:05:42,710 --> 00:05:44,430
I hope that helps you.

112
00:05:44,430 --> 00:05:45,610
Because like I said,

113
00:05:45,610 --> 00:05:49,320
many times you will encounter in

114
00:05:49,320 --> 00:05:50,930
summaries and other model

115
00:05:50,930 --> 00:05:54,600
reports the dummy variables themselves,

116
00:05:54,600 --> 00:05:56,449
as opposed to the original

117
00:05:56,449 --> 00:05:58,730
categorical feature you started with.

118
00:05:58,730 --> 00:06:00,770
So, it really helps to understand

119
00:06:00,770 --> 00:06:03,175
what's going on behind the scenes.

120
00:06:03,175 --> 00:06:05,800
Often, this is hidden from you in most R models,

121
00:06:05,800 --> 00:06:07,990
but there are cases

122
00:06:07,990 --> 00:06:10,070
where you have to deal with this directly.

123
00:06:10,070 --> 00:06:12,855
In other cases, we're interpreting the results.

124
00:06:12,855 --> 00:06:14,320
It's extremely important to

125
00:06:14,320 --> 00:06:16,570
understand how this coding and

126
00:06:16,570 --> 00:06:18,520
conversion from categorical variables to

127
00:06:18,520 --> 00:06:21,470
numeric dummy variables is done.

