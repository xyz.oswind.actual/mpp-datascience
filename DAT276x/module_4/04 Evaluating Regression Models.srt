0
00:00:04,950 --> 00:00:10,010
So let’s talk how to evaluate a regression model. So we have to figure out a way to evaluate

1
00:00:10,010 --> 00:00:15,730
the closeness of the truth to what we predicted which is f(x). So here’s what I proposed

2
00:00:15,730 --> 00:00:23,540
earlier 1-f and we didn’t like that because we could have equally proposed this one. So

3
00:00:23,540 --> 00:00:30,860
we want to use something that penalizes mistakes in either direction. So we could use that

4
00:00:30,860 --> 00:00:35,620
the distance this is the distance between the truth and the predictions and we could

5
00:00:35,620 --> 00:00:39,500
assess the mean absolute error which is the sum of these things that is one way to do

6
00:00:39,500 --> 00:00:43,500
it. But you could also square all of them and get the least squares error or the sum

7
00:00:43,500 --> 00:00:49,060
of the square error or the mean squared error. And you could think of it as capturing errors

8
00:00:49,060 --> 00:00:54,130
in both directions like the absolute value that we had earlier. The same deal penalize

9
00:00:54,130 --> 00:00:59,470
how far y is from f in either direction and then the root mean squared error is the just

10
00:00:59,470 --> 00:01:04,899
the square root of the sum of squares error. Now switching topics, let’s talk about residuals.

11
00:01:04,899 --> 00:01:12,439
The residuals or error is just the difference between the predictions and the truth. And

12
00:01:12,439 --> 00:01:17,950
obviously we want all of these to be close to 0. And we also don’t want to see any

13
00:01:17,950 --> 00:01:22,380
sort of structure or pattern in these residuals because then it would mean there is more modeling

14
00:01:22,380 --> 00:01:26,819
that we have to do. Let’s discuss this a bit more.

15
00:01:26,819 --> 00:01:30,780
Now we want the residuals to be as close to0 as possible, we don’t want any apparent

16
00:01:30,780 --> 00:01:36,219
pattern in the. So what I am going to do here is I am going to plot a histogram of all of

17
00:01:36,219 --> 00:01:44,229
the residuals, y, - f (xi) and where this histogram is higher is where most of the residuals

18
00:01:44,229 --> 00:01:49,369
are. And ideally they are mostly close to 0.

19
00:01:49,369 --> 00:01:56,119
So this would be good. Most of the residuals are around n 0 and there is no pattern in

20
00:01:56,119 --> 00:02:00,859
these residuals. IUF you see this you can smile, because you know you probably got,

21
00:02:00,859 --> 00:02:06,789
you did something right. This on the other hand is bad, this probably means that we missed

22
00:02:06,789 --> 00:02:12,470
something. So what could explain a plot like this?

23
00:02:12,470 --> 00:02:17,700
Maybe we are modeling the data with one line or something and actually the data falls under

24
00:02:17,700 --> 00:02:23,319
3 lines or something which is why there are 3 bumps. Maybe we just missed the other 2

25
00:02:23,319 --> 00:02:28,670
lines. I don’t now and I can’t tell exactly from looking at this what’s going on. In

26
00:02:28,670 --> 00:02:32,879
order to figure out what’s causing this we have to figure out what factor is common

27
00:02:32,879 --> 00:02:36,260
to everyone on this bump and then we have to figure out what’s common to everyone

28
00:02:36,260 --> 00:02:41,110
in that bump and then we have to include these extra factors in the regression so that we

29
00:02:41,110 --> 00:02:47,239
can model whatever signal is there and try to get rid of all those boxes. This is also

30
00:02:47,239 --> 00:02:52,799
bad. Right perhaps here we just have the wrong model there’s a lot of points that we are

31
00:02:52,799 --> 00:03:01,829
just not describing well. There a ton of points that are nowhere near f(x) not good. Now this

32
00:03:01,829 --> 00:03:08,480
is the list of features from the automobile pricing data set and we have all these factors

33
00:03:08,480 --> 00:03:15,109
about each car and then we are trying to predict the price. And we are going to do a regression

34
00:03:15,109 --> 00:03:20,549
using a couple of these features trying to predict the price. Now if you are curious

35
00:03:20,549 --> 00:03:28,230
we actually use the feature called fuel type engine horsepower, type of aspiration, which

36
00:03:28,230 --> 00:03:37,719
is turbo or not, and curb weight. And we use those things to predict the price of the car.

37
00:03:37,719 --> 00:03:45,620
And this is a plot that’s produced by R, which is something interesting.

38
00:03:45,620 --> 00:03:52,989
So along here along that axis is the estimated price. So the more expensive cars are over

39
00:03:52,989 --> 00:04:00,170
here. Now I just want you to pay attention here to the points here. Each point is a car.

40
00:04:00,170 --> 00:04:04,999
You can see that for the cheaper cars or predictions are very good. The residuals are close to

41
00:04:04,999 --> 00:04:09,450
a 0 for the cheaper cars. But then we get to the more expensive cars the predictions

42
00:04:09,450 --> 00:04:20,049
get worse, our predictions are farther from the truth. So why might that be? We do have

43
00:04:20,048 --> 00:04:24,770
a larger number of cheaper cars but the regression model is going to try to fit the cheaper cars

44
00:04:24,770 --> 00:04:29,420
better just because there is more of them. But also I was talking to Steve about this

45
00:04:29,420 --> 00:04:34,700
and he thinks it is because the more expensive cars are more diverse. So you just cannot

46
00:04:34,700 --> 00:04:41,840
predict very well for the more pricey cars. What the heck is that? Right that guy we didn’t

47
00:04:41,840 --> 00:04:49,040
get the rice on that one right atoll. Now this car happens to be an expensive 12-cylinder

48
00:04:49,040 --> 00:04:54,650
Jaguar with some unusual properties. It’s actually two-seater with a huge hood and an

49
00:04:54,650 --> 00:05:00,670
enormous engine. Actually this plot has the cars colored by the number of cylinders. So

50
00:05:00,670 --> 00:05:05,920
you can see that we predict better for smaller number of cylinders than we do for larger

51
00:05:05,920 --> 00:05:11,230
ones. Which I think is pretty interesting. So we might want to think about putting the

52
00:05:11,230 --> 00:05:18,350
number of cylinders into the model. And then here is a residual plot, which is again a

53
00:05:18,350 --> 00:05:24,500
histogram of residuals, and luckily for us, phew, it is nicely centered at 0. And here,

54
00:05:24,500 --> 00:05:27,840
my friends. is the Jaguar.

