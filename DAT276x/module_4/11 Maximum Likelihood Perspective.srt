0
00:00:05,299 --> 00:00:09,619
Let’s talk more in depth about logistic regression. Putting that in the corner for

1
00:00:09,619 --> 00:00:13,699
now, I wanted to give you another perspective on logistic regression, which is the maximum

2
00:00:13,699 --> 00:00:17,500
likelihood perspective. And you can skip this section if you’re not interested and nothing

3
00:00:17,500 --> 00:00:24,079
bad’s going to happen, but it might be useful to some of you. Look at this function here;

4
00:00:24,079 --> 00:00:29,310
it looks like – what does this function look like? It looks like something is growing

5
00:00:29,310 --> 00:00:34,570
and then saturating. Now this function is called the logistic function, and it was one

6
00:00:34,570 --> 00:00:42,219
of the very early population models invented by Adolphe Quetelet and his pupil, Pierre

7
00:00:42,219 --> 00:00:46,969
Francois Verhulst, somewhere in the mid-19th century, and they were modelling growth of

8
00:00:46,969 --> 00:00:50,070
populations, and they were thinking that when a country gets full, the population won’t

9
00:00:50,070 --> 00:00:53,390
grow as much and then the population will saturate, which is why it looks like that.

10
00:00:53,390 --> 00:00:57,530
And it sounds kind of funny, but that’s what they were doing. So see this is when

11
00:00:57,530 --> 00:01:00,980
the country is just growing and then here’s where it’s full and the population won’t

12
00:01:00,980 --> 00:01:07,120
grow anymore. Anyways, so how does this relate to logistic regression? Well, it does. So

13
00:01:07,120 --> 00:01:11,220
what do you know about probabilities, right? They don’t go below 0, and they don’t

14
00:01:11,220 --> 00:01:16,690
go above 1. You can take any number you like and send it through this function, and it’ll

15
00:01:16,690 --> 00:01:22,940
give you a number between 0 and 1, it’ll give you a probability. So this is the basic

16
00:01:22,940 --> 00:01:30,290
formula for that function, so when t is really big, then e to the t is much bigger than 1,

17
00:01:30,290 --> 00:01:39,910
and so this one basically gets ignored down here and you get 1. And if t is really small,

18
00:01:39,910 --> 00:01:45,680
then the top goes to 0 and the bottom goes to 1, and you get 0. Okay, so again, where

19
00:01:45,680 --> 00:01:52,090
does logistic regression come in? You know, here is where it enters logistic regression.

20
00:01:52,090 --> 00:02:03,370
Let’s model the probability that the outcome is – the outcome y is 1 for a specific accent

21
00:02:03,370 --> 00:02:09,060
beta just like this, okay? So why would we do this? It looks like a complicated function;

22
00:02:09,060 --> 00:02:13,590
where did I get this? So here’s the trick: the thing on the left is a probability, so

23
00:02:13,590 --> 00:02:22,210
the thing on the right had better be a probability. And guess what, we know it is. It’s just

24
00:02:22,210 --> 00:02:28,390
a logistic function, and a logistic function only produces probabilities. Okay so now this

25
00:02:28,390 --> 00:02:34,830
model makes sense, that’s why I want to model a probability like this. And now I’m

26
00:02:34,830 --> 00:02:38,860
just putting it in matrix notation, just to make my life a little bit easier instead of

27
00:02:38,860 --> 00:02:43,960
having to write all these sums all over the place, I can just write this matrix x times

28
00:02:43,960 --> 00:02:50,780
the vector beta. Now I also can compute the probability that the label is minus 1, given

29
00:02:50,780 --> 00:02:56,030
xi and beta using the model. So it’s just one minus the probability that it’s 1, so

30
00:02:56,030 --> 00:03:00,960
it’s just 1 minus that guy, and you can simplify that and make it look like this.

31
00:03:00,960 --> 00:03:07,760
Now I’m going to need to calculate the likelihood of each of the observations, which is the

32
00:03:07,760 --> 00:03:12,459
probability to observe the label y that I actually observed, given it’s x in the model

33
00:03:12,459 --> 00:03:19,740
beta. And I am actually almost there already, because I’ve already done all of that already.

34
00:03:19,740 --> 00:03:26,010
So this is it, right, if y is minus 1, then you use this one. If y is plus 1, then you

35
00:03:26,010 --> 00:03:32,930
use this one, and that’s – that’s this probability right here. And then I can simplify

36
00:03:32,930 --> 00:03:38,930
this a little bit more, because remember y is minus 1, so I can always put a minus y

37
00:03:38,930 --> 00:03:45,319
here because minus y is just 1, and I can do this same thing with the other term here.

38
00:03:45,319 --> 00:03:50,770
So first thing I want to just divide top and bottom by e to the x beta, and I end up with

39
00:03:50,770 --> 00:03:56,569
something that looks like that. And then I can always multiply by 1 in disguise, because

40
00:03:56,569 --> 00:04:06,190
remember y is positive 1, so I can just write this as minus yx because I just multiply by

41
00:04:06,190 --> 00:04:11,380
1 here which is just the y. Now the interesting thing is these two expressions should look

42
00:04:11,380 --> 00:04:17,440
rather similar to you; in fact, they should look exactly the same because they are. That’s

43
00:04:17,440 --> 00:04:23,509
very nice, because it means that the probability for y to equal whatever it does is written

44
00:04:23,509 --> 00:04:30,779
either this way or that way and they are the same. Okay, so I can just put it right there.

45
00:04:30,779 --> 00:04:37,240
Alright, just adding a little space there, and then I compute the likelihood for all

46
00:04:37,240 --> 00:04:45,449
the data, I have to multiply all these probabilities together. So what I end up with is this, so

47
00:04:45,449 --> 00:04:52,020
this is the full likelihood for the dataset, and it looks just like that. Okay, and so

48
00:04:52,020 --> 00:04:58,539
this guy equals this, which equals this, which equals that. So I can summarize there and

49
00:04:58,539 --> 00:05:05,119
start with a fresh page; there it is. And now, I can take negative log of both sides

50
00:05:05,119 --> 00:05:11,969
– that’s completely legal. And then, when you take the log of a product, it becomes

51
00:05:11,969 --> 00:05:22,830
the sum of the logs, so there we are. And then this fraction becomes – this is the

52
00:05:22,830 --> 00:05:26,919
log of this to the negative 1 power, so the negative 1 comes out front and cancels with

53
00:05:26,919 --> 00:05:35,809
this minus sign, and I get this expression. Now hopefully you have a good memory, because

54
00:05:35,809 --> 00:05:48,080
this expression is exactly the same as the one I have up in the corner, okay? So that’s

55
00:05:48,080 --> 00:05:55,580
cool, that minimizing negative log likelihood is the same as minimizing logistic logs. Now

56
00:05:55,580 --> 00:05:59,770
minimizing negative log likelihood is like finding the coefficients that are the most

57
00:05:59,770 --> 00:06:05,029
likely to generate your data, if you use the logistic model. I can derive the logistic

58
00:06:05,029 --> 00:06:10,569
regression a different way, but why do I care? Why do I need this other derivation when I

59
00:06:10,569 --> 00:06:21,639
have the first one? And the answer is really neat: it’s because now you have this. Remember

60
00:06:21,639 --> 00:06:28,460
this? This is the logistic function, but now it provides a probabilistic interpretation

61
00:06:28,460 --> 00:06:35,909
of the model. Whatever score that the model gives the observation, now you get the probability

62
00:06:35,909 --> 00:06:42,900
that y equals 1, given x. You don’t just get a classification, so maybe I can show

63
00:06:42,900 --> 00:06:55,249
it geometrically another way. Okay, so back to this picture over here. Now,

64
00:06:55,249 --> 00:07:02,419
this is the logistic function, and over here you get a higher probability estimate and

65
00:07:02,419 --> 00:07:09,460
over here it’s very low. And that interpretation is not something that you have with the loss

66
00:07:09,460 --> 00:07:17,629
function interpretation. Okay, so just a summary here: for a logistic regression, we split

67
00:07:17,629 --> 00:07:22,300
data randomly into training and test sets, we estimate the coefficients and train the

68
00:07:22,300 --> 00:07:28,069
model by minimizing the subjective, and then we score the model, and evaluate the model.

69
00:07:28,069 --> 00:07:33,639
And if we want to, now we have the probabilistic interpretation; we can send – we can get

70
00:07:33,639 --> 00:07:39,550
that through the function f. We can plug f into the logistic function to get an estimate

71
00:07:39,550 --> 00:07:45,999
of the probability that y equals 1 given x. and again, this is just the basic version

72
00:07:45,999 --> 00:07:51,089
in Azure ML; this is all the programming, I just – you know, literally moved the modules

73
00:07:51,089 --> 00:07:58,009
and moved the modules there and put the connectors on them and hit run and that’s it. Now this

74
00:07:58,009 --> 00:08:03,929
is just a preview of what happens when we put regularization on there; we can actually

75
00:08:03,929 --> 00:08:10,819
improve performance by asking the logistic model to be simple, and we can do that by

76
00:08:10,819 --> 00:08:16,229
adjusting this lovely lovely constant c, and that determines how much regularization we’ll

77
00:08:16,229 --> 00:08:26,589
put into the model to keep it simple. And again, we can work with linear models. So

78
00:08:26,589 --> 00:08:33,520
for regularization, we’ll choose the sum of the squares of these coefficients, and

79
00:08:33,520 --> 00:08:38,729
this is actually written in this nice neat way; this is actually called an L2 norm, and

80
00:08:38,729 --> 00:08:43,750
that’s what we’re going to use to measure simplicity of models, and that constant c

81
00:08:43,750 --> 00:08:49,120
is going to determine how much we care about the simplicity of the model and the accuracy

82
00:08:49,120 --> 00:08:56,240
of it. And here’s another kind of regularization where we take the sum of the absolute values

83
00:08:56,240 --> 00:09:02,120
of those coefficients, and this is actually written this way called the L1 norm. Now in

84
00:09:02,120 --> 00:09:06,509
practice, these two kinds of regularization – they have very different meaning and they

85
00:09:06,509 --> 00:09:11,069
change the coefficients in different ways, but they are both very helpful for purposes

86
00:09:11,069 --> 00:09:12,250
of generalization.

