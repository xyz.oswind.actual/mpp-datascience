0
00:00:05,049 --> 00:00:09,879
The key principle in statistical learning theory is the principle of Ockham’s razor.

1
00:00:09,879 --> 00:00:16,820
Now Ockham’s razor is the idea that the best models are simple models that fit the

2
00:00:16,820 --> 00:00:23,279
data well and it was named after the English friar and philosopher who said that among

3
00:00:23,279 --> 00:00:26,710
hypothesis that predict equally well, we should choose the one with the fewest assumptions.

4
00:00:26,710 --> 00:00:30,880
I’m sure he didn’t sort of have statistical learning theory in mind when he said that,

5
00:00:30,880 --> 00:00:37,760
but that’s where that expression comes from. Ok, so let’s start with a basic one dimension

6
00:00:37,760 --> 00:00:45,030
regression model. This model I have on the screen there is not good; it’s really over-fitted.

7
00:00:45,030 --> 00:00:50,210
So it’s just not going to generalize well to new points; it just can’t predict well.

8
00:00:50,210 --> 00:00:57,070
Now let’s say that I have some way to measure model complexity, and the more complex the

9
00:00:57,070 --> 00:01:04,589
models I have, the more they tend to over-fit and the simpler models, they tend to under-fit.

10
00:01:04,589 --> 00:01:11,400
Now this plot is the key to understanding learning theory. If I plot training error,

11
00:01:11,400 --> 00:01:18,760
which is this curve over here, then as the models grow more and more complex, the training

12
00:01:18,760 --> 00:01:25,360
error continues to decrease, because I can just over-fit more and more. But at the same

13
00:01:25,360 --> 00:01:33,250
time, if I do that, the test error gets worse and worse. If I, on the other hand, under-fit,

14
00:01:33,250 --> 00:01:38,490
then I won’t do well for either training or test. Where I want to go, is this sweet

15
00:01:38,490 --> 00:01:45,830
spot in the middle here. And the idea of this plot it holds true for classification, progression,

16
00:01:45,830 --> 00:01:51,240
whatever. So the idea is that the best models are simple models that fit the data well.

17
00:01:51,240 --> 00:01:56,730
So what we need is a balance between accuracy and simplicity.

18
00:01:56,730 --> 00:02:04,560
Now, Ockham probably didn’t know optimization, but this would have suited him fine, I’m

19
00:02:04,560 --> 00:02:11,590
guessing, if he were alive now. So the most common machine learning methods, they choose

20
00:02:11,590 --> 00:02:20,630
their function f, to minimize training error and model complexity, which aims to thwart

21
00:02:20,630 --> 00:02:26,560
the cursive dimensionality. So the cursive dimensionality is that we tend to over-fit

22
00:02:26,560 --> 00:02:32,450
when we have a lot of features and not as much data. Data needs to increase exponentially

23
00:02:32,450 --> 00:02:39,950
with the number of features in order not to have this issue of over-fitting. So we’re

24
00:02:39,950 --> 00:02:46,090
going to choose a model that’s both simple – low complexity – and has low training

25
00:02:46,090 --> 00:02:53,160
error; and this exactly is the principle of Ockham’s razor. And simplicity is measured

26
00:02:53,160 --> 00:02:59,550
in several different ways, and is usually called regularization machine learning. So

27
00:02:59,550 --> 00:03:05,240
this is the main foundation of machine learning; it’s all about creating functions that minimize

28
00:03:05,240 --> 00:03:10,170
the loss, but also keep the model simple. And this is the bottom line, folks; we’re

29
00:03:10,170 --> 00:03:13,700
going to do this in many different ways throughout the course. Different machine learning methods

30
00:03:13,700 --> 00:03:17,540
have different loss functions and they have different regularization terms. And so, as

31
00:03:17,540 --> 00:03:21,780
we go on, you’ll see more and more inside these machine learning methods, because I’ll

32
00:03:21,780 --> 00:03:23,130
tell you what all of these terms are.

