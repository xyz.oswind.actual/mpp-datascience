0
00:00:01,700 --> 00:00:05,385
>> Hello. So in the last demo,

1
00:00:05,385 --> 00:00:09,465
I created a logistic regression model

2
00:00:09,465 --> 00:00:13,170
for the German credit data and

3
00:00:13,170 --> 00:00:15,840
we computed probabilities and

4
00:00:15,840 --> 00:00:17,190
used those probabilities to compute

5
00:00:17,190 --> 00:00:18,810
a score of whether

6
00:00:18,810 --> 00:00:21,825
that customer was a good or bad credit customer.

7
00:00:21,825 --> 00:00:24,675
So the question is, well, okay,

8
00:00:24,675 --> 00:00:27,405
we saw where we had some errors and some correct cases.

9
00:00:27,405 --> 00:00:29,100
But overall, how did we do?

10
00:00:29,100 --> 00:00:33,605
How do we quantify the performance of a classifier?

11
00:00:33,605 --> 00:00:35,160
So in this demo,

12
00:00:35,160 --> 00:00:36,885
I'm going to show you some code

13
00:00:36,885 --> 00:00:39,540
where we can do just that.

14
00:00:41,120 --> 00:00:46,690
So I've created a logistic that avail function here.

15
00:00:46,690 --> 00:00:48,770
There's various ways you can do this in R

16
00:00:48,770 --> 00:00:52,090
but since this is a simple binary case,

17
00:00:52,090 --> 00:00:55,675
I'm just creating my own little function

18
00:00:55,675 --> 00:00:58,090
because it's very easy to do the bookkeeping for this.

19
00:00:58,090 --> 00:01:01,400
So, I'm creating a new column which I call

20
00:01:01,400 --> 00:01:05,880
con for confusion. How does this work?

21
00:01:05,880 --> 00:01:08,650
I have to do a nest it if else.

22
00:01:08,650 --> 00:01:12,535
So if the label,

23
00:01:12,535 --> 00:01:14,140
remember the label is bad credit

24
00:01:14,140 --> 00:01:16,150
is one and my score is one,

25
00:01:16,150 --> 00:01:17,900
well, that's a true positive.

26
00:01:17,900 --> 00:01:23,290
I got a positive label and a positive score.

27
00:01:23,290 --> 00:01:28,360
If the label is zero but my score is one,

28
00:01:28,360 --> 00:01:29,940
then I get a false positive.

29
00:01:29,940 --> 00:01:32,920
So you see my if else goes down like that.

30
00:01:32,920 --> 00:01:34,760
So I have an if else and then a

31
00:01:34,760 --> 00:01:37,870
nested if else and then a third nested if else.

32
00:01:37,870 --> 00:01:40,500
So then I can take care of all four cases.

33
00:01:40,500 --> 00:01:43,030
Then my third case is bad credit

34
00:01:43,030 --> 00:01:45,960
equals zero, score equals zero.

35
00:01:45,960 --> 00:01:48,310
Well, that's a true negative.

36
00:01:48,310 --> 00:01:51,540
After you go through this nested chain here,

37
00:01:51,540 --> 00:01:53,020
there's only one other possibility,

38
00:01:53,020 --> 00:01:55,030
I don't actually have to do the logical on

39
00:01:55,030 --> 00:01:57,265
it which is a false negative.

40
00:01:57,265 --> 00:01:59,060
So I've got my true positive,

41
00:01:59,060 --> 00:02:02,500
false positive, true negative, false negative.

42
00:02:02,800 --> 00:02:06,940
So the actual confusion matrix is this

43
00:02:06,940 --> 00:02:10,600
could be considered the sum or in this case the length.

44
00:02:10,600 --> 00:02:12,160
I just used the length because

45
00:02:12,160 --> 00:02:13,760
everywhere it's true positive,

46
00:02:13,760 --> 00:02:17,690
false positive, true negative, false negative.

47
00:02:17,690 --> 00:02:22,180
>> So, I just sub-setting that column of the data frame

48
00:02:22,180 --> 00:02:29,345
and then I just get the length.

49
00:02:29,345 --> 00:02:31,580
So now I have the four numbers

50
00:02:31,580 --> 00:02:35,050
for a binary confusion matrix.

51
00:02:36,650 --> 00:02:38,750
Since I have that,

52
00:02:38,750 --> 00:02:41,800
then I want to print it and I'm just going to

53
00:02:41,800 --> 00:02:45,930
print it with some nice names and things like that.

54
00:02:45,930 --> 00:02:50,230
Then I want to compute my precision recall in

55
00:02:50,230 --> 00:02:54,405
F1 and actually I compute

56
00:02:54,405 --> 00:02:58,905
accuracy just in line here and then I pretty print it.

57
00:02:58,905 --> 00:03:03,890
I paste a title I

58
00:03:03,890 --> 00:03:06,190
use as dot character on

59
00:03:06,190 --> 00:03:08,940
the rounded version of those statistics.

60
00:03:08,940 --> 00:03:12,880
So, let me just print that and you'll see how this looks.

61
00:03:12,880 --> 00:03:15,010
So there's that confusion matrix that got

62
00:03:15,010 --> 00:03:18,070
printed here and you see

63
00:03:18,070 --> 00:03:23,890
I've got 186 negative cases that I correctly classified.

64
00:03:23,890 --> 00:03:27,250
I've got 34 false positives,

65
00:03:27,250 --> 00:03:29,450
those were true negative cases

66
00:03:29,450 --> 00:03:32,775
but I classified them as positive.

67
00:03:32,775 --> 00:03:38,130
I have true positive cases where I

68
00:03:38,130 --> 00:03:40,465
classified them as positive

69
00:03:40,465 --> 00:03:43,140
36 but I'm not doing that well,

70
00:03:43,140 --> 00:03:47,315
I only got 36 and 42 are shown as false negative.

71
00:03:47,315 --> 00:03:50,325
So with this particular model,

72
00:03:50,325 --> 00:03:54,370
I would have missed 42 bad credit customers,

73
00:03:54,370 --> 00:04:01,460
only caught 36 in a fair high per portion of my score

74
00:04:01,460 --> 00:04:03,205
is positive would have been

75
00:04:03,205 --> 00:04:08,580
false positives but that's okay because this dataset,

76
00:04:08,580 --> 00:04:11,400
the cost to the bank of a

77
00:04:11,400 --> 00:04:16,440
false negative is five times that of a false positive.

78
00:04:16,440 --> 00:04:18,810
So the number we're really concerned about is

79
00:04:18,810 --> 00:04:21,850
getting this one down as we build better models.

80
00:04:21,850 --> 00:04:25,650
You notice the accuracy is 7.45.

81
00:04:25,650 --> 00:04:27,960
Well, that's seems pretty good but keep in

82
00:04:27,960 --> 00:04:30,090
mind that 70 percent of

83
00:04:30,090 --> 00:04:34,455
the cases are in fact negative.

84
00:04:34,455 --> 00:04:37,930
So it's not outstanding,

85
00:04:37,930 --> 00:04:40,500
precision recall is really not good at all,

86
00:04:40,500 --> 00:04:43,240
it's less than a half which reflects the fact

87
00:04:43,240 --> 00:04:46,705
that we correctly identified

88
00:04:46,705 --> 00:04:48,580
fewer than one half of

89
00:04:48,580 --> 00:04:52,180
the positive cases and

90
00:04:52,180 --> 00:04:55,930
therefore also reflects that somewhat poor result.

91
00:04:55,930 --> 00:04:58,460
So what about ROC and AUC?

92
00:04:58,460 --> 00:05:04,050
So I'm using our ROC curve package

93
00:05:04,050 --> 00:05:08,120
here and so I can get a performance.

94
00:05:08,120 --> 00:05:13,990
There's a performance function in that package.

95
00:05:14,830 --> 00:05:17,430
So I start with prediction,

96
00:05:17,430 --> 00:05:19,334
I create a prediction object,

97
00:05:19,334 --> 00:05:25,320
I apply the performance package for true positive rate,

98
00:05:25,320 --> 00:05:27,390
false positive rate and that gives

99
00:05:27,390 --> 00:05:30,045
me a performance object,

100
00:05:30,045 --> 00:05:33,380
and then I can compute

101
00:05:33,380 --> 00:05:36,590
an AUC from that and I can also plot it.

102
00:05:36,590 --> 00:05:40,695
I just plot true positive rate, false positive rate.

103
00:05:40,695 --> 00:05:43,020
So that's my plot here.

104
00:05:43,020 --> 00:05:45,435
>> So let me just run that and we'll see what

105
00:05:45,435 --> 00:05:48,850
our ROC curve and AUC.

106
00:05:48,850 --> 00:05:50,560
So AUC is 0.77.

107
00:05:50,560 --> 00:05:53,795
Again it seems respectable but not great.

108
00:05:53,795 --> 00:05:57,160
Recall if this were balanced cases,

109
00:05:57,160 --> 00:06:02,610
the diagonal line would represent half and half,

110
00:06:02,610 --> 00:06:05,380
basically a coin flip classifier,

111
00:06:05,380 --> 00:06:07,890
that's a useless classifier and you

112
00:06:07,890 --> 00:06:10,520
see our ROC curve is indeed above that.

113
00:06:10,520 --> 00:06:13,960
So we're doing better and you

114
00:06:13,960 --> 00:06:18,245
see on the horizontal axis is false positive rate,

115
00:06:18,245 --> 00:06:21,075
on the vertical axis is true positive rate.

116
00:06:21,075 --> 00:06:27,065
So we could pick a threshold on the probabilities

117
00:06:27,065 --> 00:06:29,170
from this classifier and find

118
00:06:29,170 --> 00:06:30,640
an operating point that we were

119
00:06:30,640 --> 00:06:33,870
happy with along this ROC curve.

120
00:06:34,510 --> 00:06:37,860
So let me just show you one last thing here.

121
00:06:37,860 --> 00:06:41,225
So what if I just had a really dumb classifier.

122
00:06:41,225 --> 00:06:43,285
This is a classifier that isn't in fact a classifier.

123
00:06:43,285 --> 00:06:45,199
I just say that the probabilities

124
00:06:45,199 --> 00:06:47,030
are all zero and the scores are all

125
00:06:47,030 --> 00:06:49,115
zero and I'm just going to go ahead and

126
00:06:49,115 --> 00:06:52,505
evaluate that using the same code we discussed.

127
00:06:52,505 --> 00:06:55,770
You see if I just looked at accuracy,

128
00:06:55,770 --> 00:06:59,400
I'd say almost 0.74.

129
00:06:59,400 --> 00:07:05,575
Well, that's just about what I got for my real model.

130
00:07:05,575 --> 00:07:08,510
So if you only look at one metric,

131
00:07:08,510 --> 00:07:11,690
you might be doing a happy dance but

132
00:07:11,690 --> 00:07:13,160
that's not what you should be

133
00:07:13,160 --> 00:07:15,275
doing because it's clearly wrong.

134
00:07:15,275 --> 00:07:17,160
Look, precision in F1

135
00:07:17,160 --> 00:07:19,485
can't even be computed, recall is zero,

136
00:07:19,485 --> 00:07:21,710
and if you look at the confusion matrix,

137
00:07:21,710 --> 00:07:24,590
you see there is no positive scores of course

138
00:07:24,590 --> 00:07:27,935
because this is a non-classifier.

139
00:07:27,935 --> 00:07:31,480
Further, if we look at AUC is 0.5 and you see

140
00:07:31,480 --> 00:07:36,025
the ROC curve lies right along that red diagonal line.

141
00:07:36,025 --> 00:07:38,060
So, but keep that in mind.

142
00:07:38,060 --> 00:07:40,670
If you were fooled into just using

143
00:07:40,670 --> 00:07:43,940
accuracy to evaluate this model,

144
00:07:43,940 --> 00:07:44,870
you couldn't tell the difference

145
00:07:44,870 --> 00:07:46,190
between a silly model like

146
00:07:46,190 --> 00:07:49,415
this and a real model.

147
00:07:49,415 --> 00:07:52,800
So that's the key points to keep in mind when

148
00:07:52,800 --> 00:07:56,475
you're evaluating classifier models.

149
00:07:56,475 --> 00:07:59,450
You need to look at all the performance metrics.

150
00:07:59,450 --> 00:08:02,440
I always first look at the confusion matrix.

151
00:08:02,440 --> 00:08:04,590
It gives me a lot of insight into

152
00:08:04,590 --> 00:08:06,510
the details and then I always

153
00:08:06,510 --> 00:08:10,280
look at all of the metrics precision, recall, accuracy,

154
00:08:10,280 --> 00:08:16,320
F1 and AUC and all those together

155
00:08:16,320 --> 00:08:17,970
give me an overall view of

156
00:08:17,970 --> 00:08:19,650
how well I'm doing and I need to keep in

157
00:08:19,650 --> 00:08:21,470
mind how I'm doing

158
00:08:21,470 --> 00:08:23,120
verses the problem I'm trying to solve.

159
00:08:23,120 --> 00:08:25,865
Like in this case with the bad credit customers,

160
00:08:25,865 --> 00:08:28,020
recall needs to be really high

161
00:08:28,020 --> 00:08:30,870
because every false negative,

162
00:08:30,870 --> 00:08:33,540
that is every bad credit customer we missed,

163
00:08:33,540 --> 00:08:35,610
cost the bank a lot of money.

164
00:08:35,610 --> 00:08:38,335
So, that would be

165
00:08:38,335 --> 00:08:41,760
a significant metric for success of

166
00:08:41,760 --> 00:08:44,155
actually having an effective classifier

167
00:08:44,155 --> 00:08:46,510
that solves that problem.

