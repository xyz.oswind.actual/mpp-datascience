0
00:00:05,009 --> 00:00:09,700
Well let’s talk about ROC curves. I say the word ROC probably at least once every

1
00:00:09,700 --> 00:00:16,080
day. So ROC curves started during World War 2 for analyzing radar signals. And the question

2
00:00:16,079 --> 00:00:22,289
that they answer is: for a particular False Positive Rate, what is the corresponding True

3
00:00:22,289 --> 00:00:28,440
Positive Rate. The false positive rate is the number of negatives that are classified

4
00:00:28,440 --> 00:00:34,160
by the algorithm as being positive, then that divided by the total number of negatives.

5
00:00:34,160 --> 00:00:39,460
And the true positive is the total number of positives that were classified by the machine

6
00:00:39,460 --> 00:00:45,219
learning algorithm as positive, divided by the total number of positives. And so, we

7
00:00:45,219 --> 00:00:50,809
want to have a, you know, for each positive false positive rate, we want to have a corresponding

8
00:00:50,809 --> 00:00:58,969
true positive rate. So let’s say that our classifier has a decision boundary which is

9
00:00:58,969 --> 00:01:03,799
right here. And the positives are over on this side and the negatives are over here.

10
00:01:03,799 --> 00:01:11,500
Then the true positive rate uses the number of positives classified as positive, so one,

11
00:01:11,500 --> 00:01:17,530
two, three, four, five, six, seven, and then divided by the total number of positives,

12
00:01:17,530 --> 00:01:26,140
which is seven on this side, eight, nine, ten, eleven – so seven out of eleven. And

13
00:01:26,140 --> 00:01:32,050
the false positive rate uses the number of negatives classified as positive – so one,

14
00:01:32,050 --> 00:01:37,790
two three, divided by the total number of negatives - so four, five, six, seven, eight,

15
00:01:37,790 --> 00:01:40,990
nine, ten, eleven, twelve – three out of twelve.

16
00:01:40,990 --> 00:01:47,490
So what if I chose the decision boundary to be a little bit different? So what if I had

17
00:01:47,490 --> 00:01:54,680
chosen f(x) = 3 to be my decision boundary? Then my boundary might be over here, like

18
00:01:54,680 --> 00:01:59,500
that. Then what happened to the true positive rate and false positive rate? Well now we

19
00:01:59,500 --> 00:02:06,570
only have three out of our eleven positives classified correctly, and we only have three

20
00:02:06,570 --> 00:02:14,790
of two negatives now, that are classified as positives, out of the twelve. And let’s

21
00:02:14,790 --> 00:02:19,189
put the decision boundary over here. And again, you can get values for the true positive rate

22
00:02:19,189 --> 00:02:24,920
and false positive rate. Ok, this game is kind of fun. So now, what if you sweep this

23
00:02:24,920 --> 00:02:32,420
thing down from top to bottom? Just take the thing and just go like that and just sweep

24
00:02:32,420 --> 00:02:36,629
it all the way down, and every time you move this line, you record the true positive rate

25
00:02:36,629 --> 00:02:43,370
and the false positive rate. And then you plot all of the true positive rates and false

26
00:02:43,370 --> 00:02:50,650
positive rates on a scatter plot; and that is an ROC curve. So as you swing that line

27
00:02:50,650 --> 00:02:59,799
across, as you sweep the thing down, the true positive rate grows because we have more positives

28
00:02:59,799 --> 00:03:04,489
that are classified as positives, but also the number of false positives grow too, because

29
00:03:04,489 --> 00:03:10,950
now we have more negatives being classified as positives. So the curve ends up looking

30
00:03:10,950 --> 00:03:18,459
something like that. Now, the quality measure people use is the

31
00:03:18,459 --> 00:03:25,310
area under the curve – the AUC, the area under curve, or area under the ROC curve – AUROC

32
00:03:25,310 --> 00:03:31,569
– so that is the quality measure that we often use for predictive models. So that’s

33
00:03:31,569 --> 00:03:41,040
why I say every day. We&#39;re always evaluating the quality of our method. Now, that line

34
00:03:41,040 --> 00:03:45,730
is random guessing. So if you’re not better than that line, you might want to turn your

35
00:03:45,730 --> 00:03:49,599
classifier upside down, so that it predicts the opposite of what it’s currently doing,

36
00:03:49,599 --> 00:03:53,129
because it’s worse than random guessing; which means it could be better if you flip

37
00:03:53,129 --> 00:03:59,010
it. So hopefully you get a curve like I showed you on the previous slide. Most of the time,

38
00:03:59,010 --> 00:04:02,469
you don’t get perfect; perfect would be just straight up and then over. Most of the

39
00:04:02,469 --> 00:04:06,249
time you won’t get that. Most prediction problems are not that easy. So you’ll get

40
00:04:06,249 --> 00:04:09,459
something actually kind of similar to what I showed you on the previous slide.

41
00:04:09,459 --> 00:04:14,199
Alright, so just to summarize, we talked about a lot of evaluation measures for machine learning

42
00:04:14,199 --> 00:04:21,530
models. You can summarize what the model does by looking at the confusion matrix, looking

43
00:04:21,529 --> 00:04:27,910
at the accuracy or the misclassification error, or you can look at the precision recall in

44
00:04:27,910 --> 00:04:33,220
F1- score if you’re doing information retrieval, and then you can look at ROC curves and the

45
00:04:33,220 --> 00:04:40,780
area under the ROC. Ok, so these are all the ways that we enumerated in the data science

46
00:04:40,780 --> 00:04:41,530
course and more.

