0
00:00:04,980 --> 00:00:09,160
Handling imbalanced data in machine learning is one of the most difficult and annoying

1
00:00:09,160 --> 00:00:16,219
problems in all of machine learning. When I think about imbalanced data, what I think

2
00:00:16,219 --> 00:00:24,160
about is a sea of negative observations with a few little positives hanging around in there.

3
00:00:24,160 --> 00:00:29,199
You can see my positives are here, and there’s a few positives sitting in there – they’re

4
00:00:29,199 --> 00:00:37,670
coloured red. And here’s a classifier. How do you like that? It seems like a great classifier,

5
00:00:37,670 --> 00:00:43,420
huh? It just votes everything as negative all the time. Actually, it’s a trivial classifier

6
00:00:43,420 --> 00:00:50,489
– it’s totally meaningless, but – and get this – it’s 99% accurate; because

7
00:00:50,489 --> 00:00:56,429
only 1% of the observations are positive. So this classifier is a really good classifier

8
00:00:56,429 --> 00:01:05,500
– it actually is, if you care about accuracy. But you see, it’s totally meaningless. Something

9
00:01:05,500 --> 00:01:11,560
like that would be much better. It misses a whole bunch of negatives and says they’re

10
00:01:11,560 --> 00:01:17,730
positive, but here, getting a true positive right is worth a lot more than getting a true

11
00:01:17,730 --> 00:01:23,290
negative, so it’s ok. So we’re no longer good to get 99% accuracy with this classifier,

12
00:01:23,290 --> 00:01:28,210
but really, accuracy is not the right thing to measure here now. Because the true positive

13
00:01:28,210 --> 00:01:34,220
is not worth the same as a true negative. So then the question is how to get the learning

14
00:01:34,220 --> 00:01:42,540
algorithm to favour a model like this over a model like that. Now there are many different

15
00:01:42,540 --> 00:01:46,670
ways to do this. The way I’m showing you is by no means the only way, but it’s my

16
00:01:46,670 --> 00:01:53,120
favourite way. So this is the usual objective function where you have a loss for each point.

17
00:01:53,120 --> 00:01:59,810
But you see, in this objective function, the positives are treated the same as the negatives.

18
00:01:59,810 --> 00:02:05,200
This is just the sum of all the points. The positives aren’t more important than the

19
00:02:05,200 --> 00:02:09,770
negatives, and they aren’t less important; but we need the positives to be more important.

20
00:02:09,770 --> 00:02:15,829
So what I’m going to do is ask the objective function to make the positives more important;

21
00:02:15,829 --> 00:02:22,819
and the way I’m going to do it is to break that sum over I up into positives and negatives,

22
00:02:22,819 --> 00:02:30,959
like that. Now that doesn’t do anything yet. All I did was split the sum up. But now

23
00:02:30,959 --> 00:02:37,390
I’m going to tell the loss function that one unit of loss from each negative is worth

24
00:02:37,390 --> 00:02:46,290
c units of loss from a positive. So we multiplied the first term by c, which means that each

25
00:02:46,290 --> 00:02:59,459
positive is worth c times negative, and that’s how we’ll get the model to favour this classifier

26
00:02:59,459 --> 00:03:05,280
over the other one. Because let’s say that each positive is worth ten times each of the

27
00:03:05,280 --> 00:03:13,450
negatives. Then yeah, ok, we miss all the negatives, but each positive is worth ten

28
00:03:13,450 --> 00:03:19,079
times one of the negatives, so this is ok. Now almost all of the machine learning algorithms

29
00:03:19,079 --> 00:03:25,730
allow you to adjust the parameter c that trades off between positives and negatives. And doing

30
00:03:25,730 --> 00:03:30,329
this works better for some algorithms than others. Like if you’re doing decision trees,

31
00:03:30,329 --> 00:03:35,310
why bother? Decision trees are totally lousy when it comes to imbalance. If you try to

32
00:03:35,310 --> 00:03:40,989
fiddle with that parameter c, the tree sometimes swings wildly from a trivial model that classifies

33
00:03:40,989 --> 00:03:45,389
everything as negative to a trivial model that classifies everything as positive, with

34
00:03:45,389 --> 00:03:50,189
nothing in between, and that’s lousy. But it works great for logistic regression, and

35
00:03:50,189 --> 00:03:54,599
it works great for support factor machines and a bunch of other algorithms. So hopefully,

36
00:03:54,599 --> 00:04:01,239
you’ll get at least some ideas from this about what to do to handle imbalanced data.

37
00:04:01,239 --> 00:04:07,680
So just do summarize, do not, when you’re dealing with imbalanced data, report vanilla

38
00:04:07,680 --> 00:04:13,870
accuracy. Please report true positive rate and false positive rate instead. Please report

39
00:04:13,870 --> 00:04:20,739
a meaningful point on an ROC curve. 99% accuracy is totally meaningless for imbalance problems.

40
00:04:20,738 --> 00:04:26,790
And the other thing you can do is adjust that c parameter to get an ideal balance between

41
00:04:26,790 --> 00:04:31,350
the true positive rate and the false positive rate. That’ll give you that nice balance

42
00:04:31,350 --> 00:04:38,820
between positives and negatives. Now, adjusting that c parameter actually has a second purpose,

43
00:04:38,820 --> 00:04:44,420
which is that you can use that same parameter to evaluate the quality of a whole algorithm.

44
00:04:44,420 --> 00:04:46,500
So I’ll tell you about that shortly.

