0
00:00:02,060 --> 00:00:05,220
>> Hi and welcome. In this video,

1
00:00:05,220 --> 00:00:07,200
I'm going to talk about a topic

2
00:00:07,200 --> 00:00:09,030
called dimensionality reduction,

3
00:00:09,030 --> 00:00:10,320
and specifically we're going to look at

4
00:00:10,320 --> 00:00:12,600
one method for dimensionality reductions,

5
00:00:12,600 --> 00:00:13,980
probably the most widely used

6
00:00:13,980 --> 00:00:15,630
dimensionality reduction method called

7
00:00:15,630 --> 00:00:19,320
principle component analysis or PCA.

8
00:00:19,320 --> 00:00:23,000
So first off, why do we do this?

9
00:00:23,000 --> 00:00:25,205
Why use dimensionality reduction?

10
00:00:25,205 --> 00:00:29,590
Well, our goal is pretty simple actually,

11
00:00:29,590 --> 00:00:31,440
we're trying to find a small number of

12
00:00:31,440 --> 00:00:33,750
transformed features that contain

13
00:00:33,750 --> 00:00:36,010
the most useful information.

14
00:00:36,010 --> 00:00:38,690
So, why would we want to do that?

15
00:00:38,690 --> 00:00:41,880
Well, maybe we have some features set,

16
00:00:41,880 --> 00:00:44,120
this shows up say in text analytics,

17
00:00:44,120 --> 00:00:45,885
in image understanding,

18
00:00:45,885 --> 00:00:49,890
in complex, other complex problems.

19
00:00:49,890 --> 00:00:51,880
We have very large numbers of features,

20
00:00:51,880 --> 00:00:53,840
you can get thousands, tens of thousands,

21
00:00:53,840 --> 00:00:55,475
even a million features,

22
00:00:55,475 --> 00:00:58,530
and it's hard to work with that.

23
00:00:58,530 --> 00:01:00,880
Your chance of your model being

24
00:01:00,880 --> 00:01:06,095
overfed are pretty high, almost guaranteed.

25
00:01:06,095 --> 00:01:08,040
So, how do you transform

26
00:01:08,040 --> 00:01:09,840
those thousands of features down to

27
00:01:09,840 --> 00:01:12,510
some manageable number that really help

28
00:01:12,510 --> 00:01:16,770
you predict the label that you're trying to predict?

29
00:01:16,770 --> 00:01:18,770
So that's what we're trying to figure out.

30
00:01:18,770 --> 00:01:20,490
What are some advantages

31
00:01:20,490 --> 00:01:22,620
of doing dimensionality reduction?

32
00:01:22,620 --> 00:01:24,900
We'll talk about the pitfalls later.

33
00:01:24,900 --> 00:01:26,970
So prevent overfitting, I

34
00:01:26,970 --> 00:01:28,770
already mentioned that if you start out

35
00:01:28,770 --> 00:01:31,530
with a million features or 100,000

36
00:01:31,530 --> 00:01:34,995
features and just try to fit them in some naive way,

37
00:01:34,995 --> 00:01:37,310
you're probably learning the data,

38
00:01:37,310 --> 00:01:40,145
you're not learning the general case of

39
00:01:40,145 --> 00:01:41,940
the function you want to learn

40
00:01:41,940 --> 00:01:44,985
to have a good predictive model.

41
00:01:44,985 --> 00:01:47,830
There's also computational efficiency.

42
00:01:47,830 --> 00:01:51,290
You can imagine that if you just naively

43
00:01:51,290 --> 00:01:53,830
through a million features

44
00:01:53,830 --> 00:01:55,950
at some machine learning model,

45
00:01:55,950 --> 00:01:58,760
it's going to require a lot of

46
00:01:58,760 --> 00:02:01,550
heavy duty computation to train

47
00:02:01,550 --> 00:02:03,050
that model even if the training

48
00:02:03,050 --> 00:02:05,410
is meaningless as I already indicated.

49
00:02:05,410 --> 00:02:08,030
But, if you can reduce that down to

50
00:02:08,030 --> 00:02:10,820
some number of useful features,

51
00:02:10,820 --> 00:02:14,670
then maybe you've gotten somewhere.

52
00:02:15,650 --> 00:02:18,060
So that's the general idea.

53
00:02:18,060 --> 00:02:22,480
Why do we want to do dimensionality reduction?

54
00:02:22,480 --> 00:02:25,640
Let's talk about a specific method

55
00:02:25,640 --> 00:02:29,490
called principal component analysis or PCA for short.

56
00:02:29,490 --> 00:02:32,345
So the idea is to find

57
00:02:32,345 --> 00:02:34,940
these principal components that

58
00:02:34,940 --> 00:02:37,025
explain the variance of the data.

59
00:02:37,025 --> 00:02:40,580
So we want to find the fewest number of components that

60
00:02:40,580 --> 00:02:44,950
explain the variance or variability in the data.

61
00:02:44,950 --> 00:02:48,445
That's what the goal is of PCA.

62
00:02:48,445 --> 00:02:50,555
So, how does that work?

63
00:02:50,555 --> 00:02:54,755
Well, the first component is a direction.

64
00:02:54,755 --> 00:02:56,929
So you're basically transforming

65
00:02:56,929 --> 00:02:58,490
to a new coordinance system,

66
00:02:58,490 --> 00:03:00,545
and so you're looking for direction

67
00:03:00,545 --> 00:03:02,310
for that first component

68
00:03:02,310 --> 00:03:04,460
that explains the largest amount of

69
00:03:04,460 --> 00:03:06,715
the variance, whatever that direction is.

70
00:03:06,715 --> 00:03:10,270
So that's going to be a new axis basically,

71
00:03:10,270 --> 00:03:12,935
a new, in your new coordinance system.

72
00:03:12,935 --> 00:03:15,240
Our second component, because we're building

73
00:03:15,240 --> 00:03:16,640
a coordinance system has to be

74
00:03:16,640 --> 00:03:18,745
orthogonal to that direction.

75
00:03:18,745 --> 00:03:21,395
It's the orthogonal direction that explains

76
00:03:21,395 --> 00:03:26,015
the second largest amount of the variance, et cetera.

77
00:03:26,015 --> 00:03:29,210
You can find as many components

78
00:03:29,210 --> 00:03:31,195
as you have dimensions in the data set.

79
00:03:31,195 --> 00:03:33,580
You'll find that the amount of,

80
00:03:33,580 --> 00:03:35,030
because we do this in

81
00:03:35,030 --> 00:03:37,010
order of how much variance is explained.

82
00:03:37,010 --> 00:03:38,330
You may only need to keep

83
00:03:38,330 --> 00:03:42,950
the first few principal components and the rest are

84
00:03:42,950 --> 00:03:46,220
just representing probably possibly noise

85
00:03:46,220 --> 00:03:49,745
in the data set which is why the whole method works.

86
00:03:49,745 --> 00:03:52,290
Okay. Let's talk specifically.

87
00:03:52,290 --> 00:03:54,330
Let's get into the maths a little bit here.

88
00:03:54,330 --> 00:03:57,300
Then I'll go through the maths and then I'm going to show

89
00:03:57,300 --> 00:04:00,825
you intuitively what I was talking about.

90
00:04:00,825 --> 00:04:03,720
So, our first component,

91
00:04:03,720 --> 00:04:06,425
let's say it's some vector w_1

92
00:04:06,425 --> 00:04:08,910
along which we want to maximize variance.

93
00:04:08,910 --> 00:04:11,550
So, we can write that this way: we

94
00:04:11,550 --> 00:04:16,090
have w_1 is the max over all

95
00:04:16,090 --> 00:04:19,450
possible w's where this

96
00:04:19,450 --> 00:04:24,585
sum of square dot product over the features i,

97
00:04:24,585 --> 00:04:29,560
so, x are features and we have some,

98
00:04:29,560 --> 00:04:31,680
I want the end or whatever of

99
00:04:31,680 --> 00:04:34,980
those and we take dot products and we square that.

100
00:04:34,980 --> 00:04:37,010
So, we take that sum. We're just trying to find

101
00:04:37,010 --> 00:04:40,560
the vector w that maximizes,

102
00:04:40,560 --> 00:04:43,350
but we have to do something else.

103
00:04:43,350 --> 00:04:45,959
We have to subject it to this constraint,

104
00:04:45,959 --> 00:04:50,220
that w_1 has to have a magnitude itself of one.

105
00:04:50,220 --> 00:04:51,550
Else we could cheat, right.

106
00:04:51,550 --> 00:04:54,780
We could make it longer or shorter and that would

107
00:04:54,780 --> 00:04:56,879
inflate or deflate the variance

108
00:04:56,879 --> 00:04:58,540
which obviously is ridiculous.

109
00:04:58,540 --> 00:05:02,645
So, we have to use a constraint like this.

110
00:05:02,645 --> 00:05:06,210
So, but that's a little hard to work with

111
00:05:06,210 --> 00:05:09,280
that formulation because of that constraint, right.

112
00:05:09,280 --> 00:05:11,030
So how do we do that?

113
00:05:11,030 --> 00:05:16,410
Well, we can expand that representation like this.

114
00:05:16,410 --> 00:05:18,620
So, this is really a norm,

115
00:05:18,620 --> 00:05:21,180
this is like an L2 norm here

116
00:05:21,180 --> 00:05:25,505
or a Euclidian norm of that dot product,

117
00:05:25,505 --> 00:05:28,870
of that, see there's no summation now.

118
00:05:28,870 --> 00:05:31,130
It's just that Euclidian norm.

119
00:05:31,130 --> 00:05:36,545
I can write it now as w transpose,

120
00:05:36,545 --> 00:05:39,315
X transpose Xw which is just the same

121
00:05:39,315 --> 00:05:45,435
as the Euclidean norm of those dot products over all i.

122
00:05:45,435 --> 00:05:51,990
Okay. But again, I still have this subject to the norm,

123
00:05:51,990 --> 00:05:56,840
the Euclidian norm of the vector w equal to one.

124
00:05:56,840 --> 00:06:00,020
So I haven't quite beat that problem back but at least I

125
00:06:00,020 --> 00:06:03,810
have a little bit more tractable representation here.

126
00:06:03,810 --> 00:06:05,660
So, well, okay,

127
00:06:05,660 --> 00:06:07,220
so it's easy enough to solve this problem,

128
00:06:07,220 --> 00:06:08,595
because if I simply,

129
00:06:08,595 --> 00:06:11,270
whatever product I get here,

130
00:06:11,270 --> 00:06:14,750
I make that a numerator and I make the denominator ww

131
00:06:14,750 --> 00:06:19,685
transpose that's just this a norm here.

132
00:06:19,685 --> 00:06:22,375
So by dividing through like that,

133
00:06:22,375 --> 00:06:24,775
I guarantee that this w_k,

134
00:06:24,775 --> 00:06:26,390
whatever it happens to be,

135
00:06:26,390 --> 00:06:30,170
is going to be Euclidian normal one.

136
00:06:30,170 --> 00:06:32,565
So that's it. That's all,

137
00:06:32,565 --> 00:06:36,070
that's how I find my principal component.

138
00:06:36,070 --> 00:06:38,285
My first principle component.

139
00:06:38,285 --> 00:06:41,200
But, I'm actually trying to find k of them,

140
00:06:41,200 --> 00:06:43,520
if I have k features,

141
00:06:43,520 --> 00:06:45,725
I'm going to wind up with k components,

142
00:06:45,725 --> 00:06:52,055
and so you can think about it this way it,

143
00:06:52,055 --> 00:06:56,675
once I found the k minus oneth components,

144
00:06:56,675 --> 00:07:00,540
I want to find the next vector, w_k,

145
00:07:00,540 --> 00:07:02,580
along which the remaining

146
00:07:02,580 --> 00:07:04,730
or unexplained variants is maximized.

147
00:07:04,730 --> 00:07:07,450
So you can write it this way, the unexplained data,

148
00:07:07,450 --> 00:07:10,030
which I'm going to call w_k

149
00:07:10,150 --> 00:07:14,645
is my original feature matrix X

150
00:07:14,645 --> 00:07:17,810
minus the sum of

151
00:07:17,810 --> 00:07:23,090
this product over my k minus one principal components.

152
00:07:25,130 --> 00:07:30,860
So, it's the remaining unexplained variance basically,

153
00:07:30,860 --> 00:07:33,655
and so all I have to do is now

154
00:07:33,655 --> 00:07:36,475
it's easy to compute X_k from this.

155
00:07:36,475 --> 00:07:39,160
simple linear algebra, and so I'm back

156
00:07:39,160 --> 00:07:41,650
to the same problem I had before where

157
00:07:41,650 --> 00:07:43,930
I'm trying to find a w_k such

158
00:07:43,930 --> 00:07:47,680
that this dot product is maximized,

159
00:07:47,680 --> 00:07:49,770
and of course subject to my constraint the w_k,

160
00:07:49,770 --> 00:07:53,735
the magnitude of w_k has to be one.

161
00:07:53,735 --> 00:07:57,500
So let's, okay. So that's enough for the math.

162
00:07:57,500 --> 00:07:59,110
Hopefully, you followed that

163
00:07:59,110 --> 00:08:01,330
but I think it helps to think

164
00:08:01,330 --> 00:08:04,980
about sort of a geometric interpretation of this,

165
00:08:04,980 --> 00:08:07,600
let's just look at a two dimensional example.

166
00:08:07,600 --> 00:08:10,685
So here I have some two dimensional data,

167
00:08:10,685 --> 00:08:13,575
and you see it's,

168
00:08:13,575 --> 00:08:17,430
I'm deliberately doing this as a bivariant normal.

169
00:08:17,430 --> 00:08:19,325
So it's like this little,

170
00:08:19,325 --> 00:08:23,020
the density is in a little ellipse here.

171
00:08:23,060 --> 00:08:25,635
But if I look at this,

172
00:08:25,635 --> 00:08:29,305
I see that on my two feature axes,

173
00:08:29,305 --> 00:08:33,270
X_1, X_2, I have equal variance.

174
00:08:33,270 --> 00:08:35,340
So that's interesting.

175
00:08:35,340 --> 00:08:37,070
In this coordinates system,

176
00:08:37,070 --> 00:08:38,780
in the coordinates system that just comes out

177
00:08:38,780 --> 00:08:40,715
of the box with these features,

178
00:08:40,715 --> 00:08:44,600
I have equal variance along those two axes.

179
00:08:44,600 --> 00:08:47,750
So that doesn't help me in terms of transforming this.

180
00:08:47,750 --> 00:08:51,645
So I want to find is a coordinate transform.

181
00:08:51,645 --> 00:08:54,260
Basically I wrote a set of

182
00:08:54,260 --> 00:08:58,550
cleverly discovered rotations to

183
00:08:58,550 --> 00:09:03,020
a new coordinate system where I can maximize,

184
00:09:03,020 --> 00:09:05,090
explain variance in an ordered way.

185
00:09:05,090 --> 00:09:09,295
Okay. So what happens.

186
00:09:09,295 --> 00:09:11,825
So I have this new coordinate system,

187
00:09:11,825 --> 00:09:15,720
things get rotated and I find that I

188
00:09:15,720 --> 00:09:19,590
have a first principle component here and you see that

189
00:09:19,590 --> 00:09:21,450
first principle component is along

190
00:09:21,450 --> 00:09:23,400
the long axis of the ellipse of

191
00:09:23,400 --> 00:09:26,230
my data and that's

192
00:09:26,230 --> 00:09:29,340
not an accident, that's very deliberate.

193
00:09:29,340 --> 00:09:31,680
That's what we were doing when we

194
00:09:31,680 --> 00:09:34,590
were maximizing the dot product

195
00:09:34,590 --> 00:09:39,660
of that component with

196
00:09:39,660 --> 00:09:43,560
respect to these two features X_1, X_2, right.

197
00:09:43,560 --> 00:09:47,460
That is going to be the maximum of that dot product.

198
00:09:47,460 --> 00:09:50,220
So I only have two dimensions here.

199
00:09:50,220 --> 00:09:52,390
So I only have this second component,

200
00:09:52,390 --> 00:09:56,535
PC_2 which explains the variance along

201
00:09:56,535 --> 00:09:58,630
this second dimension in

202
00:09:58,630 --> 00:10:01,825
my new rotated coordinate system.

203
00:10:01,825 --> 00:10:05,880
So, that's the geometric interpretation.

204
00:10:05,880 --> 00:10:08,080
I started out with features X_1, X_2.

205
00:10:08,080 --> 00:10:11,450
I found a first rotation that found

206
00:10:11,450 --> 00:10:13,940
this component PC_1 which

207
00:10:13,940 --> 00:10:16,280
explains all these variants here,

208
00:10:16,280 --> 00:10:17,510
and there was just a little bit of

209
00:10:17,510 --> 00:10:20,614
residual variance on this orthogonal direction

210
00:10:20,614 --> 00:10:22,925
which is explained by my PC_2

211
00:10:22,925 --> 00:10:25,215
and we can lay that out in tabular form.

212
00:10:25,215 --> 00:10:30,810
Okay. So I can have my components here on the table,

213
00:10:30,810 --> 00:10:33,530
on the left side of the table and the amount of variance

214
00:10:33,530 --> 00:10:34,700
explained which I'm just going to

215
00:10:34,700 --> 00:10:36,725
hypothetically throw out some numbers here.

216
00:10:36,725 --> 00:10:41,210
So let's say PC_1 which is along this long axis of

217
00:10:41,210 --> 00:10:45,945
the ellipse explains 80 percent of the variance.

218
00:10:45,945 --> 00:10:49,025
PC_2 explains 20 percent,

219
00:10:49,025 --> 00:10:51,560
and of course that has to add up that I've explained

220
00:10:51,560 --> 00:10:53,060
all the variance because I only

221
00:10:53,060 --> 00:10:55,840
have two dimensions in this problem.

222
00:10:56,100 --> 00:10:58,910
So let's talk about the pitfalls.

223
00:10:58,910 --> 00:11:01,160
This is a pretty amazing method.

224
00:11:01,160 --> 00:11:03,830
It works really well in some cases but it can

225
00:11:03,830 --> 00:11:07,195
fail for a number of reasons.

226
00:11:07,195 --> 00:11:09,890
First off, keep in mind it's a linear model.

227
00:11:09,890 --> 00:11:11,950
Remember we derived it with linear algebra,

228
00:11:11,950 --> 00:11:15,470
we're making assumptions about that linear rotation,

229
00:11:15,470 --> 00:11:19,535
that simple rotations of the coordinates system

230
00:11:19,535 --> 00:11:22,340
get us to a new coordinates

231
00:11:22,340 --> 00:11:25,100
system that really explains the variance,

232
00:11:25,100 --> 00:11:29,550
but, and so that's a fundamental assumption.

233
00:11:29,550 --> 00:11:33,710
But if you have data with really nonlinear,

234
00:11:33,710 --> 00:11:37,580
many modes or separated clusters or something like that,

235
00:11:37,580 --> 00:11:40,755
it's not clear that this is going to work that well.

236
00:11:40,755 --> 00:11:42,305
It may or it may not.

237
00:11:42,305 --> 00:11:45,470
It's also PCA is not robust to outliers.

238
00:11:45,470 --> 00:11:47,580
You can imagine if you had some outliers out there

239
00:11:47,580 --> 00:11:51,975
that really drive the variance of the data,

240
00:11:51,975 --> 00:11:55,970
those might seriously influence this.

241
00:11:55,970 --> 00:11:58,925
So if you're using this method,

242
00:11:58,925 --> 00:12:01,690
you might want to consider how you treat the outliers,

243
00:12:01,690 --> 00:12:03,040
even if the outliers have important

244
00:12:03,040 --> 00:12:04,240
information maybe you want

245
00:12:04,240 --> 00:12:06,970
to find the components without

246
00:12:06,970 --> 00:12:10,240
the outliers and then simply find the mapping to the new

247
00:12:10,240 --> 00:12:12,370
coordinates system for the outliers

248
00:12:12,370 --> 00:12:13,725
or something like that.

249
00:12:13,725 --> 00:12:19,010
So, don't get burned on the lack of robustness.

250
00:12:19,830 --> 00:12:23,460
So that's it in a nutshell.

251
00:12:23,460 --> 00:12:27,520
Dimensionality reduction is a really powerful technique

252
00:12:27,520 --> 00:12:30,890
to help you prevent overfitting your models,

253
00:12:30,890 --> 00:12:33,835
to reduce the computational complexity of your models.

254
00:12:33,835 --> 00:12:36,670
We've talked about PCA which works

255
00:12:36,670 --> 00:12:40,465
really well in many cases but not all.

256
00:12:40,465 --> 00:12:45,550
But it's a really well known and very efficient way

257
00:12:45,550 --> 00:12:48,570
of doing dimensionality reduction.

