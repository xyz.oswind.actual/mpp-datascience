0
00:00:05,250 --> 00:00:10,210
Now interpreting coefficients of machine learning models can be a dangerous thing. People always

1
00:00:10,210 --> 00:00:15,929
want to do it, but it’s not a good idea and I’ll tell you why. So first of all,

2
00:00:15,929 --> 00:00:22,570
if you have collinear features, that can really mess you up. Now collinear features are very

3
00:00:22,570 --> 00:00:27,960
highly correlated features and I have a love-hate relationship with collinear features. Collinear

4
00:00:27,960 --> 00:00:31,840
features – you should think about them as features that are almost exactly the same.

5
00:00:31,840 --> 00:00:37,710
Okay so here’s what I like, highly correlated features. Let’s say I’m analyzing the

6
00:00:37,710 --> 00:00:44,649
manhole data set, and I find out very quickly that the number of electrical cables is actually

7
00:00:44,649 --> 00:00:49,950
a really really good feature, highly predictive. And this is true in real like by the way.

8
00:00:49,950 --> 00:00:56,420
So being a good machine learner, what am I going to do? So here’s what: I’m going

9
00:00:56,420 --> 00:01:00,770
to think to myself the cables as important, and that means I’m on the right track with

10
00:01:00,770 --> 00:01:04,890
that feature. So I’m going to derive a whole bunch of very similar features to the one

11
00:01:04,890 --> 00:01:09,680
that did well, but we need to gain some predictive performance. So what I’m doing there is

12
00:01:09,680 --> 00:01:15,340
creating lots of highly correlated features. And this is what any sane data scientist would

13
00:01:15,340 --> 00:01:20,140
do, when trying to get more predictive power, because you don’t know whether you could

14
00:01:20,140 --> 00:01:25,250
get more predictive power by adding features that are similar to the ones you already added,

15
00:01:25,250 --> 00:01:30,670
so you throw them in. Okay, so that’s why I love highly correlated

16
00:01:30,670 --> 00:01:41,360
features, because data scientist create them, to get more information about important topics.

17
00:01:41,360 --> 00:01:46,820
However, they mess up the interpretation of the coefficients. Here’s an example why:

18
00:01:46,820 --> 00:01:53,290
so let’s say you do regression without regularization and you added a couple of features that are

19
00:01:53,290 --> 00:02:00,430
almost exactly the same, and these end up being your coefficients. Yuck… they’re

20
00:02:00,430 --> 00:02:06,520
like – they’re cancelling each other out basically. So at the same time, you decided

21
00:02:06,520 --> 00:02:10,299
to do something that people are tempted to do but really should not, which is that you

22
00:02:10,299 --> 00:02:16,790
look to the coefficients to see which features are most important. And what you saw was this

23
00:02:16,790 --> 00:02:20,540
awful thing that these two features were really really important when actually that’s not

24
00:02:20,540 --> 00:02:23,840
what’s going on, it’s just that the two features cancel each other out and so the

25
00:02:23,840 --> 00:02:29,530
net of these two features is basically 0, even though the coefficients are huge. So

26
00:02:29,530 --> 00:02:33,730
the coefficients don’t really have anything to do with how important the features are.

27
00:02:33,730 --> 00:02:40,660
So I drew an obvious one, but it can get very very subtle. So let’s say again that these

28
00:02:40,660 --> 00:02:47,950
four features are highly correlated with each other. So what’s the most important feature?

29
00:02:47,950 --> 00:02:54,349
Now it looks like the number of events last year, but actually since these cable features

30
00:02:54,349 --> 00:03:01,709
are all very similar, you could’ve equally written a model like this, with equal predictive

31
00:03:01,709 --> 00:03:07,640
performance, because all these features are equal. So now this feature looks like the

32
00:03:07,640 --> 00:03:17,050
most important. So again, you should not have used the coefficients to try to understand

33
00:03:17,050 --> 00:03:23,680
the importance of features, because it’s very misleading. Okay so they mess up the

34
00:03:23,680 --> 00:03:30,980
interpretation of the coefficients. Okay, so what else can mess up the interpretation

35
00:03:30,980 --> 00:03:40,650
of the coefficients? How about scaling? Now bad scaling – that can really mess things

36
00:03:40,650 --> 00:03:48,110
up. So right now, for this model right here that I have on the screen, it looks like the

37
00:03:48,110 --> 00:03:53,810
number of events last year is the most important. The number of cables, that looks seriously

38
00:03:53,810 --> 00:03:59,019
unimportant. The coefficient is tiny. So why don’t we just eliminate that one? Because

39
00:03:59,019 --> 00:04:07,099
the coefficient’s not important, right? It turns out that if you eliminate that one,

40
00:04:07,099 --> 00:04:15,739
the accuracy goes from 70 percent accuracy to 55 percent. That is really really bad.

41
00:04:15,739 --> 00:04:26,229
Okay we’ll try eliminating that one instead. Then what happens? Hold on, that didn’t

42
00:04:26,229 --> 00:04:30,210
do very much damage at all… we only went to 68 percent, how could that possibly be

43
00:04:30,210 --> 00:04:38,610
true? He looked like this one was more important but actually this one with the tiny coefficient

44
00:04:38,610 --> 00:04:43,370
was much more important and let’s assume here that there’s no multi-collinearity

45
00:04:43,370 --> 00:04:51,020
or anything like that going on. There’s something I didn’t tell you; it turns out

46
00:04:51,020 --> 00:04:58,240
that the range of this variable is either 0 or 1. Like, the manhole’s – the most

47
00:04:58,240 --> 00:05:05,479
events they had last year was one. So, this term contributes at most five points to the

48
00:05:05,479 --> 00:05:13,870
score, whereas the number of cables in a manhole can get up to 145. So this term here can actually

49
00:05:13,870 --> 00:05:19,750
contribute over 14 points to the score. But even though the coefficient is small, that

50
00:05:19,750 --> 00:05:26,840
term still has a much larger impact than the other one. So bad scaling messes up the interpretation

51
00:05:26,840 --> 00:05:32,660
of the coefficients. As it happens, it also messes up regularization; it puts more pressure

52
00:05:32,660 --> 00:05:37,270
on the larger coefficients to become smaller, which means it’ll try to reduce the coefficient

53
00:05:37,270 --> 00:05:41,650
of the features that have the smallest values, and that’s not really what you want. Let

54
00:05:41,650 --> 00:05:46,410
me show you an example. Okay so let’s say that we use regular L2

55
00:05:46,410 --> 00:05:50,419
regularization, and let’s say that there are two features and they’re equally important,

56
00:05:50,419 --> 00:05:54,970
but they’re on different scales. The value of one is much smaller than the value of the

57
00:05:54,970 --> 00:06:00,520
other, which means that the coefficients are not on the same scale. Okay, so the values

58
00:06:00,520 --> 00:06:06,319
of the first feature are just small like maybe the values are just 0 or 1. The coefficient’s

59
00:06:06,319 --> 00:06:11,440
large like 5. And the values of the second feature are really large, like 145 for something.

60
00:06:11,440 --> 00:06:16,620
So the coefficient has to be really small to bring them back down to the same scale.

61
00:06:16,620 --> 00:06:21,650
So let’s say our coefficients start out in about this range of 5 and 0.1. Okay, so

62
00:06:21,650 --> 00:06:30,130
the total regularization term is 25.01, and now I’m going to show you what happens when

63
00:06:30,130 --> 00:06:38,690
we reduce each of the coefficients by 20 percent. It would be nice if the regularization didn’t

64
00:06:38,690 --> 00:06:44,810
care which coefficient we reduce by 20 percent, but that’s not what’s going to happen.

65
00:06:44,810 --> 00:06:49,919
You’ll see that the regularization here would prefer to reduce this term over that

66
00:06:49,919 --> 00:06:57,729
term. Okay so if we reduce the first coefficient by 20 percent, we’ll go from 5 to 4, and

67
00:06:57,729 --> 00:07:05,139
the regularization term will go from 25 to 16. If on the other hand we reduced the second

68
00:07:05,139 --> 00:07:16,729
coefficient by 20 percent, then the regularization term only decreases by a tiny tiny drop. And

69
00:07:16,729 --> 00:07:23,289
what that means is that this regularization term would much rather reduce the first coefficient

70
00:07:23,289 --> 00:07:29,660
than the second. Even though the features are supposedly equally important, we’ve

71
00:07:29,660 --> 00:07:35,090
got to figure out a way to avoid this. We need both coefficients to be in the same scale,

72
00:07:35,090 --> 00:07:40,490
so that the regularization doesn’t have certain features being preferred for arbitrary

73
00:07:40,490 --> 00:07:45,349
reasons. Alright, so I’ve told you that bad scaling

74
00:07:45,349 --> 00:07:50,039
is a bad thing, but we can at least fix the problem with the regularization if we fix

75
00:07:50,039 --> 00:08:00,080
it. Now let’s do that. So we’ll scale the coefficients to be between minus 1 and

76
00:08:00,080 --> 00:08:04,789
1. Between 0 and 1 is okay too. So this formula is going to do that; you take all the feature

77
00:08:04,789 --> 00:08:11,879
values for one feature, and then you subtract the minimum and divide by the range. The largest

78
00:08:11,879 --> 00:08:17,740
that x scaled can be is x, it’s the maximum of x, that the largest scaled version can

79
00:08:17,740 --> 00:08:23,169
be one. Okay, and the smallest it can be here is 0, which happens when x is at its minimum

80
00:08:23,169 --> 00:08:29,330
value, and the numerator is 0. Now it doesn’t matter whether you use exactly this formula,

81
00:08:29,330 --> 00:08:33,339
or if you try to do something else like center each feature by 0 and then forcing them all

82
00:08:33,339 --> 00:08:37,769
to have the same sample variance; that’s fine too. You just want to get them vaguely

83
00:08:37,769 --> 00:08:42,640
in the same range so that the coefficients are approximately in the same scale. This

84
00:08:42,640 --> 00:08:47,700
mentions – this avoids the issues that I mentioned earlier, and you should do this

85
00:08:47,700 --> 00:08:50,990
as pre-processing. Before you start working with the features, just make sure that they’re

86
00:08:50,990 --> 00:08:51,940
all on the same scale.

