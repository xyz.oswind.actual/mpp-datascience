0
00:00:05,100 --> 00:00:10,920
Regularization is the concept of this chapter and it’s the reason why a lot of machine

1
00:00:10,920 --> 00:00:17,760
learning methods generalize. So let us discuss that. Now remember from statistical learning

2
00:00:17,760 --> 00:00:22,140
theory that in order to keep the error small on our test set, we need to make the model

3
00:00:22,140 --> 00:00:27,710
accurate on the training set but we also need to keep it simple. Right, if you have no regularization,

4
00:00:27,710 --> 00:00:31,490
you have no way to control simplicity of the models. Then you’re minimizing just the

5
00:00:31,490 --> 00:00:35,820
training error, and then you’re here and you’re over-fitting, right, so the problem

6
00:00:35,820 --> 00:00:40,339
is that we’re allowing the models to be too complex. If we limit the complexity of

7
00:00:40,339 --> 00:00:46,819
the models, we could do better. If you use the regularization, it actually limits the

8
00:00:46,819 --> 00:00:51,579
complexity of your models so that when you minimize training error with regularization,

9
00:00:51,579 --> 00:00:55,769
you’re just minimizing error among the simpler models. So then you’re in the good zone,

10
00:00:55,769 --> 00:01:02,109
and you can generalize and your test error will actually be good. Now, simplicity here

11
00:01:02,109 --> 00:01:07,810
is measured by that regularization term, and the constant C – this constant is very very

12
00:01:07,810 --> 00:01:13,140
important because it determines that central trade-off between accuracy and simplicity.

13
00:01:13,140 --> 00:01:21,600
Now let us start here with the regularized loss function, and for the model F, we’re

14
00:01:21,600 --> 00:01:27,369
going to choose a linear model, just because it’s natural and it’s easy. And the coefficients

15
00:01:27,369 --> 00:01:34,140
are called beta. In order to regularize, in order to keep things simple, I will ask the

16
00:01:34,140 --> 00:01:43,630
coefficients to be small. I will claim that a simpler model is one with small coefficients.

17
00:01:43,630 --> 00:01:49,229
So let me tell you two options or regularization terms that encourage those coefficients to

18
00:01:49,229 --> 00:01:55,369
be small. So first you could squirrel the coefficients and then sum them all up, and

19
00:01:55,369 --> 00:02:00,439
in that case when you minimize this regularization term, it’s encouraging all these coefficients

20
00:02:00,439 --> 00:02:05,539
to be small. Now the sum of the squares of the coefficients – this is called the L2

21
00:02:05,539 --> 00:02:11,480
norm, and this is called the l2 regularization. And here’s an alternative where we add up

22
00:02:11,480 --> 00:02:18,880
the absolute values of the coefficients, and this is called the L1 norm and this L1 regularization.

23
00:02:18,880 --> 00:02:25,140
Now to see what these things do, let’s try to do an example in one dimension. Okay so

24
00:02:25,140 --> 00:02:30,920
what happens in one dimension? We have only one coefficient; it’s called beta one. Okay,

25
00:02:30,920 --> 00:02:36,660
so when we regularize, we’re asking either the square of beta one to be small or the

26
00:02:36,660 --> 00:02:41,530
absolute value of it to be small, which is basically like the same thing – make beta

27
00:02:41,530 --> 00:02:48,420
one small. Now let’s say we’re doing plain old regression, least squares or something.

28
00:02:48,420 --> 00:02:58,250
Now what does that regularization do? So here’s our regularization, what it does is it reduces

29
00:02:58,250 --> 00:03:04,530
the slope of the line, okay, so maybe the regularized version might look like that,

30
00:03:04,530 --> 00:03:09,840
rather than the last one here. It looks kind of minor, but really what the regularization

31
00:03:09,840 --> 00:03:14,610
is doing is it’s helping the line to be flatter which means that it’s saying, you

32
00:03:14,610 --> 00:03:18,760
know, I don’t want to pay attention to the variation in the data, I want to try and ignore

33
00:03:18,760 --> 00:03:24,730
it. I want to keep as flat as you’ll let me. Now it might not be obvious for the line,

34
00:03:24,730 --> 00:03:31,180
but what about a higher dimensional polynomial? Okay so let’s try to stick to one dimension

35
00:03:31,180 --> 00:03:38,300
still, but we’ll make all the other variables monomials of x. So let me right that up there…

36
00:03:38,300 --> 00:03:43,050
so we only have one variable x, but we’ve squared and cubed it and lots of – you know,

37
00:03:43,050 --> 00:03:48,230
lots of curvy polynomials here. And they can each have a different coefficient there. So

38
00:03:48,230 --> 00:03:52,980
it’s still a linear model, it’s linear in a bunch of non-linear terms, namely all

39
00:03:52,980 --> 00:03:58,840
of these monomials; but since you’ve pre-computed all the non-linear stuff, the only modelling

40
00:03:58,840 --> 00:04:03,090
left is actually linear. I’m sort of cheating; I’m saying it’s a linear model, but it’s

41
00:04:03,090 --> 00:04:08,970
actually highly non-linear – it’s a polynomial of degree p. the computer didn’t know that

42
00:04:08,970 --> 00:04:13,350
I squared things and cubed things and so on, so as far as the computer is concerned this

43
00:04:13,350 --> 00:04:18,840
is a linear model. So let’s fit it with that regularization, and it should look something

44
00:04:18,839 --> 00:04:22,600
like that – sort of like that, although I drew this one just for the sake of being

45
00:04:22,600 --> 00:04:28,530
honest, but it would look sort of like that. And now if I add regularization, it will want

46
00:04:28,530 --> 00:04:34,560
to make all of the betas small, so it’ll reduce a lot of the curviness, because the

47
00:04:34,560 --> 00:04:39,740
regularization won’t want it to pay attention to the data so much. So the regularization,

48
00:04:39,740 --> 00:04:43,820
it’ll try to get rid of the polynomials as much as it can while still paying some

49
00:04:43,820 --> 00:04:50,100
attention to the data… kind of like that. And this model, I would say, is much more

50
00:04:50,100 --> 00:04:56,720
likely to be able to generalize than the previous one, which was badly overfed.

51
00:04:56,720 --> 00:05:01,980
Okay so now that you know what regularization does, let me tell you about the difference

52
00:05:01,980 --> 00:05:13,220
between L1 and L2 regularization. Now L2 regularization tends intuitively to make all of the coefficients

53
00:05:13,220 --> 00:05:19,820
kind of smaller, and L1 regularization – it’s a bit different. That one’s useful for making

54
00:05:19,820 --> 00:05:27,840
sparse solutions, it sends a bunch of the coefficients to 0. Let me do this in two dimensions

55
00:05:27,840 --> 00:05:37,720
just to give you the kind of geometric intuition. Let us start with L2. You have to think geometrically

56
00:05:37,720 --> 00:05:44,190
for this to make sense. I’m going to make a plot of beta 1 and beta 2. Now remember,

57
00:05:44,190 --> 00:05:48,570
regularization keeps beta 1 and beta 2 small, so the regularization is going to try to suck

58
00:05:48,570 --> 00:05:54,990
those betas close to the origin of this plot. Remember that the regularization term is forcing

59
00:05:54,990 --> 00:06:00,530
this sum here to be small, so if it were up to the regularization, the betas would be

60
00:06:00,530 --> 00:06:05,200
as close to the origin as possible. Now let’s say that we have enough regularization in

61
00:06:05,200 --> 00:06:12,100
our minimization problem that the regularization term is small, and precisely let’s say that

62
00:06:12,100 --> 00:06:20,500
we have enough regularization that this sum is at most 5, okay? So that means the regularization’s

63
00:06:20,500 --> 00:06:25,240
trying to choose a point that’s on the smallest possible circle there because beta 1 plus

64
00:06:25,240 --> 00:06:32,380
beta 2 equals 5 is the circle. But the model also wants to be accurate, so let’s say

65
00:06:32,380 --> 00:06:37,430
that the most accurate model is actually that one out here with these coordinates right

66
00:06:37,430 --> 00:06:43,790
there. So this is the one that’s very overfed, because it only minimizes training error.

67
00:06:43,790 --> 00:06:49,980
And I’ll put in a bunch of choices for the regularization, so if you regularize a little

68
00:06:49,980 --> 00:06:55,590
bit, you won’t be able to get, you know, to the point over here, but you’ll be able

69
00:06:55,590 --> 00:07:02,690
to get, you know, maybe to the point over here. Okay, so if you have too much regularization

70
00:07:02,690 --> 00:07:06,960
you’ll be stuck in this inner circle and the model won’t be very accurate, right,

71
00:07:06,960 --> 00:07:12,040
that’ll underfit. You kind of want something in the middle here that won’t underfit or

72
00:07:12,040 --> 00:07:17,260
won’t overfit, you know, depending on the trade-off. So as you increase that regularization

73
00:07:17,260 --> 00:07:22,150
parameter, your solutions will go from the overfitted one down and then they’ll be

74
00:07:22,150 --> 00:07:27,060
good and then maybe they’ll underfit. Alright, so let’s switch over to giving

75
00:07:27,060 --> 00:07:33,520
you the geometric intuition about the L1 norm, about the L1 regularization. So you’ll have

76
00:07:33,520 --> 00:07:42,169
the same thing depending on the trade-off, but now when I say let’s look at – the

77
00:07:42,169 --> 00:07:47,010
regularization will suck all of the coefficients into the origin, it sucks it in this weird

78
00:07:47,010 --> 00:07:51,900
way where the shape looks like a diamond, right, if you write the absolute value of

79
00:07:51,900 --> 00:07:56,930
beta 1 plus the absolute value of beta 2 equals 5, that’s actually a diamond shape. It’s

80
00:07:56,930 --> 00:08:05,090
so interesting. And now, the point with the best training error is often, but not always,

81
00:08:05,090 --> 00:08:11,150
a point right at one of the corners here. And so what that means here is that beta 1

82
00:08:11,150 --> 00:08:17,770
is actually 0. And more generally in higher dimensions, it tends to prefer sets of coefficients

83
00:08:17,770 --> 00:08:23,650
for a lot of them are 0, so that the model is sparse, and it doesn’t have too many

84
00:08:23,650 --> 00:08:29,490
coefficients that you need to pay attention to. So if you set up the problem like this

85
00:08:29,490 --> 00:08:36,209
with the linear model here and the L1 regularization, it means you’ll get a solution to this problem

86
00:08:36,208 --> 00:08:41,559
that has a lot of the betas being 0, which is nice because then it automatically does

87
00:08:41,558 --> 00:08:48,480
feature selection for you. You only need to consider here beta 1 and beta 6, and whatever

88
00:08:48,480 --> 00:08:55,519
other few betas are not 0. So these are the two regularization terms that I showed you;

89
00:08:55,519 --> 00:09:01,420
this one – the L2 one is also called ridge regression when you add it in, add that regularization

90
00:09:01,420 --> 00:09:07,550
term in there, and then this term – the L1 term – is also called the lasso penalty.

91
00:09:07,550 --> 00:09:13,209
And by the way, regularization is often called shrinkage, and there’s a technical reason

92
00:09:13,209 --> 00:09:17,790
that it’s called that; but when you hear the word shrinkage, you should think of regularization

93
00:09:17,790 --> 00:09:23,069
which means doing something to the model in order to get it to be simpler somehow or shrinking

94
00:09:23,069 --> 00:09:25,339
it toward simpler solutions

