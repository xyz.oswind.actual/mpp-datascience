0
00:00:01,550 --> 00:00:06,730
>> Welcome back. So, we've looked at a number of ways to

1
00:00:06,730 --> 00:00:11,030
deal with overfitting of machine learning models.

2
00:00:11,030 --> 00:00:12,420
We looked at regularization.

3
00:00:12,420 --> 00:00:15,600
Specifically, we looked at L2 and L1 regularization,

4
00:00:15,600 --> 00:00:17,360
but there's other types of regularization

5
00:00:17,360 --> 00:00:18,630
that one can use.

6
00:00:18,630 --> 00:00:24,335
That works for a wide range of features.

7
00:00:24,335 --> 00:00:26,090
We looked at feature selection,

8
00:00:26,090 --> 00:00:29,435
where we specifically remove features that's

9
00:00:29,435 --> 00:00:33,630
probably limited to some manageable number of features.

10
00:00:33,630 --> 00:00:36,365
If you had 1,000, 10,000,

11
00:00:36,365 --> 00:00:39,895
100,000 of features, feature selection,

12
00:00:39,895 --> 00:00:43,225
first of, be computationally, insanely intensive.

13
00:00:43,225 --> 00:00:46,510
But also, has all sorts of statistical problem.

14
00:00:46,510 --> 00:00:48,315
So, probably it's a poor choice.

15
00:00:48,315 --> 00:00:49,940
So, what's another alternative?

16
00:00:49,940 --> 00:00:51,950
Well, another alternative is

17
00:00:51,950 --> 00:00:54,790
to do some sort of Dimensionality Reduction,

18
00:00:54,790 --> 00:00:57,710
because if you have a large number of

19
00:00:57,710 --> 00:01:04,335
features but they're not terribly informative, each one,

20
00:01:04,335 --> 00:01:08,635
you can find some sort of new coordinance space

21
00:01:08,635 --> 00:01:13,740
that you transform the original features

22
00:01:13,740 --> 00:01:14,900
into a coordinance space,

23
00:01:14,900 --> 00:01:17,310
where the projections are

24
00:01:17,310 --> 00:01:19,230
more informative of your label

25
00:01:19,230 --> 00:01:20,340
that you're trying to predict.

26
00:01:20,340 --> 00:01:23,220
So, the most commonly used of these kind

27
00:01:23,220 --> 00:01:26,375
of methods is called Principle Component Analysis.

28
00:01:26,375 --> 00:01:29,525
Effectively, it's a very simple method.

29
00:01:29,525 --> 00:01:36,090
Mathematically, through some linear transformations,

30
00:01:36,090 --> 00:01:38,755
find a new coordinance system to project.

31
00:01:38,755 --> 00:01:40,910
So, the first principle component

32
00:01:40,910 --> 00:01:43,750
explains the greatest amount of variance on the data.

33
00:01:43,750 --> 00:01:45,180
The second principle component,

34
00:01:45,180 --> 00:01:49,950
the second most of the variance et cetera.

35
00:01:49,950 --> 00:01:51,214
These orthogonal,

36
00:01:51,214 --> 00:01:53,500
it's an orthogonal new coordinance system.

37
00:01:53,500 --> 00:01:56,040
So, let's have a look at how we go about working with

38
00:01:56,040 --> 00:01:59,700
that in R. So,

39
00:01:59,700 --> 00:02:03,020
my screen here I'm setting up to do a simple example,

40
00:02:03,020 --> 00:02:04,990
just so you see how this method

41
00:02:04,990 --> 00:02:09,735
works using a two dimensional synthetic data.

42
00:02:09,735 --> 00:02:13,230
I'm going to create a covariance matrix,

43
00:02:13,230 --> 00:02:18,260
and it has elements 1.0, 0.6, 0.6, 1.0.

44
00:02:18,260 --> 00:02:19,710
So, it's going to have unit variance on

45
00:02:19,710 --> 00:02:21,270
the two dimensions and

46
00:02:21,270 --> 00:02:24,490
covariance between the two dimensions of 0.6,

47
00:02:24,490 --> 00:02:27,195
and we're going to have mean zero.

48
00:02:27,195 --> 00:02:28,980
So, this is unit variance or unit

49
00:02:28,980 --> 00:02:31,030
standard deviation and mean zero.

50
00:02:31,030 --> 00:02:33,305
So, we don't have to worry about scaling or anything.

51
00:02:33,305 --> 00:02:40,305
The mvrnorm function, create 100 multivariate normals,

52
00:02:40,305 --> 00:02:45,090
values or realizations using that mean and covariance.

53
00:02:45,090 --> 00:02:48,940
I'm going to create a data frame out of that.

54
00:02:48,940 --> 00:02:50,575
We'll just have a quick look.

55
00:02:50,575 --> 00:02:53,640
So, we have 100 samples from that

56
00:02:53,640 --> 00:02:57,610
bivariate normal with two columns,

57
00:02:57,610 --> 00:03:01,580
x and y, and I can print that.

58
00:03:03,350 --> 00:03:06,900
You see, indeed as I advertised,

59
00:03:06,900 --> 00:03:11,570
you have same variance on each x and y,

60
00:03:11,570 --> 00:03:14,985
but a strong covariance between those values.

61
00:03:14,985 --> 00:03:18,495
Okay? So, there's a number of

62
00:03:18,495 --> 00:03:20,160
principle component functions

63
00:03:20,160 --> 00:03:21,180
in R. We're going to stick with

64
00:03:21,180 --> 00:03:24,940
a really simple one which is called prcomp.

65
00:03:26,060 --> 00:03:30,935
I run that. What you get are standard deviations,

66
00:03:30,935 --> 00:03:35,825
and this matrix of two principal components,

67
00:03:35,825 --> 00:03:38,575
which are the rotations.

68
00:03:38,575 --> 00:03:40,190
So, as I said,

69
00:03:40,190 --> 00:03:43,670
we're trying to find a projection where we rotate and

70
00:03:43,670 --> 00:03:48,085
scale to a new coordinance system.

71
00:03:48,085 --> 00:03:52,500
These standard deviations aren't quite what I want.

72
00:03:52,500 --> 00:03:55,870
What I really want to know is variance explained.

73
00:03:55,870 --> 00:04:00,345
So, I can get variance by squaring the standard deviation

74
00:04:00,345 --> 00:04:02,750
and I can normalize it by

75
00:04:02,750 --> 00:04:05,660
summing the sum of those standard deviations.

76
00:04:05,660 --> 00:04:08,150
So, I'll get sdev_scaled.

77
00:04:08,150 --> 00:04:11,540
This makes sense because I've got

78
00:04:11,540 --> 00:04:16,560
80 percent of my variance explained by this projection,

79
00:04:16,560 --> 00:04:17,870
and 20 percent of

80
00:04:17,870 --> 00:04:21,320
my variance explained by this projection.

81
00:04:22,960 --> 00:04:26,155
So now, I re-scale

82
00:04:26,155 --> 00:04:30,065
those projections and create a data frame out of them.

83
00:04:30,065 --> 00:04:31,990
So, I just re-scale there.

84
00:04:31,990 --> 00:04:36,975
I can plot those scaled vectors

85
00:04:36,975 --> 00:04:39,420
on the original data here.

86
00:04:39,420 --> 00:04:42,600
You see, that the first principle component lies

87
00:04:42,600 --> 00:04:46,110
along this major axis of the ellipse of this data.

88
00:04:46,110 --> 00:04:48,060
That makes sense, right?

89
00:04:48,060 --> 00:04:51,470
That was the direction of greatest variance.

90
00:04:51,470 --> 00:04:54,045
So, that's what we want to rotate to make that.

91
00:04:54,045 --> 00:04:55,585
Then orthogonal to that,

92
00:04:55,585 --> 00:04:57,930
is this second direction.

93
00:04:57,930 --> 00:05:00,415
It only has 20 percent of the variance.

94
00:05:00,415 --> 00:05:03,385
It's about a quarter the length of this one.

95
00:05:03,385 --> 00:05:05,490
That's exactly what I wanted to see.

96
00:05:05,490 --> 00:05:07,570
So, that is in fact

97
00:05:07,570 --> 00:05:09,960
our new coordinance system

98
00:05:09,960 --> 00:05:11,000
that we're going to project to.

99
00:05:11,000 --> 00:05:13,065
So, if I go ahead and

100
00:05:13,065 --> 00:05:16,290
apply that projection just using a matrix,

101
00:05:16,290 --> 00:05:19,325
multiply to apply the rotatio,

102
00:05:19,325 --> 00:05:22,640
I get the scaled or

103
00:05:22,640 --> 00:05:25,875
re-scaled and rotated version of my data.

104
00:05:25,875 --> 00:05:29,170
So notice, this looks like just a blob,

105
00:05:29,170 --> 00:05:30,860
but it's actually on very different scales.

106
00:05:30,860 --> 00:05:33,525
The scale goes from say minus

107
00:05:33,525 --> 00:05:38,220
2.5 to just over a little of plus two.

108
00:05:38,220 --> 00:05:40,790
This vertical scale which

109
00:05:40,790 --> 00:05:43,235
is my second principle component direction

110
00:05:43,235 --> 00:05:48,390
goes from minus 0.3 to about plus 0.25.

111
00:05:48,390 --> 00:05:49,610
So, there's almost an order of

112
00:05:49,610 --> 00:05:52,165
magnitude difference in these two scales,

113
00:05:52,165 --> 00:05:53,475
as we would expect.

114
00:05:53,475 --> 00:05:57,525
So, that now are a set of features,

115
00:05:57,525 --> 00:05:59,410
where one feature explains a lot

116
00:05:59,410 --> 00:06:01,730
more the variance than the other.

117
00:06:01,730 --> 00:06:03,520
So, let's apply this to

118
00:06:03,520 --> 00:06:07,030
some real data and

119
00:06:07,030 --> 00:06:10,245
get a feel for a little harder problem.

120
00:06:10,245 --> 00:06:13,615
So, I'm going to use the German Credit data.

121
00:06:13,615 --> 00:06:17,090
I've removed things like the customer_ID.

122
00:06:17,090 --> 00:06:20,355
You want to make sure you remove before you start

123
00:06:20,355 --> 00:06:24,240
computing principal components or anything like that.

124
00:06:24,240 --> 00:06:26,980
Non-explanatory columns

125
00:06:26,980 --> 00:06:29,580
like customer_ID is not explanatory.

126
00:06:29,580 --> 00:06:32,610
So, I have all my features and I have my label,

127
00:06:32,610 --> 00:06:35,580
but notice that some of my features are factors.

128
00:06:35,580 --> 00:06:37,880
That's a problem, because principal components is

129
00:06:37,880 --> 00:06:40,090
a linear algebra method.

130
00:06:40,090 --> 00:06:42,380
It can't compute on string variables,

131
00:06:42,380 --> 00:06:43,730
it has to have numbers.

132
00:06:43,730 --> 00:06:47,335
So, I'm going to have to convert to dummy variables.

133
00:06:47,335 --> 00:06:50,420
So, each of these factors will get

134
00:06:50,420 --> 00:06:52,940
expanded into however many dummy variables

135
00:06:52,940 --> 00:06:54,840
it takes to define the labels.

136
00:06:54,840 --> 00:06:59,750
So, I'm going to scale and I'm going to partition,

137
00:06:59,750 --> 00:07:02,870
and create the dummy variables here.

138
00:07:02,870 --> 00:07:05,290
Okay. So, I used

139
00:07:05,290 --> 00:07:08,725
the createDataPartition function from carat,

140
00:07:08,725 --> 00:07:16,525
and I make my training data from that partition,

141
00:07:16,525 --> 00:07:18,620
and then I use the predict method on

142
00:07:18,620 --> 00:07:23,280
the dummies from here, it's the coding.

143
00:07:23,280 --> 00:07:24,690
The new data is training.

144
00:07:24,690 --> 00:07:27,430
So, I get my training data as dummies,

145
00:07:27,430 --> 00:07:32,275
and I do the same thing for my test data,

146
00:07:32,275 --> 00:07:34,710
for the test partition.

147
00:07:35,030 --> 00:07:41,780
You see, I now have 701 rows with

148
00:07:41,780 --> 00:07:44,600
61 columns including all the dummy variables

149
00:07:44,600 --> 00:07:47,640
for training and around 300 for test.

150
00:07:47,640 --> 00:07:52,070
Here's just a sample of these dummy variables.

151
00:07:52,070 --> 00:07:53,855
We've looked at these before,

152
00:07:53,855 --> 00:07:55,580
with the one hot uncoding,

153
00:07:55,580 --> 00:07:59,840
for example for checking account status.

154
00:08:02,940 --> 00:08:07,330
All right. So, one last thing I have to scale.

155
00:08:07,330 --> 00:08:10,500
Of course, always scale your numeric variables for

156
00:08:10,500 --> 00:08:12,045
any machine learning including

157
00:08:12,045 --> 00:08:14,560
principal components as you can imagine,

158
00:08:14,560 --> 00:08:17,075
because we're trying to find these rotations,

159
00:08:17,075 --> 00:08:22,275
they're super sensitive to scaling.

160
00:08:22,275 --> 00:08:24,530
So, make sure your scaling's right,

161
00:08:24,530 --> 00:08:26,250
make sure you've treated outliers and

162
00:08:26,250 --> 00:08:29,380
all that errors and outliers.

163
00:08:29,380 --> 00:08:32,295
So, from the preProcessed function,

164
00:08:32,295 --> 00:08:34,630
I'm going to center and scale as

165
00:08:34,630 --> 00:08:37,230
we always do and apply the predict,

166
00:08:37,230 --> 00:08:40,250
so I train on the training data,

167
00:08:40,250 --> 00:08:43,020
predict on the training data to get the scaled,

168
00:08:43,020 --> 00:08:47,715
and predict with the same scaler on the test data,

169
00:08:47,715 --> 00:08:51,540
and that's what we want.

170
00:08:51,540 --> 00:08:53,640
So now, I'm ready to

171
00:08:53,640 --> 00:08:56,175
actually compute the principal components here.

172
00:08:56,175 --> 00:09:01,480
So, I'll do prcomp on the training data.

173
00:09:01,710 --> 00:09:04,425
Okay. I'm going to rescale.

174
00:09:04,425 --> 00:09:07,060
So, I'm calling this variance

175
00:09:07,060 --> 00:09:09,935
explained or var under var_exp.

176
00:09:09,935 --> 00:09:12,150
So, it's just that same scaling.

177
00:09:12,150 --> 00:09:14,070
You see, the first principle component

178
00:09:14,070 --> 00:09:17,650
explains not quite 15 percent of my variance.

179
00:09:17,650 --> 00:09:19,845
The second one about 13 percent,

180
00:09:19,845 --> 00:09:24,040
then eight percent, seven percent, et cetera.

181
00:09:24,040 --> 00:09:26,430
So, the question is, how many of these components?

182
00:09:26,430 --> 00:09:28,230
The whole idea is Dimensionality Reduction.

183
00:09:28,230 --> 00:09:29,910
So, we have to decide

184
00:09:29,910 --> 00:09:33,545
which component we want to place a cut-off.

185
00:09:33,545 --> 00:09:36,495
So, there's something called the scree plot.

186
00:09:36,495 --> 00:09:38,430
Let me just make the plot, it's easier to

187
00:09:38,430 --> 00:09:41,070
explain looking at the plot

188
00:09:41,070 --> 00:09:43,710
than actually trying to use it,

189
00:09:43,710 --> 00:09:45,760
or actually trying to create it.

190
00:09:45,760 --> 00:09:49,825
So, you see, variance explained.

191
00:09:49,825 --> 00:09:53,645
So, we have component number this way from 1-60,

192
00:09:53,645 --> 00:09:56,335
61 I think or 60.

193
00:09:56,335 --> 00:10:00,840
You could see that the variance explain drops very

194
00:10:00,840 --> 00:10:04,690
fast with these first few components.

195
00:10:04,690 --> 00:10:08,380
So, by the time we get to about the 10th component,

196
00:10:08,380 --> 00:10:11,040
there's a bend in the curve,

197
00:10:11,040 --> 00:10:14,345
and it starts to flatten out.

198
00:10:14,345 --> 00:10:16,740
Then, you've got components here that effectively

199
00:10:16,740 --> 00:10:19,555
don't explain any variance, they're just noise.

200
00:10:19,555 --> 00:10:21,520
Probably, some of these are two.

201
00:10:21,520 --> 00:10:22,795
So, the question is,

202
00:10:22,795 --> 00:10:26,445
where do we cut this off?

203
00:10:26,445 --> 00:10:30,385
One rule as a starting point is to say,

204
00:10:30,385 --> 00:10:32,160
where this bend is,

205
00:10:32,160 --> 00:10:34,770
this knee and the curve is about 10 components?

206
00:10:34,770 --> 00:10:35,970
So, you might start there and then,

207
00:10:35,970 --> 00:10:40,345
add a few at a time to see how your model performs.

208
00:10:40,345 --> 00:10:42,360
Especially, make sure you do something like

209
00:10:42,360 --> 00:10:44,250
a cross validation or something like that

210
00:10:44,250 --> 00:10:46,200
to make sure you aren't just fooling

211
00:10:46,200 --> 00:10:48,515
yourself by a statistical fluke.

212
00:10:48,515 --> 00:10:51,735
But say it is 10 or even 20,

213
00:10:51,735 --> 00:10:56,235
we've reduced the number of features here by a factor,

214
00:10:56,235 --> 00:10:58,850
somewhere between three and six.

215
00:10:58,850 --> 00:11:04,184
But if we had thousands of features,

216
00:11:04,184 --> 00:11:06,185
we might be able to get a reduction of

217
00:11:06,185 --> 00:11:08,730
10-1 or something like that.

218
00:11:08,730 --> 00:11:10,680
Get a much more manageable number

219
00:11:10,680 --> 00:11:12,530
of features using Principle Components.

220
00:11:12,530 --> 00:11:14,520
So, Principle Components are

221
00:11:14,520 --> 00:11:17,960
very powerful and widely used.

222
00:11:17,960 --> 00:11:20,380
Dimensionality Reduction method, they're easy to work

223
00:11:20,380 --> 00:11:23,070
with an R. There is this little trick,

224
00:11:23,070 --> 00:11:25,989
where you have to decide where to put the cut-off,

225
00:11:25,989 --> 00:11:28,910
but like a lot of things in machine learning,

226
00:11:28,910 --> 00:11:30,650
it takes just a little experimentation

227
00:11:30,650 --> 00:11:32,610
to figure that one out.

