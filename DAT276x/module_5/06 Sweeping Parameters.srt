0
00:00:05,089 --> 00:00:10,780
Let’s have a really brief chat about sweeping model parameters. So let’s say that you’re

1
00:00:10,780 --> 00:00:14,410
running your machine learning method, and you don’t know what value to choose for

2
00:00:14,410 --> 00:00:20,000
C. so you want to try several different values. Well, once you have those several values,

3
00:00:20,000 --> 00:00:25,160
you could possibly use nested cross validation to choose which one you want to use for testing,

4
00:00:25,160 --> 00:00:30,510
but Azure ML actually has a couple of different capabilities to figure out how to tune C.

5
00:00:30,510 --> 00:00:36,850
So, if you tell Azure ML to sweep the entire grid, what it’ll do is it’ll just loop

6
00:00:36,850 --> 00:00:41,300
through a giant grid of parameters, and that might take a while but you’ll get the best

7
00:00:41,300 --> 00:00:47,700
parameters. It could also do a random sweep, so if you select random sweep, it chooses

8
00:00:47,700 --> 00:00:55,940
a random selection of parameter values to try out. It’s – and this is faster, but

9
00:00:55,940 --> 00:01:01,000
you’ll never know how far you are from the best. Luckily, usually performance is not

10
00:01:01,000 --> 00:01:07,810
too sensitive to the value of the parameter, so this is actually totally okay.

