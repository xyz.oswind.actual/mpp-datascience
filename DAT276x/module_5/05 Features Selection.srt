0
00:00:01,550 --> 00:00:04,800
>> Hi and welcome. So, we've talked about

1
00:00:04,800 --> 00:00:07,390
regularization as a way

2
00:00:07,390 --> 00:00:10,060
to prevent model overfitting, but another approach,

3
00:00:10,060 --> 00:00:12,345
another tool we have is

4
00:00:12,345 --> 00:00:15,960
feature selection where we try to identify features that

5
00:00:15,960 --> 00:00:18,090
for some reason we eliminate

6
00:00:18,090 --> 00:00:24,974
from our feature training set and mostly

7
00:00:24,974 --> 00:00:27,575
for two different reasons that we're going to look at.

8
00:00:27,575 --> 00:00:30,995
One is they may be very low variance features.

9
00:00:30,995 --> 00:00:34,460
Features with zero or near-zero variance

10
00:00:34,460 --> 00:00:36,590
just don't contain a lot of information,

11
00:00:36,590 --> 00:00:39,110
so it's usually best to prune those out before you

12
00:00:39,110 --> 00:00:42,310
get too far with training a model.

13
00:00:42,310 --> 00:00:46,780
Second, so we've removed

14
00:00:46,780 --> 00:00:50,915
these low variance or near-zero variance features,

15
00:00:50,915 --> 00:00:53,590
but we still need to know if we

16
00:00:53,590 --> 00:00:56,290
want to find some number of best features,

17
00:00:56,290 --> 00:00:58,310
how do we do that?

18
00:00:59,220 --> 00:01:02,410
There's various tools and

19
00:01:02,410 --> 00:01:06,820
our models have specific tools that are for that model.

20
00:01:06,820 --> 00:01:07,960
What I'm going to show you here is

21
00:01:07,960 --> 00:01:09,340
a very general approach that

22
00:01:09,340 --> 00:01:12,260
works with virtually any model.

23
00:01:14,810 --> 00:01:17,100
So, on my screen here,

24
00:01:17,100 --> 00:01:22,550
I have some code and I've already loaded this data set,

25
00:01:22,550 --> 00:01:27,035
I've split the data set,

26
00:01:27,035 --> 00:01:31,730
I've scaled the numeric features,

27
00:01:31,730 --> 00:01:34,270
and now I'm ready to split

28
00:01:34,270 --> 00:01:40,575
the categorical variables into dummy variables.

29
00:01:40,575 --> 00:01:45,945
I'm going to actually do this on the whole data set here,

30
00:01:45,945 --> 00:01:50,250
and the reason is we're going to use

31
00:01:50,250 --> 00:01:52,140
the whole data set to

32
00:01:52,140 --> 00:01:55,415
look for these low variance features.

33
00:01:55,415 --> 00:01:59,445
So, these are the dummy variables, for example,

34
00:01:59,445 --> 00:02:03,660
checking_account_status of less than zero,

35
00:02:03,660 --> 00:02:06,450
you see there's a couple of cases here, et cetera.

36
00:02:06,450 --> 00:02:08,120
So these are just to remind you

37
00:02:08,120 --> 00:02:12,550
the binary dummy variables or the one hot encoding

38
00:02:12,550 --> 00:02:15,560
our first case here has between 0 and

39
00:02:15,560 --> 00:02:19,520
200 in their checking_account whoever that customer is.

40
00:02:19,520 --> 00:02:21,910
Here's all the names.

41
00:02:21,910 --> 00:02:27,240
It's a lot of names because every category of

42
00:02:27,240 --> 00:02:29,580
every categorical variable has

43
00:02:29,580 --> 00:02:34,245
now a separate column in this data frame.

44
00:02:34,245 --> 00:02:38,970
So, from the current package,

45
00:02:38,970 --> 00:02:42,890
there's a nearZeroVar function,

46
00:02:42,890 --> 00:02:44,830
and so I give it these dummy variables.

47
00:02:44,830 --> 00:02:46,295
That's why I start with dummy variables

48
00:02:46,295 --> 00:02:47,730
because I want to find,

49
00:02:47,730 --> 00:02:49,970
at this level of granularity of

50
00:02:49,970 --> 00:02:52,090
the actual dummy variables,

51
00:02:52,090 --> 00:02:54,510
whether they have zero variance or not.

52
00:02:54,510 --> 00:02:56,410
Now, there's two ways we can do this.

53
00:02:56,410 --> 00:02:58,460
We can do this by actual variance or

54
00:02:58,460 --> 00:03:00,590
we can do it by something called the cut frequency

55
00:03:00,590 --> 00:03:02,390
which is the frequency of

56
00:03:02,390 --> 00:03:05,720
the most frequently encountered value

57
00:03:05,720 --> 00:03:09,310
over this next most frequently encountered value.

58
00:03:09,310 --> 00:03:14,540
So, I'm giving it this ratio 95 to 100 and it needs to

59
00:03:14,540 --> 00:03:17,285
have at least 10 unique

60
00:03:17,285 --> 00:03:20,530
of each value for the categorical cases.

61
00:03:20,530 --> 00:03:22,355
I want to save the metrics

62
00:03:22,355 --> 00:03:24,240
because I want to show you the metrics,

63
00:03:24,240 --> 00:03:30,185
and then I just apply it.

64
00:03:30,185 --> 00:03:37,435
So, I'm just going to create this data frame here.

65
00:03:37,435 --> 00:03:40,530
So, I'm doing a logical if zeroVar is

66
00:03:40,530 --> 00:03:44,685
true or if near_zeroVar is true,

67
00:03:44,685 --> 00:03:47,030
we're just going to select those.

68
00:03:47,030 --> 00:03:50,280
So, you'll see only cases where it's

69
00:03:50,280 --> 00:03:57,360
either zero variance or NZV as near-zero variance.

70
00:03:57,360 --> 00:04:00,495
So, I didn't want to print all those,

71
00:04:00,495 --> 00:04:05,460
I didn't want to print a case for each

72
00:04:05,460 --> 00:04:10,135
of these many, many dummy variables.

73
00:04:10,135 --> 00:04:12,060
You'll see that like

74
00:04:12,060 --> 00:04:15,540
credit_history.all loans paid at bank,

75
00:04:15,540 --> 00:04:17,915
they're all near-zero variance,

76
00:04:17,915 --> 00:04:21,090
there's no zero variance features in this data.

77
00:04:21,090 --> 00:04:23,610
But there's certain ones we can see,

78
00:04:23,610 --> 00:04:25,460
there's a problem like foreign_worker,

79
00:04:25,460 --> 00:04:27,040
so no and yes,

80
00:04:27,040 --> 00:04:29,110
they're both near-zero variance,

81
00:04:29,110 --> 00:04:31,975
so that really isn't very useful.

82
00:04:31,975 --> 00:04:34,320
We can also see purpose,

83
00:04:34,320 --> 00:04:36,990
many purposes of the loans

84
00:04:36,990 --> 00:04:40,195
that people or their customers are taking seem

85
00:04:40,195 --> 00:04:46,220
to be near-zero variance, and other_signatories.

86
00:04:46,220 --> 00:04:48,320
There's only other_signatories with

87
00:04:48,320 --> 00:04:50,030
a co-applicant or not.

88
00:04:50,030 --> 00:04:54,930
So, we're just going to remove those features from

89
00:04:54,930 --> 00:04:59,210
our data frame and we're going to create

90
00:04:59,210 --> 00:05:01,220
a new set of dummy variables without

91
00:05:01,220 --> 00:05:04,120
those original categorical features.

92
00:05:04,120 --> 00:05:07,160
So, no point in pushing ahead with

93
00:05:07,160 --> 00:05:10,280
further feature selection when we know they have

94
00:05:10,280 --> 00:05:13,685
low or near-zero variance.

95
00:05:13,685 --> 00:05:18,040
So that's again, dummies just as we saw before.

96
00:05:18,040 --> 00:05:22,100
So this is a little more complicated now.

97
00:05:24,170 --> 00:05:28,640
We're going to train a model,

98
00:05:28,640 --> 00:05:31,910
the X is going to be our credit_dummies,

99
00:05:31,910 --> 00:05:35,085
our Y is going to be a factor,

100
00:05:35,085 --> 00:05:40,290
so I've done as.factor

101
00:05:40,290 --> 00:05:44,485
on the bad_credit column of the credit data set,

102
00:05:44,485 --> 00:05:47,430
so that's my label.

103
00:05:47,430 --> 00:05:51,815
I'm using the generalized linear model,

104
00:05:51,815 --> 00:05:56,070
glmnet that we have looked at before as my method,

105
00:05:56,070 --> 00:05:59,985
so it requires matrices not data frames.

106
00:05:59,985 --> 00:06:03,965
Okay. The details of

107
00:06:03,965 --> 00:06:06,365
the cross-validation are discussed

108
00:06:06,365 --> 00:06:08,150
elsewhere in our lessons,

109
00:06:08,150 --> 00:06:09,910
so I'm not going to go into that too much.

110
00:06:09,910 --> 00:06:12,000
But basically, we're going to re-sample

111
00:06:12,000 --> 00:06:14,970
multiple times and try

112
00:06:14,970 --> 00:06:20,070
to come up with a model here,

113
00:06:20,070 --> 00:06:28,590
and this just gives us some optimal Alpha and Lambda.

114
00:06:28,590 --> 00:06:32,370
So, remember, Alpha is the trade-off between L1 and L2

115
00:06:32,370 --> 00:06:36,230
regularization and Lambda is the regularization parameter.

116
00:06:36,230 --> 00:06:37,820
So, it looks like we're more or less right in

117
00:06:37,820 --> 00:06:41,445
the middle where it's half L1, half L2.

118
00:06:41,445 --> 00:06:46,560
Then, I have this model, this cross-validated model.

119
00:06:47,020 --> 00:06:49,980
Using this varImp for

120
00:06:49,980 --> 00:06:53,050
variable importance function from char_at,

121
00:06:53,050 --> 00:06:56,660
I can actually find the variable importance.

122
00:06:56,660 --> 00:07:02,420
So, this is done by exhaustively re-sampling from

123
00:07:02,420 --> 00:07:05,030
this cross-validation each feature to

124
00:07:05,030 --> 00:07:09,600
see how it's like backward feature elimination.

125
00:07:09,600 --> 00:07:10,950
You start with all the features in

126
00:07:10,950 --> 00:07:13,660
the dummy variables and you remove one,

127
00:07:13,660 --> 00:07:17,335
and you see which one can you remove

128
00:07:17,335 --> 00:07:24,940
next with the least impact on your model metric.

129
00:07:26,330 --> 00:07:33,155
In this case, we scales this on a scale of 0 to 100.

130
00:07:33,155 --> 00:07:35,920
So you see checking_account_status.none

131
00:07:35,920 --> 00:07:38,080
turns out to be the most important feature,

132
00:07:38,080 --> 00:07:40,500
it's very hard to guess that.

133
00:07:40,500 --> 00:07:45,320
Purpose.car (used) turns out to be the second one.

134
00:07:45,320 --> 00:07:47,265
Not too surprisingly,

135
00:07:47,265 --> 00:07:51,050
critical account and other non bank loans

136
00:07:51,050 --> 00:07:52,910
turns out to be the third, et cetera.

137
00:07:52,910 --> 00:07:56,430
So you can see, this is relative importance.

138
00:07:56,430 --> 00:07:58,550
These numbers are just a ranking,

139
00:07:58,550 --> 00:08:01,490
they don't mean much quantitatively.

140
00:08:01,490 --> 00:08:03,100
We can also make a nice plot.

141
00:08:03,100 --> 00:08:04,640
So I'm just going to make a plot of

142
00:08:04,640 --> 00:08:09,970
the top 25 features that we might have here,

143
00:08:11,800 --> 00:08:15,980
and this just gives you an idea of how these rankings go.

144
00:08:15,980 --> 00:08:19,595
Checking_account_status.none, purpose used car,

145
00:08:19,595 --> 00:08:21,500
it's actually quite a drop.

146
00:08:21,500 --> 00:08:23,300
Then you start getting

147
00:08:23,300 --> 00:08:25,220
down to features that are less and less

148
00:08:25,220 --> 00:08:26,840
important like time_employed and

149
00:08:26,840 --> 00:08:29,300
years less than one year or

150
00:08:29,300 --> 00:08:34,795
purpose radio and television, et cetera.

151
00:08:34,795 --> 00:08:41,490
So, we can then do another thing.

152
00:08:41,490 --> 00:08:44,450
So, if we look at these values,

153
00:08:44,450 --> 00:08:48,015
we're going to set a threshold of four.

154
00:08:48,015 --> 00:08:50,000
So we say anything with a variable

155
00:08:50,000 --> 00:08:51,250
importance less than four,

156
00:08:51,250 --> 00:08:53,110
some of them get down towards zero.

157
00:08:53,110 --> 00:08:56,055
We're going to remove from that,

158
00:08:56,055 --> 00:08:58,715
and then we can just print the result.

159
00:08:58,715 --> 00:09:02,560
So, that gives us still a long list of

160
00:09:02,560 --> 00:09:07,050
these dummy variables that have a feature importance.

161
00:09:07,050 --> 00:09:08,835
So you see some of them are like

162
00:09:08,835 --> 00:09:14,760
property.unknown-none is pretty low, et cetera.

163
00:09:14,760 --> 00:09:18,490
So, this may still be too aggressive a threshold,

164
00:09:18,490 --> 00:09:19,810
maybe we want to move it

165
00:09:19,810 --> 00:09:24,155
up to eight or five or who knows.

166
00:09:24,155 --> 00:09:29,110
It's hard to know a priori what the best threshold is,

167
00:09:29,110 --> 00:09:31,395
you need to experiment with that.

168
00:09:31,395 --> 00:09:33,620
Then, I've created here,

169
00:09:33,620 --> 00:09:36,555
we're going to fit again a model.

170
00:09:36,555 --> 00:09:39,150
One thing I should point out about these models,

171
00:09:39,150 --> 00:09:48,180
we're using recall as our metric because remember

172
00:09:48,180 --> 00:09:53,170
that a false negative

173
00:09:53,170 --> 00:09:55,330
that is missing a bad credit customer

174
00:09:55,330 --> 00:09:58,380
cost the bank five times more than a false

175
00:09:58,380 --> 00:10:02,820
positive where we misclassified good customer as bad.

176
00:10:02,820 --> 00:10:05,190
So, recall is more sensitive to that,

177
00:10:05,190 --> 00:10:07,600
so I'm training this on recall.

178
00:10:07,600 --> 00:10:09,550
Again, I'm using cross-validation

179
00:10:09,550 --> 00:10:12,380
which will be covered elsewhere.

180
00:10:20,330 --> 00:10:22,525
We see we get

181
00:10:22,525 --> 00:10:24,970
pretty respectable values of

182
00:10:24,970 --> 00:10:27,040
recall for some of these models.

183
00:10:27,040 --> 00:10:30,170
In this case, notice the Alpha has changed and

184
00:10:30,170 --> 00:10:31,255
the Lambda has changed

185
00:10:31,255 --> 00:10:33,980
a lot because we've changed the feature set.

186
00:10:33,980 --> 00:10:36,640
So, when we're doing this kind of

187
00:10:36,640 --> 00:10:40,665
model selection for the best features,

188
00:10:40,665 --> 00:10:44,690
we can expect significantly different behavior

189
00:10:44,690 --> 00:10:47,380
of the model in many ways as you eliminate features,

190
00:10:47,380 --> 00:10:49,850
especially as you eliminate low value features.

191
00:10:49,850 --> 00:10:53,530
So that's an overview of two widely applied

192
00:10:53,530 --> 00:10:55,480
and widely applicable because they're are

193
00:10:55,480 --> 00:10:57,690
very general feature selection methods.

194
00:10:57,690 --> 00:10:59,380
One is to look for zero variance and

195
00:10:59,380 --> 00:11:03,165
low variance features that don't have much information,

196
00:11:03,165 --> 00:11:04,480
the other is to do

197
00:11:04,480 --> 00:11:09,320
this recursive feature pruning

198
00:11:09,320 --> 00:11:13,770
basically to come up with a best feature set.

