0
00:00:05,230 --> 00:00:10,250
In this section, we’ll talk about important ways to improve machine learning models. So

1
00:00:10,250 --> 00:00:15,520
here’s the outline of the model – we’ll talk about feature selection and regularization,

2
00:00:15,520 --> 00:00:19,160
and then afterward we’ll talk about how to interpret features, sweeping model parameters,

3
00:00:19,160 --> 00:00:22,560
and cross-validation. So let us start with feature selection.

4
00:00:22,560 --> 00:00:26,849
Now I hope you recall the principle of Ockham’s razor, which is that the best models are simple

5
00:00:26,849 --> 00:00:32,490
models that fit the data well, and because if the models are not simple, there’s a

6
00:00:32,490 --> 00:00:37,910
good chance that they won’t generalize well with the new data. So we need some kind of

7
00:00:37,910 --> 00:00:43,730
balance between the accuracy of the models and their simplicity in order to thwart the

8
00:00:43,730 --> 00:00:47,879
curse of dimensionality. Now let’s go back to our running example where each manhole

9
00:00:47,879 --> 00:00:53,620
is represented by a vector of numbers. Do we really need all of these numbers to predict

10
00:00:53,620 --> 00:00:58,469
whether the manhole is going to explode? Maybe we can create a simpler model with much fewer

11
00:00:58,469 --> 00:01:03,480
numbers, and that would be helpful for many reasons. And first of all, simpler models

12
00:01:03,480 --> 00:01:09,110
tend to predict better, as we know from Ockham’s razor. Simpler models are more interpretable

13
00:01:09,110 --> 00:01:14,890
to humans. Also, simpler models are easier to make predictions from; there’s much fewer

14
00:01:14,890 --> 00:01:20,940
calculations involved so it’s much more computationally easier. However, I’m selecting

15
00:01:20,940 --> 00:01:26,200
the optimal combination of features as computationally hard, and I put that in blue because it makes

16
00:01:26,200 --> 00:01:31,320
people blue to hear that. So myself in my own research, I’m happy to do that computation.

17
00:01:31,320 --> 00:01:36,290
We have ways in our lab to do optimal or close to optimal feature selection but those are

18
00:01:36,290 --> 00:01:40,460
computationally intensive methods and they’re not the first thing I want to teach you guys.

19
00:01:40,460 --> 00:01:44,090
So let me show you some of the standard ways that people fiddle with the features to try

20
00:01:44,090 --> 00:01:47,890
to reduce the number of them that you’re going to use.

21
00:01:47,890 --> 00:01:55,820
So the first method is called Greedy Backwards selection. So you start with all features,

22
00:01:55,820 --> 00:02:03,410
and then you find the feature that hurts predictive power the least when removed, and you simply

23
00:02:03,410 --> 00:02:11,250
remove it. You just keep going, and you stop when some criterion is met. Now it’s called

24
00:02:11,250 --> 00:02:15,969
greedy because once you remove something you never look back. You could easily remove an

25
00:02:15,969 --> 00:02:20,769
important feature early on and then there’s nothing you can do to get it back. But it

26
00:02:20,769 --> 00:02:26,079
is computationally easy and here you want to stop removing features when it’s actually

27
00:02:26,079 --> 00:02:32,650
starting to hurt you – your accuracy, to keep removing them. Okay so you start here

28
00:02:32,650 --> 00:02:37,379
with all the features, and then you remove one that isn’t very important for predicting

29
00:02:37,379 --> 00:02:43,230
the label, and then you keep doing that until you start losing lots of accuracy and then

30
00:02:43,230 --> 00:02:48,950
you stop, and that’s how it works. The Greedy Forward selection is a different

31
00:02:48,950 --> 00:02:54,790
method, it’s sort of like the converse of Greedy Backwards selection. You start with

32
00:02:54,790 --> 00:03:03,049
no features this time, and then you find the feature that, acting by itself, is the best

33
00:03:03,049 --> 00:03:10,810
model. And then you keep that feature and then you add another feature so that the combination

34
00:03:10,810 --> 00:03:17,780
of the two is the best, and you keep those two and then you add another one, and you

35
00:03:17,780 --> 00:03:23,719
keep going and you stop when some criterion is met. So it’s called greedy, again, because

36
00:03:23,719 --> 00:03:30,469
you never look back. But what if you chose to include a feature near the very beginning

37
00:03:30,469 --> 00:03:35,950
of this procedure that in combination wasn’t really that useful? You can’t get rid of

38
00:03:35,950 --> 00:03:41,819
it, you’re stuck with it. On the other hand, it’s definitely computationally easier because

39
00:03:41,819 --> 00:03:48,799
you never look back. And so usually you stop here when predictive power stops increasing

40
00:03:48,799 --> 00:03:54,099
very much, and you get diminishing returns for each feature that you add in. Okay, so

41
00:03:54,099 --> 00:04:03,409
you start from nothing, and then you add in one feature at a time, like that. And you

42
00:04:03,409 --> 00:04:06,959
never end up looking at the rest of the features, right, you realize that you’ve reached a

43
00:04:06,959 --> 00:04:10,540
point of diminishing returns at some point and that’s where you’re stuck. Any feature

44
00:04:10,540 --> 00:04:14,409
you choose to add would make the model more complex but it wouldn’t really get you very

45
00:04:14,409 --> 00:04:19,209
much more accuracy and that’s where you stop. Now in order to do this method, you

46
00:04:19,209 --> 00:04:25,190
have to choose two things, and one of them is the criteria you’re going to use to choose

47
00:04:25,190 --> 00:04:29,810
which feature to add next. Now the simplest way to figure out which feature to add is

48
00:04:29,810 --> 00:04:34,200
to choose one that gives the largest boost in accuracy; I’ll talk more about that in

49
00:04:34,200 --> 00:04:40,450
a few minutes. And the other criteria is when to stop, and again, that’s when you’ve

50
00:04:40,450 --> 00:04:47,640
reached the point of diminishing returns. How do you know when to stop? Or how do you

51
00:04:47,640 --> 00:04:51,670
know when you’ve reached that point of diminishing returns? And that’s where something like

52
00:04:51,670 --> 00:04:56,980
adjusted R squared comes in. I hope you remember R squared from the data set course; R squared

53
00:04:56,980 --> 00:05:02,970
is a measure of how well the model fits the data. So if the model and the data are always

54
00:05:02,970 --> 00:05:10,350
close together, then y and f are close, their difference is small and then then this whole

55
00:05:10,350 --> 00:05:15,370
thing is small and this whole thing is close to 0, and then R squared is then close to

56
00:05:15,370 --> 00:05:20,880
1. Okay, so R squared measures goodness of fit; and then the denominator here doesn’t

57
00:05:20,880 --> 00:05:24,440
depend on the model, it’s just a property of the data, so it has no opinion about the

58
00:05:24,440 --> 00:05:33,480
model. Now, adjusted R squared has the R squared in it, but it has also a penalty for how many

59
00:05:33,480 --> 00:05:38,060
terms are in the model. So if there’s p terms in the model, this is the penalty. So

60
00:05:38,060 --> 00:05:44,500
this thing doesn’t just like accurate models, it also likes sparse models. Let’s discuss

61
00:05:44,500 --> 00:05:49,860
this a little bit further. Let’s say that you have a ton of terms in

62
00:05:49,860 --> 00:05:56,500
the model, like a ton of them. Then p is really really large, and then I’m claiming that

63
00:05:56,500 --> 00:06:03,830
the adjusted R squared will not like that. Okay, so if p is big, what happens? Then this

64
00:06:03,830 --> 00:06:09,740
ratio is big and then you’re subtracting something that’s big and so this whole thing

65
00:06:09,740 --> 00:06:14,770
is – so this is small, okay? Now that’s cool, because it says if you have too many

66
00:06:14,770 --> 00:06:20,490
variables, your adjusted R squared is small, it’s not good even if your R squared is

67
00:06:20,490 --> 00:06:27,970
good. So what if p is small? Well in that case, you’re only subtracting something

68
00:06:27,970 --> 00:06:34,150
small from R squared, so that’s good; but then since you only used a few variables,

69
00:06:34,150 --> 00:06:38,310
then your R squared in the first place might be terrible, so again there’s a trade-off.

70
00:06:38,310 --> 00:06:44,840
And what you want is something that’s just right, right, you want enough variables to

71
00:06:44,840 --> 00:06:49,770
have your R squared be large, but not so many that it’s going to bring your adjusted R

72
00:06:49,770 --> 00:06:56,870
squared down. Okay so let’s plot what happens when we do forward selection. I’m going

73
00:06:56,870 --> 00:07:02,720
to plot what I think will happen to adjusted R squared, when we do this greedy forward

74
00:07:02,720 --> 00:07:07,030
selection. The model’s going to start with one feature, and then the adjusted R squared

75
00:07:07,030 --> 00:07:12,940
is going to go up as the gains in R squared are better than the loss for having more features

76
00:07:12,940 --> 00:07:18,440
in the model, and then it goes up and just at the top when you think everything is going

77
00:07:18,440 --> 00:07:21,800
well, you’re at too many features and then the gains in R squared are not as good as

78
00:07:21,800 --> 00:07:26,070
the hit you took for adding more features to – so that the adjusted R squared goes

79
00:07:26,070 --> 00:07:33,340
down. But you found out where the sweet spot was; its right here. And now you can use just

80
00:07:33,340 --> 00:07:39,460
that many features, so R squared here told you when to stop with the forward greedy selection

81
00:07:39,460 --> 00:07:41,840
scheme. So that’s the idea.

