0
00:00:01,340 --> 00:00:05,160
>> Welcome back. In this video

1
00:00:05,160 --> 00:00:07,440
I'm going to show you how to implement

2
00:00:07,440 --> 00:00:10,350
cross-validation and nested cross-validation with

3
00:00:10,350 --> 00:00:13,440
R. Those are two very powerful techniques for

4
00:00:13,440 --> 00:00:15,210
not only model evaluation but

5
00:00:15,210 --> 00:00:16,860
what we call model selection where you're

6
00:00:16,860 --> 00:00:20,070
trying to compare models in an honest way and find

7
00:00:20,070 --> 00:00:22,050
out whether a model is really

8
00:00:22,050 --> 00:00:24,430
significantly better than another model or not.

9
00:00:24,430 --> 00:00:27,090
Model selection can be

10
00:00:27,090 --> 00:00:28,770
completely different types of

11
00:00:28,770 --> 00:00:30,975
machine learning models or it can just simply be

12
00:00:30,975 --> 00:00:33,600
changing a hyper parameter or adding or removing

13
00:00:33,600 --> 00:00:35,400
a feature that still creates

14
00:00:35,400 --> 00:00:38,185
a different model that you should do model selection on.

15
00:00:38,185 --> 00:00:42,600
If you just take one run of that model and compare to

16
00:00:42,600 --> 00:00:45,030
another one run there could be all sorts of

17
00:00:45,030 --> 00:00:47,550
statistical flukes in the training and test data.

18
00:00:47,550 --> 00:00:49,980
So, in cross-validation we use

19
00:00:49,980 --> 00:00:53,055
multiple folds where we

20
00:00:53,055 --> 00:00:56,440
retrain and retest the model several times.

21
00:00:56,440 --> 00:00:59,130
So, let's have a look at how that might work.

22
00:00:59,130 --> 00:01:00,900
So to start with,

23
00:01:00,900 --> 00:01:03,025
I'm just going to create

24
00:01:03,025 --> 00:01:07,180
a simple logistic regression model

25
00:01:07,180 --> 00:01:10,840
here using the glm model from R.

26
00:01:10,840 --> 00:01:16,330
You can see I'm going to model bad credit based on

27
00:01:16,330 --> 00:01:20,570
some features there and

28
00:01:20,570 --> 00:01:22,650
we're going to use some weights to account,

29
00:01:22,650 --> 00:01:24,060
to try to overcome

30
00:01:24,060 --> 00:01:31,070
the feature imbalance and

31
00:01:31,070 --> 00:01:33,035
we'll just go ahead and run this.

32
00:01:33,035 --> 00:01:35,650
We've looked at all these things before

33
00:01:35,650 --> 00:01:41,650
and there's our score and our probabilities,

34
00:01:41,650 --> 00:01:44,440
and this code should be familiar by now,

35
00:01:44,440 --> 00:01:47,245
it's just computing like the confusion matrix.

36
00:01:47,245 --> 00:01:51,760
Some metrics and the rocky you see.

37
00:01:51,760 --> 00:01:54,105
So, we'll go ahead and run that,

38
00:01:54,105 --> 00:01:57,600
and you see we're doing reasonably well with this model.

39
00:01:57,600 --> 00:02:01,490
We've got 53 positive cases correctly

40
00:02:01,490 --> 00:02:06,410
identified versus 27 incorrectly identified.

41
00:02:06,410 --> 00:02:10,110
But at a fairly high false positive rate

42
00:02:10,110 --> 00:02:12,285
but we're less concerned with that because

43
00:02:12,285 --> 00:02:15,410
this false negative cases

44
00:02:15,410 --> 00:02:18,360
cost the bank five times more than these.

45
00:02:18,360 --> 00:02:21,290
Our accuracy again, as we know because of

46
00:02:21,290 --> 00:02:22,730
the imbalance isn't that

47
00:02:22,730 --> 00:02:25,030
meaningful but it's fairly respectable.

48
00:02:25,030 --> 00:02:28,500
See, recall is a reasonable number here,

49
00:02:28,500 --> 00:02:30,380
and here's our rock curve in

50
00:02:30,380 --> 00:02:32,355
a ray you see but the real question is I

51
00:02:32,355 --> 00:02:36,810
did this just based on one split of the data.

52
00:02:36,810 --> 00:02:39,280
Well, how representative is that?

53
00:02:39,280 --> 00:02:41,940
Is this actually meaningful at all?

54
00:02:41,940 --> 00:02:45,210
The answer is, it's a bit questionable.

55
00:02:45,210 --> 00:02:47,000
So, we're going to do something

56
00:02:47,000 --> 00:02:50,850
now called cross-validation to

57
00:02:50,850 --> 00:02:57,280
go through and do this multiple times.

58
00:02:57,280 --> 00:02:58,370
So we're going to do it actually,

59
00:02:58,370 --> 00:03:01,685
we're going to do a 10 fold cross-validation.

60
00:03:01,685 --> 00:03:03,930
There's quite a lot of code here that I've created

61
00:03:03,930 --> 00:03:06,500
to do a simple cross-validation.

62
00:03:06,500 --> 00:03:08,310
You'll be able to study this on your

63
00:03:08,310 --> 00:03:10,105
own when you do the lab.

64
00:03:10,105 --> 00:03:12,525
But I'll just call your attention

65
00:03:12,525 --> 00:03:16,165
to this function here, create_folds.

66
00:03:16,165 --> 00:03:20,205
What it does is it goes through

67
00:03:20,205 --> 00:03:25,770
and just for however many folds

68
00:03:25,770 --> 00:03:30,150
I'm defining it creates an index for those folds.

69
00:03:30,150 --> 00:03:31,990
So say I have 10 folds,

70
00:03:31,990 --> 00:03:34,260
that index will be 1-10.

71
00:03:34,260 --> 00:03:37,650
So, I know which data to select as my test and training

72
00:03:37,650 --> 00:03:41,810
set for each fold of my cross-validation.

73
00:03:41,810 --> 00:03:44,670
So, I just create a new index there.

74
00:03:44,780 --> 00:03:47,830
I have a fit model function I'm going to call

75
00:03:47,830 --> 00:03:49,360
because I'm going to be fitting the model

76
00:03:49,360 --> 00:03:50,450
once for each fold.

77
00:03:50,450 --> 00:03:52,570
So, if I do 10-fold cross-validation,

78
00:03:52,570 --> 00:03:56,335
I'll fit it 10 times so I have got glm here,

79
00:03:56,335 --> 00:03:58,540
five or I create

80
00:03:58,540 --> 00:04:03,200
this function that runs

81
00:04:03,200 --> 00:04:05,910
the model, computes the probabilities,

82
00:04:05,910 --> 00:04:08,040
computes the score,

83
00:04:11,520 --> 00:04:15,740
and then here's the actual cross-validation function.

84
00:04:15,740 --> 00:04:17,510
So as I said, I call

85
00:04:17,510 --> 00:04:20,660
that function that computes the folds,

86
00:04:20,660 --> 00:04:24,560
I do a shuffle here and the reason

87
00:04:24,560 --> 00:04:26,540
I do a shuffle is my folds I'm

88
00:04:26,540 --> 00:04:28,595
just creating them as a lead in your list.

89
00:04:28,595 --> 00:04:31,070
They go, say I had 1,000

90
00:04:31,070 --> 00:04:32,600
cases and I'm going to do

91
00:04:32,600 --> 00:04:34,900
10-fold the first 100 will be marked one,

92
00:04:34,900 --> 00:04:36,990
the next 100 will be marked two etc.

93
00:04:36,990 --> 00:04:38,990
I want to make sure there's no order to

94
00:04:38,990 --> 00:04:41,835
the data that's going to cause

95
00:04:41,835 --> 00:04:45,640
any statistical bias because

96
00:04:45,640 --> 00:04:49,050
cross-validation assumes random sampling.

97
00:04:49,050 --> 00:04:51,155
So by doing this random shuffling

98
00:04:51,155 --> 00:04:53,730
on the data I've ensured that.

99
00:04:53,730 --> 00:04:56,825
So I loop over the folds, I do the fit,

100
00:04:56,825 --> 00:05:00,770
I get my metrics here and I'm

101
00:05:00,770 --> 00:05:04,590
also going to print some summary statistics.

102
00:05:04,590 --> 00:05:05,920
So, let me just run that,

103
00:05:05,920 --> 00:05:07,425
it's much more interesting to run it

104
00:05:07,425 --> 00:05:09,490
than to talk about the code.

105
00:05:09,490 --> 00:05:11,990
So I've got 10 folds,

106
00:05:11,990 --> 00:05:13,430
and this is what I was trying to get to

107
00:05:13,430 --> 00:05:15,650
why I created all that code.

108
00:05:15,650 --> 00:05:17,510
For the first fold you can see

109
00:05:17,510 --> 00:05:21,595
accuracy precision recall F1, AUC.

110
00:05:21,595 --> 00:05:24,435
But look at the second fold,

111
00:05:24,435 --> 00:05:27,545
the accuracy is lower

112
00:05:27,545 --> 00:05:30,380
but the precision is actually higher.

113
00:05:30,380 --> 00:05:32,120
The recall is way

114
00:05:32,120 --> 00:05:34,520
lower so you look at the difference there.

115
00:05:34,520 --> 00:05:37,085
The F1 is a little lower,

116
00:05:37,085 --> 00:05:38,850
the AUC is lower, etc.

117
00:05:38,850 --> 00:05:41,705
So, you see they all jump around.

118
00:05:41,705 --> 00:05:46,575
So, this row here I show the mean values.

119
00:05:46,575 --> 00:05:49,350
So, that's probably more representative

120
00:05:49,350 --> 00:05:50,625
than any one of these tests.

121
00:05:50,625 --> 00:05:52,910
I just want you to take in the fact that

122
00:05:52,910 --> 00:05:55,690
there's so much variation from fold to fold

123
00:05:55,690 --> 00:06:01,155
and say precision or accuracy or any of these statistics.

124
00:06:01,155 --> 00:06:04,320
Then here's the standard deviation of those numbers,

125
00:06:04,320 --> 00:06:07,470
and you see some of them are actually more

126
00:06:07,470 --> 00:06:11,970
than greater than an order

127
00:06:11,970 --> 00:06:15,050
of magnitude less than the summary statistics.

128
00:06:15,050 --> 00:06:16,630
So, there's a fair variability

129
00:06:16,630 --> 00:06:19,290
in this 10 fold cross-validation.

130
00:06:19,290 --> 00:06:21,750
But at least we have some confidence that

131
00:06:21,750 --> 00:06:24,750
these numbers are much more representative of

132
00:06:24,750 --> 00:06:26,970
what we'll see in production than

133
00:06:26,970 --> 00:06:29,580
that initial test where I just did

134
00:06:29,580 --> 00:06:33,825
one training and one test data split.

135
00:06:33,825 --> 00:06:37,190
So, now we can do nested cross-validation,

136
00:06:37,190 --> 00:06:39,370
so that was simple cross-validation,

137
00:06:39,370 --> 00:06:42,190
it's a performance evaluation method but

138
00:06:42,190 --> 00:06:47,315
nested cross-validation allows me to optimize,

139
00:06:47,315 --> 00:06:50,620
do model selection in various ways I could be doing

140
00:06:50,620 --> 00:06:54,210
feature selection or different models.

141
00:06:54,210 --> 00:06:56,350
But in this case, I'm going to optimize,

142
00:06:56,350 --> 00:07:00,140
I'm going to do what's called sweeping hyper parameters.

143
00:07:00,960 --> 00:07:04,885
To do this I don't need so much code because

144
00:07:04,885 --> 00:07:08,540
the caret package provides me

145
00:07:08,540 --> 00:07:12,405
with a really nice set of cross-validation tools.

146
00:07:12,405 --> 00:07:17,990
So, it does demand because I'm using.

147
00:07:17,990 --> 00:07:20,320
"glmnet" it does require

148
00:07:20,320 --> 00:07:26,205
this factor that may label be a factor.

149
00:07:26,205 --> 00:07:28,350
So, first thing I need to create

150
00:07:28,350 --> 00:07:32,605
a train control object which I'm calling fit control,

151
00:07:32,605 --> 00:07:36,030
and it just says what cross-validation method I'm using.

152
00:07:36,030 --> 00:07:40,050
I'm using repeated cross-validation tenfold,

153
00:07:40,050 --> 00:07:41,435
and I'm going to repeat five times.

154
00:07:41,435 --> 00:07:43,600
So, that's just what it sounds like.

155
00:07:43,600 --> 00:07:46,570
Tenfold cross-validation repeated five times

156
00:07:46,570 --> 00:07:51,590
across a sweep of model hyperparameters.

157
00:07:51,590 --> 00:07:53,435
Actually, the other nice thing

158
00:07:53,435 --> 00:07:55,065
about this is, charat will take.

159
00:07:55,065 --> 00:07:58,845
You can specify a grid to search over or charat will

160
00:07:58,845 --> 00:08:00,460
figure out a grid of

161
00:08:00,460 --> 00:08:03,790
some reasonable number of hyperparameters to try.

162
00:08:03,790 --> 00:08:06,270
So, you can be a little lazy sometimes.

163
00:08:06,270 --> 00:08:09,550
This is just my model specification,

164
00:08:09,550 --> 00:08:13,000
and the data that I'm going

165
00:08:13,000 --> 00:08:19,330
to resample from my model method is glmnet,

166
00:08:19,330 --> 00:08:21,580
as I said, and fit control tells me about

167
00:08:21,580 --> 00:08:23,480
how I'm going to do my cross-validation.

168
00:08:23,480 --> 00:08:25,320
So, let me run that.

169
00:08:34,600 --> 00:08:38,290
So, you see what we've got here

170
00:08:38,290 --> 00:08:42,940
is there's two hyperparameters for glmnet basically,

171
00:08:42,940 --> 00:08:44,440
the alpha and the lambda.

172
00:08:44,440 --> 00:08:49,280
You see different values and you see accuracy and kappa.

173
00:08:50,010 --> 00:08:52,620
It's determined based on

174
00:08:52,620 --> 00:08:56,480
basically accuracy that the optimal alpha is 0.1,

175
00:08:56,480 --> 00:09:00,790
and the optimal lambda is 0.000, essentially 0.003,

176
00:09:00,790 --> 00:09:03,610
it's a strange rounding thing they did here.

177
00:09:03,610 --> 00:09:07,970
But really, we know that accuracy isn't that good for

178
00:09:07,970 --> 00:09:09,380
this model so let me show you

179
00:09:09,380 --> 00:09:12,590
a somewhat more complicated case here.

180
00:09:12,590 --> 00:09:14,750
So I create something called the recall

181
00:09:14,750 --> 00:09:18,745
summary function which, well,

182
00:09:18,745 --> 00:09:21,775
without getting to too many details it's going to compute

183
00:09:21,775 --> 00:09:26,030
recall based on the observed data and the predicted,

184
00:09:26,030 --> 00:09:28,485
and I give it this name recall,

185
00:09:28,485 --> 00:09:30,380
and then in my train control method,

186
00:09:30,380 --> 00:09:33,670
I have to say that I have a summary function

187
00:09:33,670 --> 00:09:38,620
which is this recall summary, and down here,

188
00:09:38,620 --> 00:09:41,220
I have to say the metric is

189
00:09:41,220 --> 00:09:44,335
recall which is the name I gave it up here,

190
00:09:44,335 --> 00:09:48,095
and everything else is the same,

191
00:09:48,095 --> 00:09:53,260
but I'm allowed to use a different metric,

192
00:09:53,260 --> 00:09:55,510
that's the important point.

193
00:09:58,650 --> 00:10:01,470
All right. So, you see my

194
00:10:01,470 --> 00:10:03,735
metric that I specified here and you see

195
00:10:03,735 --> 00:10:08,745
we've got pretty high values of recall for some of these,

196
00:10:08,745 --> 00:10:12,575
and we find that alpha equals one, lambda equals 0.1,

197
00:10:12,575 --> 00:10:15,180
so there's a very different regularization parameters

198
00:10:15,180 --> 00:10:17,015
than what we had before.

199
00:10:17,015 --> 00:10:19,180
Now I can plot,

200
00:10:19,180 --> 00:10:24,120
I can make a nice plot here of these cross-validation.

201
00:10:25,120 --> 00:10:29,520
So, what is this plot?

202
00:10:29,520 --> 00:10:32,560
So, it's the regularization parameter,

203
00:10:32,560 --> 00:10:34,495
and the mixing percentage.

204
00:10:34,495 --> 00:10:38,960
So, that's our alpha and that's lambda,

205
00:10:38,960 --> 00:10:43,310
and you can see this is a scale from the magenta

206
00:10:43,310 --> 00:10:47,695
to the green of our recall,

207
00:10:47,695 --> 00:10:50,800
and so we want the darkest green possible,

208
00:10:50,800 --> 00:10:53,160
and you see that is a regularization parameter of

209
00:10:53,160 --> 00:10:57,650
0.1 and alpha of one.

210
00:10:57,650 --> 00:10:59,970
But you can see it's not that

211
00:10:59,970 --> 00:11:03,175
different from it's neighbors

212
00:11:03,175 --> 00:11:05,430
especially these lower neighbors here.

213
00:11:05,430 --> 00:11:08,980
So, and that's typical of cross-validation results.

214
00:11:08,980 --> 00:11:10,990
You don't often see,

215
00:11:10,990 --> 00:11:13,070
and that's why a very coarse scale

216
00:11:13,070 --> 00:11:15,790
often works just fine for finding hyperparameters.

217
00:11:15,790 --> 00:11:17,290
These models aren't generally that

218
00:11:17,290 --> 00:11:20,140
sensitive to small changes and hyperparameters.

219
00:11:20,140 --> 00:11:22,145
Quite fortunately because if they were,

220
00:11:22,145 --> 00:11:25,660
machine learning would really be a tedious task.

221
00:11:25,660 --> 00:11:29,970
So, we can go back then and use

222
00:11:29,970 --> 00:11:32,500
those optimal parameters and do

223
00:11:32,500 --> 00:11:37,990
another cross-validation and see what happens.

224
00:11:45,690 --> 00:11:49,339
Notice that our average recall,

225
00:11:49,339 --> 00:11:53,460
this is the same as the single fold or the non nest.

226
00:11:53,460 --> 00:11:54,980
So, this is the outer loop

227
00:11:54,980 --> 00:11:56,470
of our nested cross-validation,

228
00:11:56,470 --> 00:11:59,700
which is almost the same process

229
00:11:59,700 --> 00:12:03,040
as that first ordinary cross-validation we did.

230
00:12:03,040 --> 00:12:05,195
So, the nested cross validation, the nested part,

231
00:12:05,195 --> 00:12:08,410
the inner loop is to find the optimal hyperparameters,

232
00:12:08,410 --> 00:12:10,490
the outer loop is to

233
00:12:10,490 --> 00:12:13,570
evaluate the best model you get out of the inner loop.

234
00:12:13,570 --> 00:12:15,220
So, you see that in fact

235
00:12:15,220 --> 00:12:17,020
the recalls we get here are quite a

236
00:12:17,020 --> 00:12:20,690
bit worse than they're not,

237
00:12:20,690 --> 00:12:23,120
well, here's one that's like 0.8 something,

238
00:12:23,120 --> 00:12:27,740
but they're quite a bit smaller.

239
00:12:27,740 --> 00:12:30,590
The average is only 0.73,

240
00:12:30,590 --> 00:12:34,425
but the standard deviation is quite wide, 0.15.

241
00:12:34,425 --> 00:12:38,760
So, the recalls we got over here are

242
00:12:38,760 --> 00:12:41,139
within one standard deviation

243
00:12:41,139 --> 00:12:43,005
of these results which is good.

244
00:12:43,005 --> 00:12:45,460
I mean it's very consistent then,

245
00:12:45,460 --> 00:12:50,000
and obviously by training on recall we've let

246
00:12:50,000 --> 00:12:54,050
accuracy precision slip at

247
00:12:54,050 --> 00:12:57,660
the expense of getting the highest possible recall,

248
00:12:57,660 --> 00:13:01,400
and you see maybe a little bit too.

249
00:13:01,400 --> 00:13:05,130
So cross-validation,

250
00:13:05,130 --> 00:13:08,140
I hope you now see is a very powerful method,

251
00:13:08,140 --> 00:13:11,105
allows you to get a more objective measure

252
00:13:11,105 --> 00:13:13,300
of model performance.

253
00:13:13,300 --> 00:13:15,240
We did two different cross-validation.

254
00:13:15,240 --> 00:13:16,815
We did a simple cross-validation,

255
00:13:16,815 --> 00:13:18,670
just to evaluate model performance,

256
00:13:18,670 --> 00:13:21,150
then we did a nested cross-validation

257
00:13:21,150 --> 00:13:22,470
which the inner loop

258
00:13:22,470 --> 00:13:24,485
was sweeping across a set of

259
00:13:24,485 --> 00:13:27,420
hyperparameters to find optimal hyperparameters.

260
00:13:27,420 --> 00:13:30,345
The outer loop was then evaluating the best model we got

261
00:13:30,345 --> 00:13:33,565
which was similar to that simple cross-validation.

262
00:13:33,565 --> 00:13:35,300
But in all cases, you saw that

263
00:13:35,300 --> 00:13:39,075
the actual average performance you can expect was usually

264
00:13:39,075 --> 00:13:41,650
significantly less than just doing

265
00:13:41,650 --> 00:13:45,300
a single naive split training,

266
00:13:45,300 --> 00:13:47,770
and that's the important thing to keep in mind.

267
00:13:47,770 --> 00:13:50,200
That's why we do cross-validation because

268
00:13:50,200 --> 00:13:53,660
there's so much variability from fold to fold.

