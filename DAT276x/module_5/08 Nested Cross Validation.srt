0
00:00:05,259 --> 00:00:10,539
While cross-validation is a procedure for evaluating the quality of an algorithm on

1
00:00:10,539 --> 00:00:15,490
a data set, nested cross-validation is for tuning parameters. So it’s the most popular

2
00:00:15,490 --> 00:00:19,810
way to tune parameters of an algorithm and to do it, you need a data set, you need an

3
00:00:19,810 --> 00:00:24,640
algorithm, you need an evaluation measure for the quality of the results, and then you

4
00:00:24,640 --> 00:00:28,570
need some parameter that you want to tune. And the parameter here – I’m just going

5
00:00:28,570 --> 00:00:32,660
to call it k, but you know it just depends on the algorithm – and for this example,

6
00:00:32,659 --> 00:00:38,679
I want to consider 5 different possible values for k, and those values happen to be 1, 10,

7
00:00:38,679 --> 00:00:46,289
100, 1000, and 10000. So you can actually sweep the model parameters, so I’ll tell

8
00:00:46,289 --> 00:00:50,589
you about that shortly. Okay, so the way that nested cross-validation

9
00:00:50,589 --> 00:01:00,030
works is that you’re going to save one fold as the test fold, and then within the training

10
00:01:00,030 --> 00:01:05,140
set, you’re going to divide the training set into two parts; sort of a nested training

11
00:01:05,140 --> 00:01:10,530
set, and a validation set. And we’re going to rotate the validation set among these inner

12
00:01:10,530 --> 00:01:16,830
folds in the training set and then we’ll choose the value of k with the highest average

13
00:01:16,830 --> 00:01:24,670
validation performance. Okay, so here we rotate this validation fold among these folds, and

14
00:01:24,670 --> 00:01:31,280
then we’ll choose the k with the highest performance. Alright, so let me give you the

15
00:01:31,280 --> 00:01:36,510
full set of instructions: you divide the data into ten folds, you reserve one for tests.

16
00:01:36,510 --> 00:01:42,470
And then, we take the training set and reserve one of the nine training folds for validation.

17
00:01:42,470 --> 00:01:49,159
And then for each value of k that we’re considering, we train using our sort of nested

18
00:01:49,159 --> 00:01:54,740
training set which is the remaining eight folds, and then you evaluate it on the validation

19
00:01:54,740 --> 00:02:00,050
fold, so we now have five measurements, one for each k. And then you repeat the procedure

20
00:02:00,050 --> 00:02:05,930
nine times by rotating which training fold is the validation fold. So you now have how

21
00:02:05,930 --> 00:02:13,159
many measurements? You’ve got nine folds times five values of k measurements, and so

22
00:02:13,159 --> 00:02:18,519
you end up choosing the k that minimizes the average training error over the nine folds,

23
00:02:18,519 --> 00:02:27,629
and you use that k on the test set. And then of course, you’re going to repeat everything

24
00:02:27,629 --> 00:02:33,599
ten times from this step onward using each fold in turn as the test fold. And then in

25
00:02:33,599 --> 00:02:38,120
the end, you report the mean and standard deviation of the evaluation measure over those

26
00:02:38,120 --> 00:02:43,719
ten test folds. Okay, so this is a nice happy evaluation procedure;

27
00:02:43,719 --> 00:02:49,329
the algorithm that performed the best is the one with the best out-of-sample – average

28
00:02:49,329 --> 00:02:54,450
out-of-sample performance across those ten test folds, where you perform nested cross-validation

29
00:02:54,450 --> 00:02:59,560
10 times, one for each training set. Now, again, if you want to, you can compute significance

30
00:02:59,560 --> 00:03:04,799
test on performance across folds. And this whole procedure, this might be computationally

31
00:03:04,799 --> 00:03:09,499
expensive because you have to do an absolute ton of evaluations; you have to do ten test

32
00:03:09,499 --> 00:03:15,709
sets, and ten validation sets, times the number of, you know, parameters that we’re willing

33
00:03:15,709 --> 00:03:19,279
to consider, so it’s actually quite a lot of – you know, quite a lot of churning there

34
00:03:19,279 --> 00:03:25,329
for the algorithm. But this is the way that we generally do parameter tuning and evaluation;

35
00:03:25,329 --> 00:03:30,189
we use nested cross-validation for tuning the parameters within the cross-validation.

