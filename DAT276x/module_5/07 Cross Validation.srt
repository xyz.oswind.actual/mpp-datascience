0
00:00:05,250 --> 00:00:09,559
Let’s talk about cross-validation. So let’s say that I tried ridge regression and I tried

1
00:00:09,559 --> 00:00:15,929
least square regression. How do I know which one performed better? Now, cross-validation

2
00:00:15,929 --> 00:00:20,320
is probably the most popular way to evaluate a machine learning algorithm on a data set.

3
00:00:20,320 --> 00:00:25,990
So the culture of machine learning and data mining revolves around how to evaluate a predictive

4
00:00:25,990 --> 00:00:31,039
model – it’s like a religion, like we heavily believe in out-of-sample testing,

5
00:00:31,039 --> 00:00:36,120
and this sets us apart from a lot of the other fields. So here there’s ground truth, and

6
00:00:36,120 --> 00:00:40,480
we can actually tell when some new algorithm is an improvement. So to do cross-validation,

7
00:00:40,480 --> 00:00:43,600
you need a data set, you need an algorithm, and you need an evaluation measure for the

8
00:00:43,600 --> 00:00:48,180
quality of the results, and that evaluation measure could be the squared error between

9
00:00:48,180 --> 00:00:53,260
the predictions and the truth, or whatever other loss function you want to use. So the

10
00:00:53,260 --> 00:00:58,789
way you do it is you divide the data into 10 approximately equally sized folds, and

11
00:00:58,789 --> 00:01:04,000
then you train the algorithm on nine of the folds, and you compute the evaluation measure

12
00:01:04,000 --> 00:01:08,119
on the last fold, and you repeat this ten times using each fold in turn as the test

13
00:01:08,119 --> 00:01:12,590
fold. And then at the end, you report the mean and standard deviation of the evaluation

14
00:01:12,590 --> 00:01:18,450
measure over the ten folds. So let me do this again, with a bit more of an illustration.

15
00:01:18,450 --> 00:01:26,280
Okay, so you divide the data into ten folds, and then you train the algorithm on the first

16
00:01:26,280 --> 00:01:32,820
nine folds, and then you evaluate it on the last one. And then you rotate which fold is

17
00:01:32,820 --> 00:01:40,470
going to be the test fold, and then in the end you report the mean and standard deviation

18
00:01:40,470 --> 00:01:46,130
of – over all of the test folds. So the algorithm that performs the best is the one

19
00:01:46,130 --> 00:01:53,290
that had the best average out-of-sample performance across the ten folds. And if you want to,

20
00:01:53,290 --> 00:01:58,570
you could compute significance tests on performance across the folds, to determine whether one

21
00:01:58,570 --> 00:02:01,189
algorithm performed significantly better than another one.

