0
00:00:01,610 --> 00:00:04,350
>> Hello, in this demo,

1
00:00:04,350 --> 00:00:07,490
I'm going to show you some properties of using

2
00:00:07,490 --> 00:00:11,190
regularization with R. In particular,

3
00:00:11,190 --> 00:00:13,005
we're going to look in detail at

4
00:00:13,005 --> 00:00:15,885
l2 regularization and l1 regularization.

5
00:00:15,885 --> 00:00:17,960
l2 regularization being kind

6
00:00:17,960 --> 00:00:19,500
of a soft regularization that

7
00:00:19,500 --> 00:00:23,280
just constrains the overall size of parameters,

8
00:00:23,280 --> 00:00:26,280
l1 being a harder regularization

9
00:00:26,280 --> 00:00:29,195
that actually drives coefficients to zero.

10
00:00:29,195 --> 00:00:33,030
We can compare what happens there and look at

11
00:00:33,030 --> 00:00:34,215
how the models behave

12
00:00:34,215 --> 00:00:36,360
under these different regularizations,

13
00:00:36,360 --> 00:00:38,175
and with different degrees

14
00:00:38,175 --> 00:00:41,275
of constraint of the regularization.

15
00:00:41,275 --> 00:00:44,295
So, to get started here,

16
00:00:44,295 --> 00:00:50,270
I'm just going to create an unregularized model,

17
00:00:50,270 --> 00:00:53,125
just using a linear model,

18
00:00:53,125 --> 00:00:56,400
and I'm deliberately going to over fit this model.

19
00:00:56,400 --> 00:00:58,760
So, this is the auto price data

20
00:00:58,760 --> 00:01:01,060
set and I'm going to model the log of

21
00:01:01,060 --> 00:01:02,680
the auto price by

22
00:01:02,680 --> 00:01:07,225
basically every feature in the data set.

23
00:01:07,225 --> 00:01:09,310
You see a lot of feature names

24
00:01:09,310 --> 00:01:11,860
there and we're going to use,

25
00:01:11,860 --> 00:01:15,730
I've already split and scaled the data,

26
00:01:15,730 --> 00:01:17,680
so I already have a training set,

27
00:01:17,680 --> 00:01:21,865
and I have my predictions to get my score.

28
00:01:21,865 --> 00:01:25,420
I already have a test data set and then,

29
00:01:25,420 --> 00:01:26,810
we're going to print some metrics.

30
00:01:26,810 --> 00:01:29,025
We're going to look at the residual,

31
00:01:29,025 --> 00:01:30,760
the plots of the histogram,

32
00:01:30,760 --> 00:01:32,210
the Q-Q plot of the residual,

33
00:01:32,210 --> 00:01:34,285
and the residual plot itself.

34
00:01:34,285 --> 00:01:37,955
So, let's get going.

35
00:01:37,955 --> 00:01:39,820
And you see we've got very

36
00:01:39,820 --> 00:01:43,000
high R-squared and adjusted R-squared

37
00:01:43,000 --> 00:01:45,265
that's suspicious in a way

38
00:01:45,265 --> 00:01:49,050
that this is probably an over fit model.

39
00:01:49,530 --> 00:01:52,875
And here, we have

40
00:01:52,875 --> 00:01:56,830
the residual histogram and you see we have one outlier.

41
00:01:56,830 --> 00:01:58,440
But, generally that looks

42
00:01:58,440 --> 00:02:00,710
pretty normally distributed except

43
00:02:00,710 --> 00:02:05,430
for this bit of left skew here.

44
00:02:05,430 --> 00:02:09,680
But, again that could be because we over fit the model,

45
00:02:09,680 --> 00:02:11,520
we're learning the data rather than

46
00:02:11,520 --> 00:02:13,860
coming up with a model that generalizes well.

47
00:02:13,860 --> 00:02:16,210
Similarly, our Q-Q Normal

48
00:02:16,210 --> 00:02:17,800
plot where we have the quantiles of

49
00:02:17,800 --> 00:02:19,890
the residuals on the vertical and

50
00:02:19,890 --> 00:02:22,730
the quantiles of a standard normal on the horizontal,

51
00:02:22,730 --> 00:02:24,520
that's pretty close to a straight line.

52
00:02:24,520 --> 00:02:26,745
There's a little bit of zigzaggingness here.

53
00:02:26,745 --> 00:02:28,810
There are a few outliers at the end.

54
00:02:28,810 --> 00:02:32,145
Clearly, we have an outlier here at the low end,

55
00:02:32,145 --> 00:02:35,820
but it's, generally, actually looks pretty good.

56
00:02:35,820 --> 00:02:38,970
Our residual plot where

57
00:02:38,970 --> 00:02:42,090
we have predicted values on the horizontal axis,

58
00:02:42,090 --> 00:02:46,730
residuals on the vertical axis, looks pretty good.

59
00:02:46,730 --> 00:02:49,990
If anything, we're over predicting

60
00:02:49,990 --> 00:02:53,030
large or high priced cars,

61
00:02:53,030 --> 00:02:55,055
remember there's a log scale here.

62
00:02:55,055 --> 00:02:56,530
And here's our one outlier,

63
00:02:56,530 --> 00:02:57,850
is actually right in the middle

64
00:02:57,850 --> 00:02:59,260
of the price range it looks

65
00:02:59,260 --> 00:03:02,815
like that's our big negative residual.

66
00:03:02,815 --> 00:03:08,470
Okay. So, it's probably

67
00:03:08,470 --> 00:03:10,360
the case as I said just based on

68
00:03:10,360 --> 00:03:14,270
these very high R-squared values and what that we're,

69
00:03:14,270 --> 00:03:16,735
and just the fact that I threw

70
00:03:16,735 --> 00:03:19,630
almost every possible feature into

71
00:03:19,630 --> 00:03:22,720
this that this is an over fit model.

72
00:03:22,720 --> 00:03:24,590
So, what do we do about it?

73
00:03:24,590 --> 00:03:27,230
We're going to use a method

74
00:03:27,230 --> 00:03:30,215
called the generalized linear model

75
00:03:30,215 --> 00:03:34,290
from R and we're

76
00:03:34,290 --> 00:03:37,835
going to start out by creating a model matrix.

77
00:03:37,835 --> 00:03:39,655
In a model matrix, as you recall,

78
00:03:39,655 --> 00:03:44,219
dummy variables are binary variables

79
00:03:44,219 --> 00:03:46,810
that encode the categorical variables.

80
00:03:46,810 --> 00:03:48,545
So, for example, make,

81
00:03:48,545 --> 00:03:50,460
we had quite a few manufacturers of

82
00:03:50,460 --> 00:03:51,970
these cars fuel type there

83
00:03:51,970 --> 00:03:53,900
were two categorical variables.

84
00:03:53,900 --> 00:03:55,490
There was diesel or gas,

85
00:03:55,490 --> 00:04:00,255
aspiration was standard or turbo, etcetera.

86
00:04:00,255 --> 00:04:02,325
I think, there were like five body styles

87
00:04:02,325 --> 00:04:04,990
and three types of drive wheels: rear wheel drive,

88
00:04:04,990 --> 00:04:07,300
front wheel drive, and four wheel drive, etcetera.

89
00:04:07,300 --> 00:04:11,140
So, from the r-Caret package,

90
00:04:11,140 --> 00:04:13,640
we're going to use the dummyVars function.

91
00:04:13,640 --> 00:04:16,845
And again, our label is log price

92
00:04:16,845 --> 00:04:19,455
and we're going to model

93
00:04:19,455 --> 00:04:23,240
all these other columns by log price.

94
00:04:23,240 --> 00:04:27,550
If it's a numeric variable like city miles per gallon,

95
00:04:27,550 --> 00:04:28,710
it just stays numeric,

96
00:04:28,710 --> 00:04:30,600
but if it's something like fuel type,

97
00:04:30,600 --> 00:04:33,000
it'll become in this case,

98
00:04:33,000 --> 00:04:35,660
two dummy variables because there's two fuel types.

99
00:04:35,660 --> 00:04:39,000
And so, that's our data and then,

100
00:04:39,000 --> 00:04:43,650
we apply this dummies

101
00:04:43,650 --> 00:04:46,850
we use a predict method on the dummies object,

102
00:04:46,850 --> 00:04:48,900
which dummies object contains

103
00:04:48,900 --> 00:04:50,550
the coding or the recoding we're

104
00:04:50,550 --> 00:04:52,435
going to use to get the dummy variables,

105
00:04:52,435 --> 00:04:55,550
and we do the new data for training,

106
00:04:55,550 --> 00:05:00,060
and we'll have to do the new data later for test.

107
00:05:01,660 --> 00:05:05,995
So, here and we're going to look at the head of this,

108
00:05:05,995 --> 00:05:11,380
and you see Alfa Romeo, Audi, BMW, Chevrolet.

109
00:05:11,380 --> 00:05:13,360
So, like the first two cars are

110
00:05:13,360 --> 00:05:17,290
Alfa Romeos because you see they have ones in

111
00:05:17,290 --> 00:05:19,030
that column and they're zeroes for

112
00:05:19,030 --> 00:05:21,670
all other manufacturers which

113
00:05:21,670 --> 00:05:23,430
we can't even see all the manufacturers,

114
00:05:23,430 --> 00:05:24,445
they are so many.

115
00:05:24,445 --> 00:05:27,980
And then, we have our Audi's.

116
00:05:28,110 --> 00:05:30,730
So, that's our dummy variables and then,

117
00:05:30,730 --> 00:05:33,660
we'll do the same as I said for the test.

118
00:05:33,660 --> 00:05:35,860
And the reason we're doing this is

119
00:05:35,860 --> 00:05:41,230
the glmnet method which we're going to use

120
00:05:41,230 --> 00:05:49,780
here only allows us to only takes matrices,

121
00:05:49,780 --> 00:05:51,360
it does not take data frame.

122
00:05:51,360 --> 00:05:55,190
So, we have to give it X is our model matrix,

123
00:05:55,190 --> 00:05:58,660
which is our dummy variables of our features.

124
00:05:58,660 --> 00:06:02,960
And Y, is just the log price

125
00:06:02,960 --> 00:06:07,130
that's our label and nlambda.

126
00:06:07,130 --> 00:06:10,380
So, we're going to try 20 values of lambda,

127
00:06:10,380 --> 00:06:12,875
recall that l2 regularization

128
00:06:12,875 --> 00:06:16,145
uses this regularization parameter called lambda,

129
00:06:16,145 --> 00:06:20,955
and so, we're going to try 20 values of lambda.

130
00:06:20,955 --> 00:06:24,930
We're going to set alpha to zero,

131
00:06:24,930 --> 00:06:29,450
so alpha is the trade-off parameter between l2 and l1.

132
00:06:29,450 --> 00:06:33,270
So at zero, this is pure l2 regularization,

133
00:06:33,270 --> 00:06:36,325
at one it's pure l1 regularization.

134
00:06:36,325 --> 00:06:38,710
The family is Gaussian.

135
00:06:38,710 --> 00:06:40,530
So, because we're expecting

136
00:06:40,530 --> 00:06:42,280
a Gaussian distributed response,

137
00:06:42,280 --> 00:06:43,995
this is a regression problem.

138
00:06:43,995 --> 00:06:46,890
So, our residuals should be Gaussian or normal,

139
00:06:46,890 --> 00:06:50,090
so that's why our family is Gaussian.

140
00:06:50,570 --> 00:06:53,330
We can make a nice plot of

141
00:06:53,330 --> 00:06:56,120
this glmnet model object

142
00:06:56,120 --> 00:06:59,090
and that will tell us something interesting here.

143
00:06:59,090 --> 00:07:03,840
So, what this shows us is these are the values of

144
00:07:03,840 --> 00:07:07,785
the coefficients and it's

145
00:07:07,785 --> 00:07:09,880
a slightly odd way they do this,

146
00:07:09,880 --> 00:07:16,200
so it's the inverse of that alpha or it's really the log.

147
00:07:16,200 --> 00:07:24,300
So, regularization is increasing

148
00:07:24,300 --> 00:07:27,440
to the left decreasing to the right.

149
00:07:27,440 --> 00:07:30,540
That's the slightly odd thing about this plot.

150
00:07:30,540 --> 00:07:31,990
I'm not sure why they do it that way.

151
00:07:31,990 --> 00:07:34,380
And you can see when we have

152
00:07:34,380 --> 00:07:37,620
low levels of regularization over here on the right,

153
00:07:37,620 --> 00:07:40,870
these coefficients are widely

154
00:07:40,870 --> 00:07:43,695
dispersed and there are some fairly large ones.

155
00:07:43,695 --> 00:07:49,065
And as we increase regularization, we eventually,

156
00:07:49,065 --> 00:07:52,120
but smoothly, because it's l2 regularization,

157
00:07:52,120 --> 00:07:54,010
we eventually drive all

158
00:07:54,010 --> 00:07:55,720
of our model coefficients to zero.

159
00:07:55,720 --> 00:07:57,640
So, it's a pretty useless model

160
00:07:57,640 --> 00:07:59,095
at that extreme end member.

161
00:07:59,095 --> 00:08:01,120
But, notice that some coefficients,

162
00:08:01,120 --> 00:08:03,955
like these two showing in in red here,

163
00:08:03,955 --> 00:08:07,800
actually increase as others decrease and then,

164
00:08:07,800 --> 00:08:11,695
they go down, okay?

165
00:08:11,695 --> 00:08:14,640
So, that's l2 behavior,

166
00:08:14,640 --> 00:08:17,790
that it smoothly constrains

167
00:08:17,790 --> 00:08:20,970
model coefficients as you increase

168
00:08:20,970 --> 00:08:25,370
the level of regularization to the left on this plot.

169
00:08:26,620 --> 00:08:29,525
So, now, I'm going to,

170
00:08:29,525 --> 00:08:33,860
there's a cross validation method here which

171
00:08:33,860 --> 00:08:40,220
basically trains the model multiple times and again,

172
00:08:40,220 --> 00:08:44,690
does with 20 values of lambda and the alpha.

173
00:08:44,690 --> 00:08:46,780
So everything else. So, this is just going to

174
00:08:46,780 --> 00:08:49,340
give us a way to look at the fit,

175
00:08:49,340 --> 00:08:53,245
to look at the standard errors of our fit.

176
00:08:53,245 --> 00:08:58,345
It produces this very nice plot here,

177
00:08:58,345 --> 00:09:00,160
where you have the log of lambda.

178
00:09:00,160 --> 00:09:02,385
So, at least that makes sense, right?

179
00:09:02,385 --> 00:09:06,050
So, this is low lambda values less than one,

180
00:09:06,050 --> 00:09:07,900
lambda value of one,

181
00:09:07,900 --> 00:09:10,170
and large lambda values,

182
00:09:10,170 --> 00:09:12,975
and mean-squared error on the vertical axis.

183
00:09:12,975 --> 00:09:14,350
You see, in fact,

184
00:09:14,350 --> 00:09:17,165
that the lowest lambda value

185
00:09:17,165 --> 00:09:20,660
has the lowest mean-squared error in this case,

186
00:09:20,660 --> 00:09:25,720
and the errors increase this way.

187
00:09:25,720 --> 00:09:26,450
So, if you think about

188
00:09:26,450 --> 00:09:29,325
this in the bias-variance-trade-off,

189
00:09:29,325 --> 00:09:33,430
we're over constraining the model this way.

190
00:09:33,430 --> 00:09:36,515
So, the bias is increasing to the right,

191
00:09:36,515 --> 00:09:38,960
the variance is increasing to the left.

192
00:09:38,960 --> 00:09:40,970
Although you can't really see it.

193
00:09:40,970 --> 00:09:42,210
There is no upturn in

194
00:09:42,210 --> 00:09:45,040
this particular curve just because of the model behavior,

195
00:09:45,040 --> 00:09:47,580
but that could occur.

196
00:09:47,580 --> 00:09:50,575
But you're increasing variance this way,

197
00:09:50,575 --> 00:09:54,120
increasing bias to the right this way.

198
00:10:00,510 --> 00:10:03,430
So, we'll go ahead and print some metrics,

199
00:10:03,430 --> 00:10:07,840
and the GLMNET keeps the best model.

200
00:10:07,840 --> 00:10:13,020
So, we can just run our evaluation code on this.

201
00:10:13,220 --> 00:10:18,145
Notice that we get R-squared that's considerably lower,

202
00:10:18,145 --> 00:10:22,590
it's 0.91 when we have a properly regularized model.

203
00:10:22,590 --> 00:10:26,900
I think I actually took this value here,

204
00:10:26,900 --> 00:10:28,865
not the extreme value,

205
00:10:28,865 --> 00:10:33,295
and you can see they are all within the same error bars.

206
00:10:33,295 --> 00:10:34,540
It's a little hard to see.

207
00:10:34,540 --> 00:10:35,840
So, there's really not

208
00:10:35,840 --> 00:10:38,250
any statistical difference between these three.

209
00:10:38,250 --> 00:10:41,210
So, I took the third one over.

210
00:10:41,210 --> 00:10:43,060
So, there's a little bit of regularization applied

211
00:10:43,060 --> 00:10:45,535
because this is such an overfit model,

212
00:10:45,535 --> 00:10:48,860
and we get slightly higher errors

213
00:10:48,860 --> 00:10:50,390
and slightly lower r-squared,

214
00:10:50,390 --> 00:10:53,660
but, a model that's less overfit.

215
00:10:53,660 --> 00:10:55,780
However, our residuals now have

216
00:10:55,780 --> 00:10:57,980
heavier tails on the left and the right.

217
00:10:57,980 --> 00:11:00,000
This looks less normal,

218
00:11:00,000 --> 00:11:03,820
you see it's kind of more s-shaped.

219
00:11:03,820 --> 00:11:08,380
The residual plot with predicted values and

220
00:11:08,380 --> 00:11:13,675
residuals on the vertical looks similar,

221
00:11:13,675 --> 00:11:18,260
well-behaved for the most part as far as you can see.

222
00:11:18,260 --> 00:11:21,140
So, it looks like

223
00:11:21,140 --> 00:11:23,925
we came up with a reasonably good model.

224
00:11:23,925 --> 00:11:25,920
In a model also,

225
00:11:25,920 --> 00:11:28,310
because of doing this cross-validation,

226
00:11:28,310 --> 00:11:31,615
we have some confidence that this model isn't overfit,

227
00:11:31,615 --> 00:11:33,645
and that, we'll get

228
00:11:33,645 --> 00:11:36,230
reasonably good performance in production when it's

229
00:11:36,230 --> 00:11:38,600
faced with new data cases that we want to

230
00:11:38,600 --> 00:11:42,065
make predictions where we don't know the labels.

231
00:11:42,065 --> 00:11:45,440
So, let's try L1 regularization.

232
00:11:45,440 --> 00:11:49,045
It's the same code essentially here.

233
00:11:49,045 --> 00:11:50,625
The only difference is,

234
00:11:50,625 --> 00:11:52,130
I said alpha to one.

235
00:11:52,130 --> 00:11:54,410
So, that's the extreme end member of

236
00:11:54,410 --> 00:11:56,980
this trade-off between L2 and L1 regularization.

237
00:11:56,980 --> 00:12:01,695
L2 equals one is only L1 regularization,

238
00:12:01,695 --> 00:12:05,560
and essentially, I'm going to make the same plot again.

239
00:12:06,800 --> 00:12:08,970
You read it the same way.

240
00:12:08,970 --> 00:12:13,070
It's got the same funny backwards horizontal axis,

241
00:12:13,070 --> 00:12:16,470
where the intensity of the regularization or

242
00:12:16,470 --> 00:12:22,095
the L1 parameter is increasing to the left.

243
00:12:22,095 --> 00:12:26,495
So, the underconstrained model is to the right,

244
00:12:26,495 --> 00:12:28,960
and these are the model coefficients.

245
00:12:28,960 --> 00:12:32,190
So, you start out with a lightly constrained model

246
00:12:32,190 --> 00:12:35,960
with very wide range of model coefficient values.

247
00:12:35,960 --> 00:12:38,800
But this is L1 regularization, so,

248
00:12:38,800 --> 00:12:41,979
you see that some coefficients

249
00:12:41,979 --> 00:12:44,520
are very quickly driven to zero.

250
00:12:44,520 --> 00:12:46,620
You see, actually, it's a little hard to see but

251
00:12:46,620 --> 00:12:49,235
a lot of them are getting driven to zero.

252
00:12:49,235 --> 00:12:53,540
A few increase, and you see a few kinks in the curve,

253
00:12:53,540 --> 00:12:55,440
and odd things like that,

254
00:12:55,440 --> 00:12:58,610
because, as some coefficients get driven to zero,

255
00:12:58,610 --> 00:13:02,930
other ones suddenly might jump in value.

256
00:13:04,330 --> 00:13:07,490
Here's the plot of

257
00:13:07,490 --> 00:13:11,360
those mean squared error

258
00:13:11,360 --> 00:13:14,770
versus this inverse of regularization,

259
00:13:14,770 --> 00:13:23,460
and we see that the optimal value is here.

260
00:13:23,460 --> 00:13:26,580
It's just a little bit lower mean-squared error

261
00:13:26,580 --> 00:13:28,930
than all its neighbors.

262
00:13:28,930 --> 00:13:32,600
So, we'll go ahead and take

263
00:13:32,600 --> 00:13:37,265
that optimal bottle ,which happens to be,

264
00:13:37,265 --> 00:13:38,675
says it's the 13th.

265
00:13:38,675 --> 00:13:40,360
That's how you figure out which one it is.

266
00:13:40,360 --> 00:13:43,650
You have to count from this side.

267
00:13:43,650 --> 00:13:46,800
It's where they start, at the highest value.

268
00:13:46,800 --> 00:13:50,730
So, it's the 13th one out of 20,

269
00:13:50,730 --> 00:13:56,475
and we'll print the statistics.

270
00:13:56,475 --> 00:14:01,795
So, these statistics look about the same as for the L2.

271
00:14:01,795 --> 00:14:03,710
Our histogram of residuals

272
00:14:03,710 --> 00:14:05,060
however, looks a little better.

273
00:14:05,060 --> 00:14:06,300
See, it's a bit more symmetric.

274
00:14:06,300 --> 00:14:11,540
We still have too large outliers here,

275
00:14:11,540 --> 00:14:13,890
but the range of outliers is pretty limited,

276
00:14:13,890 --> 00:14:15,700
we don't have the tail on the other end.

277
00:14:15,700 --> 00:14:17,475
So, this is actually a pretty good model.

278
00:14:17,475 --> 00:14:19,275
The QQ normal plot,

279
00:14:19,275 --> 00:14:21,250
is actually better than any of

280
00:14:21,250 --> 00:14:23,870
the models we've created so far.

281
00:14:23,870 --> 00:14:30,290
But this plot, you can see the residuals increase.

282
00:14:30,290 --> 00:14:32,500
It looks like, the dispersion of the residuals is

283
00:14:32,500 --> 00:14:35,700
increasing as the predicted value,

284
00:14:35,700 --> 00:14:39,705
the predicted price of the automobiles goes down.

285
00:14:39,705 --> 00:14:42,820
But if we were continuing this process,

286
00:14:42,820 --> 00:14:46,940
we would try different alpha values here.

287
00:14:46,940 --> 00:14:49,785
So, we would use L1 and L2

288
00:14:49,785 --> 00:14:53,745
regularization in some weighted manner.

289
00:14:53,745 --> 00:14:55,600
So, I hope that gives you

290
00:14:55,600 --> 00:14:58,180
some overview of regularization,

291
00:14:58,180 --> 00:15:01,250
how it constrains model parameters,

292
00:15:01,250 --> 00:15:03,515
so you don't wind up with overfit models.

293
00:15:03,515 --> 00:15:06,580
We saw that with the L2 regularization,

294
00:15:06,580 --> 00:15:09,090
that constrain is applied fairly smoothly,

295
00:15:09,090 --> 00:15:11,510
fairly softly, because it's just based on

296
00:15:11,510 --> 00:15:15,180
the Euclidean norm of the model parameters.

297
00:15:15,180 --> 00:15:17,350
That leads to a smooth behavior,

298
00:15:17,350 --> 00:15:20,615
the L1 tends to be a hard constraint,

299
00:15:20,615 --> 00:15:23,760
and it just drives coefficients to zero.

300
00:15:23,760 --> 00:15:26,160
In this case, the L1 looked like it

301
00:15:26,160 --> 00:15:28,450
worked a little better than the other models,

302
00:15:28,450 --> 00:15:32,570
but in reality, you'd want to use some combination of L1,

303
00:15:32,570 --> 00:15:35,550
L2 in all likelihood.

