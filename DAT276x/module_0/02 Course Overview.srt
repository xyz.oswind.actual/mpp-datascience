0
00:00:00,130 --> 00:00:02,480
>> Hello and welcome.

1
00:00:02,480 --> 00:00:04,030
In this sequence, I'm going to talk

2
00:00:04,030 --> 00:00:06,465
about what's in this course,

3
00:00:06,465 --> 00:00:08,620
why you might want to take this course,

4
00:00:08,620 --> 00:00:11,585
and how you get the most out of this course.

5
00:00:11,585 --> 00:00:13,870
Now, as you might have guessed from our title,

6
00:00:13,870 --> 00:00:16,750
this course is primarily focused on machine learning.

7
00:00:16,750 --> 00:00:18,310
So, first off, why

8
00:00:18,310 --> 00:00:20,000
would you want to learn about machine learning?

9
00:00:20,000 --> 00:00:22,975
Well, there's a lot of good reasons actually.

10
00:00:22,975 --> 00:00:26,170
One is it's one of the hottest areas and

11
00:00:26,170 --> 00:00:29,875
most widely applied areas of artificial intelligence.

12
00:00:29,875 --> 00:00:33,670
As you know, I'm sure even if you read the popular press,

13
00:00:33,670 --> 00:00:35,650
AI is hot and machine learning is

14
00:00:35,650 --> 00:00:38,580
an important component of AI.

15
00:00:38,580 --> 00:00:41,590
It's also a very practical subject,

16
00:00:41,590 --> 00:00:44,710
it's widely deployed in many areas,

17
00:00:44,710 --> 00:00:47,110
and just a few that I've listed

18
00:00:47,110 --> 00:00:49,670
here are like demand forecasting.

19
00:00:49,670 --> 00:00:54,300
So, demand forecasting be like inventory management,

20
00:00:54,300 --> 00:00:56,350
how many people are going to ride

21
00:00:56,350 --> 00:00:58,510
buses for transit management,

22
00:00:58,510 --> 00:01:00,370
all sorts of things where you're trying to

23
00:01:00,370 --> 00:01:03,655
forecast something in the future.

24
00:01:03,655 --> 00:01:08,050
Fraud detection is another big area of machine learning.

25
00:01:08,050 --> 00:01:11,890
We've been at this for decades now and it

26
00:01:11,890 --> 00:01:14,280
just keeps getting more complicated and fraud

27
00:01:14,280 --> 00:01:15,760
is never going away,

28
00:01:15,760 --> 00:01:18,730
but we use a lot of powerful machine learning methods

29
00:01:18,730 --> 00:01:22,215
to beat it back and keep it under control, we hope.

30
00:01:22,215 --> 00:01:24,820
Spam detection, again, one of these things

31
00:01:24,820 --> 00:01:27,490
that's an ongoing issue

32
00:01:27,490 --> 00:01:29,605
that machine learning has helped

33
00:01:29,605 --> 00:01:33,460
tremendously on over several decades now.

34
00:01:33,460 --> 00:01:35,410
Targeted marketing, of course,

35
00:01:35,410 --> 00:01:37,845
has been in the news a lot and

36
00:01:37,845 --> 00:01:42,070
wouldn't work at all if it wasn't for machine learning.

37
00:01:42,070 --> 00:01:44,010
Preventive maintenance is

38
00:01:44,010 --> 00:01:47,790
another cool area for expensive equipment,

39
00:01:47,790 --> 00:01:49,490
maybe a jet engine or something.

40
00:01:49,490 --> 00:01:52,740
You build predictive models

41
00:01:52,740 --> 00:01:55,890
that try to determine what's

42
00:01:55,890 --> 00:01:59,420
the best time to do some maintenance on that jet engine,

43
00:01:59,420 --> 00:02:02,310
and industries have saved,

44
00:02:02,310 --> 00:02:05,400
if not millions probably in the billions of dollars,

45
00:02:05,400 --> 00:02:07,110
using machine learning methods

46
00:02:07,110 --> 00:02:10,950
to optimize preventive maintenance,

47
00:02:10,950 --> 00:02:12,525
and the list goes on and on.

48
00:02:12,525 --> 00:02:15,530
There's so many applications of machine learning.

49
00:02:15,530 --> 00:02:18,295
So, it's a really valuable skill to have.

50
00:02:18,295 --> 00:02:20,990
So, there are other machine learning courses.

51
00:02:20,990 --> 00:02:22,450
Why would you do this course?

52
00:02:22,450 --> 00:02:24,915
Well, I think we're giving you a good mix

53
00:02:24,915 --> 00:02:28,580
of theory and practice here.

54
00:02:28,580 --> 00:02:30,730
So, in specific,

55
00:02:30,730 --> 00:02:32,970
we're going to have theory lectures where

56
00:02:32,970 --> 00:02:35,490
we give you the background needed to understand

57
00:02:35,490 --> 00:02:37,050
machine learning concepts and

58
00:02:37,050 --> 00:02:38,510
what the algorithms are doing,

59
00:02:38,510 --> 00:02:40,920
and why they don't work sometimes,

60
00:02:40,920 --> 00:02:44,205
and how you figure out what to do.

61
00:02:44,205 --> 00:02:46,440
We'll have demos of

62
00:02:46,440 --> 00:02:49,530
practical applications of machine learning algorithms.

63
00:02:49,530 --> 00:02:52,960
So, I'll show you some code in R using

64
00:02:52,960 --> 00:02:56,410
the many R packages that are used for machine learning,

65
00:02:56,410 --> 00:02:59,105
and then you yourself are going to do

66
00:02:59,105 --> 00:03:01,530
hands-on work to develop

67
00:03:01,530 --> 00:03:04,530
skills working with and exploring data.

68
00:03:04,530 --> 00:03:07,050
As you can imagine, if you want to do machine learning,

69
00:03:07,050 --> 00:03:08,565
you got to do it with data

70
00:03:08,565 --> 00:03:10,140
and you've got to learn how to work with data,

71
00:03:10,140 --> 00:03:11,190
and you need to understand

72
00:03:11,190 --> 00:03:14,745
the complex relationships in the data set

73
00:03:14,745 --> 00:03:17,790
or else you're not going to

74
00:03:17,790 --> 00:03:20,725
really optimize how the machine learning algorithm works.

75
00:03:20,725 --> 00:03:25,800
So, it's really important to work through that and

76
00:03:25,800 --> 00:03:28,410
understand what relates to

77
00:03:28,410 --> 00:03:32,855
what and what's just noise may be in the data.

78
00:03:32,855 --> 00:03:35,220
We'll give you some hands-on experience

79
00:03:35,220 --> 00:03:36,570
through the labs of this.

80
00:03:36,570 --> 00:03:37,805
You're going to be using

81
00:03:37,805 --> 00:03:39,760
state-of-the-art machine learning algorithms

82
00:03:39,760 --> 00:03:41,065
in the R language.

83
00:03:41,065 --> 00:03:43,920
R has many packages for machine learning

84
00:03:43,920 --> 00:03:46,885
and we'll probably only touch on a few,

85
00:03:46,885 --> 00:03:50,680
but we'll hit some really important ones

86
00:03:50,680 --> 00:03:51,810
that are just used by

87
00:03:51,810 --> 00:03:55,025
machine learning engineers constantly.

88
00:03:55,025 --> 00:03:59,030
There'll be some opportunities to dig into a case study.

89
00:03:59,030 --> 00:04:01,525
At the end of the course,

90
00:04:01,525 --> 00:04:03,405
you'll do a case study.

91
00:04:03,405 --> 00:04:06,230
So, what's in this course specifically?

92
00:04:06,230 --> 00:04:08,170
Well, not too surprisingly,

93
00:04:08,170 --> 00:04:11,860
the first module is a broad overview of machine learning.

94
00:04:11,860 --> 00:04:16,270
We'll work on one particular example and you'll see how

95
00:04:16,270 --> 00:04:22,060
you use R to create machine learning models.

96
00:04:22,060 --> 00:04:25,060
The whole module two is just devoted

97
00:04:25,060 --> 00:04:27,280
to data exploration and visualization,

98
00:04:27,280 --> 00:04:29,600
so that's where you'll be working with data,

99
00:04:29,600 --> 00:04:31,990
you'll be looking at

100
00:04:31,990 --> 00:04:34,780
the complex relationships in data and learning,

101
00:04:34,780 --> 00:04:37,110
the tools to do that.

102
00:04:37,110 --> 00:04:41,440
Module three is devoted to data preparation.

103
00:04:41,440 --> 00:04:43,750
How do you get the data ready so

104
00:04:43,750 --> 00:04:46,765
when you start building machine learning models,

105
00:04:46,765 --> 00:04:49,100
you're not just wasting your time.

106
00:04:49,100 --> 00:04:51,100
In module four, we'll talk

107
00:04:51,100 --> 00:04:53,140
about supervised machine learning.

108
00:04:53,140 --> 00:04:54,460
So, supervised machine learning is

109
00:04:54,460 --> 00:04:56,320
the case where we have no one labels,

110
00:04:56,320 --> 00:04:58,610
where we know what the answer is,

111
00:04:58,610 --> 00:05:03,380
so we train and evaluate using those knowing cases.

112
00:05:03,380 --> 00:05:05,590
Module five is devoted to

113
00:05:05,590 --> 00:05:07,120
some powerful techniques for

114
00:05:07,120 --> 00:05:09,370
improving machine learning models.

115
00:05:09,370 --> 00:05:11,530
Module six, we'll go into

116
00:05:11,530 --> 00:05:14,360
more supervised machine learning algorithms,

117
00:05:14,360 --> 00:05:18,595
we'll broaden your toolbox of what you can do.

118
00:05:18,595 --> 00:05:20,190
Module seven,

119
00:05:20,190 --> 00:05:22,420
we'll talk about unsupervised machine learning.

120
00:05:22,420 --> 00:05:24,480
So, that's cases where we don't have labels,

121
00:05:24,480 --> 00:05:26,980
where it isn't marked cases.

122
00:05:26,980 --> 00:05:28,810
In reality, in the world,

123
00:05:28,810 --> 00:05:30,670
that's probably most of the data that's

124
00:05:30,670 --> 00:05:32,890
out there doesn't have mark cases.

125
00:05:32,890 --> 00:05:37,550
So, how do you get the most from this course?

126
00:05:37,680 --> 00:05:40,914
So, obviously, there are seven modules

127
00:05:40,914 --> 00:05:44,270
and we encourage you to go through those in order.

128
00:05:44,270 --> 00:05:47,240
In each module, we'll have lectures,

129
00:05:47,240 --> 00:05:48,880
most modules have demos,

130
00:05:48,880 --> 00:05:51,750
and all modules have one or more labs.

131
00:05:51,750 --> 00:05:53,410
The labs are where

132
00:05:53,410 --> 00:05:55,060
you're going to get your hands-on experience,

133
00:05:55,060 --> 00:05:56,695
so it's really important to

134
00:05:56,695 --> 00:05:59,940
spend the time and plan the time for the labs.

135
00:05:59,940 --> 00:06:03,620
The labs are performed using the R language,

136
00:06:03,620 --> 00:06:06,140
and the evaluations if you're

137
00:06:06,140 --> 00:06:08,570
working toward the certificate are based on the lab.

138
00:06:08,570 --> 00:06:09,590
So, you really have to do

139
00:06:09,590 --> 00:06:13,210
the labs if you want to get the certificate.

140
00:06:13,540 --> 00:06:16,280
Then, there's a final challenge.

141
00:06:16,280 --> 00:06:18,650
You've built skills through seven modules of

142
00:06:18,650 --> 00:06:21,170
this course and you'll

143
00:06:21,170 --> 00:06:22,760
get to test those on

144
00:06:22,760 --> 00:06:24,335
this final challenge which is

145
00:06:24,335 --> 00:06:26,215
like a case study, basically.

146
00:06:26,215 --> 00:06:28,760
So, that's the structure of the course.

147
00:06:28,760 --> 00:06:30,920
I hope you like our course.

148
00:06:30,920 --> 00:06:32,275
I hope you get a lot out of it.

149
00:06:32,275 --> 00:06:33,740
I hope it helps you in your career in

150
00:06:33,740 --> 00:06:37,350
the future and I'll see you in the next module.

