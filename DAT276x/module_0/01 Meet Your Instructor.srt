0
00:00:04,088 --> 00:00:06,800
Welcome to the principles of Machine Learning.

1
00:00:06,910 --> 00:00:09,155
My name is Cynthia Rudin and I'm Steve Elston.

2
00:00:09,244 --> 00:00:12,977
Now Machine Learning is everywhere. This is the time for Machine Learning.

3
00:00:13,088 --> 00:00:14,488
It's becoming mainstream,

4
00:00:14,577 --> 00:00:17,044
it's in the search engine, engines we use every date,

5
00:00:17,150 --> 00:00:18,970
the bank teller machines reading our checks

6
00:00:19,220 --> 00:00:21,750
it's in our smartphone assistance like Cortana,

7
00:00:21,844 --> 00:00:23,470
it's you know, Jobs and Machine Learning are

8
00:00:23,577 --> 00:00:27,333
every-- in every industry and we're thrilled to be able to give you an introduction

9
00:00:27,460 --> 00:00:28,910
to Machine Learning in this course.

10
00:00:29,155 --> 00:00:31,844
So lets Steve and I introduce ourselves first.

11
00:00:31,930 --> 00:00:35,750
So I'm an Associate professor of Computer Science and Electrical

12
00:00:35,870 --> 00:00:37,820
and Computer engineering at Duke,

13
00:00:37,911 --> 00:00:40,755
and an associate professor of Statistics at MIT.

14
00:00:40,866 --> 00:00:43,400
And my main expertise is in Machine Learning and data mining,

15
00:00:43,511 --> 00:00:46,133
my lab is called the Prediction Analysis lab.

16
00:00:46,244 --> 00:00:49,444
And I have a PhD from Princeton University,

17
00:00:49,533 --> 00:00:51,577
a lot of my work that I do is

18
00:00:51,688 --> 00:00:53,133
applied Machine Learning and it's

19
00:00:53,170 --> 00:00:56,080
applied to problems in the electric power industry and healthcare

20
00:00:56,177 --> 00:00:57,933
and in computational criminology.

21
00:00:58,777 --> 00:01:04,555
Hi and I'm Steve Elston, I'm a co-founder and prinicipal consultant

22
00:01:04,644 --> 00:01:08,866
at a Data Science consultancy in Seattle called Quantia Analytics.

23
00:01:09,022 --> 00:01:11,866
I've been working in predictive analytics

24
00:01:12,022 --> 00:01:15,540
and machine learning for several decades now.

25
00:01:15,666 --> 00:01:21,555
I've been a long-term R(S/SPLUS) Python user and developer,

26
00:01:21,666 --> 00:01:24,133
started using S when it was

27
00:01:24,240 --> 00:01:26,750
Bell Labs project and of course more

28
00:01:26,860 --> 00:01:30,200
you know in recent decade moved to R like everybody else.

29
00:01:30,311 --> 00:01:35,333
I'm currently an advisor on Azure Machine Learning

30
00:01:35,444 --> 00:01:40,577
and some other analytics products to Microsoft and I've woked

31
00:01:40,666 --> 00:01:42,044
in a variety of industries:

32
00:01:42,133 --> 00:01:44,955
payment fraud prevention,

33
00:01:45,044 --> 00:01:48,177
telecommunications, capital markets

34
00:01:48,288 --> 00:01:52,555
including things like cred-- market and credit risk models,

35
00:01:54,022 --> 00:02:00,240
clearing and collateral management and also worked in

36
00:02:00,355 --> 00:02:02,310
several industrial areas such as

37
00:02:02,444 --> 00:02:05,270
forecasting for logistics management.

38
00:02:05,377 --> 00:02:08,555
And I have a PhD also from Princeton University

39
00:02:08,644 --> 00:02:10,311
and mine is in Geophysics.

40
00:02:11,511 --> 00:02:14,244
Now when I first learned about Machine Learning

41
00:02:14,355 --> 00:02:15,577
I thought it was magic.

42
00:02:15,711 --> 00:02:18,155
A way for computers to predict the future

43
00:02:18,244 --> 00:02:19,777
just by seeing the past.

44
00:02:19,888 --> 00:02:22,622
And you know, it's a way for computers to learn on their own

45
00:02:22,733 --> 00:02:24,533
how to solve problems that I can't solve

46
00:02:24,622 --> 00:02:26,400
and that's exactly what's going on.

47
00:02:26,555 --> 00:02:28,460
Computers are learning, just from

48
00:02:28,555 --> 00:02:30,133
observing what's happened in the past

49
00:02:30,200 --> 00:02:32,333
but it's nothing like magic.

50
00:02:32,890 --> 00:02:36,590
Now Machine Learning in addition to being a really useful tool box

51
00:02:36,910 --> 00:02:39,530
for industrial applications.

52
00:02:39,600 --> 00:02:42,970
It also gives you a perspective about the way your mind works.

53
00:02:43,580 --> 00:02:47,610
So, let's say that I asked you, why you could learn and why a computer can't?

54
00:02:47,970 --> 00:02:49,700
Well, what would you say ?

55
00:02:49,840 --> 00:02:53,420
Would you say it's because you've-- you've seen more of the world,

56
00:02:53,710 --> 00:02:55,190
then a computer has.

57
00:02:55,570 --> 00:02:57,930
I mean, I think that's not particularly true anymore

58
00:02:57,980 --> 00:03:00,360
because we have lots of pictures and video and sound now

59
00:03:00,390 --> 00:03:02,050
that we could feed to any computer.

60
00:03:03,650 --> 00:03:07,550
Is it because there are more an actions in your brain than in a computer?

61
00:03:08,150 --> 00:03:10,590
Well, that might be part of it but lots of creatures with much

62
00:03:10,660 --> 00:03:14,350
smaller brains than my computer can still learn, but that's not it.

63
00:03:15,660 --> 00:03:19,220
Maybe you could argue that a brain is more flexible in some ways than a computer.

64
00:03:19,420 --> 00:03:23,090
Maybe you could think your brain is somehow more open to identifying

65
00:03:23,210 --> 00:03:25,740
new types of patterns than your computer.

66
00:03:25,830 --> 00:03:27,570
And that's why you can learn perhaps.

67
00:03:27,860 --> 00:03:30,540
The interesting thing is that actually that's not

68
00:03:30,690 --> 00:03:33,010
quite the way it is, In fact it's sort of the opposite.

69
00:03:33,200 --> 00:03:36,090
Your brain is really good at identifying only

70
00:03:36,220 --> 00:03:37,850
certain kinds of patterns.

71
00:03:38,070 --> 00:03:40,920
In fact these are the types of patterns that it's

72
00:03:40,990 --> 00:03:43,480
that it's expecting. The fact that

73
00:03:43,640 --> 00:03:47,670
humans can learn, is not so much a consequence of

74
00:03:47,820 --> 00:03:49,880
so much of the human brain being flexible.

75
00:03:49,990 --> 00:03:53,210
As it is of the human brain being inflexible

76
00:03:53,290 --> 00:03:56,970
being wired to identify exactly the types of patterns

77
00:03:57,100 --> 00:03:58,430
that it comes across.

78
00:03:58,710 --> 00:04:03,500
Right. Natural images, real sounds, patterns of behavior,

79
00:04:04,010 --> 00:04:08,230
these are-- you know these are things that we're really good at identifying.

80
00:04:08,510 --> 00:04:12,760
Humans are absolutely awful at identifying patterns in large databases.

81
00:04:13,060 --> 00:04:16,180
Right, we can't-- we just can't learn in some settings,

82
00:04:16,550 --> 00:04:19,170
and what enable it-- what enables us to learn

83
00:04:19,260 --> 00:04:21,180
in the settings we can learn in

84
00:04:21,350 --> 00:04:23,580
is the way that our brains are wired.

85
00:04:23,650 --> 00:04:26,510
It's the structure in our mind, it's not the flexibility,

86
00:04:26,870 --> 00:04:30,050
it's the limited flexibility it sets that structure.

87
00:04:30,610 --> 00:04:33,140
Okay, so what is the field of Machine Learning exactly ?

88
00:04:33,510 --> 00:04:36,750
It completely revolves around setting up

89
00:04:36,860 --> 00:04:42,850
structures in the computer that limit its flexibility and allow it to learn.

90
00:04:43,190 --> 00:04:46,870
Okay, setting up these structures is really a form of

91
00:04:47,000 --> 00:04:50,350
statistical modeling and that's what we're going to do in this course.

92
00:04:50,850 --> 00:04:53,130
And once you can teach a computer to learn,

93
00:04:53,460 --> 00:04:56,540
there are a huge number of applications that you can use it on.

94
00:04:58,300 --> 00:05:01,660
OK, now as I mentioned humans are lousy

95
00:05:01,750 --> 00:05:03,600
at finding patterns and large databases.

96
00:05:03,730 --> 00:05:06,950
And so here are some of the applications that we're working on my lab

97
00:05:07,060 --> 00:05:09,040
that use large databases in Machine Learning.

98
00:05:09,170 --> 00:05:13,260
And in all of these applications the answer is--

99
00:05:13,370 --> 00:05:15,200
is really in the data. It really is,

100
00:05:15,330 --> 00:05:17,110
and by providing the computer with

101
00:05:17,200 --> 00:05:20,020
the proper Machine Learning structure to find important patterns,

102
00:05:20,130 --> 00:05:23,400
we can really make headway into societal problems.

103
00:05:23,480 --> 00:05:25,660
For instance, we've been looking at power grid failures

104
00:05:25,770 --> 00:05:29,330
and personalized advertising and healthcare applications.

105
00:05:30,330 --> 00:05:34,020
So, why would you want to continue with this course?

106
00:05:34,110 --> 00:05:35,970
What-- what would should you expect to

107
00:05:36,040 --> 00:05:37,420
get out of this course?

108
00:05:37,530 --> 00:05:39,920
Well, first half its going to be a hands-on

109
00:05:40,080 --> 00:05:41,350
introduction to Machine Learning.

110
00:05:41,460 --> 00:05:45,170
We have some great labs laid out here, there's

111
00:05:45,260 --> 00:05:46,970
going to be demos, so you're going to gain some

112
00:05:47,060 --> 00:05:49,460
practical experience at working

113
00:05:49,570 --> 00:05:52,800
with data and applying Machine Learning algorithms

114
00:05:52,950 --> 00:05:58,330
of various types to those data. We're going to look at-- actually

115
00:05:58,400 --> 00:06:01,880
all the major focus areas in Machine Learning.

116
00:06:02,000 --> 00:06:04,130
So we'll cover a broad variety

117
00:06:04,240 --> 00:06:07,460
of algorithms, methods and techniques.

118
00:06:07,550 --> 00:06:10,970
We hope that as you go along here as you work on these examples

119
00:06:11,110 --> 00:06:14,000
as you listen to the theory lectures, you start to build a

120
00:06:14,150 --> 00:06:17,130
some intuition around analytics and Machine Learning,

121
00:06:17,220 --> 00:06:23,510
and how it all fits together and mostly get an intuition

122
00:06:23,640 --> 00:06:25,420
of what's a useful result,

123
00:06:25,530 --> 00:06:28,250
what's adding value and what's going

124
00:06:28,370 --> 00:06:32,110
in the direction you or say your bosses want you to go.

125
00:06:32,200 --> 00:06:36,330
And we're going to minimize the math, there's not going to be any heavy theory

126
00:06:36,440 --> 00:06:38,120
so if you have a little-- if you

127
00:06:38,220 --> 00:06:39,710
remember a little bit of calculus,

128
00:06:39,770 --> 00:06:42,440
and some minimal linear algebra it should be good to go here.

129
00:06:42,530 --> 00:06:45,750
So we hope you get a lot out of this course

130
00:06:45,880 --> 00:06:49,420
and we're looking forward to presenting it and I think

131
00:06:49,550 --> 00:06:51,640
it's going to be a really great informative class

132
00:06:51,730 --> 00:06:57,110
to get yourself bootstrapped into the wonderful world of Machine Learning.

