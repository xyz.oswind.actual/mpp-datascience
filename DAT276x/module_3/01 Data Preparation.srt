0
00:00:00,360 --> 00:00:02,550
>> Hi and welcome.

1
00:00:02,550 --> 00:00:04,030
In this video, I'm going to talk about

2
00:00:04,030 --> 00:00:07,280
Data Preparation and give you some ideas of

3
00:00:07,280 --> 00:00:09,080
why Data Preparation is absolutely

4
00:00:09,080 --> 00:00:12,140
vital if you want to succeed at machine learning,

5
00:00:12,140 --> 00:00:14,750
and give you some ideas of methods we

6
00:00:14,750 --> 00:00:18,410
use to prepare data.

7
00:00:18,410 --> 00:00:20,330
So, we get good machine learning results

8
00:00:20,330 --> 00:00:22,655
and that's really what it comes down to.

9
00:00:22,655 --> 00:00:24,770
So, Data Preparation is

10
00:00:24,770 --> 00:00:28,190
a key step in the machine learning pipeline.

11
00:00:28,250 --> 00:00:31,000
Our goal is simple, we want to ensure that

12
00:00:31,000 --> 00:00:32,290
the machine learning algorithm

13
00:00:32,290 --> 00:00:34,060
that we choose whatever it is,

14
00:00:34,060 --> 00:00:36,870
works in an optimal manner

15
00:00:36,870 --> 00:00:39,150
and without good Data Preparation

16
00:00:39,150 --> 00:00:40,360
that's not going to happen.

17
00:00:40,360 --> 00:00:42,360
So, Data Preparation is

18
00:00:42,360 --> 00:00:44,210
vital to good machine learning performance.

19
00:00:44,210 --> 00:00:46,230
You can imagine if you have a lot of errors

20
00:00:46,230 --> 00:00:49,870
or other data weirdness,

21
00:00:49,870 --> 00:00:51,180
your machine learning model,

22
00:00:51,180 --> 00:00:53,610
it's the old garbage in garbage out problem.

23
00:00:53,610 --> 00:00:54,720
It's just not going to work.

24
00:00:54,720 --> 00:00:56,790
So, you've got to really plan

25
00:00:56,790 --> 00:00:59,805
on spending time and effort on Data Prep.

26
00:00:59,805 --> 00:01:03,060
So, there's a saw that cuts both ways here.

27
00:01:03,060 --> 00:01:06,955
Good data preparation can allow even very simple,

28
00:01:06,955 --> 00:01:08,175
maybe not so great

29
00:01:08,175 --> 00:01:10,335
machine learning algorithms to work well.

30
00:01:10,335 --> 00:01:13,610
If the data is well-prepared, well cleaned up,

31
00:01:13,610 --> 00:01:15,465
the features are well selected,

32
00:01:15,465 --> 00:01:17,835
you can do really well

33
00:01:17,835 --> 00:01:21,080
sometimes with just simple methods.

34
00:01:21,080 --> 00:01:22,820
On the other hand,

35
00:01:22,820 --> 00:01:24,905
if you throw in data that

36
00:01:24,905 --> 00:01:28,460
hasn't been well prepared toward

37
00:01:28,460 --> 00:01:30,590
even the most sophisticated

38
00:01:30,590 --> 00:01:34,370
supposedly highest performing machine algorithms,

39
00:01:34,370 --> 00:01:38,355
you're going to get poor results even at best and so,

40
00:01:38,355 --> 00:01:39,965
just keep that in mind that

41
00:01:39,965 --> 00:01:41,960
good data preparation often makes

42
00:01:41,960 --> 00:01:46,190
more difference than the algorithm you're actually using.

43
00:01:46,190 --> 00:01:47,980
So, what are the steps?

44
00:01:47,980 --> 00:01:49,960
How do we go about this and I can't

45
00:01:49,960 --> 00:01:52,000
give you an exact linear recipe.

46
00:01:52,000 --> 00:01:53,380
I can just tell you there are steps

47
00:01:53,380 --> 00:01:55,180
and that it's iterative and

48
00:01:55,180 --> 00:01:56,710
you should plan on trying lots of

49
00:01:56,710 --> 00:01:58,550
things and that some stuffs.

50
00:01:58,550 --> 00:01:59,740
Sometimes, you're going to make

51
00:01:59,740 --> 00:02:01,705
mistakes and have to backtrack.

52
00:02:01,705 --> 00:02:04,285
So first off, you need to do exploration.

53
00:02:04,285 --> 00:02:06,430
We've already talked a lot about exploration of

54
00:02:06,430 --> 00:02:09,130
the data and through exploration,

55
00:02:09,130 --> 00:02:10,390
you understand the relationships,

56
00:02:10,390 --> 00:02:13,750
but you also will detect the problems with the data and

57
00:02:13,750 --> 00:02:15,280
that's what you need to clean up

58
00:02:15,280 --> 00:02:17,895
before you start doing any ML.

59
00:02:17,895 --> 00:02:19,270
So, what are the things?

60
00:02:19,270 --> 00:02:21,770
So, we need to remove duplicates.

61
00:02:21,770 --> 00:02:25,190
We need to treat missing values.

62
00:02:25,190 --> 00:02:28,510
We need to treat errors and outliers,

63
00:02:28,510 --> 00:02:32,150
scale the features, split the dataset,

64
00:02:32,150 --> 00:02:35,490
and then we go

65
00:02:35,490 --> 00:02:38,130
back and we do more visualization to make sure

66
00:02:38,130 --> 00:02:41,550
that nothing's gone wrong with all these steps and that

67
00:02:41,550 --> 00:02:43,260
we haven't missed something that we

68
00:02:43,260 --> 00:02:45,240
need to do further preparation.

69
00:02:45,240 --> 00:02:47,100
So that's our general outline of

70
00:02:47,100 --> 00:02:49,230
the steps and they're not

71
00:02:49,230 --> 00:02:52,360
necessarily always done in that order.

