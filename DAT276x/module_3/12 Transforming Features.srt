0
00:00:00,950 --> 00:00:05,170
>> So, let's talk about a really simple example

1
00:00:05,170 --> 00:00:06,380
which is transforming

2
00:00:06,380 --> 00:00:10,840
features and why would you do that?

3
00:00:10,840 --> 00:00:12,830
What are we trying to gain?

4
00:00:12,830 --> 00:00:14,650
Well, we may want to improve

5
00:00:14,650 --> 00:00:16,330
their distribution properties for

6
00:00:16,330 --> 00:00:17,890
many machine learning algorithms

7
00:00:17,890 --> 00:00:20,225
having distributions that are closer to

8
00:00:20,225 --> 00:00:23,840
normal distributions may be an advantage.

9
00:00:23,840 --> 00:00:28,840
So there's ways we can transform the feature that may be

10
00:00:28,840 --> 00:00:31,375
skewed or have some other distribution

11
00:00:31,375 --> 00:00:34,020
get closer than normal.

12
00:00:34,020 --> 00:00:37,130
We may also find some transformations that

13
00:00:37,130 --> 00:00:40,070
make that feature more covariate with the label.

14
00:00:40,070 --> 00:00:42,005
Notice I don't say more highly correlated

15
00:00:42,005 --> 00:00:43,670
because that relationship could be highly

16
00:00:43,670 --> 00:00:47,180
nonlinear which may have low correlation,

17
00:00:47,180 --> 00:00:53,280
but still be highly covariate in some transformed space.

18
00:00:53,280 --> 00:00:58,010
So what are some common single feature transformations?

19
00:00:58,010 --> 00:01:01,540
So these are operations we can apply to a single feature.

20
00:01:01,540 --> 00:01:04,680
We can use logarithms,

21
00:01:04,680 --> 00:01:07,060
exponential, square, square roots.

22
00:01:07,060 --> 00:01:09,075
We can compute the variance,

23
00:01:09,075 --> 00:01:11,380
the mean, the median, etc.

24
00:01:11,380 --> 00:01:17,690
Those could all be ways to transform a feature.

25
00:01:17,690 --> 00:01:20,780
We can do other operations that

26
00:01:20,780 --> 00:01:24,830
use several values of the feature.

27
00:01:24,830 --> 00:01:27,770
The above like log,

28
00:01:27,770 --> 00:01:29,760
exponential, square, square root,

29
00:01:29,760 --> 00:01:31,420
just use a single value.

30
00:01:31,420 --> 00:01:33,140
But we could take the difference

31
00:01:33,140 --> 00:01:35,240
between one feature in the

32
00:01:35,240 --> 00:01:37,730
previous or a feature value in the previous one

33
00:01:37,730 --> 00:01:38,900
especially if they're like time

34
00:01:38,900 --> 00:01:40,730
ordered or something like that.

35
00:01:40,730 --> 00:01:42,550
We could use a cumulative sum.

36
00:01:42,550 --> 00:01:45,930
Again, generally, do that with time ordered data.

37
00:01:46,480 --> 00:01:51,770
Something to keep in mind, almost all the transformations

38
00:01:51,770 --> 00:01:54,770
I'm talking about here are nonlinear.

39
00:01:54,770 --> 00:01:58,970
So, nonlinear transformed features

40
00:01:58,970 --> 00:02:01,490
are not going to be colinear.

41
00:02:01,490 --> 00:02:03,020
That is they're not going to necessarily have

42
00:02:03,020 --> 00:02:05,440
high correlation with the original one.

43
00:02:05,440 --> 00:02:10,145
If you have a numeric feature

44
00:02:10,145 --> 00:02:12,504
and it's square value squared,

45
00:02:12,504 --> 00:02:14,885
they're not going to be that correlated.

46
00:02:14,885 --> 00:02:17,780
So, you don't have to worry about introducing

47
00:02:17,780 --> 00:02:20,550
new colinearity between the features so much.

48
00:02:20,550 --> 00:02:22,320
So, people often worried about

49
00:02:22,320 --> 00:02:24,325
that point that's why I'm talking about it.

50
00:02:24,325 --> 00:02:28,540
But generally, that's not such a big problem.

