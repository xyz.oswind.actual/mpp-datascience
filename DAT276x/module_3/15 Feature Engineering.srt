0
00:00:00,000 --> 00:00:04,620
>> So, now I'm going to talk about Feature Engineering,

1
00:00:04,620 --> 00:00:06,125
which is a vast topic.

2
00:00:06,125 --> 00:00:07,220
A lot of creativity,

3
00:00:07,220 --> 00:00:08,250
a lot of human energy,

4
00:00:08,250 --> 00:00:09,895
goes into feature engineering.

5
00:00:09,895 --> 00:00:11,340
In this demo, I'm only going to show

6
00:00:11,340 --> 00:00:12,895
you a couple of techniques.

7
00:00:12,895 --> 00:00:15,570
But I do want to give you a flavor

8
00:00:15,570 --> 00:00:18,750
for what sorts of thinking and

9
00:00:18,750 --> 00:00:20,925
processes you can use to

10
00:00:20,925 --> 00:00:23,010
create features that might be

11
00:00:23,010 --> 00:00:25,560
more predictive of your label.

12
00:00:25,560 --> 00:00:28,800
The difference between good features and

13
00:00:28,800 --> 00:00:30,225
bad features can be night

14
00:00:30,225 --> 00:00:31,760
and day in terms of model performance.

15
00:00:31,760 --> 00:00:33,630
Often, feature engineering is more

16
00:00:33,630 --> 00:00:36,210
important than the exact choice of model or

17
00:00:36,210 --> 00:00:38,040
optimizing the hyper parameters of

18
00:00:38,040 --> 00:00:41,230
your model or anything like that. So, keep that in mind.

19
00:00:41,230 --> 00:00:43,620
A lot of energy goes into Feature Engineering,

20
00:00:43,620 --> 00:00:47,160
a lot of creativity, sometimes a lot of domain knowledge.

21
00:00:47,160 --> 00:00:50,020
But here's two examples I'd like to show you,

22
00:00:50,020 --> 00:00:51,905
one for categorical variables

23
00:00:51,905 --> 00:00:54,770
and one for numeric variables.

24
00:00:54,920 --> 00:00:58,629
So, let's start with a categorical variable,

25
00:00:58,629 --> 00:01:01,560
a common feature engineering practice,

26
00:01:01,560 --> 00:01:03,550
which is to aggregate categories.

27
00:01:03,550 --> 00:01:06,420
If you have too many categories or categories with

28
00:01:06,420 --> 00:01:12,150
very few members, it's statistically problematic.

29
00:01:12,150 --> 00:01:15,830
So, let's look at this for the auto prices,

30
00:01:15,830 --> 00:01:18,565
the number of cylinders in the cars.

31
00:01:18,565 --> 00:01:21,420
You see, there's only four cars

32
00:01:21,420 --> 00:01:23,205
with eight cylinder engines,

33
00:01:23,205 --> 00:01:25,590
one car with three cylinder engine

34
00:01:25,590 --> 00:01:28,685
and one car with a twelve cylinder engine.

35
00:01:28,685 --> 00:01:32,820
Additionally, five and six cylinder cars

36
00:01:32,820 --> 00:01:36,940
don't have too many members either.

37
00:01:37,310 --> 00:01:42,590
So, we have seen previously when we did

38
00:01:42,590 --> 00:01:44,780
the visualization that the prices for

39
00:01:44,780 --> 00:01:46,160
example of twelve and eight

40
00:01:46,160 --> 00:01:48,025
cylinder cars are pretty similar,

41
00:01:48,025 --> 00:01:50,960
of five and six cylinder cars are pretty similar,

42
00:01:50,960 --> 00:01:54,170
and the three and four cylinder cars were pretty similar.

43
00:01:54,170 --> 00:01:57,680
So, we're going to build this little list

44
00:01:57,680 --> 00:02:03,110
here which says, three = three_four,

45
00:02:03,110 --> 00:02:08,720
four = four_three, five = five_six,

46
00:02:08,720 --> 00:02:10,460
six = five_six, eight

47
00:02:10,460 --> 00:02:13,590
= eight_twelve, twelve = eight_twelve.

48
00:02:18,410 --> 00:02:21,060
So, we create another list,

49
00:02:21,060 --> 00:02:26,460
and we're going to loop over the number of cylinders in

50
00:02:26,460 --> 00:02:34,780
the auto prices and take the value from this list.

51
00:02:35,640 --> 00:02:38,400
You see with the double brackets

52
00:02:38,400 --> 00:02:40,190
we get the values out of the list.

53
00:02:40,190 --> 00:02:42,470
So, you can think of this almost like this

54
00:02:42,470 --> 00:02:45,470
is the name of that element in the list.

55
00:02:45,470 --> 00:02:47,180
So, it's like we're using it like a key

56
00:02:47,180 --> 00:02:49,430
here and this is the value.

57
00:02:49,430 --> 00:02:51,470
So, we'll get the value

58
00:02:51,470 --> 00:02:54,300
out and we'll have a look at how we do it,

59
00:02:54,300 --> 00:02:57,580
and we'll make a frequency table at the end.

60
00:02:57,840 --> 00:03:00,610
So, now we have five,

61
00:03:00,610 --> 00:03:01,970
eight and twelve cylinder cars.

62
00:03:01,970 --> 00:03:03,640
Well, it's still a small sample but

63
00:03:03,640 --> 00:03:05,650
it's better than one and four.

64
00:03:05,650 --> 00:03:08,620
We now have 34, five and six cylinder cars.

65
00:03:08,620 --> 00:03:13,195
That's a respectable number and still a whopping 156,

66
00:03:13,195 --> 00:03:14,650
three and four cylinder cars.

67
00:03:14,650 --> 00:03:17,260
They still dominate, but at least we have a little

68
00:03:17,260 --> 00:03:21,895
bit less in balance with some of these other categories.

69
00:03:21,895 --> 00:03:25,830
Let's just make a box plot and you'll

70
00:03:25,830 --> 00:03:32,085
see how well those categories do or don't overlap.

71
00:03:32,085 --> 00:03:33,880
Well, this looks pretty good.

72
00:03:33,880 --> 00:03:36,285
You see three and four cylinder cars,

73
00:03:36,285 --> 00:03:38,130
five and six cylinder cars,

74
00:03:38,130 --> 00:03:39,495
and eight and twelve cylinder cars.

75
00:03:39,495 --> 00:03:43,380
They all have fairly distinct ranges.

76
00:03:43,380 --> 00:03:46,235
None of the interquartile ranges overlap.

77
00:03:46,235 --> 00:03:50,130
So, the chances are good that in

78
00:03:50,130 --> 00:03:53,250
this new engineered feature is actually going to

79
00:03:53,250 --> 00:03:57,675
be good at predicting the price range of automobiles.

80
00:03:57,675 --> 00:04:02,850
Okay. So, let's talk about numeric variables.

81
00:04:02,850 --> 00:04:06,980
Generally, we can do

82
00:04:06,980 --> 00:04:08,870
more complicated transformations but we're

83
00:04:08,870 --> 00:04:09,940
just going to look at

84
00:04:09,940 --> 00:04:12,500
a direct mathematical transformation

85
00:04:12,500 --> 00:04:14,590
on a single unit variable.

86
00:04:14,590 --> 00:04:18,169
So, let's focus on our label,

87
00:04:18,169 --> 00:04:19,730
which is price, and of course we

88
00:04:19,730 --> 00:04:21,530
care a lot about our label.

89
00:04:21,530 --> 00:04:25,670
So we're going to create a histogram and density plot

90
00:04:25,670 --> 00:04:28,280
overlaid for label and just have a look

91
00:04:28,280 --> 00:04:30,875
at what the distribution looks like.

92
00:04:30,875 --> 00:04:34,795
That's often a good first step for understanding numeric.

93
00:04:34,795 --> 00:04:37,655
We see that it's strongly right skewed.

94
00:04:37,655 --> 00:04:39,560
There's this long tail,

95
00:04:39,560 --> 00:04:41,860
it drops pretty quickly

96
00:04:41,860 --> 00:04:44,120
to the low price and that makes sense.

97
00:04:44,120 --> 00:04:48,490
No car can have a price of less than zero.

98
00:04:48,490 --> 00:04:50,380
Even a scrap value,

99
00:04:50,380 --> 00:04:54,980
they're probably worth something and there's

100
00:04:54,980 --> 00:04:57,020
just a few really low price cars in

101
00:04:57,020 --> 00:04:59,750
this long tail of presumably luxury cars.

102
00:04:59,750 --> 00:05:02,330
So, my first thought when I see

103
00:05:02,330 --> 00:05:05,510
a distribution like this is maybe it's closer to log,

104
00:05:05,510 --> 00:05:08,120
normally distributed than normally distributed.

105
00:05:08,120 --> 00:05:12,350
So, we'll try a log transformation here.

106
00:05:12,350 --> 00:05:14,150
So, we'll just apply

107
00:05:14,150 --> 00:05:16,310
the log function to that and we'll create

108
00:05:16,310 --> 00:05:17,915
a new column called

109
00:05:17,915 --> 00:05:22,020
log_price and we'll plot the histogram.

110
00:05:24,840 --> 00:05:29,830
Sure enough, it's not really a normal distribution still.

111
00:05:29,830 --> 00:05:31,660
It's still a bit right skewed

112
00:05:31,660 --> 00:05:33,910
but it's a whole lot more symmetric.

113
00:05:33,910 --> 00:05:35,935
So, we're definitely making progress.

114
00:05:35,935 --> 00:05:37,360
I don't think there's any simple

115
00:05:37,360 --> 00:05:38,920
transformation that's going to make

116
00:05:38,920 --> 00:05:42,145
this symmetric given that it's almost multi-modal,

117
00:05:42,145 --> 00:05:43,300
but this isn't bad.

118
00:05:43,300 --> 00:05:47,230
It's certainly more symmetric and probably

119
00:05:47,230 --> 00:05:54,830
an improvement over this original raw distribution.

120
00:05:54,830 --> 00:05:58,240
So, that's a bit of feature engineering for

121
00:05:58,240 --> 00:06:01,700
categorical variables and numeric variables.

122
00:06:01,700 --> 00:06:02,840
Of course, sometimes you have

123
00:06:02,840 --> 00:06:04,490
interactions between the two

124
00:06:04,490 --> 00:06:07,630
and you can do other kinds of feature engineering,

125
00:06:07,630 --> 00:06:09,820
and keep that in mind,

126
00:06:09,820 --> 00:06:11,360
there's lots of dimensions to

127
00:06:11,360 --> 00:06:13,250
this problem of feature engineering,

128
00:06:13,250 --> 00:06:16,185
and a lot you can gain in getting better machine learning

129
00:06:16,185 --> 00:06:18,140
model performance if you are

130
00:06:18,140 --> 00:06:21,750
persistent and clever and creative.

