0
00:00:00,000 --> 00:00:03,940
>> So, let's talk about scaling and scaling

1
00:00:03,940 --> 00:00:06,580
is a really important step

2
00:00:06,580 --> 00:00:08,735
before you do machine learning.

3
00:00:08,735 --> 00:00:13,600
Improper scaling biases machine learning training.

4
00:00:13,600 --> 00:00:16,570
What you don't want to have is a feature that

5
00:00:16,570 --> 00:00:19,930
has a large numeric range dominating the training.

6
00:00:19,930 --> 00:00:23,560
Say, I have the age of someone and their salary.

7
00:00:23,560 --> 00:00:25,720
Now, we hope their salary is maybe

8
00:00:25,720 --> 00:00:29,510
a couple of order of magnitude larger than their age.

9
00:00:29,850 --> 00:00:33,430
You don't want that to be a more important feature,

10
00:00:33,430 --> 00:00:34,870
just because it has this bigger

11
00:00:34,870 --> 00:00:37,195
numerical range than their age.

12
00:00:37,195 --> 00:00:39,160
There's no reason to believe at the beginning

13
00:00:39,160 --> 00:00:42,100
that that should be, so we scale.

14
00:00:43,890 --> 00:00:46,060
If you don't, you're going to

15
00:00:46,060 --> 00:00:47,610
get some sort of strange skew,

16
00:00:47,610 --> 00:00:49,630
or bias in your model parameters.

17
00:00:49,630 --> 00:00:51,190
How can we scale? I'm going

18
00:00:51,190 --> 00:00:52,980
to just tell you about two methods.

19
00:00:52,980 --> 00:00:56,420
There's many others, but the two most commonly used.

20
00:00:56,420 --> 00:00:59,250
One that we use constantly is called Z-Score,

21
00:00:59,250 --> 00:01:03,135
and basically we remove,

22
00:01:03,135 --> 00:01:06,505
we demean, so the mean becomes zero,

23
00:01:06,505 --> 00:01:08,400
and we divide by the standard deviation,

24
00:01:08,400 --> 00:01:09,720
so the standard deviation is one.

25
00:01:09,720 --> 00:01:12,210
This is sometimes called standardization where

26
00:01:12,210 --> 00:01:15,890
Z-Score scaling.

27
00:01:15,990 --> 00:01:18,830
It's good if the distribution,

28
00:01:18,830 --> 00:01:21,740
its numeric distribution is approximately normal,

29
00:01:21,740 --> 00:01:23,190
and I mean really approximately.

30
00:01:23,190 --> 00:01:24,905
It doesn't have to be spot on.

31
00:01:24,905 --> 00:01:29,150
But, it shouldn't be five modes or something.

32
00:01:29,150 --> 00:01:31,360
If you have other distributions

33
00:01:31,360 --> 00:01:32,710
that are really strange looking,

34
00:01:32,710 --> 00:01:35,265
that you can't pretend are normal,

35
00:01:35,265 --> 00:01:37,570
Min-Max normalization might be

36
00:01:37,570 --> 00:01:40,410
the best one, or Min-Max scaling.

37
00:01:42,690 --> 00:01:45,580
The idea is simple, you simply say, "Okay,

38
00:01:45,580 --> 00:01:49,605
I'm going to do a transformation of the values,

39
00:01:49,605 --> 00:01:52,125
so that they're in a range, zero to one,

40
00:01:52,125 --> 00:01:56,210
or minus one to one, or something like that."

41
00:01:56,210 --> 00:02:01,090
But, it fails with outliers very severely.

42
00:02:01,090 --> 00:02:02,830
You can see, if you had an outlier that was

43
00:02:02,830 --> 00:02:04,775
a hundred times all the other values.

44
00:02:04,775 --> 00:02:06,110
All those other values get

45
00:02:06,110 --> 00:02:09,480
scrunched into maybe a range of zero to 0.1,

46
00:02:09,480 --> 00:02:11,390
and you have your outlier out at one.

47
00:02:11,390 --> 00:02:13,515
That's kind of ridiculous.

48
00:02:13,515 --> 00:02:17,580
So, be careful when you use Min-Max scaling.

49
00:02:17,580 --> 00:02:20,540
That's a general thing.

50
00:02:20,540 --> 00:02:23,300
You want to treat errors, missing values,

51
00:02:23,300 --> 00:02:24,920
things like that before

52
00:02:24,920 --> 00:02:26,300
you apply scaling because you don't want to

53
00:02:26,300 --> 00:02:30,900
contaminate your scaling with the errors and outliers.

