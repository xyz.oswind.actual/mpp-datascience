0
00:00:00,680 --> 00:00:03,510
>> So, let me try to wrap this up,

1
00:00:03,510 --> 00:00:05,485
it's a big topic

2
00:00:05,485 --> 00:00:07,360
and we're only spending a few minutes on it,

3
00:00:07,360 --> 00:00:10,480
but let me leave you with a few thoughts here.

4
00:00:10,480 --> 00:00:13,150
Finding good predictive features

5
00:00:13,150 --> 00:00:14,920
are the key to success in machine learning.

6
00:00:14,920 --> 00:00:18,760
So, if you can if you can figure out better ways to

7
00:00:18,760 --> 00:00:21,280
transform or engineer new features

8
00:00:21,280 --> 00:00:23,065
that are more predictive,

9
00:00:23,065 --> 00:00:24,690
your machine learning models,

10
00:00:24,690 --> 00:00:26,115
no matter what those models are,

11
00:00:26,115 --> 00:00:28,405
just simply going to work better.

12
00:00:28,405 --> 00:00:29,985
It's an iterative process

13
00:00:29,985 --> 00:00:31,290
you're going to try lots of things,

14
00:00:31,290 --> 00:00:32,400
some things are going to work,

15
00:00:32,400 --> 00:00:35,665
some things aren't going to work, but expect that.

16
00:00:35,665 --> 00:00:37,730
Keep the things that work.

17
00:00:37,730 --> 00:00:42,420
Try lots of ideas and you'll make progress.

18
00:00:42,420 --> 00:00:44,870
You can use data

19
00:00:44,870 --> 00:00:49,245
exploration to help up identify those opportunities,

20
00:00:49,245 --> 00:00:52,355
create ideas, test ideas.

21
00:00:52,355 --> 00:00:55,425
You're going to need to test lots of ideas,

22
00:00:55,425 --> 00:00:58,290
and keep in mind that it is iterative.

23
00:00:58,290 --> 00:00:59,985
You're going to test your ideas.

24
00:00:59,985 --> 00:01:03,850
You should fail fast and keep what works.

