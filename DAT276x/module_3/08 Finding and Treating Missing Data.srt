0
00:00:01,580 --> 00:00:03,870
>> Hello and welcome.

1
00:00:03,870 --> 00:00:08,020
So, we need to go through some steps

2
00:00:08,020 --> 00:00:09,610
whenever we're going to build

3
00:00:09,610 --> 00:00:12,565
machine learning models to prepare our data.

4
00:00:12,565 --> 00:00:15,040
If you don't go through these steps fairly

5
00:00:15,040 --> 00:00:18,070
systematically and sometimes have to backtrack,

6
00:00:18,070 --> 00:00:20,500
and redo a step when you find

7
00:00:20,500 --> 00:00:23,690
something new that is a problem with your data,

8
00:00:23,690 --> 00:00:26,510
you're not going to get good machine learning results.

9
00:00:26,510 --> 00:00:28,915
There's just no way that poorly prepared data

10
00:00:28,915 --> 00:00:31,865
can produce good machine learning results, okay?

11
00:00:31,865 --> 00:00:35,140
So, the first one of these steps is

12
00:00:35,140 --> 00:00:38,605
generally finding and treating missing values.

13
00:00:38,605 --> 00:00:41,815
Almost all data sets have missing values.

14
00:00:41,815 --> 00:00:44,560
It's a perennial problem.

15
00:00:44,560 --> 00:00:48,715
It never goes away and sometimes it's quite challenging.

16
00:00:48,715 --> 00:00:50,300
So, there's two steps, of course,

17
00:00:50,300 --> 00:00:51,490
find them and then,

18
00:00:51,490 --> 00:00:53,015
decide how to treat them.

19
00:00:53,015 --> 00:00:55,695
So, let's have a look at an example.

20
00:00:55,695 --> 00:00:58,470
So, I have on

21
00:00:58,470 --> 00:01:01,110
my screen here some code and I'm going to load

22
00:01:01,110 --> 00:01:04,245
the automotive price data set

23
00:01:04,245 --> 00:01:09,460
and we'll look at the first 20 rows of that.

24
00:01:10,800 --> 00:01:13,810
What you can notice is, for example,

25
00:01:13,810 --> 00:01:15,770
in the normalized losses column,

26
00:01:15,770 --> 00:01:19,560
there's a lot of question marks, okay?

27
00:01:19,560 --> 00:01:26,500
Elsewhere, if we scroll over in the price for example,

28
00:01:26,500 --> 00:01:28,230
which is our label, it's the thing we care

29
00:01:28,230 --> 00:01:30,710
most about, there's question marks.

30
00:01:30,710 --> 00:01:33,680
So, it turns out that whoever assembled

31
00:01:33,680 --> 00:01:37,290
this data set didn't use a null value,

32
00:01:37,290 --> 00:01:40,750
they used a character string question mark

33
00:01:40,750 --> 00:01:43,740
to indicate a missing value.

34
00:01:43,740 --> 00:01:45,510
So, we're going to have to deal with that.

35
00:01:45,510 --> 00:01:49,455
So, let's see if we can find those.

36
00:01:49,455 --> 00:01:51,450
So first of, I'm going to

37
00:01:51,450 --> 00:01:53,080
run this little bit of code here,

38
00:01:53,080 --> 00:01:57,415
so it's lapply over the auto prices.

39
00:01:57,415 --> 00:02:01,800
So, lapply treats the data frame as a list and then,

40
00:02:01,800 --> 00:02:05,280
I've got this anonymous function which says,

41
00:02:05,280 --> 00:02:09,000
are any of those values of X

42
00:02:09,000 --> 00:02:14,995
equal to this question mark and we'll just get a logical.

43
00:02:14,995 --> 00:02:18,200
So, you can see symbolizing,

44
00:02:18,200 --> 00:02:22,525
but normalized losses which we already know is true.

45
00:02:22,525 --> 00:02:23,995
And I keep going,

46
00:02:23,995 --> 00:02:28,930
body number of doors is true.

47
00:02:28,930 --> 00:02:32,300
And I keep going mostly false, and then,

48
00:02:32,300 --> 00:02:36,075
I get down to what should be numeric variables: bore,

49
00:02:36,075 --> 00:02:39,265
stroke, horsepower,

50
00:02:39,265 --> 00:02:43,360
peak.rpm, and of course our label price, those all true.

51
00:02:43,360 --> 00:02:46,250
So, they all have missing values.

52
00:02:46,280 --> 00:02:49,400
I'm going to run str on this.

53
00:02:49,400 --> 00:02:51,360
I just want you to see

54
00:02:51,360 --> 00:02:53,130
that normalized losses which

55
00:02:53,130 --> 00:02:55,820
should be numeric, is character.

56
00:02:55,820 --> 00:02:57,840
Price is character, even though,

57
00:02:57,840 --> 00:02:59,690
obviously the price should be numeric,

58
00:02:59,690 --> 00:03:03,090
the same with things like peak.rpm, horsepower.

59
00:03:03,090 --> 00:03:06,630
So, because there's this question mark being

60
00:03:06,630 --> 00:03:10,880
used as a missing value indicator,

61
00:03:10,880 --> 00:03:14,785
R was not able to coerce these.

62
00:03:14,785 --> 00:03:16,815
Obviously, numeric columns, obvious to

63
00:03:16,815 --> 00:03:18,900
us humans to numeric,

64
00:03:18,900 --> 00:03:21,640
because there was a character string there.

65
00:03:21,640 --> 00:03:25,955
So, the next question is how bad is this, okay?

66
00:03:25,955 --> 00:03:28,925
So, I'm going to loop over

67
00:03:28,925 --> 00:03:32,020
all the column names in my data

68
00:03:32,020 --> 00:03:35,150
set and I'm going

69
00:03:35,150 --> 00:03:38,795
to determine if it's a character column or not.

70
00:03:38,795 --> 00:03:41,230
Because I know from looking at

71
00:03:41,230 --> 00:03:43,555
this that and the fact that

72
00:03:43,555 --> 00:03:45,640
missing values are indicated as

73
00:03:45,640 --> 00:03:47,890
a question mark that they will be character columns.

74
00:03:47,890 --> 00:03:52,200
Then, I'm going to count by doing an ifelse.

75
00:03:52,200 --> 00:03:58,290
So, if the value is a question mark then,

76
00:03:58,290 --> 00:04:01,310
I assign one to it otherwise zero,

77
00:04:01,310 --> 00:04:04,210
so I sum over it and that will give me a count of

78
00:04:04,210 --> 00:04:08,215
the total number of missing values in that column.

79
00:04:08,215 --> 00:04:12,675
I make a little table here and I see there's

80
00:04:12,675 --> 00:04:19,420
41 missing values and normalized losses out of 190,

81
00:04:19,420 --> 00:04:24,860
205 cases, so it's pretty high number of missing values.

82
00:04:24,860 --> 00:04:27,490
Number of doors, two,

83
00:04:27,490 --> 00:04:31,105
four, four, two, two, four, okay?

84
00:04:31,105 --> 00:04:33,560
So, the question is now.

85
00:04:33,560 --> 00:04:36,250
So, we found missing values,

86
00:04:36,250 --> 00:04:39,010
we know how many missing values there are.

87
00:04:39,010 --> 00:04:41,690
Now, we have to decide how to treat them.

88
00:04:41,690 --> 00:04:44,110
Well, for normalized losses,

89
00:04:44,110 --> 00:04:46,840
my proposal is because there's such a high percentage,

90
00:04:46,840 --> 00:04:48,990
we simply eliminate that column.

91
00:04:48,990 --> 00:04:52,655
It's not very predictive of price anyway.

92
00:04:52,655 --> 00:04:55,870
So, getting rid of it is just the easy solution.

93
00:04:55,870 --> 00:04:59,275
The other thing we'll try is to treat these,

94
00:04:59,275 --> 00:05:04,580
here, is to simply remove the row.

95
00:05:04,580 --> 00:05:07,240
So, the code I've got here does just that.

96
00:05:07,240 --> 00:05:11,010
We set the normalized losses column to null,

97
00:05:11,010 --> 00:05:13,880
so that'll remove it from the data frame.

98
00:05:13,880 --> 00:05:17,730
For price, bore, stroke, horsepower, peak.rpm,

99
00:05:18,240 --> 00:05:21,135
we're just going to,

100
00:05:21,135 --> 00:05:23,030
first off we run through, and again,

101
00:05:23,030 --> 00:05:25,340
we use a anonymous function.

102
00:05:25,340 --> 00:05:28,340
So, it's just a function without a name and then,

103
00:05:28,340 --> 00:05:31,190
we do an ifelse X equals question mark.

104
00:05:31,190 --> 00:05:33,810
We're going to give an NA which is

105
00:05:33,810 --> 00:05:40,640
the R's missing value indicator or the value itself,

106
00:05:40,640 --> 00:05:43,385
if it's not coded as a question mark.

107
00:05:43,385 --> 00:05:47,640
Then, I can use this complete.cases function.

108
00:05:47,640 --> 00:05:51,435
So, complete.cases looks across all the rows and says,

109
00:05:51,435 --> 00:05:53,950
is that a complete case?

110
00:05:54,460 --> 00:05:58,625
Meaning, are there any missing values.

111
00:05:58,625 --> 00:06:03,200
So, I apply that to auto prices,

112
00:06:03,200 --> 00:06:06,685
I'm just applying it to these columns, okay?

113
00:06:06,685 --> 00:06:09,785
But, this gives complete case

114
00:06:09,785 --> 00:06:12,575
returns a logical for each row.

115
00:06:12,575 --> 00:06:15,760
So, by doing this selecting auto prices then,

116
00:06:15,760 --> 00:06:17,050
I get auto prices

117
00:06:17,050 --> 00:06:23,605
without the missing values in those columns.

118
00:06:23,605 --> 00:06:27,740
So and then, we'll just look at the dimension of that.

119
00:06:27,740 --> 00:06:35,350
Okay. So, we've gone from 205 rows and 27 columns.

120
00:06:35,350 --> 00:06:42,815
We've lost one column here and we have 195 cases left.

121
00:06:42,815 --> 00:06:46,515
So, we've lost 10 from 205 to 195.

122
00:06:46,515 --> 00:06:50,545
So, that's one example of looking for missing values.

123
00:06:50,545 --> 00:06:52,760
But, maybe in other cases there could

124
00:06:52,760 --> 00:06:55,125
be other ways the missing values are coded.

125
00:06:55,125 --> 00:06:57,860
That can sometimes take some creativity and

126
00:06:57,860 --> 00:06:59,480
maybe a few alterations to find

127
00:06:59,480 --> 00:07:01,865
missing values and treat them correctly.

128
00:07:01,865 --> 00:07:04,280
But, I hope this little demo has given you

129
00:07:04,280 --> 00:07:07,950
some idea how to go about getting started on that.

