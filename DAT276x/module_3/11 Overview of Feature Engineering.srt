0
00:00:00,000 --> 00:00:02,550
>> Hi and welcome.

1
00:00:02,550 --> 00:00:04,365
In this video I'm going to talk about

2
00:00:04,365 --> 00:00:06,390
a very important subject in

3
00:00:06,390 --> 00:00:07,710
machine learning which is feature

4
00:00:07,710 --> 00:00:09,390
engineering and feature engineering

5
00:00:09,390 --> 00:00:11,160
is a really broad subject.

6
00:00:11,160 --> 00:00:14,280
People spend like almost entire careers on

7
00:00:14,280 --> 00:00:16,140
feature engineering and I

8
00:00:16,140 --> 00:00:18,870
can't give you all the ins and outs of it.

9
00:00:18,870 --> 00:00:20,800
But I want to give you the basics,

10
00:00:20,800 --> 00:00:22,515
you understand what it's about

11
00:00:22,515 --> 00:00:24,600
and some techniques you can apply

12
00:00:24,600 --> 00:00:27,060
yourself when you're faced with problems with

13
00:00:27,060 --> 00:00:28,500
machine learning models and need

14
00:00:28,500 --> 00:00:30,645
to do some feature engineering.

15
00:00:30,645 --> 00:00:34,020
So, the general ideas

16
00:00:34,020 --> 00:00:35,760
good features are the key to

17
00:00:35,760 --> 00:00:37,800
good machine learning performance.

18
00:00:37,800 --> 00:00:40,170
If we have features that are highly predictive

19
00:00:40,170 --> 00:00:42,600
of the label we're trying to predict,

20
00:00:42,600 --> 00:00:45,225
we're going to get good machine learning performance.

21
00:00:45,225 --> 00:00:48,105
If not, it may just be

22
00:00:48,105 --> 00:00:49,935
noise garbage in garbage out

23
00:00:49,935 --> 00:00:51,840
and we may not be getting anywhere.

24
00:00:51,840 --> 00:00:54,805
Our goal like I said is to develop

25
00:00:54,805 --> 00:00:59,530
highly predictive features. Keep that in mind.

26
00:01:00,370 --> 00:01:03,280
Often times, when we get started on

27
00:01:03,280 --> 00:01:06,870
a new ML project the raw features we're presented

28
00:01:06,870 --> 00:01:09,940
aren't the best they just whatever happens to be in

29
00:01:09,940 --> 00:01:13,790
the data we've assembled from whatever sources.

30
00:01:13,790 --> 00:01:16,345
But that may not be

31
00:01:16,345 --> 00:01:18,090
the best way to view them

32
00:01:18,090 --> 00:01:20,010
just whatever raw forms they've come in.

33
00:01:20,010 --> 00:01:22,255
We've already talked about data preparation,

34
00:01:22,255 --> 00:01:24,490
but there may be fundamental issues

35
00:01:24,490 --> 00:01:28,150
about those features themselves that may not be the

36
00:01:28,150 --> 00:01:31,510
best and the thing to keep in mind is that

37
00:01:31,510 --> 00:01:34,540
good features allow simple machine learning

38
00:01:34,540 --> 00:01:36,510
algorithms to work really well.

39
00:01:36,510 --> 00:01:41,080
They're often doing good feature engineering even if

40
00:01:41,080 --> 00:01:43,690
you're not using the latest whiz bang state

41
00:01:43,690 --> 00:01:45,429
of the art ML algorithm,

42
00:01:45,429 --> 00:01:48,120
you can get really great results.

43
00:01:48,120 --> 00:01:49,890
On the other hand,

44
00:01:49,890 --> 00:01:52,450
if your features are just not that

45
00:01:52,450 --> 00:01:55,300
good they're they poorly predict your label,

46
00:01:55,300 --> 00:01:58,900
you can use the best machine learning algorithm

47
00:01:58,900 --> 00:02:00,340
for that situation and

48
00:02:00,340 --> 00:02:02,620
still you're going to get poor results.

49
00:02:02,620 --> 00:02:04,910
Feature engineering is really important,

50
00:02:04,910 --> 00:02:09,890
you can often gain more in terms of model performance by

51
00:02:09,890 --> 00:02:12,760
a better feature engineering than you can by trying

52
00:02:12,760 --> 00:02:17,050
some small variation in the algorithms themselves.

53
00:02:17,050 --> 00:02:20,150
So let's talk about the steps,

54
00:02:20,150 --> 00:02:22,550
again like a lot of these things in

55
00:02:22,550 --> 00:02:25,580
machine learning I can't give you the magic one,

56
00:02:25,580 --> 00:02:28,340
the exact recipe of how you go about this.

57
00:02:28,340 --> 00:02:29,480
But I can tell you there are

58
00:02:29,480 --> 00:02:31,340
certain steps you're going to repeat

59
00:02:31,340 --> 00:02:33,320
probably multiple times in

60
00:02:33,320 --> 00:02:35,865
a complicated real world project.

61
00:02:35,865 --> 00:02:38,060
So first off, you're going to explore and understand

62
00:02:38,060 --> 00:02:39,260
the data relationships and

63
00:02:39,260 --> 00:02:42,685
we've been talking about that quite a lot.

64
00:02:42,685 --> 00:02:44,940
You're going to look at ways you

65
00:02:44,940 --> 00:02:46,940
can transform those features.

66
00:02:46,940 --> 00:02:50,220
What can you do by transform a feature,

67
00:02:50,220 --> 00:02:52,590
I mean transform a single feature

68
00:02:52,590 --> 00:02:55,455
by some mathematical process.

69
00:02:55,455 --> 00:02:57,985
We're going to look at how you,

70
00:02:57,985 --> 00:02:59,635
what are the interaction terms?

71
00:02:59,635 --> 00:03:01,560
Maybe new features you can

72
00:03:01,560 --> 00:03:04,650
compute using some mathematical operation on

73
00:03:04,650 --> 00:03:09,470
other features and you

74
00:03:09,470 --> 00:03:10,490
want to go back and use

75
00:03:10,490 --> 00:03:12,640
visualization to check those results.

76
00:03:12,640 --> 00:03:14,090
Are you really gaining

77
00:03:14,090 --> 00:03:16,560
ground or are you just chasing your tail?

78
00:03:16,560 --> 00:03:18,680
You need to ultimately

79
00:03:18,680 --> 00:03:22,120
test any feature even if you think it's great,

80
00:03:22,120 --> 00:03:25,400
you need to try it with a machine learning model and see

81
00:03:25,400 --> 00:03:27,110
if you are really getting anywhere if

82
00:03:27,110 --> 00:03:28,960
you're really improving the situation.

83
00:03:28,960 --> 00:03:30,635
You're going to wind up,

84
00:03:30,635 --> 00:03:31,745
as I said already,

85
00:03:31,745 --> 00:03:34,205
you're going to repeat the above steps

86
00:03:34,205 --> 00:03:36,970
probably many times.

