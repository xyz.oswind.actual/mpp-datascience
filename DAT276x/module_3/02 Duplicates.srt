0
00:00:00,650 --> 00:00:03,810
>> So, let's talk about treating duplicates.

1
00:00:03,810 --> 00:00:05,160
Duplicate show up in

2
00:00:05,160 --> 00:00:08,450
databases all the time for various reasons,

3
00:00:08,450 --> 00:00:11,905
sometimes deliberately, sometimes just accidentally.

4
00:00:11,905 --> 00:00:13,990
The problem with duplicates for

5
00:00:13,990 --> 00:00:16,275
Machine Learning is they overweight that case.

6
00:00:16,275 --> 00:00:19,965
Say, one customer is

7
00:00:19,965 --> 00:00:24,240
with a supposedly unique ID shows up 100 times,

8
00:00:24,240 --> 00:00:27,430
and every other customer in your database shows up once.

9
00:00:27,430 --> 00:00:29,535
So, you don't want that customer

10
00:00:29,535 --> 00:00:31,895
who somehow wound up with

11
00:00:31,895 --> 00:00:35,145
100 duplicated accounts to

12
00:00:35,145 --> 00:00:36,900
have 100 times the weight of

13
00:00:36,900 --> 00:00:39,165
a customer who only has one account.

14
00:00:39,165 --> 00:00:41,760
It doesn't make sense. So, you

15
00:00:41,760 --> 00:00:43,850
need to treat the duplicate in some way

16
00:00:43,850 --> 00:00:46,110
so you don't overweight just

17
00:00:46,110 --> 00:00:48,750
because of random duplication.

18
00:00:48,750 --> 00:00:50,309
First off, you have to identify

19
00:00:50,309 --> 00:00:52,895
the duplicate cases and that can be hard.

20
00:00:52,895 --> 00:00:55,890
If you're lucky, there's some kind of unique ID you can

21
00:00:55,890 --> 00:00:59,130
use like a customer number, or email address,

22
00:00:59,130 --> 00:01:01,430
or something like that that's guaranteed

23
00:01:01,430 --> 00:01:05,320
unique but not always.

24
00:01:05,320 --> 00:01:09,510
So, you may have to do it by value of certain columns,

25
00:01:09,510 --> 00:01:12,450
certain features but be really cautious because it's

26
00:01:12,450 --> 00:01:15,239
possible there's multiple customers

27
00:01:15,239 --> 00:01:17,400
who live at the same address,

28
00:01:17,400 --> 00:01:19,750
have similar same last name,

29
00:01:19,750 --> 00:01:21,740
et cetera, things like that.

30
00:01:21,740 --> 00:01:23,100
So you have to really think through

31
00:01:23,100 --> 00:01:25,275
the business problem of

32
00:01:25,275 --> 00:01:26,955
what you're doing if you're going to

33
00:01:26,955 --> 00:01:29,190
look for duplicates by value.

34
00:01:29,190 --> 00:01:32,310
So, let's say we've identified a bunch of duplicates.

35
00:01:32,310 --> 00:01:34,270
Well, now, we need a removal strategy.

36
00:01:34,270 --> 00:01:37,005
We need to treat them in some way.

37
00:01:37,005 --> 00:01:42,300
Well, if we have dates of creation, save accounts,

38
00:01:42,300 --> 00:01:44,520
or of transaction records or

39
00:01:44,520 --> 00:01:47,110
something like that and there's clear duplicates,

40
00:01:47,110 --> 00:01:48,780
maybe we want to keep the most recent

41
00:01:48,780 --> 00:01:50,685
because that might be the most up to date.

42
00:01:50,685 --> 00:01:52,500
But if it's like when was an account

43
00:01:52,500 --> 00:01:54,340
created or something like that,

44
00:01:54,340 --> 00:01:56,755
well maybe we want the oldest one

45
00:01:56,755 --> 00:01:59,490
because that's got the most history behind it.

46
00:01:59,490 --> 00:02:02,500
So you have to kind of think that through.

47
00:02:02,570 --> 00:02:06,030
If you don't have something like dates or

48
00:02:06,030 --> 00:02:09,000
some other ordering identifier,

49
00:02:09,000 --> 00:02:11,310
maybe you just keep the first one as you start at

50
00:02:11,310 --> 00:02:13,955
the top of the table and work to the bottom,

51
00:02:13,955 --> 00:02:16,700
or you keep the last one.

52
00:02:17,120 --> 00:02:19,230
And maybe it doesn't matter,

53
00:02:19,230 --> 00:02:20,415
but you need to think it through.

54
00:02:20,415 --> 00:02:23,220
There's no magic wand that tells you what

55
00:02:23,220 --> 00:02:27,570
the best removal strategy is until you think it through.

