0
00:00:00,590 --> 00:00:02,760
>> Let's talk about a little trickier.

1
00:00:02,760 --> 00:00:04,530
So we've talked about 1D features

2
00:00:04,530 --> 00:00:05,985
and how we can transform them.

3
00:00:05,985 --> 00:00:07,740
Let's talk about a little trickier problem

4
00:00:07,740 --> 00:00:09,750
which is interaction terms.

5
00:00:09,750 --> 00:00:11,740
So in interaction term,

6
00:00:11,740 --> 00:00:14,145
let's look what's an example.

7
00:00:14,145 --> 00:00:16,395
Let's say we want to predict the number

8
00:00:16,395 --> 00:00:18,960
of people riding on a bus route.

9
00:00:18,960 --> 00:00:21,060
Okay. That might be important

10
00:00:21,060 --> 00:00:23,720
for for a transit company, for example.

11
00:00:23,720 --> 00:00:25,580
So, schedule buses.

12
00:00:25,580 --> 00:00:29,025
Well, it depends on more than one feature in the data.

13
00:00:29,025 --> 00:00:30,870
It probably depends on the time of day

14
00:00:30,870 --> 00:00:33,180
and whether it's a holiday or not.

15
00:00:33,180 --> 00:00:35,180
You can imagine on a work day,

16
00:00:35,180 --> 00:00:36,960
there are certain rush hours going

17
00:00:36,960 --> 00:00:39,090
certain directions where there's a lot of people

18
00:00:39,090 --> 00:00:40,230
needing to say get to

19
00:00:40,230 --> 00:00:43,230
a downtown area and

20
00:00:43,230 --> 00:00:45,310
the buses are going to have a higher load.

21
00:00:45,310 --> 00:00:47,470
But if it turns out to be a Sunday or

22
00:00:47,470 --> 00:00:50,095
a national holiday or something,

23
00:00:50,095 --> 00:00:54,015
the time profile there still may be a lot of people going

24
00:00:54,015 --> 00:00:56,250
downtown but they might be going downtown to

25
00:00:56,250 --> 00:00:58,670
shop or go to restaurants or movies or something.

26
00:00:58,670 --> 00:01:00,220
And so the time of day they're

27
00:01:00,220 --> 00:01:02,780
going is different and

28
00:01:02,780 --> 00:01:04,380
maybe the total volumes are different.

29
00:01:04,380 --> 00:01:07,240
So that's an example of an interaction,

30
00:01:07,240 --> 00:01:09,470
the interaction is between time of

31
00:01:09,470 --> 00:01:13,205
day and the holiday and we call that an interaction term.

32
00:01:13,205 --> 00:01:14,400
And there's lots of these,

33
00:01:14,400 --> 00:01:16,535
you'll find in the real world.

34
00:01:16,535 --> 00:01:18,920
And so there's lots of

35
00:01:18,920 --> 00:01:21,560
ways you can computer interaction terms.

36
00:01:21,560 --> 00:01:24,840
I'll just give you a few ideas here.

37
00:01:25,350 --> 00:01:27,840
The mean or the median,

38
00:01:27,840 --> 00:01:30,695
something like that if you have a lot of features

39
00:01:30,695 --> 00:01:35,610
that maybe need to be aggregated in some way,

40
00:01:35,610 --> 00:01:38,900
for example, maybe bus riders we have it

41
00:01:38,900 --> 00:01:43,400
by different kinds of

42
00:01:43,400 --> 00:01:46,615
transit cards they use to pay for their ride.

43
00:01:46,615 --> 00:01:49,040
Well, in terms of capacity management,

44
00:01:49,040 --> 00:01:51,340
that's interesting from an accounting point of view,

45
00:01:51,340 --> 00:01:53,650
but for capacity management, we don't care.

46
00:01:53,650 --> 00:01:55,850
So, it's the sum or the mean

47
00:01:55,850 --> 00:01:59,220
or something like that that we really care about.

48
00:02:00,100 --> 00:02:04,295
It's the difference we care about,

49
00:02:04,295 --> 00:02:08,130
we know that someone is riding on an airplane.

50
00:02:08,130 --> 00:02:10,250
The fare they pick might

51
00:02:10,250 --> 00:02:12,620
be dependent on the difference

52
00:02:12,620 --> 00:02:14,475
of whether it's business or pleasure.

53
00:02:14,475 --> 00:02:17,015
So you might have two categorical columns;

54
00:02:17,015 --> 00:02:18,295
business and pleasure.

55
00:02:18,295 --> 00:02:19,970
But what you really care about is

56
00:02:19,970 --> 00:02:21,590
that interaction that they're either

57
00:02:21,590 --> 00:02:23,895
traveling for business or pleasure.

58
00:02:23,895 --> 00:02:26,960
And you can think of cases where you'd multiply or

59
00:02:26,960 --> 00:02:30,180
do some other arithmetics sum or something like that.

60
00:02:30,180 --> 00:02:34,620
The ratio, ratio of features is often powerful.

