0
00:00:00,000 --> 00:00:04,830
>> So let's talk about missing values.

1
00:00:04,830 --> 00:00:07,580
Missing values are probably the most common headache

2
00:00:07,580 --> 00:00:09,980
you're going to have as a machine learning person.

3
00:00:09,980 --> 00:00:13,770
First off, you can detect them usually in

4
00:00:13,770 --> 00:00:18,840
your data exploration and if you don't detect them,

5
00:00:18,840 --> 00:00:23,890
I should say you're going to have real problems because

6
00:00:24,410 --> 00:00:28,130
a lot of machine learning algorithms just fail on

7
00:00:28,130 --> 00:00:33,385
missing values or give very weird results sometimes.

8
00:00:33,385 --> 00:00:35,735
So, then there's the question,

9
00:00:35,735 --> 00:00:38,555
we know we've got to find our missing values.

10
00:00:38,555 --> 00:00:40,220
How do we hunt for them?

11
00:00:40,220 --> 00:00:41,380
How are they coded?

12
00:00:41,380 --> 00:00:43,190
Again I wish I could give you

13
00:00:43,190 --> 00:00:45,290
a magic one that you could wave around

14
00:00:45,290 --> 00:00:46,550
and that would be how it

15
00:00:46,550 --> 00:00:49,355
works but I don't have one and nobody does.

16
00:00:49,355 --> 00:00:53,090
If you're lucky, they're coded as some null value or

17
00:00:53,090 --> 00:00:55,250
missing value or empty string

18
00:00:55,250 --> 00:00:58,370
or something like that and that's great.

19
00:00:58,370 --> 00:01:02,365
If you've got that then you've got it made.

20
00:01:02,365 --> 00:01:04,145
But in other cases,

21
00:01:04,145 --> 00:01:06,670
maybe they've been coded in some other way,

22
00:01:06,670 --> 00:01:09,050
old fashioned mainframe systems and

23
00:01:09,050 --> 00:01:11,030
you still see a lot of that databases

24
00:01:11,030 --> 00:01:15,210
around use like -999 as a missing value,

25
00:01:15,210 --> 00:01:17,260
it is just a convention.

26
00:01:17,260 --> 00:01:20,400
Sometimes, I've seen this in records,

27
00:01:20,400 --> 00:01:22,685
like in medical records for example a test

28
00:01:22,685 --> 00:01:25,490
isn't done on a patient and the default value

29
00:01:25,490 --> 00:01:27,890
is zero because it

30
00:01:27,890 --> 00:01:30,680
doesn't mean their blood pressure was actually zero,

31
00:01:30,680 --> 00:01:32,360
it just means no one measured

32
00:01:32,360 --> 00:01:35,540
the blood pressure and entered it into the database.

33
00:01:35,540 --> 00:01:38,850
NA, sometimes a character string,

34
00:01:38,850 --> 00:01:41,990
sometimes a special value

35
00:01:41,990 --> 00:01:44,680
can be a good one question mark et cetera.

36
00:01:44,680 --> 00:01:47,090
So, there's lots of ways and you need to dig

37
00:01:47,090 --> 00:01:50,410
around and figure out for your particular case,

38
00:01:50,410 --> 00:01:52,535
your particular database,

39
00:01:52,535 --> 00:01:54,910
how are the missing values coded?

40
00:01:54,910 --> 00:01:58,760
So, let's say you've hunted down your missing values,

41
00:01:58,760 --> 00:01:59,825
what are you going to do about it?

42
00:01:59,825 --> 00:02:00,890
How do you treat them?

43
00:02:00,890 --> 00:02:02,560
Well, there's lots of ways.

44
00:02:02,560 --> 00:02:05,185
First off, look at the columns.

45
00:02:05,185 --> 00:02:06,860
If there's a column that's

46
00:02:06,860 --> 00:02:08,650
mostly missing values maybe it's 70,

47
00:02:08,650 --> 00:02:10,670
80, 90 percent missing values,

48
00:02:10,670 --> 00:02:12,380
it's probably not going to have

49
00:02:12,380 --> 00:02:16,675
that much information unless somehow you can fix it,

50
00:02:16,675 --> 00:02:19,700
like all the missing value

51
00:02:19,700 --> 00:02:22,880
should be a certain default or something like that.

52
00:02:22,880 --> 00:02:25,430
But generally, those kind of

53
00:02:25,430 --> 00:02:28,005
features just don't have very much information

54
00:02:28,005 --> 00:02:31,295
and so it may be safe to just remove them and not

55
00:02:31,295 --> 00:02:35,620
contaminate your training by introducing noise.

56
00:02:35,620 --> 00:02:38,030
If it's just a few rows,

57
00:02:38,030 --> 00:02:40,310
say you've got thousands of cases and

58
00:02:40,310 --> 00:02:43,650
only five rows have missing values in them,

59
00:02:43,650 --> 00:02:45,380
maybe it's better just to not think about

60
00:02:45,380 --> 00:02:47,150
it and just get rid of

61
00:02:47,150 --> 00:02:51,320
those five rows or a small percentage of rows,

62
00:02:51,320 --> 00:02:53,130
that might just be the easiest thing to do,

63
00:02:53,130 --> 00:02:54,660
the safest thing to do.

64
00:02:54,660 --> 00:02:57,020
But say you can't do that,

65
00:02:57,020 --> 00:03:00,930
you can do fill which is a nearest neighbor strategy.

66
00:03:00,930 --> 00:03:03,190
You can either go forward or backward if there's

67
00:03:03,190 --> 00:03:05,690
a time order to the data.

68
00:03:05,690 --> 00:03:07,220
It's obvious forward and

69
00:03:07,220 --> 00:03:08,990
backward actually means forward and backward for time.

70
00:03:08,990 --> 00:03:10,430
It may just mean forward and

71
00:03:10,430 --> 00:03:12,650
backward by record number but you're just

72
00:03:12,650 --> 00:03:15,110
using nearest neighbor to forward or

73
00:03:15,110 --> 00:03:17,780
backward fill those missing values.

74
00:03:17,780 --> 00:03:20,690
You can impute missing values by various methods,

75
00:03:20,690 --> 00:03:22,655
you can use mean, median.

76
00:03:22,655 --> 00:03:24,590
You could do some sort of interpolation

77
00:03:24,590 --> 00:03:26,645
like a trend value and there's

78
00:03:26,645 --> 00:03:29,060
much more sophisticated methods out there as

79
00:03:29,060 --> 00:03:32,680
well to impute missing values.

