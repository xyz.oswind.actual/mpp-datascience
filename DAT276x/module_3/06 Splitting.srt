0
00:00:01,020 --> 00:00:04,420
>> So let's talk about data splitting.

1
00:00:04,420 --> 00:00:06,280
Data splitting is another one of these data

2
00:00:06,280 --> 00:00:08,530
preparation steps that we'll be doing over

3
00:00:08,530 --> 00:00:10,930
and over again in this class and you'll be doing it

4
00:00:10,930 --> 00:00:12,370
over and over again in your career

5
00:00:12,370 --> 00:00:13,650
doing machine learning.

6
00:00:13,650 --> 00:00:17,920
So, you always split the data before training.

7
00:00:17,920 --> 00:00:21,600
This is for supervised learning, I should say.

8
00:00:21,870 --> 00:00:25,180
You want independent training test

9
00:00:25,180 --> 00:00:26,390
and evaluation subsets,

10
00:00:26,390 --> 00:00:28,120
and I mean statistically independent.

11
00:00:28,120 --> 00:00:30,280
So, if you don't

12
00:00:30,280 --> 00:00:32,890
do that you're going to have

13
00:00:32,890 --> 00:00:34,825
what's called information leakage.

14
00:00:34,825 --> 00:00:38,860
And basically, if you're say testing or

15
00:00:38,860 --> 00:00:43,545
evaluating and training a model on the same data,

16
00:00:43,545 --> 00:00:45,610
the model may be getting more

17
00:00:45,610 --> 00:00:47,275
accurate but it's getting more accurate

18
00:00:47,275 --> 00:00:48,895
because it's learning the training data

19
00:00:48,895 --> 00:00:51,100
not because it's learning the general case.

20
00:00:51,100 --> 00:00:52,915
And so, that's called information

21
00:00:52,915 --> 00:00:55,135
leakage and it's a big problem.

22
00:00:55,135 --> 00:00:57,190
And what you wind up

23
00:00:57,190 --> 00:00:59,270
with is you'll look and you'll say, "Wow!

24
00:00:59,270 --> 00:01:01,160
My model is really accurate.

25
00:01:01,160 --> 00:01:02,960
It really fits these data well."

26
00:01:02,960 --> 00:01:05,580
Well, it really fits those data well,

27
00:01:05,580 --> 00:01:06,670
but it doesn't fit maybe

28
00:01:06,670 --> 00:01:08,020
the general case that you're going

29
00:01:08,020 --> 00:01:09,605
to encounter in production.

30
00:01:09,605 --> 00:01:12,185
So, don't be fooled by that.

31
00:01:12,185 --> 00:01:14,650
So the most common way to split

32
00:01:14,650 --> 00:01:17,205
data is what we call Bernoulli sampling,

33
00:01:17,205 --> 00:01:19,615
which is just random sampling.

34
00:01:19,615 --> 00:01:21,580
It's just a random sampling method and we

35
00:01:21,580 --> 00:01:23,830
try to do it in a way that we avoid

36
00:01:23,830 --> 00:01:25,285
the biases I've been talking

37
00:01:25,285 --> 00:01:29,140
about and the other is cross validation,

38
00:01:29,140 --> 00:01:31,975
which is a re-sampling method and we'll talk about that

39
00:01:31,975 --> 00:01:35,840
more in another part of the course.

