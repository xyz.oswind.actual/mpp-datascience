0
00:00:00,000 --> 00:00:02,815
>> So let me just summarize.

1
00:00:02,815 --> 00:00:04,040
We've gone through a lot of

2
00:00:04,040 --> 00:00:07,030
Data preparation methods and reasons,

3
00:00:07,030 --> 00:00:10,115
and let me just try to wrap this up for you.

4
00:00:10,115 --> 00:00:13,460
So, Data preparation is key to success machinery.

5
00:00:13,460 --> 00:00:16,260
I think if you think it through,

6
00:00:16,260 --> 00:00:20,460
all these methods I've talked about if you don't do that,

7
00:00:20,460 --> 00:00:24,180
if you're not carefully doing each of those steps,

8
00:00:24,180 --> 00:00:27,210
in some order not necessarily the order I told you,

9
00:00:27,210 --> 00:00:29,955
you're going to have some problems.

10
00:00:29,955 --> 00:00:32,040
It's an iterative process.

11
00:00:32,040 --> 00:00:34,550
You can probably see you're going to find fixing

12
00:00:34,550 --> 00:00:37,870
one problem may highlight another,

13
00:00:37,870 --> 00:00:40,820
and usually use data exploration

14
00:00:40,820 --> 00:00:44,255
to identify problems and test your results.

15
00:00:44,255 --> 00:00:48,750
You may find the problem after you start

16
00:00:48,750 --> 00:00:51,080
training and evaluating a machine learning models

17
00:00:51,080 --> 00:00:52,130
because things just don't

18
00:00:52,130 --> 00:00:54,185
work the way you think they should.

19
00:00:54,185 --> 00:00:59,375
And, then you go back and do more treatments.

20
00:00:59,375 --> 00:01:05,390
So, you're going to wind up going around and around,

21
00:01:05,390 --> 00:01:09,430
and it'll feel like forever when you're doing this.

22
00:01:09,430 --> 00:01:11,570
You're going to address data issues multiple

23
00:01:11,570 --> 00:01:14,570
times in any real -world Machine Learning Project.

24
00:01:14,570 --> 00:01:17,830
There's an old joke that machine learning Scientists

25
00:01:17,830 --> 00:01:19,820
spent 80 percent of their time

26
00:01:19,820 --> 00:01:22,245
dealing with data preparation problems,

27
00:01:22,245 --> 00:01:23,730
and 20 percent of their time

28
00:01:23,730 --> 00:01:25,550
complaining about how long it takes

29
00:01:25,550 --> 00:01:29,940
to deal with all the data preparation problems.

