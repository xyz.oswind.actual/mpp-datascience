0
00:00:00,000 --> 00:00:02,740
>> So, let's talk

1
00:00:02,740 --> 00:00:06,300
about errors and outliers and this is a tricky problem.

2
00:00:06,300 --> 00:00:08,260
First off, you'll detect them in

3
00:00:08,260 --> 00:00:11,425
your exploration like a lot of these problems,

4
00:00:11,425 --> 00:00:14,555
and you can just have erroneous values.

5
00:00:14,555 --> 00:00:16,840
Erroneous value like a typical case,

6
00:00:16,840 --> 00:00:18,915
the price of a product,

7
00:00:18,915 --> 00:00:21,950
what customers bought it for over time,

8
00:00:21,950 --> 00:00:24,030
and all of a sudden you see the price is

9
00:00:24,030 --> 00:00:26,365
10 times more, 10 times less.

10
00:00:26,365 --> 00:00:29,710
It's pretty clear that is an error.

11
00:00:29,710 --> 00:00:32,565
It's just the price of that car,

12
00:00:32,565 --> 00:00:34,360
that soap, or whatever didn't

13
00:00:34,360 --> 00:00:37,095
suddenly jumped by a factor of 10 probably.

14
00:00:37,095 --> 00:00:39,160
So, that's an erroneous value,

15
00:00:39,160 --> 00:00:42,240
but outliers could actually be useful information.

16
00:00:42,240 --> 00:00:44,500
For example, if you're trying to detect fraud or

17
00:00:44,500 --> 00:00:47,290
some other anomaly failures

18
00:00:47,290 --> 00:00:48,920
of equipment or something like that,

19
00:00:48,920 --> 00:00:50,240
those outlier values,

20
00:00:50,240 --> 00:00:51,850
those strange values might

21
00:00:51,850 --> 00:00:53,540
be the most valuable data you have.

22
00:00:53,540 --> 00:00:56,130
So, you don't want to just arbitrarily

23
00:00:56,130 --> 00:00:58,380
go through and axe anything that

24
00:00:58,380 --> 00:01:01,780
looks out of place because you

25
00:01:01,780 --> 00:01:02,830
really need to apply

26
00:01:02,830 --> 00:01:06,200
some domain knowledge and understand what's going on.

27
00:01:06,660 --> 00:01:11,095
Here's some ideas of how you can identify these cases.

28
00:01:11,095 --> 00:01:12,990
You can use summary statistics.

29
00:01:12,990 --> 00:01:15,830
So, if you find you have

30
00:01:15,830 --> 00:01:17,870
10 values out of thousands that

31
00:01:17,870 --> 00:01:20,385
are three standard deviations out,

32
00:01:20,385 --> 00:01:22,395
well that's kind of suspicious.

33
00:01:22,395 --> 00:01:25,580
If you find their categorical values

34
00:01:25,580 --> 00:01:27,440
where there's a case that only

35
00:01:27,440 --> 00:01:29,540
has three or something like that.

36
00:01:29,540 --> 00:01:30,720
You should think about that,

37
00:01:30,720 --> 00:01:32,270
why are there only three,

38
00:01:32,270 --> 00:01:33,500
when there might be thousands of

39
00:01:33,500 --> 00:01:35,620
cases in the other categories.

40
00:01:35,620 --> 00:01:40,950
You can use visualization and scatter plot.

41
00:01:40,950 --> 00:01:43,970
Sometimes it's very obvious what outliers are in

42
00:01:43,970 --> 00:01:49,350
various other graphical methods, histograms, and what.

43
00:01:49,370 --> 00:01:52,670
Let's say you've hunted down some cases that you

44
00:01:52,670 --> 00:01:55,620
truly believe are errors and you want to treat them.

45
00:01:55,620 --> 00:01:56,720
So, what can you do?

46
00:01:56,720 --> 00:02:00,065
Well, one thing you could do is you could just

47
00:02:00,065 --> 00:02:01,850
set a max and min range of

48
00:02:01,850 --> 00:02:04,995
credible values and just limit to that.

49
00:02:04,995 --> 00:02:10,715
You could do, use one of the missing value strategies.

50
00:02:10,715 --> 00:02:13,610
You could replace with the median,

51
00:02:13,610 --> 00:02:16,430
or the mean, or something like that.

