0
00:00:00,000 --> 00:00:03,820
>> Hello.

1
00:00:03,820 --> 00:00:08,215
So, another common problem in

2
00:00:08,215 --> 00:00:10,690
data that we're going to encounter from when we do

3
00:00:10,690 --> 00:00:13,345
machine learning is duplicate cases.

4
00:00:13,345 --> 00:00:16,360
Duplicates arrives from all sorts of reasons,

5
00:00:16,360 --> 00:00:20,075
mistakes, moldable entries, whatever.

6
00:00:20,075 --> 00:00:23,320
They're important in the sense that having a lot of

7
00:00:23,320 --> 00:00:26,560
duplicates puts more weight on a case

8
00:00:26,560 --> 00:00:28,390
than it should have in training

9
00:00:28,390 --> 00:00:31,000
the model for no other reason than it just happens to be

10
00:00:31,000 --> 00:00:32,680
duplicated because of

11
00:00:32,680 --> 00:00:35,560
some process issue further upstream.

12
00:00:35,560 --> 00:00:41,740
So, we want to find duplicates and remove them.

13
00:00:41,740 --> 00:00:47,090
So, let's look at a simple example here of doing that.

14
00:00:47,410 --> 00:00:53,700
So, here's some code and we can find duplicates.

15
00:00:53,700 --> 00:01:03,640
We can print the dimensions of our credit data frame.

16
00:01:04,190 --> 00:01:08,880
There's a distinct function which really helps

17
00:01:08,880 --> 00:01:10,080
you especially if you have

18
00:01:10,080 --> 00:01:13,030
a column that has to have unique values.

19
00:01:13,030 --> 00:01:18,000
In this case, we do because there's a customer ID column.

20
00:01:18,230 --> 00:01:22,220
In fact, this function is smart enough to figure that out

21
00:01:22,220 --> 00:01:23,370
so that's why I haven't given it

22
00:01:23,370 --> 00:01:26,490
any other arguments but just the data frame.

23
00:01:26,490 --> 00:01:33,060
You see there's 111 rows with 22 columns,

24
00:01:33,060 --> 00:01:36,225
but 999 distinct cases.

25
00:01:36,225 --> 00:01:39,930
So, it means there's 12 duplicates

26
00:01:39,930 --> 00:01:43,680
here and we have to deal with that.

27
00:01:43,680 --> 00:01:46,710
Well, we already know about the distinct function.

28
00:01:46,710 --> 00:01:48,670
So it's pretty easy,

29
00:01:48,670 --> 00:01:51,570
we just run the distinct function on

30
00:01:51,570 --> 00:01:56,495
the data frame and we'll look at the result,

31
00:01:56,495 --> 00:01:59,170
and now we have 999 rows.

32
00:01:59,170 --> 00:02:02,260
So, with distinct we've

33
00:02:02,260 --> 00:02:05,290
identified those duplicates, remove those duplicate.

34
00:02:05,290 --> 00:02:06,950
So, this is obviously an easy case

35
00:02:06,950 --> 00:02:09,965
because we have a unique customer ID.

36
00:02:09,965 --> 00:02:17,210
So, that's a short example of locating duplicates,

37
00:02:17,210 --> 00:02:19,010
determining we have duplicates and

38
00:02:19,010 --> 00:02:22,560
then removing and cleaning duplicates.

