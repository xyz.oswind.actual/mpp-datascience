0
00:00:01,520 --> 00:00:05,860
>> Hello. So, I need to talk about

1
00:00:05,860 --> 00:00:09,130
an important issue of

2
00:00:09,130 --> 00:00:12,100
data preparation before you

3
00:00:12,100 --> 00:00:14,630
start doing any training of machine learning models,

4
00:00:14,630 --> 00:00:17,255
and that is splitting the data set and scaling.

5
00:00:17,255 --> 00:00:20,110
We have to kind of talk about those together.

6
00:00:20,110 --> 00:00:23,950
Scaling is very important because you don't want

7
00:00:23,950 --> 00:00:25,990
your training biased by

8
00:00:25,990 --> 00:00:28,975
the numeric range of your features.

9
00:00:28,975 --> 00:00:32,290
For example, if you have income and age,

10
00:00:32,290 --> 00:00:34,260
income is probably order of

11
00:00:34,260 --> 00:00:38,550
magnitude or two greater than age, presumably.

12
00:00:39,410 --> 00:00:42,300
So you need to scale,

13
00:00:42,300 --> 00:00:46,930
so that they're both on some similar numeric range.

14
00:00:46,930 --> 00:00:49,070
We're going to look at a specific example here.

15
00:00:49,070 --> 00:00:50,500
We're going to use Z-scores scaling,

16
00:00:50,500 --> 00:00:56,740
which is zero mean and unit standard deviation scaling.

17
00:00:56,840 --> 00:00:59,900
But before we do scaling,

18
00:00:59,900 --> 00:01:01,910
we also have to split the data set.

19
00:01:01,910 --> 00:01:03,530
So, you always want to

20
00:01:03,530 --> 00:01:07,010
scale based on the training data you're using

21
00:01:07,010 --> 00:01:09,275
and then use whatever scale you've computed

22
00:01:09,275 --> 00:01:12,130
for any test or evaluation data set.

23
00:01:12,130 --> 00:01:13,995
So, keep that in mind.

24
00:01:13,995 --> 00:01:16,660
So my screen here,

25
00:01:16,660 --> 00:01:19,940
I have some code to do those two steps.

26
00:01:19,940 --> 00:01:24,610
So, here's the auto_price data set,

27
00:01:24,610 --> 00:01:28,610
and I'm just going to use "Create Data

28
00:01:28,610 --> 00:01:32,855
Partition" is from the R caret package,

29
00:01:32,855 --> 00:01:36,070
and I get a partition.

30
00:01:36,110 --> 00:01:40,870
So, I can create my training sample by partition or not.

31
00:01:40,870 --> 00:01:42,240
See the minus sign there?

32
00:01:42,240 --> 00:01:45,890
Not partition for the test data set.

33
00:01:45,890 --> 00:01:48,775
So, I'm just going to run all that.

34
00:01:48,775 --> 00:01:51,990
You see, because I said I wanted

35
00:01:51,990 --> 00:01:54,970
75 percent of my data and training,

36
00:01:54,970 --> 00:01:58,800
I get 147 cases of training data,

37
00:01:58,800 --> 00:02:01,310
48 cases of test data.

38
00:02:01,310 --> 00:02:05,880
Okay. Now, let's go to scaling.

39
00:02:05,880 --> 00:02:12,655
Again, from caret, there's this nice preProcess function,

40
00:02:12,655 --> 00:02:15,560
and I have my training data set.

41
00:02:15,560 --> 00:02:18,620
I just want to apply this of course to numeric columns.

42
00:02:18,620 --> 00:02:22,460
Don't try to scale the categorical columns.

43
00:02:23,130 --> 00:02:25,430
I wind up with

44
00:02:25,430 --> 00:02:30,525
a preProcess object just for those three columns.

45
00:02:30,525 --> 00:02:32,790
I'm just doing three columns for demohere.

46
00:02:32,790 --> 00:02:33,890
Curb weight, horsepower,

47
00:02:33,890 --> 00:02:36,020
city miles per gallon and my method,

48
00:02:36,020 --> 00:02:38,645
my scaling method is to center and scale.

49
00:02:38,645 --> 00:02:40,720
So, that gives me zero mean.

50
00:02:40,720 --> 00:02:42,265
That's what I mean by center,

51
00:02:42,265 --> 00:02:44,655
and Unit Standard Deviation.

52
00:02:44,655 --> 00:02:46,535
That's what we mean by scale.

53
00:02:46,535 --> 00:02:51,050
Then, I apply a predict method

54
00:02:51,050 --> 00:02:55,890
to that scalar on the training data and the test data,

55
00:02:55,890 --> 00:02:59,060
and we'll just look at the first few columns of

56
00:02:59,060 --> 00:03:01,460
the first few rows of

57
00:03:01,460 --> 00:03:04,040
those numeric columns for my training data,

58
00:03:04,040 --> 00:03:06,270
and that will be our result.

59
00:03:07,030 --> 00:03:10,870
There we go. Looks like we've got it.

60
00:03:10,870 --> 00:03:12,650
You see those all?

61
00:03:12,650 --> 00:03:14,350
Even though curb weight, horsepower,

62
00:03:14,350 --> 00:03:15,610
city miles per gallon were on

63
00:03:15,610 --> 00:03:18,305
very different numeric scales originally,

64
00:03:18,305 --> 00:03:21,230
they're all in a similar range.

65
00:03:21,230 --> 00:03:23,755
They're all Z-scores scaled here.

66
00:03:23,755 --> 00:03:26,040
So, I hope this short demo shows you,

67
00:03:26,040 --> 00:03:28,875
first off, that scaling is very important.

68
00:03:28,875 --> 00:03:31,620
But second, I hope it's actually not that hard to do.

69
00:03:31,620 --> 00:03:33,420
You just need to remember to do it,

70
00:03:33,420 --> 00:03:37,120
and make sure you do it after you've split your data set.

