0
00:00:01,580 --> 00:00:04,680
>> Welcome back. So, we've

1
00:00:04,680 --> 00:00:07,705
looked at two-dimensional plots,

2
00:00:07,705 --> 00:00:09,700
we've used aesthetics to project

3
00:00:09,700 --> 00:00:12,595
additional dimensions onto a 2D plot service,

4
00:00:12,595 --> 00:00:14,540
but how do we get beyond that because there's

5
00:00:14,540 --> 00:00:17,165
a limit to what we can do with aesthetics.

6
00:00:17,165 --> 00:00:19,280
Well, there are a couple of solutions.

7
00:00:19,280 --> 00:00:23,420
They all involve projecting more axis onto your plots.

8
00:00:23,420 --> 00:00:25,345
So, we'll look at two different ways to do that.

9
00:00:25,345 --> 00:00:27,000
One is, what's called a "Scatter

10
00:00:27,000 --> 00:00:29,510
Plot Matrix" or a "Paris plot",

11
00:00:29,510 --> 00:00:33,055
so you basically have a matrix of

12
00:00:33,055 --> 00:00:39,660
all possible axis pairs for some subset of your data,

13
00:00:39,660 --> 00:00:41,590
and we'll look at how that works.

14
00:00:41,590 --> 00:00:44,540
We'll also use a method that goes by different names,

15
00:00:44,540 --> 00:00:46,150
it's sometimes called the method of

16
00:00:46,150 --> 00:00:50,680
small multiples or conditioning, or more commonly,

17
00:00:50,680 --> 00:00:52,510
faceting, and that's where we

18
00:00:52,510 --> 00:00:54,565
do a group by operation and we create

19
00:00:54,565 --> 00:00:58,680
an array of plots of subsets of the data,

20
00:00:58,680 --> 00:01:01,140
that we can then compare.

21
00:01:01,210 --> 00:01:05,450
So, I'll show you the first one of these,

22
00:01:05,450 --> 00:01:08,435
which is this "Pair-wise Scatter Plot".

23
00:01:08,435 --> 00:01:12,190
So, from the GG plot two package,

24
00:01:12,190 --> 00:01:16,260
there's gg pairs, and of course,

25
00:01:16,260 --> 00:01:19,475
we're working on the auto price dataset,

26
00:01:19,475 --> 00:01:24,570
and we're going to look at five numeric columns,

27
00:01:24,570 --> 00:01:26,010
curb weight, engine size,

28
00:01:26,010 --> 00:01:28,945
horsepower, city miles per gallon, and price.

29
00:01:28,945 --> 00:01:30,770
We're going to make

30
00:01:30,770 --> 00:01:34,905
the color equal to the fuel type, gas or diesel.

31
00:01:34,905 --> 00:01:36,990
We're going to use a pretty high transparency

32
00:01:36,990 --> 00:01:39,595
because we're going to have dense points.

33
00:01:39,595 --> 00:01:41,480
Our lower part of

34
00:01:41,480 --> 00:01:44,090
this scatter plot matrix is going to

35
00:01:44,090 --> 00:01:46,890
be point plot or scatter plot.

36
00:01:46,890 --> 00:01:51,990
The upper part will be a contour plot, a density plot.

37
00:01:51,990 --> 00:01:55,010
All right, let me run this.

38
00:02:01,430 --> 00:02:06,975
Okay, and so, let's start on the diagonal here.

39
00:02:06,975 --> 00:02:10,610
So, we have curb weight versus curb weight,

40
00:02:10,610 --> 00:02:13,825
engine size, horse power,

41
00:02:13,825 --> 00:02:17,450
city miles per gallon, price,

42
00:02:17,450 --> 00:02:22,675
and we have for gas and diesel,

43
00:02:22,675 --> 00:02:26,595
the kernel density estimation plots for,

44
00:02:26,595 --> 00:02:29,670
say curb weight of gas and diesel cars,

45
00:02:29,670 --> 00:02:32,860
engine size for gas and diesel cars,

46
00:02:32,860 --> 00:02:35,410
and price for gas and diesel cars.

47
00:02:35,410 --> 00:02:39,355
So, that's along the diagonal of this array.

48
00:02:39,355 --> 00:02:42,485
So, let's look at the lower set.

49
00:02:42,485 --> 00:02:46,980
So, here we have curb weight versus engine size.

50
00:02:46,980 --> 00:02:51,105
So, this is curb weight on the horizontal axis,

51
00:02:51,105 --> 00:02:54,135
engine size on the vertical axis,

52
00:02:54,135 --> 00:02:58,830
and you can see the diesel cars and the gasoline cars,

53
00:02:58,830 --> 00:03:01,950
like I said, there's still quite a lot overplotting here.

54
00:03:01,950 --> 00:03:06,980
Or we could look at horsepower versus engine size.

55
00:03:06,980 --> 00:03:08,945
Again, not too surprising that

56
00:03:08,945 --> 00:03:11,760
large engines produce more horsepower.

57
00:03:11,760 --> 00:03:14,555
Diesels tend to have

58
00:03:14,555 --> 00:03:18,600
lower horsepower for the engine size,

59
00:03:18,600 --> 00:03:21,735
so that's kind of interesting, maybe.

60
00:03:21,735 --> 00:03:24,320
Here, we have price.

61
00:03:24,320 --> 00:03:28,730
Price on the vertical and city miles per gallon,

62
00:03:28,730 --> 00:03:30,860
we already saw that relationship with

63
00:03:30,860 --> 00:03:33,385
the diesels and the gas cars,

64
00:03:33,385 --> 00:03:38,970
or price versus the horsepower

65
00:03:38,970 --> 00:03:42,040
with the diesel cars being more expensive for

66
00:03:42,040 --> 00:03:46,370
a given horsepower, etc.

67
00:03:46,380 --> 00:03:50,610
On the upper part of the matrix,

68
00:03:50,610 --> 00:03:54,445
you get the contour plots of price and curb weight.

69
00:03:54,445 --> 00:03:57,220
So, low cost cars tends to be

70
00:03:57,220 --> 00:04:00,870
a tight cluster of low cost,

71
00:04:00,870 --> 00:04:03,770
low weight cars, and only a few,

72
00:04:03,770 --> 00:04:08,430
high priced, heavy cars out here.

73
00:04:08,430 --> 00:04:16,240
Similarly, say, engine size versus city miles per gallon,

74
00:04:16,240 --> 00:04:18,260
again for gas and diesel,

75
00:04:18,260 --> 00:04:20,330
you can see the two different colors

76
00:04:20,330 --> 00:04:22,400
of plots of contour, sorry,

77
00:04:22,400 --> 00:04:29,280
and engine, the gas cars have a mode here.

78
00:04:29,280 --> 00:04:33,370
The diesels seem to be bimodal,

79
00:04:33,370 --> 00:04:38,400
and they're generally larger engines

80
00:04:38,400 --> 00:04:41,280
for a given miles per gallon.

81
00:04:41,280 --> 00:04:43,050
So, you could spend a lot of time

82
00:04:43,050 --> 00:04:44,900
looking at the scatter plot matrices,

83
00:04:44,900 --> 00:04:47,595
and if you have many numeric features,

84
00:04:47,595 --> 00:04:49,514
you may want to create several

85
00:04:49,514 --> 00:04:51,420
different scatter plot matrices.

86
00:04:51,420 --> 00:04:55,020
But, you can see now we're projecting 1, 2, 3, 4,

87
00:04:55,020 --> 00:04:59,490
5 numeric dimensions, plus gas versus diesel.

88
00:04:59,490 --> 00:05:01,270
So, six dimensions on this plot.

89
00:05:01,270 --> 00:05:04,080
We could also say add shape or something

90
00:05:04,080 --> 00:05:06,840
like that to go to seven dimensions fairly easily.

91
00:05:06,840 --> 00:05:09,505
So, we're getting to

92
00:05:09,505 --> 00:05:11,790
pretty high dimensional relationships here

93
00:05:11,790 --> 00:05:15,010
that are starting to become hard to understand.

94
00:05:15,010 --> 00:05:18,030
But, that's good because it gives us a tool

95
00:05:18,030 --> 00:05:20,845
to really drill down on some complex stuff.

96
00:05:20,845 --> 00:05:25,200
So, let's look at the other way we can do this,

97
00:05:25,200 --> 00:05:28,440
which is with these faceted plots.

98
00:05:28,440 --> 00:05:31,950
So, this code structure

99
00:05:31,950 --> 00:05:33,510
is very similar to what we've been doing,

100
00:05:33,510 --> 00:05:37,970
we iterate over some set of columns,

101
00:05:37,970 --> 00:05:40,490
and we decide if they're numeric.

102
00:05:40,490 --> 00:05:43,230
We set a binwidth because we're going

103
00:05:43,230 --> 00:05:45,990
to do geom histogram with

104
00:05:45,990 --> 00:05:49,170
a bin width and we're going to again

105
00:05:49,170 --> 00:05:53,375
use aes with y equals.. density..,

106
00:05:53,375 --> 00:05:55,040
so, we can also have

107
00:05:55,040 --> 00:05:59,590
the kernel density estimation plot overlaid on that,

108
00:05:59,590 --> 00:06:01,695
and we'll put a rug on it.

109
00:06:01,695 --> 00:06:03,895
But, the important thing is then,

110
00:06:03,895 --> 00:06:07,865
the new thing is facet_grid.

111
00:06:07,865 --> 00:06:13,950
So, there's this our formula.by drive.wheels,

112
00:06:13,950 --> 00:06:15,370
so this little squiggle line,

113
00:06:15,370 --> 00:06:18,850
I say in English is by so,.by drive.wheels.

114
00:06:18,850 --> 00:06:20,675
So, this is the rows,

115
00:06:20,675 --> 00:06:22,320
they're just going to be one row,

116
00:06:22,320 --> 00:06:23,790
that's why I put a dot there.

117
00:06:23,790 --> 00:06:27,000
In the columns, it's going to be the categories.

118
00:06:27,000 --> 00:06:28,890
"Drive wheels" is a categorical variable.

119
00:06:28,890 --> 00:06:30,330
So, we're going to do

120
00:06:30,330 --> 00:06:33,220
subgroups or group-bys drive wheels,

121
00:06:33,220 --> 00:06:36,360
and we'll get a set of histograms here.

122
00:06:37,880 --> 00:06:41,490
So, for curb weight,

123
00:06:41,490 --> 00:06:44,710
that's our first numeric variable,

124
00:06:46,530 --> 00:06:51,250
you can see for four-wheel drive, front-wheel drive,

125
00:06:51,250 --> 00:06:55,650
rear-wheel drive cars, how the curb weight varies.

126
00:06:55,650 --> 00:06:59,850
Notice what's important about these faceted plots,

127
00:06:59,850 --> 00:07:02,955
the horizontal axis dimensions

128
00:07:02,955 --> 00:07:05,980
and the vertical axis dimensions are exactly the same.

129
00:07:05,980 --> 00:07:08,640
So, comparisons from one to the other to the

130
00:07:08,640 --> 00:07:12,035
other are easy to do for your eye.

131
00:07:12,035 --> 00:07:15,810
You don't have to mentally try to rescale or go well at

132
00:07:15,810 --> 00:07:18,315
that axis is a different length

133
00:07:18,315 --> 00:07:20,350
or different numerical range.

134
00:07:20,350 --> 00:07:22,555
So, you can see

135
00:07:22,555 --> 00:07:24,810
there are that many four-wheel drive cars,

136
00:07:24,810 --> 00:07:26,700
but you can see they tend to

137
00:07:26,700 --> 00:07:29,660
cluster toward low curb weight,

138
00:07:29,660 --> 00:07:32,100
whereas rear-wheel drive cars are clearly left

139
00:07:32,100 --> 00:07:36,165
skewed toward heavy curb weight etc.

140
00:07:36,165 --> 00:07:39,180
So, you can see similar things with

141
00:07:39,180 --> 00:07:44,460
like horsepower and eventually,

142
00:07:44,460 --> 00:07:47,145
it will get down to price.

143
00:07:47,145 --> 00:07:49,760
So, a lot of overlap in price of

144
00:07:49,760 --> 00:07:52,960
four-wheel drive and front-wheel drive cars

145
00:07:52,960 --> 00:07:56,350
and our rear-wheel drive cars have the wide range,

146
00:07:56,350 --> 00:07:59,410
which you can tell immediately because you see

147
00:07:59,410 --> 00:08:03,670
that axis range is the same.

148
00:08:03,670 --> 00:08:08,420
In fact, the root front-wheel drive cars have

149
00:08:08,420 --> 00:08:11,645
the most peaked density curve

150
00:08:11,645 --> 00:08:13,460
because they tend to be

151
00:08:13,460 --> 00:08:15,555
clustered toward the low end of price,

152
00:08:15,555 --> 00:08:17,180
four-wheel drive cars mostly,

153
00:08:17,180 --> 00:08:21,000
but there's a few kind of medium priced ones.

154
00:08:21,000 --> 00:08:22,700
So, we've learned quite a lot

155
00:08:22,700 --> 00:08:26,275
about breaking down by groups,

156
00:08:26,275 --> 00:08:28,865
by drive wheels like the price

157
00:08:28,865 --> 00:08:32,034
or the curb weights of these cars,

158
00:08:32,034 --> 00:08:35,030
and we can continue this because facet grid

159
00:08:35,030 --> 00:08:37,770
lets us expand this formula.

160
00:08:37,770 --> 00:08:39,575
So, almost everything else here

161
00:08:39,575 --> 00:08:42,590
is the same as what I showed you before.

162
00:08:42,590 --> 00:08:44,180
Although my title keeps getting

163
00:08:44,180 --> 00:08:46,780
longer and longer as you can see.

164
00:08:46,780 --> 00:08:50,850
But, now I have drive wheels by body style.

165
00:08:50,850 --> 00:08:54,630
So, drive wheels will be in the row and

166
00:08:54,630 --> 00:08:59,530
body style will be in the columns.

167
00:09:02,840 --> 00:09:11,670
So, you see there's

168
00:09:11,670 --> 00:09:14,610
three types of drive wheels: four-wheel drive,

169
00:09:14,610 --> 00:09:15,630
front-wheel drive, rear-wheel

170
00:09:15,630 --> 00:09:16,850
drive which we already knew,

171
00:09:16,850 --> 00:09:19,270
five types of body styles.

172
00:09:19,270 --> 00:09:23,970
So, I have this three by five scatter plot matrix.

173
00:09:25,370 --> 00:09:29,140
Then, I have gas versus diesel here.

174
00:09:29,140 --> 00:09:30,760
So, how many dimensions do I have?

175
00:09:30,760 --> 00:09:32,505
I have 1, 2, 3.

176
00:09:32,505 --> 00:09:34,390
Well, I have one dimension this way,

177
00:09:34,390 --> 00:09:36,400
two dimensions this way,

178
00:09:36,400 --> 00:09:43,945
and then each one of these is price and curb weight,

179
00:09:43,945 --> 00:09:46,830
so that's four and then six.

180
00:09:46,830 --> 00:09:48,080
So, there's six dimensional plots,

181
00:09:48,080 --> 00:09:49,430
but I can also use shape,

182
00:09:49,430 --> 00:09:51,410
gets seven dimensions, etc.

183
00:09:51,410 --> 00:09:55,720
So, pretty high dimensional visualization here.

184
00:09:55,720 --> 00:09:57,665
But, some things, like there's

185
00:09:57,665 --> 00:09:59,710
no convertibles or hard tops,

186
00:09:59,710 --> 00:10:00,760
that are four-wheel drive,

187
00:10:00,760 --> 00:10:03,730
and there's only one four-wheel drive hatchback, etc.

188
00:10:03,730 --> 00:10:06,150
So, you can't make a lot of inference on that,

189
00:10:06,150 --> 00:10:09,290
but the fact that these scales are exactly the same,

190
00:10:09,290 --> 00:10:11,290
I can start, say,

191
00:10:11,290 --> 00:10:13,640
for the front-wheel drive cars.

192
00:10:13,640 --> 00:10:17,640
I can see that sedans and

193
00:10:17,640 --> 00:10:22,355
hatchbacks have similar relationship,

194
00:10:22,355 --> 00:10:26,310
although the price of sedans seems to go up

195
00:10:26,310 --> 00:10:30,620
faster with curb weight, for example,

196
00:10:30,620 --> 00:10:33,970
and similar with engine size,

197
00:10:33,970 --> 00:10:39,740
you see how tightly clustered front-wheel drive cars are,

198
00:10:39,740 --> 00:10:41,520
and that the clusters are very

199
00:10:41,520 --> 00:10:44,875
similar for hatchbacks and sedans.

200
00:10:44,875 --> 00:10:47,840
Then, finally for, say,

201
00:10:47,840 --> 00:10:50,365
let's look at city miles per gallon,

202
00:10:50,365 --> 00:10:54,970
and you can see that sedans,

203
00:10:56,280 --> 00:11:00,525
the price goes up fairly quickly,

204
00:11:00,525 --> 00:11:03,130
as the fuel economy drops,

205
00:11:03,130 --> 00:11:05,220
whereas our hatchbacks tend to

206
00:11:05,220 --> 00:11:09,300
be the most economical cars to drive.

207
00:11:09,300 --> 00:11:13,635
Then, you've got like the rear-wheel drive cars,

208
00:11:13,635 --> 00:11:16,290
sedans are definitely the

209
00:11:16,290 --> 00:11:19,140
most expensive and the least fuel efficient.

210
00:11:19,140 --> 00:11:21,705
So, we're getting a lot of relationships

211
00:11:21,705 --> 00:11:25,300
from looking at a large number of axis.

212
00:11:26,810 --> 00:11:29,895
So, that's the takeaway message here.

213
00:11:29,895 --> 00:11:32,510
You can use these two different methods scatter plot

214
00:11:32,510 --> 00:11:35,340
matrices and conditioned or faceted plots,

215
00:11:35,340 --> 00:11:36,930
which are a group by method

216
00:11:36,930 --> 00:11:40,530
to compare complex relationships

217
00:11:40,530 --> 00:11:46,240
that require more than one set of axis to project.

218
00:11:46,240 --> 00:11:48,030
You can do lots of things with

219
00:11:48,030 --> 00:11:51,045
that and sometimes it takes some creativity,

220
00:11:51,045 --> 00:11:55,620
but you'll find some really interesting relationships

221
00:11:55,620 --> 00:11:58,940
and complex data if you apply those tools.

