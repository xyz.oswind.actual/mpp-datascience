0
00:00:01,610 --> 00:00:05,610
>> In this video, I'll give you an overview of how to

1
00:00:05,610 --> 00:00:08,670
look at the relationship between two numeric variables.

2
00:00:08,670 --> 00:00:10,530
So that could be between a feature and

3
00:00:10,530 --> 00:00:12,660
the label which is something you really

4
00:00:12,660 --> 00:00:14,700
want to investigate if you have

5
00:00:14,700 --> 00:00:17,360
numeric labels like for a regression problem.

6
00:00:17,360 --> 00:00:21,300
You definitely want to understand which features seem to

7
00:00:21,300 --> 00:00:23,400
have some relationship to that label

8
00:00:23,400 --> 00:00:25,850
or just might be random noise,

9
00:00:25,850 --> 00:00:27,640
or between two features,

10
00:00:27,640 --> 00:00:32,190
you can look for say collinearity or covariance between

11
00:00:32,190 --> 00:00:36,000
your features or some other interaction

12
00:00:36,000 --> 00:00:37,620
that might be going on there.

13
00:00:37,620 --> 00:00:39,645
So it's very important to create

14
00:00:39,645 --> 00:00:41,820
lots of scatter plots and really understand

15
00:00:41,820 --> 00:00:46,510
the detailed structure between numeric variables.

16
00:00:46,510 --> 00:00:49,080
That's what we'll focus on here.

17
00:00:49,700 --> 00:00:53,500
So our go-to approach is to

18
00:00:53,500 --> 00:00:56,790
use scatter plots and I've created a function here

19
00:00:56,790 --> 00:01:02,300
called plot_scatter and there's

20
00:01:02,300 --> 00:01:04,265
arguments or a data frame,

21
00:01:04,265 --> 00:01:06,140
a list of columns,

22
00:01:06,140 --> 00:01:10,430
a y-column which default I'm

23
00:01:10,430 --> 00:01:12,200
giving price because price is

24
00:01:12,200 --> 00:01:15,305
our label and it is numeric in this case.

25
00:01:15,305 --> 00:01:19,065
So, we iterate over that list of columns,

26
00:01:19,065 --> 00:01:20,725
and get a column,

27
00:01:20,725 --> 00:01:24,980
and for ggplot, the ggplot function,

28
00:01:24,980 --> 00:01:27,345
the first argument is that data frame,

29
00:01:27,345 --> 00:01:31,320
then again we're using aes_string because price,

30
00:01:31,320 --> 00:01:34,060
you see that column name is given as a string,

31
00:01:34,060 --> 00:01:37,330
and col here, say the first one will

32
00:01:37,330 --> 00:01:40,805
be curb_weight is going to be a string,

33
00:01:40,805 --> 00:01:44,245
and then I'm going to use geom_ point.

34
00:01:44,245 --> 00:01:49,430
So geometry for a pointwise plot which is a scatter plot,

35
00:01:49,430 --> 00:01:53,500
and I can then add another attribute here with another

36
00:01:53,500 --> 00:01:57,634
plus which is a title, ggtitle,

37
00:01:57,634 --> 00:01:59,495
and I'm going to say I'm just

38
00:01:59,495 --> 00:02:01,780
using the R-paste function to paste together

39
00:02:01,780 --> 00:02:03,460
the text scatter plot

40
00:02:03,460 --> 00:02:06,870
of price versus whatever that column is.

41
00:02:06,870 --> 00:02:08,530
Okay? So that way,

42
00:02:08,530 --> 00:02:13,765
we get a title and our audience knows what the plot is.

43
00:02:13,765 --> 00:02:16,850
So let me run that for you and we'll talk about it.

44
00:02:17,390 --> 00:02:19,770
All right. So this first one

45
00:02:19,770 --> 00:02:28,670
is curb_weight on the horizontal axis,

46
00:02:28,670 --> 00:02:30,680
price on the vertical axis.

47
00:02:30,680 --> 00:02:32,300
Right? You can tell that right away

48
00:02:32,300 --> 00:02:34,995
from this in the axis labels.

49
00:02:34,995 --> 00:02:37,460
You see there is a general trend,

50
00:02:37,460 --> 00:02:39,725
this is the kind of structure you're looking for.

51
00:02:39,725 --> 00:02:41,135
So this is our label,

52
00:02:41,135 --> 00:02:43,760
so curb_weight seems to be

53
00:02:43,760 --> 00:02:47,030
a pretty good predictor of price up to a point,

54
00:02:47,030 --> 00:02:50,840
and then you get things like these cars that are very

55
00:02:50,840 --> 00:02:52,890
high priced despite being in

56
00:02:52,890 --> 00:02:55,175
the middle of the range of weights,

57
00:02:55,175 --> 00:02:58,560
and then you have this other cluster of fairly heavy,

58
00:02:58,560 --> 00:03:00,350
fairly expensive cars, but they seem to

59
00:03:00,350 --> 00:03:03,260
be off this general trend.

60
00:03:03,260 --> 00:03:06,800
Likewise with engine size vs price,

61
00:03:06,800 --> 00:03:10,060
it makes sense, big engine cars cost a bit more,

62
00:03:10,060 --> 00:03:11,440
but then you have these cars with

63
00:03:11,440 --> 00:03:14,440
really large engines that are definitely off

64
00:03:14,440 --> 00:03:16,960
the trend and you have these cars with

65
00:03:16,960 --> 00:03:21,430
really small engines that are also well off the trend.

66
00:03:21,430 --> 00:03:23,340
So, that's interesting.

67
00:03:23,340 --> 00:03:25,450
We need to understand and explore that,

68
00:03:25,450 --> 00:03:27,550
and that's why you do this kind of

69
00:03:27,550 --> 00:03:30,130
exploratory graphics before you

70
00:03:30,130 --> 00:03:31,675
build a machine learning model.

71
00:03:31,675 --> 00:03:33,590
A similar story with horsepower,

72
00:03:33,590 --> 00:03:34,690
not quite as pronounced,

73
00:03:34,690 --> 00:03:37,390
but you see there are a fair number of

74
00:03:37,390 --> 00:03:41,065
cars off the main trend here,

75
00:03:41,065 --> 00:03:42,685
mostly on the high side,

76
00:03:42,685 --> 00:03:46,075
that seem to be expensive for the amount of horsepower

77
00:03:46,075 --> 00:03:49,700
they produce and city miles per gallon.

78
00:03:49,700 --> 00:03:51,990
Now, this one is even more complicated because we

79
00:03:51,990 --> 00:03:54,990
have the bulk of the data in here.

80
00:03:54,990 --> 00:03:57,510
It looks like one trend.

81
00:03:57,510 --> 00:03:59,950
We have this other group up here,

82
00:03:59,950 --> 00:04:02,110
so that's what's going on with that.

83
00:04:02,110 --> 00:04:05,290
Then you have this high priced,

84
00:04:05,290 --> 00:04:08,285
low fuel efficiency cluster of cars

85
00:04:08,285 --> 00:04:09,520
and these three cars that are

86
00:04:09,520 --> 00:04:11,940
really highly fuel efficient,

87
00:04:11,940 --> 00:04:15,640
but they're in the low end

88
00:04:15,640 --> 00:04:18,220
of the price with many other cars.

89
00:04:18,220 --> 00:04:21,250
So that is interesting and

90
00:04:21,250 --> 00:04:24,595
gives us many more things we want to understand here,

91
00:04:24,595 --> 00:04:27,205
and we will try to as we go,

92
00:04:27,205 --> 00:04:30,370
but there is a problem with this plot.

93
00:04:30,370 --> 00:04:32,320
I don't know if you can see it or not,

94
00:04:32,320 --> 00:04:34,630
but look at how many dots lie

95
00:04:34,630 --> 00:04:37,855
on top of each other in some of these areas.

96
00:04:37,855 --> 00:04:40,090
You could make the dots a little smaller,

97
00:04:40,090 --> 00:04:43,495
but you'd still have what's called overplotting.

98
00:04:43,495 --> 00:04:47,740
Overplotting is a real problem.

99
00:04:47,740 --> 00:04:50,270
We cannot tell how many cars,

100
00:04:50,270 --> 00:04:52,640
we don't really know the density of data

101
00:04:52,640 --> 00:04:54,150
in some of these areas because there

102
00:04:54,150 --> 00:04:55,900
is so much overplotting.

103
00:04:55,900 --> 00:05:00,035
This is with only 195 samples in our data set.

104
00:05:00,035 --> 00:05:02,180
What if we had thousands which is

105
00:05:02,180 --> 00:05:04,720
often the case or tens of thousands?

106
00:05:04,720 --> 00:05:07,490
Would just be a big black blob

107
00:05:07,490 --> 00:05:10,470
here and you couldn't tell anything about it.

108
00:05:10,470 --> 00:05:14,430
So, how do we deal with this overplotting?

109
00:05:14,430 --> 00:05:16,115
Well, I'm going to give you three

110
00:05:16,115 --> 00:05:17,945
different methods to do that.

111
00:05:17,945 --> 00:05:20,870
One is using transparency.

112
00:05:20,870 --> 00:05:23,040
So this function here,

113
00:05:23,040 --> 00:05:26,120
I call it plot_scatter_t for

114
00:05:26,120 --> 00:05:30,025
transparency and I've only changed one thing.

115
00:05:30,025 --> 00:05:34,320
In geom_point, I've given it a new argument, Alpha.

116
00:05:34,320 --> 00:05:37,580
Now, Alpha is the inverse of transparency,

117
00:05:37,580 --> 00:05:41,130
so Alpha equals one is perfectly opaque,

118
00:05:41,130 --> 00:05:44,280
Alpha equals zero is perfectly transparent.

119
00:05:44,280 --> 00:05:48,750
But I'm going to specify Alpha

120
00:05:48,750 --> 00:05:53,235
of 0.2 and we'll just run that same.

121
00:05:53,235 --> 00:05:54,710
It's basically the same plot,

122
00:05:54,710 --> 00:05:58,770
but with a higher transparency of the points.

123
00:05:59,630 --> 00:06:04,570
You can still see, we're seeing 0.2 transparency or

124
00:06:04,570 --> 00:06:06,080
Alpha is still not so

125
00:06:06,080 --> 00:06:09,820
transparent that the single points disappear.

126
00:06:10,660 --> 00:06:15,880
But you can see the difference between

127
00:06:15,880 --> 00:06:18,310
fairly dense areas and areas

128
00:06:18,310 --> 00:06:21,540
that aren't quite so much overlap.

129
00:06:21,540 --> 00:06:25,409
Especially, when you get these tight groupings,

130
00:06:25,409 --> 00:06:27,430
you really start to see that maybe

131
00:06:27,430 --> 00:06:29,825
31 miles per gallon and at

132
00:06:29,825 --> 00:06:35,080
the fairly low price maybe eight, $9,000.

133
00:06:35,080 --> 00:06:37,255
There is just a lot of cars there still,

134
00:06:37,255 --> 00:06:39,470
even with this high transparency.

135
00:06:39,470 --> 00:06:42,620
But it gives you a clearer view

136
00:06:42,620 --> 00:06:46,385
at least of where those dense areas are.

137
00:06:46,385 --> 00:06:48,680
But transparency is only so good,

138
00:06:48,680 --> 00:06:51,250
it works for small datasets like this.

139
00:06:51,250 --> 00:06:56,124
But, again, if we had 10,000 cars in our sample,

140
00:06:56,124 --> 00:06:58,970
we would still have this blob effect and we couldn't

141
00:06:58,970 --> 00:07:01,680
really pick out any detail.

142
00:07:01,680 --> 00:07:03,840
It's just a big blob.

143
00:07:03,840 --> 00:07:05,340
So, what do we do?

144
00:07:05,340 --> 00:07:09,035
Well, there's two other methods we can try.

145
00:07:09,035 --> 00:07:12,780
One is a 2D Kernel Density estimation plot.

146
00:07:12,780 --> 00:07:14,025
Now, recall, we looked at a

147
00:07:14,025 --> 00:07:19,875
1D Kernel Density estimation plot for distributions.

148
00:07:19,875 --> 00:07:23,304
This is the two dimensional equivalent,

149
00:07:23,304 --> 00:07:25,860
and I'm going to plot under

150
00:07:25,860 --> 00:07:29,780
that the points as well using the same Alpha.

151
00:07:29,780 --> 00:07:31,590
So we see where the points lie

152
00:07:31,590 --> 00:07:34,320
and we'll see the density estimate.

153
00:07:34,320 --> 00:07:36,570
It's plotted as a contour,

154
00:07:36,570 --> 00:07:40,320
so let me just create that plot and I'll show you.

155
00:07:40,390 --> 00:07:43,295
So, for example here,

156
00:07:43,295 --> 00:07:46,310
this is the curb_weight versus price,

157
00:07:46,310 --> 00:07:50,930
and you see we have our high priced cars,

158
00:07:50,930 --> 00:07:52,940
some of the heaviest cars,

159
00:07:52,940 --> 00:07:55,520
but also this curious group here.

160
00:07:55,520 --> 00:07:57,730
But most of our cars,

161
00:07:57,730 --> 00:07:59,240
this is like a contour map,

162
00:07:59,240 --> 00:08:00,580
like a Topo map.

163
00:08:00,580 --> 00:08:03,200
So the peak of our mountain is down

164
00:08:03,200 --> 00:08:06,060
in this area here where the curb_weight of

165
00:08:06,060 --> 00:08:11,810
maybe 20 to 2,300 in a low price,

166
00:08:11,810 --> 00:08:14,105
maybe eight or 9,000.

167
00:08:14,105 --> 00:08:17,450
Okay? Similar for engine size,

168
00:08:17,450 --> 00:08:22,235
even more starkly clustered around the peak,

169
00:08:22,235 --> 00:08:23,600
a narrow peak in here,

170
00:08:23,600 --> 00:08:28,405
but you can see it's really composed of several chunks.

171
00:08:28,405 --> 00:08:32,620
The same for city miles per gallon.

172
00:08:32,620 --> 00:08:35,250
There's actually almost a bi-modal,

173
00:08:35,250 --> 00:08:39,550
there's really two peaks here at two different mileages,

174
00:08:39,550 --> 00:08:41,750
and you see there are

175
00:08:41,750 --> 00:08:43,280
these other clusters of

176
00:08:43,280 --> 00:08:47,400
high priced cars that are not fuel efficient.

177
00:08:49,810 --> 00:08:52,730
So there's this third method

178
00:08:52,730 --> 00:08:55,185
which is called a Hexbin plot.

179
00:08:55,185 --> 00:09:00,795
So the Hexbin plot is like the contour density plot,

180
00:09:00,795 --> 00:09:03,290
but it's computationally much more efficient.

181
00:09:03,290 --> 00:09:06,785
It's the 2D equivalent of a histogram and we use

182
00:09:06,785 --> 00:09:11,690
hexagonal bins because they pack tightly in 2D.

183
00:09:12,240 --> 00:09:14,640
There's a few things I'm doing here.

184
00:09:14,640 --> 00:09:18,100
So I've got geom_hex here,

185
00:09:18,440 --> 00:09:21,730
and I show a legend equals true,

186
00:09:21,730 --> 00:09:24,595
so I know how dense this is.

187
00:09:24,595 --> 00:09:27,240
This is the number of bins on

188
00:09:27,240 --> 00:09:29,190
the horizontal and vertical axis,

189
00:09:29,190 --> 00:09:30,240
so I can adjust that,

190
00:09:30,240 --> 00:09:33,495
I can have fewer or more bins

191
00:09:33,495 --> 00:09:39,360
depending on how much detail I want to show.

192
00:09:39,800 --> 00:09:47,765
So let me run that and you see how this goes.

193
00:09:47,765 --> 00:09:51,690
So, the lighter the color here,

194
00:09:51,690 --> 00:09:54,120
the denser, the hotter basically.

195
00:09:54,120 --> 00:09:56,390
So you see the really hot area of

196
00:09:56,390 --> 00:10:01,395
curb_weight is actually under 2,000.

197
00:10:01,395 --> 00:10:04,755
There's a little bit over 2,000 as well,

198
00:10:04,755 --> 00:10:07,630
and at a low price point, maybe 9,000.

199
00:10:07,630 --> 00:10:10,275
You see some other dense,

200
00:10:10,275 --> 00:10:13,970
a little bit lighter areas in here too.

201
00:10:13,970 --> 00:10:18,670
Then these areas with only one car in them

202
00:10:18,670 --> 00:10:23,475
basically are in this very darkest color.

203
00:10:23,475 --> 00:10:25,180
But you get a lot of view of

204
00:10:25,180 --> 00:10:27,560
the structure with this hexbins.

205
00:10:27,560 --> 00:10:30,940
Similar for say engine size,

206
00:10:30,940 --> 00:10:32,965
you see there's really one hot point,

207
00:10:32,965 --> 00:10:35,090
one bright point here,

208
00:10:35,090 --> 00:10:38,800
fairly small engine, fairly low price.

209
00:10:38,800 --> 00:10:41,965
So we're learning quite a bit

210
00:10:41,965 --> 00:10:44,710
about the structure of the data from these Hexbin plots,

211
00:10:44,710 --> 00:10:46,420
and we can see

212
00:10:46,420 --> 00:10:48,490
a little more complexity with

213
00:10:48,490 --> 00:10:51,255
city miles per gallon versus price that

214
00:10:51,255 --> 00:10:55,270
there's 30 or 31 miles per gallon low

215
00:10:55,270 --> 00:10:57,250
price and then maybe another

216
00:10:57,250 --> 00:11:00,155
light area in here as we suspect it.

217
00:11:00,155 --> 00:11:02,130
So this is another way,

218
00:11:02,130 --> 00:11:03,390
and because this is

219
00:11:03,390 --> 00:11:06,120
a bidding process rather than a contouring process,

220
00:11:06,120 --> 00:11:09,345
it's quite a bit more computationally inefficient.

221
00:11:09,345 --> 00:11:11,420
You can create Hexbin plots

222
00:11:11,420 --> 00:11:14,590
for very, very large datasets.

223
00:11:15,830 --> 00:11:20,220
So, that gives you some methods

224
00:11:20,220 --> 00:11:21,600
for looking at

225
00:11:21,600 --> 00:11:24,045
the relationship between numeric variables.

226
00:11:24,045 --> 00:11:26,280
We looked at plain old scatter plots,

227
00:11:26,280 --> 00:11:30,225
which you've surely worked with before and seen before.

228
00:11:30,225 --> 00:11:33,900
We then dealt with the problem of overplotting,

229
00:11:33,900 --> 00:11:35,610
which is a really big problem

230
00:11:35,610 --> 00:11:37,410
for machine learning problems,

231
00:11:37,410 --> 00:11:38,880
well, in data science in general.

232
00:11:38,880 --> 00:11:41,640
I gave you three different methods

233
00:11:41,640 --> 00:11:43,875
to deal with it: transparency,

234
00:11:43,875 --> 00:11:48,165
which works for small to moderate size datasets,

235
00:11:48,165 --> 00:11:54,200
contour plots, 2D Kernel Density estimation

236
00:11:54,200 --> 00:11:56,060
plots which worked for, again,

237
00:11:56,060 --> 00:11:58,460
small to medium sized datasets

238
00:11:58,460 --> 00:12:00,560
and give you a different view of that,

239
00:12:00,560 --> 00:12:02,280
and finally, Hexbin plots,

240
00:12:02,280 --> 00:12:05,930
which can be scaled up to very large datasets.

