0
00:00:00,380 --> 00:00:02,655
>> Hello and welcome.

1
00:00:02,655 --> 00:00:03,990
In this video, I'm going to talk

2
00:00:03,990 --> 00:00:06,030
about exploratory methods

3
00:00:06,030 --> 00:00:09,240
we use for data before we apply

4
00:00:09,240 --> 00:00:13,120
Machine Learning models and actually after we apply

5
00:00:13,120 --> 00:00:14,865
Machine Learning models to

6
00:00:14,865 --> 00:00:17,550
understand the results in many cases.

7
00:00:17,550 --> 00:00:20,830
So, the first question you might be asking is,

8
00:00:20,830 --> 00:00:23,250
why are we talking about data exploration and

9
00:00:23,250 --> 00:00:25,810
data visualization in a Machine Learning class?

10
00:00:25,810 --> 00:00:27,390
Well, it's very simple.

11
00:00:27,390 --> 00:00:29,445
We need to understand the data

12
00:00:29,445 --> 00:00:32,690
before we get too far down the road.

13
00:00:32,820 --> 00:00:37,105
We're dealing with complex datasets, almost invariably,

14
00:00:37,105 --> 00:00:38,730
and it can be a real challenge to

15
00:00:38,730 --> 00:00:41,880
understand what the content of the data are,

16
00:00:41,880 --> 00:00:43,640
what the relationships between,

17
00:00:43,640 --> 00:00:46,980
say, features and features and label are,

18
00:00:46,980 --> 00:00:49,530
and what's just noise,

19
00:00:50,360 --> 00:00:53,610
what's content of that data that's going to cause us

20
00:00:53,610 --> 00:00:56,455
problems when we go to training Machine Learning models.

21
00:00:56,455 --> 00:00:58,380
The more time you spend

22
00:00:58,380 --> 00:01:00,350
exploring and understanding the data,

23
00:01:00,350 --> 00:01:02,850
the less time you're going to waste building

24
00:01:02,850 --> 00:01:06,045
the wrong model or getting substandard results.

25
00:01:06,045 --> 00:01:09,720
Our goal is always

26
00:01:09,720 --> 00:01:13,230
to make the best use of the information in the dataset.

27
00:01:13,230 --> 00:01:16,260
We're trying to understand what parts of the data really

28
00:01:16,260 --> 00:01:19,560
inform understanding the label,

29
00:01:19,560 --> 00:01:21,450
and what is just redundant or

30
00:01:21,450 --> 00:01:24,350
noise or errors or something like that.

31
00:01:24,350 --> 00:01:27,300
So, we're trying to avoid problems,

32
00:01:27,300 --> 00:01:30,710
we're trying to avoid backtracking,

33
00:01:30,710 --> 00:01:35,845
and having poor results from our Machine Learning model.

34
00:01:35,845 --> 00:01:38,030
I would go so far as to say that

35
00:01:38,030 --> 00:01:40,310
Machine Learning without understanding the data,

36
00:01:40,310 --> 00:01:42,230
without doing a detailed exploration

37
00:01:42,230 --> 00:01:43,850
is just a waste of time.

38
00:01:43,850 --> 00:01:45,260
Maybe you'll get lucky,

39
00:01:45,260 --> 00:01:47,210
maybe you're the luckiest person in the world,

40
00:01:47,210 --> 00:01:48,740
but I wouldn't bet on it.

41
00:01:48,740 --> 00:01:49,880
It's one of those cases, you build

42
00:01:49,880 --> 00:01:51,260
your own luck and you're going to build

43
00:01:51,260 --> 00:01:52,850
your own luck in Machine Learning by

44
00:01:52,850 --> 00:01:55,355
understanding the data and exploring it.

45
00:01:55,355 --> 00:01:57,070
So, what are you looking for?

46
00:01:57,070 --> 00:01:59,400
What's the what's the point here?

47
00:01:59,400 --> 00:02:01,250
First off, obviously,

48
00:02:01,250 --> 00:02:03,280
just general characteristics of the data.

49
00:02:03,280 --> 00:02:05,640
How big is it? What data types are there?

50
00:02:05,640 --> 00:02:07,790
How many features are there?

51
00:02:07,790 --> 00:02:09,410
What kind of label is there?

52
00:02:09,410 --> 00:02:12,705
These are very simple questions they ask,

53
00:02:12,705 --> 00:02:14,930
but if you don't really understand that you're really

54
00:02:14,930 --> 00:02:17,655
not even at the starting gates too.

55
00:02:17,655 --> 00:02:19,910
You can look at summary statistics.

56
00:02:19,910 --> 00:02:22,730
You can look at things like,

57
00:02:22,730 --> 00:02:24,230
if it's numeric features or

58
00:02:24,230 --> 00:02:26,180
numeric label, what's the mean,

59
00:02:26,180 --> 00:02:29,700
the median, the quantiles, things like that.

60
00:02:29,700 --> 00:02:34,040
So you understand the scale and range of variables,

61
00:02:34,040 --> 00:02:38,730
and you're looking also for errors and outliers.

62
00:02:38,730 --> 00:02:40,380
You're doing this both through

63
00:02:40,380 --> 00:02:43,930
statistical methods and visualization.

64
00:02:43,930 --> 00:02:47,090
There's a difference between errors and outliers,

65
00:02:47,090 --> 00:02:48,570
which we'll talk about,

66
00:02:48,570 --> 00:02:52,995
but keep that in mind,

67
00:02:52,995 --> 00:02:55,020
an error is one thing,

68
00:02:55,020 --> 00:02:58,155
an outlier may actually be interesting data.

69
00:02:58,155 --> 00:03:01,165
For numeric features, particularly we want understand

70
00:03:01,165 --> 00:03:04,290
the distribution of the feature or the label,

71
00:03:04,290 --> 00:03:06,630
if the label is a numeric label

72
00:03:06,630 --> 00:03:09,110
as opposed to a categorical label.

73
00:03:09,110 --> 00:03:14,740
For categorical variables, instead of distributions,

74
00:03:14,740 --> 00:03:16,305
we look at frequencies.

75
00:03:16,305 --> 00:03:20,070
So, there'll be different categories or

76
00:03:20,070 --> 00:03:22,560
classes and we want to know what

77
00:03:22,560 --> 00:03:25,930
the frequencies of those and how they relate and how,

78
00:03:25,930 --> 00:03:28,200
say, the frequencies of

79
00:03:28,200 --> 00:03:32,075
a feature relate to the frequencies in the label.

80
00:03:32,075 --> 00:03:34,680
And that's really the key,

81
00:03:34,680 --> 00:03:37,260
we want to know what features are going to

82
00:03:37,260 --> 00:03:41,250
inform predicting that label, and also,

83
00:03:41,250 --> 00:03:42,000
we want to understand

84
00:03:42,000 --> 00:03:43,380
the relationship between the features,

85
00:03:43,380 --> 00:03:45,835
because you may see that there are features,

86
00:03:45,835 --> 00:03:48,190
there may be a feature that says,

87
00:03:48,190 --> 00:03:49,970
"Highest degree earned" and

88
00:03:49,970 --> 00:03:51,990
another one that says, "Years in education."

89
00:03:51,990 --> 00:03:53,400
Well, maybe you don't want to use

90
00:03:53,400 --> 00:03:55,365
both of those because they may be highly,

91
00:03:55,365 --> 00:03:58,190
even though one's categorical and one might be numeric,

92
00:03:58,190 --> 00:04:01,890
they're probably highly correlated.

93
00:04:02,780 --> 00:04:05,640
So, let's talk about the process here.

94
00:04:05,640 --> 00:04:07,790
Data exploration is interactive.

95
00:04:07,790 --> 00:04:09,650
There is no linear path,

96
00:04:09,650 --> 00:04:11,760
there is no recipe I can give you that starts at

97
00:04:11,760 --> 00:04:14,680
the top and you fall out at the bottom and you're done.

98
00:04:14,680 --> 00:04:17,630
You plan on repeating steps,

99
00:04:17,630 --> 00:04:21,480
plan on going back and doing things.

100
00:04:21,480 --> 00:04:24,960
So, for example, say you started exploring

101
00:04:24,960 --> 00:04:26,370
data and you find there's a bunch of

102
00:04:26,370 --> 00:04:28,170
errors and you correct those errors,

103
00:04:28,170 --> 00:04:30,615
well, you need to go back and redo your visualization.

104
00:04:30,615 --> 00:04:32,670
Maybe you'll find some new problem that wasn't

105
00:04:32,670 --> 00:04:35,595
evident because of the initial problems or errors,

106
00:04:35,595 --> 00:04:39,650
or maybe everything's peachy now, you don't know.

107
00:04:39,650 --> 00:04:42,480
You want to look before and after you do

108
00:04:42,480 --> 00:04:45,575
any data preparation steps,

109
00:04:45,575 --> 00:04:47,460
and before and after

110
00:04:47,460 --> 00:04:48,820
constructing a Machine Learning model.

111
00:04:48,820 --> 00:04:51,070
You want to visualize as best you can

112
00:04:51,070 --> 00:04:53,040
the results from the Machine Learning model

113
00:04:53,040 --> 00:04:55,250
to understand the errors,

114
00:04:55,250 --> 00:04:57,540
are the errors just coming from

115
00:04:57,540 --> 00:04:59,700
random variation in the data or is there

116
00:04:59,700 --> 00:05:01,830
something systematic wrong here

117
00:05:01,830 --> 00:05:05,040
that you've missed in preparing your data?

118
00:05:05,250 --> 00:05:07,840
Visualization can also be very

119
00:05:07,840 --> 00:05:10,030
helpful in evaluating Machine Learning models,

120
00:05:10,030 --> 00:05:11,135
and we'll go through that in

121
00:05:11,135 --> 00:05:13,700
other modules of this course.

122
00:05:13,700 --> 00:05:16,060
You can imagine that, any one of

123
00:05:16,060 --> 00:05:17,630
those bullet points there,

124
00:05:17,630 --> 00:05:20,270
something can go wrong or you miss something,

125
00:05:20,270 --> 00:05:22,900
it's inevitable, and you need to then go

126
00:05:22,900 --> 00:05:25,960
back and repeat, often probably.

127
00:05:25,960 --> 00:05:28,560
I find myself, when I'm doing Machine Learning,

128
00:05:28,560 --> 00:05:29,775
building Machine Learning models,

129
00:05:29,775 --> 00:05:32,140
I'm going back and forth and back and forth,

130
00:05:32,140 --> 00:05:36,620
and trying to see what works and what doesn't.

131
00:05:37,150 --> 00:05:39,440
So, my general guidance is,

132
00:05:39,440 --> 00:05:40,685
try lots of ideas,

133
00:05:40,685 --> 00:05:44,880
expect some to work and expect a lot not to work.

134
00:05:44,880 --> 00:05:46,830
It is an exploration process and that's

135
00:05:46,830 --> 00:05:48,870
why we call it data exploration.

136
00:05:48,870 --> 00:05:50,880
So, just like if you landed on

137
00:05:50,880 --> 00:05:53,100
an island and you had no map and you had to

138
00:05:53,100 --> 00:05:55,275
find resources to survive

139
00:05:55,275 --> 00:05:57,300
on an island that you might be stranded on,

140
00:05:57,300 --> 00:06:00,900
you'd need to find water and food and other things,

141
00:06:00,900 --> 00:06:02,580
maybe find areas to avoid with

142
00:06:02,580 --> 00:06:04,815
hostile animals or something like that.

143
00:06:04,815 --> 00:06:06,900
You're going to explore and you're not going

144
00:06:06,900 --> 00:06:10,540
to know where to go to start with.

