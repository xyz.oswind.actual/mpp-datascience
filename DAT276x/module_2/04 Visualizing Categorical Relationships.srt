0
00:00:01,610 --> 00:00:04,350
>> Hello, welcome back.

1
00:00:04,350 --> 00:00:08,630
So in the last demo we looked at ways we could

2
00:00:08,630 --> 00:00:13,845
visualize the relationship between two numeric variables.

3
00:00:13,845 --> 00:00:16,795
A lot of our variables are categorical,

4
00:00:16,795 --> 00:00:20,150
and so how do we visualize the relationship between

5
00:00:20,150 --> 00:00:24,350
a categorical variable and a numeric variable?

6
00:00:24,350 --> 00:00:26,940
And there's two different ways we can do that.

7
00:00:26,940 --> 00:00:29,585
And again, this is important if you have

8
00:00:29,585 --> 00:00:33,905
categorical features in a numeric label

9
00:00:33,905 --> 00:00:36,140
or a categorical label

10
00:00:36,140 --> 00:00:37,970
in numeric features and you want to see what

11
00:00:37,970 --> 00:00:40,490
their relationships are or if you

12
00:00:40,490 --> 00:00:42,350
want to see if there's interaction

13
00:00:42,350 --> 00:00:45,920
between a categorical feature and a numeric feature.

14
00:00:45,920 --> 00:00:48,930
So the two different methods we can use.

15
00:00:48,930 --> 00:00:52,030
The first one is called a boxplot,

16
00:00:52,030 --> 00:00:53,330
and the reason for that will become

17
00:00:53,330 --> 00:00:55,230
evident when I make it.

18
00:00:55,230 --> 00:00:58,010
And so I have a list

19
00:00:58,010 --> 00:01:00,960
here of categorical columns: Fuel type,

20
00:01:00,960 --> 00:01:02,610
aspiration, number of doors,

21
00:01:02,610 --> 00:01:05,050
body style et cetera.

22
00:01:06,610 --> 00:01:10,400
I have this function which I call plot_box.

23
00:01:10,400 --> 00:01:14,450
And again just like

24
00:01:14,450 --> 00:01:18,605
before it iterates over a list of columns.

25
00:01:18,605 --> 00:01:22,430
We have the Y so that's our vertical axis,

26
00:01:22,430 --> 00:01:23,560
our Y is going to be price.

27
00:01:23,560 --> 00:01:25,625
So that's our numeric variable,

28
00:01:25,625 --> 00:01:30,840
and we want to see how price interacts with fuel type,

29
00:01:30,840 --> 00:01:33,860
aspiration, number of doors, et cetera.

30
00:01:33,860 --> 00:01:37,890
So this part should be old hat

31
00:01:37,890 --> 00:01:41,575
by now we use ggplot on the data frame,

32
00:01:41,575 --> 00:01:43,845
aes string on a column.

33
00:01:43,845 --> 00:01:45,960
But now notice that

34
00:01:45,960 --> 00:01:49,270
this is going to be the name of a categorical column,

35
00:01:49,270 --> 00:01:53,460
this is the name of a numeric column, namely price,

36
00:01:53,460 --> 00:01:58,410
and we use geom boxplot and our title.

37
00:01:58,410 --> 00:02:01,050
So let me make some boxplots and you'll see

38
00:02:01,050 --> 00:02:04,750
what happens and why this is interesting.

39
00:02:05,360 --> 00:02:09,220
So here's our first boxplot.

40
00:02:12,240 --> 00:02:14,765
So it's fuel type and there's

41
00:02:14,765 --> 00:02:16,690
only two fuel types in these cars.

42
00:02:16,690 --> 00:02:18,995
They're either diesel or gas.

43
00:02:18,995 --> 00:02:22,025
And here's price on the vertical.

44
00:02:22,025 --> 00:02:24,465
So this is a categorical variable,

45
00:02:24,465 --> 00:02:25,880
has two possible values,

46
00:02:25,880 --> 00:02:28,360
diesel or gas, and price.

47
00:02:28,360 --> 00:02:30,410
So looking at the boxplot you can

48
00:02:30,410 --> 00:02:35,150
compare the price behavior of diesel to gas cars.

49
00:02:35,150 --> 00:02:37,675
This boxplot's kind of a funny looking thing.

50
00:02:37,675 --> 00:02:39,445
What does this mean?

51
00:02:39,445 --> 00:02:43,810
So first off, I'll draw your attention to the box,

52
00:02:43,810 --> 00:02:47,255
which is why we call this a boxplot.

53
00:02:47,255 --> 00:02:50,830
And you see the box

54
00:02:50,830 --> 00:02:54,220
and half the data values are within the box.

55
00:02:54,220 --> 00:02:57,310
So that's the inner two quartiles and we call

56
00:02:57,310 --> 00:03:00,560
this distance from here to here the interquartile range.

57
00:03:00,560 --> 00:03:04,830
And you see this dark black line

58
00:03:04,830 --> 00:03:07,130
here in the middle of the box,

59
00:03:07,130 --> 00:03:11,550
that is the median or the 50 percent quartile.

60
00:03:11,550 --> 00:03:14,290
And then there are

61
00:03:14,290 --> 00:03:17,745
these little things called the whiskers that stick out,

62
00:03:17,745 --> 00:03:19,480
and the length of the whisker is

63
00:03:19,480 --> 00:03:21,070
determined by a rule that it's

64
00:03:21,070 --> 00:03:24,555
either the most extreme value.

65
00:03:24,555 --> 00:03:26,500
So the whiskers are short on

66
00:03:26,500 --> 00:03:31,210
the downward side here because that probably represents

67
00:03:31,210 --> 00:03:34,120
the cheapest diesel car in the sample and

68
00:03:34,120 --> 00:03:35,680
the cheapest gas car in

69
00:03:35,680 --> 00:03:38,250
the sample was around, I believe, 5,000.

70
00:03:38,250 --> 00:03:41,315
So we just chopped the whisker off at that.

71
00:03:41,315 --> 00:03:45,920
Or it can be as long as one and a half times this

72
00:03:45,920 --> 00:03:48,025
interquartile range and it doesn't look like

73
00:03:48,025 --> 00:03:52,180
either of these really are.

74
00:03:52,180 --> 00:03:56,270
But if you have values that are outside that

75
00:03:56,270 --> 00:04:00,615
one and a half times that interquartile range like these,

76
00:04:00,615 --> 00:04:02,570
then you use some sort of marker,

77
00:04:02,570 --> 00:04:05,360
like in this case a little diamond is used,

78
00:04:05,360 --> 00:04:07,220
to show that we have these outliers because we have

79
00:04:07,220 --> 00:04:09,170
these very expensive gas cars

80
00:04:09,170 --> 00:04:13,630
that are kinda outliers from the bulk of our data.

81
00:04:13,630 --> 00:04:15,980
So something you can look at here since we're

82
00:04:15,980 --> 00:04:18,470
trying to predict prices,

83
00:04:18,470 --> 00:04:23,190
is fuel type actually a useful predictor of price?

84
00:04:23,190 --> 00:04:24,600
Well, it's questionable.

85
00:04:24,600 --> 00:04:31,100
You can see that this quartile

86
00:04:31,100 --> 00:04:34,470
somewhat overlaps with this quartile

87
00:04:34,470 --> 00:04:36,165
and then the whisker,

88
00:04:36,165 --> 00:04:37,730
in this case because there is no outliers,

89
00:04:37,730 --> 00:04:40,570
represents another quartile completely

90
00:04:40,570 --> 00:04:41,950
overlap the gas car.

91
00:04:41,950 --> 00:04:43,510
So you would say at best this is

92
00:04:43,510 --> 00:04:46,500
a weak predictor of auto price.

93
00:04:46,500 --> 00:04:49,480
Plus, the range of the gas cars,

94
00:04:49,480 --> 00:04:51,774
even though it may be on average or median,

95
00:04:51,774 --> 00:04:53,885
diesel cars are more expensive.

96
00:04:53,885 --> 00:04:56,260
They're completely overlapped by

97
00:04:56,260 --> 00:04:58,580
the range of price of gas cars.

98
00:04:58,580 --> 00:05:00,945
And we get a similar view for, say,

99
00:05:00,945 --> 00:05:03,550
standard cars versus turbo cars only here

100
00:05:03,550 --> 00:05:06,670
the overlap doesn't seem to be quite as extreme because,

101
00:05:06,670 --> 00:05:08,410
again, here's the median.

102
00:05:08,410 --> 00:05:11,590
So half the price values of

103
00:05:11,590 --> 00:05:14,890
standard aspiration cars are

104
00:05:14,890 --> 00:05:19,900
actually almost below the cheapest turbo car.

105
00:05:20,990 --> 00:05:23,180
And we have these, again,

106
00:05:23,180 --> 00:05:27,230
expensive cars that are outliers in this case.

107
00:05:27,230 --> 00:05:33,390
But it still might be a predictor and we can keep going.

108
00:05:33,390 --> 00:05:36,400
You get complicated relationships

109
00:05:36,400 --> 00:05:38,370
like number of drive wheel.

110
00:05:38,370 --> 00:05:40,870
So now we start to see this is where boxplots are really

111
00:05:40,870 --> 00:05:43,510
useful when you have many relationships.

112
00:05:43,510 --> 00:05:45,460
So you can see that rear wheel drive cars,

113
00:05:45,460 --> 00:05:47,710
yeah they really do look like they're

114
00:05:47,710 --> 00:05:50,780
more expensive than four wheel drive or front wheel drive

115
00:05:50,780 --> 00:05:54,175
whereas there's not that much difference.

116
00:05:54,175 --> 00:05:57,010
Basically, a lot of overlap between four wheel drive

117
00:05:57,010 --> 00:05:59,925
and front wheel drive cars et cetera.

118
00:05:59,925 --> 00:06:02,705
And engine location,

119
00:06:02,705 --> 00:06:04,960
rear engine location cars are all

120
00:06:04,960 --> 00:06:07,460
expensive and et cetera.

121
00:06:07,460 --> 00:06:09,520
So let me just talk about one

122
00:06:09,520 --> 00:06:12,030
more of these which is number of cylinders.

123
00:06:12,030 --> 00:06:13,780
And you see, that's another thing you can

124
00:06:13,780 --> 00:06:15,670
get out of visual examination.

125
00:06:15,670 --> 00:06:17,950
We only have one 12 cylinder car,

126
00:06:17,950 --> 00:06:19,570
one three cylinder car.

127
00:06:19,570 --> 00:06:26,170
Well the 12 cylinder car seems to fit into the 8s, maybe?

128
00:06:26,170 --> 00:06:28,120
The 5 and the 6 have

129
00:06:28,120 --> 00:06:32,060
very similar range and maybe this 3 fits in with a 4.

130
00:06:32,060 --> 00:06:35,230
So in terms of maybe doing some feature engineering,

131
00:06:35,230 --> 00:06:37,110
this gives us a clue where to go.

132
00:06:37,110 --> 00:06:40,495
Well, there's another great way to

133
00:06:40,495 --> 00:06:42,550
present that same information and

134
00:06:42,550 --> 00:06:45,230
that's with a violin plot.

135
00:06:45,230 --> 00:06:48,185
And you see it's basically the same code as we had

136
00:06:48,185 --> 00:06:53,760
before except I use geom_violin here.

137
00:06:53,760 --> 00:06:55,430
Let me recreate that.

138
00:06:55,430 --> 00:06:59,500
It's easier to talk about than to- so remember,

139
00:06:59,500 --> 00:07:01,540
we talked about kernel density

140
00:07:01,540 --> 00:07:03,730
estimation plots when we looked

141
00:07:03,730 --> 00:07:08,485
at distributions and so that's what this curve is here.

142
00:07:08,485 --> 00:07:13,150
It's two back to back kernel density estimation plots.

143
00:07:13,150 --> 00:07:18,120
And then the data the autos are grouped by,

144
00:07:18,120 --> 00:07:21,100
this is like a group by operation, basically.

145
00:07:21,100 --> 00:07:23,170
So diesel and gas.

146
00:07:23,170 --> 00:07:24,770
And now you see you get

147
00:07:24,770 --> 00:07:26,470
somewhat of a different view from this.

148
00:07:26,470 --> 00:07:30,935
You see these few high priced cars,

149
00:07:30,935 --> 00:07:34,830
you see this peak here of gas cars and the

150
00:07:34,830 --> 00:07:40,140
diesel's mostly more expensive than beyond that peak.

151
00:07:40,140 --> 00:07:42,625
And again, similar story for

152
00:07:42,625 --> 00:07:45,550
standard versus turbo aspiration.

153
00:07:45,550 --> 00:07:47,395
So you get the idea that maybe

154
00:07:47,395 --> 00:07:49,930
aspiration might be a decent predictor,

155
00:07:49,930 --> 00:07:52,290
not a great predictor.

156
00:07:53,480 --> 00:07:59,160
And let's go back down to number of cylinders.

157
00:07:59,160 --> 00:08:01,090
Well, they don't even plot the 3 and the

158
00:08:01,090 --> 00:08:04,175
12 but you can see that 8s,

159
00:08:04,175 --> 00:08:07,845
there's a little overlap between 8s and 6s.

160
00:08:07,845 --> 00:08:10,400
Now one thing you should keep in mind,

161
00:08:10,400 --> 00:08:12,530
they are kernel density estimation plots,

162
00:08:12,530 --> 00:08:13,800
so the area under

163
00:08:13,800 --> 00:08:18,220
each half curve has to integrate to one.

164
00:08:18,220 --> 00:08:20,900
So, the fact that the five cylinder cars

165
00:08:20,900 --> 00:08:22,480
are more strung out,

166
00:08:22,480 --> 00:08:24,300
there's no real peak here,

167
00:08:24,300 --> 00:08:29,850
but that area under that half or that half is one.

168
00:08:29,850 --> 00:08:32,280
The area under this half or that half is one.

169
00:08:32,280 --> 00:08:34,410
So there's really a lot more concentration of

170
00:08:34,410 --> 00:08:37,830
the four cylinder cars than the five cylinder cars.

171
00:08:37,830 --> 00:08:43,890
So that gives you some useful other information.

172
00:08:43,890 --> 00:08:46,690
So those are two different ways,

173
00:08:46,690 --> 00:08:50,200
they give you two different views of how to work

174
00:08:50,200 --> 00:08:52,090
with the relationship between

175
00:08:52,090 --> 00:08:54,250
categorical features and numerical features,

176
00:08:54,250 --> 00:08:57,530
which are the boxplot and the violin plot.

