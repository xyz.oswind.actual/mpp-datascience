0
00:00:00,000 --> 00:00:05,150
>> So we've been looking at some visualization methods,

1
00:00:05,150 --> 00:00:06,890
but there's one important topic

2
00:00:06,890 --> 00:00:08,800
we still need to pick up here which is,

3
00:00:08,800 --> 00:00:14,240
what are visualizations we can use to understand

4
00:00:14,240 --> 00:00:17,255
how well features can be

5
00:00:17,255 --> 00:00:20,380
used for classification problems.

6
00:00:20,380 --> 00:00:23,900
So in classification, so the ideal feature for

7
00:00:23,900 --> 00:00:28,290
a classifier does what we call separates the classes.

8
00:00:28,290 --> 00:00:32,410
So, you want distinct values of that feature,

9
00:00:32,410 --> 00:00:35,110
be it categorical or numerical,

10
00:00:35,110 --> 00:00:40,180
that are different for each category of the label,

11
00:00:40,180 --> 00:00:43,060
and most features aren't going to be that great,

12
00:00:43,060 --> 00:00:45,700
but you still want to look at and see

13
00:00:45,700 --> 00:00:48,820
which ones might be probably just noise,

14
00:00:48,820 --> 00:00:50,860
which ones seem promising, etc.

15
00:00:50,860 --> 00:00:53,020
So, we'll look at methods for

16
00:00:53,020 --> 00:00:56,020
the numerical and categorical features

17
00:00:56,020 --> 00:00:59,390
to try to understand that relationship a little bit.

18
00:00:59,390 --> 00:01:04,030
So, I'm going to start with

19
00:01:04,030 --> 00:01:07,420
numeric features in our first way

20
00:01:07,420 --> 00:01:09,710
we do this is with boxplots.

21
00:01:09,710 --> 00:01:12,480
So, not too surprisingly,

22
00:01:12,480 --> 00:01:15,710
so I'm working with the German Credit Data and I've got

23
00:01:15,710 --> 00:01:21,650
these six numeric features.

24
00:01:21,650 --> 00:01:24,500
My label is Bad Credit

25
00:01:24,500 --> 00:01:27,645
is what it's called and so it has two values;

26
00:01:27,645 --> 00:01:30,350
either good customer is labeled

27
00:01:30,350 --> 00:01:33,540
as zero or bad customer is labeled as one.

28
00:01:33,540 --> 00:01:36,730
And so I loop over each of

29
00:01:36,730 --> 00:01:38,500
those columns and I'm going to create

30
00:01:38,500 --> 00:01:41,870
a boxplot and give it a proper title.

31
00:01:43,460 --> 00:01:47,620
Okay. So, let's look at the first boxplot here.

32
00:01:47,620 --> 00:01:49,370
So I've got my good credit customers,

33
00:01:49,370 --> 00:01:51,505
my bad credit customers,

34
00:01:51,505 --> 00:01:54,640
I've got my loan duration in months.

35
00:01:54,640 --> 00:01:58,160
Well, here's the inner two quartiles

36
00:01:58,160 --> 00:02:00,540
of the good customers and

37
00:02:00,540 --> 00:02:03,110
the inner two quartiles of the bad customer.

38
00:02:03,110 --> 00:02:06,795
So, it looks like three quarters

39
00:02:06,795 --> 00:02:10,345
of the good customers have loan durations less than,

40
00:02:10,345 --> 00:02:14,675
say, half because we've got one quartile,

41
00:02:14,675 --> 00:02:18,130
quartile here of the bad credit customer.

42
00:02:18,130 --> 00:02:20,620
So, there is some tendency for

43
00:02:20,620 --> 00:02:23,845
the numeric value loan duration,

44
00:02:23,845 --> 00:02:27,430
that longer loan durations have a slight tendency,

45
00:02:27,430 --> 00:02:29,330
and I wouldn't say it's overwhelming,

46
00:02:29,330 --> 00:02:33,060
toward having longer duration

47
00:02:33,060 --> 00:02:35,320
for the bad credit customers.

48
00:02:35,320 --> 00:02:39,730
Now, a little bit more dubious is say, loan amount.

49
00:02:39,730 --> 00:02:41,680
You see, there's mostly

50
00:02:41,680 --> 00:02:44,430
overlap with these interquartile ranges.

51
00:02:44,430 --> 00:02:48,640
So, this is probably even a weaker predictor.

52
00:02:48,640 --> 00:02:50,740
This is, say you

53
00:02:50,740 --> 00:02:52,800
sometimes run into tricky cases like this.

54
00:02:52,800 --> 00:02:54,655
This is payment percent of

55
00:02:54,655 --> 00:02:58,565
income and it's quantized as only 1, 2,

56
00:02:58,565 --> 00:03:02,020
3 and 4, and I suspect the bank won't give

57
00:03:02,020 --> 00:03:03,365
loans to people of

58
00:03:03,365 --> 00:03:06,730
more than 4% of their income is required to pay it back.

59
00:03:06,730 --> 00:03:12,765
And you see, the median value for good customers is 3%,

60
00:03:12,765 --> 00:03:15,490
the median value for bad credit customers

61
00:03:15,490 --> 00:03:19,420
and in fact the upper two quartiles are 4%,

62
00:03:19,420 --> 00:03:22,520
and you don't even see the whisker on this one either.

63
00:03:22,520 --> 00:03:23,770
So, there's a lot of

64
00:03:23,770 --> 00:03:26,855
overlap here but this might be predictive.

65
00:03:26,855 --> 00:03:29,650
And then we get to age and you can see there's

66
00:03:29,650 --> 00:03:32,640
almost complete overlap of the inner quartile ranges,

67
00:03:32,640 --> 00:03:37,325
so that's probably not going to be a great predictor.

68
00:03:37,325 --> 00:03:42,940
So, the other way we can do this is with violin plots,

69
00:03:42,950 --> 00:03:45,910
and the only difference between the code I

70
00:03:45,910 --> 00:03:48,430
showed you and this one is I'm using geom_violin.

71
00:03:48,430 --> 00:03:53,860
But you get a little different detail,

72
00:03:53,860 --> 00:03:57,105
not better not worse, but definitely different.

73
00:03:57,105 --> 00:03:59,000
For example, for loan duration,

74
00:03:59,000 --> 00:04:00,960
we can see for our good credit customers,

75
00:04:00,960 --> 00:04:04,085
loans tend to be annual;

76
00:04:04,085 --> 00:04:08,510
12-month loans, 24-month loans, 36-month loans,

77
00:04:08,510 --> 00:04:13,255
48-month loans and even a few 60-month loans.

78
00:04:13,255 --> 00:04:15,840
So, our bad credit customers,

79
00:04:15,840 --> 00:04:19,100
it's a little more diffused and you can

80
00:04:19,100 --> 00:04:20,720
see the neck of the violin is

81
00:04:20,720 --> 00:04:22,550
definitely thicker and longer.

82
00:04:22,550 --> 00:04:24,620
So, there is, as we saw before,

83
00:04:24,620 --> 00:04:30,230
there are some predictive power in loan duration.

84
00:04:30,230 --> 00:04:32,215
Loan amount,

85
00:04:32,215 --> 00:04:36,690
and the peaks are almost the same, but this peak is,

86
00:04:36,690 --> 00:04:39,300
keep in mind these are the kernel density estimate plots,

87
00:04:39,300 --> 00:04:43,850
so the area under each half curve is the same.

88
00:04:43,850 --> 00:04:47,690
So again, the neck of this violin is longer and

89
00:04:47,690 --> 00:04:52,990
thicker out here and this bulk is less.

90
00:04:52,990 --> 00:04:55,220
So, it does look like loan amount is

91
00:04:55,220 --> 00:04:57,795
somewhat predictive but not overwhelmingly.

92
00:04:57,795 --> 00:05:00,900
This percent income, again,

93
00:05:00,900 --> 00:05:04,350
you've got a bigger bulge at lower percents of income,

94
00:05:04,350 --> 00:05:05,990
not a great one,

95
00:05:05,990 --> 00:05:08,330
but and then when you get to age and we see there is

96
00:05:08,330 --> 00:05:11,670
a peak in the neck thins out,

97
00:05:11,670 --> 00:05:15,090
but it's so maybe younger customers are a little more

98
00:05:15,090 --> 00:05:18,650
likely to be bad credit risks but it looks pretty,

99
00:05:18,650 --> 00:05:22,120
like a pretty dubious relationship to me.

100
00:05:22,120 --> 00:05:27,095
So, this is how we can look at numeric values.

101
00:05:27,095 --> 00:05:30,770
So, let's talk about categorical variables

102
00:05:30,770 --> 00:05:32,595
and how we figure out whether they're

103
00:05:32,595 --> 00:05:35,120
predictive and we are go to

104
00:05:35,120 --> 00:05:37,985
plot for categorical variables as a bar plot,

105
00:05:37,985 --> 00:05:40,165
so we use geom_bar,

106
00:05:40,165 --> 00:05:42,530
but we have to arrange

107
00:05:42,530 --> 00:05:46,610
this plot in a certain way so that we can quickly

108
00:05:46,610 --> 00:05:50,060
compare differences in counts

109
00:05:50,060 --> 00:05:53,530
of categories between good and bad credit customer.

110
00:05:53,530 --> 00:05:54,965
So, what I've done is I've created

111
00:05:54,965 --> 00:05:57,260
two temporary data frames,

112
00:05:57,260 --> 00:05:58,490
which I'm calling temp zero

113
00:05:58,490 --> 00:06:01,850
which our good credit customers and temp one which is

114
00:06:01,850 --> 00:06:04,880
our bad credit customers just based on whether they're

115
00:06:04,880 --> 00:06:09,105
bad credit variable is zero or one,

116
00:06:09,105 --> 00:06:12,510
and then I again loop over

117
00:06:12,610 --> 00:06:19,200
the categorical columns and I

118
00:06:19,200 --> 00:06:22,325
create one ggplot object for

119
00:06:22,325 --> 00:06:25,860
good credit customers and one for bad credit customers.

120
00:06:25,860 --> 00:06:28,310
The only difference being which of

121
00:06:28,310 --> 00:06:31,880
these subsample data frames I use.

122
00:06:31,880 --> 00:06:35,940
And notice I subsample once outside my loops,

123
00:06:35,940 --> 00:06:39,905
I'm not over subsampling over and over again.

124
00:06:39,905 --> 00:06:42,520
And then I'm using from

125
00:06:42,520 --> 00:06:46,360
the grid extra package this function called

126
00:06:46,360 --> 00:06:48,550
grid.arrange and I can put

127
00:06:48,550 --> 00:06:52,340
p1 and p2 and I'm going to say nrow equals one.

128
00:06:52,340 --> 00:06:53,795
So, I'm going to put them in the same row

129
00:06:53,795 --> 00:06:55,350
so they're going to be side by side.

130
00:06:55,350 --> 00:06:58,200
If I'd said ncall equals

131
00:06:58,200 --> 00:06:59,830
one they would have been one above the other,

132
00:06:59,830 --> 00:07:01,100
but I want them side by side.

133
00:07:01,100 --> 00:07:05,090
So, let me run this and we'll talk about the results.

134
00:07:05,550 --> 00:07:09,240
Okay. So, let's start with

135
00:07:09,240 --> 00:07:12,200
our first feature which is checking account status.

136
00:07:12,200 --> 00:07:15,450
It has four possible categories and we

137
00:07:15,450 --> 00:07:19,235
see for our bad credit customers, not too surprisingly,

138
00:07:19,235 --> 00:07:24,435
their most common, notice the counts are quite different

139
00:07:24,435 --> 00:07:26,500
but because I did these as

140
00:07:26,500 --> 00:07:30,150
independent plots you can look at the proportions.

141
00:07:30,150 --> 00:07:32,180
What we really care about is the proportions.

142
00:07:32,180 --> 00:07:36,520
The fact that there's 700 good credit customers

143
00:07:36,520 --> 00:07:39,820
and only 300 bad credit customers should

144
00:07:39,820 --> 00:07:42,520
not affect our perception of

145
00:07:42,520 --> 00:07:45,160
which categories are more likely to be

146
00:07:45,160 --> 00:07:46,870
indicative of good credit customers

147
00:07:46,870 --> 00:07:48,410
versus bad credit customers.

148
00:07:48,410 --> 00:07:51,850
So, we see overdrawn checking accounts,

149
00:07:51,850 --> 00:07:55,150
that is they have less than zero balance seemed to

150
00:07:55,150 --> 00:07:57,730
be the most common situation

151
00:07:57,730 --> 00:07:58,960
for our bad credit customers.

152
00:07:58,960 --> 00:08:00,565
Well, that's not too surprising.

153
00:08:00,565 --> 00:08:04,480
What was a little surprising I would say is that having

154
00:08:04,480 --> 00:08:06,400
no checking account is actually

155
00:08:06,400 --> 00:08:09,830
the most common case for our good credit customers.

156
00:08:09,830 --> 00:08:13,260
And then we go on and we look at, say,

157
00:08:13,260 --> 00:08:15,960
credit history and critical

158
00:08:15,960 --> 00:08:20,120
account for other loans

159
00:08:20,120 --> 00:08:22,730
actually is more common for good credit customers.

160
00:08:22,730 --> 00:08:27,040
Again, a bit surprising in the most common is

161
00:08:27,040 --> 00:08:29,080
that their current loans are all paid for

162
00:08:29,080 --> 00:08:31,830
both our categories of customers.

163
00:08:31,830 --> 00:08:33,790
Purpose of loans, you can see

164
00:08:33,790 --> 00:08:36,145
some difference and some similarities.

165
00:08:36,145 --> 00:08:38,590
So again, it's not clear how predictive this will be,

166
00:08:38,590 --> 00:08:41,890
but a new car is the second most

167
00:08:41,890 --> 00:08:45,690
common for good credit and televisions and radios,

168
00:08:45,690 --> 00:08:48,210
I guess entertainment devices,

169
00:08:48,210 --> 00:08:53,115
are the most common and new cars

170
00:08:53,115 --> 00:08:56,270
are the most common for our bad credit customers

171
00:08:56,270 --> 00:08:59,200
followed by television and radio.

172
00:08:59,200 --> 00:09:03,899
And in both cases the second most is furniture,

173
00:09:04,840 --> 00:09:11,780
and then we start to see a slight differences or in

174
00:09:11,780 --> 00:09:14,780
a case like this like good credit customers

175
00:09:14,780 --> 00:09:17,990
tend to have longer employment,

176
00:09:17,990 --> 00:09:21,160
bad credit customers are

177
00:09:21,160 --> 00:09:25,310
shorter term employment and then you get cases like this,

178
00:09:25,310 --> 00:09:29,240
gender status, it absolutely doesn't appear to matter.

179
00:09:29,730 --> 00:09:33,179
So, in this demo we've gone through

180
00:09:33,179 --> 00:09:37,830
two methods for looking at how numeric variables

181
00:09:37,830 --> 00:09:41,250
affect or might be useful for

182
00:09:41,250 --> 00:09:45,000
predicting the categories of a label for

183
00:09:45,000 --> 00:09:48,690
a classifier problem and using side by

184
00:09:48,690 --> 00:09:52,890
side bar charts to get a feel for which feature,

185
00:09:52,890 --> 00:09:55,290
which categorical features might be useful for

186
00:09:55,290 --> 00:09:59,440
predicting the label of a classifier problem.

