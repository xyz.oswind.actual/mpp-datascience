0
00:00:01,610 --> 00:00:05,430
>> Hello. So, we've been working with

1
00:00:05,430 --> 00:00:08,655
plots that are essentially 2D plots.

2
00:00:08,655 --> 00:00:11,635
We've had one or two axes,

3
00:00:11,635 --> 00:00:13,590
and they are projected onto

4
00:00:13,590 --> 00:00:16,545
our 2D computer screens, that makes sense.

5
00:00:16,545 --> 00:00:19,425
We do 2D plots on 2D computer screens,

6
00:00:19,425 --> 00:00:21,340
but with machine learning,

7
00:00:21,340 --> 00:00:24,605
we work with complex, high-dimensional data set.

8
00:00:24,605 --> 00:00:27,855
So, we need to think about ways we can project

9
00:00:27,855 --> 00:00:31,670
more dimensions onto our 2D screens.

10
00:00:31,670 --> 00:00:38,625
So, one way to do that is what we call plot aesthetics.

11
00:00:38,625 --> 00:00:41,505
I'm going to go through three of

12
00:00:41,505 --> 00:00:44,670
many possible plot aesthetics in this demo,

13
00:00:44,670 --> 00:00:47,550
and I'm going to go in order of effectiveness,

14
00:00:47,550 --> 00:00:50,130
effectiveness by how well

15
00:00:50,130 --> 00:00:52,725
humans are able to perceive those.

16
00:00:52,725 --> 00:00:56,200
So, we're going to start with marker shape,

17
00:00:56,200 --> 00:01:01,140
and that works well for categorical variables,

18
00:01:01,330 --> 00:01:06,335
marker size, which works well for numeric variables

19
00:01:06,335 --> 00:01:07,850
or can also be used for

20
00:01:07,850 --> 00:01:11,040
categorical variables that have a natural order,

21
00:01:11,040 --> 00:01:13,880
and then color, which again

22
00:01:13,880 --> 00:01:16,585
is generally used for categorical variables.

23
00:01:16,585 --> 00:01:19,080
So, let's get started.

24
00:01:19,090 --> 00:01:24,850
So first off, we're going to work with marker shape.

25
00:01:24,950 --> 00:01:28,025
So the code here,

26
00:01:28,025 --> 00:01:36,245
I have this plot_scatter_sp for shape,

27
00:01:36,245 --> 00:01:38,970
and it looks very much

28
00:01:38,970 --> 00:01:41,860
like the code you've been working with.

29
00:01:41,860 --> 00:01:44,970
Data frame here, a list of

30
00:01:44,970 --> 00:01:48,900
columns that we're going to iterate over,

31
00:01:48,900 --> 00:01:52,480
a Y column which is price, that's our label.

32
00:01:52,480 --> 00:01:55,500
So, we're continuing to focus on our label.

33
00:01:55,500 --> 00:01:59,770
Transparency argument,

34
00:02:00,260 --> 00:02:05,055
and then this all looks familiar, right?

35
00:02:05,055 --> 00:02:07,979
Ggplot, data frame,

36
00:02:07,979 --> 00:02:11,785
aes_string with the column and the Y column.

37
00:02:11,785 --> 00:02:13,665
But then in geom_point,

38
00:02:13,665 --> 00:02:16,909
I'm adding another aes call,

39
00:02:16,909 --> 00:02:22,205
and it's the shape equals factor fuel.type,

40
00:02:22,205 --> 00:02:23,560
and of course Alpha,

41
00:02:23,560 --> 00:02:26,160
Alpha being our transparency control.

42
00:02:26,160 --> 00:02:32,365
So, fuel.type is a string categorical variable.

43
00:02:32,365 --> 00:02:34,400
Recall, there's two fuel types,

44
00:02:34,400 --> 00:02:35,980
there's diesel and gas.

45
00:02:35,980 --> 00:02:37,980
But I have to wrap it in

46
00:02:37,980 --> 00:02:41,070
this factor function so that it

47
00:02:41,070 --> 00:02:45,040
becomes a categorical variable that ggplot understands.

48
00:02:45,040 --> 00:02:48,660
So, if you get some strange error that's about that,

49
00:02:48,660 --> 00:02:50,670
that might be the problem.

50
00:02:50,670 --> 00:02:53,400
So, just keep that in mind when you're working with this,

51
00:02:53,400 --> 00:02:54,960
and of course, my title.

52
00:02:54,960 --> 00:02:56,565
So, let me just run this,

53
00:02:56,565 --> 00:02:59,590
and we'll see what happens.

54
00:03:01,940 --> 00:03:07,065
So, the shapes are two distinct shapes.

55
00:03:07,065 --> 00:03:10,935
Diesel is shown in circles,

56
00:03:10,935 --> 00:03:15,150
and gas in these triangles, and so,

57
00:03:15,150 --> 00:03:19,995
it's quite evident where you have these.

58
00:03:19,995 --> 00:03:23,685
This is curb.weight versus price,

59
00:03:23,685 --> 00:03:30,540
and so you can see our heavy, high-priced gasoline cars,

60
00:03:30,540 --> 00:03:33,570
some midway expensive gasoline cars

61
00:03:33,570 --> 00:03:34,915
that we still don't understand,

62
00:03:34,915 --> 00:03:38,850
and you see that a few of these cars are actually part of

63
00:03:38,850 --> 00:03:41,820
this group of diesel cars and

64
00:03:41,820 --> 00:03:44,800
there's some diesel cars that get down in here.

65
00:03:44,800 --> 00:03:48,510
Most of these low-priced cars

66
00:03:48,510 --> 00:03:52,395
turn out to be gasoline cars.

67
00:03:52,395 --> 00:03:55,560
So, we've already learned some new structure by

68
00:03:55,560 --> 00:03:57,180
projecting another dimension

69
00:03:57,180 --> 00:03:59,505
onto our two-dimensional plot.

70
00:03:59,505 --> 00:04:02,925
If we look at engine.size, again,

71
00:04:02,925 --> 00:04:05,760
our big engine cars

72
00:04:05,760 --> 00:04:09,145
interestingly enough are all gas cars,

73
00:04:09,145 --> 00:04:11,590
there's a few diesels in here.

74
00:04:11,590 --> 00:04:13,200
But most of the diesels,

75
00:04:13,200 --> 00:04:14,790
they're very hard to see even

76
00:04:14,790 --> 00:04:17,355
with this transparency applied,

77
00:04:17,355 --> 00:04:19,500
there's a lot of overplotting here.

78
00:04:19,500 --> 00:04:21,630
So, it's a little hard to tell,

79
00:04:21,630 --> 00:04:26,060
but they're hidden here and there, down in there.

80
00:04:26,690 --> 00:04:30,320
Similarly, for horsepower, in here is

81
00:04:30,320 --> 00:04:34,960
our diesel cars forming a group in the mid-range

82
00:04:34,960 --> 00:04:37,600
of horsepower and price with

83
00:04:37,600 --> 00:04:40,705
our very expensive high horsepower cars

84
00:04:40,705 --> 00:04:43,355
all being gasoline.

85
00:04:43,355 --> 00:04:46,380
Now, let's go to the city miles per gallon.

86
00:04:46,380 --> 00:04:48,820
Remember, this was a plot that was a little harder to

87
00:04:48,820 --> 00:04:51,880
interpret because we had essentially four groups.

88
00:04:51,880 --> 00:04:55,580
So, it looks like we have low fuel efficiency,

89
00:04:55,580 --> 00:04:59,650
high-priced presumably luxury cars that are all gasoline.

90
00:04:59,650 --> 00:05:04,100
The diesel cars are this group here,

91
00:05:04,100 --> 00:05:05,425
you see the circles,

92
00:05:05,425 --> 00:05:06,790
that stand out from

93
00:05:06,790 --> 00:05:10,420
this mostly gasoline car group down here,

94
00:05:10,420 --> 00:05:13,675
and then we have these ultra high efficiency cars,

95
00:05:13,675 --> 00:05:15,520
two gas and one diesel.

96
00:05:15,520 --> 00:05:17,540
So, that helps us just by adding

97
00:05:17,540 --> 00:05:20,200
this aesthetic, this shape,

98
00:05:20,200 --> 00:05:22,300
we start to really be

99
00:05:22,300 --> 00:05:24,370
able to separate what's going on with

100
00:05:24,370 --> 00:05:28,740
these actually four distinct groups of cars,

101
00:05:28,740 --> 00:05:32,695
and that has perhaps tremendous implications

102
00:05:32,695 --> 00:05:35,665
for modeling the price of cars.

103
00:05:35,665 --> 00:05:42,220
So, let's go to the next easiest to perceive aesthetic,

104
00:05:42,220 --> 00:05:45,735
which is marker size. All right.

105
00:05:45,735 --> 00:05:49,340
So, everything's pretty much the same here

106
00:05:49,340 --> 00:05:54,315
except now I've added in my geom_point,

107
00:05:54,315 --> 00:05:57,230
I still have aes and I'm going to keep shape,

108
00:05:57,230 --> 00:06:00,155
shape is a factor by fuel.type.

109
00:06:00,155 --> 00:06:04,890
But size is curb.weight.2.

110
00:06:04,890 --> 00:06:06,960
So I've created a feature,

111
00:06:06,960 --> 00:06:08,840
which has curb.weight squared.

112
00:06:08,840 --> 00:06:12,240
The reason I'm using the square of curb.weight is I

113
00:06:12,240 --> 00:06:16,130
want the horizontal and vertical extent

114
00:06:16,130 --> 00:06:19,125
of my marker to be

115
00:06:19,125 --> 00:06:21,775
proportional to the curb.weight

116
00:06:21,775 --> 00:06:24,580
not the area of the marker.

117
00:06:25,850 --> 00:06:28,320
So, the curb.weight squared or

118
00:06:28,320 --> 00:06:31,070
the area squared is what I want,

119
00:06:31,070 --> 00:06:32,620
and of course Alpha.

120
00:06:32,620 --> 00:06:33,910
So everything else here is pretty

121
00:06:33,910 --> 00:06:36,650
much as we've done before,

122
00:06:39,320 --> 00:06:42,345
and let's look at this first plot

123
00:06:42,345 --> 00:06:45,180
of curb.weight versus price.

124
00:06:45,180 --> 00:06:49,940
All right. Well, sure enough, these heavy cars,

125
00:06:49,940 --> 00:06:51,955
you see the markers get much

126
00:06:51,955 --> 00:06:55,870
bigger generally as we go to heavier and heavier,

127
00:06:55,870 --> 00:06:58,030
and also get bigger as we go to

128
00:06:58,030 --> 00:07:00,790
more expensive cars except

129
00:07:00,790 --> 00:07:03,185
for this kind of enigmatic group here

130
00:07:03,185 --> 00:07:11,780
that seemed to have mid-range curb.weight.

131
00:07:11,780 --> 00:07:16,650
So, but that's not

132
00:07:16,650 --> 00:07:18,174
so interesting because it's curb.weight

133
00:07:18,174 --> 00:07:19,310
versus curb.weight.

134
00:07:19,310 --> 00:07:22,225
So, let's look at engine.size.

135
00:07:22,225 --> 00:07:24,360
Well, again, not too surprisingly,

136
00:07:24,360 --> 00:07:28,980
we've got small markers for small engine.sizes,

137
00:07:28,980 --> 00:07:31,175
which means low curb.weight,

138
00:07:31,175 --> 00:07:33,875
and the markers get bigger

139
00:07:33,875 --> 00:07:38,710
generally as we go up in engine.size and up in price.

140
00:07:38,710 --> 00:07:41,120
Again, that's not too surprising,

141
00:07:41,120 --> 00:07:44,690
but it helps us separate out that there is

142
00:07:44,690 --> 00:07:47,040
a weight and engine.size

143
00:07:47,040 --> 00:07:49,510
relationship that's pretty strong here.

144
00:07:49,510 --> 00:07:53,625
But we have a few exceptions like these diesel,

145
00:07:53,625 --> 00:07:55,400
the diesel seemed to be an exception

146
00:07:55,400 --> 00:08:00,635
generally and horse power.

147
00:08:00,635 --> 00:08:04,840
So heavier, bigger cars mostly have bigger engines,

148
00:08:04,840 --> 00:08:07,410
but you see there's some cars with

149
00:08:07,410 --> 00:08:10,410
high horsepower that aren't necessarily so heavy.

150
00:08:10,410 --> 00:08:13,740
So, maybe those are high-performance cars.

151
00:08:13,740 --> 00:08:17,640
Likewise, we've got some fairly heavy diesel cars

152
00:08:17,640 --> 00:08:21,000
here that don't seem to have a lot of horsepower,

153
00:08:21,000 --> 00:08:26,050
so those maybe they're fairly sluggish cars.

154
00:08:26,050 --> 00:08:31,420
Similarly, our high-priced, low fuel efficiencies cars,

155
00:08:31,420 --> 00:08:35,565
not too surprisingly all seem to have large curb.weight.

156
00:08:35,565 --> 00:08:39,255
Our ultra high efficiency cars here are all pretty light.

157
00:08:39,255 --> 00:08:41,850
In fact, one is the markers

158
00:08:41,850 --> 00:08:43,950
really tiny you can hardly see it,

159
00:08:43,950 --> 00:08:46,245
it's so such a lightweight car.

160
00:08:46,245 --> 00:08:49,440
So, and our diesels and gas cars

161
00:08:49,440 --> 00:08:53,640
seem to get heavier as the fuel economy drops.

162
00:08:55,130 --> 00:08:57,935
Finally, our final aesthetic

163
00:08:57,935 --> 00:08:59,640
we're going to look at is color.

164
00:08:59,640 --> 00:09:02,460
As I mentioned earlier,

165
00:09:03,730 --> 00:09:06,650
small differences in color are

166
00:09:06,650 --> 00:09:08,870
harder for humans to perceive.

167
00:09:08,870 --> 00:09:11,405
So, you need to use color sparingly.

168
00:09:11,405 --> 00:09:13,320
In fact, any of these aesthetics,

169
00:09:13,320 --> 00:09:14,375
you need to use sparingly.

170
00:09:14,375 --> 00:09:16,675
Don't use huge numbers of shapes.

171
00:09:16,675 --> 00:09:19,160
If you have a categorical variable

172
00:09:19,160 --> 00:09:21,425
with 30 different categories,

173
00:09:21,425 --> 00:09:24,470
and you try to use shape to represent it,

174
00:09:24,470 --> 00:09:27,500
you're just going to wind up with a confusing mess.

175
00:09:27,500 --> 00:09:29,555
Even more so with color,

176
00:09:29,555 --> 00:09:33,035
and libraries like ggplot

177
00:09:33,035 --> 00:09:35,884
too try to pick distinct colors,

178
00:09:35,884 --> 00:09:37,490
but yet there are

179
00:09:37,490 --> 00:09:41,395
only so many distinct colors that the human can perceive.

180
00:09:41,395 --> 00:09:44,735
In this case, so we've already got

181
00:09:44,735 --> 00:09:49,700
fuel type as a factor for shape,

182
00:09:49,700 --> 00:09:53,960
we've got size for curb.weight squared,

183
00:09:53,960 --> 00:09:57,560
we've got now color for aspiration.

184
00:09:57,560 --> 00:10:00,280
So that will be standard or turbo.

185
00:10:01,520 --> 00:10:07,425
Okay. So, starting with curb.weight,

186
00:10:07,425 --> 00:10:13,575
you can see these are all standard.

187
00:10:13,575 --> 00:10:16,385
You see standard is in red,

188
00:10:16,385 --> 00:10:20,670
turbo is in kind of green, greenish blue.

189
00:10:20,670 --> 00:10:25,795
So, but these diesel cars are standards,

190
00:10:25,795 --> 00:10:28,540
and you see where the turbo diesels

191
00:10:28,540 --> 00:10:30,175
are all down in this area.

192
00:10:30,175 --> 00:10:31,895
So that's interesting.

193
00:10:31,895 --> 00:10:33,490
In this view, again,

194
00:10:33,490 --> 00:10:36,490
these are all standard gasoline cars

195
00:10:36,490 --> 00:10:40,175
in this group that seemed outlie the trend,

196
00:10:40,175 --> 00:10:43,130
and then here's our standard diesels

197
00:10:43,130 --> 00:10:44,520
and then our turbo diesels.

198
00:10:44,520 --> 00:10:47,255
But there's still a few standard diesels down here

199
00:10:47,255 --> 00:10:58,010
and mostly standard gasoline,

200
00:10:58,010 --> 00:11:02,300
but a few turbo gasoline up in here.

201
00:11:02,300 --> 00:11:07,490
Let me just go to this one,

202
00:11:07,490 --> 00:11:09,810
which is probably our most confused plot,

203
00:11:09,810 --> 00:11:11,940
city miles per gallon versus price.

204
00:11:11,940 --> 00:11:15,920
So, we have this group of presumably luxury cars,

205
00:11:15,920 --> 00:11:19,770
which we've decided are low fuel efficiency.

206
00:11:19,770 --> 00:11:22,395
They're obviously heavy cars,

207
00:11:22,395 --> 00:11:24,740
they're expensive cars, and they're

208
00:11:24,740 --> 00:11:28,160
all standard gas cars.

209
00:11:28,160 --> 00:11:35,360
We have some turbo diesel cars that form this category,

210
00:11:35,360 --> 00:11:38,360
and we've got these three, our turbo diesels,

211
00:11:38,360 --> 00:11:40,240
and some other turbo diesels,

212
00:11:40,240 --> 00:11:42,660
and then we've got some standard diesels,

213
00:11:42,660 --> 00:11:46,240
and a few turbo gas cars that tend to

214
00:11:46,240 --> 00:11:48,070
be on the lower end of

215
00:11:48,070 --> 00:11:51,055
fuel efficiency than the standard cars.

216
00:11:51,055 --> 00:11:53,200
So, now we've got

217
00:11:53,200 --> 00:11:56,340
five dimensions on our two-dimensional plot service.

218
00:11:56,340 --> 00:11:58,700
Here, we have price and city miles per gallon,

219
00:11:58,700 --> 00:12:00,910
we have aspiration, we have fuel.type,

220
00:12:00,910 --> 00:12:02,220
and we have curb.weight.

221
00:12:02,220 --> 00:12:07,754
So, just by using three fairly different aesthetics,

222
00:12:07,754 --> 00:12:09,850
we've extended from two dimensions

223
00:12:09,850 --> 00:12:12,010
to five dimensions so that gives us a lot

224
00:12:12,010 --> 00:12:14,020
more power to understand

225
00:12:14,020 --> 00:12:17,920
complex relationships in our data set.

