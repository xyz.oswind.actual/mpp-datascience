0
00:00:00,000 --> 00:00:03,985
>> So, a useful technique

1
00:00:03,985 --> 00:00:06,320
in understanding categorical variables

2
00:00:06,320 --> 00:00:08,580
is to create frequency tables,

3
00:00:08,580 --> 00:00:12,105
and you can do one and two-level frequency table.

4
00:00:12,105 --> 00:00:15,430
So, a frequency table basically is

5
00:00:15,430 --> 00:00:17,200
just the frequency of each of

6
00:00:17,200 --> 00:00:19,380
those categories laid out in a table.

7
00:00:19,380 --> 00:00:21,190
It's very simple concept and it's

8
00:00:21,190 --> 00:00:24,620
a useful tool to have in your bag.

9
00:00:24,630 --> 00:00:28,590
So, on my desktop here,

10
00:00:28,590 --> 00:00:30,775
I've got this code,

11
00:00:30,775 --> 00:00:32,800
and I'm going to loop through

12
00:00:32,800 --> 00:00:37,210
all the columns in our auto price dataset.

13
00:00:37,210 --> 00:00:38,680
If it's a character column,

14
00:00:38,680 --> 00:00:40,580
that is a categorical column,

15
00:00:40,580 --> 00:00:47,180
I'm going to print out a frequency table.

16
00:00:48,200 --> 00:00:51,480
In R, there's a table function

17
00:00:51,480 --> 00:00:53,860
and you can have- I only give it one argument,

18
00:00:53,860 --> 00:00:56,950
in this case, which is the column, one column.

19
00:00:56,950 --> 00:00:59,090
So, we're just going to get a one-way table.

20
00:00:59,090 --> 00:01:01,320
But you can have two and three,

21
00:01:01,320 --> 00:01:03,920
even three in whatever way tables.

22
00:01:03,920 --> 00:01:05,840
Generally, when you get past three-way tables,

23
00:01:05,840 --> 00:01:08,065
they get really harder to interpret.

24
00:01:08,065 --> 00:01:10,660
So, let me just run this for you and we'll look

25
00:01:10,660 --> 00:01:13,420
at what a frequency table is.

26
00:01:13,420 --> 00:01:16,785
Okay. So, the first up is make,

27
00:01:16,785 --> 00:01:19,610
and you can see the categories are

28
00:01:19,610 --> 00:01:22,950
like Alfa-Romero, Audi, BMW, etc.,

29
00:01:22,950 --> 00:01:25,030
and you see we have three Alfa-Romero,

30
00:01:25,030 --> 00:01:28,280
six Audis, eight BMWs, three Chevrolets,

31
00:01:28,280 --> 00:01:29,540
and my eye scans

32
00:01:29,540 --> 00:01:33,480
the frequency table and I see there's one Mercury,

33
00:01:33,480 --> 00:01:35,235
so that's the least.

34
00:01:35,235 --> 00:01:42,520
There are 32 Toyotas here.

35
00:01:42,520 --> 00:01:45,130
So, that's probably the most.

36
00:01:45,130 --> 00:01:47,610
So with this many categories,

37
00:01:47,610 --> 00:01:50,750
a little hard to see, but- okay.

38
00:01:50,820 --> 00:01:53,955
For fuel type, it's a lot easier to interpret.

39
00:01:53,955 --> 00:01:56,835
There's 20 gas, I'm sorry,

40
00:01:56,835 --> 00:01:58,965
20 diesel, 175 gas.

41
00:01:58,965 --> 00:02:01,050
So, in terms of our analysis,

42
00:02:01,050 --> 00:02:05,535
we need to keep in mind that 20 samples of diesel cars

43
00:02:05,535 --> 00:02:08,760
is statistically problematic compared to

44
00:02:08,760 --> 00:02:12,120
175 cases of gas cars,

45
00:02:12,120 --> 00:02:13,290
and we have a similar,

46
00:02:13,290 --> 00:02:18,250
almost as bad an imbalance of standard 159 versus turbo.

47
00:02:18,890 --> 00:02:22,320
Again, convertibles and hard tops,

48
00:02:22,320 --> 00:02:23,700
there are very few convertibles and

49
00:02:23,700 --> 00:02:26,470
hard tops and not that many wagons,

50
00:02:26,470 --> 00:02:31,300
lots of hatchbacks, lots of sedans, and etc.

51
00:02:31,300 --> 00:02:33,720
So, that's a frequency table of

52
00:02:33,720 --> 00:02:35,910
these categorical features and

53
00:02:35,910 --> 00:02:37,770
it gives you a very quick way to get

54
00:02:37,770 --> 00:02:39,810
a quantitative view of

55
00:02:39,810 --> 00:02:44,970
how many members there are of each category.

56
00:02:44,970 --> 00:02:47,700
So, let me show you one other thing

57
00:02:47,700 --> 00:02:50,345
you can do with kit frequency tables.

58
00:02:50,345 --> 00:02:53,230
This is for the German credit data.

59
00:02:53,230 --> 00:02:56,625
We can use the table function on the label.

60
00:02:56,625 --> 00:02:57,690
This is very important.

61
00:02:57,690 --> 00:02:59,980
Before you start building a machine learning model,

62
00:02:59,980 --> 00:03:02,220
you really always want to look at

63
00:03:02,220 --> 00:03:05,090
the categories of the label,

64
00:03:05,090 --> 00:03:06,305
and we see we have

65
00:03:06,305 --> 00:03:12,400
about 700 good credit customers

66
00:03:12,400 --> 00:03:14,010
and only 300 bad customers.

67
00:03:14,010 --> 00:03:18,120
So, there's a significant class imbalance, we call it,

68
00:03:18,120 --> 00:03:21,040
in this dataset and that of course

69
00:03:21,040 --> 00:03:22,690
has huge implications when we go

70
00:03:22,690 --> 00:03:25,360
to train a machine learning model.

71
00:03:25,360 --> 00:03:29,615
So, that's the frequency table technique.

72
00:03:29,615 --> 00:03:30,700
It's, as you see,

73
00:03:30,700 --> 00:03:32,095
very simple to use,

74
00:03:32,095 --> 00:03:36,530
simple to interpret, and therefore, extremely useful.

