0
00:00:01,580 --> 00:00:04,850
>> Hello. In this video,

1
00:00:04,850 --> 00:00:06,020
I'm going to show you

2
00:00:06,020 --> 00:00:08,330
some methods you can use to visualize

3
00:00:08,330 --> 00:00:11,450
distributions of features and

4
00:00:11,450 --> 00:00:14,135
the label for machine learning models.

5
00:00:14,135 --> 00:00:15,620
Why would you want to do this?

6
00:00:15,620 --> 00:00:19,160
Well, it's very important to understand what

7
00:00:19,160 --> 00:00:22,385
the distributional properties of

8
00:00:22,385 --> 00:00:23,820
your features and labels are.

9
00:00:23,820 --> 00:00:25,850
You may want to transform them.

10
00:00:25,850 --> 00:00:29,690
You may find problematic situations

11
00:00:29,690 --> 00:00:31,910
by looking at the distribution.

12
00:00:31,910 --> 00:00:34,130
So, it's really important to drill down

13
00:00:34,130 --> 00:00:37,225
at least a bit on just the distributions.

14
00:00:37,225 --> 00:00:39,805
There's really two types.

15
00:00:39,805 --> 00:00:41,780
So, we have categorical variables,

16
00:00:41,780 --> 00:00:43,835
and we're going to use bar charts for that,

17
00:00:43,835 --> 00:00:46,760
and numeric variables, and I'll show you three different

18
00:00:46,760 --> 00:00:52,240
ways we can visualize numeric variables.

19
00:00:53,090 --> 00:00:55,725
So, on my screen here,

20
00:00:55,725 --> 00:00:59,910
I have our first bit of code

21
00:00:59,910 --> 00:01:01,920
and we're going to be working with

22
00:01:01,920 --> 00:01:05,330
this auto prices data set.

23
00:01:05,660 --> 00:01:10,110
I have created this function called

24
00:01:10,110 --> 00:01:16,380
plot_bars and given the data frame and I set a plot area

25
00:01:16,380 --> 00:01:20,550
using this from the repr package which lets

26
00:01:20,550 --> 00:01:23,790
me scale the area that I

27
00:01:23,790 --> 00:01:27,110
want to put the plot on in my notebook.

28
00:01:27,110 --> 00:01:29,940
I'm going to iterate

29
00:01:29,940 --> 00:01:32,580
through all the columns of this data frame.

30
00:01:32,580 --> 00:01:34,350
But what I'm going to say is if

31
00:01:34,350 --> 00:01:37,270
it's a character type, so I do,

32
00:01:37,270 --> 00:01:40,720
is.character of that column, then,

33
00:01:40,720 --> 00:01:41,955
I'm going to go ahead and make a plot

34
00:01:41,955 --> 00:01:43,080
because I only want to do this

35
00:01:43,080 --> 00:01:46,110
for character data types.

36
00:01:46,110 --> 00:01:48,855
This isn't going to work for numeric.

37
00:01:48,855 --> 00:01:51,150
So, here's our basic recipe.

38
00:01:51,150 --> 00:01:52,890
It's always ggplot,

39
00:01:52,890 --> 00:01:56,910
the data frame and aes or in this case,

40
00:01:56,910 --> 00:02:02,190
it's aes_string because this

41
00:02:02,190 --> 00:02:04,620
is being this call here will be

42
00:02:04,620 --> 00:02:09,100
a string because it's coming from the list of names.

43
00:02:10,010 --> 00:02:13,185
So, for that column,

44
00:02:13,185 --> 00:02:19,645
then we do geom_bar and I'm giving it an alpha.

45
00:02:19,645 --> 00:02:22,850
Alpha is a transparency measure.

46
00:02:22,850 --> 00:02:26,390
So, just a little bit color isn't so glaring.

47
00:02:26,390 --> 00:02:28,650
I'm doing some- one other trick here,

48
00:02:28,650 --> 00:02:33,600
I'm using a theme where I angle and adjust the text.

49
00:02:33,600 --> 00:02:35,370
I'm doing that so that the text

50
00:02:35,370 --> 00:02:37,165
doesn't lie on top of each other.

51
00:02:37,165 --> 00:02:41,225
Now, because I'm using ggplot within a function,

52
00:02:41,225 --> 00:02:44,325
I save it as a plot object P

53
00:02:44,325 --> 00:02:46,350
and then I have to print that object.

54
00:02:46,350 --> 00:02:49,230
So, if you don't get output and you're

55
00:02:49,230 --> 00:02:52,570
using ggplot in a function,

56
00:02:52,570 --> 00:02:57,070
that's probably why you probably didn't print it.

57
00:02:58,220 --> 00:03:01,050
Okay. So, run it.

58
00:03:01,050 --> 00:03:02,210
Let's look at this first one.

59
00:03:02,210 --> 00:03:03,665
This first one is pretty complicated.

60
00:03:03,665 --> 00:03:05,810
This is auto make.

61
00:03:05,810 --> 00:03:09,740
You notice I gave it- ggplot gives

62
00:03:09,740 --> 00:03:14,840
it some reasonable axis labels initially.

63
00:03:14,840 --> 00:03:20,885
You can see how I turn the text by 90 degrees.

64
00:03:20,885 --> 00:03:23,210
This would be a mess with all these labels

65
00:03:23,210 --> 00:03:25,865
lying on top of each other if I hadn't done that.

66
00:03:25,865 --> 00:03:28,910
You can see Toyotas are the most frequent,

67
00:03:28,910 --> 00:03:33,140
Nissans probably the least most frequent.

68
00:03:33,140 --> 00:03:36,970
It looks like Mercury is the least frequent.

69
00:03:36,970 --> 00:03:38,690
Then, there's some like Isuzu,

70
00:03:38,690 --> 00:03:41,980
maybe the next to the least frequent, etc.

71
00:03:41,980 --> 00:03:44,970
You can see for some things, it's easier.

72
00:03:44,970 --> 00:03:46,380
It just get two bars,

73
00:03:46,380 --> 00:03:47,550
like here, fuel type.

74
00:03:47,550 --> 00:03:49,865
There's only diesel cars and gas cars.

75
00:03:49,865 --> 00:03:51,710
Right away, you know something

76
00:03:51,710 --> 00:03:54,320
important about the distribution of these cars.

77
00:03:54,320 --> 00:03:56,330
You know that most of them

78
00:03:56,330 --> 00:03:58,250
are gas and only a few are diesel and that

79
00:03:58,250 --> 00:04:00,125
has implications when you're doing

80
00:04:00,125 --> 00:04:03,345
analysis on this data set of any sort.

81
00:04:03,345 --> 00:04:06,905
Similar, the minority of cars are turbo,

82
00:04:06,905 --> 00:04:11,264
most are standard and etc.

83
00:04:11,264 --> 00:04:12,770
You can see, again,

84
00:04:12,770 --> 00:04:15,020
there's a big imbalance in the number of

85
00:04:15,020 --> 00:04:18,110
cases like very few convertibles or

86
00:04:18,110 --> 00:04:21,200
hard tops but lots are sedans

87
00:04:21,200 --> 00:04:25,410
and somewhere in between with hatchbacks and wagons, etc.

88
00:04:25,410 --> 00:04:27,365
We're not going to go through all these plots.

89
00:04:27,365 --> 00:04:30,650
You should if you were doing machine learning on them,

90
00:04:30,650 --> 00:04:32,870
you would really want to understand what each of

91
00:04:32,870 --> 00:04:36,300
these cases had to tell you and what

92
00:04:36,300 --> 00:04:39,610
problems that might occur from

93
00:04:39,610 --> 00:04:42,010
just the numbers or frequencies or

94
00:04:42,010 --> 00:04:44,720
distributions of these categories.

95
00:04:44,720 --> 00:04:48,855
If you have a categorical like a label for a classifier,

96
00:04:48,855 --> 00:04:50,890
then you really want to understand

97
00:04:50,890 --> 00:04:54,645
the balance or imbalance in your different categories.

98
00:04:54,645 --> 00:04:56,680
So, let's move to

99
00:04:56,680 --> 00:05:00,504
the next case here which is numeric variables,

100
00:05:00,504 --> 00:05:03,175
looking at distributions in numeric variables.

101
00:05:03,175 --> 00:05:06,290
Our first go to method is a histogram.

102
00:05:06,290 --> 00:05:08,905
Histogram is one of the oldest methods

103
00:05:08,905 --> 00:05:10,795
in statistical graphics.

104
00:05:10,795 --> 00:05:12,115
It's been around for

105
00:05:12,115 --> 00:05:14,995
about almost a century and a half now.

106
00:05:14,995 --> 00:05:19,280
The code I'm using here is very similar.

107
00:05:19,280 --> 00:05:24,720
I illiterate over all the columns and except here,

108
00:05:24,720 --> 00:05:27,465
I go if it's numeric,

109
00:05:27,465 --> 00:05:28,870
now, I'm going to make a histogram.

110
00:05:28,870 --> 00:05:31,055
So, a histogram superficially

111
00:05:31,055 --> 00:05:33,430
looks like a bar chart except that

112
00:05:33,430 --> 00:05:38,250
it splits the data into bins.

113
00:05:38,250 --> 00:05:42,540
Because they're are continuous numeric values,

114
00:05:42,540 --> 00:05:44,640
you can't plot them as a bar chart,

115
00:05:44,640 --> 00:05:46,770
you can't count discrete values.

116
00:05:46,770 --> 00:05:49,410
You have to bin them and then count.

117
00:05:49,410 --> 00:05:53,970
In ggplot, we set a binwidth which I'm using as

118
00:05:53,970 --> 00:05:55,950
the difference between the max and

119
00:05:55,950 --> 00:05:58,770
the mean divided by the number of bins plus 1.

120
00:05:58,770 --> 00:06:01,080
So, I have set my bins up here.

121
00:06:01,080 --> 00:06:05,910
I give that as an argument to geom_histogram.

122
00:06:05,910 --> 00:06:07,190
I'm setting an Alpha again

123
00:06:07,190 --> 00:06:09,855
just to make the display prettier.

124
00:06:09,855 --> 00:06:13,870
But more importantly, I'm setting a binwidth.

125
00:06:13,870 --> 00:06:16,080
This part is the same,

126
00:06:16,080 --> 00:06:18,270
I use ggplot of

127
00:06:18,270 --> 00:06:21,620
my data frame aes_string

128
00:06:21,620 --> 00:06:24,675
because this is going to be a string variable,

129
00:06:24,675 --> 00:06:27,425
and that's the column name.

130
00:06:27,425 --> 00:06:30,325
Again, I have to print the plot object.

131
00:06:30,325 --> 00:06:35,360
I'm going to iterate over these numeric columns.

132
00:06:39,450 --> 00:06:43,160
You see like curb weight.

133
00:06:45,690 --> 00:06:48,630
So, what can we tell from this?

134
00:06:48,630 --> 00:06:51,450
Well, first off, it's got a pretty strong right skew.

135
00:06:51,450 --> 00:06:54,840
Most cars are more toward the lighter weight and there's

136
00:06:54,840 --> 00:06:59,505
a few really lightweight cars down here in these bins.

137
00:06:59,505 --> 00:07:02,150
But there's also some really heavy cars,

138
00:07:02,150 --> 00:07:03,260
but they're the minority,

139
00:07:03,260 --> 00:07:04,970
the majority are in here.

140
00:07:04,970 --> 00:07:06,630
So, just knowing you have

141
00:07:06,630 --> 00:07:08,880
skew in a distribution like this tells you

142
00:07:08,880 --> 00:07:10,700
something and it tells you that

143
00:07:10,700 --> 00:07:12,990
maybe some transforms required.

144
00:07:12,990 --> 00:07:16,170
Similar with engine size or even more so,

145
00:07:16,170 --> 00:07:18,180
you can see the skew is really extreme

146
00:07:18,180 --> 00:07:20,430
here because there's these, I think,

147
00:07:20,430 --> 00:07:24,690
three cars with really huge engines and a few cars with

148
00:07:24,690 --> 00:07:26,880
really small engines and most cars

149
00:07:26,880 --> 00:07:30,000
have engines where the mode is around 100,

150
00:07:30,000 --> 00:07:33,205
so it's actually really toward the low end.

151
00:07:33,205 --> 00:07:35,760
Again, horsepower, a little more

152
00:07:35,760 --> 00:07:38,940
symmetric, etc., and price.

153
00:07:38,940 --> 00:07:40,200
So, price is our label.

154
00:07:40,200 --> 00:07:42,160
That's the thing we're trying to predict.

155
00:07:42,160 --> 00:07:45,240
There is extreme right skew, and in fact,

156
00:07:45,240 --> 00:07:47,160
there's almost-you can see

157
00:07:47,160 --> 00:07:51,985
there's a bulk of cars have fairly low prices.

158
00:07:51,985 --> 00:07:57,720
There's almost another mode here in the mid-30,000s,

159
00:07:57,720 --> 00:08:02,085
which must be some luxury cars.

160
00:08:02,085 --> 00:08:04,580
So, that's an important thing.

161
00:08:04,580 --> 00:08:05,950
First off, you've got this,

162
00:08:05,950 --> 00:08:08,200
a cemetery or the extreme right skew

163
00:08:08,200 --> 00:08:09,930
and then possibly two modes.

164
00:08:09,930 --> 00:08:13,270
So, those are really important observations

165
00:08:13,270 --> 00:08:15,465
when you're trying to build a machine learning model.

166
00:08:15,465 --> 00:08:17,260
So, what are other ways we

167
00:08:17,260 --> 00:08:21,315
can visualize the distribution of numeric variables?

168
00:08:21,315 --> 00:08:28,330
Well, another great one is using a density,

169
00:08:28,330 --> 00:08:30,380
kernel density estimate plot.

170
00:08:30,380 --> 00:08:33,480
It's basically a local regression that's run over

171
00:08:33,480 --> 00:08:37,610
the bins just like they were a histogram.

172
00:08:37,730 --> 00:08:42,435
So, you can see geom_density here.

173
00:08:42,435 --> 00:08:45,805
I'm going to make my density plot blue.

174
00:08:45,805 --> 00:08:48,960
Just the rest of this is all

175
00:08:48,960 --> 00:08:52,410
very similar to what we just looked at.

176
00:08:52,410 --> 00:08:54,220
I've added one other thing here,

177
00:08:54,220 --> 00:08:59,290
geom_rug, and I'll show you what that is in a minute.

178
00:09:02,510 --> 00:09:08,470
So, here for curb weight

179
00:09:08,470 --> 00:09:11,020
is the kernel density estimate plot.

180
00:09:11,020 --> 00:09:13,795
You see, it looks like a smooth version

181
00:09:13,795 --> 00:09:16,360
of that histogram we saw,

182
00:09:16,360 --> 00:09:17,620
and that's the whole point.

183
00:09:17,620 --> 00:09:22,010
It gives you a very different view, the smooth view.

184
00:09:22,010 --> 00:09:25,410
The rug are these little ticks down here.

185
00:09:25,410 --> 00:09:28,930
So, each case that we're using to estimate

186
00:09:28,930 --> 00:09:33,415
the density up here has a tick in the rug.

187
00:09:33,415 --> 00:09:36,595
You can see where the rug is dense,

188
00:09:36,595 --> 00:09:38,830
this curve is high.

189
00:09:38,830 --> 00:09:40,260
Where the rug is sparse,

190
00:09:40,260 --> 00:09:42,470
this curve is low.

191
00:09:42,470 --> 00:09:45,490
You get similar things for the case of

192
00:09:45,490 --> 00:09:50,310
this engine size where it's very skewed and price.

193
00:09:50,310 --> 00:09:52,145
You see, there's these three,

194
00:09:52,145 --> 00:09:53,980
oh, sorry, this price.

195
00:09:53,980 --> 00:09:57,530
Here, you see there's these few for very expensive cars.

196
00:09:57,530 --> 00:10:04,120
There is a little clump of mid to low $30,000 cars.

197
00:10:04,120 --> 00:10:07,035
Presumably, these are all luxury cars out here.

198
00:10:07,035 --> 00:10:11,630
Then, there's really dense area here below $10,000,

199
00:10:11,630 --> 00:10:12,990
and you can't even tell

200
00:10:12,990 --> 00:10:14,920
really how many ticks there are there.

201
00:10:14,920 --> 00:10:16,540
There's obviously,

202
00:10:16,540 --> 00:10:21,250
it's an attractive price point for consumers apparently.

203
00:10:21,250 --> 00:10:24,270
So, there's one other trick we can do.

204
00:10:24,270 --> 00:10:26,130
We can combine the histograms

205
00:10:26,130 --> 00:10:28,605
and the kernel density estimate plots.

206
00:10:28,605 --> 00:10:32,400
But there's something you have to do to make this look

207
00:10:32,400 --> 00:10:36,480
right because you can't compare counts to density.

208
00:10:36,480 --> 00:10:39,060
Notice that the density curve is on

209
00:10:39,060 --> 00:10:42,000
a scale that isn't counts.

210
00:10:42,000 --> 00:10:44,460
It's because the integral

211
00:10:44,460 --> 00:10:46,240
under this curve has to add up to one.

212
00:10:46,240 --> 00:10:50,330
The probabilities of the price

213
00:10:50,330 --> 00:10:54,755
being some value has to integrate over to one.

214
00:10:54,755 --> 00:10:58,710
So, we have to add this in aes,

215
00:10:58,710 --> 00:11:01,020
y equals density.

216
00:11:01,020 --> 00:11:04,350
We have to do that for both the histogram.

217
00:11:04,350 --> 00:11:05,980
We have to add an aes,

218
00:11:05,980 --> 00:11:08,740
y equals  density.

219
00:11:08,740 --> 00:11:11,365
Now, this is something also to point out.

220
00:11:11,365 --> 00:11:15,410
You can use aes or aes_string in different places.

221
00:11:15,410 --> 00:11:18,170
So, here we have aes_string,

222
00:11:18,170 --> 00:11:21,505
just tells us who we're working on whichever column,

223
00:11:21,505 --> 00:11:26,535
but aes is also used for certain plot attributes

224
00:11:26,535 --> 00:11:32,145
like the fact that we want the y-axis to be density.

225
00:11:32,145 --> 00:11:33,500
So, we're using that in

226
00:11:33,500 --> 00:11:36,335
both our geom_histogram and geom_density.

227
00:11:36,335 --> 00:11:38,265
We don't need it for the rug.

228
00:11:38,265 --> 00:11:41,880
You see how in ggplot 2, we're building this up.

229
00:11:41,880 --> 00:11:43,665
That's the third point here.

230
00:11:43,665 --> 00:11:46,985
So, you see with a plus operator.

231
00:11:46,985 --> 00:11:51,680
We have histogram, another plus.

232
00:11:51,680 --> 00:11:55,260
We have density, another plus.

233
00:11:55,260 --> 00:11:57,660
We have the rug. So, that's the gg,

234
00:11:57,660 --> 00:11:59,520
stands for grammar of graphics.

235
00:11:59,520 --> 00:12:02,085
That's the grammar of graphics that we can keep

236
00:12:02,085 --> 00:12:06,590
adding plot layers or plot attributes with the plus sign,

237
00:12:06,590 --> 00:12:08,740
and we could add other labeling.

238
00:12:08,740 --> 00:12:10,830
We could add lots of other- these

239
00:12:10,830 --> 00:12:13,335
can get very complicated as you'll see.

240
00:12:13,335 --> 00:12:14,280
But in this case,

241
00:12:14,280 --> 00:12:17,610
we have actually three different plot geometries

242
00:12:17,610 --> 00:12:20,420
that are all going to be displayed on this one plot.

243
00:12:20,420 --> 00:12:23,080
So, let me run that for you.

244
00:12:24,110 --> 00:12:28,345
Let's just go to the last one which is price.

245
00:12:28,345 --> 00:12:33,290
So, you see the rug which we talked about already.

246
00:12:33,290 --> 00:12:40,110
You see the density plot and the histogram under that.

247
00:12:40,110 --> 00:12:43,290
It's quite clear that- so,

248
00:12:43,290 --> 00:12:46,680
the histogram looks blocky and we could use

249
00:12:46,680 --> 00:12:49,230
more bins and it would look

250
00:12:49,230 --> 00:12:53,485
jagged or maybe less, or even block here.

251
00:12:53,485 --> 00:12:56,340
The density plot gives our eyes

252
00:12:56,340 --> 00:12:59,420
a nice smooth curve to follow.

253
00:12:59,420 --> 00:13:01,860
So, they do give you

254
00:13:01,860 --> 00:13:07,060
different impressions what the distribution is.

255
00:13:07,190 --> 00:13:10,380
So, that's some methods you can use,

256
00:13:10,380 --> 00:13:14,425
using ggplot in R to look at

257
00:13:14,425 --> 00:13:18,215
distributions and distributions of

258
00:13:18,215 --> 00:13:22,225
categorical variables by bar charts.

259
00:13:22,225 --> 00:13:25,045
For numeric variables, we did histograms,

260
00:13:25,045 --> 00:13:27,200
kernel density estimated plots,

261
00:13:27,200 --> 00:13:30,180
and the combination of the two.

