0
00:00:01,500 --> 00:00:05,450
So let's go get those neighborhoods and let's add them to the data.

1
00:00:06,450 --> 00:00:10,070
When we're done, we wanna have one column called

2
00:00:10,070 --> 00:00:13,130
pick up neighborhood and one column called drop off neighborhood.

3
00:00:13,130 --> 00:00:17,711
Where neighborhood here is just what Zillow in it's

4
00:00:17,711 --> 00:00:21,790
shapefile had as a neighbourhood Manhattan.

5
00:00:21,790 --> 00:00:25,330
So once again a common mistake would be for

6
00:00:25,330 --> 00:00:29,650
us to try to do too many things at the same time.

7
00:00:29,650 --> 00:00:34,380
It's a challenge when working with RevoScaleR and

8
00:00:34,380 --> 00:00:38,390
to be able to tell if in case something doesn't work,

9
00:00:38,390 --> 00:00:43,740
if it's something that maybe we did wrong in RevoScaleR.

10
00:00:43,740 --> 00:00:47,850
Or if it has to do with the base R functions that we're

11
00:00:47,850 --> 00:00:49,390
using and so on.

12
00:00:49,390 --> 00:00:52,210
So, let's not jump ahead too much.

13
00:00:52,210 --> 00:00:54,370
Let's start very small.

14
00:00:54,370 --> 00:00:59,520
We're gonna try to use the NYC sample data frame that we have.

15
00:01:01,620 --> 00:01:05,190
And we're gonna try to see if we can add the neighborhoods

16
00:01:05,190 --> 00:01:06,690
to this data frame.

17
00:01:06,690 --> 00:01:10,790
Once we have some R code snippet up and

18
00:01:10,790 --> 00:01:14,930
running that seems to be working, we can then move on to the next step.

19
00:01:14,930 --> 00:01:19,040
Which is to wrap the code inside of a function and to pass that function

20
00:01:19,040 --> 00:01:24,170
to rxDataStep, so that it would run the function on the entire xdf file.

21
00:01:25,710 --> 00:01:30,630
So here we're gonna take the New York City sample data frame and

22
00:01:30,630 --> 00:01:36,600
we're gonna extract the pick up longitude and latitude columns.

23
00:01:36,600 --> 00:01:42,190
One of the things about working with the gis packages

24
00:01:42,190 --> 00:01:47,380
that we're gonna use in specific certain functions in those packages

25
00:01:47,380 --> 00:01:52,230
is that, like any other functions in third party packages,

26
00:01:52,230 --> 00:01:54,180
they might have certain quirks.

27
00:01:54,180 --> 00:01:55,870
In this case, the quirk is that

28
00:01:57,170 --> 00:02:02,270
it does not like having NAs in the longitude and latitude columns.

29
00:02:02,270 --> 00:02:06,680
So we're gonna use a workaround to fix this.

30
00:02:06,680 --> 00:02:09,380
We're basically gonna go in to those columns and

31
00:02:09,380 --> 00:02:11,639
we're gonna replace the NAs with zeros.

32
00:02:13,350 --> 00:02:17,904
In this case we're safe, because our longitude and

33
00:02:17,904 --> 00:02:23,286
latitudes are in a very constrained area around Manhattan and

34
00:02:23,286 --> 00:02:27,280
so a zero is not gonna give us any problem.

35
00:02:27,280 --> 00:02:29,820
It's gonna be as good as NAs but

36
00:02:29,820 --> 00:02:34,260
it's going to work with the gis packages whereas the NAs don't.

37
00:02:35,330 --> 00:02:38,727
So that's our first task as we go to those columns in

38
00:02:38,727 --> 00:02:40,781
the New York City sample data.

39
00:02:40,781 --> 00:02:42,989
We replace NAs with zeroes and

40
00:02:42,989 --> 00:02:47,325
we create just a temporary data frame called data coords.

41
00:02:47,325 --> 00:02:48,841
We can do a head on it.

42
00:02:51,961 --> 00:02:55,124
And it basically just has the longitude and latitude for

43
00:02:55,124 --> 00:02:56,340
pickup in this case.

44
00:02:57,980 --> 00:03:01,600
Next, we're gonna use a function called, coordinates.

45
00:03:01,600 --> 00:03:05,370
And this function is basically a pointer function.

46
00:03:05,370 --> 00:03:10,940
It says, go to this data frame, find the longitude and latitude columns.

47
00:03:10,940 --> 00:03:13,700
Which are the only two columns we have in this data frame,

48
00:03:13,700 --> 00:03:17,680
and think of those as coordinates, right.

49
00:03:17,680 --> 00:03:21,430
Otherwise nothing really happens, there is no calculation going on.

50
00:03:21,430 --> 00:03:25,680
It's just becoming aware of what those columns represent.

51
00:03:25,680 --> 00:03:30,210
And this is gonna be important in the next step when we actually

52
00:03:30,210 --> 00:03:34,050
use the coordinates to perform the complicated calculation.

53
00:03:35,600 --> 00:03:41,600
So the really complex stuff is happening in this one line of code.

54
00:03:41,600 --> 00:03:43,415
We're using a function called,over.

55
00:03:44,450 --> 00:03:46,656
As it's input, over takes in,

56
00:03:46,656 --> 00:03:50,600
the temporary data frame with the coordinates that we

57
00:03:50,600 --> 00:03:53,715
called in the last step, and it uses the shapefile.

58
00:03:56,010 --> 00:04:01,420
And it's gonna go and brush these coordinates

59
00:04:01,420 --> 00:04:06,420
in the data against the shapefile to find which

60
00:04:06,420 --> 00:04:10,390
neighborhood the coordinates fall into.

61
00:04:12,770 --> 00:04:16,800
Once over finishes running, it returns another data frame.

62
00:04:16,800 --> 00:04:19,170
And, we can take a look at the data frame right now.

63
00:04:20,540 --> 00:04:23,640
We store it in an object called nhoods.

64
00:04:23,640 --> 00:04:25,840
And so it says, in this case, for

65
00:04:25,840 --> 00:04:28,920
example that this is the first coordinates.

66
00:04:28,920 --> 00:04:33,130
And over found that that coordinate falls in the state of New York,

67
00:04:33,130 --> 00:04:34,410
in the county of New York.

68
00:04:35,410 --> 00:04:37,190
In the city of New York City,

69
00:04:37,190 --> 00:04:41,220
Manhattan, in the neighbourhood called the Lower East Side.

70
00:04:41,220 --> 00:04:46,160
And we have some ID columns here that are not very important, right.

71
00:04:46,160 --> 00:04:49,570
So, it seems like the piece of information that we care about

72
00:04:49,570 --> 00:04:53,180
the most is in this column called NAME.

73
00:04:53,180 --> 00:04:56,490
And it has the neighborhood that we care about.

74
00:04:56,490 --> 00:04:59,710
So we have some additional information that is just completely

75
00:04:59,710 --> 00:05:00,320
redundant.

76
00:05:00,320 --> 00:05:02,568
The state is just always going to be New York.

77
00:05:02,568 --> 00:05:07,920
The county is for our purposes, going to be always New York.

78
00:05:07,920 --> 00:05:11,030
And the city, since we only care about Manhattan,

79
00:05:11,030 --> 00:05:14,522
is going to be New York City and the borough is Manhattan.

80
00:05:14,522 --> 00:05:20,250
And in this case the borough is just part of the name for the city, okay.

81
00:05:20,250 --> 00:05:23,810
So we're gonna rename the columns, so

82
00:05:23,810 --> 00:05:26,560
that they have a prefix of pick up.

83
00:05:26,560 --> 00:05:30,170
And we're going to go back to the New York City taxi data, and we're

84
00:05:30,170 --> 00:05:35,570
going to attach only the columns containing, in this case, the name.

85
00:05:35,570 --> 00:05:37,225
Which is the neighbourhood name and

86
00:05:37,225 --> 00:05:41,880
the city which also has the borough in it.

87
00:05:41,880 --> 00:05:46,020
So, now if we look at the New York City taxi data, scrolling to

88
00:05:46,020 --> 00:05:50,210
the bottom, we can see that we have a column called pickup_city, and

89
00:05:50,210 --> 00:05:53,100
it has the city and the borough.

90
00:05:53,100 --> 00:05:56,080
And we have a column called pickup_name, which has the name of

91
00:05:56,080 --> 00:05:58,960
the neighbourhood where the pickup happened.

92
00:05:58,960 --> 00:06:03,960
And that information was extracted from the shapefile using

93
00:06:03,960 --> 00:06:06,370
the coordinates for pickup longitude and

94
00:06:06,370 --> 00:06:10,040
latitude to brush to against the shapefile.

95
00:06:10,040 --> 00:06:15,295
So that's the gist of our function, and we haven't relied on any

96
00:06:15,295 --> 00:06:20,360
RevoScaleR tools to accomplish this, right.

97
00:06:20,360 --> 00:06:23,815
Now that we have working code, and

98
00:06:23,815 --> 00:06:28,829
everything seems to be working the way it should.

99
00:06:28,829 --> 00:06:30,679
We can move to the net step,

100
00:06:30,679 --> 00:06:34,303
which is actually implementing this in RevoScaleR.

101
00:06:34,303 --> 00:06:37,990
There is a couple of things that we have to do.

102
00:06:37,990 --> 00:06:40,760
One is that we're gonna do this twice.

103
00:06:40,760 --> 00:06:45,420
We're gonna do it once for pick up, and once for drop-off.

104
00:06:45,420 --> 00:06:49,790
And then we're going to do it slightly differently, but

105
00:06:49,790 --> 00:06:53,170
that's really just a matter of preference.

106
00:06:53,170 --> 00:06:58,120
So, the important thing here is that we're gonna

107
00:06:58,120 --> 00:07:02,520
repeat the same process but we need to be aware that, in this case,

108
00:07:02,520 --> 00:07:05,050
the function is gonna be once again the function of the data.

109
00:07:06,050 --> 00:07:09,160
And so, our first thing is gonna go to the data and

110
00:07:09,160 --> 00:07:14,050
pick up longitude, replace any NAs with zeros.

111
00:07:14,050 --> 00:07:16,470
We're gonna do the same with pick up latitude.

112
00:07:16,470 --> 00:07:20,137
And we're gonna store those in vectors called pickup_longitude and

113
00:07:20,137 --> 00:07:21,630
pickup_latitude.

114
00:07:21,630 --> 00:07:24,880
And then we can put those two vectors in a single data frame

115
00:07:24,880 --> 00:07:28,880
called data_coords, which is going to be the temporary data frame that

116
00:07:28,880 --> 00:07:31,570
will have only the coordinates.

117
00:07:31,570 --> 00:07:35,640
Just as before we use the coordinates function to

118
00:07:35,640 --> 00:07:38,370
delineate those columns as coordinates.

119
00:07:38,370 --> 00:07:44,900
And we use the over function to take the data with the coordinates,

120
00:07:44,900 --> 00:07:47,570
brush it against the shapefile and find the neighborhoods.

121
00:07:49,210 --> 00:07:53,180
We then take from nhood,

122
00:07:53,180 --> 00:07:56,970
the column called name and the column called city.

123
00:07:56,970 --> 00:08:00,680
We place them back into the data and

124
00:08:00,680 --> 00:08:05,430
we name them pickup nhood and pickup_borough in this case.

125
00:08:05,430 --> 00:08:07,740
Because this city will always be New York City, so

126
00:08:07,740 --> 00:08:10,100
the borough is going to be Manhattan.

127
00:08:10,100 --> 00:08:10,990
So that's it,

128
00:08:10,990 --> 00:08:16,290
it's really just a slightly modified version of the above code.

129
00:08:16,290 --> 00:08:20,680
Running once for pickup, running a second time for drop off.

130
00:08:20,680 --> 00:08:24,810
And as you can see we have some intermediate objects

131
00:08:24,810 --> 00:08:28,410
that get created such as data chords and so on.

132
00:08:28,410 --> 00:08:30,650
Pickup longitude, pickup latitude, and

133
00:08:30,650 --> 00:08:34,770
eventually the only thing that we care about is these two columns,

134
00:08:34,770 --> 00:08:38,510
which get placed back into the data and renamed.

135
00:08:38,510 --> 00:08:41,670
So we repeat that for drop off, and then we're done.

136
00:08:41,670 --> 00:08:44,100
So we have a function now, and

137
00:08:44,100 --> 00:08:47,550
it's going to implement the same code for us.

138
00:08:47,550 --> 00:08:51,190
And it's gonna do it for pick up once and for drop off a second time.

139
00:08:52,460 --> 00:08:56,680
We're gonna go and test this function just as we did in the case

140
00:08:56,680 --> 00:09:00,050
of the last transformation that we performed.

141
00:09:00,050 --> 00:09:05,120
And the way that we're gonna test it is using rxDataStep but

142
00:09:05,120 --> 00:09:10,085
once again just the New York City sample data frame.

143
00:09:10,085 --> 00:09:12,520
Right, so before we use the xdf file,

144
00:09:12,520 --> 00:09:15,502
we're going to use the sample file to test it.

145
00:09:15,502 --> 00:09:19,053
Now I'm gonna run both of these so that while I'm explaining it,

146
00:09:19,053 --> 00:09:21,454
we can see the code running at the same time.

147
00:09:24,427 --> 00:09:27,320
I can see that I forgot to load my function, so

148
00:09:27,320 --> 00:09:29,460
let me just go back up and do that.

149
00:09:32,400 --> 00:09:39,100
So here is my function and here is us running the code.

150
00:09:40,140 --> 00:09:43,530
So we can see that in the case of the test,

151
00:09:43,530 --> 00:09:45,220
everything seems to have worked.

152
00:09:45,220 --> 00:09:49,860
We only have 1,000 rows in the data frame and

153
00:09:49,860 --> 00:09:51,720
when we pass it to rxDataStep,

154
00:09:51,720 --> 00:09:56,620
we specify that the transformation function is find_nhoods.

155
00:09:56,620 --> 00:09:59,780
And we specify that these are the packages that need to be loaded.

156
00:09:59,780 --> 00:10:04,900
And then we have a new argument in here that we need to talk about.

157
00:10:04,900 --> 00:10:08,470
So this argument is called transform objects.

158
00:10:08,470 --> 00:10:14,710
And in this case it's saying, this shapefile, that in the global in

159
00:10:14,710 --> 00:10:20,930
our current R session in the global environment is called mht_shapefile.

160
00:10:20,930 --> 00:10:26,200
Is going to be assign to something that we're gonna call shapefile.

161
00:10:26,200 --> 00:10:28,710
So let's see where this is coming from.

162
00:10:28,710 --> 00:10:32,945
If we scroll up to our transformation function,

163
00:10:32,945 --> 00:10:37,802
it's important to think about our functions as running

164
00:10:37,802 --> 00:10:40,916
in a sterile environment, right.

165
00:10:40,916 --> 00:10:45,286
So whatever inputs the functions need in order to run should be

166
00:10:45,286 --> 00:10:47,820
explicitly specified.

167
00:10:47,820 --> 00:10:51,980
And the function obviously needs the data as one of its inputs.

168
00:10:51,980 --> 00:10:57,990
But this function, if we scroll down to where the over

169
00:10:57,990 --> 00:11:02,280
function is running, also needs the shapefile as one of its inputs.

170
00:11:02,280 --> 00:11:05,250
It's gonna use it twice to get the neighborhoods for

171
00:11:05,250 --> 00:11:09,310
a pick up and drop off.

172
00:11:09,310 --> 00:11:13,710
So this is what this transform objects argument does for us.

173
00:11:13,710 --> 00:11:18,666
It basically says shapefile is another input to this function.

174
00:11:18,666 --> 00:11:21,286
The function thinks of it as shapefile.

175
00:11:21,286 --> 00:11:26,749
And that object is going to be the same as the mht_shapefile

176
00:11:26,749 --> 00:11:31,885
object that I currently have in my global environment,

177
00:11:31,885 --> 00:11:34,195
in my current R session.

178
00:11:34,195 --> 00:11:39,324
And this goes back to what we talked about in talking

179
00:11:39,324 --> 00:11:43,992
about transformations, using RevoScaleR.

180
00:11:43,992 --> 00:11:48,179
There is a certain amount of explicitness that is incumbant upon

181
00:11:48,179 --> 00:11:51,292
us, in order to perform this transformation.

182
00:11:51,292 --> 00:11:55,666
Which, when working in a local R session like we're doing here,

183
00:11:55,666 --> 00:11:58,470
seems a little bit like overkill.

184
00:11:58,470 --> 00:12:00,460
But when we take these transformations and

185
00:12:00,460 --> 00:12:04,350
deploy them in other environments, we can see that this

186
00:12:04,350 --> 00:12:09,324
sort of architecture, this sort of construct is a must-have.

187
00:12:10,710 --> 00:12:13,160
Okay, and the transformation finished deploying

188
00:12:13,160 --> 00:12:14,180
to the whole data set.

189
00:12:14,180 --> 00:12:15,650
We can scroll up here.

190
00:12:15,650 --> 00:12:19,930
We can see that it took about two minutes, so

191
00:12:19,930 --> 00:12:23,228
this is one of the more time consuming transformations.

192
00:12:23,228 --> 00:12:30,616
The over function is where the majority of the time gets spent on.

193
00:12:30,616 --> 00:12:36,283
And, after two minutes, we have, in the xdf file, if we scroll down,

194
00:12:36,283 --> 00:12:42,801
a column, called, pickup_borough, and a column, called, pickup_nhood,

195
00:12:42,801 --> 00:12:47,816
and one, called, dropoff_nhood, and dropoff_borough.

