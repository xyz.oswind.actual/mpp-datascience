0
00:00:01,130 --> 00:00:06,110
In the last section we finished the complex transformation and

1
00:00:06,110 --> 00:00:10,790
we saw that the complexity of the transformation is really buried

2
00:00:10,790 --> 00:00:13,130
inside of the function itself.

3
00:00:14,250 --> 00:00:20,213
So MRS Microsoft R Server and the RevoScaleR

4
00:00:20,213 --> 00:00:25,220
package has a particular requirement in the way the function

5
00:00:25,220 --> 00:00:30,320
has to be set up, in that it takes in the data as its input and

6
00:00:30,320 --> 00:00:33,690
it usually modifies the data somehow and then returns it back.

7
00:00:35,200 --> 00:00:36,265
Other than that,

8
00:00:36,265 --> 00:00:39,467
there are two things that we have to be careful about.

9
00:00:39,467 --> 00:00:44,734
One is that once the data is brought in as an input

10
00:00:44,734 --> 00:00:49,091
to the function, it unpacks as a list.

11
00:00:49,091 --> 00:00:53,532
And so this in the majority of cases, is fine for us, but

12
00:00:53,532 --> 00:00:58,165
it's something that we have to be aware of when performing

13
00:00:58,165 --> 00:01:02,986
certain transformations that require ua to take the list and

14
00:01:02,986 --> 00:01:05,750
reshape it back into a data frame.

15
00:01:07,120 --> 00:01:11,470
The second thing that we have to be very careful about is that the data

16
00:01:11,470 --> 00:01:14,560
is coming in chunk by chunk.

17
00:01:15,830 --> 00:01:21,410
So in this case, the trickiness is that

18
00:01:21,410 --> 00:01:27,040
we have to avoid performing computations.

19
00:01:27,040 --> 00:01:30,663
On the data such as computing sums or averages,

20
00:01:30,663 --> 00:01:35,554
thinking that this is happening for the whole data, when in fact

21
00:01:35,554 --> 00:01:40,009
it could be happening for only a chunk of the data at a time.

22
00:01:40,009 --> 00:01:44,570
Let's say for example that we're interested in computing the minimum

23
00:01:44,570 --> 00:01:46,700
for a particular column.

24
00:01:46,700 --> 00:01:50,890
If we compute the minimum inside of our transformation function,

25
00:01:50,890 --> 00:01:53,850
then we're going to be computing a separate minimum for

26
00:01:53,850 --> 00:01:56,940
each chunk of the data as it's being brought in.

27
00:01:56,940 --> 00:02:02,110
So the correct way of doing it is to perform the computation

28
00:02:02,110 --> 00:02:06,858
outside using one of the RX functions that we've been

29
00:02:06,858 --> 00:02:12,784
familiarizing ourselves with in this case it would be RX summary.

30
00:02:12,784 --> 00:02:15,518
And once we have the minimum, we can then go and

31
00:02:15,518 --> 00:02:18,955
put it inside of the transformation function as the global

32
00:02:18,955 --> 00:02:22,700
minimum instead of the minimum for each of the chunks.

33
00:02:22,700 --> 00:02:28,140
So these are the two things that RevoScaleR, these are the two

34
00:02:28,140 --> 00:02:31,370
constraints, if you would, that is going to impose on us.

35
00:02:31,370 --> 00:02:34,179
But other than that, the ins and

36
00:02:34,179 --> 00:02:39,072
outs of the transformation function is all basically,

37
00:02:39,072 --> 00:02:43,251
putting us back into our familiar R territory.

38
00:02:43,251 --> 00:02:47,910
We can use the base R functions that we know to use.

39
00:02:47,910 --> 00:02:51,630
And we can use any third-party packages that would

40
00:02:51,630 --> 00:02:54,350
facilitate what we're trying to do.

41
00:02:54,350 --> 00:02:57,600
So in this section, we're gonna take a look at one other complex

42
00:02:57,600 --> 00:03:00,980
transformation so that we can really drive this point home

43
00:03:00,980 --> 00:03:04,720
of how to perform these sorts of transformations.

44
00:03:04,720 --> 00:03:09,404
In this case, we're gonna be relying on a third party package,

45
00:03:09,404 --> 00:03:14,453
a GIS package, in order to extract some information out of the data.

46
00:03:14,453 --> 00:03:19,144
In this case, it's gonna be the kind of transformation that would be very

47
00:03:19,144 --> 00:03:23,152
hard for us to do if we did not rely on a third party package in this

48
00:03:23,152 --> 00:03:27,180
case, and as we're gonna see also, on some third party data.

49
00:03:28,760 --> 00:03:30,700
Before we perform the transformation,

50
00:03:30,700 --> 00:03:34,860
let's take a look at what we're trying to accomplish.

51
00:03:34,860 --> 00:03:39,780
So we're here in our R session and

52
00:03:39,780 --> 00:03:44,000
we have three packages that we're gonna load and

53
00:03:44,000 --> 00:03:48,730
I mentioned in the beginning of the course that we have this

54
00:03:48,730 --> 00:03:53,050
folder that we need to download as part of the course content.

55
00:03:53,050 --> 00:03:57,451
And this is a folder that contains, among other things,

56
00:03:57,451 --> 00:03:59,752
what's called a shape file.

57
00:03:59,752 --> 00:04:04,074
In this case, the information comes to us from Zillow, so

58
00:04:04,074 --> 00:04:08,460
this is a file that we downloaded from the Zillow website.

59
00:04:08,460 --> 00:04:12,296
And it's a shape file containing information about the state of

60
00:04:12,296 --> 00:04:12,944
New York.

61
00:04:12,944 --> 00:04:15,234
A shape file is not unique to R,

62
00:04:15,234 --> 00:04:20,168
a lot of programming languages have APIs for reading shape file and

63
00:04:20,168 --> 00:04:25,308
it's a popular format for storing GIS type of information.

64
00:04:25,308 --> 00:04:28,660
Namely, the kinda information that you have when you need to have

65
00:04:29,820 --> 00:04:31,470
neighborhoods with boundaries.

66
00:04:33,700 --> 00:04:36,850
So that's why we have to rely on third party packages,

67
00:04:36,850 --> 00:04:42,040
to be able to read and occasionally manipulate the shape file.

68
00:04:42,040 --> 00:04:46,460
And in this case, we're gonna use the function readShapePoly to go and

69
00:04:46,460 --> 00:04:48,130
read the shape file.

70
00:04:48,130 --> 00:04:53,260
Once we read it in, we're going to subset the shape file, so

71
00:04:53,260 --> 00:04:58,370
that there is only information about Manhattan itself and we're gonna try

72
00:04:58,370 --> 00:05:03,920
to hone in on just taxi trips that are happening inside of Manhattan.

73
00:05:05,210 --> 00:05:08,700
So now we have this shapefile called mht_shapefile and

74
00:05:08,700 --> 00:05:11,870
it contains information about Manhattan.

75
00:05:12,950 --> 00:05:16,050
Let's see what sort of information is in there.

76
00:05:16,050 --> 00:05:22,220
If I just select the shape file and try to print it in the console,

77
00:05:22,220 --> 00:05:27,300
you can see that this is not our standard idea of data.

78
00:05:27,300 --> 00:05:30,590
At least it's not what you would call tabular data.

79
00:05:30,590 --> 00:05:33,774
We can see that there is some tabular information in here.

80
00:05:33,774 --> 00:05:37,018
But there is also non-tabular information, so

81
00:05:37,018 --> 00:05:42,220
this is a more complex type of data format than a data frame would be.

82
00:05:42,220 --> 00:05:44,260
But it is data nevertheless.

83
00:05:44,260 --> 00:05:48,360
And in the case of a shape file that data is going to encode

84
00:05:48,360 --> 00:05:52,720
some information about neighborhoods such as their names and so on.

85
00:05:52,720 --> 00:05:56,130
And more importantly it's also going to encode information about

86
00:05:56,130 --> 00:06:00,400
neighborhood boundaries so that we know how to draw them on a map.

87
00:06:01,940 --> 00:06:03,839
And so, we're gonna do exactly that.

88
00:06:03,839 --> 00:06:08,422
I'm gonna be skipping over some of the more complex details about

89
00:06:08,422 --> 00:06:10,108
what's going on here.

90
00:06:10,108 --> 00:06:15,630
But the goal in this section is just to take the information

91
00:06:15,630 --> 00:06:20,750
that is stored in the shape file, to do some basic data manipulation

92
00:06:20,750 --> 00:06:26,210
to obtain, in this case, the medians for each of the neighborhoods.

93
00:06:26,210 --> 00:06:32,640
And then use those medians to draw a map of Manhattan that shows

94
00:06:32,640 --> 00:06:38,520
each of the neighborhoods with the name superimposed on the map itself.

95
00:06:38,520 --> 00:06:40,130
And out comes the following map.

96
00:06:41,290 --> 00:06:44,243
And so, we can see here a map of Manhattan,

97
00:06:44,243 --> 00:06:48,262
we can see neighborhoods that are color coded with the name

98
00:06:48,262 --> 00:06:53,118
of the neighborhood showing more or less on the neighborhood itself.

99
00:06:53,118 --> 00:06:57,493
And so, this is the information that was encoded in

100
00:06:57,493 --> 00:07:00,560
the shape file that we just loaded.

101
00:07:00,560 --> 00:07:03,960
In this case, we use the information to draw a map.

102
00:07:03,960 --> 00:07:08,411
In the next section, what we're gonna be doing is we're gonna write

103
00:07:08,411 --> 00:07:12,707
a complex transformation that is going to go to the coordinates for

104
00:07:12,707 --> 00:07:15,948
pickup and drop-off, which we have in our data.

105
00:07:15,948 --> 00:07:19,613
And it's gonna find the neighborhood that the pickup and

106
00:07:19,613 --> 00:07:21,685
the drop-off is happening at.

