0
00:00:01,180 --> 00:00:05,810
So, we now have an XDF file that has the same content as the six

1
00:00:05,810 --> 00:00:07,100
CSV files put together.

2
00:00:08,280 --> 00:00:11,070
And we might be wondering what is this newfangled,

3
00:00:11,070 --> 00:00:16,230
fancy XDF file, and do we have to use it?

4
00:00:16,230 --> 00:00:19,500
And the answer is no, but there are tradeoffs.

5
00:00:19,500 --> 00:00:20,290
And in this section,

6
00:00:20,290 --> 00:00:23,830
we wanna get a better handle on what these tradeoffs are.

7
00:00:23,830 --> 00:00:26,380
So, let's take a look at an example here.

8
00:00:26,380 --> 00:00:31,580
We're gonna start with the XDF file that we just created and

9
00:00:31,580 --> 00:00:38,280
we're gonna use the RxXdfData to point to the file path and

10
00:00:38,280 --> 00:00:43,600
make it aware that it's an XDF file, and we're

11
00:00:43,600 --> 00:00:49,430
gonna take that and store it in an object called nyc_xdf.

12
00:00:49,430 --> 00:00:51,080
So, for all intents and purposes,

13
00:00:51,080 --> 00:00:56,270
from now on, nyc_xdf is going to point to our XDF file,

14
00:00:56,270 --> 00:01:01,090
the same way that we point to a data frame by giving it an enigma.

15
00:01:03,100 --> 00:01:04,960
Then we're gonna do the following.

16
00:01:04,960 --> 00:01:07,500
We're gonna run our first summary function and

17
00:01:07,500 --> 00:01:10,040
we will scale R, it's gonna be called rxSummary.

18
00:01:11,090 --> 00:01:12,800
And in this case,

19
00:01:12,800 --> 00:01:16,290
we wanna get a summary of the column called fare amount.

20
00:01:16,290 --> 00:01:19,654
You see here the popular formula notation that a lot of

21
00:01:19,654 --> 00:01:22,300
R functions used with the tilde.

22
00:01:22,300 --> 00:01:27,157
So, here we're asking for a summary of the column

23
00:01:27,157 --> 00:01:32,280
called fare amount in the XDF file called nyc_xdf.

24
00:01:32,280 --> 00:01:38,760
So, let's run this and as we can see, it runs really quickly and

25
00:01:38,760 --> 00:01:43,230
our summary is shown right here on the bottom of Rconsole.

26
00:01:43,230 --> 00:01:48,170
In addition to the summary, we can see the runtime shown right there.

27
00:01:48,170 --> 00:01:51,060
And we can some additional information about the file

28
00:01:51,060 --> 00:01:55,220
such as the name of the file, the number of observations, and so on.

29
00:01:57,060 --> 00:02:02,190
So, for the fare amount, the average fare amount is about $13,

30
00:02:02,190 --> 00:02:05,618
the standard deviation is relatively high.

31
00:02:05,618 --> 00:02:09,815
We can see that we have somethings about the data that we have to

32
00:02:09,815 --> 00:02:13,960
question such as why we would have negative fare amounts.

33
00:02:13,960 --> 00:02:16,939
It's likely that this could correspond to refunds.

34
00:02:18,060 --> 00:02:22,760
We can see that the largest taxi trip had a fare

35
00:02:22,760 --> 00:02:25,180
amount of about $2,500.

36
00:02:25,180 --> 00:02:27,460
The number of valid observations, and

37
00:02:27,460 --> 00:02:31,810
if there were any missing observations in the data.

38
00:02:31,810 --> 00:02:34,168
So, with an XDF file,

39
00:02:34,168 --> 00:02:40,160
we just pass it to a function such as rxSummary and we get our summary.

40
00:02:40,160 --> 00:02:44,660
Now, to convince you that we don't have to use an XDF,

41
00:02:44,660 --> 00:02:48,010
we're gonna run our rxSummary function a second time, but

42
00:02:48,010 --> 00:02:54,130
this time we're gonna pass one of the CSV files directly to rxSummary.

43
00:02:54,130 --> 00:02:59,219
So, in this case, the CSV file is going to be for January of 2016.

44
00:03:00,970 --> 00:03:02,880
So, there is the file path.

45
00:03:02,880 --> 00:03:07,680
And then, in the prior step, we use the RxXdfData

46
00:03:07,680 --> 00:03:13,150
function and gave the file path directly to it.

47
00:03:13,150 --> 00:03:15,870
Because a CSV file is not an XDF file,

48
00:03:15,870 --> 00:03:19,290
we're gonna use the RxTextData function this time.

49
00:03:19,290 --> 00:03:22,740
And once again, we just passed at the file path, but

50
00:03:22,740 --> 00:03:28,550
now we have a secondary argument here that we can pass to RxTextData.

51
00:03:28,550 --> 00:03:32,930
Recall that in the case of the XDF file, when we use RxImport,

52
00:03:32,930 --> 00:03:38,190
we specified the column classes in RxImport statement.

53
00:03:38,190 --> 00:03:43,310
We can go up here to double-check that here's our rxImport and

54
00:03:43,310 --> 00:03:45,110
here's colClasses.

55
00:03:45,110 --> 00:03:49,630
So, the column types are built into the XDF file.

56
00:03:49,630 --> 00:03:53,370
Column types however, are not built into the CSV file.

57
00:03:53,370 --> 00:03:58,555
So, when we specify RxTextData, we have the optional argument,

58
00:03:58,555 --> 00:04:03,220
the recommended optional argument of also specifying the column types.

59
00:04:04,600 --> 00:04:09,085
Otherwise, no amount of calculation is gonna happen in this case.

60
00:04:09,085 --> 00:04:11,095
We're just specifying the file path and

61
00:04:11,095 --> 00:04:14,895
then we're pointing to it using the RxTextData function.

62
00:04:14,895 --> 00:04:19,185
The calculation is gonna happen when we run the rxSummary a second time,

63
00:04:19,185 --> 00:04:22,145
this time directly on the CSV file.

64
00:04:22,145 --> 00:04:26,115
So, let's run that and let's actually see how long it takes for

65
00:04:26,115 --> 00:04:26,685
it to run.

66
00:04:28,290 --> 00:04:31,200
And what we're gonna notice here is first of all,

67
00:04:31,200 --> 00:04:36,120
it takes quite a bit longer to run rxSummary on a single

68
00:04:36,120 --> 00:04:40,740
CSV file than it took to run rxSummary on

69
00:04:40,740 --> 00:04:46,200
the XDF file that had the content of all CSV files put together.

70
00:04:46,200 --> 00:04:50,980
So, the reason is because XDF is a highly efficient format and

71
00:04:50,980 --> 00:04:54,170
because of that, it's gonna run faster.

72
00:04:54,170 --> 00:04:56,630
Another reason has to do with I/O,

73
00:04:56,630 --> 00:05:00,510
I mentioned before that the CSV's put together, take up a lot more

74
00:05:00,510 --> 00:05:04,990
space than the XDF file because XDF uses compression.

75
00:05:04,990 --> 00:05:07,230
And because of that, we have reduced I/O,

76
00:05:07,230 --> 00:05:11,990
we have reduced reading going on when rxSummary runs on the XDF file

77
00:05:11,990 --> 00:05:15,120
than when it does on a single CSV file.

78
00:05:15,120 --> 00:05:20,060
So, we pay the cost of using a CSV

79
00:05:20,060 --> 00:05:24,970
file at the time when we run our rxSummary.

80
00:05:24,970 --> 00:05:26,900
However, there is a benefit and

81
00:05:26,900 --> 00:05:30,700
there is a little catch that we have to specify here.

82
00:05:30,700 --> 00:05:34,990
The benefit is that in the second case,

83
00:05:34,990 --> 00:05:37,770
we didn't have to convert the data to XDF.

84
00:05:37,770 --> 00:05:40,620
So, this could be a benefit in the sense that

85
00:05:40,620 --> 00:05:43,670
we don't have to have two separate copies of the data.

86
00:05:43,670 --> 00:05:50,040
One as the individual CSV and the second one as the XDF file.

87
00:05:50,040 --> 00:05:53,450
Another little catch that we have to specify is that we

88
00:05:53,450 --> 00:05:58,960
did have to convert the CSVs into XDF in the prior step.

89
00:05:58,960 --> 00:06:03,440
So, to make it really an apples to apples sort of comparison,

90
00:06:03,440 --> 00:06:08,520
we have to consider the runtime that it takes to convert a CSV file into

91
00:06:08,520 --> 00:06:14,070
an XDF file and the runtime that it takes to run rxSummary on the XDF

92
00:06:14,070 --> 00:06:18,560
file, and compare that with the run time that it takes to run rxSummary

93
00:06:18,560 --> 00:06:23,810
directly on the CSV file and skip the conversion all together.

94
00:06:25,260 --> 00:06:29,310
In that case, the answer is it depends.

95
00:06:29,310 --> 00:06:34,120
However, if our only goal was to just run rxSummary,

96
00:06:34,120 --> 00:06:37,350
we can run a test and we can see which is more efficient.

97
00:06:37,350 --> 00:06:39,220
The problem is that most of the time,

98
00:06:39,220 --> 00:06:42,420
we're not just interested in doing an individual summary on the data,

99
00:06:42,420 --> 00:06:45,480
we're interested in summarizing the data and then building

100
00:06:45,480 --> 00:06:48,800
models with it and doing all kinds of data transformations on it.

101
00:06:49,950 --> 00:06:54,330
So, the more we use the RevoScaleR functions, the more we benefit from

102
00:06:54,330 --> 00:06:59,920
converting it from the original flat files into an XDF format.

103
00:06:59,920 --> 00:07:01,823
So that once it's in XDF,

104
00:07:01,823 --> 00:07:06,071
we can efficiently process the data to run our analytics.

