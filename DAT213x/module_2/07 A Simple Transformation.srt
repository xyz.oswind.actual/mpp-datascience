0
00:00:01,600 --> 00:00:05,160
So we have our data in an XDF format.

1
00:00:05,160 --> 00:00:09,260
And we're gonna start by performing our first transformation

2
00:00:09,260 --> 00:00:09,800
on the data.

3
00:00:11,340 --> 00:00:15,106
As it turns out, we have two different ways of performing

4
00:00:15,106 --> 00:00:19,350
transformations with XDF files, or in RevoScaleR in general.

5
00:00:20,950 --> 00:00:23,670
One of the ways is to perform the transformation and

6
00:00:23,670 --> 00:00:27,830
write it out to the data as a new column in the data.

7
00:00:27,830 --> 00:00:31,460
And we're gonna see an example of that in just a little bit.

8
00:00:31,460 --> 00:00:34,030
But a second way of doing a transformation

9
00:00:34,030 --> 00:00:37,810
is to perform the transformation on the fly when we get

10
00:00:37,810 --> 00:00:39,350
some kind of summary function.

11
00:00:39,350 --> 00:00:41,380
Or some kind of analytics function and

12
00:00:41,380 --> 00:00:44,060
we need to create a variable in order to do it.

13
00:00:45,720 --> 00:00:50,110
Now, the same concept does exist to some extent

14
00:00:50,110 --> 00:00:52,950
in open source R when dealing with data frames.

15
00:00:52,950 --> 00:00:56,750
It's just that with data frames, we are a little bit more sloppy

16
00:00:56,750 --> 00:00:59,990
with these sorts of transformations, because ultimately

17
00:00:59,990 --> 00:01:03,480
the transformation has to happen in the memory.

18
00:01:03,480 --> 00:01:05,610
And so whether it happens on the fly or

19
00:01:05,610 --> 00:01:09,550
whether it happens and gets written out to the data.

20
00:01:09,550 --> 00:01:12,880
Doesn't matter to us as much, because it doesn't make much of

21
00:01:12,880 --> 00:01:16,180
a difference in terms of efficiency and run time.

22
00:01:16,180 --> 00:01:18,910
In the case of RevoScaleR, that's not the case.

23
00:01:18,910 --> 00:01:23,590
With RevoScaleR, because the data that we have is external, because it

24
00:01:23,590 --> 00:01:30,480
is saved on disk, I/O is going to be a considerable factor in run time.

25
00:01:30,480 --> 00:01:34,430
And because of that, we need to be a little bit picky about which

26
00:01:34,430 --> 00:01:38,850
transformations we actually need to write out to that data and which

27
00:01:38,850 --> 00:01:42,750
transformation are easy enough that we can just compute them on the fly.

28
00:01:42,750 --> 00:01:44,560
And we're gonna see some examples of that now.

29
00:01:45,720 --> 00:01:49,784
So first, let's see how we can perform a transformation and

30
00:01:49,784 --> 00:01:51,466
write it out to the data.

31
00:01:51,466 --> 00:01:54,745
The function that we're gonna use is called rxDataStep.

32
00:01:54,745 --> 00:01:57,315
This is a function that we're gonna see quite a bit of

33
00:01:57,315 --> 00:02:00,985
throughout this course, because it's the general purpose function that we

34
00:02:00,985 --> 00:02:03,662
use to do a lot of data processing.

35
00:02:05,042 --> 00:02:08,892
Data processing could involved transformations.

36
00:02:08,892 --> 00:02:11,622
It could be involve subsetting data.

37
00:02:11,622 --> 00:02:15,922
And occasionally it could be involve joining data together.

38
00:02:17,522 --> 00:02:21,943
So in this case we're gonna use rxDataStep to perform

39
00:02:21,943 --> 00:02:24,212
a simple transformation.

40
00:02:24,212 --> 00:02:28,150
We're gonna pass as the first argument the NYC taxi data.

41
00:02:28,150 --> 00:02:30,670
And because we want the transformation to happen to

42
00:02:30,670 --> 00:02:31,630
the same data,

43
00:02:31,630 --> 00:02:37,890
we're gonna write it out to nyc_xdf once the transformation is done.

44
00:02:37,890 --> 00:02:40,860
So notice that with rxDataStep we have an input and

45
00:02:40,860 --> 00:02:42,860
we have an output in this case.

46
00:02:42,860 --> 00:02:46,619
Because the output is the same as the input, we specify that overwrite

47
00:02:46,619 --> 00:02:51,040
= TRUE so that we can be explicit about overwriting to the same data.

48
00:02:51,040 --> 00:02:55,090
The rest of this is the transformation itself.

49
00:02:55,090 --> 00:02:58,840
The transformation is passed to an argument called transforms.

50
00:02:59,860 --> 00:03:02,970
And this argument has to be a list.

51
00:03:02,970 --> 00:03:07,480
So some of the concepts that we're gonna meet as we

52
00:03:07,480 --> 00:03:10,750
familiarize ourselves with the RevoScaleR functions have to do with

53
00:03:10,750 --> 00:03:11,960
just constructs.

54
00:03:11,960 --> 00:03:16,030
And in this case transforms expects a list.

55
00:03:16,030 --> 00:03:19,840
And within that list we can see the transformation itself.

56
00:03:19,840 --> 00:03:22,030
Let's see what the transformation is.

57
00:03:22,030 --> 00:03:24,900
The transformation is gonna compute

58
00:03:24,900 --> 00:03:29,790
a column called tip_percent based on the following logic.

59
00:03:29,790 --> 00:03:32,602
If fare_amount is greater than zero and

60
00:03:32,602 --> 00:03:37,068
tip_amount is less than fare_amount, then we're gonna take

61
00:03:37,068 --> 00:03:41,375
tip_amount divided by fare_amount multiplied by 100.

62
00:03:41,375 --> 00:03:44,287
And we're gonna round it into a integer, and

63
00:03:44,287 --> 00:03:46,450
that's gonna be tip_percent.

64
00:03:46,450 --> 00:03:49,558
If, on the other hand, this condition is not met.

65
00:03:49,558 --> 00:03:52,294
So if either fare_amount is less than zero,

66
00:03:52,294 --> 00:03:54,531
which we saw occasionally happens.

67
00:03:54,531 --> 00:03:58,893
Or if tip_amount is greater than fare_amount for some reason,

68
00:03:58,893 --> 00:04:02,555
we're gonna make the value for tip_percent to be NA.

69
00:04:02,555 --> 00:04:07,215
Now, let's not worry too much about the whys of the transformation.

70
00:04:07,215 --> 00:04:09,185
This is somewhat subjective and

71
00:04:09,185 --> 00:04:12,005
sometimes dependent on business decisions.

72
00:04:12,005 --> 00:04:15,525
What we care about is kind of seeing how the transformation is passed

73
00:04:15,525 --> 00:04:20,845
as a list to rxDataStep in order to perform the transformation.

74
00:04:20,845 --> 00:04:22,615
So I'm gonna run the transformation now.

75
00:04:22,615 --> 00:04:27,291
And as soon as the transformation is over, we're gonna run an rxSummary

76
00:04:27,291 --> 00:04:30,818
on the XDF file on the new column called tip_percent so

77
00:04:30,818 --> 00:04:33,055
that we can see what it looks like.

78
00:04:35,264 --> 00:04:37,428
Now, while this is running,

79
00:04:37,428 --> 00:04:41,940
we can see some information being printed out to the console.

80
00:04:43,240 --> 00:04:45,890
This is namely going to be information about the number of

81
00:04:45,890 --> 00:04:49,410
rows of the data that are processed as this is happening.

82
00:04:49,410 --> 00:04:55,170
So we get some report progress as the transformation is happening.

83
00:04:55,170 --> 00:04:57,660
And this is useful if we have very large data sets and

84
00:04:57,660 --> 00:05:01,690
we need to know how much longer we have to wait before we

85
00:05:01,690 --> 00:05:04,200
finish chunking through all the data.

86
00:05:04,200 --> 00:05:08,890
We can also see that it confirms something that we learned

87
00:05:08,890 --> 00:05:12,840
about RevoScaleR, namely that the data is processed chunk by chunk.

88
00:05:12,840 --> 00:05:17,600
So I mentioned in the beginning of the course that RevoScaleR does not

89
00:05:17,600 --> 00:05:19,830
load the data unless it's very small.

90
00:05:19,830 --> 00:05:24,200
It does not load the data in its entirety into the R session.

91
00:05:24,200 --> 00:05:27,960
What it does instead is it takes a chunk of the data and

92
00:05:27,960 --> 00:05:32,260
it loads it as a data frame in R so that the transformation can happen

93
00:05:32,260 --> 00:05:34,620
when the chunk is finished being processed.

94
00:05:34,620 --> 00:05:38,110
In this case the processing involved creating a new column.

95
00:05:38,110 --> 00:05:40,110
Then it's written out to the data and

96
00:05:40,110 --> 00:05:44,740
a new chunk is brought into the R session for the same transformation.

97
00:05:44,740 --> 00:05:48,150
And it's gonna keep doing that until it's done chunking through all

98
00:05:48,150 --> 00:05:48,670
of the data.

99
00:05:49,710 --> 00:05:54,267
When all of it is done, we now have the original XDF file with

100
00:05:54,267 --> 00:05:58,107
the new column called tip_percent inside of it.

101
00:05:58,107 --> 00:06:02,016
And so we can see that in the case of rxSummary,

102
00:06:02,016 --> 00:06:05,560
we can see the summary for tip_percent.

103
00:06:05,560 --> 00:06:12,180
We can see that on average, customers paid about 14% in tip.

104
00:06:12,180 --> 00:06:14,450
The standard deviation is a bit high.

105
00:06:14,450 --> 00:06:16,560
We can see the minimum and the maximum.

106
00:06:16,560 --> 00:06:19,270
And we can see that we have quite a few missing values.

107
00:06:19,270 --> 00:06:22,868
So these are the missing values that are generated,

108
00:06:22,868 --> 00:06:27,671
probably because the condition that we set here was not met.

109
00:06:27,671 --> 00:06:32,150
Okay, so that's it, we have a transformation on the data.

110
00:06:32,150 --> 00:06:36,030
And this transformation involved using the rxDataStep function,

111
00:06:36,030 --> 00:06:39,090
because we wanted the new column, tip_percent in this case,

112
00:06:39,090 --> 00:06:41,040
to be written out to the data.

113
00:06:42,540 --> 00:06:45,280
Now we're gonna do the same transformation but

114
00:06:45,280 --> 00:06:47,720
we're gonna do it slightly differently.

115
00:06:47,720 --> 00:06:51,040
And what I want to point out about this transformation, first of all,

116
00:06:51,040 --> 00:06:53,900
is that it's going to be quite a bit faster.

117
00:06:53,900 --> 00:06:56,290
So if you were paying attention to the run time before,

118
00:06:56,290 --> 00:06:58,480
you can see that the second transformation,

119
00:06:58,480 --> 00:07:01,860
even though it's returning exactly the same results,

120
00:07:01,860 --> 00:07:05,150
happened a lot faster than the first transformation.

121
00:07:05,150 --> 00:07:09,780
Let's see first what's going on with this transformation.

122
00:07:09,780 --> 00:07:12,140
So we don't have an rxDataStep anymore.

123
00:07:12,140 --> 00:07:13,940
Now we simply have rxSummary.

124
00:07:14,960 --> 00:07:17,420
And so we can see that

125
00:07:17,420 --> 00:07:21,870
we want the summary of a column called tip_percent2.

126
00:07:21,870 --> 00:07:27,312
And it's going to be in this nyc_xdf data file.

127
00:07:27,312 --> 00:07:31,930
However, tip_percent2 is not a column in the data yet.

128
00:07:31,930 --> 00:07:36,380
And so what we're gonna do is we're gonna use the transforms argument,

129
00:07:36,380 --> 00:07:40,770
this time not inside of rxDataStep but inside of rxSummary, so

130
00:07:40,770 --> 00:07:44,550
that we can create the column called tip_percent2 on the fly.

131
00:07:46,280 --> 00:07:49,360
In other words, we're gonna take the data,

132
00:07:49,360 --> 00:07:53,805
we're gonna use the columns in the data to create tip_percent2.

133
00:07:53,805 --> 00:07:56,701
When this column is created, we're gonna pass it directly to

134
00:07:56,701 --> 00:08:00,253
rxSummary so that rxSummary could summarize it for us.

135
00:08:00,253 --> 00:08:03,665
And when rxSummary is done, this column is gonna disappear and

136
00:08:03,665 --> 00:08:05,345
it's not gonna be written out to the data.

137
00:08:06,395 --> 00:08:10,595
Now, this is the main reason, the fact that it's not written out to

138
00:08:10,595 --> 00:08:16,000
the data is the main reason that rxSummary

139
00:08:16,000 --> 00:08:20,940
in this second call is faster than rxSummary in the first call.

140
00:08:20,940 --> 00:08:24,070
In other words, in the first call using rxDataStep,

141
00:08:24,070 --> 00:08:27,740
we had to create the column and write it out to the data.

142
00:08:27,740 --> 00:08:33,974
Because of that, we pay a certain amount of cost in terms of I/O.

143
00:08:33,974 --> 00:08:37,000
Namely, the cost of writing it out to the data.

144
00:08:37,000 --> 00:08:39,940
In the second column, we're using rxSummary.

145
00:08:39,940 --> 00:08:43,180
We're only reading from the data so that we can

146
00:08:43,180 --> 00:08:48,250
create the transformation, create the new column, run rxSummary,

147
00:08:48,250 --> 00:08:50,540
perform the summarization on the fly.

148
00:08:50,540 --> 00:08:55,290
And then there is no writing back to the data involved in this case.

149
00:08:55,290 --> 00:08:57,460
So we get some efficiency out of doing that.

150
00:08:58,900 --> 00:09:03,950
So we might be wondering, when do we perform a transformation and

151
00:09:03,950 --> 00:09:06,150
write it out to the data and

152
00:09:06,150 --> 00:09:09,740
when do we perform a transformation and do it on the fly?

153
00:09:11,280 --> 00:09:14,404
And the answer to that is somewhat subjective.

154
00:09:14,404 --> 00:09:18,235
It's somewhat dependent on how often we're gonna be using this column,

155
00:09:18,235 --> 00:09:18,990
for example.

156
00:09:20,080 --> 00:09:24,490
If the column tip_percent2 here was created only for the purpose

157
00:09:24,490 --> 00:09:27,910
of running an rxSummary on it and we're not gonna need it later for

158
00:09:27,910 --> 00:09:30,125
building a model or doing other things with it.

159
00:09:30,125 --> 00:09:33,880
Then I guess we can basically perform the transformation on

160
00:09:33,880 --> 00:09:37,920
the fly, pass it to rxSummary, and be done with it.

161
00:09:37,920 --> 00:09:38,840
If, on the other hand,

162
00:09:38,840 --> 00:09:43,450
we're gonna be using this column a lot, then it might be to

163
00:09:43,450 --> 00:09:46,570
our advantage to just have the column written out to the data.

164
00:09:47,980 --> 00:09:51,250
However, that doesn't mean that we can't perform the transformation on

165
00:09:51,250 --> 00:09:52,800
the fly every time we need it.

166
00:09:52,800 --> 00:09:54,590
So there is another option.

167
00:09:54,590 --> 00:09:57,400
Of course, it means that we're gonna have the same transformation

168
00:09:57,400 --> 00:09:59,410
happening in a lot of different places.

169
00:09:59,410 --> 00:10:01,760
So it makes the code a little bit less modular.

170
00:10:03,350 --> 00:10:09,347
But it does give us potentially some gain in terms of run time.

171
00:10:11,230 --> 00:10:15,440
As a general rule of thumb, this is the way to think about it.

172
00:10:15,440 --> 00:10:19,660
If the transformation is gonna be pretty complicated and

173
00:10:19,660 --> 00:10:24,570
it's gonna involve some heavy computation, it's probably gonna be

174
00:10:24,570 --> 00:10:28,680
worth it for us to put the transformation into the data.

175
00:10:28,680 --> 00:10:33,059
This is especially true if the transformation in

176
00:10:33,059 --> 00:10:38,092
question is going to be hard for us to reverse engineer.

177
00:10:38,092 --> 00:10:42,053
If, on the other hand, the transformation is really simple,

178
00:10:42,053 --> 00:10:46,164
in the sense that we can derive it from existing columns in the data

179
00:10:46,164 --> 00:10:49,676
any time we need to, and it's a pretty simple formula,

180
00:10:49,676 --> 00:10:54,350
it tends to be better to perform the transformation on the fly.

181
00:10:54,350 --> 00:10:56,260
Now, it tends to be better.

182
00:10:56,260 --> 00:11:00,190
I'm intentionally being a little bit vague here, because as I said,

183
00:11:00,190 --> 00:11:04,480
it depends on how we're using it, how often we're using it.

184
00:11:04,480 --> 00:11:06,230
It depends on what the context is.

185
00:11:07,230 --> 00:11:09,790
But let's see if we can cover an example of that

186
00:11:09,790 --> 00:11:11,950
in the next code chunk.

187
00:11:11,950 --> 00:11:14,470
So here we're using the rxCrossTabs function.

188
00:11:14,470 --> 00:11:16,480
It's a new function in RevoScaleR.

189
00:11:17,560 --> 00:11:22,300
And what it does is basically gives us counts.

190
00:11:22,300 --> 00:11:25,120
In this case, we wanna get counts and

191
00:11:25,120 --> 00:11:29,070
we have the following formula that we pass to rxCrossTabs.

192
00:11:30,780 --> 00:11:33,620
Once again, it's the formula and the station similar to the one that we

193
00:11:33,620 --> 00:11:35,380
were using in the case of rxSummary.

194
00:11:36,670 --> 00:11:42,480
And what we want to get counts for is for month and year combinations.

195
00:11:42,480 --> 00:11:45,770
I say combinations because in the formula notation,

196
00:11:45,770 --> 00:11:47,742
we're using the colon there and

197
00:11:47,742 --> 00:11:51,187
the colon is how we can get interactions of two columns.

198
00:11:51,187 --> 00:11:54,742
So we want the interaction of month and year, and

199
00:11:54,742 --> 00:11:58,310
we wanna get counts out of it using rxCrossTabs.

200
00:11:59,520 --> 00:12:01,830
We're gonna pass, once again, the data.

201
00:12:01,830 --> 00:12:03,440
And because month and

202
00:12:03,440 --> 00:12:07,310
year are not columns in the data, we're gonna have to perform

203
00:12:07,310 --> 00:12:11,790
a transformation on the fly to calculate what those columns are.

204
00:12:11,790 --> 00:12:13,940
So there's two ways that we can do it.

205
00:12:13,940 --> 00:12:16,420
And just for the sake of illustration,

206
00:12:16,420 --> 00:12:18,670
I'm gonna show both ways here.

207
00:12:19,700 --> 00:12:22,680
In the first way, we're gonna use transforms and

208
00:12:22,680 --> 00:12:27,750
we're going to treat the pickup_datetime column,

209
00:12:27,750 --> 00:12:31,780
which is the column that year and month are gonna be extracted out of.

210
00:12:31,780 --> 00:12:35,300
We're gonna treat that as a character column.

211
00:12:35,300 --> 00:12:39,350
Let's actually double-check that it is in fact a character column.

212
00:12:39,350 --> 00:12:42,510
If we go back up to our rxGetInfo.

213
00:12:42,510 --> 00:12:49,270
When we ran rxGetInfo on the XDF file, we can see in the column types

214
00:12:49,270 --> 00:12:53,730
right here that pickup_datetime is in fact a character column.

215
00:12:53,730 --> 00:12:56,600
So this is just a way that we specify the column type for

216
00:12:56,600 --> 00:12:57,840
this column.

217
00:12:58,850 --> 00:13:01,010
And so we're gonna treat it as a character column,

218
00:13:01,010 --> 00:13:01,870
which is what it is.

219
00:13:01,870 --> 00:13:06,970
And we're gonna use the substr function to extract the year and

220
00:13:06,970 --> 00:13:10,084
then use it at second time to extract the month from

221
00:13:10,084 --> 00:13:13,870
the pickup_datetime column.

222
00:13:13,870 --> 00:13:16,620
Once we have those, we're gonna convert them into integers.

223
00:13:17,640 --> 00:13:19,900
And then we have one last step that we need to do.

224
00:13:21,240 --> 00:13:25,090
Because we want counts for year and

225
00:13:25,090 --> 00:13:30,630
month in RevoScaleR, in this case for the rxCrossTabs function,

226
00:13:30,630 --> 00:13:35,190
the expectation is that these columns need to be factors, why?

227
00:13:35,190 --> 00:13:39,350
Because we are treating year and month as categorical data and

228
00:13:39,350 --> 00:13:43,250
categorical data in R needs to be formatted as a factor.

229
00:13:43,250 --> 00:13:47,570
So we're gonna convert year and month into factors.

230
00:13:47,570 --> 00:13:51,809
And then we have leeway here in specifying whatever levels we want

231
00:13:51,809 --> 00:13:52,356
to see.

232
00:13:52,356 --> 00:13:56,410
For the months, it makes sense to specify 1 through 12 as the levels.

233
00:13:56,410 --> 00:14:00,115
For the year, we only 2016 as the data here, but we can specify 2014

234
00:14:00,115 --> 00:14:06,200
through 2016 if we choose to, which could be useful in some cases.

235
00:14:06,200 --> 00:14:08,371
So I'm gonna run this transformation and

236
00:14:08,371 --> 00:14:13,340
then we're gonna look at what's gonna happen.

237
00:14:13,340 --> 00:14:16,810
So as this transformation is happening,

238
00:14:16,810 --> 00:14:20,826
the data is being processed chunk by chunk.

239
00:14:20,826 --> 00:14:25,570
The pickup_datetime column is being used to extract year and

240
00:14:25,570 --> 00:14:27,640
month, convert it into factors.

241
00:14:27,640 --> 00:14:30,320
Those factors are being passed to rxCrossTabs,

242
00:14:30,320 --> 00:14:35,080
which then takes them and returns counts for the year and

243
00:14:35,080 --> 00:14:39,410
month of the range of the data that we have.

244
00:14:39,410 --> 00:14:44,570
And we can see here, we can confirm here that we only have data for

245
00:14:44,570 --> 00:14:48,020
the first six months of the year 2016.

246
00:14:48,020 --> 00:14:49,490
So that's our transformation.

247
00:14:49,490 --> 00:14:53,360
We did it on the fly inside of the rxCrossTabs function.

248
00:14:53,360 --> 00:14:56,440
And this is an example of a transformation that just makes sense

249
00:14:56,440 --> 00:14:58,060
for us to perform on the fly,

250
00:14:58,060 --> 00:15:00,300
just because it's a pretty simple transformation.

251
00:15:01,600 --> 00:15:05,704
And anytime we need to calculate year and month, we can go back to

252
00:15:05,704 --> 00:15:09,665
the original column, pickup_datetime, to extract them.

253
00:15:09,665 --> 00:15:13,553
We're gonna perform this transformation on the fly once again

254
00:15:13,553 --> 00:15:15,990
but slightly differently.

255
00:15:15,990 --> 00:15:20,890
And again, which one of this you choose somewhat depends on which is

256
00:15:20,890 --> 00:15:22,210
more efficient and

257
00:15:22,210 --> 00:15:27,260
somewhat dependet on our preference for using certain functions.

258
00:15:27,260 --> 00:15:30,550
Right, so we're gonna perform the same transformation.

259
00:15:31,820 --> 00:15:34,580
Notice that we have the same formula, we're pointing to the same

260
00:15:34,580 --> 00:15:38,410
data, but transforms in this case is slightly different.

261
00:15:39,470 --> 00:15:43,447
Instead of thinking as pickup_datetime as a character

262
00:15:43,447 --> 00:15:48,354
column and then using substr to extract year and month, we're gonna

263
00:15:48,354 --> 00:15:54,470
pass pickup_datetime to the ymd_hms function, which is in lubridate.

264
00:15:54,470 --> 00:15:59,261
This function will convert the datetime column from a character

265
00:15:59,261 --> 00:16:01,130
into a datetime column.

266
00:16:02,670 --> 00:16:05,820
So once we do that, we have a column.

267
00:16:05,820 --> 00:16:09,250
I'm gonna run this, we have a column called date, and

268
00:16:09,250 --> 00:16:10,660
this column is datetime.

269
00:16:10,660 --> 00:16:14,220
So now we can use the year function and

270
00:16:14,220 --> 00:16:18,070
the month function, both of which are functions inside of lubridate,

271
00:16:19,160 --> 00:16:23,970
in order to extract year and month for the datetime.

272
00:16:23,970 --> 00:16:26,940
Once this is done, we can pass those to factor, and

273
00:16:26,940 --> 00:16:31,380
just as before, convert them into factors so that rxCrossTabs can

274
00:16:31,380 --> 00:16:34,110
treat them as categorical data and give us counts for them.

275
00:16:35,150 --> 00:16:39,760
So the transformation is over, and we can see the same result as we saw

276
00:16:39,760 --> 00:16:45,360
when we performed the earlier transformation with rxCrossTab.

277
00:16:45,360 --> 00:16:49,407
The last thing I wanna point out about this transformation is that in

278
00:16:49,407 --> 00:16:56,090
addition to specifying, to writing out the transformation,

279
00:16:56,090 --> 00:17:01,060
we're also specifying another argument called transformPackages.

280
00:17:01,060 --> 00:17:04,300
This argument is set to lubridate.

281
00:17:04,300 --> 00:17:08,310
Now, we're gonna see that in the future this sort of

282
00:17:08,310 --> 00:17:11,800
construct will make more sense to us as we become more and

283
00:17:11,800 --> 00:17:15,170
more familiar with the way that RevoScaleR works.

284
00:17:15,170 --> 00:17:20,030
So for now I'm just gonna say that RevoScaleR requires on

285
00:17:20,030 --> 00:17:24,420
our part to be a little bit more explicit about what's happening.

286
00:17:24,420 --> 00:17:27,940
And we're not always used to being this explicit in R.

287
00:17:27,940 --> 00:17:29,880
We tend to be a little bit sloppy in R.

288
00:17:29,880 --> 00:17:32,760
Usually we just specify library at the very top and

289
00:17:32,760 --> 00:17:35,200
then the functions automatically work.

290
00:17:35,200 --> 00:17:39,826
So why is it that here when using the hms_ymd function or the year and

291
00:17:39,826 --> 00:17:44,292
the month function we need to specify that they come directly from

292
00:17:44,292 --> 00:17:46,009
the lubridate library?

293
00:17:46,009 --> 00:17:49,114
And that will be more clear over time, but for

294
00:17:49,114 --> 00:17:53,609
now let's just say that we need to be more explicit because we're

295
00:17:53,609 --> 00:17:58,595
interested in the future potentially to take this code and deploy it on,

296
00:17:58,595 --> 00:18:02,260
say, a Hadoop cluster or deploy it on SQL Server.

297
00:18:02,260 --> 00:18:06,670
So deploy it on other environments other than the one that is our

298
00:18:06,670 --> 00:18:07,330
current machine.

