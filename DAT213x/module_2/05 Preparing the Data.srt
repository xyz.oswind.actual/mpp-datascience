0
00:00:01,280 --> 00:00:05,940
So we saw how we can use rxSummary on just one of the CSV files,

1
00:00:07,050 --> 00:00:09,980
in this case we use January

2
00:00:11,010 --> 00:00:13,370
to get a summary of the column called Paramount.

3
00:00:15,150 --> 00:00:18,070
And it takes a little bit of time to run on

4
00:00:18,070 --> 00:00:21,640
the CSV file compared to the XCF file, because of IO.

5
00:00:21,640 --> 00:00:26,030
We also saw how we can use rxSummary on

6
00:00:28,410 --> 00:00:34,179
the xdf file, so fare_amount, nyc_xdf and

7
00:00:34,179 --> 00:00:38,960
we can get the summary and this time, it's gonna be much faster even

8
00:00:38,960 --> 00:00:43,890
though the xdf file contains all six of the CSV files put together.

9
00:00:43,890 --> 00:00:46,210
Not just one of them.

10
00:00:46,210 --> 00:00:48,940
And the last example that we can see is that,

11
00:00:48,940 --> 00:00:52,640
we can use rxSummary just as before.

12
00:00:52,640 --> 00:01:00,852
But this time pointed to the nyc_sample_df.

13
00:01:01,930 --> 00:01:07,880
Which is the data frame with the top 1,000 rows of the January 2016 CSV

14
00:01:09,100 --> 00:01:14,960
file that we created in the beginning of this module.

15
00:01:16,060 --> 00:01:19,030
And so, rxSummary works in either of the cases.

16
00:01:19,030 --> 00:01:24,644
It understand data frames, it understands SDX,

17
00:01:24,644 --> 00:01:27,867
it understands flat files.

18
00:01:27,867 --> 00:01:32,300
So the choice between a data frame and

19
00:01:32,300 --> 00:01:36,340
a flat file or an XDF file is not a very complicated choice.

20
00:01:36,340 --> 00:01:40,320
A data frame works, it's great,

21
00:01:40,320 --> 00:01:44,560
but it's the traditional format that R uses it's a memory objects and

22
00:01:44,560 --> 00:01:47,190
because of that, we're always limited when we

23
00:01:47,190 --> 00:01:50,480
use data frame to the available memory that we have on our machine.

24
00:01:51,750 --> 00:01:56,400
On the other hand the choice between the CSP and the XTF

25
00:01:56,400 --> 00:02:00,100
comes down to the trade offs that we talked about in the last section.

26
00:02:00,100 --> 00:02:04,380
Because of that and because in this course, we're

27
00:02:04,380 --> 00:02:08,920
going to be doing a lot of data processing and data transformations.

28
00:02:08,920 --> 00:02:11,765
And then later, we're gonna run various analytical models

29
00:02:11,765 --> 00:02:16,070
we're gonna be doing to be using the XCF file.

30
00:02:16,070 --> 00:02:18,770
This means that, we pay the cost

31
00:02:18,770 --> 00:02:23,310
of converting the CSVs into an XDF format that we did earlier.

32
00:02:24,380 --> 00:02:28,061
But once the data is in XDF format we're gonna get faster run time,

33
00:02:28,061 --> 00:02:29,980
every time your process the data.

34
00:02:31,330 --> 00:02:36,270
And over time the faster run time is going to certainly

35
00:02:37,970 --> 00:02:41,070
give us improved run time overall.

