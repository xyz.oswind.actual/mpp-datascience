0
00:00:01,360 --> 00:00:03,010
In the last example,

1
00:00:03,010 --> 00:00:05,830
we saw what a simple transformation would look like.

2
00:00:07,250 --> 00:00:10,960
What I call a simple transformation is usually a transformation that you

3
00:00:10,960 --> 00:00:14,520
can write in one or two simple lines of R code.

4
00:00:14,520 --> 00:00:17,140
It's a pretty straight forward transformation and

5
00:00:17,140 --> 00:00:19,688
we saw that there is a pretty good use case for

6
00:00:19,688 --> 00:00:25,060
wanting to either write the transformation out to the data or

7
00:00:25,060 --> 00:00:27,010
perform the transformation on the fly.

8
00:00:27,010 --> 00:00:30,849
So that we can pass it directly to one of RevoScaleR's analytics

9
00:00:30,849 --> 00:00:31,580
functions.

10
00:00:31,580 --> 00:00:32,630
In this section,

11
00:00:32,630 --> 00:00:36,520
we're gonna look at more complex kinds of transformations.

12
00:00:37,660 --> 00:00:42,120
So once again, I don't really have a formal definition of complex here.

13
00:00:42,120 --> 00:00:45,010
What I mean by a complex transformation is gonna be more

14
00:00:45,010 --> 00:00:46,700
clear when we look at the example.

15
00:00:46,700 --> 00:00:50,690
But for now let's just think of it as a kind of transformation where

16
00:00:50,690 --> 00:00:53,100
a lot of things are happening and it's hard to

17
00:00:55,150 --> 00:01:00,480
write it out into one or two succinct lines of R code.

18
00:01:00,480 --> 00:01:03,510
Usually with the complex transformations,

19
00:01:03,510 --> 00:01:06,810
the transformation in question is a good candidate for

20
00:01:06,810 --> 00:01:10,030
the kind of transformation that we want to write out to the data.

21
00:01:10,030 --> 00:01:12,920
It could be the kind of transformation that takes longer

22
00:01:12,920 --> 00:01:14,070
to run.

23
00:01:14,070 --> 00:01:17,020
It could be the kind of transformation that's gonna require

24
00:01:17,020 --> 00:01:21,000
loading up a bunch of packages using some very specific functions

25
00:01:21,000 --> 00:01:23,780
out of those packages and so on.

26
00:01:23,780 --> 00:01:27,970
So hopefully by the end of this module, some of the distinction

27
00:01:27,970 --> 00:01:31,670
between more simple transformations that we can perform, and

28
00:01:31,670 --> 00:01:36,958
more complex transformations is gonna be a little bit more clear.

29
00:01:36,958 --> 00:01:40,880
So let's look look at an example of a complex transformation.

30
00:01:40,880 --> 00:01:44,010
And before I show you the overall architecture for

31
00:01:44,010 --> 00:01:48,810
this transformation let's actually look at some simple R code.

32
00:01:48,810 --> 00:01:52,200
So this doesn't have anything to do with RevoScaleR.

33
00:01:52,200 --> 00:01:56,030
We're just gonna use R to perform some kind of transformation.

34
00:01:56,030 --> 00:01:59,110
And then we're gonna take this transformation and wrap it

35
00:01:59,110 --> 00:02:02,792
inside of a function so that we can use RevoScaleR to leverage it.

36
00:02:04,340 --> 00:02:08,330
So the transformation, it's gonna require that we load the lubridate

37
00:02:08,330 --> 00:02:12,410
package, and then we have a bunch of objects here that we need to create

38
00:02:12,410 --> 00:02:15,810
ahead of time, cuz we're gonna use them as part of the transformation.

39
00:02:15,810 --> 00:02:18,743
So for now, kind of just go with it.

40
00:02:18,743 --> 00:02:22,550
And here I have a basic example.

41
00:02:23,630 --> 00:02:28,643
So, let's say we have one datetime value and in this case,

42
00:02:28,643 --> 00:02:33,657
I just hard coded it as one of the examples of how the datetime

43
00:02:33,657 --> 00:02:37,590
column pick up day time looks like in the data.

44
00:02:37,590 --> 00:02:41,750
And the first transformation is going to be the following.

45
00:02:41,750 --> 00:02:45,340
First, we're gonna use the hour function in lubridates,

46
00:02:45,340 --> 00:02:48,540
to extract the hour from pickup_datetime.

47
00:02:48,540 --> 00:02:53,620
So here, we can see that the hour for this datetime column is 16,

48
00:02:53,620 --> 00:02:56,860
which it comes straight from the hour there.

49
00:02:58,410 --> 00:03:06,300
We're then going to use the cut function inside of base R so

50
00:03:06,300 --> 00:03:13,830
that we can bin the hour based on certain bins that we predetermine.

51
00:03:13,830 --> 00:03:17,640
So let's run this and we can see what that looks like.

52
00:03:17,640 --> 00:03:21,330
So in this case, the bins come from cut levels, and

53
00:03:21,330 --> 00:03:24,720
cut levels is something that we created out there.

54
00:03:24,720 --> 00:03:30,459
So we're basically saying cut to let us know if the hour of the day falls

55
00:03:30,459 --> 00:03:35,617
between one and five, between five and nine, between nine and

56
00:03:35,617 --> 00:03:40,309
12, 12 and 16, 16 and 18, and 18 and 22.

57
00:03:40,309 --> 00:03:44,145
And in this case, because the hour of the day is 16,

58
00:03:44,145 --> 00:03:48,600
cut is telling us that that falls in the interval 12 to 16.

59
00:03:48,600 --> 00:03:52,640
Where the parenthesis means 12 itself is not included but

60
00:03:52,640 --> 00:03:56,870
the closing bracket mean that 16 is included.

61
00:03:58,210 --> 00:04:00,960
So, we bin the hour of the day, and

62
00:04:00,960 --> 00:04:05,490
for this single example this is the individual bin and

63
00:04:05,490 --> 00:04:10,700
we can confirm that cut is returning factors with the following levels.

64
00:04:12,590 --> 00:04:14,600
Okay, so far, so good.

65
00:04:14,600 --> 00:04:18,430
There is only one thing that we need to account for here.

66
00:04:19,520 --> 00:04:24,950
So, if these are the hours of the day and it's on a 24-hour scale

67
00:04:24,950 --> 00:04:29,850
we can see that any hour of the day between 10

68
00:04:29,850 --> 00:04:34,980
PM and 1 AM is not gonna be accounted for.

69
00:04:34,980 --> 00:04:38,310
What cut does in those cases, well, we can check that.

70
00:04:38,310 --> 00:04:43,460
We can change pickup_datetime to be 23 here.

71
00:04:43,460 --> 00:04:47,650
And then we can run cut and

72
00:04:47,650 --> 00:04:52,390
we can see that cut, returns an NA for those hours.

73
00:04:52,390 --> 00:04:57,360
But this NA is not exactly an NA the way that we think of missing data,

74
00:04:57,360 --> 00:05:01,350
this NA just represents another category namely,

75
00:05:01,350 --> 00:05:04,801
all of the hours that fell between 10 PM and 1 AM.

76
00:05:06,760 --> 00:05:11,880
So we can use the addNA function to make it so

77
00:05:11,880 --> 00:05:18,240
that NA is one of the valid factor levels for this function, okay.

78
00:05:18,240 --> 00:05:22,397
So we're gonna run that and what comes out of that is

79
00:05:22,397 --> 00:05:28,280
pickup_hour and now we can see the same NA as before.

80
00:05:28,280 --> 00:05:32,520
But if we look at the levels, we can see that the last level for

81
00:05:32,520 --> 00:05:36,360
this factor now does have an NA in it, okay?

82
00:05:36,360 --> 00:05:37,380
So for all intents and

83
00:05:37,380 --> 00:05:41,230
purposes, the NA's will be treated as a separate category.

84
00:05:41,230 --> 00:05:45,290
In fact, the fact the their named NA is just coincidental.

85
00:05:45,290 --> 00:05:47,980
And what we're gonna do is we're gonna give them better names

86
00:05:47,980 --> 00:05:51,870
instead of using those names for the factor levels.

87
00:05:51,870 --> 00:05:56,300
We're gonna rename them so that we can see this more appropriate

88
00:05:56,300 --> 00:06:00,170
names that are in the hour_labels object.

89
00:06:02,038 --> 00:06:05,375
So that's one transformation is we're gonna create a column called

90
00:06:05,375 --> 00:06:10,290
pickup_hour, and it's gonna be a column that tell us if for

91
00:06:10,290 --> 00:06:16,240
each taxi trip, the hour of the day was one of these categories.

92
00:06:18,040 --> 00:06:22,600
The next transformation is going to use the wday function

93
00:06:22,600 --> 00:06:24,600
to extract the day of the week.

94
00:06:25,770 --> 00:06:31,603
If we run an example of wday on the string that we have

95
00:06:31,603 --> 00:06:37,177
as our test string we can see that wday return 2.

96
00:06:37,177 --> 00:06:41,595
Now, it turns out that wday is gonna return an integer,

97
00:06:41,595 --> 00:06:47,130
where one is Sunday so two in this case is a Monday and so on.

98
00:06:47,130 --> 00:06:51,450
So, it's nice to get these integers if we wanna store them in the data.

99
00:06:51,450 --> 00:06:55,670
Integers can be efficient, but because we're thinking of the day of

100
00:06:55,670 --> 00:06:59,400
the week, the more appropriate column type is a factor.

101
00:06:59,400 --> 00:07:01,080
So we're gonna take these integers,

102
00:07:01,080 --> 00:07:03,660
we're gonna pass them over to the factor function.

103
00:07:03,660 --> 00:07:07,130
Set the levels to be 1 through 7 and we have to make sure,

104
00:07:07,130 --> 00:07:08,710
I've already checked the documentation.

105
00:07:08,710 --> 00:07:14,770
We have to make sure that 1 is a Sunday, not a Monday and

106
00:07:14,770 --> 00:07:18,470
that the mapping is going to be correct, okay?

107
00:07:18,470 --> 00:07:23,940
And so what comes out of that is a column called pickup day of week.

108
00:07:25,580 --> 00:07:29,670
And in this case we can see that pickup day of week is Monday and

109
00:07:29,670 --> 00:07:32,200
we can see the factor levels which are the days of the week.

110
00:07:34,180 --> 00:07:34,760
So that's it.

111
00:07:34,760 --> 00:07:38,360
So we did not just one transformation, but

112
00:07:38,360 --> 00:07:41,540
multiple transformations, in a way they're all related

113
00:07:41,540 --> 00:07:44,040
because they're all datetime transformations.

114
00:07:44,040 --> 00:07:47,618
They're relying on certain functions that are inside of lubridates.

115
00:07:47,618 --> 00:07:53,526
And we never wanna create too many of our own functions if there

116
00:07:53,526 --> 00:08:00,400
are efficient functions that we can already use and leverage.

117
00:08:00,400 --> 00:08:01,198
So in this case,

118
00:08:01,198 --> 00:08:04,621
we're gonna use the functionality that's built in with lubridates.

119
00:08:04,621 --> 00:08:07,151
And we're gonna use the code that we have here.

120
00:08:07,151 --> 00:08:09,672
And we're gonna make some modifications to it.

121
00:08:09,672 --> 00:08:14,263
And we're gonna pass it directly to RevoScaleR to

122
00:08:14,263 --> 00:08:19,000
perform a similar transformation on the xdf file.

123
00:08:19,000 --> 00:08:22,160
So, here is the transformation in question.

124
00:08:23,810 --> 00:08:26,770
It is wrapped inside of the function called xforms.

125
00:08:27,810 --> 00:08:31,040
And the important thing about this function is that it's going to

126
00:08:31,040 --> 00:08:32,740
be function of the data.

127
00:08:32,740 --> 00:08:37,680
That means that as it's input, we're going to be giving it the data.

128
00:08:37,680 --> 00:08:42,510
Now, because we never load the data in it's entirety into

129
00:08:42,510 --> 00:08:46,970
this transformation, we only load one chunk of the data at a time.

130
00:08:46,970 --> 00:08:49,708
You can think of this as one chunk of the data.

131
00:08:49,708 --> 00:08:54,500
Every time RevoScaleR is gonna run, one chunk of the data is

132
00:08:54,500 --> 00:08:57,820
gonna be brought in and it's gonna be handed down to this function.

133
00:08:57,820 --> 00:09:01,830
So that the transformation is going to happen on that chunk.

134
00:09:01,830 --> 00:09:03,882
When the transformation is done,

135
00:09:03,882 --> 00:09:06,570
we're either gonna write it out to the data or

136
00:09:06,570 --> 00:09:10,267
we're gonna use it to perform the transformation on the fly.

137
00:09:10,267 --> 00:09:12,947
And use the column to get some kind of summary or

138
00:09:12,947 --> 00:09:14,750
run some kind of analytics on.

139
00:09:16,470 --> 00:09:19,400
And then the rest of this is basically the code

140
00:09:19,400 --> 00:09:22,950
that we just covered in our little example.

141
00:09:22,950 --> 00:09:26,970
So here, we can see that we're loading the lubridate package,

142
00:09:26,970 --> 00:09:30,960
that we have some of these extraneous objects at the very top.

143
00:09:32,010 --> 00:09:36,330
That we are then running some transformations

144
00:09:36,330 --> 00:09:39,360
to extract pickup_hour and pickup day of week.

145
00:09:41,200 --> 00:09:44,946
We can see that, we have to first go to the data,

146
00:09:44,946 --> 00:09:48,999
take the column called tpep_pickup_datetime.

147
00:09:48,999 --> 00:09:53,198
And because this column is a character column in the data we use

148
00:09:53,198 --> 00:09:56,348
the ymd_hms function to convert it into a date

149
00:09:56,348 --> 00:10:00,405
before we can pass it down to these other transformations.

150
00:10:00,405 --> 00:10:03,211
Okay, so we have Three steps.

151
00:10:03,211 --> 00:10:07,948
One is converted to daytime, extract the hour of the day and

152
00:10:07,948 --> 00:10:10,890
categorize it, or bin it.

153
00:10:10,890 --> 00:10:15,450
Then extract the day of the week and give it the proper labels. Okay?

154
00:10:15,450 --> 00:10:17,370
And then that's it.

155
00:10:17,370 --> 00:10:21,800
We do the transformation once for the pick-up columns and

156
00:10:21,800 --> 00:10:24,980
we do the transformations a second time for the drop-off columns.

157
00:10:27,460 --> 00:10:29,590
Then we come to this here.

158
00:10:29,590 --> 00:10:31,580
Now, before I talk about what's going on here,

159
00:10:31,580 --> 00:10:34,580
let me actually run this function.

160
00:10:36,710 --> 00:10:42,590
So the function, with the function f the data and this is.

161
00:10:42,590 --> 00:10:47,220
In RevoScaleR, we think of that as the data being loaded into r

162
00:10:47,220 --> 00:10:50,120
one chunk at a time, and then handed down to this function.

163
00:10:51,250 --> 00:10:55,640
A bunch of things are happening to this function here at the very top,

164
00:10:55,640 --> 00:10:59,770
and when it finishes running we need to ask what are we returning.

165
00:11:01,570 --> 00:11:04,760
And so almost every time,

166
00:11:04,760 --> 00:11:08,520
what we're interested in returning is the data itself, right?

167
00:11:08,520 --> 00:11:09,650
We have a function,

168
00:11:09,650 --> 00:11:13,580
we have the data, we fit the data to the function.

169
00:11:13,580 --> 00:11:18,420
We make some transformations on the data, and these are the same

170
00:11:18,420 --> 00:11:22,460
sorts of transformations that we do with the data frame.

171
00:11:22,460 --> 00:11:26,570
And then we need to decide what we're going to return.

172
00:11:26,570 --> 00:11:31,920
Now this could be the original data in a modified form and that modified

173
00:11:31,920 --> 00:11:36,450
form usually means certain columns are going to be new so a new columns

174
00:11:36,450 --> 00:11:40,750
are gonna be created, certain columns are gonna be modified, so

175
00:11:40,750 --> 00:11:44,530
conversions could happen, rounding could happen and so on.

176
00:11:44,530 --> 00:11:49,110
But ultimately we have the data in this, transformed format

177
00:11:49,110 --> 00:11:52,810
that is going to be returned by the function as a new chunk.

178
00:11:52,810 --> 00:11:56,170
That chunk is gonna get written out and then we're gonna move on to

179
00:11:56,170 --> 00:11:58,150
the next chunk and we're gonna keep doing this.

180
00:11:58,150 --> 00:12:01,090
Of course the fact that this is happening chunk

181
00:12:01,090 --> 00:12:03,760
by chunk is completely abstracted away from us.

182
00:12:03,760 --> 00:12:06,910
We don't need to worry about the data at a chunk level.

183
00:12:06,910 --> 00:12:10,764
That will get taken care of by the RevoScaleR package but

184
00:12:10,764 --> 00:12:15,274
we still need to be able to have some amount of understanding about

185
00:12:15,274 --> 00:12:17,734
what is going on behind the scenes.

186
00:12:17,734 --> 00:12:21,607
So that we can be careful about certain kinds of transformations

187
00:12:21,607 --> 00:12:27,210
that are tricky when data is chunked up like this. Okay.

188
00:12:27,210 --> 00:12:28,130
So in this case

189
00:12:29,210 --> 00:12:33,470
what we're gonna do with the data is we're going to return the data, but

190
00:12:33,470 --> 00:12:38,210
before returning the data we're gonna take pickup_hour, pickup_dow,

191
00:12:38,210 --> 00:12:41,760
dropoff_hour, dropoff_dow, the four columns that we created

192
00:12:41,760 --> 00:12:45,050
in the earlier steps there and we're gonna append them to the data,

193
00:12:45,050 --> 00:12:47,020
we're gonna attach them as new columns to the data.

194
00:12:48,050 --> 00:12:50,500
And then we have one last transformation that we're gonna

195
00:12:50,500 --> 00:12:55,610
perform there that is going to calculate trip duration.

196
00:12:55,610 --> 00:13:00,290
And the reason we wanna do it there is because pickup_datetime and

197
00:13:00,290 --> 00:13:04,100
dropoff_datetime are datetime columns and

198
00:13:04,100 --> 00:13:06,820
we're not interested in keeping them around, so

199
00:13:06,820 --> 00:13:10,300
not every column transformation here is a transformation that we're going

200
00:13:10,300 --> 00:13:13,960
to stick around when the function finishes running.

201
00:13:13,960 --> 00:13:17,430
Some of it is just temporary transformations.

202
00:13:17,430 --> 00:13:21,310
And so pick up day time and drop off day time are being converted into

203
00:13:21,310 --> 00:13:26,490
day time columns, so that we can extract hour and day of the week,

204
00:13:26,490 --> 00:13:30,960
from them, and here we're going to use the difference between drop off

205
00:13:30,960 --> 00:13:34,261
day time and pick up day time to find the trip duration, and

206
00:13:34,261 --> 00:13:38,760
we keep our end day of week and we keep duration.

207
00:13:38,760 --> 00:13:41,408
But we don't necessarily keep dropoff_datetime and

208
00:13:41,408 --> 00:13:43,880
pickup_datetime in this case, all right?

209
00:13:43,880 --> 00:13:47,180
And the reason is because they are easier to derive from the data.

210
00:13:47,180 --> 00:13:50,440
We have a column that has the datetime stored in it.

211
00:13:50,440 --> 00:13:56,420
It's just that, right now, it's stored as a character column so

212
00:13:56,420 --> 00:14:01,570
if we need to do the sought of transmission very often, if we need

213
00:14:01,570 --> 00:14:04,520
the daytime column to actually be stored as a daytime column

214
00:14:04,520 --> 00:14:08,220
it might be a good idea for us to just perform the transformation,

215
00:14:08,220 --> 00:14:09,430
write it out to the data.

216
00:14:09,430 --> 00:14:12,150
So now the column in the data is a date

217
00:14:12,150 --> 00:14:14,670
time column instead of a character column.

218
00:14:14,670 --> 00:14:18,250
But here the assumption is that we have it as a character column,

219
00:14:18,250 --> 00:14:22,710
we're gonna use it to extract a bunch of date time related columns,

220
00:14:22,710 --> 00:14:27,310
such as hour of the day, day of the week, or the trip duration.

221
00:14:27,310 --> 00:14:31,180
And once we have those columns we don't really care if the original

222
00:14:31,180 --> 00:14:33,880
column is in character or if it's a date time column.

223
00:14:36,680 --> 00:14:39,660
So, that's it, that’s the transformation and

224
00:14:39,660 --> 00:14:44,920
it's stored inside of the logic for this function called xforms.

225
00:14:44,920 --> 00:14:45,420
Right?

226
00:14:46,620 --> 00:14:50,400
Everything up to this point has really nothing to do

227
00:14:50,400 --> 00:14:52,250
with RevoScaleR.

228
00:14:52,250 --> 00:14:55,698
It's all basically the way that we would write functions of r.

229
00:14:55,698 --> 00:15:01,280
The only RevoScaleR part of it is we have to be aware of how this

230
00:15:01,280 --> 00:15:05,930
function is, has the data as it's input and how it's going to return

231
00:15:05,930 --> 00:15:09,810
the data in some kind of modified form usually as it's output.

232
00:15:11,520 --> 00:15:17,020
Now we're gonna go and run this transmission and

233
00:15:17,020 --> 00:15:18,750
this is where we wanna be really careful.

234
00:15:20,180 --> 00:15:25,530
So in the early part of the course,

235
00:15:25,530 --> 00:15:30,110
we took the first 1000 rows of the data,

236
00:15:30,110 --> 00:15:33,670
of one of the CSV files in the data and

237
00:15:33,670 --> 00:15:37,990
we dumped it into a dataframe called NYC sample DF.

238
00:15:37,990 --> 00:15:40,500
And I mentioned back then that

239
00:15:40,500 --> 00:15:44,370
we're gonna just keep this data frame in our current r session so,

240
00:15:44,370 --> 00:15:47,630
that if we need to test some of our transformations,

241
00:15:47,630 --> 00:15:50,940
we can first test them on the data frame to make sure that

242
00:15:50,940 --> 00:15:55,070
there isn't anything wrong with the transformation itself before we move

243
00:15:55,070 --> 00:15:59,360
on and deploy that transformation to the large XDF file right.

244
00:15:59,360 --> 00:16:04,650
And this intermediate step is a very highly recommended step because it

245
00:16:04,650 --> 00:16:10,620
make it so that were less likely to make mistakes when writing

246
00:16:10,620 --> 00:16:14,320
the transformation when applying the transformation to a large XDF file.

247
00:16:14,320 --> 00:16:16,760
Remember that by assumption in XDF file

248
00:16:18,000 --> 00:16:20,360
Is going to be a large data set.

249
00:16:20,360 --> 00:16:24,700
If we do a transformation on the data and

250
00:16:24,700 --> 00:16:28,780
that transformation turns out to be the long transformation because we

251
00:16:28,780 --> 00:16:32,130
overlooked something because our function was suppose

252
00:16:32,130 --> 00:16:36,350
to do something and there was an error and we didn't catch it.

253
00:16:36,350 --> 00:16:40,450
Now we are going to pay two costs.

254
00:16:40,450 --> 00:16:44,220
One is we're gonna pay the cost of running the transmission on the data

255
00:16:44,220 --> 00:16:46,170
which could take a while, and

256
00:16:46,170 --> 00:16:49,670
it's gonna be a while before we discover that something went wrong.

257
00:16:49,670 --> 00:16:55,690
And the second one is that we're not

258
00:16:55,690 --> 00:16:59,530
If we get an error we're not gonna know is it maybe

259
00:16:59,530 --> 00:17:03,320
because of the XDF file, is it maybe because something with the data or

260
00:17:03,320 --> 00:17:07,160
is it more because I don't have the right transformation function?

261
00:17:07,160 --> 00:17:09,520
Maybe I made a mistake in my R code.

262
00:17:09,520 --> 00:17:14,270
So it's important to have this sort of in between step

263
00:17:14,270 --> 00:17:18,910
in which we isolate The R code, we run it.

264
00:17:18,910 --> 00:17:20,460
We make sure that it's working.

265
00:17:21,530 --> 00:17:24,270
And when we've done that, we can then go

266
00:17:24,270 --> 00:17:28,140
to the step of running the transformation on the XDF file.

267
00:17:28,140 --> 00:17:30,070
And we can be reasonably sure.

268
00:17:30,070 --> 00:17:31,870
We can never be a 100% sure, but

269
00:17:31,870 --> 00:17:34,390
we can be reasonably sure that if the transformation work

270
00:17:34,390 --> 00:17:39,200
in a dataframe That it's going to work on the xdf file as well.

271
00:17:39,200 --> 00:17:40,360
So let's go and

272
00:17:40,360 --> 00:17:43,350
run this transformation first on the data frame.

273
00:17:44,940 --> 00:17:49,870
So, as you can see here, here is our data frame nyc_sample_df.

274
00:17:49,870 --> 00:17:53,550
And because our transformation function has, takes in the data as

275
00:17:53,550 --> 00:17:57,360
its input, we can just pass them directly here to the x forms.

276
00:17:57,360 --> 00:18:00,700
Okay, and then because we don't want to see all of the results,

277
00:18:00,700 --> 00:18:03,840
we're going to use head to just look at the top six rows.

278
00:18:04,900 --> 00:18:09,780
And so we can see the original data back but if we scroll down

279
00:18:09,780 --> 00:18:14,640
we can see the columns pick up hour, pick up day of week,

280
00:18:14,640 --> 00:18:17,950
drop off hour, drop off day of week and trip duration.

281
00:18:17,950 --> 00:18:20,639
Okay so the transformation seems to be working.

282
00:18:21,690 --> 00:18:26,050
Now we're gonna go and take one other cautionary step

283
00:18:26,050 --> 00:18:30,010
before we actually run the transformation on the xdf file.

284
00:18:30,010 --> 00:18:32,340
We're gonna keep the data frame but

285
00:18:32,340 --> 00:18:35,330
instead of performing the transformation by

286
00:18:35,330 --> 00:18:39,960
passing it directly to the function, we're gonna use rxDataStep.

287
00:18:39,960 --> 00:18:45,200
Recall that rxDataStep works with a data frame Connects the f file or

288
00:18:45,200 --> 00:18:46,670
some flat file.

289
00:18:46,670 --> 00:18:50,460
So we can pass the data frame to rxDataStep.

290
00:18:50,460 --> 00:18:53,430
The only difference is that when we have a data frame,

291
00:18:53,430 --> 00:18:57,090
we don't need to specify the out file argument,

292
00:18:57,090 --> 00:19:00,030
because we're not writing it out to an external file.

293
00:19:00,030 --> 00:19:02,400
We might be interested in doing so, but in this case we're not.

294
00:19:02,400 --> 00:19:05,010
We have a data frame and we're going to transform it and

295
00:19:05,010 --> 00:19:07,530
we want the result to be Just another data frame.

296
00:19:08,880 --> 00:19:13,390
So we specify the input data frame, we don't have an output data

297
00:19:15,610 --> 00:19:18,020
and this is where the transformation is happening.

298
00:19:18,020 --> 00:19:21,420
So we have an argument called transformFunc and

299
00:19:21,420 --> 00:19:24,300
transformFunc expects a function

300
00:19:24,300 --> 00:19:27,830
Like the function that we just created in the previous step.

301
00:19:27,830 --> 00:19:30,270
So here we specify xForms.

302
00:19:30,270 --> 00:19:35,340
And then here's something that we visited a little bit earlier and

303
00:19:35,340 --> 00:19:37,200
we're revisiting it here.

304
00:19:37,200 --> 00:19:41,310
The transformation function xForms

305
00:19:41,310 --> 00:19:44,410
requires the lubridate package in order to run.

306
00:19:45,450 --> 00:19:51,000
And even though in open source R we usually just put

307
00:19:51,000 --> 00:19:55,970
a require argument at the top of the function and then everything works.

308
00:19:55,970 --> 00:20:01,550
In RevoScaleR we need to be a little bit more careful and a little

309
00:20:01,550 --> 00:20:06,140
bit more explicit about packages and where they're coming from.

310
00:20:06,140 --> 00:20:09,400
So in this case we have to explicitly specify that this

311
00:20:09,400 --> 00:20:16,080
transformation relies on the lubridate package and as a reminder,

312
00:20:16,080 --> 00:20:20,900
this more rigid architecture has to do with the fact that we

313
00:20:20,900 --> 00:20:27,830
occasionally want to deploy this code on Hadoop clusters or an R

314
00:20:27,830 --> 00:20:31,690
server that is other than the single machine that we're running it here.

315
00:20:33,840 --> 00:20:36,530
So I'm gonna run the transmission again and

316
00:20:36,530 --> 00:20:41,530
notice that just as before, we can see the data frame coming out and

317
00:20:41,530 --> 00:20:46,070
if we scroll down, we can see our new columns created right there.

318
00:20:46,070 --> 00:20:50,070
Okay, so, the transformation seems to be working.

319
00:20:50,070 --> 00:20:54,990
It's time for us to go and deploy this transformation to the XDF file.

320
00:20:57,040 --> 00:20:58,710
And in doing so, we're also gonna time it.

321
00:20:59,900 --> 00:21:04,917
So, notice that if we take this intermediate step, then performing

322
00:21:04,917 --> 00:21:10,480
the transformation on the XDF file is pretty straightforward.

323
00:21:10,480 --> 00:21:13,750
Everything in here is going to be the same, except for

324
00:21:13,750 --> 00:21:16,550
two changes highlighted here.

325
00:21:16,550 --> 00:21:18,630
First, we have an out file.

326
00:21:18,630 --> 00:21:23,734
So here, we are reading from nyc_xdf and we're writing back to nyc_xdf

327
00:21:25,040 --> 00:21:29,330
So that's the first change, and because the out file is the same as

328
00:21:29,330 --> 00:21:33,850
the input file, we have to specify over ride equals true as usual.

329
00:21:33,850 --> 00:21:37,120
Otherwise, transformfunc is

330
00:21:37,120 --> 00:21:40,560
showing up the same way that it did in the prior call.

331
00:21:40,560 --> 00:21:42,940
And transformed packages,

332
00:21:42,940 --> 00:21:46,260
is throwing up the same way that it did in the prior call.

333
00:21:46,260 --> 00:21:49,320
The transformation is now over and we can see that it took

334
00:21:49,320 --> 00:21:53,950
approximately 40 seconds for the transformation to happen.

