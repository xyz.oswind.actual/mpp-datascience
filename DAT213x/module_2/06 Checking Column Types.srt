0
00:00:00,120 --> 00:00:01,800
So moving forward,

1
00:00:01,800 --> 00:00:05,830
I'm gonna use the revo scalar function only on XDF files.

2
00:00:06,830 --> 00:00:10,530
Just keep in mind that you can use the function,

3
00:00:10,530 --> 00:00:12,365
that work a learn about in this course.

4
00:00:12,365 --> 00:00:14,860
RX summary is the example that we covered so

5
00:00:14,860 --> 00:00:19,270
far, but any of other function that were gonna be using directly

6
00:00:19,270 --> 00:00:24,290
on the flat files themselves without having to convert into XDF.

7
00:00:24,290 --> 00:00:28,035
Assuming that we understand the trade offs that are involved.

8
00:00:28,035 --> 00:00:30,936
So the first function that I'm gonna run,

9
00:00:30,936 --> 00:00:35,447
to just take a look at the data the XDF file that we created is gonna be

10
00:00:35,447 --> 00:00:37,400
the Rx get in full function.

11
00:00:39,070 --> 00:00:43,390
By now we should see a common trend, a lot of legal scalar functions in

12
00:00:43,390 --> 00:00:49,020
fact almost all the legal scalar functions have this RX prefix.

13
00:00:49,020 --> 00:00:51,080
So this is one way that we can identify them.

14
00:00:52,325 --> 00:00:58,580
rxGetInfo, in this case, is going to take the XDF file as it's inputs and

15
00:00:58,580 --> 00:01:03,530
then if we wanna get Information about column types something

16
00:01:03,530 --> 00:01:07,730
I can to what the STR function and open source are gives us.

17
00:01:07,730 --> 00:01:11,210
We can use getVarInfo = TRUE as an argument.

18
00:01:11,210 --> 00:01:14,438
And then, we can also specify the number of rows of the data that we'd

19
00:01:14,438 --> 00:01:19,300
like to see, so at the very top we see some information about the data,

20
00:01:19,300 --> 00:01:22,380
such as the number of rows, the number of columns.

21
00:01:22,380 --> 00:01:25,860
Some information about the way the data is being stored into blocks and

22
00:01:25,860 --> 00:01:29,280
the kind of compression that is being used on the data.

23
00:01:29,280 --> 00:01:32,640
Then, because we have getVarInfo = TRUE,

24
00:01:32,640 --> 00:01:35,050
we can see some information about our column types.

25
00:01:36,120 --> 00:01:37,380
We can see, for example,

26
00:01:37,380 --> 00:01:42,130
that the column called payment_type is a factor column with four levels

27
00:01:42,130 --> 00:01:45,370
and in this case, the four levels represented by those integers.

28
00:01:46,430 --> 00:01:48,347
And if we keep scrolling down,

29
00:01:48,347 --> 00:01:52,105
we can see the first ten rows of the data the same way that it had

30
00:01:52,105 --> 00:01:55,599
functioned would show us the first ten rows of the data.

