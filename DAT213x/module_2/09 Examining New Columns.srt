0
00:00:01,530 --> 00:00:04,740
We saw an example of a simple transformation an then,

1
00:00:04,740 --> 00:00:07,050
we saw an example of a complex transformation.

2
00:00:07,050 --> 00:00:11,020
In the case of the simple transformation we wrote it

3
00:00:11,020 --> 00:00:14,570
as a simple onliner passed it to the transforms

4
00:00:15,800 --> 00:00:19,720
argument and ran the transmission.

5
00:00:19,720 --> 00:00:22,640
In the case of the complex transmission we wrapped

6
00:00:22,640 --> 00:00:24,600
it inside of an r function and

7
00:00:24,600 --> 00:00:29,720
passed it to the transform func argument to run the transmission.

8
00:00:31,320 --> 00:00:36,390
In this step we're gonna now go and use some summary functions to

9
00:00:38,050 --> 00:00:41,070
visualize our column transformations and

10
00:00:41,070 --> 00:00:45,080
see if we can quickly get a sense of how they look.

11
00:00:46,250 --> 00:00:49,330
The thing that I wanna specify in this section is that

12
00:00:50,780 --> 00:00:55,820
there is a point where some of the code that we're gonna use in

13
00:00:55,820 --> 00:01:01,100
RevoScaleR and some of the code that we're gonna be using from just base

14
00:01:01,100 --> 00:01:05,010
R are gonna really come together and they're gonna get mixed up.

15
00:01:05,010 --> 00:01:08,650
And this is not unusual, at the end of the day,

16
00:01:08,650 --> 00:01:11,370
RevoScaleR is just another R package.

17
00:01:11,370 --> 00:01:14,700
And that means for us to be able to learn to use it well,

18
00:01:14,700 --> 00:01:16,760
we have to know our R pretty well.

19
00:01:16,760 --> 00:01:18,260
We have to know our data types,

20
00:01:18,260 --> 00:01:23,810
we have to know how to drill into complicated nested lists and so on.

21
00:01:23,810 --> 00:01:28,330
And so we have some examples of that in this section.

22
00:01:28,330 --> 00:01:33,196
But it's important to emphasize that this sort of gray area of

23
00:01:33,196 --> 00:01:37,127
knowing where RevoScaleR is going to leave off and

24
00:01:37,127 --> 00:01:41,780
where Base R is gonna pick it back up or vice versa.

25
00:01:41,780 --> 00:01:46,110
That's a grey area that we become more and more familiar with,

26
00:01:46,110 --> 00:01:51,450
more and more comfortable with as we program more and

27
00:01:51,450 --> 00:01:54,130
more in RevoScaleR and understand its architecture better.

28
00:01:55,660 --> 00:01:58,760
So Let's go ahead and

29
00:01:58,760 --> 00:02:03,760
look at some summarizations for the columns that we just created.

30
00:02:04,980 --> 00:02:08,800
Here we're back with the RX summary function, and we're gonna ask for

31
00:02:08,800 --> 00:02:10,000
the following summaries.

32
00:02:10,000 --> 00:02:13,940
We wanna ask for summaries of pickup hour, pickup day of week, and

33
00:02:13,940 --> 00:02:14,670
trip duration.

34
00:02:15,700 --> 00:02:18,780
Notice that we're using, once again, the formula notation.

35
00:02:18,780 --> 00:02:22,500
And here we have pluses separating the individual columns.

36
00:02:22,500 --> 00:02:26,480
So we're gonna get individual summaries for each of these columns.

37
00:02:27,630 --> 00:02:33,410
So let's run that, and the result is stored in an object called rxs1.

38
00:02:35,430 --> 00:02:39,740
If we look at our excess one we can see everything shown here.

39
00:02:39,740 --> 00:02:43,720
We can see the summary for trip duration here and because

40
00:02:43,720 --> 00:02:48,300
trip duration is a numeric column we're seeing numeric summaries.

41
00:02:49,420 --> 00:02:53,960
We can scroll down a little bit and then see summaries for pick up hour.

42
00:02:53,960 --> 00:02:58,560
Now pick up hour is a factor, and because of that the only summary

43
00:02:58,560 --> 00:03:02,800
that makes sense just right off the bat is gonna be counts.

44
00:03:02,800 --> 00:03:07,060
Same thing with pick up day of week okay.

45
00:03:07,060 --> 00:03:12,490
So r x x 1 Is this object that has our summaries.

46
00:03:12,490 --> 00:03:13,890
But occasionally,

47
00:03:13,890 --> 00:03:18,630
we might be interested in going inside of this object,

48
00:03:18,630 --> 00:03:23,570
drilling inside of this object, and adding some bits and pieces to it.

49
00:03:23,570 --> 00:03:28,650
So one interesting example would be, well, we have counts here for

50
00:03:28,650 --> 00:03:33,350
pick up hour, but it would be nice to also have proportions.

51
00:03:33,350 --> 00:03:35,120
How can we get proportions?

52
00:03:35,120 --> 00:03:39,480
Well rx summary doesn't give us the proportions but

53
00:03:39,480 --> 00:03:44,510
that's because rx summary is just basically giving us the essentials.

54
00:03:44,510 --> 00:03:50,610
And if we wanna add to it we can go back to the base r functions that

55
00:03:50,610 --> 00:03:54,970
we're familiar with or any third party packages that we know of and

56
00:03:54,970 --> 00:03:57,820
we can start manipulating this object and we can start

57
00:03:59,170 --> 00:04:03,280
handling it to get in this case to proportions added to it.

58
00:04:03,280 --> 00:04:08,675
So the way we gonna do that is first of all the proportions only make

59
00:04:08,675 --> 00:04:13,985
sense for the factor columns and so as it turns out we can type

60
00:04:13,985 --> 00:04:20,130
rxs1$ and already we can see that there are different

61
00:04:20,130 --> 00:04:25,790
sorts of information stored in this rsx1 object.

62
00:04:25,790 --> 00:04:29,810
There is one for example that is called S data frame,

63
00:04:29,810 --> 00:04:31,330
if we look at S data frame.

64
00:04:31,330 --> 00:04:34,194
We can see the numeric summaries for trip duration here.

65
00:04:34,194 --> 00:04:37,200
And then for pickup_hour and

66
00:04:37,200 --> 00:04:41,400
pickup_dow being factors we don't see any numeric summaries.

67
00:04:41,400 --> 00:04:43,730
But we can see the valid observations and

68
00:04:43,730 --> 00:04:45,880
we can see if there were any missing observations.

69
00:04:47,260 --> 00:04:52,230
Similarly there's an object called rxs1 Categorical.

70
00:04:53,250 --> 00:04:57,030
And if we look at our exs1categorical, we can see that

71
00:04:57,030 --> 00:05:01,970
our exs1categorical is a list and each element of that

72
00:05:01,970 --> 00:05:07,170
list is gonna contain the counts for one of the categorical columns,

73
00:05:07,170 --> 00:05:10,110
one of the factors that we asked to have the summaries for.

74
00:05:11,730 --> 00:05:17,800
So with a little bit of R magic using the lapply function we can go

75
00:05:17,800 --> 00:05:24,500
to each of these elements and we can then use the prop.table function

76
00:05:24,500 --> 00:05:29,910
to turn the counts into proportions and round them to only two decimals.

77
00:05:29,910 --> 00:05:32,790
So I'm gonna run this code And

78
00:05:32,790 --> 00:05:36,910
now when we look at our excess fund we can see that in addition

79
00:05:36,910 --> 00:05:40,460
to the accounts, we also have proportions, right?

80
00:05:40,460 --> 00:05:44,180
So this is the place where we can see sort of the interplay between

81
00:05:45,380 --> 00:05:49,920
using one of the reveal scale R packages obtaining a summary.

82
00:05:49,920 --> 00:05:53,360
Ultimately that summary is just like any R objects,

83
00:05:53,360 --> 00:05:56,820
it's gonna be some kind of Object that we can drill into,

84
00:05:56,820 --> 00:06:01,780
that we can reshape, reformat that we can add to or subtract from.

85
00:06:02,800 --> 00:06:07,040
And in this case we used it knowing that it was a list to go and

86
00:06:07,040 --> 00:06:09,920
add proportions to the accounts.

87
00:06:09,920 --> 00:06:14,830
To do that we basically just rely it on the base R functions or

88
00:06:14,830 --> 00:06:17,060
other R functions that we're familiar with.

89
00:06:18,400 --> 00:06:20,380
Here's another example of that.

90
00:06:20,380 --> 00:06:22,460
We're gonna use rxSummary a second time,

91
00:06:22,460 --> 00:06:27,460
and this time this is the summary that we're asking for.

92
00:06:27,460 --> 00:06:31,100
So we want a summary of pick up day of week and pick up hour, but

93
00:06:31,100 --> 00:06:35,440
notice that the two columns are separated not by a plus anymore but

94
00:06:35,440 --> 00:06:36,960
by a colon.

95
00:06:36,960 --> 00:06:40,530
Because of that colon, we're gonna be getting interactions between

96
00:06:40,530 --> 00:06:47,360
the two columns So the resulting object is called rxx2 and

97
00:06:47,360 --> 00:06:52,720
if we look at it right now we can see that it is a data frame.

98
00:06:52,720 --> 00:06:54,955
We have pick-up day of week and

99
00:06:54,955 --> 00:06:59,940
pick-up hour, every row of this data frame is gonna be a combination

100
00:06:59,940 --> 00:07:04,020
of pick-up day of week and pick-up hour Hence the interaction.

101
00:07:04,020 --> 00:07:06,610
And then we can see a column for the counts.

102
00:07:08,170 --> 00:07:12,370
We can now use the spread

103
00:07:12,370 --> 00:07:16,140
function inside of tidyr to reshape this data frame.

104
00:07:16,140 --> 00:07:19,240
Right now it's in what's called a long format.

105
00:07:19,240 --> 00:07:22,800
With spread we can turn it into a wide format.

106
00:07:24,250 --> 00:07:28,230
And then with some further manipulation, we can turn

107
00:07:28,230 --> 00:07:33,400
that into a matrix when we're done the result looks like this.

108
00:07:33,400 --> 00:07:36,680
So it's the same information as before, right.

109
00:07:36,680 --> 00:07:39,240
It's the same information that we had in a data frame that we're

110
00:07:39,240 --> 00:07:43,290
looking at earlier but now it's been re-shaped to a wide format and

111
00:07:43,290 --> 00:07:47,950
it's been turn into something that looks more like a matrix.

112
00:07:47,950 --> 00:07:51,670
And we can see that each cell in the matrix, each entry in the matrix,

113
00:07:51,670 --> 00:07:52,680
is one of the counts.

114
00:07:54,360 --> 00:08:00,030
Okay, so the useful thing however about having it as a matrix

115
00:08:00,030 --> 00:08:03,854
is that we can go back now to using prop.table and

116
00:08:03,854 --> 00:08:07,870
prop.table understands this matrix object as well as

117
00:08:09,300 --> 00:08:13,890
a Flood vector and by specifying the second

118
00:08:13,890 --> 00:08:18,130
argument to be a two, we can say give me proportions so

119
00:08:18,130 --> 00:08:22,600
that for each column the proportion add up to a 100%.

120
00:08:22,600 --> 00:08:25,430
So in this case we're gonna get proportions but

121
00:08:25,430 --> 00:08:27,360
for each time of the day.

122
00:08:28,910 --> 00:08:32,340
1AM to 5AM, 5AM to 9AM, and so on.

123
00:08:32,340 --> 00:08:34,375
The proportions are gonna add up to a 100%.

124
00:08:35,720 --> 00:08:37,880
So let's see what that looks like.

125
00:08:37,880 --> 00:08:44,500
If we just run this section of the code we can see proportions here.

126
00:08:44,500 --> 00:08:49,320
And so, the interpretation would be something like this if I just select

127
00:08:49,320 --> 00:08:50,240
this number there.

128
00:08:51,370 --> 00:08:55,620
The interpretation is that of the taxi trips

129
00:08:55,620 --> 00:08:59,880
between the hours of 1 AM to 5 AM, about 7%

130
00:08:59,880 --> 00:09:04,380
happened on a Tuesday as opposed to any other day of the week.

131
00:09:06,460 --> 00:09:09,230
Instead of looking at the proportions individually, however,

132
00:09:09,230 --> 00:09:11,810
we're gonna use the level plot function.

133
00:09:13,160 --> 00:09:14,190
To just visualize them.

134
00:09:15,570 --> 00:09:17,690
So, I just run the visualization for

135
00:09:17,690 --> 00:09:20,710
the plot and it's showing up down there.

136
00:09:20,710 --> 00:09:24,620
But what I'm gonna do is make it a little bit larger so

137
00:09:24,620 --> 00:09:26,560
that it's easier to see.

138
00:09:26,560 --> 00:09:29,890
And so, here's the visualization of the level plots, and

139
00:09:29,890 --> 00:09:32,660
we can see some interesting results on this plot.

140
00:09:32,660 --> 00:09:37,550
We can see for example, that there is a spike In the number of

141
00:09:37,550 --> 00:09:42,700
trips happening on a Sunday between 1 AM and 5 AM.

142
00:09:43,790 --> 00:09:46,400
So this is actually more,

143
00:09:46,400 --> 00:09:51,200
I guess more accurate to call it the Saturday night late crowd.

144
00:09:51,200 --> 00:09:53,930
So these are people who probably went out on a Saturday night and

145
00:09:53,930 --> 00:09:55,810
they stayed past midnight.

146
00:09:55,810 --> 00:10:00,380
And now it's Sunday and they're probably going home.

147
00:10:00,380 --> 00:10:02,580
We can see a similar spike for Saturday.

148
00:10:02,580 --> 00:10:04,860
So these are the people who went out on a Friday and

149
00:10:04,860 --> 00:10:07,900
they stayed past midnight and now they're going home.

150
00:10:07,900 --> 00:10:12,884
We can also see another spike here between 10 PM and 1 AM for

151
00:10:12,884 --> 00:10:15,817
Saturday going into Sunday and so on.

152
00:10:15,817 --> 00:10:21,480
Just like we have spikes we can also see Dips so we can see that

153
00:10:21,480 --> 00:10:27,300
late night taxi traffic definitely starts dipping down during

154
00:10:27,300 --> 00:10:32,060
working days of the week and so on.

155
00:10:32,060 --> 00:10:36,488
So Levelplot was used to turn the proportions into a plot that kind

156
00:10:36,488 --> 00:10:40,757
of, based on some preliminary glance, seems to make, more or

157
00:10:40,757 --> 00:10:42,545
less, intuitive sense.

