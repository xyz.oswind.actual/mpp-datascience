0
00:00:05,663 --> 00:00:07,060
Welcome back.

1
00:00:07,060 --> 00:00:09,626
Now that we have the R client,

2
00:00:09,626 --> 00:00:14,452
we have the necessary R packages, and we have the data,

3
00:00:14,452 --> 00:00:19,497
we're ready to start running our R code against the data.

4
00:00:19,497 --> 00:00:24,969
In this module, we're gonna learn how we can read the data into R,

5
00:00:24,969 --> 00:00:30,847
and we're gonna spend quite a bit of time doing data transformations.

6
00:00:30,847 --> 00:00:34,616
The goal of the data transformation is gonna be to get the data ready

7
00:00:34,616 --> 00:00:35,525
for analysis.

8
00:00:35,525 --> 00:00:39,573
We're gonna start with some basic data transformation tasks, and

9
00:00:39,573 --> 00:00:43,777
we're gonna slowly make our way up to more complex transformations.

10
00:00:43,777 --> 00:00:49,286
Learning in the process about the architecture that RevoScaleR uses,

11
00:00:49,286 --> 00:00:51,120
to leverage our R code,

12
00:00:51,120 --> 00:00:55,267
to perform complicated data transformation tasks.

