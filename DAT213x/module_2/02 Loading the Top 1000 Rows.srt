0
00:00:00,950 --> 00:00:04,560
So we're gonna go and load our data into R.

1
00:00:04,560 --> 00:00:06,890
First thing I'm gonna do is hit Control L, so

2
00:00:06,890 --> 00:00:08,230
that we can clear the console.

3
00:00:09,860 --> 00:00:16,290
And before we use reverse scale R to load the data into R,

4
00:00:16,290 --> 00:00:20,410
we're gonna use an open source R function.

5
00:00:20,410 --> 00:00:24,821
Namely read.csv to load the first

6
00:00:24,821 --> 00:00:29,620
1,000 rows of the data as a data frame into R.

7
00:00:31,140 --> 00:00:36,490
There's going to be multiple reasons for us doing this.

8
00:00:36,490 --> 00:00:41,310
One is occasionally, we're gonna draw some parallels between things

9
00:00:41,310 --> 00:00:46,091
in open source R and things the way things are done in revo scale R.

10
00:00:47,200 --> 00:00:50,960
But the even more important reason for

11
00:00:50,960 --> 00:00:54,420
us doing this is in this chapter, in this module,

12
00:00:54,420 --> 00:00:59,268
we're gonna be focused a lot on doing transformations.

13
00:00:59,268 --> 00:01:03,275
And running transmissions in revo scale R is a little bit more

14
00:01:03,275 --> 00:01:05,413
involved than in Open Source R.

15
00:01:05,413 --> 00:01:10,186
So, we're gonna have a data frame that we're gonna keep around so

16
00:01:10,186 --> 00:01:14,534
that we can test our transmissions on the data frame before we

17
00:01:14,534 --> 00:01:18,030
deploy the transformation to our big data in revo

18
00:01:18,030 --> 00:01:22,806
scale R Before

19
00:01:22,806 --> 00:01:27,282
running read.csv I have an object called call

20
00:01:27,282 --> 00:01:32,219
Classes which contains the column information for

21
00:01:32,219 --> 00:01:38,680
each of the column types for each of the columns in the data.

22
00:01:38,680 --> 00:01:42,360
So, here for example, the column vendor ID is gonna be a factor and

23
00:01:42,360 --> 00:01:44,440
so on.

24
00:01:44,440 --> 00:01:48,190
The next thing I do, is I point to the data and

25
00:01:48,190 --> 00:01:51,000
because our working directory is already set we

26
00:01:51,000 --> 00:01:53,990
don't have to specify the absolute path of the data.

27
00:01:53,990 --> 00:01:55,830
We can just specify the relative path.

28
00:01:57,080 --> 00:02:02,075
And this is where we use the read.csv function to load

29
00:02:02,075 --> 00:02:07,403
a data frame called NYC Sample DF that will contain the top

30
00:02:07,403 --> 00:02:13,064
1000 rows of the data and the column classes are provided

31
00:02:13,064 --> 00:02:19,299
to the read.csv function through the called classes arguments.

32
00:02:21,992 --> 00:02:26,780
Let's take a look at the data to see if everything worked.

33
00:02:26,780 --> 00:02:33,540
So we can now see the top 10 rows of the data and it looks like

34
00:02:34,940 --> 00:02:41,800
everything has been properly loaded into our, we can scroll down.

35
00:02:41,800 --> 00:02:45,520
We can see our fare amount column showing up there.

36
00:02:45,520 --> 00:02:47,340
We can see tip amounts.

37
00:02:47,340 --> 00:02:49,013
We can see the coordinate columns.

38
00:02:49,013 --> 00:02:50,873
Pickup longtitude and latitude.

39
00:02:50,873 --> 00:02:52,504
Dropoff longitude and latitude.

40
00:02:52,504 --> 00:02:54,030
Payment type and so on, okay?

41
00:02:55,430 --> 00:02:57,440
So we're gonna keep this data frame around.

42
00:02:57,440 --> 00:03:02,355
And occasionally we're gonna use it to test some of our code before we

43
00:03:02,355 --> 00:03:06,508
deploy the code to the large data set that we're gonna load

44
00:03:06,508 --> 00:03:07,790
in the next step.

