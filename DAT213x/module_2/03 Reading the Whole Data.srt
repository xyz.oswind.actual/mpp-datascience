0
00:00:01,370 --> 00:00:06,100
In the last section, we loaded the top 1,000 rows of the data

1
00:00:06,100 --> 00:00:10,120
as a data frame in our R session.

2
00:00:10,120 --> 00:00:14,280
Now we're gonna go and we're going to read the six

3
00:00:14,280 --> 00:00:18,870
CSV files that we have and convert them into what's called an XDF file.

4
00:00:18,870 --> 00:00:23,310
An XDF file, just like the CSV files, is going to be an external

5
00:00:23,310 --> 00:00:25,956
file in the sense that it's stored on the disk, it's not a memory

6
00:00:25,956 --> 00:00:32,055
object, and it's going to be similar to a data frame in the sense

7
00:00:32,055 --> 00:00:36,045
that it's an object that is only uniquely understood by R itself.

8
00:00:36,045 --> 00:00:39,242
Let's see how we can create an XDF file now.

9
00:00:39,242 --> 00:00:44,392
So, here I have an input specified yellow trip data 2016,

10
00:00:44,392 --> 00:00:48,332
this is a file that doesn't exist yet, but

11
00:00:48,332 --> 00:00:51,587
it will get created when the code from this section finishes running.

12
00:00:51,587 --> 00:00:56,495
I then used the lubridate package to automatically

13
00:00:56,495 --> 00:01:01,755
make it easy to loop through the CSV files that we have.

14
00:01:01,755 --> 00:01:07,515
And start with the most recent CSV file, which is for June of 2016,

15
00:01:07,515 --> 00:01:12,987
and keep going back, so that for each iteration of the loop, we read

16
00:01:12,987 --> 00:01:18,080
one of the CSV files and create an XDF file out of all six of them.

17
00:01:19,680 --> 00:01:22,690
So, I'm gonna start running this and

18
00:01:22,690 --> 00:01:26,030
then we can explore what the code is doing.

19
00:01:26,030 --> 00:01:30,810
And in the process, we're also gonna time to see how long it takes.

20
00:01:30,810 --> 00:01:32,000
So, it started running now.

21
00:01:32,000 --> 00:01:36,340
We can see some results are being generated about the number of rows

22
00:01:36,340 --> 00:01:41,340
for each step and the name of the file that was just read.

23
00:01:42,930 --> 00:01:47,460
So, as I mentioned, we start with the most recent file.

24
00:01:47,460 --> 00:01:52,950
We dynamically generate the file name here, and

25
00:01:52,950 --> 00:01:55,820
then we use this function called rxImport.

26
00:01:55,820 --> 00:01:58,690
rxImport is similar to

27
00:01:58,690 --> 00:02:01,710
the read.csv function that we were using in the last section.

28
00:02:02,780 --> 00:02:06,481
We specify the input that we are reading from, which for

29
00:02:06,481 --> 00:02:10,355
each iteration of the loop is gonna be one of the CSV files.

30
00:02:10,355 --> 00:02:15,645
And then we specify the output that we are dumping the data to,

31
00:02:15,645 --> 00:02:19,390
which for us is gonna be a single XDF file.

32
00:02:19,390 --> 00:02:22,805
So when the loop finishes running, we're gonna have one XDF file that's

33
00:02:22,805 --> 00:02:26,330
gonna have the same content as the six CSV files put together.

34
00:02:27,710 --> 00:02:31,424
We can then use call classes just like we did in the case of

35
00:02:31,424 --> 00:02:34,680
read.csv to specify what the column types are.

36
00:02:34,680 --> 00:02:39,590
And because we are outputting to this same file in every step here,

37
00:02:39,590 --> 00:02:44,070
every iteration of the loop, we specify overwrite = TRUE.

38
00:02:45,070 --> 00:02:48,120
Finally, we have an argument called append.

39
00:02:48,120 --> 00:02:51,720
And the first iteration of the loop append is gonna be set to none,

40
00:02:51,720 --> 00:02:54,690
because we're creating a new XDF file.

41
00:02:54,690 --> 00:02:58,870
And then after that, append is going to be set to rows so

42
00:02:58,870 --> 00:03:03,470
that we can just keep taking the content of each CSV file and

43
00:03:03,470 --> 00:03:05,460
append it to the same XDF file.

44
00:03:07,240 --> 00:03:13,170
It took about a minute for this code to finish running, and if we go

45
00:03:13,170 --> 00:03:17,180
to our working directory now, we can see that we have an XDF file here.

46
00:03:18,200 --> 00:03:21,470
One of the important things to point out about the XDF file

47
00:03:21,470 --> 00:03:23,980
is that if we select our six CSV files,

48
00:03:23,980 --> 00:03:29,410
we can see that together they have about 650 MB of size.

49
00:03:29,410 --> 00:03:34,926
If we select the XDF file, we see about 130 MB in size.

50
00:03:34,926 --> 00:03:39,176
So, already we can see one of the advantages of using XDF and

51
00:03:39,176 --> 00:03:42,321
that is the fact that XDF uses compression so

52
00:03:42,321 --> 00:03:45,065
that data sizes are gonna be smaller.

53
00:03:45,065 --> 00:03:50,553
This is gonna be important because as we will see in later sections,

54
00:03:50,553 --> 00:03:55,367
I/O, reading and writing from and to the disc, is going to

55
00:03:55,367 --> 00:03:59,724
be one of the contributors to runtime in RevoScaleR.

