0
00:00:01,590 --> 00:00:05,594
So that's it.

1
00:00:05,594 --> 00:00:07,780
We were able to run some simple transformations.

2
00:00:07,780 --> 00:00:11,270
We were able to run some more complex transformations.

3
00:00:11,270 --> 00:00:13,730
We started with the initial dataset.

4
00:00:13,730 --> 00:00:16,953
And we now made it a richer dataset,

5
00:00:16,953 --> 00:00:22,976
as far as analyzing it is concerned, by adding new features to it.

6
00:00:22,976 --> 00:00:27,900
In some cases, the features that we added were pretty simple features.

7
00:00:27,900 --> 00:00:30,550
And in some cases, we had to

8
00:00:30,550 --> 00:00:33,510
do some more complex transformations to get those features.

9
00:00:33,510 --> 00:00:37,740
And we're gonna spend quite a bit of time moving forward looking at those

10
00:00:37,740 --> 00:00:41,780
features, trying to visualize them, trying to make sense of them.

11
00:00:41,780 --> 00:00:46,691
And being able to tie them together into a coherent story about

12
00:00:46,691 --> 00:00:48,590
what we see in the data.

