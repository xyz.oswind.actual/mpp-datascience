0
00:00:01,110 --> 00:00:05,942
So far in this module we started by building a model and

1
00:00:05,942 --> 00:00:13,800
the model was meant to predict tip percent using four input variables.

2
00:00:15,190 --> 00:00:19,900
And then we looked at how the model was doing by basically

3
00:00:19,900 --> 00:00:23,620
having the model run predictions on a data set

4
00:00:23,620 --> 00:00:27,130
that had all the different combinations of the input variables.

5
00:00:27,130 --> 00:00:30,540
And the predictions looked all right.

6
00:00:31,580 --> 00:00:36,563
All right here in the sense that they seem to be in a reasonable

7
00:00:36,563 --> 00:00:41,155
range, we then ask if we could somehow take our model and

8
00:00:41,155 --> 00:00:46,138
make it even more simple, more parsimonious by dropping two

9
00:00:46,138 --> 00:00:50,850
out of the four input variables used in the model.

10
00:00:50,850 --> 00:00:54,780
So we built a second model and we were able to then compare

11
00:00:54,780 --> 00:00:56,990
the prediction made by the original model.

12
00:00:56,990 --> 00:00:58,700
With the predictions made by the second model.

13
00:00:59,850 --> 00:01:03,770
Of course it's hard to determine which model is better.

14
00:01:05,130 --> 00:01:09,350
There is some art and some science going on.

15
00:01:09,350 --> 00:01:11,970
Certainly a certain amount of domain knowledge

16
00:01:11,970 --> 00:01:14,810
is also gonna be very important in this case.

17
00:01:14,810 --> 00:01:19,180
The more we know about our data, the more we can

18
00:01:20,750 --> 00:01:24,020
decide which model is gonna be more appropriate.

19
00:01:24,020 --> 00:01:27,010
We also need to be careful about some of the assumptions that some

20
00:01:27,010 --> 00:01:28,140
models make.

21
00:01:28,140 --> 00:01:31,900
We might be careful, for example, about using a regression model,

22
00:01:33,490 --> 00:01:37,820
a linear regression model in this case to predict tip percent,

23
00:01:37,820 --> 00:01:43,147
which by definition is between 0% and 100%.

24
00:01:43,147 --> 00:01:47,270
And in practice it's also in a much narrower range.

25
00:01:48,620 --> 00:01:53,960
So we're gonna get a little bit more methodical about our process of

26
00:01:53,960 --> 00:02:00,360
building models and we're also gonna explore some other possibilities.

27
00:02:00,360 --> 00:02:07,030
We're gonna build models using decision trees and random forests.

28
00:02:07,030 --> 00:02:09,430
We're then gonna compare the models and

29
00:02:09,430 --> 00:02:12,030
we're gonna do the comparison in two ways.

30
00:02:12,030 --> 00:02:14,790
One is we're gonna do something similar to what we did in

31
00:02:14,790 --> 00:02:16,230
the last section.

32
00:02:16,230 --> 00:02:19,600
Namely we're gonna look at predictions

33
00:02:19,600 --> 00:02:23,680
on a data set with all the different combinations of the inputs

34
00:02:24,750 --> 00:02:26,410
to see how they're distributed.

35
00:02:27,540 --> 00:02:31,750
But then we're gonna look at another comparison and

36
00:02:31,750 --> 00:02:34,250
that's the more fundamental comparison,

37
00:02:34,250 --> 00:02:40,371
that is we're going to look at how the different models are predicting.

38
00:02:40,371 --> 00:02:44,370
In other words, how close the predictions are coming to the actual

39
00:02:44,370 --> 00:02:47,970
observations by and the true test of how good a model is,

40
00:02:47,970 --> 00:02:53,180
in our case is really to know how good it is in making predictions.

41
00:02:53,180 --> 00:02:56,200
And for that, we need to do two things.

42
00:02:56,200 --> 00:03:00,900
One is we need to compare the predictions from the model

43
00:03:00,900 --> 00:03:04,700
to the actual observations and the second thing is for

44
00:03:04,700 --> 00:03:09,410
that comparison to be fair, it has to be on a portion of

45
00:03:09,410 --> 00:03:14,640
the data that we set aside, that we don't use to build the model with.

46
00:03:14,640 --> 00:03:17,790
So that we can build the model and

47
00:03:17,790 --> 00:03:20,140
then go to the data set that we set aside,

48
00:03:20,140 --> 00:03:25,120
what we usually refer to as the test data, run predictions on it.

49
00:03:25,120 --> 00:03:29,740
And on that data set, if the model is doing a good job of making

50
00:03:29,740 --> 00:03:33,210
predictions, if the predictions are coming pretty close to what we

51
00:03:33,210 --> 00:03:37,530
actually observed for tip percen then we know we have a good model.

52
00:03:39,510 --> 00:03:42,810
So, we're gonna do that in the following section.

53
00:03:44,640 --> 00:03:47,070
For us to do that we're gonna start

54
00:03:47,070 --> 00:03:51,480
by splitting the data into a training set and a test set.

55
00:03:51,480 --> 00:03:57,240
Now to automate this a little bit, I'm gonna write a function called

56
00:03:57,240 --> 00:04:02,760
rx split xdf, and what is gonna do is first of all,

57
00:04:02,760 --> 00:04:08,940
it is going to accept a ratio as the split percentage.

58
00:04:08,940 --> 00:04:11,889
By default that is at 275%.

59
00:04:11,889 --> 00:04:18,030
So 75% of the data, by default, is gonna go towards training.

60
00:04:18,030 --> 00:04:21,500
And the other 25% is gonna go toward testing.

61
00:04:21,500 --> 00:04:23,870
The way that it's happening is,

62
00:04:23,870 --> 00:04:28,480
using rxDataStep, we're going to take the XDF file.

63
00:04:28,480 --> 00:04:32,320
We're gonna start by performing a transformation,

64
00:04:32,320 --> 00:04:35,100
in which we create a column called split.

65
00:04:35,100 --> 00:04:40,330
And that column is going to randomly be set to either train or

66
00:04:40,330 --> 00:04:43,540
test, based on some random number generation

67
00:04:43,540 --> 00:04:45,030
from the binomial distribution.

68
00:04:46,900 --> 00:04:51,640
Once the transformation is done, we end up with the same data but

69
00:04:51,640 --> 00:04:56,080
a column called split that we can then use to split the data by.

70
00:04:56,080 --> 00:04:59,360
The actual splitting is gonna happen in the next step

71
00:05:00,390 --> 00:05:02,890
using a function called RxSplit.

72
00:05:02,890 --> 00:05:07,860
RxSplit is gonna take an XDF file and it's going to split it based on

73
00:05:07,860 --> 00:05:10,930
some column, in this case the column is called split.

74
00:05:12,130 --> 00:05:16,240
And when we finish running this we're gonna end up with

75
00:05:16,240 --> 00:05:19,740
two XDF files, one for training and the other one for testing.

76
00:05:20,980 --> 00:05:24,080
So let's float this function and

77
00:05:24,080 --> 00:05:28,000
in the next step we're going to run the actual

78
00:05:29,540 --> 00:05:33,100
function so that we can split the data in training and testing sets.

79
00:05:35,120 --> 00:05:37,390
In the process we're also going to

80
00:05:39,110 --> 00:05:42,740
only keep the variables that we care about, in this case the variables

81
00:05:42,740 --> 00:05:48,060
that we think we might be interested in using to build our model.

82
00:05:48,060 --> 00:05:53,010
And if there are any other variables that so far we haven't used but

83
00:05:53,010 --> 00:05:56,850
we think might be helpful this would be a good place to include them.

84
00:05:58,460 --> 00:06:00,440
Any other variables are gonna be left out.

85
00:06:02,550 --> 00:06:07,500
Once this is done we can go to our working

86
00:06:07,500 --> 00:06:12,510
directory which is in C>Data>NYC_taxi for me.

87
00:06:12,510 --> 00:06:16,560
And we should see a new folder called Output.

88
00:06:16,560 --> 00:06:20,460
In it there's going to be a folder called Split.

89
00:06:20,460 --> 00:06:24,848
And in it we should see the two XDF files for training and for testing.

90
00:06:24,848 --> 00:06:30,370
Okay, so that would be step one.

91
00:06:30,370 --> 00:06:35,600
Step two is now for us to go and start building our models.

92
00:06:35,600 --> 00:06:40,990
Here's the linear model that we built earlier.

93
00:06:40,990 --> 00:06:42,860
And we can see that.

94
00:06:42,860 --> 00:06:44,840
We have something similar to the,

95
00:06:44,840 --> 00:06:47,820
in fact we have the exact same formula as our first model.

96
00:06:49,070 --> 00:06:53,030
The linear model is going to predict tip percent based on the interaction

97
00:06:53,030 --> 00:06:55,620
of pick up neighborhood and drop off neighborhood and

98
00:06:55,620 --> 00:07:00,250
the interaction of pickup_dow and pickup_hour.

99
00:07:00,250 --> 00:07:04,450
Notice however that the model is going to be built

100
00:07:04,450 --> 00:07:08,763
on only the training data portion of our data.

101
00:07:08,763 --> 00:07:13,150
So, the 75% of the data that we set aside for training.

102
00:07:14,300 --> 00:07:16,310
In the process we're also going to time this.

103
00:07:17,350 --> 00:07:22,560
Let me go ahead and run all three models now and we'll

104
00:07:22,560 --> 00:07:27,610
return to talk about what the other two are as the code is running.

105
00:07:27,610 --> 00:07:31,170
So the linear model runs really quickly and it's done.

106
00:07:31,170 --> 00:07:33,930
The next model we're gonna build is a position tree.

107
00:07:33,930 --> 00:07:37,050
And for that we use the RXD tree function.

108
00:07:37,050 --> 00:07:39,920
And in the case of the decision tree the formula is similar

109
00:07:39,920 --> 00:07:45,080
except that we don't have any colons separating the two

110
00:07:45,080 --> 00:07:47,640
variables that we want the interactions of.

111
00:07:47,640 --> 00:07:51,120
This is because interactions are built into the decision trees.

112
00:07:51,120 --> 00:07:55,130
Decision trees by the way that the algorithm works

113
00:07:55,130 --> 00:07:58,440
take interactions into account and we don't need to

114
00:07:59,800 --> 00:08:02,790
specify which variables we wanna see interactions for.

115
00:08:02,790 --> 00:08:06,330
We just specify the variables that we want to be included and

116
00:08:06,330 --> 00:08:10,490
separate them using pluses, Okay otherwise,

117
00:08:10,490 --> 00:08:13,150
the four input columns that we're using are the same.

118
00:08:13,150 --> 00:08:15,450
And we can see that, as we're speaking, the code for

119
00:08:15,450 --> 00:08:17,190
the decision tree finished running.

120
00:08:17,190 --> 00:08:22,920
And it took longer than the rxLinMod function did.

121
00:08:22,920 --> 00:08:27,850
But We now have a decision tree in addition to a regression model.

122
00:08:29,030 --> 00:08:33,210
The last model that we're gonna build is a random forest model.

123
00:08:33,210 --> 00:08:37,700
For that we use the RxdForest function and

124
00:08:37,700 --> 00:08:43,140
a random forest model is essentially a set of decision trees

125
00:08:43,140 --> 00:08:47,640
that we built, in this case we gonna built a hundred decision trees and

126
00:08:47,640 --> 00:08:50,990
at the end we're gonna find a way to average up the results, so

127
00:08:50,990 --> 00:08:54,490
that we can come up with more accurate predictions than we

128
00:08:54,490 --> 00:08:58,710
usually would Relying on other single decision tree.

129
00:08:58,710 --> 00:09:01,850
Because we're building multiple decision trees, the random forest

130
00:09:01,850 --> 00:09:07,540
model is gonna take considerably longer than the decision tree will.

131
00:09:09,250 --> 00:09:14,410
When it's over will also have a little summary showing

132
00:09:14,410 --> 00:09:18,890
the variable importance for each of the inputs into the model.

133
00:09:18,890 --> 00:09:21,980
This could be important if we have lots of inputs into the model and

134
00:09:21,980 --> 00:09:25,560
we wanna know which ones really do matter for

135
00:09:25,560 --> 00:09:27,130
the purpose of making the prediction.

136
00:09:29,100 --> 00:09:31,050
So we're gonna let this run and

137
00:09:31,050 --> 00:09:35,030
the last thing we're gonna do when the decision tree finishes running

138
00:09:35,030 --> 00:09:41,400
is since modeling tends to be a computational intensive

139
00:09:41,400 --> 00:09:45,510
task, we're just going to take the three models that we built,

140
00:09:45,510 --> 00:09:49,420
we're gonna dump them into a list called train of models and

141
00:09:49,420 --> 00:09:52,560
we're gonna save that somewhere in our working directory.

142
00:09:53,770 --> 00:09:57,100
In case we lose our R session.

143
00:09:57,100 --> 00:09:58,850
In case we have to restart,

144
00:09:58,850 --> 00:10:02,350
we don't have to run the models a second time.

145
00:10:02,350 --> 00:10:06,320
We can just go and pull them out and we can use them to make predictions.

146
00:10:06,320 --> 00:10:11,360
This is also very helpful if we build the models,

147
00:10:11,360 --> 00:10:16,060
in this case locally on our laptops.

148
00:10:16,060 --> 00:10:20,890
And then we wanna take the model and go to let's say a Hadoop cluster or

149
00:10:20,890 --> 00:10:25,810
a SQL Server or a more beefy server with more data sitting in it.

150
00:10:25,810 --> 00:10:27,840
And used the model to make predictions.

151
00:10:27,840 --> 00:10:30,270
In that case we want to do something similar to this.

152
00:10:30,270 --> 00:10:34,210
We wanna save the models and then we wanna open them in a different

153
00:10:34,210 --> 00:10:36,930
compute context, so we can use them to make predictions.

