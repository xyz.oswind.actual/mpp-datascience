0
00:00:01,800 --> 00:00:04,180
Our random forest model finished running.

1
00:00:04,180 --> 00:00:08,170
And as we can see it took approximately ten times longer than

2
00:00:08,170 --> 00:00:09,810
the decision tree.

3
00:00:09,810 --> 00:00:12,450
So looking at this we can

4
00:00:12,450 --> 00:00:16,810
kind of get an idea of the run time of the three different algorithms.

5
00:00:16,810 --> 00:00:21,470
Linear models tend to be fairly efficient.

6
00:00:21,470 --> 00:00:24,040
This is not always the case by the way and

7
00:00:24,040 --> 00:00:27,600
open source are certainly not the case that linear models are by

8
00:00:27,600 --> 00:00:30,760
default going to be more efficient and say a decision tree.

9
00:00:32,655 --> 00:00:35,515
Because the ln function in open source R

10
00:00:35,515 --> 00:00:36,875
is not a parallel algorithm.

11
00:00:37,895 --> 00:00:42,445
Because of that, especially when the input columns are factors with lots

12
00:00:42,445 --> 00:00:48,265
of levels, the ln function is going to run into performance hurdles.

13
00:00:48,265 --> 00:00:53,859
The rx ln function in on the other hand, being a parallel algorithm,

14
00:00:53,859 --> 00:00:57,570
is going to be Very efficient and run very quickly.

15
00:01:01,190 --> 00:01:08,120
Let's now shift gear from the process of building models and

16
00:01:08,120 --> 00:01:12,980
that go into having the models run predictions for us and

17
00:01:12,980 --> 00:01:14,700
compare the predictions.

18
00:01:14,700 --> 00:01:18,730
We're gonna do this in two different steps.

19
00:01:18,730 --> 00:01:23,790
In the first step, we're gonna do something similar to what we did at

20
00:01:23,790 --> 00:01:25,120
the beginning of the module.

21
00:01:25,120 --> 00:01:30,310
Namely, using the models to make predictions on

22
00:01:30,310 --> 00:01:36,620
a data frame with all the different combinations of the input columns.

23
00:01:36,620 --> 00:01:42,410
So the data frame is called pred.df and right now

24
00:01:42,410 --> 00:01:48,090
it just has the four input columns, one row for each combination.

25
00:01:48,090 --> 00:01:53,340
We're going to use the rxPredict functions three different times to

26
00:01:53,340 --> 00:01:56,130
make predictions on this data frame, and

27
00:01:56,130 --> 00:02:00,478
we're going to call the predictions pred_linmod, pred_dtree,

28
00:02:00,478 --> 00:02:03,700
and pred_dforest so that we can tell them apart.

29
00:02:05,480 --> 00:02:10,480
Then, going to combine the predictions and

30
00:02:10,480 --> 00:02:13,320
put them into the same data frame.

31
00:02:13,320 --> 00:02:16,670
Notice that from the run times as this is happening we can tell that

32
00:02:16,670 --> 00:02:20,100
the predictions for the linear model run very quickly.

33
00:02:20,100 --> 00:02:23,510
The predictions for the decision tree are also pretty fast.

34
00:02:23,510 --> 00:02:25,910
The predictions for the random forest on the other hand,

35
00:02:25,910 --> 00:02:27,980
take a little bit longer to run.

36
00:02:27,980 --> 00:02:29,760
This is not by accident.

37
00:02:29,760 --> 00:02:34,850
Random forest as I mentioned before is going to be more computationally

38
00:02:34,850 --> 00:02:37,970
intensive because it's gonna build multiple decision trees.

39
00:02:37,970 --> 00:02:40,710
It's gonna make predictions multiple times and

40
00:02:40,710 --> 00:02:45,020
it's gonna then find a way to have reach out to predictions and because

41
00:02:45,020 --> 00:02:49,210
of that we expect it to take longer to come up with the predictions.

42
00:02:50,400 --> 00:02:55,650
So we can see now for example that let's say on row four someone who

43
00:02:55,650 --> 00:03:00,410
is picked up from Little Italy who is dropped off in Chinatown

44
00:03:00,410 --> 00:03:03,520
between the hours of 1:00 AM to 5:00 AM on a Sunday.

45
00:03:05,080 --> 00:03:10,350
The linear model is predicting a tip of 14, about 14%.

46
00:03:10,350 --> 00:03:15,357
The decision tree is predicting a tip of about 11%.

47
00:03:15,357 --> 00:03:20,540
And the random forest is predicting a prediction

48
00:03:20,540 --> 00:03:23,940
somewhere between the prediction made by the linear model and

49
00:03:23,940 --> 00:03:25,980
the prediction made by the decision tree.

50
00:03:25,980 --> 00:03:28,080
So, about 13%.

51
00:03:28,080 --> 00:03:33,170
So we have differences and occasionally,

52
00:03:33,170 --> 00:03:36,280
the differences are gonna be pretty small and

53
00:03:36,280 --> 00:03:39,046
occasionally they're gonna be pretty far apart.

54
00:03:39,046 --> 00:03:44,470
At this point we can tell

55
00:03:44,470 --> 00:03:48,940
how each model is making predictions and how they're different but

56
00:03:48,940 --> 00:03:53,940
what we don't know is if they're making good predictions.

57
00:03:53,940 --> 00:03:57,740
So another thing that we can do is we can use the rx summary function

58
00:03:57,740 --> 00:04:01,380
to obtain average or tip percent.

59
00:04:01,380 --> 00:04:06,680
Or each of these combinations so we can go to every trip

60
00:04:06,680 --> 00:04:09,420
from Chinatown to Chinatown between 1 AM and

61
00:04:09,420 --> 00:04:13,300
5 AM on a Sunday and there's gonna be lots of those in the data.

62
00:04:13,300 --> 00:04:17,150
We can average them out and then ask what the average tip percent is.

63
00:04:17,150 --> 00:04:21,050
And then we can see if the models are predicting something close to

64
00:04:21,050 --> 00:04:24,550
the average or if the predictions are way off.

65
00:04:26,200 --> 00:04:29,000
So, in the next step, we're gonna do exactly that.

66
00:04:29,000 --> 00:04:34,560
We are going to go to pred.df and we're gonna put the averages for

67
00:04:34,560 --> 00:04:38,820
tip percent into the data as well.

68
00:04:38,820 --> 00:04:43,640
So now, if we look at our pred.df.

69
00:04:43,640 --> 00:04:46,950
We also have a new column called means.

70
00:04:46,950 --> 00:04:51,770
We can see that, in some cases it could be for

71
00:04:51,770 --> 00:04:54,690
example, in this case because there aren't

72
00:04:54,690 --> 00:04:58,530
that many trips happening in between one am and five am.

73
00:04:58,530 --> 00:05:03,510
The predictions are just going to be relying on just a few samples and

74
00:05:03,510 --> 00:05:05,360
they're going to be way off.

75
00:05:05,360 --> 00:05:09,000
So the sample size in this case is going to

76
00:05:09,000 --> 00:05:12,140
make these averages not always very reliable.

77
00:05:12,140 --> 00:05:16,980
But in a lot of cases we have a large enough sample size that we can

78
00:05:18,630 --> 00:05:20,810
set store by the averages.

79
00:05:22,260 --> 00:05:26,220
So once again, we're faced with a situation where we have a lot of

80
00:05:26,220 --> 00:05:27,770
data points to look at.

81
00:05:27,770 --> 00:05:30,420
So let's just come up with a visualization that's going to

82
00:05:30,420 --> 00:05:32,710
summarize what's going on.

83
00:05:32,710 --> 00:05:35,060
Then we're gonna use ggplot2,

84
00:05:35,060 --> 00:05:40,130
and we're gonna look at the distribution of the predictions

85
00:05:40,130 --> 00:05:45,550
as well as the distribution of the averages for the observed prediction

86
00:05:45,550 --> 00:05:49,190
across the different combinations of the input columns.

87
00:05:50,270 --> 00:05:54,860
And so let's start with the observed averages which is shown in purple.

88
00:05:54,860 --> 00:05:57,870
We can see a couple of things that are note worthy.

89
00:05:59,210 --> 00:06:03,229
One of them is that the observed averages

90
00:06:04,810 --> 00:06:10,850
cover a wider range than the predictions seem to cover.

91
00:06:10,850 --> 00:06:13,890
So that's something to take into account.

92
00:06:13,890 --> 00:06:16,760
The predictions are more narrow than what

93
00:06:16,760 --> 00:06:19,600
we actually observe in the data.

94
00:06:19,600 --> 00:06:24,590
In the case of the observed averages we can also see

95
00:06:24,590 --> 00:06:28,510
second peaks happening in certain places, and third peaks.

96
00:06:28,510 --> 00:06:30,980
So the second peak that is noteworthy is,

97
00:06:30,980 --> 00:06:33,920
we have a bunch of 0 predictions.

98
00:06:33,920 --> 00:06:37,330
I'm sorry, not predictions, we have a bunch of 0 averages.

99
00:06:37,330 --> 00:06:38,450
So, in reality,

100
00:06:38,450 --> 00:06:44,400
there is going to be people who just don't tip or certain trips.

101
00:06:44,400 --> 00:06:48,810
And we can see that shown in the actual data but

102
00:06:48,810 --> 00:06:54,940
right now none of the models are really taking that into account.

103
00:06:56,000 --> 00:07:00,810
None of the models are predicting, are doing a good job predicting for

104
00:07:00,810 --> 00:07:03,550
the people who do not tip.

105
00:07:03,550 --> 00:07:07,490
We're going to see in the homework exercises that

106
00:07:07,490 --> 00:07:09,950
there is a good reason for that.

107
00:07:09,950 --> 00:07:13,510
There is something about the data that we are not taking into account

108
00:07:13,510 --> 00:07:20,050
that does in fact affect our ability to be able to make good predictions.

109
00:07:20,050 --> 00:07:24,280
What's missing about our models is that we are not taking payment type

110
00:07:24,280 --> 00:07:25,580
into account.

111
00:07:25,580 --> 00:07:26,870
And as it turns out,

112
00:07:27,870 --> 00:07:32,880
people who pay in cash do not get their tips recorded in the data.

113
00:07:32,880 --> 00:07:36,250
A tip for those people would just show up as 0.

114
00:07:36,250 --> 00:07:40,190
And so, the fact that we see a second peak and

115
00:07:40,190 --> 00:07:44,530
it's a quite a large peak at 0, could be a reflection of that.

116
00:07:45,570 --> 00:07:50,350
And this is not unusual, when we have a situation

117
00:07:50,350 --> 00:07:54,910
where we have to build model,s one of the most important things about

118
00:07:54,910 --> 00:07:57,740
models is what features are going into the model.

119
00:07:59,000 --> 00:08:03,370
Having the right set of features for our models is going to make

120
00:08:03,370 --> 00:08:08,880
a significant improvement in our predictive ability probably more so

121
00:08:08,880 --> 00:08:12,610
than spending a lot of time tuning our models and

122
00:08:12,610 --> 00:08:14,580
building different models to see which one

123
00:08:16,620 --> 00:08:20,920
is going to make better predictions than the last one and so on.

124
00:08:20,920 --> 00:08:24,310
So feature engineering is a whole field

125
00:08:24,310 --> 00:08:27,410
dedicated to how can we come up with good features.

126
00:08:28,570 --> 00:08:33,538
And on top of that, we should also have a certain amount of domain

127
00:08:33,538 --> 00:08:38,966
knowledge about the data and this is why we spend a good portion of the,

128
00:08:38,966 --> 00:08:43,014
of the analysis just looking at various summaries,

129
00:08:43,014 --> 00:08:46,786
looking at certain visualizations of the data.

130
00:08:46,786 --> 00:08:51,673
And so, all the information that we glean from doing that in the last

131
00:08:51,673 --> 00:08:56,218
module is certainly used and can be used in the current module to

132
00:08:56,218 --> 00:09:00,286
decide which features we should include in our models.

133
00:09:00,286 --> 00:09:02,122
And as it turns out in this case,

134
00:09:02,122 --> 00:09:05,594
payment type should probably have it been one of the features

135
00:09:05,594 --> 00:09:08,810
that we should have included in this model and didn't.

136
00:09:08,810 --> 00:09:13,419
And because of that, our model is failing to capture as turn

137
00:09:13,419 --> 00:09:17,950
amount of behavior in the data that it does not know about.

138
00:09:20,250 --> 00:09:24,730
Let's now go back to the, to our distributions and

139
00:09:24,730 --> 00:09:29,530
compare the predictions made by the three different models.

140
00:09:29,530 --> 00:09:33,160
One of the things that jumps out is how the predictions made by

141
00:09:33,160 --> 00:09:36,210
the decision tree intends to be pretty choppy.

142
00:09:36,210 --> 00:09:39,390
Compared to the predictions made by the linear model and

143
00:09:39,390 --> 00:09:40,980
the random forest.

144
00:09:40,980 --> 00:09:43,740
This is not an uncommon behavior of decision trees,

145
00:09:43,740 --> 00:09:48,510
decision trees by their nature tend to

146
00:09:48,510 --> 00:09:53,490
rake up the data into segments and make predictions for each segment.

147
00:09:53,490 --> 00:09:58,090
So, it could very easily turn out that the predictions Have

148
00:09:58,090 --> 00:10:02,980
a choppy distribution like the one we're looking at here.

149
00:10:02,980 --> 00:10:07,310
Looking at the linear model we can see that it does probably a better

150
00:10:07,310 --> 00:10:13,076
job of making predictions

151
00:10:13,076 --> 00:10:18,320
in the making wider predictions than the random forest and

152
00:10:18,320 --> 00:10:19,810
the decision tree.

153
00:10:19,810 --> 00:10:24,430
And the random forest once again not too surprisingly, is making

154
00:10:24,430 --> 00:10:28,820
predictions that seem to be very similar to what we would get if we

155
00:10:28,820 --> 00:10:34,030
just averaged out the predictions made by a bunch of decision trees.

156
00:10:34,030 --> 00:10:38,580
So the random forest for the most part is making smoother

157
00:10:38,580 --> 00:10:42,870
predictions but we can see that the red line is kind of following

158
00:10:43,950 --> 00:10:47,870
distribution that is almost like it and

159
00:10:47,870 --> 00:10:52,570
what we would get if we thought of this distribution as a.

160
00:10:53,950 --> 00:10:58,370
Has a stock price and we took moving averages, so it seems to be just

161
00:10:58,370 --> 00:11:04,120
an average of what the single decision tree is showing to us.

162
00:11:04,120 --> 00:11:07,561
And so this should also not be too surprising.

