0
00:00:00,770 --> 00:00:01,590
So let's take a look.

1
00:00:01,590 --> 00:00:04,935
The function we're gonna use is called rxLinMod.

2
00:00:06,120 --> 00:00:11,380
It is the equivalent of the LM function in open source R.

3
00:00:11,380 --> 00:00:15,020
The difference of course being that LM only works with dataframes and

4
00:00:15,020 --> 00:00:16,670
it's not a distributed function.

5
00:00:16,670 --> 00:00:21,810
So with large dataframes, it's gonna run into performance issues.

6
00:00:21,810 --> 00:00:26,548
RXLM is a distributed function that works with Data frames,

7
00:00:26,548 --> 00:00:30,870
xdf files with flat files and so on.

8
00:00:30,870 --> 00:00:32,550
And because it's distributed,

9
00:00:32,550 --> 00:00:35,550
it's gonna be extremely efficient and very fast.

10
00:00:37,560 --> 00:00:39,390
Just like in the case of LM,

11
00:00:39,390 --> 00:00:42,920
we need to decide what the formula needs to be.

12
00:00:42,920 --> 00:00:47,340
So here I have the formula created in a prior step.

13
00:00:47,340 --> 00:00:52,140
And what we're gonna do is we're going to predict tip percent

14
00:00:52,140 --> 00:00:55,830
given the following variables.

15
00:00:55,830 --> 00:00:58,880
Pick up neighborhood and drop off neighborhood, interaction.

16
00:00:59,970 --> 00:01:07,273
And pick up day of week and drop off and pick up hour interactions, okay.

17
00:01:07,273 --> 00:01:11,142
So let's see what intuitively we're talking about here.

18
00:01:11,142 --> 00:01:15,503
What we're talking about here is that we wanna take pick up

19
00:01:15,503 --> 00:01:16,838
neighborhood and

20
00:01:16,838 --> 00:01:22,060
drop off neighborhood as two of the inputs into this model.

21
00:01:22,060 --> 00:01:26,857
But our intuition says that, where someone gets picked up is

22
00:01:26,857 --> 00:01:31,100
not enough to determine how much they're gonna tip.

23
00:01:31,100 --> 00:01:34,398
Where someone gets dropped off,

24
00:01:34,398 --> 00:01:38,990
on its own is probably not enough to account for

25
00:01:38,990 --> 00:01:43,607
the variation in how much people are tipping.

26
00:01:43,607 --> 00:01:46,797
But if we know where someone is picked up and

27
00:01:46,797 --> 00:01:49,170
where they're dropped off.

28
00:01:49,170 --> 00:01:53,020
So the interaction between those two columns, then we should be able to

29
00:01:53,020 --> 00:01:58,310
do a pretty good job of what the tip is going to be.

30
00:01:58,310 --> 00:02:03,160
Same thing with day of week, and hour of the day.

31
00:02:03,160 --> 00:02:07,860
Knowing what day of the week somebody is getting picked up

32
00:02:07,860 --> 00:02:12,350
might not be enough for us to do a good job predicting tip percent.

33
00:02:12,350 --> 00:02:16,513
Knowing what particular time of the day they

34
00:02:16,513 --> 00:02:19,895
were picked up might not be enough.

35
00:02:19,895 --> 00:02:24,940
But if we know the day of the week and we know the time of the day that

36
00:02:24,940 --> 00:02:29,894
the person was picked up, we might be able to do a pretty good job

37
00:02:29,894 --> 00:02:34,880
of accounting for the variability that exists in tip percent.

38
00:02:34,880 --> 00:02:38,180
So that's the intuition behind building this model.

39
00:02:38,180 --> 00:02:41,125
Let's now actually go and build the model.

40
00:02:41,125 --> 00:02:45,808
So the next step is for us to run the rxLinMod function.

41
00:02:45,808 --> 00:02:49,700
We're gonna give it the formula that we created in the last step.

42
00:02:49,700 --> 00:02:50,388
Here's the data.

43
00:02:50,388 --> 00:02:55,310
And, for rxLinMod

44
00:02:55,310 --> 00:02:59,895
to emulate exactly the results that the LM function returns,

45
00:02:59,895 --> 00:03:04,250
we're going to set drop first equals true.

46
00:03:04,250 --> 00:03:09,440
This means that we have factor columns as one of the inputs in

47
00:03:09,440 --> 00:03:15,780
the model by defaults were going to use the first level as the baseline.

48
00:03:15,780 --> 00:03:18,450
If we don't specify that our rxLinMod

49
00:03:18,450 --> 00:03:21,940
is going to use the last level as the baseline.

50
00:03:21,940 --> 00:03:24,520
But because LM drops of the first level, we're gonna say

51
00:03:24,520 --> 00:03:29,780
dropfirst = TRUE, so that rxLinMod and LM return the exact same result.

52
00:03:29,780 --> 00:03:34,940
Finally, to be able to get, not just the predictions,

53
00:03:34,940 --> 00:03:39,180
but the errors surrounding the predictions

54
00:03:39,180 --> 00:03:43,090
we're gonna set covCoeF = TRUE as well.

55
00:03:46,020 --> 00:03:51,124
I'm gonna run both of these lines together,

56
00:03:51,124 --> 00:03:58,258
and we can see that within no time we get our linear model built.

57
00:03:58,258 --> 00:04:03,660
So, it's stored in an object called rxlm 1.

58
00:04:03,660 --> 00:04:05,284
And if we just try to look at that,

59
00:04:05,284 --> 00:04:08,250
we're gonna see that there's a lot of information in here.

60
00:04:09,550 --> 00:04:13,480
So, we can see the interaction between day of week and

61
00:04:13,480 --> 00:04:16,730
the pick up hour, because both of those are factors.

62
00:04:16,730 --> 00:04:22,080
We're gonna see a coefficient for each combination of day of week,

63
00:04:22,080 --> 00:04:26,690
and pick up hour, except for the ones that involve the baseline.

64
00:04:28,470 --> 00:04:34,560
In which case, we would see a little note that says it was dropped.

65
00:04:34,560 --> 00:04:36,495
Same thing with pick up neighborhood, and

66
00:04:36,495 --> 00:04:37,631
drop off neighborhood.

67
00:04:37,631 --> 00:04:39,872
We're gonna see one coefficient for

68
00:04:39,872 --> 00:04:42,618
each interaction of pick up neighborhood and

69
00:04:42,618 --> 00:04:46,312
drop off neighborhoods, so each combination of these two.

70
00:04:46,312 --> 00:04:48,975
And so we have a lot of coefficient to look at.

71
00:04:48,975 --> 00:04:53,854
And it might help, for example to see which ones are significant and

72
00:04:53,854 --> 00:04:56,290
which ones aren't.

73
00:04:56,290 --> 00:05:00,050
But the preponderance of information here, is making it hard for

74
00:05:00,050 --> 00:05:03,100
us to be able to really tell if we had a good model or not.

75
00:05:04,290 --> 00:05:07,495
So instead of bothering too much with the individual coefficients.

76
00:05:08,595 --> 00:05:12,425
And of course, before leaving that off,

77
00:05:12,425 --> 00:05:16,995
I should also probably mention that just as in the case of LM,

78
00:05:16,995 --> 00:05:20,985
once we get the model object, we can pass that to the summary function.

79
00:05:22,005 --> 00:05:25,555
And we would be able to not just see the coefficients, but

80
00:05:25,555 --> 00:05:29,450
we would see the P values around the coefficients and so on.

81
00:05:29,450 --> 00:05:32,030
We can also see the standard error at the bottom,

82
00:05:32,030 --> 00:05:33,870
the degrees of freedom.

83
00:05:33,870 --> 00:05:38,060
We can see the multiple r squared, which is very small in this case, so

84
00:05:38,060 --> 00:05:40,910
this is already not very promising.

85
00:05:40,910 --> 00:05:44,650
But we can see that the overall model

86
00:05:44,650 --> 00:05:46,580
is definitely highly significant.

87
00:05:49,298 --> 00:05:55,290
From the P values, there is not too much that we can derive.

88
00:05:55,290 --> 00:05:59,430
First of all, because the majority of the P values are significant.

89
00:05:59,430 --> 00:06:03,580
And second, because P values tend to be significant when we have

90
00:06:03,580 --> 00:06:05,900
very large samples sizes.

91
00:06:05,900 --> 00:06:11,740
So, instead of bothering too much with the individual coefficients,

92
00:06:11,740 --> 00:06:14,940
we're gonna go back to our preferred way of just

93
00:06:14,940 --> 00:06:17,200
coming up with some visualizations.

94
00:06:17,200 --> 00:06:21,400
And being able to tell if the visualizations are making

95
00:06:21,400 --> 00:06:25,417
intuitive sense, and if we're on the right track,

96
00:06:25,417 --> 00:06:28,535
as far as our little modeling exercise.

97
00:06:28,535 --> 00:06:34,058
Before doing that we are going to run RX summary and

98
00:06:34,058 --> 00:06:39,560
we're gonna use it in a very specific way.

99
00:06:39,560 --> 00:06:44,080
So, we're gonna run RX summary, and we gonna ask our RX summary to give

100
00:06:44,080 --> 00:06:50,400
us summaries for the four input columns

101
00:06:50,400 --> 00:06:55,750
that we used to build the model in our previous step.

102
00:06:55,750 --> 00:06:57,330
Right, so pick up neighbourhood,

103
00:06:57,330 --> 00:07:00,210
drop off neighbourhood, pick up hour, and pick up day of week.

104
00:07:01,870 --> 00:07:09,760
We're then go to extract, the levels for each of these columns.

105
00:07:09,760 --> 00:07:11,950
Recall that each of them is a factor,

106
00:07:11,950 --> 00:07:14,310
so they're gonna have factor levels.

107
00:07:14,310 --> 00:07:17,906
And we're going to extract the individual levels, and

108
00:07:17,906 --> 00:07:20,978
we're gonna end up with this object called LL.

109
00:07:20,978 --> 00:07:24,315
If we look at our LL right now we can see that, for

110
00:07:24,315 --> 00:07:28,322
pickup neighborhood, these are the factor levels, and

111
00:07:28,322 --> 00:07:30,341
we should have 28 of them.

112
00:07:30,341 --> 00:07:34,285
The drop-off neighborhood, we should see the same factor levels.

113
00:07:34,285 --> 00:07:37,715
And for pick up hour these are the factor levels, and for

114
00:07:37,715 --> 00:07:39,465
pick up day of week, there they are.

115
00:07:40,555 --> 00:07:45,919
Now what we're gonna do, is we're gonna use the expand.grid function,

116
00:07:45,919 --> 00:07:51,590
to create a data frame containing every combination

117
00:07:51,590 --> 00:07:56,480
of these four input columns that we used in our modeling step.

118
00:07:58,000 --> 00:08:04,120
So, right now we can do a quick hit on that, pred DF1 and we

119
00:08:04,120 --> 00:08:07,790
can see that we have our dataframe starting with pick up neighborhood,

120
00:08:07,790 --> 00:08:10,940
drop off neighborhood, pick up hour and pick up day of week.

121
00:08:10,940 --> 00:08:14,745
This is the first combination and the second combination and so on.

122
00:08:14,745 --> 00:08:19,088
So pred DF1 is a dataframe where each row

123
00:08:19,088 --> 00:08:24,630
is combination of the four input columns.

124
00:08:24,630 --> 00:08:28,720
We're now going to use the rxPredict function

125
00:08:28,720 --> 00:08:30,570
to do what's called scoring.

126
00:08:30,570 --> 00:08:35,080
That is, we're going to give it the model that we built in

127
00:08:35,080 --> 00:08:39,260
the previous step when we ran our rxLinMod function.

128
00:08:39,260 --> 00:08:43,510
And we're gonna give it this dataset

129
00:08:43,510 --> 00:08:48,940
with all the columns, all the inputs used to build the model.

130
00:08:48,940 --> 00:08:53,460
And we're going to ask rxPredict to make predictions for

131
00:08:53,460 --> 00:08:57,120
each of these inputs, for each of these rows.

132
00:08:59,260 --> 00:09:03,364
So we're then going to do some renaming and

133
00:09:03,364 --> 00:09:06,905
what we end up with is the following.

134
00:09:06,905 --> 00:09:11,839
We can see the four columns that we started with right here and

135
00:09:11,839 --> 00:09:14,548
here is the prediction for tip and

136
00:09:14,548 --> 00:09:18,150
the standard error around the prediction.

137
00:09:18,150 --> 00:09:23,134
So we're predicting based on the model that someone who

138
00:09:23,134 --> 00:09:27,588
was picked up at Chinatown, who was dropped off,

139
00:09:27,588 --> 00:09:31,937
again,at Chinatown on a Sunday between 1 AM and

140
00:09:31,937 --> 00:09:35,877
5 AM is going to tip on average 5.72%.

141
00:09:35,877 --> 00:09:41,042
If everything else being equal, the person who was picked up,

142
00:09:41,042 --> 00:09:46,304
not from Chinatown, but from the Financial District instead,

143
00:09:46,304 --> 00:09:50,884
the model is predicting that the tip is going to be around

144
00:09:50,884 --> 00:09:53,850
11% instead of the 5.72%.

145
00:09:53,850 --> 00:09:58,840
So the model is definitely capturing some amount of variability.

146
00:09:58,840 --> 00:10:04,340
And we use the rxPredict function to essentially have the model go and

147
00:10:04,340 --> 00:10:07,200
make predictions for every combinations of the four

148
00:10:08,620 --> 00:10:10,940
input columns that we use to built the model.

