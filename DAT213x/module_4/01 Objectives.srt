0
00:00:00,008 --> 00:00:02,370
Welcome back.

1
00:00:02,370 --> 00:00:06,240
We're gonna get to the analysis and

2
00:00:06,240 --> 00:00:09,760
the analytics portion of the course in this module.

3
00:00:11,150 --> 00:00:13,404
So far, we've done a lot of data processing.

4
00:00:15,195 --> 00:00:17,795
And we've done a little bit of

5
00:00:17,795 --> 00:00:21,725
analysis by looking at various plots and charts.

6
00:00:21,725 --> 00:00:23,485
And if we go back,

7
00:00:23,485 --> 00:00:28,315
we'll notice that we've really been using a lot of the same functions.

8
00:00:28,315 --> 00:00:33,295
We used rx data step quite a bit in order to process the data.

9
00:00:33,295 --> 00:00:39,370
And then we used rx summary, rx cross steps, or rx cube to get

10
00:00:39,370 --> 00:00:44,380
counts, to get averages, to get various statistical summaries.

11
00:00:45,460 --> 00:00:49,330
What we haven't used yet, are probably

12
00:00:49,330 --> 00:00:53,645
the most important functions in the regal scale our package,

13
00:00:53,645 --> 00:00:56,267
namely the analytics algorithms.

14
00:00:56,267 --> 00:00:59,165
There is quite a few of those,

15
00:00:59,165 --> 00:01:01,745
starting with the clustering algorithm that we are gonna

16
00:01:01,745 --> 00:01:06,905
use as our first order of business in this module.

17
00:01:06,905 --> 00:01:10,655
And then moving on to some predictive modeling algorithms,

18
00:01:10,655 --> 00:01:15,860
such as linear models, decision trees, and random forests.

19
00:01:15,860 --> 00:01:22,030
These algorithms are highly efficient,

20
00:01:22,030 --> 00:01:26,250
scalable algorithms, because they are distributed, and

21
00:01:26,250 --> 00:01:29,850
because they work with large data sets,

22
00:01:29,850 --> 00:01:35,560
either large data sets sitting on a local computer, local machine,

23
00:01:35,560 --> 00:01:40,780
or large data sets that are stored in distributed

24
00:01:40,780 --> 00:01:46,475
formats on HTFS or inside of, say, SQL Server.

25
00:01:46,475 --> 00:01:54,345
So let's start with an example of a clustering algorithm.

26
00:01:54,345 --> 00:01:57,170
And to do that, we're gonna set the stage for

27
00:01:57,170 --> 00:01:59,585
why we want to use the algorithm.

28
00:01:59,585 --> 00:02:04,645
It's definitely, this might be a good place for us to talk

29
00:02:04,645 --> 00:02:11,875
a little bit about one of the underlying themes of this module.

30
00:02:11,875 --> 00:02:18,490
Namely the fact that as data scientists we have to

31
00:02:18,490 --> 00:02:24,890
sometimes ask ourselves, do we actually have a big data problem?

32
00:02:24,890 --> 00:02:30,840
What I mean by that is that statisticians

33
00:02:30,840 --> 00:02:35,220
know of ways that we can take a sample from the data.

34
00:02:35,220 --> 00:02:39,620
And from that sample derive conclusions and

35
00:02:39,620 --> 00:02:43,770
results that we can generalize to the big population.

36
00:02:43,770 --> 00:02:46,399
So, since we have a sample of the data,

37
00:02:46,399 --> 00:02:50,320
we're gonna be able to derive summaries from our sample.

38
00:02:50,320 --> 00:02:53,730
We're gonna be able to run analytics on the sample data.

39
00:02:53,730 --> 00:02:58,440
And we're gonna get pretty far, just because we have a sample of the data

40
00:02:58,440 --> 00:03:03,080
doesn't mean that we're just gonna be lost, right?

41
00:03:03,080 --> 00:03:08,037
There are, however, times when running on analytics on

42
00:03:08,037 --> 00:03:13,922
the totality of the data, as opposed to just the sample of the data,

43
00:03:13,922 --> 00:03:17,639
is gonna give us a certain amount of bang for

44
00:03:17,639 --> 00:03:23,245
our buck that we might not get if we were just using a sample data.

45
00:03:23,245 --> 00:03:27,320
So this is another sort of underlying theme in this module,

46
00:03:27,320 --> 00:03:30,988
is just wondering about that, wondering when we have

47
00:03:30,988 --> 00:03:35,360
a big data situation, when we have a big data problem.

48
00:03:35,360 --> 00:03:40,360
And wondering also what exactly is the analytics on the big

49
00:03:40,360 --> 00:03:44,370
data telling me that I might not be able to get, or

50
00:03:44,370 --> 00:03:47,100
I would have a really difficult time obtaining

51
00:03:47,100 --> 00:03:51,820
if I only had access to a subset of the data, to a sample of the data?

