0
00:00:00,940 --> 00:00:01,720
So let's get started.

1
00:00:03,220 --> 00:00:09,198
First thing we're gonna do is we're going to use the ggmap

2
00:00:09,198 --> 00:00:15,429
package which uses the Google API to obtain a couple of maps.

3
00:00:15,429 --> 00:00:21,550
In this case, the maps are going to be at different zoom levels.

4
00:00:21,550 --> 00:00:25,182
So one of them is gonna be slightly zoomed out, and

5
00:00:25,182 --> 00:00:30,025
the other one is gonna be a little bit of a closer look at a particular

6
00:00:30,025 --> 00:00:31,510
area of Manhattan.

7
00:00:32,510 --> 00:00:36,040
And we're going to go and use ggmap, and

8
00:00:36,040 --> 00:00:39,980
pass it the Manhattan sample data set that we have.

9
00:00:39,980 --> 00:00:45,080
So, we took a sample, a random sample of the Manhattan xdf file,

10
00:00:45,080 --> 00:00:50,410
mhdxdf and we stored it into mhdsampledf.

11
00:00:50,410 --> 00:00:56,040
And ggmap does not understand xdf files,

12
00:00:56,040 --> 00:00:59,500
so we can only use it with a data frame like this one.

13
00:01:01,170 --> 00:01:06,200
Of course, even if ggmap they did understand xdf files, drawing

14
00:01:06,200 --> 00:01:11,220
a plot like this using the totality of the data might be useless,

15
00:01:11,220 --> 00:01:16,240
because to many points would easily overcrowd the plot.

16
00:01:17,240 --> 00:01:20,590
So but the sample we can do that, we can go and

17
00:01:20,590 --> 00:01:26,020
place the individual drop-off locations based on longitude and

18
00:01:26,020 --> 00:01:29,460
latitude into the plot.

19
00:01:29,460 --> 00:01:33,360
And then we can look at what something like that would give us.

20
00:01:35,530 --> 00:01:42,520
So here's an example of a plot, based on the sample of the data.

21
00:01:42,520 --> 00:01:44,940
The plot on the left side and

22
00:01:44,940 --> 00:01:48,510
the plot on the right side are using the same data.

23
00:01:48,510 --> 00:01:54,000
It's just that the plot on the left side is zoomed out, and the plot

24
00:01:54,000 --> 00:02:00,550
on the right side is zooming in in the area that we're at.

25
00:02:00,550 --> 00:02:05,430
We can see that, in the plot, we have mostly the midtown

26
00:02:05,430 --> 00:02:08,460
neighborhoods, with some of the Upper West Side and

27
00:02:08,460 --> 00:02:11,210
the Upper East Side showing on the corners of the plot.

28
00:02:12,950 --> 00:02:16,770
And looking at the plot, we can see the red dots.

29
00:02:16,770 --> 00:02:19,860
Those are locations where drop-offs happened.

30
00:02:19,860 --> 00:02:26,630
And if I can go back to the code for the plot.

31
00:02:26,630 --> 00:02:27,432
In ggplot,

32
00:02:27,432 --> 00:02:32,254
we used the alpha argument to make the points a little bit transparent.

33
00:02:32,254 --> 00:02:36,735
And the transparency makes it so that If,

34
00:02:36,735 --> 00:02:42,631
we have too many points we can still kind of see the map.

35
00:02:42,631 --> 00:02:47,515
And once we get to certain locations where drop-offs are very be

36
00:02:47,515 --> 00:02:52,309
common we're gonna see points overlayed on top of each other,

37
00:02:52,309 --> 00:02:56,740
which is going to make it a little bit darker shade of red and

38
00:02:56,740 --> 00:02:59,580
we can identify those points.

39
00:02:59,580 --> 00:03:02,710
Okay, so from looking at the sample data

40
00:03:02,710 --> 00:03:06,030
we can see that even in the sample data, we can

41
00:03:06,030 --> 00:03:10,600
quickly identify certain points that are common drop-off locations.

42
00:03:10,600 --> 00:03:13,590
Let's begin with the plot on the left side.

43
00:03:13,590 --> 00:03:17,210
One of the things that we can see is that a lot of

44
00:03:17,210 --> 00:03:19,390
ships are happening more likely

45
00:03:20,490 --> 00:03:25,240
in the more vertical direction that represent the avenues.

46
00:03:25,240 --> 00:03:29,565
As opposed to the street level data, or

47
00:03:29,565 --> 00:03:35,341
the street data, which is more in the off diagonal.

48
00:03:35,341 --> 00:03:38,716
So the diagonal lines on the plot are the avenues and

49
00:03:38,716 --> 00:03:42,263
the off diagonal lines are the streets in Manhattan.

50
00:03:42,263 --> 00:03:47,802
If you're familiar with some of the geography of Manhattan.

51
00:03:47,802 --> 00:03:52,033
And so we can identify some of the streets by just looking

52
00:03:52,033 --> 00:03:55,013
at the way the red dots are aligning up.

53
00:03:55,013 --> 00:03:59,253
And we can go to let's say where my mouse is pointing to right now and

54
00:03:59,253 --> 00:04:03,344
we can see there are certain locations where drop-offs are very

55
00:04:03,344 --> 00:04:04,802
commonly happening.

56
00:04:04,802 --> 00:04:09,547
And just from the sample data we were able to identify some of these

57
00:04:09,547 --> 00:04:11,380
locations.

58
00:04:11,380 --> 00:04:14,680
If we look at the plot on the right side, we can see a more

59
00:04:14,680 --> 00:04:18,880
detailed view, because we're zooming in a little bit on the same plot.

60
00:04:18,880 --> 00:04:24,310
We can see a more detailed view and even on this plot we can see that

61
00:04:26,170 --> 00:04:30,130
already, certain locations are standing out.

62
00:04:30,130 --> 00:04:31,230
We can see here for

63
00:04:31,230 --> 00:04:35,000
example Columbus Circle as a common drop-off location.

64
00:04:35,000 --> 00:04:40,670
We can come down here to 8th avenue between let's say for

65
00:04:40,670 --> 00:04:43,299
around 42nd street and

66
00:04:43,299 --> 00:04:47,970
38th street a lot of drop-offs are happening there and so on.

67
00:04:47,970 --> 00:04:52,765
So, it's an interesting kind of a way to set the stage

68
00:04:52,765 --> 00:04:57,345
just looking at the data, looking at what the sample is telling us.

69
00:04:57,345 --> 00:05:01,985
And we might be wondering now is there more that we could do?

70
00:05:01,985 --> 00:05:05,825
Is there something that we could do with the totality of the data

71
00:05:05,825 --> 00:05:08,345
that's gonna tell us something that

72
00:05:08,345 --> 00:05:10,805
the sample is not gonna be able to tell us?

