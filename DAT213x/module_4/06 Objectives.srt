0
00:00:00,370 --> 00:00:01,770
Now we're gonna shift gear and

1
00:00:01,770 --> 00:00:04,480
we're gonna look at something a little bit different.

2
00:00:04,480 --> 00:00:09,060
We're gonna build our first supervised learning algorithm.

3
00:00:09,060 --> 00:00:09,670
In this case,

4
00:00:09,670 --> 00:00:14,160
we're gonna run a little example of a linear regression problem.

5
00:00:15,760 --> 00:00:18,361
In supervised learning algorithms,

6
00:00:18,361 --> 00:00:21,295
we have something that we need to predict.

7
00:00:21,295 --> 00:00:25,440
And so, we all ready have the correct answer right in front of us.

8
00:00:25,440 --> 00:00:27,300
It's gonna be whatever we're trying to predict.

9
00:00:27,300 --> 00:00:31,680
The algorithm is gonna use whatever data that we

10
00:00:31,680 --> 00:00:35,980
throw at it to try to come up with a way to make predictions.

11
00:00:35,980 --> 00:00:40,220
And obviously, the closer the predictions come

12
00:00:40,220 --> 00:00:44,610
to the actual observed values, the better the model is going to be.

