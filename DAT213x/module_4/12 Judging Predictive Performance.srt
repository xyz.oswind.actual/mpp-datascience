0
00:00:01,460 --> 00:00:06,830
So we looked at our predictions made by the three models and

1
00:00:06,830 --> 00:00:09,370
we compared them to averages for

2
00:00:09,370 --> 00:00:12,330
their combinations of the four input columns.

3
00:00:12,330 --> 00:00:19,250
But of course, this is just because we have a lot of data, and we wanted

4
00:00:19,250 --> 00:00:23,030
to be able to just get a sense of what the predictions are doing.

5
00:00:23,030 --> 00:00:28,180
But really the true test of how the model is doing

6
00:00:29,400 --> 00:00:33,649
would be to take the model and to run the predictions

7
00:00:34,900 --> 00:00:39,870
by each of the models on the 25% of the data that we set aside,

8
00:00:39,870 --> 00:00:42,640
what we called the test data.

9
00:00:42,640 --> 00:00:45,070
So we're gonna do that in the following step.

10
00:00:47,170 --> 00:00:52,190
So we're here back with our rxPredict function, but

11
00:00:52,190 --> 00:00:54,160
now we're gonna used it slightly differently.

12
00:00:54,160 --> 00:00:56,560
Before when we were using rxPredict,

13
00:00:56,560 --> 00:01:00,920
we were running rxPredict on a data frame, so this is an example that.

14
00:01:01,930 --> 00:01:04,920
And so, we took the model the we built, and

15
00:01:04,920 --> 00:01:08,740
we passed to the data frame pred_df with all the combinations

16
00:01:08,740 --> 00:01:13,310
of the input columns, and we ran predictions on that, and

17
00:01:13,310 --> 00:01:16,410
the output would just be another data frame.

18
00:01:16,410 --> 00:01:21,236
So we just called it in this case pred_df1.

19
00:01:21,236 --> 00:01:25,350
When When we set aside 25% of the test data,

20
00:01:25,350 --> 00:01:29,160
we set that aside as an XDF file.

21
00:01:29,160 --> 00:01:32,340
So now we're gonna run our predictions on an XDF file, and

22
00:01:32,340 --> 00:01:36,510
it's going to potentially be a very large file, all right?

23
00:01:36,510 --> 00:01:40,080
So we're not necessarily interested in running predictions and

24
00:01:40,080 --> 00:01:41,850
dumping the results in the data frame,

25
00:01:41,850 --> 00:01:44,940
we want the results to simply go back into the XDF file.

26
00:01:44,940 --> 00:01:50,730
So we're gonna run rxPredict without doing any assignments to the left.

27
00:01:50,730 --> 00:01:54,750
We're gonna pass it, the model, three different times,

28
00:01:54,750 --> 00:01:57,920
the linear model, decision tree, and the random forest.

29
00:01:59,600 --> 00:02:03,280
And notice here that the data that we give it is the test data.

30
00:02:03,280 --> 00:02:08,360
So, we built the model if we go back up,

31
00:02:08,360 --> 00:02:13,670
we can see that we built the model on the training data, and

32
00:02:13,670 --> 00:02:19,520
now we are running predictions on the test data.

33
00:02:19,520 --> 00:02:21,470
Once the predictions are obtained,

34
00:02:21,470 --> 00:02:24,400
we're simply gonna put them back into the test data

35
00:02:24,400 --> 00:02:28,555
which is why we have to specify that override equals true.

36
00:02:28,555 --> 00:02:33,425
And we're also going to specify the name of the column in

37
00:02:33,425 --> 00:02:36,925
the test data that we're gonna give to the predictions, to each of

38
00:02:36,925 --> 00:02:40,439
the predictions so that we can tell them apart from each other.

39
00:02:41,560 --> 00:02:44,384
So I'm going to run this.

40
00:02:44,384 --> 00:02:49,167
And once again we can see that this runs fairly quickly, in the case of

41
00:02:49,167 --> 00:02:54,115
the linear model, that it doesn't run as quickly for the decision tree

42
00:02:54,115 --> 00:02:58,343
but certainly quick enough In the case of the random forest.

43
00:02:58,343 --> 00:03:02,588
However, it does take a while to get the predictions because of

44
00:03:02,588 --> 00:03:06,834
the amount of computation that's happening under the hood for

45
00:03:06,834 --> 00:03:08,930
us to obtain the predictions.

46
00:03:10,100 --> 00:03:13,520
Once we have the predictions, we can now look at

47
00:03:15,640 --> 00:03:19,640
different metrics to see how well the predictions are.

48
00:03:19,640 --> 00:03:24,249
There's different ways of deciding of how we're going to

49
00:03:24,249 --> 00:03:27,644
judge the model based on the predictions.

50
00:03:27,644 --> 00:03:31,341
Certainly to some extent, it depends whether we're making numeric

51
00:03:31,341 --> 00:03:35,103
predictions which we are here, or whether we're making categorical

52
00:03:35,103 --> 00:03:37,540
predictions in the case of classification.

53
00:03:39,050 --> 00:03:42,780
And even in the case of numeric predictions, we have

54
00:03:42,780 --> 00:03:46,800
different ways that we can decide how accurate the predictions are,

55
00:03:46,800 --> 00:03:50,780
how much they come close to the actual predictions.

56
00:03:51,780 --> 00:03:57,030
So we have two very basic examples here that we're gonna cover.

57
00:03:57,030 --> 00:04:02,342
But this is definitely an open-ended field and

58
00:04:02,342 --> 00:04:06,591
there are different ways that we could

59
00:04:06,591 --> 00:04:11,520
have done this to judge our predictions by.

60
00:04:13,660 --> 00:04:21,270
In our case, we're gonna start by running a transformation.

61
00:04:21,270 --> 00:04:26,920
And the transformation is going to give us the squared residuals for

62
00:04:26,920 --> 00:04:28,580
each of the models.

63
00:04:28,580 --> 00:04:33,310
That is to say that we're gonna look at the observed 10%,

64
00:04:33,310 --> 00:04:37,110
we're going to subtract the prediction made by each model,

65
00:04:37,110 --> 00:04:38,920
in this case the linear model.

66
00:04:38,920 --> 00:04:42,180
In the next step the decision tree, and

67
00:04:42,180 --> 00:04:47,100
lastly the random forest, and we're gonna that's called a residual, and

68
00:04:47,100 --> 00:04:48,050
we're going to square it.

69
00:04:49,470 --> 00:04:50,745
And the column,

70
00:04:50,745 --> 00:04:55,666
in the case of the linear model is gonna be called squared error,

71
00:04:55,666 --> 00:05:00,238
so selin.mod, and then we have sed tree, and sed forest.

72
00:05:00,238 --> 00:05:06,188
We have three different columns that we're gonna obtain,

73
00:05:06,188 --> 00:05:11,543
and we're gonna ask rx Summary to then give us summary

74
00:05:11,543 --> 00:05:16,202
statistics for the three different columns.

75
00:05:16,202 --> 00:05:18,160
We're gonna do this on the fly as you can see,

76
00:05:18,160 --> 00:05:22,460
the transformation is happening in the same rx summary call, so

77
00:05:22,460 --> 00:05:24,460
the transformation is gonna be on the fly.

78
00:05:24,460 --> 00:05:27,110
We're not going to put the residuals back in the data and

79
00:05:27,110 --> 00:05:30,340
this is another example of a transformation that makes sense for

80
00:05:30,340 --> 00:05:34,250
us to perform on the fly like this, simply because we can.

81
00:05:34,250 --> 00:05:36,904
Once we have the predictions and the data,

82
00:05:36,904 --> 00:05:39,863
we can obtain the residuals anytime we want to.

83
00:05:39,863 --> 00:05:45,993
So this is gonna be one of the ways that we're gonna look at how well

84
00:05:45,993 --> 00:05:52,260
each of the models are doing on the task of predicting tip percent.

85
00:05:52,260 --> 00:05:56,730
Another way that we're gonna look at it is looking at correlations.

86
00:05:56,730 --> 00:06:02,190
So RX core is the function in this case, and we're gonna ask for

87
00:06:02,190 --> 00:06:08,630
a correlation matrix with the four following columns.

88
00:06:08,630 --> 00:06:12,200
One is the observed value, called tip percent, and

89
00:06:12,200 --> 00:06:16,810
then the three predictions that the three different models are making.

90
00:06:18,370 --> 00:06:22,470
And once again the correlations that

91
00:06:22,470 --> 00:06:27,900
we wanna get are the correlations on the test data.

92
00:06:27,900 --> 00:06:32,650
This is still running, so I'm gonna wait for this to finish running.

93
00:06:34,980 --> 00:06:39,230
Our random forest is done scoring the test data, so

94
00:06:39,230 --> 00:06:40,790
we can now run our rxSummary.

95
00:06:42,810 --> 00:06:45,870
And it runs pretty quickly.

96
00:06:45,870 --> 00:06:50,735
And we can see that here we have the average for

97
00:06:50,735 --> 00:06:55,345
this quit residuals as well as the standard

98
00:06:55,345 --> 00:06:59,699
deviation made by the linear model, for

99
00:06:59,699 --> 00:07:04,327
the predictions made by the linear model.

100
00:07:04,327 --> 00:07:09,093
And we can see that we have certain missing observations,

101
00:07:09,093 --> 00:07:14,452
this is probably because of missing the use in the input columns.

102
00:07:14,452 --> 00:07:18,776
What's surprising is that the numbers are very close to each

103
00:07:18,776 --> 00:07:19,650
other.

104
00:07:19,650 --> 00:07:24,530
So whether we look at the linear model, the decision tree or

105
00:07:24,530 --> 00:07:27,970
the random forest, it seems like the random forest is better,

106
00:07:27,970 --> 00:07:30,510
but only very very slightly so.

107
00:07:31,710 --> 00:07:36,540
So, at this point, we might be quick to decide

108
00:07:36,540 --> 00:07:39,420
that the random forest is the better model and

109
00:07:39,420 --> 00:07:42,210
let's just go with that, but of course it's not that easy.

110
00:07:42,210 --> 00:07:46,050
We saw for example, that even though the random forest might do

111
00:07:46,050 --> 00:07:51,560
a slightly better job of making predictions, it's also doing so

112
00:07:51,560 --> 00:07:56,360
at the cost of a bigger run time.

113
00:07:56,360 --> 00:07:59,290
So a random forest model is gonna mean

114
00:07:59,290 --> 00:08:03,310
we have to pay some computational cost, and we

115
00:08:03,310 --> 00:08:07,315
are also gonna be waiting a little bit longer to get our predictions.

116
00:08:07,315 --> 00:08:11,865
This might be important for us, and so it might not, it might be

117
00:08:11,865 --> 00:08:16,165
better for us to not quickly jump to picking the better model here.

118
00:08:16,165 --> 00:08:20,305
Another thing might be that even though

119
00:08:20,305 --> 00:08:23,395
the random forest appears to be slightly better that the other two,

120
00:08:23,395 --> 00:08:29,180
it could be that all three of our models are not good.

121
00:08:29,180 --> 00:08:33,110
And so the fact that one of them is slightly better than the other ones,

122
00:08:33,110 --> 00:08:36,110
doesn't necessarily mean that it's a good model to begin with.

123
00:08:37,830 --> 00:08:42,720
We can confirm that intuition by looking at our correlations.

124
00:08:42,720 --> 00:08:47,913
Here when we look at correlations, we can see tip percent here in

125
00:08:47,913 --> 00:08:53,492
this column, and so the correlation between the predictions made by

126
00:08:53,492 --> 00:08:58,587
the linear model, the decision tree, and the random forest,

127
00:08:58,587 --> 00:09:02,755
and the observed tip percent are all pretty small.

128
00:09:02,755 --> 00:09:06,929
We can see that the correlation in the case of the random forest is

129
00:09:06,929 --> 00:09:11,510
slightly higher than the other two, but once again only slightly so.

130
00:09:12,760 --> 00:09:16,580
We can also see that the correlation between

131
00:09:17,850 --> 00:09:21,835
the actual predictions made by the three models,

132
00:09:21,835 --> 00:09:24,890
in all cases are pretty close to each other.

133
00:09:24,890 --> 00:09:29,020
The random forest is making predictions that

134
00:09:29,020 --> 00:09:33,390
are very close to the linear model, and pretty close but

135
00:09:33,390 --> 00:09:36,440
not as close, to the decision tree.

136
00:09:36,440 --> 00:09:42,080
And we can see that the correlation between the decision

137
00:09:42,080 --> 00:09:47,320
tree and the linear model is going to be

138
00:09:47,320 --> 00:09:51,610
shown right here, and that's also pretty close.

139
00:09:52,900 --> 00:09:58,580
So it seems like what we're faced with is a situation

140
00:09:58,580 --> 00:10:04,340
where we have three different models, all three of them

141
00:10:04,340 --> 00:10:09,000
not necessarily doing a good job of getting close to the predictions.

142
00:10:09,000 --> 00:10:12,325
But all three of them nevertheless making

143
00:10:12,325 --> 00:10:17,177
predictions that are reasonable, and close to each other.

144
00:10:17,177 --> 00:10:21,938
All three models are certainly ultimately coming up with

145
00:10:21,938 --> 00:10:25,870
the same predictions at the end of the day.

146
00:10:25,870 --> 00:10:29,380
This is an indication that we should probably go back and

147
00:10:29,380 --> 00:10:35,240
ask how can we make these models richer by adding other features,

148
00:10:35,240 --> 00:10:37,845
by adding other inputs to the models.

149
00:10:37,845 --> 00:10:44,280
And we discussed about how we could probably be adding payment type

150
00:10:44,280 --> 00:10:48,720
to make predictions for tip percent, because tip percent is always gonna

151
00:10:48,720 --> 00:10:54,740
be zero in the case of customers who paid in cash.

152
00:10:54,740 --> 00:10:58,970
Another way we could do this is, is we can just think about just

153
00:10:58,970 --> 00:11:02,610
running the model, not on the whole of the data, but

154
00:11:02,610 --> 00:11:06,330
only on the subset of the data where a customer is paying in card.

155
00:11:07,390 --> 00:11:09,840
So there's different approaches to be taken here, and

156
00:11:09,840 --> 00:11:14,840
this is certainly not a course about how to come up with a perfect model.

157
00:11:14,840 --> 00:11:20,100
We have given you just enough to kind of inspire you to try

158
00:11:20,100 --> 00:11:25,639
different things, and to see how Reboscalar and

159
00:11:25,639 --> 00:11:31,715
it's algorithms can be used to create models and make predictions.

160
00:11:31,715 --> 00:11:37,305
But we're gonna leave the task of improving on the models and

161
00:11:37,305 --> 00:11:40,580
being able to actually come up with better predictions,

162
00:11:40,580 --> 00:11:43,970
we're gonna leave some of that to the homework exercises so

163
00:11:43,970 --> 00:11:46,380
that you can get your hands dirty a little bit.

164
00:11:46,380 --> 00:11:51,660
And we're also going to leave off a lot of the topics that

165
00:11:51,660 --> 00:11:57,000
usually go in a modelling class, such as for example, the task of

166
00:11:57,000 --> 00:12:00,640
building more rigorous models using the technique of cross-validation.

167
00:12:04,040 --> 00:12:09,610
So we hope that you've kind of enjoyed looking at how

168
00:12:09,610 --> 00:12:15,590
we can use Reboscale r on large data sets, building predictive models,

169
00:12:15,590 --> 00:12:18,560
using algorithms that are parallel and scalable.

170
00:12:18,560 --> 00:12:21,220
We haven't actually seen

171
00:12:21,220 --> 00:12:26,060
the scalability of these algorithms in action, because we've really

172
00:12:26,060 --> 00:12:30,690
just been using a small portion of the New York City taxi data.

173
00:12:30,690 --> 00:12:35,710
Remember that, first of all, we looked at six months of data, and

174
00:12:35,710 --> 00:12:39,070
we didn't look at the totality of the first,

175
00:12:39,070 --> 00:12:43,710
of the six months of the data, we looked at a 5% sample of that.

176
00:12:43,710 --> 00:12:48,260
And that's because we were using the ARC client, and

177
00:12:48,260 --> 00:12:51,590
our goal was to be able to learn about the functions and

178
00:12:51,590 --> 00:12:55,550
to be able to run them in a reasonable time frame, and

179
00:12:55,550 --> 00:12:58,330
see how the functions are used and practiced.

180
00:12:58,330 --> 00:13:04,190
In the next module, we're actually gonna see what scalability means

181
00:13:04,190 --> 00:13:09,130
when we look at using not the R client,

182
00:13:09,130 --> 00:13:12,870
but the actual Microsoft R Server,

183
00:13:12,870 --> 00:13:17,430
which is the beefy version of which the R Client is just a lightweight

184
00:13:18,670 --> 00:13:23,300
kind of development tool.

185
00:13:23,300 --> 00:13:27,700
And we're gonna do it in three different compute contexts,

186
00:13:27,700 --> 00:13:30,240
the first compute context is just gonna

187
00:13:30,240 --> 00:13:34,820
be running Microsoft R Client on a single machine, on a server.

188
00:13:34,820 --> 00:13:40,230
The second one is gonna be running Microsoft R Client in Spark,

189
00:13:40,230 --> 00:13:45,189
and looking at how this problem can scale when we use

190
00:13:45,189 --> 00:13:50,530
a cluster, and have the data distributed on the cluster.

191
00:13:50,530 --> 00:13:56,300
And in the third compute context, we're gonna be using SQL Server to

192
00:13:57,410 --> 00:14:02,140
see how SQL Server is gonna perform when we throw this problem at it.

193
00:14:03,280 --> 00:14:04,123
So, we'll see you there.

