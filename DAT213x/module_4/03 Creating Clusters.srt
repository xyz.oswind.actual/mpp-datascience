0
00:00:01,390 --> 00:00:05,500
If you're not familiar with K means, K means is an algorithm that says

1
00:00:06,910 --> 00:00:11,990
given a set of cases, let's say rows

2
00:00:11,990 --> 00:00:17,130
in the data and a set of attributes, let's say columns in the data.

3
00:00:17,130 --> 00:00:22,780
I'm gonna go and tell you which cases are similar to each other

4
00:00:22,780 --> 00:00:28,870
on account of their attributes, more or less being close to each other.

5
00:00:28,870 --> 00:00:33,490
There is a bit of subjectivity involved and

6
00:00:33,490 --> 00:00:37,067
it's obviously not perfect.

7
00:00:37,067 --> 00:00:40,425
Depending on the use case,

8
00:00:40,425 --> 00:00:44,935
we could end up spending quite a bit of time developing clusters or

9
00:00:44,935 --> 00:00:49,763
we could kind of let the clusters have an easy to interpret,

10
00:00:49,763 --> 00:00:55,670
but the clusters be easy to interpret.

11
00:00:55,670 --> 00:00:58,790
In this case our clusters are actually pretty easy to interpret.

12
00:00:58,790 --> 00:01:02,250
We have the data, we were looking at it on the map.

13
00:01:02,250 --> 00:01:05,550
We know that the data is just a set of coordinates

14
00:01:05,550 --> 00:01:08,190
that represent drop off locations.

15
00:01:08,190 --> 00:01:13,360
And we're going to take that data and we're going to build clusters

16
00:01:13,360 --> 00:01:17,700
from the data so that instead of the drop off location being

17
00:01:17,700 --> 00:01:21,426
an individual drop off location from a single thread taxi trip,

18
00:01:21,426 --> 00:01:26,810
we can kind of take all the points that are close to each other and

19
00:01:26,810 --> 00:01:30,260
cluster them together in the same cluster.

20
00:01:30,260 --> 00:01:33,130
So that they can represent a single point

21
00:01:33,130 --> 00:01:38,570
where drop offs are very common, commonly happening.

22
00:01:38,570 --> 00:01:43,707
To get our K means to work, we're gonna once again start

23
00:01:43,707 --> 00:01:49,730
with the sample data and then we're gonna move on to the big data.

24
00:01:50,980 --> 00:01:55,420
The reason is that K means is somewhat

25
00:01:55,420 --> 00:01:59,330
dependent on the way that we initialize our clusters.

26
00:01:59,330 --> 00:02:01,270
Here, we have the sample of the data.

27
00:02:01,270 --> 00:02:04,170
And there are two things that we need to do

28
00:02:04,170 --> 00:02:05,810
to make the clusters work.

29
00:02:07,020 --> 00:02:10,270
Before doing anything, let me actually start running the code

30
00:02:10,270 --> 00:02:12,950
because this is gonna take a little bit of time to run.

31
00:02:14,380 --> 00:02:17,260
In our sample data the first thing that we're gonna do is we're

32
00:02:17,260 --> 00:02:21,840
going to take the drop off longitude and the drop off latitude column,

33
00:02:21,840 --> 00:02:25,290
these are the only attributes that we're using to

34
00:02:25,290 --> 00:02:29,930
build the clusters with and we're going to standardize them.

35
00:02:29,930 --> 00:02:34,319
Now usually the way that we standardize things is we would

36
00:02:34,319 --> 00:02:37,063
either subtract the mean from it and

37
00:02:37,063 --> 00:02:41,825
divide by the standard deviation or some other fancy method.

38
00:02:41,825 --> 00:02:46,517
In this case we're just going to divide the longitudes by

39
00:02:46,517 --> 00:02:51,220
negative 74, divide the latitudes by positive 40.

40
00:02:51,220 --> 00:02:56,440
And this will more or less put these two columns on the same scale.

41
00:02:56,440 --> 00:02:58,560
And that's the only thing that we intend to do,

42
00:02:58,560 --> 00:03:01,640
is we want the columns to be on the same scale so

43
00:03:01,640 --> 00:03:06,969
that they both have an equal say in determining the clusters.

44
00:03:08,640 --> 00:03:12,960
Next, we're gonna take this XY data that

45
00:03:12,960 --> 00:03:18,100
we just created with the standardize longitude and latitude columns.

46
00:03:18,100 --> 00:03:22,230
And we're going to build 300 clusters.

47
00:03:22,230 --> 00:03:24,940
This is where the subjectivity comes in.

48
00:03:24,940 --> 00:03:28,560
The number 300 in this case was kind of picked.

49
00:03:29,620 --> 00:03:34,840
Not completely randomly, but based some intuition that there should

50
00:03:34,840 --> 00:03:41,590
be around 300 very common drop off locations on Manhattan Island for

51
00:03:41,590 --> 00:03:43,700
taxis to drop off passengers.

52
00:03:45,110 --> 00:03:48,340
The next thing we're gonna do is we're going to

53
00:03:48,340 --> 00:03:50,900
set the maximum number of iterations.

54
00:03:50,900 --> 00:03:53,380
Clustering is an iterative algorithms,

55
00:03:53,380 --> 00:03:55,550
K means clustering is an iterative algorithm.

56
00:03:56,800 --> 00:04:00,950
We're gonna say that if after 2,000 iterations you haven't converged yet

57
00:04:00,950 --> 00:04:02,130
then you should probably give up.

58
00:04:03,550 --> 00:04:09,580
And then this is where some important argument

59
00:04:09,580 --> 00:04:13,460
is used and the reason were gonna start with the sample of the data.

60
00:04:13,460 --> 00:04:15,760
We have this argument called N start.

61
00:04:15,760 --> 00:04:18,290
And here were not notice that were not using by the way

62
00:04:18,290 --> 00:04:20,940
the RX K means function, were using the K means

63
00:04:20,940 --> 00:04:24,120
function that is part of the open source to our package.

64
00:04:25,430 --> 00:04:28,860
And K means as an argument called the N start.

65
00:04:28,860 --> 00:04:35,270
What N start does, is it says because the final clusters that we

66
00:04:35,270 --> 00:04:42,320
end up with is somewhat dependent on the initialization of the clusters.

67
00:04:42,320 --> 00:04:45,170
I'm going to do this 50 different times.

68
00:04:45,170 --> 00:04:47,940
So that at the end I can average out the results and

69
00:04:47,940 --> 00:04:49,930
get some pretty stable clusters.

70
00:04:52,480 --> 00:04:55,700
In other words, we're gonna run the clustering code, in this case,

71
00:04:55,700 --> 00:04:58,830
50 different times on the sample data.

72
00:04:58,830 --> 00:05:02,310
And we're gonna end up with some clustered centroids that we can be

73
00:05:02,310 --> 00:05:06,110
relatively confident are gonna be stable.

74
00:05:06,110 --> 00:05:10,270
Stable in the sense that if we run the clustering code a second time,

75
00:05:10,270 --> 00:05:12,930
we're not going to end up with dramatically different

76
00:05:12,930 --> 00:05:14,030
cluster centroids.

77
00:05:15,820 --> 00:05:18,270
Let's look at what the cluster centroids look like,

78
00:05:18,270 --> 00:05:22,780
to do that we're just gonna go to this object

79
00:05:22,780 --> 00:05:27,992
that the K means function returns and we're gonna drill inside of it.

80
00:05:27,992 --> 00:05:31,820
There was a object inside called centers and

81
00:05:31,820 --> 00:05:34,560
so we can go that object.

82
00:05:34,560 --> 00:05:40,060
Convert it into a dataframe and then we can do the transformations

83
00:05:40,060 --> 00:05:44,780
that we did to standardize the columns in the reverse direction to

84
00:05:44,780 --> 00:05:49,960
get the original longitude and latitude columns back.

85
00:05:49,960 --> 00:05:53,820
In addition we're gonna have a column that says this is the size

86
00:05:53,820 --> 00:05:57,950
of each of the clusters, so we have 300 clusters.

87
00:05:57,950 --> 00:06:01,960
And this data frame called centroids sample.

88
00:06:01,960 --> 00:06:07,470
In fact, I can display the entire data frame here on the R console.

89
00:06:07,470 --> 00:06:10,880
And so we can see here that starting at the very top,

90
00:06:12,250 --> 00:06:16,710
the first cluster as the following centroids and

91
00:06:16,710 --> 00:06:21,050
they are 110 different points inside of it.

92
00:06:21,050 --> 00:06:24,120
The second cluster has the following centroids and

93
00:06:24,120 --> 00:06:27,670
its size is 105 and so on.

94
00:06:27,670 --> 00:06:34,030
If we scroll down we can see that some clusters are relatively small

95
00:06:34,030 --> 00:06:38,890
about the smallest that I see here is maybe this one with 36.

96
00:06:38,890 --> 00:06:44,220
Some clusters are bigger, I think the biggest one I've seen so

97
00:06:44,220 --> 00:06:48,044
far is maybe this one, 198.

98
00:06:49,310 --> 00:06:51,390
And we have a good range in between, but

99
00:06:51,390 --> 00:06:54,300
we don't have any clusters that are drastically too big and

100
00:06:54,300 --> 00:06:57,790
we don't have any clusters that are drastically too small.

101
00:06:57,790 --> 00:06:59,760
And that also makes intuitive sense.

102
00:06:59,760 --> 00:07:02,400
We don't expect to have any locations that are so

103
00:07:02,400 --> 00:07:05,740
common that they're going to overwhelm the rest of the data.

104
00:07:05,740 --> 00:07:10,510
We expect to see a lot of common locations but not one that is going

105
00:07:10,510 --> 00:07:16,220
to be taking up the biggest, the lions share of the trips.

106
00:07:17,810 --> 00:07:20,160
Let's recap what we've done so far.

107
00:07:20,160 --> 00:07:24,630
We're not done, so far we've only used a sample of the data.

108
00:07:25,950 --> 00:07:30,890
Using a sample of the data, we fed it to the K means function in R,

109
00:07:30,890 --> 00:07:32,880
which only understands the dataframes,

110
00:07:32,880 --> 00:07:36,250
that's why we have to work with the sample data.

111
00:07:36,250 --> 00:07:40,140
And we built clusters on the sample data.

112
00:07:41,480 --> 00:07:45,740
We used the N start argument and set it to 50 so

113
00:07:45,740 --> 00:07:48,380
that we would do this multiple times.

114
00:07:48,380 --> 00:07:51,660
Making sure that the clusters that we end up with

115
00:07:51,660 --> 00:07:52,710
are relatively stable.

116
00:07:55,080 --> 00:07:58,853
Finally, we end up with the cluster centroids, and

117
00:07:58,853 --> 00:08:02,647
we can take a look at what the cluster centroids are.

118
00:08:02,647 --> 00:08:07,370
But we're gonna do now, something else with the centroids.

119
00:08:07,370 --> 00:08:11,650
We're gonna now run the K, the RX K means function

120
00:08:11,650 --> 00:08:16,730
which is the equivalent to the K means function in base R but

121
00:08:16,730 --> 00:08:22,310
it's the RevoScaleR function so even if it's equivalent, it's a parallel

122
00:08:22,310 --> 00:08:25,990
algorithm and it's an algorithm that scales the size of the data.

123
00:08:27,340 --> 00:08:31,170
We're gonna run RX K means on our XTF file.

124
00:08:33,540 --> 00:08:37,710
Let me start by running the code and

125
00:08:37,710 --> 00:08:40,110
we can talk about what's going on while the code is running.

126
00:08:43,990 --> 00:08:48,650
We're now using our RX K means and the two columns that we're gonna

127
00:08:48,650 --> 00:08:53,330
use just as before are gonna be the longitude and latitude but

128
00:08:53,330 --> 00:08:57,810
these two columns are not standardized in the data.

129
00:08:57,810 --> 00:09:01,040
We're going to standardize them by performing a transformation

130
00:09:01,040 --> 00:09:01,980
on the flyl.

131
00:09:01,980 --> 00:09:05,980
The transformation is happening here, and just as before

132
00:09:05,980 --> 00:09:08,650
we're taking longitude, dividing it by negative 74,

133
00:09:08,650 --> 00:09:12,260
and we're taking latitude and dividing it by 40.

134
00:09:12,260 --> 00:09:16,180
We're calling the resulting columns a long STD and

135
00:09:16,180 --> 00:09:20,100
lat STD and those are the two columns that we're using out there

136
00:09:20,100 --> 00:09:22,520
to build our clusters with.

137
00:09:23,760 --> 00:09:28,920
This is the data and we're using the DXDF file the big data as

138
00:09:28,920 --> 00:09:33,910
our data and when the clusters are finished it's going

139
00:09:33,910 --> 00:09:38,860
to create a new column that's going to be put back in the same XDF file.

140
00:09:38,860 --> 00:09:45,279
The column is going to be called dropoff clusters, dropoff cluster.

141
00:09:45,279 --> 00:09:50,595
And this is where the sample data that we used

142
00:09:50,595 --> 00:09:55,775
to extract the cluster of centroids width is

143
00:09:57,105 --> 00:10:00,205
being used as the centroids for

144
00:10:00,205 --> 00:10:04,546
the initial iteration of the RX K means function.

145
00:10:06,565 --> 00:10:10,930
In the last step, we took the sample data,

146
00:10:10,930 --> 00:10:15,996
we gave it to the K means function, we ended up with some centroids.

147
00:10:15,996 --> 00:10:20,840
And the centroids in

148
00:10:20,840 --> 00:10:25,795
their standardized form are stored in this object called RX KM

149
00:10:25,795 --> 00:10:30,850
sample $ centers and we are taking that object and

150
00:10:30,850 --> 00:10:35,940
we are putting it here so that the RX K means function

151
00:10:35,940 --> 00:10:40,740
can initialize using the centroids obtained from the sample.

152
00:10:42,930 --> 00:10:48,710
This is what we're doing here is we are making the RX K means function

153
00:10:48,710 --> 00:10:54,870
run only for a single iteration and it's gonna run more efficiently

154
00:10:54,870 --> 00:10:58,870
because it doesn't have to figure out the best way to initialize.

155
00:10:58,870 --> 00:11:03,094
We're gonna let it know the best way to initialize by

156
00:11:03,094 --> 00:11:07,517
telling it what the initial centroids are going to be.

157
00:11:07,517 --> 00:11:12,602
Finally, because we're putting the clusters as a new column

158
00:11:12,602 --> 00:11:17,897
into the original data, we're gonna say blocksPerRead = 1.

159
00:11:17,897 --> 00:11:20,220
This is just one of the requirements if we do so.

160
00:11:21,260 --> 00:11:24,090
And we also need to say that overwrite equals true.

161
00:11:25,340 --> 00:11:29,130
We're gonna set the maximum number of iterations, in this case three

162
00:11:29,130 --> 00:11:33,080
500, and report progress equals negative one is just gonna make it,

163
00:11:33,080 --> 00:11:37,830
so that as the code is running, it's not gonna generate too many results.

164
00:11:37,830 --> 00:11:41,270
It's not gonna give us a progress report as it's happening, we're just

165
00:11:41,270 --> 00:11:43,940
gonna wait for it to finish and we'll know when it's done.

166
00:11:46,030 --> 00:11:49,555
If I hold my mouse here on the left side, you can see from the way that

167
00:11:49,555 --> 00:11:54,410
the mouse changes to the circle that the code is still running.

168
00:11:55,540 --> 00:11:59,630
When this finishes running, we're gonna come back and

169
00:11:59,630 --> 00:12:01,980
we're gonna visualize the clusters.

170
00:12:01,980 --> 00:12:05,521
We're gonna look at the centroids, both from the sample and

171
00:12:05,521 --> 00:12:08,720
the centroids that we obtain when we run the clusters

172
00:12:08,720 --> 00:12:11,380
on the whole of the data.

173
00:12:11,380 --> 00:12:14,095
And we're gonna talk about some of the differences and

174
00:12:14,095 --> 00:12:15,870
the similarities that we notice.

