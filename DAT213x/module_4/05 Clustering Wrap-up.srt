0
00:00:01,300 --> 00:00:05,440
We hope you had fun playing around with the clustering code.

1
00:00:05,440 --> 00:00:07,280
There's a lot of things we can do.

2
00:00:07,280 --> 00:00:10,930
We can change the number of clusters.

3
00:00:10,930 --> 00:00:15,190
We can try using a larger sample and compare the results from

4
00:00:15,190 --> 00:00:18,560
the larger sample, or a smaller sample for that matter,

5
00:00:18,560 --> 00:00:22,470
with the results that we get when we use the whole of the data to build.

6
00:00:22,470 --> 00:00:23,910
The clusters with.

7
00:00:23,910 --> 00:00:27,140
Of course keep in mind in this case that when we're talking about

8
00:00:27,140 --> 00:00:31,610
the whole of the data, because we're limited to using the Microsoft R

9
00:00:31,610 --> 00:00:35,360
client which is only meant for developing code and

10
00:00:35,360 --> 00:00:39,240
playing out with the scalar functions.

11
00:00:39,240 --> 00:00:44,100
We still are not able to run this on the entirety of the data that

12
00:00:44,100 --> 00:00:46,650
is under the New York City taxis website.

13
00:00:48,890 --> 00:00:52,140
If we had access to Microsoft R server,

14
00:00:52,140 --> 00:00:56,120
installed on our local machine or under remote server somewhere,

15
00:00:56,120 --> 00:00:59,890
we would actually be able to run the RX means function on.

16
00:00:59,890 --> 00:01:02,560
The entirety of the data.

17
00:01:02,560 --> 00:01:06,060
And that's multiple years of data

18
00:01:06,060 --> 00:01:09,930
with lots of different CSV files, one for each month.

19
00:01:09,930 --> 00:01:15,200
And then, total, if we have just one year worth of data,

20
00:01:15,200 --> 00:01:20,560
we're gonna have about ten gigabytes of data in slice.

21
00:01:20,560 --> 00:01:25,850
So all of the data is just referring to the XCF file that we have but

22
00:01:25,850 --> 00:01:29,920
that's still only 5% of the first 6 months of the data.

23
00:01:31,280 --> 00:01:36,000
And then the sample of the data is just a very small section

24
00:01:37,880 --> 00:01:38,800
of that XCF file.

25
00:01:40,530 --> 00:01:45,685
Now we're gonna look at a different example of an analytics algorithm.

26
00:01:45,685 --> 00:01:48,555
We're gonna shift gear a little bit to machine learning,

27
00:01:48,555 --> 00:01:49,785
a clustering algorithm.

28
00:01:49,785 --> 00:01:54,105
That's what's called an unsupervised learning algorithm.

29
00:01:54,105 --> 00:01:57,265
It's unsupervised in the sense that we

30
00:01:57,265 --> 00:01:59,935
don't really have a correct answer.

31
00:01:59,935 --> 00:02:02,095
Once we have clusters, we don't know

32
00:02:03,200 --> 00:02:08,660
which clusters are correct because there aren't any correct clusters.

33
00:02:08,660 --> 00:02:12,100
We have no way of judging if the clusters are good or

34
00:02:12,100 --> 00:02:17,000
bad other than basically going with some business logic,

35
00:02:17,000 --> 00:02:19,050
going with some rules of thumb.

36
00:02:19,050 --> 00:02:22,890
And just kind of seeing if they makes sense and practice.

37
00:02:22,890 --> 00:02:27,750
So, sometimes we don't know if we have good clusters.

38
00:02:27,750 --> 00:02:32,710
Until, first of all, we know the use case actually is, and

39
00:02:32,710 --> 00:02:37,330
we create our clusters and we go and put them to use and

40
00:02:37,330 --> 00:02:41,950
we see if they're actually, kind of making sense the way that they're

41
00:02:41,950 --> 00:02:45,510
being used to answer our particular business problem.

