0
00:00:01,610 --> 00:00:07,050
You may have heard that all models are wrong but some are useless.

1
00:00:07,050 --> 00:00:09,540
And certainly when it comes to modelling,

2
00:00:09,540 --> 00:00:14,970
it's hard to be able to tell if we have a really,

3
00:00:14,970 --> 00:00:19,920
really good model, until we look at how good are predictions are and

4
00:00:19,920 --> 00:00:26,500
until we built some other models, and do some comparisons and so on.

5
00:00:26,500 --> 00:00:29,100
One of the things that we wanna do is

6
00:00:29,100 --> 00:00:31,940
we wanna be a little bit parsimonious.

7
00:00:31,940 --> 00:00:36,130
What I mean by that is that if there are certain

8
00:00:36,130 --> 00:00:41,010
features that are just not particularly helpful to the model,

9
00:00:41,010 --> 00:00:42,862
we don't want to include those in.

10
00:00:42,862 --> 00:00:47,790
We wanna make good predictions using as few features as possible.

11
00:00:47,790 --> 00:00:50,450
This not only makes it so that we can be more efficient,

12
00:00:50,450 --> 00:00:52,520
but it also makes it easier for

13
00:00:52,520 --> 00:00:58,340
us to interpret our models later when and if we need to do that.

14
00:00:58,340 --> 00:01:01,880
So an interesting thing that we might be asking about the model that

15
00:01:01,880 --> 00:01:04,780
we just built in the last section is,

16
00:01:04,780 --> 00:01:07,210
can we make it a little bit more simple?

17
00:01:08,240 --> 00:01:12,420
We used pick up neighborhood and drop off neighborhood as well as

18
00:01:12,420 --> 00:01:14,890
pick up day of week and drop off day of week to build the model.

19
00:01:15,940 --> 00:01:21,680
And we're gonna wonder now maybe pickup day of week and

20
00:01:21,680 --> 00:01:27,940
pickup time of day or pickup hour are not that useful to the model.

21
00:01:27,940 --> 00:01:32,410
It seems like, just knowing the neighborhoods between which people

22
00:01:32,410 --> 00:01:37,980
are travelling on their taxi trip is going to be a much better predictor?

23
00:01:37,980 --> 00:01:41,230
Of how much people are gonna tip, then knowing what time of

24
00:01:41,230 --> 00:01:45,730
the day it is and knowing what day of the week it is.

25
00:01:45,730 --> 00:01:47,400
So we might be wrong about that.

26
00:01:47,400 --> 00:01:49,110
But it's a legitimate question.

27
00:01:49,110 --> 00:01:50,940
And we're gonna see if we can investigate that

28
00:01:50,940 --> 00:01:52,280
a little bit right now.

29
00:01:53,770 --> 00:01:55,580
So we're gonna build another model.

30
00:01:57,730 --> 00:02:01,397
And this time, We're gonna predict tip percent just as before but

31
00:02:01,397 --> 00:02:04,541
only using the interaction of pickup_neighborhood and

32
00:02:04,541 --> 00:02:06,070
dropoff_neighborhood.

33
00:02:06,070 --> 00:02:08,540
So we're gonna throw out this the other two columns.

34
00:02:10,870 --> 00:02:15,693
And once we have a model we're going to take our model and once again,

35
00:02:15,693 --> 00:02:19,268
hand it down to our expedite and go back to the data to

36
00:02:19,268 --> 00:02:24,175
the same data that the first model was able to make predictions on, and

37
00:02:24,175 --> 00:02:28,776
we're gonna make predictions this time using the second model.

38
00:02:32,450 --> 00:02:37,703
Once we have the predictions, we're going to rearrange the data

39
00:02:37,703 --> 00:02:43,049
a little bit so that we end up with one dataset that has the four input

40
00:02:43,049 --> 00:02:47,940
columns that the first model was using to make predictions.

41
00:02:49,320 --> 00:02:52,490
Including the two input columns that are the only ones that

42
00:02:52,490 --> 00:02:57,830
the second model cares about or takes into consideration.

43
00:02:57,830 --> 00:03:00,360
And then, we have the prediction for

44
00:03:00,360 --> 00:03:03,470
tip percent that was made by the first model.

45
00:03:03,470 --> 00:03:08,760
And the prediction for tip percent that is made by the second model.

46
00:03:08,760 --> 00:03:12,419
Recall that we can think of the first model as let's say the more

47
00:03:12,419 --> 00:03:16,562
complicated model in the sense that it's taking more information into

48
00:03:16,562 --> 00:03:18,790
account when making its prediction.

49
00:03:18,790 --> 00:03:24,034
And we can think of the second model as the more parsimonious model,

50
00:03:24,034 --> 00:03:25,874
the more simple model,

51
00:03:25,874 --> 00:03:31,026
in the sense that it's only taking two of those four initial inputs

52
00:03:31,026 --> 00:03:36,540
into account when deciding what the prediction needs to be.

53
00:03:36,540 --> 00:03:41,160
This would explain why the first model is showing us a different

54
00:03:41,160 --> 00:03:47,140
prediction for all of the trips from Chinatown to Chinatown,

55
00:03:47,140 --> 00:03:50,730
meeting on a Sunday but at different hours of the day.

56
00:03:50,730 --> 00:03:54,730
Whereas, the second model is gonna look at all of these trips,

57
00:03:54,730 --> 00:03:59,020
since all of them are from Chinatown to Chinatown, since I don't really

58
00:03:59,020 --> 00:04:02,130
care what the day of the week is and what the hour of the day is,

59
00:04:02,130 --> 00:04:05,450
I'm gonna make the same prediction across for all of these trips.

60
00:04:06,510 --> 00:04:09,643
So the second model is predicting 5.76 for

61
00:04:09,643 --> 00:04:12,900
any trip from Chinatown to Chinatown.

62
00:04:12,900 --> 00:04:16,350
The first model is going to take, in this case,

63
00:04:16,350 --> 00:04:19,408
hour of the day into account if the day of the week was different,

64
00:04:19,408 --> 00:04:22,140
it's also gonna take the day of the weekend into account and

65
00:04:22,140 --> 00:04:27,090
it is gonna give us a certain amount of variation around that prediction.

66
00:04:27,090 --> 00:04:32,265
In some cases the prediction is going to be a little bit lower,

67
00:04:32,265 --> 00:04:34,200
5.72 in this case.

68
00:04:34,200 --> 00:04:37,005
In some cases it's gonna be quite a bit lower, and so on.

69
00:04:37,005 --> 00:04:42,102
So we have this data frame called pred_df,

70
00:04:42,102 --> 00:04:46,300
we can see the first six rows here.

71
00:04:46,300 --> 00:04:49,610
And we can see that each model was able to make predictions on

72
00:04:49,610 --> 00:04:51,190
this data frame.

73
00:04:51,190 --> 00:04:57,210
We're now going to visualize the results, the predictions made by

74
00:04:57,210 --> 00:05:01,450
the simple model and the predictions made by the more complex model.

75
00:05:02,730 --> 00:05:05,380
And what we're going to visualize is the following.

76
00:05:06,560 --> 00:05:09,260
The simple model is shown in blue.

77
00:05:09,260 --> 00:05:11,290
The complex model is shown in red.

78
00:05:12,340 --> 00:05:17,210
We're going to look at the distribution of the predictions.

79
00:05:17,210 --> 00:05:20,075
So whatever the tip percent prediction is.

80
00:05:20,075 --> 00:05:21,160
It's distribution

81
00:05:22,860 --> 00:05:27,050
made by the simple model versus the one made by the complex model.

82
00:05:27,050 --> 00:05:32,700
And we're going to break it up by day of the week shown in the x-axis.

83
00:05:32,700 --> 00:05:36,404
And by the time of the day shown in the y-axis.

84
00:05:37,480 --> 00:05:41,390
Now, because the sample model doesn't care about the day of

85
00:05:41,390 --> 00:05:44,310
the week or the time of the day, because it doesn't take that into

86
00:05:44,310 --> 00:05:48,930
consideration when making its predictions, the blue distribution

87
00:05:48,930 --> 00:05:53,740
is going to be the same on each of these tiles.

88
00:05:55,490 --> 00:05:58,340
So we have the same blue line

89
00:05:58,340 --> 00:06:03,290
representing the distribution of the predictions for

90
00:06:03,290 --> 00:06:06,590
each combination of day of the week and time of the day.

91
00:06:06,590 --> 00:06:10,720
On the other hand, because the more complex model does take day of

92
00:06:10,720 --> 00:06:14,440
the week and time of the day into account, the red distribution is

93
00:06:14,440 --> 00:06:20,430
going to shift up by a little bit every time, depending

94
00:06:20,430 --> 00:06:24,750
on the particular combination of day of week and time of day.

95
00:06:24,750 --> 00:06:28,905
And so, we can see for example that on a Sunday, the red and the blue

96
00:06:28,905 --> 00:06:33,305
are pretty much over-lapping and so the predictions are pretty close but

97
00:06:33,305 --> 00:06:37,995
if we move to a Monday from 1 AM to 5 AM we can see that

98
00:06:37,995 --> 00:06:42,910
the predictions are now much lower, when the complex

99
00:06:42,910 --> 00:06:45,720
model is making a prediction compared to the simple model.

100
00:06:46,930 --> 00:06:50,310
How much lower based on this plot, looks about one or

101
00:06:50,310 --> 00:06:52,170
two percentage points?

102
00:06:52,170 --> 00:06:57,678
So 1 or 2 percentage points and 2% for a large enough customer

103
00:06:57,678 --> 00:07:02,580
base of course could mean a significant amount of money.

104
00:07:02,580 --> 00:07:07,180
So the difference might be very small, the shift

105
00:07:07,180 --> 00:07:10,000
that we see between the red and the blue might be very small.

106
00:07:10,000 --> 00:07:13,460
But it might result in a significant difference in terms of

107
00:07:13,460 --> 00:07:14,640
something like revenue.

108
00:07:17,260 --> 00:07:21,821
And it might be a little bit over killed for us to look at this plot

109
00:07:21,821 --> 00:07:26,750
just because the distribution is a little bit to detail.

110
00:07:26,750 --> 00:07:31,510
So instead what we're gonna do, is first of all we're gonna used the RX

111
00:07:31,510 --> 00:07:35,690
quantile function to get a better handle on tip percent and

112
00:07:35,690 --> 00:07:37,850
how it should be distributed.

113
00:07:37,850 --> 00:07:41,250
So we're gonna go to the tip percent column in the data.

114
00:07:41,250 --> 00:07:44,510
This is not the prediction, this is the actual observed value for

115
00:07:44,510 --> 00:07:46,180
tip percent.

116
00:07:46,180 --> 00:07:51,057
And we're gonna ask for quantiles full tip

117
00:07:51,057 --> 00:07:55,947
percent starting at 0 going to 100%.

118
00:07:55,947 --> 00:07:59,640
And skipping every other five.

119
00:07:59,640 --> 00:08:02,710
So the 0th percentile is the minimum, and

120
00:08:02,710 --> 00:08:05,191
obviously it's gonna be at 0%.

121
00:08:05,191 --> 00:08:09,210
The maximum is at 100 and that's at 99%.

122
00:08:09,210 --> 00:08:15,130
And then, we can see that let's say if I stop here,

123
00:08:15,130 --> 00:08:21,469
half of the customers tipped less than 17.41%,

124
00:08:21,469 --> 00:08:28,355
and the other half greater than by the time we get to 90%,

125
00:08:28,355 --> 00:08:32,763
you can see that 90% of customers,

126
00:08:32,763 --> 00:08:38,876
90% of trips tipped about 26.73% or less.

127
00:08:38,876 --> 00:08:44,080
So this gives us an idea of how trip percent itself is distributed.

128
00:08:45,560 --> 00:08:50,611
Based on this distribution we can make a somewhat

129
00:08:50,611 --> 00:08:55,046
business oriented decision to take the tip

130
00:08:55,046 --> 00:08:59,972
predictions and to use the cut function to group

131
00:08:59,972 --> 00:09:04,914
them based on whether they're less than 8%.

132
00:09:04,914 --> 00:09:10,700
They're between 8 and 12%, between 15 and 18% or anything greater.

133
00:09:11,920 --> 00:09:14,710
So we gonna look at something similar to the last plot.

134
00:09:14,710 --> 00:09:18,408
But instead of a distribution like we had,

135
00:09:18,408 --> 00:09:23,548
we're gonna take that distribution and we're gonna break

136
00:09:23,548 --> 00:09:28,498
it up into whether it falls into one of those categories.

137
00:09:28,498 --> 00:09:32,635
I'm gonna run this again, it didn't produce the plot.

138
00:09:32,635 --> 00:09:36,270
We have it now.

139
00:09:38,090 --> 00:09:39,570
So we have our plot here.

140
00:09:39,570 --> 00:09:42,620
And we can see something similar to what we saw

141
00:09:43,750 --> 00:09:45,570
when were comparing the distribution.

142
00:09:45,570 --> 00:09:49,130
But we can see in anyway that makes more business sense now.

143
00:09:49,130 --> 00:09:53,620
We can see, for example, going back to the plot for some days between 1

144
00:09:53,620 --> 00:09:59,300
AM and 5 AM that the simple model and the complex model are making

145
00:09:59,300 --> 00:10:02,820
about the same prediction for all of the different categories.

146
00:10:03,890 --> 00:10:07,890
But if we move to the next square to the right, we can see that for

147
00:10:07,890 --> 00:10:12,523
Mondays, during the same time frame from 1 AM to 5 AM,

148
00:10:12,523 --> 00:10:16,830
the simple model is making the same predictions,

149
00:10:16,830 --> 00:10:20,180
because it doesn't care that it's a Monday or a Sunday, but

150
00:10:20,180 --> 00:10:24,070
the complex model is taking that information into account and

151
00:10:24,070 --> 00:10:26,466
it is shifting its predictions to the left.

152
00:10:26,466 --> 00:10:32,170
So based on the complex model,

153
00:10:32,170 --> 00:10:36,540
the majority of customers are gonna tip somewhere between 8 and 12%,

154
00:10:36,540 --> 00:10:40,970
whereas the simple model was predicting that the majority will

155
00:10:40,970 --> 00:10:44,702
tip somewhere between 12 and 15%.

156
00:10:44,702 --> 00:10:48,810
And so, we can kinda visualize how the complex model and

157
00:10:48,810 --> 00:10:52,910
simple model are discriminating based on taking information into

158
00:10:52,910 --> 00:10:56,900
account about the time of the day and

159
00:10:56,900 --> 00:11:01,110
the day of the week, more specifically how the complex model

160
00:11:01,110 --> 00:11:04,860
is taking that into account and how it is adjusting its predictions.

161
00:11:06,335 --> 00:11:11,589
Whether that adjustment, on the one hand, makes business sense and

162
00:11:11,589 --> 00:11:15,767
whether it's also in a practical sense significant.

