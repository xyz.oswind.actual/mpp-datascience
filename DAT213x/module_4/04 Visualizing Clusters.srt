0
00:00:00,450 --> 00:00:03,960
So RXK means finished running, it took about 11 minutes for me.

1
00:00:05,090 --> 00:00:09,230
And RXK means it's one of those algorithms that's just not

2
00:00:09,230 --> 00:00:13,920
necessarily going to give us the best runtime when we use a small

3
00:00:13,920 --> 00:00:15,190
dataset on it.

4
00:00:15,190 --> 00:00:18,610
But for bigger datasets the scalability of it

5
00:00:18,610 --> 00:00:23,020
is gonna give us the, a considerable boost in performance.

6
00:00:23,020 --> 00:00:27,690
Another thing about the clustering algorithm in general is that

7
00:00:27,690 --> 00:00:30,120
the runtime is gonna be very sensitive to

8
00:00:30,120 --> 00:00:32,180
the number of clusters that you want to build.

9
00:00:33,210 --> 00:00:34,940
We chose 300 here,

10
00:00:34,940 --> 00:00:40,190
which is somewhat of a large number for the number of clusters to build.

11
00:00:40,190 --> 00:00:46,330
And so it's gonna take longer for it to run because of that.

12
00:00:47,620 --> 00:00:52,089
What comes out of RXK means it's not very different from

13
00:00:52,089 --> 00:00:55,642
what we get when we run the K means function.

14
00:00:55,642 --> 00:00:58,955
So we can take that RXKM function,

15
00:00:58,955 --> 00:01:02,445
RXKM object is what we called it in this case.

16
00:01:02,445 --> 00:01:10,460
And just as it was the case with the sample data inside of RXKM sample.

17
00:01:10,460 --> 00:01:15,270
We had the centroids stored in an object called centers

18
00:01:15,270 --> 00:01:19,770
inside of the RXKM object.

19
00:01:19,770 --> 00:01:25,670
We have an object called centers which contains the centroids for

20
00:01:25,670 --> 00:01:26,460
the clusters.

21
00:01:28,700 --> 00:01:31,700
Okay, so here we can take that the longitude and the latitude.

22
00:01:31,700 --> 00:01:36,270
We can turn them back into their non standardized forms and

23
00:01:36,270 --> 00:01:39,070
we can see that the sizes are now considerably larger,

24
00:01:39,070 --> 00:01:43,010
because we're using BXDF file that has all of the data,

25
00:01:43,010 --> 00:01:45,690
not just the data frame that has the sample of the data.

26
00:01:46,730 --> 00:01:50,530
Additionally, we can see that within clusters sum of the squares.

27
00:01:50,530 --> 00:01:55,378
Which is gonna be a measure of how homogenous each cluster is.

28
00:01:55,378 --> 00:01:59,996
Once again, as our object, our data frame here

29
00:01:59,996 --> 00:02:05,250
called CLSDF is gonna have 300 clusters.

30
00:02:05,250 --> 00:02:08,340
And we can see that some of them are larger than other ones.

31
00:02:09,890 --> 00:02:15,170
And we're going to take now this data frame,

32
00:02:15,170 --> 00:02:20,080
and we're gonna take the clusters centroids that it is returning us.

33
00:02:20,080 --> 00:02:24,530
So the clusters centroids that the RXK mean function is returning us

34
00:02:24,530 --> 00:02:26,570
based on the whole of the data.

35
00:02:27,760 --> 00:02:33,690
And we're gonna also take the cluster centroids that the K

36
00:02:33,690 --> 00:02:38,600
means function is returning to us based on a sample of the data.

37
00:02:38,600 --> 00:02:43,680
And we're gonna plot both of them so that we can compare

38
00:02:43,680 --> 00:02:49,610
what the clusters look like when we look at the data as a whole,

39
00:02:49,610 --> 00:02:54,440
and what the clusters look like when we look at the data as in a sample.

40
00:02:55,630 --> 00:03:00,260
So the data frame called centroids whole is gonna contain the clusters

41
00:03:00,260 --> 00:03:05,660
centroids based on what the RXK means function return.

42
00:03:05,660 --> 00:03:09,680
And we already have a data frame called centroids sample that has

43
00:03:09,680 --> 00:03:14,200
the clusters centroids based on what the K means function

44
00:03:14,200 --> 00:03:16,800
returned using a sample of the data set.

45
00:03:18,660 --> 00:03:21,790
We're going to use ggmap and

46
00:03:21,790 --> 00:03:27,620
we're going to visualize both results side by side so

47
00:03:27,620 --> 00:03:33,630
that we can compare what the data is returning when we built clusters

48
00:03:33,630 --> 00:03:37,230
on the whole of the data as supposed to just say sample of the data.

49
00:03:38,450 --> 00:03:42,570
So, the results from the sample are shown on the left side and

50
00:03:42,570 --> 00:03:44,510
the results from the whole are shown on the right side.

51
00:03:44,510 --> 00:03:51,650
The individual centroids our little red dots that we can see on the map.

52
00:03:51,650 --> 00:03:56,740
And the dots are going to have a darker shade of red

53
00:03:56,740 --> 00:04:00,200
when the clusters are more populous, and

54
00:04:00,200 --> 00:04:03,340
a lighter shade of red, when the clusters are smaller.

55
00:04:06,240 --> 00:04:10,170
So starting with the plot on the left, we can see for example that

56
00:04:11,240 --> 00:04:15,730
one of the clusters that was identified is,

57
00:04:15,730 --> 00:04:21,540
let's say on this location between 57th and 58th Street on 8th Avenue.

58
00:04:22,800 --> 00:04:28,403
And if we now move to the cluster centroid on the right.

59
00:04:28,403 --> 00:04:33,176
We can see that the corresponding centroid for that cluster is

60
00:04:33,176 --> 00:04:38,470
very close to the one that we had when we ran the sample of the data.

61
00:04:38,470 --> 00:04:41,540
We're almost in the same location, a little bit north of it,

62
00:04:41,540 --> 00:04:46,030
more on the corner of 58th and

63
00:04:46,030 --> 00:04:50,240
8th Avenue rather than midway between 57th and 58th, right?

64
00:04:51,870 --> 00:04:56,140
And so we can keep playing this exercise for

65
00:04:56,140 --> 00:04:57,670
each of the individual centroids.

66
00:04:57,670 --> 00:05:00,370
Of course it gets a little bit tedious doing so, so

67
00:05:00,370 --> 00:05:02,540
we're just gonna pick a few examples to go by.

68
00:05:03,600 --> 00:05:07,480
But one of the things that we're gonna notice doing so is that for

69
00:05:07,480 --> 00:05:13,222
the larger clusters the ones that have a more darker shade of red.

70
00:05:13,222 --> 00:05:14,890
What the data is returning based on the sample and

71
00:05:14,890 --> 00:05:22,632
what the data is returning based on using all of the data.

72
00:05:22,632 --> 00:05:25,530
So what the algorithm is returning based on the sample versus all of

73
00:05:25,530 --> 00:05:32,130
the data is going to be in most cases, pretty close to each other.

74
00:05:34,030 --> 00:05:38,070
But when we get to the smaller clusters and

75
00:05:38,070 --> 00:05:42,170
in this case for our example we can,

76
00:05:42,170 --> 00:05:46,340
let's say pick the one that my mouse is right now holding.

77
00:05:46,340 --> 00:05:50,706
So it's on Park Avenue between 57th and 58th.

78
00:05:50,706 --> 00:05:55,932
And if we go to that location on Park Avenue between 57th and

79
00:05:55,932 --> 00:06:00,849
58th, we can see that the point is still pretty close,

80
00:06:00,849 --> 00:06:05,160
but a little bit off to where it was before.

81
00:06:05,160 --> 00:06:09,200
In some of the cases, it's gonna be off by almost a whole block.

82
00:06:09,200 --> 00:06:14,340
If we look at this next cluster right here, the location there is

83
00:06:14,340 --> 00:06:19,320
almost, isn't almost exactly on the intersection of 57th Avenue and

84
00:06:19,320 --> 00:06:19,920
Madison.

85
00:06:20,950 --> 00:06:24,405
If we go to the intersection of 57th and Madison.

86
00:06:24,405 --> 00:06:30,040
Now we can see another point there that's pretty close to it.

87
00:06:30,040 --> 00:06:35,819
Let's find some example of a case right

88
00:06:35,819 --> 00:06:42,680
here on 42nd in Brian park.

89
00:06:42,680 --> 00:06:46,630
So on the corner of Brian park we can see a cluster, it's very hard to

90
00:06:46,630 --> 00:06:52,990
see because the little m sign is overlaid on top of it.

91
00:06:52,990 --> 00:06:56,470
But there is a point there on the corner of 6th Avenue and

92
00:06:56,470 --> 00:06:57,680
42nd Street.

93
00:06:57,680 --> 00:07:00,510
And that's one of the closer centroids that

94
00:07:00,510 --> 00:07:02,380
the sample data found.

95
00:07:02,380 --> 00:07:05,840
Now if I move to the same location there

96
00:07:05,840 --> 00:07:09,260
with the algorithm that was run on the whole data,

97
00:07:09,260 --> 00:07:12,170
we can see that we're almost off by an entire block.

98
00:07:13,760 --> 00:07:15,340
So in some cases,

99
00:07:15,340 --> 00:07:18,260
the cluster centroids are pretty close to each other and

100
00:07:18,260 --> 00:07:26,410
in some cases the difference is off by let's say about a block.

101
00:07:27,560 --> 00:07:29,040
In the majority of cases

102
00:07:30,480 --> 00:07:34,830
we might be able to say that the difference is somewhat negligible.

103
00:07:34,830 --> 00:07:39,530
Of course how negligible it is depends on the used case.

104
00:07:39,530 --> 00:07:43,270
It could be that the used case is for

105
00:07:43,270 --> 00:07:49,780
us to determine as accurately as possible common drop off locations.

106
00:07:49,780 --> 00:07:52,590
So that we can use that to decide

107
00:07:53,840 --> 00:07:56,820
how we can best direct the flow of traffic.

108
00:07:56,820 --> 00:08:02,295
If taxis dropping off passengers is gonna be one of the causes,

109
00:08:02,295 --> 00:08:05,065
one of the root causes of traffic in the city and

110
00:08:05,065 --> 00:08:08,215
we want to find a way to alleviate that.

111
00:08:08,215 --> 00:08:11,885
That's a case where we might be interested in knowing the common

112
00:08:11,885 --> 00:08:16,315
drop off locations to see if taxis dropping

113
00:08:16,315 --> 00:08:21,810
off passengers at those locations is causing traffic to back up.

114
00:08:21,810 --> 00:08:28,840
So in a case like this, knowing the location of the cluster centroids

115
00:08:28,840 --> 00:08:32,810
as accurately as possible, can make a big difference in us being able to

116
00:08:32,810 --> 00:08:37,650
determine those locations in being able to follow up and

117
00:08:37,650 --> 00:08:42,180
see if they trace back to a backup of traffic.

118
00:08:43,970 --> 00:08:48,640
So ultimately, how much of a margin of error

119
00:08:48,640 --> 00:08:52,930
we're willing to tolerate depends on what the used case is and whether

120
00:08:52,930 --> 00:08:58,150
the difference that we see and the sample data versus the whole data

121
00:08:59,320 --> 00:09:04,500
is practically significant or if it's not.

122
00:09:04,500 --> 00:09:07,730
So, practical significant is something that just depends

123
00:09:07,730 --> 00:09:09,760
on the business use case.

124
00:09:09,760 --> 00:09:13,212
Whether the differences are our statistically significant or

125
00:09:13,212 --> 00:09:14,720
not is another matter.

126
00:09:14,720 --> 00:09:17,550
In most cases we might be able to argue in case that they

127
00:09:17,550 --> 00:09:21,240
are not gonna be that statistically different from each other.

128
00:09:21,240 --> 00:09:26,242
Or that the results from the sample data is pretty fair

129
00:09:26,242 --> 00:09:30,810
representation of the population that we have.

130
00:09:30,810 --> 00:09:34,220
But of course we're more interested in the practical significance

131
00:09:34,220 --> 00:09:35,670
of this.

132
00:09:35,670 --> 00:09:39,863
Which for one hypothetical use case means that we're

133
00:09:39,863 --> 00:09:44,720
going to need to know the location as accurately as possible.

