0
00:00:01,850 --> 00:00:05,880
Let's now see if we can visualize the results so

1
00:00:05,880 --> 00:00:09,000
that we don't have to worry about the individual predictions and

2
00:00:09,000 --> 00:00:11,730
we can see some overall trends.

3
00:00:12,750 --> 00:00:16,540
So we're gonna take our dataframe with all the predictions in it and

4
00:00:16,540 --> 00:00:23,990
we're gonna use ggplot to once again run predictions.

5
00:00:23,990 --> 00:00:28,370
And we're gonna run the predictions once by breaking it up

6
00:00:28,370 --> 00:00:31,490
by pick up neighborhood and drop off neighborhood in the second time

7
00:00:31,490 --> 00:00:35,390
by the other two variables namely pickup day of week and pick up hour.

8
00:00:36,620 --> 00:00:39,850
So this is what we see when we looked at the neighborhood

9
00:00:39,850 --> 00:00:41,740
by neighborhood breakdown.

10
00:00:41,740 --> 00:00:44,470
We can see the actual prediction here,

11
00:00:44,470 --> 00:00:49,430
shown by the color coding in the tile plots.

12
00:00:49,430 --> 00:00:52,346
And we can see two of the inputs to the model, mainly,

13
00:00:52,346 --> 00:00:55,590
pick-up neighborhood and drop-off neighborhood showing up in the x and

14
00:00:55,590 --> 00:00:57,030
y axes, respectively.

15
00:00:59,050 --> 00:01:03,460
We can see that for certain pick up and

16
00:01:03,460 --> 00:01:07,720
drop off combinations, the prediction is quite high.

17
00:01:07,720 --> 00:01:16,168
One of them as an example is going from Inwood to West Village.

18
00:01:16,168 --> 00:01:18,578
So as just from eyeballing the plot,

19
00:01:18,578 --> 00:01:23,230
we can tell that this is probably the largest prediction.

20
00:01:23,230 --> 00:01:27,820
Now, in this case, this particular prediction is probably gonna

21
00:01:27,820 --> 00:01:31,910
also have a higher standard deviation associated with it.

22
00:01:31,910 --> 00:01:36,614
We call that in this and, that in the data set that we have,

23
00:01:36,614 --> 00:01:39,111
the northern neighborhoods,

24
00:01:39,111 --> 00:01:44,776
the neighborhoods that are almost at the very tippy top of Manhattan,

25
00:01:44,776 --> 00:01:48,245
are not very well represented in the data.

26
00:01:48,245 --> 00:01:50,795
Although sample sizes are quite smaller.

27
00:01:50,795 --> 00:01:53,345
So one of the things that we can already see just from looking at

28
00:01:53,345 --> 00:01:57,655
this plot is that the predictions are a little bit more varied when we

29
00:01:57,655 --> 00:02:02,575
look at the trips that

30
00:02:02,575 --> 00:02:08,375
are starting in one of these top most neighborhoods in Manhattan.

31
00:02:08,375 --> 00:02:14,870
It could of course be that this is just the way that

32
00:02:14,870 --> 00:02:20,650
the tipping behavior is when they start at one of these neighborhoods.

33
00:02:20,650 --> 00:02:25,530
But more likely than not in our case it's a result of smaller

34
00:02:25,530 --> 00:02:27,050
sample size.

35
00:02:27,050 --> 00:02:30,930
So, let's focus our attention more on the midtown neighborhoods and

36
00:02:30,930 --> 00:02:32,730
downtown neighborhoods.

37
00:02:32,730 --> 00:02:36,210
Even in those cases we can see that there is definitely a certain amount

38
00:02:36,210 --> 00:02:38,730
of variation going on.

39
00:02:38,730 --> 00:02:44,900
We can see for example that people who get

40
00:02:44,900 --> 00:02:49,980
picked up at the Garment District tend to tip a little bit less

41
00:02:51,530 --> 00:02:57,070
regardless of their destination than

42
00:02:57,070 --> 00:03:00,518
the people who get picked up at some of the other neighborhoods.

43
00:03:00,518 --> 00:03:05,120
So, of course,

44
00:03:05,120 --> 00:03:08,860
we don't know if this is actually so or not.

45
00:03:08,860 --> 00:03:12,330
This is what the model is determining to be the case.

46
00:03:13,570 --> 00:03:17,710
And we would have to investigate this more to see if these

47
00:03:17,710 --> 00:03:22,460
predictions are good predictions or if the predictions are just not

48
00:03:22,460 --> 00:03:26,750
capturing enough, taking enough information into account.

49
00:03:26,750 --> 00:03:30,270
But we have here a visualization that kind of breaks it down for

50
00:03:30,270 --> 00:03:33,330
us and we can see that the model is definitely

51
00:03:33,330 --> 00:03:34,660
taking pick-up neighborhood and

52
00:03:34,660 --> 00:03:38,329
drop of neighborhood into account and that these two columns

53
00:03:39,820 --> 00:03:44,500
together give us a certain amount of discriminatory power.

54
00:03:46,210 --> 00:03:51,310
We're gonna look at the next example now, involving the combination

55
00:03:51,310 --> 00:03:54,870
of day of week and time of day.

56
00:03:54,870 --> 00:03:58,790
So pick up day of week and pick up hour are the name of the columns and

57
00:03:58,790 --> 00:04:02,430
we can see day of week shown here in the x axis, we can see hour

58
00:04:03,670 --> 00:04:08,290
which represents really the time of the day on the y axis.

59
00:04:08,290 --> 00:04:11,810
And once again we can see some trends that we also noticed

60
00:04:11,810 --> 00:04:16,362
when we were looking at the data a little while back.

61
00:04:16,362 --> 00:04:23,020
Namely, that people who get picked up

62
00:04:23,020 --> 00:04:29,580
during the morning rush hour on a weekday or

63
00:04:29,580 --> 00:04:33,330
in the afternoon rush hour on a week day,

64
00:04:33,330 --> 00:04:38,820
tend to be generally more generously tippers than people travelling

65
00:04:38,820 --> 00:04:41,640
on any other time of the day.

66
00:04:41,640 --> 00:04:45,100
So some of this trends are jumping out to us and

67
00:04:45,100 --> 00:04:50,106
it's kind of comforting for us to know that the model has been able

68
00:04:50,106 --> 00:04:54,932
to capture some of the variability that we noticed in the data,

69
00:04:54,932 --> 00:04:58,484
when we were looking at various breakdowns and

70
00:04:58,484 --> 00:05:01,490
various visualizations of the data and

71
00:05:01,490 --> 00:05:06,255
creating different summaries of the data in the last module.

