0
00:00:00,610 --> 00:00:03,258
So, let's spend some time now,

1
00:00:03,258 --> 00:00:07,988
looking at some of the transformations that we've done and

2
00:00:07,988 --> 00:00:12,650
being able to see, how far we've gotten so, far.

3
00:00:12,650 --> 00:00:19,780
So, the first thing I'm gonna do is I'm going to run another rxSummary.

4
00:00:19,780 --> 00:00:22,940
And in this case I'm just gonna be a little bit lazy and

5
00:00:22,940 --> 00:00:27,490
instead of saying, give me a summary of a specific column, I'm gonna use

6
00:00:27,490 --> 00:00:30,800
the formula notation and put a period there at the end that says,

7
00:00:30,800 --> 00:00:32,722
give me a summary of everything.

8
00:00:32,722 --> 00:00:36,100
I wanna see everything, every column in the data,

9
00:00:36,100 --> 00:00:39,680
find the relevant summary for it and show it to me.

10
00:00:39,680 --> 00:00:43,080
So we dump all of that in something called rxs_all and

11
00:00:43,080 --> 00:00:46,390
if we just try to look at what's inside of rxs_all

12
00:00:46,390 --> 00:00:49,220
we can see that there's quite a bit of information in here.

13
00:00:49,220 --> 00:00:53,350
And we're gonna cover that, we're gonna talk more about those.

14
00:00:53,350 --> 00:00:59,370
But let's just look at the information that we have in

15
00:00:59,370 --> 00:01:03,300
what's called the sDataFrame object inside rxs_all.

16
00:01:03,300 --> 00:01:09,970
If we do a head on that we can see that for the columns in the data

17
00:01:09,970 --> 00:01:14,350
when the columns are numeric, we can see relevant numeric summaries.

18
00:01:14,350 --> 00:01:17,700
And when the columns are non-numeric, so

19
00:01:17,700 --> 00:01:22,850
they could be character or they could be factors, we're still

20
00:01:22,850 --> 00:01:26,090
gonna see some valid observations, missing observations and so on.

21
00:01:26,090 --> 00:01:30,690
But the numeric summaries will just show N/As.

22
00:01:30,690 --> 00:01:34,130
Now let's hone in on the neighborhood columns

23
00:01:34,130 --> 00:01:36,160
that we created earlier.

24
00:01:36,160 --> 00:01:39,100
So I'm gonna run RXCrossTabs again, and

25
00:01:39,100 --> 00:01:43,940
in this case, I wanna see cross tabulations as showing

26
00:01:43,940 --> 00:01:47,840
the interaction between pick up neighborhood and pick up borough.

27
00:01:49,080 --> 00:01:53,735
One of the things that we have to do when

28
00:01:53,735 --> 00:01:58,975
using RXCrossTabs is we need to make sure that these neighborhoods,

29
00:01:58,975 --> 00:02:01,480
these two neighborhood columns, neighborhood and

30
00:02:01,480 --> 00:02:05,645
borough columns that we wanna get counts for are actually factors.

31
00:02:05,645 --> 00:02:09,870
We need to think of those as categorical data, and

32
00:02:09,870 --> 00:02:12,320
there's different ways that we can check that.

33
00:02:12,320 --> 00:02:17,343
We could, for example, scroll up to our rxSummary that we just ran and

34
00:02:17,343 --> 00:02:19,472
we'd notice, for example,

35
00:02:19,472 --> 00:02:24,180
that in the case of drop-off borough we can actually see counts.

36
00:02:25,590 --> 00:02:30,100
Not only can we see counts, but we can see that for the case of

37
00:02:30,100 --> 00:02:35,420
Manhattan, the borough of Manhattan, the counts are positive.

38
00:02:35,420 --> 00:02:40,870
And for all the other categories that are shown in here,

39
00:02:40,870 --> 00:02:42,250
the counts are zero.

40
00:02:42,250 --> 00:02:46,645
So we see something that is common in the case of dealing with factors,

41
00:02:46,645 --> 00:02:51,560
mainly that it is certainly possible to have more factor levels

42
00:02:51,560 --> 00:02:55,530
in a factor column then is actually present in the data.

43
00:02:55,530 --> 00:02:59,635
And in this case we kind of force that upon the data ourselves.

44
00:02:59,635 --> 00:03:02,750
We've limited the shape file to only be Manhattan, and

45
00:03:02,750 --> 00:03:04,990
then we used that to find the neighborhoods.

46
00:03:04,990 --> 00:03:08,240
And so we shouldn't be surprised to see that in the case of the borough

47
00:03:08,240 --> 00:03:11,670
the only borough that it could find were the Manhattan boroughs.

48
00:03:11,670 --> 00:03:13,949
And if we scroll up here to look at the neighborhood,

49
00:03:13,949 --> 00:03:17,120
the only neighborhoods where the counts are actually positive

50
00:03:17,120 --> 00:03:19,950
are the neighborhoods that fall into Manhattan.

51
00:03:19,950 --> 00:03:22,490
All the other ones have negative counts, so

52
00:03:22,490 --> 00:03:25,540
here we can see some of the things

53
00:03:26,640 --> 00:03:31,250
that essentially are a carry-over from dealing with factor data.

54
00:03:31,250 --> 00:03:35,780
We were a little bit maybe sloppy when we worked with,

55
00:03:36,830 --> 00:03:39,330
when we wrote our column transformation.

56
00:03:39,330 --> 00:03:44,290
In that we didn't think too hard about what is being returned and

57
00:03:44,290 --> 00:03:47,570
if it is a factor, does it have all the levels that it should have,

58
00:03:47,570 --> 00:03:49,660
or does it have more levels.

59
00:03:49,660 --> 00:03:54,730
In this case it has far more levels than actually needs to have, right.

60
00:03:54,730 --> 00:03:57,500
So that's fine we're gonna take care of that in

61
00:03:57,500 --> 00:03:59,130
some of the following steps.

62
00:03:59,130 --> 00:04:03,480
But in this case we just wanna be

63
00:04:03,480 --> 00:04:06,210
more aware of this as a general concept.

64
00:04:06,210 --> 00:04:11,056
And I wanna point out that this is not necessarily something that

65
00:04:11,056 --> 00:04:15,814
is unique to working with data in RevoScaleR obviously factors

66
00:04:15,814 --> 00:04:20,346
area big deal in trading data in Open Source R and in Base R.

67
00:04:20,346 --> 00:04:25,220
In general but they just kinda get carried over into

68
00:04:25,220 --> 00:04:27,840
dealing with data in RevoScaleR as well.

69
00:04:29,810 --> 00:04:31,560
So, let's run our crosstabs,

70
00:04:31,560 --> 00:04:35,860
now that we know that the columns are factors, and we're going to

71
00:04:35,860 --> 00:04:41,160
reshape the data a little bit and out comes this object.

72
00:04:41,160 --> 00:04:45,850
It's a data frame, it's reshaped, it's kind of in a wide format, and

73
00:04:45,850 --> 00:04:49,120
so we can say the neighborhood's there as rows, and

74
00:04:49,120 --> 00:04:51,820
we can see the five boroughs as columns.

75
00:04:51,820 --> 00:04:54,680
We can confirm that every count is zero.

76
00:04:55,680 --> 00:04:57,430
Other than the account for,

77
00:04:57,430 --> 00:05:00,950
the column that corresponds to the borough of Manhattan.

78
00:05:03,530 --> 00:05:08,310
So it looks like we should probably go to this data, and we should pull

79
00:05:08,310 --> 00:05:13,210
out the names of the neighborhoods that fall into Manhattan.

80
00:05:13,210 --> 00:05:14,020
In other words,

81
00:05:14,020 --> 00:05:16,510
the names of the neighborhoods were the counts are positive.

82
00:05:17,640 --> 00:05:22,325
So that later when dealing with the data, we can basically

83
00:05:22,325 --> 00:05:27,242
re-factor the column and only have the neighborhoods that fall

84
00:05:27,242 --> 00:05:32,510
into Manhattan as the relevant factor levels for the column.

85
00:05:32,510 --> 00:05:39,490
So we'll see more how that works in the next section but

86
00:05:39,490 --> 00:05:44,890
for now, we use rxSummary to notice

87
00:05:44,890 --> 00:05:49,230
certain behaviors that are that certain columns that the data have.

88
00:05:49,230 --> 00:05:51,810
We use rxCrossTabs to get counts and

89
00:05:51,810 --> 00:05:56,050
we use the return object from rxCrossTabs to get a list thing

90
00:05:56,050 --> 00:05:59,210
of the neighborhoods that fall into Manhattan.

91
00:05:59,210 --> 00:06:03,980
And when we look at just the top few here, we can see that yes,

92
00:06:03,980 --> 00:06:09,210
in fact, any other category, Albany,

93
00:06:09,210 --> 00:06:13,790
Buffalo and the other four boroughs that make up New York City

94
00:06:13,790 --> 00:06:18,730
have counts of 0 neighborhoods, they have no data that we can see.

95
00:06:18,730 --> 00:06:25,540
And, we get, in this case, the top six but we have a data called lnbs

96
00:06:25,540 --> 00:06:29,240
that has all of the neighborhoods that fallen into Manhattan.

97
00:06:29,240 --> 00:06:31,630
And we can see that with their positive counts.

98
00:06:31,630 --> 00:06:35,747
So we're gonna use this data a little bit later to do some more

99
00:06:35,747 --> 00:06:37,136
transformations.

