0
00:00:00,780 --> 00:00:04,580
So let's dive in, first thing we're gonna do is we're

1
00:00:04,580 --> 00:00:07,570
gonna look at some relationships in the data.

2
00:00:07,570 --> 00:00:11,320
And we're gonna create some summaries that we can then

3
00:00:11,320 --> 00:00:12,418
visualize.

4
00:00:12,418 --> 00:00:16,380
So we're gonna use RXCrossTabs, and

5
00:00:16,380 --> 00:00:21,420
this is the formula in this case that we pass to rxCrossTabs.

6
00:00:21,420 --> 00:00:25,310
The neighborhood columns that we created in the last

7
00:00:25,310 --> 00:00:28,120
module are going to be very important to us.

8
00:00:28,120 --> 00:00:31,940
We're gonna use it a couple of times for different visualizations.

9
00:00:31,940 --> 00:00:36,120
And so here we can see that we have our results broken up

10
00:00:36,120 --> 00:00:38,600
by the interaction of pickup neighborhood and

11
00:00:38,600 --> 00:00:40,100
dropoff neighborhood.

12
00:00:40,100 --> 00:00:44,190
But notice that, in this case, inside of our rxCrossTabs function

13
00:00:44,190 --> 00:00:47,750
we have a column to the left of the tilde.

14
00:00:48,770 --> 00:00:53,270
So we're asking for summaries of trip distance and specific for

15
00:00:53,270 --> 00:00:58,520
the average trip distance to show up or the sum of trip distance to show

16
00:00:58,520 --> 00:01:02,600
up, broken up by the pickup and dropoff neighborhoods.

17
00:01:02,600 --> 00:01:06,430
So let's take a look at what that actually looks like.

18
00:01:06,430 --> 00:01:10,450
So, we can see here that the result is something that looks

19
00:01:10,450 --> 00:01:15,290
like a matrix, where we have pick up neighborhood in that row and

20
00:01:15,290 --> 00:01:17,950
we have the different dropoff neighborhoods showing up.

21
00:01:17,950 --> 00:01:21,110
And in here, the individual cells

22
00:01:21,110 --> 00:01:25,810
show the total trip distance that was traveled from,

23
00:01:25,810 --> 00:01:27,856
in this case, Chelsea, going to the garment district.

24
00:01:27,856 --> 00:01:35,950
Is going to the total trip distance is about 16000 miles,

25
00:01:37,550 --> 00:01:43,040
because we have the totals we can get averages,

26
00:01:43,040 --> 00:01:47,880
and to get the averages, we need to draw in to the totals.

27
00:01:47,880 --> 00:01:52,740
And we also need to draw in to the counts, so, the count,

28
00:01:52,740 --> 00:01:58,380
the totals are in rxct$sums, the counts are in rxct$counts.

29
00:01:58,380 --> 00:02:02,140
And so we can divide the sums by the counts and

30
00:02:02,140 --> 00:02:06,406
we end up with an object called res, which has averages.

31
00:02:06,406 --> 00:02:11,645
So, as another example, the average trip

32
00:02:11,645 --> 00:02:16,738
distance traveled between Carnegie Hill

33
00:02:16,738 --> 00:02:21,550
to West Village is about 5.6 miles.

34
00:02:24,150 --> 00:02:28,885
Now we're gonna do something using a package called seriation.

35
00:02:29,890 --> 00:02:33,330
So notice that this is a matrix,

36
00:02:33,330 --> 00:02:37,508
it's what we can refer to as a distance matrix.

37
00:02:37,508 --> 00:02:44,950
And with seriation we're gonna do something that is going to reorder

38
00:02:44,950 --> 00:02:49,549
this matrix in a particular order that makes more topological sense.

39
00:02:50,550 --> 00:02:55,050
And for now I'm gonna skip on the detail of why we're doing this

40
00:02:55,050 --> 00:03:00,090
until we get to the actual plot where it makes sense,

41
00:03:00,090 --> 00:03:03,830
where we can actually see the reasoning behind it.

42
00:03:03,830 --> 00:03:05,570
But notice that the quote is pretty short,

43
00:03:05,570 --> 00:03:09,970
we basically load the library, we go to that matrix.

44
00:03:09,970 --> 00:03:13,060
And for any cases where we have N/As,

45
00:03:13,060 --> 00:03:17,010
we're gonna replace the distance with some kind of average.

46
00:03:17,010 --> 00:03:19,340
So this is just something that we have to do to make sure that we

47
00:03:19,340 --> 00:03:20,720
don't have any N/As in the data.

48
00:03:22,960 --> 00:03:27,610
We're using only a small, a very small sample of the data and so

49
00:03:27,610 --> 00:03:31,520
it's possible that N/As could occasionally pop up of course.

50
00:03:31,520 --> 00:03:37,800
If we use the big data itself that would not be a problem.

51
00:03:37,800 --> 00:03:40,290
And then we use these function called seriate,

52
00:03:41,390 --> 00:03:43,180
which is in the seriation package and

53
00:03:43,180 --> 00:03:47,345
it doesa sort of a re-ordering of the data if you will.

54
00:03:48,480 --> 00:03:52,070
Okay, next thing we're gonna do is pull up some other summaries.

55
00:03:52,070 --> 00:03:55,380
The first summary we're gonna pull up,

56
00:03:55,380 --> 00:03:58,880
is gonna be similar to the one that we did in the last one.

57
00:03:58,880 --> 00:04:01,910
It's gonna give us trip distance broken up by pick up neighborhood

58
00:04:01,910 --> 00:04:02,890
and drop off neighborhood.

59
00:04:04,260 --> 00:04:08,660
Then we do the same for this column called minutes per mile.

60
00:04:08,660 --> 00:04:11,140
Now this is not a column in the data so

61
00:04:11,140 --> 00:04:16,310
we calculate that column on the fly here using the transforms argument.

62
00:04:16,310 --> 00:04:21,783
And minute per mile is basically the duration of the trip divided by

63
00:04:21,783 --> 00:04:27,957
the distance that was traveled times 60 to turn it into minutes, right.

64
00:04:27,957 --> 00:04:34,170
So it's for every mile of the trip, how long were we in the taxi?

65
00:04:34,170 --> 00:04:38,054
And the last column is tip percent,

66
00:04:38,054 --> 00:04:42,329
when we combine the results together,

67
00:04:42,329 --> 00:04:46,990
we get this data frame, called, res, and

68
00:04:46,990 --> 00:04:51,660
res has the following information in it.

69
00:04:51,660 --> 00:04:54,481
So, we can see a pick up neighborhood,

70
00:04:54,481 --> 00:04:58,422
a drop off neighborhood, the average trip distance.

71
00:04:58,422 --> 00:05:03,162
The average number of minutes that a passenger was waiting in the cab to

72
00:05:03,162 --> 00:05:07,920
get their destination or sitting on a cab to get to their destination.

73
00:05:07,920 --> 00:05:11,520
And the average percentage that they tipped for

74
00:05:11,520 --> 00:05:14,850
the trip between these two neighborhoods.

75
00:05:15,930 --> 00:05:20,250
Okay, let's now take this data called the res, and

76
00:05:20,250 --> 00:05:23,140
let's come up with the first visualization.

77
00:05:25,560 --> 00:05:28,630
That's going to summarize the information for us.

78
00:05:28,630 --> 00:05:31,380
So this is what we have, in this case we're looking at

79
00:05:31,380 --> 00:05:34,268
the relationship between pickup neighborhood and

80
00:05:34,268 --> 00:05:35,930
drop off neighborhood.

81
00:05:35,930 --> 00:05:38,852
And we're color coding it using trip distance,

82
00:05:38,852 --> 00:05:40,570
and we end up with this plot.

83
00:05:40,570 --> 00:05:44,222
It's called a tile plot,

84
00:05:44,222 --> 00:05:49,040
because we have tiles in the plot.

85
00:05:49,040 --> 00:05:52,010
And so we have this mosaic looking shape.

86
00:05:52,010 --> 00:05:55,053
And we can see that pickup_neighborhood is in the x

87
00:05:55,053 --> 00:05:57,970
axis, dropoff_neighborhood is in the y axis.

88
00:05:57,970 --> 00:06:05,760
And the intensity of the color is going to show the trip distance.

89
00:06:05,760 --> 00:06:11,180
What we notice about this plot is it seems to be a little bit arbitrary.

90
00:06:11,180 --> 00:06:14,620
The arbitrariness has to do with the fact that

91
00:06:15,930 --> 00:06:19,620
the neighborhoods are sorted in alphabetical order.

92
00:06:20,950 --> 00:06:21,690
We can see,

93
00:06:21,690 --> 00:06:25,040
starting with Battery Park, going all the way to Yorkville.

94
00:06:26,290 --> 00:06:29,160
And we can see that the drop off neighborhood are sorted

95
00:06:29,160 --> 00:06:29,890
in the same order.

96
00:06:31,740 --> 00:06:33,094
Right, so because of that, and

97
00:06:33,094 --> 00:06:36,207
because the neighborhoods are really just categories we should be able to

98
00:06:36,207 --> 00:06:38,300
reorder them in any other way that we want to.

99
00:06:38,300 --> 00:06:40,860
And based on that this plot is gonna look a little bit

100
00:06:40,860 --> 00:06:41,600
different every time.

101
00:06:42,740 --> 00:06:46,620
So we might be asking is there a particular ordering

102
00:06:46,620 --> 00:06:49,280
that actually makes sense, okay.

103
00:06:49,280 --> 00:06:54,227
Alphabetical ordering, in this case, doesn't really make sense, but

104
00:06:54,227 --> 00:06:58,057
if we could somehow put the neighborhoods that are closer

105
00:06:58,057 --> 00:07:00,870
together next to each other on this plot.

106
00:07:00,870 --> 00:07:05,719
And come up with an ordering so that we start let's say somewhere towards

107
00:07:05,719 --> 00:07:10,011
the bottom of Manhattan where we have the financial district and

108
00:07:10,011 --> 00:07:12,090
the downtown neighborhoods.

109
00:07:12,090 --> 00:07:17,084
And we slowly move our way up to the northern tip of Manhattan where we

110
00:07:17,084 --> 00:07:20,769
have Inwood and some of the other neighborhoods.

111
00:07:22,530 --> 00:07:28,870
So the answer is yes, and we don't have to do much.

112
00:07:28,870 --> 00:07:31,830
That is what the seriation package does for us.

113
00:07:31,830 --> 00:07:35,620
It comes up with a particular ordering of these neighborhoods so

114
00:07:35,620 --> 00:07:37,990
that based on the trip distances,

115
00:07:37,990 --> 00:07:42,690
the average trip distances, we can come up with some kind of ordering.

116
00:07:42,690 --> 00:07:45,960
The ordering doesn't always have to be unique, but

117
00:07:45,960 --> 00:07:49,990
it's gonna be the one that makes more sense than simply ordering

118
00:07:49,990 --> 00:07:54,974
the neighborhoods based on alphabetical order.

119
00:07:54,974 --> 00:07:59,150
Okay, so, to do that, it seems like

120
00:07:59,150 --> 00:08:04,110
all we need to do is go back to our data right now.

121
00:08:04,110 --> 00:08:07,540
Res which has the results and

122
00:08:07,540 --> 00:08:12,300
go to the column at pickup_nb and dropoff_nb.

123
00:08:12,300 --> 00:08:15,480
As we can see here in the data both of them are factors, right.

124
00:08:15,480 --> 00:08:20,420
So we're happy with that, it's just that the levels for these columns

125
00:08:20,420 --> 00:08:24,870
have the Manhattan neighborhoods sorted in alphabetical order.

126
00:08:24,870 --> 00:08:29,460
And all we need to do is to reorder them so that they

127
00:08:29,460 --> 00:08:34,240
show up in the order that seriation determines to be the best order.

128
00:08:34,240 --> 00:08:38,480
And this is what we do in the following step.

129
00:08:40,170 --> 00:08:43,270
So we're gonna go and based on what seriation returned,

130
00:08:43,270 --> 00:08:47,910
we're gonna find the right levels for the neighborhoods to be ordered.

131
00:08:47,910 --> 00:08:50,590
And we're gonna go back to the results, and

132
00:08:50,590 --> 00:08:54,270
we're gonna pass the columns to the factor function.

133
00:08:54,270 --> 00:08:56,990
And we're gonna specify the levels and

134
00:08:56,990 --> 00:08:58,820
this time they're gonna ordered in a new way.

135
00:08:59,890 --> 00:09:02,880
Once we do that, we can recreate the plot from the last step.

136
00:09:03,900 --> 00:09:07,230
And this time, we can see something that makes more sense.

137
00:09:07,230 --> 00:09:10,600
So we start right here with Chinatown,

138
00:09:10,600 --> 00:09:13,600
Financial District, Tribeca, Little Italy.

139
00:09:13,600 --> 00:09:17,350
So these are some of the downtown neighborhoods.

140
00:09:17,350 --> 00:09:22,500
And slowly we move our way up, until we get to the up-town

141
00:09:22,500 --> 00:09:26,850
neighborhoods of Harlem, Hamilton Heights, and Washington Heights.

142
00:09:26,850 --> 00:09:30,540
You'll notice that the ordering is not perfect.

143
00:09:30,540 --> 00:09:34,720
It might, for example, have been better to put Inwood at

144
00:09:34,720 --> 00:09:39,960
the very top, because Inwood is the most uptown neighborhood.

145
00:09:39,960 --> 00:09:42,960
And, in the case of downtown, there is also some subjectivity.

146
00:09:42,960 --> 00:09:44,660
So, this is not gonna be perfect, and,

147
00:09:44,660 --> 00:09:48,210
if we use a larger size for the data,

148
00:09:48,210 --> 00:09:51,830
we're gonna get a better ordering than the one that we have here.

149
00:09:51,830 --> 00:09:54,523
But it's good enough for our purposes.

150
00:09:54,523 --> 00:09:57,487
We can also see that in the case of Inwood,

151
00:09:57,487 --> 00:10:00,210
we don't have too many trips anyway.

152
00:10:00,210 --> 00:10:02,984
So Inwood is one of those neighborhoods that is not often

153
00:10:02,984 --> 00:10:04,170
traveled too and from.

154
00:10:04,170 --> 00:10:08,157
And so since we only have six months of data, and

155
00:10:08,157 --> 00:10:13,540
we only took 5% of the data for those six months, we just don't

156
00:10:13,540 --> 00:10:19,140
have enough information about some of the neighborhoods to go by.

157
00:10:21,020 --> 00:10:26,390
But other than that, this matrix, this tile plot should make sense.

158
00:10:26,390 --> 00:10:30,230
We can see that in the diagonal direction, the colors are nearly

159
00:10:30,230 --> 00:10:35,050
white and as we move away towards the off diagonal elements,

160
00:10:35,050 --> 00:10:38,730
we can see a darker shade of blue.

