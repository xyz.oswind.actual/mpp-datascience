0
00:00:01,500 --> 00:00:06,920
To believe it or not, there is actually more that we have to do to

1
00:00:06,920 --> 00:00:10,520
clean up if you will, our factor column.

2
00:00:10,520 --> 00:00:16,370
We eliminated all of the levels from the factor columns that

3
00:00:16,370 --> 00:00:21,580
belong to neighborhoods that are outside of the Manhattan limits,

4
00:00:21,580 --> 00:00:25,850
but there is another thing that we have to do to fix that.

5
00:00:25,850 --> 00:00:30,380
For now, let's set it aside, but I'm just throwing it out there

6
00:00:31,520 --> 00:00:36,110
because I want you to know that factors are still very important.

7
00:00:36,110 --> 00:00:40,500
And there is a lot of little things that we sometimes take for

8
00:00:40,500 --> 00:00:44,160
granted when it comes to dealing with factors that come and

9
00:00:45,200 --> 00:00:48,440
kind of comeback to kick us a little bit later down the road.

10
00:00:49,820 --> 00:00:54,220
But let's go to the data now and let's look at some other columns,

11
00:00:54,220 --> 00:00:58,240
and at some point we're gonna return to the neighborhood columns and

12
00:00:58,240 --> 00:01:00,730
see some other things that we need to do.

13
00:01:00,730 --> 00:01:04,460
For now let's shift gear a little bit.

14
00:01:04,460 --> 00:01:08,010
Let's look at some other columns in the data

15
00:01:09,270 --> 00:01:11,850
that might be interesting for us to look at.

16
00:01:11,850 --> 00:01:13,160
In this case,

17
00:01:13,160 --> 00:01:16,170
let's say we're interested in the column trip distance.

18
00:01:16,170 --> 00:01:20,320
Trip distance is a numeric column, and so one of the things that we can

19
00:01:20,320 --> 00:01:25,370
do in RevoScaleR is we can use the rxHistogram function

20
00:01:25,370 --> 00:01:30,980
to plot a histogram of trip distance.

21
00:01:30,980 --> 00:01:35,370
I'm gonna do that now and out comes the following histogram.

22
00:01:35,370 --> 00:01:38,940
This is good for us to look at because we can see in this case for

23
00:01:38,940 --> 00:01:40,060
example that we have

24
00:01:41,270 --> 00:01:45,460
some cases where the trip distance is either zero or below zero.

25
00:01:45,460 --> 00:01:49,130
So this is probably something that is erroneous and

26
00:01:49,130 --> 00:01:52,620
we need to either ignore or fix about the data.

27
00:01:52,620 --> 00:01:56,130
We can see that the majority of the trips are pretty short trips.

28
00:01:56,130 --> 00:01:59,810
The great majority of trips travel less than five miles.

29
00:01:59,810 --> 00:02:01,980
The unit here is in miles.

30
00:02:01,980 --> 00:02:06,220
And we can see that there is a kind of a second bump there in

31
00:02:06,220 --> 00:02:07,480
the histogram.

32
00:02:07,480 --> 00:02:09,860
Whether or not that bump is significant or

33
00:02:09,860 --> 00:02:13,595
not is hard to determine, but we can look at that.

34
00:02:13,595 --> 00:02:18,059
We can see if they correspond to maybe trips between two

35
00:02:18,059 --> 00:02:22,815
neighborhoods that are pretty far away from each other but

36
00:02:22,815 --> 00:02:25,936
where people tend to travel between.

37
00:02:29,794 --> 00:02:35,600
It's interesting to look at this histogram for another reason.

38
00:02:35,600 --> 00:02:41,850
We don't have too many visualization functions in RevoScaleR.

39
00:02:41,850 --> 00:02:42,820
Now the reason for

40
00:02:42,820 --> 00:02:46,230
that is because visualizations with big data are tricky.

41
00:02:48,130 --> 00:02:52,960
Think about for example creating a scatter plot of

42
00:02:52,960 --> 00:02:58,050
the data when you have, let's say, a billion rows in the data, right?

43
00:02:58,050 --> 00:03:02,770
Even assuming that plotting a billion rows on a scatter plot

44
00:03:02,770 --> 00:03:06,160
is still going to give us a plot that looks reasonably well.

45
00:03:07,370 --> 00:03:09,770
It's going to be very time consuming,

46
00:03:09,770 --> 00:03:13,700
because creating a visualization and just plotting a billion points on

47
00:03:13,700 --> 00:03:19,350
a scatter plot is not something that we can do fast.

48
00:03:19,350 --> 00:03:25,070
Even if it was possible, it would be difficult to implement it.

49
00:03:25,070 --> 00:03:30,690
And even if we could do it, it would probably not be a scatter plot that

50
00:03:30,690 --> 00:03:35,960
would look reasonably well because there's just far too many points.

51
00:03:35,960 --> 00:03:38,790
And they're probably gonna clutter the scatter plot.

52
00:03:39,810 --> 00:03:44,770
So, with that said, there are certainly, tools and

53
00:03:44,770 --> 00:03:49,370
packages out there that try to do data visualization on very

54
00:03:49,370 --> 00:03:53,790
large data sets, but in RevoScaleR, we don't do too much of that.

55
00:03:53,790 --> 00:03:58,420
Histogram on the other hand is not in the same category.

56
00:03:58,420 --> 00:04:02,690
In the case of the histogram, we can have billions of points but

57
00:04:02,690 --> 00:04:06,040
ultimately we can always come up with a histogram for

58
00:04:06,040 --> 00:04:11,310
the plot not going to depend on, the histogram

59
00:04:11,310 --> 00:04:15,032
is not gonna get cluttered because we have too many rows in our data.

60
00:04:15,032 --> 00:04:23,750
So most data visualizations such as scatter plots are going to be

61
00:04:23,750 --> 00:04:28,935
a little bit sensitive to how much data we have and in those cases,

62
00:04:28,935 --> 00:04:33,685
what we wanna do in RevoScaleR and we will do that in one of the next

63
00:04:33,685 --> 00:04:38,445
sections is, we probably wanna take a sample of the data and then

64
00:04:38,445 --> 00:04:42,875
draw scatter plots on the sample of the data instead of the whole data.

65
00:04:42,875 --> 00:04:45,935
And once we take a sample of the data that sample is going to be

66
00:04:45,935 --> 00:04:51,460
dumped in the data frame so that's the R objects, the data objects,

67
00:04:51,460 --> 00:04:56,490
the traditional R format for data that we're pretty familiar with.

68
00:04:56,490 --> 00:04:59,030
And that's the one that ggplot2 understands and

69
00:04:59,030 --> 00:05:03,430
all the other visualization packages understand.

70
00:05:03,430 --> 00:05:06,230
But in the case of the histogram, this histogram is

71
00:05:06,230 --> 00:05:09,610
really the distribution of the whole column in the data.

72
00:05:09,610 --> 00:05:10,960
It's not based on a sample.

73
00:05:12,700 --> 00:05:18,250
Let's look at another, Thing that we can do with RevoScaleR.

74
00:05:19,420 --> 00:05:21,690
So we're gonna run our X summary again.

75
00:05:21,690 --> 00:05:24,500
But if you remember from the histogram,

76
00:05:24,500 --> 00:05:26,030
we saw an interesting thing.

77
00:05:26,030 --> 00:05:28,000
We saw sort of a second peak.

78
00:05:28,000 --> 00:05:31,070
Let me go back to the histogram.

79
00:05:31,070 --> 00:05:39,010
We saw a second peak showing up here for miles between 16 and 20.

80
00:05:39,010 --> 00:05:43,340
And we were wondering even though it's a pretty small peak,

81
00:05:43,340 --> 00:05:46,970
we were wondering if there's maybe a trend to that.

82
00:05:46,970 --> 00:05:49,850
So we're gonna go back to our rxSummary function and

83
00:05:49,850 --> 00:05:51,290
we're gonna see if we can answer that.

84
00:05:52,450 --> 00:05:53,900
We're going to see,

85
00:05:53,900 --> 00:05:56,446
in this case we're interested in answering the following question.

86
00:05:56,446 --> 00:06:02,020
We wanna see if the distances between 16 and

87
00:06:02,020 --> 00:06:06,166
20, or in this case we're gonna limit it to 15 and 22.

88
00:06:06,166 --> 00:06:09,510
We're gonna see if those are commonly,

89
00:06:10,820 --> 00:06:14,780
those are distances that are traveled most commonly between

90
00:06:14,780 --> 00:06:17,740
two neighborhoods that are the same neighborhoods, right?

91
00:06:17,740 --> 00:06:21,110
So it could be that there are two neighborhoods that are pretty far

92
00:06:21,110 --> 00:06:22,650
away from each other, and

93
00:06:22,650 --> 00:06:25,000
that people commonly travel between those two.

94
00:06:25,000 --> 00:06:28,572
And we wanna see if we can use rxSummary to answer that.

95
00:06:28,572 --> 00:06:32,600
So rxSummary is gonna give us in this case a summary of

96
00:06:32,600 --> 00:06:34,840
pick up neighborhood to drop off neighborhood.

97
00:06:35,900 --> 00:06:41,890
And we're using this row selection arguments to limit the data,

98
00:06:43,210 --> 00:06:49,780
to be the only cases where the trip distance is greater than 15.

99
00:06:49,780 --> 00:06:52,870
And the trip distance is less than 22, right?

100
00:06:54,170 --> 00:06:58,450
So that's more or less where we saw that second peak happen.

101
00:06:58,450 --> 00:07:02,110
So we're gonna limit the data to only the following trips.

102
00:07:02,110 --> 00:07:06,973
And then we're gonna summarize the data to see between which

103
00:07:06,973 --> 00:07:10,321
neighborhoods those trips were traveled

104
00:07:14,262 --> 00:07:19,101
When we look at ahead of that data, and we sort it by descending counts,

105
00:07:19,101 --> 00:07:21,720
we can see something interesting.

106
00:07:21,720 --> 00:07:26,670
We can see that the most common trips

107
00:07:26,670 --> 00:07:31,620
where the distance traveled was somewhere between 15 and 22 miles.

108
00:07:31,620 --> 00:07:36,030
Or actually happening between the same neighborhoods.

109
00:07:36,030 --> 00:07:39,680
The most common one is happening from midtown to midtown.

110
00:07:39,680 --> 00:07:42,220
The next one is happening from the upper east side

111
00:07:42,220 --> 00:07:43,580
to the upper east side.

112
00:07:43,580 --> 00:07:46,170
So these are the trips that somebody is getting picked up

113
00:07:46,170 --> 00:07:49,220
from say midtown and dropped off back in midtown.

114
00:07:49,220 --> 00:07:50,830
Or picked up in the upper east side and

115
00:07:50,830 --> 00:07:52,910
dropped off again in the upper east side.

116
00:07:52,910 --> 00:07:57,500
And then between and upper west side and upper west side.

117
00:07:57,500 --> 00:07:59,580
So this is an interesting trend.

118
00:07:59,580 --> 00:08:01,770
What it's telling us is, it's actually

119
00:08:02,980 --> 00:08:07,520
counteracting our earlier intuition when looking at the histogram.

120
00:08:07,520 --> 00:08:09,420
We were assuming that maybe

121
00:08:10,430 --> 00:08:13,420
the reason we have sort of a second peak going on there.

122
00:08:13,420 --> 00:08:17,350
Is because, two neighborhoods are pretty far from each other.

123
00:08:17,350 --> 00:08:19,620
But people commonly travel between the two.

124
00:08:19,620 --> 00:08:22,420
And now we can see that it's probably the case that

125
00:08:22,420 --> 00:08:25,950
the second peak corresponds more to people who

126
00:08:26,990 --> 00:08:30,210
take a taxi starting at the same location,

127
00:08:30,210 --> 00:08:34,610
ride around town maybe to run a bunch of errands, but eventually

128
00:08:34,610 --> 00:08:37,870
come back to the same location to get dropped back off again.

129
00:08:37,870 --> 00:08:42,420
So, of course this is just an assumption, and

130
00:08:42,420 --> 00:08:46,720
we would have to collect more data to confirm if this is true or not.

131
00:08:46,720 --> 00:08:51,046
But it's interesting to look at these counts, seeing just how common

132
00:08:51,046 --> 00:08:54,489
it is, in this case, when looking at large distances,

133
00:08:54,489 --> 00:08:59,288
that the pickup neighborhood and the drop-off neighborhoods are the same.

