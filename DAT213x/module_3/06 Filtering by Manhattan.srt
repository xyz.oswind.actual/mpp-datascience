0
00:00:01,390 --> 00:00:03,640
So let's see how far we've gotten so

1
00:00:03,640 --> 00:00:08,320
far in our data exploration and analysis process.

2
00:00:08,320 --> 00:00:15,730
I'm gonna run rxGetInfo and we're just gonna say nyc_xdf.

3
00:00:15,730 --> 00:00:20,805
So the xdf data, the whole data and

4
00:00:20,805 --> 00:00:24,492
numRows = 10, okay?

5
00:00:24,492 --> 00:00:29,579
So we started with the data, and we had some challenges that we faced.

6
00:00:29,579 --> 00:00:34,108
We had to make sure that the column types were correct,

7
00:00:34,108 --> 00:00:38,840
we had to combine six CSVs into a single XDF file and so on.

8
00:00:40,220 --> 00:00:45,710
The next thing we did is, we asked how can we enrich this data?

9
00:00:46,840 --> 00:00:50,220
We noticed for example, that we have two columns in the data

10
00:00:50,220 --> 00:00:53,790
called pickup_datetime and dropoff_datetime.

11
00:00:53,790 --> 00:00:57,062
And we said that there's probably some interesting features,

12
00:00:57,062 --> 00:00:58,767
we could extract from this data.

13
00:00:58,767 --> 00:01:02,467
Namely, the hour of the day and we didn't have to

14
00:01:02,467 --> 00:01:06,634
necessarily hone in on the particular hour of the day.

15
00:01:06,634 --> 00:01:10,243
We could just basically bucket it based on,

16
00:01:10,243 --> 00:01:16,093
is it sort of the morning rush hour, is it the late in the evening crowd,

17
00:01:16,093 --> 00:01:19,816
is it in the wee hours of the morning and so on.

18
00:01:19,816 --> 00:01:24,179
Another interesting feature to extract from the pickup_datetime and

19
00:01:24,179 --> 00:01:27,164
dropoff_datetime would be the day of the week.

20
00:01:27,164 --> 00:01:32,781
So we have another column here called pickup_dow.

21
00:01:32,781 --> 00:01:35,450
And here we have dropoff_hour and dropoff_dow.

22
00:01:36,580 --> 00:01:38,098
Now, the majority of trips,

23
00:01:38,098 --> 00:01:41,459
the dropoff day of week is not necessarily gonna be interesting.

24
00:01:41,459 --> 00:01:45,138
Cuz it's gonna be the same as the pickup_day of week unless,

25
00:01:45,138 --> 00:01:48,842
the trip happened before midnight and ended after midnight.

26
00:01:48,842 --> 00:01:51,421
But those are pretty rare, so in this case,

27
00:01:51,421 --> 00:01:55,397
the dropoff_day of the week is maybe not as interesting a feature as

28
00:01:55,397 --> 00:01:58,839
just knowing the day of the week when the pickup happened.

29
00:01:58,839 --> 00:02:03,540
For the hour of the day, both of those are certainly useful to know.

30
00:02:03,540 --> 00:02:08,314
So that was the first transformation that we did.

31
00:02:08,314 --> 00:02:13,608
We also extracted the duration of the trip which is in seconds.

32
00:02:13,608 --> 00:02:19,170
And the last set of transformations was using the coordinates.

33
00:02:19,170 --> 00:02:23,548
So here, we have pickup_longitude and pickup_latitude in the data,

34
00:02:23,548 --> 00:02:27,500
as well as dropoff_longitude and dropoff_latitude.

35
00:02:27,500 --> 00:02:31,751
And in this last transformation that we performed,

36
00:02:31,751 --> 00:02:36,391
we used this coordinates to extract pickup_borough and

37
00:02:36,391 --> 00:02:40,645
dropoff_borough, and pickup_neighborhood and

38
00:02:40,645 --> 00:02:46,365
dropoff_neighborhood, pickup_borough and dropoff_borough.

39
00:02:46,365 --> 00:02:50,140
We notice that, first of all, it's certainly possible that

40
00:02:50,140 --> 00:02:53,347
occasionally, we're gonna have missing values.

41
00:02:53,347 --> 00:02:54,686
There's not much we can do about that.

42
00:02:54,686 --> 00:02:58,153
It's possible for example, that for

43
00:02:58,153 --> 00:03:01,954
a particular set of pickup coordinates.

44
00:03:01,954 --> 00:03:04,583
We could see that the GIS package or

45
00:03:04,583 --> 00:03:10,120
the shape file was not able to brush it against any neighbourhood.

46
00:03:10,120 --> 00:03:14,030
This could be that it's a way out of town somewhere, it could be that

47
00:03:14,030 --> 00:03:19,460
it's in a location where the information was not available.

48
00:03:19,460 --> 00:03:23,950
It could also be that the coordinates were wrongly recorded

49
00:03:23,950 --> 00:03:30,600
and took us to a location that is not recorded for us.

50
00:03:30,600 --> 00:03:36,693
Another possibility of course, is that in this in dataset,

51
00:03:36,693 --> 00:03:43,032
a taxi trip begun in Manhattan and ended outside of Manhattan.

52
00:03:43,032 --> 00:03:44,729
Begun outside of Manhattan and

53
00:03:44,729 --> 00:03:48,930
ended in Manhattan,r it just wasn't in Manhattan all together.

54
00:03:48,930 --> 00:03:54,570
And in those cases, because our shape files was limited to be only

55
00:03:54,570 --> 00:04:01,280
that Manhattan City limits, we would still end up with NAs, right?

56
00:04:01,280 --> 00:04:05,584
The last thing we did to kind of clean up our data and

57
00:04:05,584 --> 00:04:10,400
add new features to it, is we set the dropoff_nhood and

58
00:04:10,400 --> 00:04:12,977
dropoff_borough columns.

59
00:04:12,977 --> 00:04:17,957
They're both factor columns, and they contain levels that are outside

60
00:04:17,957 --> 00:04:21,590
of the ones that we're interested in.

61
00:04:21,590 --> 00:04:25,782
And so we created a column called pickup_nb and dropoff_nb.

62
00:04:25,782 --> 00:04:31,168
What they do is they basically limit the neighborhood

63
00:04:31,168 --> 00:04:37,640
columns to the factor levels that are only for Manhattan.

64
00:04:37,640 --> 00:04:41,820
So if we look at pickup_nb, and if we look at pickup_nhood,

65
00:04:41,820 --> 00:04:45,790
we would see the same information in the data, right?

66
00:04:45,790 --> 00:04:48,680
And it looks like those two columns are identical.

67
00:04:48,680 --> 00:04:54,328
But if we go back to our rxGetInfo and

68
00:04:54,328 --> 00:04:58,292
ask getVarInfo = TRUE.

69
00:04:58,292 --> 00:05:03,155
We can see that the difference between those two columns are not

70
00:05:03,155 --> 00:05:06,220
in the data itself but in the metadata.

71
00:05:06,220 --> 00:05:10,772
Here's the information about the column called pickup_nhood.

72
00:05:10,772 --> 00:05:15,944
And we can see that it's a factor column with 263 levels.

73
00:05:15,944 --> 00:05:21,488
If we scroll down here, we can see that in the case of the pickup_nb

74
00:05:21,488 --> 00:05:25,943
column, even though those two columns are the same,

75
00:05:25,943 --> 00:05:31,388
this one is a factor column with only 28 factor levels, right?

76
00:05:31,388 --> 00:05:34,705
So this is because the 28 are simply

77
00:05:34,705 --> 00:05:39,203
the neighborhoods that are inside of Manhattan.

78
00:05:39,203 --> 00:05:43,992
So that's it, we got the data this far, and

79
00:05:43,992 --> 00:05:49,436
we have a now richer dataset than we started with.

80
00:05:49,436 --> 00:05:54,430
And the next thing we did is that we spend sometimes cleaning up

81
00:05:54,430 --> 00:05:55,302
our data.

82
00:05:55,302 --> 00:05:59,649
So we spent some time making sure that the data is in

83
00:05:59,649 --> 00:06:04,731
the right format, that we deal with the factors and so on.

84
00:06:04,731 --> 00:06:08,799
And then we looked at some outliers, we looked at some distributions for

85
00:06:08,799 --> 00:06:10,466
certain columns of the data,

86
00:06:10,466 --> 00:06:13,083
we asked some basic questions about the data.

87
00:06:13,083 --> 00:06:18,012
And we noticed that we have things that we probably shouldn't be

88
00:06:18,012 --> 00:06:18,840
having.

89
00:06:18,840 --> 00:06:22,588
We had trips where the distance traveled was negative.

90
00:06:22,588 --> 00:06:25,666
We had trips where the fare amount was negative, right?

91
00:06:25,666 --> 00:06:29,314
And it's one question to ask if this makes business sense.

92
00:06:29,314 --> 00:06:32,237
It's another question to ask if for

93
00:06:32,237 --> 00:06:37,110
the sake of the particular analysis that we're gonna run,

94
00:06:37,110 --> 00:06:42,379
we need to necessarily carry around these CodonCode outliers.

95
00:06:42,379 --> 00:06:47,167
So the next thing we're gonna do, is we're gonna go to our data.

96
00:06:47,167 --> 00:06:51,177
And we're going to only keep the rows that make sense,

97
00:06:51,177 --> 00:06:55,972
only keep the rows that are really relevant to the analysis that we

98
00:06:55,972 --> 00:06:57,295
want to perform.

99
00:06:57,295 --> 00:07:01,694
And also only keep the columns that are the ones that

100
00:07:01,694 --> 00:07:04,980
we're going to use for the analysis.

101
00:07:07,120 --> 00:07:10,291
So we're gonna create a new XDF file, and

102
00:07:10,291 --> 00:07:15,463
it's gonna be called yellow_tripdata_2016_manhattan.

103
00:07:15,463 --> 00:07:21,720
So right now, it doesn't exist, but once the next code snippet,

104
00:07:21,720 --> 00:07:27,216
our rxDataStep initials running, we'll have it set up.

105
00:07:27,216 --> 00:07:30,383
So in this case, we have an rxDataStep,

106
00:07:30,383 --> 00:07:32,811
we read from New York City XDF.

107
00:07:32,811 --> 00:07:39,270
But this time, we output to a new data frame called mht_xdf.

108
00:07:39,270 --> 00:07:43,605
And what we're gonna do is, we're going to select only specific rows

109
00:07:43,605 --> 00:07:46,180
of the data based on the following logic.

110
00:07:46,180 --> 00:07:51,162
So we're gonna select rows where passenger_count is greater than 0,

111
00:07:51,162 --> 00:07:56,077
where trip_distance is greater than equal to 0, but less than 30.

112
00:07:56,077 --> 00:08:03,210
Trip_duration is somewhere between 0 and 24 hours, right?

113
00:08:03,210 --> 00:08:06,913
So this is just trip_duration is in second, so

114
00:08:06,913 --> 00:08:09,327
this would give us 24 hours.

115
00:08:09,327 --> 00:08:14,270
And where the pickup_borough is inside Manhattan and

116
00:08:14,270 --> 00:08:18,790
the dropoff_borough is also inside Manhattan.

117
00:08:18,790 --> 00:08:22,538
And where the pickup_neighborhoods are not missing, so

118
00:08:22,538 --> 00:08:24,458
we need to know what they are.

119
00:08:24,458 --> 00:08:27,253
And where the fair_amount is greater than 0.

120
00:08:27,253 --> 00:08:33,432
This is not really a unique way to identify what is legitimate

121
00:08:33,432 --> 00:08:38,510
data that would make the analysis interesting.

122
00:08:38,510 --> 00:08:42,190
But it's just one way that we can kind of think about,

123
00:08:42,190 --> 00:08:47,180
these are the things that we sort of expect our data to comply to.

124
00:08:49,320 --> 00:08:53,413
Finally, because we are using a function inside of

125
00:08:53,413 --> 00:08:55,230
the stringr package.

126
00:08:55,230 --> 00:08:59,859
Namely, str_detect, we're gonna specify transformpackages = stringr.

127
00:08:59,859 --> 00:09:04,723
And we're gonna use this argument called varsToDrop to drop a bunch

128
00:09:04,723 --> 00:09:07,890
of variables from the data when we run this.

129
00:09:09,050 --> 00:09:12,950
So the transformation finished running, and now,

130
00:09:12,950 --> 00:09:15,100
if we go back to our working directory.

131
00:09:15,100 --> 00:09:17,594
We can see that we have a new XDF file called

132
00:09:17,594 --> 00:09:20,385
yellow_tripdata_2016_manhattan.

133
00:09:20,385 --> 00:09:23,736
It's slightly smaller than the original data.

134
00:09:23,736 --> 00:09:28,260
So we can see that the great majority of the data was

135
00:09:28,260 --> 00:09:29,673
carried over.

136
00:09:29,673 --> 00:09:33,222
In fact, the reason it's probably smaller is because we dropped

137
00:09:33,222 --> 00:09:34,899
a bunch of columns not so much,

138
00:09:34,899 --> 00:09:37,750
because a lot of your's were thrown out of the data.

139
00:09:40,158 --> 00:09:43,010
And we're gonna do one more thing before we move

140
00:09:43,010 --> 00:09:45,168
onto the next section.

141
00:09:45,168 --> 00:09:50,039
We began our course by taking a sample of the data and

142
00:09:50,039 --> 00:09:54,809
storing it in a data frame that we carried around.

143
00:09:54,809 --> 00:09:58,905
We're gonna do that now a second time, but this time for

144
00:09:58,905 --> 00:10:03,610
the Manhattan XDF file, and we're gonna be a little bit smarter

145
00:10:03,610 --> 00:10:06,665
about the way that we took the sample.

146
00:10:06,665 --> 00:10:09,715
So the first time that we took the sample from data, we simply just

147
00:10:09,715 --> 00:10:14,075
went to one of the CSV files, and we grab the top 1,000 rows.

148
00:10:14,075 --> 00:10:17,125
Now we're gonna go to the XDF file, and

149
00:10:17,125 --> 00:10:20,737
we're gonna take a random sample of the data.

150
00:10:20,737 --> 00:10:25,021
So we're gonna do a column transformation, we're gonna generate

151
00:10:25,021 --> 00:10:29,315
some random numbers between zero and one using the runit function.

152
00:10:29,315 --> 00:10:33,502
And then we're gonna select only those rows where the random number

153
00:10:33,502 --> 00:10:37,844
generated is less than 0.01 and this will give us 1% of our data.

154
00:10:37,844 --> 00:10:41,798
We can see that we now have a sample of the data with

155
00:10:41,798 --> 00:10:45,183
approximately 29,000 rows in it.

156
00:10:45,183 --> 00:10:49,367
So we have the whole data, we have a sample of the data, which is a data

157
00:10:49,367 --> 00:10:53,335
frame that we can use to visualize the data in particular ways, or

158
00:10:53,335 --> 00:10:55,590
run a quick test on the data and so on.

159
00:10:57,960 --> 00:11:03,333
So we're now in a good place, we have the data in a clean format.

160
00:11:03,333 --> 00:11:07,510
We have a bunch of new columns that we've added to make the data richer.

161
00:11:07,510 --> 00:11:11,430
And we can get a little bit more serious about the analysis now.

162
00:11:11,430 --> 00:11:16,113
We can start asking more serious questions about the data,

163
00:11:16,113 --> 00:11:20,047
we can start looking at more visualizations, and

164
00:11:20,047 --> 00:11:24,277
see what sorts of stories we can tell about this data.

