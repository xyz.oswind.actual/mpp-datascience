0
00:00:01,270 --> 00:00:04,970
So let's set aside the neighborhood relationships and

1
00:00:04,970 --> 00:00:10,110
go back to some of the other columns that we extracted from our data.

2
00:00:10,110 --> 00:00:14,400
Namely, pick up day of week and pick up hour.

3
00:00:15,800 --> 00:00:19,770
And those two columns are gonna be the ones that we're gonna look at.

4
00:00:19,770 --> 00:00:23,880
And what we wanna see is if we can find out

5
00:00:23,880 --> 00:00:28,692
some interesting information about tip percent and about fare amount.

6
00:00:30,990 --> 00:00:34,720
And for fare amount, of course, because it's going to depend on

7
00:00:34,720 --> 00:00:37,880
the length of the trip, we're gonna standardize it a little bit.

8
00:00:37,880 --> 00:00:40,600
So here we're going to the data,

9
00:00:40,600 --> 00:00:45,166
we're using rxCube to get summaries for fair amount but

10
00:00:45,166 --> 00:00:49,733
what we're gonna do is we're gonna take fair amount and

11
00:00:49,733 --> 00:00:54,717
we're gonna divide it by the trip duration divided by 60.

12
00:00:54,717 --> 00:00:59,237
Because trip duration is in seconds by dividing it by 60 we get

13
00:00:59,237 --> 00:01:03,246
trip duration in minutes and so we get the fair amount for

14
00:01:03,246 --> 00:01:05,140
every minute of the trip.

15
00:01:06,910 --> 00:01:10,679
So we're gonna look at tip percent and fare amount for

16
00:01:10,679 --> 00:01:13,871
every minute of the trip as our last example.

17
00:01:13,871 --> 00:01:15,770
Kinda visualizing the data and

18
00:01:15,770 --> 00:01:19,240
getting a sense of what we're dealing with.

19
00:01:19,240 --> 00:01:23,722
Let's quickly jump now to our visualization.

20
00:01:23,722 --> 00:01:28,367
And in this case, we can see the day of the week showing up in the x

21
00:01:28,367 --> 00:01:33,278
axis, and we can see the pick up hour which is sort of bend depending

22
00:01:33,278 --> 00:01:38,102
on the particular hour of instead of the particular hour of the day

23
00:01:38,102 --> 00:01:43,013
bend on what time of the day it is in general based on categories that

24
00:01:43,013 --> 00:01:44,800
we chose ahead of time.

25
00:01:45,870 --> 00:01:49,230
We can see that there's some color coding going on and the color coding

26
00:01:49,230 --> 00:01:53,410
is showing us the fare amount for every minute of the ride.

27
00:01:54,660 --> 00:01:57,740
What stands out as far as the color coding goes is that,

28
00:01:59,200 --> 00:02:03,830
on a weekday we can see that we pay the most for

29
00:02:03,830 --> 00:02:08,320
trips that are happening between 1 AM and 5 AM.

30
00:02:08,320 --> 00:02:11,900
Now, I don't think that this is accidental I think that there is

31
00:02:11,900 --> 00:02:17,630
actually a surcharge in the fare for trips that are happening

32
00:02:17,630 --> 00:02:20,870
very late and because of that we're gonna be paying higher.

33
00:02:20,870 --> 00:02:22,670
It could also just be supply and demand.

34
00:02:22,670 --> 00:02:27,964
But it's something worth investigating.

35
00:02:27,964 --> 00:02:30,840
Now, let's look at some information about tip percent.

36
00:02:30,840 --> 00:02:34,020
So tip percent is not color coded on this tile plot,

37
00:02:34,020 --> 00:02:38,390
but the average tip percent is shown for

38
00:02:38,390 --> 00:02:42,450
the different categories and if we take a closer look at it,

39
00:02:42,450 --> 00:02:47,470
we can see that the most generous tippers seem to be

40
00:02:48,520 --> 00:02:55,910
people who travel on a weekday from 5 AM to 9 AM.

41
00:02:55,910 --> 00:03:01,420
Probably people going to work, the morning rush hour traffic.

42
00:03:01,420 --> 00:03:03,690
And if we go back up here,

43
00:03:03,690 --> 00:03:08,910
we can also see people tipping highly between 4 PM to 6 PM, so

44
00:03:10,130 --> 00:03:15,230
that's probably people going back home from work.

45
00:03:16,310 --> 00:03:21,590
And from 6 PM to 10 PM, and again from 10 PM to 1 AM.

46
00:03:21,590 --> 00:03:26,400
So we have definitely It seems to be the case that

47
00:03:26,400 --> 00:03:30,940
people are more likely to tip higher during weekdays than on weekends.

48
00:03:30,940 --> 00:03:36,800
And during hours when there is more traffic going on in general than

49
00:03:36,800 --> 00:03:39,480
during the very late night hours.

50
00:03:40,870 --> 00:03:43,330
So that's what we have with this plot.

51
00:03:45,490 --> 00:03:50,600
We kind of started this module with a discussion of,

52
00:03:50,600 --> 00:03:55,200
how can we create visualizations that

53
00:03:57,450 --> 00:04:00,040
show us trends that are going on in the data?

54
00:04:00,040 --> 00:04:04,840
And we saw quite a few examples of such visualizations.

55
00:04:06,460 --> 00:04:10,280
We also mentioned, and it's important in the process of doing

56
00:04:10,280 --> 00:04:13,270
this, to occasionally take a step back and

57
00:04:13,270 --> 00:04:17,850
go back to our data and see how we could have done things better.

58
00:04:19,640 --> 00:04:23,750
So in the next section, were gonna be moving from

59
00:04:23,750 --> 00:04:28,500
the topic of visualizing the data and creating summaries and tables.

60
00:04:28,500 --> 00:04:32,660
And were gonna get to more analytics.

61
00:04:34,840 --> 00:04:38,620
Analytics are generally more

62
00:04:40,030 --> 00:04:44,000
Involve in terms of how much work we have to do to get the data ready.

63
00:04:44,000 --> 00:04:48,260
And visualizations can be a good intermediate stuff for

64
00:04:48,260 --> 00:04:52,788
us to get a good sense of the data to get a good intuition build

65
00:04:52,788 --> 00:04:57,591
around the data on the various columns in the data before we move

66
00:04:57,591 --> 00:05:01,240
on to the step of running analytics on the data.

67
00:05:01,240 --> 00:05:07,816
And becoming more adapt at making predictions and such.

