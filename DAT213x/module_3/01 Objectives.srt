0
00:00:00,770 --> 00:00:03,840
One of the things that we're gonna notice, and

1
00:00:03,840 --> 00:00:07,890
this is gonna be a big theme throughout this course,

2
00:00:07,890 --> 00:00:12,030
is that a lot of things about our data, the way it's set up,

3
00:00:13,570 --> 00:00:17,480
kind of get carried over when we work inside of RevoScaleR.

4
00:00:17,480 --> 00:00:22,160
And this is especially true when dealing with our factor columns.

5
00:00:22,160 --> 00:00:26,610
So you may have taken an introductory course and

6
00:00:26,610 --> 00:00:29,060
noticed that factors are a big deal.

7
00:00:29,060 --> 00:00:33,890
Usually we spend an entire module dedicated to just

8
00:00:33,890 --> 00:00:36,820
working with factors, manipulating them and so on.

9
00:00:36,820 --> 00:00:40,140
And we're gonna see the same thing playing out here.

10
00:00:40,140 --> 00:00:44,900
So it's important to kind of know that being aware of our data,

11
00:00:44,900 --> 00:00:49,490
being aware of our column types, being aware of the especially

12
00:00:49,490 --> 00:00:53,990
our factor columns is going to be important in RevoScaleR,

13
00:00:53,990 --> 00:00:57,520
just as it is on OpenSourceR, and maybe even more so.

14
00:00:57,520 --> 00:01:02,282
Because occasionally things about working with data in chunks at

15
00:01:02,282 --> 00:01:08,530
a certain layer of complexity to the transformations and make it so

16
00:01:08,530 --> 00:01:12,580
that we have to be even more careful when dealing with our data.

