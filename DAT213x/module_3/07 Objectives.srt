0
00:00:01,520 --> 00:00:02,580
Welcome back.

1
00:00:02,580 --> 00:00:07,640
In this module we want to run some data visualizations,

2
00:00:07,640 --> 00:00:11,350
we want to find different ways that we can explore the data,

3
00:00:12,350 --> 00:00:17,330
look at various results and see what we can pick up from the data,

4
00:00:17,330 --> 00:00:21,010
what sorts of trends we can make out in the data.

5
00:00:22,110 --> 00:00:27,600
And a big theme of this module is gonna be that

6
00:00:27,600 --> 00:00:32,260
first of all once we start looking at the data, once we start

7
00:00:32,260 --> 00:00:37,760
breaking it down into different plots and charts and tables,

8
00:00:37,760 --> 00:00:43,178
we often notice things about the data that isn't maybe quite right.

9
00:00:43,178 --> 00:00:46,640
And so, sometimes we have to go back to our previous step

10
00:00:47,690 --> 00:00:51,240
of transforming the data, of cleaning the data.

11
00:00:51,240 --> 00:00:56,640
To do things right a second time so, it's a very iterative process and

12
00:00:56,640 --> 00:01:01,800
it's definitely not a linear process of going from transforming

13
00:01:01,800 --> 00:01:06,410
to cleaning to sub-setting to visualizing and

14
00:01:06,410 --> 00:01:08,200
plotting results and so on.

15
00:01:08,200 --> 00:01:11,520
And we're gonna circle back to a couple

16
00:01:11,520 --> 00:01:16,380
of the earlier steps that we took on a couple of occasions, so

17
00:01:16,380 --> 00:01:21,700
that we can see how that works in practice.

18
00:01:21,700 --> 00:01:26,310
Another big theme of this module is going to be that often

19
00:01:26,310 --> 00:01:27,350
when we have big data,

20
00:01:27,350 --> 00:01:31,430
and this could be big in the sense of having lots of rows, but it's

21
00:01:31,430 --> 00:01:35,885
also big in the sense that we can have lots of columns in the data.

22
00:01:35,885 --> 00:01:39,900
Or we can have particular columns in the data that are factor columns

23
00:01:39,900 --> 00:01:45,290
with lots and lots of categories and

24
00:01:45,290 --> 00:01:51,150
producing results from such data sets can mean that we end up with

25
00:01:51,150 --> 00:01:56,420
too many numbers to look at, too much information to sift through and

26
00:01:56,420 --> 00:02:01,920
so it can be a challenge to try to understand at a high level,

27
00:02:01,920 --> 00:02:05,500
what the information has saying, without necessarily

28
00:02:05,500 --> 00:02:08,980
spending too much time inspecting the data too closely.

29
00:02:08,980 --> 00:02:11,510
Unless we see, you know something interesting on this,

30
00:02:11,510 --> 00:02:15,530
we have a place where can put our finger and we can kinda hone down

31
00:02:15,530 --> 00:02:19,860
on the information, it's challenging at first to have

32
00:02:19,860 --> 00:02:24,150
lots of results to look at all at once and

33
00:02:24,150 --> 00:02:28,700
really be able to take it in without spending too much time on it.

34
00:02:28,700 --> 00:02:31,230
So that's why visualization is so helpful and

35
00:02:31,230 --> 00:02:36,400
we're gonna look at quite a few examples of visualization that allow

36
00:02:36,400 --> 00:02:43,630
us to kind of take away some of the not-so-important details,

37
00:02:43,630 --> 00:02:48,730
and be able to come up with plots where the information just jumps

38
00:02:48,730 --> 00:02:53,200
right at us and tells us what we need to know at a high level.

