0
00:00:01,490 --> 00:00:05,975
So let's go to the object that we created in the last section,

1
00:00:05,975 --> 00:00:11,200
nhoods_by_borough, and let's use it to pull out

2
00:00:11,200 --> 00:00:15,630
the names of all the neighborhoods that are in Manhattan.

3
00:00:15,630 --> 00:00:18,980
And we do so by basically looking at all the neighborhoods that have

4
00:00:18,980 --> 00:00:20,870
positive counts.

5
00:00:20,870 --> 00:00:24,310
And we end up with this short list of neighborhoods.

6
00:00:24,310 --> 00:00:27,440
So what we wanna do now is we wanna go back to the data.

7
00:00:27,440 --> 00:00:31,570
We were a little bit sloppy when we ran the transformation,

8
00:00:31,570 --> 00:00:35,200
we ended up with a factor column which is what we wanted to have.

9
00:00:35,200 --> 00:00:38,820
This is categorical data it makes sense for it to be a factor.

10
00:00:38,820 --> 00:00:42,550
But that factor column had a lot of neighborhoods in it that

11
00:00:42,550 --> 00:00:44,790
were not neighborhoods inside of Manhattan or

12
00:00:44,790 --> 00:00:47,750
even inside of New York City in some cases.

13
00:00:47,750 --> 00:00:50,380
So we're gonna go back and we're gonna fix that,

14
00:00:52,140 --> 00:00:55,320
the way we're gonna fix it is using another transformation.

15
00:00:55,320 --> 00:00:57,500
So here we have a transformation function, and

16
00:00:57,500 --> 00:01:02,260
what it does is it goes to the data and

17
00:01:02,260 --> 00:01:06,870
it runs the factor function on the pick up neighborhood and

18
00:01:06,870 --> 00:01:09,280
the drop off neighborhood columns.

19
00:01:09,280 --> 00:01:12,020
But this time, it's using levels.

20
00:01:12,020 --> 00:01:16,600
To specify what the factor levels need to be,

21
00:01:16,600 --> 00:01:21,110
and the specification is going to limit the factor levels

22
00:01:21,110 --> 00:01:23,390
to be only the neighborhoods that are in Manhattan.

23
00:01:24,708 --> 00:01:27,720
So at a high level, this is what the function does.

24
00:01:27,720 --> 00:01:29,730
There was a couple of things that

25
00:01:30,990 --> 00:01:35,560
I want to point out about this transformation, that are important,

26
00:01:35,560 --> 00:01:38,440
especially in the context of RevoScaleR.

27
00:01:38,440 --> 00:01:43,760
Right, so dealing with factors is not unique to RevoScaleR.

28
00:01:43,760 --> 00:01:47,480
But there is one thing that you're gonna notice about this function.

29
00:01:47,480 --> 00:01:49,910
I call the argument dataList.

30
00:01:49,910 --> 00:01:53,810
And so this is going to be a function of the data in this case

31
00:01:53,810 --> 00:01:55,560
it's just called data list.

32
00:01:55,560 --> 00:02:00,450
And it's gonna go to two columns of the data, pick up neighbourhood and

33
00:02:00,450 --> 00:02:02,760
drop off neighbourhood, and

34
00:02:02,760 --> 00:02:06,750
these two columns are right now factors with lots of levels in them.

35
00:02:06,750 --> 00:02:13,390
Some of them levels that we wanna drop so we pass that back to factor.

36
00:02:13,390 --> 00:02:17,670
And we specify only the levels that we're interested in,

37
00:02:17,670 --> 00:02:21,610
which are the Manhattan levels shown in the R console there.

38
00:02:24,530 --> 00:02:28,230
We might be wondering why don't we just go and

39
00:02:28,230 --> 00:02:33,470
take the data and think of it, not as a factor.

40
00:02:33,470 --> 00:02:36,850
Why don't we just convert it to a character column and

41
00:02:36,850 --> 00:02:41,230
then reconvert it back in to a factor.

42
00:02:41,230 --> 00:02:44,040
So why do we have to take the extra step here

43
00:02:44,040 --> 00:02:46,880
of finding the neighborhoods that are in Manhattan and

44
00:02:46,880 --> 00:02:49,170
then passing them as the appropriate levels.

45
00:02:49,170 --> 00:02:53,058
Why don't we just scan the data and find what's there,

46
00:02:53,058 --> 00:02:57,455
ignore all the cases where the count is zero and do it like that.

47
00:02:57,455 --> 00:03:01,295
This is a case where if our data was a data frame,

48
00:03:01,295 --> 00:03:03,830
we could certainly do that.

49
00:03:03,830 --> 00:03:07,860
We could basically just scan the data, find all of the levels

50
00:03:07,860 --> 00:03:12,270
that are in the data, and use those as the levels for the data.

51
00:03:12,270 --> 00:03:16,120
However, in RevoScaleR we would be

52
00:03:17,920 --> 00:03:21,290
probably doing the wrong transformation by doing that.

53
00:03:21,290 --> 00:03:24,240
Probably because sometimes, this is gonna work, but

54
00:03:24,240 --> 00:03:28,060
sometimes it's not and it's not the right way of doing it.

55
00:03:28,060 --> 00:03:32,580
The reason is we have to remember that the data in RevoScaleR.

56
00:03:32,580 --> 00:03:36,738
In this case, the XDF data is going to be evicted out.

57
00:03:36,738 --> 00:03:43,090
We're not gonna load the data in its entirety to the current R session.

58
00:03:43,090 --> 00:03:46,070
We're only doing it one trunk at a time.

59
00:03:46,070 --> 00:03:50,810
And so if we want to scan the data to find what all the levels are,

60
00:03:50,810 --> 00:03:54,630
we need to do it the way that we're doing it here.

61
00:03:54,630 --> 00:03:57,170
So we need to do it ahead of time so that we know for

62
00:03:57,170 --> 00:03:59,540
the whole data what levels are observed.

63
00:04:00,620 --> 00:04:05,130
Otherwise if we do it inside of the transformation function we're going

64
00:04:05,130 --> 00:04:09,220
to be doing it for each chunk of the data that we bring in individually.

65
00:04:09,220 --> 00:04:11,960
And this means that occasionally,

66
00:04:11,960 --> 00:04:15,590
we could bring in a chunk where not all the levels are gonna be present.

67
00:04:15,590 --> 00:04:19,470
And so we end up with the wrong transformation because of that.

68
00:04:19,470 --> 00:04:23,340
So this is one of the cases what we have to be careful about our

69
00:04:23,340 --> 00:04:28,010
transformation in the sense that we need to compute some global

70
00:04:29,240 --> 00:04:34,350
objects ahead of time and then pass those objects to the transformation.

71
00:04:34,350 --> 00:04:40,250
In this case, down to what the factual levels need to be.

72
00:04:42,430 --> 00:04:45,040
So we can run the transmission.

73
00:04:45,040 --> 00:04:48,750
I'm gonna create,

74
00:04:48,750 --> 00:04:54,330
I'm gonna load the function first and in this case I'm going

75
00:04:54,330 --> 00:04:58,010
to basically run the transformation directly on the XDF file.

76
00:04:59,220 --> 00:05:03,110
This is a pretty simple transmission, so we can

77
00:05:03,110 --> 00:05:06,430
skip the step of actually testing the transmission on a data frame.

78
00:05:06,430 --> 00:05:09,506
If you're new to using RevoScaleR and

79
00:05:09,506 --> 00:05:14,670
you're still not quite sure about the transformation.

80
00:05:14,670 --> 00:05:16,790
It's probably a good idea to test it anyway,

81
00:05:16,790 --> 00:05:21,340
but in my case I'm just gonna run the transformation on the XDF file.

82
00:05:21,340 --> 00:05:24,700
So, notice as before we have the transformation function

83
00:05:24,700 --> 00:05:26,520
as specified here.

84
00:05:26,520 --> 00:05:29,440
Past to the transform Func argument.

85
00:05:29,440 --> 00:05:34,840
And because the Manhattan levels are another input to this function,

86
00:05:34,840 --> 00:05:37,110
we're gonna specify them the following way.

87
00:05:37,110 --> 00:05:40,420
We're gonna pass them to the transform objects argument, and

88
00:05:40,420 --> 00:05:43,510
the name that we have here on the left side is just a name that

89
00:05:43,510 --> 00:05:48,080
the function expects inside of the body of the function.

90
00:05:48,080 --> 00:05:51,966
The name on the right side is the name of the object as it is sitting

91
00:05:51,966 --> 00:05:56,440
in the global environment.

92
00:05:56,440 --> 00:05:58,310
The transformation finished running.

93
00:05:58,310 --> 00:06:01,710
And on the face of it,

94
00:06:01,710 --> 00:06:06,045
at least when looking at the data, we should not see any differences.

95
00:06:06,045 --> 00:06:08,816
The differences happened, if you will,

96
00:06:08,816 --> 00:06:12,785
to the meta data to the factor levels not to the data itself.

97
00:06:14,355 --> 00:06:15,685
We can double check that.

98
00:06:15,685 --> 00:06:19,525
We can run our RX summary a second time.

99
00:06:19,525 --> 00:06:26,160
And when we look at the data what we see is, we now

100
00:06:26,160 --> 00:06:30,990
see summaries for neighborhoods that are only inside of Manhattan,

101
00:06:30,990 --> 00:06:33,460
we don't see counts of zero anymore.

102
00:06:33,460 --> 00:06:36,880
So, all of those other neighborhoods have disappeared and

103
00:06:36,880 --> 00:06:41,560
our data is limited only to the Manhattan neighborhoods.

