0
00:00:01,540 --> 00:00:03,050
Let's start with a visualization

1
00:00:04,090 --> 00:00:08,296
that shows the distribution of the total percentages.

2
00:00:08,296 --> 00:00:12,750
So we have the pick up neighborhood on the x axis,

3
00:00:12,750 --> 00:00:15,820
drop off neighborhood on the y axis and

4
00:00:15,820 --> 00:00:20,980
the intensity of the plot shows the intensity of the color in the time

5
00:00:20,980 --> 00:00:26,980
plot shows the percentage out of the total number of trips.

6
00:00:26,980 --> 00:00:32,367
And we can really see in this plot that the majority of the trips

7
00:00:32,367 --> 00:00:37,030
are dominated by neighborhoods in the Midtown area,

8
00:00:37,030 --> 00:00:41,810
followed by some neighborhoods in the Downtown area.

9
00:00:41,810 --> 00:00:46,350
But the Midtown area is definitely the big one here and

10
00:00:46,350 --> 00:00:49,550
that within the neighborhoods in the Midtown area,

11
00:00:49,550 --> 00:00:55,110
the great majority seem to be taken up by Midtown.

12
00:00:55,110 --> 00:00:59,520
And I guess in this case I'm kind of including the Upper East Side and

13
00:00:59,520 --> 00:01:01,930
West Side as Midtown neighborhoods,

14
00:01:01,930 --> 00:01:04,750
even though they're more the upper neighborhoods.

15
00:01:04,750 --> 00:01:08,230
But let's just, for now, for the sake of simplicity,

16
00:01:08,230 --> 00:01:12,230
let's include those as the Midtown neighborhoods.

17
00:01:12,230 --> 00:01:16,050
And so the three dominant ones are Midtown itself,

18
00:01:16,050 --> 00:01:18,970
the Upper East Side and the Upper West Side.

19
00:01:18,970 --> 00:01:23,604
The other thing that stands out is the fact that the trips

20
00:01:23,604 --> 00:01:28,830
are pretty symmetric and that the most common ones, as we saw in

21
00:01:28,830 --> 00:01:34,173
the table before are happening within the same neighborhoods.

22
00:01:34,173 --> 00:01:37,482
So especially for Midtown, Upper East Side and

23
00:01:37,482 --> 00:01:42,113
Upper West Side, the majority of the time, people are not leaving

24
00:01:42,113 --> 00:01:45,504
the neighborhood that they were picked up from,

25
00:01:45,504 --> 00:01:50,530
they are getting dropped off from the same neighborhood.

26
00:01:50,530 --> 00:01:55,053
We can also see that there's quite a bit of traffic between these three

27
00:01:55,053 --> 00:01:56,580
neighborhoods.

28
00:01:56,580 --> 00:02:00,700
So, it's an interesting plot in the sense that some of the results

29
00:02:00,700 --> 00:02:04,790
are certainly intuitive, some of the results are more surprising,

30
00:02:04,790 --> 00:02:09,850
especially the fact that there is a lot of locality going on.

31
00:02:09,850 --> 00:02:14,160
Okay, so let's close this, and let's go and

32
00:02:14,160 --> 00:02:21,070
now look at the relationship between the neighborhoods,

33
00:02:21,070 --> 00:02:27,520
as far as the percentages by pick-up neighborhood.

34
00:02:27,520 --> 00:02:30,570
So, here's another plot and this time,

35
00:02:30,570 --> 00:02:35,560
we're not looking at the total proportion which was the last plot.

36
00:02:35,560 --> 00:02:38,699
Now we're looking at the proportion by pick-up neighborhood.

37
00:02:38,699 --> 00:02:43,910
Since pickup_nb is represented by the x-axis, what we're saying here

38
00:02:43,910 --> 00:02:49,800
is that each column in this tiled plot, each vertical column in this

39
00:02:49,800 --> 00:02:55,350
tile plot is going to have proportions that add up to 100%.

40
00:02:55,350 --> 00:03:01,370
So some of the columns in the tile plot are gonna have very few

41
00:03:01,370 --> 00:03:06,430
shades of blue and those are the ones where we have one or

42
00:03:06,430 --> 00:03:08,790
two tiles that are really dominating.

43
00:03:08,790 --> 00:03:11,230
And then a few that are coming afterwards and

44
00:03:11,230 --> 00:03:13,560
the rest of them are pretty bright.

45
00:03:13,560 --> 00:03:19,530
And some other columns, especially in this case the Downtown columns,

46
00:03:19,530 --> 00:03:22,600
don't have any particular one that stand out, but

47
00:03:22,600 --> 00:03:26,424
they have quite a few better joining the distribution, okay.

48
00:03:29,290 --> 00:03:32,690
So starting with say the Upper East Side,

49
00:03:32,690 --> 00:03:37,670
we can see that for trips that leave the Upper East Side,

50
00:03:38,840 --> 00:03:44,240
the majority of those trips end up back in the Upper East Side.

51
00:03:44,240 --> 00:03:48,040
And then after that, Midtown is the next destination, and

52
00:03:48,040 --> 00:03:51,680
after that the Upper West Side is the next destination.

53
00:03:51,680 --> 00:03:54,880
And the other destinations are just not that common.

54
00:03:54,880 --> 00:04:00,220
So the Upper East Side as an origin has three very

55
00:04:00,220 --> 00:04:03,570
common destinations and the rest of them are just not that common.

56
00:04:05,020 --> 00:04:09,840
If we compare that to say the Soho neighborhood

57
00:04:09,840 --> 00:04:13,390
which is a downtown neighborhood, we can see that starting in Soho,

58
00:04:13,390 --> 00:04:18,150
people tend to spread out to midtown pretty often.

59
00:04:18,150 --> 00:04:21,430
To Chelsea, to Greenwich village but

60
00:04:21,430 --> 00:04:25,260
the other downtown neighborhoods are also quite common as well.

61
00:04:26,680 --> 00:04:30,900
So, the Upper East Side, Upper West Side and

62
00:04:30,900 --> 00:04:33,350
Midtown neighborhoods are more local.

63
00:04:33,350 --> 00:04:36,870
The downtown neighborhoods tend to have the wider spread.

64
00:04:36,870 --> 00:04:38,510
When we get picked up from one of them,

65
00:04:38,510 --> 00:04:42,070
we tend to kind of spread out to other neighborhoods in the Downtown

66
00:04:42,070 --> 00:04:46,890
area or go to Midtown and the Upper East Side commonly as well.

67
00:04:46,890 --> 00:04:49,300
I'm gonna close this plot and

68
00:04:49,300 --> 00:04:52,980
we're gonna look at the last plot in this series.

69
00:04:52,980 --> 00:04:57,050
And in this last plot, we're not looking at proportion of

70
00:04:57,050 --> 00:05:00,780
trips leaving a particular neighborhoods.

71
00:05:00,780 --> 00:05:04,972
We're looking at proportion of trips arriving in a particular

72
00:05:04,972 --> 00:05:08,935
neighborhood and asking where are they originating from.

73
00:05:08,935 --> 00:05:13,655
So drop off neighborhood is now where the proportions are adding up

74
00:05:13,655 --> 00:05:18,539
to 100% and instead of thinking in this plot in terms of columns,

75
00:05:18,539 --> 00:05:20,746
we're gonna look at the rows.

76
00:05:20,746 --> 00:05:27,709
And we can see that it's almost a reflection of the last spot.

77
00:05:27,709 --> 00:05:32,265
And certain neighborhoods such as the downtown neighborhoods,

78
00:05:32,265 --> 00:05:36,570
if we look at someone arriving at the downtown neighborhood,

79
00:05:36,570 --> 00:05:40,543
they are most likely to have originated from either one of

80
00:05:40,543 --> 00:05:43,879
the other downtown neighborhoods or midtown.

81
00:05:43,879 --> 00:05:47,090
And the next one seems to be the Upper East Side.

82
00:05:49,490 --> 00:05:53,460
Whereas, someone who was just dropped off in say,

83
00:05:53,460 --> 00:05:58,628
the Upper West Side is most likely going to have come from Midtown,

84
00:05:58,628 --> 00:06:00,473
the Upper East Side, or

85
00:06:00,473 --> 00:06:04,193
even more common is the Upper West Side itself.

86
00:06:04,193 --> 00:06:08,571
All right, so the three plots are helping us

87
00:06:08,571 --> 00:06:13,540
make sense of the distribution of the trips as they

88
00:06:13,540 --> 00:06:18,035
first of all pertain to the whole of the data or

89
00:06:18,035 --> 00:06:24,543
the distribution of the trips as they leave a particular origin or

90
00:06:24,543 --> 00:06:30,140
as they arrive into a particular destination.

91
00:06:30,140 --> 00:06:35,560
And it's just one of the ways that we can make sense of the data and

92
00:06:35,560 --> 00:06:39,250
we can sift through the preponderance of results and

93
00:06:39,250 --> 00:06:44,930
numbers that we have without having to deal with the numbers directly.

94
00:06:44,930 --> 00:06:49,560
This is what makes visualizations such a popular tool for us.

95
00:06:49,560 --> 00:06:54,169
Visualizations allow us to develop an intuition about the data,

96
00:06:54,169 --> 00:06:57,186
develop an intuition about the results and

97
00:06:57,186 --> 00:07:01,710
how the columns are related to each other without having to spend

98
00:07:01,710 --> 00:07:06,516
too much time looking at numbers and sifting through information.

