0
00:00:01,470 --> 00:00:04,170
Let's look at some other relationships in the data.

1
00:00:04,170 --> 00:00:07,770
Let's look at the relationship between the neighborhoods and

2
00:00:07,770 --> 00:00:11,660
the column called minutes per mile that we

3
00:00:11,660 --> 00:00:13,720
created in our summary earlier.

4
00:00:16,110 --> 00:00:21,690
So we can create a similar plot to the one that we had before, and

5
00:00:21,690 --> 00:00:25,470
from now on the neighborhoods are gonna be ordered in the topological

6
00:00:25,470 --> 00:00:30,270
way that we did it in using the seriation function.

7
00:00:30,270 --> 00:00:34,950
And we can see here that minutes per mile

8
00:00:34,950 --> 00:00:39,240
is now showing up as the color coding for the tiles, and

9
00:00:39,240 --> 00:00:43,990
because minutes per mile can be thought of as a good proxy for

10
00:00:43,990 --> 00:00:46,760
the amount of traffic that we have to sit through, right?

11
00:00:46,760 --> 00:00:50,070
So for the cases where we don't travel very far but

12
00:00:50,070 --> 00:00:51,380
we're sitting in a taxi for

13
00:00:51,380 --> 00:00:55,830
quite some time, we're probably sitting through a lot of traffic.

14
00:00:56,910 --> 00:01:01,960
And so this plot is a representation of basically which

15
00:01:01,960 --> 00:01:05,430
neighborhoods we should expect to

16
00:01:05,430 --> 00:01:08,540
get stuck in a lot of traffic if we travel between them.

17
00:01:09,710 --> 00:01:14,560
It's interesting to see the one that stands out the most,

18
00:01:14,560 --> 00:01:18,570
is between two neighborhoods that are the same.

19
00:01:18,570 --> 00:01:21,380
So we're going between the Garment District, and

20
00:01:21,380 --> 00:01:23,790
we're going back to the Garment District.

21
00:01:23,790 --> 00:01:29,350
And it seems like there is quite a bit of traffic traveling between

22
00:01:30,710 --> 00:01:33,560
these two neighborhoods that are really just the same neighborhoods.

23
00:01:33,560 --> 00:01:36,635
So travelling in the Garment District, in other words,

24
00:01:36,635 --> 00:01:40,170
using a taxi, we should expect to run into quite a bit of traffic.

25
00:01:40,170 --> 00:01:45,154
And looking at this plot in general, we can see that there are, in fact,

26
00:01:45,154 --> 00:01:48,098
some neighborhoods that are like that, so

27
00:01:48,098 --> 00:01:52,840
looking in the diagonal direction on this plot, we can see that there

28
00:01:52,840 --> 00:01:57,335
are quite a few points that stand out where we see more traffic if we

29
00:01:57,335 --> 00:02:02,180
start in one neighborhood and end up in the same neighborhood.

30
00:02:02,180 --> 00:02:05,110
Then if starting from that neighborhood we went to some

31
00:02:05,110 --> 00:02:06,230
other neighborhood.

32
00:02:06,230 --> 00:02:10,240
So this is an interesting feature of the data, and it speaks to some

33
00:02:10,240 --> 00:02:15,535
extent about the fact that maybe when we

34
00:02:15,535 --> 00:02:19,395
travel within the same neighborhood we don't really, the taxi driver

35
00:02:19,395 --> 00:02:23,205
doesn't really have much of a choice, can't really think about

36
00:02:23,205 --> 00:02:27,311
the alternative routes that is going to make it so we avoid less traffic.

37
00:02:27,311 --> 00:02:31,345
We kinda have to just go with the flow, and there is going to be

38
00:02:33,140 --> 00:02:37,220
quite a bit of traffic to content with in New York City.

39
00:02:39,340 --> 00:02:42,618
Another one we're gonna look at is the relationship between

40
00:02:42,618 --> 00:02:44,618
the neighborhoods and tip percent.

41
00:02:44,618 --> 00:02:48,713
And because tip percent is a numeric colon,

42
00:02:48,713 --> 00:02:53,483
it might be a little bit overkill to keep it as such.

43
00:02:53,483 --> 00:02:56,610
So instead we're gonna use the cut function in R.

44
00:02:56,610 --> 00:03:00,100
And we're gonna think of tip percent as such.

45
00:03:00,100 --> 00:03:03,562
We're gonna think of everyone who tipped between 0 and

46
00:03:03,562 --> 00:03:05,640
5% as one category.

47
00:03:05,640 --> 00:03:09,380
Everyone who tipped between 5 and 8% is another.

48
00:03:09,380 --> 00:03:10,339
Then 8 to 10.

49
00:03:10,339 --> 00:03:11,979
Then 10 to 12.

50
00:03:11,979 --> 00:03:16,003
Then anyone tipping greater than 12%.

51
00:03:16,003 --> 00:03:19,300
Of course, we can change these numbers around, so that's

52
00:03:19,300 --> 00:03:23,227
one thing that would be interesting for us to experiment with, but

53
00:03:23,227 --> 00:03:27,280
that's taken as they are right now, and see what we end up with.

54
00:03:27,280 --> 00:03:32,450
So we can see the color coding now, showing in a different way.

55
00:03:32,450 --> 00:03:37,710
In the previous tile plots, because we were plotting

56
00:03:37,710 --> 00:03:44,510
a numeric column, the numeric column would be using a different shading

57
00:03:44,510 --> 00:03:49,500
to show the intensity of the number, the scale of the number.

58
00:03:50,820 --> 00:03:54,240
But now, we're using tip_color, and

59
00:03:54,240 --> 00:03:58,880
that is a factor cut by default, is going to return a factor.

60
00:03:58,880 --> 00:04:03,670
And so that factor is gonna be color coded using colors that

61
00:04:03,670 --> 00:04:05,880
are clearly delineated from each other.

62
00:04:07,110 --> 00:04:11,960
We can see in this plot that the great majority of

63
00:04:11,960 --> 00:04:16,740
high tippers are going to be traveling

64
00:04:16,740 --> 00:04:21,060
between the downtown neighborhoods and the midtown neighborhoods.

65
00:04:21,060 --> 00:04:25,700
We can see that we probably have quite a few examples of high

66
00:04:25,700 --> 00:04:30,440
tippers, maybe 12% is too lower the threshold, so

67
00:04:30,440 --> 00:04:32,970
we could go back and we could change things around.

68
00:04:32,970 --> 00:04:38,353
We could maybe look at 8 to 12% and 12 to 18%,

69
00:04:38,353 --> 00:04:43,990
and then anyone higher to see how the plot be different.

70
00:04:45,280 --> 00:04:48,490
So in this case, we don't see big differences

71
00:04:48,490 --> 00:04:53,750
between the plots, let's try one other example.

72
00:04:53,750 --> 00:04:57,310
Let's do 12 to 15%.

73
00:04:58,970 --> 00:04:59,930
Yeah, that's fine.

74
00:04:59,930 --> 00:05:02,540
Let's just look at the plot now.

75
00:05:02,540 --> 00:05:07,930
So now we see an interesting delineation.

76
00:05:07,930 --> 00:05:11,950
Now we see that the downtown neighborhoods are standing out,

77
00:05:11,950 --> 00:05:13,879
even compared to the midtown neighborhoods.

78
00:05:15,200 --> 00:05:21,270
So, the best tippers seem to be travelling from and

79
00:05:21,270 --> 00:05:23,260
to the downtown neighborhoods.

80
00:05:23,260 --> 00:05:27,780
So all the ones between, say Chinatown and Chelsea.

81
00:05:27,780 --> 00:05:33,565
And then after that, people travelling from downtown to midtown

82
00:05:33,565 --> 00:05:40,990
orr from midtown to downtown, are also likely to tip relatively high.

83
00:05:40,990 --> 00:05:44,270
And after that it seems like it's between people traveling from

84
00:05:44,270 --> 00:05:50,065
the midtown neighborhoods to the midtown neighborhoods, okay?

85
00:05:50,065 --> 00:05:54,355
So, interesting things stand out when we look at these plots.

86
00:05:54,355 --> 00:05:58,053
But we can see that we have to occasionally make some subjective

87
00:05:58,053 --> 00:06:01,564
decisions about where the cutoffs are gonna be, and so on.

88
00:06:01,564 --> 00:06:07,416
But it's certainly easier to look at such visualizations and

89
00:06:07,416 --> 00:06:09,328
let TRAN stand out,

90
00:06:09,328 --> 00:06:14,504
then to try to make sense of the numbers individually,

91
00:06:14,504 --> 00:06:19,250
or to even bother too much with the numbers per se.

