0
00:00:01,420 --> 00:00:03,920
So, we summarize the data.

1
00:00:03,920 --> 00:00:08,150
We use the summaries to create some visualizations.

2
00:00:08,150 --> 00:00:12,700
We notice that the visualization, the tile plot in this case,

3
00:00:12,700 --> 00:00:14,389
that we created was a little bit off

4
00:00:15,410 --> 00:00:18,790
because the neighborhoods were ordered in alphabetical order.

5
00:00:18,790 --> 00:00:23,780
And so on the results that our summary function returned,

6
00:00:23,780 --> 00:00:26,720
we went back and reordered the neighborhoods in

7
00:00:26,720 --> 00:00:29,670
a particular way that we wanted to see.

8
00:00:29,670 --> 00:00:33,799
And we used the help of the seriation package to do that.

9
00:00:33,799 --> 00:00:40,540
So this is a kind of this was an involved process.

10
00:00:40,540 --> 00:00:44,510
And once we reordered the neighborhoods, we started looking at

11
00:00:44,510 --> 00:00:49,550
more visualizations, and things kind of fell in place nicely.

12
00:00:49,550 --> 00:00:55,130
We had certainly some interesting results that jumped out to us.

13
00:00:57,110 --> 00:01:02,780
But now, we might be wanting to take a step back and go back to our data.

14
00:01:04,360 --> 00:01:09,020
So, when we changed the order of the neighborhoods and the factor column,

15
00:01:09,020 --> 00:01:13,350
we change that on the results that Rx summary was returning.

16
00:01:14,400 --> 00:01:18,580
So in the original data, the column that represents pick up neighborhood

17
00:01:18,580 --> 00:01:22,320
and drop off neighborhood are still ordered in alphabetical order.

18
00:01:22,320 --> 00:01:24,800
The reason they are ordered in that way

19
00:01:24,800 --> 00:01:27,560
is because that's just how they came out.

20
00:01:27,560 --> 00:01:34,250
So it could be that, when we were using the GIS packages

21
00:01:34,250 --> 00:01:38,810
to get the neighborhood information, that they were returning

22
00:01:38,810 --> 00:01:42,700
the neighborhoods, not just as a factor column, but as a factor

23
00:01:42,700 --> 00:01:46,780
column where the neighborhoods are alphabetically ordered, right?

24
00:01:46,780 --> 00:01:50,070
And, of course, that sort of makes sense, because

25
00:01:51,980 --> 00:01:56,780
one, factors by default if we don't specify the ordering of

26
00:01:56,780 --> 00:02:00,160
the factor levels in R are gonna be alphabetically ordered.

27
00:02:00,160 --> 00:02:03,920
And two, because the way that we want the neighborhoods to be ordered

28
00:02:03,920 --> 00:02:07,720
is really subjective, it depends on the analysis that we're doing.

29
00:02:07,720 --> 00:02:12,650
Here, we want the neighborhoods to be ordered such that

30
00:02:12,650 --> 00:02:16,870
the neighborhoods in Manhattan that are close to each other are gonna

31
00:02:16,870 --> 00:02:21,065
show up next to each other in the order of the factor levels.

32
00:02:21,065 --> 00:02:23,805
But of course, the analysis could have been

33
00:02:23,805 --> 00:02:29,025
some other analysis not necessarily pertaining to Manhattan only.

34
00:02:29,025 --> 00:02:33,685
And so in that case, we might have chosen a different ordering.

35
00:02:33,685 --> 00:02:38,810
So depending on the analysis, there is some amount of work that is

36
00:02:38,810 --> 00:02:43,750
incumbent on us to do in order to make sure that the data is really

37
00:02:43,750 --> 00:02:50,130
ready and shaped and curated for

38
00:02:50,130 --> 00:02:55,300
the analysis that we're gonna be focusing on.

39
00:02:55,300 --> 00:02:59,575
So, we're gonna take a step back and we're gonna now go back to the data.

40
00:02:59,575 --> 00:03:02,945
And, we're going to clean up the neighborhood columns.

41
00:03:02,945 --> 00:03:07,048
Cleaning up in this case being that we're gonna reorder the factor

42
00:03:07,048 --> 00:03:08,659
levels in those columns so

43
00:03:08,659 --> 00:03:12,852
that the new ordering that we found can now be reflected in the data.

44
00:03:12,852 --> 00:03:17,149
And this makes sense for us because form now on when we think of those

45
00:03:17,149 --> 00:03:21,218
neighborhoods, we don't wanna have to, every time it create

46
00:03:21,218 --> 00:03:25,671
a visualization, we don't wanna have to go to the visualization and

47
00:03:25,671 --> 00:03:27,630
reorder the neighborhoods.

48
00:03:27,630 --> 00:03:32,540
From now on when we run some kind of summary function on the data,

49
00:03:32,540 --> 00:03:34,920
if it involves the pick up neighborhood or

50
00:03:34,920 --> 00:03:39,660
drop off neighborhoods, we want the summaries to show up

51
00:03:39,660 --> 00:03:43,960
in the new ordering that we just found with seriate.

52
00:03:43,960 --> 00:03:47,970
So we're here, and we're back to our data and

53
00:03:47,970 --> 00:03:52,910
we're gonna run an rxDataStep transformation so that we can

54
00:03:52,910 --> 00:03:56,990
reorder the neighborhoods in the new way that we found.

55
00:03:56,990 --> 00:04:01,430
So, rxDataStep is going to read from Manhattan XDF and

56
00:04:01,430 --> 00:04:03,560
it's gonna write back to it.

57
00:04:03,560 --> 00:04:06,038
And in this case, we're not gonna create a new column,

58
00:04:06,038 --> 00:04:08,574
we're just gonna overwrite the pickup neighborhood and

59
00:04:08,574 --> 00:04:10,022
drop off neighborhood column.

60
00:04:10,022 --> 00:04:14,959
And we're gonna overwrite it with the same columns, but the levels

61
00:04:14,959 --> 00:04:19,895
are going to be levels that are specified and in this case, they're

62
00:04:19,895 --> 00:04:24,960
gonna be the same as the old levels, just not the same order, right.

63
00:04:24,960 --> 00:04:31,520
So this is the transformation, and I'm gonna run this.

64
00:04:31,520 --> 00:04:33,549
It should be relatively fast,

65
00:04:33,549 --> 00:04:37,920
we're not really changing anything in the data we're changing,

66
00:04:37,920 --> 00:04:41,470
if you will, something at the level of the metadata.

67
00:04:41,470 --> 00:04:47,188
If we think of the factor level is being metadata, we're just changing

68
00:04:47,188 --> 00:04:52,256
the order of the factor levels, not any of the rows of the data.

69
00:04:52,256 --> 00:04:53,584
The pick up neighborhood and

70
00:04:53,584 --> 00:04:56,690
drop off neighborhood are still going to be what they were before.

71
00:04:58,970 --> 00:05:04,770
Now, we have another example of doing exactly what we just did.

72
00:05:04,770 --> 00:05:09,770
So, the transformation finished and it's done, but I have here

73
00:05:09,770 --> 00:05:15,100
a secondary way of doing exactly what we just did using rxDataStep.

74
00:05:15,100 --> 00:05:19,639
And I wanna draw some contrast between doing it the second way and

75
00:05:19,639 --> 00:05:22,020
doing it the first way.

76
00:05:22,020 --> 00:05:25,165
In the first way, we can think of

77
00:05:25,165 --> 00:05:30,070
rxDataStep as simply the framework that

78
00:05:30,070 --> 00:05:34,580
allows us to deal with XDF files and run these complex transformations.

79
00:05:34,580 --> 00:05:39,191
And, the transformation itself as just a transformation that

80
00:05:39,191 --> 00:05:43,454
relies on the base R functions that we're familiar with,

81
00:05:43,454 --> 00:05:47,204
in this case factor, in order to accomplish that.

82
00:05:47,204 --> 00:05:52,343
The second way of doing this is using a function called rxFactors,

83
00:05:52,343 --> 00:05:53,260
and it's,

84
00:05:53,260 --> 00:05:59,260
we will scale our function that is meant to treat factor columns.

85
00:05:59,260 --> 00:06:02,650
And so in this case with rxFactors, we're telling it go,

86
00:06:02,650 --> 00:06:04,740
read from Manhattan XDF file,

87
00:06:04,740 --> 00:06:08,450
make some changes that you write back to the Manhattan XDF file.

88
00:06:08,450 --> 00:06:13,140
And the change is going to be encoded in this

89
00:06:13,140 --> 00:06:14,540
argument called factorInfo.

90
00:06:15,570 --> 00:06:18,288
So, the first change is you're gonna go to that

91
00:06:18,288 --> 00:06:20,260
pickup neighborhood column.

92
00:06:20,260 --> 00:06:24,390
And, you're going to assign newLevels to it and

93
00:06:24,390 --> 00:06:28,336
the new levels are just whatever I give you.

94
00:06:28,336 --> 00:06:31,388
And then, we're gonna do the same with the drop off neighborhood

95
00:06:31,388 --> 00:06:32,231
column, right?

96
00:06:32,231 --> 00:06:37,527
So, we basically have to go through the documentation frxFactors,

97
00:06:37,527 --> 00:06:41,905
in this case, so that we know that we need to specify a list

98
00:06:41,905 --> 00:06:46,590
where we have one column as each element of the list.

99
00:06:46,590 --> 00:06:50,470
And then, we have things like newLevels that allow us

100
00:06:50,470 --> 00:06:53,990
to in this case reassigned new levels to the factor column.

101
00:06:55,710 --> 00:06:58,810
So, this is an alternative way to doing it

102
00:06:58,810 --> 00:07:00,417
compare to the rxDataStep way.

103
00:07:01,440 --> 00:07:04,635
It does the exact same thing and I'm gonna run it.

104
00:07:04,635 --> 00:07:09,621
We can see that it happens quite a bit faster than rxDataStep does,

105
00:07:09,621 --> 00:07:10,260
right?

106
00:07:10,260 --> 00:07:14,140
So, the advantage of using rxFactors is that, it's going to be faster in

107
00:07:14,140 --> 00:07:18,480
this case because we're not changing anything in the data, right?

108
00:07:18,480 --> 00:07:21,530
Because we're changing just the metadata.

109
00:07:21,530 --> 00:07:26,404
rxFactors is a slightly mother function than rxDataStep is.

110
00:07:26,404 --> 00:07:31,135
And, it's gonna just go and change the metadata and quickly return.

111
00:07:31,135 --> 00:07:34,210
rxDataStep doesn't really know

112
00:07:34,210 --> 00:07:37,015
that the change is really just happening at the metadata level.

113
00:07:37,015 --> 00:07:39,940
Let's gonna think of this as a transformation like any other

114
00:07:39,940 --> 00:07:41,390
transformation.

115
00:07:41,390 --> 00:07:45,010
And, the only thing that makes this a transformation at the metadata

116
00:07:45,010 --> 00:07:49,830
level is the fact, that we are aware of the fact that we're

117
00:07:49,830 --> 00:07:53,990
just changing the factor levels and putting them in a new order.

118
00:07:55,580 --> 00:08:00,730
So, it's not as smart as the rxFactor function in this case and

119
00:08:00,730 --> 00:08:02,780
it's gonna take longer to run.

120
00:08:02,780 --> 00:08:07,170
There is, however, one advantage to using rxDataStep,

121
00:08:07,170 --> 00:08:09,970
that is not true in the case of rxFactors.

122
00:08:12,020 --> 00:08:17,000
rxFactors as we will see in the last week of the course,

123
00:08:17,000 --> 00:08:20,450
only works in a local compute context.

124
00:08:20,450 --> 00:08:23,320
That means that it only works on a single machine.

125
00:08:23,320 --> 00:08:28,580
If the data, Manhattan XDF here was pointing to data that was on

126
00:08:28,580 --> 00:08:33,592
a Hadoop cluster on HDFS or pointing to data that was on a SQL Server,

127
00:08:33,592 --> 00:08:37,895
rxFactor would fail to run, in this case.

128
00:08:37,895 --> 00:08:43,103
Whereas, rxDataStep is going to run in any compute context, right?

129
00:08:43,103 --> 00:08:48,930
If few functions in scalar that are like rxFactors,

130
00:08:48,930 --> 00:08:54,650
in the sense that they only work in a local

131
00:08:54,650 --> 00:08:57,800
compute context but rxFactors is one of them.

132
00:08:57,800 --> 00:09:02,418
Because of that, I try to avoid using it, assuming that in

133
00:09:02,418 --> 00:09:07,415
the future I might be interested to deploy my code in a different

134
00:09:07,415 --> 00:09:12,528
compute context than the local compute context its running in.

