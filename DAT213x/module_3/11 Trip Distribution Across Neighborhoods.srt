0
00:00:02,150 --> 00:00:05,200
We saw in the last section an example of

1
00:00:06,990 --> 00:00:10,720
doing an analysis and letting the analysis guide

2
00:00:10,720 --> 00:00:14,210
a previous step that is the data cleaning step.

3
00:00:16,510 --> 00:00:20,400
This is an important thing for us to do as data scientist, we always need

4
00:00:20,400 --> 00:00:25,050
to be at the same time forward looking and backward looking.

5
00:00:25,050 --> 00:00:29,000
Forward looking in the sense that we have to have an intuition for

6
00:00:29,000 --> 00:00:33,465
what sort of analysis we're gonna do so that we have some kinda direction

7
00:00:33,465 --> 00:00:36,551
and some kind of guidance for where we're going.

8
00:00:36,551 --> 00:00:41,953
But backward looking in the sense that the current steps that we're

9
00:00:41,953 --> 00:00:47,258
taking in the analysis so far, has to somewhat guide earlier steps

10
00:00:47,258 --> 00:00:52,182
that we took to clean the data and force us to take a step back and

11
00:00:52,182 --> 00:00:57,706
wonder what else we could have done to make this process more smooth.

12
00:00:57,706 --> 00:01:02,556
And to make the data as ready as possible to save us from doing

13
00:01:02,556 --> 00:01:06,850
more cleaning work a little bit down the line.

14
00:01:06,850 --> 00:01:09,530
As much as possible it's really nice when you get to

15
00:01:09,530 --> 00:01:12,800
the analysis to not have to worry about the ins and outs of the data,

16
00:01:12,800 --> 00:01:17,410
to not have to worry about doing things like reordering factor levels

17
00:01:17,410 --> 00:01:20,640
to have everything be kind of just stay plug and play.

18
00:01:20,640 --> 00:01:24,290
So that we can focus on visualizations, on summaries,

19
00:01:24,290 --> 00:01:25,460
on tables.

20
00:01:25,460 --> 00:01:29,700
And as we'll see a little bit later on building models and

21
00:01:29,700 --> 00:01:30,950
evaluating models and so on.

22
00:01:33,070 --> 00:01:38,280
So let's see how this is gonna work in practice with one example

23
00:01:38,280 --> 00:01:39,720
regarding the neighborhoods.

24
00:01:40,880 --> 00:01:46,120
So we're back here in R and we're gonna run an RX cubed function to

25
00:01:46,120 --> 00:01:50,350
get counts for pick up neighborhood by drop off neighborhood.

26
00:01:52,740 --> 00:01:57,450
The result is stored in this object called RXC.

27
00:01:57,450 --> 00:02:01,020
And so, here, we can see a couple of things.

28
00:02:01,020 --> 00:02:04,860
One is that notice that pick up neighborhood is now

29
00:02:04,860 --> 00:02:09,540
starting with Chinatown as it's drop off neighborhood.

30
00:02:09,540 --> 00:02:15,680
So we're gonna see Chinatown as the first drop off neighborhood and

31
00:02:15,680 --> 00:02:18,400
it's gonna be combined with each pick up neighborhood.

32
00:02:18,400 --> 00:02:19,660
And then, we're gonna move on to the next.

33
00:02:19,660 --> 00:02:24,257
So the change that we made to the data in the last step has made it so

34
00:02:24,257 --> 00:02:27,580
that RX cube is now ordering the neighborhoods

35
00:02:27,580 --> 00:02:31,520
starting with the first level which now is china town.

36
00:02:32,590 --> 00:02:34,880
It's not alphabetically ordered anymore.

37
00:02:36,500 --> 00:02:38,330
Then we can see a column for the counts.

38
00:02:39,700 --> 00:02:42,190
We're gonna take these counts and we're gonna use

39
00:02:43,220 --> 00:02:48,020
the dplyr function to get some proportions out of the counts.

40
00:02:49,080 --> 00:02:50,140
So this is what we're gonna do.

41
00:02:51,230 --> 00:02:54,434
We're going to take the counts and

42
00:02:54,434 --> 00:02:57,977
first get a proportion for the total.

43
00:02:57,977 --> 00:02:59,818
That means, for example, so

44
00:02:59,818 --> 00:03:03,733
the proportion is shown in the column here called percent all.

45
00:03:03,733 --> 00:03:09,343
And this means that approximately 5.7%

46
00:03:09,343 --> 00:03:17,540
of the trips were from the upper east side to the Upper East Side.

47
00:03:19,830 --> 00:03:24,100
And the data is sorted by descending counts, so we can see that

48
00:03:24,100 --> 00:03:29,270
the majority of the trips happened between the Upper East Side

49
00:03:29,270 --> 00:03:34,910
and that they represent about 5.7% of the data.

50
00:03:34,910 --> 00:03:37,470
The next one is from Midtown to Midtown.

51
00:03:37,470 --> 00:03:40,790
So it's interesting to see that the majority of taxi trips

52
00:03:40,790 --> 00:03:43,690
seemed to be happening between the same neighborhood.

53
00:03:43,690 --> 00:03:49,814
A lot of locality has built into this data.

54
00:03:49,814 --> 00:03:53,140
But we don't have to stop at the percentages for

55
00:03:53,140 --> 00:03:56,950
all of the neighborhoods, the total percentages.

56
00:03:56,950 --> 00:04:00,200
We can ask the question the following way.

57
00:04:00,200 --> 00:04:06,270
We can ask for all of the trips leaving the Upper East Side,

58
00:04:06,270 --> 00:04:11,540
what is the proportion that end in the Upper East Side?

59
00:04:11,540 --> 00:04:14,640
So in this case, our denominator is not all of the trips.

60
00:04:14,640 --> 00:04:18,690
It's all of the trips leaving a particular neighborhood.

61
00:04:18,690 --> 00:04:23,840
And how they distribute to their different destinations.

62
00:04:23,840 --> 00:04:29,240
And in this case, the answer is 36.97%.

63
00:04:30,660 --> 00:04:36,286
We can also ask, of all of the trips that end up in the Upper East Side,

64
00:04:36,286 --> 00:04:40,560
how many of them originate from the Upper East Side?

65
00:04:40,560 --> 00:04:45,702
So the denominator, in this case, is trips that end up in the same

66
00:04:45,702 --> 00:04:50,856
neighborhood, and we're asking, where are they coming from.

67
00:04:50,856 --> 00:04:55,186
And the answer in this case is 38.37% and so

68
00:04:55,186 --> 00:04:59,470
we have proportions out of the total.

69
00:04:59,470 --> 00:05:05,870
Proportions that add up to 100% for trips leaving the same neighborhood.

70
00:05:05,870 --> 00:05:09,630
And proportions that are out of 100% for

71
00:05:09,630 --> 00:05:13,295
trips ending up in the same neighborhood.

72
00:05:13,295 --> 00:05:17,645
Now, we could basically keep taking this table and

73
00:05:17,645 --> 00:05:23,695
sorting it in various ways, and trying to make sense of the numbers.

74
00:05:23,695 --> 00:05:26,985
But, of course, the easier thing to do is for us to move quickly to

75
00:05:26,985 --> 00:05:30,665
a visualization that's going to give us the gist of the information.

76
00:05:32,465 --> 00:05:36,883
So now we're gonna create three visualizations that are going to

77
00:05:36,883 --> 00:05:39,380
help us visualize the proportions.

