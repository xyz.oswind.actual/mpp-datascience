0
00:00:00,000 --> 00:00:01,265
So we saw how,

1
00:00:01,265 --> 00:00:06,436
using the rowSelection argument in the last section, and

2
00:00:06,436 --> 00:00:11,817
passing that to rxSummary, we were able to subset our data,

3
00:00:11,817 --> 00:00:16,690
and then get whichever summary we're interested in.

4
00:00:16,690 --> 00:00:21,542
We're gonna use rowSelection now to do something slightly different, but

5
00:00:21,542 --> 00:00:22,462
very common.

6
00:00:22,462 --> 00:00:28,561
And that is we're gonna look at ways that we can identify outliers,

7
00:00:28,561 --> 00:00:29,670
right?

8
00:00:29,670 --> 00:00:30,530
And this is a case where

9
00:00:30,530 --> 00:00:33,710
visualizations can be very helpful, right?

10
00:00:33,710 --> 00:00:37,980
We're gonna go to our data and we're gonna come up with some kind of

11
00:00:37,980 --> 00:00:42,670
business logic for what we think should be an outlier.

12
00:00:42,670 --> 00:00:45,096
And this is gonna be somewhat subjective, right?

13
00:00:45,096 --> 00:00:49,909
It is more determined based on what we think makes intuitive sense than

14
00:00:49,909 --> 00:00:52,610
anything particularly statistical.

15
00:00:54,010 --> 00:00:58,590
And an outlier here is gonna be any trip that we think is just a little

16
00:00:58,590 --> 00:01:02,150
bit odd, should not be happening or should not be very common.

17
00:01:02,150 --> 00:01:06,240
And then we're gonna see if we can use something like visualizations to

18
00:01:06,240 --> 00:01:13,050
answer one simple question about the behavior of some of our outliers.

19
00:01:13,050 --> 00:01:17,740
So we're back here with a rxDataStep column.

20
00:01:17,740 --> 00:01:22,820
And in this case, we're gonna use rxDataStep to pull out

21
00:01:22,820 --> 00:01:27,525
a data frame containing all of our outliers.

22
00:01:27,525 --> 00:01:31,565
So notice that the first thing we do is we feed the large data

23
00:01:31,565 --> 00:01:33,185
to rxDataStep.

24
00:01:33,185 --> 00:01:36,095
But what's missing here is we don't have an out file,

25
00:01:36,095 --> 00:01:38,975
we're not putting the output anywhere.

26
00:01:38,975 --> 00:01:42,534
Instead, we are doing assignment on the left, so

27
00:01:42,534 --> 00:01:45,840
when the out file is missing from rxDataStep,

28
00:01:45,840 --> 00:01:50,610
by default rxDataStep is gonna dump everything into a data frame.

29
00:01:50,610 --> 00:01:56,490
And in this case we're gonna call that data frame odd_trips.

30
00:01:56,490 --> 00:01:58,370
Then we use our rowSelection again.

31
00:01:58,370 --> 00:02:02,750
So, in the last section we used rowSelection with rxSummary,

32
00:02:02,750 --> 00:02:05,880
now we're using it with rxDataStep to

33
00:02:05,880 --> 00:02:08,220
basically just take a subset of the rows.

34
00:02:09,440 --> 00:02:11,717
We're not summarizing the data here in any way.

35
00:02:11,717 --> 00:02:15,999
We're just taking a subset of the rows, and when we have our subset

36
00:02:15,999 --> 00:02:20,525
we're going to dump the results in the data frame called odd_trips.

37
00:02:21,530 --> 00:02:25,470
And this is the main logic for

38
00:02:25,470 --> 00:02:30,330
what we think is a candidate for an outlier.

39
00:02:30,330 --> 00:02:33,600
So we're gonna say that anything where the trip distance is greater

40
00:02:33,600 --> 00:02:38,830
than 50 or less than equal to 0 is gonna be an outlier.

41
00:02:38,830 --> 00:02:41,870
Or anything where passenger count is greater than five or

42
00:02:41,870 --> 00:02:44,450
passenger count is zero is an outlier, right?

43
00:02:44,450 --> 00:02:48,460
These are things that just would be unusual data and

44
00:02:48,460 --> 00:02:51,631
we're also gonna look at fare amount and

45
00:02:51,631 --> 00:02:56,402
say anything where fare amount is greater than 5,000.

46
00:02:56,402 --> 00:03:01,487
It's probably not very common for someone to pay $5,000 for their

47
00:03:01,487 --> 00:03:06,250
taxi fare or anything where the fare amount is less than equal to 0.

48
00:03:06,250 --> 00:03:10,019
And this is a case where it might actually be common, especially if

49
00:03:11,560 --> 00:03:16,950
refunds in the data are represented by negative fare amounts.

50
00:03:16,950 --> 00:03:19,620
But it's an outlier in the sense that

51
00:03:19,620 --> 00:03:22,390
those are not trips that we necessarily want to know about, so

52
00:03:22,390 --> 00:03:25,880
we're gonna leave it out of the data.

53
00:03:25,880 --> 00:03:28,730
And before doing so we wanna take those,

54
00:03:28,730 --> 00:03:30,290
we wanna dump it into a data frame.

55
00:03:30,290 --> 00:03:34,490
And make sure that really they actually are what we think they are,

56
00:03:34,490 --> 00:03:39,520
that we have the right intuition about them.

57
00:03:39,520 --> 00:03:42,243
There is one other thing that we're gonna do.

58
00:03:42,243 --> 00:03:47,026
Recall that analyze CXDF is a potentially very large data set and

59
00:03:47,026 --> 00:03:52,245
we're using rowSelection to select the subset of the data.

60
00:03:52,245 --> 00:03:56,790
And we think that this subset is going to be a pretty small subset

61
00:03:56,790 --> 00:03:59,540
compared to the data at large.

62
00:03:59,540 --> 00:04:03,170
Because based on some business logic,

63
00:04:03,170 --> 00:04:07,440
the criteria that we are using here to subset the data are going to

64
00:04:07,440 --> 00:04:10,920
limit it to a very small piece of the data.

65
00:04:10,920 --> 00:04:14,660
But it could still be that the end result is

66
00:04:14,660 --> 00:04:17,490
still gonna be a very large data frame.

67
00:04:17,490 --> 00:04:22,611
So what we're gonna do is of the rows that meet this criteria,

68
00:04:22,611 --> 00:04:25,470
we're only going to select 5%.

69
00:04:25,470 --> 00:04:29,063
And the way we're gonna do that is we're gonna first perform

70
00:04:29,063 --> 00:04:32,930
a transformation, and the transformation is pretty basic.

71
00:04:32,930 --> 00:04:36,024
It's gonna use the runif function, and

72
00:04:36,024 --> 00:04:41,488
it's gonna give us a column called u with numbers between zero and one.

73
00:04:41,488 --> 00:04:44,695
Now in the data, we already had a column called u.

74
00:04:44,695 --> 00:04:47,961
In this case, we're going to overwrite that column, and

75
00:04:47,961 --> 00:04:50,904
it's going to just be numbers between zero and one.

76
00:04:50,904 --> 00:04:54,952
And then, as part of the rowSelection,

77
00:04:54,952 --> 00:04:59,368
we're going to let u be less than 0.05.

78
00:04:59,368 --> 00:05:02,953
So this is our way of telling the data that give me all the rows

79
00:05:02,953 --> 00:05:05,730
that meet the following criteria.

80
00:05:05,730 --> 00:05:09,161
And on top of that, where u is less than 0.05.

81
00:05:09,161 --> 00:05:14,585
And because that happens with a probability of 5%,

82
00:05:14,585 --> 00:05:23,010
we're gonna end up with 5% of the data that meets this criteria, okay?

83
00:05:23,010 --> 00:05:27,720
The last thing to point out in here is we have this object called

84
00:05:27,720 --> 00:05:29,560
.rxNumRows.

85
00:05:29,560 --> 00:05:32,610
The fact that there is an rx prefix to it should tell you that

86
00:05:32,610 --> 00:05:36,630
it's probably something you need to RevoScaleR, and it is.

87
00:05:36,630 --> 00:05:41,340
This is just one example of some

88
00:05:42,940 --> 00:05:45,360
variables that automatically get assigned, so

89
00:05:45,360 --> 00:05:48,990
that we can use them in our transformations if we need to.

90
00:05:48,990 --> 00:05:55,340
In this case, rxNumRows is always going to be equal to the size

91
00:05:55,340 --> 00:05:59,600
of the chunk of the data when the transformation is happening.

92
00:05:59,600 --> 00:06:03,550
Because the data is being brought in chunk by chunk, each

93
00:06:03,550 --> 00:06:08,090
chunk is gonna have a particular size or a certain number of rows.

94
00:06:08,090 --> 00:06:11,340
And the number of rows is going to be stored

95
00:06:11,340 --> 00:06:14,790
in this object called .rxNumRows.

96
00:06:14,790 --> 00:06:19,210
And so, when we need to know that information,

97
00:06:19,210 --> 00:06:21,820
we can simply refer to this object.

98
00:06:21,820 --> 00:06:25,177
And in this case, we need to know it because we're gonna generate

99
00:06:25,177 --> 00:06:27,080
a certain number of random numbers.

100
00:06:27,080 --> 00:06:30,930
And how many we generate depends on how many rows we have in the data

101
00:06:30,930 --> 00:06:35,130
for each chunk of the data as the transformation is happening.

102
00:06:35,130 --> 00:06:41,210
So let's run this, there and so the transformation is gonna run and

103
00:06:41,210 --> 00:06:46,890
we should be able to see how many rows we ended up with in the final

104
00:06:46,890 --> 00:06:52,600
data frame that is called odd_trips.

105
00:06:52,600 --> 00:06:55,342
Once we have the data frame,

106
00:06:55,342 --> 00:07:01,070
we can then go back to our familiar open source R territory.

107
00:07:01,070 --> 00:07:06,202
And in this case, we're gonna use the data frame and we're gonna

108
00:07:06,202 --> 00:07:11,440
limit it to only the rows where trip distance is greater than 50.

109
00:07:11,440 --> 00:07:17,780
Notice, that we're using the dplyr notation here to accomplish this.

110
00:07:17,780 --> 00:07:22,712
And we're gonna pass that to ggplot 2 to draw a plot.

111
00:07:22,712 --> 00:07:24,104
So the transformation is over.

112
00:07:24,104 --> 00:07:28,825
We have about 6,800 rows in the data that is

113
00:07:28,825 --> 00:07:34,620
supposed to be a representation of our outliers.

114
00:07:34,620 --> 00:07:38,470
So that's a pretty small data frame and it's certainly not gonna

115
00:07:38,470 --> 00:07:41,590
be a problem for ggplot 2 to give us plots on this data.

116
00:07:43,210 --> 00:07:45,270
So, in this case we're gonna look at the following plot.

117
00:07:48,130 --> 00:07:51,470
So we're limiting the data to cases where trip distance is greater

118
00:07:51,470 --> 00:07:52,940
than 50.

119
00:07:52,940 --> 00:07:59,130
And we're asking for a histogram of fare_amount.

120
00:07:59,130 --> 00:08:03,080
And we're gonna color code the histogram by whether or

121
00:08:03,080 --> 00:08:07,565
not the trip took less than 10 minutes or not, okay?

122
00:08:08,710 --> 00:08:14,235
So whether the trip took less than 10 minutes or not and because trip

123
00:08:14,235 --> 00:08:19,963
duration is in seconds, we use 10*60 to get 10 minutes, okay?

124
00:08:19,963 --> 00:08:24,430
So let's go back to the plot to look at it.

125
00:08:24,430 --> 00:08:25,150
Here is the plot.

126
00:08:26,190 --> 00:08:29,550
And the intuition here is we wanna see first of all,

127
00:08:29,550 --> 00:08:33,790
these are trips where the distance traveled was pretty wide.

128
00:08:33,790 --> 00:08:35,649
We traveled more than 50 miles, right?

129
00:08:36,730 --> 00:08:39,650
And so, technically these are trips where the fare amount

130
00:08:39,650 --> 00:08:40,665
should be pretty high.

131
00:08:40,665 --> 00:08:45,023
But what we see is that the majority of these trips have fare amount,

132
00:08:45,023 --> 00:08:48,547
well, all of them have a fare amount less than 300.

133
00:08:48,547 --> 00:08:52,862
And there are some where the fare amount is not even $50 or

134
00:08:52,862 --> 00:08:54,196
close to $50.

135
00:08:54,196 --> 00:08:59,220
Right, so this doesn't really make business sense, so we can kind of

136
00:08:59,220 --> 00:09:02,440
start guessing that this is probably due to errors in the data.

137
00:09:02,440 --> 00:09:07,549
It could be that the trip distance was wrongly recorded.

138
00:09:07,549 --> 00:09:11,858
We can also see that all of these trips

139
00:09:11,858 --> 00:09:16,030
took more than 10 minutes to run.

140
00:09:17,360 --> 00:09:22,450
So, now we're getting a little bit suspicious.

141
00:09:22,450 --> 00:09:24,870
These are trips that took more than 10 minutes.

142
00:09:26,450 --> 00:09:29,840
They supposedly traveled greater than 50 miles,

143
00:09:29,840 --> 00:09:32,260
the fare amount is pretty small.

144
00:09:32,260 --> 00:09:34,620
We can go back and we can make certain changes.

145
00:09:34,620 --> 00:09:36,480
We can go here and say, okay,

146
00:09:36,480 --> 00:09:40,810
can you tell me if the trips took more than 90 minutes?

147
00:09:42,230 --> 00:09:43,820
And recreate our plots.

148
00:09:45,130 --> 00:09:49,232
And we can see now that there are cases where the trip took more than

149
00:09:49,232 --> 00:09:50,840
90 minutes.

150
00:09:50,840 --> 00:09:53,410
It's the majority of cases.

151
00:09:55,150 --> 00:09:58,010
So if the trip distance is greater than 50 and

152
00:09:58,010 --> 00:10:04,130
the trip really did take quite a while to run, more than an hour and

153
00:10:04,130 --> 00:10:08,500
a half, it could be that it's a legitimate trip.

154
00:10:08,500 --> 00:10:10,700
So why is the fare amount so small?

155
00:10:10,700 --> 00:10:13,020
That's something that we would have to investigate.

156
00:10:14,660 --> 00:10:18,720
Or it could be that the trip distance is what's wrongly recorded.

157
00:10:19,920 --> 00:10:22,690
Or it could be somehow that the whole thing is legitimate

158
00:10:22,690 --> 00:10:26,770
in the sense that a trip really did travel that far,

159
00:10:26,770 --> 00:10:29,000
the fare was really small.

160
00:10:29,000 --> 00:10:31,090
And as we can tell from the duration,

161
00:10:31,090 --> 00:10:35,680
it took at least an hour and a half for the trip to be over, right?

162
00:10:35,680 --> 00:10:37,680
So there's interesting things about the data,

163
00:10:37,680 --> 00:10:41,230
about these outliers in the data.

164
00:10:41,230 --> 00:10:46,264
And we should be wondering if something in the data is wrongly

165
00:10:46,264 --> 00:10:51,396
recorded or if all of this is legitimate and we just have a wrong

166
00:10:51,396 --> 00:10:57,242
understanding of the distribution of some of the columns in the data.

