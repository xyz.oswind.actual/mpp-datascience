0
00:00:01,300 --> 00:00:02,060
Welcome back.

1
00:00:02,060 --> 00:00:04,260
I'm here this week with Jeremy Reynolds,

2
00:00:04,260 --> 00:00:07,410
who is a fellow data scientist at Microsoft.

3
00:00:07,410 --> 00:00:11,910
And Jeremy is here to help us understand what it is going to be

4
00:00:11,910 --> 00:00:16,145
like running Microsoft R Server inside of SQL Server.

5
00:00:16,145 --> 00:00:17,075
So welcome, Jeremy.

6
00:00:17,075 --> 00:00:19,275
>> Thanks, I'm happy to be here.

7
00:00:19,275 --> 00:00:22,325
>> So let me give you a kind of an overview of what we've

8
00:00:22,325 --> 00:00:24,215
done in the course so far.

9
00:00:24,215 --> 00:00:29,840
We started with some CSV files for the New York City taxi dataset.

10
00:00:29,840 --> 00:00:33,510
We got a sample of those files and

11
00:00:33,510 --> 00:00:36,320
we used the Microsoft R client to read that.

12
00:00:36,320 --> 00:00:40,810
And we used the RevoScaleR library to run some analytics on that data.

13
00:00:40,810 --> 00:00:42,860
So we have various plots and summaries,

14
00:00:42,860 --> 00:00:47,350
we have some examples of models as well as taking the models and

15
00:00:47,350 --> 00:00:52,220
scoring so having like training the models on a training data set and

16
00:00:52,220 --> 00:00:55,698
then scoring it on test data sets we evaluate the models.

17
00:00:55,698 --> 00:01:01,210
And what I'm hoping to use your expertise for

18
00:01:01,210 --> 00:01:05,510
is understanding what is going to change, how things are gonna be

19
00:01:05,510 --> 00:01:10,440
different when we have data that is sitting inside of SQL Server

20
00:01:10,440 --> 00:01:15,510
instead of data that is sitting locally on our machines.

21
00:01:15,510 --> 00:01:16,160
>> Okay, great. Yeah,

22
00:01:16,160 --> 00:01:17,790
I can definitely help out with that.

23
00:01:17,790 --> 00:01:20,990
>> Okay, so what is I guess we'll start at the top,

24
00:01:20,990 --> 00:01:23,230
what is a production environment?

25
00:01:23,230 --> 00:01:25,000
What do we usually mean by that?

26
00:01:25,000 --> 00:01:30,080
>> Yeah so production environment is typically a scenario or

27
00:01:30,080 --> 00:01:35,450
a computational environment that we are using with customers or

28
00:01:35,450 --> 00:01:38,780
in a public domain.

29
00:01:38,780 --> 00:01:42,860
So if there's a situation where we have done a lot of data science and

30
00:01:42,860 --> 00:01:46,620
developed a model that we might want to score very quickly or

31
00:01:46,620 --> 00:01:49,600
that we might want to provide kind of end points

32
00:01:49,600 --> 00:01:54,480
to then the production environment would be that deployment.

33
00:01:55,930 --> 00:02:00,070
And in that context we care about a few different things in

34
00:02:00,070 --> 00:02:04,660
a production environment rather than in the environment where we're doing

35
00:02:04,660 --> 00:02:06,190
exploratory data analysis.

36
00:02:06,190 --> 00:02:08,030
So for production environment,

37
00:02:08,030 --> 00:02:11,060
we really care a lot about two things in particular.

38
00:02:11,060 --> 00:02:14,170
One, we care about security, right?

39
00:02:14,170 --> 00:02:15,650
So we don't necessarily want

40
00:02:17,430 --> 00:02:22,350
to surface all of our data or all of our

41
00:02:24,630 --> 00:02:29,052
interactions with the data to a public facing scenario right, and

42
00:02:29,052 --> 00:02:33,780
two, one thing we care a lot about is speed,

43
00:02:33,780 --> 00:02:37,870
we want our production environment to be really really efficient and

44
00:02:37,870 --> 00:02:41,570
that might be because we're expecting millions of

45
00:02:41,570 --> 00:02:44,830
hits on that particular instance to that server at any point in time.

46
00:02:46,540 --> 00:02:49,180
Or it might be because we want to provide the best

47
00:02:49,180 --> 00:02:52,940
possible user experience to our customers.

48
00:02:52,940 --> 00:02:53,480
>> Okay. >> And

49
00:02:53,480 --> 00:02:57,619
so in the context of that kind of production environment in order to

50
00:02:57,619 --> 00:03:02,685
satisfy a couple of a couple of those constraints we actually

51
00:03:02,685 --> 00:03:06,785
might go back and redo or reimplement some of our kind of EDA

52
00:03:06,785 --> 00:03:10,625
type of approaches that we did kind of to begin with.

53
00:03:10,625 --> 00:03:14,355
>> Okay. So is it fair to say that doing

54
00:03:14,355 --> 00:03:17,965
exploratory data analysis which is what we've done in the course so

55
00:03:17,965 --> 00:03:21,130
far is a little bit sloppy?

56
00:03:21,130 --> 00:03:24,690
And when we think about implementing that in a production environment,

57
00:03:24,690 --> 00:03:30,840
we're talking about revisiting our code and making some changes to it.

58
00:03:30,840 --> 00:03:35,420
Making it more efficient, making it more of a straight forward

59
00:03:35,420 --> 00:03:37,850
linear approach instead of being all over

60
00:03:40,940 --> 00:03:44,020
looking at too many summaries, too many plots and charts, kind of.

61
00:03:44,020 --> 00:03:46,950
It's more of a focused sort of approach.

62
00:03:48,100 --> 00:03:50,470
>> I think sloppy's a strong word to use.

63
00:03:50,470 --> 00:03:51,440
>> Yeah, okay.

64
00:03:51,440 --> 00:03:54,310
>> But I think that where exploratory data

65
00:03:54,310 --> 00:03:57,360
analysis is really kind of driven by what the data tells you,

66
00:03:57,360 --> 00:03:59,980
because you don't know a lot kind of when you're doing it.

67
00:03:59,980 --> 00:04:01,230
>> Yeah. >> Production is when you know

68
00:04:01,230 --> 00:04:02,090
something, right?

69
00:04:02,090 --> 00:04:07,010
And so you have the very prescribed set of steps that you want to take

70
00:04:07,010 --> 00:04:10,560
and so you want to be as efficient as possible in terms of driving

71
00:04:10,560 --> 00:04:14,260
towards what you already know what you want to do as opposed to letting

72
00:04:14,260 --> 00:04:15,860
the data tell you a lot of the answers.

73
00:04:15,860 --> 00:04:18,370
>> Okay so the production environment that

74
00:04:18,370 --> 00:04:22,190
we're gonna talk about in this case is gonna be Microsoft SQL Server.

75
00:04:22,190 --> 00:04:26,580
What are some of the advantages of using Microsoft SQL Server to

76
00:04:28,160 --> 00:04:30,150
as our production environment?

77
00:04:30,150 --> 00:04:37,985
Well Microsoft SQL Server is world class at a variety of topics.

78
00:04:37,985 --> 00:04:42,935
Two of which are speed, so it is extremely fast for

79
00:04:42,935 --> 00:04:45,945
relational database management system.

80
00:04:45,945 --> 00:04:51,020
And then two, it's also top of class in terms of security features.

81
00:04:51,020 --> 00:04:54,210
And so there's two things we care about in the production environment.

82
00:04:54,210 --> 00:04:56,460
It really sets up the pinnacle of those

83
00:04:56,460 --> 00:04:57,720
two domains, >> Yeah.

84
00:04:57,720 --> 00:05:03,020
Now is reading data from a relational database like SQL server,

85
00:05:03,020 --> 00:05:06,870
is that a new thing when using R or

86
00:05:06,870 --> 00:05:10,830
is that something that we've had before?

87
00:05:10,830 --> 00:05:14,400
I mean how is Microsoft R server, what I'm trying to get at is how is

88
00:05:14,400 --> 00:05:20,080
it gonna be different from interacting with relational

89
00:05:20,080 --> 00:05:23,510
database is using R the way that we've traditionally done that?

90
00:05:23,510 --> 00:05:27,690
>> Yeah, so R has been able to speak to relational databases including

91
00:05:27,690 --> 00:05:28,518
SQL server.

92
00:05:28,518 --> 00:05:30,904
Previously, right?

93
00:05:30,904 --> 00:05:31,860
>> Mm-hm.

94
00:05:31,860 --> 00:05:36,060
>> And so typically how that goes about is that you define

95
00:05:36,060 --> 00:05:39,260
a particular connection to the database.

96
00:05:39,260 --> 00:05:42,860
And in with typical packages

97
00:05:42,860 --> 00:05:47,300
within open source R you simply do a query on the database.

98
00:05:47,300 --> 00:05:49,600
So you access the database, you pull data out,

99
00:05:49,600 --> 00:05:52,920
and then you do analytics on the data that you pull out.

100
00:05:52,920 --> 00:05:53,468
Right.

101
00:05:53,468 --> 00:05:57,430
What Microsoft R server and SQL R services really allows

102
00:05:57,430 --> 00:06:02,430
is to actually take the analytics inside the database.

103
00:06:02,430 --> 00:06:05,760
And so we take it to the data so that the server,

104
00:06:05,760 --> 00:06:11,470
the SQL server can actually do the computation right.

105
00:06:11,470 --> 00:06:15,210
Rather than either moving the data out of the database which we might

106
00:06:15,210 --> 00:06:18,190
not want to do for a variety of reasons.

107
00:06:18,190 --> 00:06:21,400
And not necessarily doing the computation on our

108
00:06:21,400 --> 00:06:25,820
local client machine, which might be limited in a lot of capacities that

109
00:06:25,820 --> 00:06:28,050
the SQL instance is not.

110
00:06:28,050 --> 00:06:31,980
>> Okay, so we have the situation where we have as the data scientist,

111
00:06:31,980 --> 00:06:35,180
we have R on a client machine and

112
00:06:35,180 --> 00:06:38,920
it is connecting to a database on a remote server.

113
00:06:41,840 --> 00:06:46,246
But instead of establishing some kind of audio VC connection and

114
00:06:46,246 --> 00:06:50,244
transferring data over the network to our local machine,

115
00:06:50,244 --> 00:06:53,440
we are launching an R process on the database.

116
00:06:53,440 --> 00:06:56,280
So there is a separate installation of R on the database.

117
00:06:56,280 --> 00:06:56,890
>> Separate, yeah.

118
00:06:56,890 --> 00:06:59,760
>> Okay, and that separate installation is going to take

119
00:06:59,760 --> 00:07:00,940
our R code.

120
00:07:00,940 --> 00:07:04,940
It's going to take the data in the database without

121
00:07:04,940 --> 00:07:06,380
transferring it over the network.

122
00:07:06,380 --> 00:07:08,690
It's gonna run the analytics on the data.

123
00:07:08,690 --> 00:07:11,430
And then, what happens with the results?

124
00:07:11,430 --> 00:07:13,560
Well you can do lots of things with the results.

125
00:07:13,560 --> 00:07:20,980
So the results might be, it could be as simple as the results are just

126
00:07:20,980 --> 00:07:24,270
printed out to the client machine that you're interacting with.

127
00:07:24,270 --> 00:07:28,850
So in an exploratory data analysis stop, you might simply interact

128
00:07:28,850 --> 00:07:33,370
with that SQL Server instance and pretty much exactly the same way

129
00:07:33,370 --> 00:07:37,280
that you're talking about up to this point where you might be running

130
00:07:37,280 --> 00:07:41,340
summary statistic, or creating graphs that are visualized or

131
00:07:41,340 --> 00:07:44,040
read on the screen that you're working with.

132
00:07:44,040 --> 00:07:48,530
If you want to do something beyond that you can either

133
00:07:48,530 --> 00:07:53,460
save the results as a file on the SQL server instance, or

134
00:07:53,460 --> 00:07:58,050
save it as an object or a table within the database itself.

135
00:07:58,050 --> 00:08:01,450
So you have a lot of flexibility with respect to how you

136
00:08:01,450 --> 00:08:05,070
interact with the database, and one other thing that

137
00:08:06,710 --> 00:08:09,310
I wanted to bring up is because you have this flexibility,

138
00:08:09,310 --> 00:08:13,990
because you can write new tables to the database or so on.

139
00:08:13,990 --> 00:08:17,390
One of the things about doing these in database analytics

140
00:08:17,390 --> 00:08:20,370
that's really nice is that you get all of the auditing and

141
00:08:20,370 --> 00:08:25,310
all of the kind of reporting that SQL allows for and

142
00:08:25,310 --> 00:08:29,290
permits but all you're really kind of interacting with is through R.

143
00:08:29,290 --> 00:08:32,185
>> Okay, so

144
00:08:32,185 --> 00:08:36,575
relational databases come with a programming language called SQL.

145
00:08:36,575 --> 00:08:40,255
And everyone knows SQL, everyone loves SQL, right.

146
00:08:40,255 --> 00:08:43,925
I mean I guess the question that comes up is

147
00:08:45,870 --> 00:08:51,650
what is a general rule of thumb about where SQL kind of leaves off

148
00:08:51,650 --> 00:08:56,540
and R is a good candidate to sort of pick things up and run with it?

149
00:08:56,540 --> 00:09:00,690
>> Yeah so I think our general rule of thumb is that

150
00:09:02,320 --> 00:09:07,440
we want to let each of the tools do what they're really strong at.

151
00:09:07,440 --> 00:09:10,230
>> And with SQL what that really means, or

152
00:09:10,230 --> 00:09:15,380
SQL, what that really means is that all of the, not all of the but

153
00:09:15,380 --> 00:09:20,680
a lot of the data aggregations, a lot of the table merges or

154
00:09:20,680 --> 00:09:25,680
sorts are those are the kinds of things that SQL server and

155
00:09:25,680 --> 00:09:29,200
other relational data management systems have really been designed

156
00:09:29,200 --> 00:09:33,480
to optimize and say we want to layer, let those RDMSs or

157
00:09:33,480 --> 00:09:38,100
Relational Database Management Systems do those workhorse kind

158
00:09:38,100 --> 00:09:38,889
of tasks.

159
00:09:38,889 --> 00:09:43,620
Whereas things that are is really particularly good at or

160
00:09:43,620 --> 00:09:48,400
has particular strings are in the context of either visualizations or

161
00:09:48,400 --> 00:09:52,240
in the context of in terms of algorithms, analytics, so

162
00:09:52,240 --> 00:09:55,980
model estimation and so on.

163
00:09:55,980 --> 00:10:01,010
And so if we were to draw kind of a line in

164
00:10:01,010 --> 00:10:06,650
the sand then if you are extremely proficient with both R and SQL

165
00:10:06,650 --> 00:10:12,580
then you would probably draw that line around kind of aggregations,

166
00:10:12,580 --> 00:10:18,980
table joins, and sorting on one hand and analytics on the other.

167
00:10:18,980 --> 00:10:22,120
>> Okay, what about visualizations?

168
00:10:22,120 --> 00:10:24,010
>> Visualization, it depends a little bit.

169
00:10:24,010 --> 00:10:29,420
So if you have access to SQL Server Reporting Services or

170
00:10:29,420 --> 00:10:32,600
SSRS and you particularly adapt those kinds of

171
00:10:32,600 --> 00:10:35,430
visualization then you could absolutely you can go that route.

172
00:10:35,430 --> 00:10:39,859
But if you are an expert in R, let's say visualization with an AR and

173
00:10:39,859 --> 00:10:42,894
in particular sophisticated visualizations

174
00:10:42,894 --> 00:10:45,771
that might require multiple overlays and so

175
00:10:45,771 --> 00:10:49,210
on are really kind of bread and butter inside R.

176
00:10:49,210 --> 00:10:49,730
>> Okay.

177
00:10:49,730 --> 00:10:52,060
So if you're a ggplot2 two user,

178
00:10:52,060 --> 00:10:56,660
you can still use SQL server to host your data.

179
00:10:56,660 --> 00:10:58,870
>> Yep. >> And you can hit SQL server and

180
00:10:58,870 --> 00:11:00,510
run visualizations.

181
00:11:00,510 --> 00:11:03,921
Now let's say, you do exactly that.

182
00:11:03,921 --> 00:11:08,270
Let's say you use R for what R is good at.

183
00:11:08,270 --> 00:11:12,760
Which in a lot of cases is gonna be, either some kind of analytics,

184
00:11:12,760 --> 00:11:16,060
algorithm or some kind of sophisticated visualization.

185
00:11:16,060 --> 00:11:20,180
What do you do when it comes to saving the results?

186
00:11:20,180 --> 00:11:23,170
>> Yeah, so that's a great question and

187
00:11:23,170 --> 00:11:27,800
it depends upon ultimately what your outcome measure is.

188
00:11:27,800 --> 00:11:30,690
Right or what you want to do with it.

189
00:11:30,690 --> 00:11:35,540
Just like any other R session you can save the visualization or

190
00:11:35,540 --> 00:11:40,840
the results of an analysis as a binary file,

191
00:11:40,840 --> 00:11:44,380
either on the server machine or on the client machine.

192
00:11:44,380 --> 00:11:49,890
But if you're trying to integrate into some other kind of topic or

193
00:11:49,890 --> 00:11:51,530
some other kind of reporting, right?

194
00:11:51,530 --> 00:11:55,370
So maybe you have a SQL Server reporting services

195
00:11:55,370 --> 00:11:57,510
pipeline all ready kind of charted up.

196
00:11:57,510 --> 00:11:59,930
And all you really want to do is insert another graph,

197
00:11:59,930 --> 00:12:03,330
there's actually pretty easy ways to do that by writing the results out

198
00:12:03,330 --> 00:12:06,500
whether it's a visualization or

199
00:12:06,500 --> 00:12:12,350
a model that we might use for scoring into a SQL

200
00:12:12,350 --> 00:12:15,496
table that other services can then understand and then read back in.

201
00:12:15,496 --> 00:12:17,714
>> Okay, great.

202
00:12:17,714 --> 00:12:22,170
Now I think one area that might be a little bit hazy is, so we're

203
00:12:22,170 --> 00:12:26,820
talking in this course and specific about the RevoScaleR package.

204
00:12:26,820 --> 00:12:30,310
But what happens with all the other

205
00:12:30,310 --> 00:12:32,450
R functions that are not part of RevoScaleR?

206
00:12:32,450 --> 00:12:38,180
Can we still let's say use DLN function to build a linear model and

207
00:12:38,180 --> 00:12:41,710
data that is sitting in SQL Server and do it in database?

208
00:12:41,710 --> 00:12:44,570
>> Yeah so the answer to that is,

209
00:12:45,870 --> 00:12:48,650
you can give them some constraints, right?

210
00:12:48,650 --> 00:12:53,050
So R on SQL Server is going to have the same kinds of

211
00:12:53,050 --> 00:12:58,050
constraints that R has on a local file system or on a local client,

212
00:12:58,050 --> 00:13:02,180
which is to say that, if your data can fit in memory, then sure,

213
00:13:02,180 --> 00:13:08,300
you can use open source packages to your R's content or

214
00:13:08,300 --> 00:13:13,280
open source analytics functions to your R's content.

215
00:13:13,280 --> 00:13:14,070
>> Yeah.

216
00:13:14,070 --> 00:13:17,660
>> Right? But as soon as you start getting to

217
00:13:17,660 --> 00:13:21,580
larger data sets that may not fit into memory or

218
00:13:21,580 --> 00:13:26,340
that might start involving swap space and so on.

219
00:13:26,340 --> 00:13:29,060
Then you are going to get much more performance.

220
00:13:31,220 --> 00:13:35,620
Results are a much more performance experience when

221
00:13:35,620 --> 00:13:37,360
leveraging the RevoScaleR.

222
00:13:37,360 --> 00:13:39,980
>> Okay, so it's fair to say that it's not very different from the way

223
00:13:39,980 --> 00:13:42,250
it works with say R on a local machine.

224
00:13:42,250 --> 00:13:45,444
>> Yeah. >> As we can use the RevoScaleR

225
00:13:45,444 --> 00:13:48,890
functions, rxLinMod in this case

226
00:13:48,890 --> 00:13:51,990
on a data frame the same way that we can use at LinMod on a data frame.

227
00:13:51,990 --> 00:13:56,070
But if the data frame starts getting really big, even assuming that we

228
00:13:56,070 --> 00:14:00,775
have enough memory to accommodate the size of the larger data frame,

229
00:14:00,775 --> 00:14:05,890
rxLinMmod is gonna be faster than LM because it's a function

230
00:14:05,890 --> 00:14:10,060
that is parallel and so it's gonna process the data frame.

231
00:14:10,060 --> 00:14:13,010
It's gonna give us our linear model faster than LM would.

232
00:14:13,010 --> 00:14:13,610
>> Yeah.

233
00:14:13,610 --> 00:14:18,030
>> Okay so I think that gives us a pretty good idea of what

234
00:14:18,030 --> 00:14:23,290
the advantages of using SQL Server as the production environment and

235
00:14:23,290 --> 00:14:27,000
the place where the data is gonna get stored and

236
00:14:27,000 --> 00:14:31,570
to kind of leverage R when we need it.

237
00:14:31,570 --> 00:14:34,860
Maybe Jeremy the last thing that you could help us too is just do

238
00:14:34,860 --> 00:14:39,460
a quick recap of the discussion that we just had.

239
00:14:39,460 --> 00:14:42,400
Just maybe in like a minute, 30 seconds.

240
00:14:42,400 --> 00:14:46,640
Can you tell us the benefits, the real benefits of doing

241
00:14:46,640 --> 00:14:50,900
what we're about to do running the same code that we sort of developed

242
00:14:50,900 --> 00:14:56,300
over the course of the last three weeks and

243
00:14:56,300 --> 00:15:01,380
now be able to run that on data that is housed inside of SQL Server?

244
00:15:01,380 --> 00:15:06,370
>> Yeah, so the primary benefits of being able to leverage

245
00:15:06,370 --> 00:15:11,095
Microsoft R Server with or Microsoft R with SQL Server

246
00:15:11,095 --> 00:15:16,290
are that you are able to use and

247
00:15:16,290 --> 00:15:20,800
perform scalable analytics inside the database, right?

248
00:15:20,800 --> 00:15:25,210
So you don't have to move large reams of data outside of

249
00:15:25,210 --> 00:15:29,850
the database, which we care about for both speed and for security.

250
00:15:29,850 --> 00:15:31,580
And we don't have to bring those data out.

251
00:15:32,580 --> 00:15:34,710
And analyze them externally.

252
00:15:34,710 --> 00:15:40,550
Rather, we can take the analytics into the database to leverage

253
00:15:41,640 --> 00:15:48,590
both the server's security features, the server's auditing

254
00:15:48,590 --> 00:15:53,937
kind of features and not only that but also its resources right.

255
00:15:53,937 --> 00:15:59,800
So typically SQL servers are pretty beefy and we can

256
00:15:59,800 --> 00:16:04,650
have the opportunity to leverage those resources not just for

257
00:16:04,650 --> 00:16:09,730
doing the types of exploratory data analysis or

258
00:16:09,730 --> 00:16:13,770
the types of SQL queries that we usually do,

259
00:16:13,770 --> 00:16:17,060
but rather to perform sophisticated analytics.

260
00:16:17,060 --> 00:16:19,940
>> So we can be a data scientist who actually gets along with

261
00:16:19,940 --> 00:16:21,250
the database administrator.

262
00:16:21,250 --> 00:16:22,170
>> Let's hope. >> Is that okay, okay.

263
00:16:22,170 --> 00:16:22,920
>> Yeah let's hope.

