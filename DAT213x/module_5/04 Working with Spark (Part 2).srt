0
00:00:01,460 --> 00:00:04,900
So we have our compute context set to spar.

1
00:00:04,900 --> 00:00:08,010
Can we have our data pointed to HDFS?

2
00:00:08,010 --> 00:00:12,110
Can you show us how the rest of the code is now

3
00:00:12,110 --> 00:00:13,640
gonna run in this spar computer.

4
00:00:13,640 --> 00:00:16,170
>> Absolutely, sure, so we have our two objects.

5
00:00:16,170 --> 00:00:18,230
One's called testu_text.

6
00:00:18,230 --> 00:00:21,300
That contains our CSV files in HTFS and

7
00:00:21,300 --> 00:00:24,350
we have another file object called taxi_xdf which

8
00:00:24,350 --> 00:00:27,305
is our pointer to a directory where we will see the XDF files.

9
00:00:27,305 --> 00:00:28,040
>> Okay.

10
00:00:28,040 --> 00:00:30,920
>> Quite fortunately the import function is exactly

11
00:00:30,920 --> 00:00:31,940
the same as before.

12
00:00:31,940 --> 00:00:33,580
It's called rx import.

13
00:00:33,580 --> 00:00:37,443
And it will import the taxi_text data into the taxi_xdf data.

14
00:00:37,443 --> 00:00:40,050
>> Okay. >> One of the things that we did in

15
00:00:40,050 --> 00:00:47,240
the local compute context is we showed that we have the option of

16
00:00:47,240 --> 00:00:52,250
converting something into XDF, but that we don't necessarily have to.

17
00:00:52,250 --> 00:00:55,410
That the analytics functions in RevoScaleR, and

18
00:00:55,410 --> 00:00:57,340
the data processing functions and so on.

19
00:00:57,340 --> 00:01:01,480
They work with the original CSVs or flat files and

20
00:01:01,480 --> 00:01:03,160
they also work with XDF.

21
00:01:03,160 --> 00:01:08,340
That we have pros and cons in either case, related to IO to reading and

22
00:01:08,340 --> 00:01:12,940
writing, so you have the same sort of interactions at play here.

23
00:01:12,940 --> 00:01:15,850
>> So when we worked with local text data, we had some

24
00:01:15,850 --> 00:01:20,060
considerations on where to go to the extra step of going to XDF file.

25
00:01:20,060 --> 00:01:23,550
Same thing resides here by having it in XDF files and

26
00:01:23,550 --> 00:01:26,490
HTFS it'll make the readers for Spark a lot fast and

27
00:01:26,490 --> 00:01:29,130
allows to iterate our algorithm a bit faster as well.

28
00:01:29,130 --> 00:01:31,080
>> Okay >> Let me show you how that looks so

29
00:01:31,080 --> 00:01:34,380
we can run that rx import call directly on our data set.

30
00:01:34,380 --> 00:01:36,230
It will import those CSV files.

31
00:01:36,230 --> 00:01:38,660
Again we have to specify each file individually you

32
00:01:38,660 --> 00:01:39,790
have to supply directory.

33
00:01:39,790 --> 00:01:43,010
And because schema is the same it will automatically import them all

34
00:01:43,010 --> 00:01:44,610
as one big large file.

35
00:01:44,610 --> 00:01:47,066
>> Okay and is it gonna be the output,

36
00:01:47,066 --> 00:01:49,535
is it gonna be a single XDF file or?

37
00:01:49,535 --> 00:01:51,185
>> So let's get to that in a second, so

38
00:01:51,185 --> 00:01:53,760
we will see here that we kick off a Spark job.

39
00:01:53,760 --> 00:01:57,150
It's gonna partition the CSV files into chunks that can

40
00:01:57,150 --> 00:02:00,760
reside in HDFS and after its done all the repartitioning it's gonna do

41
00:02:00,760 --> 00:02:06,850
a reduction, and reduce them into a single XDF object.

42
00:02:06,850 --> 00:02:12,190
That XDF object is actually a big directory called XDF taxi data and

43
00:02:12,190 --> 00:02:15,660
if you look at it, it has 140 million rows 19 variables,

44
00:02:15,660 --> 00:02:20,080
290 blocks, but the very first thing you should notice here is the number

45
00:02:20,080 --> 00:02:22,190
of comps in the data file is equal to 48.

46
00:02:22,190 --> 00:02:26,490
So, before we had one big XTF file that had multiple blocks, and

47
00:02:26,490 --> 00:02:30,640
now we have multiple XTF blocks for each xdf composite file.

48
00:02:30,640 --> 00:02:36,480
>> Okay, and these blocks are stored kind of on a fashion that is decided

49
00:02:36,480 --> 00:02:40,780
by spark across the cluster, right it's not on a single machine or.

50
00:02:40,780 --> 00:02:43,123
>> It's not a single machine, it's distributed across your cluster or

51
00:02:43,123 --> 00:02:45,770
in this case, it's distributed across Azure blob storage.

52
00:02:45,770 --> 00:02:50,990
And it will respect the values or parameters to find by your clusters,

53
00:02:50,990 --> 00:02:54,480
so your cluster's block side for files, respect those files.

54
00:02:54,480 --> 00:02:56,300
It will also save a metadata object.

55
00:02:56,300 --> 00:02:59,200
So, the data object, where the data resides, all the XTF composite

56
00:02:59,200 --> 00:03:01,580
blocks reside, in a separate folder called metadata.

57
00:03:01,580 --> 00:03:05,380
Where it will save some metadata for your coms as well.

58
00:03:05,380 --> 00:03:08,570
>> What about all the output that you were showing us

59
00:03:08,570 --> 00:03:09,460
a little bit earlier?

60
00:03:09,460 --> 00:03:11,500
Is that our output?

61
00:03:11,500 --> 00:03:15,650
Is it output that's generated by spark.

62
00:03:15,650 --> 00:03:18,366
>> Yes in this case I think you often make your progress as concise

63
00:03:18,366 --> 00:03:20,835
as possible, because it can be kind of redundant here.

64
00:03:20,835 --> 00:03:24,067
But in our case because we're doing something in a cluster where many

65
00:03:24,067 --> 00:03:27,075
things that can go wrong and fail, it's good to keep an eye on how

66
00:03:27,075 --> 00:03:29,955
the jobs are created and how they are consumed.

67
00:03:29,955 --> 00:03:33,015
So this is actually rxSpark output, so this is the console

68
00:03:33,015 --> 00:03:36,605
output argument in the rxSpark compute variable we created earlier.

69
00:03:36,605 --> 00:03:41,605
And it will tell us what executors it is creating for this job, so

70
00:03:41,605 --> 00:03:44,590
executor is a unit of computation in spark and

71
00:03:44,590 --> 00:03:47,080
how far it's gone in the process of that computation.

72
00:03:47,080 --> 00:03:47,585
>> Okay. >> So

73
00:03:47,585 --> 00:03:50,047
in the first stage it says your partitions,

74
00:03:50,047 --> 00:03:54,048
this is where it takes the CSV files and makes them into smaller blocks.

75
00:03:54,048 --> 00:03:58,060
It's created 47 executives for this task.

76
00:03:58,060 --> 00:04:00,861
Completes one, completes two, so on and so forth.

77
00:04:00,861 --> 00:04:02,409
It will tell you the progress of that operation.

78
00:04:04,600 --> 00:04:08,690
>> Okay, what's nice about this is just like we had in local compu

79
00:04:08,690 --> 00:04:12,940
context, we use the RS get info and get more info arguments to

80
00:04:12,940 --> 00:04:16,150
get some net data out of our files and look at the first few rows.

81
00:04:16,150 --> 00:04:18,450
The same exact function will work in the spark context.

82
00:04:18,450 --> 00:04:21,400
No need to change our code,add any spark variables to it.

83
00:04:21,400 --> 00:04:24,672
>> How do you know whether your compute context is currently set to?

84
00:04:24,672 --> 00:04:29,040
>> So there's a variable or a function called rxGetComputeContext.

85
00:04:29,040 --> 00:04:32,390
So I'm gonna go scroll up a little bit further and

86
00:04:32,390 --> 00:04:34,680
I will show you where we define our Spark compute context.

87
00:04:34,680 --> 00:04:36,820
We said rxSetComputeContext.

88
00:04:36,820 --> 00:04:38,470
>> Yeah. >> And the value we gave it was

89
00:04:38,470 --> 00:04:42,130
spark_cc which was the RxSpark object or

90
00:04:42,130 --> 00:04:44,770
environment available to define this Spark Compute Context.

91
00:04:44,770 --> 00:04:48,430
If you change the set to a get, so rxGetComputeContext and

92
00:04:48,430 --> 00:04:51,410
give it no arguments, it will tell you your current environment.

93
00:04:51,410 --> 00:04:52,960
>> Because you run that so we can see.

94
00:04:52,960 --> 00:04:55,170
>> So we can run that, yes, rxGetComputeContext,

95
00:04:55,170 --> 00:04:58,030
I'm a little bit lazy, so I'm just gonna use tab to complete it.

96
00:04:58,030 --> 00:04:58,912
That's control enter and

97
00:04:58,912 --> 00:05:02,580
it tells you, we currently running in dupe spark local object.

98
00:05:02,580 --> 00:05:06,317
It's kinda mouthful and suddenly confusing that his local in there,

99
00:05:06,317 --> 00:05:07,844
but it's actually a spark.

100
00:05:07,844 --> 00:05:08,640
>> Mm-hm.

101
00:05:08,640 --> 00:05:13,480
>> So create a spark application for each iteration of RxJob.

102
00:05:13,480 --> 00:05:15,540
It will tell you some of the primaries that we chose.

103
00:05:15,540 --> 00:05:18,390
So, we did choose executive memory at 20 gigabytes.

104
00:05:18,390 --> 00:05:21,740
Executive overhead memory at 20 gigabytes as well.

105
00:05:21,740 --> 00:05:25,900
We add some extra parameters and some console output as well.

106
00:05:27,490 --> 00:05:29,917
So you can always change between compute context,

107
00:05:29,917 --> 00:05:31,730
you'll see an example of that later.

108
00:05:31,730 --> 00:05:33,440
I want to something locally.

109
00:05:33,440 --> 00:05:36,030
I can keep this compute context variable but

110
00:05:36,030 --> 00:05:40,300
change back to local compute context to something locally for a second,

111
00:05:40,300 --> 00:05:43,043
get some results calculated spawn datasets.

112
00:05:43,043 --> 00:05:45,829
And then once I"m comfortable with that function, that operation,

113
00:05:45,829 --> 00:05:48,564
I can change my compute contacts back to spark, compute contacts and

114
00:05:48,564 --> 00:05:49,970
deploy at SQL.

115
00:05:49,970 --> 00:05:55,320
>> Okay so this sorta the idea that our development environment and

116
00:05:55,320 --> 00:05:59,070
our production environment could really be just one in the same.

117
00:05:59,070 --> 00:06:02,970
Simply by changing the compute context of local compute context,

118
00:06:02,970 --> 00:06:06,740
we can run some code in a local environment and

119
00:06:06,740 --> 00:06:10,040
then when we're comfortable with the code running we can then deploy it

120
00:06:10,040 --> 00:06:13,460
on a spark cluster by just changing the compute context back to.

121
00:06:13,460 --> 00:06:14,795
>> Okay. So it makes your life a lot easier,

122
00:06:14,795 --> 00:06:17,231
you don't have to juggle around two different environments to do your

123
00:06:17,231 --> 00:06:18,899
development and your production deployment.

124
00:06:21,530 --> 00:06:23,740
>> Okay so I'm gonna scroll down here again.

125
00:06:23,740 --> 00:06:26,080
We saw how you get the meta data from your object,

126
00:06:26,080 --> 00:06:28,990
so R get info would work will tell you data.

127
00:06:28,990 --> 00:06:32,130
As I mentioned the taxi_xdf will

128
00:06:32,130 --> 00:06:34,450
contain all data as well as meta data.

129
00:06:34,450 --> 00:06:37,470
So it will require a full pass through data to get the summary

130
00:06:37,470 --> 00:06:39,720
statistics, will do it kind of automatically.

131
00:06:39,720 --> 00:06:43,400
So I'll continue on, I'll show you how you could do summary as well.

132
00:06:43,400 --> 00:06:48,050
So as we had done with local file system and local object,

133
00:06:48,050 --> 00:06:51,910
rxSummary will give you the summary sys takes for a specific object.

134
00:06:51,910 --> 00:06:54,990
I'll go ahead and run this actually from scratch, so

135
00:06:54,990 --> 00:06:55,989
you can just see how it goes.

136
00:06:57,260 --> 00:06:59,990
Okay, and I guess this is a good point for

137
00:06:59,990 --> 00:07:04,567
us to mention that you had to run some of the code ahead because it

138
00:07:04,567 --> 00:07:08,110
does take awhile to trunk through the data.

139
00:07:08,110 --> 00:07:10,920
So all of the results that you were showing us so

140
00:07:10,920 --> 00:07:13,370
far were results that where generated ahead of time.

141
00:07:13,370 --> 00:07:15,410
But now you're actually running the code.

142
00:07:15,410 --> 00:07:17,300
The compute context is set to Spark.

143
00:07:17,300 --> 00:07:19,280
And we're just running an example of rxSummary,

144
00:07:19,280 --> 00:07:23,670
pointing to one year worth of data sitting on HTFS.

145
00:07:23,670 --> 00:07:25,630
And then we're gonna run rxSummary on that data right now.

146
00:07:25,630 --> 00:07:28,310
>> Precisely, but one other thing I should mention is,

147
00:07:28,310 --> 00:07:31,500
we mentioned different consideration between a local file system and

148
00:07:31,500 --> 00:07:32,960
HTFS file system.

149
00:07:32,960 --> 00:07:35,160
One very important distinction is that, when you look for

150
00:07:35,160 --> 00:07:37,900
local objects, it's pretty easy to overwrite objects.

151
00:07:37,900 --> 00:07:41,560
I actually have a data set you can easily add new columns to it,

152
00:07:41,560 --> 00:07:44,550
or do some filters and selection and

153
00:07:44,550 --> 00:07:46,362
you'll be able to compute that pretty easily.

154
00:07:46,362 --> 00:07:50,600
My RTF file system is a lot more stubborn with overriding files.

155
00:07:50,600 --> 00:07:53,920
So if you wanna add new columns or if you want to sub the data,

156
00:07:53,920 --> 00:07:57,578
it's much better practice to start a new directory and save it there.

157
00:07:57,578 --> 00:08:00,100
So the other reason why I kind of run some of this codes earlier was

158
00:08:00,100 --> 00:08:03,200
so that I don't want to create multiple direction at same data.

159
00:08:03,200 --> 00:08:07,450
I had them already computed so that data ready for us to let us.

160
00:08:07,450 --> 00:08:08,010
>> Okay.

161
00:08:08,010 --> 00:08:11,640
>> As you can see here, it started off a spark application.

162
00:08:11,640 --> 00:08:16,680
So the invocation of that function created a Spark job that says

163
00:08:16,680 --> 00:08:20,010
master HPA process started on today's date.

164
00:08:20,010 --> 00:08:25,390
It will show us how many executives are created for this job.

165
00:08:25,390 --> 00:08:28,640
So, we can see it created those executors, it runs through them,

166
00:08:28,640 --> 00:08:30,370
and sends us the results.

167
00:08:30,370 --> 00:08:32,820
And then there's a repartition of the data and

168
00:08:32,820 --> 00:08:35,390
then finally a reduction of the data, all right?

169
00:08:35,390 --> 00:08:38,668
So, it's a very similar concept to MapReduce where you split your

170
00:08:38,668 --> 00:08:42,680
datasets into multiple small blocks, summarize them and reduce them.

171
00:08:42,680 --> 00:08:44,060
Similar things happening in Spark as well.

172
00:08:45,120 --> 00:08:47,618
And so you can see it ran through all that code.

173
00:08:47,618 --> 00:08:51,819
It took about 71 seconds, so over a minute to summarize

174
00:08:51,819 --> 00:08:56,120
about 115 million records, or 20 million records.

175
00:08:56,120 --> 00:08:59,670
>> Okay, that's pretty impressive.

176
00:08:59,670 --> 00:09:02,060
>> And the nice thing about HTCI clusters is that you can scale them

177
00:09:02,060 --> 00:09:02,940
out even further.

178
00:09:02,940 --> 00:09:05,810
So, if you want to add more worker nodes you could so,

179
00:09:05,810 --> 00:09:08,540
and that way the computation will take place not on 6

180
00:09:08,540 --> 00:09:10,030
broken nodes but say on 10 or 12.

181
00:09:10,030 --> 00:09:13,280
So, depending on your problem you may want to add a bunch of worker

182
00:09:13,280 --> 00:09:15,210
nodes for a specific algorithm and

183
00:09:15,210 --> 00:09:18,300
then shrink it down once you've completed your analysis.

184
00:09:18,300 --> 00:09:18,800
>> Okay.

