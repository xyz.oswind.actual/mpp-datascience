0
00:00:00,780 --> 00:00:04,880
I'm here with Ali Zaidi who is a data scientist at Microsoft.

1
00:00:04,880 --> 00:00:08,470
And he's here to tell us more about

2
00:00:08,470 --> 00:00:12,710
running Microsoft r-Server in the Spark compute context.

3
00:00:12,710 --> 00:00:13,860
So, welcome Ali.

4
00:00:13,860 --> 00:00:15,100
>> Thank you Seth, it's great to be here.

5
00:00:16,190 --> 00:00:18,870
>> So what can you tell us about Spark?

6
00:00:18,870 --> 00:00:22,420
How do we now that we have, I guess I'll start it this way.

7
00:00:22,420 --> 00:00:27,590
How do we know that we have a Spark problem?

8
00:00:27,590 --> 00:00:30,190
Or how do we know that we should move into Spark.

9
00:00:30,190 --> 00:00:31,830
>> That's a very good question.

10
00:00:31,830 --> 00:00:35,740
So often you consider yourself having a big data problem.

11
00:00:35,740 --> 00:00:37,330
If you don't think it will fit in one computer.

12
00:00:37,330 --> 00:00:41,260
So you wanna use multiple computers to spread your computation across

13
00:00:41,260 --> 00:00:43,850
those computers in that cluster.

14
00:00:43,850 --> 00:00:44,810
That's the way thinking about it.

15
00:00:44,810 --> 00:00:47,960
Other way thinking about it is you wanna do more functions and

16
00:00:47,960 --> 00:00:51,410
more jobs in parallel rather than waiting for one job to finish to try

17
00:00:51,410 --> 00:00:56,390
a new job, we can do multiple jobs all at once, simultaneously.

18
00:00:56,390 --> 00:01:00,520
>> So in RevoScaleR, we have the XDF file format and

19
00:01:00,520 --> 00:01:03,420
it turned out to be pretty darn efficient.

20
00:01:03,420 --> 00:01:08,260
With an XDF file, we can process data as big as we have,

21
00:01:08,260 --> 00:01:11,070
you know, disk space to accommodate the data.

22
00:01:11,070 --> 00:01:14,390
And the processing is gonna be pretty quick.

23
00:01:14,390 --> 00:01:19,900
We turned CSV files that were 2 gigabytes in size each,

24
00:01:19,900 --> 00:01:21,530
one for each month.

25
00:01:21,530 --> 00:01:25,440
So a whole year of data, it's gonna have anywhere between say,

26
00:01:25,440 --> 00:01:30,720
20 to 24 gigabytes of a file sized for the CSVs.

27
00:01:30,720 --> 00:01:34,925
But we can turn that into an XDF file that's about 4 gigs in size.

28
00:01:34,925 --> 00:01:35,840
>> Mm-hm.

29
00:01:35,840 --> 00:01:39,770
So we have basically efficient use of space and

30
00:01:39,770 --> 00:01:44,050
we have RevoScaleR's parallel algorithms that are gonna chunk

31
00:01:44,050 --> 00:01:45,580
through that data pretty quickly.

32
00:01:46,740 --> 00:01:50,410
When do we know that our data is maybe like big enough

33
00:01:50,410 --> 00:01:54,700
that even an XDF file with its highly efficient

34
00:01:54,700 --> 00:01:59,070
distributed data format is not gonna cut the mustard?

35
00:01:59,070 --> 00:02:00,180
>> That's a good question.

36
00:02:00,180 --> 00:02:04,370
There's no sort of golden number at which you reach that number now you

37
00:02:04,370 --> 00:02:07,220
should use HTFS or a Spark or Hadoop cluster.

38
00:02:07,220 --> 00:02:10,000
But typically if you're working with data that you're storing not just

39
00:02:10,000 --> 00:02:11,240
for one year but for multiple years.

40
00:02:11,240 --> 00:02:13,190
So no longer is it 20 gigabytes, but

41
00:02:13,190 --> 00:02:15,738
it could be 100 gigabytes close to a terabyte of range.

42
00:02:15,738 --> 00:02:18,380
So you're kinda using HTFS as sort of the data

43
00:02:18,380 --> 00:02:20,110
storage system like a data lake.

44
00:02:20,110 --> 00:02:22,100
And you wanna do analysis on that directly rather than moving

45
00:02:22,100 --> 00:02:23,190
that to local system or

46
00:02:23,190 --> 00:02:27,860
to a RDBMS and then do a computation you can do it directly in HTFS.

47
00:02:27,860 --> 00:02:29,340
That's kinda our goal here.

48
00:02:29,340 --> 00:02:33,440
>> When you think of data do you think more in terms of the actual

49
00:02:33,440 --> 00:02:35,010
physical size of the data?

50
00:02:35,010 --> 00:02:39,460
Or do you think more in terms of data that's really wide,

51
00:02:39,460 --> 00:02:40,690
has lots of columns.

52
00:02:41,690 --> 00:02:45,020
Like what is a good way to think about it?

53
00:02:45,020 --> 00:02:46,940
>> Big data comes in different shape and sizes.

54
00:02:46,940 --> 00:02:49,560
So I think one really interesting use case of big data

55
00:02:49,560 --> 00:02:51,590
is when you have a lot of features, a lot of different variables.

56
00:02:51,590 --> 00:02:53,200
So yeah. They're very, very wide or

57
00:02:53,200 --> 00:02:54,620
a fat data set.

58
00:02:54,620 --> 00:02:58,920
And you may not have terabytes in terms of the length but

59
00:02:58,920 --> 00:03:01,620
they're terabytes because they're so wide.

60
00:03:01,620 --> 00:03:04,400
So that's a good example of large data sets as well.

61
00:03:04,400 --> 00:03:07,210
But the nice thing about HTFS and Spark is that,

62
00:03:07,210 --> 00:03:10,630
they aren't designed to be set up for one type of use case.

63
00:03:10,630 --> 00:03:11,580
They are very general.

64
00:03:11,580 --> 00:03:13,430
They can be utilized for all kinda data problems.

65
00:03:13,430 --> 00:03:15,250
So it could be data that's very wide, very long,

66
00:03:15,250 --> 00:03:18,300
it could be data that comes in streams, rather contained by month,

67
00:03:18,300 --> 00:03:21,105
it could be coming out of the second or the minutes, so on and so forth.

68
00:03:21,105 --> 00:03:22,290
>> Mm-hm. >> In that case,

69
00:03:22,290 --> 00:03:25,610
finding a way to store it in local storage might not be very efficient.

70
00:03:25,610 --> 00:03:27,740
Your disk may, it may die.

71
00:03:27,740 --> 00:03:30,990
Your hard drive may not be working properly so you may wanna put

72
00:03:30,990 --> 00:03:33,500
it in a reputable system like HTFS and Azure Blob Storage.

73
00:03:33,500 --> 00:03:36,700
There are many different use cases of big data.

74
00:03:36,700 --> 00:03:39,330
It may not be just for wide or long.

75
00:03:39,330 --> 00:03:41,470
>> Yeah, so maybe in a nutshell,

76
00:03:41,470 --> 00:03:46,840
you can tell us sort of the history of R and Spark.

77
00:03:48,020 --> 00:03:50,430
>> Sure, so the history of R and Spark.

78
00:03:50,430 --> 00:03:53,580
So Spark is a project that started around 2011, 2012.

79
00:03:53,580 --> 00:03:58,090
It was a project started at the University of Berkley that

80
00:03:58,090 --> 00:04:02,690
a lab called AMPLab for automation machines at people.

81
00:04:02,690 --> 00:04:05,930
It's one of the labs in the pure sciences department.

82
00:04:05,930 --> 00:04:09,960
And it started off as a project to see if we could improve

83
00:04:09,960 --> 00:04:14,010
the efficiency of MapReduce or distributed computation for

84
00:04:14,010 --> 00:04:15,860
things that have a lot of iterations.

85
00:04:15,860 --> 00:04:18,590
Like algorithms or doing EDAs,

86
00:04:18,590 --> 00:04:22,650
exploratory data analysis, so on and so forth.

87
00:04:22,650 --> 00:04:25,720
It was written entirely in Scala, so which is a functional programming

88
00:04:25,720 --> 00:04:28,670
language based on a Java virtual machine type of system.

89
00:04:28,670 --> 00:04:31,900
And that was the initial API.

90
00:04:31,900 --> 00:04:33,980
Many data scientists including myself and

91
00:04:33,980 --> 00:04:37,050
probably you as well, Scala is not one of our first languages.

92
00:04:37,050 --> 00:04:39,060
So rather than learning a new language,

93
00:04:39,060 --> 00:04:42,555
the developers of Spark made other API's that you could use.

94
00:04:42,555 --> 00:04:43,820
>> Mm-hm. >> So one of the most common ones

95
00:04:43,820 --> 00:04:45,678
was pyspark for the Python API.

96
00:04:45,678 --> 00:04:47,864
Which is very nice.

97
00:04:47,864 --> 00:04:52,683
R's API calls spark card and it come until version 1.31.4,

98
00:04:52,683 --> 00:04:55,449
resumed about 2013, 2012.

99
00:04:55,449 --> 00:04:57,622
>> Mm-hm.

100
00:04:57,622 --> 00:04:58,820
>> So that's were the R API first Spark came around.

101
00:05:00,370 --> 00:05:04,880
We started developing the MRS, API for Spark the last year.

102
00:05:05,910 --> 00:05:06,850
>> Okay.

103
00:05:06,850 --> 00:05:11,080
And so with the MRS API, is the idea that it's gonna

104
00:05:11,080 --> 00:05:15,910
completely replace the SparkR API or is it gonna be say,

105
00:05:15,910 --> 00:05:20,070
complementary to functionality that is already in SparkR?

106
00:05:20,070 --> 00:05:22,280
>> Yeah, I think it's gonna be more complimentary and

107
00:05:22,280 --> 00:05:25,260
sort of extension of what the SparkR API provides.

108
00:05:25,260 --> 00:05:30,080
The SparkR API will never be as performant or

109
00:05:30,080 --> 00:05:32,470
as, actually let's just not say performant.

110
00:05:32,470 --> 00:05:36,390
So, it wouldn't be as fully developed as a Scala or

111
00:05:36,390 --> 00:05:38,370
Java API for RSpark.

112
00:05:38,370 --> 00:05:41,320
Whereas MRS APIs tended to give you all the functionality that

113
00:05:41,320 --> 00:05:44,930
you'd want for big data through MRS, but in a Spark computer context.

114
00:05:44,930 --> 00:05:45,902
>> Okay.

115
00:05:45,902 --> 00:05:49,220
>> So being the extension of it, you provide lot of mauling functions and

116
00:05:49,220 --> 00:05:52,390
you can realized directly on your Spark clusters.

117
00:05:52,390 --> 00:05:53,170
>> Okay.

118
00:05:53,170 --> 00:05:56,330
So you just use the word computes context which in

119
00:05:56,330 --> 00:06:00,085
the last demo we were running a SQL server.

120
00:06:00,085 --> 00:06:04,955
And we were looking at how we could change our computer context to SQL

121
00:06:04,955 --> 00:06:09,975
server and then build a linear model and the SQL server computer context.

122
00:06:09,975 --> 00:06:12,977
And we mentioned at that time that

123
00:06:12,977 --> 00:06:15,787
a compute context was not unique to SQL Server.

124
00:06:15,787 --> 00:06:19,247
So now we're gonna talk about a Spark compute context.

125
00:06:19,247 --> 00:06:23,857
Now is it so when we set the compute context to Spark,

126
00:06:25,217 --> 00:06:30,547
is it basically our way of telling R that this computation

127
00:06:30,547 --> 00:06:35,180
is gonna be piggy backing on the Spark infrastructure?

128
00:06:35,180 --> 00:06:37,220
Is that a fair description of it?

129
00:06:37,220 --> 00:06:38,320
>> Yeah, precisely right.

130
00:06:38,320 --> 00:06:40,078
What we're doing with the compute context,

131
00:06:40,078 --> 00:06:41,889
we're defining an environmental variable.

132
00:06:41,889 --> 00:06:45,196
Which will tell MRS and your R session that on this computation

133
00:06:45,196 --> 00:06:48,186
should not take place locally in your local computer but

134
00:06:48,186 --> 00:06:49,810
in different environment.

135
00:06:49,810 --> 00:06:52,790
Just SparkR SQL server box.

136
00:06:52,790 --> 00:06:53,450
>> Okay.

137
00:06:53,450 --> 00:06:54,660
>> That's what happening.

138
00:06:54,660 --> 00:06:57,729
Around what happens is all the end-rest functions could all that

139
00:06:57,729 --> 00:07:00,461
converted into code that can be run in Spark and the results

140
00:07:00,461 --> 00:07:04,000
are summarized and being brought back to your local R session.

141
00:07:04,000 --> 00:07:07,040
>> So what are some of the things that are different about this Spark

142
00:07:07,040 --> 00:07:11,630
compute context that maybe is not the case in a local compute context,

143
00:07:11,630 --> 00:07:14,440
or even in a SQL server compute context?

144
00:07:14,440 --> 00:07:17,765
>> The biggest difference is your data is no longer a contiguous block

145
00:07:17,765 --> 00:07:19,170
on local file system.

146
00:07:19,170 --> 00:07:21,351
It's now spread across your cluster.

147
00:07:21,351 --> 00:07:22,775
So in HTFS file system,

148
00:07:22,775 --> 00:07:26,560
you have multiple data nodes that contain proportional data.

149
00:07:26,560 --> 00:07:28,050
The data has been charted.

150
00:07:28,050 --> 00:07:29,450
That's not a curse world.

151
00:07:29,450 --> 00:07:30,654
It's just partition,

152
00:07:30,654 --> 00:07:34,399
a data set into different blocks of data center cluster.

153
00:07:34,399 --> 00:07:35,020
>> Mm-hm. >> So

154
00:07:35,020 --> 00:07:37,590
what you need to do is you have to make sure that all your algorithms

155
00:07:37,590 --> 00:07:40,268
are able to take the data as a chunk by chunk basis.

156
00:07:40,268 --> 00:07:43,990
It's going to be the result on it and then summarize it for you.

157
00:07:43,990 --> 00:07:46,742
So similar to our XDFs, as you mentioned are blocked as well,

158
00:07:46,742 --> 00:07:48,040
chunk-wise block.

159
00:07:48,040 --> 00:07:51,430
So similar to that, but now it's no longer one continuous file system

160
00:07:51,430 --> 00:07:54,860
but multiple computers that have to coordinate communication together.

161
00:07:54,860 --> 00:07:59,330
>> Okay, so the overall architecture is not very different in the sense

162
00:07:59,330 --> 00:08:03,190
that the MRS functions are already parallelized.

163
00:08:03,190 --> 00:08:06,350
It's just that the way that implementation is gonna happen

164
00:08:06,350 --> 00:08:09,390
in Spark is gonna be different from the way that it would happen in

165
00:08:09,390 --> 00:08:11,425
a local compute context, for example.

166
00:08:11,425 --> 00:08:15,040
>> Mm-hm, exactly, there'll be more communication between the computers.

167
00:08:15,040 --> 00:08:17,052
So there'll be some network communication now as well.

168
00:08:17,052 --> 00:08:20,158
You gotta factor into our performance into our analysis.

169
00:08:20,158 --> 00:08:22,770
And the algorithm has to make sure that it

170
00:08:22,770 --> 00:08:24,250
communicate across all the nodes.

171
00:08:24,250 --> 00:08:28,390
>> Okay, now in the context of SQL Server we talked about how

172
00:08:28,390 --> 00:08:32,830
one of the biggest advantages of running things in database was that

173
00:08:32,830 --> 00:08:36,820
we take the computation to the data not the data to the computation.

174
00:08:36,820 --> 00:08:39,800
Is that also true in the case of a Spark compute context?

175
00:08:39,800 --> 00:08:43,440
>> Precisely, the way it works is your data is stored in HTFS which is

176
00:08:43,440 --> 00:08:47,840
the file system for Hadoop and for Spark running on Hadoop clusters.

177
00:08:47,840 --> 00:08:52,160
And what happens is those blocks of data in HTFS are brought

178
00:08:52,160 --> 00:08:56,480
into the worker, into the compute nodes, and calculated there.

179
00:08:56,480 --> 00:08:57,460
There's no movable data.

180
00:08:57,460 --> 00:09:00,850
The code is brought to the worker nodes, computed, and

181
00:09:00,850 --> 00:09:02,150
then summarized.

182
00:09:02,150 --> 00:09:03,180
>> Okay, great.

183
00:09:03,180 --> 00:09:04,910
Can we see a demo?

184
00:09:04,910 --> 00:09:05,770
>> Yeah, let's go for it.

185
00:09:07,500 --> 00:09:12,220
So I'll show you here how to utilize a AT inside cluster.

186
00:09:12,220 --> 00:09:14,860
We're doing a scalable data analysis.

187
00:09:14,860 --> 00:09:15,498
And what you're

188
00:09:15,498 --> 00:09:18,290
gonna do is you're gonna create some pointers to our files.

189
00:09:18,290 --> 00:09:19,660
So I took this that you've been using.

190
00:09:19,660 --> 00:09:22,020
I actually took a hold of your data.

191
00:09:22,020 --> 00:09:25,320
Then, I start that in my AT inside cluster.

192
00:09:25,320 --> 00:09:30,012
So the data has been saved in a ATFS directory called User Regular Share.

193
00:09:30,012 --> 00:09:31,790
And then my alias [INAUDIBLE].

194
00:09:31,790 --> 00:09:36,040
And I will make a file path to that pointer to that file,

195
00:09:36,040 --> 00:09:39,910
that directory and it's gonna list the files in that directory.

196
00:09:39,910 --> 00:09:42,880
So you can see here we have 12 months of data from

197
00:09:42,880 --> 00:09:45,030
July 2015 until May of this year.

198
00:09:45,030 --> 00:09:49,780
>> Now a Spark cluster is a collection of specifically,

199
00:09:49,780 --> 00:09:51,970
Linux machines, is that correct?

200
00:09:51,970 --> 00:09:53,150
>> That is correct, yes.

201
00:09:53,150 --> 00:09:54,920
>> Okay, so you downloaded the data.

202
00:09:54,920 --> 00:09:58,020
Into the machine and

203
00:09:58,020 --> 00:10:04,820
then you use some kinda command line utility to send that over into HDFS.

204
00:10:04,820 --> 00:10:06,150
Is that.

205
00:10:06,150 --> 00:10:07,770
>> That's truly the other way.

206
00:10:07,770 --> 00:10:10,760
In this case the data is actually residing Azure blob storage.

207
00:10:10,760 --> 00:10:12,120
>> Okay. >> So I was directly able to import

208
00:10:12,120 --> 00:10:14,980
it to Azure Blob storage a bunch of utilities for

209
00:10:14,980 --> 00:10:17,420
loading data into Azure blob storage from the command line

210
00:10:17,420 --> 00:10:19,740
as well as third party GUI tools as well.

211
00:10:19,740 --> 00:10:21,380
>> Okay, all right.

212
00:10:21,380 --> 00:10:24,610
>> So that data is in Azure blob storage, which is HTFS compliant.

213
00:10:24,610 --> 00:10:28,430
So all the HTFS commands that we would use on HTFS cluster would

214
00:10:28,430 --> 00:10:30,110
also work here as well.

215
00:10:30,110 --> 00:10:36,770
>> And I've noticed that you're using R studio here as your GUI.

216
00:10:36,770 --> 00:10:40,530
Is there anything that you wanna mention about the R studio?

217
00:10:40,530 --> 00:10:43,490
>> Yes, the way our clusters set up here is we have

218
00:10:43,490 --> 00:10:46,295
a set of worker knowledge which is where our computation takes place.

219
00:10:46,295 --> 00:10:47,575
>> Uh-huh. >> In this case I have six worker

220
00:10:47,575 --> 00:10:50,990
nodes and then we have two head nodes from master nodes, that's

221
00:10:50,990 --> 00:10:56,650
where the initial communication of the program happens.

222
00:10:56,650 --> 00:10:59,390
So the communication of the worker knows to do some work.

223
00:10:59,390 --> 00:11:01,990
And then two of them just because you don't want to lose your head

224
00:11:01,990 --> 00:11:02,970
node, and lose all your work.

225
00:11:02,970 --> 00:11:04,500
So you have two head nodes.

226
00:11:04,500 --> 00:11:07,167
And because R is very memory intensive program and

227
00:11:07,167 --> 00:11:10,130
we're working directly in this environment.

228
00:11:10,130 --> 00:11:12,360
We also have another node, called the Edge Node.

229
00:11:12,360 --> 00:11:13,990
And that is where I'm currently working in.

230
00:11:13,990 --> 00:11:16,650
It's also a Linux VM in the cluster.

231
00:11:16,650 --> 00:11:20,150
And I've installed an IDE here just to make my life a bit easier for

232
00:11:20,150 --> 00:11:21,490
development.

233
00:11:21,490 --> 00:11:24,540
So R2 server is a very nice tool,

234
00:11:24,540 --> 00:11:27,460
it has a very nice IDE for developing R code.

235
00:11:27,460 --> 00:11:31,109
And you can install edge node, you can also use any sort of

236
00:11:31,109 --> 00:11:33,705
command-line tool like E max or VIM so.

237
00:11:33,705 --> 00:11:37,909
>> So the edge node is a node that is part of the cluster, but

238
00:11:37,909 --> 00:11:41,225
as such is gonna be within the same network.

239
00:11:41,225 --> 00:11:44,460
It's gonna be able to communicate with the head node.

240
00:11:44,460 --> 00:11:49,790
But it's for our purpose, its basically there,

241
00:11:49,790 --> 00:11:53,740
so that it could serve our studio through a port and you could just

242
00:11:53,740 --> 00:11:57,870
go into your browser and login to an Rstudio server session.

243
00:11:57,870 --> 00:11:59,650
>> Precisely. So we get the same sort of

244
00:11:59,650 --> 00:12:02,870
functionality that we get with desktop version of Rstudio.

245
00:12:02,870 --> 00:12:06,080
That's not pitted to our same cluster so we can develop our

246
00:12:06,080 --> 00:12:09,640
code here and have it run across the entire cluster.

247
00:12:09,640 --> 00:12:11,070
>> Okay, great.

248
00:12:11,070 --> 00:12:14,450
>> Okay so, as you could see here,

249
00:12:14,450 --> 00:12:18,140
I have a file path that are called txt_path, which

250
00:12:18,140 --> 00:12:23,240
points to a directory called taxi data inside of my HTFS file system.

251
00:12:23,240 --> 00:12:26,500
And these 12 files here they're all CSV files.

252
00:12:26,500 --> 00:12:32,640
I located our file path called taxi_xdf,

253
00:12:32,640 --> 00:12:37,180
and that will be pointing to also /user/RevoShare/alizaidi,

254
00:12:37,180 --> 00:12:39,810
but to a new directory called XDF taxi data.

255
00:12:39,810 --> 00:12:40,330
>> Okay. >> And

256
00:12:40,330 --> 00:12:42,970
my goal here is to import the data from

257
00:12:42,970 --> 00:12:45,870
my file system in a CSV into an XDF file.

258
00:12:45,870 --> 00:12:49,990
So, in the case of the XDF file when we working locally,

259
00:12:49,990 --> 00:12:54,410
we basically combine all of the CSVs into a single XDF.

260
00:12:54,410 --> 00:12:57,730
Do we have to do that here or can you just basically

261
00:12:57,730 --> 00:13:00,780
have individual CSVs that are treated as one?

262
00:13:00,780 --> 00:13:01,570
>> Precisely, yes.

263
00:13:01,570 --> 00:13:05,200
I think in the previous examples we were working locally use the append argument-

264
00:13:05,200 --> 00:13:05,560
>> Yeah.

265
00:13:05,560 --> 00:13:07,010
>> To append by rows.

266
00:13:07,010 --> 00:13:10,160
In this case we are gonna point directly to a directory.

267
00:13:10,160 --> 00:13:12,690
And because all these files have the same schema,

268
00:13:12,690 --> 00:13:14,540
the same number of rows, I'm sorry,

269
00:13:14,540 --> 00:13:17,460
same number of columns, you don't have to attend to it.

270
00:13:17,460 --> 00:13:20,390
It'll automatically know to read all them as individual blocks

271
00:13:20,390 --> 00:13:21,570
into the data set.

272
00:13:21,570 --> 00:13:22,070
>> Okay.

273
00:13:23,660 --> 00:13:27,270
>> Okay, so we've created our point list to our files.

274
00:13:27,270 --> 00:13:27,900
And so,

275
00:13:27,900 --> 00:13:30,670
we also added a pointer to our out file that we wanna save our data to.

276
00:13:31,810 --> 00:13:37,080
Our next goal is to create a environment variable for

277
00:13:37,080 --> 00:13:41,605
defending HDFS as a file system and for our Spark few context.

278
00:13:41,605 --> 00:13:42,570
>> Uh-huh.

279
00:13:42,570 --> 00:13:45,830
>> So we're gonna create a RX HDFS file system object which will

280
00:13:45,830 --> 00:13:50,740
tell us MRS and R that the points that we define earlier are not for

281
00:13:50,740 --> 00:13:52,170
local file systems.

282
00:13:52,170 --> 00:13:55,020
Before HDFS file system.

283
00:13:55,020 --> 00:13:57,790
That's this object here HDFS capital F capital S.

284
00:13:57,790 --> 00:14:01,960
In these two arguments, one is the host name which is where the cluster

285
00:14:01,960 --> 00:14:05,700
is hosted and it was support to access to HDFS file system.

286
00:14:05,700 --> 00:14:06,200
>> Okay. >> And

287
00:14:06,200 --> 00:14:09,320
Azure Blob storage is a default that is our default for

288
00:14:09,320 --> 00:14:12,400
the name known and zero for the port.

289
00:14:12,400 --> 00:14:15,890
>> So in the case of a local file system

290
00:14:15,890 --> 00:14:20,485
when we work locally we don't have to do these things because they're

291
00:14:20,485 --> 00:14:22,720
kinda implicitly done for us.

292
00:14:22,720 --> 00:14:25,970
But in the case of running things in a different compute context we have

293
00:14:25,970 --> 00:14:27,470
to be more explicit.

294
00:14:27,470 --> 00:14:32,190
Point to where the data is setting and set the compute

295
00:14:32,190 --> 00:14:37,190
context to be the context within which the computation is happening.

296
00:14:37,190 --> 00:14:37,740
>> Precisely,

297
00:14:37,740 --> 00:14:40,820
here we're defining the file system where most data resides.

298
00:14:40,820 --> 00:14:42,255
And in local file system,

299
00:14:42,255 --> 00:14:45,916
actually the file system default argument is rs data file system.

300
00:14:45,916 --> 00:14:46,858
>> Mm-hm. >> By default,

301
00:14:46,858 --> 00:14:48,350
they will point to a local file system.

302
00:14:48,350 --> 00:14:50,275
In our case we wanna point to HDFS file system.

303
00:14:50,275 --> 00:14:52,777
Which is actually right.

304
00:14:52,777 --> 00:14:56,596
We here are the more explicit about defining where the data resides and

305
00:14:56,596 --> 00:14:58,805
where the computation will take place.

306
00:14:58,805 --> 00:15:01,357
So that's our pointers to the data set,

307
00:15:01,357 --> 00:15:03,909
this lets MRS know that data taxi_txt,

308
00:15:03,909 --> 00:15:09,290
dataset is actually txt file, where is taxi_xdf will be an XDF file.

309
00:15:09,290 --> 00:15:10,760
Okay.

310
00:15:10,760 --> 00:15:13,410
>> Our next job is to create the Spark environment variable the Spark

311
00:15:13,410 --> 00:15:14,750
compute context.

312
00:15:14,750 --> 00:15:19,240
So we use the rx Spark it's analogous to the rx in SQL server

313
00:15:19,240 --> 00:15:22,260
object you created in the SQL server demo.

314
00:15:22,260 --> 00:15:25,240
Again, we'll use a name node and port from before.

315
00:15:25,240 --> 00:15:27,910
And then we'll define some other arguments for

316
00:15:27,910 --> 00:15:30,230
how Spark will do the computation.

317
00:15:30,230 --> 00:15:32,990
So at each job Spark will create some executors which will allow us

318
00:15:32,990 --> 00:15:37,840
to do computing on kinda the unit of compute in a Spark environment.

319
00:15:37,840 --> 00:15:41,160
And we'll define their memory size and overhead so

320
00:15:41,160 --> 00:15:43,410
much they reserved for the R session.

321
00:15:43,410 --> 00:15:45,400
And in this case I'm working with d 14 nodes so

322
00:15:45,400 --> 00:15:49,640
each node has about 112 gigs of RAM they're pretty beefy nodes.

323
00:15:49,640 --> 00:15:51,780
And I will say each executor should have 20 gigs of RAM.

324
00:15:52,900 --> 00:15:57,715
>> Okay, so it looks like we have some options as far as how

325
00:15:57,715 --> 00:16:02,336
we wanna set the limits to the computation and such.

326
00:16:02,336 --> 00:16:03,560
>> Mm-hm, exactly.

327
00:16:03,560 --> 00:16:07,530
Default values are configured to be the ultimate values on the default

328
00:16:07,530 --> 00:16:09,744
cluster configuration on the HTM side.

329
00:16:09,744 --> 00:16:11,710
In this case, I just played around with it and

330
00:16:11,710 --> 00:16:13,150
tried these values as well.

331
00:16:13,150 --> 00:16:13,650
>> Okay.

