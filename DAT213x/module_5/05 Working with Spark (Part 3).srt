0
00:00:02,370 --> 00:00:05,060
Okay. So what else you wanna show us?

1
00:00:05,060 --> 00:00:07,970
>> All right, so let's see how we can create new column and

2
00:00:07,970 --> 00:00:10,070
then make some crafts out of it, okay?

3
00:00:10,070 --> 00:00:13,500
So the one for consideration in working with

4
00:00:13,500 --> 00:00:17,930
HTFS data is that it is very stubborn about writing its files.

5
00:00:17,930 --> 00:00:19,760
>> Uh-huh. >> So rather than doing overwrite =

6
00:00:19,760 --> 00:00:22,028
show argument as we did in the previous examples,

7
00:00:22,028 --> 00:00:24,282
you have to create a hole in your XTF file.

8
00:00:24,282 --> 00:00:25,470
So overwrite=true,

9
00:00:25,470 --> 00:00:28,640
it doesn't haven't any effect in the spark compute context?

10
00:00:28,640 --> 00:00:29,840
>> Very rarely.

11
00:00:29,840 --> 00:00:31,530
>> Okay. >> [LAUGH] It's generally better

12
00:00:31,530 --> 00:00:33,970
practice to work within a new XTF D object.

13
00:00:33,970 --> 00:00:35,390
>> Okay. >> Okay, so,

14
00:00:35,390 --> 00:00:38,220
we basically create a new directory.

15
00:00:38,220 --> 00:00:41,600
And we dump the data

16
00:00:41,600 --> 00:00:44,080
at the transform data into the new directory.

17
00:00:44,080 --> 00:00:45,080
>> Precisely.

18
00:00:45,080 --> 00:00:47,250
We could do it on the go transformation if all you wanna

19
00:00:47,250 --> 00:00:50,130
do is add it for a visualization or for a model.

20
00:00:50,130 --> 00:00:52,050
We could do it on the go that's totally fine.

21
00:00:52,050 --> 00:00:54,970
In this case we gonna keep this column for later on.

22
00:00:54,970 --> 00:00:58,470
So you file with a new column counter percent.

23
00:00:59,620 --> 00:01:03,690
>> For under fly transformation, I'm guessing that,

24
00:01:03,690 --> 00:01:09,110
in a sense we're gonna be doing a lot of writing, it

25
00:01:09,110 --> 00:01:14,140
could be -- Far more beneficial to us to do on the fly transformations.

26
00:01:15,570 --> 00:01:18,370
Even if we have to do it several different times.

27
00:01:18,370 --> 00:01:21,070
If we're going to use the same column multiple times.

28
00:01:21,070 --> 00:01:23,980
Is that a fair assessment or is it just?

29
00:01:25,590 --> 00:01:27,100
>> It's relatively fair.

30
00:01:27,100 --> 00:01:30,280
I think if you're gonna use same column multiple times,

31
00:01:30,280 --> 00:01:33,000
you might wanna consider adding it do your XDF data objects.

32
00:01:33,000 --> 00:01:37,460
And the reason why is that you might end up in a case where you're

33
00:01:37,460 --> 00:01:40,650
adding a column, but you're using it in a very iterative sense.

34
00:01:40,650 --> 00:01:42,950
The algorithm is not gonna be a single pass-through algorithm.

35
00:01:42,950 --> 00:01:44,890
It would be a multiple pass-through algorithm.

36
00:01:44,890 --> 00:01:47,516
In that case, at each pass, when we create that column,

37
00:01:47,516 --> 00:01:49,176
that could become kinda annoying.

38
00:01:49,176 --> 00:01:50,140
>> Okay.

39
00:01:50,140 --> 00:01:53,920
>> By having it persistent in the dataset, No idea available to you.

40
00:01:53,920 --> 00:01:56,290
So it won't require create new accomplish time.

41
00:01:56,290 --> 00:02:02,020
>> Okay, is there a good way to maybe kind of guess which

42
00:02:02,020 --> 00:02:04,800
algorithms are the one's that are gonna make it to a single pass and

43
00:02:04,800 --> 00:02:07,780
which one's are probably gonna need multiple alterations.

44
00:02:07,780 --> 00:02:09,830
>> Yeah I think the machine learning algorithms,

45
00:02:09,830 --> 00:02:12,450
specifically the tree algorithm the ensemble tree algorithms all

46
00:02:12,450 --> 00:02:13,858
require multiple pass to data.

47
00:02:13,858 --> 00:02:17,036
Where as that only requires calculating averages or

48
00:02:17,036 --> 00:02:18,945
summaries using Apache data.

49
00:02:18,945 --> 00:02:19,720
>> Okay.

50
00:02:19,720 --> 00:02:22,970
>> We are trying to MRS the [INAUDIBLE] as clever as possible

51
00:02:22,970 --> 00:02:24,950
by minimizing the number of reads through data.

52
00:02:24,950 --> 00:02:27,620
So, you must know a more innovated algorithms

53
00:02:27,620 --> 00:02:29,710
have a minimum number of reads.

54
00:02:29,710 --> 00:02:32,470
But my rule of thumb is, if I do any sort of machine

55
00:02:32,470 --> 00:02:36,050
learning algorithms that required trees I will present the data,

56
00:02:36,050 --> 00:02:39,680
I will do on the fly transformation, do some persistent transformation.

57
00:02:39,680 --> 00:02:42,410
Whereas if I wanna do an average or a quantile or

58
00:02:42,410 --> 00:02:44,660
a histogram I'll do it on the fly.

59
00:02:44,660 --> 00:02:45,970
>> Okay that's very useful.

60
00:02:45,970 --> 00:02:46,490
>> Thank you.

61
00:02:46,490 --> 00:02:50,300
>> All right, so you're gonna add a new column called tip percent.

62
00:02:50,300 --> 00:02:53,680
So I'll create a new object here called xdf taxi tips.

63
00:02:53,680 --> 00:02:58,170
Gonna be same directory as before, but now it's called xdf taxi tips.

64
00:02:58,170 --> 00:03:01,650
And then the mutate operation is similar to what we did in the past.

65
00:03:01,650 --> 00:03:04,200
It's data step and then the transforms.

66
00:03:04,200 --> 00:03:07,870
Our transform will take a list with a named argument for

67
00:03:07,870 --> 00:03:11,120
tip percent, and it'll create a value tip amount and

68
00:03:11,120 --> 00:03:13,333
then a fare amount whenever the fare amount is >0.

69
00:03:13,333 --> 00:03:14,837
Otherwise it'll be missing.

70
00:03:14,837 --> 00:03:15,364
>> Mm-hm.

71
00:03:15,364 --> 00:03:16,182
>> Okay.

72
00:03:17,667 --> 00:03:20,740
So let's see how long this one took.

73
00:03:22,800 --> 00:03:26,851
Again, it requires a spark job which requires repartitioning.

74
00:03:26,851 --> 00:03:29,850
This took about 3/2 to four minutes.

75
00:03:30,900 --> 00:03:32,290
We can visualize the results here.

76
00:03:34,300 --> 00:03:37,320
As well, as you can see here, our data has a very,

77
00:03:37,320 --> 00:03:42,456
very large right tail, it's a very, very large quality percent.

78
00:03:42,456 --> 00:03:47,309
This is actually in percentage, so anything >1 is actually 100% or

79
00:03:47,309 --> 00:03:52,460
1000%, so and so forth, so very, very ridiculous values.

80
00:03:52,460 --> 00:03:56,110
>> We could do here, a on-the-go server selection, just drag and

81
00:03:56,110 --> 00:03:58,565
filtering to a new data sets, saving it to disc.

82
00:03:58,565 --> 00:04:00,680
>> Uh-huh. >> You can do all on the fly,

83
00:04:00,680 --> 00:04:03,110
take that data set, test it on a descriptive data set.

84
00:04:03,110 --> 00:04:06,980
I'm gonna summarize 10%, but I'm gonna to filter it to just those

85
00:04:06,980 --> 00:04:11,450
values below 5, getting 500%, and they'll guys create a zero.

86
00:04:11,450 --> 00:04:13,090
So in the positive tip percent value,

87
00:04:13,090 --> 00:04:14,820
hopefully don't get negative tip that's kind of,

88
00:04:14,820 --> 00:04:15,875
mean to taxi drivers.

89
00:04:15,875 --> 00:04:18,718
>> [LAUGH] >> And so you if tip 0 and 5 and

90
00:04:18,718 --> 00:04:21,341
I'll give it 21 break points.

91
00:04:23,337 --> 00:04:24,150
See results here.

92
00:04:24,150 --> 00:04:27,040
>> It depends on the taxi driver, by the way.

93
00:04:27,040 --> 00:04:30,990
>> [LAUGH] The thing you driver, you might give a different tip value,

94
00:04:30,990 --> 00:04:33,770
but here it's between 0 and 100% tip.

95
00:04:33,770 --> 00:04:36,770
I actually filtered out all the 0% tips.

96
00:04:36,770 --> 00:04:39,330
There's probably a very large proportion of 0% tips.

97
00:04:39,330 --> 00:04:41,460
But you're looking at the positive value of 10%.

98
00:04:41,460 --> 00:04:44,160
There seems to be a very large peak around 20, 25%.

99
00:04:44,160 --> 00:04:50,630
Then it decreases on after that.

100
00:04:50,630 --> 00:04:54,530
Okay so that's a nice simple way of doing on the go transforms.

101
00:04:54,530 --> 00:04:57,140
Another example directly from the previous local

102
00:04:57,140 --> 00:04:59,390
example is doing cross tabulations.

103
00:04:59,390 --> 00:05:04,357
You can tabulate the year, month for pick up and for drop off and

104
00:05:04,357 --> 00:05:06,337
get the results as well.

105
00:05:09,110 --> 00:05:13,316
And so again if you're doing a simple transformation requiring one

106
00:05:13,316 --> 00:05:15,603
pass through data and a tabulation or

107
00:05:15,603 --> 00:05:19,163
a summary on the go transform is probably pretty useful.

108
00:05:21,974 --> 00:05:24,166
Okay.

109
00:05:24,166 --> 00:05:24,666
Ready.

110
00:05:27,950 --> 00:05:29,860
This show, this is, show me the results.

111
00:05:32,140 --> 00:05:33,470
Now there is. Okay.

112
00:05:33,470 --> 00:05:37,520
Here you see the results here, is the second half of 2015 and

113
00:05:37,520 --> 00:05:39,770
the first half 2016.

114
00:05:39,770 --> 00:05:41,260
Ok?

115
00:05:41,260 --> 00:05:45,810
Ok, maybe we could skip ahead a little bit and talk about some other

116
00:05:46,960 --> 00:05:51,290
use cases for using spark especially in the context of modeling.

117
00:05:51,290 --> 00:05:52,930
>> Absolutely that's a very good idea.

118
00:05:52,930 --> 00:05:56,860
So I'm gonna dive into a couple of modeling samples.

119
00:05:56,860 --> 00:06:00,380
I have them saved here in a slide deck.

120
00:06:00,380 --> 00:06:03,390
So as you've seen before we have a bunch of modeling functions

121
00:06:03,390 --> 00:06:05,140
available to you in MRS.

122
00:06:05,140 --> 00:06:06,940
>> Yeah. >> It's available to you in a Spark

123
00:06:06,940 --> 00:06:08,040
context.

124
00:06:08,040 --> 00:06:11,990
If you take four of them and try to create a binary classification model

125
00:06:11,990 --> 00:06:13,860
try to estimate something that's a binary value.

126
00:06:15,000 --> 00:06:17,550
What I can do is I have to compose

127
00:06:17,550 --> 00:06:20,960
my operation into functions that I can reuse depending on the context.

128
00:06:20,960 --> 00:06:22,500
In this case,

129
00:06:22,500 --> 00:06:27,720
my function's gonna create a model which I'll specify as an argument.

130
00:06:27,720 --> 00:06:28,902
So i have a data set.

131
00:06:28,902 --> 00:06:32,382
I'll have a [INAUDIBLE] which i wanna model in this case the binary

132
00:06:32,382 --> 00:06:33,746
classification model.

133
00:06:33,746 --> 00:06:35,713
And then i'll pick the specific model that i wanna model. Right.

134
00:06:35,713 --> 00:06:38,573
The default value [INAUDIBLE] can be

135
00:06:38,573 --> 00:06:39,670
[INAUDIBLE].

136
00:06:39,670 --> 00:06:46,130
And i can change that [INAUDIBLE] >> So you took basically functions

137
00:06:46,130 --> 00:06:50,620
that are already in [INAUDIBLE] you Grow that wrapper function around

138
00:06:50,620 --> 00:06:55,050
them so that you can call those functions in a more flexible way.

139
00:06:55,050 --> 00:06:55,550
>> Precisely.

140
00:06:57,720 --> 00:06:59,290
All right. So now I added it before we

141
00:06:59,290 --> 00:07:01,550
create a spark compute context.

142
00:07:01,550 --> 00:07:07,080
That requires cutting a variable set to the value from the RxSpark.

143
00:07:07,080 --> 00:07:08,610
And will gave it the name node and

144
00:07:08,610 --> 00:07:12,130
the port values before define the second or size.

145
00:07:12,130 --> 00:07:15,493
And is not the memory and we're not going to keep this application

146
00:07:15,493 --> 00:07:17,831
persistent or just keep it forcing will run.

147
00:07:19,248 --> 00:07:22,730
I do next is creating a list of four different models that want to use.

148
00:07:22,730 --> 00:07:26,250
We're gonna use the RX logit or I just take regression.

149
00:07:26,250 --> 00:07:30,320
rxDTree which is our decision tree algorithm, rxDForest for a random

150
00:07:30,320 --> 00:07:33,720
forest algorithm, and rxBTrees for our gradient boosted trees.

151
00:07:33,720 --> 00:07:36,250
These last two are sample algorithms,

152
00:07:36,250 --> 00:07:38,680
they estimate many trees at once.

153
00:07:38,680 --> 00:07:40,509
So they can often give very good performance.

154
00:07:42,024 --> 00:07:46,124
The next thing that's kinda unique to a parallel computing environment

155
00:07:46,124 --> 00:07:50,510
or a concurrent environment is the Rxexec argument of function.

156
00:07:50,510 --> 00:07:53,695
Rxexec is quite somewhere to the apply family and base are

157
00:07:53,695 --> 00:07:54,320
>> Uh-huh.

158
00:07:54,320 --> 00:07:57,440
>> we're apply a single function to the multiple arguments.

159
00:07:57,440 --> 00:07:58,740
And does it in the parallel function.

160
00:07:58,740 --> 00:08:01,920
So in this case we're gonna apply the estimate moral function which is

161
00:08:01,920 --> 00:08:04,180
my wrapper for doing modeling.

162
00:08:04,180 --> 00:08:07,108
And we apply to each element of the models list.

163
00:08:07,108 --> 00:08:07,927
>> Okay.

164
00:08:07,927 --> 00:08:13,970
>> So, maybe since RX exact is a function that is new to this course.

165
00:08:13,970 --> 00:08:15,500
Maybe you could talk about first of all,

166
00:08:15,500 --> 00:08:19,980
what are exactly look like if we use it in a local computer context.

167
00:08:19,980 --> 00:08:20,810
>> Okay, absolutely.

168
00:08:20,810 --> 00:08:24,010
So, (INAUDIBLE) context and locals equation compute context.

169
00:08:24,010 --> 00:08:27,700
What will happen is, it will indurate the first model which is

170
00:08:27,700 --> 00:08:32,110
are (INAUDIBLE) Estimate the value, and save that value and then go to

171
00:08:32,110 --> 00:08:35,250
the next one, estimate the model, save the value, so on and so forth.

172
00:08:35,250 --> 00:08:37,660
>> Okay, so it'll be just like using lapply?

173
00:08:37,660 --> 00:08:39,910
>> Using lapply, precisely.

174
00:08:39,910 --> 00:08:42,720
If she wanna use an RS local parallel computer context,

175
00:08:42,720 --> 00:08:45,530
that's also a local computer context but it will paralyze

176
00:08:45,530 --> 00:08:48,690
the operation across all the cores of your machine.

177
00:08:48,690 --> 00:08:52,480
In my case I think I have 8 cores on this computer.

178
00:08:52,480 --> 00:08:54,355
They're hyperthreaded, so actually 16 cores.

179
00:08:54,355 --> 00:08:55,440
>> Mm-hm. >> So what it'll

180
00:08:55,440 --> 00:08:58,400
do is it will do them all at once simultaneously.

181
00:08:58,400 --> 00:09:02,480
And each model only gets a subset of the cores available to it.

182
00:09:02,480 --> 00:09:04,240
>> Okay, so- >> Rather than all of it.

183
00:09:04,240 --> 00:09:07,800
>> If we have the parallel lapply function, that's what it would.

184
00:09:07,800 --> 00:09:11,540
So it's basically using the multiple cores on our machine And

185
00:09:11,540 --> 00:09:16,410
each core is launching a separate R session to run a separate model.

186
00:09:16,410 --> 00:09:20,870
And this way they can all run concurrently, and when the last one

187
00:09:20,870 --> 00:09:23,600
has finished running we get our object back with all the models.

188
00:09:23,600 --> 00:09:26,790
>> Precisely, so each individual run for the model might take longer than

189
00:09:26,790 --> 00:09:28,260
if it had all the cores available to it?

190
00:09:28,260 --> 00:09:30,330
But because we're doing it all simultaneously or

191
00:09:30,330 --> 00:09:32,720
concurrently, the end result will be a bit faster.

192
00:09:32,720 --> 00:09:33,570
>> Okay. >> Mm-hm.

193
00:09:33,570 --> 00:09:35,950
>> In our case we actually don't just have multiple codes,

194
00:09:35,950 --> 00:09:37,320
we have multiple nodes.

195
00:09:37,320 --> 00:09:40,220
>> We have full computers and able to us that can do computation.

196
00:09:40,220 --> 00:09:43,080
So what we're gonna do is we're gonna have spark

197
00:09:43,080 --> 00:09:45,720
create a spark job and it should the models.

198
00:09:45,720 --> 00:09:48,302
Each of the nodes in the computer and the cluster.

199
00:09:48,302 --> 00:09:51,030
And get also all get it for you.

200
00:09:51,030 --> 00:09:51,690
>> Okay. So,

201
00:09:51,690 --> 00:09:54,900
you could say that there is two kinds of imparallelism happening in

202
00:09:54,900 --> 00:09:55,510
this case.

203
00:09:55,510 --> 00:10:01,715
One is each node is doing a separate model, building a separate model.

204
00:10:01,715 --> 00:10:03,020
>> Mm-hm. >> And on top of that,

205
00:10:03,020 --> 00:10:07,660
each node it self is using the Spark infrastructure to run

206
00:10:07,660 --> 00:10:09,570
the computation in parallel is that.

207
00:10:09,570 --> 00:10:10,140
>> Precisely and

208
00:10:10,140 --> 00:10:12,060
each node could have multiple cores >> Yeah

209
00:10:12,060 --> 00:10:12,946
>> multiple cores meaning

210
00:10:12,946 --> 00:10:14,360
the CPU cores >> Yeah

211
00:10:14,360 --> 00:10:15,280
>> so that could also be

212
00:10:15,280 --> 00:10:17,955
much faster than using a subset of single machine's cores.

213
00:10:17,955 --> 00:10:19,380
>> Okay.

214
00:10:19,380 --> 00:10:23,200
>> This is a very nice way to finding your own Spark Functions

215
00:10:23,200 --> 00:10:26,020
without having to write any Spark or Scholar code.

216
00:10:26,020 --> 00:10:31,090
>> So, why didn't we use RX exact to distribute our computation

217
00:10:31,090 --> 00:10:34,070
when we ran, let's say, RX or

218
00:10:34,070 --> 00:10:39,560
one of the RevoScale algorithm, the analytics algorithms?

219
00:10:39,560 --> 00:10:42,650
>> So, quite fortunately all the analytics algorithms in RevoScale R

220
00:10:42,650 --> 00:10:44,960
are automatically paralyzed.

221
00:10:44,960 --> 00:10:46,580
So you'd have to worry about creating your own

222
00:10:46,580 --> 00:10:47,840
parallel wrapper for it.

223
00:10:47,840 --> 00:10:51,700
Whenever you call or a SpeedTrees, it'll automatically utilize all

224
00:10:51,700 --> 00:10:53,235
the cores or all the nodes in your system.

225
00:10:53,235 --> 00:10:53,880
>> Mm-hm.

226
00:10:53,880 --> 00:10:56,810
>> In our case, we wanna not at one model, but multiple models.

227
00:10:56,810 --> 00:11:00,450
And there's no sort of rx Ensemble function that we can use for that.

228
00:11:00,450 --> 00:11:03,880
We're gonna do it ourselves, create a new list of objects, and

229
00:11:03,880 --> 00:11:05,640
then distribute that across our cluster.

230
00:11:05,640 --> 00:11:07,580
>> Okay, very impressive.

231
00:11:07,580 --> 00:11:10,530
>> So that's our simple set of code.

232
00:11:10,530 --> 00:11:13,480
This is often called a functional because its

233
00:11:13,480 --> 00:11:14,715
arguments are another function.

234
00:11:14,715 --> 00:11:17,460
>> Mm-hm. >> So we give it a function and

235
00:11:17,460 --> 00:11:20,110
it will distribute the computation that function across your cluster

236
00:11:20,110 --> 00:11:22,500
using the argument supplied by rxElemArg.

237
00:11:24,880 --> 00:11:28,330
Okay, for prediction it's a stay wrapper for RSpredict.

238
00:11:28,330 --> 00:11:33,010
We will take our resulting model, take the validate sets and

239
00:11:33,010 --> 00:11:36,020
then score it and return the scored dataset.

240
00:11:36,020 --> 00:11:39,580
The nice thing about this is we can store multiple models the same.

241
00:11:39,580 --> 00:11:40,150
File system.

242
00:11:40,150 --> 00:11:43,550
So we have multiple models saved and scored on one object.

243
00:11:43,550 --> 00:11:46,010
>> So we're gonna have multiple predictions,

244
00:11:46,010 --> 00:11:49,500
made by each of the different models, going into the same file.

245
00:11:49,500 --> 00:11:54,310
>> Exactly, and then we have a ability to get model metrics out of

246
00:11:54,310 --> 00:11:56,150
the resulting datasets.

247
00:11:56,150 --> 00:11:59,240
So here, I visualized the results of the different models.

248
00:11:59,240 --> 00:12:01,900
As you can see here the GBM performed very well, the gradient

249
00:12:01,900 --> 00:12:05,540
boosted tree has 95% accuracy which is very, very impressive.

250
00:12:05,540 --> 00:12:09,780
The forest did not do as well had a AUC of 75%.

251
00:12:09,780 --> 00:12:12,440
Which means that it's classifying quite a few of them correctly, but

252
00:12:12,440 --> 00:12:14,192
it's also getting quite a few of them wrong.

253
00:12:14,192 --> 00:12:15,030
>> Mm-hm.

254
00:12:15,030 --> 00:12:19,556
>> What's nice is that you can iterate across multiple algorithms

255
00:12:19,556 --> 00:12:23,159
with one function call and visualize the matrix and

256
00:12:23,159 --> 00:12:25,790
about the function called.

257
00:12:25,790 --> 00:12:30,259
>> So thanks Ali for coming and showing us how to run R

258
00:12:30,259 --> 00:12:34,946
inside of Spark using the Microsoft R server API and

259
00:12:34,946 --> 00:12:39,088
we hope to be able to see more of these demos and

260
00:12:39,088 --> 00:12:43,240
kind of indefinitely in the course content.

261
00:12:43,240 --> 00:12:46,000
We're gonna put some examples of runtime.

262
00:12:46,000 --> 00:12:48,900
Running things locally, running things in sequel server and

263
00:12:48,900 --> 00:12:51,040
running things inside of R.

264
00:12:51,040 --> 00:12:55,380
So that we can maybe get an idea of some of the advantages and

265
00:12:55,380 --> 00:12:59,560
disadvantages of using each.

266
00:12:59,560 --> 00:13:01,883
Thanks a lot and see you soon.

