0
00:00:05,287 --> 00:00:08,970
So welcome to Analyzing Big Data with Microsoft R.

1
00:00:10,140 --> 00:00:13,750
My name is Seth Mottaghinejad and I'm gonna be the instructor for

2
00:00:13,750 --> 00:00:15,050
this course.

3
00:00:15,050 --> 00:00:17,850
I am a Data Scientist at Microsoft.

4
00:00:17,850 --> 00:00:21,246
Prior to joining Microsoft, I was working at Revolution Analytics.

5
00:00:21,246 --> 00:00:26,960
Our company made a product called Revolution R Enterprise which,

6
00:00:26,960 --> 00:00:33,250
as you can guess by the name, made R more accessible to enterprises.

7
00:00:33,250 --> 00:00:37,310
Revolution analytics was acquired by Microsoft in May of 2015 and

8
00:00:37,310 --> 00:00:39,730
I joined Microsoft as part of that.

9
00:00:39,730 --> 00:00:40,601
Since then,

10
00:00:40,601 --> 00:00:45,363
significant enhancements have been made to Revolution R Enterprise.

11
00:00:45,363 --> 00:00:49,140
Including SQL Server R Services, support for Spark,

12
00:00:49,140 --> 00:00:53,982
both of which we're gonna be able to see in the last week of the course,

13
00:00:53,982 --> 00:00:57,425
as well as R Tools for Visual Studio.

14
00:00:57,425 --> 00:00:58,565
In addition,

15
00:00:58,565 --> 00:01:03,395
Revolution R Enterprise has been rebranded as Microsoft R Server.

16
00:01:03,395 --> 00:01:07,557
In this course, we're gonna learn how to use Microsoft R Server

17
00:01:07,557 --> 00:01:12,117
to load, process, analyze and build models on very large datasets.

18
00:01:12,117 --> 00:01:16,260
And then deploy those models to production environments such as

19
00:01:16,260 --> 00:01:17,725
Sparks or SQL Server.

20
00:01:18,940 --> 00:01:21,128
So it's good to have you and let's dive right in.

