0
00:00:01,030 --> 00:00:05,590
So let's see what Microsoft R does to address R's limitations.

1
00:00:05,590 --> 00:00:08,850
With Microsoft R Server, we can have the peace of mind

2
00:00:08,850 --> 00:00:13,026
that our R code is going to run with data of any size.

3
00:00:13,026 --> 00:00:18,800
Moreover, Microsoft R Server's algorithms are parallelized, giving

4
00:00:18,800 --> 00:00:22,990
us the ability to run our R code in an efficient and scalable fashion.

5
00:00:24,190 --> 00:00:29,210
Finally, Microsoft R Server allows us to deploy our code

6
00:00:29,210 --> 00:00:32,820
in a flexible way inside a production environments

7
00:00:32,820 --> 00:00:36,710
with minimal changes made to the overall code structure.

8
00:00:38,470 --> 00:00:42,810
So let's begin at the top and look at Microsoft R's portfolio.

9
00:00:42,810 --> 00:00:47,050
As we can see, Microsoft R comes in two different flavors.

10
00:00:47,050 --> 00:00:50,570
There's a community version called Microsoft R Open.

11
00:00:50,570 --> 00:00:55,540
With Microsoft R Open, we can run our open source R code

12
00:00:55,540 --> 00:00:59,850
in a more efficient manner using multithreading and

13
00:00:59,850 --> 00:01:02,390
changes made to the math kernel library.

14
00:01:03,480 --> 00:01:07,360
But our focus is gonna be on the commercial product of Microsoft R.

15
00:01:08,580 --> 00:01:13,420
With Microsoft R Server we can write our code that runs inside of

16
00:01:13,420 --> 00:01:16,180
production environment such as Hadoop or Teradata.

17
00:01:17,250 --> 00:01:22,449
Or we can do in database analytics using SQL Server R services.

18
00:01:23,570 --> 00:01:27,940
To see how we can do that, we're gonna use the RevoScaleR package.

19
00:01:27,940 --> 00:01:32,380
With RevoScaleR, we can escape R's traditional memory limits for

20
00:01:32,380 --> 00:01:33,320
the size of the data.

21
00:01:34,410 --> 00:01:38,250
We can also write a code for predictive models and analytics that

22
00:01:38,250 --> 00:01:43,120
is scalable because RevoScaleR's algorithms are parallel.

23
00:01:43,120 --> 00:01:48,780
Our code can distribute to multiple course inside of a single machine or

24
00:01:48,780 --> 00:01:53,150
multiple nodes and a cluster there by giving us scalability.

25
00:01:53,150 --> 00:01:57,440
Finally as I mentioned before with very little changes to the overall

26
00:01:57,440 --> 00:02:02,210
code structure, we can take our R code and we can deploy it inside of

27
00:02:02,210 --> 00:02:06,780
production environments such as Hadoop or Spark, or SQL server.

