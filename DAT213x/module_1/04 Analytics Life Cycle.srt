0
00:00:01,170 --> 00:00:04,230
Let's take a look at a typical analytics lifecycle.

1
00:00:04,230 --> 00:00:07,585
And we're gonna do something very similar to this in this course.

2
00:00:07,585 --> 00:00:10,980
Any analytics lifecycle begins with data.

3
00:00:10,980 --> 00:00:13,720
And the first thing we wanna do with that data is ingest it.

4
00:00:14,920 --> 00:00:16,100
Once we ingest the data,

5
00:00:16,100 --> 00:00:20,890
we have to ask what we need to do to prepare the data for analysis.

6
00:00:20,890 --> 00:00:23,930
This usually includes various transformations on the data and

7
00:00:23,930 --> 00:00:29,070
various explorations of the data to see what needs to be done.

8
00:00:29,070 --> 00:00:32,300
Once we have the data ready, we're gonna move to the modeling phase.

9
00:00:32,300 --> 00:00:35,690
In this phase, we're gonna run different models on the data.

10
00:00:35,690 --> 00:00:39,130
We're gonna be interested in comparing the performance of each

11
00:00:39,130 --> 00:00:45,290
model by deploying the model on some out-of-sample data that we have.

12
00:00:45,290 --> 00:00:47,600
Once we settle down on a model,

13
00:00:47,600 --> 00:00:50,780
we can then move onto the operationalization phase.

14
00:00:50,780 --> 00:00:55,426
In this phase, we're interested in scoring data that is sitting

15
00:00:55,426 --> 00:00:57,203
in a production server.

16
00:00:57,203 --> 00:01:00,701
We could also be interested in rebuilding our model on the large

17
00:01:00,701 --> 00:01:04,110
dataset that is sitting on the production server.

18
00:01:04,110 --> 00:01:06,900
Either way, at the end of the day, we're gonna look at

19
00:01:06,900 --> 00:01:11,550
various measurements and somehow visualize the results.

20
00:01:11,550 --> 00:01:15,600
The last thing we wanna point out is that it's important to know that,

21
00:01:15,600 --> 00:01:19,350
more often than not, we loop back and go back to square one.

22
00:01:19,350 --> 00:01:23,210
This is because the way that the data is being used in production

23
00:01:23,210 --> 00:01:24,500
can often guide us and

24
00:01:24,500 --> 00:01:28,870
inform us on how we can do a better job of ingesting the data.

25
00:01:28,870 --> 00:01:32,420
How we can do a better job of transforming the data and

26
00:01:32,420 --> 00:01:34,890
getting it ready for the analysis.

27
00:01:34,890 --> 00:01:38,740
Or which models seem to work better and which models don't.

28
00:01:39,810 --> 00:01:41,850
So this is a continuous lifecycle.

29
00:01:41,850 --> 00:01:43,180
And even though, in this course,

30
00:01:43,180 --> 00:01:45,195
we're gonna follow it step by step and

31
00:01:45,195 --> 00:01:49,385
in a somewhat linear fashion, it's important to know that, in reality,

32
00:01:49,385 --> 00:01:52,605
what we do is, once we get to the end, we go back to the beginning.

