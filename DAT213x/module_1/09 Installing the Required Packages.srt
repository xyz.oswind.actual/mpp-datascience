0
00:00:01,840 --> 00:00:04,690
So I've downloaded the sample data

1
00:00:05,750 --> 00:00:10,815
into a location on my laptop that is in C:\Data\NYC_taxi.

2
00:00:12,670 --> 00:00:16,005
So I recommend that you either follow the same location or

3
00:00:16,005 --> 00:00:19,964
in the R code you may have to change this location to the location that

4
00:00:19,964 --> 00:00:21,302
you have for the data.

5
00:00:21,302 --> 00:00:25,768
In addition to the six CSV files, there is also a file that you

6
00:00:25,768 --> 00:00:30,247
should have downloaded called ZillowNeighborhoods-NY.

7
00:00:30,247 --> 00:00:33,269
And we'll get to what sort of information is stored in this

8
00:00:33,269 --> 00:00:34,570
file a little bit later.

9
00:00:36,010 --> 00:00:40,223
I also have a script, an R script in this case with all of the code for

10
00:00:40,223 --> 00:00:42,330
the data that, all of the code for

11
00:00:42,330 --> 00:00:45,710
the course that we're gonna be using.

12
00:00:45,710 --> 00:00:48,270
I'm gonna go back to Visual Studio and

13
00:00:48,270 --> 00:00:51,700
the first thing I'm gonna do is open up the R script.

14
00:00:54,710 --> 00:00:57,319
The next thing I'm gonna do is,

15
00:00:57,319 --> 00:01:02,055
right now I have the code editor on the left side at the top,

16
00:01:02,055 --> 00:01:07,002
and the R console on the left side at the bottom of the screen.

17
00:01:07,002 --> 00:01:10,780
I think we can make a little bit better use of the screen

18
00:01:10,780 --> 00:01:15,340
real estate if we drag this and drop it on this side.

19
00:01:16,380 --> 00:01:20,680
And then if we minimize the Solutions Explorer.

20
00:01:22,290 --> 00:01:26,430
So now we have the editor on the left side of the screen, and

21
00:01:26,430 --> 00:01:28,900
we have the R console on the right side of the screen and

22
00:01:28,900 --> 00:01:33,010
we can see more of the code.

23
00:01:33,010 --> 00:01:34,230
Now we're gonna go and

24
00:01:34,230 --> 00:01:39,700
install the necessary R packages in order to run the course.

25
00:01:39,700 --> 00:01:42,350
So in addition to the RevoScaleR package,

26
00:01:42,350 --> 00:01:45,920
which is the one that we're gonna learn about in this course.

27
00:01:45,920 --> 00:01:49,180
There is also going to be some third party packages,

28
00:01:49,180 --> 00:01:52,160
some open source packages that we're gonna be loading,

29
00:01:52,160 --> 00:01:55,100
that we're gonna be using throughout the course.

30
00:01:55,100 --> 00:01:57,610
So we wanna make sure that those packages are installed.

31
00:01:58,700 --> 00:02:02,640
And because we are going to get those packages from CRAN,

32
00:02:02,640 --> 00:02:05,850
we're gonna set the repository to be CRAN.

33
00:02:05,850 --> 00:02:10,960
Now, our first step then is to

34
00:02:10,960 --> 00:02:15,390
set the repo to be CRAN and I'm gonna run the install.packages for

35
00:02:15,390 --> 00:02:19,220
all of these packages that we're gonna use throughout the course.

36
00:02:21,270 --> 00:02:24,781
This is gonna take a couple of minutes to run and

37
00:02:24,781 --> 00:02:29,474
you should see something like what I'm seeing on my console.

38
00:02:29,474 --> 00:02:33,506
It's gonna point to the location where the package is and

39
00:02:33,506 --> 00:02:38,381
if the package is successfully installed, you should see something

40
00:02:38,381 --> 00:02:42,270
like package successfully unpacked and sum checked.

41
00:02:42,270 --> 00:02:46,580
So we can wait for this to finished and

42
00:02:46,580 --> 00:02:51,400
then we can load all of our packages to make sure that everything worked.

43
00:02:52,670 --> 00:02:56,340
Okay, so we have the packages installed and

44
00:02:56,340 --> 00:03:00,630
we are now going to load all of the packages to make sure

45
00:03:00,630 --> 00:03:03,850
that everything was properly installed.

46
00:03:03,850 --> 00:03:07,120
Now we're gonna load the packages a little bit later when we get to

47
00:03:07,120 --> 00:03:09,170
actually using the packages.

48
00:03:09,170 --> 00:03:12,760
But there is a location here at the very top where we're gonna load all

49
00:03:12,760 --> 00:03:15,130
of them at the same time just to make sure that

50
00:03:16,240 --> 00:03:19,730
nothing unusual happened in installation phase.

51
00:03:19,730 --> 00:03:24,252
So the first thing I'm gonna do is set the working directory to be

52
00:03:24,252 --> 00:03:29,534
C:\Data\NYC_taxi because that's the location where my CSV files and

53
00:03:29,534 --> 00:03:33,252
the folder ZillowNeighborhoods-NY are sitting.

54
00:03:34,380 --> 00:03:37,350
So that's gonna be my working directory from now on.

55
00:03:37,350 --> 00:03:41,570
And now we're gonna load each of the packages that we need to use in this

56
00:03:41,570 --> 00:03:42,070
course.

57
00:03:43,140 --> 00:03:47,990
I'm going to set some options for

58
00:03:47,990 --> 00:03:53,960
how much information is printed, in this case, the maximum print size.

59
00:03:53,960 --> 00:03:57,920
We're gonna tell it to avoid using scientific notation and

60
00:03:57,920 --> 00:04:00,310
we're gonna set the width of the console.

61
00:04:01,970 --> 00:04:04,984
We're gonna load the RevoScaleR library,

62
00:04:04,984 --> 00:04:07,841
now this is the only one that is optional.

63
00:04:07,841 --> 00:04:11,665
The RevoScaleR library is gonna be pre-loaded with the R clients so

64
00:04:11,665 --> 00:04:16,470
we don't need to load the RevoScaleR library every time we wanna use it.

65
00:04:16,470 --> 00:04:19,370
But I have it here just for

66
00:04:19,370 --> 00:04:23,426
us to make sure that we're using the right installation of R and

67
00:04:23,426 --> 00:04:29,030
RevoScaleR is pre-packaged into it.

68
00:04:29,030 --> 00:04:32,840
We're gonna set some options for RevoScaleR, we're gonna load dplyr.

69
00:04:33,920 --> 00:04:38,205
And just like me, you should usually see some

70
00:04:38,205 --> 00:04:42,520
warnings when loading certain third party packages.

71
00:04:42,520 --> 00:04:45,700
Usually warnings about certain objects being masked,

72
00:04:45,700 --> 00:04:47,960
but we can ignore all of the warnings.

73
00:04:47,960 --> 00:04:51,270
What we don't wanna see is an error message that says that the package

74
00:04:51,270 --> 00:04:54,130
could not be found.

75
00:04:54,130 --> 00:04:57,861
In that case, we either forgot to run install.packages or

76
00:04:57,861 --> 00:05:00,782
something with the installation went wrong.

77
00:05:00,782 --> 00:05:04,770
We're gonna set some options for the dplyr as well.

78
00:05:04,770 --> 00:05:11,112
We're gonna load Stringr, lubridate, regeos,

79
00:05:11,112 --> 00:05:17,307
sp, maptools, ggmap, ggplot2, gridExtra,

80
00:05:17,307 --> 00:05:22,330
ggrepel, tidyr and seriation, okay?

81
00:05:22,330 --> 00:05:26,950
Just like in RStudio, I'm using the Ctrl+Enter

82
00:05:26,950 --> 00:05:32,232
command in our RTVS to click the run the code line by line.

83
00:05:32,232 --> 00:05:36,383
So Ctrl+Enter is probably the most useful

84
00:05:36,383 --> 00:05:40,773
keyboard shortcut when working with an R ID.

85
00:05:40,773 --> 00:05:45,420
Now it looks like everything worked.

86
00:05:45,420 --> 00:05:49,690
We get a bunch of warnings but we don't get any error messages.

87
00:05:49,690 --> 00:05:52,755
One way to quickly double-check and make sure that we

88
00:05:52,755 --> 00:05:57,015
didn't get any error messages is we can go back to the top here and

89
00:05:57,015 --> 00:05:59,855
we can run all of the library commands.

90
00:05:59,855 --> 00:06:04,483
So I'm gonna select everything from library(RevoScaleR) to

91
00:06:04,483 --> 00:06:07,085
the end of library(seriation).

92
00:06:07,085 --> 00:06:11,575
I'm gonna hit Ctrl+Enter again, and notice that this time when we

93
00:06:11,575 --> 00:06:16,143
load a library for a second time we don't get any warning messages and

94
00:06:16,143 --> 00:06:17,900
I can see my prompt there.

95
00:06:17,900 --> 00:06:21,604
So if there were any error messages to be had I should have seen it.

96
00:06:21,604 --> 00:06:25,872
But because everything worked and I see the prompt, it means that all of

97
00:06:25,872 --> 00:06:29,161
the libraries are now loaded in the current R session.

