0
00:00:01,620 --> 00:00:05,010
So we should be up and running, with the Microsoft R client.

1
00:00:06,360 --> 00:00:10,740
We should have the Microsoft R client installed, on our machine.

2
00:00:10,740 --> 00:00:14,410
We should have an IDE and we should make sure that our IDE,

3
00:00:14,410 --> 00:00:17,140
is pointing to the Microsoft R installation.

4
00:00:18,240 --> 00:00:19,880
Not to any other R installation.

5
00:00:20,940 --> 00:00:23,480
Now, we're gonna talk about the data.

6
00:00:25,370 --> 00:00:28,480
So we're here, on the course website and the data that we're gonna use,

7
00:00:28,480 --> 00:00:31,197
is called the New York City Taxi data set.

8
00:00:32,590 --> 00:00:37,030
A link to the entirety of the data, is provided.

9
00:00:37,030 --> 00:00:38,440
On the course website.

10
00:00:38,440 --> 00:00:42,750
So I'm gonna click, on the New York City Taxi data set link.

11
00:00:42,750 --> 00:00:46,790
And it takes us to a webpage that is maintained, by the city of New York.

12
00:00:46,790 --> 00:00:50,780
Scrolling down, we can see the data shown right here.

13
00:00:50,780 --> 00:00:53,801
There are three separate data sets,

14
00:00:53,801 --> 00:00:58,586
one for yellow cabs, one for green cabs, and one for FHV.

15
00:00:58,586 --> 00:01:01,823
We're only gonna care about the yellow cabs, in this case and

16
00:01:01,823 --> 00:01:03,452
we can see that for each month.

17
00:01:03,452 --> 00:01:07,340
And we have a separate data set, that we can download.

18
00:01:07,340 --> 00:01:11,650
If I hold my mouse over it and look at the bottom of my browser,

19
00:01:11,650 --> 00:01:15,390
I can see that this links directly to a CSV file.

20
00:01:15,390 --> 00:01:19,074
Each of these CSV files in the case of yellow cabs,

21
00:01:19,074 --> 00:01:21,530
is about two gigabytes in size.

22
00:01:21,530 --> 00:01:23,280
So these are large CSV files.

23
00:01:24,550 --> 00:01:26,100
Because the Microsoft,

24
00:01:26,100 --> 00:01:30,700
our client is a light weight installation of Microsoft R.

25
00:01:31,710 --> 00:01:36,600
Whose purpose is not necessarily to be able to handle, big data sets.

26
00:01:36,600 --> 00:01:40,590
Its purpose is only to learn about the RevoScaleR libraries and

27
00:01:40,590 --> 00:01:44,440
the functions, that are in the RevoScaleR library.

28
00:01:44,440 --> 00:01:49,093
We're going to be working with a sample of the big data,

29
00:01:49,093 --> 00:01:51,569
that we have on this website.

30
00:01:51,569 --> 00:01:57,761
The sample is gonna consists, of the first six months of 2016.

31
00:01:57,761 --> 00:02:01,975
So from January through June and for each month.

32
00:02:01,975 --> 00:02:07,650
We're gonna take 5% of the data and store it, as a separate CSV file.

33
00:02:07,650 --> 00:02:10,540
So the sample data, is gonna look like the data that

34
00:02:10,540 --> 00:02:13,610
we have on the website, but it's only gonna be 5% of the data.

35
00:02:15,430 --> 00:02:18,470
The link to the sample data, is provided in the course website.

36
00:02:20,470 --> 00:02:24,070
Once we download the sample data.

37
00:02:24,070 --> 00:02:27,575
We're gonna have six CSV files and in total,

38
00:02:27,575 --> 00:02:32,105
they're gonna be about seven hundred megabytes in size.

39
00:02:32,105 --> 00:02:36,989
So this is the data that we're gonna load, into visual studio.

40
00:02:36,989 --> 00:02:41,986
And use Microsoft R servers RevoScaleR

41
00:02:41,986 --> 00:02:46,838
package, to process and to analyze.

