0
00:00:01,300 --> 00:00:05,289
So we're back now to the New York City taxis website.

1
00:00:05,289 --> 00:00:08,007
And we're gonna scroll down here, and we can see

2
00:00:08,007 --> 00:00:12,020
that there is a data dictionary for each of the data sets.

3
00:00:12,020 --> 00:00:17,070
If I click on the data dictionary for Yellow, it's gonna open

4
00:00:17,070 --> 00:00:21,640
a PDF file that has a description of the columns that are in the data.

5
00:00:21,640 --> 00:00:26,880
We can see, There are columns that

6
00:00:26,880 --> 00:00:32,170
show the pickup and drop-off date time of a taxi trip.

7
00:00:32,170 --> 00:00:36,070
So each record of the data is gonna be a taxi trip with a particular

8
00:00:36,070 --> 00:00:38,580
date time for pickup and drop-off,

9
00:00:38,580 --> 00:00:41,770
the number of passengers, the distance that was traveled.

10
00:00:42,800 --> 00:00:48,000
We can see coordinates for longitude and latitude of the pickup and

11
00:00:48,000 --> 00:00:51,640
a little bit further down for drop-off.

12
00:00:51,640 --> 00:00:55,210
We can see the amount that was paid and

13
00:00:55,210 --> 00:00:57,320
the type of payment that was used.

14
00:00:57,320 --> 00:01:01,950
So in this case the payment type, and here we can see the fare amount.

15
00:01:01,950 --> 00:01:06,750
We can see the tip amount and the total amount for

16
00:01:06,750 --> 00:01:08,911
the trip, and so on.

17
00:01:08,911 --> 00:01:16,366
So we summarized that information on the course website.

18
00:01:16,366 --> 00:01:20,240
And we're going to be using this data set.

19
00:01:20,240 --> 00:01:24,540
We're going to be analyzing this data set based on the information

20
00:01:24,540 --> 00:01:26,810
that we can find in the columns.

21
00:01:26,810 --> 00:01:32,260
So our next task is gonna be to load this data into R and

22
00:01:32,260 --> 00:01:34,070
get ready to run some R code.

