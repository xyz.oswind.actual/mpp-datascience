0
00:00:01,130 --> 00:00:04,441
So, lets review the three main benefits of the RevoScaleR our

1
00:00:04,441 --> 00:00:05,060
package.

2
00:00:05,060 --> 00:00:07,230
The first benefit is that,

3
00:00:07,230 --> 00:00:11,960
with the RevoScaleR package, even if we have off memory to load our data

4
00:00:11,960 --> 00:00:16,470
has a data frame into our current R session, we can use RevoScaleR's

5
00:00:16,470 --> 00:00:21,310
parallel algorithms to run analytics on that data, much faster than we

6
00:00:21,310 --> 00:00:25,650
would with the open source, our counterparts to those algorithms.

7
00:00:25,650 --> 00:00:30,320
The second benefit of the RevoScaleR package is that if our data is now

8
00:00:30,320 --> 00:00:33,460
too large to fit in available memory

9
00:00:33,460 --> 00:00:37,670
we can still use the RevoScaleR algorithms just as before

10
00:00:37,670 --> 00:00:40,880
by simply pointing to the data that is sitting on disks.

11
00:00:41,890 --> 00:00:45,540
The way that RevoScaleR operates in this case is that it loads

12
00:00:45,540 --> 00:00:50,560
the data into the R session as a data frame, only a chunk at a time.

13
00:00:50,560 --> 00:00:55,010
Where a chunk is by default set to 500,000 rows but it can be changed.

14
00:00:56,490 --> 00:01:01,450
By doing so RevoScaleR can simply load the data one chunk at a time

15
00:01:01,450 --> 00:01:03,780
and process it, move on to the next chunk,

16
00:01:03,780 --> 00:01:06,750
and keep doing this until all of the data has been handled.

17
00:01:07,850 --> 00:01:10,590
The fact that we will scale our logarithm or

18
00:01:10,590 --> 00:01:13,180
parallel is what makes this possible.

19
00:01:14,460 --> 00:01:19,480
The third benefit of using the RevoScaleR package is that

20
00:01:19,480 --> 00:01:23,840
with the RevoScaleR we can now take our code that we've developed and

21
00:01:23,840 --> 00:01:27,090
we can deploy it inside the production environment.

22
00:01:27,090 --> 00:01:32,070
Be at a Hadoop cluster or a SQL Server database, and this code is

23
00:01:32,070 --> 00:01:36,940
going to run with very little changes made to the code structure.

24
00:01:38,960 --> 00:01:42,710
Of course in this case the code could be running on a dataset

25
00:01:42,710 --> 00:01:46,150
that is much larger than the data that would

26
00:01:46,150 --> 00:01:47,770
be setting on a single machine.

