0
00:00:01,440 --> 00:00:04,220
So let's take a high level view of

1
00:00:04,220 --> 00:00:08,050
the sort of functionality that's built into RevoScaleR.

2
00:00:08,050 --> 00:00:12,770
Starting at the left side, we can see that RevoScaleR lets us

3
00:00:12,770 --> 00:00:15,280
read data from a variety of sources and

4
00:00:15,280 --> 00:00:18,370
prepare that data for some kind of analysis.

5
00:00:18,370 --> 00:00:22,260
In the process of doing that, if we need to run descriptive statistics

6
00:00:22,260 --> 00:00:25,760
on the data, or if we need to perform statistical tests,

7
00:00:25,760 --> 00:00:29,440
we have functions built into RevoScaleR for doing that.

8
00:00:29,440 --> 00:00:33,590
What we can also do is we can take a sample from our data.

9
00:00:33,590 --> 00:00:38,420
And with that sample, we can have a data frame that

10
00:00:38,420 --> 00:00:43,240
can be used by all of R's packages for performing various analysis.

11
00:00:44,470 --> 00:00:49,450
The bread and butter of RevoScaleR lies in the analytics algorithms.

12
00:00:50,680 --> 00:00:53,340
These algorithms include regression and

13
00:00:53,340 --> 00:00:57,670
classification algorithms such as linear models, logistic regression,

14
00:00:57,670 --> 00:01:01,170
decision trees, or ensemble models such as random forests.

15
00:01:02,270 --> 00:01:06,350
They also include the Camians algorithms for building clusters.

16
00:01:07,410 --> 00:01:10,300
As you can see, all of these algorithms have their counterparts

17
00:01:10,300 --> 00:01:12,890
in various open source R functions.

18
00:01:12,890 --> 00:01:16,000
However, the strength of these algorithms lies in the fact

19
00:01:16,000 --> 00:01:17,670
that they are parallel.

20
00:01:17,670 --> 00:01:21,585
The fact that they are parallel makes it so that we can run these

21
00:01:21,585 --> 00:01:25,292
algorithms on very large data sets in a scalable fashion.

22
00:01:26,460 --> 00:01:29,245
In addition to being parallel and scalable,

23
00:01:29,245 --> 00:01:33,800
these algorithms can also run inside of production environments.

24
00:01:33,800 --> 00:01:37,730
Let's see how that's gonna look like at a very high level.

25
00:01:37,730 --> 00:01:41,570
So here, we have an example of that happening inside of Hadoop.

26
00:01:41,570 --> 00:01:47,498
On the left side of the screen, we can see some code that essentially

27
00:01:47,498 --> 00:01:52,395
points to some data on a local Linux or Windows machine.

28
00:01:52,395 --> 00:01:55,930
At the bottom of the screen, we can see that we can then take that

29
00:01:55,930 --> 00:01:59,080
data and run various statistical summaries on it.

30
00:01:59,080 --> 00:02:00,850
Run some cross tabulations and

31
00:02:00,850 --> 00:02:04,380
finally perform some kind of modeling task with the data.

32
00:02:05,960 --> 00:02:08,340
Now, let's take a look at the box on the right side.

33
00:02:09,780 --> 00:02:12,650
We can see that the box on the right side looks

34
00:02:12,650 --> 00:02:14,880
very similar to the box on the left side.

35
00:02:14,880 --> 00:02:18,740
Except, instead of pointing to data that is sitting on the local

36
00:02:18,740 --> 00:02:22,490
machine, it is pointing to data that is sitting on HDFS.

37
00:02:22,490 --> 00:02:25,070
In other words, inside of a Hadoop cluster.

38
00:02:25,070 --> 00:02:29,453
But once you change the code to pointing to data on HDFS,

39
00:02:29,453 --> 00:02:31,984
the rest of the code is the same.

40
00:02:31,984 --> 00:02:35,428
Namely the code for doing the statistical summaries, or

41
00:02:35,428 --> 00:02:39,015
running various cross tabulations and finally the code for

42
00:02:39,015 --> 00:02:43,280
building a linear model on the data, is gonna stay the same.

43
00:02:43,280 --> 00:02:47,300
This sort of code portability is very important in order to be able

44
00:02:47,300 --> 00:02:50,220
to run our R code inside of production environments

45
00:02:50,220 --> 00:02:53,700
without having to completely change the structure of the code.

46
00:02:53,700 --> 00:02:55,340
And with RevoScaleR, we can do that.

