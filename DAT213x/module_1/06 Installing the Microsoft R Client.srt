0
00:00:01,810 --> 00:00:07,070
We're gonna start by preparing our R environment for this course.

1
00:00:08,410 --> 00:00:09,690
We need two things.

2
00:00:09,690 --> 00:00:14,740
One is an installation of R and the second one is an ID.

3
00:00:14,740 --> 00:00:19,560
For an ID, we can either use Visual Studio with our tools or

4
00:00:19,560 --> 00:00:20,760
we can use RStudio.

5
00:00:22,000 --> 00:00:24,420
In this course, we're gonna use RTVS.

6
00:00:24,420 --> 00:00:26,950
But you're free to use RStudio if you want to it doesn't

7
00:00:26,950 --> 00:00:27,700
change anything.

8
00:00:29,770 --> 00:00:33,500
The important part now is our installation of R itself.

9
00:00:35,160 --> 00:00:37,870
We're gonna be using the Microsoft R Client.

10
00:00:39,210 --> 00:00:42,800
The link to that is provided here in the course website.

11
00:00:42,800 --> 00:00:47,280
Microsoft R Client is a lightweight version of the Microsoft R Server.

12
00:00:47,280 --> 00:00:50,675
It's a great way for us to learn about the Revo scalar library and

13
00:00:50,675 --> 00:00:53,142
all the functions that are built in to it.

14
00:00:53,142 --> 00:00:57,640
But it is light weight and the sense that is not meant

15
00:00:57,640 --> 00:01:01,600
to be use as a production environment for

16
00:01:01,600 --> 00:01:03,940
the Microsoft R Server on a single machine.

17
00:01:05,440 --> 00:01:11,110
So, we're gonna go to this link which take us to a page on MSDN.

18
00:01:11,110 --> 00:01:15,176
If we scroll down here we can see that there is a download link for

19
00:01:15,176 --> 00:01:16,801
the Microsoft R Client.

20
00:01:16,801 --> 00:01:21,406
I'm gonna download Microsoft R Client here and

21
00:01:21,406 --> 00:01:24,522
I'm gonna launch the install.

22
00:01:24,522 --> 00:01:27,554
And it should be pretty straight forward to install

23
00:01:27,554 --> 00:01:29,730
the Microsoft R Client.

24
00:01:29,730 --> 00:01:31,200
We accept the agreement.

25
00:01:31,200 --> 00:01:32,720
Hit Next a couple of times.

26
00:01:34,490 --> 00:01:38,010
And notice that I'm gonna put R Client in the following location.

27
00:01:38,010 --> 00:01:40,720
C:\Program Files/Microsoft/R Client\.

28
00:01:40,720 --> 00:01:42,020
This is gonna be important.

29
00:01:43,210 --> 00:01:45,340
Once we launch our ID.

30
00:01:46,450 --> 00:01:48,760
As if your gonna save it on any other location.

31
00:01:48,760 --> 00:01:49,910
Make sure to keep track of that.

32
00:01:52,950 --> 00:01:55,130
So, installation is finished.

33
00:01:55,130 --> 00:01:57,920
As I mentioned, it's pretty straight forward and pretty quick.

34
00:01:59,300 --> 00:02:01,200
And now we have the Microsoft R Client.

35
00:02:02,640 --> 00:02:06,860
What Microsoft R Client gives us is a separate installation of.

36
00:02:06,860 --> 00:02:11,930
Separate from CRAN in case some of you already have CRAN installed.

37
00:02:11,930 --> 00:02:15,840
We now, on top of that, have a separate R installation

38
00:02:15,840 --> 00:02:18,500
that points to the Microsoft R Client installation.

39
00:02:19,940 --> 00:02:24,010
The next thing we wanna do is install RTVS.

40
00:02:24,010 --> 00:02:26,578
So, the link to that is also provided in the course.

41
00:02:26,578 --> 00:02:31,550
In here, I' gonna download R Tools for Visual Studio.

42
00:02:31,550 --> 00:02:33,990
I already have Visual Studio installed on my computer.

43
00:02:35,250 --> 00:02:38,100
So, the only thing I need to do is install RTVS.

44
00:02:39,690 --> 00:02:42,830
And this is also a pretty straight forward installation.

45
00:02:44,370 --> 00:02:49,267
Simply hit the Install button And step through it and pretty quickly,

46
00:02:49,267 --> 00:02:51,591
you should have RTVS ready to go.

47
00:02:55,152 --> 00:02:57,111
So, we have RTVS now.

48
00:02:57,111 --> 00:03:03,212
We're going to be using RTVS to run R from inside of Visual Studio.

49
00:03:03,212 --> 00:03:06,232
As I mentioned before, if you prefer to use R Studio,

50
00:03:06,232 --> 00:03:07,720
that's another option.

51
00:03:09,090 --> 00:03:14,280
I'm gonna close the install, and we can now launch Visual Studio.

52
00:03:16,480 --> 00:03:20,630
And Visual Studio now has R Tools built on top of it.

53
00:03:22,340 --> 00:03:26,040
There's a couple of things that we have to do to get Visual Studio

54
00:03:26,040 --> 00:03:29,440
ready for running our R code.

55
00:03:30,750 --> 00:03:34,030
I'm gonna give Visual Studio a moment to load, and

56
00:03:34,030 --> 00:03:35,450
here we have Visual Studio.

57
00:03:36,480 --> 00:03:39,240
There's a couple of things we need to do to make sure that

58
00:03:39,240 --> 00:03:40,310
we're set up.

59
00:03:40,310 --> 00:03:43,895
First I'm gonna go to Tools Import and

60
00:03:43,895 --> 00:03:47,645
export settings, reset all settings.

61
00:03:47,645 --> 00:03:50,375
Now, if you're a Visual Studio user,

62
00:03:50,375 --> 00:03:53,205
you might wanna save your current settings, if you choose to.

63
00:03:53,205 --> 00:03:54,975
But I'm just gonna say no.

64
00:03:54,975 --> 00:03:56,790
Just reset settings.

65
00:03:56,790 --> 00:03:57,573
Hit Next and

66
00:03:57,573 --> 00:04:02,202
here make sure that you select data science R with RStudio shortcuts.

67
00:04:02,202 --> 00:04:06,946
What this will do is it'll make Visual Studio have sort of look and

68
00:04:06,946 --> 00:04:12,142
feel similar to RStudio and give it the same set of keyboard shortcuts.

69
00:04:12,142 --> 00:04:15,442
So, we're gonna finish and close and

70
00:04:15,442 --> 00:04:19,060
nothing particularly drastic happens.

71
00:04:20,160 --> 00:04:24,176
We can see that the R interactive window is shown here,

72
00:04:24,176 --> 00:04:27,682
this is where we can interact with our R Client.

73
00:04:27,682 --> 00:04:29,220
But before doing so,

74
00:04:29,220 --> 00:04:33,844
we need to make sure that we're pointing to the R Client in fact.

75
00:04:33,844 --> 00:04:36,863
Now if you have no other installation of R and

76
00:04:36,863 --> 00:04:40,943
you just installed the R Client then it should be pointing to

77
00:04:40,943 --> 00:04:42,584
the R Client just fine.

78
00:04:42,584 --> 00:04:45,665
But if you have multiple R installations including a CRAN

79
00:04:45,665 --> 00:04:46,762
installation of R.

80
00:04:46,762 --> 00:04:50,350
This is where we need to be careful to make sure that we point this to

81
00:04:50,350 --> 00:04:51,604
the R Client install.

82
00:04:51,604 --> 00:04:58,502
One way we can do that is we can simply type library(RevoScaleR).

83
00:04:58,502 --> 00:05:03,103
Make sure that you pay attention to the cases there.

84
00:05:03,103 --> 00:05:08,460
And we can see that hitting Enter, we get no error messages.

85
00:05:08,460 --> 00:05:12,405
So, the RevoScaleR library is in this current installation.

86
00:05:12,405 --> 00:05:15,353
If you are using other CRAN installation of R,

87
00:05:15,353 --> 00:05:17,706
RevoScaleR would not be available.

88
00:05:17,706 --> 00:05:20,203
And it's not something that we can download and

89
00:05:20,203 --> 00:05:22,770
install from CRAN using install that packages.

90
00:05:24,640 --> 00:05:29,980
If you are not pointing to the right R client installations,

91
00:05:29,980 --> 00:05:38,190
you would just go to Tools, Options, and here we're in R Tools Advanced.

92
00:05:38,190 --> 00:05:41,500
And we can change that right now from the other set to

93
00:05:41,500 --> 00:05:45,980
to C:Program Files/MicrosoftRClient/ R_SERVER.

94
00:05:47,290 --> 00:05:51,440
If you installed the R client in the default directory like I did,

95
00:05:51,440 --> 00:05:54,530
then it should be pointing to the same location.

96
00:05:54,530 --> 00:05:59,360
If it's not, you can simply click here and hit the three dots.

97
00:06:01,020 --> 00:06:05,230
And then from here we can go to whichever location you selected for

98
00:06:05,230 --> 00:06:06,130
the R client.

99
00:06:06,130 --> 00:06:09,079
For me it's just Microsoft R Client.

100
00:06:09,079 --> 00:06:14,461
R Server, and here I'm gonna say select folder, hit OK.

101
00:06:14,461 --> 00:06:19,328
And you'll might get a message that's said that you need to

102
00:06:19,328 --> 00:06:21,382
restart Visual Studio.

103
00:06:21,382 --> 00:06:26,177
But otherwise once we restart, we're gonna be pointing Visual Studio now

104
00:06:26,177 --> 00:06:28,393
to the R Client installation of R.

