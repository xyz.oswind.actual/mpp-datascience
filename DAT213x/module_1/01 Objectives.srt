0
00:00:01,680 --> 00:00:06,040
Let set the stage for why we need to use Microsoft R.

1
00:00:06,040 --> 00:00:08,790
And to do that, we're gonna begin with R itself.

2
00:00:10,100 --> 00:00:13,550
R is a popular statistical programming language that's been

3
00:00:13,550 --> 00:00:17,530
around for a while, with a very rich set of features for

4
00:00:17,530 --> 00:00:19,980
data visualization and data analysis.

5
00:00:19,980 --> 00:00:24,300
The strength of R lies in the huge community that is behind it, and

6
00:00:24,300 --> 00:00:28,150
that community over the years has contributed to thousands and

7
00:00:28,150 --> 00:00:30,290
thousands of packages.

8
00:00:30,290 --> 00:00:34,060
These packages run the gamut of various applications.

9
00:00:34,060 --> 00:00:37,950
However, R comes with its own set of challenges.

10
00:00:37,950 --> 00:00:42,480
Traditionally, data in R has to be loaded as a data frame,

11
00:00:42,480 --> 00:00:44,200
which is a memory object.

12
00:00:44,200 --> 00:00:49,000
This means, that the amount of data that we can load into R is limited

13
00:00:49,000 --> 00:00:53,110
to the amount of memory that we have available on our machine.

14
00:00:53,110 --> 00:00:55,150
This of course, is a serious challenge for

15
00:00:55,150 --> 00:00:58,180
enterprises that deal with large data sets.

16
00:00:58,180 --> 00:01:03,130
Even when enough memory is available for us to load our data,

17
00:01:03,130 --> 00:01:07,280
large data sets can quickly run into performance issues as well.

18
00:01:07,280 --> 00:01:11,452
This is a especially true when modeling and analytics is involved.

19
00:01:11,452 --> 00:01:14,790
The lack of commercial support has also been a challenge for

20
00:01:14,790 --> 00:01:17,240
enterprises wanting to adopt R.

21
00:01:17,240 --> 00:01:22,150
Last but not least, the ability to run R in a production environment

22
00:01:22,150 --> 00:01:26,950
has been another major hurdle to get R's foot in the door of industry.

