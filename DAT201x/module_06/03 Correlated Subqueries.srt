0
00:00:02,310 --> 00:00:05,540
We&#39;re going to have a look now about whether something is self-contained or 

1
00:00:05,540 --> 00:00:08,910
correlated. Now that&#39;s a very distinct thing. So 

2
00:00:08,910 --> 00:00:12,769
a query we&#39;ve talked about so far is a self-contained query.

3
00:00:12,769 --> 00:00:16,209
So the subquery portion

4
00:00:16,209 --> 00:00:21,769
can be run and it works, and then it supplies its value to the calling

5
00:00:21,769 --> 00:00:26,019
outer query. And you can run them independently. So I can run a value

6
00:00:26,019 --> 00:00:29,649
and then we can test it. Now this

7
00:00:29,649 --> 00:00:35,780
correlated idea is slightly different. So what we&#39;re going to do,

8
00:00:35,780 --> 00:00:38,860
if you have a look at the subquery portion of this, we&#39;ve got select maximum order

9
00:00:38,860 --> 00:00:39,330
date

10
00:00:39,330 --> 00:00:43,430
from sales orders as O2. That&#39;s fine. Then we&#39;ve got WHERE O2.empid

11
00:00:43,430 --> 00:00:47,490
equals O1.empid. Well, O1.empid doesn&#39;t exist within that

12
00:00:47,490 --> 00:00:48,160
query.

13
00:00:48,160 --> 00:00:51,300
So it&#39;s going to go to the outer query

14
00:00:51,300 --> 00:00:56,420
where you&#39;ll see that O1 does exist, get the employee ID value from there,

15
00:00:56,420 --> 00:00:59,870
and use that. So essentially what happens is

16
00:00:59,870 --> 00:01:03,000
for each row in the

17
00:01:03,000 --> 00:01:07,140
outer query, return me an employee ID

18
00:01:07,140 --> 00:01:12,380
and run the subquery using that value, and then go backwards, logically you&#39;re

19
00:01:12,380 --> 00:01:14,249
going backwards and forwards each time. So for

20
00:01:14,249 --> 00:01:17,319
each row of the outer query, supply the  value,

21
00:01:17,319 --> 00:01:21,100
run the subquery, then do the comparison of the outer query, 

22
00:01:21,100 --> 00:01:24,950
then go to the next row, do it again. Backwards and forwards each time.

23
00:01:24,950 --> 00:01:28,770
I presume because SQL Server is set  based, that&#39;s not actually how it&#39;s executes it.

24
00:01:28,770 --> 00:01:32,409
It&#39;s not, however, they do tend to be quite

25
00:01:32,409 --> 00:01:36,109
intensive in your resources. So they&#39;re not 

26
00:01:36,109 --> 00:01:39,560
the highest performing. It&#39;s something you think, you wouldn&#39;t think I&#39;ll do a

27
00:01:39,560 --> 00:01:41,850
correlated subquery just because I can.

28
00:01:41,850 --> 00:01:45,740
There is a logical backwards and forwards process,

29
00:01:45,740 --> 00:01:49,709
actually SQL Server has amazing optimization abilities and

30
00:01:49,709 --> 00:01:53,170
and actually doesn&#39;t do quite

31
00:01:53,170 --> 00:01:57,270
anything that complex. Right, I see. The logic and also

32
00:01:57,270 --> 00:02:00,560
the logic of them as well, I think  whenever you see one,

33
00:02:00,560 --> 00:02:04,659
when you get to understand them quite well, you can work out what it does but it takes a

34
00:02:04,659 --> 00:02:05,649
bit of thought to think

35
00:02:05,649 --> 00:02:09,550
okay what is that actually doing in there? I have to have a look at that and see

36
00:02:09,550 --> 00:02:14,510
what&#39;s going on. So it&#39;s logically, I wouldn&#39;t use them just because you think oh well nowadays

37
00:02:14,510 --> 00:02:19,409
they&#39;re not as slow as they used to be. It&#39;s something that is a bit slower and

38
00:02:19,409 --> 00:02:22,480
it&#39;s logically quite complex. But it can do something that is quite complicated

39
00:02:22,480 --> 00:02:26,709
because what we&#39;re looking for here is for each employee ID, show me

40
00:02:26,709 --> 00:02:32,560
the values where the order date is equal to the maximum order date for that employee.

41
00:02:32,560 --> 00:02:36,420
So the last order that employee took in English is what we&#39;re saying. Yeah.

42
00:02:36,420 --> 00:02:36,829
Okay.

