0
00:00:02,220 --> 00:00:05,399
Now that we&#39;ve talked about correlated subqueries, there is

1
00:00:05,399 --> 00:00:09,830
a technique we can use with an APPLY operator and a table-valued function.

2
00:00:09,830 --> 00:00:13,890
Which can get us -- it can do a number of things actually, but one of the things it can do is

3
00:00:13,890 --> 00:00:15,740
get us a very similar result

4
00:00:15,740 --> 00:00:18,929
in maybe a less complex piece of code.

5
00:00:18,929 --> 00:00:24,310
You can use it to do something which is essentially like a JOIN as well,

6
00:00:24,310 --> 00:00:27,779
but here we&#39;re going to use it to do something we can&#39;t do with joins,

7
00:00:27,779 --> 00:00:32,239
that we could only achieve with the correlated subquery. And that&#39;s using a 

8
00:00:32,238 --> 00:00:32,989
CROSS APPLY

9
00:00:32,989 --> 00:00:36,080
operator and

10
00:00:36,080 --> 00:00:39,590
we going to pass a value into

11
00:00:39,590 --> 00:00:42,860
a function. Now this function is a function that returns tables.

12
00:00:42,860 --> 00:00:46,800
So S.supplierid

13
00:00:46,800 --> 00:00:50,940
does not exist within the function.

14
00:00:50,940 --> 00:00:54,360
It won&#39;t know what it is. So it&#39;s going to again say well where do I get that from?

15
00:00:54,360 --> 00:00:58,670
Oh, it comes from the calling query. And S is suppliers so it goes ok that&#39;s 

16
00:00:58,670 --> 00:01:01,720
suppliers.supplierid. Okay, I can see that.

17
00:01:01,720 --> 00:01:06,770
I&#39;ll get that, I&#39;ll run on that value, then I&#39;ll return the value to the

18
00:01:06,770 --> 00:01:10,750
calling query, then I&#39;ll go to the next line.

19
00:01:10,750 --> 00:01:13,850
And we&#39;ll run it in that way. In the same way that we created a subquery

20
00:01:13,850 --> 00:01:17,350
backwards and forwards, run it for the next supplier id in there.

21
00:01:17,350 --> 00:01:21,310
So conceptually the CROSS APPLY  then is running the function

22
00:01:21,310 --> 00:01:24,870
for each row with a value that we&#39;re getting for each row.

23
00:01:24,870 --> 00:01:29,200
Yes, so each individual supplier id it will go get 

24
00:01:29,200 --> 00:01:32,440
another value to run it again, another value to run it again in there.

25
00:01:32,440 --> 00:01:35,890
Now we also have 

26
00:01:35,890 --> 00:01:39,830
and OUTER APPLY. So as well as CROSS APPLY, we can use OUTER APPLY.

27
00:01:39,830 --> 00:01:43,190
And then what we have is extra

28
00:01:43,190 --> 00:01:46,680
values for the right table. So if you take a

29
00:01:46,680 --> 00:01:50,200
LEFT and a RIGHT JOIN, in here we&#39;re going to basically say

30
00:01:50,200 --> 00:01:54,790
what we want is a LEFT JOIN, a LEFT OUTER JOIN. I want nulls in that right

31
00:01:54,790 --> 00:01:55,369
table

32
00:01:55,369 --> 00:01:59,400
because they don&#39;t exist, because I don&#39;t have any values for that supplier id.

33
00:01:59,400 --> 00:02:00,159
I would like to.

34
00:02:00,159 --> 00:02:03,540
The right table in this case being the table that&#39;s returned by

35
00:02:03,540 --> 00:02:04,659
the function.

36
00:02:04,659 --> 00:02:08,190
The P table in this case. So it would be P.productid, product name

37
00:02:08,190 --> 00:02:12,620
unit price. Those values would be null where we don&#39;t have matching

38
00:02:12,620 --> 00:02:15,049
records in the same way we would with an OUTER JOIN.

