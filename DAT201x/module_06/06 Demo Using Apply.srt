0
00:00:04,649 --> 00:00:05,979
What we have here then

1
00:00:05,979 --> 00:00:10,420
is a function which is udfMaxUnitPrice.

2
00:00:10,420 --> 00:00:13,969
We can run that against any sales

3
00:00:13,969 --> 00:00:18,100
order ID and it will go out and it will find from sales order detail,

4
00:00:18,100 --> 00:00:22,670
the maximum unit price for that sales order id

5
00:00:22,670 --> 00:00:25,920
from the sales order detail table. 

6
00:00:25,920 --> 00:00:29,789
But I&#39;m running at on SOH.SalesOrderID.

7
00:00:29,789 --> 00:00:33,770
Well, before we do that, I know in this course we&#39;re not covering

8
00:00:33,770 --> 00:00:37,410
how to create functions or design functions. But can we at least see the

9
00:00:37,410 --> 00:00:40,690
definition of that function so we get an idea for what is being returned by it?

10
00:00:40,690 --> 00:00:44,480
I&#39;m glad you mentioned that because  already happened to have that in another,

11
00:00:44,480 --> 00:00:48,360
already up my sleeve, another one here. &lt;laughter&gt; We have

12
00:00:48,360 --> 00:00:51,700
sp_help text. So what we can do with this is

13
00:00:51,700 --> 00:00:55,570
get it to return the syntax

14
00:00:55,570 --> 00:01:00,710
of that function. Okay, so let&#39;s have a look at that. Now as you said we&#39;re not

15
00:01:00,710 --> 00:01:02,040
actually using this

16
00:01:02,040 --> 00:01:06,149
to create functions at the moment, but we can certainly see

17
00:01:06,149 --> 00:01:10,740
that it returns the sales order ID, the maximum unit price,

18
00:01:10,740 --> 00:01:14,319
and it&#39;s going to get that where the sales order id 

19
00:01:14,319 --> 00:01:18,189
is equal to @SalesOrderID. So that&#39;s actually a variable that&#39;s being passed in.

20
00:01:18,189 --> 00:01:21,350
We&#39;re passing it that value, SalesOrderID, 

21
00:01:21,350 --> 00:01:24,729
and from that it&#39;s going to return the max unit price. Okay.

22
00:01:24,729 --> 00:01:28,060
So it&#39;s quite straightforward to be honest. It&#39;s just the most expensive item in the order

23
00:01:28,060 --> 00:01:29,719
gets passed back. Yeah, exactly. 

24
00:01:29,719 --> 00:01:33,429
And is doing that from sales order detail where in the CROSS APPLY you see

25
00:01:33,429 --> 00:01:35,389
that we&#39;re actually look at sales order  header.

26
00:01:35,389 --> 00:01:40,060
So it&#39;s saying for values in sales order header, go and look at the sale order ID and find

27
00:01:40,060 --> 00:01:41,590
me the maximum unit price.

28
00:01:41,590 --> 00:01:46,490
Right, so we&#39;re saving our self a more complex join here where we&#39;re joining to another table.

29
00:01:46,490 --> 00:01:51,319
Yes, right, so you would have to join together, and also we&#39;d have to aggregate values

30
00:01:51,319 --> 00:01:52,169
because we&#39;re going to have to look at the

31
00:01:52,169 --> 00:01:56,179
maximum. And you&#39;ll say well that&#39;s not quite right because the maximum sales

32
00:01:56,179 --> 00:02:01,399
is not I want, I want it for each individual order header.

33
00:02:01,399 --> 00:02:05,639
So that&#39;s where we&#39;re back to correlated subqueries again.

34
00:02:05,639 --> 00:02:09,319
And this is an alternative way. We could do this with the correlated subquery.

35
00:02:09,318 --> 00:02:12,760
But we can also do it using this approach. Again you are still having the

36
00:02:12,760 --> 00:02:14,340
idea of passing things from

37
00:02:14,340 --> 00:02:18,910
calling query to the essentially is not a subquery, but passing it to the 

38
00:02:18,910 --> 00:02:22,230
function which is actually as a subquery in this case.

39
00:02:22,230 --> 00:02:26,590
Okay. So there&#39;s some concepts that are quite similar, but a lot of people

40
00:02:26,590 --> 00:02:29,849
think this is conceptually more straightforward and it&#39;s certainly easier to

41
00:02:29,849 --> 00:02:30,530
read.

42
00:02:30,530 --> 00:02:34,799
And you think, I can see, once and assuming you understand the function does,

43
00:02:34,799 --> 00:02:38,170
I can understand that. Effectively think

44
00:02:38,170 --> 00:02:41,549
of it as being I&#39;m running that function for each row that the query 

45
00:02:41,549 --> 00:02:42,349
returns.

46
00:02:42,349 --> 00:02:45,690
Absolutely. So if we execute this we should get the same sort of results.

47
00:02:45,690 --> 00:02:49,220
And there we go, for each sales order id, there&#39;s the maximum unit price.

48
00:02:49,220 --> 00:02:52,260
It&#39;s the same results that we would get using a correlated subquery.

