0
00:00:02,630 --> 00:00:06,839
So we&#39;ve had a look at subqueries. So queries within queries, remember that

1
00:00:06,839 --> 00:00:11,870
when we started, we talked about scalar/multivalued but self-contained

2
00:00:11,870 --> 00:00:13,080
subqueries and 

3
00:00:13,080 --> 00:00:17,330
the key thing I would say is that the vast majority of subqueries

4
00:00:17,330 --> 00:00:20,859
are not correlated. So that you can test them

5
00:00:20,859 --> 00:00:24,330
as self-contained queries. So you can try them out

6
00:00:24,330 --> 00:00:28,070
that works, that returns these results. From that, I can then

7
00:00:28,070 --> 00:00:32,910
craft an outer query which uses those results. So it&#39;s much more easy to test.

8
00:00:32,910 --> 00:00:36,410
There are cases where that&#39;s not possible so then we&#39;ll have a look at

9
00:00:36,410 --> 00:00:37,489
self-contained

10
00:00:37,489 --> 00:00:40,950
versus correlated. And you may need a correlated subquery. You may 

11
00:00:40,950 --> 00:00:44,790
need to use an APPLY with a table- value function. But the majority of them

12
00:00:44,790 --> 00:00:48,739
are probably going to be those self-contained ones. Alright, fantastic. Well, thanks!

13
00:00:48,739 --> 00:00:49,920
That&#39;s a 

14
00:00:49,920 --> 00:00:52,989
really good introduction to subqueries

15
00:00:52,989 --> 00:00:56,090
and to the APPLY operator.

16
00:00:56,090 --> 00:00:59,480
To take that a little bit further and experiment a little bit on your own,

17
00:00:59,480 --> 00:01:02,570
as always in this course there&#39;s a lab that you can have a go at.

18
00:01:02,570 --> 00:01:06,220
So if you haven&#39;t already done so, download the lab files, set up your

19
00:01:06,220 --> 00:01:07,390
environment using

20
00:01:07,390 --> 00:01:11,210
Azure SQL Database and the AdventureWorksLT database and

21
00:01:11,210 --> 00:01:14,240
have a go at the lab and try some this for yourself.

22
00:01:14,240 --> 00:01:18,439
And then when you&#39;ve had a go at that,  come back in and join us for the next

23
00:01:18,439 --> 00:01:19,170
module

24
00:01:19,170 --> 00:01:21,020
in this course on Transact-SQL querying.

