0
00:00:05,930 --> 00:00:08,080
Well we&#39;re all the way up to module six

1
00:00:08,080 --> 00:00:12,670
in our Querying with Transact-SQL course. So far we&#39;ve covered the

2
00:00:12,670 --> 00:00:13,809
SELECT statement,

3
00:00:13,809 --> 00:00:18,960
selecting from single tables, selecting from multiple tables using joins,

4
00:00:18,960 --> 00:00:23,400
using unions to get the results of two queries put together into a single

5
00:00:23,400 --> 00:00:24,300
row set,

6
00:00:24,300 --> 00:00:27,519
and in the previous module, we looked at

7
00:00:27,519 --> 00:00:31,650
using functions to get additional information from the values in our

8
00:00:31,650 --> 00:00:32,450
databases

9
00:00:32,450 --> 00:00:36,000
and then aggregating those functions and grouping the rows that are returned

10
00:00:36,000 --> 00:00:39,440
in the result set. So we&#39;re already building reasonably sophisticated

11
00:00:39,440 --> 00:00:40,470
queries,

12
00:00:40,470 --> 00:00:44,620
what we&#39;re going to look at in this module is using subqueries

13
00:00:44,620 --> 00:00:48,400
and a special SQL operator called the the APPLY operator.

14
00:00:48,400 --> 00:00:51,890
And what we&#39;re going to do here is see how we can take those queries we&#39;re writing 

15
00:00:51,890 --> 00:00:53,930
have queries within queries, which

16
00:00:53,930 --> 00:00:57,420
sounds a little bit meta, so Jeff can you

17
00:00:57,420 --> 00:01:00,740
tell us a little bit about what we&#39;re going to cover? Indeed, we&#39;ll have a look

18
00:01:00,740 --> 00:01:04,070
at subqueries and we&#39;ll talk about them in  general 

19
00:01:04,069 --> 00:01:07,689
context and then we&#39;ll have a look at scalar subqueries where we&#39;re going to look

20
00:01:07,689 --> 00:01:11,250
at single values, and multivalued subqueries where we&#39;re looking at lots of values.

21
00:01:11,250 --> 00:01:14,680
And then we also have a self-contained or typical subquery if you

22
00:01:14,680 --> 00:01:16,980
like, and a special type called a correlated subquery.

23
00:01:16,980 --> 00:01:20,190
And then going on to that another way of dealing with that information is using the

24
00:01:20,190 --> 00:01:20,909
APPLY

25
00:01:20,909 --> 00:01:25,140
operator with table-value functions to return similar output.

26
00:01:25,140 --> 00:01:29,610
Sounds good. So to start off with they&#39;re queries within queries. 

27
00:01:29,610 --> 00:01:33,340
So we get a single query, select list

28
00:01:33,340 --> 00:01:36,450
as normal. So highlighted

29
00:01:36,450 --> 00:01:40,110
down here, we&#39;ve got a select query and that then

30
00:01:40,110 --> 00:01:43,510
returns its results to a calling query.

31
00:01:43,510 --> 00:01:47,970
So that could be a set of multiple different 

32
00:01:47,970 --> 00:01:52,190
values, it could be a single value. You have different types.

33
00:01:52,190 --> 00:01:56,360
So results are passed to the outer query,

34
00:01:56,360 --> 00:01:59,950
you can test them. A typical and a non-correlated one you could test it, you can run that

35
00:01:59,950 --> 00:02:01,240
subquery, it should work

36
00:02:01,240 --> 00:02:05,740
and then you can try it in the outer query, or you could even actually take the results of that

37
00:02:05,740 --> 00:02:08,819
subquery and say well, actually if I just type them in, does it work in

38
00:02:08,818 --> 00:02:12,200
on the outer query. So we&#39;ve got the results of the inner query,

39
00:02:12,200 --> 00:02:17,250
and we want to have a look at what sort of results they will be. They could be

40
00:02:17,250 --> 00:02:21,530
a single value. So in here I&#39;ve got select maximum order ID

41
00:02:21,530 --> 00:02:25,430
as lastorder from sales.order. So we&#39;re looking at the maximum order ID, one single value.

42
00:02:25,430 --> 00:02:27,819
We&#39;re then passing that to the calling query

43
00:02:27,819 --> 00:02:31,260
and comparing the values in that outer query

44
00:02:31,260 --> 00:02:35,500
with the results of that subquery. So this is like a scalar function that we talked about

45
00:02:35,500 --> 00:02:36,590
previously.

46
00:02:36,590 --> 00:02:41,400
Exactly. We&#39;re basically using this  almost as a function, we&#39;re just saying ok

47
00:02:41,400 --> 00:02:44,720
I want to find a function which finds me the maximum value

48
00:02:44,720 --> 00:02:47,920
in the order table of the order id, and I&#39;m just using that as a result,

49
00:02:47,920 --> 00:02:53,330
a single results. So I could actually run that subquery, say oh actually what I returned was 979,

50
00:02:53,330 --> 00:02:56,870
and then as a test say what happens if I put WHERE order id 

51
00:02:56,870 --> 00:02:58,299
equals 979?

52
00:02:58,299 --> 00:03:02,470
We&#39;ll get exactly the same results. So it&#39;s entirely encapsulated in brackets

53
00:03:02,470 --> 00:03:02,959
and it just

54
00:03:02,959 --> 00:03:06,480
generates that value, compares that value with the outer query. 

55
00:03:06,480 --> 00:03:09,150
And as it says there, I can use it anywhere that I would use a function

56
00:03:09,150 --> 00:03:11,070
basically. Completely. You can --

57
00:03:11,070 --> 00:03:14,720
that bracketed part there, you can actually

58
00:03:14,720 --> 00:03:19,000
drop it in, literal anyway, you can put it into a select list even,

59
00:03:19,000 --> 00:03:23,519
you can put it anywhere you want. Once in brackets it just treats that as an

60
00:03:23,519 --> 00:03:27,840
individual unit. Now the multivalued one, they&#39;re a little bit different because here we

61
00:03:27,840 --> 00:03:28,900
getting multiple

62
00:03:28,900 --> 00:03:32,940
different entries returned. So I&#39;ve got a query there that returns

63
00:03:32,940 --> 00:03:37,220
every customer ID for anyone with a  country/region of Mexico.

64
00:03:37,220 --> 00:03:41,890
So that&#39;s going to be a long list of a entries in there. So I can&#39;t just say is

65
00:03:41,890 --> 00:03:43,130
something equal to this.

66
00:03:43,130 --> 00:03:46,220
But remember when we had 

67
00:03:46,220 --> 00:03:50,600
that ability to search for multiple values and rather than using an ORs we used an IN

68
00:03:50,600 --> 00:03:54,639
and then a bracketed list. Essentially, my bracketed list now is the results

69
00:03:54,639 --> 00:03:57,870
all this query. Right. So it can supply

70
00:03:57,870 --> 00:04:01,730
a list of values and this one I obviously couldn&#39;t use in other places

71
00:04:01,730 --> 00:04:04,019
necessarily because I&#39;m returning multiple values.

72
00:04:04,019 --> 00:04:06,510
So you could think of this, I mean I guess what we&#39;re saying is

73
00:04:06,510 --> 00:04:07,609
a multi-valued subquery

74
00:04:07,609 --> 00:04:11,480
is returning, you could think of it as a being table that has one column.

75
00:04:11,480 --> 00:04:15,720
And so if you&#39;re coming from a database developer background, 

76
00:04:15,720 --> 00:04:17,000
that&#39;s a way of thinking of it. It&#39;s a row set

77
00:04:17,000 --> 00:04:20,680
with one column and a value in each row. And it has to be that way around, you can&#39;t say

78
00:04:20,680 --> 00:04:24,560
oh, could I use it on one row with multiple columns, right, no it&#39;s one column

79
00:04:24,560 --> 00:04:25,370
with multiple rows.

80
00:04:25,370 --> 00:04:28,170
And actually if you&#39;re a developer and you&#39;re attending this course because

81
00:04:28,170 --> 00:04:29,280
you&#39;re already building

82
00:04:29,280 --> 00:04:33,140
applications in C#, we&#39;re really talking here about the equivalent of a one-dimensional

83
00:04:33,140 --> 00:04:33,580
array.

84
00:04:33,580 --> 00:04:37,820
It&#39;s an array of values that&#39;s coming back. Yeah, it is. Essentially just a list

85
00:04:37,820 --> 00:04:41,910
of values returned, and I&#39;m not looking for value that are in that

86
00:04:41,910 --> 00:04:44,530
list. Okay, yeah I see.

