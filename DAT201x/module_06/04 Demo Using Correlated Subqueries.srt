0
00:00:03,560 --> 00:00:05,799
Okay now I&#39;ve got 

1
00:00:05,799 --> 00:00:09,509
a list in here where we&#39;re going to start off

2
00:00:09,509 --> 00:00:12,929
with a simple query. So I&#39;ve got order ID, sales order ID,

3
00:00:12,929 --> 00:00:17,630
order date, and it&#39;s just going do an ORDER BY in there. So there&#39;s nothing complex about this at all.

4
00:00:17,630 --> 00:00:20,810
Then to take that step further,

5
00:00:20,810 --> 00:00:23,940
we&#39;re going to see actually I&#39;m interested only

6
00:00:23,940 --> 00:00:28,090
where the order date is the same as the maximum order date

7
00:00:28,090 --> 00:00:31,340
for everyone from the

8
00:00:31,340 --> 00:00:37,230
sales order table. So we&#39;re just doing that for absolutely everyone. So I only want,

9
00:00:37,230 --> 00:00:38,469
I&#39;m only interested in

10
00:00:38,469 --> 00:00:41,609
where it&#39;s equal to the absolute maximum grand total.

11
00:00:41,609 --> 00:00:45,359
But that&#39;s not what I&#39;m interested in really, what I want to do is to find

12
00:00:45,359 --> 00:00:50,870
the totals for each customer id. So we&#39;re going to go through here.

13
00:00:50,870 --> 00:00:56,350
And now my subquery has got this clause where it&#39;s saying WHERE SO2 customer id

14
00:00:56,350 --> 00:00:57,969
equals SO1 customer id.

15
00:00:57,969 --> 00:01:02,289
Now SO2 is defined within that query. That&#39;s fine.

16
00:01:02,289 --> 00:01:06,770
SO1 it won&#39;t find. So it&#39;s going to look to the calling query and say okay

17
00:01:06,770 --> 00:01:11,740
I need the customer id from there. So for  each customer id you have

18
00:01:11,740 --> 00:01:17,070
in there, give me the maximum order date. So now we&#39;re looking at by customer

19
00:01:17,070 --> 00:01:20,500
not the grand total, for each

20
00:01:20,500 --> 00:01:23,920
individual customer give me the value. So I got the customer id,

21
00:01:23,920 --> 00:01:28,610
then I&#39;ve got the value which has got the maximum order date there 

22
00:01:28,610 --> 00:01:34,010
for that customer. One row per customer. So it&#39;s returning those values from the

23
00:01:34,010 --> 00:01:38,850
inner query to the calling query, backwards and forwards because we&#39;ve got a value in there that doesn&#39;t

24
00:01:38,850 --> 00:01:42,360
exist in the subquery. Probably the most complex thing here is that they can&#39;t be

25
00:01:42,360 --> 00:01:42,940
tested.

26
00:01:42,940 --> 00:01:47,400
So if you run it and doesn&#39;t work, it&#39;s quite difficult, you can&#39;t do that step

27
00:01:47,400 --> 00:01:50,750
by step process of saying well let&#39;s try the subquery, does it work? Well, no it doesn&#39;t.

28
00:01:50,750 --> 00:01:54,570
Because there&#39;s a dependency on the outer query. Yeah, so it&#39;s 

29
00:01:54,570 --> 00:01:58,670
not a query where we&#39;ve said you can build one&#39;s up, this you can&#39;t.

30
00:01:58,670 --> 00:02:00,050
You&#39;re basically starting

31
00:02:00,050 --> 00:02:03,430
from here. This is my first point that I could start with,

32
00:02:03,430 --> 00:02:05,620
so it&#39;s already reasonably complex in there. 

