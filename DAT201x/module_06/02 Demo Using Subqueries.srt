0
00:00:04,590 --> 00:00:07,630
So we&#39;re going to start with a scalar subquery.

1
00:00:07,630 --> 00:00:12,150
Here we&#39;re going to build this by stages. So

2
00:00:12,150 --> 00:00:18,100
I&#39;ve got firstly the maximum unit price. So what is the maximum unit price

3
00:00:18,100 --> 00:00:21,439
in the sales order detail table? 1467.01.

4
00:00:21,439 --> 00:00:26,380
So then we can say well what would be

5
00:00:26,380 --> 00:00:30,249
the results if I actually typed that value in?

6
00:00:30,249 --> 00:00:34,360
So let&#39;s have a look of products who have a list price greater than that

7
00:00:34,360 --> 00:00:39,280
maximum unit price from sales order details. So then we can search for that,

8
00:00:39,280 --> 00:00:43,540
and that will find me any products who&#39;ve got that list price.

9
00:00:43,540 --> 00:00:47,900
And then I can then say, ok well rather  than typing in, it&#39;s obviously not a very

10
00:00:47,900 --> 00:00:52,040
scaleable solution to have people typing things in

11
00:00:52,040 --> 00:00:55,670
for different values, I would like it to work whenever. So instead of typing 

12
00:00:55,670 --> 00:00:57,320
that value in, I just replace

13
00:00:57,320 --> 00:01:01,550
that with the query in brackets. So we&#39;ve got a query which generated the value,

14
00:01:01,550 --> 00:01:05,329
we drop that in where the bracket, where the value was in there.

15
00:01:05,328 --> 00:01:08,650
So what we&#39;re saying then really is bring me back all the details of the

16
00:01:08,650 --> 00:01:09,630
Products table

17
00:01:09,630 --> 00:01:14,140
for any product that has a list price that&#39;s higher than whatever the maximum

18
00:01:14,140 --> 00:01:15,860
price of something we&#39;ve ever sold is.

19
00:01:15,860 --> 00:01:19,970
Yes. So it&#39;s going off and having a look at, essentially we&#39;re saying are

20
00:01:19,970 --> 00:01:23,860
we overcharging for some products? So we&#39;ve gone out and found the maximum unit price

21
00:01:23,860 --> 00:01:27,620
from sales order detail, that&#39;s the maximum we&#39;ve sold things from,

22
00:01:27,620 --> 00:01:32,820
what do we&#39;ve got that&#39;s more expensive than that? And you&#39;ll see if I run this one,

23
00:01:32,820 --> 00:01:37,729
then we get the same number rows, exactly the same rows.

24
00:01:37,729 --> 00:01:42,100
So essentially you can use that  step-by-step process to build up

25
00:01:42,100 --> 00:01:46,440
a typical non-correlated subquery, you can

26
00:01:46,440 --> 00:01:50,409
say ok let&#39;s run the subquery part, that works. Right, just take its value,

27
00:01:50,409 --> 00:01:53,810
use its value in the outer query. Does  that work? Yeah, that works. 

28
00:01:53,810 --> 00:01:57,820
So I&#39;m effectively replacing a function just with some inline SQL

29
00:01:57,820 --> 00:01:58,130
that

30
00:01:58,130 --> 00:02:01,770
does the same job as a scalar function, it returns a single value that I then use

31
00:02:01,770 --> 00:02:03,580
in the outer query. Yeah. Okay.

