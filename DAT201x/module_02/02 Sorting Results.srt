0
00:00:02,290 --> 00:00:05,970
And then we&#39;ll have a look sorting results. Now we mentioned this before,

1
00:00:05,970 --> 00:00:10,299
quite often it appears that you have the correct order.

2
00:00:10,299 --> 00:00:14,000
Normally, the order they come out will be ordered the primary key.

3
00:00:14,000 --> 00:00:17,190
So if I&#39;ve got a product id

4
00:00:17,190 --> 00:00:20,960
normally they&#39;ll be in product id order. But we can&#39;t guarantee that

5
00:00:20,960 --> 00:00:25,119
and it may be that actually if you delete some products from

6
00:00:25,119 --> 00:00:29,269
earlier in the numbers, and then later they get reinserted into your database

7
00:00:29,269 --> 00:00:31,609
they might actually be later

8
00:00:31,609 --> 00:00:34,780
in the result set. So although it appears correct,

9
00:00:34,780 --> 00:00:38,629
we can&#39;t guarantee correctness. So we can use ORDER BY,

10
00:00:38,629 --> 00:00:42,000
by default another word that you don&#39;t see very much is

11
00:00:42,000 --> 00:00:45,610
ASC which is ascending. So typically our ORDER BY 

12
00:00:45,610 --> 00:00:49,260
would go ascending say from 0 to 100.

13
00:00:49,260 --> 00:00:52,480
So that&#39;s the default. That&#39;s the default and we don&#39;t tend to type it

14
00:00:52,480 --> 00:00:53,140
because

15
00:00:53,140 --> 00:00:56,750
we don&#39;t need to type it and you expect things to go 

16
00:00:56,750 --> 00:00:57,320
A to Zed.

17
00:00:57,320 --> 00:01:00,809
Right. But if you want them to go Zed to A, or Z to A

18
00:01:00,809 --> 00:01:06,240
depending on where we are in the world, then we can do DESC for descending. So here we&#39;re saying

19
00:01:06,240 --> 00:01:09,250
I would like the Category, I would like the ProductName

20
00:01:09,250 --> 00:01:13,170
and I would like to order it by Category ascending

21
00:01:13,170 --> 00:01:17,400
and then by price descending. So it would only do it by price

22
00:01:17,400 --> 00:01:20,630
if you&#39;ve already ordered it by category. So it&#39;s ordered by Category first, 

23
00:01:20,630 --> 00:01:24,370
and then where we&#39;ve got the same Category, then order by price descending.

24
00:01:24,370 --> 00:01:28,090
So one ascending and one descending, we can do that. 

25
00:01:28,090 --> 00:01:32,460
The other thing we&#39;ve got in there is there&#39;s aliases. So we&#39;ve got category alias.

26
00:01:32,460 --> 00:01:36,440
Remember the order things are done, we can use the alias

27
00:01:36,440 --> 00:01:41,180
in ORDER BY because it is being defined. The ORDER BY happens after the SELECT.

28
00:01:41,180 --> 00:01:45,380
The SELECT is the second last thing done, so the ORDER BY knows the aliases I&#39;ve

29
00:01:45,380 --> 00:01:46,760
used in the SELECT.

30
00:01:46,760 --> 00:01:51,300
So we&#39;re fine to use it, we could equally do 

31
00:01:51,300 --> 00:01:55,730
ORDER BY ProductCategory, we don&#39;t have to use an alias all the time. Sometimes

32
00:01:55,730 --> 00:01:58,390
you do though there are times when it will

33
00:01:58,390 --> 00:02:03,650
completely override it, but it&#39;s one of those, as I said before if you make a

34
00:02:03,650 --> 00:02:06,020
mistake, you quite enough get used to it and say okay well

35
00:02:06,020 --> 00:02:08,910
I haven&#39;t used names or I haven&#39;t used an alias and 

36
00:02:08,910 --> 00:02:15,070
switch backwards and forwards. So now we&#39;re going to have a look at saying

37
00:02:15,070 --> 00:02:18,290
I don&#39;t want all of the results.

38
00:02:18,290 --> 00:02:21,680
I&#39;m interested in our most expensive products.

39
00:02:21,680 --> 00:02:25,190
I don&#39;t want a thousand products. That&#39;s just very like you to be interested in the 

40
00:02:25,190 --> 00:02:25,940
expensive products. &lt;laughter&gt;

41
00:02:25,940 --> 00:02:29,030
Says the person with the 

42
00:02:29,030 --> 00:02:32,850
new Surface. &lt;laughter&gt; 

43
00:02:32,850 --> 00:02:36,030
Yeah, so we&#39;re only interested in the top most

44
00:02:36,030 --> 00:02:39,140
say 5 or 10 most expensive products we do.

45
00:02:39,140 --> 00:02:42,580
We could return to a client application

46
00:02:42,580 --> 00:02:47,210
all of them sorted and they could then just display the first five rows they get.

47
00:02:47,210 --> 00:02:49,750
That&#39;s pretty straightforward for any cloud application to do.

48
00:02:49,750 --> 00:02:53,660
But I&#39;ve just transferred, potentially over the Internet

49
00:02:53,660 --> 00:02:58,140
or at least from a server to a client, potentially

50
00:02:58,140 --> 00:03:01,170
100 hundreds of thousands of products. It could be

51
00:03:01,170 --> 00:03:04,400
vast numbers of products. I might be a  multinational supermarket.

52
00:03:04,400 --> 00:03:09,030
So I don&#39;t want to do that. I want to limit the amount of information I&#39;m sending. So I&#39;m

53
00:03:09,030 --> 00:03:09,870
going to say 

54
00:03:09,870 --> 00:03:13,230
I only want the top 10 or it could be even

55
00:03:13,230 --> 00:03:18,470
more than that, we could say top I&#39;m 70 percent as well. Now we must use ORDER BY here because

56
00:03:18,470 --> 00:03:22,100
we don&#39;t have a guaranteed order. So we need to make sure I know

57
00:03:22,100 --> 00:03:25,980
what top 10 am I displaying. So it must be

58
00:03:25,980 --> 00:03:29,390
the top 10 sales price for example.

59
00:03:29,390 --> 00:03:32,880
So if it&#39;s unordered, presumably it just returns the first 10 rows that

60
00:03:32,880 --> 00:03:37,110
throws out if you&#39;ve specified TOP 10. If you do TOP 10 and don&#39;t do ORDER By, I think it is going to throw

61
00:03:37,110 --> 00:03:37,890
an error actually.

62
00:03:37,890 --> 00:03:42,560
Okay. You need to make sure they&#39;re in order

63
00:03:42,560 --> 00:03:48,870
to do this. Now the other thing you might want, so I said you could have the top 10,

64
00:03:48,870 --> 00:03:52,560
you could have the top 10 percent, or 5 percent whatever number you want.

65
00:03:52,560 --> 00:03:56,750
Equally you could have WITH TIES because

66
00:03:56,750 --> 00:03:59,940
actually I may well have multiple products

67
00:03:59,940 --> 00:04:03,950
that are the 10th most expensive product. There might be quite a few that have the

68
00:04:03,950 --> 00:04:04,989
same price.

69
00:04:04,989 --> 00:04:08,310
So therefore, how do I decide between them?

70
00:04:08,310 --> 00:04:13,360
And if we don&#39;t specify anything, we&#39;re deciding between them and it will just

71
00:04:13,360 --> 00:04:18,060
display one of them. So actually WITH TIES is quite useful in that we leave in

72
00:04:18,060 --> 00:04:20,240
any equal to.

73
00:04:20,240 --> 00:04:23,960
So it&#39;s TOP 10 or if you match the value displayed that one as well.

74
00:04:23,960 --> 00:04:27,760
I do have a question for you. What if I wanted to display the bottom 10? I don&#39;t

75
00:04:27,760 --> 00:04:29,280
seem to have a bottom keyword.

76
00:04:29,280 --> 00:04:32,610
Well no but your ORDER BY would be the other way around.

77
00:04:32,610 --> 00:04:35,900
So if I ORDER BY ascending as opposed to descending

78
00:04:35,900 --> 00:04:39,220
you would get the bottom 10 items

79
00:04:39,220 --> 00:04:42,770
they would be in the wrong order. 

80
00:04:42,770 --> 00:04:46,260
The top 10 bottom items. The top 10 bottom items, exactly yes. &lt;laughter&gt;

81
00:04:46,260 --> 00:04:50,350
And

82
00:04:50,350 --> 00:04:54,670
here we&#39;re looking a little bit more complex. So paging through our results.

83
00:04:54,670 --> 00:04:57,050
It&#39;s saying well I okay you&#39;ve got the top 10,

84
00:04:57,050 --> 00:05:00,890
but I don&#39;t actually want the top 10. I want to have

85
00:05:00,890 --> 00:05:04,470
numbers 11 through 20 and then I might want numbers

86
00:05:04,470 --> 00:05:08,160
21 to 30. I want to have the next set.

87
00:05:08,160 --> 00:05:11,730
So this might be on like on a Web page where I get the first page

88
00:05:11,730 --> 00:05:15,550
of results, but I want page through and see the next page and the next page. And then you go through 

89
00:05:15,550 --> 00:05:15,910
 

90
00:05:15,910 --> 00:05:20,530
10 or 100 at a time, we can display that. So here what we&#39;re saying is --

91
00:05:20,530 --> 00:05:24,020
and this is one where we&#39;ve got 

92
00:05:24,020 --> 00:05:27,680
language getting even more fluid.

93
00:05:27,680 --> 00:05:31,810
Because in here is saying firstly we&#39;re going to offset

94
00:05:31,810 --> 00:05:34,920
so many rows, so I&#39;m not interest in the first 10

95
00:05:34,920 --> 00:05:37,960
I&#39;ve already done those without TOP. So this is the number of rows I&#39;m going to skip?

96
00:05:37,960 --> 00:05:42,790
So I&#39;m going to skip 10 rows, yep, and that&#39;s OFFSET. And the rows you can see

97
00:05:42,790 --> 00:05:46,560
on this slide that we actually have a bracket &lt;parentheses&gt; around the S. We can say

98
00:05:46,560 --> 00:05:50,550
OFFSET 1 ROW because that&#39;s good English or we can say OFFSET 

99
00:05:50,550 --> 00:05:53,850
10 ROWS because it&#39;s good English. Okay. But, equally I can say

100
00:05:53,850 --> 00:05:58,090
OFFSET 10 ROW or OFFSET 1 ROWS.

101
00:05:58,090 --> 00:06:02,300
It doesn&#39;t make any difference. Interchangeably it recognizes ROW and ROWs as meaning the same thing.

102
00:06:02,300 --> 00:06:05,700
You tend to type it in English as you would normally write, but if your

103
00:06:05,700 --> 00:06:06,910
changing it from 1 to 2, 

104
00:06:06,910 --> 00:06:11,520
you don&#39;t bother changing it because it does the same thing. And then we get to FETCH -- what

105
00:06:11,520 --> 00:06:12,450
do I want to get?

106
00:06:12,450 --> 00:06:17,570
And I would like to fetch the first 10 rows after I skip 10.

107
00:06:17,570 --> 00:06:20,890
But equally you might say well isn&#39;t that the next 10 rows? Right.

108
00:06:20,890 --> 00:06:25,500
They&#39;re the same. So FIRST and NEXT are treated the same. It doesn&#39;t make any difference

109
00:06:25,500 --> 00:06:27,560
at all. So ROW or ROWS

110
00:06:27,560 --> 00:06:30,969
doesn&#39;t matter, FIRST or NEXT doesn&#39;t matter. You can use them either way around

111
00:06:30,969 --> 00:06:35,629
it does exactly the same thing. Okay. So then we&#39;re basically saying so skip 10

112
00:06:35,629 --> 00:06:36,199
rows

113
00:06:36,199 --> 00:06:41,449
fetch the next 10 rows. So I can display those next 10 entries

114
00:06:41,449 --> 00:06:46,129
that we were looking for before. It&#39;s not something you do all the time, but 

115
00:06:46,129 --> 00:06:50,349
it&#39;s something if you need to do it. Previously we had to do

116
00:06:50,349 --> 00:06:54,310
sub queries and really quite complicated queries before we had this capability.

117
00:06:54,310 --> 00:06:57,919
When it&#39;s used, it&#39;s priceless

118
00:06:57,919 --> 00:07:02,169
Although often actually, often you don&#39;t often want

119
00:07:02,169 --> 00:07:06,610
the next 10 rows. But if you do, as you said a Web page might be a good example,

120
00:07:06,610 --> 00:07:07,979
where you want to page through.

