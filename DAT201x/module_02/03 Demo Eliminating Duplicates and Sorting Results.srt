0
00:00:02,060 --> 00:00:07,049
So we&#39;ll have a look at getting rid of duplicates and sorting and those

1
00:00:07,049 --> 00:00:08,980
things we just talked about in the slides.

2
00:00:08,980 --> 00:00:13,440
So I&#39;m just using Visual Studio Express. Again it&#39;s Express Edition so it&#39;s free to

3
00:00:13,440 --> 00:00:14,299
download

4
00:00:14,299 --> 00:00:18,350
and you might be using this for developing other things as well but we can do it in

5
00:00:18,350 --> 00:00:19,160
here as well.

6
00:00:19,160 --> 00:00:22,800
In exactly the same way that Graeme set up before we have a

7
00:00:22,800 --> 00:00:28,210
Connect option here so we can set up a new project, we can connect this up

8
00:00:28,210 --> 00:00:33,030
and which one is this one, and we can specify connection string.

9
00:00:33,030 --> 00:00:36,160
Again, I&#39;ve just called it MVAJeff in here and

10
00:00:36,160 --> 00:00:39,570
specify the logon details in there.

11
00:00:39,570 --> 00:00:44,640
So it&#39;s exactly the same window basically that you used before. So to start off with,

12
00:00:44,640 --> 00:00:45,149
this is

13
00:00:45,149 --> 00:00:48,600
a query which we&#39;ve already seen the

14
00:00:48,600 --> 00:00:52,449
sort of results we can get. So it&#39;s a list of colors,

15
00:00:52,449 --> 00:00:56,230
and we&#39;ve got an ISNULL in there just to  display

16
00:00:56,230 --> 00:00:59,480
and if we&#39;ve got any nulls, we can clear those down and display null. 

17
00:00:59,480 --> 00:01:03,030
But we have got a lot of different rows in here.

18
00:01:03,030 --> 00:01:06,160
There&#39;s a lot of Reds, a lot of Whites, a lot of Multis,

19
00:01:06,160 --> 00:01:11,310
So, I don&#39;t have a list of colors, I have every single item

20
00:01:11,310 --> 00:01:14,620
for each product. You&#39;ve got the color of that product on one row.

21
00:01:14,620 --> 00:01:18,900
So I&#39;ve got 200, you see in the bottom right 295 rows. I don&#39;t have 295 

22
00:01:18,900 --> 00:01:19,630
different colors.

23
00:01:19,630 --> 00:01:22,950
Ok so what then happens

24
00:01:22,950 --> 00:01:26,150
if we go into here

25
00:01:26,150 --> 00:01:31,940
and do it a DISTINCT in there and it&#39;s just literally adding that

26
00:01:31,940 --> 00:01:35,760
keyword will

27
00:01:35,760 --> 00:01:40,030
strip out all of those duplicates. And we now have 10 rows.

28
00:01:40,030 --> 00:01:45,500
So that is the different colors that I have in there, it&#39;s specific. So I&#39;ve got silver,

29
00:01:45,500 --> 00:01:49,090
and I&#39;ve got silver black, so those are different things. Even if 

30
00:01:49,090 --> 00:01:50,480
someone puts a space in

31
00:01:50,480 --> 00:01:54,100
which is something to watch out for, actually. If someone puts a space in after the word

32
00:01:54,100 --> 00:01:54,720
black

33
00:01:54,720 --> 00:01:58,350
that&#39;s different than black without the space. They might look

34
00:01:58,350 --> 00:01:58,980
the same,

35
00:01:58,980 --> 00:02:03,350
but it is absolutely specific in there. And I&#39;ve got my Nones

36
00:02:03,350 --> 00:02:05,550
in there as well because we&#39;ve got the ISNULL test in there

37
00:02:05,550 --> 00:02:09,160
as well. And that&#39;s useful because

38
00:02:09,160 --> 00:02:13,390
nulls, they add confusion in here. You know

39
00:02:13,390 --> 00:02:17,520
it&#39;s an unknown, always think null means unknown, so then you have think what would

40
00:02:17,520 --> 00:02:18,070
happen if I put

41
00:02:18,070 --> 00:02:23,550
unknown values into there. So to continue on, I&#39;ll go a bit farther.

42
00:02:23,550 --> 00:02:30,550
 

43
00:02:30,860 --> 00:02:35,430
So we&#39;ve got our color.

44
00:02:35,430 --> 00:02:38,470
And this one we&#39;re just adding a little bit more because we want to put them in

45
00:02:38,470 --> 00:02:39,060
order.

46
00:02:39,060 --> 00:02:42,770
So always worth if you want the output be in order to do an ORDER BY.

47
00:02:42,770 --> 00:02:46,600
Right. It is now just ordering them by

48
00:02:46,600 --> 00:02:50,810
alphanumeric order so the null  comes in the middle

49
00:02:50,810 --> 00:02:54,200
because it&#39;s an n and it says None. In sort of

50
00:02:54,200 --> 00:02:57,810
classic application design here, if this were

51
00:02:57,810 --> 00:03:03,410
say countries or regions or something you were choosing from a

52
00:03:03,410 --> 00:03:06,250
drop-down list in a web page for example,

53
00:03:06,250 --> 00:03:10,170
this is pretty much what you would do. You would want to return them in order because the way

54
00:03:10,170 --> 00:03:12,270
that user interface element is going work,

55
00:03:12,270 --> 00:03:15,150
you&#39;re going to be able to scroll through them or type the letter to

56
00:03:15,150 --> 00:03:15,709
get to the

57
00:03:15,709 --> 00:03:19,959
the value. So ordering is very important. Well yeah because you expect

58
00:03:19,959 --> 00:03:22,590
to be able to get through to the, I mean,

59
00:03:22,590 --> 00:03:27,120
we&#39;re living in the United Kingdom, it&#39;s quite useful that a lot of things jump

60
00:03:27,120 --> 00:03:28,140
to United States

61
00:03:28,140 --> 00:03:32,140
because the right next to it. Yes. But  if we weren&#39;t that would be

62
00:03:32,140 --> 00:03:35,510
annoying when it&#39;s Great Britain because it&#39;s miles away,

63
00:03:35,510 --> 00:03:39,739
so you have to scroll a long way. So you expect them to be in that order

64
00:03:39,739 --> 00:03:43,810
and that&#39;s what people expect. There are other ways we can get you to say well

65
00:03:43,810 --> 00:03:44,900
what if you wanted to have the

66
00:03:44,900 --> 00:03:48,180
most commonly used ones at the top. And we&#39;ll talk about unions and things that

67
00:03:48,180 --> 00:03:48,880
where you could

68
00:03:48,880 --> 00:03:52,610
have other methods, but essentially yes, you want these to be in order.

69
00:03:52,610 --> 00:03:57,110
Just adding a little bit more to that,

70
00:03:57,110 --> 00:04:00,459
so same sort of thing in here, so we&#39;re going to do a list again.

71
00:04:00,459 --> 00:04:03,989
Now what we&#39;re doing is starting to have multiple 

72
00:04:03,989 --> 00:04:08,700
columns now. So what happens now, notice that the

73
00:04:08,700 --> 00:04:13,180
black that&#39;s displaying multiple times because we&#39;ve got

74
00:04:13,180 --> 00:04:17,500
also the size. So in there we now have

75
00:04:17,500 --> 00:04:21,590
color and size so it&#39;s unique across color and size. Now have sixty eight rows, I think

76
00:04:21,589 --> 00:04:22,990
there were 295

77
00:04:22,990 --> 00:04:27,090
products. We had ten different colors

78
00:04:27,090 --> 00:04:30,170
but we&#39;ve got sixty eight different color

79
00:04:30,170 --> 00:04:34,530
and size combinations. So it&#39;s the distinct combinations of all of the columns that you&#39;ve selected.

80
00:04:34,530 --> 00:04:35,940
Exactly yep. So 

81
00:04:35,940 --> 00:04:39,600
they all look like black, if I scroll down through that you&#39;ll see that

82
00:04:39,600 --> 00:04:44,180
we&#39;ve got blues and the grays and so on. So it&#39;s combinations of those two.

83
00:04:44,180 --> 00:04:47,950
If it can&#39;t decide which ones display, it would it always display 

84
00:04:47,950 --> 00:04:54,270
all of those in there. Now the TOP we talked about, so I&#39;m not interested in all of them.

85
00:04:54,270 --> 00:04:57,310
So, I only want the top 100,

86
00:04:57,310 --> 00:05:00,680
again we must do the order by to make sure we want the top.

87
00:05:00,680 --> 00:05:04,490
So we&#39;re going to do descending order in there so that gives me the

88
00:05:04,490 --> 00:05:07,880
most expensive products and in here.

89
00:05:07,880 --> 00:05:11,280
Now we might

90
00:05:11,280 --> 00:05:14,850
also want to fetch

91
00:05:14,850 --> 00:05:18,050
the next set. So what we&#39;re going to do in here

92
00:05:18,050 --> 00:05:21,970
is to say, initially

93
00:05:21,970 --> 00:05:25,450
we&#39;re going to use this OFFSET. Now I know

94
00:05:25,450 --> 00:05:28,650
that I&#39;m going to use the OFFSET to select each 10

95
00:05:28,650 --> 00:05:33,700
in the row. Now it makes it much easier if we setup the same code for each

96
00:05:33,700 --> 00:05:34,290
block

97
00:05:34,290 --> 00:05:37,380
10. So the first block of 10, you think well yeah but that&#39;s not doing that, you just

98
00:05:37,380 --> 00:05:38,160
want TOP 10.

99
00:05:38,160 --> 00:05:42,040
But if I&#39;m passing values from a client

100
00:05:42,040 --> 00:05:45,229
it&#39;s a lot easier if I&#39;m passing them into the same place each time.

101
00:05:45,229 --> 00:05:49,390
So if I know I&#39;m going to page, rather than just selecting the TOP and

102
00:05:49,390 --> 00:05:53,910
TOP 10, it might be better to use the OFFSET in this case because then I&#39;m

103
00:05:53,910 --> 00:05:56,169
going to want the second page and the  third page and so on. Exactly.

104
00:05:56,169 --> 00:06:00,260
And in most cases actually in terms performance, people say but wouldn&#39;t that affect performance or

105
00:06:00,260 --> 00:06:00,640
something?

106
00:06:00,640 --> 00:06:04,530
A lot of the time, SQL Server is pretty clever about what it does. So

107
00:06:04,530 --> 00:06:09,740
it&#39;s not actually, this language we see is something that it then compiles as

108
00:06:09,740 --> 00:06:11,250
the instructions of what we need to do.

109
00:06:11,250 --> 00:06:15,640
Those are not the same things. So quite often it will do exactly the same thing with

110
00:06:15,640 --> 00:06:16,320
the TOP

111
00:06:16,320 --> 00:06:20,410
or with the OFFSET with an OFFSET of 0, it would actually run the same.

112
00:06:20,410 --> 00:06:24,229
Okay. That hidden magical code underneath that we can&#39;t,

113
00:06:24,229 --> 00:06:26,470
with the matrix if you like,

114
00:06:26,470 --> 00:06:29,099
that we can&#39;t understand 

115
00:06:29,099 --> 00:06:33,310
is probably going to be exactly the same whatever we run. So we&#39;ve got an OFFSET of 0 rows

116
00:06:33,310 --> 00:06:36,969
so that basically means the top, then fetching the next 10 rows

117
00:06:36,969 --> 00:06:40,289
only in there. So you need to put the ONLY. 

118
00:06:40,289 --> 00:06:43,590
Rows are rows, Fetch could be the Next 

119
00:06:43,590 --> 00:06:46,030
or it could be first wouldn&#39;t make a difference. You could say well

120
00:06:46,030 --> 00:06:46,990
really that&#39;s the first.

121
00:06:46,990 --> 00:06:50,439
This hasn&#39;t been mentioned I don&#39;t think actually.

122
00:06:50,439 --> 00:06:54,740
We&#39;ve got a lot of these green lines with two dashes, now I&#39;m 

123
00:06:54,740 --> 00:06:56,949
highlighting that one there, but what happens if I --

124
00:06:56,949 --> 00:06:59,639
Well we are going to come onto that later on the course, but these are just 

125
00:06:59,639 --> 00:07:02,550
comments. Like in any programming language typically there&#39;s a way of 

126
00:07:02,550 --> 00:07:05,900
annotating your code with comments and that&#39;s one of the ways we have in

127
00:07:05,900 --> 00:07:06,889
Transact-SQL

128
00:07:06,889 --> 00:07:10,849
of indicating this line is not to be executed, this is just a comment. So I&#39;ve

129
00:07:10,849 --> 00:07:14,129
highlighted that by mistake but I can just run it, it won&#39;t make any difference.

130
00:07:14,129 --> 00:07:17,569
Okay, so now we&#39;ve got in here, that&#39;s the first

131
00:07:17,569 --> 00:07:22,569
10 rows in there. Now notice these ones are ordering by product number,

132
00:07:22,569 --> 00:07:26,909
so it&#39;s not by ListPrice in this situation. So that&#39;s why the not

133
00:07:26,909 --> 00:07:30,610
list price order it&#39;s actually showing the first 10 products by product

134
00:07:30,610 --> 00:07:31,029
number.

135
00:07:31,029 --> 00:07:34,340
If we want to display the next 10 products

136
00:07:34,340 --> 00:07:38,050
in here, then we&#39;re just going to go

137
00:07:38,050 --> 00:07:42,810
and set our OFFSET to 10 rows. Okay, so we can go through and then

138
00:07:42,810 --> 00:07:46,050
and you switched to FIRST this time, but it doesn&#39;t matter, FIRST means the 

139
00:07:46,050 --> 00:07:46,620
same as NEXT.

140
00:07:46,620 --> 00:07:49,960
And even though I&#39;m actually kind of selecting the next rows.

141
00:07:49,960 --> 00:07:53,849
You&#39;re being deliberately obtuse. And actually I&#39;ve changed the 

142
00:07:53,849 --> 00:07:54,529
10 to ROW

143
00:07:54,529 --> 00:07:58,710
even though it&#39;s a singular, it doesn&#39;t make any difference you can

144
00:07:58,710 --> 00:08:02,259
just run it either way around. So basically  it&#39;s going to go through

145
00:08:02,259 --> 00:08:05,680
10 at a time finding those. And then so if we,

146
00:08:05,680 --> 00:08:09,229
we&#39;re not really doing things where we&#39;re passing values into

147
00:08:09,229 --> 00:08:13,409
functions and stored procedures on this course, but ultimately it may be that we

148
00:08:13,409 --> 00:08:15,969
actually pass a value from a client into there

149
00:08:15,969 --> 00:08:19,300
and this becomes a lot simpler because actually all I&#39;m doing

150
00:08:19,300 --> 00:08:22,669
is passing a value into where we have a zero

151
00:08:22,669 --> 00:08:25,810
here, or where we have a 10 here.

152
00:08:25,810 --> 00:08:30,229
So I can drop a value in there and then just decide which 10 I&#39;m after, and actually

153
00:08:30,229 --> 00:08:32,010
I could supply another value

154
00:08:32,010 --> 00:08:36,529
to say how many I&#39;m interested in. But if I&#39;m always interested in 10 it would be

155
00:08:36,529 --> 00:08:38,150
really simple to drop a value in

156
00:08:38,150 --> 00:08:41,970
there and so we can use a zero we can use a 10. Reuse the same code just

157
00:08:41,970 --> 00:08:44,570
depending on which page you want to go to, we have a different value for that.

158
00:08:44,570 --> 00:08:48,680
Exactly, and that would be really simple for a programmer of a 

159
00:08:48,680 --> 00:08:51,040
website or something to drop that value in.

