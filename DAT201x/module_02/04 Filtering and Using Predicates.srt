0
00:00:02,510 --> 00:00:05,200
Filtering then, so we&#39;ve talked about the WHERE clause.

1
00:00:05,200 --> 00:00:08,410
There&#39;s lots of different things we put in the WHERE clause. So the

2
00:00:08,410 --> 00:00:11,629
the most straightforward being equals. So we can say

3
00:00:11,629 --> 00:00:15,780
WHERE color equals red for example. So bring me back all the red products 

4
00:00:15,780 --> 00:00:20,199
is what we&#39;re saying. Yeah, matching that exactly. We can also do 

5
00:00:20,199 --> 00:00:23,510
greater than, and we could do less than. So obviously we can do

6
00:00:23,510 --> 00:00:28,910
where value greater than 100. We can also, to add into that, we can do 

7
00:00:28,910 --> 00:00:29,949
greater than or equal to.

8
00:00:29,949 --> 00:00:33,780
So if we want to find a hundred, then we&#39;re looking at greater than or equal to.

9
00:00:33,780 --> 00:00:37,870
If we just do greater than, it will find the smallest increment greater than, so

10
00:00:37,870 --> 00:00:40,480
100.000001

11
00:00:40,480 --> 00:00:44,750
is greater than 100, but it wouldn&#39;t find 100. 100 has to

12
00:00:44,750 --> 00:00:48,399
be greater than or equal. Okay. We use that quite a lot, use greater than or equal to.

13
00:00:48,399 --> 00:00:53,390
The next one IN, we might be looking for a list of values. So we can say

14
00:00:53,390 --> 00:00:57,800
color IN and then in a bracketed list, do red, blue, green,

15
00:00:57,800 --> 00:01:01,899
and we can define a list of different ones I&#39;m interested in. You could

16
00:01:01,899 --> 00:01:06,030
and I&#39;m going to jump down a little bit in here, I have an OR as well. So I&#39;ll come back 

17
00:01:06,030 --> 00:01:07,350
to that because we could say

18
00:01:07,350 --> 00:01:11,980
instead of using IN, we could use OR. So equals Red OR, ok I see.

19
00:01:11,980 --> 00:01:16,130
Yep. BETWEEN, now if I say BETWEEN

20
00:01:16,130 --> 00:01:20,030
100 and 200, the only

21
00:01:20,030 --> 00:01:25,070
slight confusion that might be there is does that include 100

22
00:01:25,070 --> 00:01:28,070
and 200? So if we&#39;re saying between,

23
00:01:28,070 --> 00:01:31,440
it&#39;s slightly confusing because it&#39;s,

24
00:01:31,440 --> 00:01:35,980
what about the absolute values? So we actually going say

25
00:01:35,980 --> 00:01:39,460
are greater than or equal to and then less than or equal to. We&#39;re going to see in between.

26
00:01:39,460 --> 00:01:42,860
So it is included. If I say BETWEEN  between 100 and 200 and something is

27
00:01:42,860 --> 00:01:43,660
100,

28
00:01:43,660 --> 00:01:47,320
that will be returned. That is in the set. 

29
00:01:47,320 --> 00:01:51,330
LIKE, now we&#39;ve talked about numbers. It&#39;s quite

30
00:01:51,330 --> 00:01:53,659
easy to find something that&#39;s

31
00:01:53,659 --> 00:01:58,179
in the hundreds. Because I mean hundreds is between 100 and 199

32
00:01:58,179 --> 00:02:03,240
or less than 200 maybe even. You can  you can specify a range.

33
00:02:03,240 --> 00:02:06,909
But what about with text strings? We don&#39;t have that

34
00:02:06,909 --> 00:02:10,890
similar ability. What we have is that it begins an A.

35
00:02:10,889 --> 00:02:15,510
Right. Or it begins with AZE, it has a bit of a 

36
00:02:15,510 --> 00:02:20,400
a bit text string that we&#39;re interested in. So we can do a simple LIKE which basically

37
00:02:20,400 --> 00:02:23,400
says, I&#39;m setting this up say accept some wildcards.

38
00:02:23,400 --> 00:02:28,489
So I can say LIKE A and we have a wildcard, which is a percentage symbol,

39
00:02:28,489 --> 00:02:33,940
and that says anything after the A is fine, and equally nothing after the A is fine as well. It would find an A,

40
00:02:33,940 --> 00:02:37,379
and it would find an A with any number of different characters after it.

41
00:02:37,379 --> 00:02:41,090
So it&#39;s a bit like on the filesystem if you&#39;re looking at

42
00:02:41,090 --> 00:02:44,769
folder names, you could use the star as an asterisk

43
00:02:44,769 --> 00:02:49,010
as a placeholder. Yes. In SQL Server,  the equivalent wildcard for any

44
00:02:49,010 --> 00:02:49,680
character

45
00:02:49,680 --> 00:02:52,879
isn&#39;t an asterisk, which we&#39;ve seen used already to say all rows,

46
00:02:52,879 --> 00:02:56,910
but actually when I&#39;m using LIKE it&#39;s a percentage symbol. Yes.

47
00:02:56,910 --> 00:03:00,120
So it and actually if anyone came from Access

48
00:03:00,120 --> 00:03:03,900
SQL, Microsoft Access, there you did use a percentage symbol, 

49
00:03:03,900 --> 00:03:08,540
I&#39;m sorry, an asterisk. Yeah so in that, a star is what it would have been, but over here we use

50
00:03:08,540 --> 00:03:12,489
a percentage symbol. There are the ones as well, we could use underscore as well.

51
00:03:12,489 --> 00:03:17,120
Because it may be that actually within that LIKE, we don&#39;t want to find

52
00:03:17,120 --> 00:03:20,510
just As, we want to find and an A

53
00:03:20,510 --> 00:03:23,540
followed by two digits and then a Zed.

54
00:03:23,540 --> 00:03:28,389
Right. So we could put in a percentage symbol, if we put A%Z that would

55
00:03:28,389 --> 00:03:29,840
be begins A and ends with Zed

56
00:03:29,840 --> 00:03:33,349
and I don&#39;t care what&#39;s in between. It could be nothing in between.

57
00:03:33,349 --> 00:03:37,200
That would be fine. But I want two digits, so I actually can use an underscore in there.

58
00:03:37,200 --> 00:03:37,720
And that says

59
00:03:37,720 --> 00:03:41,150
any digit is fine. I don&#39;t care what it is,  but there has to be something in there. 

60
00:03:41,150 --> 00:03:44,760
So A__Z &lt;A underscore underscore Zed&gt; says A, 

61
00:03:44,760 --> 00:03:49,919
two digits of any sort, yeah it could be numbers, it could be text, whatever.

62
00:03:49,919 --> 00:03:55,010
A, two things, and then a Zed. So we can actually mix

63
00:03:55,010 --> 00:03:55,620
and match.

64
00:03:55,620 --> 00:04:00,799
If you&#39;ve got complex codes, then actually those codes you can find bits of them. It could 

65
00:04:00,799 --> 00:04:03,579
be the project category somewhere in there, it could be the

66
00:04:03,579 --> 00:04:05,770
that car registration number,

67
00:04:05,770 --> 00:04:10,070
the year the registration might be somewhere in there. So if it&#39;s a pattern of some

68
00:04:10,070 --> 00:04:14,020
you can discover that. We can find bits using wildcards in that way we would like.

69
00:04:14,020 --> 00:04:17,980
AND we&#39;re going to use when

70
00:04:17,980 --> 00:04:22,290
multiple things need to be true. So color is red or color equals red rather,

71
00:04:22,290 --> 00:04:28,580
AND size equals 58, and we would find both those have to be true to return

72
00:04:28,580 --> 00:04:29,320
that set.

73
00:04:29,320 --> 00:04:33,010
And then almost, well not the opposite of that, but 

74
00:04:33,010 --> 00:04:36,190
another technique would be OR. It could be either one could be true.

75
00:04:36,190 --> 00:04:39,320
Now if I come back to what we said about IN, so

76
00:04:39,320 --> 00:04:43,000
IN is a list of red, blue, green in brackets

77
00:04:43,000 --> 00:04:47,660
OR we could do as an alternative. So we could say color equals red. Now you&#39;ll see why

78
00:04:47,660 --> 00:04:52,700
IN is so useful, because color equals red and then we have, we can&#39;t just put OR blue,

79
00:04:52,700 --> 00:04:56,920
we have put OR color equals blue, OR color equals green. 

80
00:04:56,920 --> 00:05:02,490
So there&#39;s a lot more typing. Right. Most SQL programmers are lazy, so they will use IN. &lt;laughter&gt;

81
00:05:02,490 --> 00:05:07,050
And that&#39;s much much easier. And then if you combine

82
00:05:07,050 --> 00:05:11,330
the ORs and the ANDs, we might say

83
00:05:11,330 --> 00:05:15,150
I would like to find a size of large

84
00:05:15,150 --> 00:05:20,510
in red or blue. So color equals red

85
00:05:20,510 --> 00:05:25,050
and then we could say what are we doing to do for color, color equals red OR color equals blue

86
00:05:25,050 --> 00:05:28,870
AND size equals large.

87
00:05:28,870 --> 00:05:32,310
But the problem is that the OR,

88
00:05:32,310 --> 00:05:35,860
I have to put the ANDs on each sides of the ORs. So I have to go in there and say

89
00:05:35,860 --> 00:05:39,340
size equals large AND color equals red

90
00:05:39,340 --> 00:05:45,050
OR size equals large AND color equals blue. Okay, isn&#39;t there another way I could shortcut

91
00:05:45,050 --> 00:05:45,330
that?

92
00:05:45,330 --> 00:05:49,020
We could shortcut it, I mean INs would be ideal for that again because you could do

93
00:05:49,020 --> 00:05:53,790
IN that AND that. Well, couldn&#39;t I use parenthesis? Couldn&#39;t I say color equals...

94
00:05:53,790 --> 00:05:58,450
Exactly like mathematics. Yeah. In exactly the same way that math works, 

95
00:05:58,450 --> 00:06:02,680
because everyone remembers their BODMAS, the order of things, there&#39;s a simple way of

96
00:06:02,680 --> 00:06:06,280
making sure what you&#39;re doing is correct, it&#39;s to use brackets. So you say ok

97
00:06:06,280 --> 00:06:09,880
work out that side of the brackets and then work out that side of the brackets.

98
00:06:09,880 --> 00:06:13,530
So we could say work out this AND work out that. Or work out this 

99
00:06:13,530 --> 00:06:17,410
OR work out that. Right. And then inside the brackets is done first. 

100
00:06:17,410 --> 00:06:21,009
Right. So there&#39;s different techniques.

101
00:06:21,009 --> 00:06:25,720
Just make sure always when you&#39;re doing these things making sure that you&#39;ve got

102
00:06:25,720 --> 00:06:27,550
records that you would expect to get.

103
00:06:27,550 --> 00:06:31,550
Because it&#39;s quite easy to do that and just say oh, I&#39;ll just put in an OR

104
00:06:31,550 --> 00:06:35,699
the end, and actually you get records that you&#39;re not interested in. So run them against test data

105
00:06:35,699 --> 00:06:39,020
to make sure that your getting expected results. Does that look right?

106
00:06:39,020 --> 00:06:42,220
And then NOT, it&#39;s quite simple. We get the opposite of

107
00:06:42,220 --> 00:06:45,330
whatever we&#39;re doing. So we can say and NOT IN

108
00:06:45,330 --> 00:06:48,490
and ones that are not in a list. But remembering our nulls

109
00:06:48,490 --> 00:06:52,270
which won&#39;t occur in either list. Right.

110
00:06:52,270 --> 00:06:56,400
Because there&#39;re unknown. Maybe the do, it&#39;s quite possible

111
00:06:56,400 --> 00:06:59,479
that although however unlikely it may be

112
00:06:59,479 --> 00:07:02,849
it is possible that actually

113
00:07:02,849 --> 00:07:07,660
your manager is Queen Elizabeth. It&#39;s

114
00:07:07,660 --> 00:07:12,500
unlikely but possible. So therefore, it won&#39;t occur in either the

115
00:07:12,500 --> 00:07:16,310
IN list or the NOT IN list because it&#39;s an unknown value.

