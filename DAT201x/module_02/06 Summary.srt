0
00:00:02,020 --> 00:00:05,730
So we&#39;ve had a look at slightly more advanced things with our SELECT

1
00:00:05,730 --> 00:00:06,270
statement.

2
00:00:06,270 --> 00:00:09,920
We can do removing duplicates, so we&#39;ve had a look at

3
00:00:09,920 --> 00:00:13,919
I&#39;m using that DISTINCT word. Sorting, we&#39;ve seen actually

4
00:00:13,919 --> 00:00:17,790
that&#39;s quite important. That we need to make sure that we&#39;ve got an ORDER BY

5
00:00:17,790 --> 00:00:20,930
to remove any duplicate, 

6
00:00:20,930 --> 00:00:25,410
order everything in the correct result set that we want, so if we want it by

7
00:00:25,410 --> 00:00:27,019
product id, we would tell it to it by

8
00:00:27,019 --> 00:00:31,910
product id. Paging because we might want the top 100 things, but equally we might want the 

9
00:00:31,910 --> 00:00:32,710
next one hundred

10
00:00:32,710 --> 00:00:36,190
things. So we can sort, search for blocks within there. 

11
00:00:36,190 --> 00:00:40,920
And using our WHERE clause as well so we&#39;ve got lots of different things we can do. There&#39;s WHERE, 

12
00:00:40,920 --> 00:00:42,300
we can use Like and

13
00:00:42,300 --> 00:00:47,489
wildcards, we can do greater than. There&#39;s multiple ways we can search through our data 

14
00:00:47,489 --> 00:00:51,420
to find the correct results. And always trying to make sure you only present those

15
00:00:51,420 --> 00:00:53,780
correct results to the client. You might think we&#39;ll

16
00:00:53,780 --> 00:00:57,270
there&#39;s the correct results and a load more, 

17
00:00:57,270 --> 00:01:00,620
but remembering that actually can cost your network

18
00:01:00,620 --> 00:01:03,719
and the actual performance at the end

19
00:01:03,719 --> 00:01:07,500
product, the website or where ever it is  ending up being,

20
00:01:07,500 --> 00:01:11,600
will affect will be affected by you moving large amounts of data around

21
00:01:11,600 --> 00:01:16,000
you don&#39;t need. Okay, so great! Thanks for the that. That&#39;s a fantastic trip

22
00:01:16,000 --> 00:01:16,510
through

23
00:01:16,510 --> 00:01:20,870
using the SELECT statement, building on what we did in the previous module.

24
00:01:20,870 --> 00:01:25,420
Take a look at this video make sure you&#39;re happy with everything in it,

25
00:01:25,420 --> 00:01:28,940
and then download the lab files for the course,

26
00:01:28,940 --> 00:01:33,450
and do the lab for this module, module two. And that will give you a chance to

27
00:01:33,450 --> 00:01:36,300
get hands on and try some of this stuff for yourself that we&#39;ve seen.

28
00:01:36,300 --> 00:01:39,340
There&#39;s a number challenges in there where we&#39;ll get you to figure out the right Transact-SQL

29
00:01:39,340 --> 00:01:42,610
to write, and there is solution files for you to check that your

30
00:01:42,610 --> 00:01:46,370
answers are correct. When you&#39;ve done that and you&#39;re comfortable that you&#39;re ready to

31
00:01:46,370 --> 00:01:46,970
move on,

32
00:01:46,970 --> 00:01:50,650
join us for the next module when we&#39;ll look at starting to work with multiple

33
00:01:50,650 --> 00:01:51,030
tables.

