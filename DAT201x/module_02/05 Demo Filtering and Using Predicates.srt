0
00:00:02,280 --> 00:00:05,850
So let&#39;s have a look at filtering with predicates. I&#39;m going to

1
00:00:05,850 --> 00:00:12,850
open up a separate query I have in here. So let&#39;s start off with

2
00:00:13,820 --> 00:00:19,290
very simple information in here where we&#39;re going to do a simple WHERE

3
00:00:19,290 --> 00:00:22,420
ProductModelID = 6. So it&#39;s really simple.

4
00:00:22,420 --> 00:00:25,750
I&#39;m just looking for product model 6 within my

5
00:00:25,750 --> 00:00:29,070
database. And I just need to reconnect because I&#39;m using another query,

6
00:00:29,070 --> 00:00:33,329
it will just take a second. And

7
00:00:33,329 --> 00:00:36,790
I&#39;ve done the, I do this a lot, querying master. &lt;laughter&gt; 

8
00:00:36,790 --> 00:00:42,650
You know just got a new connection and it&#39;s gone back to master again. Alright there we go

9
00:00:42,650 --> 00:00:43,780
AdventureWorksLT

10
00:00:43,780 --> 00:00:47,570
and there is all my products which have a model

11
00:00:47,570 --> 00:00:53,120
ID of 6. We could change that, maybe this is somewhat irrelevant, but

12
00:00:53,120 --> 00:00:57,950
if we want to look with a product model of greater than 6, 11 rows got returned

13
00:00:57,950 --> 00:01:02,600
before. And if I run it with the greater than, then we can see 264 rows

14
00:01:02,600 --> 00:01:07,049
have a product model ID greater than 6. So we can use greater than, equal to

15
00:01:07,049 --> 00:01:10,810
less than all those different filters. And you can use greater than

16
00:01:10,810 --> 00:01:15,620
or less than to effectively mean not equal to. We could,

17
00:01:15,620 --> 00:01:18,940
we could to greater than, less than or we could do

18
00:01:18,940 --> 00:01:22,530
that it&#39;s not equal to because we can do greater than less than symbols. 

19
00:01:22,530 --> 00:01:26,450
Do you mean -- that&#39;s what I mean do less than greater than to mean it&#39;s not equal to. 

20
00:01:26,450 --> 00:01:29,960
Sorry, I thought you meant to say where ProductModelID greater than 6

21
00:01:29,960 --> 00:01:33,350
or less than. No, no.

22
00:01:33,350 --> 00:01:37,330
We can do the greater than and less than symbols in there

23
00:01:37,330 --> 00:01:40,360
and then it will find the ones that are not 6

24
00:01:40,360 --> 00:01:46,530
in here. If I can highlight things properly. Okay, so now we have 284 rows

25
00:01:46,530 --> 00:01:50,350
in here. Now using wildcards,

26
00:01:50,350 --> 00:01:53,360
so we&#39;re looking at things which have a product number.

27
00:01:53,360 --> 00:01:56,740
So if I display all of them in here, again

28
00:01:56,740 --> 00:01:59,810
the highlight ability is really useful because you can just pick the bits of

29
00:01:59,810 --> 00:02:04,100
code that you want to run within here. So that&#39;s why it&#39;s useful to use client tools.

30
00:02:04,100 --> 00:02:08,349
And you see we&#39;ve got some of them  beginning FR, others beginning HL, SO

31
00:02:08,348 --> 00:02:11,670
and so on in here. So if I add the WHERE,

32
00:02:11,670 --> 00:02:13,150
if I don&#39;t put

33
00:02:13,150 --> 00:02:16,590
Like, and just start out with this being equal to,

34
00:02:16,590 --> 00:02:20,129
then I am literally looking

35
00:02:20,129 --> 00:02:23,310
for those values and I would expect to get none.

36
00:02:23,310 --> 00:02:27,159
So nothing gets returned because you might have products

37
00:02:27,159 --> 00:02:31,260
which have a percentage sign in the name. It&#39;s a good idea,

38
00:02:31,260 --> 00:02:35,440
again this is some database design, but if at all you have any influence, a good

39
00:02:35,440 --> 00:02:36,760
idea to try and avoid that.

40
00:02:36,760 --> 00:02:40,900
Right. If at all possible because it will cause you a few headaches.

41
00:02:40,900 --> 00:02:44,409
You have to use odd escape characters and so on in here to 

42
00:02:44,409 --> 00:02:48,510
search for things that have, oh caps lock was already on.

43
00:02:48,510 --> 00:02:52,970
My caps lock was already on so I just typed like in lowercase. Does that matter?

44
00:02:52,970 --> 00:02:57,480
No, Transact-SQL itself is not a case-sensitive language. 

45
00:02:57,480 --> 00:02:58,590
The convention is to

46
00:02:58,590 --> 00:03:03,549
uppercase keywords. You might want to be careful with the values that you&#39;re

47
00:03:03,549 --> 00:03:06,760
searching for because depending on how the database is configured

48
00:03:06,760 --> 00:03:10,220
strings might be case sensitive or not case-sensitive.

49
00:03:10,220 --> 00:03:14,549
That&#39;s a setting at the database level that

50
00:03:14,549 --> 00:03:16,129
says we should compare strings

51
00:03:16,129 --> 00:03:20,669
using case-sensitivity or not case-sensitivity. But the key words themselves,

52
00:03:20,669 --> 00:03:24,040
SQL server doesn&#39;t care, it just knows the like is like and

53
00:03:24,040 --> 00:03:27,739
select is select. Yeah so I just, I meant to type like, I hit

54
00:03:27,739 --> 00:03:31,540
caps lock forgetting I had already typed caps lock last time I did it. We&#39;ll forgive

55
00:03:31,540 --> 00:03:31,769
you.

56
00:03:31,769 --> 00:03:35,709
I can live with that. In fact, if you look at my SQL code, there&#39;s lots of 

57
00:03:35,709 --> 00:03:40,389
instances of lowercase when it should technically be upper.

58
00:03:40,389 --> 00:03:43,919
Right, so in here now we&#39;re filtering this. So we&#39;ve got

59
00:03:43,919 --> 00:03:48,760
FR begins with FR, so all those begin FR they can have anything after it. It could be

60
00:03:48,760 --> 00:03:52,680
any sort of data, any number of characters. Or it could actually just an FR.

61
00:03:52,680 --> 00:03:57,079
So anything is allowed after that. If I  wanted to do ending with something as well, we

62
00:03:57,079 --> 00:03:58,849
could do it the other way around. So we could --

63
00:03:58,849 --> 00:04:03,530
it&#39;s where we put our percentage symbol that&#39;s relevant. So if I wanted to find

64
00:04:03,530 --> 00:04:08,400
things ending in 58 in here, then we could 58 at the end.

65
00:04:08,400 --> 00:04:13,540
And we can run that and they now end 58.

66
00:04:13,540 --> 00:04:17,560
And if we wanted to do ones that contain something, so if I said

67
00:04:17,560 --> 00:04:22,990
contains an R. Now it may be it&#39;s at the beginning or maybe it&#39;s the end.

68
00:04:22,990 --> 00:04:27,530
I still have caps lock on.

69
00:04:27,530 --> 00:04:33,250
Now

70
00:04:33,250 --> 00:04:36,650
it doesn&#39;t matter beginning, end, middle, it could be anywhere in there.

71
00:04:36,650 --> 00:04:40,660
Because that percent can be nothing as well. So all the ones that have an R, you&#39;ll see

72
00:04:40,660 --> 00:04:40,860
there&#39;s an R

73
00:04:40,860 --> 00:04:45,560
at the end there, yeah, and equally an R...there&#39;s a couple of Rs in the middle there...

74
00:04:45,560 --> 00:04:49,210
all of those are fine. They can be anywhere within it. Okay. So containing essentially is

75
00:04:49,210 --> 00:04:54,229
to do that. Okay. Now we&#39;re going to get a little more complexity in here. So we&#39;re going to

76
00:04:54,229 --> 00:04:54,940
use some

77
00:04:54,940 --> 00:04:58,240
underscore which means any number of,

78
00:04:58,240 --> 00:05:02,280
any character but in that place. So, I&#39;m starting with FR

79
00:05:02,280 --> 00:05:06,889
so it begins FR, then it has a dash, then it has anything.

80
00:05:06,889 --> 00:05:10,750
So we&#39;ve got in here -- the dash is a literal in this case.

81
00:05:10,750 --> 00:05:15,009
That is literally a dash and the underscore says you can have anything after that. Now

82
00:05:15,009 --> 00:05:19,520
what I then want, I don&#39;t want any letters to be found because the codes I&#39;m

83
00:05:19,520 --> 00:05:20,419
interested in

84
00:05:20,419 --> 00:05:24,310
are very specific, it has to be a number. It could be any number,

85
00:05:24,310 --> 00:05:27,949
so I&#39;ve done zero to nine. If I wanted it to begin there with a five, I could do zero to 

86
00:05:27,949 --> 00:05:29,120
five as well.

87
00:05:29,120 --> 00:05:33,610
Sorry no, up to a 5 would be zero to five or any range you are interested in

88
00:05:33,610 --> 00:05:37,330
we can specify in there. And then we&#39;ve got two 

89
00:05:37,330 --> 00:05:40,949
digits, then anything, then a dash, then two more digits.

90
00:05:40,949 --> 00:05:44,330
So we can be quite prescriptive about what it actually contains.

91
00:05:44,330 --> 00:05:47,330
Because a lot of these codes, we&#39;ve seen the

92
00:05:47,330 --> 00:05:51,710
58 that&#39;s probably the size, that actually it may be that your database

93
00:05:51,710 --> 00:05:53,560
doesn&#39;t contain these things

94
00:05:53,560 --> 00:05:57,370
a separate columns. It might be that code actually contains the color and

95
00:05:57,370 --> 00:06:00,600
the size. And so you need to search on that.

96
00:06:00,600 --> 00:06:05,560
Again it would be nice to speak to the database design people to strip that out

97
00:06:05,560 --> 00:06:07,229
as an individual piece of information, but

98
00:06:07,229 --> 00:06:12,020
we can&#39;t always do that. Okay so now we&#39;re filtering

99
00:06:12,020 --> 00:06:17,400
and let me just display the product number rather than the name.

100
00:06:17,400 --> 00:06:22,110
 

101
00:06:22,110 --> 00:06:27,870
And then we&#39;ll see

102
00:06:27,870 --> 00:06:33,729
as it gets more complex you can actually see that it takes a bit longer.

103
00:06:33,729 --> 00:06:37,729
The queries might take a little bit longer to run. We&#39;ve got

104
00:06:37,729 --> 00:06:42,530
FR- in here, and we&#39;re specifying exactly

105
00:06:42,530 --> 00:06:45,940
FR dash

106
00:06:45,940 --> 00:06:49,720
anything/something because we&#39;ve got an R next in that one, so any character.

107
00:06:49,720 --> 00:06:53,300
Then two digits, the 9 and the 2 that&#39;s fine, then any character,

108
00:06:53,300 --> 00:06:57,180
then a dash, then two digits. Right, so it&#39;s very specific.

109
00:06:57,180 --> 00:07:00,639
And I&#39;m going to guess you could use this if you were looking for email addresses 

110
00:07:00,639 --> 00:07:01,350
you&#39;d find

111
00:07:01,350 --> 00:07:05,190
any contact detail that is a string,

112
00:07:05,190 --> 00:07:08,620
an at symbol, a string, a dot

113
00:07:08,620 --> 00:07:11,620
and then anything. And maybe you&#39;re looking for dot 

114
00:07:11,620 --> 00:07:16,340
specifically dot code it UK or .com or whatever and you can specify exactly that. 

115
00:07:16,340 --> 00:07:16,639
 

116
00:07:16,639 --> 00:07:19,940
And in here I&#39;m not interested in the product which is called

117
00:07:19,940 --> 00:07:24,889
free range chicken which just happens to begin FR, I&#39;m interested in -- this

118
00:07:24,889 --> 00:07:27,080
specific structure means I

119
00:07:27,080 --> 00:07:30,449
know what I&#39;m getting back. I know I&#39;m going to get all the sizes back

120
00:07:30,449 --> 00:07:35,099
of those, but I&#39;m not going to get other products which just happened to begin FR.

121
00:07:35,099 --> 00:07:39,180
Okay good. So you can -- this is something you can spend

122
00:07:39,180 --> 00:07:39,849
ages

123
00:07:39,849 --> 00:07:43,780
if people ask for a really specific thing. Then you&#39;re using your underscores, because

124
00:07:43,780 --> 00:07:45,630
I haven&#39;t used a wildcard with a percent there

125
00:07:45,630 --> 00:07:49,300
either. So you could add to that which would be containing this or beginning

126
00:07:49,300 --> 00:07:50,150
with this,

127
00:07:50,150 --> 00:07:54,190
you get more and more prescriptive.

128
00:07:54,190 --> 00:07:57,680
Alright, what about nulls? Let&#39;s move on to something else. Alright, so we talked about nulls already a bit. 

129
00:07:57,680 --> 00:07:58,550
Here we&#39;re looking at

130
00:07:58,550 --> 00:08:01,700
our,

131
00:08:01,700 --> 00:08:05,840
in fact I think you mentioned this specifically before, that we have no

132
00:08:05,840 --> 00:08:10,450
end date in here. So we&#39;re going to do an IS NOT NULL. So we&#39;re not going to

133
00:08:10,450 --> 00:08:11,180
put

134
00:08:11,180 --> 00:08:14,740
in here that it&#39;s not equal to null

135
00:08:14,740 --> 00:08:21,340
because if you remember logically that doesn&#39;t work. What you&#39;re saying is

136
00:08:21,340 --> 00:08:25,830
not equal to something we don&#39;t know. Something and we don&#39;t know what is so

137
00:08:25,830 --> 00:08:27,810
we can&#39;t say it&#39;s not equal, it might be equal to it.

138
00:08:27,810 --> 00:08:30,940
It might be very unlikely,

139
00:08:30,940 --> 00:08:34,280
but it might be the case. So in here we shouldn&#39;t be doing

140
00:08:34,280 --> 00:08:38,770
and not equal to null in here. And that says there aren&#39;t any where

141
00:08:38,770 --> 00:08:42,090
the sale end is not null which we know is not true. And so if we go in there and 

142
00:08:42,090 --> 00:08:43,710
change that to IS NOT

143
00:08:43,710 --> 00:08:50,710
then we&#39;ve got records.

144
00:08:51,390 --> 00:08:54,950
It&#39;s important to do that. That&#39;s an example again where we&#39;re checking

145
00:08:54,950 --> 00:08:56,680
result set. Because we did, 

146
00:08:56,680 --> 00:09:00,250
it works. As far as it&#39;s concerned, there&#39;s the results,

147
00:09:00,250 --> 00:09:04,550
there are none. And you could present that to a customer or another person in your

148
00:09:04,550 --> 00:09:06,000
company and there you go

149
00:09:06,000 --> 00:09:09,300
and the data&#39;s not right. Yeah, it&#39;s not right.

150
00:09:09,300 --> 00:09:12,370
Now we&#39;re going to do a range search in here.

151
00:09:12,370 --> 00:09:16,110
Now trying with dates

152
00:09:16,110 --> 00:09:21,520
to in here, we could in here put different date formats that we&#39;ve mentioned.

153
00:09:21,520 --> 00:09:22,820
So I&#39;ve used 

154
00:09:22,820 --> 00:09:26,510
the standard the ISO standard for dates which is that we&#39;re going to put

155
00:09:26,510 --> 00:09:27,330
year first,

156
00:09:27,330 --> 00:09:30,930
then the month, then the day in these. Right.

157
00:09:30,930 --> 00:09:37,140
If I put in here 2006/31/12

158
00:09:37,140 --> 00:09:40,210
it was still work because it understands it. 

159
00:09:40,210 --> 00:09:44,070
When you were talking about the conversion of dates trying to make sure you know

160
00:09:44,070 --> 00:09:45,600
what you&#39;re presenting in here

161
00:09:45,600 --> 00:09:49,080
and where we could have potential for confusion

162
00:09:49,080 --> 00:09:53,050
i.e. any day before 12, 12 or earlier.

163
00:09:53,050 --> 00:09:56,890
Then you could potentially cause confusion.

164
00:09:56,890 --> 00:10:00,870
And also being aware that if I&#39;d accidentally typed 

165
00:10:00,870 --> 00:10:04,870
a one after the three meaning I&#39;m interested in the

166
00:10:04,870 --> 00:10:07,770
the third as a month,

167
00:10:07,770 --> 00:10:11,090
but actually drop a one in there, then  suddenly it&#39;s going to look at it and

168
00:10:11,090 --> 00:10:11,730
think

169
00:10:11,730 --> 00:10:15,760
oh the day and the month are the other way around. So it tries to be helpful and 

170
00:10:15,760 --> 00:10:17,290
sometimes that in itself is not helpful.

171
00:10:17,290 --> 00:10:20,800
&lt;laughter&gt; It does when you want, sometimes when you want an error, 

172
00:10:20,800 --> 00:10:27,360
you don&#39;t get one. So in here, so now I&#39;m searching for a range products. So I&#39;ve got product

173
00:10:27,360 --> 00:10:29,720
with a SellEndDate between 2006

174
00:10:29,720 --> 00:10:35,290
1/1 and 2006, so I&#39;ve got 29 products as opposed to the 285

175
00:10:35,290 --> 00:10:39,000
prior, I think that we have total. So we&#39;ve got between ranges in here.

176
00:10:39,000 --> 00:10:43,390
So it is including so I will have the 31st and the 12th,

177
00:10:43,390 --> 00:10:47,450
I will have the 1st January, so I&#39;m going to have the whole range 

178
00:10:47,450 --> 00:10:51,070
in there if we sold much on the first of January or 31st of 12.

179
00:10:51,070 --> 00:10:55,510
Now we&#39;re going have a look at a list. Now remember

180
00:10:55,510 --> 00:10:59,190
what we could do with a list, is we could say WHERE product id

181
00:10:59,190 --> 00:11:02,840
equals 5 or product id 6 or product id

182
00:11:02,840 --> 00:11:07,250
7. That&#39;s an awful lot of typing compared to

183
00:11:07,250 --> 00:11:11,900
in and then 5, 6, 7. And we can put any  number of things in here. We could put

184
00:11:11,900 --> 00:11:16,890
from one thing to a thousand things, if you want to type a thousand things in there. 

185
00:11:16,890 --> 00:11:18,320
It&#39;s just a comma-separated list

186
00:11:18,320 --> 00:11:21,940
and then all of those items will appear

187
00:11:21,940 --> 00:11:24,990
as ORs essentially, this or this or this

188
00:11:24,990 --> 00:11:31,990
without having to type in all ORs.

189
00:11:32,430 --> 00:11:36,330
So we can do that one, now we&#39;ve got five 6s and 7s in there if I scroll down. 

190
00:11:36,330 --> 00:11:39,510
Notice the order as well actually. So what we said about ORDER BY this

191
00:11:39,510 --> 00:11:44,810
is sort of the first example where it&#39;s done it wrong. There is not logical order. I might

192
00:11:44,810 --> 00:11:48,300
want them ordered by CategoryID in here. So the product category ID is the

193
00:11:48,300 --> 00:11:49,470
key thing I&#39;m interested in.

194
00:11:49,470 --> 00:11:53,480
When it started with 6s, then we had some 5s, 

195
00:11:53,480 --> 00:11:57,210
then we had some more 6s then we got some 7s. So

196
00:11:57,210 --> 00:12:01,050
it may be that when I first created this query, they were actually in order.

197
00:12:01,050 --> 00:12:04,490
The order I wanted. But ultimately

198
00:12:04,490 --> 00:12:08,110
data has changed, things move around.

199
00:12:08,110 --> 00:12:12,130
So we don&#39;t want to rely on that order. If you need them in order, use an ORDER BY. 

200
00:12:12,130 --> 00:12:13,880
It&#39;s easier to always use an ORDER BY.

201
00:12:13,880 --> 00:12:17,480
So now we&#39;re going to say

202
00:12:17,480 --> 00:12:21,120
multiple clauses. So we&#39;re going to an And.

203
00:12:21,120 --> 00:12:26,390
Okay, so they have to be in there. Now remember the IN being quite useful here

204
00:12:26,390 --> 00:12:27,090
because 

205
00:12:27,090 --> 00:12:30,580
we&#39;ve effectively bracketed that as a search

206
00:12:30,580 --> 00:12:34,630
on one side of it. So we&#39;re going to they can be 5, 6, 7

207
00:12:34,630 --> 00:12:39,240
and the SellEndDate is null, we&#39;re going to find those. But if we&#39;d used

208
00:12:39,240 --> 00:12:44,070
ORs we would have to make sure that in every segment of the OR we&#39;d also

209
00:12:44,070 --> 00:12:45,980
search for SellEndDate being null.

210
00:12:45,980 --> 00:12:49,850
Or have some sort of system of brackets. Yes, or use brackets. 

211
00:12:49,850 --> 00:12:54,020
So now we&#39;ve got

212
00:12:54,020 --> 00:12:58,060
all of those with a SellEndDate of null but it&#39;s still finding 5s, 6s and 7s in

213
00:12:58,060 --> 00:12:58,670
here.

214
00:12:58,670 --> 00:13:02,260
Here actually there&#39;s an example where it&#39;s worked. It&#39;s in the right order

215
00:13:02,260 --> 00:13:06,530
5,  6, 7, so I&#39;m happy with that, send it out to someone

216
00:13:06,530 --> 00:13:10,480
there&#39;s your code. And then later on it might not come out in the right order. Yeah, it falls over.

217
00:13:10,480 --> 00:13:15,230
Now we&#39;re doing wildcards again.

218
00:13:15,230 --> 00:13:18,560
So we&#39;re going to say where ProductNumber is LIKE

219
00:13:18,560 --> 00:13:23,750
OR product category. So now, we&#39;re saying they don&#39;t have to be 5, 6, or 7

220
00:13:23,750 --> 00:13:26,810
as long as they begin FR

221
00:13:26,810 --> 00:13:30,350
in the product number. No, oh yeah, okay. So 

222
00:13:30,350 --> 00:13:33,970
we&#39;re saying so they can be 5, 6, 7

223
00:13:33,970 --> 00:13:37,400
but they don&#39;t have to be, one of those two conditions needs to be true. So either they&#39;re like FR or 

224
00:13:37,400 --> 00:13:38,320
in 5, 6, or 7

225
00:13:38,320 --> 00:13:42,560
or both. Yeah, or both. 

226
00:13:42,560 --> 00:13:45,340
So you&#39;ll find ones in here which

227
00:13:45,340 --> 00:13:49,160
begin FR, and you&#39;ll find ones in here

228
00:13:49,160 --> 00:13:53,670
which do not begin FR but they&#39;ll be in category 5, 6 or 7,

229
00:13:53,670 --> 00:13:57,010
and you find ones in here which probably, I&#39;m not going to search too long,

230
00:13:57,010 --> 00:14:01,030
which probably match both. Yeah. So we can search for both clauses in there. 

