0
00:00:05,160 --> 00:00:08,590
Welcome back! I hope you enjoyed doing the labs and I hope that you&#39;ve got

1
00:00:08,590 --> 00:00:12,300
your head around some the basic SELECT syntax now and you&#39;re ready to move on.

2
00:00:12,300 --> 00:00:15,389
So for this module we&#39;re going to look at some slightly more

3
00:00:15,389 --> 00:00:19,790
sophisticated examples of using SELECT, and Jeff is the expert so Jeff why don&#39;t 

4
00:00:19,790 --> 00:00:20,150
you

5
00:00:20,150 --> 00:00:24,730
tell us a little bit about using the SELECT statement. Thank you. So we&#39;re going to --

6
00:00:24,730 --> 00:00:27,359
we&#39;re extending what we&#39;ve done, so we  have done some SELECT already.

7
00:00:27,359 --> 00:00:31,720
Taking that a bit further we&#39;re going to look at removing duplicates from our record set

8
00:00:31,720 --> 00:00:35,540
and we saw there were some before where we got the same sizes and they were repeated in there.

9
00:00:35,540 --> 00:00:38,940
We talked about ORDER BY and we&#39;re going to have a look at

10
00:00:38,940 --> 00:00:43,089
using that in practice and how we can sort our results.

11
00:00:43,089 --> 00:00:48,610
And then a few more things we will work with a bit more complex 

12
00:00:48,610 --> 00:00:49,049
sorted

13
00:00:49,049 --> 00:00:53,199
results and we&#39;ll also look at how we can filter our data. So at the moment

14
00:00:53,199 --> 00:00:54,870
we&#39;re finding every single row

15
00:00:54,870 --> 00:00:58,699
and we want to reduce that down to actually the specific rows we are

16
00:00:58,699 --> 00:00:59,640
interested in,

17
00:00:59,640 --> 00:01:04,940
we want find, and how we can do that. Ok, great. So to start off with removing duplicates.

18
00:01:04,940 --> 00:01:08,899
A lot of the data, we&#39;ve been showing a few columns but if we had just shown one

19
00:01:08,899 --> 00:01:10,299
column and there had been lots

20
00:01:10,299 --> 00:01:14,240
of size 58 or medium and that would have been repeated.

21
00:01:14,240 --> 00:01:17,250
So every single row that we&#39;ve got

22
00:01:17,250 --> 00:01:21,560
will be returned. So if I&#39;ve  58 rows and they all have the same piece of information

23
00:01:21,560 --> 00:01:28,420
I&#39;m going to get 58 entries with that same piece of information. So

24
00:01:28,420 --> 00:01:31,780
in here we&#39;ve got a list of colors

25
00:01:31,780 --> 00:01:35,140
for our product. Now we&#39;re going to get one row per

26
00:01:35,140 --> 00:01:38,640
every single item I&#39;ve got in there. So although I&#39;ve got

27
00:01:38,640 --> 00:01:44,460
two blues, two yellows, I&#39;m getting one per row. So I don&#39;t really want that, I want to know

28
00:01:44,460 --> 00:01:48,289
what colors are we producing. So this is --

29
00:01:48,289 --> 00:01:51,590
you&#39;re getting one for each product, it&#39;s not one for each color. It&#39;s one for each product. 

30
00:01:51,590 --> 00:01:53,439
Every single product got its own row.

31
00:01:53,439 --> 00:01:56,460
Yeah, so you&#39;re getting one row back for every product and

32
00:01:56,460 --> 00:02:00,249
that means you&#39;re going to have multiple colors because some products are the same color.

33
00:02:00,249 --> 00:02:04,079
Now, you&#39;ve got SELECT ALL but you haven&#39;t put ALL in the query. 

34
00:02:04,079 --> 00:02:08,450
In many cases actually people who write SQL are somewhat lazy.

35
00:02:08,449 --> 00:02:11,700
Okay. And we have a tendency to leave out

36
00:02:11,700 --> 00:02:14,890
unnecessary words. In fact SELECT ALL,

37
00:02:14,890 --> 00:02:19,580
you will practically never see written because everyone understands that you want,

38
00:02:19,580 --> 00:02:22,950
if you don&#39;t define anything else, you want all of the rows.

39
00:02:22,950 --> 00:02:27,330
So we don&#39;t tend to write SELECTA ALL. You mentioned as when we were doing aliases,

40
00:02:27,330 --> 00:02:31,170
it&#39;s good practice to use that because if we leave it out looks very similar to

41
00:02:31,170 --> 00:02:34,200
a comma being there and that means something completely different. So we do

42
00:02:34,200 --> 00:02:36,200
tend to type as although we don&#39;t need it.

43
00:02:36,200 --> 00:02:40,340
ALL I&#39;m trying to think, I don&#39;t think I&#39;ve ever seen anyone

44
00:02:40,340 --> 00:02:43,900
write it in all seriousness because you don&#39;t have to. And as we get later

45
00:02:43,900 --> 00:02:46,290
on you&#39;ll see there&#39;s some things actually where we have

46
00:02:46,290 --> 00:02:50,310
optional, we&#39;ll talk well you could put this word in, or sometimes you could

47
00:02:50,310 --> 00:02:52,150
but different words in, it does the same thing.

48
00:02:52,150 --> 00:02:56,370
Because it&#39;s quite a fluid language, it&#39;s  quite

49
00:02:56,370 --> 00:02:59,430
reasonable with its programmers.

50
00:02:59,430 --> 00:03:03,440
A lot of procedural languages are very very strict, this is not. We can actually

51
00:03:03,440 --> 00:03:07,410
do things in slightly different ways. It doesn&#39;t make any difference to what the

52
00:03:07,410 --> 00:03:10,849
end result is. SELECT or SELECT ALL it&#39;s going to bring in this case, it&#39;s going to back a 

53
00:03:10,849 --> 00:03:14,690
row for every product. And that means in my result set, if I&#39;m only looking at the color,

54
00:03:14,690 --> 00:03:18,560
I still get a row for each product. So I&#39;ve I&#39;ve got multiple products with  the same color, I get

55
00:03:18,560 --> 00:03:19,150
multiple colors. 

56
00:03:19,150 --> 00:03:23,280
Yep, you get the same number of rows  as you have products. Okay, so

57
00:03:23,280 --> 00:03:27,030
to get rid of those excess ones, because I only want to know what colors

58
00:03:27,030 --> 00:03:27,919
do we sell,

59
00:03:27,919 --> 00:03:31,470
then we can use that keyword DISTINCT. So it&#39;s an alternative to ALL

60
00:03:31,470 --> 00:03:33,480
actually, as we said we don&#39;t really type ALL, 

61
00:03:33,480 --> 00:03:37,150
so if we put the words SELECT DISTINCT in we get distinct colors

62
00:03:37,150 --> 00:03:40,269
in there. Now if I had another column though,

63
00:03:40,269 --> 00:03:45,200
we&#39;re talking about distinct in terms of everything I&#39;ve typed in. So if I had

64
00:03:45,200 --> 00:03:48,260
DISTINCT Color, Size

65
00:03:48,260 --> 00:03:51,470
then it would be distinct /color /size combinations.

66
00:03:51,470 --> 00:03:55,120
So it&#39;s distinct at the row level not at the individual column level. That&#39;s right, yeah, 

67
00:03:55,120 --> 00:03:57,299
because otherwise how would it which

68
00:03:57,299 --> 00:04:01,660
color row to display. So if it were blue, it wouldn&#39;t know to do medium,

69
00:04:01,660 --> 00:04:03,989
large, or small it doesn&#39;t understand. So it displays all of 

70
00:04:03,989 --> 00:04:05,639
the distinct rows. 

