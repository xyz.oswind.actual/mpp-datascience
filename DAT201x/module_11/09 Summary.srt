0
00:00:02,360 --> 00:00:05,750
And that takes us to the end of our

1
00:00:05,750 --> 00:00:10,400
final module. We&#39;ve talked a little bit about errors and error messages and

2
00:00:10,400 --> 00:00:13,970
the way that those are thrown by the system or that you can throw

3
00:00:13,970 --> 00:00:15,370
yourself. You can raise errors

4
00:00:15,370 --> 00:00:20,010
using RAISERROR or THROW. We&#39;ve talked about catching and handling errors using TRY/CATCH

5
00:00:20,010 --> 00:00:20,680
blocks.

6
00:00:20,680 --> 00:00:25,260
And then we talked about transactions to keep your data modifications consistent

7
00:00:25,260 --> 00:00:28,770
even if you&#39;re updating multiple tables or you doing multiple inserts.

8
00:00:28,770 --> 00:00:32,070
Making sure they all happened as a unit or they all fail

9
00:00:32,070 --> 00:00:34,840
so that you don&#39;t end up with partially committed data.

