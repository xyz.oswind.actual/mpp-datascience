0
00:00:02,280 --> 00:00:07,340
Let&#39;s dive on and have a look first of all at errors and error messages. And Jeff, I

1
00:00:07,340 --> 00:00:12,749
know that you&#39;ve worked with a lot of different database solutions in the past. It&#39;s a critical

2
00:00:12,749 --> 00:00:16,720
thing really isn&#39;t it? Being able to handle errors. Yes, and I think you&#39;ve brought up

3
00:00:16,720 --> 00:00:22,620
a good point in the introduction saying you can raise these yourself. This isn&#39;t my hard

4
00:00:22,620 --> 00:00:27,870
disk has failed necessarily, this is I would like to raise an error because of a condition.

5
00:00:27,870 --> 00:00:33,000
So we&#39;re not just saying there&#39;s been a system error, it could be some business logic that

6
00:00:33,000 --> 00:00:37,679
says that&#39;s perfectly legal thing to do in terms of what&#39;s in the database, but my business

7
00:00:37,679 --> 00:00:42,079
logic determines that you can&#39;t have a price greater than 100 or something like that and

8
00:00:42,079 --> 00:00:46,440
I want to throw an error. Yeah, and it can even be informational. We can be very low

9
00:00:46,440 --> 00:00:52,510
level. Yeah, and that&#39;s an interesting point as well that when we see error we assume somethings

10
00:00:52,510 --> 00:00:57,539
gone catastrophically wrong and there&#39;s an error message. An error might just be I want

11
00:00:57,539 --> 00:01:02,659
to pass back some sort of informational message to the client to say yeah you know you did

12
00:01:02,659 --> 00:01:06,899
this update, actually it succeeded but it didn&#39;t affect any rows. It&#39;s just not really an

13
00:01:06,899 --> 00:01:10,750
error, but it&#39;s some information I might want to pass back to the client application and

14
00:01:10,750 --> 00:01:17,479
let the client application deal with that. Yep. So when we&#39;re talking about errors, we

15
00:01:17,479 --> 00:01:23,000
kind of have to understand what errors actually are. And really they consist of a number of

16
00:01:23,000 --> 00:01:27,780
different kind of attributes, a number of attributes of an error. It&#39;s a situation that

17
00:01:27,780 --> 00:01:34,170
arises, the system or your own code will flag up and error, throw an error is the terminology

18
00:01:34,170 --> 00:01:40,259
we use. And an error consists of some sort of unique number that identifies that error.

19
00:01:40,259 --> 00:01:45,030
Like everything else in databases, we have unique identifiers and errors have a number

20
00:01:45,030 --> 00:01:50,119
that identify the error. And then there&#39;s usually a message that goes along with that.

21
00:01:50,119 --> 00:01:54,789
Some text that describes you know what is the error, what&#39;s the problem, what&#39;s message

22
00:01:54,789 --> 00:02:01,579
we want to send back. So those two things are fairly kind of obvious, and I guess at

23
00:02:01,579 --> 00:02:05,039
the very minimum those are the things that you would deal with when handling errors.

24
00:02:05,039 --> 00:02:10,399
But there are some other aspects of errors that are interesting. So we&#39;ve got in the

25
00:02:10,399 --> 00:02:16,760
list here, severity. So Jeff, tell me a little bit about that. Well, the low level severities

26
00:02:16,760 --> 00:02:22,340
we&#39;re saying could just be a message. So we want to notify the client of something. And

27
00:02:22,340 --> 00:02:27,989
that&#39;s typically your low level severities. One to ten are just informational messages,

28
00:02:27,989 --> 00:02:32,269
and then when you start to get to higher numbers and they&#39;ve become -- there is actually a

29
00:02:32,269 --> 00:02:36,409
problem. Something has gone wrong in here, we&#39;re saying it&#39;s a higher severity. And when

30
00:02:36,409 --> 00:02:41,830
you get to the very highest up in the 20s then actually that&#39;s catastrophic errors.

31
00:02:41,830 --> 00:02:47,780
That kills the connection to the server, you know bad things have happened to the server.

32
00:02:47,780 --> 00:02:53,250
So we want to be able to distinguish between, because they are as well as errors, they can

33
00:02:53,250 --> 00:02:57,560
just be messages. They can be informational. We need to be able to distinguish those levels,

34
00:02:57,560 --> 00:03:03,170
really that sort of three levels that we have of error in there. So when you throw a message

35
00:03:03,170 --> 00:03:08,950
that&#39;s not going to be the very highest one. We&#39;ve got to mention that not in SQL, not

36
00:03:08,950 --> 00:03:14,040
in Azure SQL Database sys.messages, but in SQL Server we can create a message. And you

37
00:03:14,040 --> 00:03:19,230
can define some of those portions of it and say ok, I&#39;d like it to be at this level. And

38
00:03:19,230 --> 00:03:22,909
then we can test for that as well and say well let&#39;s have a condition to say what is

39
00:03:22,909 --> 00:03:29,129
the severity of that value? And then based on that, perform different things. Yeah and

40
00:03:29,129 --> 00:03:33,129
I guess it&#39;s worth pointing out as well, when we throw our own errors, as you say in SQL

41
00:03:33,129 --> 00:03:38,269
Server we can add messages to sys.messages and we&#39;ll talk more about in a second, but

42
00:03:38,269 --> 00:03:42,189
even the errors that you don&#39;t raise, the system errors that get thrown will have a

43
00:03:42,189 --> 00:03:48,930
severity that&#39;s predefined. And if an error is thrown that has a critical level of severity,

44
00:03:48,930 --> 00:03:53,819
SQL Server itself will do certain things in order to cope with that. So as you said if

45
00:03:53,819 --> 00:03:58,019
it&#39;s a really high severity message, SQL Server will automatically close the connection and

46
00:03:58,019 --> 00:04:02,769
perhaps take the database offline or whatever it needs to do. So if the disk becomes corrupt

47
00:04:02,769 --> 00:04:07,310
or if something major happens to the system, SQL Server will throw an error and then it

48
00:04:07,310 --> 00:04:10,989
will take some sort of action to mitigate that error. But what you might want to do

49
00:04:10,989 --> 00:04:15,500
is have any code that you&#39;ve got running at the time to catch the error and log it

50
00:04:15,500 --> 00:04:18,769
so that later on you know what went wrong and you get an idea of what was happening

51
00:04:18,769 --> 00:04:23,910
there. Some of these you&#39;ll have seen if you do the labs, that some of them have intentional failures

52
00:04:23,910 --> 00:04:27,900
in there for a reason. We&#39;ll say oh no, we should have coded it that way. And you&#39;ll

53
00:04:27,900 --> 00:04:33,410
see all this information because it will be thrown out by the system and you&#39;ll see all

54
00:04:33,410 --> 00:04:37,110
that information in there, the message, line number is all listed in there. Yeah, in the

55
00:04:37,110 --> 00:04:43,340
UI. So as well as the severity, the other thing that you have is a state. And state

56
00:04:43,340 --> 00:04:49,610
isn&#39;t really anything to do with how severe the error is, it&#39;s a code that&#39;s used to indicate

57
00:04:49,610 --> 00:04:54,410
what the SQL Server operating system, rather than the Windows operating system, what the

58
00:04:54,410 --> 00:04:59,490
SQL Server system was doing at the time. And it is primarily something that you would use

59
00:04:59,490 --> 00:05:04,210
if you went to your support representative who is supporting your database or you came

60
00:05:04,210 --> 00:05:07,960
to Microsoft support to get help with something that was happening, they would look at that

61
00:05:07,960 --> 00:05:12,830
state and use it to diagnose what was going on in the system at the time and use that

62
00:05:12,830 --> 00:05:18,419
to figure things out. So it&#39;s really for kind of after the fact troubleshooting. There&#39;s

63
00:05:18,419 --> 00:05:23,340
another couple of things that get thrown out as well, the procedure. If you&#39;ve written

64
00:05:23,340 --> 00:05:27,949
stored procedures as we covered in the last module, and while that stored procedure is

65
00:05:27,949 --> 00:05:33,400
running an error occurs, one of the things that will be part of the error that gets raised

66
00:05:33,400 --> 00:05:37,800
is the name of the procedure. So we know which stored procedure was running when the error

67
00:05:37,800 --> 00:05:42,630
occurred. And if you&#39;ve got a stored procedure that calls another stored procedure that calls another stored procedure, you can

68
00:05:42,630 --> 00:05:48,500
use that to track down where the error occurred. And then to help you get even more accurate information

69
00:05:48,500 --> 00:05:51,949
about where it occurred, there&#39;s the line number within that stored procedure where

70
00:05:51,949 --> 00:05:57,080
the error actually occurred. So you can get right to the individual statement of Transact-SQL

71
00:05:57,080 --> 00:06:03,340
that ran and presumably caused the error or at least it was running when the error occurred.

72
00:06:03,340 --> 00:06:08,130
So I mentioned, just before we move off this slide, we mentioned the sys.messages table. And this

73
00:06:08,130 --> 00:06:12,849
is another one of those areas where certainly at the moment, at the time we&#39;re recording

74
00:06:12,849 --> 00:06:19,289
this, there&#39;s a difference between SQL Server and Azure SQL Database. In SQL Server and

75
00:06:19,289 --> 00:06:23,080
historically over lots of versions of SQL Server it&#39;s been like this certainly as far

76
00:06:23,080 --> 00:06:28,229
as I can remember, there&#39;s a system table called sys.messages. And that&#39;s where all

77
00:06:28,229 --> 00:06:33,259
of the system error messages are stored. SQL Server actually uses a table in the database

78
00:06:33,259 --> 00:06:38,660
to store the different messages that it can throw. And when it&#39;s ready to throw a message,

79
00:06:38,660 --> 00:06:45,660
it throws the relevant message from that database table. Now you can in SQL Server yourself, add your

80
00:06:46,699 --> 00:06:52,319
own user-defined errors to that table. And there&#39;s a system stored procedure called sp_addmessage

81
00:06:52,319 --> 00:06:58,889
that you can use to go ahead and add your own custom error. And that error could include

82
00:06:58,889 --> 00:07:04,250
a custom severity level, a custom message, all of the kind of attributes that you would

83
00:07:04,250 --> 00:07:09,729
have of a system error. And then if you have code that wants to throw that error or raise

84
00:07:09,729 --> 00:07:13,960
that error, you can simply throw it by specifying it&#39;s message number and all the rest of the

85
00:07:13,960 --> 00:07:19,490
information is got out of the sys.messages table and thrown. And that&#39;s a technique that

86
00:07:19,490 --> 00:07:24,550
has been used in a lot of databases over the years in SQL Server. It&#39;s not necessarily

87
00:07:24,550 --> 00:07:30,509
one that I would use now going forward because we have a new piece of syntax that we use

88
00:07:30,509 --> 00:07:34,759
to throw errors and we&#39;re trying to get away from the idea of user-defined messages having

89
00:07:34,759 --> 00:07:38,930
different severity levels because that affects how the system behaves and can actually make

90
00:07:38,930 --> 00:07:44,889
it a bit unpredictable. So you can use that technique, you may be maintaining a lot of

91
00:07:44,889 --> 00:07:49,300
code that already uses that technique and it&#39;s good to be aware of that. But also be

92
00:07:49,300 --> 00:07:55,229
aware that if you are migrating to Azure SQL Database, and you&#39;re doing some code there,

93
00:07:55,229 --> 00:08:00,259
there is no sys.messages table there that you can access. And you haven&#39;t got the ability

94
00:08:00,259 --> 00:08:04,150
to create your own messages and store them permanently. Rather what you do is throw the

95
00:08:04,150 --> 00:08:09,470
error messages as and when you need to throw them from your line of code. Yep. If you do

96
00:08:09,470 --> 00:08:13,949
create them, one thing I would say that can be useful, the message you have can have

97
00:08:13,949 --> 00:08:19,560
parameters with in it. So we can -- we don&#39;t have to say well we need one message for the Product table,

98
00:08:19,560 --> 00:08:26,139
we need another message for the Sales table we need another message -- we can pass let&#39;s say the name of that table

99
00:08:26,139 --> 00:08:31,060
into the message that we&#39;ve got. So we can create a message which has parameters and they can

100
00:08:31,060 --> 00:08:36,540
be numbers, they can be strings, and we can pass values into there so you can have a message

101
00:08:36,539 --> 00:08:41,970
that&#39;s a bit more variable. Yeah. And of course if you wanted to use that technique, there&#39;s

102
00:08:41,970 --> 00:08:46,610
nothing to stop you creating your own sys.messages table for your own user-defined error messages

103
00:08:46,610 --> 00:08:47,840
and effectively using the same technique.

