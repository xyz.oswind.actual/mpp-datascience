0
00:00:04,480 --> 00:00:06,460
So in this demonstration we&#39;ll have a look

1
00:00:06,460 --> 00:00:10,250
at seeing how we can throw errors. How we can raise errors.

2
00:00:10,250 --> 00:00:14,020
So I&#39;ve got in this example here

3
00:00:14,020 --> 00:00:18,050
just a regular INSERT statement here were I&#39;m inserting into a

4
00:00:18,050 --> 00:00:23,329
table some values. And I&#39;m going to go ahead and run that code.

5
00:00:23,329 --> 00:00:28,359
And I immediately get

6
00:00:28,359 --> 00:00:31,269
an error. I can see in the user interface in SQL Server 

7
00:00:31,269 --> 00:00:32,180
Management Studio it&#39;s an error because

8
00:00:32,180 --> 00:00:36,080
I get this bright red text at the bottom the tells me. And I can see the

9
00:00:36,080 --> 00:00:39,920
message number, that&#39;s the error number there, which I&#39;ve just clicked away from,

10
00:00:39,920 --> 00:00:42,950
the level, the severity level, the state,

11
00:00:42,950 --> 00:00:45,920
the line on which it occurred. There&#39;s no procedure because of course this isn&#39;t in a stored

12
00:00:45,920 --> 00:00:48,620
procedure, but I can see that it&#39;s line 2 of this

13
00:00:48,620 --> 00:00:52,100
set of script here because the first line has the comment on it.

14
00:00:52,100 --> 00:00:57,440
And I can see what the error message is. It&#39;s the INSERT statement conflicted with

15
00:00:57,440 --> 00:00:58,710
foreign key constraint

16
00:00:58,710 --> 00:01:02,690
which has a really confusing name. The conflict occurred in the database

17
00:01:02,690 --> 00:01:05,379
AdventureWorksLT and the table was

18
00:01:05,379 --> 00:01:09,640
SalesOrderHeader, the column SalesOrderID. And this is an example of

19
00:01:09,640 --> 00:01:12,820
what Jeff mentioned earlier on when we were looking at the slides,

20
00:01:12,820 --> 00:01:15,920
that these error messages may be parameterized.

21
00:01:15,920 --> 00:01:20,050
That same generic error number 547

22
00:01:20,050 --> 00:01:23,860
will be thrown anytime you try to insert into a table that

23
00:01:23,860 --> 00:01:27,800
has a foreign key and there isn&#39;t a matching primary key in the other

24
00:01:27,800 --> 00:01:28,290
table

25
00:01:28,290 --> 00:01:31,970
which is what this means. But we parameterize the name of the

26
00:01:31,970 --> 00:01:35,110
table and the name of the column, and even the name to the database

27
00:01:35,110 --> 00:01:39,260
so that is customizing the message if you like.

28
00:01:39,260 --> 00:01:44,080
So that&#39;s an error that&#39;s been raised by SQL Server itself, it&#39;s not -- I

29
00:01:44,080 --> 00:01:44,619
didn&#39;t

30
00:01:44,619 --> 00:01:48,020
write any code to throw that error. SQL Server threw it automatically

31
00:01:48,020 --> 00:01:51,530
because I tried to insert a row into the SalesOrderDetail table

32
00:01:51,530 --> 00:01:55,490
and there isn&#39;t a matching sales order to go with that sales order detail.

33
00:01:55,490 --> 00:01:59,140
What if I try to do something that is

34
00:01:59,140 --> 00:02:03,729
perfectly legitimate in terms of Transact-SQL, it isn&#39;t going to break any

35
00:02:03,729 --> 00:02:06,299
constraints or change any data consistency,

36
00:02:06,299 --> 00:02:10,490
but my business rules are such that I want to you know mark that as an error and then do

37
00:02:10,490 --> 00:02:12,069
something about it?

38
00:02:12,069 --> 00:02:15,250
Well we&#39;ve looked at row count. In a couple of the previous modules we&#39;ve talked

39
00:02:15,250 --> 00:02:18,250
about the row count when you update tables or insert tables. And 

40
00:02:18,250 --> 00:02:21,630
we previously saw how to use an IF statement to check on the value of

41
00:02:21,630 --> 00:02:22,690
row count and 

42
00:02:22,690 --> 00:02:26,599
print a different message depending on how many rows were affected. And what I might want

43
00:02:26,599 --> 00:02:27,830
to do is I might want to say well

44
00:02:27,830 --> 00:02:32,269
if I have a statement that doesn&#39;t manage to affect any rows, that&#39;s

45
00:02:32,269 --> 00:02:34,220
probably not what I intended.

46
00:02:34,220 --> 00:02:37,810
But that&#39;s not an error though. It&#39;s not an error. Not doing anything isn&#39;t an error. 

47
00:02:37,810 --> 00:02:41,260
Yeah, nothing has failed, but I do want to throw a message to the client to say

48
00:02:41,260 --> 00:02:43,489
hey you tried to do this statement and

49
00:02:43,489 --> 00:02:47,310
it didn&#39;t actually succeed and do rows. It&#39;s then up to the client to decide

50
00:02:47,310 --> 00:02:50,549
whether it wants to handle that error and continue or whether it wants to do

51
00:02:50,549 --> 00:02:51,510
something about it.

52
00:02:51,510 --> 00:02:54,560
But I&#39;m effectively just throwing the error to the client so that it

53
00:02:54,560 --> 00:02:55,060
knows

54
00:02:55,060 --> 00:02:58,700
something anomalous happened. So

55
00:02:58,700 --> 00:03:02,170
if I go and run all that code, I&#39;m updating  the

56
00:03:02,170 --> 00:03:05,590
the Product table, I&#39;m setting the DiscontinuedDate to today&#39;s date

57
00:03:05,590 --> 00:03:10,459
where ProductID equals 0. Now I know for a fact there aren&#39;t any products with the ID 0,

58
00:03:10,459 --> 00:03:13,769
so I&#39;m checking after I&#39;ve done that if the row count is less than one,

59
00:03:13,769 --> 00:03:18,030
then I&#39;m going to use RAISERROR to go ahead and throw that error. So I&#39;m 

60
00:03:18,030 --> 00:03:21,290
raising an error with this message here &quot;The product was not found -- no products have been 

61
00:03:21,290 --> 00:03:21,880
updated&quot;

62
00:03:21,880 --> 00:03:24,910
severity level 16, state 0.

63
00:03:24,910 --> 00:03:31,290
So I&#39;ll go ahead and run that. And you&#39;ll see yes, it returned 50000 is the default

64
00:03:31,290 --> 00:03:35,299
error number. I didn&#39;t specify an error number, but because I used RAISERROR 

65
00:03:35,299 --> 00:03:35,790
to

66
00:03:35,790 --> 00:03:40,049
raise a custom error message, it automatically gets assigned that number,

67
00:03:40,049 --> 00:03:43,109
on it passes out my message.

68
00:03:43,109 --> 00:03:47,319
Now I mentioned in the slides that RAISERROR has been there for a while and it has

69
00:03:47,319 --> 00:03:50,170
kind of been superseded by the THROW statement.

70
00:03:50,170 --> 00:03:54,019
So here&#39;s an example of exactly the same scenario

71
00:03:54,019 --> 00:03:58,660
but this time I&#39;m going to use THROW if no rows affected. And I&#39;m throwing

72
00:03:58,660 --> 00:03:59,690
specifically

73
00:03:59,690 --> 00:04:03,319
a message number, it has to be more than 50000, so I&#39;m 

74
00:04:03,319 --> 00:04:07,139
throwing 50001. And I can use whatever numbers I want to use and I might

75
00:04:07,139 --> 00:04:10,540
you know again for troubleshooting purposes, I might categorize my errors 

76
00:04:10,540 --> 00:04:11,030
and I might

77
00:04:11,030 --> 00:04:14,380
create a table that stores them so that I  know which one means which.

78
00:04:14,380 --> 00:04:17,650
And I&#39;m passing the same error message

79
00:04:17,649 --> 00:04:22,220
with a state of 0. I&#39;m not specifying the severity because I can&#39;t do that with

80
00:04:22,220 --> 00:04:27,180
THROW. But it comes back with the severity level of 16 and

81
00:04:27,180 --> 00:04:30,860
I get the information about what&#39;s been thrown. Note it automatically picks up a line

82
00:04:30,860 --> 00:04:31,830
number as well, 

83
00:04:31,830 --> 00:04:36,720
that comes out there as well. So we&#39;ve seen a couple examples there. First of all

84
00:04:36,720 --> 00:04:37,160
that

85
00:04:37,160 --> 00:04:41,790
SQL Server itself might throw errors. And  secondly in my own code I can use 

86
00:04:41,790 --> 00:04:42,420
RAISERROR

87
00:04:42,420 --> 00:04:45,580
or more commonly going forward I could use THROW

88
00:04:45,580 --> 00:04:49,250
to throw an error. And that just protects you. You can&#39;t do anything

89
00:04:49,250 --> 00:04:52,560
too bad with the THROW. You&#39;re not allowed to

90
00:04:52,560 --> 00:04:57,130
elevate your severity up to an unreasonably high-level.

91
00:04:57,130 --> 00:05:00,510
Yeah the idea is to try to avoid writing your own Transact-SQL code 

92
00:05:00,510 --> 00:05:04,050
that ultimately could bring the server down because you&#39;ve raised a really

93
00:05:04,050 --> 00:05:04,900
critical error.

94
00:05:04,900 --> 00:05:08,920
If there is a really critical error that means SQL Server has to stop,

95
00:05:08,920 --> 00:05:11,460
SQL Server itself will spot that and it will deal with that. Yep.

