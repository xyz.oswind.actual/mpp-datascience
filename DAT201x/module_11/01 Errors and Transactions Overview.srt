0
00:00:05,259 --> 00:00:07,569
Well, welcome back to the 11th

1
00:00:07,569 --> 00:00:10,860
and final module in our Querying with Transact-SQL course.

2
00:00:10,860 --> 00:00:14,629
In this module we&#39;re going to build on all of the things that we&#39;ve talked about up

3
00:00:14,629 --> 00:00:15,209
to here,

4
00:00:15,209 --> 00:00:18,490
and we&#39;ll deal with one final really critical part of

5
00:00:18,490 --> 00:00:22,200
building solutions with Transact-SQL and that is handling any errors that

6
00:00:22,200 --> 00:00:22,930
might occur

7
00:00:22,930 --> 00:00:27,010
and using transactions to ensure the consistency of data

8
00:00:27,010 --> 00:00:30,810
that we&#39;re working with in the database. So it&#39;s a critical part of any

9
00:00:30,810 --> 00:00:31,350
 

10
00:00:31,350 --> 00:00:35,660
Transact-SQL solution and this will really kind of round off everything that we&#39;ve

11
00:00:35,660 --> 00:00:36,340
talked about it

12
00:00:36,340 --> 00:00:40,120
in the previous modules. So we&#39;re

13
00:00:40,120 --> 00:00:43,300
going to talk about a number different things in this module. We&#39;re going to talk about

14
00:00:43,300 --> 00:00:47,930
errors and error messages. So either something goes wrong in the

15
00:00:47,930 --> 00:00:49,880
system itself and an error occurs,

16
00:00:49,880 --> 00:00:54,710
and we&#39;ll see the error and the error message, or you yourself will have some logic that

17
00:00:54,710 --> 00:00:58,210
you&#39;ve determined a condition has arisen that you want to throw an error for. So we&#39;ll 

18
00:00:58,210 --> 00:01:02,950
look at how we handle that. We&#39;ll look at how we raise those errors, we&#39;ll look

19
00:01:02,950 --> 00:01:05,939
at catching and handling errors in your Transact-SQL code so that

20
00:01:05,939 --> 00:01:08,049
you can recover from an error situation and

21
00:01:08,049 --> 00:01:11,070
get the database to state that you&#39;re happy with

22
00:01:11,070 --> 00:01:14,840
even in the event of something going wrong. And then related to that, we&#39;ll talk

23
00:01:14,840 --> 00:01:15,990
about transactions

24
00:01:15,990 --> 00:01:20,070
where I might want to do updates or inserts to multiple tables or

25
00:01:20,070 --> 00:01:24,390
make changes that I want to remain consistent across multiple operations.

26
00:01:24,390 --> 00:01:28,350
And I&#39;ll use transactions to ensure that either everything I&#39;m doing succeeds

27
00:01:28,350 --> 00:01:32,270
or it all gets rolled back and cancelled in the event that something goes wrong.

