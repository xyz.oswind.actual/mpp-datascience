0
00:00:02,429 --> 00:00:05,210
So we&#39;ve talked about throwing errors and 

1
00:00:05,210 --> 00:00:08,549
we mentioned catching errors a little bit. Let&#39;s look in more detail at

2
00:00:08,549 --> 00:00:13,039
what&#39;s involved in catching and handling our errors. And the first thing is

3
00:00:13,039 --> 00:00:17,840
in common with a lot of programming languages some of what you might already be

4
00:00:17,840 --> 00:00:22,270
using. We use this technique -- the technique is generally called

5
00:00:22,270 --> 00:00:25,700
structured exception handling -- and what we&#39;re really doing is we have a

6
00:00:25,700 --> 00:00:29,280
TRY block of the thing, the code that we want to run, the code 

7
00:00:29,280 --> 00:00:32,820
that does what we want to do, but then we have a CATCH block

8
00:00:32,820 --> 00:00:35,940
so that in the event something failed in what we were trying,

9
00:00:35,940 --> 00:00:39,629
we then catch the error that happened and we handle that error. And we either

10
00:00:39,629 --> 00:00:42,929
deal with the error and fix it, or we

11
00:00:42,929 --> 00:00:46,269
log the error, we recognize that something went wrong and we

12
00:00:46,269 --> 00:00:50,249
clean up as best we can, and possibly throw an error to the client

13
00:00:50,249 --> 00:00:51,420
application the called this

14
00:00:51,420 --> 00:00:56,039
so that it can then deal with that error. So if nothing goes wrong in

15
00:00:56,039 --> 00:00:56,569
there,

16
00:00:56,569 --> 00:01:01,139
the TRY, does it just behave normally? Yeah, 

17
00:01:01,139 --> 00:01:04,970
if nothing goes wrong, all the code in the TRY block gets executed and the CATCH 

18
00:01:04,970 --> 00:01:06,780
never runs. The CATCH only runs 

19
00:01:06,780 --> 00:01:12,760
if an exception occurs. So we have this TRY/CATCH block, it is worth

20
00:01:12,760 --> 00:01:17,050
mentioning you can handle errors in line. And that&#39;s a technique that a

21
00:01:17,050 --> 00:01:17,520
lot of

22
00:01:17,520 --> 00:01:20,800
people who grew up on kind of basic programming might be used to the

23
00:01:20,800 --> 00:01:21,460
idea of just

24
00:01:21,460 --> 00:01:24,340
after every time you do something that might cause an error, you check to

25
00:01:24,340 --> 00:01:25,930
see if it caused an error, and then you

26
00:01:25,930 --> 00:01:29,910
deal with it. You can do that, you can check for, there&#39;s a global

27
00:01:29,910 --> 00:01:33,490
error number that you can check for global variable called error 

28
00:01:33,490 --> 00:01:34,690
number that you can check for.

29
00:01:34,690 --> 00:01:39,490
But really we try and advise people you know don&#39;t try and do that, at least

30
00:01:39,490 --> 00:01:41,410
the really messy code that you can&#39;t

31
00:01:41,410 --> 00:01:45,110
troubleshoot easily, much better to use this structured exception handling

32
00:01:45,110 --> 00:01:45,630
approach.

33
00:01:45,630 --> 00:01:50,600
The TRY and CATCH block. If you have used TRY and CATCH in other languages

34
00:01:50,600 --> 00:01:53,560
like C#, there is no FINALLY block, there&#39;s no

35
00:01:53,560 --> 00:01:56,710
additional thing that I can run in the event of whatever happens.

36
00:01:56,710 --> 00:02:02,790
It&#39;s just TRY and CATCH in Transact-SQL. In that CATCH block, that&#39;s where I&#39;m going to

37
00:02:02,790 --> 00:02:05,330
actually handle the error that occurs.

38
00:02:05,330 --> 00:02:08,640
And of course one of the things I might want to do is actually interrogate the error. Find

39
00:02:08,639 --> 00:02:09,690
out about what happened.

40
00:02:09,690 --> 00:02:13,680
And so there&#39;s a number of system global variables

41
00:02:13,680 --> 00:02:17,860
and functions that I can use. And I can do things like look at the --

42
00:02:17,860 --> 00:02:21,990
find out what error number occurred, I can find out all the different bits of the error. 

43
00:02:21,990 --> 00:02:24,270
So the message, the states, the

44
00:02:24,270 --> 00:02:28,510
severity, I can get all of that. And I can write some fairly sophisticated logic in my 

45
00:02:28,510 --> 00:02:30,690
CATCH block that depending on 

46
00:02:30,690 --> 00:02:35,570
the error, the number, the state or whatever does something different. So

47
00:02:35,570 --> 00:02:36,730
I might catch 

48
00:02:36,730 --> 00:02:40,940
an error and if I know the severity level is 16 or less,

49
00:02:40,940 --> 00:02:44,950
then I might tidy up, log it, and then try and continue.

50
00:02:44,950 --> 00:02:48,150
If the severity level is higher than that, I might

51
00:02:48,150 --> 00:02:51,760
throw an error to the client, I might you know do some additional detailed logging

52
00:02:51,760 --> 00:02:52,290
or

53
00:02:52,290 --> 00:02:56,480
or some other thing like that. I&#39;ve seen loads of people using that information, the

54
00:02:56,480 --> 00:02:57,460
@@ERROR,

55
00:02:57,460 --> 00:03:01,860
and quite often they end up writing another line of code though. And so what

56
00:03:01,860 --> 00:03:02,480
happens?

57
00:03:02,480 --> 00:03:06,770
Then they go well it doesn&#39;t seem to have been an error anymore. Yeah, that&#39;s a good point.

58
00:03:06,770 --> 00:03:07,810
Once you&#39;ve 

59
00:03:07,810 --> 00:03:11,050
read the @@ERROR, if you then execute a line of code, in fact even executing the 

60
00:03:11,050 --> 00:03:13,500
line of code that reads the @@ERROR

61
00:03:13,500 --> 00:03:16,550
means that there&#39;s now no longer an outstanding error in the system. Because

62
00:03:16,550 --> 00:03:18,310
the last piece of code that was executed

63
00:03:18,310 --> 00:03:22,030
run successfully. It was the piece of code  that checked the error number.

64
00:03:22,030 --> 00:03:24,940
So we really need to put it somewhere. Yeah, you need to catch it and then put it somewhere

65
00:03:24,940 --> 00:03:27,790
in the first place. Into a variable or something like that.

66
00:03:27,790 --> 00:03:33,070
Techniques that you can use, like I say, you could write really sophisticated 

67
00:03:33,070 --> 00:03:34,950
code in your CATCH block and really it&#39;s

68
00:03:34,950 --> 00:03:38,060
you know entirely in your hands what you want to write. But a couple of

69
00:03:38,060 --> 00:03:41,489
fairly common techniques are to either catch it,

70
00:03:41,489 --> 00:03:46,370
write some code that corrects the issue if you can figure out what the

71
00:03:46,370 --> 00:03:50,280
issue is or at least log what&#39;s happened. And then typically you either

72
00:03:50,280 --> 00:03:52,030
re-throw the original error 

73
00:03:52,030 --> 00:03:55,530
so that a client application can then catch it and do it own error handling.

74
00:03:55,530 --> 00:03:59,350
Or you log the original error

75
00:03:59,350 --> 00:04:02,650
and you throw your own custom error to the client that effectively says look 

76
00:04:02,650 --> 00:04:03,450
an error occurred

77
00:04:03,450 --> 00:04:07,410
but I handled it, I logged it, and go and check the log file to find out details

78
00:04:07,410 --> 00:04:08,530
about what happened.

79
00:04:08,530 --> 00:04:12,910
So you will find this idea of a chain of exceptions. And when people are

80
00:04:12,910 --> 00:04:13,269
 

81
00:04:13,269 --> 00:04:17,390
troubleshooting applications and looking at error logs, we quite often talk about

82
00:04:17,390 --> 00:04:21,350
the exception hierarchy of exceptions within exceptions within

83
00:04:21,350 --> 00:04:22,460
exceptions.

84
00:04:22,460 --> 00:04:25,840
And in this case we&#39;re catching one exception, we can either re-throw the

85
00:04:25,840 --> 00:04:26,980
same thing or we can

86
00:04:26,980 --> 00:04:30,020
catch the error and throw our own error out again.

