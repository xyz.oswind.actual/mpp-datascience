0
00:00:02,780 --> 00:00:05,580
Now handling errors is one thing. And  we&#39;ve seen

1
00:00:05,580 --> 00:00:08,870
how to throw errors and how to catch them and handle them. But

2
00:00:08,870 --> 00:00:11,860
what we have to think about in the context of a database is, how do I

3
00:00:11,860 --> 00:00:14,119
protect the integrity of the data 

4
00:00:14,119 --> 00:00:18,380
in the database? And just handling exceptions doesn&#39;t necessarily mean that

5
00:00:18,380 --> 00:00:18,970
that&#39;s going 

6
00:00:18,970 --> 00:00:23,419
protect that integrity. To do that, to product the integrity of my data, I need to 

7
00:00:23,419 --> 00:00:25,220
think about things in terms of transactions.

8
00:00:25,220 --> 00:00:29,269
So a transaction is a group of tasks

9
00:00:29,269 --> 00:00:34,440
that together form a single unit of work. So up until know we&#39;ve been

10
00:00:34,440 --> 00:00:37,940
you know executing UPDATE statements or INSERT statements kind of one at a

11
00:00:37,940 --> 00:00:40,969
time. We&#39;ve updated one table then we&#39;ve updated another table.

12
00:00:40,969 --> 00:00:44,629
But what if a logical unit of work involves updating

13
00:00:44,629 --> 00:00:47,629
two tables or inserting into two tables? And I

14
00:00:47,629 --> 00:00:50,949
can&#39;t have it partially you know succeed or partially fail.

15
00:00:50,949 --> 00:00:54,460
All of it has to work together. So we

16
00:00:54,460 --> 00:00:57,559
have in the past quite often talked about transactions as being

17
00:00:57,559 --> 00:01:00,710
ACID transactions. If you think the word ACID

18
00:01:00,710 --> 00:01:05,330
as an acrostic for the transaction be atomic. It&#39;s one unit of work that all of the, 

19
00:01:05,330 --> 00:01:10,120
that everything contributes towards. It&#39;s  -- you can&#39;t split it up into separate

20
00:01:10,120 --> 00:01:11,430
things, it&#39;s all one thing.

21
00:01:11,430 --> 00:01:14,830
It&#39;s consistent so when the transaction finishes

22
00:01:14,830 --> 00:01:18,310
the database will be left in a consistent state. There won&#39;t be any

23
00:01:18,310 --> 00:01:20,020
orphaned rows in one table that

24
00:01:20,020 --> 00:01:24,200
have you know missing rows in another table. It&#39;s

25
00:01:24,200 --> 00:01:28,390
isolated. So while that transaction is actually happening,

26
00:01:28,390 --> 00:01:31,930
it doesn&#39;t affect the operations of other ongoing

27
00:01:31,930 --> 00:01:35,220
operations in the database. If other queries are querying tables, they don&#39;t

28
00:01:35,220 --> 00:01:39,740
get to see data that I half inserted or something like that. And it&#39;s durable. Once

29
00:01:39,740 --> 00:01:41,710
the transaction is finished and I&#39;m sure 

30
00:01:41,710 --> 00:01:45,390
that I&#39;ve done all the bits of work that I  need to do, then it&#39;s persisted to the

31
00:01:45,390 --> 00:01:47,090
database permanently. It won&#39;t then 

32
00:01:47,090 --> 00:01:50,930
get deleted unless I explicitly delete it. So if you see 

33
00:01:50,930 --> 00:01:51,360
people doing

34
00:01:51,360 --> 00:01:55,400
ACID transactions that&#39;s what they&#39;re talking about. The point is it&#39;s a single

35
00:01:55,400 --> 00:01:58,750
atomic unit of work where everything must succeed

36
00:01:58,750 --> 00:02:02,660
or all of it must fail. I can&#39;t have a partial success.

37
00:02:02,660 --> 00:02:05,800
So thinking about the example on the slide 

38
00:02:05,800 --> 00:02:08,790
imagine I want to insert an order. So

39
00:02:08,789 --> 00:02:11,960
customer&#39;s on the website, they&#39;ve placed an order and I want to insert that order into the

40
00:02:11,960 --> 00:02:12,520
database

41
00:02:12,520 --> 00:02:16,550
Well the order might consist of an order header which has the date, and the

42
00:02:16,550 --> 00:02:17,930
customer ID, and the

43
00:02:17,930 --> 00:02:22,530
order number and all that type of stuff. But then the order might consist of multiple line items.

44
00:02:22,530 --> 00:02:24,200
They might order a bike,

45
00:02:24,200 --> 00:02:27,310
helmet, and gloves, and a whole bunch different line items.

46
00:02:27,310 --> 00:02:31,560
So I actually have to insert into two tables here. I have to insert in the order header

47
00:02:31,560 --> 00:02:32,280
table

48
00:02:32,280 --> 00:02:37,220
and then I have to insert the corresponding order details into the OrderDetail table.

49
00:02:37,220 --> 00:02:41,850
What I don&#39;t want is a situation where I insert the order header but

50
00:02:41,850 --> 00:02:45,520
then don&#39;t manage to insert all of the order details that go with that. I don&#39;t want to 

51
00:02:45,520 --> 00:02:47,110
lose any of the order details.

52
00:02:47,110 --> 00:02:51,260
If something goes wrong while I&#39;m in the process of doing that, I want to not insert 

53
00:02:51,260 --> 00:02:51,820
any of it.

54
00:02:51,820 --> 00:02:56,250
I want to roll it back. And the other example that&#39;s commonly used is

55
00:02:56,250 --> 00:03:00,650
if you imagine transferring funds from one account to another,

56
00:03:00,650 --> 00:03:06,670
the idealistic banking scenario that, indeed, instantly moves money 

57
00:03:06,670 --> 00:03:10,330
from one account to another. &lt;laughter&gt; Yeah, instantly moving money from one account to another. You don&#39;t want a

58
00:03:10,330 --> 00:03:11,320
situation,

59
00:03:11,320 --> 00:03:14,870
if you&#39;re the customer, you don&#39;t want a situation where the bank

60
00:03:14,870 --> 00:03:18,520
debits the money out of one account and doesn&#39;t credit into the other.

61
00:03:18,520 --> 00:03:22,080
And if you&#39;re the bank, you certainly don&#39;t want a situation where you credit

62
00:03:22,080 --> 00:03:25,430
the money into one account but then don&#39;t debit it out of the other. So

63
00:03:25,430 --> 00:03:29,310
both of those operations have to succeed or both of them have to fail.

64
00:03:29,310 --> 00:03:33,100
So in

65
00:03:33,100 --> 00:03:36,370
SQL Server and Azure SQL Database and Transact-SQL generally

66
00:03:36,370 --> 00:03:41,160
if you do an INSERT statement or and UPDATE statement or a DELETE statement as we&#39;ve seen

67
00:03:41,160 --> 00:03:42,700
in previous modules,

68
00:03:42,700 --> 00:03:46,530
it might affect multiple rows just that one statement.

69
00:03:46,530 --> 00:03:49,530
And you might be wondering at this point well how do I make sure that it

70
00:03:49,530 --> 00:03:52,390
doesn&#39;t just affect some of the rows and not all of the rows? How do I make that a

71
00:03:52,390 --> 00:03:53,220
transaction?

72
00:03:53,220 --> 00:03:56,730
Well the answer is you don&#39;t have to because any individual

73
00:03:56,730 --> 00:04:00,260
Transact-SQL statement that gets executed, SQL Server itself

74
00:04:00,260 --> 00:04:04,230
treats that as a transaction. So if it falls over half way through the rows,

75
00:04:04,230 --> 00:04:09,340
it will never never just do half the rows. Well, even if the server stopped half way through the rows,

76
00:04:09,340 --> 00:04:11,680
what will happen when it restarts? Presumably then it&#39;s done

77
00:04:11,680 --> 00:04:15,820
half of the rows. No, if the server stops --  if you do an INSERT statement that inserts 

78
00:04:15,820 --> 00:04:17,289
let&#39;s say 10,000 rows.

79
00:04:17,289 --> 00:04:20,959
5,000 of them have been inserted and somebody pulls the plug out and the SQL Server 

80
00:04:20,959 --> 00:04:25,030
itself goes down. When it comes back up, the way that SQL Server works 

81
00:04:25,030 --> 00:04:28,950
is it has a write-ahead transaction log. It logs all the things it&#39;s going to do.

82
00:04:28,950 --> 00:04:33,200
And only when it&#39;s finished does it write that it&#39;s finished. It writes that it&#39;s 

83
00:04:33,200 --> 00:04:35,300
committed that transaction and  that&#39;s been

84
00:04:35,300 --> 00:04:39,350
what they call check-pointed. What it will do is when it comes back up again it will

85
00:04:39,350 --> 00:04:40,470
read the log,

86
00:04:40,470 --> 00:04:43,960
and if it finds something where it started to do something

87
00:04:43,960 --> 00:04:48,830
but then never finished it, it will roll it back. So it will make sure that we finish with

88
00:04:48,830 --> 00:04:50,130
a consistent database.

89
00:04:50,130 --> 00:04:53,340
Where either all of the transactions succeeded or

90
00:04:53,340 --> 00:04:57,310
they failed. And in the case of an individual statement, there&#39;s nothing

91
00:04:57,310 --> 00:04:59,320
special you need to do in your Transact-SQL code.

92
00:04:59,320 --> 00:05:03,650
An individual INSERT statement or an UPDATE statement or a DELECT statement is in 

93
00:05:03,650 --> 00:05:05,060
itself a transaction

94
00:05:05,060 --> 00:05:09,080
and SQL Server will look after the consistency of that. However

95
00:05:09,080 --> 00:05:13,600
if you&#39;ve got multiple statements that you want to execute,

96
00:05:13,600 --> 00:05:16,860
then you have to explicitly make that a transaction and we&#39;ll look at that

97
00:05:16,860 --> 00:05:20,250
in a few seconds. So we&#39;ve got this notion of

98
00:05:20,250 --> 00:05:24,240
SQL Server using the transaction log to support transactions. The other thing it uses

99
00:05:24,240 --> 00:05:26,440
we talked about transactions being isolated

100
00:05:26,440 --> 00:05:29,930
so you don&#39;t impact other users at the same time.

101
00:05:29,930 --> 00:05:34,120
And SQL Server uses a mechanism of locking. It locks the data pages

102
00:05:34,120 --> 00:05:38,520
so that other operations, other concurrent sessions, can&#39;t read the data or

103
00:05:38,520 --> 00:05:41,820
write to the data while your transaction is in the middle of modifying it.

104
00:05:41,820 --> 00:05:45,590
And that are, it&#39;s a very complex topic. There are lots of different ways that you can

105
00:05:45,590 --> 00:05:46,910
handle locking and handle

106
00:05:46,910 --> 00:05:50,950
different levels of isolation. And if you are interested in that,

107
00:05:50,950 --> 00:05:54,600
we have other courses available that you can come on and learn how to

108
00:05:54,600 --> 00:05:57,650
manage those different isolation levels and handle the locking. 

109
00:05:57,650 --> 00:06:01,110
For now, all you really need to be aware of is that SQL Server is using

110
00:06:01,110 --> 00:06:06,200
those techniques to ensure that each individual statement is a transaction

111
00:06:06,200 --> 00:06:09,789
and if you create your own explicit transactions, then it will also use the

112
00:06:09,789 --> 00:06:11,410
same techniques to manage those.

113
00:06:11,410 --> 00:06:16,410
So speaking of explicit transactions then,

114
00:06:16,410 --> 00:06:19,610
how do we implement them? Let&#39;s have a look at that.

115
00:06:19,610 --> 00:06:23,240
So the way that we do this is there&#39;s

116
00:06:23,240 --> 00:06:27,090
a number statements in Transact-SQL. The first one is the BEGIN 

117
00:06:27,090 --> 00:06:28,140
TRANSACTION

118
00:06:28,140 --> 00:06:31,500
statement. You can say BEGIN TRAN as well, like a lot of things there&#39;s

119
00:06:31,500 --> 00:06:35,400
optional ways to do it. But BEGIN TRANSACTION is the full syntax.

120
00:06:35,400 --> 00:06:38,419
And if I do a BEGIN TRANSACTION

121
00:06:38,419 --> 00:06:42,599
then everything from that point on until I either commit the transaction

122
00:06:42,599 --> 00:06:47,160
or the transaction fails and is rolled back is considered atomic. It&#39;s considered a

123
00:06:47,160 --> 00:06:47,940
transaction.

124
00:06:47,940 --> 00:06:52,340
So I&#39;ve got in my code here I&#39;ve got BEGIN TRANSACTION

125
00:06:52,340 --> 00:06:56,380
and then I&#39;m going to INSERT into the Sales.Order table and insert into the Sale.OrderDetail

126
00:06:56,380 --> 00:06:57,780
table. That&#39;s

127
00:06:57,780 --> 00:07:01,319
the atomic unit of work I want to complete. Both of those things

128
00:07:01,319 --> 00:07:05,900
have to complete as one transaction. And you&#39;ve put that inside a TRY block so you

129
00:07:05,900 --> 00:07:07,110
got, you&#39;re combining --

130
00:07:07,110 --> 00:07:10,669
Yeah, I&#39;m still -- this doesn&#39;t prevent me from having to handle errors, I

131
00:07:10,669 --> 00:07:11,840
still want to handle errors. 

132
00:07:11,840 --> 00:07:15,300
It&#39;s just that I&#39;m encapsulating those statements to be a single

133
00:07:15,300 --> 00:07:20,159
logical unit of work even though there&#39;s multiple statements. So

134
00:07:20,159 --> 00:07:23,639
once I&#39;ve done everything I need to do within that TRY block I&#39;ve managed to do

135
00:07:23,639 --> 00:07:24,870
the INSERT into order, 

136
00:07:24,870 --> 00:07:29,300
I&#39;ve done the INSERT to order detail. Nothing&#39;s gone wrong, the Sun is in the sky, Happy Days,

137
00:07:29,300 --> 00:07:30,110
everything&#39;s fine!

138
00:07:30,110 --> 00:07:33,990
I can commit the transaction at that point and SQL Server will

139
00:07:33,990 --> 00:07:37,900
at that point in the log say this  transaction is committed. So it knows that 

140
00:07:37,900 --> 00:07:41,139
that complete atomic unit of work has been performed successfully.

141
00:07:41,139 --> 00:07:45,590
If however while I&#39;m in the process of doing the INSERT

142
00:07:45,590 --> 00:07:48,699
an error occurs, at that point

143
00:07:48,699 --> 00:07:51,930
my error handler will catch the, error

144
00:07:51,930 --> 00:07:55,340
and I can in that error handler start to

145
00:07:55,340 --> 00:07:58,750
look at what&#39;s going on with the transaction. So in this case you can

146
00:07:58,750 --> 00:07:59,190
see 

147
00:07:59,190 --> 00:08:03,389
I&#39;ve checked the TRANCOUNT. And that&#39;s odd because that implies that I

148
00:08:03,389 --> 00:08:05,370
could have multiple transactions going on,

149
00:08:05,370 --> 00:08:09,830
which in fact is true. So are these other transactions from other people?

150
00:08:09,830 --> 00:08:13,340
No these would typically be transactions where I&#39;ve done a BEGIN 

151
00:08:13,340 --> 00:08:16,349
TRANSACTION and then within that I&#39;ve begun another transaction.

152
00:08:16,349 --> 00:08:19,770
So I could have a transaction that&#39;s inside a transaction that&#39;s inside a transaction.

153
00:08:19,770 --> 00:08:20,840
They could be nested.

154
00:08:20,840 --> 00:08:24,880
So if you had to -- say you called a stored procedures and in itself it had a transaction

155
00:08:24,880 --> 00:08:26,000
within it, then you

156
00:08:26,000 --> 00:08:30,460
have a nested transaction within there. Exactly, yeah. Now what&#39;s interesting here is I&#39;m

157
00:08:30,460 --> 00:08:33,300
handling, I&#39;m dealing with the issue by doing a ROLLBACK

158
00:08:33,299 --> 00:08:37,060
and if I roll back, I don&#39;t just roll back this transaction, I roll back all the

159
00:08:37,059 --> 00:08:38,510
transactions all the way up the calling stack.

160
00:08:38,510 --> 00:08:42,659
So whatever happens, you can&#39;t just roll back the bit 

161
00:08:42,659 --> 00:08:43,610
that&#39;s 

162
00:08:43,610 --> 00:08:46,959
in there. It&#39;s basically going to roll back all of the calling ones as well. So you&#39;re going all the 

163
00:08:46,959 --> 00:08:50,079
way back to the beginning. Yeah because I want the whole thing to be consistent, I

164
00:08:50,079 --> 00:08:51,630
want it to be atomic and consistent.

165
00:08:51,630 --> 00:08:57,010
So I&#39;ve got that ROLLBACK statement in there and you&#39;ll notice that I&#39;ve checked the

166
00:08:57,010 --> 00:08:59,980
TRANCOUNT to see how many transactions are going. There are few other

167
00:08:59,980 --> 00:09:03,560
ways of interrogating transactions that we&#39;ll talk about a second. The other thing

168
00:09:03,560 --> 00:09:04,240
is

169
00:09:04,240 --> 00:09:08,760
I&#39;ve explicitly rolled back the transaction here. So I can write the logic that checks

170
00:09:08,760 --> 00:09:09,430
to see

171
00:09:09,430 --> 00:09:12,519
you know do I actually want to roll back the transaction. So all is not lost at this

172
00:09:12,519 --> 00:09:14,690
point. If I get to this point and what&#39;s happened is

173
00:09:14,690 --> 00:09:18,279
I&#39;ve done the successful INSERT into the Sales.Order and I&#39;ve done 

174
00:09:18,279 --> 00:09:21,829
the successful INSERT into the Sales.OrderDetail and if perhaps there&#39;s been

175
00:09:21,829 --> 00:09:24,110
another bit of code that&#39;s triggered an error,

176
00:09:24,110 --> 00:09:27,640
but that hasn&#39;t really affected the transaction,

177
00:09:27,640 --> 00:09:31,230
I can check down here on the state of the transaction and choose whether to

178
00:09:31,230 --> 00:09:33,070
call ROLLBACK TRANSACTION or not.

179
00:09:33,070 --> 00:09:37,329
One of the alternatives techniques I can have is I can set this option called

180
00:09:37,329 --> 00:09:38,630
XACT_ABORT.

181
00:09:38,630 --> 00:09:42,560
And if I set that ON then as soon as an error occurs,

182
00:09:42,560 --> 00:09:45,640
all transactions are automatically rolled back. So

183
00:09:45,640 --> 00:09:50,040
any error? Indeed. You can specify an error? Or just anything that&#39;s errored? No, 

184
00:09:50,040 --> 00:09:52,949
in that case any error occurs then everything is rolled back. So

185
00:09:52,949 --> 00:09:56,380
in some ways it makes life easier because I don&#39;t have to do all the code that 

186
00:09:56,380 --> 00:09:59,329
checks and I don&#39;t have to explicitly call ROLLBACK TRANSACTION.

187
00:09:59,329 --> 00:10:02,709
But in other ways it&#39;s kind of an all-or-nothing deal, if anything goes

188
00:10:02,709 --> 00:10:03,860
wrong anywhere at all

189
00:10:03,860 --> 00:10:07,320
regardless the whether it affected the integrity of the data or not, I&#39;m going to 

190
00:10:07,320 --> 00:10:08,279
roll back the transaction. 

191
00:10:08,279 --> 00:10:11,820
Yep. So the other 

192
00:10:11,820 --> 00:10:14,269
thing I talked about there was we looked at TRANCOUNT, we checked the number of

193
00:10:14,269 --> 00:10:15,260
transactions going on.

194
00:10:15,260 --> 00:10:19,510
There is another system function I can use called XACT_STATE

195
00:10:19,510 --> 00:10:22,329
that lets me go and check the transactional state.

