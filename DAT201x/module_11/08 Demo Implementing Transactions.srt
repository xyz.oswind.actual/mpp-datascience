0
00:00:04,149 --> 00:00:05,050
Let&#39;s have a look

1
00:00:05,050 --> 00:00:09,040
at handling transactions and getting those transactions to make sure

2
00:00:09,040 --> 00:00:09,410
my

3
00:00:09,410 --> 00:00:12,720
work succeeds or fails as a single unit.

4
00:00:12,720 --> 00:00:16,830
So I&#39;m going to first of all, I&#39;m not using a transaction in this case, I&#39;m going to go

5
00:00:16,830 --> 00:00:19,710
and insert into the SalesOrderHeader table

6
00:00:19,710 --> 00:00:24,840
an order. And I&#39;m then going to get the identity of the order that&#39;s been inserted

7
00:00:24,840 --> 00:00:29,590
and I&#39;m going to insert some order details to go along with the order here.

8
00:00:29,590 --> 00:00:33,539
And if something goes wrong, I&#39;m just simply going to catch the error message

9
00:00:33,539 --> 00:00:35,159
in print the error message. So

10
00:00:35,159 --> 00:00:42,159
let&#39;s go ahead and try to do that. And now my error handler has

11
00:00:44,479 --> 00:00:47,510
caught an error. So there isn&#39;t an outstanding error in the system because I

12
00:00:47,510 --> 00:00:49,069
handled it. And what it&#39;s saying is

13
00:00:49,069 --> 00:00:52,379
well I got an INSERT statement conflicting because of a foreign key

14
00:00:52,379 --> 00:00:54,129
constraint. We&#39;ve seen this before.

15
00:00:54,129 --> 00:00:58,489
And what actually has happened here is I&#39;ve tried to insert an order detail

16
00:00:58,489 --> 00:01:02,690
for a product that doesn&#39;t exist. So I get an error there because I&#39;ve tried 

17
00:01:02,690 --> 00:01:02,969
to

18
00:01:02,969 --> 00:01:07,890
insert for a non-existent product. But you&#39;ve also inserted a SalesOrderHeader.

19
00:01:07,890 --> 00:01:11,220
Indeed. I&#39;ve managed to insert my sales order header

20
00:01:11,220 --> 00:01:14,290
and I can see the 1 row affected there from that.

21
00:01:14,290 --> 00:01:17,470
But I&#39;ve got a 0 rows affected from the  second statement

22
00:01:17,470 --> 00:01:20,730
and it triggered an error which I caught and displayed the message for.

23
00:01:20,730 --> 00:01:24,710
So we have an order with nothing in it. Yeah. So if I go and have a look, I can 

24
00:01:24,710 --> 00:01:28,080
actually do a query here that says find me all of the

25
00:01:28,080 --> 00:01:32,670
sales order headers that have no corresponding order details.

26
00:01:32,670 --> 00:01:37,330
So I&#39;m using a LEFT OUTER JOIN to do that and we talked about LEFT OUTER JOINs earlier on in the

27
00:01:37,330 --> 00:01:38,570
course, it&#39;s a good job we did!

28
00:01:38,570 --> 00:01:41,800
So let&#39;s go ahead and look at that. And I can actually see well here&#39;s a

29
00:01:41,800 --> 00:01:43,090
sales order header,

30
00:01:43,090 --> 00:01:47,760
that&#39;s the one I inserted, but it doesn&#39;t have any order details. So my data is 

31
00:01:47,760 --> 00:01:48,800
now inconsistent, there&#39;s

32
00:01:48,800 --> 00:01:51,980
a problem in my database. So

33
00:01:51,980 --> 00:01:54,980
I guess the first thing we&#39;ll do is we&#39;ll go ahead and fix that. So the

34
00:01:54,980 --> 00:01:56,800
scope identity will still be valid because I haven&#39;t 

35
00:01:56,800 --> 00:01:59,920
inserted anything else. So I&#39;ll go ahead and get rid of that guy.

36
00:01:59,920 --> 00:02:06,180
That&#39;s deleted that one row. I&#39;ll just  confirm, my data is now consistent. I don&#39;t have

37
00:02:06,180 --> 00:02:07,740
any order headers that don&#39;t have any

38
00:02:07,740 --> 00:02:11,659
matching order details. And I&#39;ll try that again but this time I&#39;m going to use a

39
00:02:11,659 --> 00:02:12,560
transaction.

40
00:02:12,560 --> 00:02:14,150
So I&#39;ll just

41
00:02:14,150 --> 00:02:16,970
hide that result for the moment so we can see the code.

42
00:02:16,970 --> 00:02:21,420
Still got the BEGIN TRY and the END TRY because I still want to catch any errors that occur,

43
00:02:21,420 --> 00:02:25,800
but this time I&#39;ve got a BEGIN TRANSATION and a COMMIT TRANSACTION 

44
00:02:25,800 --> 00:02:29,190
around the in this case three statements that

45
00:02:29,190 --> 00:02:33,290
are effectively the atomic unit of work. So I want to insert into the SalesOrderHeader,

46
00:02:33,290 --> 00:02:33,930
 

47
00:02:33,930 --> 00:02:37,310
I want to get the identity that was inserted, and then I want to insert into the

48
00:02:37,310 --> 00:02:38,440
SalesOrderDetail.

49
00:02:38,440 --> 00:02:41,780
And I want all of that to succeed or I want all of it to fail.

50
00:02:41,780 --> 00:02:45,420
If that succeeds, great. If not an error

51
00:02:45,420 --> 00:02:49,460
occurs, then I&#39;m going to check the transaction count and if there is

52
00:02:49,460 --> 00:02:53,640
a transaction in process, I&#39;m going to print the transaction state. That&#39;s just

53
00:02:53,640 --> 00:02:57,110
simply so I can see what sort of state  the transaction was in.

54
00:02:57,110 --> 00:03:00,340
But regardless of that, I&#39;m then going to roll back the transaction.

55
00:03:00,340 --> 00:03:03,460
I don&#39;t want -- because this error occurred, I don&#39;t want to 

56
00:03:03,460 --> 00:03:07,020
make this happen. And then I&#39;ll print the error message and throw custom error  for

57
00:03:07,020 --> 00:03:07,770
good measure

58
00:03:07,770 --> 00:03:11,070
just to indicate that the transaction was cancelled. Which is typically what

59
00:03:11,070 --> 00:03:12,570
you would do. You would log the error

60
00:03:12,570 --> 00:03:15,910
and then throw an error message to the client to say you tried to commit

61
00:03:15,910 --> 00:03:17,240
transaction and it failed.

62
00:03:17,240 --> 00:03:21,040
So let&#39;s go ahead and run that

63
00:03:21,040 --> 00:03:26,800
code. We&#39;ll just execute that.

64
00:03:26,800 --> 00:03:32,660
And sure enough, I got my 1 row affected, so it did do the insert of the sales order header.

65
00:03:32,660 --> 00:03:36,640
It&#39;s still saying there&#39;s 1 row affected there then. Still say&#39;s there&#39;s 1 row affected.

66
00:03:36,640 --> 00:03:39,790
Then it says there&#39;s 0 rows affected. So it has in fact -- I mean it does these operations, it&#39;s

67
00:03:39,790 --> 00:03:43,830
done them. But then when the error occurred, I get my error thrown.

68
00:03:43,830 --> 00:03:47,060
I can see that the transaction state here was 1.

69
00:03:47,060 --> 00:03:51,610
So I could add some more complex logic in here to do something based on

70
00:03:51,610 --> 00:03:53,060
that, in this case I haven&#39;t bothered.

71
00:03:53,060 --> 00:03:57,680
But what I can now do is I&#39;ll go and check for my orphaned order. And even though it said

72
00:03:57,680 --> 00:03:59,170
there was 1 row affected,

73
00:03:59,170 --> 00:04:03,710
I don&#39;t have any order headers without matching order details because what happened

74
00:04:03,710 --> 00:04:04,220
was

75
00:04:04,220 --> 00:04:08,000
it did succeed in that statement, 1 row was affected,

76
00:04:08,000 --> 00:04:11,440
but then it rolled back that transaction, it undid it if you like

77
00:04:11,440 --> 00:04:15,070
is the point because it couldn&#39;t partially finish the whole transaction.

78
00:04:15,070 --> 00:04:19,410
So if I had a scenario where I&#39;ve got a large number records, I&#39;ve got ten thousand

79
00:04:19,410 --> 00:04:20,980
records that I&#39;m updating,

80
00:04:20,980 --> 00:04:24,800
and I put it in a transaction, I want it all to happen as a unit of work.

81
00:04:24,800 --> 00:04:26,670
They&#39;re not one select statement 

82
00:04:26,670 --> 00:04:29,670
let&#39;s say, so it&#39;s multiple things. Runs through and does it. 

83
00:04:29,670 --> 00:04:33,230
And then at the end of it, we do a test on what

84
00:04:33,230 --> 00:04:37,050
the values that we&#39;ve updated are. And we decide actually no, they&#39;re not valid values.

85
00:04:37,050 --> 00:04:40,140
It will have done all changes, then it will

86
00:04:40,140 --> 00:04:43,580
have undone all the changes. So inside of a transaction

87
00:04:43,580 --> 00:04:46,890
maybe if we put a test at the beginning

88
00:04:46,890 --> 00:04:51,010
to say oh what are the values that you&#39;re passing us for this? And you say actually those 

89
00:04:51,010 --> 00:04:55,350
aren&#39;t values we want. You could avoid because actually what&#39;s going to happen it would 

90
00:04:55,350 --> 00:04:59,560
do ten thousand updates and then it will do because of what&#39;s happened ten thousand 

91
00:04:59,560 --> 00:05:01,670
rollbacks. It actually does do it,

92
00:05:01,670 --> 00:05:04,940
yes, and then does undo it. So if we could test

93
00:05:04,940 --> 00:05:08,500
some logic somewhere. It would be a good idea if we could test it at the beginning so it didn&#39;t

94
00:05:08,500 --> 00:05:09,290
do it at all.

95
00:05:09,290 --> 00:05:12,680
It&#39;s not that it didn&#39;t do it, because it actually did do it there, and did undo it.

96
00:05:12,680 --> 00:05:17,460
Yes so if you&#39;re I mean if you&#39;re implementing your code like this

97
00:05:17,460 --> 00:05:20,910
if you can do any sort of logical checks before you actually do the updates, you

98
00:05:20,910 --> 00:05:22,410
would do those logical test before you

99
00:05:22,410 --> 00:05:25,270
do the updates to save yourself some work. The point of the

100
00:05:25,270 --> 00:05:26,390
transaction though

101
00:05:26,390 --> 00:05:30,260
is that I&#39;ve tried to do some work that involves multiple statements

102
00:05:30,260 --> 00:05:33,580
and even if the first and second ones succeed,

103
00:05:33,580 --> 00:05:36,940
then if any of the other subsequent ones fail, I can roll back the whole thing and

104
00:05:36,940 --> 00:05:38,010
keep the database

105
00:05:38,010 --> 00:05:41,080
consistent. And because it&#39;s isolated remember

106
00:05:41,080 --> 00:05:44,770
that in ACID the I is isolated, it won&#39;t have affected any of the other users -- they won&#39;t have

107
00:05:44,770 --> 00:05:45,450
seen,

108
00:05:45,450 --> 00:05:50,120
well depending on the isolation level I use, they may not have seen

109
00:05:50,120 --> 00:05:50,520
that

110
00:05:50,520 --> 00:05:54,940
orphaned order header. They typically won&#39;t see the data

111
00:05:54,940 --> 00:05:55,430
that you&#39;re

112
00:05:55,430 --> 00:06:00,220
inserting. So there is another option. We talked about this issue of using

113
00:06:00,220 --> 00:06:04,750
XACT_ABORT. So I&#39;m just going to use that just to show how that works. We&#39;re going to

114
00:06:04,750 --> 00:06:05,730
turn it on.

115
00:06:05,730 --> 00:06:08,820
And then we&#39;re going to do our transaction the same as we did before,

116
00:06:08,820 --> 00:06:10,040
exactly the same.

117
00:06:10,040 --> 00:06:13,660
This time in the CATCH block I&#39;m going to print the error message and throw an 

118
00:06:13,660 --> 00:06:17,250
error, but I haven&#39;t got a ROLLBACK statement. I haven&#39;t done anything to roll the

119
00:06:17,250 --> 00:06:18,050
transaction back

120
00:06:18,050 --> 00:06:22,190
explicitly. So let&#39;s go ahead and run that.

121
00:06:22,190 --> 00:06:27,290
And again I&#39;ll just bring this up so we can see the results of that.

122
00:06:27,290 --> 00:06:31,850
Exactly the same as before, 1 row affected for the first statement, 0 is affected for

123
00:06:31,850 --> 00:06:33,870
the order details table,

124
00:06:33,870 --> 00:06:37,710
I&#39;ve got my error message that occurred being printed by my error handler,

125
00:06:37,710 --> 00:06:41,320
and then my error handler threw a custom error which came back 

126
00:06:41,320 --> 00:06:44,510
to the client application. So it looks at this point, because I didn&#39;t have a

127
00:06:44,510 --> 00:06:47,530
ROLLBACK statement in there, it looks like well this might well leave me with

128
00:06:47,530 --> 00:06:48,590
inconsistent data.

129
00:06:48,590 --> 00:06:51,880
But because I had XACT_ABORT ON

130
00:06:51,880 --> 00:06:56,560
any error condition will cause any transactions to be rolled back.

131
00:06:56,560 --> 00:07:01,660
So even if that error had occurred after all the inserts had failed and that for some

132
00:07:01,660 --> 00:07:03,500
reason there was some other error message 

133
00:07:03,500 --> 00:07:06,780
that came up in the system, then it would have been rolled back.

134
00:07:06,780 --> 00:07:10,760
So it&#39;s like I said when we were looking the slides, it&#39;s an all or

135
00:07:10,760 --> 00:07:11,500
nothing

136
00:07:11,500 --> 00:07:15,550
solution. But it does mean I didn&#39;t have to explicitly write a ROLLBACK because

137
00:07:15,550 --> 00:07:16,510
an error occurred

138
00:07:16,510 --> 00:07:20,280
the system just automatically rolled back the transaction. But you would tend towards

139
00:07:20,280 --> 00:07:23,470
doing it explicitly so we can test what the error was and make sure.

140
00:07:23,470 --> 00:07:27,480
I think you get more control being able to do it explicitly.

141
00:07:27,480 --> 00:07:32,050
So if I want to be more sophisticated about how I handle the

142
00:07:32,050 --> 00:07:33,360
error and deal with it, then

143
00:07:33,360 --> 00:07:38,260
I would use an explicit ROLLBACK. If it&#39;s a relatively simple example like this, where

144
00:07:38,260 --> 00:07:41,110
the only thing that code is doing is inserting a header and inserting a

145
00:07:41,110 --> 00:07:41,780
detail,

146
00:07:41,780 --> 00:07:44,860
if anything goes wrong, well there&#39;s a pretty good chance I want to roll back the 

147
00:07:44,860 --> 00:07:45,500
transaction. So

148
00:07:45,500 --> 00:07:48,880
I might want to turn on XACT_ABORT that way and just keep it easy.

