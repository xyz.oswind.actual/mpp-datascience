0
00:00:02,340 --> 00:00:05,029
I do want to talk about raising errors or  throwing errors as the

1
00:00:05,029 --> 00:00:06,290
terminology might have it. 

2
00:00:06,290 --> 00:00:10,490
And let&#39;s start with the RAISERROR and this is the command that&#39;s

3
00:00:10,490 --> 00:00:13,059
been in SQL Server many many versions 

4
00:00:13,059 --> 00:00:17,300
and it&#39;s how you raise an error that&#39;s in the sys.messages table

5
00:00:17,300 --> 00:00:20,790
or just an ad hoc explicit error. So

6
00:00:20,790 --> 00:00:24,520
in SQL Server like I said, you can use this to raise an error and then just give

7
00:00:24,520 --> 00:00:26,689
it the ID of the error, the number,

8
00:00:26,689 --> 00:00:31,000
and it will go and look up that error in the sys.messages table and raise

9
00:00:31,000 --> 00:00:31,520
that error.

10
00:00:31,520 --> 00:00:36,550
That technique only works in SQL Server. You can&#39;t use that in Azure SQL database

11
00:00:36,550 --> 00:00:41,219
because as we said before you don&#39;t have the sys.messages table. What you can

12
00:00:41,219 --> 00:00:44,280
also do though is you can use RAISERROR and this is true in both SQL 

13
00:00:44,280 --> 00:00:44,960
Server

14
00:00:44,960 --> 00:00:49,449
and Azure SQL Database, you can use RAISERROR to raise a customer error at

15
00:00:49,449 --> 00:00:53,280
this point. You&#39;re going to define it as part of the RAISERROR statement. And in the

16
00:00:53,280 --> 00:00:55,129
example on the slide, you can see

17
00:00:55,129 --> 00:00:59,679
RAISERROR, we then just specify what&#39;s the error message we want to raise, &quot;An error

18
00:00:59,679 --> 00:01:00,370
occurred&quot;,

19
00:01:00,370 --> 00:01:03,569
it will be assigned the value I believe it&#39;s

20
00:01:03,569 --> 00:01:07,110
50001 is the error message. You know not to use anything 50000 and below.

21
00:01:07,110 --> 00:01:08,439
You can&#39;t use anything --

22
00:01:08,439 --> 00:01:12,000
that&#39;s right yeah. So you can&#39;t specify -- when you create your own custom

23
00:01:12,000 --> 00:01:16,390
error numbers, they can&#39;t be less than 50,000. And I believe this is, 

24
00:01:16,390 --> 00:01:19,960
it may be 50000 or 50001. We&#39;ll actually see in the demo.

25
00:01:19,960 --> 00:01:23,000
But it will raise the error with that error message, and

26
00:01:23,000 --> 00:01:26,110
I can specify the severity, and I can specify the state

27
00:01:26,110 --> 00:01:30,829
when I do that. So in this case I&#39;ve set a  the severity of 16 and state of 0.

28
00:01:30,829 --> 00:01:35,079
Well that, as I&#39;ve said that syntax has been around for a while

29
00:01:35,079 --> 00:01:40,250
and a few versions ago some new syntax was introduced as a slightly

30
00:01:40,250 --> 00:01:43,399
better, more consistent with other applications way of

31
00:01:43,399 --> 00:01:46,509
handling errors, and that&#39;s the THROW command.

32
00:01:46,509 --> 00:01:50,119
When you&#39;re programming in other environments than SQL Server, 

33
00:01:50,119 --> 00:01:54,250
we typically talk about throwing errors rather than raising errors. So

34
00:01:54,250 --> 00:01:57,700
we have this idea of the THROW command. You can think of this as

35
00:01:57,700 --> 00:01:59,570
the replacement for RAISERROR.

36
00:01:59,570 --> 00:02:03,909
RAISERROR is still there and it&#39;s still available in SQL Server and

37
00:02:03,909 --> 00:02:08,080
Azure SQL Database, but going forward you&#39;re probably better to use the THROW

38
00:02:08,080 --> 00:02:10,750
syntax to throw custom errors.

39
00:02:10,750 --> 00:02:15,120
And it lets me throw an explicit error number, I can specify the number

40
00:02:15,120 --> 00:02:16,280
of error I want to throw,

41
00:02:16,280 --> 00:02:20,580
a message, and a state. And again we  mentioned about the error number, it

42
00:02:20,580 --> 00:02:25,410
has to be above 50000. I can specify any

43
00:02:25,410 --> 00:02:28,940
message and I can specify a state that I want to use for

44
00:02:28,940 --> 00:02:32,050
troubleshooting later on. Notice that I can&#39;t specify the severity.

45
00:02:32,050 --> 00:02:35,430
The severity is always 16 if I throw a user-defined error. 

46
00:02:35,430 --> 00:02:39,170
So we can&#39;t throw a really high level message in your code.

47
00:02:39,170 --> 00:02:42,850
You can&#39;t just in your code suddenly define a really high level

48
00:02:42,850 --> 00:02:45,970
that&#39;s going to bring the system down and that&#39;s why. It&#39;s so that

49
00:02:45,970 --> 00:02:49,740
SQL Server itself will handle those critical system level

50
00:02:49,740 --> 00:02:53,180
severity levels and deal with them in its own way.

51
00:02:53,180 --> 00:02:57,090
But we don&#39;t want you confusing things by throwing you know critical errors 

52
00:02:57,090 --> 00:02:58,690
that are going to bring the entire system down.

53
00:02:58,690 --> 00:03:03,820
So the severity is always 16. And the State, you can you said you could investigate

54
00:03:03,820 --> 00:03:07,130
that afterwards. So I could just use that for my own purposes. So if I&#39;m 

55
00:03:07,130 --> 00:03:08,280
throwing a 1,

56
00:03:08,280 --> 00:03:11,340
I mean one thing, if I&#39;m throwing a 2, it actually -- 

57
00:03:11,340 --> 00:03:14,470
so when I invest afterwards and I can see what they are, if that&#39;s 2

58
00:03:14,470 --> 00:03:18,430
I know what happened is you had a problem with this

59
00:03:18,430 --> 00:03:21,500
particular stored procedure whatever. I know some more information

60
00:03:21,500 --> 00:03:25,410
because of that value being passed. Yeah, it&#39;s a way for me to add some sort of diagnostic

61
00:03:25,410 --> 00:03:29,390
indicator that means something to me as the developer but without it popping up

62
00:03:29,390 --> 00:03:34,570
obviously to the person or the client saying with a different message. Yes, exactly, yes.

63
00:03:34,570 --> 00:03:40,020
So one of the things that we can do, what we can do is throw a

64
00:03:40,020 --> 00:03:44,390
custom error number above 50000 and a custom message and state.

65
00:03:44,390 --> 00:03:48,570
The other thing I can do is if an error occurs that I&#39;m not expecting,

66
00:03:48,570 --> 00:03:52,580
and we&#39;ll see in a minute how to catch that error, and what I can then do is just

67
00:03:52,580 --> 00:03:54,780
simply re-throw the error that happened.

68
00:03:54,780 --> 00:03:58,459
And you might think, well why would I do that? What&#39;s the point in that? Well I 

69
00:03:58,459 --> 00:04:00,520
might CATCH an error in order to log it

70
00:04:00,520 --> 00:04:04,320
and do any cleanup that I think is necessary, but then I still want to throw 

71
00:04:04,320 --> 00:04:06,209
the error so that the client application

72
00:04:06,209 --> 00:04:09,790
handles the error. So I&#39;m effectively I&#39;m doing what database level cleanup I can

73
00:04:09,790 --> 00:04:10,310
do,

74
00:04:10,310 --> 00:04:13,760
but then throwing the error back so that the next layer

75
00:04:13,760 --> 00:04:17,540
out, the client that made the call to the database can actually catch the error and do

76
00:04:17,540 --> 00:04:18,730
whatever error handling it

77
00:04:18,730 --> 00:04:21,250
has to do as well. So that CATCH is essentially intercepting.

78
00:04:22,250 --> 00:04:25,830
Yeah I&#39;m intercepting the error, I might handle the error completely and deal

79
00:04:25,830 --> 00:04:26,920
with it or might just

80
00:04:26,920 --> 00:04:30,270
do whatever cleanup I can do and then throw the same error. Because they&#39;re not going 

81
00:04:30,270 --> 00:04:31,690
to see the original one because it would have been caught. 

82
00:04:32,690 --> 00:04:33,300
Yeah, exactly yeah.

