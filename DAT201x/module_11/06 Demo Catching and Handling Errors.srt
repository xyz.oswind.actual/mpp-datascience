0
00:00:04,560 --> 00:00:05,300
So let&#39;s look

1
00:00:05,300 --> 00:00:09,160
at a demo where we&#39;re going to deal with catching errors 

2
00:00:09,160 --> 00:00:13,040
and handling them. And I&#39;m going to start with this fairly simple

3
00:00:13,040 --> 00:00:17,720
example here. You can see I&#39;ve got the BEGIN TRY block, so there&#39;s the TRY block,

4
00:00:17,720 --> 00:00:20,749
BEGIN TRY and END TRY. And what I&#39;m going to do is I&#39;m going to

5
00:00:20,749 --> 00:00:24,009
update the Product table. I&#39;m going to set the ProductNumber

6
00:00:24,009 --> 00:00:27,269
to the ProductID divided by

7
00:00:27,269 --> 00:00:30,919
the Weight. And if the weight is null, I&#39;m going to use 0 for the value of the weight.

8
00:00:30,919 --> 00:00:34,989
And that might immediately trigger some alarms. Yeah, you seem to be dividing by 0 there. I seem 

9
00:00:34,989 --> 00:00:35,550
to be

10
00:00:35,550 --> 00:00:39,239
infinity. If I&#39;ve got any null weights in there, what I will end up doing is trying

11
00:00:39,239 --> 00:00:41,519
to divide by zero which actually you can&#39;t do.

12
00:00:41,519 --> 00:00:46,320
So in the event that something goes wrong, then my CATCH block will execute.

13
00:00:46,320 --> 00:00:49,850
And all I&#39;m going to do in my CATCH block in this case is simply print

14
00:00:49,850 --> 00:00:53,640
that an error occurred and then I&#39;ll print the error message, the outstanding error message

15
00:00:53,640 --> 00:00:54,960
of what went wrong.

16
00:00:54,960 --> 00:01:01,960
So let&#39;s actually go ahead and try that. And sure enough, I don&#39;t get red text because

17
00:01:03,149 --> 00:01:06,520
at this point there now is no error. My CATCH block handled the error so the

18
00:01:06,520 --> 00:01:07,800
system hasn&#39;t thrown an error.

19
00:01:07,800 --> 00:01:11,170
But my CATCH block, my error handler,

20
00:01:11,170 --> 00:01:15,470
actually gave me that print message. It said &quot;The following error occurred:&quot;

21
00:01:15,470 --> 00:01:18,450
and then it printed out whatever the error message was. And the error message was

22
00:01:18,450 --> 00:01:20,330
&quot;Divide by zero error encountered.&quot;

23
00:01:20,330 --> 00:01:24,980
So in this case from the calling applications perspective,

24
00:01:24,980 --> 00:01:28,270
and of course in this example the calling application is SQL Server Management

25
00:01:28,270 --> 00:01:28,840
Studio,

26
00:01:28,840 --> 00:01:32,290
SQL Server Management Studio doesn&#39;t indicate that there&#39;s an error

27
00:01:32,290 --> 00:01:35,490
because as far as it&#39;s concerned there isn&#39;t. The error has been captured by my

28
00:01:35,490 --> 00:01:36,020
code.

29
00:01:36,020 --> 00:01:39,220
But my code has caught the error and handled it.

30
00:01:39,220 --> 00:01:43,550
Well that&#39;s one way of doing it, but what might be useful is to handle

31
00:01:43,550 --> 00:01:44,800
it but then also

32
00:01:44,800 --> 00:01:48,650
re-throw the error so that the client application does know that there&#39;s an

33
00:01:48,650 --> 00:01:50,300
error and it can do it&#39;s own error handling.

34
00:01:50,300 --> 00:01:53,550
So in this case, exactly the same TRY block.

35
00:01:53,550 --> 00:01:57,480
And the CATCH block this time I&#39;m printing the same message that I

36
00:01:57,480 --> 00:01:58,310
printed before,

37
00:01:58,310 --> 00:02:02,670
but then I&#39;m throwing, and I&#39;m just saying THROW, I haven&#39;t specified a number, or a message,

38
00:02:02,670 --> 00:02:03,500
or anything like that. 

39
00:02:03,500 --> 00:02:07,080
I&#39;m just saying THROW which means throw the currently outstanding error.

40
00:02:07,080 --> 00:02:10,330
So that&#39;s that re-throw that you were talking about. Exactly, yes.

41
00:02:10,330 --> 00:02:13,060
So it isn&#39;t a re-throw keyword, it&#39;s still the THROW keyword

42
00:02:13,060 --> 00:02:16,670
but without any parameters which basically says the error that I&#39;ve caught, 

43
00:02:16,670 --> 00:02:19,790
I want you through it now. Just you know  pass it on to the client application

44
00:02:19,790 --> 00:02:21,370
that called me.

45
00:02:21,370 --> 00:02:28,370
So if I go ahead and run that code, I get my error handler running so it

46
00:02:28,390 --> 00:02:30,760
deals with the original error and it

47
00:02:30,760 --> 00:02:34,330
writes down the message and that&#39;s all -- you&#39;ll notice that&#39;s not in red because at that 

48
00:02:34,330 --> 00:02:35,380
point it there wasn&#39;t an error.

49
00:02:35,380 --> 00:02:40,140
But then it throws the error again and this time the error gets caught by SQL

50
00:02:40,140 --> 00:02:41,340
Server Management Studio

51
00:02:41,340 --> 00:02:45,350
which responds with it&#39;s error handling  code. And in SQL Server Management

52
00:02:45,350 --> 00:02:47,049
Studio, when it encounters an error

53
00:02:47,049 --> 00:02:50,400
what it does is it displays red text with the error details. So there&#39;s a

54
00:02:50,400 --> 00:02:53,950
difference between those two things. The the first one is just a message.

55
00:02:53,950 --> 00:02:57,870
Yes. It&#39;s not actually an error. It may say, it may have error text in it,

56
00:02:57,870 --> 00:03:01,569
but not technically an error. In the second one --

57
00:03:01,569 --> 00:03:04,760
It&#39;s more insidious than. There are two errors going on here, but

58
00:03:04,760 --> 00:03:08,580
the first error has been handled by my code and the way it handled it was 

59
00:03:08,580 --> 00:03:09,459
to write a message.

60
00:03:09,459 --> 00:03:12,840
And then the second time, once I read through the error

61
00:03:12,840 --> 00:03:16,680
the SQL Server Management Studio caught the error and it handled it in the way that

62
00:03:16,680 --> 00:03:20,230
it handles errors which happens to be to to write out the error message

63
00:03:20,230 --> 00:03:23,890
but this time in red. And the client only saw that second one.

64
00:03:23,890 --> 00:03:27,299
The client application only saw the  second one, the first one was handled by

65
00:03:27,299 --> 00:03:29,549
my own code, my own Transact-SQL code.

66
00:03:29,549 --> 00:03:34,850
So there is a kind of further sort of advanced way that we could deal with this. 

67
00:03:34,850 --> 00:03:35,730
So let&#39;s have a look at

68
00:03:35,730 --> 00:03:39,370
a slightly more interesting example. And this time

69
00:03:39,370 --> 00:03:42,450
what I&#39;m going to do is I&#39;m going to have the same TRY block so

70
00:03:42,450 --> 00:03:45,920
effectively I&#39;m just triggering the same error. But what I&#39;m going to do this time in

71
00:03:45,920 --> 00:03:46,760
the CATCH block

72
00:03:46,760 --> 00:03:50,530
is I&#39;m going to create myself a couple variables. I&#39;ve got an ErrorLogID

73
00:03:50,530 --> 00:03:53,530
as an integer, and I&#39;ve got an error message  

74
00:03:53,530 --> 00:03:56,650
variable. I&#39;m going to 

75
00:03:56,650 --> 00:03:59,660
call a stored procedure. There&#39;s a store procedure here called,

76
00:03:59,660 --> 00:04:03,299
it&#39;s in the dbo schema, called uspLogError.

77
00:04:03,299 --> 00:04:06,889
And that&#39;s in the Azure sample database that we&#39;re using. 

78
00:04:06,889 --> 00:04:10,209
That&#39;s in the Azure sample database that&#39;s been been created as part of that database. And I can

79
00:04:10,209 --> 00:04:11,329
actually go and see that

80
00:04:11,329 --> 00:04:15,139
stored procedure if I go into programmability, and

81
00:04:15,139 --> 00:04:18,720
store procedures I can see there&#39;s a

82
00:04:18,720 --> 00:04:23,680
dbo.uspLogError. And I can even actually generate

83
00:04:23,680 --> 00:04:26,090
the code that&#39;s used. Remember we used sp.helptext

84
00:04:26,090 --> 00:04:29,930
in one of the previous demos to do this. I can actually do the same thing

85
00:04:29,930 --> 00:04:31,050
with the user interface and

86
00:04:31,050 --> 00:04:35,440
generate the code that used to create that stored procedure. It&#39;s quite a long and 

87
00:04:35,440 --> 00:04:37,160
complex stored procedure so I&#39;m not going to 

88
00:04:37,160 --> 00:04:41,150
go into details too much. But effectively what it does is it catches 

89
00:04:41,150 --> 00:04:46,460
it, it logs it into a local table. I have a table here called dbo.ErrorLog, 

90
00:04:46,460 --> 00:04:51,860
it logs the error into that. We&#39;ll actually see it do the insert into that error

91
00:04:51,860 --> 00:04:52,590
log table.

92
00:04:52,590 --> 00:04:56,699
So it will log the error in there. And then it will get the

93
00:04:56,699 --> 00:05:00,090
identity. Remember we talked about retrieving the identity value in some previous

94
00:05:00,090 --> 00:05:00,720
modules.

95
00:05:00,720 --> 00:05:04,250
It gets the identity of the error that it&#39;s just entered at the error log.

96
00:05:04,250 --> 00:05:07,470
It&#39;s not the error number. It&#39;s the number in the error log.

97
00:05:07,470 --> 00:05:12,220
And then it will throw that back as an output parameter.

98
00:05:12,220 --> 00:05:16,340
It returns that as an output parameter to the store procedure. So we could then go and have a

99
00:05:16,340 --> 00:05:16,889
look at --

100
00:05:16,889 --> 00:05:20,080
we can then go and look at in the error log to see what happens. Yeah. So this

101
00:05:20,080 --> 00:05:24,620
putting error code, a quite complex

102
00:05:24,620 --> 00:05:28,320
way of dealing with errors, putting that inside a stored procedure -- 

103
00:05:28,320 --> 00:05:32,510
is that to try and make it more reusable?

104
00:05:32,510 --> 00:05:35,650
Yeah, it makes it more reusable and encapsulates all that logic. I wouldn&#39;t want all that

105
00:05:35,650 --> 00:05:38,750
logic in every single error handler that I write in Transact-SQL.

106
00:05:38,750 --> 00:05:41,570
Because it would have to be in -- every single CATCH block would have to have that. Exactly.

107
00:05:41,570 --> 00:05:43,500
If everything has TRY/CATCH blocks then...

108
00:05:43,500 --> 00:05:47,060
Yeah, so if I write it once, I put it in a stored procedure and then every time I&#39;ve got a

109
00:05:47,060 --> 00:05:47,880
CATCH block,

110
00:05:47,880 --> 00:05:51,620
if I want to log the error, I just call that stored procedure to log the error. So

111
00:05:51,620 --> 00:05:53,260
here I&#39;ve got my 

112
00:05:53,260 --> 00:05:56,770
call here, my EXECUTE, or EXEC I could have, to

113
00:05:56,770 --> 00:06:00,020
my stored procedure. And you&#39;ll notice that I&#39;ve got the error log

114
00:06:00,020 --> 00:06:03,389
parameter, and specifically that&#39;s an output parameter because I want to get 

115
00:06:03,389 --> 00:06:08,599
that number back. When that comes back, I&#39;ve called the stored procedure, logged the error, 

116
00:06:08,599 --> 00:06:12,940
I&#39;ve got the ID of the error in the log file, what I then do is I set my

117
00:06:12,940 --> 00:06:14,550
error message variable.

118
00:06:14,550 --> 00:06:18,169
And there&#39;s a number things I&#39;ve got in here. First of all, &quot;The update failed due to

119
00:06:18,169 --> 00:06:21,240
because of an error. View error number&quot;

120
00:06:21,240 --> 00:06:24,610
and then I&#39;m taking output value that I got from the stored procedure. So I 

121
00:06:24,610 --> 00:06:26,229
know what number that is

122
00:06:26,229 --> 00:06:30,160
in the table. I&#39;m having to convert that to a varchar because

123
00:06:30,160 --> 00:06:31,010
obviously this is a

124
00:06:31,010 --> 00:06:35,260
message, it&#39;s a string. And then to finish off the message &quot;in the error log for details.&quot;

125
00:06:35,260 --> 00:06:37,660
And then I&#39;m throwing a custom error.

126
00:06:37,660 --> 00:06:40,970
And the customer error is number 50001.

127
00:06:40,970 --> 00:06:45,200
This message that I&#39;ve just created with a state of 0.

128
00:06:45,200 --> 00:06:49,350
So we actually see how that kind of hangs together. Let&#39;s go ahead and try

129
00:06:49,350 --> 00:06:54,080
it. And I get my error message, we can see it down here.

130
00:06:54,080 --> 00:06:57,510
And what&#39;s happened here is the error has occurred. The error was a divide by

131
00:06:57,510 --> 00:06:58,390
zero error.

132
00:06:58,390 --> 00:07:02,860
That&#39;s not the message I&#39;ve thrown. I&#39;ve caught that, I&#39;ve handled that by logging it,

133
00:07:02,860 --> 00:07:06,650
and then I&#39;ve generated my own error to pass back to the client application to

134
00:07:06,650 --> 00:07:07,840
say look an error occurred

135
00:07:07,840 --> 00:07:11,670
but I handled it and logged it. If you want to see the details of it,

136
00:07:11,670 --> 00:07:15,230
then go and look error, in this case, error number 1 in the error log

137
00:07:15,230 --> 00:07:18,610
because up until now all my code&#39;s been perfect and I haven&#39;t caused 

138
00:07:18,610 --> 00:07:22,990
any errors. So that means that I could now as part of my troubleshooting say well my

139
00:07:22,990 --> 00:07:24,420
application got this error,

140
00:07:24,420 --> 00:07:28,270
obviously something happened at the database level. The store procedure in

141
00:07:28,270 --> 00:07:29,020
the database

142
00:07:29,020 --> 00:07:32,840
handled the error but it&#39;s logged it. I&#39;m going to go and see what the problem was.

143
00:07:32,840 --> 00:07:36,000
And if I then go and look in the error log table,

144
00:07:36,000 --> 00:07:39,100
I can actually see the error log, the time it

145
00:07:39,100 --> 00:07:43,130
happened, various other bits of information, the error number, the severity, the error

146
00:07:43,130 --> 00:07:44,040
state.

147
00:07:44,040 --> 00:07:47,170
In this case it wasn&#39;t in a stored procedure because I just did it in the

148
00:07:47,170 --> 00:07:51,000
script file here. But there&#39;s the error message of the

149
00:07:51,000 --> 00:07:53,150
original error that occurred that I caught and handled. 

