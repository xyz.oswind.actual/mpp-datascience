0
00:00:02,140 --> 00:00:05,770
So that was our lap around table expressions.

1
00:00:05,770 --> 00:00:09,400
As you can see there&#39;s lots of ways to do effectively similar things. To

2
00:00:09,400 --> 00:00:10,150
bring back

3
00:00:10,150 --> 00:00:14,280
some sort of tabular structure of data that we&#39;re then going to treat

4
00:00:14,280 --> 00:00:17,730
as if it were a table. So we&#39;ve seen views which are

5
00:00:17,730 --> 00:00:22,470
effectively queries that we then save with a name and we can then

6
00:00:22,470 --> 00:00:26,500
reference those later on as if they were tables. Temporary tables which

7
00:00:26,500 --> 00:00:30,130
are as their name suggests, tables that are defined temporarily

8
00:00:30,130 --> 00:00:34,570
and usually they&#39;re defined at the the scope of the connection

9
00:00:34,570 --> 00:00:37,690
or the session. And when the session finishes, the table is automatically

10
00:00:37,690 --> 00:00:42,850
deleted. We saw table variables which are in some ways a more efficient alternative to

11
00:00:42,850 --> 00:00:43,429
using

12
00:00:43,429 --> 00:00:46,460
temporary tables, but they do have that consideration

13
00:00:46,460 --> 00:00:49,519
that they&#39;re defined at the scope of the  batch.

14
00:00:49,519 --> 00:00:52,690
So as soon as the batch of statements goes out of scope,

15
00:00:52,690 --> 00:00:58,059
we lose the variable. We saw Table Valued Functions where you define a function

16
00:00:58,059 --> 00:01:02,019
that could accept a parameter, and then returns a table. And then we can query 

17
00:01:02,019 --> 00:01:02,300
that

18
00:01:02,300 --> 00:01:06,189
as if it were a table. We saw derived tables which are effectively

19
00:01:06,189 --> 00:01:11,049
subqueries that are named and return multi-column tables to the outer

20
00:01:11,049 --> 00:01:11,630
query.

21
00:01:11,630 --> 00:01:14,939
And then we saw an alternative approach to doing

22
00:01:14,939 --> 00:01:17,939
a similar thing to derive tables called common table expressions

23
00:01:17,939 --> 00:01:20,979
where we separate out the definition of the

24
00:01:20,979 --> 00:01:24,149
query that we want to use for the table

25
00:01:24,149 --> 00:01:28,060
ahead of actually using it. And it has the added benefit that also supports

26
00:01:28,060 --> 00:01:30,460
recursion. So we can do those recursive queries

27
00:01:30,460 --> 00:01:35,430
into common table expressions. So lots for you to explore in the lab.

28
00:01:35,430 --> 00:01:40,759
Lots of different types of table expressions that you&#39;ll be challenged to

29
00:01:40,759 --> 00:01:42,820
use when do the lab for this module.

30
00:01:42,820 --> 00:01:46,729
A chance to get hands-on with all these and then when you are comfortable with

31
00:01:46,729 --> 00:01:47,409
all those

32
00:01:47,409 --> 00:01:50,060
come back and join us for the next module in this course.

