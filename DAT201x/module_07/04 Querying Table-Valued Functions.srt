0
00:00:02,679 --> 00:00:05,819
Table Valued Functions is the next thing that we&#39;re going to talk about.

1
00:00:05,819 --> 00:00:10,139
And we saw functions previously, we saw functions when we looked

2
00:00:10,139 --> 00:00:14,499
at the APPLY operator in the previous module on subqueries and APPLY.

3
00:00:14,499 --> 00:00:20,010
We did the sp_helptext. Yeah, we used sp_helptext to actually see the definition. Yeah.

4
00:00:20,010 --> 00:00:24,360
So you should be familiar with functions,

5
00:00:24,360 --> 00:00:28,220
at least in a passing way you&#39;ve seen them in the previous module.

6
00:00:28,220 --> 00:00:31,650
A Table Valued Function as its name

7
00:00:31,650 --> 00:00:35,089
implies is a specific type a function that returns a table.

8
00:00:35,089 --> 00:00:40,069
So it&#39;s an object again, a bit like a view it&#39;s stored in the database.

9
00:00:40,069 --> 00:00:41,170
It&#39;s a permanently

10
00:00:41,170 --> 00:00:46,179
defined object that lives in the database. And it returns a virtual table

11
00:00:46,179 --> 00:00:50,850
to a query that calls it. So in some ways is kind of like a view in the way that it is

12
00:00:50,850 --> 00:00:51,719
persisted.

13
00:00:51,719 --> 00:00:55,550
The major difference is that we can parameterize these things.

14
00:00:55,550 --> 00:00:58,949
We saw variables just a short while ago  and 

15
00:00:58,949 --> 00:01:03,149
we saw table variables. Variables can be lots of different data types. So I could have a variable

16
00:01:03,149 --> 00:01:03,729
that is

17
00:01:03,729 --> 00:01:07,450
an integer, or a string, or whatever it might be, or a

18
00:01:07,450 --> 00:01:11,380
varchar I guess we should call it. And those

19
00:01:11,380 --> 00:01:15,940
variables, if I use those variables as being parameters, I can pass values in to

20
00:01:15,940 --> 00:01:16,770
a function.

21
00:01:16,770 --> 00:01:20,660
It will then use those parameters within the function and return a table based on

22
00:01:20,660 --> 00:01:21,060
that.

23
00:01:21,060 --> 00:01:25,910
So in the example that we&#39;ve got here, it&#39;s a function that takes a parameter

24
00:01:25,910 --> 00:01:27,200
called OrderID

25
00:01:27,200 --> 00:01:31,010
which is an integer. It returns a table, and the table it returns

26
00:01:31,010 --> 00:01:34,920
is defined by that SELECT statement. And what it&#39;s returning is the

27
00:01:34,920 --> 00:01:39,940
details of the order that has that order id that we&#39;ve passed in.

28
00:01:39,940 --> 00:01:44,010
So it is a little more flexible than a view.  I don&#39;t have to have a WHERE clause when I query it,

29
00:01:44,010 --> 00:01:47,730
instead I pass in a parameter. And does this thing carry on

30
00:01:47,730 --> 00:01:51,690
existing or does it vanish when we&#39;ve finish our statement?

31
00:01:51,690 --> 00:01:56,200
No, just like a view it&#39;s an object that&#39;s  defined, it&#39;s stored in the database and I

32
00:01:56,200 --> 00:01:57,100
can actually see it

33
00:01:57,100 --> 00:01:57,880
in my

34
00:01:57,880 --> 00:02:00,860
database and come back to it again and  again and again and reuse it with

35
00:02:00,860 --> 00:02:07,860
different parameter values every time if I want to.

36
00:02:08,910 --> 00:02:10,560
So in

37
00:02:10,560 --> 00:02:13,970
this demo, we&#39;re going to look at a Table Valued Function. And

38
00:02:13,970 --> 00:02:17,920
a Table Valued Function is a function that&#39;s created,

39
00:02:17,920 --> 00:02:21,340
it defines a query, it may accept parameters, and returns

40
00:02:21,340 --> 00:02:24,470
a tabular set of data. So I&#39;m creating

41
00:02:24,470 --> 00:02:28,130
a Table Valued Function in the SalesLT schema called

42
00:02:28,130 --> 00:02:33,150
udfCustomersByCity. And the udf, user-defined function, is just a

43
00:02:33,150 --> 00:02:34,459
naming convention to help me

44
00:02:34,459 --> 00:02:39,209
realize that this is a function. I&#39;m passing in a parameter and, or at least I&#39;m going to 

45
00:02:39,209 --> 00:02:41,250
define a parameter called city

46
00:02:41,250 --> 00:02:45,000
which is a varchar 20 character string that we&#39;re going to pass to that.

47
00:02:45,000 --> 00:02:48,540
And it returns a table based on the SELECT statement which

48
00:02:48,540 --> 00:02:53,660
is a SELECT statement, it has a JOIN and again is based on passing in the city.

49
00:02:53,660 --> 00:02:56,310
So it&#39;s going to return all of the customers that live in a

50
00:02:56,310 --> 00:03:00,110
specific city. So if I go ahead and

51
00:03:00,110 --> 00:03:05,390
create that function by running that code, command completed successfully.

52
00:03:05,390 --> 00:03:09,040
It doesn&#39;t show up in my tables, it doesn&#39;t show up in my views.

53
00:03:09,040 --> 00:03:12,280
If you really want to see this thing in SQL Server Management Studio,

54
00:03:12,280 --> 00:03:15,480
you have to go exploring in the programmability folder,

55
00:03:15,480 --> 00:03:19,250
and then in functions, table valued functions

56
00:03:19,250 --> 00:03:23,100
and we should find that I have 

57
00:03:23,100 --> 00:03:26,680
my udfCustomersByCity function

58
00:03:26,680 --> 00:03:31,049
in there. So now that I&#39;ve got that function, I can use it. And I

59
00:03:31,049 --> 00:03:35,540
use it by a bit like a table, select from it. So I can select individual columns

60
00:03:35,540 --> 00:03:36,420
or I could select all of

61
00:03:36,420 --> 00:03:40,549
the rows or whatever. SELECT star from this

62
00:03:40,549 --> 00:03:44,519
function and I pass in a parameter in parenthesis. So I pass in the city of

63
00:03:44,519 --> 00:03:45,239
Bellevue

64
00:03:45,239 --> 00:03:49,560
because that&#39;s the one I want to see my customers for, and what I get back is the 

65
00:03:49,560 --> 00:03:50,530
customers who are

66
00:03:50,530 --> 00:03:51,100
base in that city.

