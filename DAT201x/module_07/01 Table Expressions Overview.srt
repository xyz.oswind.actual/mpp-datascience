0
00:00:05,600 --> 00:00:07,819
And welcome back once again.

1
00:00:07,819 --> 00:00:10,889
Hopefully you&#39;ve enjoy doing the lab from

2
00:00:10,889 --> 00:00:14,370
the previous module on subqueries and you&#39;re now absolutely comfortable

3
00:00:14,370 --> 00:00:15,250
using those,

4
00:00:15,250 --> 00:00:18,520
and we&#39;re ready now to move on to module seven. We&#39;re making

5
00:00:18,520 --> 00:00:23,080
righteous progress to this course. What we&#39;re going to look at in this module -- up until

6
00:00:23,080 --> 00:00:25,789
now, we&#39;ve talked a lot about different ways of

7
00:00:25,789 --> 00:00:29,170
querying tables and getting back row sets.

8
00:00:29,170 --> 00:00:33,160
And it should have become pretty obvious by now that an important part of

9
00:00:33,160 --> 00:00:36,379
Transact-SQL is working with these sets of data, working with

10
00:00:36,379 --> 00:00:41,010
tabular kind of constructs. So what we&#39;re going to look at in this module

11
00:00:41,010 --> 00:00:44,239
is one or two of the options that we have for generating

12
00:00:44,239 --> 00:00:48,129
tables of results that you can then work with in your Transact-SQL code,

13
00:00:48,129 --> 00:00:52,250
over above just referencing tables directly. So

14
00:00:52,250 --> 00:00:56,379
I&#39;m going to talk you through some of the  different options that we have for that.

15
00:00:56,379 --> 00:00:59,610
Here&#39;s what we&#39;re going to look at in the module.

16
00:00:59,610 --> 00:01:02,920
So we&#39;ll start off with views which is a  fairly straightforward

17
00:01:02,920 --> 00:01:06,010
part of building databases.

18
00:01:06,010 --> 00:01:08,890
And then we&#39;ll talk a little bit about  some other options like temporary tables

19
00:01:08,890 --> 00:01:10,079
and table variables.

20
00:01:10,079 --> 00:01:13,619
We&#39;ll get into table-valued functions which you have seen before. We

21
00:01:13,619 --> 00:01:16,479
talked a little bit about table-valued functions when we talked about the APPLY

22
00:01:16,479 --> 00:01:17,219
operator

23
00:01:17,219 --> 00:01:21,109
in the previous module. We&#39;ll talk about something called derived tables which

24
00:01:21,109 --> 00:01:21,450
you&#39;ll

25
00:01:21,450 --> 00:01:25,520
find familiar from the work we did on subqueries. And then we&#39;ll talk about common

26
00:01:25,520 --> 00:01:26,670
table expressions.

27
00:01:26,670 --> 00:01:30,960
So there&#39;s lots of ways to kind of do the same sort of thing and it can be a little

28
00:01:30,960 --> 00:01:31,700
confusing

29
00:01:31,700 --> 00:01:34,719
to kind of understand which one is which.

30
00:01:34,719 --> 00:01:36,409
And we will hopefully make that clear as we go through.

