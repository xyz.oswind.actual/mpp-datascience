0
00:00:02,280 --> 00:00:07,819
Now derived queries are straightforward to use if you&#39;re used to using subqueries,

1
00:00:07,819 --> 00:00:11,200
but I&#39;m sure you would agree Jeff that the syntax can be a little bit

2
00:00:11,200 --> 00:00:14,919
complex once you&#39;ve got queries within queries it can be quite confusing, yeah?

3
00:00:14,919 --> 00:00:18,200
Yeah, it just seems like some of these things we could do in other ways, but we&#39;re just

4
00:00:18,200 --> 00:00:21,430
adding complexity to understand the code.

5
00:00:21,430 --> 00:00:25,410
Right and I think for that reason a lot of people prefer to use this 

6
00:00:25,410 --> 00:00:26,900
alternative thing which is a

7
00:00:26,900 --> 00:00:31,150
a common table expression. So with a common table expression,

8
00:00:31,150 --> 00:00:34,890
in a similar way to using a derived  table, you&#39;re defining

9
00:00:34,890 --> 00:00:40,120
a temporary tabular object that you&#39;re going to use within the

10
00:00:40,120 --> 00:00:43,260
scope of a query.

11
00:00:43,260 --> 00:00:47,100
These things are again named table expression, so they have a name

12
00:00:47,100 --> 00:00:50,089
and they&#39;re defined within the scope of a query. But the way that we define them

13
00:00:50,089 --> 00:00:51,359
is we have this WITH 

14
00:00:51,359 --> 00:00:55,760
clause. So WITH CTE_year and then I&#39;ve got my column names.

15
00:00:55,760 --> 00:00:59,600
And again like a derived table, I could do that here after the name or I could do

16
00:00:59,600 --> 00:01:00,149
it in the

17
00:01:00,149 --> 00:01:03,799
the query itself. I&#39;m creating a thing called CTE_Year 

18
00:01:03,799 --> 00:01:07,310
and then in parentheses I&#39;ve got the query that defines that.

19
00:01:07,310 --> 00:01:12,460
So I&#39;m doing the inner subquery right up front. It&#39;s a bit like when you write

20
00:01:12,460 --> 00:01:12,840
a

21
00:01:12,840 --> 00:01:15,850
C# program or any other language,

22
00:01:15,850 --> 00:01:19,690
usually it&#39;s good practice to declare the variables at the top. So you declare all those variables

23
00:01:19,690 --> 00:01:23,030
so you can see the declarations of what you going to use before you use it,

24
00:01:23,030 --> 00:01:26,920
rather than declare them as you go along. Similar idea here,

25
00:01:26,920 --> 00:01:30,710
I&#39;m defining the common table expression at the beginning of the

26
00:01:30,710 --> 00:01:31,240
query,

27
00:01:31,240 --> 00:01:34,300
and then later on in that query I can reference it and I can use it.

28
00:01:34,300 --> 00:01:38,280
So this is really an alternative way of doing what we saw with

29
00:01:38,280 --> 00:01:42,920
derived tables. There&#39;s a difference

30
00:01:42,920 --> 00:01:46,720
these guys support multiple references, so I could refer to this

31
00:01:46,720 --> 00:01:48,910
multiple times. I could query from that

32
00:01:48,910 --> 00:01:52,590
common table expression again and again in the same query which I can&#39;t do 

33
00:01:52,590 --> 00:01:55,980
with a derived query. But it won&#39;t live on like a view?

34
00:01:55,980 --> 00:01:59,700
No, once the query that contains it ends, and then it goes out of scope

35
00:01:59,700 --> 00:02:00,790
and it disappears.

36
00:02:00,790 --> 00:02:03,930
But while it&#39;s in scope, I can hit it multiple times.

37
00:02:03,930 --> 00:02:07,320
And the other thing it supports is

38
00:02:07,320 --> 00:02:08,950
something called recursion. Now

39
00:02:08,949 --> 00:02:12,330
some of you here who are programmers might be familiar with the idea of recursion,

40
00:02:12,330 --> 00:02:16,570
in programming terms it usually refers to a function that calls itself. So the function

41
00:02:16,570 --> 00:02:17,380
calls its own

42
00:02:17,380 --> 00:02:21,280
function and iteratively works its way through a kind of

43
00:02:21,280 --> 00:02:25,640
recursive loop of processing. In this case we can use

44
00:02:25,640 --> 00:02:29,060
a common table expression

45
00:02:29,060 --> 00:02:32,420
if we have a table that defines a hierarchy. And again

46
00:02:32,420 --> 00:02:36,270
when we talked earlier on about self joins in one of the previous modules, we had

47
00:02:36,270 --> 00:02:37,250
the example

48
00:02:37,250 --> 00:02:41,520
employees who have managers and managers are themselves employees.

49
00:02:41,520 --> 00:02:46,190
Similar idea, we can use recursive queries and CTEs to

50
00:02:46,190 --> 00:02:49,950
deal with that. So here&#39;s the kind of  example

51
00:02:49,950 --> 00:02:53,230
of how we might do that. I&#39;ve created a common table expression called

52
00:02:53,230 --> 00:02:57,380
OrgReport and it consists of a manager ID column,

53
00:02:57,380 --> 00:03:00,950
an employee ID column, an employee name, and 

54
00:03:00,950 --> 00:03:03,950
the level within the hierarchy that that

55
00:03:03,950 --> 00:03:07,720
employee lives at, with 0 being the managing director at the top,

56
00:03:07,720 --> 00:03:11,970
and then 1 the direct reports to there, and 2 the direct reports to those guys and

57
00:03:11,970 --> 00:03:13,170
and working your way down

58
00:03:13,170 --> 00:03:17,260
through the levels of the company. So

59
00:03:17,260 --> 00:03:21,180
the way that we create a common table expression for recursion

60
00:03:21,180 --> 00:03:25,530
is slightly different to just a regular common table expression

61
00:03:25,530 --> 00:03:29,520
the defines a table of data. Because it, what we actually have to do is 

62
00:03:29,520 --> 00:03:33,130
use two tables. So we start with

63
00:03:33,130 --> 00:03:37,390
what we call the anchor query. And effectively what that&#39;s doing is getting the top

64
00:03:37,390 --> 00:03:38,780
level of the hierarchy.

65
00:03:38,780 --> 00:03:42,660
So in the employees example, it&#39;s getting that the person at the top of

66
00:03:42,660 --> 00:03:44,070
the tree, the person who was nobody

67
00:03:44,070 --> 00:03:47,670
reporting to them, &lt;Graeme meant the person who reports to nobody&gt; who is the CEO or Managing Director.

68
00:03:47,670 --> 00:03:52,020
And that anchor query, in this case we&#39;re selecting

69
00:03:52,020 --> 00:03:55,660
the manager ID, the employee ID, the employee name and

70
00:03:55,660 --> 00:03:59,850
we&#39;re actually literally giving the value 0 to the level. We&#39;re saying this is level 0, this

71
00:03:59,850 --> 00:04:00,810
is the top-level

72
00:04:00,810 --> 00:04:04,160
from the employee table where the manager ID is null. So we&#39;re getting

73
00:04:04,160 --> 00:04:05,800
everybody who doesn&#39;t have a manager

74
00:04:05,800 --> 00:04:09,060
there at level 0. Then

75
00:04:09,060 --> 00:04:13,120
we&#39;re back to some familiar syntax, we&#39;re using a UNION ALL statement.

76
00:04:13,120 --> 00:04:17,190
So we&#39;re eliminating the, we&#39;re assuming there aren&#39;t any duplicates and

77
00:04:17,190 --> 00:04:19,150
if there are, we&#39;re going to include them.

78
00:04:19,149 --> 00:04:23,510
We&#39;re doing a UNION ALL, we&#39;re getting the manager ID, the employee ID, the

79
00:04:23,510 --> 00:04:24,210
employee name,

80
00:04:24,210 --> 00:04:27,630
and then we&#39;re adding 1 to the level which was in the

81
00:04:27,630 --> 00:04:31,370
anchor query. So we&#39;re now up to level 1, and then the next time

82
00:04:31,370 --> 00:04:32,430
it will be level 2, and then level

83
00:04:32,430 --> 00:04:35,480
3, and off we go and get the

84
00:04:35,480 --> 00:04:39,520
employees under their manager ID&#39;s. You&#39;re actually joining there

85
00:04:39,520 --> 00:04:44,780
to OrgReport which doesn&#39;t yet exist because actually that&#39;s itself -- 

86
00:04:44,780 --> 00:04:48,580
Exactly it. Yeah, we have an INNER JOIN at the end that is, this is the recursive

87
00:04:48,580 --> 00:04:49,280
join. This is

88
00:04:49,280 --> 00:04:52,890
joining back again to my common table expression.

89
00:04:52,890 --> 00:04:56,210
So I&#39;ve partially declared the common table expression,

90
00:04:56,210 --> 00:04:59,669
and as part of the definition of the common table expression, I&#39;m using the

91
00:04:59,669 --> 00:05:00,760
common table expression.

92
00:05:00,760 --> 00:05:04,840
Which is again that classic recursive programming. It&#39;s a bit of a head-scratcher

93
00:05:04,840 --> 00:05:05,620
but

94
00:05:05,620 --> 00:05:08,710
that&#39;s how it works. So

95
00:05:08,710 --> 00:05:13,610
because I&#39;ve done that, I can then once I&#39;ve declared this, I can then select from

96
00:05:13,610 --> 00:05:17,350
it. I&#39;ve got my anchor query unioned to my recursive query that goes back through the

97
00:05:17,350 --> 00:05:20,560
the same CTE again and again.

98
00:05:20,560 --> 00:05:24,580
I can select from it. And one of the  options I can use, you don&#39;t have to because by default

99
00:05:24,580 --> 00:05:27,570
this is going to go through and get every single level of employee that there is.

100
00:05:27,570 --> 00:05:30,700
Because recursive queries 

101
00:05:30,700 --> 00:05:34,970
can be quite an overhead, what I might want to do is say well I don&#39;t want to

102
00:05:34,970 --> 00:05:36,020
go and

103
00:05:36,020 --> 00:05:39,120
get the entire organization structure. If it&#39;s a small

104
00:05:39,120 --> 00:05:42,150
you know Mom and Pop store as they call it 

105
00:05:42,150 --> 00:05:46,460
organization with you know maybe up to 20 employees or something and

106
00:05:46,460 --> 00:05:51,250
three levels of management, then that&#39;s fine. If it&#39;s a large organization, a

107
00:05:51,250 --> 00:05:53,530
multi-national that has hundreds of levels and

108
00:05:53,530 --> 00:05:57,290
you know potentially hundreds of thousands of employees, you probably don&#39;t want to go

109
00:05:57,290 --> 00:05:59,040
all the way through them just to generate your

110
00:05:59,040 --> 00:06:02,710
org chart. The query will be -- if you&#39;ve got, if you haven&#39;t thought about your logic

111
00:06:02,710 --> 00:06:05,919
properly, could you end up with an infinite loop effectively, that just goes on forever? Yeah.

112
00:06:05,919 --> 00:06:10,820
Yeah, so well it would go on until the server runs out of resources

113
00:06:10,820 --> 00:06:11,410
effectively.

114
00:06:11,410 --> 00:06:15,820
So to avoid that kind of situation, you can specify the max

115
00:06:15,820 --> 00:06:19,300
level of recursion you want to get to. How many recursive

116
00:06:19,300 --> 00:06:24,210
iterations do I want to allow. And in this case, I&#39;m saying MAXRECURSION 3

117
00:06:24,210 --> 00:06:27,340
and so I&#39;m getting to that third level and then stopping.

118
00:06:27,340 --> 00:06:30,270
So

119
00:06:30,270 --> 00:06:34,720
we&#39;ve got the definition of the anchor, the root level query, and then we used the UNION ALL

120
00:06:34,720 --> 00:06:37,970
to add a recursive query for all the  other levels that loops back upon the

121
00:06:37,970 --> 00:06:38,430
same

122
00:06:38,430 --> 00:06:45,100
CTE. Okay let&#39;s look

123
00:06:45,100 --> 00:06:50,610
at an example of the common table expression. So we&#39;ve got a query hear

124
00:06:50,610 --> 00:06:51,360
that has

125
00:06:51,360 --> 00:06:54,970
a WITH clause at the beginning that declares the CTE,

126
00:06:54,970 --> 00:06:59,060
and we&#39;re giving it product id, product name, and category as the names of the columns that

127
00:06:59,060 --> 00:07:02,210
come from there. And within that

128
00:07:02,210 --> 00:07:05,290
WITH statement, here&#39;s the query that defines

129
00:07:05,290 --> 00:07:09,480
the common table expression. And again just like a subquery or a derived table,

130
00:07:09,480 --> 00:07:13,010
I can run that code and see this is

131
00:07:13,010 --> 00:07:17,090
effectively what&#39;s going to be returned by my CTE.

132
00:07:17,090 --> 00:07:20,570
And then outside of that

133
00:07:20,570 --> 00:07:24,770
I&#39;ve got a SELECT statement that references the CTE, so ProductsByCategory

134
00:07:24,770 --> 00:07:26,460
is the name of the CTE

135
00:07:26,460 --> 00:07:30,350
and I&#39;m referencing it just as if it were a table here and getting back the count

136
00:07:30,350 --> 00:07:33,400
products by category ID. So if I run all of that

137
00:07:33,400 --> 00:07:37,800
as one statement, sure enough it works.

138
00:07:37,800 --> 00:07:40,970
So I&#39;ve broken out logically in my query, 

139
00:07:40,970 --> 00:07:45,330
I&#39;ve broken out the bit that gets the inner query into a separate CTE

140
00:07:45,330 --> 00:07:48,920
and then I&#39;m querying it below. And I think that&#39;s in many ways much easier to read

141
00:07:48,920 --> 00:07:52,140
and easier to work your way through than using a subquery to create

142
00:07:52,140 --> 00:07:55,440
a derived table. Now

143
00:07:55,440 --> 00:07:58,890
that&#39;s one use of a CTE and it&#39;s

144
00:07:58,890 --> 00:08:02,160
effectively an equivalent of using a derived table with an alternative

145
00:08:02,160 --> 00:08:03,300
syntax I guess.

146
00:08:03,300 --> 00:08:07,250
The other real use of a CTE is this recursive idea that we talked

147
00:08:07,250 --> 00:08:08,250
about previously.

148
00:08:08,250 --> 00:08:12,300
So to explain, I&#39;m just going to remind you of the employee table that we have,

149
00:08:12,300 --> 00:08:17,080
that we created earlier. And what we&#39;ve got in here are all of my employees

150
00:08:17,080 --> 00:08:20,480
on the ID of their manager except for

151
00:08:20,480 --> 00:08:23,370
employee number 3 who is the managing director who doesn&#39;t have a

152
00:08:23,370 --> 00:08:25,510
manager. So we have a hierarchy of

153
00:08:25,510 --> 00:08:28,990
of employees. And previously we used a  self join

154
00:08:28,990 --> 00:08:32,670
to query this. This time we&#39;re going to use a CTE.

155
00:08:32,669 --> 00:08:36,760
So I&#39;m just going to get rid of the grid at the bottom there so we can

156
00:08:36,760 --> 00:08:38,399
see the whole query.

157
00:08:38,399 --> 00:08:41,469
And what I&#39;m doing is defining an anchor query

158
00:08:41,469 --> 00:08:44,580
that gets the manager ID, the employee ID, the employee name,

159
00:08:44,580 --> 00:08:48,600
and I&#39;m hardcoding level 0 because this is the root level.

160
00:08:48,600 --> 00:08:52,730
I&#39;m unioning the results of that to the recursive query

161
00:08:52,730 --> 00:08:56,560
which gets the manager id, the employee id, the employee name, and whatever level is

162
00:08:56,560 --> 00:08:57,790
plus one

163
00:08:57,790 --> 00:09:01,529
from the employee table joined back to the CTE

164
00:09:01,529 --> 00:09:05,490
again. So that recursive join that makes the thing recursive.

165
00:09:05,490 --> 00:09:08,550
And we&#39;re then going to SELECT FROM

166
00:09:08,550 --> 00:09:14,930
this CTE with a maximum recursion level of 3. So I only want to go three levels deep

167
00:09:14,930 --> 00:09:18,839
into the hierarchy, any further than that, I want to flag an error so that

168
00:09:18,839 --> 00:09:19,459
we 

169
00:09:19,459 --> 00:09:25,220
abandon the query effectively. So let&#39;s go ahead and

170
00:09:25,220 --> 00:09:28,660
run that query. And sure enough

171
00:09:28,660 --> 00:09:32,680
I get level 0 is my managing director here who

172
00:09:32,680 --> 00:09:37,850
doesn&#39;t have a manager, and then level 1 are the kind of supervisory level if you

173
00:09:37,850 --> 00:09:39,560
like, and then level 2

174
00:09:39,560 --> 00:09:44,020
reports to level 1, and level 3 reports to level 2. So

175
00:09:44,020 --> 00:09:48,339
you can see how I can use that. It is in some ways

176
00:09:48,339 --> 00:09:51,950
more intuitive than using a self join because we&#39;re

177
00:09:51,950 --> 00:09:55,350
able to actually bring back the levels and define how many levels we

178
00:09:55,350 --> 00:09:56,650
want to get out of the query.

179
00:09:56,650 --> 00:10:01,000
And you couldn&#39;t do that recursive technique in those other table

180
00:10:01,000 --> 00:10:04,120
expressions that we&#39;ve seen? We couldn&#39;t do it in a table function or?

181
00:10:04,120 --> 00:10:08,600
No. Only in a common table expression can you use that approach. So

182
00:10:08,600 --> 00:10:12,070
if you need to do recursive queries, CTE is pretty much the only way to go.

