0
00:00:02,290 --> 00:00:05,340
So moving on from views then, temporary tables

1
00:00:05,340 --> 00:00:08,790
is something that we might want to talk about. Views are persisted in the database

2
00:00:08,790 --> 00:00:12,309
whereas temporary tables as their name suggests are temporary.

3
00:00:12,309 --> 00:00:15,980
So here&#39;s a little example, you&#39;ll notice that

4
00:00:15,980 --> 00:00:19,160
a temporary table is always, it&#39;s name is always prefixed with

5
00:00:19,160 --> 00:00:22,570
the hash symbol or the pound symbol as we call it in the UK.

6
00:00:22,570 --> 00:00:26,650
So it&#39;s #tmpProducts. 

7
00:00:26,650 --> 00:00:30,360
In the modern parlance. But yeah it&#39;s a table called

8
00:00:30,360 --> 00:00:33,469
tmpProducts, it&#39;s a temporary table that&#39;s why you have the hash marks

9
00:00:33,469 --> 00:00:33,980
in front of it.

10
00:00:33,980 --> 00:00:37,360
Other than that, it&#39;s created pretty much like any other table. We 

11
00:00:37,360 --> 00:00:38,490
create the

12
00:00:38,490 --> 00:00:41,960
column names and we specify the data types and all that type of thing.

13
00:00:41,960 --> 00:00:45,650
I guess one thing that we need to be aware of that,

14
00:00:45,650 --> 00:00:49,270
temporary tables, they aren&#39;t actually created in the database that you&#39;re working in.

15
00:00:49,270 --> 00:00:50,590
They&#39;re created in a separate

16
00:00:50,590 --> 00:00:54,720
temporary work space database that in SQL Server is called

17
00:00:54,720 --> 00:01:00,309
tempdb. In that database they&#39;re maintained by the system, so they&#39;re 

18
00:01:00,309 --> 00:01:03,690
there until they are no longer required, and then they&#39;re automatically deleted

19
00:01:03,690 --> 00:01:04,259
and that

20
00:01:04,259 --> 00:01:07,359
space is reclaimed. But they do take up resources in the

21
00:01:07,359 --> 00:01:11,799
tempdb database. So there is a consideration there. You can also create,

22
00:01:11,799 --> 00:01:13,850
the ones we&#39;ve got here are

23
00:01:13,850 --> 00:01:17,420
scoped to the current user&#39;s session. So

24
00:01:17,420 --> 00:01:21,270
with that single hash mark in front of it, it means that in this section the

25
00:01:21,270 --> 00:01:22,030
user can

26
00:01:22,030 --> 00:01:26,359
work with that table. And at the end of the session the table gets deleted, it gets

27
00:01:26,359 --> 00:01:30,389
torn away. And I guess the final point to make, as it says in the slide,

28
00:01:30,389 --> 00:01:34,100
if you want to make a temporary table that spans multiple sessions, it&#39;s a global

29
00:01:34,100 --> 00:01:35,240
temporary table, then you

30
00:01:35,240 --> 00:01:38,799
prefix it with two hashes. And then you could pass values between

31
00:01:38,799 --> 00:01:43,569
sessions? Yes. It begins to get pretty complex that way and to be honest

32
00:01:43,569 --> 00:01:47,590
if you&#39;ve got a database application that relies heavily on temporary tables,

33
00:01:47,590 --> 00:01:51,569
it&#39;s probably an indication that the design of the application is not that

34
00:01:51,569 --> 00:01:53,299
great because they do tend to

35
00:01:53,299 --> 00:01:57,020
use resources spinning them up and then tearing them down, 

36
00:01:57,020 --> 00:02:00,099
in the meantime taking up space in the tempdb.

37
00:02:00,099 --> 00:02:03,529
So that being said,

38
00:02:03,529 --> 00:02:07,539
an alternative approach might be to use table variables which in some ways

39
00:02:07,539 --> 00:02:10,390
are very similar to temporary tables.

40
00:02:10,389 --> 00:02:13,470
They were really introduced largely because

41
00:02:13,470 --> 00:02:17,070
temporary tables can cause recompilation

42
00:02:17,070 --> 00:02:20,160
of queries. We&#39;re getting perhaps into the nitty-gritty of 

43
00:02:20,160 --> 00:02:24,220
how the internals work here a bit much, but if I&#39;ve got Transact-SQL

44
00:02:24,220 --> 00:02:25,470
procedures that are

45
00:02:25,470 --> 00:02:29,070
querying temporary tables, because those temporary tables are being dropped and

46
00:02:29,070 --> 00:02:34,700
recreated, that compiled query that&#39;s used to query it will get recompile the

47
00:02:34,700 --> 00:02:35,620
next time. And

48
00:02:35,620 --> 00:02:39,280
you end up with an awful lot of a recompilations

49
00:02:39,280 --> 00:02:44,500
of queries that use up system resources.  Variables are a way of getting around that.

50
00:02:44,500 --> 00:02:47,870
So we&#39;ve got a variable here in this example.

51
00:02:47,870 --> 00:02:52,680
It&#39;s prefixed instead of with the hash symbol, it&#39;s prefixed with the at (@) symbol.

52
00:02:52,680 --> 00:02:53,230
That&#39;s how

53
00:02:53,230 --> 00:02:56,250
we know it&#39;s a variable. We declare the variable

54
00:02:56,250 --> 00:02:59,910
of type table. And of course we can have variables of lots of different types.

55
00:02:59,910 --> 00:03:00,600
You can have variables of 

56
00:03:00,600 --> 00:03:04,510
string, or integer, or whatever, table is just another data type as far as 

57
00:03:04,510 --> 00:03:07,910
variables are concerned. And then, from then on, 

58
00:03:07,910 --> 00:03:11,630
we effectively define it the same way we would define a table with columns and

59
00:03:11,630 --> 00:03:12,230
data types.

60
00:03:12,230 --> 00:03:16,150
And you can then treated just like it a table. You can SELECT star from

61
00:03:16,150 --> 00:03:19,370
you know you&#39;re @varProducts  in this case.

62
00:03:19,370 --> 00:03:22,960
So they are similar to temporary tables, they&#39;re scoped 

63
00:03:22,960 --> 00:03:26,920
to the batch not to the session. And we&#39;ll  talk in one of the later modules

64
00:03:26,920 --> 00:03:28,760
when we get into talking about programming with

65
00:03:28,760 --> 00:03:32,490
Transact-SQL, we&#39;ll get a bit more specific about what these things mean,

66
00:03:32,490 --> 00:03:36,830
but for the time being what we&#39;re saying is that if you create a temporary table

67
00:03:36,830 --> 00:03:40,970
you can then continue using that until you disconnect that session because it&#39;s

68
00:03:40,970 --> 00:03:42,250
scoped to the session.

69
00:03:42,250 --> 00:03:46,570
If you create a variable it stays in scope

70
00:03:46,570 --> 00:03:49,990
until you finish running the current set of, 

71
00:03:49,990 --> 00:03:53,440
executing the current set of Transact-SQL commands that you&#39;ve got

72
00:03:53,440 --> 00:03:57,990
in scope. So if you have a GO command in there in your

73
00:03:57,990 --> 00:03:58,710
client tool

74
00:03:58,710 --> 00:04:02,630
that will end that batch and start a new batch. If you just select some code and run it

75
00:04:02,630 --> 00:04:03,910
like we&#39;ve been doing in the demos,

76
00:04:03,910 --> 00:04:08,320
that&#39;s a batch. And once we finished that batch, the variables goes out of scope so I couldn&#39;t

77
00:04:08,320 --> 00:04:09,620
then reference it again.

78
00:04:09,620 --> 00:04:13,180
It&#39;s only available while we&#39;re in that batch.

79
00:04:13,180 --> 00:04:16,500
And the other final thing to say is

80
00:04:16,500 --> 00:04:19,590
generally the recommendation is to use these things on

81
00:04:19,589 --> 00:04:22,420
relatively small datasets. Don&#39;t take your entire

82
00:04:22,420 --> 00:04:26,050
product catalog and stick it into a variable thinking that that will improve performance because it

83
00:04:26,050 --> 00:04:26,590
won&#39;t.

84
00:04:26,590 --> 00:04:30,940
If you want to improve performance by having data in memory

85
00:04:30,940 --> 00:04:33,520
there are much more sophisticated and clever ways to do it

86
00:04:33,520 --> 00:04:37,020
than by just shoving it into a variable. They&#39;re great for something where you

87
00:04:37,020 --> 00:04:38,490
just need a little tabular

88
00:04:38,490 --> 00:04:41,660
store of data for a temporary operation as a

89
00:04:41,660 --> 00:04:46,260
and holding space. They&#39;re not really designed to take large volumes of data,

90
00:04:46,260 --> 00:04:49,960
you can do it, but you&#39;ll find that the code runs pretty slowly.

91
00:04:49,960 --> 00:04:53,490
Anything to add about variables?

92
00:04:53,490 --> 00:04:57,340
Well, only that we&#39;ll talk about variables  more later in the course. So we 

93
00:04:57,340 --> 00:04:59,820
haven&#39;t really introduced the concept of a variable.

94
00:04:59,820 --> 00:05:03,840
Yep. We will go into depth about the different types variables, what they&#39;re

95
00:05:03,840 --> 00:05:04,460
for and 

96
00:05:04,460 --> 00:05:07,870
passing values to them.

97
00:05:07,870 --> 00:05:10,870
Okay, so in the slides we we talked about

98
00:05:10,870 --> 00:05:13,910
temporary tables and variables. I just wanted to kind of 

99
00:05:13,910 --> 00:05:17,560
show you those in this demo to try and and clarify the use of them.

100
00:05:17,560 --> 00:05:21,610
So we start off with a temporary table, and you can see I&#39;m going to create one called

101
00:05:21,610 --> 00:05:23,659
colors here. I want to just temporarily store

102
00:05:23,659 --> 00:05:27,650
all the colors that we have. So it&#39;s only got one column in this case,

103
00:05:27,650 --> 00:05:32,340
it&#39;s a varchar column with 15 characters. And I&#39;ll just go ahead and run that code.

104
00:05:32,340 --> 00:05:36,990
And that creates this temporary table. Now it&#39;s not going to be listed in my list of 

105
00:05:36,990 --> 00:05:39,379
tables, I could refresh that and it still wouldn&#39;t be there.

106
00:05:39,379 --> 00:05:43,099
It&#39;s actually created as a temporary object in the tempdb

107
00:05:43,099 --> 00:05:47,139
system database. But I can treat it just  like a table, so I can insert some data

108
00:05:47,139 --> 00:05:47,780
into there.

109
00:05:47,780 --> 00:05:51,130
And off I&#39;ve gone and entered some rows into there

110
00:05:51,130 --> 00:05:55,750
and then I can start selecting from it just like any other table. So I&#39;m getting

111
00:05:55,750 --> 00:05:56,020
my

112
00:05:56,020 --> 00:05:59,440
list of colors back from that  temporary table.

113
00:05:59,440 --> 00:06:03,590
So that&#39;s fairly straightforward then, it&#39;s just 

114
00:06:03,590 --> 00:06:08,080
pretty much working with a table. A table variable is pretty similar, I

115
00:06:08,080 --> 00:06:09,960
declare it as a variable of type

116
00:06:09,960 --> 00:06:14,219
table and I specify the columns. So again we&#39;re creating one column in this

117
00:06:14,219 --> 00:06:20,699
particular variable called Color. And if  I just select that

118
00:06:20,699 --> 00:06:24,080
and then try to insert into it,

119
00:06:24,080 --> 00:06:28,779
I get an error that says the variable isn&#39;t declared.

120
00:06:28,779 --> 00:06:32,979
And that&#39;s because it&#39;s scoped to the batch of commands.

121
00:06:32,979 --> 00:06:36,620
It doesn&#39;t hang around. That worked  when you did the temporary table.

122
00:06:36,620 --> 00:06:39,909
Yes, it worked with the temporary table because that&#39;s scoped to the session

123
00:06:39,909 --> 00:06:43,710
and it would continue to work, I could still work with my temporary table now

124
00:06:43,710 --> 00:06:47,669
until I actually disconnect this session. And then that temporary table will disappear.

125
00:06:47,669 --> 00:06:51,009
But if I&#39;m going to work with a variable, I have to do everything in the one batch

126
00:06:51,009 --> 00:06:52,300
of commands. So I&#39;m going to 

127
00:06:52,300 --> 00:06:55,509
declare the variable, insert the data into it,

128
00:06:55,509 --> 00:06:58,520
and then select from it all in the one batch of statements.

129
00:06:58,520 --> 00:07:02,240
So if I go ahead run that, then it works

130
00:07:02,240 --> 00:07:07,029
and I get my data coming back. But if of course I then start a new batch,

131
00:07:07,029 --> 00:07:10,520
well I can still get to my temporary table as we saw,

132
00:07:10,520 --> 00:07:13,569
but my variable is now out of scope.

133
00:07:13,569 --> 00:07:18,070
So it&#39;s gone. It&#39;s very, it&#39;s more temporary that a temporary 

134
00:07:18,070 --> 00:07:18,610
table

135
00:07:18,610 --> 00:07:22,599
if you see what I mean. And can we treat, like views, can we treat these

136
00:07:22,599 --> 00:07:24,300
things as tables? We can

137
00:07:24,300 --> 00:07:27,789
maybe do a join to something else? Yes,  once they&#39;re declared, as long as they&#39;re

138
00:07:27,789 --> 00:07:28,460
in scope,

139
00:07:28,460 --> 00:07:31,560
they behave just like tables. I can select from them, I can join them, I can do all

140
00:07:31,560 --> 00:07:31,900
that type of thing.

