0
00:00:02,560 --> 00:00:05,600
Now derived tables, we saw in a previous

1
00:00:05,600 --> 00:00:12,150
module subqueries and you can think of derived tables as being subqueries

2
00:00:12,150 --> 00:00:15,929
that return an entire table that might have multiple columns.

3
00:00:15,929 --> 00:00:20,340
So some of the subqueries that we saw previously, there were scalar 

4
00:00:20,340 --> 00:00:22,000
subqueries that returned a single value

5
00:00:22,000 --> 00:00:25,230
and there were multi-value subqueries that returned

6
00:00:25,230 --> 00:00:28,689
effectively a table with one column, an array

7
00:00:28,689 --> 00:00:33,540
in programming terms. These derived tables are subqueries that return

8
00:00:33,540 --> 00:00:37,190
a multi-column table. So we&#39;re returning a table that has multiple columns.

9
00:00:37,190 --> 00:00:41,530
And there are of couple of rules around the way that we use these things. So

10
00:00:41,530 --> 00:00:45,190
the first thing is that they&#39;re named query expressions. They&#39;re created within

11
00:00:45,190 --> 00:00:49,269
a SELECT statement. So they aren&#39;t  permanent objects, they&#39;re not stored in the database or

12
00:00:49,269 --> 00:00:52,570
anything like that. They are a virtual table for the purposes of

13
00:00:52,570 --> 00:00:57,659
simplifying a query effectively. The scope of the

14
00:00:57,659 --> 00:01:02,350
derived table, unlike a temporary table or even a variable

15
00:01:02,350 --> 00:01:05,570
they&#39;re not even scoped to the batch, they&#39;re scoped to within that statement,

16
00:01:05,570 --> 00:01:08,250
within the SELECT statement in which they are defined. As soon as that SELECT

17
00:01:08,250 --> 00:01:09,060
statement finishes,

18
00:01:09,060 --> 00:01:13,619
the derived table no longer exists. So it&#39;s purely a programming

19
00:01:13,619 --> 00:01:14,340
construct

20
00:01:14,340 --> 00:01:18,610
within a SELECT statement. So if we had the same

21
00:01:18,610 --> 00:01:23,100
piece of code within a SELECT statement that we repeated,

22
00:01:23,100 --> 00:01:26,409
you could use it for that. You can  essentially name if you like

23
00:01:26,409 --> 00:01:31,270
and then refer to it in that way. Yeah,  exactly. So some guidelines for

24
00:01:31,270 --> 00:01:32,369
using these things.

25
00:01:32,369 --> 00:01:37,680
They must have an alias and in the example we saw previously, we had the entire 

26
00:01:37,680 --> 00:01:38,280
subquery

27
00:01:38,280 --> 00:01:41,799
encapsulated in parenthesis, and then at the end it was

28
00:01:41,799 --> 00:01:45,329
AS and then we gave it a name. So just in the same way that you 

29
00:01:45,329 --> 00:01:48,670
apply aliases to tables in a SELECT statement if you&#39;re going join them,

30
00:01:48,670 --> 00:01:53,560
quite often you have to apply an alias if it&#39;s a derived table.

31
00:01:53,560 --> 00:01:58,009
There has to be a unique name for all the columns that it returns. So if 

32
00:01:58,009 --> 00:02:01,799
the derived query itself references multiple tables and those tables have

33
00:02:01,799 --> 00:02:02,350
the same

34
00:02:02,350 --> 00:02:05,530
column name, like a Product has a Name column and

35
00:02:05,530 --> 00:02:09,420
ProductCategory also a Name column, you have to assign aliases to those

36
00:02:09,419 --> 00:02:10,450
columns to make sure they&#39;re

37
00:02:10,449 --> 00:02:12,610
unique.

38
00:02:12,610 --> 00:02:15,980
You can&#39;t use an ORDER BY clause in the query.

39
00:02:15,980 --> 00:02:20,210
You can use an ORDER BY if you have a TOP or an OFFSET/FETCH in there but other

40
00:02:20,210 --> 00:02:21,010
than that, you can&#39;t just

41
00:02:21,010 --> 00:02:24,100
apply an ORDER BY to sort the data  you know arbitrarily

42
00:02:24,100 --> 00:02:28,040
in a derived query. So if you desperately wanted to.

43
00:02:28,040 --> 00:02:31,400
you could do a TOP 100 percent 

44
00:02:31,400 --> 00:02:34,840
with an ORDER BY. Yeah, performance wise probably 

45
00:02:34,840 --> 00:02:37,880
at that point I think that&#39;s probably not  the right way to do it, but yeah I guess

46
00:02:37,880 --> 00:02:40,940
that would be one way around that. You should be doing the ORDER BY in the final part of the

47
00:02:40,940 --> 00:02:45,080
SELECT statement. Exactly, remember you&#39;re derived table is within a query, so if you want to

48
00:02:45,080 --> 00:02:46,810
order the results, order them in the outer

49
00:02:46,810 --> 00:02:50,380
query rather than in the inner query. Right.

50
00:02:50,380 --> 00:02:54,459
There are, you&#39;re not allowed to refer them multiple times in the same query. So

51
00:02:54,459 --> 00:02:54,890
we

52
00:02:54,890 --> 00:02:59,760
talked about views being able to use them again and again and variables

53
00:02:59,760 --> 00:03:03,120
being able to use them again and again, these guys are defined within a query and you can&#39;t use them

54
00:03:03,120 --> 00:03:04,440
multiple times in that query.

55
00:03:04,440 --> 00:03:08,600
You can only use them the once. There are some additional

56
00:03:08,600 --> 00:03:12,890
things you might think about though. They can use internal or external aliases

57
00:03:12,890 --> 00:03:14,230
for columns and we&#39;ll talk about

58
00:03:14,230 --> 00:03:17,700
the difference between those two on the next slide. And

59
00:03:17,700 --> 00:03:21,269
they can refer to parameters and variables. So if you have declared variables

60
00:03:21,269 --> 00:03:23,760
you can use references to those variables within

61
00:03:23,760 --> 00:03:28,170
the query. So your derived query could in fact reference a table

62
00:03:28,170 --> 00:03:28,670
variable

63
00:03:28,670 --> 00:03:33,670
and then query that. And they can be nested within other derived tables. So I could have a

64
00:03:33,670 --> 00:03:37,019
query that contains a derived table that contains a derived table that contains a derived 

65
00:03:37,019 --> 00:03:37,640
table.

66
00:03:37,640 --> 00:03:41,600
So you are able to kind of reuse them in that sense, you can nest 

67
00:03:41,600 --> 00:03:42,500
them within one another.

68
00:03:42,500 --> 00:03:45,780
So I talked about

69
00:03:45,780 --> 00:03:49,370
the two different types of column alias. And 

70
00:03:49,370 --> 00:03:52,340
I guess the most intuitive way if you&#39;ve been working with Transact-SQL for 

71
00:03:52,340 --> 00:03:53,680
awhile, you&#39;re probably used to

72
00:03:53,680 --> 00:03:57,299
defining aliases inline. So when we&#39;ve got that 

73
00:03:57,299 --> 00:04:01,239
example here you can see that the derived table is

74
00:04:01,239 --> 00:04:06,370
the inner query if you like. It&#39;s in this parenthesis here SELECT

75
00:04:06,370 --> 00:04:09,940
YEAR(orderdate) AS orderyear. So I&#39;m

76
00:04:09,940 --> 00:04:13,720
creating the definition of the column name

77
00:04:13,720 --> 00:04:17,930
as an alias actually in the SELECT statement of the derived query itself.

78
00:04:17,930 --> 00:04:21,310
And then I finish the derived query and then give it a name AS derived_year.

79
00:04:21,310 --> 00:04:24,610
So that&#39;s a fairly you know intuitive way to 

80
00:04:24,610 --> 00:04:25,200
add

81
00:04:25,200 --> 00:04:29,230
column aliases. The other way that you can do this is you could actually

82
00:04:29,230 --> 00:04:33,630
use them externally to the derived query. So in the query itself, I&#39;m 

83
00:04:33,630 --> 00:04:35,180
selecting the year of the order date

84
00:04:35,180 --> 00:04:39,060
but I have assigned an alias there. What what I&#39;ve done is I&#39;ve waited until I&#39;ve

85
00:04:39,060 --> 00:04:40,930
completed the definition of the query,

86
00:04:40,930 --> 00:04:46,060
given it a name AS derived_year, and then in parenthesis after that I can declare

87
00:04:46,060 --> 00:04:46,430
the

88
00:04:46,430 --> 00:04:49,860
names of the columns. So either of these syntaxes

89
00:04:49,860 --> 00:04:56,860
is perfectly acceptable. Let&#39;s see an example

90
00:04:56,960 --> 00:05:00,230
of derived queries. So in this case I have

91
00:05:00,230 --> 00:05:03,870
a table that I want to get

92
00:05:03,870 --> 00:05:07,890
some data out of. So I&#39;m getting the category and the number of products

93
00:05:07,890 --> 00:05:08,970
within that category.

94
00:05:08,970 --> 00:05:12,130
And I&#39;m doing that not from a table but from

95
00:05:12,130 --> 00:05:16,750
a derived query. So I&#39;ve got an outer  SELECT statement that contains an inner

96
00:05:16,750 --> 00:05:17,700
SELECT statement

97
00:05:17,700 --> 00:05:20,970
which defines the derived query with the name ProdCats.

98
00:05:20,970 --> 00:05:24,110
So I&#39;m selecting in the derived 

99
00:05:24,110 --> 00:05:29,270
table, I&#39;m selecting the ProductID, the Name AS Product so I&#39;m using the

100
00:05:29,270 --> 00:05:32,830
inline convention for naming the column names of the derived table,

101
00:05:32,830 --> 00:05:38,520
and Name AS Category from the Product table JOIN to the ProductCategory table

102
00:05:38,520 --> 00:05:42,100
and then giving that the name ProdCats.  And I&#39;m then,

103
00:05:42,100 --> 00:05:45,610
once I get the results from the derived table, I&#39;m back to the outer query again 

104
00:05:45,610 --> 00:05:47,740
and at that point, I&#39;m grouping by the category and 

105
00:05:47,740 --> 00:05:52,160
I&#39;m ordering by the category. So as we saw when we worked with 

106
00:05:52,160 --> 00:05:55,750
scalar subqueries, I can go ahead and I can select

107
00:05:55,750 --> 00:05:59,750
the entire definition of that subquery  just to test that it works. And if I run that I

108
00:05:59,750 --> 00:06:00,520
can see

109
00:06:00,520 --> 00:06:03,730
this is what my derived table is going to  look like.

110
00:06:03,730 --> 00:06:08,350
So that&#39;s what I&#39;m selecting from in the outer query. And if I go ahead run the

111
00:06:08,350 --> 00:06:11,510
the entire thing, I&#39;m then able to

112
00:06:11,510 --> 00:06:13,720
group into the categories and get the count.

