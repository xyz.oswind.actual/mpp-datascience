0
00:00:02,600 --> 00:00:06,930
So Jeff we&#39;re going talk a little bit about querying views.

1
00:00:06,930 --> 00:00:11,530
And I guess the first thing is to talk about what a view is. 

2
00:00:11,530 --> 00:00:15,849
So how do you think of a view as opposed to say a table? I think it&#39;s quite

3
00:00:15,849 --> 00:00:17,210
well named in that it&#39;s

4
00:00:17,210 --> 00:00:20,690
a view of the data. So we&#39;re actually looking 

5
00:00:20,690 --> 00:00:25,240
through this view into the underlying tables. It&#39;s a way of presenting

6
00:00:25,240 --> 00:00:29,980
the data that&#39;s in our tables. We might want to -- let&#39;s say we have a join.

7
00:00:29,980 --> 00:00:33,090
We&#39;re spending a lot of time constantly joining the

8
00:00:33,090 --> 00:00:36,450
two tables together and selecting from them, what we could do is create a view

9
00:00:36,450 --> 00:00:37,149
which

10
00:00:37,149 --> 00:00:40,640
actually defines that joined syntax within it and then we can just select

11
00:00:40,640 --> 00:00:42,649
from the view as if it were a table.

12
00:00:42,649 --> 00:00:46,530
But we&#39;re actually viewing through  the view into the underlying data in the table,

13
00:00:46,530 --> 00:00:48,079
the data is still in the table,

14
00:00:48,079 --> 00:00:51,860
it&#39;s just a presentation, if you like, of that data.

15
00:00:51,860 --> 00:00:55,820
Right and that&#39;s an important point. Views are

16
00:00:55,820 --> 00:01:00,160
they&#39;re effectively named queries as it says on the slide. What we&#39;ve done is taken the

17
00:01:00,160 --> 00:01:01,239
query that we&#39;ve written,

18
00:01:01,239 --> 00:01:05,009
which might be a really complex query  the references multiple tables or

19
00:01:05,009 --> 00:01:09,200
uses various different constructs within it,  and what we&#39;ve done is we&#39;ve

20
00:01:09,200 --> 00:01:10,619
encapsulated that in a view.

21
00:01:10,619 --> 00:01:14,430
And that creates a layer of abstraction over the underlying tables

22
00:01:14,430 --> 00:01:18,650
which can be used to simplify, provide a simpler interface if you like for

23
00:01:18,650 --> 00:01:20,479
developers to use to query those tables.

24
00:01:20,479 --> 00:01:24,610
And also to give you an additional security layer. We can start 

25
00:01:24,610 --> 00:01:27,720
adding security at the view layer and

26
00:01:27,720 --> 00:01:31,000
then we don&#39;t have to worry about people  accessing the tables directly underneath.

27
00:01:31,000 --> 00:01:33,610
So you give someone rights to the view

28
00:01:33,610 --> 00:01:37,500
without giving it them rights to the table. Yeah. So then they see it the way

29
00:01:37,500 --> 00:01:38,979
you intend it to be shown.

30
00:01:38,979 --> 00:01:42,210
Exactly. So pretty useful tools.

31
00:01:42,210 --> 00:01:45,549
On the slide you can see the syntax for  creating a view.

32
00:01:45,549 --> 00:01:49,299
I keep emphasizing this course is really about Transact-SQL, it&#39;s not 

33
00:01:49,299 --> 00:01:49,600
about

34
00:01:49,600 --> 00:01:53,369
implementing databases. There are other courses you can go on to learn more

35
00:01:53,369 --> 00:01:53,930
about that. But

36
00:01:53,930 --> 00:01:57,610
we do need to kind of show you just the basics of how you would create a

37
00:01:57,610 --> 00:01:58,420
view. So

38
00:01:58,420 --> 00:02:01,840
we&#39;ve got the CREATE VIEW statement. We give it a name,

39
00:02:01,840 --> 00:02:05,200
and then it&#39;s defined as a SELECT statement effectively. It&#39;s

40
00:02:05,200 --> 00:02:08,250
a bunch of SQL that we&#39;re encapsulating in that view.

41
00:02:08,250 --> 00:02:10,840
And I can then query that view

42
00:02:10,840 --> 00:02:14,530
just like I&#39;m querying a table. Just as Jeff said. So I can then use a SELECT

43
00:02:14,530 --> 00:02:15,849
statement and query from it.

44
00:02:15,849 --> 00:02:19,400
And you can see in the example on the slide were just selecting some columns

45
00:02:19,400 --> 00:02:19,800
from

46
00:02:19,800 --> 00:02:23,069
it. From the point of view of writing the Transact-SQL,

47
00:02:23,069 --> 00:02:26,290
it&#39;s unclear as to whether that&#39;s a table or not.

48
00:02:26,290 --> 00:02:30,069
I happen to have used a naming convention where I&#39;ve prefixed it with a v, so I know that it&#39;s

49
00:02:30,069 --> 00:02:30,959
a view, but

50
00:02:30,959 --> 00:02:34,709
if I hadn&#39;t done that, you wouldn&#39;t be able to tell if that was a table or view.

51
00:02:34,709 --> 00:02:37,830
So you could even join it to another table and

52
00:02:37,830 --> 00:02:42,090
treat it as if it were a table. Yes, you could create a complex query that 

53
00:02:42,090 --> 00:02:43,860
includes a view joined to tables and

54
00:02:43,860 --> 00:02:47,620
all that kind of thing. You treat it just like a table. There are one or two

55
00:02:47,620 --> 00:02:51,829
slight considerations with views when it comes to inserting and updating data,

56
00:02:51,829 --> 00:02:55,849
which we don&#39;t necessarily need to go into in huge detail in this course.

57
00:02:55,849 --> 00:02:59,400
I guess the only thing you probably need to be aware of is if you are updating or

58
00:02:59,400 --> 00:03:00,769
inserting data through a view,

59
00:03:00,769 --> 00:03:04,489
you can only affect one of the underlying tables that that view is based on.

60
00:03:04,489 --> 00:03:07,920
And that is one of the restrictions around using views. But other than that

61
00:03:07,920 --> 00:03:08,709
there 

62
00:03:08,709 --> 00:03:12,500
pretty much exactly the same as tables. So let&#39;s have a look at using views.

63
00:03:12,500 --> 00:03:17,519
So I&#39;m in transact, I&#39;m in SQL Server Management Studio, I&#39;ve got some

64
00:03:17,519 --> 00:03:19,040
Transact-SQL code here

65
00:03:19,040 --> 00:03:23,290
and you can see I&#39;ve got a CREATE VIEW statement where I&#39;m creating a view

66
00:03:23,290 --> 00:03:27,060
called vCustomerAddress in the SalesLT schema.

67
00:03:27,060 --> 00:03:30,060
And it simply selects from the

68
00:03:30,060 --> 00:03:33,760
Customers table joined to the CustomerAddress table which is where we store

69
00:03:33,760 --> 00:03:35,410
all of our address information.

70
00:03:35,410 --> 00:03:40,030
And I&#39;ve got a couple of bits of query in there.

71
00:03:40,030 --> 00:03:43,380
It&#39;s not a particularly complex query. It has the join where I&#39;m joining the address table.

72
00:03:43,380 --> 00:03:47,109
But I want to encapsulate that and make it simpler for people to use that.

73
00:03:47,109 --> 00:03:51,069
So if I go ahead and run that piece of code,

74
00:03:51,069 --> 00:03:56,139
what that&#39;s done is created this object in the database called vCustomerAddress.

75
00:03:56,139 --> 00:03:56,459
And

76
00:03:56,459 --> 00:04:00,389
and because I&#39;m using SQL Server Management Studio, I can actually look in the views

77
00:04:00,389 --> 00:04:00,980
folder,

78
00:04:00,980 --> 00:04:04,230
rather than the tables folder, and I can see that

79
00:04:04,230 --> 00:04:08,030
has been created there. So it is an object that&#39;s persisted in the database itself.

80
00:04:08,030 --> 00:04:13,510
And now as a developer, I can use Transact-SQL to go ahead and referenced that view.

81
00:04:13,510 --> 00:04:14,930
So I just simply query it 

82
00:04:14,930 --> 00:04:18,280
just like it&#39;s a table. And sure enough it returns

83
00:04:18,279 --> 00:04:21,880
the data that I need. The customer ID and the city that that customer

84
00:04:21,880 --> 00:04:27,230
is living in. And similarly I can treat it as we said just like a table.

85
00:04:27,230 --> 00:04:30,390
So I can join it, I can treat it the same way I do with any other table, join it

86
00:04:30,390 --> 00:04:31,530
to other tables and

87
00:04:31,530 --> 00:04:34,410
all that kind of thing. There&#39;s no real special consideration just because

88
00:04:34,410 --> 00:04:35,130
it&#39;s a view.

89
00:04:35,130 --> 00:04:38,450
So in this case I&#39;m joining it

90
00:04:38,450 --> 00:04:42,110
to the sales order header. And I&#39;m getting the customer address information

91
00:04:42,110 --> 00:04:45,660
and the order information. And if I go ahead and run that,

92
00:04:45,660 --> 00:04:50,720
you can see that I&#39;m getting back  the revenue broken down by state and

93
00:04:50,720 --> 00:04:52,310
province and then by city.

94
00:04:52,310 --> 00:04:57,330
And so I&#39;m encapsulating some of the complexity in a view and then

95
00:04:57,330 --> 00:04:59,590
building a more complex query on top of the view

96
00:04:59,590 --> 00:05:04,000
just as if it were a table. I think, could you  even create a view

97
00:05:04,000 --> 00:05:08,510
of that new definition that you&#39;ve now got? Because if I do that, I can

98
00:05:08,510 --> 00:05:13,250
reduced the code again. Absolutely, yeah I can create a view that references a view.

99
00:05:13,250 --> 00:05:14,040
So I could 

100
00:05:14,040 --> 00:05:18,600
take that as far as I really need to go. And create a layer of abstraction

101
00:05:18,600 --> 00:05:20,200
over the underlying tables that

102
00:05:20,200 --> 00:05:23,860
just makes easier for either reporting applications to use or for

103
00:05:23,860 --> 00:05:27,310
applications that developers are building to interact with the database

104
00:05:27,310 --> 00:05:27,560
to use.

