0
00:00:00,000 --> 00:00:02,240
Now this course includes some labs,

1
00:00:02,240 --> 00:00:03,520
that will give you a chance to get some

2
00:00:03,530 --> 00:00:07,310
hands-on, practice with the transact  SQL code that we&#39;re going to be teaching

3
00:00:07,310 --> 00:00:11,300
and if you want to complete the labs, then you have to set up the environments.

4
00:00:11,300 --> 00:00:15,740
So, you&#39;re ready to work with your database and run your code.

5
00:00:15,760 --> 00:00:17,840
So, to help you do that we&#39;ve got this getting

6
00:00:17,860 --> 00:00:20,330
started guide here, which provide you

7
00:00:20,330 --> 00:00:23,320
with a little bit of information about, how we expect you to work through the

8
00:00:23,510 --> 00:00:25,500
the course materials and I&#39;ll tell you,

9
00:00:25,510 --> 00:00:27,500
what you need, in order to set up your

10
00:00:27,510 --> 00:00:31,280
environment to complete the labs. Now there&#39;s a couple of ways you can do this

11
00:00:31,280 --> 00:00:34,300
and recommended approach is to use azure,

12
00:00:34,340 --> 00:00:36,960
and to have an azure SQL database

13
00:00:36,960 --> 00:00:40,960
instance hosted an Azure, that you can connect you from a local to your

14
00:00:40,960 --> 00:00:44,649
computer, in order to work with the database and that save you

15
00:00:44,640 --> 00:00:46,880
installing a database server on your

16
00:00:46,880 --> 00:00:48,560
your client computer, you just basically need to

17
00:00:48,560 --> 00:00:52,380
install the client tools and connect to Azure. If you prefer to work with a

18
00:00:52,390 --> 00:00:55,670
local database, you can do that there is additional instructions at the end of

19
00:00:55,670 --> 00:01:00,300
this document , on a setting up a SQL server express and getting ready

20
00:01:00,320 --> 00:01:04,150
to go with that but we do recommend that you use the Azure option

21
00:01:04,150 --> 00:01:05,500
that&#39;s that&#39;s the one that we&#39;ve we&#39;ve

22
00:01:05,519 --> 00:01:08,330
based  labs and then we&#39;ve tested. So, the

23
00:01:08,330 --> 00:01:12,220
instructions include some steps, for setting up an Azure subscription

24
00:01:12,320 --> 00:01:15,070
and there&#39;s a link here if you haven&#39;t already gone Azure subscription,

25
00:01:15,070 --> 00:01:16,270
there&#39;s a link here that will get you to

26
00:01:16,280 --> 00:01:17,680
the point, where you can create a free

27
00:01:17,680 --> 00:01:23,820
one month trial of azure and its name suggests it&#39;s a 30 day trial period,

28
00:01:24,200 --> 00:01:26,040
and you do have to enter a credit card in order

29
00:01:26,080 --> 00:01:28,410
to and verify your identity but you&#39;re not

30
00:01:28,420 --> 00:01:32,780
going to be charged for that trial period and actually you get $200 credit

31
00:01:32,820 --> 00:01:39,000
or your local currency to spend on Azure services, so you&#39;ll have more than enough

32
00:01:39,020 --> 00:01:41,180
credits in Euro and your free trial to

33
00:01:41,220 --> 00:01:42,530
spend on the resources that were going to,

34
00:01:42,530 --> 00:01:46,360
use in this course. See we go ahead the some frequently asked questions your do

35
00:01:46,360 --> 00:01:49,880
recommend you read through those, just make sure that you&#39;re you&#39;re happy to go

36
00:01:49,900 --> 00:01:53,220
ahead and then you can sign up for Azure trial subscription and you can use

37
00:01:53,230 --> 00:01:57,990
that for the course. Now once you&#39;ve got your Azure trial subscription,

38
00:01:57,990 --> 00:02:01,939
you&#39;ll be able to log-in to the Azure portal, so we just go there we&#39;ll see what

39
00:02:01,939 --> 00:02:04,939
that looks like,

40
00:02:05,020 --> 00:02:11,940
and the Azure portal looks a bit like this, it&#39;s a web based

41
00:02:12,000 --> 00:02:15,510
administrative portal, through which I can manage all of my Azure services

42
00:02:15,510 --> 00:02:19,840
and the purposes of this course, the service I&#39;m going to need to create is an

43
00:02:19,860 --> 00:02:23,750
Azure SQL database, so to do that I put this menu here on the left here

44
00:02:23,750 --> 00:02:27,870
I can expand that, I&#39;ll get the full menu or just kept it down, but the button is

45
00:02:27,870 --> 00:02:31,810
important to me here, is this new button here and going to use that to add a

46
00:02:31,810 --> 00:02:35,739
resource to my Azure subscription and there&#39;s various categories of

47
00:02:35,730 --> 00:02:37,140
resources I can use,

48
00:02:37,160 --> 00:02:40,220
and unsurprisingly the one that we are looking for is data and storage

49
00:02:40,260 --> 00:02:44,140
and within that there is SQL database, we&#39;re going to want to create an instance

50
00:02:44,140 --> 00:02:50,720
of a SQL database service here and I do that I get this blade appearing here

51
00:02:50,720 --> 00:02:53,000
with all these these pins, blades So, I got to

52
00:02:53,040 --> 00:02:54,760
believe here asking for a name from my

53
00:02:54,760 --> 00:02:58,970
SQL database, we&#39;re going to use a sample database, to set this up and it is

54
00:02:58,970 --> 00:03:03,400
based on a sample code adventure works so, I&#39;m just going to call the the database

55
00:03:03,420 --> 00:03:08,200
AdventureWorksLT. So that&#39;s the name of the database, that we&#39;re going to work

56
00:03:08,220 --> 00:03:13,040
with in the labs in this course. Now that database has to be hosted on our server

57
00:03:13,060 --> 00:03:16,940
and I&#39;m in currently I don&#39;t have any SQL sever in my, my azure

58
00:03:16,960 --> 00:03:21,100
subscription second, click on the server option here now opens up another blade

59
00:03:21,120 --> 00:03:24,400
and here I&#39;m going to create a new server.

60
00:03:24,420 --> 00:03:28,080
So click on and opens to another blade. Now we&#39;re going to enter the

61
00:03:28,120 --> 00:03:33,480
name for my server, I want to create that this has to be globally unique fully

62
00:03:33,500 --> 00:03:37,140
full name here, is this whatever I type in here dot database dot Windows dot net.

63
00:03:37,160 --> 00:03:40,720
And has to be globally unique, so has to be something nobody else is already using.

64
00:03:40,740 --> 00:03:46,350
So if I were, for example just type sqltest as a pretty good chance, somebody is

65
00:03:46,360 --> 00:03:49,000
already using that should goes often as a check.

66
00:03:49,010 --> 00:03:50,480
I can see that it&#39;s hovering here and then

67
00:03:50,490 --> 00:03:52,540
and I can get this little red exclamation mark to say that

68
00:03:52,560 --> 00:03:54,540
that name is already in use.

69
00:03:54,560 --> 00:03:56,260
So we try something different. We put in

70
00:03:56,280 --> 00:04:00,700
something like as the course number dat201sql.

71
00:04:00,720 --> 00:04:05,080
And this time I get a little green tick and it tells me that the name is available.

72
00:04:05,100 --> 00:04:07,710
I can use up, so that&#39;s fine

73
00:04:07,720 --> 00:04:11,920
and I need to specify a name for the admin login. So this is the, the login for

74
00:04:11,940 --> 00:04:15,920
the administrator of our server and I&#39;m just going to use my name for that.

75
00:04:15,960 --> 00:04:18,680
And then I need to specify a password as well

76
00:04:18,690 --> 00:04:20,500
and there some rules of the password I just put

77
00:04:20,510 --> 00:04:23,000
in something, finished  straight forward in that

78
00:04:23,020 --> 00:04:25,060
I get, again an error here to tell me

79
00:04:25,080 --> 00:04:28,340
that&#39;s involved in there some rules, I need to apply to my password

80
00:04:28,360 --> 00:04:32,460
to make it complex. So we&#39;re just going to put on a slightly more complex password

81
00:04:32,480 --> 00:04:38,080
there I&#39;m going to confirm that password as well just in case if we miss type check.

82
00:04:38,100 --> 00:04:39,960
Do you want to remember the username and password

83
00:04:39,980 --> 00:04:41,110
you specify their you&#39;re going to need

84
00:04:41,130 --> 00:04:45,780
those when you connect and then you can choose, where about you want to host this

85
00:04:45,800 --> 00:04:49,680
server. So the location is actually the the azure data center within which

86
00:04:49,700 --> 00:04:53,740
do you want to host your data. So in my case I&#39;m currently in Europe

87
00:04:53,760 --> 00:04:55,300
so I going to find one of the here.

88
00:04:55,320 --> 00:04:58,220
The European data centers now choose west Europe

89
00:04:58,240 --> 00:05:00,360
and that&#39;s going to go ahead and choose

90
00:05:00,390 --> 00:05:04,320
that location. So the only thing I&#39;m going to look at here

91
00:05:04,330 --> 00:05:05,500
is that the latest version.

92
00:05:05,520 --> 00:05:08,000
The latest version of the time that I&#39;m recording this is V12

93
00:05:08,040 --> 00:05:09,480
just if that&#39;s available in that

94
00:05:09,480 --> 00:05:13,320
region is going to tell me, do I want to create the latest version? yes, I do

95
00:05:13,340 --> 00:05:16,760
and do I want allow azure services to access the server.

96
00:05:16,760 --> 00:05:18,420
we yes, I do in essence

97
00:05:18,440 --> 00:05:21,940
and some going to believe that checked. So we click ok there

98
00:05:21,960 --> 00:05:23,940
and I goes increase my server

99
00:05:24,060 --> 00:05:28,330
then is slightly slow back to our sequel database

100
00:05:28,350 --> 00:05:30,780
and blade here, we were creating this SQL Database.

101
00:05:30,790 --> 00:05:32,780
So we were created a database got AdventureWorksLT.

102
00:05:32,800 --> 00:05:38,040
In the new server that I&#39;ve just specified in West Europe. As a source of

103
00:05:38,060 --> 00:05:40,830
this database by default is going to get a Blank database, that doesn&#39;t have

104
00:05:40,840 --> 00:05:44,650
any tables in it and that&#39;s not really what we want for this course. We need to

105
00:05:44,670 --> 00:05:48,020
meet some our tables in order to query. So I&#39;m going to select

106
00:05:48,040 --> 00:05:50,020
the source here, just open up

107
00:05:50,040 --> 00:05:54,100
and I&#39;m going to choose sample. And when I do the

108
00:05:54,120 --> 00:05:57,140
back in my SQL Database blade here

109
00:05:57,180 --> 00:06:00,980
You can see, I can select the sample i want to use in by default it selected

110
00:06:01,000 --> 00:06:05,140
this AdventureWorksLT[V12]. which is actually the one I want to use

111
00:06:05,160 --> 00:06:08,140
and there is a list of various samples in here, just two versions of

112
00:06:08,160 --> 00:06:10,020
AdventureWorksLT depending on the version

113
00:06:10,040 --> 00:06:15,020
of the server you&#39;ve got, we&#39;re going to use the latest version. So that&#39;s we

114
00:06:15,120 --> 00:06:18,300
specified that the database, I want to create based on the sample.

115
00:06:18,360 --> 00:06:21,210
And that&#39;s important, if you say your environment up and you discover

116
00:06:21,230 --> 00:06:24,740
that you, you go to create and there aren&#39;t any tables there it&#39;s probably

117
00:06:24,760 --> 00:06:28,300
because you haven&#39;t selected the sample is probably, if you created a blank

118
00:06:28,320 --> 00:06:33,380
database. So just be careful to select that. As a pricing tier, if you&#39;re using

119
00:06:33,400 --> 00:06:35,340
a free trial, then obviously that&#39;s just

120
00:06:35,340 --> 00:06:36,680
going to use the create that&#39;s part of your

121
00:06:36,680 --> 00:06:39,720
free trial. If you&#39;re using, you don&#39;t azure subscription

122
00:06:39,740 --> 00:06:43,910
and there is a cost associated with running the database and just to

123
00:06:43,920 --> 00:06:48,140
minimize the cost of a comments the pricing tier here, I can look all of the

124
00:06:48,180 --> 00:06:52,620
other different available options for my my database server and I&#39;m also going to

125
00:06:52,640 --> 00:06:55,910
scroll the way down, the bottom here and I&#39;m going to choose this basic

126
00:06:55,930 --> 00:07:01,340
option here, which is the cheapest option and I&#39;ll get the local price per months

127
00:07:01,360 --> 00:07:05,080
listed there. Says it is relatively inexpensive and I&#39;m going to go ahead

128
00:07:05,100 --> 00:07:08,650
and choose the lowest price anyway, just to a make sure I&#39;m not paying any more

129
00:07:08,670 --> 00:07:11,250
than I need to there&#39;s just going to be learning SQL. I don&#39;t really need to be

130
00:07:11,270 --> 00:07:15,980
hosting a huge amount of data. Will leave the collision as default is just the

131
00:07:16,020 --> 00:07:18,400
the sort order in the character codes for the database

132
00:07:18,400 --> 00:07:20,000
to leave that and is default

133
00:07:20,040 --> 00:07:24,150
and it&#39;s going to create a Resource group for this SQL database

134
00:07:24,170 --> 00:07:27,300
server and the database and again we can just leave that as its before.

135
00:07:27,340 --> 00:07:30,870
We&#39;re going to use that for anything else on this course. Just make sure that&#39;s

136
00:07:30,890 --> 00:07:35,000
associated with the subscription. And you want to add to create in, if you come out

137
00:07:35,020 --> 00:07:38,180
multiple subscriptions you could choose here, if you set up a free trial

138
00:07:38,220 --> 00:07:41,620
then it&#39;s obviously just going to be your free trial subscription and you can

139
00:07:41,640 --> 00:07:45,100
optionally choose to pin it to the dashboard, once it&#39;s has been created

140
00:07:45,140 --> 00:07:48,220
and just make easier to get to it&#39;s actually is enough to Browse to through the

141
00:07:48,240 --> 00:07:51,060
the menu here, but we can pin it to the dashboard to make it quick

142
00:07:51,080 --> 00:07:52,600
to get to our database.

143
00:07:52,720 --> 00:07:58,820
So go ahead and click Create, and if it goes and I can see the initializing the

144
00:07:58,840 --> 00:08:02,000
deployment and in submitting the deployment and it started the deployment

145
00:08:02,080 --> 00:08:06,840
And I get this little tile here on my, my dashboard telling me that is

146
00:08:06,860 --> 00:08:10,810
currently creating the database so that&#39;s going to take maybe five minutes

147
00:08:10,840 --> 00:08:13,360
or so, as we&#39;ll sit back and leave that running

148
00:08:13,360 --> 00:08:15,480
and with the magic of video editing.

149
00:08:15,520 --> 00:08:24,300
It shouldn&#39;t take too long at all.

150
00:08:24,340 --> 00:08:27,380
And then when your database is ready you&#39;ll get this

151
00:08:27,440 --> 00:08:29,980
little notification here to say that ready.

152
00:08:30,020 --> 00:08:34,940
And it will open up automatically at the blade for your database

153
00:08:35,039 --> 00:08:39,380
Now if you close these bases also with the settings displayed here close those down

154
00:08:39,400 --> 00:08:46,380
and goodbye to my home my dashboard

155
00:08:46,400 --> 00:08:48,880
I can still see the tile here on the dashboard that

156
00:08:48,920 --> 00:08:52,260
I specified should be created so I can get to it quite easily from there,

157
00:08:52,300 --> 00:08:56,880
or I could browse to adjust by browsing through all of my resources

158
00:08:56,920 --> 00:09:00,500
and I can see the database there as well so there&#39;s a number of ways I can get to that

159
00:09:00,540 --> 00:09:03,680
database, just by opening up and when it

160
00:09:03,680 --> 00:09:05,040
opens up you can see here it&#39;s got

161
00:09:05,040 --> 00:09:08,340
the settings for the, the database now the database should remember is hosted in a

162
00:09:08,360 --> 00:09:13,640
server and if I have a look at the lead for the database itself I can see that server

163
00:09:13,680 --> 00:09:18,700
name. Here is the server and the server dat201sql.database.Windows.net

164
00:09:18,760 --> 00:09:20,240
Now, one of things you going to

165
00:09:20,240 --> 00:09:21,840
have to do is make sure that connect, to

166
00:09:21,840 --> 00:09:26,840
that server from my local computer so I&#39;m going to go and have a look at the sever.

167
00:09:26,860 --> 00:09:31,660
We just going to click on the server name and it opens up a blade for the server

168
00:09:31,700 --> 00:09:34,520
and also the settings blade for that server.

169
00:09:34,520 --> 00:09:36,820
and then the blade for the server here I can see

170
00:09:36,840 --> 00:09:41,670
some as in details about line including the option to show the firewall rules

171
00:09:41,670 --> 00:09:46,820
for that Server, so good, choose that option and show firewall rules.

172
00:09:46,880 --> 00:09:49,600
I now open&#39;s up this page and the firewall

173
00:09:49,600 --> 00:09:52,140
rules are harder to restrict.

174
00:09:52,140 --> 00:09:55,860
The computers that can connect to the server across the internet and by default,

175
00:09:55,900 --> 00:09:59,100
it&#39;s going to allow access from other Azure services are running

176
00:09:59,120 --> 00:10:02,320
within the same data center but doesn&#39;t allow any sort of access

177
00:10:02,340 --> 00:10:03,840
externally across the Internet,

178
00:10:03,900 --> 00:10:08,120
including from my own machine but has picked up my own IP address

179
00:10:08,160 --> 00:10:10,720
Note this is that, this is the IP address, I&#39;m connecting

180
00:10:10,720 --> 00:10:13,940
from and it gives me the option to add that

181
00:10:14,000 --> 00:10:18,700
to the list of IP addresses are allowed to access the servers of that it

182
00:10:18,740 --> 00:10:20,560
decrease of firewall rule effectively

183
00:10:20,560 --> 00:10:22,550
called ClientIPAddress and in today&#39;s

184
00:10:22,550 --> 00:10:28,880
date and specifies my IP address in there as the IP address to be going to allowed to

185
00:10:28,880 --> 00:10:32,740
access. So, you can do that to allow access from your local computer.

186
00:10:32,780 --> 00:10:38,400
Now, if you are connected to a network perhaps at school or university or work

187
00:10:38,440 --> 00:10:41,060
you might find that, there is an externally

188
00:10:41,080 --> 00:10:43,620
facing IP address, rather than the new

189
00:10:43,620 --> 00:10:47,680
one local IP address is being used to connect to the server. So, you may want to

190
00:10:47,720 --> 00:10:52,300
create a rule that allows a range of IP addresses to access the server,

191
00:10:52,360 --> 00:10:58,420
you couldn&#39;t find to arrange that, it was from 00000 to 255 255 255 255

192
00:10:58,460 --> 00:10:59,980
that would allow access from every

193
00:10:59,980 --> 00:11:02,120
computer that is connected to the internet so you wouldn&#39;t

194
00:11:02,180 --> 00:11:05,440
want to do that with a production server but if you are trying to troubleshoot

195
00:11:05,460 --> 00:11:09,020
connectivity and then that&#39;s one place to start just you know

196
00:11:09,060 --> 00:11:11,720
allow you access from any computer and just eliminate the fight that

197
00:11:11,780 --> 00:11:13,920
might be your IP address is the problem.

198
00:11:13,980 --> 00:11:17,820
So once we&#39;ve added that rule to allow you access from my local IP.

199
00:11:17,850 --> 00:11:22,100
I&#39;m going to save that for a while, don&#39;t forget to save it otherwise it&#39;ll will

200
00:11:22,100 --> 00:11:23,480
loose the rule you&#39;ve created.

201
00:11:23,480 --> 00:11:27,980
And once it saves, successfully update the firewall rules, I&#39;m not able to access

202
00:11:28,020 --> 00:11:32,810
my server from this local computer. Now ofcourse that means I need

203
00:11:32,810 --> 00:11:37,440
some sort of client to on the local computer that I can use to connect to my

204
00:11:37,480 --> 00:11:41,120
my database server. Now in this course we

205
00:11:41,140 --> 00:11:42,060
are going to use SQL server

206
00:11:42,060 --> 00:11:46,170
Our Management Studio and at one of the prerequisites we need for installing

207
00:11:46,170 --> 00:11:51,320
sql server management studio is we need to .net framework 3.5 and as a feature of

208
00:11:51,320 --> 00:11:53,160
Windows. so, if you are going to have a

209
00:11:53,160 --> 00:11:55,200
look at the programs and features of

210
00:11:55,200 --> 00:12:00,000
Windows, your installation and turn Windows features on or off

211
00:12:00,000 --> 00:12:03,920
even you want to make sure that the dotnet framework 3.5

212
00:12:03,960 --> 00:12:07,680
is installed and ready to go. So once

213
00:12:07,720 --> 00:12:11,000
you&#39;ve made sure of that, you&#39;re ready to go and install SQL server Management

214
00:12:11,060 --> 00:12:14,940
Studio and in the setup guide will give you the link to go ahead and install that

215
00:12:14,960 --> 00:12:19,840
which is here and this is actually the

216
00:12:19,840 --> 00:12:23,480
link to install SQL server 2014 Express,

217
00:12:23,480 --> 00:12:27,670
which includes all of the bits of SQL server. So, when you

218
00:12:27,670 --> 00:12:31,140
click download here as a whole bunch of different options for download. Now we

219
00:12:31,140 --> 00:12:32,400
don&#39;t need to actually install the

220
00:12:32,400 --> 00:12:34,060
database engines. We need to install

221
00:12:34,060 --> 00:12:38,570
everything here, all we really need here are the management tools So we need to

222
00:12:38,570 --> 00:12:44,370
SQL Server Management Studio either for 32 bit or 64 bit installation of

223
00:12:44,370 --> 00:12:46,580
Windows depending on what versions of Windows you are running.

224
00:12:46,620 --> 00:12:48,860
So I&#39;ve got a 64 bit installation of Windows.

225
00:12:48,860 --> 00:12:56,160
So choose that one there, I next click and that&#39;s going to go ahead

226
00:12:56,220 --> 00:13:02,500
and download the installation files for SQL server 2014 Express and specifically

227
00:13:02,500 --> 00:13:04,600
for the Management Studio components.

228
00:13:04,600 --> 00:13:05,660
So, now take a little while to

229
00:13:05,660 --> 00:13:09,690
download, incidentally if you haven&#39;t already signed in with your Microsoft

230
00:13:09,690 --> 00:13:12,660
account, when you go to download it will prompt you to sign in again, just sign in

231
00:13:12,660 --> 00:13:14,540
with your Microsoft account and then

232
00:13:15,910 --> 00:13:17,580
we&#39;ll just wait for that to download.

233
00:13:17,580 --> 00:13:27,180
So that&#39;s done, downloaded we will go ahead and run the downloaded executable

234
00:13:27,380 --> 00:13:31,760
and what that&#39;s going to do is simply going to extract the

235
00:13:31,760 --> 00:13:33,720
files the installation files for

236
00:13:33,780 --> 00:13:38,100
SQL server Express. So we go ahead and extract those to a local folder.

237
00:13:42,560 --> 00:13:47,260
And after they&#39;ve been extracted automatically the SQL server

238
00:13:47,260 --> 00:13:50,000
setup program should start

239
00:13:50,000 --> 00:13:53,160
and that looks like this, so we got the SQL server

240
00:13:53,200 --> 00:13:58,360
installation center and we&#39;re going to new SQL server stand-alone installation

241
00:13:58,400 --> 00:14:00,240
or add features to an existing installation.

242
00:14:00,240 --> 00:14:02,240
In this case we are creating a new installation,

243
00:14:02,250 --> 00:14:10,300
so go ahead and click that, refresh  and accept the license

244
00:14:10,320 --> 00:14:14,100
agreement carry on and said it will do

245
00:14:14,100 --> 00:14:16,320
some share rule checking just make sure

246
00:14:16,320 --> 00:14:19,660
that we&#39;re ready to do the install.

247
00:14:19,680 --> 00:14:23,840
And we can optionally go and use Microsoft Update to get any necessary

248
00:14:23,860 --> 00:14:26,590
updates to this point,

249
00:14:33,670 --> 00:14:37,500
and I&#39;m ready to install, we just select all the features that we need.

250
00:14:37,500 --> 00:14:40,860
In  this case we&#39;re only installing all the client tools, is the shared features for a

251
00:14:40,860 --> 00:14:44,220
SQL Server instance that were installing and that the important things

252
00:14:44,220 --> 00:14:48,430
here in the management tools-complete, here that were installing there. So, just go

253
00:14:48,430 --> 00:14:57,340
ahead and carry on with that and then SQL Server setup,

254
00:14:57,340 --> 00:15:01,900
will start to install various  components that we need.

255
00:15:06,940 --> 00:15:10,780
When installation is complete, we get this message here to tell us that

256
00:15:10,780 --> 00:15:16,360
everything has been installed. So, we can close out of the setup program, that we

257
00:15:16,360 --> 00:15:21,530
close all over various Windows now, that we&#39;ve got open and I should find

258
00:15:21,530 --> 00:15:23,700
if I&#39;ll look at the Apps that are installed

259
00:15:23,700 --> 00:15:27,680
on my computer here, it&#39;s just going to list all of the apps,

260
00:15:27,700 --> 00:15:30,180
and turn here among the Microsoft applications

261
00:15:30,180 --> 00:15:34,890
and I can see Microsoft SQL Server 2008, as just the

262
00:15:34,890 --> 00:15:37,820
the installation and package and I make sure

263
00:15:37,860 --> 00:15:41,090
SQL Server 2014,  never going to there&#39;s a

264
00:15:41,090 --> 00:15:45,040
whole bunch of different components, of SQL Server in their,

265
00:15:45,080 --> 00:15:50,120
the one I&#39;m interested in is SQL Server 2014 Management Studio, just make life a

266
00:15:50,120 --> 00:15:56,080
useful for myself, I&#39;m going to go ahead and I&#39;m going to pin that to my taskbar

267
00:15:56,140 --> 00:16:01,610
Just see how it is easy for me to get to hear on the desktop, so I can open up from

268
00:16:01,610 --> 00:16:10,160
the task there. And  the First time is opens is going to load some user settings,

269
00:16:10,160 --> 00:16:10,500
to get yourself ready. the task there. And  the First time is opens is going to load some user settings,

270
00:16:10,500 --> 00:16:13,500
to get yourself ready.

271
00:16:17,170 --> 00:16:22,730
and once it opens, I can then connect to an instance of SQL Server and in this

272
00:16:22,730 --> 00:16:28,240
case we&#39;ve already created, Azure SQL database instance, in  Azure so,

273
00:16:28,240 --> 00:16:31,649
we&#39;ll go ahead and connect that, and the server name l choose when I created that was

274
00:16:31,640 --> 00:16:41,920
dat 2O12 SQL and the fully-qualified name is that .database.windows.net

275
00:16:41,940 --> 00:16:45,540
and I don&#39;t use Windows authentication to do, that I&#39;m going to

276
00:16:45,540 --> 00:16:50,720
use SQL Server Authentication, because I specified are an admin login, you might

277
00:16:50,720 --> 00:16:55,800
remember, I just use my name for that and I specified password for that as well

278
00:16:55,800 --> 00:16:59,340
So, just type in the password, now in

279
00:16:59,360 --> 00:17:01,750
most cases this is all going to be finding

280
00:17:01,750 --> 00:17:06,260
just the name and the password like that, actually the fully-qualified name

281
00:17:06,260 --> 00:17:09,549
for some other client tools other than SQL Server, to do you may need to

282
00:17:09,549 --> 00:17:18,439
do this, is actually the user name at the server names who is dat 21 SQL. So, you

283
00:17:18,430 --> 00:17:20,220
can adjust the name of the name at the

284
00:17:20,240 --> 00:17:22,160
server name of that, in this case, I&#39;m going to

285
00:17:22,160 --> 00:17:25,429
log in using our password, to get to remember my password enough to type it in.

286
00:17:25,429 --> 00:17:35,230
Again, I&#39;ll click Connect and because I enabled the firewall rule, in Azure allows

287
00:17:35,230 --> 00:17:38,840
access from my local computer, will be able to connect and authenticate to the

288
00:17:38,840 --> 00:17:40,640
server using the username and password

289
00:17:40,660 --> 00:17:42,600
and I can see in the list of databases

290
00:17:42,600 --> 00:17:42,620
here, I&#39;ve got my adventuresworksLt database, there and if I expand database, I can see and I can see in the list of databases

291
00:17:42,620 --> 00:17:48,450
here, I&#39;ve got my adventuresworksLt database, there and if I expand database, I can see

292
00:17:48,450 --> 00:17:50,840
the tables and so on in that database,

293
00:17:50,840 --> 00:17:53,950
all that information is there and if I

294
00:17:53,950 --> 00:17:58,460
wanted  to create a Query, I just go ahead and create a new query and you

295
00:17:58,480 --> 00:18:00,320
always have to choose the database you want to run your Query,

296
00:18:00,320 --> 00:18:01,020
and your going to choose the

297
00:18:01,020 --> 00:18:04,830
the AdventureWorksLt  database and you get used to doing that, as this course

298
00:18:04,830 --> 00:18:07,600
progresses and what am i doing just

299
00:18:07,600 --> 00:18:09,870
pump the Zoom here up so, we can

300
00:18:09,870 --> 00:18:13,540
see what we&#39;re doing a little bit better and I could put in a Query here like

301
00:18:13,540 --> 00:18:18,240
select star from, I have got the list of tables at

302
00:18:18,280 --> 00:18:21,570
the rest left with me SalesLt,

303
00:18:21,570 --> 00:18:27,659
the schema dot product so, as go ahead and run the query, click execute

304
00:18:28,700 --> 00:18:33,580
and it runs the Query, I&#39;ll get back my results in this this grid underneath.

305
00:18:33,600 --> 00:18:38,530
So, now I got a working connection to my Azure SQL database from my local

306
00:18:38,530 --> 00:18:41,790
installation of SQL Server Management Studio. So, this is a little bit of

307
00:18:41,790 --> 00:18:44,780
installation, to do a little bit of configuration, remember you could install

308
00:18:44,780 --> 00:18:44,800
the Dotnet framework 3.5, before you can installation, to do a little bit of configuration, remember you could install

309
00:18:44,800 --> 00:18:47,440
the Dotnet framework 3.5, before you can

310
00:18:47,440 --> 00:18:49,590
install the client tools and once you do,

311
00:18:49,590 --> 00:18:52,580
that you can install SQL Server- Management Studio and connect your

312
00:18:52,580 --> 00:18:58,240
Azure SQL database instance and work with the date of this in there. So, that should

313
00:18:58,240 --> 00:18:59,780
be everything you need to get set up to

314
00:18:59,780 --> 00:19:02,790
do the labs and the course. Let&#39;s move on

315
00:19:02,790 --> 00:19:07,300
and you can download the setup guide and for those instructions for yourself.

