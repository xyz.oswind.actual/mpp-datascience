Preparing for the Labs
================

Transact-SQL is an essential skill for database professionals and
developers working with Microsoft SQL Server or Microsoft Azure SQL
Database. This course combines online presentations with hands-on labs
that will give you practical experience and a chance to test and extend
your Transact-SQL programming skills.

To complete the labs in this course, you will need to set up a lab
environment that includes the [AdventureWorksLT sample
database](https://gitlab.com/xyz.oswind.actual/mpp-datascience/blob/master/DAT201x/module_00/AdventureWorksLT2012_Data.mdf).
There is an Entity Relationship (ER) diagram of the tables in this
database in the [Course
Handouts](https://gitlab.com/xyz.oswind.actual/mpp-datascience/blob/master/DAT201x/module_00/AdventureWorksLT_ER.pdf).

To prepare for the labs, follow the instructions in the [Getting Started
Guide](https://gitlab.com/xyz.oswind.actual/mpp-datascience/blob/master/DAT201x/module_00/Getting_Started_with_DAT201x.pdf),
which explains how to setup a local database instance and how to
provision the database using Azure.

The main repository for AdventureWorks sample databases is located
[here](https://github.com/Microsoft/sql-server-samples/releases/tag/adventureworks).
