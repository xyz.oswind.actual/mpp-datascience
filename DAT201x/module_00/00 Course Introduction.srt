0
00:00:02,400 --> 00:00:06,040
Hi and welcome to this course on Querying with Transact-SQL.

1
00:00:06,040 --> 00:00:09,780
We&#39;ve got a pretty packed course for you, we&#39;re going to be covering all sorts of

2
00:00:09,780 --> 00:00:10,280
things and

3
00:00:10,280 --> 00:00:13,429
exploring ways that we can use Transact-SQL to get data

4
00:00:13,429 --> 00:00:16,900
databases. So let me start by introducing myself,

5
00:00:16,900 --> 00:00:21,700
my name is Graeme Malcolm. I&#39;m a senior content developer at Microsoft and

6
00:00:21,700 --> 00:00:24,050
what I do is I focus on developing

7
00:00:24,050 --> 00:00:27,270
are learning content for people who want to work with our data platform

8
00:00:27,270 --> 00:00:28,070
technologies.

9
00:00:28,070 --> 00:00:32,410
Prior to that, I was a consultant and a trainer. I&#39;ve been working with the

10
00:00:32,409 --> 00:00:35,670
with SQL Server and with data related technologies for a number years.

11
00:00:35,670 --> 00:00:39,019
I&#39;m joined today by Geoff Allix who

12
00:00:39,019 --> 00:00:42,329
I&#39;ll get to introduce himself, tell us a little bit about yourself, Geoff.

13
00:00:42,329 --> 00:00:45,859
Hi, my name is Geoff Allix, I&#39;m principal technologist

14
00:00:45,859 --> 00:00:50,129
at a company called Content Master and we develop content primarily for

15
00:00:50,129 --> 00:00:50,879
Microsoft.

16
00:00:50,879 --> 00:00:55,809
And my own job is to work with SQL Server and so I do language and other

17
00:00:55,809 --> 00:00:57,109
database technologies

18
00:00:57,109 --> 00:01:00,969
and I&#39;ve done that for quite number years, and also do bits of consultancy

19
00:01:00,969 --> 00:01:04,420
and another work with databases essentially.

20
00:01:04,420 --> 00:01:07,550
Great, so great having you on board Geoff. So 

21
00:01:07,550 --> 00:01:10,700
let&#39;s move on and talk a little bit about what we&#39;re going cover in this

22
00:01:10,700 --> 00:01:11,480
course.

23
00:01:11,480 --> 00:01:15,250
This is quite a long course, there&#39;s eleven modules of things that we&#39;re going

24
00:01:15,250 --> 00:01:15,880
to cover

25
00:01:15,880 --> 00:01:20,000
and we&#39;ll get you to pace yourself as you work your way through the content.

26
00:01:20,000 --> 00:01:23,490
For each of the modules, we&#39;ll deliver

27
00:01:23,490 --> 00:01:26,970
one of these video presentations and we&#39;ll  demonstrate some of the concepts and

28
00:01:26,970 --> 00:01:27,750
show you how the

29
00:01:27,750 --> 00:01:32,540
the syntax of the language works. And there some labs that you can download and try

30
00:01:32,540 --> 00:01:35,390
out for yourself. So you&#39;ll get some hands-on practice as well.

31
00:01:35,390 --> 00:01:38,620
We&#39;re gonna start with an introduction to

32
00:01:38,620 --> 00:01:42,740
Transact-SQL and explain what it is, where it came from, what sort of things

33
00:01:42,740 --> 00:01:43,610
we can do with it,

34
00:01:43,610 --> 00:01:47,650
and then we&#39;ll gradually look at more  complex

35
00:01:47,650 --> 00:01:51,110
features of the Transact-SQL language as we progress through

36
00:01:51,110 --> 00:01:53,090
the course and look at each of the different modules.

37
00:01:53,090 --> 00:01:56,520
So drilling into a little bit of detail,

38
00:01:56,520 --> 00:02:00,200
just some expectations that we have, if you&#39;re attending this course we

39
00:02:00,200 --> 00:02:02,710
assume that you&#39;re interested in databases or

40
00:02:02,710 --> 00:02:06,730
development of some description. So really we&#39;re trying to

41
00:02:06,730 --> 00:02:10,649
target this training content for people who are aspiring database professionals,

42
00:02:10,649 --> 00:02:14,430
perhaps you&#39;re at the beginning of your career in IT and you want to be a

43
00:02:14,430 --> 00:02:18,819
a database administrator or a database developer. Or perhaps you&#39;re already an

44
00:02:18,819 --> 00:02:21,689
application developer and your building applications in things like

45
00:02:21,689 --> 00:02:24,860
C# or java or whatever language it might be

46
00:02:24,860 --> 00:02:30,390
and you want to start working with databases and those applications.

47
00:02:30,390 --> 00:02:34,620
The other a part of the audience that we might be looking at here is

48
00:02:34,620 --> 00:02:40,030
anyone who&#39;s considering doing the SQL Server certification exams.

49
00:02:40,030 --> 00:02:43,670
So if you&#39;re planning to sit the exams for the Microsoft Certified

50
00:02:43,670 --> 00:02:44,849
Professional program,

51
00:02:44,849 --> 00:02:49,129
there is a Transact-SQL exam and this course will help prepare you for

52
00:02:49,129 --> 00:02:51,480
that exam. It doesn&#39;t necessarily cover everything

53
00:02:51,480 --> 00:02:55,099
that will be covered on the exam but certainly the core Transact-SQL

54
00:02:55,099 --> 00:02:58,470
syntax will be covered. There&#39;s

55
00:02:58,470 --> 00:03:02,329
various materials, there&#39;s these online video presentations which you can

56
00:03:02,329 --> 00:03:02,950
watch

57
00:03:02,950 --> 00:03:06,989
and we will be demonstrating as well as well as explaining some the concepts,

58
00:03:06,989 --> 00:03:10,340
and I said earlier there are some labs that you can download and try out for

59
00:03:10,340 --> 00:03:11,049
yourself.

60
00:03:11,049 --> 00:03:14,450
And our suggested approach is that you pace yourself through this,

61
00:03:14,450 --> 00:03:17,650
watch the module, download the lab,

62
00:03:17,650 --> 00:03:20,690
there may be some links to additional reading that we suggest you look at,

63
00:03:20,690 --> 00:03:24,280
look at that, do the lab make sure that you&#39;ve absorbed everything in that,

64
00:03:24,280 --> 00:03:27,340
and then move on to the next module. So pace yourself through

65
00:03:27,340 --> 00:03:30,730
you can go as quickly or slowly as you like, but 

66
00:03:30,730 --> 00:03:34,099
make sure that you&#39;re comfortable before moving on to the next module.

67
00:03:34,099 --> 00:03:37,169
There is, I mention there 

68
00:03:37,169 --> 00:03:41,150
about Born to Learn sites. Born to Learn is a website that

69
00:03:41,150 --> 00:03:44,290
we&#39;ve created at Microsoft for  people who are learning Microsoft

70
00:03:44,290 --> 00:03:46,239
technologies. So if you

71
00:03:46,239 --> 00:03:49,280
want to engage with your fellow students around the world who are also

72
00:03:49,280 --> 00:03:50,030
studying

73
00:03:50,030 --> 00:03:54,510
Transact-SQL and other aspects of  database technologies with Microsoft,

74
00:03:54,510 --> 00:03:56,459
then go to the Born to Learn site

75
00:03:56,459 --> 00:03:59,699
and sign up. And again when you download the lab materials there&#39;s 

76
00:03:59,699 --> 00:04:00,919
information about how to do that.

