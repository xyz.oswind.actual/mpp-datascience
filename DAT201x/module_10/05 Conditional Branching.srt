0
00:00:02,440 --> 00:00:06,149
Now we&#39;re going to have a look at branching our code. So we&#39;re going to

1
00:00:06,149 --> 00:00:07,049
have a look

2
00:00:07,049 --> 00:00:11,370
at a condition in this case if, and

3
00:00:11,370 --> 00:00:15,220
if the condition is true we&#39;ll one thing and if it&#39;s false we&#39;ll do another. 

4
00:00:15,220 --> 00:00:18,780
Or actually to make it even simpler, if it&#39;s true

5
00:00:18,780 --> 00:00:22,070
we&#39;ll do this thing. So that&#39;s the most  basic one. We can say if 

6
00:00:22,070 --> 00:00:25,300
and then a condition, if that&#39;s true we do something.

7
00:00:25,300 --> 00:00:28,360
Then we can add a little bit more to that and have else and say ok

8
00:00:28,360 --> 00:00:31,710
if it&#39;s true do this, else do something else.

9
00:00:31,710 --> 00:00:36,440
Now these ones in here are quite simplified in that they just have one

10
00:00:36,440 --> 00:00:37,579
line of code,

11
00:00:37,579 --> 00:00:41,170
one statement. So even though the second one is SELECT * FROM 

12
00:00:41,170 --> 00:00:44,230
Production.Product WHERE Color equals that color is on two

13
00:00:44,230 --> 00:00:47,690
lines, it&#39;s one single statement. So that&#39;s fine. Now

14
00:00:47,690 --> 00:00:51,469
you could also have multiple statements. And if you do, then you need to wrap them

15
00:00:51,469 --> 00:00:53,149
up in a begin and an end.

16
00:00:53,149 --> 00:00:56,469
So I would typically actually start my blocks,

17
00:00:56,469 --> 00:00:59,870
even with only one statement, I&#39;d tend to 

18
00:00:59,870 --> 00:01:03,710
wrap them up in a begin and an end just if I were to add another line later on, 

19
00:01:03,710 --> 00:01:07,170
it&#39;s wrapped up inside that logic within there.

20
00:01:07,170 --> 00:01:11,140
So it allows us to do, that&#39;s more of that sort of programming idea.

21
00:01:11,140 --> 00:01:15,100
We&#39;re not just doing things always running straight through everything. We&#39;re

22
00:01:15,100 --> 00:01:16,500
saying no, we have a condition,

23
00:01:16,500 --> 00:01:19,780
we have a test. And based on that test, we&#39;re going to

24
00:01:19,780 --> 00:01:26,780
perform some statements. Starting off here I have a very very simple test.

25
00:01:28,460 --> 00:01:32,799
If Yes equals No print True. 

26
00:01:32,799 --> 00:01:37,420
Now, we know that Yes doesn&#39;t equal No, so it doesn&#39;t do anything.

27
00:01:37,420 --> 00:01:41,680
It&#39;s not going to return false. It&#39;s just basically saying I&#39;ve no

28
00:01:41,680 --> 00:01:45,970
alternative for it to do. Obviously if I say

29
00:01:45,970 --> 00:01:52,970
if Yes equals Yes, then our logic evaluates well yes

30
00:01:53,570 --> 00:01:57,430
yes yes equals yes. That&#39;s a lot of yes&#39;s.

31
00:01:57,430 --> 00:02:01,310
And then it evaluates to True and then it will print it out. So there is not an

32
00:02:01,310 --> 00:02:04,710
alternative there. So when we saw it didn&#39;t work, it didn&#39;t print something else.

33
00:02:04,710 --> 00:02:08,869
It&#39;s just said okay, I&#39;m not going to do anything then. And command completed successfully is just

34
00:02:08,869 --> 00:02:10,229
saying that we didn&#39;t have any errors,

35
00:02:10,229 --> 00:02:11,190
and it didn&#39;t

36
00:02:11,190 --> 00:02:15,640
do anything really. So now we&#39;re going to say

37
00:02:15,640 --> 00:02:19,380
something a little bit more complicated. So we&#39;re going to say we&#39;re going to do an 

38
00:02:19,380 --> 00:02:26,170
update in here, and we&#39;re going to update the sales product 

39
00:02:26,170 --> 00:02:29,240
SET DiscontinuedDate equal to the current date

40
00:02:29,240 --> 00:02:33,400
WHERE the ProductID equals 680. Now there&#39;s an if.

41
00:02:33,400 --> 00:02:37,820
So that will do it&#39;s update. If the row count is less than 1, 

42
00:02:37,820 --> 00:02:43,150
i.e. there was zero updates, then I would like to say

43
00:02:43,150 --> 00:02:47,250
PRINT &quot;Product was not found&quot;. Now I&#39;ve wrapped up in a begin and an end, it&#39;s

44
00:02:47,250 --> 00:02:51,390
unnecessary because it&#39;s only one line, but I&#39;d just like to

45
00:02:51,390 --> 00:02:55,320
wrap it up for neatness. And I know if I added another line there it would still be within 

46
00:02:55,320 --> 00:02:56,500
that block, 

47
00:02:56,500 --> 00:03:00,140
of that begin/end block. And then we have an ELSE, so we have an alternative now.

48
00:03:00,140 --> 00:03:04,860
So if it&#39;s, if the reverse logic, if it&#39;s not the case,

49
00:03:04,860 --> 00:03:08,560
then product has been updated. So the true is

50
00:03:08,560 --> 00:03:12,270
the row count was less than 1, i.e. -- you could make the logic the other way around if

51
00:03:12,270 --> 00:03:16,230
you wanted to, you could do a greater than 0 and have the logic reversed -- but essentially in our case

52
00:03:16,230 --> 00:03:19,430
if it&#39;s less than one there was no row, so the product was not found.

53
00:03:19,430 --> 00:03:23,209
The false is actually that there were, so I want to say &quot;Product updated&quot;. 

54
00:03:23,209 --> 00:03:26,700
Let&#39;s have a look at running that. Let&#39;s just get rid of my

55
00:03:26,700 --> 00:03:33,209
results for a second. So we&#39;ll do the update and we&#39;re 

56
00:03:33,209 --> 00:03:37,000
basing this on ProductID 680.

57
00:03:37,000 --> 00:03:41,920
So if I execute that, product was updated.

58
00:03:41,920 --> 00:03:47,660
Let&#39;s try with a different number. So let&#39;s try it with ProductID

59
00:03:47,660 --> 00:03:53,750
1. And if I run with this one

60
00:03:53,750 --> 00:03:56,760
&quot;Product was not found&quot;. So we&#39;ve got

61
00:03:56,760 --> 00:04:00,950
logic which checked to see how many rows were affected.

62
00:04:00,950 --> 00:04:04,670
Actually, after they&#39;d been affected it said how many were there,

63
00:04:04,670 --> 00:04:09,320
were there 0, was there more than 0. I don&#39;t, there is no ProductID 1.

64
00:04:09,320 --> 00:04:11,870
So that&#39;s why it didn&#39;t get updated in that situation.

