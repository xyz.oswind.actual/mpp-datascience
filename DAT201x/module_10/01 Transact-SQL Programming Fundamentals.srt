0
00:00:05,589 --> 00:00:07,700
Well we&#39;ve made it to module 10

1
00:00:07,700 --> 00:00:11,950
on our Querying with Transact-SQL course. We&#39;ve covered a lot of ground so far.

2
00:00:11,950 --> 00:00:15,469
I hope you&#39;ve enjoyed the modules you&#39;ve done so far and you&#39;ve been keeping up with

3
00:00:15,469 --> 00:00:16,209
the labs and

4
00:00:16,209 --> 00:00:20,529
getting your hands on with this stuff. In this module we&#39;re going to start moving

5
00:00:20,529 --> 00:00:23,859
beyond the basic set-based queries that we&#39;ve been

6
00:00:23,859 --> 00:00:28,730
looking at to query tables to get data out or to insert, update, and delete

7
00:00:28,730 --> 00:00:31,929
data. And we&#39;re going to start looking at actually

8
00:00:31,929 --> 00:00:35,670
doing some more extensive programming. Writing some procedural logic

9
00:00:35,670 --> 00:00:39,460
with Transact-SQL. So Jeff you&#39;re going to take the helm on this one,

10
00:00:39,460 --> 00:00:39,890
so

11
00:00:39,890 --> 00:00:43,140
why don&#39;t you tell us what we&#39;re going to cover. Well, well we&#39;re going to, as you said we&#39;re

12
00:00:43,140 --> 00:00:44,370
going to extend

13
00:00:44,370 --> 00:00:48,960
what we can typically do with our set-based queries, we&#39;re adding in some

14
00:00:48,960 --> 00:00:52,179
programming logic, some procedure logic. So we&#39;ve got some

15
00:00:52,179 --> 00:00:55,590
branching and looping. We&#39;ll also look at  batches, comments.

16
00:00:55,590 --> 00:00:58,620
We&#39;ve covered some of these things already. We&#39;ve seen a few comments, we&#39;ve seen some

17
00:00:58,620 --> 00:01:00,579
variables, and we&#39;ve seen some table variables

18
00:01:00,579 --> 00:01:04,430
already, so we&#39;re going to look at those in a bit more detail at how we can pass

19
00:01:04,430 --> 00:01:04,900
values

20
00:01:04,900 --> 00:01:08,340
to and get values from those. We&#39;ll loop through code,

21
00:01:08,340 --> 00:01:12,040
we might want to iterate a number of times through some code. And also we&#39;ll

22
00:01:12,040 --> 00:01:13,750
have a look at stored procedures.

23
00:01:13,750 --> 00:01:16,770
Again, we sort of touched on these things, but we&#39;ll

24
00:01:16,770 --> 00:01:20,030
actually encapsulate our code in a stored procedure at the end

25
00:01:20,030 --> 00:01:20,299
as well.

