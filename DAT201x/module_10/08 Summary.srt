0
00:00:02,090 --> 00:00:05,580
So we have seen a few additional things we can do with SQL to 

1
00:00:05,580 --> 00:00:10,480
add in some programming logic into SQL code. So that Transact-SQL code,

2
00:00:10,480 --> 00:00:15,110
we&#39;ve extended its functionality and we&#39;ve added to it what we can do. So

3
00:00:15,110 --> 00:00:20,300
the use of batches and comments is  fairly straightforward. Comments are really useful

4
00:00:20,300 --> 00:00:23,489
I think to document what you doing. But

5
00:00:23,489 --> 00:00:27,930
then we get into things like variables where we can store values and then reuse them later on.

6
00:00:27,930 --> 00:00:31,130
We&#39;ve got the idea of branching, so IF

7
00:00:31,130 --> 00:00:34,359
and ELSE. And we&#39;ve got the idea looping through code.

8
00:00:34,359 --> 00:00:37,440
And then finally the idea we can encapsulate

9
00:00:37,440 --> 00:00:40,879
some of our logic and call it as a stored procedure. So

10
00:00:40,879 --> 00:00:45,100
it&#39;s a really useful thing. If we have a stored procedure we can do some testing

11
00:00:45,100 --> 00:00:46,960
in there, we can make sure everything is right,

12
00:00:46,960 --> 00:00:51,339
and then rather than just saying oh yep, you could do an INSERT, we say no what you 

13
00:00:51,339 --> 00:00:51,789
do is

14
00:00:51,789 --> 00:00:55,659
you call this stored procedure and you supply the values there. Then I can test what you&#39;re

15
00:00:55,659 --> 00:00:56,260
doing

16
00:00:56,260 --> 00:01:01,269
before it actually goes out runs it against the database to make sure. So stored

17
00:01:01,269 --> 00:01:02,059
procedures are very useful.

18
00:01:02,059 --> 00:01:06,320
And as with views, really useful because we can have security. I can allow you to run a stored

19
00:01:06,320 --> 00:01:07,049
procedure,

20
00:01:07,049 --> 00:01:11,049
but not actually update the table directly. And you can

21
00:01:11,049 --> 00:01:14,590
learn more about stored procedures on some of the other courses that we have 

22
00:01:14,590 --> 00:01:18,079
where we talk about implementing a database. There&#39;s much much more to it

23
00:01:18,079 --> 00:01:18,490
than

24
00:01:18,490 --> 00:01:22,430
we covered in this course. But we did want to kind of show you how to

25
00:01:22,430 --> 00:01:25,020
encapsulate the Transact-SQL logic that you&#39;ve created

26
00:01:25,020 --> 00:01:28,860
and some kind of reusable procedure. So

27
00:01:28,860 --> 00:01:33,009
hopefully once again you&#39;ve found this module useful and we really have

28
00:01:33,009 --> 00:01:36,659
moved a long way since the very first module with our basic SELECT statements

29
00:01:36,659 --> 00:01:37,710
to the point where we&#39;re now

30
00:01:37,710 --> 00:01:42,360
capable of building fairly sophisticated sets of logic, of logical

31
00:01:42,360 --> 00:01:45,759
steps and procedures using Transact-SQL.

32
00:01:45,759 --> 00:01:49,960
Do have a go at the lab for this module. Have a go at

33
00:01:49,960 --> 00:01:53,530
using the various different techniques we&#39;ve talked about in here to write some

34
00:01:53,530 --> 00:01:57,619
some programmatic logic in your Transact-SQL. And when you have

35
00:01:57,619 --> 00:02:00,009
completed the lab and you&#39;re ready to move on,

36
00:02:00,009 --> 00:02:04,509
join us for what will be the final module in this course which is about

37
00:02:04,509 --> 00:02:08,020
error handling and transactions. So look forward to seeing you for that one.

