0
00:00:02,950 --> 00:00:06,700
The next thing we&#39;re going to have a look at is looping. Now we may

1
00:00:06,700 --> 00:00:11,349
decide that we need to run through our code multiple times.

2
00:00:11,349 --> 00:00:16,000
We can base that number of times on a value we type in, we can base it on a variable

3
00:00:16,000 --> 00:00:19,489
that we populated from the value in a table somewhere, so we can get

4
00:00:19,489 --> 00:00:22,939
the value from somewhere. But the idea is looping through.

5
00:00:22,939 --> 00:00:26,310
Typically, we&#39;re doing set-based 

6
00:00:26,310 --> 00:00:30,259
operations in SQL server. So we don&#39;t typically want

7
00:00:30,259 --> 00:00:33,860
to loop through our

8
00:00:33,860 --> 00:00:37,600
data row by row. We want to work on it as a set.

9
00:00:37,600 --> 00:00:42,539
It&#39;s efficient to work on it as a set. We don&#39;t want to go through 1, then row 2, then row 

10
00:00:42,539 --> 00:00:45,579
3, then row 4, then row 5. It&#39;s a slow way of doing things.

11
00:00:45,579 --> 00:00:49,679
So what we&#39;re going to add in is looping logic. But just be wary that

12
00:00:49,679 --> 00:00:56,679
the ideal is to work with sets. So what we&#39;re going to say here in this example, we&#39;ve got 

13
00:00:56,890 --> 00:01:01,989
WHILE @custid, so we&#39;ve declared a customer ID, it&#39;s an integer and it starts off being equal to 1.

14
00:01:01,989 --> 00:01:05,570
So while that is less than or equal to 5, and then we have a

15
00:01:05,570 --> 00:01:09,759
begin and end block, and in that block we&#39;re going to say set

16
00:01:09,759 --> 00:01:14,960
@lname, which we&#39;ve also declared at the beginning, equals lastname from the sales

17
00:01:14,960 --> 00:01:15,689
customer

18
00:01:15,689 --> 00:01:19,990
where the customerid equals that custid value starting off at 1. 

19
00:01:19,990 --> 00:01:24,979
Then printing out the last name, and then iterating through. So we&#39;re going to go through and add one

20
00:01:24,979 --> 00:01:28,520
each time so then it will go through and run it against two, and then run it against three, 

21
00:01:28,520 --> 00:01:31,689
and then eventually it will  get to a point and say

22
00:01:31,689 --> 00:01:36,619
ah, customer ID is not less or equal to, so that condition

23
00:01:36,619 --> 00:01:40,719
won&#39;t -- it will then jump down to after  the end -- that condition won&#39;t evaluate 

24
00:01:40,719 --> 00:01:41,320
to true.

25
00:01:41,320 --> 00:01:48,320
And there it goes, loop will end when the  predicate evaluates to false or unknown.

26
00:01:48,990 --> 00:01:52,859
Is there any other way of ending the loop? I&#39;m mean if I&#39;ve got halfway

27
00:01:52,859 --> 00:01:53,450
through

28
00:01:53,450 --> 00:01:58,159
and then for some reason decided to cancel what I was doing. Absolutely, so if we

29
00:01:58,159 --> 00:02:01,770
maybe have some sort of test in there. So we maybe have a lot of code going on

30
00:02:01,770 --> 00:02:02,850
in there and say well

31
00:02:02,850 --> 00:02:06,210
you&#39;ve been adding 10 percent to our sales values,

32
00:02:06,210 --> 00:02:11,240
and you decided to do that as many times as

33
00:02:11,240 --> 00:02:14,560
there are products in the table,

34
00:02:14,560 --> 00:02:18,019
but once you get to a certain value I want to stop that.

35
00:02:18,019 --> 00:02:21,219
So we can say, let&#39;s put even condition in there. We can have

36
00:02:21,219 --> 00:02:23,030
conditions inside our loops and

37
00:02:23,030 --> 00:02:26,950
so we can nest these things together. And what you have is

38
00:02:26,950 --> 00:02:30,430
two things you can use. You can have BREAK or CONTINUE. So we can basically

39
00:02:30,430 --> 00:02:35,849
break that code which basically -- I want to stop everything and continue on after

40
00:02:35,849 --> 00:02:40,340
that block of code. So we can explicitly do that. So you&#39;re going to put a test in

41
00:02:40,340 --> 00:02:40,750
there.

42
00:02:40,750 --> 00:02:45,980
You wouldn&#39;t just drop that in for no reason. You&#39;d test for something,

43
00:02:45,980 --> 00:02:49,250
if that condition is true or it&#39;s false  potentially,

44
00:02:49,250 --> 00:02:52,919
then you would decide that actually you don&#39;t want to continue your code as you were going -- 

45
00:02:52,919 --> 00:02:56,129
or continue looping rather -- as you go.

46
00:02:56,129 --> 00:03:00,299
Looking at WHILE.

47
00:03:00,299 --> 00:03:03,439
Similar example, we have a counter, so we declare counter.

48
00:03:03,439 --> 00:03:07,030
We set it to 1 to start off with. While it&#39;s

49
00:03:07,030 --> 00:03:10,389
less than 5, let&#39;s put less than or equal to 5 because I want to

50
00:03:10,389 --> 00:03:14,769
say 5 is ok. And we&#39;re going to have a loop. Again

51
00:03:14,769 --> 00:03:18,599
really useful, create a BEGIN/END loop and actually I

52
00:03:18,599 --> 00:03:22,250
like personally -- it doesn&#39;t make any difference to SQL Server --

53
00:03:22,250 --> 00:03:27,060
but for neatness to indent everything in that block. It just makes you aware of everything

54
00:03:27,060 --> 00:03:29,010
that&#39;s running, I can see that code.

55
00:03:29,010 --> 00:03:32,979
Indentation just makes it easier to read the code in

56
00:03:32,979 --> 00:03:36,079
there, it doesn&#39;t make any difference at all to the system 

57
00:03:36,079 --> 00:03:39,530
when it runs it. So what we&#39;re going to do is we&#39;re going to insert

58
00:03:39,530 --> 00:03:43,209
into this demo table a description, that&#39;s all that it requires, 

59
00:03:43,209 --> 00:03:46,940
and we&#39;re going to put in there values, and it will have

60
00:03:46,940 --> 00:03:51,579
the word &quot;Row&quot; and then it will convert that counter into a varchar.

61
00:03:51,579 --> 00:03:56,040
So basically that count is going to be, that value is going to be supplied is going to say 

62
00:03:56,040 --> 00:03:57,590
row one, row two, row three, row four. 

63
00:03:57,590 --> 00:04:00,709
Then it sets it to itself plus one and then

64
00:04:00,709 --> 00:04:04,939
it loops back round because got a WHILE in there, so it&#39;s going to go back

65
00:04:04,939 --> 00:04:08,939
around through as long as that is less or equal to 5, it will keep going.

66
00:04:08,939 --> 00:04:14,159
So if we run that one, make sure we&#39;ve got the declares where it has to be in the same

67
00:04:14,159 --> 00:04:17,699
batch as before. And you can see, now there is a point at -- 

68
00:04:17,699 --> 00:04:21,209
why has it done one row, one row, one row, one row?

69
00:04:21,209 --> 00:04:23,620
Because it&#39;s actually doing

70
00:04:23,620 --> 00:04:27,770
the INSERT five times. It&#39;s not like earlier where we did a set-based operation and you had

71
00:04:27,770 --> 00:04:29,150
one INSERT that inserted

72
00:04:29,150 --> 00:04:32,479
multiple rows. This is actually doing multiple INSERT 

73
00:04:32,479 --> 00:04:36,479
operations, once for each loop. So if we had a set-based operation 

74
00:04:36,479 --> 00:04:40,439
that worked on a thousand rows, would that be the same

75
00:04:40,439 --> 00:04:44,090
as having a loop that did one row a thousand times?

76
00:04:44,090 --> 00:04:47,849
No. If you did a set-based operation and inserted a thousand rows,

77
00:04:47,849 --> 00:04:51,150
it would run once and the message returned would be one thousand rows

78
00:04:51,150 --> 00:04:51,819
affected.

79
00:04:51,819 --> 00:04:55,520
But in terms of performance does it make any difference? In terms of performance, it would be much more

80
00:04:55,520 --> 00:04:58,219
efficient to use the set-based operation than to loop through

81
00:04:58,219 --> 00:05:01,639
a thousand times. Okay. So set-based is  really the way we want to go. So 

82
00:05:01,639 --> 00:05:03,969
we&#39;re not doing this just because we can.

83
00:05:03,969 --> 00:05:07,629
It&#39;s something because we have 

84
00:05:07,629 --> 00:05:11,270
a scenario where we need to be able to loop through, we have to avoid that set-

85
00:05:11,270 --> 00:05:11,900
based way of 

86
00:05:11,900 --> 00:05:15,439
working. But the set-based system gives you the efficiency.

87
00:05:15,439 --> 00:05:18,240
I guess is worth pointing as well, I mean, the loops that we&#39;ve looked at

88
00:05:18,240 --> 00:05:19,710
here were using

89
00:05:19,710 --> 00:05:23,080
a number to indicate the number of times we want to loop through.

90
00:05:23,080 --> 00:05:26,509
Well actually that condition can be anything. It&#39;s any condition that could

91
00:05:26,509 --> 00:05:27,779
be true or false.

92
00:05:27,779 --> 00:05:31,199
So you could actually have a loop where you say WHILE 

93
00:05:31,199 --> 00:05:35,169
true do this. In which case we keep doing it indefinitely

94
00:05:35,169 --> 00:05:38,719
because true is always true. But then somewhere in that loop you might have another 

95
00:05:38,719 --> 00:05:41,180
test the checks for something that calls a break

96
00:05:41,180 --> 00:05:44,270
if a condition happens. So you might have a loop that&#39;s always running in the

97
00:05:44,270 --> 00:05:44,879
background

98
00:05:44,879 --> 00:05:48,189
unless something happens at which point it breaks out. Probably because of

99
00:05:48,189 --> 00:05:51,599
something it&#39;s doing, it&#39;s causing a value  to go out of range or something

100
00:05:51,599 --> 00:05:55,259
is that essentially. Exactly, yeah. So loops can be useful when you

101
00:05:55,259 --> 00:05:58,479
don&#39;t necessarily know how many times you want to try and do something,

102
00:05:58,479 --> 00:06:01,539
you just want to keep trying until a condition means that you stop trying.

103
00:06:01,539 --> 00:06:05,099
So typically you don&#39;t use it for

104
00:06:05,099 --> 00:06:09,099
you know doing set-based operations like inserts or updates or

105
00:06:09,099 --> 00:06:14,849
those type things. But you might use it in a case where perhaps your

106
00:06:14,849 --> 00:06:18,009
checking for a specific time, or your

107
00:06:18,009 --> 00:06:22,029
checking until something happens. You want to keep trying to do something and then stop.

108
00:06:22,029 --> 00:06:25,360
It&#39;s to be honest not very common in Transact-SQL that 

109
00:06:25,360 --> 00:06:29,779
you have those types of operations. Because databases tend to be set-based

110
00:06:29,779 --> 00:06:31,750
tasks. But

111
00:06:31,750 --> 00:06:35,690
there may well be situations where you want iterate an indefinite number of times

112
00:06:35,690 --> 00:06:36,250
until

113
00:06:36,250 --> 00:06:39,300
a particular condition is true and then stop looping.

114
00:06:39,300 --> 00:06:42,470
Yep. So we can&#39;t know how many to loop until we do it 

115
00:06:42,470 --> 00:06:47,200
and then always, certainly not in  the future 

116
00:06:47,200 --> 00:06:50,440
with data that might change, we wouldn&#39;t know, we can&#39;t know until it

117
00:06:50,440 --> 00:06:50,750
does it.

118
00:06:50,750 --> 00:06:54,620
So to just to very quickly just to run that,  so we can see row 1,2,3,4,5.

119
00:06:54,620 --> 00:06:59,880
It looped through 5 times and that you can see it did those 5 loops. Now

120
00:06:59,880 --> 00:07:01,690
you actually can see it before from the rows

121
00:07:01,690 --> 00:07:03,590
that it had done 5 individual rows.

