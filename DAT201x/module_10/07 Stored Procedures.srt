0
00:00:02,540 --> 00:00:05,359
We&#39;re now going to look at something a little bit different which is stored procedures. So

1
00:00:05,359 --> 00:00:05,840
 

2
00:00:05,840 --> 00:00:09,900
we&#39;re encapsulating code into a named

3
00:00:09,900 --> 00:00:12,950
block of code if you like. A named 

4
00:00:12,950 --> 00:00:17,720
object which contains that code. Now we&#39;ve already done views

5
00:00:17,720 --> 00:00:21,919
so this is a little bit different. Now typically we&#39;re using a view

6
00:00:21,919 --> 00:00:25,950
in the same way that we&#39;d use a table. So we have a table 

7
00:00:25,950 --> 00:00:29,480
and a view we use in exactly the same way. We can select from it and we mentioned there are

8
00:00:29,480 --> 00:00:31,230
situations where we can update

9
00:00:31,230 --> 00:00:35,350
and there are certain rules. Now a stored procedure is a little bit different and typically

10
00:00:35,350 --> 00:00:39,920
you can actually use a stored procedure to output results but typically we&#39;re using them 

11
00:00:39,920 --> 00:00:43,469
do a little bit more. So we might be doing some updates,

12
00:00:43,469 --> 00:00:46,239
we might be doing some deletes. Here we actually have an example where we do a

13
00:00:46,239 --> 00:00:47,530
SELECT. So you can do

14
00:00:47,530 --> 00:00:51,839
either of those things. The other thing that&#39;s nice about them is that they&#39;re kind of set up to use

15
00:00:51,839 --> 00:00:57,329
variables or parameters if you like. So you can pass a parameter

16
00:00:57,329 --> 00:01:02,609
into the procedure and then it will run it using that parameter.

17
00:01:02,609 --> 00:01:06,299
They also can have an output parameter. So they can return a value as well. So they

18
00:01:06,299 --> 00:01:07,280
can actually have a

19
00:01:07,280 --> 00:01:10,479
parameter set up and marked as output and then that

20
00:01:10,479 --> 00:01:13,680
is the value they return. So they can return a value

21
00:01:13,680 --> 00:01:20,680
as well as having values going in. Right, we&#39;re going to create a procedure now.

22
00:01:22,579 --> 00:01:27,469
Starting with the first part of this before we get into any of the code.

23
00:01:27,469 --> 00:01:31,579
We&#39;ve got a name, CREATE PROCEDURE, the procedure name, and then you can see in brackets we 

24
00:01:31,579 --> 00:01:32,130
have got

25
00:01:32,130 --> 00:01:36,079
@CategoryID so that&#39;s a variable we&#39;ve seen. So essentially it&#39;s being declared as part 

26
00:01:36,079 --> 00:01:40,240
of the CREATE PROCEDURE. It&#39;s an integer and has a default value of null.

27
00:01:40,240 --> 00:01:43,310
So it&#39;s just the same way of declaring any  variable, 

28
00:01:43,310 --> 00:01:47,719
we&#39;ve given it a default, we&#39;ve given it a data type, we&#39;ve given it a name. Then we have an AS

29
00:01:47,719 --> 00:01:51,680
and then everything after the AS is the code essentially that we&#39;re running.

30
00:01:51,680 --> 00:01:56,340
So we have an IF in here, an IF statement. If the 

31
00:01:56,340 --> 00:01:59,700
CategoryID is null, either because you&#39;ve passed null or

32
00:01:59,700 --> 00:02:04,399
you&#39;ve passed nothing because it defaults to null, then I would like to

33
00:02:04,399 --> 00:02:08,509
SELECT all products in here. We want

34
00:02:08,508 --> 00:02:12,620
the ProductID, Name, Color, Size, ListPrice of all products.

35
00:02:12,620 --> 00:02:16,620
If you supplied a value, so I actually want to create a procedure which has more than one

36
00:02:16,620 --> 00:02:18,310
functions, so it&#39;s a more variable.

37
00:02:18,310 --> 00:02:23,750
If I supply a value in there, then I would like to query the same table,

38
00:02:23,750 --> 00:02:25,180
the Product table, the same

39
00:02:25,180 --> 00:02:28,950
columns but this time looking for a ProductCategoryID

40
00:02:28,950 --> 00:02:32,720
matching the CategoryID that you supplied when you ran

41
00:02:32,720 --> 00:02:36,890
the stored procedure. So if I run all of this, all this will do at them moment is create

42
00:02:36,890 --> 00:02:41,840
this object. It&#39;s not going to actually run any code to

43
00:02:41,840 --> 00:02:45,750
do those selects in there. It&#39;s just creates the object. So all I&#39;m expecting

44
00:02:45,750 --> 00:02:49,070
in here is command completed successfully, and that&#39;s fine.

45
00:02:49,070 --> 00:02:52,180
So to use it, we have an

46
00:02:52,180 --> 00:02:58,570
EXEC statement. Now I&#39;ve seen actually some people run stored procedures

47
00:02:58,570 --> 00:03:01,709
without EXEC. So

48
00:03:01,709 --> 00:03:05,010
why do we need an EXEC statement?

49
00:03:05,010 --> 00:03:08,510
So it&#39;s typically a best practice to have an EXEC or 

50
00:03:08,510 --> 00:03:11,730
EXECUTE is the full version of the thing they both do the same thing.

51
00:03:11,730 --> 00:03:15,860
You don&#39;t have to if it&#39;s the first statement in the batch.

52
00:03:15,860 --> 00:03:19,880
But if there are multiple statements in the batch and one of them is executing a

53
00:03:19,880 --> 00:03:21,830
stored procedure, then you have to explicitly say

54
00:03:21,830 --> 00:03:25,650
EXECUTE or EXEC. So if I had a GO and then a 

55
00:03:25,650 --> 00:03:30,910
stored procedure, it would work. But what if I put another line of code in after the GO?

56
00:03:30,910 --> 00:03:34,850
Yeah, exactly. So typically, to be honest when you&#39;re executing stored

57
00:03:34,850 --> 00:03:38,100
procedures most commonly you&#39;re doing it from a client application.

58
00:03:38,100 --> 00:03:42,010
So the code that actually calls it isn&#39;t Transact-SQL, it&#39;s going to be

59
00:03:42,010 --> 00:03:46,380
C# and I&#39;d be using some sort of object to call the stored procedure.

60
00:03:46,380 --> 00:03:50,810
But if I am calling a stored procedure from some Transact-SQL code,

61
00:03:50,810 --> 00:03:53,880
and of course I could call a stored procedure from within a store procedure,

62
00:03:53,880 --> 00:03:58,660
then best practice is regardless of whether it&#39;s the first statement in the batch

63
00:03:58,660 --> 00:03:59,430
or not,

64
00:03:59,430 --> 00:04:03,660
to use EXEC or EXECUTE to explicitly say I want to execute the stored

65
00:04:03,660 --> 00:04:04,200
procedure.

66
00:04:04,200 --> 00:04:08,430
Okay so we&#39;ve got this one and as we should do we have EXEC at the beginning.

67
00:04:08,430 --> 00:04:12,140
It is optional. We said some things are optional and we don&#39;t bother,

68
00:04:12,140 --> 00:04:15,560
like the UTE at the end of EXECUTE. I never ever type that. 

69
00:04:15,560 --> 00:04:19,769
Three letters I don&#39;t need to type, but the EXEC is optional but it does

70
00:04:19,769 --> 00:04:22,849
it&#39;s useful, it&#39;s clear what you doing as well. I know you&#39;re

71
00:04:22,849 --> 00:04:27,000
running a store procedure because you&#39;ve put EXEC before it. And so here we go, 

72
00:04:27,000 --> 00:04:27,720
let&#39;s run it

73
00:04:27,720 --> 00:04:31,520
against absolutely everything. 295 rows  is all the products we have.

74
00:04:31,520 --> 00:04:35,069
We didn&#39;t pass a parameter. So it did, there was a parameter.

75
00:04:35,069 --> 00:04:39,590
The parameter that was passed was null because that was the default. It wasn&#39;t that there was no parameter,

76
00:04:39,590 --> 00:04:41,949
it was that it defaulted to null. So then we tested for that,

77
00:04:41,949 --> 00:04:44,960
we saw that it was null, and we ran it against

78
00:04:44,960 --> 00:04:48,560
absolutely every row. This time passing the number 6.

79
00:04:48,560 --> 00:04:53,280
Execute that one and we only have 43 rows because we doing it against

80
00:04:53,280 --> 00:04:57,800
the category 6 and so it&#39;s filtering using that category id

81
00:04:57,800 --> 00:04:59,030
because we&#39;ve passed that value in.

