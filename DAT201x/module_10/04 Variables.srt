0
00:00:03,009 --> 00:00:05,190
And variables, so we touched on them before

1
00:00:05,190 --> 00:00:08,690
with our table variables. They can be

2
00:00:08,690 --> 00:00:14,350
lots of other things. So in most cases, I say they&#39;re actually singular scalar values, but we can have

3
00:00:14,350 --> 00:00:15,520
tables, we can have

4
00:00:15,520 --> 00:00:20,190
really anything can be stored in a variable. And we have a DECLARE keyword. So we&#39;re

5
00:00:20,190 --> 00:00:21,690
going to declare a variable,

6
00:00:21,690 --> 00:00:24,960
we&#39;re going to give it a data type and then

7
00:00:24,960 --> 00:00:28,600
the other thing -- oh a question for Graeme -- So what&#39;s going on in there with, 

8
00:00:28,600 --> 00:00:31,949
so we&#39;ve got that color variable, and we&#39;ve got the size variable but they&#39;ve got an

9
00:00:31,949 --> 00:00:32,850
equals to?

10
00:00:32,850 --> 00:00:36,899
Yeah, so in common with many other programming languages,

11
00:00:36,899 --> 00:00:40,870
you can, at the time you declare the variable you can initialize it with 

12
00:00:40,870 --> 00:00:41,540
a value

13
00:00:41,540 --> 00:00:45,520
because a variable is just a placeholder for a value that you want to use.

14
00:00:45,520 --> 00:00:48,390
You don&#39;t have to. You could just declare it and say what data type it is 

15
00:00:48,390 --> 00:00:49,200
and then

16
00:00:49,200 --> 00:00:52,460
use a SET statement later on to actually assign a value to it.

17
00:00:52,460 --> 00:00:56,379
But in this case we&#39;re assigning an  initial value at the time we declare it.

18
00:00:56,379 --> 00:01:01,770
So, we&#39;re not fixing it as Black, Large. No literally as it&#39;s name suggests, it&#39;s 

19
00:01:01,770 --> 00:01:02,410
variable.

20
00:01:02,410 --> 00:01:06,380
The value could be varied. So later on you can assign a different value to it.

21
00:01:06,380 --> 00:01:10,750
Okay. So that&#39;s example there, declare two variables,

22
00:01:10,750 --> 00:01:14,450
color and size. It begins with an @ symbol and then using it

23
00:01:14,450 --> 00:01:19,380
where we would just use, whether it be a string, whether it be a number,

24
00:01:19,380 --> 00:01:23,119
where we would use a value. So we&#39;ve got a SELECT WHERE the color is equal that color, 

25
00:01:23,119 --> 00:01:26,799
size is equal to that size. So we&#39;re going to use it there. But remembering not to put a

26
00:01:26,799 --> 00:01:29,130
GO in between the two because then it would lose 

27
00:01:29,130 --> 00:01:32,159
the variable. Because it would be in a  separate batch.

28
00:01:32,159 --> 00:01:35,560
There we go. So always local to the batch,

29
00:01:35,560 --> 00:01:42,560
go out of scope when the batch ends. Now we&#39;re going to have a look at using a variable. 

30
00:01:44,329 --> 00:01:47,579
So starting out, declaring a variable @City,

31
00:01:47,579 --> 00:01:52,509
giving it a default value of Toronto and also we&#39;ve got a comment in there.

32
00:01:52,509 --> 00:01:55,939
So we&#39;ve got commented out, because I&#39;m 

33
00:01:55,939 --> 00:01:59,409
going to use that little bit later, but you can actually see that won&#39;t run. I can just go

34
00:01:59,409 --> 00:02:02,560
and highlight all of that, and it won&#39;t run. And then we&#39;re going off

35
00:02:02,560 --> 00:02:05,909
and we&#39;re going to go and find information where the city is equal to

36
00:02:05,909 --> 00:02:10,049
that variable. Now when I run this,

37
00:02:10,049 --> 00:02:14,870
deliberate mistake in there. And it&#39;s saying well, what is that City? I don&#39;t understand

38
00:02:14,870 --> 00:02:18,959
what this @City thing is. That&#39;s because we&#39;ve got that GO,

39
00:02:18,959 --> 00:02:22,150
we&#39;ve got two batches going on here, the second batch

40
00:02:22,150 --> 00:02:25,709
has no idea what the @City variable is.

41
00:02:25,709 --> 00:02:29,489
As far as it&#39;s concerned, it doesn&#39;t exist. So if I remove the GO,

42
00:02:29,489 --> 00:02:36,489
then we have one batch, run the same thing again,

43
00:02:36,670 --> 00:02:40,319
and this time we&#39;re getting all of the people in Toronto.

44
00:02:40,319 --> 00:02:43,450
We can change that variable because

45
00:02:43,450 --> 00:02:46,659
we have a SET statement in there. So we can also

46
00:02:46,659 --> 00:02:50,989
explicitly say I&#39;d like that to be any particular value in here.

47
00:02:50,989 --> 00:02:54,780
And if I run it with this one, um a little too much,

48
00:02:54,780 --> 00:02:58,549
then we have Bellevue

49
00:02:58,549 --> 00:03:01,890
selected in there. So we can use separate, so basically we can pass

50
00:03:01,890 --> 00:03:06,269
values into a query in here. And we&#39;ll see at the end of this section when we do

51
00:03:06,269 --> 00:03:11,030
stored procedures that it&#39;s really useful to be able to pass values

52
00:03:11,030 --> 00:03:14,470
into code there. Now a variable 

53
00:03:14,470 --> 00:03:19,000
is an input in that respect but it needs to have a value in it.

54
00:03:19,000 --> 00:03:22,209
So I supplied one by having it typed in. But equally

55
00:03:22,209 --> 00:03:26,940
we can have an output. So we can have a variable which actually receives information.

56
00:03:26,940 --> 00:03:29,549
So here we&#39;ve got DECLARE @Result money

57
00:03:29,549 --> 00:03:32,799
and then SELECT @Result equal to. So

58
00:03:32,799 --> 00:03:37,239
it&#39;s a SELECT statement with a little bit extra in there to say I would like to take the

59
00:03:37,239 --> 00:03:38,090
results

60
00:03:38,090 --> 00:03:41,799
of that SELECT statement and they have to fit, I have to have something that is going to go

61
00:03:41,799 --> 00:03:42,629
in there.

62
00:03:42,629 --> 00:03:46,150
Take the result of that SELECT statement and drop it into

63
00:03:46,150 --> 00:03:49,419
the @Result variable. And that

64
00:03:49,419 --> 00:03:53,109
initially is all that will do. So as it stands

65
00:03:53,109 --> 00:03:57,280
there we can run that, it says completed successfully and that&#39;s all it&#39;s done. It&#39;s put a value

66
00:03:57,280 --> 00:03:58,799
in a variable. It hasn&#39;t done anything with it.

67
00:03:58,799 --> 00:04:02,209
And then I&#39;m very simply going to just print it out on the screen,

68
00:04:02,209 --> 00:04:06,040
again part of the same batch to make sure it works, and then it prints the results there. So we

69
00:04:06,040 --> 00:04:06,889
can see that

70
00:04:06,889 --> 00:04:10,079
I&#39;ve actually taken a value from data,

71
00:04:10,079 --> 00:04:14,489
dropped it into a variable, and in a very simple way in that case used it

72
00:04:14,489 --> 00:04:18,810
to output. But of course we could add that into the previous one and looking at our cities

73
00:04:18,810 --> 00:04:19,430
and so on. So

74
00:04:19,430 --> 00:04:24,020
we can take values from somewhere and supply them to another query somewhere else.

75
00:04:24,020 --> 00:04:27,660
So previously we used SET to assign

76
00:04:27,660 --> 00:04:31,289
a literal value to the variable. When I was assigning a value

77
00:04:31,289 --> 00:04:36,400
we used SET. Effectively I&#39;m assigning a value

78
00:04:36,400 --> 00:04:40,120
to a variable here as well, but the difference is I&#39;m actually getting

79
00:04:40,120 --> 00:04:42,400
that value from the results of a query. So 

80
00:04:42,400 --> 00:04:46,120
it&#39;s SELECT the variable equals. Yes, SELECT. And you can,

81
00:04:46,120 --> 00:04:49,160
and there are other ways -- we can do 

82
00:04:49,160 --> 00:04:52,479
as long as we&#39;ve got the right amount of data.  

83
00:04:52,479 --> 00:04:56,320
I know that that total due is a money data type, I know it&#39;s a

84
00:04:56,320 --> 00:04:57,389
single value.

85
00:04:57,389 --> 00:05:01,470
Because if returned 10 rows

86
00:05:01,470 --> 00:05:04,770
or 2 columns that won&#39;t fit in there. So as long as I know it&#39;s going to fit, 

87
00:05:04,770 --> 00:05:06,360
I can supply that value.

88
00:05:06,360 --> 00:05:09,800
So the type has to be compatible with. Yeah, it&#39;s not just going to put your first row,

89
00:05:09,800 --> 00:05:12,370
first column, it will just fail if it can&#39;t fit

90
00:05:12,370 --> 00:05:15,949
into the variable. Okay. We have seen already, we had table

91
00:05:15,949 --> 00:05:19,099
variables and then we can do a bit more of that with them.

