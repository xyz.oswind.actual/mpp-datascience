0
00:00:02,360 --> 00:00:05,880
Comments. Now comments are really really useful in my opinion.

1
00:00:05,880 --> 00:00:11,160
Myself and Graeme both have gone into companies doing consultancy work and

2
00:00:11,160 --> 00:00:11,789
they say well

3
00:00:11,789 --> 00:00:15,010
my database isn&#39;t working properly, 

4
00:00:15,010 --> 00:00:20,009
here it is, here&#39;s all my scripts that I run against it, here&#39;s my procedures. And you look at

5
00:00:20,009 --> 00:00:20,820
it and you think,

6
00:00:20,820 --> 00:00:24,669
I don&#39;t -- it&#39;s really difficult to work out what&#39;s going on there,

7
00:00:24,669 --> 00:00:28,710
I can&#39;t quite see why you&#39;ve done it. Or the classic is I have no idea why

8
00:00:28,710 --> 00:00:31,189
you&#39;ve done it that way? You seem to have done that wrong. 

9
00:00:31,189 --> 00:00:35,379
So the first thing that we&#39;re going to do is let&#39;s fix that. Then actually you realize, ah

10
00:00:35,379 --> 00:00:39,969
you did it that way because of a reason. It wasn&#39;t actually wrong,

11
00:00:39,969 --> 00:00:43,840
it was just unusual. Now if you had put a comment in there

12
00:00:43,840 --> 00:00:47,960
to say I&#39;ve done this -- anything that&#39;s out of the ordinary or unusual needs

13
00:00:47,960 --> 00:00:48,769
describing --

14
00:00:48,769 --> 00:00:52,960
we can put a comment block in them. There&#39;s two ways of doing it, 

15
00:00:52,960 --> 00:00:57,090
forward slash and then star we can add,

16
00:00:57,090 --> 00:01:01,579
and then everything after that point is a comment block. And it could be all on one line and it could

17
00:01:01,579 --> 00:01:02,960
be on multiple lines. As you see there,

18
00:01:02,960 --> 00:01:07,280
this is a block of commented code. That whole block is commented out and it typically goes

19
00:01:07,280 --> 00:01:08,320
green in the client,

20
00:01:08,320 --> 00:01:12,760
it does depend on the client you&#39;ve got. And the other one is just to put two dashes in

21
00:01:12,760 --> 00:01:16,460
and that will comment out the rest of the line.

22
00:01:16,460 --> 00:01:19,760
So if it&#39;s just a one line, I quite often just use two dashes. But if you

23
00:01:19,760 --> 00:01:22,340
want to have a long description of what you doing, why you&#39;ve done it --

24
00:01:22,340 --> 00:01:25,340
I think that&#39;s the key thing it&#39;s not the

25
00:01:25,340 --> 00:01:30,200
what, it&#39;s the why. In terms of someone else coming in and see it,

26
00:01:30,200 --> 00:01:34,290
if you&#39;ve done anything unusual, go into  why that is. So say

27
00:01:34,290 --> 00:01:37,560
I&#39;ve done this because of this, and then you think alright, ok

28
00:01:37,560 --> 00:01:44,560
I&#39;m not gonna correct that, that&#39;s correct. And as it says, so typically we color code them,

29
00:01:45,160 --> 00:01:49,150
not necessarily the case, but mostly they get color-coded

30
00:01:49,150 --> 00:01:49,900
typically green.

