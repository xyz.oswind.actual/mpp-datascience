0
00:00:02,670 --> 00:00:07,220
So to start off with batches. We&#39;ve  mentioned batches

1
00:00:07,220 --> 00:00:11,420
because of losing values in those

2
00:00:11,420 --> 00:00:15,330
table variables before. As it says there,

3
00:00:15,330 --> 00:00:19,949
they&#39;re sets of commands sent to SQL Server as a unit. So it&#39;s sent,

4
00:00:19,949 --> 00:00:24,239
we talked about carriage returns in our SQL

5
00:00:24,239 --> 00:00:27,390
not really meaning anything. So it could actually be written as one long line.

6
00:00:27,390 --> 00:00:30,769
But when we have a GO, then that batch is sent

7
00:00:30,769 --> 00:00:34,850
and then there&#39;s a GO word, and then the next batch is sent. So they are

8
00:00:34,850 --> 00:00:39,310
definitively separate. They have

9
00:00:39,310 --> 00:00:42,590
functionality in that our variables, 

10
00:00:42,590 --> 00:00:47,520
we declared a variable, we got values out of a variable, we put values in and got values

11
00:00:47,520 --> 00:00:47,900
out,

12
00:00:47,900 --> 00:00:51,410
but it didn&#39;t work when it was either side of a GO. When it was in a different

13
00:00:51,410 --> 00:00:52,100
batch.

14
00:00:52,100 --> 00:00:55,160
So they are relevant to what we doing.

15
00:00:55,160 --> 00:00:58,280
Now, it mentions here that

16
00:00:58,280 --> 00:01:01,750
uses the GO keyword, but it&#39;s not a T-SQL command. 

17
00:01:01,750 --> 00:01:05,850
So this is a little bit odd because it&#39;s in T-SQL, we&#39;ve just typed it in the load of T-SQL 

18
00:01:05,850 --> 00:01:08,040
that we&#39;re doing there, and you&#39;ve got this word GO in the middle.

19
00:01:08,040 --> 00:01:11,400
Actually what happens is that the

20
00:01:11,400 --> 00:01:15,310
client applications, so we&#39;re using  Management Studio a lot, but equally

21
00:01:15,310 --> 00:01:19,610
any of the clients we&#39;ve used in this course, they interpret the GO and they send

22
00:01:19,610 --> 00:01:23,370
the statement before and then they send the next statement after the word GO.

23
00:01:23,370 --> 00:01:27,350
So it&#39;s actually the client that&#39;s doing that, it&#39;s not going to run it

24
00:01:27,350 --> 00:01:31,100
in the back-end database engine. So this is just a

25
00:01:31,100 --> 00:01:34,380
signal to the client that now it&#39;s time to batch up these commands I&#39;ve

26
00:01:34,380 --> 00:01:36,450
given you, and send them to the server to be executed.

27
00:01:36,450 --> 00:01:40,540
Yeah, we&#39;ll hit GO right and send everything we&#39;ve got so far. Okay. And then the next bit. 

28
00:01:40,540 --> 00:01:42,690
So that -- and actually when you

29
00:01:42,690 --> 00:01:46,770
see that logic, it makes sense how those variables don&#39;t carry on through because

30
00:01:46,770 --> 00:01:47,950
they&#39;re sent as separate things.

