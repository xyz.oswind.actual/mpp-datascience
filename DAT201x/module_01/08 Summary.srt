0
00:00:02,190 --> 00:00:05,150
So that&#39;s the end to the first module, we&#39;ve covered a fair amount of

1
00:00:05,150 --> 00:00:05,799
ground.

2
00:00:05,799 --> 00:00:09,700
We started off with our basic SELECT statement having introduced

3
00:00:09,700 --> 00:00:13,010
what Transact-SQL is and a little bit of the context. We started with the basic

4
00:00:13,010 --> 00:00:15,190
SELECT statement, we looked at how to select 

5
00:00:15,190 --> 00:00:19,740
all of the rows, we looked at selecting specific columns, using aliases to name those

6
00:00:19,740 --> 00:00:23,119
columns, using expressions to calculate values that get returned,

7
00:00:23,119 --> 00:00:26,570
and then dealing with different data types where I might convert 

8
00:00:26,570 --> 00:00:30,060
strings to numbers, numbers to strings, dates to strings, that type of thing,

9
00:00:30,060 --> 00:00:34,379
and also dealing with values that are null, that are unknown and how we 

10
00:00:34,379 --> 00:00:35,010
deal with that.

11
00:00:35,010 --> 00:00:38,660
So there&#39;s a lot of ground that we&#39;ve covered. What I would suggest is

12
00:00:38,660 --> 00:00:44,030
go and do the lab, and in that lab, you can download the lab

13
00:00:44,030 --> 00:00:45,719
instructions, you can set up your

14
00:00:45,719 --> 00:00:49,090
databases, there&#39;s instructions on setting up the database in Azure SQL Database.

15
00:00:49,090 --> 00:00:52,309
Use the lab to go and try out some of these things,

16
00:00:52,309 --> 00:00:56,050
there&#39;s some challenges in there that will make you think how might you get the

17
00:00:56,050 --> 00:00:57,519
results that we&#39;re asking you to get

18
00:00:57,519 --> 00:01:00,440
and there some solution files that you can use to check your answers and

19
00:01:00,440 --> 00:01:01,129
compare

20
00:01:01,129 --> 00:01:05,269
with what we&#39;ve produced. So hopefully you&#39;ll enjoy doing that and

21
00:01:05,269 --> 00:01:08,880
when you&#39;re comfortable with that basic SELECT syntax, you&#39;ve done the lab and your happy with

22
00:01:08,880 --> 00:01:09,270
that,

23
00:01:09,270 --> 00:01:12,680
come back and join us for Module 2 and we&#39;ll explore a little bit more

24
00:01:12,680 --> 00:01:14,630
about using SELECT in Transact-SQL.

