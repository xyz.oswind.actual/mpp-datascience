0
00:00:02,350 --> 00:00:07,220
So we talked about working with data types and one of the things that we mentioned

1
00:00:07,220 --> 00:00:09,650
earlier on was nulls. We talked about the idea that a

2
00:00:09,650 --> 00:00:13,469
null value means unknown or  missing. It doesn&#39;t mean

3
00:00:13,469 --> 00:00:17,080
an empty string, it doesn&#39;t mean nothing or zero. It means we don&#39;t know

4
00:00:17,080 --> 00:00:22,460
what the value this thing is. So it represents a missing or an unknown value.

5
00:00:22,460 --> 00:00:27,050
The ANSI behavior, the American National Standards Institute, has a behavior.

6
00:00:27,050 --> 00:00:29,130
Because this is ambiguous, different systems

7
00:00:29,130 --> 00:00:33,260
originally all treated nulls different ways. So there is a standard

8
00:00:33,260 --> 00:00:37,020
for database systems that says look if a value is null, this is the way you should

9
00:00:37,020 --> 00:00:37,620
treat it.

10
00:00:37,620 --> 00:00:41,630
The result of any expression that includes that null should

11
00:00:41,630 --> 00:00:45,370
result in null. Because if you try and add something to something that&#39;s unknown,

12
00:00:45,370 --> 00:00:50,450
the answer is unknown. Now you can actually switch that behavior off in SQL

13
00:00:50,450 --> 00:00:51,050
Server.

14
00:00:51,050 --> 00:00:54,810
There is an option to specify

15
00:00:54,810 --> 00:00:58,890
that you want explicitly nulls to mean zero  or empty spaces.

16
00:00:58,890 --> 00:01:02,830
Really recommend you don&#39;t do that. Keep the standard approach

17
00:01:02,830 --> 00:01:07,120
switched on. So that if you try and add numbers you get null,

18
00:01:07,120 --> 00:01:11,670
if you try and concatenate strings to null, you get null. The same is true if you try and

19
00:01:11,670 --> 00:01:12,490
compare

20
00:01:12,490 --> 00:01:16,310
values are null. And if you think about this, it is logical. If

21
00:01:16,310 --> 00:01:19,320
I don&#39;t know your middle name and I don&#39;t know

22
00:01:19,320 --> 00:01:23,240
someone else&#39;s middle name, I can&#39;t check to see if your name&#39;s the same as their

23
00:01:23,240 --> 00:01:26,620
middle name. It might be for all I know but might not be. So the answer is unknown.

24
00:01:26,620 --> 00:01:31,880
So when you compare null to null, when you check is that null equal to that null, well

25
00:01:31,880 --> 00:01:32,810
the answer is null,

26
00:01:32,810 --> 00:01:35,960
We don&#39;t know or the answer is actually false, it

27
00:01:35,960 --> 00:01:38,979
isn&#39;t equal because we can&#39;t determine whether null is null.

28
00:01:38,979 --> 00:01:43,680
There is a way of checking for that. That sounds really confusing. How do I check to

29
00:01:43,680 --> 00:01:44,720
see if something is null?

30
00:01:44,720 --> 00:01:48,350
The answer is you check -- is it null? Right, so

31
00:01:48,350 --> 00:01:52,330
if you have a table that you&#39;re trying to query and you want to bring back the rows

32
00:01:52,330 --> 00:01:52,790
were

33
00:01:52,790 --> 00:01:56,290
the price is null, you don&#39;t check for price equals null, you check for price

34
00:01:56,290 --> 00:01:59,460
is null. And that will then bring back the rows that have

35
00:01:59,460 --> 00:02:04,270
a null value in that column. So nulls can be confusing, it&#39;s a

36
00:02:04,270 --> 00:02:07,850
potential source of confusion. I&#39;m sure you&#39;ve come across this. Well, I

37
00:02:07,850 --> 00:02:11,200
often find there are way too many a database.

38
00:02:11,200 --> 00:02:14,279
For example, I have a very short address because

39
00:02:14,279 --> 00:02:15,459
I

40
00:02:15,459 --> 00:02:19,640
live in a village that doesn&#39;t have street names. So I technically don&#39;t have

41
00:02:19,640 --> 00:02:25,099
a street name, there&#39;s a number and the village. And so there&#39;s lot of empty fields

42
00:02:25,099 --> 00:02:25,569
in

43
00:02:25,569 --> 00:02:29,180
that address. So what you&#39;re saying is

44
00:02:29,180 --> 00:02:34,299
that you know what they are, they&#39;re nothing. So there&#39;s a difference between them being empty

45
00:02:34,299 --> 00:02:37,760
and them being null. Right. And that&#39;s

46
00:02:37,760 --> 00:02:41,310
where the confusion lies I think. Because I think a lot of databases have those

47
00:02:41,310 --> 00:02:42,230
in as null

48
00:02:42,230 --> 00:02:46,120
because no one typed anything in. And it&#39;s just saying well ok, you

49
00:02:46,120 --> 00:02:47,459
didn&#39;t type anything in, so it&#39;s unknown.

50
00:02:47,459 --> 00:02:51,980
It&#39;s not actually unknown, it&#39;s known to be nothing.

51
00:02:51,980 --> 00:02:55,230
You know we can actually put in two speech marks &lt;two single quotation marks&gt; and

52
00:02:55,230 --> 00:02:58,439
even in the text, obviously you can enter a zero,

53
00:02:58,439 --> 00:03:01,700
my bonus this year &lt;laughter&gt;,

54
00:03:01,700 --> 00:03:05,810
or you could enter two speech marks &lt;two single quotation marks&gt; for a text string

55
00:03:05,810 --> 00:03:09,439
and that&#39;s saying I know what it is, it&#39;s nothing and that&#39;s different to

56
00:03:09,439 --> 00:03:13,109
null. So yeah, it is potentially confusing

57
00:03:13,109 --> 00:03:16,819
and when you&#39;re querying a database it becomes important to

58
00:03:16,819 --> 00:03:17,370
handle

59
00:03:17,370 --> 00:03:20,489
situations where there&#39;s null. And to help us do that

60
00:03:20,489 --> 00:03:23,359
Transact-SQL includes a number of functions, just like there were functions to

61
00:03:23,359 --> 00:03:23,930
convert

62
00:03:23,930 --> 00:03:28,349
data types, values between different data types. There are functions that help us

63
00:03:28,349 --> 00:03:33,799
handle nulls. So there&#39;s a function, the first one ISNULL. Now I mentioned the

64
00:03:33,799 --> 00:03:35,150
syntax earlier on, again a

65
00:03:35,150 --> 00:03:38,579
source confusion. I mentioned it earlier on, when you&#39;re comparing nulls

66
00:03:38,579 --> 00:03:41,819
you check for is space null. Your checking

67
00:03:41,819 --> 00:03:45,139
is this thing null. The ISNULL function is not the same thing,

68
00:03:45,139 --> 00:03:49,620
there&#39;s no space in ISNULL. And what I can do is say ISNULL

69
00:03:49,620 --> 00:03:52,829
a particular column, and if it is null,

70
00:03:52,829 --> 00:03:57,439
then return a particular value. So classically what I might do is check for

71
00:03:57,439 --> 00:04:02,639
ISNULL address line 2, and if the address line 2 is null return an empty

72
00:04:02,639 --> 00:04:03,220
string.

73
00:04:03,220 --> 00:04:06,430
Or ISNULL discount,

74
00:04:06,430 --> 00:04:10,669
return 0 because I&#39;m assuming if there&#39;s no discount there then it has to be 0.

75
00:04:10,669 --> 00:04:13,799
So you can specify the value you want to use

76
00:04:13,799 --> 00:04:19,070
if the column or the variable happens to be null. And kind of the opposite way around from that,

77
00:04:19,070 --> 00:04:23,460
coming at it from the other end, I can check for specific value, if I find that

78
00:04:23,460 --> 00:04:24,130
value

79
00:04:24,130 --> 00:04:26,530
say well use null instead.

80
00:04:26,530 --> 00:04:30,500
So what I might do is check for hey if the address is an empty string

81
00:04:30,500 --> 00:04:34,380
return a null, if the value is zero return a null. More likely it is going

82
00:04:34,380 --> 00:04:35,570
to be things like

83
00:04:35,570 --> 00:04:39,170
if I&#39;ve got a color field, we&#39;ll see this in a little while in the demo,

84
00:04:39,170 --> 00:04:43,440
if I&#39;ve got a color field and it&#39;s multi, I want to treat that as null because

85
00:04:43,440 --> 00:04:46,560
for the purposes of what I&#39;m doing that&#39;s confusing, so let&#39;s just return it as null.

86
00:04:46,560 --> 00:04:52,390
And then there&#39;s the slightly more esoteric case called COALESCE.

87
00:04:52,390 --> 00:04:56,830
And Jeff, do you want to talk us through COALESC? Well, you&#39;re going sometimes get

88
00:04:56,830 --> 00:04:59,620
situations where you&#39;ve got multiple different columns

89
00:04:59,620 --> 00:05:04,900
and some of them will contain nulls, and you&#39;ve got an order. And you say I&#39;d like to display the

90
00:05:04,900 --> 00:05:09,270
first column, but if it is null, then I&#39;ll fall back to the second and maybe third,

91
00:05:09,270 --> 00:05:14,530
forth column. So we had an example where we talking about

92
00:05:14,530 --> 00:05:18,470
email addresses and you say I&#39;d like to contact someone by email address.

93
00:05:18,470 --> 00:05:22,620
I don&#39;t have everyone&#39;s email address. Now that&#39;s a classic

94
00:05:22,620 --> 00:05:26,100
correct use of null because almost certainly nowadays everyone does have an

95
00:05:26,100 --> 00:05:28,290
email address, you just don&#39;t know what it is. It&#39;s not like it

96
00:05:28,290 --> 00:05:32,410
doesn&#39;t exist, like my middle name, you don&#39;t know what it is, and I&#39;m not telling you. I will find out. &lt;laughter&gt;

97
00:05:32,410 --> 00:05:37,180
So if we don&#39;t have their email address,

98
00:05:37,180 --> 00:05:40,530
which is null because we don&#39;t have it, we would like to display their telephone

99
00:05:40,530 --> 00:05:41,880
number. Now maybe I don&#39;t have

100
00:05:41,880 --> 00:05:45,400
their telephone number either in which case I&#39;d like to display their cell phone

101
00:05:45,400 --> 00:05:45,960
number.

102
00:05:45,960 --> 00:05:51,180
And you could carry on and on and on, but ultimately saying OK let&#39;s find

103
00:05:51,180 --> 00:05:54,550
the best case scenario value and display that.

104
00:05:54,550 --> 00:05:59,340
If ultimately they&#39;re all null, we&#39;ll still display a null.

105
00:05:59,340 --> 00:06:02,840
Hopefully we&#39;ll have some means of communicating. So the case may be a

106
00:06:02,840 --> 00:06:04,500
classic address book where you&#39;ve got,

107
00:06:04,500 --> 00:06:08,760
I&#39;m sure we&#39;ve all seen this, actually you may not have seen an address book, but on your phone

108
00:06:08,760 --> 00:06:12,560
I&#39;m sure there&#39;s an address book there, where you have multiple fields for phone numbers. Did that used to actually be a book?

109
00:06:12,560 --> 00:06:13,750
Yeah, it

110
00:06:13,750 --> 00:06:17,870
used to be a paper book and you&#39;d to score things out and replacement them, yeah. &lt;laughter&gt;

111
00:06:17,870 --> 00:06:22,230
But in a cell phone system, you might have multiple telephone number fields for

112
00:06:22,230 --> 00:06:24,840
home number, work number, mobile number,

113
00:06:24,840 --> 00:06:28,550
you know emergency number, whatever it might be. And what we might be doing in

114
00:06:28,550 --> 00:06:30,570
this case is saying, alright find the first one

115
00:06:30,570 --> 00:06:34,650
in this order. If I have a home number, I want that, if I don&#39;t try

116
00:06:34,650 --> 00:06:35,960
the cell phone number,

117
00:06:35,960 --> 00:06:39,410
if I don&#39;t have that, try the work phone number and bring back whichever value you find first.

118
00:06:39,410 --> 00:06:44,099
So COALESCE is a function that lets me specify multiple columns

119
00:06:44,099 --> 00:06:48,030
and try to find the first not null one. And if they are all null, I return null.

