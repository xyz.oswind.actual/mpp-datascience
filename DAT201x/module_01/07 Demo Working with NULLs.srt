0
00:00:02,060 --> 00:00:05,790
Let&#39;s have a look at some examples of working with NULL values.

1
00:00:05,790 --> 00:00:10,050
I&#39;m going to stay in my SQL

2
00:00:10,050 --> 00:00:13,180
Server Management interface that I have here, but I&#39;m gonna open up another

3
00:00:13,180 --> 00:00:14,270
script file here.

4
00:00:14,270 --> 00:00:21,270
And the first thing I&#39;m going to do is I&#39;m going say well, I&#39;ve got this idea of a Size.

5
00:00:21,340 --> 00:00:24,650
And you might remember earlier on, and in fact we&#39;ve still got

6
00:00:24,650 --> 00:00:27,940
query I think in here where we CAST the

7
00:00:27,940 --> 00:00:32,559
Size as a number and actually the results are there.

8
00:00:32,558 --> 00:00:36,480
So some of the sizes were numeric so I was able to cast them.

9
00:00:36,480 --> 00:00:39,760
But the ones that weren&#39;t come back as null because

10
00:00:39,760 --> 00:00:44,410
obviously we don&#39;t know what those values are. So what we gonna do this case is we&#39;re going

11
00:00:44,410 --> 00:00:48,239
slightly extend that, we&#39;re going to say well, TRY_CAST the number as

12
00:00:48,239 --> 00:00:51,500
an Integer. If you can&#39;t do that,

13
00:00:51,500 --> 00:00:54,870
if the value itself isn&#39;t a number, if the size isn&#39;t a number

14
00:00:54,870 --> 00:00:58,149
that will return a null. The TRY_CAST will return null. 

15
00:00:58,149 --> 00:01:01,420
If it does return null, then use 0

16
00:01:01,420 --> 00:01:04,670
is what we&#39;re saying here. So, we&#39;ve got a function within a function.

17
00:01:04,670 --> 00:01:10,990
And we&#39;ll just run the app and this time it brings back the size, where it is a

18
00:01:10,990 --> 00:01:12,080
number brings by the size.

19
00:01:12,080 --> 00:01:15,900
Where it was either a string or it was null or whatever it might be

20
00:01:15,900 --> 00:01:19,400
it&#39;s brought back a 0. So I haven&#39;t got any nulls in my results here because

21
00:01:19,400 --> 00:01:24,049
where I had nulls, I&#39;ve converted them to zeros. And similarly, I could do the same with

22
00:01:24,049 --> 00:01:26,130
strings, in this case I&#39;m checking for Color.

23
00:01:26,130 --> 00:01:29,580
And I&#39;m using this because I want to concatenate the Color and the Size

24
00:01:29,580 --> 00:01:30,200
together.

25
00:01:30,200 --> 00:01:34,860
And in a previous demo we concatenated Color and Size and

26
00:01:34,860 --> 00:01:38,350
either one of them was null, remember that any expression that uses 

27
00:01:38,350 --> 00:01:41,729
one null value, the result is always null. Even though we knew

28
00:01:41,729 --> 00:01:45,579
what the size was in some cases, or we knew what the color was.

29
00:01:45,579 --> 00:01:48,930
What we&#39;re doing here is checking to see if it&#39;s null and if it is

30
00:01:48,930 --> 00:01:54,000
make it an empty string, which can be concatenated. So if we then try that

31
00:01:54,000 --> 00:01:59,490
what we&#39;ll find is even in cases where I&#39;ve got one but not the other

32
00:01:59,490 --> 00:02:02,440
I still get the value, we can worry about the trailing commas

33
00:02:02,440 --> 00:02:05,530
a bit later. This is just a detail at the moment have. I wouldn&#39;t have picked that up.

34
00:02:05,530 --> 00:02:10,650
Ah well, there you go. But it&#39;s able to check to see if

35
00:02:10,650 --> 00:02:13,950
the color is null then convert it to an empty string. So I&#39;ve worked with both

36
00:02:13,950 --> 00:02:15,950
numbers and with strings using that, and

37
00:02:15,950 --> 00:02:20,780
in fact any data type. So the other thing I can do is the other way around. I can use

38
00:02:20,780 --> 00:02:21,170
NULLIF

39
00:02:21,170 --> 00:02:24,760
function and in this case I&#39;ve got Multi color sometimes as the color

40
00:02:24,760 --> 00:02:25,700
that&#39;s specified.

41
00:02:25,700 --> 00:02:29,110
If I actually look there you can see one there is a Multi color product.

42
00:02:29,110 --> 00:02:32,850
But perhaps I want to display color chart, I don&#39;t really want to have a

43
00:02:32,850 --> 00:02:33,560
square for

44
00:02:33,560 --> 00:02:36,850
Multi color. So I&#39;ll ignore that and just you know

45
00:02:36,850 --> 00:02:41,040
treat that as null. So I&#39;m saying make it null if the Color is Multi.

46
00:02:41,040 --> 00:02:46,190
And sure enough it then comes back with null for that value. The other ones

47
00:02:46,190 --> 00:02:51,490
it has the color, if it was Multi it comes back with null. And you could even then covert those.

48
00:02:51,490 --> 00:02:54,910
Yes, I could do an ISNULL around those and then compare it to something else.

49
00:02:54,910 --> 00:02:59,290
So the other thing we talked about was this

50
00:02:59,290 --> 00:03:02,760
COALESCE function where I can

51
00:03:02,760 --> 00:03:07,790
take multiple columns and find the first one that&#39;s not null.

52
00:03:07,790 --> 00:03:08,300
And there&#39;s actually

53
00:03:08,300 --> 00:03:11,730
three dates in that product table that I care about.

54
00:03:11,730 --> 00:03:15,820
There&#39;s potentially a date that the product was discontinued. If it hasn&#39;t been

55
00:03:15,820 --> 00:03:18,420
discontinued, if it&#39;s still an active product, that will be null.

56
00:03:18,420 --> 00:03:23,260
But there might well be a date where I stop selling, I haven&#39;t discontinued it but I&#39;ve stopped

57
00:03:23,260 --> 00:03:26,620
selling at particular date, I might restart again which is why I haven&#39;t discontinued it.

58
00:03:26,620 --> 00:03:29,780
So I might check for the SellEndDate and if that&#39;s null

59
00:03:29,780 --> 00:03:33,769
then I probably am still selling it. In  which case there should be a SellStartDate,

60
00:03:33,769 --> 00:03:35,440
the date that I started selling it.

61
00:03:35,440 --> 00:03:38,940
So I can go and check for that and COALESCE those values together.

62
00:03:38,940 --> 00:03:42,340
And I what I get is a date, for every 

63
00:03:42,340 --> 00:03:46,070
row that&#39;s in there I get a date.

64
00:03:46,070 --> 00:03:49,000
Now, the problem is of course I don&#39;t have any idea which of these things &lt;which date&gt; it

65
00:03:49,000 --> 00:03:49,459
is.

66
00:03:49,459 --> 00:03:53,290
So I might want to make the function a little more sophisticated and indicate

67
00:03:53,290 --> 00:03:53,900
some how.

68
00:03:53,900 --> 00:03:58,690
But we can see how that COALESCE function works. Now one thing we didn&#39;t

69
00:03:58,690 --> 00:04:00,610
really talk about on the slides, but I want to

70
00:04:00,610 --> 00:04:03,660
show you in the demo -- the functions that we&#39;ve used

71
00:04:03,660 --> 00:04:06,980
up til now have been functions that work with nulls.

72
00:04:06,980 --> 00:04:10,239
We can use another

73
00:04:10,239 --> 00:04:12,470
type of expression, another clause in our

74
00:04:12,470 --> 00:04:16,070
Transact-SQL called the CASE clause. And CASE is used

75
00:04:16,070 --> 00:04:20,780
to have a column where the value depends on some logic that I want to work out.

76
00:04:20,779 --> 00:04:24,840
So what I&#39;m doing here is an example of something called a searched case. I&#39;m saying bring back the

77
00:04:24,840 --> 00:04:25,380
name.

78
00:04:25,380 --> 00:04:28,440
And then for the second column, which we&#39;re calling 

79
00:04:28,440 --> 00:04:31,590
AS SaleStatus, we&#39;re going to use this CASE expression to

80
00:04:31,590 --> 00:04:35,770
to figure out what should go in this column. And what I&#39;m saying is, in cases

81
00:04:35,770 --> 00:04:36,680
where the SaleEndDate

82
00:04:36,680 --> 00:04:40,260
is null, then put &#39;On Sale&#39;.

83
00:04:40,260 --> 00:04:43,920
So we&#39;re going to return a string column in this case where we&#39;re saying

84
00:04:43,920 --> 00:04:48,020
if the sell end date is null, then we haven&#39;t stopped selling it so it&#39;s on

85
00:04:48,020 --> 00:04:48,470
sale.

86
00:04:48,470 --> 00:04:51,660
Otherwise if the SellEndEate isn&#39;t null,

87
00:04:51,660 --> 00:04:55,420
ELSE, then returned &#39;Discontinued&#39;. So

88
00:04:55,420 --> 00:04:58,630
now I&#39;m getting, rather that COALESCE thing where I could see the dates, but I didn&#39;t 

89
00:04:58,630 --> 00:05:00,060
really know which dates they were,

90
00:05:00,060 --> 00:05:03,420
I&#39;m using CASE to actually make some logical sense of what I&#39;m seeing in those dates.

91
00:05:03,420 --> 00:05:05,240
And you could combine the two.

92
00:05:05,240 --> 00:05:08,590
Yeah. And then actually have a date and you would know what that date is rather than

93
00:05:08,590 --> 00:05:09,650
just being a date that

94
00:05:09,650 --> 00:05:13,010
could mean anything. Exactly, so if I run that,

95
00:05:13,010 --> 00:05:16,150
we can actually see the results that come back. Some products are on sale

96
00:05:16,150 --> 00:05:16,830
because the

97
00:05:16,830 --> 00:05:20,720
the SellEndDate was null. Whereas other ones the SellEndDate wasn&#39;t null,

98
00:05:20,720 --> 00:05:22,970
and those ones are discontinued. So we can actually see

99
00:05:22,970 --> 00:05:26,780
that logic. Now that searched case, there

100
00:05:26,780 --> 00:05:30,760
two variants of the CASE statement. You&#39;ll notice the way that is worded, it is CASE

101
00:05:30,760 --> 00:05:34,650
and then WHEN and I check in the WHEN clause what it is I want to check for.

102
00:05:34,650 --> 00:05:37,790
There is another example of CASE, there&#39;s a simple CASE.

103
00:05:37,790 --> 00:05:42,000
And I just wanna cover it just for completeness. There&#39;s a CASE statement where

104
00:05:42,000 --> 00:05:42,740
I can say

105
00:05:42,740 --> 00:05:45,919
CASE and then a specific column or variable. So in this case

106
00:05:45,919 --> 00:05:49,900
look at the Size column and when it&#39;s &#39;S&#39;

107
00:05:49,900 --> 00:05:53,990
then return &#39;Small&#39;; when it&#39;s &#39;M&#39; then return &#39;Medium&#39;; when it&#39;s &#39;L&#39; return &#39;Large&#39;;

108
00:05:53,990 --> 00:05:55,000
&#39;XL&#39; &#39;Extra-Large&#39;;

109
00:05:55,000 --> 00:05:58,040
otherwise, return

110
00:05:58,040 --> 00:06:01,650
if it&#39;s null, sorry, 

111
00:06:01,650 --> 00:06:04,910
if it&#39;s not null return it, but otherwise return &#39;n/a&#39;.

112
00:06:04,910 --> 00:06:09,479
Alright, so I&#39;m going to return, some of my sizes are numbers, so I&#39;ll return the number.

113
00:06:09,479 --> 00:06:12,820
And if I haven&#39;t got anything, if it&#39;s null, then return not applicable --

114
00:06:12,820 --> 00:06:18,060
size isn&#39;t applicable to this particular product. Like a spanner for example.

115
00:06:18,060 --> 00:06:21,860
And return all of that as ProductSize. So it is a bit of a more,

116
00:06:21,860 --> 00:06:25,430
although it is a simple CASE expression, there&#39;s more options going on in this one because I

117
00:06:25,430 --> 00:06:26,130
added more

118
00:06:26,130 --> 00:06:29,880
WHEN causes, and as a catch all ELSE at the bottom.

119
00:06:29,880 --> 00:06:35,040
If I run that, the results which I&#39;ll just bring up so we can see them a bit more

120
00:06:35,040 --> 00:06:35,590
clearly.

121
00:06:35,590 --> 00:06:39,420
You can see that where it was a number this was the CASE

122
00:06:39,420 --> 00:06:43,230
ELSE, so it wasn&#39;t any of these things, CASE ELSE.

123
00:06:43,230 --> 00:06:47,000
However, it wasn&#39;t null, so we return what actually is. So that&#39;s the value that&#39;s in

124
00:06:47,000 --> 00:06:47,570
the column,

125
00:06:47,570 --> 00:06:53,850
58. In this case it wasn&#39;t S, M, L or XL, it must have been null in that case so we returned n/a;

126
00:06:53,850 --> 00:06:58,070
and this case it must have been M so we returned Medium. So you can kinda see the logic

127
00:06:58,070 --> 00:06:58,600
of how 

128
00:06:58,600 --> 00:07:01,620
it&#39;s worked through that simple CASE  expression.

129
00:07:01,620 --> 00:07:04,630
So what we&#39;ve seen here 

130
00:07:04,630 --> 00:07:08,130
is worth emphasizing the CASE expression is not explicitly designed just to deal

131
00:07:08,130 --> 00:07:09,320
with things that might be null,

132
00:07:09,320 --> 00:07:12,710
but it turns out to be very useful when you&#39;re checking to see if something&#39;s

133
00:07:12,710 --> 00:07:14,550
null to use that CASE expression to

134
00:07:14,550 --> 00:07:18,030
say well if it&#39;s this then do something else, if it&#39;s that then do something else

135
00:07:18,030 --> 00:07:18,870
and if it&#39;s null, then

136
00:07:18,870 --> 00:07:23,090
do this. And that&#39;s why we&#39;ve kinda included it at this part the course.

137
00:07:23,090 --> 00:07:26,120
I think if you&#39;re getting lots of null

138
00:07:26,120 --> 00:07:30,600
conversion, then it might point towards the database design sometimes 

139
00:07:30,600 --> 00:07:34,710
may be needing a bit of work that we shouldn&#39;t have all these nulls.

140
00:07:34,710 --> 00:07:38,410
Yeah, exactly. If your database developer and you&#39;re finding these types

141
00:07:38,410 --> 00:07:41,100
queries are common, then yeah you&#39;ve probably got too many nulls in the

142
00:07:41,100 --> 00:07:42,080
database design.

143
00:07:42,080 --> 00:07:46,090
Sometimes though, however, your creating queries to create reports

144
00:07:46,090 --> 00:07:49,230
you have no control over the design the database so you just have to

145
00:07:49,230 --> 00:07:49,650
deal with it.

