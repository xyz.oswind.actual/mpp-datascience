0
00:00:06,091 --> 00:00:09,691
So let&#39;s take a look at using pivots to pivot some data.

1
00:00:09,691 --> 00:00:15,051
And what I&#39;m doing is I&#39;m querying my AdventureWorksLT database where I&#39;ve got 

2
00:00:15,051 --> 00:00:17,778
a number of products which are categorized into different categories

3
00:00:17,979 --> 00:00:23,179
and those products may or may not be colored. So if they are, if they have a color,

4
00:00:25,088 --> 00:00:29,888
if they&#39;re red or blue or yellow or whatever, I want to see what the color is. 

5
00:00:29,888 --> 00:00:31,310
If there is no color, then it&#39;s null. So I want to see it as uncolored.

6
00:00:31,310 --> 00:00:35,213
So I&#39;m effectively just seeing which color my different products are and which categories they are in.

7
00:00:35,435 --> 00:00:41,115
And that&#39;s defined in this subquery here which creates a derived table effectively.

8
00:00:43,485 --> 00:00:50,045
So if I go ahead and run that, I can see for each ProductID, what category it is in and what color it is.

9
00:00:50,045 --> 00:00:55,710
So I can get across here various different products that are Mountain Bikes that are Silver,

10
00:00:55,710 --> 00:01:00,475
and some of them are Mountain Bikes that are Black, and so on through the different colors.

11
00:01:00,475 --> 00:01:05,106
So, well that&#39;s pretty useful. But what I want to do is actually count them up. I want to know how many

12
00:01:05,105 --> 00:01:09,426
Mountain Bikes do I have that are Yellow, and how many Mountain Bikes do I have that are Red,

13
00:01:09,426 --> 00:01:15,122
and whatever it might be. So I&#39;m going to pivot these values here, the different category names,

14
00:01:17,277 --> 00:01:23,117
sorry, I&#39;m going to pivot the colors, the yellow color, the red color, whatever, as columns.

15
00:01:23,117 --> 00:01:23,617
So I&#39;m going to pivot that across and have columns for each color.

16
00:01:23,617 --> 00:01:27,255
And then a count of the number of products there are in each of those colors

17
00:01:27,255 --> 00:01:29,763
for each category. So each row will be a category.

18
00:01:29,763 --> 00:01:34,230
You&#39;re actually defining the colors you are expecting to see in code there. So you

19
00:01:34,230 --> 00:01:37,734
know that there is Red, Blue, Black, Silver, Yellow, Grey. Exactly, so I have

20
00:01:37,734 --> 00:01:43,548
to know the data. I have to know what columns I want to create using the pivot statement and 

21
00:01:43,774 --> 00:01:49,614
that&#39;s going to come from this source data. So at some future point if we started selling

22
00:01:49,614 --> 00:01:53,749
a green bike and I don&#39;t have a color for green, then it&#39;s going to be missed out of this. I&#39;m not

23
00:01:53,749 --> 00:01:58,852
going to see it when I pivot the data. The query would work it just would lose the green bikes, yeah.

24
00:02:00,115 --> 00:02:04,595
Okay. So if I go ahead and run that SELECT including the PIVOT clause

25
00:02:04,595 --> 00:02:10,698
at the bottom, we&#39;re pivoting the color, sorry pivoting a COUNT of the ProductID for each color

26
00:02:13,346 --> 00:02:19,506
in those different color categories. If I go ahead and run it, what I get is the category Name,

27
00:02:19,506 --> 00:02:23,920
so I&#39;ve got a row for each category, and a count of the different products that are different colors

28
00:02:23,920 --> 00:02:30,792
in that category. So in this case there are three multi-colored Bib-Shorts, there are two Silver Brakes.

29
00:02:32,958 --> 00:02:39,118
And it gets a little more interesting if I just bring this up a bit and scroll down a bit further,

30
00:02:39,118 --> 00:02:42,679
we start to get to a point where there are 16 Black Mountain Bikes and 16 Silver Mountain Bikes.

31
00:02:43,777 --> 00:02:49,537
And there are 20 Red Road Bikes, and 14 Black Road Bikes. So I start to see those counts of 

32
00:02:49,537 --> 00:02:54,052
different products across the different colors. I&#39;ve pivoted across and that might be what I

33
00:02:54,052 --> 00:02:59,567
want for my report. Now of course what I could do with those results is I might go and create,

34
00:02:59,567 --> 00:03:05,444
I could create a view based on this, or I could create a table based on that. So I could actually

35
00:03:05,459 --> 00:03:10,979
go ahead and create a table called ProductColorPivot and go and stick these values

36
00:03:11,154 --> 00:03:17,154
into that table. So I&#39;ll go define that table and then use exactly the same statement we just saw

37
00:03:19,220 --> 00:03:25,060
to insert data into this temporary table. So there&#39;s 37 rows now, we&#39;ve effectively 

38
00:03:25,060 --> 00:03:26,341
archived that off as a report that I can use again and again.

39
00:03:26,530 --> 00:03:32,450
So we&#39;ve got some data in from somewhere that&#39;s already been pivoted, but we don&#39;t want it

40
00:03:32,450 --> 00:03:37,075
that way around. We&#39;d like to change the orientation. Yeah. And that&#39;s actually a 

41
00:03:37,075 --> 00:03:40,904
more realistic example. Typically what happens is you don&#39;t run a pivot against your own data 

42
00:03:40,904 --> 00:03:44,454
and then store that pivot somewhere unless you need it for a report. But what you might do

43
00:03:44,454 --> 00:03:48,064
is get data in that you&#39;re importing from elsewhere that&#39;s already been pivoted

44
00:03:48,768 --> 00:03:54,928
and you want to switch it around into the more detailed view where you&#39;re getting unpivoted.

45
00:03:54,928 --> 00:03:56,799
So in this case, well imagine that&#39;s

46
00:03:59,343 --> 00:04:04,623
happened and what I want to do is unpivot that data so that I can get the different

47
00:04:04,623 --> 00:04:07,041
product categories and the colors and the counts across that way. So if I

48
00:04:08,084 --> 00:04:13,924
run that UNPIVOT statement sure enough I get back those product categories. So I&#39;ve

49
00:04:14,407 --> 00:04:19,447
no longer columsn for each color. I&#39;ve just got a Color column that has the different values in it. 

50
00:04:20,466 --> 00:04:25,346
Because I unpivoted the ProductCount value for Color and then specified

51
00:04:25,346 --> 00:04:27,494
this is what&#39;s in the source data. I want all of these values to end up

52
00:04:30,066 --> 00:04:35,586
in a column called Color. And so if we&#39;ve got individual products in there, could we

53
00:04:36,052 --> 00:04:41,572
get back the ProductID? No, and that&#39;s something we discussed in the slides as well is --

54
00:04:41,699 --> 00:04:47,219
what you can&#39;t do -- if you&#39;ve pivoted the originally from source data and in doing so 

55
00:04:47,219 --> 00:04:51,504
you&#39;ve aggregated it, so for example, if we look at my original query which was here.

56
00:04:52,991 --> 00:04:55,071
Actually I can do it from there. 

57
00:04:57,166 --> 00:05:02,526
There&#39;s my source data, I had a ProductID associated with each product. And then 

58
00:05:04,258 --> 00:05:09,618
I had the category and I had the color. I then pivoted that to count the ProductIDs.

59
00:05:09,618 --> 00:05:14,997
Now I&#39;ve just reinserted that into the same table. But that meant then that when I unpivoted it

60
00:05:15,215 --> 00:05:21,775
which probably won&#39;t work now, oh yeah, it still works. When I unpivot it, I don&#39;t get the ProductID

61
00:05:21,775 --> 00:05:25,366
back because I counted that. What I&#39;ve got is a count of ProductIDs that I can never get back

62
00:05:26,840 --> 00:05:32,680
to that source data again. So you will lose the detail if you pivot and then unpivot back.

63
00:05:32,680 --> 00:05:36,146
What it&#39;s letting me do though is to take aggregations that are currently displayed across

64
00:05:36,146 --> 00:05:42,024
multiple columns, each one representing a different color, and bringing them in to just

65
00:05:42,852 --> 00:05:46,452
simply having a Color column that has the values in it. 

66
00:05:46,452 --> 00:05:49,021
So someone gives you a load of data, and you think well that doesn&#39;t work, it&#39;s the wrong way

67
00:05:49,021 --> 00:05:50,605
around, we can still deal with that. Yep.

