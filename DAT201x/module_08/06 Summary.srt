0
00:00:02,710 --> 00:00:05,620
So we&#39;ve covered a lot of ways of

1
00:00:05,620 --> 00:00:09,379
aggregating and grouping our data and summarizing it in this module. We 

2
00:00:09,379 --> 00:00:10,639
looked at GROUPING SETS

3
00:00:10,639 --> 00:00:14,600
where I can explicitly say how I want the different groupings to behave and

4
00:00:14,600 --> 00:00:16,199
the different levels of aggregation.

5
00:00:16,199 --> 00:00:20,670
We looked at ROLLUP and CUBE which are built-in grouping sets effectively that I can

6
00:00:20,670 --> 00:00:21,740
use to get

7
00:00:21,740 --> 00:00:25,539
summarize data. And we looked at how we would go about identifying the different

8
00:00:25,539 --> 00:00:27,579
groupings that I have in the results.

9
00:00:27,579 --> 00:00:32,029
And then we looked at pivoting data and upivoting data. So taking data

10
00:00:32,029 --> 00:00:35,110
that has you know

11
00:00:35,110 --> 00:00:38,180
values in there that I could aggregate and switching

12
00:00:38,180 --> 00:00:41,870
what our values in one column across to be multiple columns with

13
00:00:41,870 --> 00:00:43,300
the aggregations in each column,

14
00:00:43,300 --> 00:00:46,530
and then unpivoting it back to the the kind of 

15
00:00:46,530 --> 00:00:50,280
row format. So you&#39;ve got a chance to experiment with this

16
00:00:50,280 --> 00:00:55,269
in the lab. So as usual as for all of the modules in this course,

17
00:00:55,269 --> 00:00:58,539
download the lab documents and the lab files,

18
00:00:58,539 --> 00:01:01,910
use the Getting Started Guide to get yourself set up in Azure SQL 

19
00:01:01,910 --> 00:01:02,510
Database,

20
00:01:02,510 --> 00:01:06,440
and use the lab to challenge yourself to learn a little bit more about using the

21
00:01:06,440 --> 00:01:07,330
syntax here.

22
00:01:07,330 --> 00:01:11,120
And when you&#39;re comfortable with grouping and pivoting data, you&#39;re ready to come

23
00:01:11,120 --> 00:01:13,170
back and join us for the next module

24
00:01:13,170 --> 00:01:16,390
in which we&#39;ll start looking at not only how do we query data

25
00:01:16,390 --> 00:01:20,160
but how do we update data in tables and insert new data into tables and

26
00:01:20,160 --> 00:01:23,210
delete data from tables. So join us back for that

27
00:01:23,210 --> 00:01:24,110
when you&#39;re ready.

