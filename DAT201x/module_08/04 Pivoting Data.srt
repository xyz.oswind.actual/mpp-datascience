0
00:00:02,820 --> 00:00:05,920
So GROUPING SETS and the CUBE and 

1
00:00:05,920 --> 00:00:09,599
ROLLUP operator are really useful for getting those different levels of 

2
00:00:09,599 --> 00:00:10,280
aggregation. 

3
00:00:10,280 --> 00:00:13,759
We&#39;re going to look at another form of summarizing data, but this is 

4
00:00:13,759 --> 00:00:15,820
slightly different, we&#39;re going to look at pivoting data.

5
00:00:15,820 --> 00:00:20,550
As its name suggests, what we&#39;re doing is taking data from a row-based

6
00:00:20,550 --> 00:00:22,600
orientation where we&#39;ve got rows of

7
00:00:22,600 --> 00:00:26,850
data that perhaps shows me each sale with its product category of

8
00:00:26,850 --> 00:00:28,220
what it is that we&#39;ve bought.

9
00:00:28,220 --> 00:00:32,509
And then I&#39;m pivoting that to a different, to a column-based orientation where I can actually

10
00:00:32,509 --> 00:00:32,809
get

11
00:00:32,809 --> 00:00:36,909
totals of how many sales were there for that category or how many

12
00:00:36,909 --> 00:00:37,820
sales were there for 

13
00:00:37,820 --> 00:00:41,129
that product or whatever it might be. So

14
00:00:41,129 --> 00:00:45,210
conceptually this is, you can see the diagram there. I&#39;ve got 

15
00:00:45,210 --> 00:00:48,699
distinct values from a single column projected across as headings in the

16
00:00:48,699 --> 00:00:49,510
other column.

17
00:00:49,510 --> 00:00:53,030
So where I&#39;ve got values of category over here, like

18
00:00:53,030 --> 00:00:57,559
Accessories and so on, what I&#39;ve done is I&#39;ve pivoted that across

19
00:00:57,559 --> 00:01:00,809
so those values become the column headings, Bike,

20
00:01:00,809 --> 00:01:05,040
Accessories and Clothing. And I&#39;m getting a summarized amount of revenue 

21
00:01:05,040 --> 00:01:08,230
within each of those categories, across each OrderID.

22
00:01:08,230 --> 00:01:12,140
So it looks a bit like a spreadsheet. It&#39;s very like a spreadsheet.

23
00:01:12,140 --> 00:01:15,500
Yeah, it&#39;s a pivot table that you would have in something like Excel.

24
00:01:15,500 --> 00:01:18,610
And I guess that&#39;s an important point, that the reality is this

25
00:01:18,610 --> 00:01:23,150
technique is really useful in environments where perhaps

26
00:01:23,150 --> 00:01:27,700
you as the database developer or database manager are responsible for

27
00:01:27,700 --> 00:01:30,020
creating reports that people want

28
00:01:30,020 --> 00:01:34,140
you know pivoted, summarized reports of  data where they don&#39;t themselves have

29
00:01:34,140 --> 00:01:37,710
any wherewithal to create those reports or don&#39;t have the necessary permissions or

30
00:01:37,710 --> 00:01:40,930
you want to very strictly control the reports that they get.

31
00:01:40,930 --> 00:01:44,030
The reality is in most,

32
00:01:44,030 --> 00:01:47,540
certainly within most modern organizations, users who need this type of data

33
00:01:47,540 --> 00:01:49,610
probably have something like Excel

34
00:01:49,610 --> 00:01:52,880
and they can get the source data themselves

35
00:01:52,880 --> 00:01:56,100
and then pivot it and do whatever they want to do with that to 

36
00:01:56,100 --> 00:01:56,880
aggregate it. So

37
00:01:56,880 --> 00:02:00,800
while this is useful if you are actually needing to generate those reports

38
00:02:00,800 --> 00:02:01,970
in Transact-SQL,

39
00:02:01,970 --> 00:02:05,620
the reality is a lot of reporting tools and client tools will actually do this

40
00:02:05,620 --> 00:02:08,810
kind of thing for you and you just provide the source data.

41
00:02:08,810 --> 00:02:12,240
But with that said, here&#39;s the

42
00:02:12,240 --> 00:02:15,960
syntax that we can use to pivot that data across into those columns and

43
00:02:15,960 --> 00:02:18,990
it can be useful way to generate those types of aggregated reports.

44
00:02:18,990 --> 00:02:22,430
Now the

45
00:02:22,430 --> 00:02:26,230
converse of that having pivoted the data, the obvious question is well can I

46
00:02:26,230 --> 00:02:29,980
get it back to how it was? Can I unpivot it? And there&#39;s kind of a

47
00:02:29,980 --> 00:02:32,460
qualified yes is the answer to that.

48
00:02:32,460 --> 00:02:37,430
To a certain extent I can get it back in the sense that I can

49
00:02:37,430 --> 00:02:40,840
re-rotate the data back from being  pivoted with those columns

50
00:02:40,840 --> 00:02:45,050
back to being row-based, that&#39;s fine. And it will

51
00:02:45,050 --> 00:02:49,720
split or spread the values from the row source across into the target

52
00:02:49,720 --> 00:02:50,380
rows. 

53
00:02:50,380 --> 00:02:55,510
But there is a problem that when I do that, you can see I&#39;ve pivoted back across, I&#39;ve got

54
00:02:55,510 --> 00:02:57,240
the OrderID, I&#39;ve got the Bikes,

55
00:02:57,240 --> 00:03:00,610
I&#39;ve got the value coming across there from my

56
00:03:00,610 --> 00:03:03,960
table here. So in this case I&#39;ve got pretty much what 

57
00:03:03,960 --> 00:03:08,660
I needed to get back from there. But I may lose some detail because I&#39;ve aggregated

58
00:03:08,660 --> 00:03:10,640
when I did the pivot in the first place

59
00:03:10,640 --> 00:03:13,890
I might lose some data when I unpivot it back

60
00:03:13,890 --> 00:03:17,030
because I might lose some of those aggregations. So you&#39;ve just got the same numbers

61
00:03:17,030 --> 00:03:20,780
on the right that you had on the left. Whereas when we did it originally you

62
00:03:20,780 --> 00:03:24,300
actually, I actually summed up, yeah, so we brought back across those OrderIDs, 

63
00:03:24,300 --> 00:03:27,610
I&#39;ve got the values for each individual

64
00:03:27,610 --> 00:03:31,320
category. If we go back to how it was before we

65
00:03:31,320 --> 00:03:34,590
pivoted it, add the product ID&#39;s in there.

66
00:03:34,590 --> 00:03:39,570
And I had you know details for each OrderID, there was two different products

67
00:03:39,570 --> 00:03:40,060
in

68
00:03:40,060 --> 00:03:43,450
OrderID here where I had Bikes and Accessories

69
00:03:43,450 --> 00:03:46,910
and actually in order number

70
00:03:46,910 --> 00:03:50,080
25 I had some Accessories, but had 

71
00:03:50,080 --> 00:03:54,190
two products that were in Clothing, one at 26.57 one at

72
00:03:54,190 --> 00:03:58,240
5.78. So I got you know I had that level of detail for each

73
00:03:58,240 --> 00:04:02,070
individual line item of clothing, I got a total. When we

74
00:04:02,070 --> 00:04:07,860
unpivoted that back, for order 1025 all I know is there was a total of 32.35

75
00:04:07,860 --> 00:04:08,840
spent on clothing.

76
00:04:08,840 --> 00:04:12,540
I don&#39;t get the further break down into the individual line items.

77
00:04:12,540 --> 00:04:16,970
So if you can&#39;t see it in the pivoted data, you can&#39;t get it back when you unpivot it.

78
00:04:16,970 --> 00:04:22,150
Exactly. So that&#39;s the consideration you&#39;ve got. Yes you can pivot data and

79
00:04:22,150 --> 00:04:23,190
you can unpivot it

80
00:04:23,190 --> 00:04:24,820
to kind of get back again, but

81
00:04:24,820 --> 00:04:27,890
you may lose some levels of detail. You may lose the detail rows

82
00:04:27,890 --> 00:04:30,920
if you&#39;ve aggregated multiple rows when you pivoted in the first place.

