0
00:00:03,639 --> 00:00:05,319
Let&#39;s look an example of

1
00:00:05,319 --> 00:00:09,100
grouping data using the various different options that we have. And I&#39;ve

2
00:00:09,100 --> 00:00:11,830
commented out some of the options down here and I&#39;ll 

3
00:00:11,830 --> 00:00:14,559
switch them around as we go through. But let&#39;s start with something

4
00:00:14,559 --> 00:00:16,910
that should be familiar to us, just a basic GROUP BY.

5
00:00:16,910 --> 00:00:20,859
I&#39;m getting the ProductCategoryName, sorry the ParentProductCatagoryName which

6
00:00:20,859 --> 00:00:22,189
will be the high-level

7
00:00:22,189 --> 00:00:26,180
category like Bikes or Components or whatever. I&#39;m then getting the

8
00:00:26,180 --> 00:00:29,289
category name which you can think of is being the subcategory of Bike or the

9
00:00:29,289 --> 00:00:30,599
subcategory of Component or

10
00:00:30,599 --> 00:00:35,390
Mountain Bikes or whatever. And then a count of the number of products that there are

11
00:00:35,390 --> 00:00:36,140
in that

12
00:00:36,140 --> 00:00:39,870
subcategory. Then I&#39;m

13
00:00:39,870 --> 00:00:43,280
getting that data out, and I&#39;m in this case just GROUPING BY

14
00:00:43,280 --> 00:00:46,379
the ProductCategoryName, rather the ParentProductCategoryName and the 

15
00:00:46,379 --> 00:00:47,120
ProductCategoryName.

16
00:00:47,120 --> 00:00:52,600
So if I run that query, I get back what I would expect.

17
00:00:52,600 --> 00:00:57,100
I get my parent product category, Accessories.

18
00:00:57,100 --> 00:01:01,120
The product category within that, Bike Racks, and the number of products that there

19
00:01:01,120 --> 00:01:01,520
are,

20
00:01:01,520 --> 00:01:05,729
that fit that particular grouping. And again within Accessories, Bottles

21
00:01:05,729 --> 00:01:06,460
and Cages,

22
00:01:06,460 --> 00:01:10,049
that are 3 products that are within that particular category.

23
00:01:10,049 --> 00:01:14,250
And that works all the way down, I can count them out. But we don&#39;t know 

24
00:01:14,250 --> 00:01:19,210
how much we sold in Accessories. Well we don&#39;t know how many products we have in Accessories

25
00:01:19,210 --> 00:01:21,330
in total unless we add all these things up.

26
00:01:21,330 --> 00:01:25,420
To get a grand total, we would have to add them all up. So we have no grand total.

27
00:01:25,420 --> 00:01:29,110
And I have no way of knowing, I&#39;m mean there&#39;s also a possibility here that I might have 

28
00:01:29,110 --> 00:01:34,659
Bike Racks that are in Accessories but Bike Racks might also be in another category, they 

29
00:01:34,659 --> 00:01:37,860
might be in Components as well. So there might be some Bike Racks that are in Accessories

30
00:01:37,860 --> 00:01:41,400
some Bike Racks that are Components and what I don&#39;t have is something here that says well

31
00:01:41,400 --> 00:01:43,590
how many Bike Racks do I have  regardless of which

32
00:01:43,590 --> 00:01:47,630
category they&#39;re in. So I don&#39;t have every possible combination, but I

33
00:01:47,630 --> 00:01:49,210
have what I asked for in the GROUP BY.

34
00:01:49,210 --> 00:01:52,829
Instead of grouping by those individual columns,

35
00:01:52,829 --> 00:01:57,360
what I&#39;m going to do is use the next option here where I&#39;m grouping by GROUPING

36
00:01:57,360 --> 00:01:58,299
SETS.

37
00:01:58,299 --> 00:02:02,680
I&#39;m going to create GROUPING SETS based on the ParentProductCategoryName

38
00:02:02,680 --> 00:02:06,329
and the ProductCategoryName and a grand total.

39
00:02:06,329 --> 00:02:10,349
So I&#39;m using that parenthesis option there. So if I go ahead and

40
00:02:10,348 --> 00:02:13,590
run that,

41
00:02:13,590 --> 00:02:16,629
I start to see some nulls appear and what I&#39;m getting here

42
00:02:16,629 --> 00:02:22,010
is the grand total across the tops, there are 295 products all told

43
00:02:22,010 --> 00:02:25,260
in my catalog. And then within my

44
00:02:25,260 --> 00:02:28,930
Bib-Shorts regardless of the parent product category, I&#39;ve got 

45
00:02:28,930 --> 00:02:29,519
three

46
00:02:29,519 --> 00:02:33,410
things that are Bib-Shorts, I&#39;ve got three things that are Bottles and Cages.

47
00:02:33,410 --> 00:02:38,640
And if I just move this up a little to make it clear what&#39;s going on

48
00:02:38,640 --> 00:02:42,940
and scroll down, I can see a whole bunch nulls here. So each individual product

49
00:02:42,940 --> 00:02:45,190
category name I get a total count for that

50
00:02:45,190 --> 00:02:50,390
and then it switches across and I get totals for how many Accessories I&#39;ve got, how many Bikes, 

51
00:02:50,390 --> 00:02:50,760
how much

52
00:02:50,760 --> 00:02:54,230
Clothing, how many Components. So

53
00:02:54,230 --> 00:02:57,579
that&#39;s pretty good, that&#39;s given me some of what I was missing just from the basic GROUP

54
00:02:57,579 --> 00:02:58,040
BY.

55
00:02:58,040 --> 00:03:01,519
But you&#39;ve lost the combination of 

56
00:03:01,519 --> 00:03:05,680
ProductCategoryName with product parent. Exactly, so I don&#39;t know for sure

57
00:03:05,680 --> 00:03:09,440
out of these three Cranksets which parent

58
00:03:09,440 --> 00:03:13,110
product category do they come from? Is that one parent product category or are there

59
00:03:13,110 --> 00:03:16,950
two Cranksets in Components and one Crankset in Accessories 

60
00:03:16,950 --> 00:03:20,780
or whatever, I&#39;m not getting that level of detail. Now I could get that, I could

61
00:03:20,780 --> 00:03:24,030
manipulate the GROUPING SETS clause here

62
00:03:24,030 --> 00:03:28,400
and have more sophisticated subclauses within there and get back every

63
00:03:28,400 --> 00:03:29,130
combination,

64
00:03:29,130 --> 00:03:32,489
but that becomes quite complex to write. I have to actually think through

65
00:03:32,489 --> 00:03:35,540
a lot of different combinations. It is

66
00:03:35,540 --> 00:03:39,040
highly customizable which is great but if all I want to do is 

67
00:03:39,040 --> 00:03:42,670
say look I just want to roll up by my product categories,

68
00:03:42,670 --> 00:03:45,930
I can use the built in ROLLUP option instead. So I&#39;m 

69
00:03:45,930 --> 00:03:50,680
rolling up by ProductCategoryName and by, sorry by ParentProductCategoryName

70
00:03:50,680 --> 00:03:51,810
and by ProductCategoryName.

71
00:03:51,810 --> 00:03:57,079
So I run that and that gives me my grand total at the top

72
00:03:57,079 --> 00:04:00,790
and then for Accessories in total there are 29,

73
00:04:00,790 --> 00:04:04,560
and then within Accessories that 29 is made up of one Bike Rack,

74
00:04:04,560 --> 00:04:08,370
one Stand, three Bottles and Cages, and so on. So I&#39;m getting that 

75
00:04:08,370 --> 00:04:12,609
additional level of detail. Now again what I&#39;m

76
00:04:12,609 --> 00:04:16,329
perhaps not getting here is that level of detail, well

77
00:04:16,329 --> 00:04:19,780
okay, you&#39;ve given me a total for Accessories, but you haven&#39;t given me a total for

78
00:04:19,779 --> 00:04:24,630
Bike Racks that might --  well there&#39;s only one Bike Rack -- well you haven&#39;t given me a total for

79
00:04:24,630 --> 00:04:28,040
Bottles and Cages if there are Bottles and Cages

80
00:04:28,040 --> 00:04:32,500
in different parent categories. So I want to see the total Bottle and Cages regardless of

81
00:04:32,500 --> 00:04:33,560
the parent category.

82
00:04:33,560 --> 00:04:36,830
So again there are some you know aggregations we&#39;re not getting.

83
00:04:36,830 --> 00:04:41,790
And to get that, I can actually use CUBE

84
00:04:41,790 --> 00:04:45,190
which will just -- effectively what I&#39;m saying is look just aggregate these every way they can

85
00:04:45,190 --> 00:04:46,070
be aggregated and

86
00:04:46,070 --> 00:04:49,600
give me back the results. So if I run that,

87
00:04:49,600 --> 00:04:53,590
I get a grand total. I get a total of

88
00:04:53,590 --> 00:04:57,000
Bib-Shorts regardless of the parent  category and

89
00:04:57,000 --> 00:05:01,920
a total of Bottles and Cages and so on. Again we&#39;ll just move this up a bit.

90
00:05:01,920 --> 00:05:06,450
Then I get a total for Accessories

91
00:05:06,450 --> 00:05:10,120
and then within Accessories I get the totals of the individual product categories.

92
00:05:10,120 --> 00:05:10,750
So 

93
00:05:10,750 --> 00:05:13,810
it&#39;s effectively slicing and dicing every way that it can

94
00:05:13,810 --> 00:05:17,300
to give me every possible combination and every possible total.

95
00:05:17,300 --> 00:05:22,080
So that&#39;s a use of 

96
00:05:22,080 --> 00:05:26,610
GROUPING SETS. I guess the main point is that the GROUPING SETS clause

97
00:05:26,610 --> 00:05:30,810
itself, you can define exactly what you want, but you

98
00:05:30,810 --> 00:05:34,350
the downside of that is you have to define exactly what you want and it can be

99
00:05:34,350 --> 00:05:35,290
fairly complex

100
00:05:35,290 --> 00:05:38,580
syntax. As a shortcut to fairly commonly accessed

101
00:05:38,580 --> 00:05:42,830
aggregations, you could use ROLLUP or if you want to get every possible

102
00:05:42,830 --> 00:05:46,170
aggregation, you can use CUBE and that will bring you back that data.

