0
00:00:02,260 --> 00:00:05,299
So GROUPING SETS is an extension to

1
00:00:05,299 --> 00:00:08,740
the GROUP BY. It&#39;s a sub clause that builds on the

2
00:00:08,740 --> 00:00:12,030
GROUP BY clause that we saw previously.

3
00:00:12,030 --> 00:00:16,890
And if you remember a GROUP BY query, effectively what it would let us do was take one of the

4
00:00:16,890 --> 00:00:17,660
columns or

5
00:00:17,660 --> 00:00:21,480
multiple columns from the query and generate aggregated totals for each of

6
00:00:21,480 --> 00:00:21,850
those.

7
00:00:21,850 --> 00:00:26,310
So that seems pretty good. What would I want to do over and above

8
00:00:26,310 --> 00:00:30,320
that Jeff, if I&#39;m grouping my data? Well, a simple thing might be

9
00:00:30,320 --> 00:00:33,379
grand total. So I&#39;d like to have

10
00:00:33,379 --> 00:00:38,440
everything grouped up in a sum, but I want also to have

11
00:00:38,440 --> 00:00:42,780
the sum of the total. Because we&#39;ve seen we could do that, we left off

12
00:00:42,780 --> 00:00:46,309
GROUP BY initially and we got the grand total. But then I didn&#39;t get the detail.

13
00:00:46,309 --> 00:00:50,219
Right so I want the grant total, I want maybe some subtotals

14
00:00:50,219 --> 00:00:53,640
and I want at each level I&#39;d like to have the information,

15
00:00:53,640 --> 00:00:56,940
in one query. Okay. So

16
00:00:56,940 --> 00:01:00,059
the way that we can do that is we can use this GROUPING SETS subclause and 

17
00:01:00,059 --> 00:01:03,710
it lets me define multiple groupings I guess is the point. Rather than grouping by

18
00:01:03,710 --> 00:01:06,760
one GROUP BY clause, even though there&#39;s multiple columns in there,

19
00:01:06,760 --> 00:01:10,030
I&#39;m getting multiple levels of grouping if I do it this way. Yeah?

20
00:01:10,030 --> 00:01:15,510
Mhmm. So there&#39;s a couple of things to look at in the syntax here, you can see it&#39;s 

21
00:01:15,510 --> 00:01:20,610
GROUP BY GROUPING SETS and then I declare what are the grouping sets that I want to

22
00:01:20,610 --> 00:01:21,270
include.

23
00:01:21,270 --> 00:01:24,479
And those GROUPING SETS can be based on

24
00:01:24,479 --> 00:01:28,600
columns just like the GROUP BY statement. So each of these is its own GROUP BY

25
00:01:28,600 --> 00:01:32,340
that goes into here, into the GROUPING SETS. And there&#39;s a special one, I can

26
00:01:32,340 --> 00:01:36,170
specify an empty parenthesis to say give me the grand total, aggregate all of

27
00:01:36,170 --> 00:01:38,000
the rows and give me a grand total

28
00:01:38,000 --> 00:01:42,509
using that. So using that technique I can define

29
00:01:42,509 --> 00:01:46,110
effectively multiple GROUP BY clauses in the one query.

30
00:01:46,110 --> 00:01:51,320
So here&#39;s a kind of worked example on the slide to help us kind of understand

31
00:01:51,320 --> 00:01:52,200
what&#39;s going on here.

32
00:01:52,200 --> 00:01:55,390
I&#39;ve got my EmployeeID,

33
00:01:55,390 --> 00:02:00,020
my CustomerID, the sum of the Amount as the TotalAmount from my SalesOrder. 

34
00:02:00,020 --> 00:02:00,540
So it&#39;s a fairly

35
00:02:00,540 --> 00:02:03,920
straightforward query where I&#39;m looking at

36
00:02:03,920 --> 00:02:07,920
effectually getting the sum total that each customer and employee

37
00:02:07,920 --> 00:02:09,240
 

38
00:02:09,240 --> 00:02:14,030
has placed on an order. But what I want to do is I want to also get the

39
00:02:14,030 --> 00:02:17,200
the groupings to show me, show me for each employee

40
00:02:17,200 --> 00:02:21,910
what&#39;s their total, how much revenue has each employee brought in,

41
00:02:21,910 --> 00:02:26,190
and show me for each customer how much revenue has that customer

42
00:02:26,190 --> 00:02:30,310
spent with me in total regardless of who which employees sold on them the

43
00:02:30,310 --> 00:02:32,140
details. But all in the same query.

44
00:02:32,140 --> 00:02:35,960
But all in the one query. And then finally, give me a grand total of

45
00:02:35,960 --> 00:02:39,510
the total amount of sales across all customers and all employees.

46
00:02:39,510 --> 00:02:44,250
So I&#39;m able to define those three different groupings within the one query using

47
00:02:44,250 --> 00:02:44,870
these 

48
00:02:44,870 --> 00:02:48,470
GROUPING SETS. So here&#39;s what the results

49
00:02:48,470 --> 00:02:52,440
would look like from that query. And you&#39;ll see that there&#39;s a number of nulls in there and

50
00:02:52,440 --> 00:02:55,460
the first row would have got a null EmployeeID and 

51
00:02:55,460 --> 00:03:00,330
and a null for CustomerID. Which of our GROUPING SETS do you think that might be?

52
00:03:00,330 --> 00:03:04,490
Well if we don&#39;t have a specific customer or a specific employee,

53
00:03:04,490 --> 00:03:07,660
is that everyone? Bingo! That&#39;s the total for all sales.

54
00:03:07,660 --> 00:03:11,990
Yup, so you&#39;ll get one back that has a null in all of the columns that 

55
00:03:11,990 --> 00:03:15,350
we&#39;ve got in the query and it will just give me a grand total for the total

56
00:03:15,350 --> 00:03:17,980
amount a revenue that we&#39;ve generated. As you can see we&#39;ve

57
00:03:17,980 --> 00:03:22,340
done startlingly well and generated 256.23.

58
00:03:22,340 --> 00:03:26,590
So the next one down has CustomerID&#39;s

59
00:03:26,590 --> 00:03:31,460
and no EmployeeID, so we would assume -- well the subtotal 

60
00:03:31,460 --> 00:03:35,220
for each customer I guess. Yeah exactly. So what we&#39;ve got there is

61
00:03:35,220 --> 00:03:38,320
the subtotal is generated and that&#39;s that CustomerID

62
00:03:38,320 --> 00:03:43,130
clause that I&#39;ve got in my GROUPING SETS. And then the last one that we can see there

63
00:03:43,130 --> 00:03:46,780
obviously it&#39;s the only one remaining, that&#39;s the subtotal for each

64
00:03:46,780 --> 00:03:50,720
employee that&#39;s coming out of there. So I&#39;ve got those three different GROUPING

65
00:03:50,720 --> 00:03:51,980
SETS that I&#39;ve defined,

66
00:03:51,980 --> 00:03:55,990
I&#39;ve got CustomerID, I&#39;ve got EmployeeID, and I&#39;ve got a grand total

67
00:03:55,990 --> 00:03:59,670
at the top. So pretty good,

68
00:03:59,670 --> 00:04:04,230
pretty useful. The syntax is a little bit complex to kind of get your head around.

69
00:04:04,230 --> 00:04:08,680
And to make life a little bit easier there are kind of common

70
00:04:08,680 --> 00:04:12,100
themes that people use when they&#39;re grouping data. So there are a couple

71
00:04:12,100 --> 00:04:12,640
of built in

72
00:04:12,640 --> 00:04:17,060
operators that will automatically defined the right groupings for you.

73
00:04:17,060 --> 00:04:20,850
So the first of those that we want to look it is something called ROLLUP.

74
00:04:20,850 --> 00:04:24,510
And ROLLUP is a really common thing to want to do where you have

75
00:04:24,510 --> 00:04:27,930
perhaps a hierarchy of things, maybe products and product categories and

76
00:04:27,930 --> 00:04:29,690
product paring categories or

77
00:04:29,690 --> 00:04:34,700
maybe you have states that are in cities,

78
00:04:34,700 --> 00:04:38,370
sorry cities that are in state provinces and state provinces that are in countries or

79
00:04:38,370 --> 00:04:39,210
regions so

80
00:04:39,210 --> 00:04:43,160
you&#39;ve got that hierarchy of things you want to group at each level. ROLLUP is really 

81
00:04:43,160 --> 00:04:44,090
useful for that.

82
00:04:44,090 --> 00:04:47,400
And what it will do is give me

83
00:04:47,400 --> 00:04:50,840
a grand total and subtotals at each of the

84
00:04:50,840 --> 00:04:55,400
different levels. And then CUBE is  actually even

85
00:04:55,400 --> 00:04:59,260
more combinations. It actually gives me every possible combination

86
00:04:59,260 --> 00:05:02,660
of groupings that I would need with that data. So

87
00:05:02,660 --> 00:05:06,430
as a short hand to say just show me every possible aggregation here,

88
00:05:06,430 --> 00:05:10,270
I can just use CUBE to go and get that result back. So in that one there where you&#39;ve got 

89
00:05:10,270 --> 00:05:12,260
salesperson and customer, you would get

90
00:05:12,260 --> 00:05:16,040
the totals for each salesperson and the totals for each customer

91
00:05:16,040 --> 00:05:21,410
and then also and the total for each salesperson and each customer combined, so

92
00:05:21,410 --> 00:05:25,990
yeah, every combination coming back out. So

93
00:05:25,990 --> 00:05:29,900
you might have spotted the potential flaw in all of this.

94
00:05:29,900 --> 00:05:32,970
You had null values

95
00:05:32,970 --> 00:05:36,580
in there, so if we&#39;ve got

96
00:05:36,580 --> 00:05:40,550
a null specifying a grand total, and we&#39;ve got a null specifying a subtotal,

97
00:05:40,550 --> 00:05:45,680
what&#39;s in there to say well we have some nulls as well? Because I have some

98
00:05:45,680 --> 00:05:47,960
web sales and they don&#39;t have, no 

99
00:05:47,960 --> 00:05:51,350
employee sold them, so they have null.

100
00:05:51,350 --> 00:05:54,890
Yeah and that&#39;s part of the problem. The first thing is obviously

101
00:05:54,890 --> 00:05:56,410
if you have multiple grouping sets

102
00:05:56,410 --> 00:06:00,320
in those rows, I told you on the slides basically told you, we have to

103
00:06:00,320 --> 00:06:03,840
sort of look carefully to see well, it&#39;s got a CustomerID but not an EmployeeID

104
00:06:03,840 --> 00:06:06,970
to figure out which of the subtotals we were seeing there.

105
00:06:06,970 --> 00:06:11,050
And then secondly as you say, if I&#39;ve got nulls, well it might be because there&#39;s

106
00:06:11,050 --> 00:06:14,440
been aggregation created and null goes  in the column that&#39;s not part of

107
00:06:14,440 --> 00:06:15,260
the aggregation.

108
00:06:15,260 --> 00:06:20,400
Or it might be because the original data was null. And I have a null EmployeeID or a null

109
00:06:20,400 --> 00:06:24,570
Category or whatever it might be. So there is potential confusion in

110
00:06:24,570 --> 00:06:28,219
identifying well where did this row in the result set come from?

111
00:06:28,219 --> 00:06:31,039
Is it part of the original data? Or is it

112
00:06:31,039 --> 00:06:34,050
an aggregation? And if it&#39;s an aggregation at which level

113
00:06:34,050 --> 00:06:37,490
is the aggregation? So to help

114
00:06:37,490 --> 00:06:40,880
us deal with that, there is actually a function, a GROUPING_ID function

115
00:06:40,880 --> 00:06:44,020
which you specify in the query 

116
00:06:44,020 --> 00:06:47,830
just like it&#39;s a column. And that column will indicate

117
00:06:47,830 --> 00:06:52,789
which group, I say it will indicate,

118
00:06:52,789 --> 00:06:54,250
what happens, it&#39;s a grouping id function and you pass in

119
00:06:54,250 --> 00:06:57,250
the name of the different groupings you&#39;ve got, and it will give you a one or zero

120
00:06:57,250 --> 00:06:58,330
to tell you

121
00:06:58,330 --> 00:07:01,520
if this row was returned as a result of that grouping

122
00:07:01,520 --> 00:07:05,840
or not. So then we could filter to find our grand total

123
00:07:05,840 --> 00:07:11,000
because it&#39;s going to have, it&#39;s summarizing on both of the those columns in this situation.

124
00:07:11,000 --> 00:07:13,610
That&#39;s our grand total, or if it&#39;s on one of the columns that&#39;s our

125
00:07:13,610 --> 00:07:18,840
subtotal. Exactly, yes. So for each possible row, possible column I&#39;ve got that I might be

126
00:07:18,840 --> 00:07:19,660
grouping on,

127
00:07:19,660 --> 00:07:22,919
I can use the GROUPING_ID function to give me a one or a zero to say is this

128
00:07:22,919 --> 00:07:24,340
row grouped on this function or not.

