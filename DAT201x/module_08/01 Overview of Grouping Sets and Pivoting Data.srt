0
00:00:05,740 --> 00:00:06,530
Welcome back

1
00:00:06,530 --> 00:00:10,400
once again. Hopefully you&#39;ve gone and completed the lab on

2
00:00:10,400 --> 00:00:15,190
the table sets that we had previously from the previous module.

3
00:00:15,190 --> 00:00:18,900
We&#39;re going to move on now in module  8 we&#39;re going to

4
00:00:18,900 --> 00:00:22,140
look again at something we looked at previously. Previously we looked

5
00:00:22,140 --> 00:00:25,730
at the grouping using the GROUP BY clause. So we were able to aggregate

6
00:00:25,730 --> 00:00:28,890
the results of our query using into groups using GROUP BY.

7
00:00:28,890 --> 00:00:32,169
We&#39;re going to extend that a little bit further and see how we can get different

8
00:00:32,168 --> 00:00:33,590
levels of aggregation

9
00:00:33,590 --> 00:00:37,230
and try and understand grouping sets and pivoting 

10
00:00:37,230 --> 00:00:40,649
for summarizing the data values that we might get back from our query.

11
00:00:40,649 --> 00:00:41,300
So there&#39;s

12
00:00:41,300 --> 00:00:44,839
a fair amount of reporting involved in this module.

13
00:00:44,839 --> 00:00:48,760
And a fair amount of aggregation, but hopefully it will all become clear as we go through.

14
00:00:48,760 --> 00:00:53,969
So the module covers various different things. We&#39;re going to start

15
00:00:53,969 --> 00:00:55,850
with something called grouping sets.

16
00:00:55,850 --> 00:01:00,809
And we can declare our grouping sets but we can also use another couple techniques

17
00:01:00,809 --> 00:01:04,070
called ROLLUP and CUBE to automatically define

18
00:01:04,069 --> 00:01:08,040
grouping sets. So we&#39;ll talk about those and we&#39;ll talk about how we identify the

19
00:01:08,040 --> 00:01:08,700
groupings

20
00:01:08,700 --> 00:01:12,170
within the results. Which one&#39;s the

21
00:01:12,170 --> 00:01:16,409
grand total, which one&#39;s the subtotal and so on. And then we&#39;ll switch gears a little

22
00:01:16,409 --> 00:01:18,460
bit and talk about something that&#39;s 

23
00:01:18,460 --> 00:01:21,500
I guess is related in the sense that it&#39;s about summarizing data,

24
00:01:21,500 --> 00:01:25,030
but we will talk about pivoting data. And conversely

25
00:01:25,030 --> 00:01:26,020
un-pivoting data.

