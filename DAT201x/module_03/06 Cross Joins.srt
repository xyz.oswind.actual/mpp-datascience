0
00:00:02,029 --> 00:00:05,089
So we&#39;ve looked at INNER JOINS, we&#39;ve looked at OUTER JOINs,

1
00:00:05,089 --> 00:00:08,890
a couple of different types of joins there. The other type of join we&#39;re going to look at is CROSS

2
00:00:08,890 --> 00:00:10,940
JOINs. And you alluded to this earlier on

3
00:00:10,940 --> 00:00:13,960
Jeff, we talked about Cartesian products. Yeah.

4
00:00:13,960 --> 00:00:17,310
And that&#39;s exactly what CROSS JOINs are designed to do. So we combine each row

5
00:00:17,310 --> 00:00:18,210
from the first table

6
00:00:18,210 --> 00:00:22,070
with each row from the second table and we get

7
00:00:22,070 --> 00:00:25,779
all possible combinations back out of that. So here&#39;s an example where I&#39;ve got

8
00:00:25,779 --> 00:00:29,449
employees and products and as I run the query

9
00:00:29,449 --> 00:00:33,160
there&#39;s a CROSS JOIN. Notice there&#39;s no ON clause because CROSS JOINs you don&#39;t 

10
00:00:33,160 --> 00:00:34,590
filter, you&#39;re basically saying CROSS JOIN

11
00:00:34,590 --> 00:00:37,940
everything. So you don&#39;t need to have an ON clause. 

12
00:00:37,940 --> 00:00:41,850
I&#39;ve cross joined my employee tables and products table

13
00:00:41,850 --> 00:00:45,219
and what I get is every possible employee

14
00:00:45,219 --> 00:00:48,530
with every possible product. Which is  probably not something I would

15
00:00:48,530 --> 00:00:52,289
generally do, but as we said earlier there may be occasions

16
00:00:52,289 --> 00:00:55,559
when you might want to get that type of result. And this is a bit nicer than the

17
00:00:55,559 --> 00:00:58,820
SQL 89 syntax because it&#39;s explicit. You said

18
00:00:58,820 --> 00:01:03,629
I typed the word CROSS, so I know what I&#39;m doing. Yeah, and again that&#39;s why we want to use that

19
00:01:03,629 --> 00:01:07,360
more modern syntax. I&#39;ve we use the older syntax is easy to do this accidentally.

20
00:01:07,360 --> 00:01:12,190
So this is actually the logical foundation for INNER and OUTER joins. This is 

21
00:01:12,190 --> 00:01:15,170
actually how SQL Server does things under the covers. It starts with the

22
00:01:15,170 --> 00:01:16,180
Cartesian product

23
00:01:16,180 --> 00:01:20,990
and then filters to get an INNER JOIN. And it starts with the Cartesian product

24
00:01:20,990 --> 00:01:25,070
filters and then adds back non-matching null rows to get an OUTER JOIN.

25
00:01:25,070 --> 00:01:28,830
So actually fundamentally all joins our Cartesian products, but

26
00:01:28,830 --> 00:01:32,570
then they&#39;re filtered. CROSS JOINs are just Cartesian products without any filters

27
00:01:32,570 --> 00:01:33,090
applied.

28
00:01:33,090 --> 00:01:36,830
As we said they&#39;re typically not

29
00:01:36,830 --> 00:01:39,900
useful. We&#39;ve come up with a couple examples,

30
00:01:39,900 --> 00:01:44,720
one was the matching employees to roles, so I want to compare every employee to every

31
00:01:44,720 --> 00:01:46,840
role and see if they&#39;ve got the skills for those roles.

32
00:01:46,840 --> 00:01:51,740
The other is if you were generating a league of matches between two

33
00:01:51,740 --> 00:01:52,420
different types

34
00:01:52,420 --> 00:01:55,640
teams, you might you know have a CROSS JOIN to get every possible combination there.

35
00:01:55,640 --> 00:01:55,990
 

36
00:01:55,990 --> 00:01:59,340
And the other times I&#39;ve seen it used, is  just to generate test data.

37
00:01:59,340 --> 00:02:03,150
Where you take a list of random first names and last names, and then generate every

38
00:02:03,150 --> 00:02:06,450
possible combination to give you some data. I&#39;ll be honest that&#39;s the only time I&#39;ve seen it

39
00:02:06,450 --> 00:02:07,220
used in anger.

40
00:02:07,220 --> 00:02:10,050
is to generate some test data. The other ones are making up scenarios

41
00:02:10,050 --> 00:02:15,910
for training courses. &lt;laughter&gt; Yeah, it&#39;s pretty unusual that you&#39;ll use one these, but it&#39;s good to be aware

42
00:02:15,910 --> 00:02:19,640
of the CROSS JOIN sometimes. Well, as you say, generating test data is  something you need to do.

43
00:02:19,640 --> 00:02:26,640
Yes, absolutely. Let&#39;s look an example of using CROSS JOINs to generate

44
00:02:26,970 --> 00:02:28,320
every possible combination.

45
00:02:28,320 --> 00:02:31,380
And we&#39;ll just switch across to this one,

46
00:02:31,380 --> 00:02:35,270
and I&#39;ll remember to change the database context this time. Here&#39;s one very

47
00:02:35,270 --> 00:02:37,590
simple example, what I&#39;m doing is getting the

48
00:02:37,590 --> 00:02:42,030
product name, and the first and last name of each customer,

49
00:02:42,030 --> 00:02:45,920
and the customer&#39;s phone number. And the scenario for this, it is a highly realistic

50
00:02:45,920 --> 00:02:46,740
scenario,

51
00:02:46,740 --> 00:02:50,810
I have tele sales operatives who are going to phone every customer

52
00:02:50,810 --> 00:02:53,950
and ask them if they want to buy a product. And their going to do that for every

53
00:02:53,950 --> 00:02:54,450
product.

54
00:02:54,450 --> 00:02:58,360
Perhaps not the most realistic example, or would be really

55
00:02:58,360 --> 00:02:59,380
annoying if they did.

56
00:02:59,380 --> 00:03:03,720
But for each product, I&#39;ve got every single product and every single customer

57
00:03:03,720 --> 00:03:08,080
and then I&#39;m going to phone the customer and ask them if they want to buy that product. So that&#39;s

58
00:03:08,080 --> 00:03:09,020
that&#39;s a CROSS JOIN.

59
00:03:09,020 --> 00:03:12,750
It is every single possible combination if we have a look there. And actually the

60
00:03:12,750 --> 00:03:14,430
query is still executing

61
00:03:14,430 --> 00:03:19,030
you can see. Because we have so many possible combinations it&#39;s going to carry on

62
00:03:19,030 --> 00:03:20,800
executing until he gets them all. So

63
00:03:20,800 --> 00:03:23,900
even as I scroll down, and I can see there&#39;s already

64
00:03:23,900 --> 00:03:29,730
quite a substantial number rows. It&#39;s continuing to execute to get those back and that&#39;s another

65
00:03:29,730 --> 00:03:31,410
reason you want to avoid accidentally

66
00:03:31,410 --> 00:03:36,459
cross joining things because it brings back a huge volume of data.

67
00:03:36,459 --> 00:03:40,970
If you think about how each, how big  each table is, it&#39;s then a factor that that can

68
00:03:40,970 --> 00:03:45,890
tell you how many possible combinations there are. Finally executed with the

69
00:03:45,890 --> 00:03:49,320
249,865 rows

70
00:03:49,320 --> 00:03:49,720
returned.

