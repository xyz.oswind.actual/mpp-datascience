0
00:00:02,290 --> 00:00:05,029
Okay so self joins. 

1
00:00:05,029 --> 00:00:09,000
Let&#39;s finish off our discussion of joins with the final type of join, which is a self

2
00:00:09,000 --> 00:00:13,280
join. And what we&#39;re doing here, as the name suggests, is we&#39;re comparing rows 

3
00:00:13,280 --> 00:00:17,030
in a table to other rows in the same table. And there&#39;s

4
00:00:17,030 --> 00:00:21,960
the kind of archetypal example of this, where we do it we obviously need

5
00:00:21,960 --> 00:00:23,310
two instances of the same table,

6
00:00:23,310 --> 00:00:27,140
and the example that almost everybody uses when they talk about this is this one here.

7
00:00:27,140 --> 00:00:30,689
Probably because it&#39;s the most common way that you would do this. 

8
00:00:30,689 --> 00:00:34,100
If you have a table of employees and 

9
00:00:34,100 --> 00:00:37,820
one key fields for each employee is the ID of their manager.

10
00:00:37,820 --> 00:00:40,950
Well, managers are also employees, so you&#39;re effectively

11
00:00:40,950 --> 00:00:45,020
creating a self-reference. And what we would do is use a

12
00:00:45,020 --> 00:00:48,090
self join to join 

13
00:00:48,090 --> 00:00:52,489
the employees to their managers. And the way that we do that is we have

14
00:00:52,489 --> 00:00:56,579
in this case the employee FirstName as Employee

15
00:00:56,579 --> 00:01:01,180
and manager FirstName as Manager from the HR. Employee table

16
00:01:01,180 --> 00:01:05,070
as emp LEFT JOIN to the same table again

17
00:01:05,069 --> 00:01:08,270
but with a different alias. So you need to use aliases here.

18
00:01:08,270 --> 00:01:11,360
You have to use aliases because you&#39;ve got the same table twice.

19
00:01:11,360 --> 00:01:15,100
Let&#39;s see an example of how we might use

20
00:01:15,100 --> 00:01:20,550
self join to join data in one table to another, or one table to itself rather.

21
00:01:20,550 --> 00:01:24,920
What I&#39;m going to do, because in the database that we&#39;ve got in the sample

22
00:01:24,920 --> 00:01:25,360
database,

23
00:01:25,360 --> 00:01:28,750
there isn&#39;t an employee table. So I&#39;m going to do a little bit of work first of all.

24
00:01:28,750 --> 00:01:31,850
So don&#39;t worry too much about the syntax. But the first thing I&#39;m doing... You need to change your

25
00:01:31,850 --> 00:01:32,830
context there.

26
00:01:32,830 --> 00:01:36,100
Oh yes, good point. Change myself to the,

27
00:01:36,100 --> 00:01:39,340
and that would&#39;ve worked in master database. Change myself to the 

28
00:01:39,340 --> 00:01:44,130
AdventureWorksLT database. And I&#39;m going to create a table called 

29
00:01:44,130 --> 00:01:47,260
SalesLT.Employee which has the EmployeeID,

30
00:01:47,260 --> 00:01:50,680
EmployeeName, and the manager which is an integer

31
00:01:50,680 --> 00:01:57,330
ID that I&#39;m going to use. So I&#39;ll go ahead and execute that.

32
00:01:57,330 --> 00:02:00,630
I&#39;m then going to insert some data, we&#39;re going to cover inserts later on in this course, 

33
00:02:00,630 --> 00:02:03,970
so don&#39;t worry too much about it. But I&#39;m taking some data from the existing

34
00:02:03,970 --> 00:02:08,090
Customers table, we have Salesperson in there. So I&#39;m going to grab the 

35
00:02:08,090 --> 00:02:09,759
Salesperson from the Customers table,

36
00:02:09,758 --> 00:02:13,170
and I&#39;m going to pour a bunch of them into the

37
00:02:13,170 --> 00:02:17,590
Employee table I&#39;ve just created. Then I&#39;m going to do some updates,

38
00:02:17,590 --> 00:02:21,160
effectively all this is doing is ending up with a bunch of employees the have

39
00:02:21,160 --> 00:02:21,970
managers

40
00:02:21,970 --> 00:02:26,300
that ultimately they report to. So if I go ahead and do all that. 

41
00:02:26,300 --> 00:02:29,940
And let&#39;s not worry too much about the details of the that.

42
00:02:29,940 --> 00:02:33,730
Let&#39;s actually just have a quick look and see what&#39;s in there. So if I do

43
00:02:33,730 --> 00:02:36,950
a quick SELECT

44
00:02:36,950 --> 00:02:41,290
* FROM 

45
00:02:41,290 --> 00:02:44,620
SalesLT.Employee

46
00:02:44,620 --> 00:02:51,620
 

47
00:02:54,500 --> 00:02:58,120
You can see that I&#39;ve got, each employee has a unique ID,

48
00:02:58,120 --> 00:03:03,690
and their name, and then the ManagerID  references the EmployeeID. So employee

49
00:03:03,690 --> 00:03:04,620
number one

50
00:03:04,620 --> 00:03:08,290
is managed by employee number eight. Employee number two is managed by employee

51
00:03:08,290 --> 00:03:09,020
number one.

52
00:03:09,020 --> 00:03:12,560
And the only one who doesn&#39;t have a manager, the person at the top of the pyramid,

53
00:03:12,560 --> 00:03:15,890
is employee number three. So we&#39;re able to

54
00:03:15,890 --> 00:03:18,930
see how that works. And that&#39;s  typically how you might organize your

55
00:03:18,930 --> 00:03:23,430
organization chart. To find out who reports to who, and to get some

56
00:03:23,430 --> 00:03:24,020
sort of

57
00:03:24,020 --> 00:03:27,910
details about that organization, I can use a self join. So, I&#39;m getting the EmployeeName

58
00:03:27,910 --> 00:03:28,470
 

59
00:03:28,470 --> 00:03:32,480
from the Employee table and the EmployeeName from another

60
00:03:32,480 --> 00:03:36,600
copy of that table which I&#39;m using to represent managers as the ManagerName.

61
00:03:36,600 --> 00:03:40,220
I&#39;m using a LEFT JOIN to join my employees.

62
00:03:40,220 --> 00:03:44,750
So why do you use a LEFT JOIN there? Because I&#39;m going to have some employees who don&#39;t have

63
00:03:44,750 --> 00:03:45,300
managers.

64
00:03:45,300 --> 00:03:48,930
At least I&#39;m going to have one employee that doesn&#39;t have a manager. So that top of the tree one. Top of the tree, I want

65
00:03:48,930 --> 00:03:50,020
to include him so

66
00:03:50,020 --> 00:03:53,170
I bring him out, he doesn&#39;t have a ManagerID, but I still want to include him.

67
00:03:53,170 --> 00:03:56,200
And I&#39;m bringing them out, I&#39;m joining on the 

68
00:03:56,200 --> 00:03:59,300
the e.ManagerID equals the m.EmployeeID.

69
00:03:59,300 --> 00:04:02,940
So referencing across to the same table, and if I go ahead and run that.

70
00:04:02,940 --> 00:04:07,140
And I actually get the EmployeeName and the name of their manager. And

71
00:04:07,140 --> 00:04:11,090
I can see that the employee doesn&#39;t have a manager. And then I can see the name of the

72
00:04:11,090 --> 00:04:15,310
the manager for each of the others. So that&#39;s a case where a self join is a

73
00:04:15,310 --> 00:04:19,959
a useful thing to be able to do. It&#39;s just the same as any other type a join except that

74
00:04:19,959 --> 00:04:24,760
you have to use aliases because you&#39;ve got the same table referenced multiple

75
00:04:24,760 --> 00:04:25,040
times.

