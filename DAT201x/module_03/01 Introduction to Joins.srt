0
00:00:05,190 --> 00:00:08,600
Welcome back to module three. So far in this course

1
00:00:08,600 --> 00:00:11,820
we&#39;ve looked at using the SELECT statement to query

2
00:00:11,820 --> 00:00:16,599
tables in our SQL Server database. And  Jeff you&#39;ve talked us through doing

3
00:00:16,599 --> 00:00:17,520
some fairly

4
00:00:17,520 --> 00:00:21,290
advanced SELECT techniques using ordering by and

5
00:00:21,290 --> 00:00:24,899
using the WHERE clause to filter the rows that are coming back. So we&#39;re getting

6
00:00:24,899 --> 00:00:25,840
to the point now

7
00:00:25,840 --> 00:00:29,960
writing some queries that bring back the data that we want from one table.

8
00:00:29,960 --> 00:00:33,260
But as I said right at the beginning of the court, actually most databases have

9
00:00:33,260 --> 00:00:34,250
multiple tables.

10
00:00:34,250 --> 00:00:38,379
So let&#39;s take a look at querying  multiple tables with joins in this module.

11
00:00:38,379 --> 00:00:41,609
And what we&#39;re going to cover,

12
00:00:41,609 --> 00:00:45,550
there&#39;s a number things to get through. We&#39;ll talk about some some basic concepts

13
00:00:45,550 --> 00:00:47,620
of what a join is and how they work.

14
00:00:47,620 --> 00:00:51,760
And then we&#39;ll look at the syntax that we use to define joins in our

15
00:00:51,760 --> 00:00:54,589
queries that join together data from one table to another.

16
00:00:54,589 --> 00:00:58,050
And then we&#39;ll look at four different types of joins the we can implement

17
00:00:58,050 --> 00:00:59,420
and Transact-SQL

18
00:00:59,420 --> 00:01:03,140
and understand the differences between each of those and when we would use which

19
00:01:03,140 --> 00:01:03,469
of

20
00:01:03,469 --> 00:01:07,980
when we would use each of them. So I want to start off with some basic

21
00:01:07,980 --> 00:01:11,200
join concepts. And now Jeff, you&#39;ve

22
00:01:11,200 --> 00:01:14,799
worked with their databases for many years, you&#39;ll kind of know about this.

23
00:01:14,799 --> 00:01:18,350
Never with more than one table though. Never with more than one table, oh well. Ok, you can leave for the

24
00:01:18,350 --> 00:01:19,060
rest the course then. &lt;laughter&gt;

25
00:01:19,060 --> 00:01:22,170
So it&#39;s actually fairly common.

26
00:01:22,170 --> 00:01:26,180
I mean in most cases you, sometimes you query one table, but actually is pretty

27
00:01:26,180 --> 00:01:27,060
common to be

28
00:01:27,060 --> 00:01:31,049
bringing together data from multiple tables. And what your doing is combining

29
00:01:31,049 --> 00:01:31,890
the rows

30
00:01:31,890 --> 00:01:36,469
by matching some, using some sort of criteria to match rows in one table to

31
00:01:36,469 --> 00:01:37,320
rows to another.

32
00:01:37,320 --> 00:01:40,700
So we&#39;re not querying one table and then querying another and then just

33
00:01:40,700 --> 00:01:45,079
sticking the results together as one result set. There are times when we will

34
00:01:45,079 --> 00:01:47,149
do that and we&#39;ll talk about that later module.

35
00:01:47,149 --> 00:01:51,100
But here what we&#39;re talking about here is matching rows in one table

36
00:01:51,100 --> 00:01:54,430
with corresponding rows in another table and mixing them together.

37
00:01:54,430 --> 00:01:59,770
So it is usually based not always be usually based on a relationship between

38
00:01:59,770 --> 00:02:01,079
a primary key

39
00:02:01,079 --> 00:02:05,270
and a foreign key. You may remember at the beginning of the course, I talked about

40
00:02:05,270 --> 00:02:07,130
a primary key as being

41
00:02:07,130 --> 00:02:10,300
the thing that uniquely identifies a row 

42
00:02:10,300 --> 00:02:15,090
in a particular table. So give an example a primary key. So order

43
00:02:15,090 --> 00:02:15,590
ID,

44
00:02:15,590 --> 00:02:18,830
or customer ID, they often end in to be honest.

45
00:02:18,830 --> 00:02:22,930
Yes that&#39;s true, yes.  Usually, sometimes the primary key can be 

46
00:02:22,930 --> 00:02:26,820
multiple columns. It can be a  composite key like an order and

47
00:02:26,820 --> 00:02:28,100
line item might

48
00:02:28,100 --> 00:02:32,550
together make a unique identifier. But more often than not in tables that

49
00:02:32,550 --> 00:02:36,490
represent business entities, you have a  unique value that identifies that as the

50
00:02:36,490 --> 00:02:37,150
primary key.

51
00:02:37,150 --> 00:02:40,880
When you have another table that references that

52
00:02:40,880 --> 00:02:44,670
thing. So let&#39;s say the thing is a customer. We have a customer ID that&#39;s the primary

53
00:02:44,670 --> 00:02:45,640
key for a customer.

54
00:02:45,640 --> 00:02:50,500
In an order table, a sales order table, I don&#39;t want to have customer&#39;s name, or

55
00:02:50,500 --> 00:02:53,070
the customer&#39;s address, or all the detail in there. I just want to say

56
00:02:53,070 --> 00:02:57,720
this customer over in the customer table. And so what I do is I put the

57
00:02:57,720 --> 00:02:58,600
customer key.

58
00:02:58,600 --> 00:03:02,330
And in that case it&#39;s called a foreign key because it&#39;s not uniquely identifying

59
00:03:02,330 --> 00:03:06,500
the sales order, it&#39;s the key that uniquely identifies the customer for the

60
00:03:06,500 --> 00:03:08,750
sales order. So

61
00:03:08,750 --> 00:03:11,990
we have these primary key, foreign key relationships. That&#39;s not the only

62
00:03:11,990 --> 00:03:14,340
thing we can join on but it&#39;s the most common.

63
00:03:14,340 --> 00:03:19,150
An example on the slide there is returning rows the combine data from

64
00:03:19,150 --> 00:03:20,170
the employee

65
00:03:20,170 --> 00:03:23,430
and sales order table. You might have employees if your sales organization,

66
00:03:23,430 --> 00:03:26,959
some your employees will be salespeople and you want to figure out which are the

67
00:03:26,959 --> 00:03:29,500
orders that that sales person is responsible 

68
00:03:29,500 --> 00:03:32,980
for selling. And we match it based on the employee ID 

69
00:03:32,980 --> 00:03:36,670
relationship. Now it can be useful when you are

70
00:03:36,670 --> 00:03:40,160
thinking about sets of data. Remember right at the beginning we said that

71
00:03:40,160 --> 00:03:45,110
a lot of the things we&#39;ll do in Transact-SQL is based on set based theory

72
00:03:45,110 --> 00:03:45,600
or

73
00:03:45,600 --> 00:03:49,950
set theory in mathematics. It&#39;s useful, especially when your thinking about joins, to

74
00:03:49,950 --> 00:03:52,830
think of things in terms of venn diagrams. Collections of things.

75
00:03:52,830 --> 00:03:57,160
So in this case we have a collection of employees and we have a collection of

76
00:03:57,160 --> 00:03:58,070
sales orders.

77
00:03:58,070 --> 00:04:02,520
And where those two sets overlap, where they intersect, we&#39;ve got

78
00:04:02,520 --> 00:04:06,790
the sales orders that were taken by employees. So any employees you haven&#39;t sold

79
00:04:06,790 --> 00:04:07,400
anything,

80
00:04:07,400 --> 00:04:11,030
they won&#39;t be in that set, in that join by default. We&#39;ll talk about 

81
00:04:11,030 --> 00:04:11,790
different ways of

82
00:04:11,790 --> 00:04:15,310
creating joins. But think of venn diagrams. It&#39;s a really useful way of

83
00:04:15,310 --> 00:04:16,579
just kind of visualizing in your head

84
00:04:16,579 --> 00:04:20,250
how you&#39;re joining things together. And  as you&#39;ll see as we progress through

85
00:04:20,250 --> 00:04:20,889
the module,

86
00:04:20,889 --> 00:04:26,090
it will help you figure which types of  joins your trying to define. So

87
00:04:26,090 --> 00:04:29,259
let&#39;s look at the syntax that we would use when we&#39;re

88
00:04:29,259 --> 00:04:33,699
working on this in Transact-SQL. There&#39;s actually two very,

89
00:04:33,699 --> 00:04:36,819
two variants of how we can do this. Two ways that you can express joins.

90
00:04:36,819 --> 00:04:40,460
In 92, the ANSI standard 

91
00:04:40,460 --> 00:04:45,199
for making joins was actually to  do this join operator in the

92
00:04:45,199 --> 00:04:47,090
the FROM clause. We created a standard

93
00:04:47,090 --> 00:04:50,099
in the FROM clause we have the join keyword.

94
00:04:50,099 --> 00:04:53,009
And there&#39;s other keywords that go with  that which we&#39;ll talk about a minute. But

95
00:04:53,009 --> 00:04:55,259
basically we&#39;re saying SELECT whatever FROM

96
00:04:55,259 --> 00:04:58,770
this table JOIN this other table

97
00:04:58,770 --> 00:05:01,930
on and then whatever is we&#39;re using to join them, whatever the predicate. So

98
00:05:01,930 --> 00:05:04,080
we&#39;re explicitly joining in the FROM clause.

99
00:05:04,080 --> 00:05:08,460
There is an older syntax that was a standard for a while

100
00:05:08,460 --> 00:05:12,139
where we did in the WHERE clause. We saw the WHERE clause in the previous module

101
00:05:12,139 --> 00:05:13,800
and it&#39;s used to filter rows.

102
00:05:13,800 --> 00:05:17,550
Well you can think about joining as being something that filters rows. It

103
00:05:17,550 --> 00:05:21,159
filtering rows where there is a match between the two tables. SO

104
00:05:21,159 --> 00:05:24,949
you will find code out there were the join is done in the WHERE clause.

105
00:05:24,949 --> 00:05:28,110
My recommendation to you, and I&#39;m

106
00:05:28,110 --> 00:05:31,639
sure you would agree Jeff, it that&#39;s probably not the best place to do it.

107
00:05:31,639 --> 00:05:34,699
No, as you&#39;ll Cartesian products later on,

108
00:05:34,699 --> 00:05:37,930
intentional Cartesian products, but you don&#39;t want accidental ones.

109
00:05:37,930 --> 00:05:41,469
The sad thing is that we&#39;re both old enough to remember when SQL-92 was

110
00:05:41,469 --> 00:05:42,479
the new thing.

111
00:05:42,479 --> 00:05:45,659
That&#39;s right, I remember that was the new syntax. 

112
00:05:45,659 --> 00:05:48,990
Yes, so we went over, we really want to  avoid using the older syntax, avoid doing

113
00:05:48,990 --> 00:05:52,979
the join in the WHERE clause. Try and use a join, explicitly a JOIN statement in

114
00:05:52,979 --> 00:05:53,690
the FROM 

115
00:05:53,690 --> 00:05:56,860
clause. Cartesian products has been mentioned there.

116
00:05:56,860 --> 00:05:59,900
Since you mentioned it Jeff, I&#39;ll give you the honor.

117
00:05:59,900 --> 00:06:04,539
What&#39;s a Cartesian product? So, I don&#39;t know where the name 

118
00:06:04,539 --> 00:06:05,849
comes from because I think of it as meaning some sort of well

119
00:06:05,849 --> 00:06:09,020
in ancient times. Artisan is it?

120
00:06:09,020 --> 00:06:12,409
I thought that was a type of product at a delicatessen. &lt;laughter&gt;

121
00:06:12,409 --> 00:06:15,889
Anyway, Cartesian products, basically saying every combination. So

122
00:06:15,889 --> 00:06:19,520
I&#39;m not interested in employees and

123
00:06:19,520 --> 00:06:22,759
their sales. If I do a Cartesian product, I have

124
00:06:22,759 --> 00:06:26,310
every employee against every single order. So

125
00:06:26,310 --> 00:06:29,930
every single combination. Now it could be that that&#39;s a useful thing.

126
00:06:29,930 --> 00:06:35,349
It almost certainly isn&#39;t a useful thing, the only relevant example I

127
00:06:35,349 --> 00:06:39,699
ever come out with tends to be, if you have a thing at work and they

128
00:06:39,699 --> 00:06:42,879
assessed you in various categories. And they say so your

129
00:06:42,879 --> 00:06:47,009
skills management are this, skills at  sales are this, and then you get a number. And then we have all the

130
00:06:47,009 --> 00:06:48,349
roles in our company,

131
00:06:48,349 --> 00:06:51,889
and we say well this is the required skill level in these different

132
00:06:51,889 --> 00:06:53,509
categories for this role.

133
00:06:53,509 --> 00:06:57,099
Then what we could do is search our employees

134
00:06:57,099 --> 00:07:02,120
to find any employee that meets the requirements for a role that&#39;s become available.

135
00:07:02,120 --> 00:07:05,729
So to do that, I want every combination of rows available

136
00:07:05,729 --> 00:07:10,130
and employees, and then filter it to say I&#39;m only interested if

137
00:07:10,130 --> 00:07:13,669
the employee has greater than or equal to value

138
00:07:13,669 --> 00:07:17,409
than the requirement in the job. So you&#39;re matching every employee to ever row

139
00:07:17,409 --> 00:07:19,169
and then filtering. And I actually want ever row.

140
00:07:19,169 --> 00:07:23,639
I know it&#39;s quite unusual. Yeah, the only other one that I can think of is if you

141
00:07:23,639 --> 00:07:24,000
were,

142
00:07:24,000 --> 00:07:28,199
if you have a table of say soccer teams, and you&#39;re trying to arrange a

143
00:07:28,199 --> 00:07:30,199
tournament where every team plays every other team.

144
00:07:30,199 --> 00:07:33,690
You would in that case you would do a Cartesian join that&#39;s actually a self join on

145
00:07:33,690 --> 00:07:34,560
the one table,

146
00:07:34,560 --> 00:07:38,569
which we&#39;ll get to it a short while, where we get every possible combination.

147
00:07:38,569 --> 00:07:42,889
And in fact, you would get home and away because it would have combinations that

148
00:07:42,889 --> 00:07:48,300
work in both directions. So Cartesian products, it&#39;s a shorthand way of 

149
00:07:48,300 --> 00:07:50,599
saying every possible combination of the records. And

150
00:07:50,599 --> 00:07:53,770
the reality is, apart from these fairly esoteric examples, it&#39;s

151
00:07:53,770 --> 00:07:57,229
generally not something you want. It&#39;s very easy to get them accidentally if

152
00:07:57,229 --> 00:07:57,830
you use

153
00:07:57,830 --> 00:08:01,159
WHERE clause to define your join. Whereas

154
00:08:01,159 --> 00:08:04,529
the join keyword in the FROM clause is much more explicit

155
00:08:04,529 --> 00:08:06,630
and it&#39;s easier to avoid getting these Cartesian products.

