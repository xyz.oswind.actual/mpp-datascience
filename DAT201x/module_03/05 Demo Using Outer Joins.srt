0
00:00:02,320 --> 00:00:05,970
Let&#39;s see a demo of an OUTER JOIN and see how that works.

1
00:00:05,970 --> 00:00:09,240
So in this case I am taking the

2
00:00:09,240 --> 00:00:14,679
first and last name from the Customer table which I&#39;m LEFT OUTER joining to the

3
00:00:14,679 --> 00:00:15,960
SalesOrderHeader table.

4
00:00:15,960 --> 00:00:19,350
So I&#39;m bringing back customers who have

5
00:00:19,350 --> 00:00:23,940
bought something and customers who have never bought something. Which might

6
00:00:23,940 --> 00:00:27,250
sound a bit odd, how would they be a customer? In this case in

7
00:00:27,250 --> 00:00:30,830
the sample data that we&#39;ve got, we have customers who&#39;ve perhaps registered with us.

8
00:00:30,830 --> 00:00:31,500
 

9
00:00:31,500 --> 00:00:35,019
We sell to retailers, so retail companies may have signed up with us

10
00:00:35,019 --> 00:00:38,940
but not place any orders yet. And they would be, I want them to be included.

11
00:00:38,940 --> 00:00:41,989
So I&#39;m bringing those back, I&#39;m LEFT OUTER joining

12
00:00:41,989 --> 00:00:45,129
on the CustomerID equalling the OrderHeaderID.

13
00:00:45,129 --> 00:00:50,320
So let&#39;s run that, and once again I&#39;m in the wrong

14
00:00:50,320 --> 00:00:56,039
database. So we&#39;ll do that. You can see how easy it is to do that. So you can see I&#39;m bringing back,

15
00:00:56,039 --> 00:00:58,069
and I&#39;ve got a bunch of customers

16
00:00:58,069 --> 00:01:01,309
with no corresponding sales order. So these will be from my

17
00:01:01,309 --> 00:01:05,089
LEFT OUTER join, I&#39;m getting them from the Customer table, but they haven&#39;t placed

18
00:01:05,089 --> 00:01:06,280
any sales orders.

19
00:01:06,280 --> 00:01:12,200
And if I scroll down through the result set, there&#39;s a few popping in here and

20
00:01:12,200 --> 00:01:13,180
there. I can see a

21
00:01:13,180 --> 00:01:17,170
customer there who has placed an order. So

22
00:01:17,170 --> 00:01:20,710
I&#39;m getting the ones that match, and all of  the customers that don&#39;t match.

23
00:01:20,710 --> 00:01:25,490
I am &lt;not&gt; in this case getting any orders that don&#39;t have customers which

24
00:01:25,490 --> 00:01:29,610
logically wouldn&#39;t make sense in this context anyway. But it&#39;s a LEFT OUTER.

25
00:01:29,610 --> 00:01:32,140
So the only table where I&#39;m getting all the rows from

26
00:01:32,140 --> 00:01:35,490
is the Employee table, and I&#39;m getting rows from the Customer table were

27
00:01:35,490 --> 00:01:36,260
there&#39;s a match.

28
00:01:36,260 --> 00:01:40,400
So one of the useful things you can do with

29
00:01:40,400 --> 00:01:44,630
OUTER JOINs is actually check for the null values that get returned.

30
00:01:44,630 --> 00:01:48,420
So if I wanted to find a list of the customers who haven&#39;t bought anything,

31
00:01:48,420 --> 00:01:52,760
all I do is I append an additional WHERE clause on to that query

32
00:01:52,760 --> 00:01:57,620
that says where the sales order number is null. I know that every sales order has a sales

33
00:01:57,620 --> 00:01:58,420
order number,

34
00:01:58,420 --> 00:02:02,330
it&#39;s a non-nullable column. So the only way it would be null would be if there was a

35
00:02:02,330 --> 00:02:02,980
customer

36
00:02:02,980 --> 00:02:08,009
without any corresponding sales. And if I run that, what I get is

37
00:02:08,008 --> 00:02:12,280
815 rows. So I&#39;ve 815 customers who&#39;ve never bought anything from me.

38
00:02:12,280 --> 00:02:15,540
Clearly there are some work to be done on the sales side of the business.

39
00:02:15,540 --> 00:02:19,930
Now, it gets a bit more interesting if I have multiple

40
00:02:19,930 --> 00:02:23,560
tables that I want to join using OUTER JOINs. So

41
00:02:23,560 --> 00:02:28,260
you can see here that I want to get the product name and the sales order number

42
00:02:28,260 --> 00:02:33,640
from the Product table LEFT JOIN to the SalesOrderDetail table which is where that

43
00:02:33,640 --> 00:02:37,470
product information is stored. Which in turn is going to be joined to the 

44
00:02:37,470 --> 00:02:38,460
SalesOrderHeader table.

45
00:02:38,460 --> 00:02:43,470
Now, if I think about this logically, I&#39;m not going to have any order details that

46
00:02:43,470 --> 00:02:47,090
don&#39;t have order headers. So you would think well that should just be an INNER

47
00:02:47,090 --> 00:02:47,560
JOIN.

48
00:02:47,560 --> 00:02:50,980
But I can&#39;t make it an INNER JOIN because of the way that the

49
00:02:50,980 --> 00:02:53,980
result set is actually formulated. What you would get is

50
00:02:53,980 --> 00:02:58,210
initially a Cartesian join and then we&#39;re filtering rules out the don&#39;t match.

51
00:02:58,210 --> 00:03:02,850
And because I&#39;ve got a left join here in that first one,

52
00:03:02,850 --> 00:03:05,950
the next table that I join onto the right of

53
00:03:05,950 --> 00:03:10,040
my chain of tables is going to have to join onto a table that may contain nulls

54
00:03:10,040 --> 00:03:10,550
because

55
00:03:10,550 --> 00:03:14,960
the result of that first join is a LEFT JOIN. And I don&#39;t want to filter out the rows

56
00:03:14,960 --> 00:03:15,960
have got nulls because 

57
00:03:15,960 --> 00:03:20,170
that was the whole point of having the LEFT JOIN. I want to have the products that haven&#39;t been sold.

58
00:03:20,170 --> 00:03:23,930
So once you&#39;ve declared a LEFT JOIN or a RIGHT JOIN,

59
00:03:23,930 --> 00:03:28,960
if you then continue to add tables onto the chain of tables, so if you&#39;ve got a

60
00:03:28,960 --> 00:03:29,770
LEFT JOIN 

61
00:03:29,770 --> 00:03:33,520
and you add another table to the right, you have to keep using LEFT JOINs

62
00:03:33,520 --> 00:03:38,020
is basically the rule. So in this case, I&#39;ve got a LEFT JOIN to join

63
00:03:38,020 --> 00:03:42,460
the SalesOrderDetail table to the Product table, and then another LEFT JOIN

64
00:03:42,460 --> 00:03:42,980
to

65
00:03:42,980 --> 00:03:46,260
join the OrderHeader to the Product table. So that I retain those nulls

66
00:03:46,260 --> 00:03:50,240
that indicate products that haven&#39;t been sold. So quickly drawing out a venn diagram would

67
00:03:50,240 --> 00:03:54,570
help you decide maybe which one?

68
00:03:54,570 --> 00:03:58,940
It would help me me decide. Right, yeah. So if you think about that in a venn diagram it would help you to

69
00:03:58,940 --> 00:04:01,670
to figure out which order your tables are in and whether you need to keep the

70
00:04:01,670 --> 00:04:05,470
left or the right. Because it&#39;s possible you might do an INNER JOIN in the second one I guess, but

71
00:04:05,470 --> 00:04:10,140
unlikely. Yes, well if you did an INNER JOIN you would lose all of the products that you&#39;d never

72
00:04:10,140 --> 00:04:10,670
sold.

73
00:04:10,670 --> 00:04:15,470
Yes. So if we run that, you can see that I get my

74
00:04:15,470 --> 00:04:18,930
products there that I&#39;ve never sold,  there&#39;s no sales order number for those.

75
00:04:18,930 --> 00:04:22,140
But I continue to get my details of

76
00:04:22,140 --> 00:04:22,980
the

77
00:04:22,980 --> 00:04:26,230
products that I have sold. So I&#39;m getting from the Product table here and the sales order

78
00:04:26,230 --> 00:04:27,080
number here.

79
00:04:27,080 --> 00:04:30,500
And earlier on you asked about what if that middle table I didn&#39;t want

80
00:04:30,500 --> 00:04:33,040
any columns from, well it still had to be joined because

81
00:04:33,040 --> 00:04:36,060
it&#39;s necessary to get from Products to SalesOrderDetails

82
00:04:36,060 --> 00:04:39,130
to go through, sorry, to get from Products to orders

83
00:04:39,130 --> 00:04:42,440
to go through an order detail, I&#39;m just not bringing back any columns from there. Yep.

84
00:04:42,440 --> 00:04:45,700
So

85
00:04:45,700 --> 00:04:49,190
here&#39;s another example that has multiple tables but this time

86
00:04:49,190 --> 00:04:53,900
the order of the tables is different. I&#39;m selecting from the Product table,

87
00:04:53,900 --> 00:04:58,200
I&#39;m LEFT OUTER joining the SalesOrderDetail table because I want products

88
00:04:58,200 --> 00:05:02,700
that have never sold, and I&#39;m LEFT OUTER joining again to the order header table

89
00:05:02,700 --> 00:05:03,660
because I want the

90
00:05:03,660 --> 00:05:07,650
the order number. And I&#39;m having to use the LEFT OUTER join because I&#39;m continuing to add

91
00:05:07,650 --> 00:05:09,610
tables to the right of where I started

92
00:05:09,610 --> 00:05:14,390
a LEFT OUTER join. But then I&#39;m inner joining the ProductCategory

93
00:05:14,390 --> 00:05:18,500
and it&#39;s because I&#39;m joining that back to the first table again. Back to

94
00:05:18,500 --> 00:05:20,300
the ProductCategory table,

95
00:05:20,300 --> 00:05:23,710
sorry, back to the Product table, p.

96
00:05:23,710 --> 00:05:28,570
So because I&#39;m joining that back to the first one I started with,

97
00:05:28,570 --> 00:05:31,700
if you think about these tables as being in order from

98
00:05:31,700 --> 00:05:35,660
left to right, this one was back again to the very beginning at the chain, back

99
00:05:35,660 --> 00:05:36,290
to the left.

100
00:05:36,290 --> 00:05:39,390
So it&#39;s before the outer joins so I can use an INNER JOIN.

101
00:05:39,390 --> 00:05:43,310
And this is where the syntax gets confusing. Whether you need to continue

102
00:05:43,310 --> 00:05:45,200
using an OUTER JOIN depends on

103
00:05:45,200 --> 00:05:48,760
which side of the list of current tables are you adding this new table.

104
00:05:48,760 --> 00:05:54,180
So it might be nice to reorder, yeah I could reorder the joins to do it differently.

105
00:05:54,180 --> 00:05:55,950
Yeah I could INNER JOIN that one first

106
00:05:55,950 --> 00:05:59,570
and do the LEFT OUTER later, but it kind of makes the point that

107
00:05:59,570 --> 00:06:03,410
sometimes you&#39;ll find yourself writing a query, and logically you know that there&#39;s

108
00:06:03,410 --> 00:06:07,060
no way there&#39;s going to be non-matching rows, so you might as well

109
00:06:07,060 --> 00:06:08,100
use an INNER JOIN.

110
00:06:08,100 --> 00:06:10,850
But because you&#39;ve already used an OUTER JOIN, and this one&#39;s being added

111
00:06:10,850 --> 00:06:11,440
to the

112
00:06:11,440 --> 00:06:15,830
the right or left of that join, you&#39;ll have to use another OUTER JOIN.

113
00:06:15,830 --> 00:06:20,900
If I run that, I can see that it works, I get the ProductName and

114
00:06:20,900 --> 00:06:24,180
the Category. That was an INNER JOIN between products and categories.

115
00:06:24,180 --> 00:06:27,760
And then a couple of OUTER JOINs to get me to the SalesOrderNumber.

116
00:06:27,760 --> 00:06:32,550
And I can see that for my Road Frames here, I haven&#39;t sold any.

117
00:06:32,550 --> 00:06:34,560
But for the other products that are listed there

118
00:06:34,560 --> 00:06:34,810
I have.

