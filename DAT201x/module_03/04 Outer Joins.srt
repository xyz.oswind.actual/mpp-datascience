0
00:00:02,860 --> 00:00:06,210
Okay so we&#39;ve looked at inner joins, let&#39;s talk a little bit about it

1
00:00:06,210 --> 00:00:11,370
outer joins. And an outer join is when we return all the rows from one table

2
00:00:11,370 --> 00:00:16,170
and any matching rows from the second table. But from the first table I want

3
00:00:16,170 --> 00:00:19,349
all of the rows regardless of whether they&#39;re matching or not.

4
00:00:19,349 --> 00:00:23,819
So an example this might be where I&#39;ve got employees and orders and I want a list of

5
00:00:23,819 --> 00:00:25,009
all the employees

6
00:00:25,009 --> 00:00:27,899
and for the ones who&#39;ve taken orders I want details of the orders that 

7
00:00:27,899 --> 00:00:28,659
they&#39;ve taken.

8
00:00:28,659 --> 00:00:32,029
Now what happens here as I said

9
00:00:32,029 --> 00:00:35,810
and earlier on that what SQL Server does is actually create a Cartesian

10
00:00:35,810 --> 00:00:39,580
product before it filters out the results in the join. So in this case what it does is

11
00:00:39,580 --> 00:00:40,880
create that Cartesian product

12
00:00:40,880 --> 00:00:44,660
and preserve all the ones from whichever table it is that I&#39;ve said

13
00:00:44,660 --> 00:00:48,610
is the outer table. And then filters out the ones that don&#39;t match in the inner

14
00:00:48,610 --> 00:00:49,140
table.

15
00:00:49,140 --> 00:00:52,530
So that&#39;s kind of the way it works. And you specify

16
00:00:52,530 --> 00:00:55,530
an OUTER JOIN using LEFT,

17
00:00:55,530 --> 00:01:00,829
RIGHT, or FULL. And in this case the order in which I specify the tables does

18
00:01:00,829 --> 00:01:04,129
kind of matter because if it&#39;s a LEFT  OUTER JOIN then

19
00:01:04,129 --> 00:01:07,530
I&#39;m going to retain all the rows from the first table I mention, the one that&#39;s on

20
00:01:07,530 --> 00:01:10,060
the left of the statement. If I&#39;m

21
00:01:10,060 --> 00:01:13,740
am specifying the RIGHT OUTER JOIN, then I&#39;m keeping all the rows from the

22
00:01:13,740 --> 00:01:17,329
table on the right. And if I use a FULL OUTER JOIN, 

23
00:01:17,329 --> 00:01:20,999
I&#39;m keeping the rows from both of those tables.

24
00:01:20,999 --> 00:01:24,740
FULL OUTER JOINs are pretty unusual. You don&#39;t commonly use them,

25
00:01:24,740 --> 00:01:27,770
but what you might use it for is if you wanted to get

26
00:01:27,770 --> 00:01:32,369
all of the customers and all of the products

27
00:01:32,369 --> 00:01:37,090
and where you had sold, not all of the customers -- all of the employees and all of the products,

28
00:01:37,090 --> 00:01:40,729
and where you had sold things you&#39;d want to indicate that they&#39;ve been sold.

29
00:01:40,729 --> 00:01:44,299
But you&#39;d still want products that hadn&#39;t been sold and you&#39;d still want employees I haven&#39;t sold

30
00:01:44,299 --> 00:01:44,909
anything.

31
00:01:44,909 --> 00:01:50,600
So you get that full result set. So

32
00:01:50,600 --> 00:01:54,049
matches from the other table, the table is not the left or right the outer

33
00:01:54,049 --> 00:01:54,939
table that we defined.

34
00:01:54,939 --> 00:01:59,869
are retrieved. Additional rows are added to the results for any non-matches. 

35
00:01:59,869 --> 00:02:03,749
So if I&#39;ve got employees that I&#39;ve brought back as an outer table

36
00:02:03,749 --> 00:02:08,110
and sales as the inner table, and there are employees who haven&#39;t had any sales,

37
00:02:08,110 --> 00:02:11,620
then any of the columns that are going to be brought back from the

38
00:02:11,620 --> 00:02:16,100
sales table, of course there aren&#39;t any matching columns for those in the

39
00:02:16,100 --> 00:02:18,020
employees, so we get nulls for those.

40
00:02:18,020 --> 00:02:22,160
So basically nulls when you&#39;re working with joins indicate either that there were nulls

41
00:02:22,160 --> 00:02:23,240
in the underlying data

42
00:02:23,240 --> 00:02:25,950
or they indicate that this is from outer table that doesn&#39;t have a

43
00:02:25,950 --> 00:02:26,940
matching inner table.

44
00:02:26,940 --> 00:02:30,270
So we&#39;ve nulled out any of the  columns from the corresponding

45
00:02:30,270 --> 00:02:30,730
inner table.

46
00:02:30,730 --> 00:02:35,740
I said in the beginning when we talked about joins, that it&#39;s useful to  conceptualize these as venn

47
00:02:35,740 --> 00:02:38,700
diagrams and especially when you&#39;re working with outer joins it&#39;s useful to

48
00:02:38,700 --> 00:02:39,780
think of it as being

49
00:02:39,780 --> 00:02:42,810
like a venn diagram, a set diagram that shows

50
00:02:42,810 --> 00:02:46,880
here&#39;s my outer join that joins employees

51
00:02:46,880 --> 00:02:50,700
to sales orders. But I want all the employees regardless of whether they

52
00:02:50,700 --> 00:02:51,340
they&#39;ve sold any

53
00:02:51,340 --> 00:02:55,330
products or not. So my left outer join, and you see that again the

54
00:02:55,330 --> 00:03:00,490
the outer is optional, we don&#39;t have to specify that the outer. 

55
00:03:00,490 --> 00:03:04,260
I&#39;ve put it in there just for fullness. Because if your putting left or right then

56
00:03:04,260 --> 00:03:08,110
it&#39;s outer whatever. Yes, if I put LEFT JOIN, then it knows it&#39;s an outer join.

57
00:03:08,110 --> 00:03:11,110
So I&#39;ve left joined the

58
00:03:11,110 --> 00:03:15,140
order table to the employee table and because employee is mentioned first,

59
00:03:15,140 --> 00:03:18,530
it&#39;s the one on the left, so I&#39;m going to have all of the employees

60
00:03:18,530 --> 00:03:22,840
and that the sales orders that match. And where the sale orders don&#39;t match,

61
00:03:22,840 --> 00:03:25,550
I&#39;m going to get null values for the columns that come from that table.

