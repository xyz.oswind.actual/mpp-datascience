0
00:00:02,480 --> 00:00:06,519
So that&#39;s the end of our module on joins and querying multiple tables.

1
00:00:06,519 --> 00:00:09,910
It&#39;s a fairly commonly done thing

2
00:00:09,910 --> 00:00:13,540
when you&#39;re working with databases is to use joins to query multiple tables.

3
00:00:13,540 --> 00:00:17,260
Sometimes they&#39;ll be inner joins, quite a lot of the time they&#39;ll be inner joins actually.

4
00:00:17,260 --> 00:00:21,539
Sometimes they&#39;ll be outer joins. And sometimes they&#39;ll be combinations of inner

5
00:00:21,539 --> 00:00:23,810
and outer joins. And that&#39;s when you have to be careful about

6
00:00:23,810 --> 00:00:26,500
the order. It&#39;s pretty much the only time you have to worry about the order that you

7
00:00:26,500 --> 00:00:27,270
specify

8
00:00:27,270 --> 00:00:31,930
the clauses in. Crossed joins we talked about bringing back the 

9
00:00:31,930 --> 00:00:36,320
Cartesian product. Again just to emphasize, it&#39;s not something you&#39;ll do a lot but it&#39;s good

10
00:00:36,320 --> 00:00:37,310
to be aware of that

11
00:00:37,310 --> 00:00:41,149
ability to bring back every possible combination. And self joins,

12
00:00:41,149 --> 00:00:44,760
just like any other join except the the tables you are joining together, it&#39;s the

13
00:00:44,760 --> 00:00:46,630
same table with different aliases

14
00:00:46,630 --> 00:00:49,770
to get back self referencing data.

15
00:00:49,770 --> 00:00:53,460
So hopefully you&#39;ve found that module useful.

16
00:00:53,460 --> 00:00:57,489
Please go away and do the lab, have a crack at doing that lab.

17
00:00:57,489 --> 00:01:01,329
Again it again will challenge you to create multiple queries that 

18
00:01:01,329 --> 00:01:04,299
have joins from various different tables and will challenge you to do

19
00:01:04,299 --> 00:01:07,900
different types of join. See how you get on with that, there are some solution files

20
00:01:07,900 --> 00:01:09,560
provided so you can check your answers.

21
00:01:09,560 --> 00:01:13,540
And when you&#39;re comfortable and am happy that you understand how you&#39;re using

22
00:01:13,540 --> 00:01:14,890
joins in Transact-SQL,

23
00:01:14,890 --> 00:01:17,939
come back and join us for the next module where we&#39;ll look at a different

24
00:01:17,939 --> 00:01:20,619
way of bringing multiple sets of results together

25
00:01:20,619 --> 00:01:24,250
when we look at using UNION to bring together different sets of data.

