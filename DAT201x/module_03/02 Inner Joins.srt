0
00:00:02,000 --> 00:00:05,120
So we talked about different types of joins. I mentioned a

1
00:00:05,120 --> 00:00:08,800
self join and we&#39;ll talk about other types of joins later on. But the first

2
00:00:08,800 --> 00:00:10,650
one, the most common type of join that people want to

3
00:00:10,650 --> 00:00:14,120
deal with or start with, is what we call on inner join.

4
00:00:14,120 --> 00:00:17,130
And again we want to think back to the idea of the

5
00:00:17,130 --> 00:00:20,550
venn diagrams. So we&#39;re returning rows, where only,

6
00:00:20,550 --> 00:00:24,630
only rows we&#39;re a match is found in both tables. So where there&#39;s an employee

7
00:00:24,630 --> 00:00:28,500
that matches a sale. I might have sales that don&#39;t have employees attached to them

8
00:00:28,500 --> 00:00:28,990
because

9
00:00:28,990 --> 00:00:32,070
I might have a store where customers just take things off the shelf and there is a

10
00:00:32,070 --> 00:00:33,270
sales person involved.

11
00:00:33,270 --> 00:00:36,740
Or I might have employees who have never sold something. And I&#39;m not interested any

12
00:00:36,740 --> 00:00:39,550
them. I&#39;m only interested in when an employee comes together with 

13
00:00:39,550 --> 00:00:42,840
a sales order. So 

14
00:00:42,840 --> 00:00:46,950
the way to think of that, as I said if you think about this conceptualized idea

15
00:00:46,950 --> 00:00:48,040
of the venn diagram,

16
00:00:48,040 --> 00:00:51,590
that looks like that then. An inner join is the bit where,

17
00:00:51,590 --> 00:00:54,970
the overlapping areas is where I&#39;m finding the equality. So, I&#39;ve got an

18
00:00:54,970 --> 00:00:55,710
employee and

19
00:00:55,710 --> 00:00:59,230
a sales order and both of those things exist.

20
00:00:59,230 --> 00:01:03,260
When we do it using an equal

21
00:01:03,260 --> 00:01:07,939
or an equality predicate, which is most of the time if I&#39;m honest,

22
00:01:07,939 --> 00:01:12,020
you&#39;ll occasionally see it referred to as an equi-join. And some people use the

23
00:01:12,020 --> 00:01:14,240
terms interchangeably, you&#39;ve got to  be quite careful.

24
00:01:14,240 --> 00:01:17,280
Explicitly an equi-join is when we&#39;re checking on equality.

25
00:01:17,280 --> 00:01:21,119
An inner join is any case where the thing is in both tables.

26
00:01:21,119 --> 00:01:24,479
I think you tend to, if is not equal to

27
00:01:24,479 --> 00:01:27,539
you would make it clear. Yeah because

28
00:01:27,539 --> 00:01:30,999
by default, virtually everyone you see will be equal to.

29
00:01:30,999 --> 00:01:36,249
Yeah, exactly. So yeah, so that&#39;s an inner join that we&#39;ve got an example

30
00:01:36,249 --> 00:01:37,600
there, you can see that the code

31
00:01:37,600 --> 00:01:40,859
example we&#39;re taking the first name and the order amount.

32
00:01:40,859 --> 00:01:43,990
Notice that because I&#39;m getting from two tables,

33
00:01:43,990 --> 00:01:47,209
now previously we&#39;ve used aliases for columns,

34
00:01:47,209 --> 00:01:50,939
here I&#39;m using aliases for the tables as well.

35
00:01:50,939 --> 00:01:54,859
So I could have column aliases and table aliases, and the advantage of

36
00:01:54,859 --> 00:01:55,359
having the

37
00:01:55,359 --> 00:02:00,329
the table alias is I can then explicitly say which table a column comes from.

38
00:02:00,329 --> 00:02:04,200
So in this case emp.FirstName is the  first name column from the employee

39
00:02:04,200 --> 00:02:04,819
table.

40
00:02:04,819 --> 00:02:08,060
And the amount order from the order table

41
00:02:08,060 --> 00:02:11,269
from employees which is joined

42
00:02:11,269 --> 00:02:14,700
to the sales order detail table.

43
00:02:14,700 --> 00:02:17,769
Now you&#39;ll notice I&#39;ve put the inner in square brackets there.

44
00:02:17,769 --> 00:02:21,019
And that&#39;s because like much of SQL we&#39;ve talked about 

45
00:02:21,019 --> 00:02:24,930
before the inner is actually optional. If I just put join,

46
00:02:24,930 --> 00:02:29,290
then SQL Server or Azure SQL Database knows that that means an inner

47
00:02:29,290 --> 00:02:29,739
join.

48
00:02:29,739 --> 00:02:33,439
Alright, so I don&#39;t need to specify inner.

49
00:02:33,439 --> 00:02:36,849
I usually do when I&#39;m running Transact-SQL, I usually do just because I like

50
00:02:36,849 --> 00:02:39,950
to be explicit about what types of joins I&#39;m creating, so I tend to be as

51
00:02:39,950 --> 00:02:41,010
explicit as I can.

52
00:02:41,010 --> 00:02:44,340
But you don&#39;t have to, you can just say FROM employee

53
00:02:44,340 --> 00:02:48,849
JOIN order. What if you had another table as well? Could you

54
00:02:48,849 --> 00:02:53,769
add to that and say well actually I&#39;d like some information about the customer as well?

55
00:02:53,769 --> 00:02:57,269
Yeah, I could join on multiple tables, you just keep having additional join clauses

56
00:02:57,269 --> 00:03:00,340
for all the table that I want. It gets a little more interesting when I 

57
00:03:00,340 --> 00:03:02,829
have different types of joins and we&#39;ll see that in a little while.

58
00:03:02,829 --> 00:03:05,829
But assuming they&#39;re all inner joins, I&#39;d just keep adding join,

59
00:03:05,829 --> 00:03:09,069
join, join. And then the second part of the join clause is

60
00:03:09,069 --> 00:03:13,689
on and what it is that I&#39;m using to match the two tables. In this case joining it where

61
00:03:13,689 --> 00:03:17,010
the employee ID and the employee ID are the same in the two tables.

