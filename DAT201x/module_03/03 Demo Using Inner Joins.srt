0
00:00:02,830 --> 00:00:05,540
Let&#39;s look at a demo of

1
00:00:05,540 --> 00:00:10,429
how we can create some inner join. So I&#39;m

2
00:00:10,429 --> 00:00:13,619
back in SQL Server Management Studio and I&#39;ve got some 

3
00:00:13,619 --> 00:00:17,880
code here that I want to use to create some joins. And the first one I&#39;m doing is

4
00:00:17,880 --> 00:00:20,910
I want to get some information about products and the categories that I&#39;m using to

5
00:00:20,910 --> 00:00:21,760
categorize them.

6
00:00:21,760 --> 00:00:26,690
And in the database I&#39;ve actually got a product table and a separate category

7
00:00:26,690 --> 00:00:30,550
table. So I&#39;m storing information about the categories separately from the

8
00:00:30,550 --> 00:00:31,160
products.

9
00:00:31,160 --> 00:00:35,620
And I&#39;m joining them on a common key, the category table has a

10
00:00:35,620 --> 00:00:36,730
primary key called

11
00:00:36,730 --> 00:00:40,749
product category ID and I&#39;m using that as a foreign key in the product table to

12
00:00:40,749 --> 00:00:42,449
say which category a product

13
00:00:42,449 --> 00:00:45,699
belongs to. So let&#39;s look at the syntax, 

14
00:00:45,699 --> 00:00:48,890
and I&#39;ve not used any aliases for the tables here. So SELECT 

15
00:00:48,890 --> 00:00:53,510
FROM the SalesLT schema the Product table

16
00:00:53,510 --> 00:00:58,329
the Name column, so I&#39;m getting very explicit in terms of what I want to get

17
00:00:58,329 --> 00:01:00,280
out of there. The Name column from that table.

18
00:01:00,280 --> 00:01:03,649
And the Name column from the ProductCategory table. Now, the columns have

19
00:01:03,649 --> 00:01:04,280
the same

20
00:01:04,280 --> 00:01:07,490
name and if I didn&#39;t alias the columns,

21
00:01:07,490 --> 00:01:11,440
as I&#39;ve done here, then I would end up with two columns in the result set

22
00:01:11,440 --> 00:01:15,120
both called Name which would be confusing. So I want the product name as

23
00:01:15,120 --> 00:01:15,810
ProductName

24
00:01:15,810 --> 00:01:20,860
and the product category name as Category. From the Product table

25
00:01:20,860 --> 00:01:24,410
inner join, and the inner is optional but I&#39;ve put it in there,

26
00:01:24,410 --> 00:01:28,800
the ProductCategory table where the ProductCategoryID

27
00:01:28,800 --> 00:01:32,130
in the Product table is the same as the ProductCategoryID

28
00:01:32,130 --> 00:01:35,780
and the ProductCategory table. So it&#39;s a bit of a kind of a mouthful to work your way

29
00:01:35,780 --> 00:01:37,490
through the syntax. But if I run that,

30
00:01:37,490 --> 00:01:41,380
what we should get is an error because I&#39;m not in the right database.

31
00:01:41,380 --> 00:01:45,780
l will switch it to the right database. And  then what we&#39;ll get

32
00:01:45,780 --> 00:01:50,170
is a list of product names and the category that those products belong to.

33
00:01:50,170 --> 00:01:54,790
If I scroll through the results, you can see there&#39;s different types of product and

34
00:01:54,790 --> 00:02:00,100
I can see what they are. So the business of having to explicitly specify the

35
00:02:00,100 --> 00:02:02,540
schema and the table and the column

36
00:02:02,540 --> 00:02:05,960
becomes a little cumbersome when you&#39;re dealing with joins. It can

37
00:02:05,960 --> 00:02:08,039
be quite confusing in the syntax.

38
00:02:08,038 --> 00:02:11,990
So usually what you do, as well as using column aliases as we&#39;ve done here for

39
00:02:11,990 --> 00:02:12,400
the

40
00:02:12,400 --> 00:02:14,830
name columns, product name and category,

41
00:02:14,830 --> 00:02:18,930
we also use aliases for the table. And quite often because these aren&#39;t visible

42
00:02:18,930 --> 00:02:21,910
in the results, they don&#39;t show up is as column titles or whatever,

43
00:02:21,910 --> 00:02:25,440
you&#39;ll see a lot of developer just use something simple like the first letter

44
00:02:25,440 --> 00:02:29,000
is quite common. Sometimes you might be more explicit if you&#39;ve got lots of

45
00:02:29,000 --> 00:02:30,130
tables, you kind of want to 

46
00:02:30,130 --> 00:02:33,630
use something this you know immediately intuitive as that table.

47
00:02:33,630 --> 00:02:37,650
But in this case I&#39;m saying P is the Product table and C is the ProductCategory table.

48
00:02:37,650 --> 00:02:41,760
So that&#39;s a much simpler version of  exactly the same query that we just

49
00:02:41,760 --> 00:02:45,560
executed, this time just using an alias to represent the

50
00:02:45,560 --> 00:02:48,910
the name the table. So

51
00:02:48,910 --> 00:02:52,580
what if I&#39;ve got multiple tables. Now we&#39;ve got our

52
00:02:52,580 --> 00:02:56,560
products and our different categories and we can see the relationship between

53
00:02:56,560 --> 00:02:57,780
product and product category.

54
00:02:57,780 --> 00:03:02,860
We&#39;ve also got products that are sold. But actually what happens is in a sale

55
00:03:02,860 --> 00:03:04,110
there&#39;s a sales order

56
00:03:04,110 --> 00:03:07,450
and then the sales order has line items.

57
00:03:07,450 --> 00:03:11,640
So over here my list of the tables, I can see there&#39;s a sales order header table

58
00:03:11,640 --> 00:03:14,910
which has all the columns to do with, at 

59
00:03:14,910 --> 00:03:18,840
the sales header level. And I could list those out there. There&#39;s a lot of them

60
00:03:18,840 --> 00:03:19,989
just at the header level.

61
00:03:19,989 --> 00:03:23,670
But then for each order, there could be multiple sales items. So there&#39;s also an

62
00:03:23,670 --> 00:03:25,230
order detail table.

63
00:03:25,230 --> 00:03:28,580
And the order detail table has

64
00:03:28,580 --> 00:03:31,290
all the things relating to the individual line items that make up the

65
00:03:31,290 --> 00:03:31,860
order.

66
00:03:31,860 --> 00:03:35,430
And one of those is the product, ProductID. 

67
00:03:35,430 --> 00:03:38,900
That&#39;s the foreign key to the Product table which has all the details for the

68
00:03:38,900 --> 00:03:42,040
product. So there&#39;s three tables involved

69
00:03:42,040 --> 00:03:45,430
in getting a list of products that have been sold. Because I want to get the

70
00:03:45,430 --> 00:03:48,860
order level details, the order date.

71
00:03:48,860 --> 00:03:52,370
I also want to get the individual line item, the number of

72
00:03:52,370 --> 00:03:55,510
the product they were ordered, the quantity.

73
00:03:55,510 --> 00:04:00,080
And then I want to get the product details. So let&#39;s take a look at the query. We&#39;ve got

74
00:04:00,080 --> 00:04:03,080
from the order header table,

75
00:04:03,080 --> 00:04:07,019
I&#39;ve aliases that, get me the order date and the sales order number.

76
00:04:07,019 --> 00:04:11,019
And then I&#39;m joining the sales order detail table,

77
00:04:11,019 --> 00:04:15,340
calling it od, because from there I want to get the quantity and the unit price and

78
00:04:15,340 --> 00:04:16,220
the line total.

79
00:04:16,220 --> 00:04:19,790
And I&#39;m also joining the product table,

80
00:04:19,790 --> 00:04:23,240
which I&#39;m calling p, because from there I want to get the product name.

81
00:04:23,240 --> 00:04:27,700
So there&#39;s three tables there. I just simply join them all on one after the other. 

82
00:04:27,700 --> 00:04:28,200
The order

83
00:04:28,200 --> 00:04:31,160
actually is unimportant in this instance.  It doesn&#39;t matter what order I put them in

84
00:04:31,160 --> 00:04:31,520
there.

85
00:04:31,520 --> 00:04:35,050
And notice that I&#39;ve missed out the inner, I&#39;ve just said join. 

86
00:04:35,050 --> 00:04:39,270
And by default that will be an inner join. So I&#39;m doing that, and then I&#39;ve got my ORDER BY

87
00:04:39,270 --> 00:04:41,690
at the end where I&#39;m doing an ORDER BY clause as well.

88
00:04:41,690 --> 00:04:45,710
So if you didn&#39;t want things from that middle table,

89
00:04:45,710 --> 00:04:50,000
so you want some of the order details and you want some of the product information,

90
00:04:50,000 --> 00:04:54,250
but the order header table didn&#39;t have any data you wanted. Would you still join that

91
00:04:54,250 --> 00:04:54,570
one?

92
00:04:54,570 --> 00:04:58,340
You&#39;d still have to join it, you would just omit it out in the SELECT,

93
00:04:58,340 --> 00:05:01,890
you&#39;d omit it in the SELECT statement. Remember the order things get done in.

94
00:05:01,890 --> 00:05:05,490
The FROM is the first thing, so 

95
00:05:05,490 --> 00:05:09,500
at the point where I&#39;m defining my joins, I&#39;m not really worrying about the

96
00:05:09,500 --> 00:05:12,620
columns I&#39;m going to ultimately select. What I&#39;m doing is defining the 

97
00:05:12,620 --> 00:05:16,340
kind of master row set, the virtual table, from which I&#39;m eventually going to select

98
00:05:16,340 --> 00:05:17,070
some columns.

99
00:05:17,070 --> 00:05:20,620
And if that includes a join, then it has to, the join has to be there all the way around.

100
00:05:20,620 --> 00:05:23,980
The other thing that&#39;s interesting, we talked about Cartesian products

101
00:05:23,980 --> 00:05:24,530
earlier on,

102
00:05:24,530 --> 00:05:28,400
internally the way that works is that what SQL Server will do is initially do a

103
00:05:28,400 --> 00:05:30,630
Cartesian product and then filter out

104
00:05:30,630 --> 00:05:34,360
based on the predicates and the joins that I&#39;ve defined. So 

105
00:05:34,360 --> 00:05:37,970
under-the-covers Cartesian joins are used, it&#39;s generally that&#39;s not the results we

106
00:05:37,970 --> 00:05:40,850
want. So if I go ahead and run

107
00:05:40,850 --> 00:05:46,910
that. It&#39;s bringing me back data, the order date and the sales order number came from the

108
00:05:46,910 --> 00:05:48,020
order header table.

109
00:05:48,020 --> 00:05:51,560
The product name came from the product table, and the quantity, unit price, line

110
00:05:51,560 --> 00:05:52,850
total came from

111
00:05:52,850 --> 00:05:56,690
the order detail table. So I&#39;ve got multiple tables being joined.

112
00:05:56,690 --> 00:06:01,710
Now there&#39;s one other thing to mention here as well, that&#39;s worth making a note, 

113
00:06:01,710 --> 00:06:04,730
we&#39;ve joined our

114
00:06:04,730 --> 00:06:08,930
tables, and in this query here, we&#39;ve joined the sales order details table to the

115
00:06:08,930 --> 00:06:10,240
sales order header table

116
00:06:10,240 --> 00:06:13,600
based on a common key the SalesOrderID.

117
00:06:13,600 --> 00:06:17,180
And similarly we&#39;ve joined the product table based on the ProductID being the

118
00:06:17,180 --> 00:06:19,110
same. So there&#39;s one thing that joins

119
00:06:19,110 --> 00:06:23,770
those two tables. The ON part of the join is a bit like a WHERE clause.

120
00:06:23,770 --> 00:06:27,610
You could have multiple things like ANDs, and ORs, and all that type of things. So

121
00:06:27,610 --> 00:06:33,280
we&#39;ve actually got here an example where I&#39;ve said bring me back all of the sales

122
00:06:33,280 --> 00:06:37,600
where the product ID is the same as a product id,

123
00:06:37,600 --> 00:06:39,650
so all of the sales for that product.

124
00:06:39,650 --> 00:06:44,120
And the unit price that I sold it at is the same as the list price for the product.

125
00:06:44,120 --> 00:06:47,660
So I&#39;ve got a list price in my list of products which is the

126
00:06:47,660 --> 00:06:51,539
price I want to sell it at. And what I&#39;m doing is bring me back all the times when I sold this

127
00:06:51,539 --> 00:06:52,060
product

128
00:06:52,060 --> 00:06:55,199
at the price that I originally wanted to sell at.

129
00:06:55,199 --> 00:06:58,520
So there&#39;s an AND clause in the ON part of the join.

130
00:06:58,520 --> 00:07:01,530
So you said that you could have a twopart primary key.

131
00:07:01,530 --> 00:07:05,419
So if you had a two part primary key, then you&#39;d be using a join

132
00:07:05,419 --> 00:07:09,669
on multiple columns. Yes, and again you&#39;d do the same thing as I&#39;ve done here.

133
00:07:09,669 --> 00:07:13,300
So interestingly you&#39;ll notice that in our dataset if Irun that,

134
00:07:13,300 --> 00:07:17,570
we have never at any point sold a product at the price we wanted to sell

135
00:07:17,570 --> 00:07:18,150
it for.

136
00:07:18,150 --> 00:07:23,330
So it&#39;s a safe bet we&#39;ve been discounting, so we&#39;ll change our

137
00:07:23,330 --> 00:07:27,970
clause to a less than there And sure enough there are orders.

138
00:07:27,970 --> 00:07:32,389
So sometimes by adding that additional clause in the ON part of the join

139
00:07:32,389 --> 00:07:35,530
statement, you&#39;re able to add additional filtering criteria

140
00:07:35,530 --> 00:07:39,110
as the query engine assembles that row set that we&#39;re going to get the columns

141
00:07:39,110 --> 00:07:39,360
from.

