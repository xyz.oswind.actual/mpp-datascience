0
00:00:05,470 --> 00:00:09,670
Good to have you back with us on the Querying with Transact-SQL course.

1
00:00:09,670 --> 00:00:14,120
We&#39;re up to module nine, so we&#39;ve covered a lot of ground so far in this course.

2
00:00:14,120 --> 00:00:17,439
And up until now, we&#39;ve pretty much focused on

3
00:00:17,439 --> 00:00:21,169
querying tables and getting data out of them, that&#39;s pretty much

4
00:00:21,169 --> 00:00:22,779
exclusively been what we&#39;ve looked at.

5
00:00:22,779 --> 00:00:26,880
In this module we&#39;re going to turn it on it&#39;s head a little bit and look at what

6
00:00:26,880 --> 00:00:29,290
happens when we want to update or insert or

7
00:00:29,290 --> 00:00:33,489
delete data that&#39;s already in tables. So

8
00:00:33,489 --> 00:00:36,940
we have a number of things to look at. We&#39;ll look at inserting data into tables

9
00:00:36,940 --> 00:00:40,250
and as part of that, we&#39;ll look at what we do with 

10
00:00:40,250 --> 00:00:43,720
rows that might have identity columns. Some unique identifier, how do we

11
00:00:43,720 --> 00:00:48,810
generate that? We&#39;ll look at updating data that&#39;s already in the table, so

12
00:00:48,810 --> 00:00:50,590
changing values of different columns.

13
00:00:50,590 --> 00:00:54,440
And we&#39;ll look at deleting data removing rows from tables and getting rid of data

14
00:00:54,440 --> 00:00:55,450
that we no longer need.

15
00:00:55,450 --> 00:00:56,490
So quite a lot to cover.

