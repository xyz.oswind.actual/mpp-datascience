0
00:00:02,480 --> 00:00:05,000
After inserting data into a table, we might want to

1
00:00:05,000 --> 00:00:09,370
talk about updating data in a table. So I&#39;ve got a table that&#39;s got some data in it

2
00:00:09,370 --> 00:00:13,340
because we&#39;ve inserted it, and what I might want to do is update it. And to do

3
00:00:13,340 --> 00:00:14,469
that, we have the

4
00:00:14,469 --> 00:00:18,689
UPDATE statement. And what that will do is it will update all the rows

5
00:00:18,689 --> 00:00:24,329
in a table or view, which might be what you want. I might want to do 

6
00:00:24,329 --> 00:00:27,559
a price increase across the board and update every single

7
00:00:27,559 --> 00:00:32,809
product in my Product table. More likely I&#39;m going to want to filter it. I&#39;m going to want to use a WHERE clause to 

8
00:00:32,809 --> 00:00:34,649
say only update the rows

9
00:00:34,649 --> 00:00:39,440
where this criteria is true. So a bit like a SELECT statement that restricts the

10
00:00:39,440 --> 00:00:42,870
rows that you select out. You can use a WHERE clause in an UPDATE

11
00:00:42,870 --> 00:00:44,640
statement to restrict the rows that get updated.

12
00:00:44,640 --> 00:00:48,359
And the other thing that we can do is

13
00:00:48,359 --> 00:00:52,410
we&#39;ve got the SET clause in the UPDATE statement where I set

14
00:00:52,410 --> 00:00:57,519
different columns to specific values. And in the example here you can see that

15
00:00:57,519 --> 00:00:57,909
I&#39;m

16
00:00:57,909 --> 00:01:02,460
updating the unit price, I&#39;m referencing itself, I&#39;m updating unit price. It&#39;s because you&#39;ve set it to itself

17
00:01:02,460 --> 00:01:06,649
multiplied by or added to or something. So it depends on what it is

18
00:01:06,649 --> 00:01:11,770
at the moment, what it will be. Exactly, yes. So in this case I&#39;m saying categoryid 

19
00:01:11,770 --> 00:01:15,290
1 where the products are not discontinued,

20
00:01:15,290 --> 00:01:18,409
I want to increase the price. But I can&#39;t just

21
00:01:18,409 --> 00:01:21,520
explicitly say here&#39;s the price I want you to set because of course all the

22
00:01:21,520 --> 00:01:22,869
products might be different prices.

23
00:01:22,869 --> 00:01:26,380
So I&#39;ve referenced the existing price and then just used

24
00:01:26,380 --> 00:01:31,259
a function to, or rather I use an expression to indicate what I want to update it to.

25
00:01:31,259 --> 00:01:34,299
So you can update to literal value, you can update to an expression.

26
00:01:34,299 --> 00:01:39,149
And that expression could reference the existing value.

27
00:01:39,149 --> 00:01:41,960
So you can see that I&#39;m setting, in this case I&#39;m setting the unit price, you could

28
00:01:41,960 --> 00:01:42,490
set 

29
00:01:42,490 --> 00:01:45,539
multiple columns to different values. So it&#39;s not, you don&#39;t have to have

30
00:01:45,539 --> 00:01:48,890
an update statement for each column that you want to change. If I wanted to change

31
00:01:48,890 --> 00:01:50,000
the price

32
00:01:50,000 --> 00:01:54,359
and also the, I don&#39;t know, the notes about that column or something,

33
00:01:54,359 --> 00:01:57,479
I could up -- you know SET unitprice equals whatever

34
00:01:57,479 --> 00:02:02,759
comma and then Notes equals whatever. I could update multiple columns.

35
00:02:02,759 --> 00:02:05,710
Now we&#39;re providing the values in the SET clause here.

36
00:02:05,710 --> 00:02:09,099
The other thing you can do, much like you can do

37
00:02:09,098 --> 00:02:13,370
an INSERT into a table from another table,

38
00:02:13,370 --> 00:02:16,540
you can do an UPDATE from another table. So I could

39
00:02:16,540 --> 00:02:20,340
update a table based on values that I get from a query.

40
00:02:20,340 --> 00:02:24,620
So UPDATE the columns in the WHERE clause, sorry UPDATE the rows that are

41
00:02:24,620 --> 00:02:28,120
defined by the WHERE clause setting the columns to whatever the columns are

42
00:02:28,120 --> 00:02:28,819
that are returned

43
00:02:28,819 --> 00:02:32,310
by this query in the FROM clause. There&#39;s

44
00:02:32,310 --> 00:02:35,849
pretty good documentation, some great examples of using

45
00:02:35,849 --> 00:02:39,940
that type of technique and various different approaches to updating data

46
00:02:39,940 --> 00:02:42,680
in the documentation. And I do recommend go and have a look at the

47
00:02:42,680 --> 00:02:44,220
Transact-SQL documentation

48
00:02:44,220 --> 00:02:48,500
in MSDN for that.

49
00:02:48,500 --> 00:02:51,680
So only the columns specified in the SET clause are modified. I can specify 

50
00:02:51,680 --> 00:02:55,140
multiple columns in the SET clause, but only the ones that I specify are affected.

51
00:02:55,140 --> 00:02:58,410
The other columns remain at their existing value. So

52
00:02:58,410 --> 00:03:02,030
if you have an identity column for example, it doesn&#39;t generate a new ID or anything

53
00:03:02,030 --> 00:03:03,920
like that. That stays as it was.

54
00:03:03,920 --> 00:03:10,390
Only the columns in the SET clause are affected. So

55
00:03:10,390 --> 00:03:14,200
what you&#39;ve done at the moment is we&#39;ve looked at inserting, 

56
00:03:14,200 --> 00:03:18,130
and we&#39;ve looked at updating, so what if we were to

57
00:03:18,130 --> 00:03:22,389
at add a new product? We&#39;ve got some products coming in, now we&#39;ve got 

58
00:03:22,389 --> 00:03:26,510
5 percent increase across the board, but then also as well as some new prices for

59
00:03:26,510 --> 00:03:27,329
products

60
00:03:27,329 --> 00:03:31,260
we&#39;ve got -- some of them are new products. So if I&#39;m putting in something that&#39;s brand new,

61
00:03:31,260 --> 00:03:32,290
we don&#39;t have it yet,

62
00:03:32,290 --> 00:03:36,569
I want to do an INSERT. But if I&#39;m putting in new pricing for something that already

63
00:03:36,569 --> 00:03:40,420
exists, I just want to update the value. I don&#39;t want a new record, I just want to update 

64
00:03:40,420 --> 00:03:41,160
the value 

65
00:03:41,160 --> 00:03:45,310
of the existing record. How could we deal with that situation?

66
00:03:45,310 --> 00:03:48,780
Right, and that&#39;s a really common situation especially in things like data warehousing

67
00:03:48,780 --> 00:03:50,069
scenarios where you want to do,

68
00:03:50,069 --> 00:03:54,250
some people call it an upsert. I hate these kind of word crashes that people use.

69
00:03:54,250 --> 00:03:57,989
Some people call it an upsert. Basically what we&#39;re going to do is update records that are already there

70
00:03:57,989 --> 00:04:01,329
or insert new records if there aren&#39;t  already records there.

71
00:04:01,329 --> 00:04:05,090
And to support that in Transact-SQL, we have the MERGE statement.

72
00:04:05,090 --> 00:04:09,510
So what MERGE does is it modifies data based on a condition.

73
00:04:09,510 --> 00:04:14,449
And what we do is we look at when the source matches the target. So typically

74
00:04:14,449 --> 00:04:15,049
you&#39;ve got

75
00:04:15,049 --> 00:04:15,920
data in

76
00:04:15,920 --> 00:04:18,980
in one, perhaps a staging table where you&#39;re loading data in,

77
00:04:18,980 --> 00:04:22,740
and so you&#39;ve got a query that gets data out of the staging table. And you&#39;ve got the table

78
00:04:22,740 --> 00:04:26,169
where you&#39;re either inserting or updating. So you&#39;re loading data into that table.

79
00:04:26,169 --> 00:04:29,790
And what you&#39;re going to do is you&#39;re going to compare the source to the target

80
00:04:29,790 --> 00:04:33,150
and you going to say if there&#39;s a match,

81
00:04:33,150 --> 00:04:36,430
so the record exists in both the source

82
00:04:36,430 --> 00:04:40,210
and the target. So perhaps a product with this product id 

83
00:04:40,210 --> 00:04:43,380
already exists, then I&#39;m going to do an UPDATE.

84
00:04:43,380 --> 00:04:47,800
I&#39;m going to update the price of that product. But if the source doesn&#39;t match the target, so

85
00:04:47,800 --> 00:04:48,190
it&#39;s

86
00:04:48,190 --> 00:04:52,000
in the source but not in the target, well clearly this is a new product

87
00:04:52,000 --> 00:04:56,240
so I&#39;m going to do an INSERT and insert a new product. And there is of course actually a third

88
00:04:56,240 --> 00:04:56,900
variant,

89
00:04:56,900 --> 00:05:01,919
I might have a product that&#39;s in this  target, so it&#39;s in my existing Product

90
00:05:01,919 --> 00:05:02,389
table,

91
00:05:02,389 --> 00:05:05,940
but it&#39;s not listed in my new list of prices that I&#39;m loading.

92
00:05:05,940 --> 00:05:09,030
So we don&#39;t have a price for it, so we&#39;re not going to stock product anymore.

93
00:05:09,030 --> 00:05:12,770
Well, it may mean that, and you would write logic to do whatever you want. It may

94
00:05:12,770 --> 00:05:15,440
mean hey there&#39;s just no change to this product or it may mean

95
00:05:15,440 --> 00:05:18,280
if that product is not on the new list, then we need to get rid of it. We&#39;re no longer

96
00:05:18,280 --> 00:05:19,210
going to sell it.

97
00:05:19,210 --> 00:05:22,800
So I might set it to be discontinued or I might delete it.

98
00:05:22,800 --> 00:05:26,960
So we&#39;ve got this option of  looking at the source and target,

99
00:05:26,960 --> 00:05:27,979
comparing them,

100
00:05:27,979 --> 00:05:32,200
if it matches what do we want to do, if it doesn&#39;t match because

101
00:05:32,200 --> 00:05:35,520
it&#39;s in the source but it&#39;s not in the target what do we want to do,

102
00:05:35,520 --> 00:05:38,780
and if it doesn&#39;t match because it&#39;s in the target but not in the source what do we

103
00:05:38,780 --> 00:05:39,300
want to do?

104
00:05:39,300 --> 00:05:44,470
So the syntax is, there&#39;s an example here, you can see the syntax, is MERGE

105
00:05:44,470 --> 00:05:49,100
INTO rather than INSERT INTO Production.Products as P

106
00:05:49,100 --> 00:05:53,190
USING the source that I want to compare it to. So the

107
00:05:53,190 --> 00:05:56,919
USING clause is where to find the source. In this case the source is a staging table,

108
00:05:56,919 --> 00:06:00,780
and that could be any table expression. So it is just, in this case it&#39;s a table.

109
00:06:00,780 --> 00:06:04,330
And I&#39;m matching it based on the ProductID in this case. So this is

110
00:06:04,330 --> 00:06:08,000
a bit like a JOIN clause in some ways, the way that I&#39;m saying

111
00:06:08,000 --> 00:06:12,710
I want to relate these two tables. How do I determine if they match or not? When

112
00:06:12,710 --> 00:06:13,590
they&#39;re matched,

113
00:06:13,590 --> 00:06:16,750
so in other words when the product id in the source is equal to a product id

114
00:06:16,750 --> 00:06:20,200
in the target, then I want to update

115
00:06:20,200 --> 00:06:24,220
and notice I don&#39;t have to say which table I&#39;m updating because that&#39;s implied by

116
00:06:24,220 --> 00:06:26,349
the MERGE. I just want to update, I want to set 

117
00:06:26,349 --> 00:06:28,760
the unit price in the

118
00:06:28,760 --> 00:06:33,200
target equal to whatever the unit price is in the source, and set discontinued equal

119
00:06:33,200 --> 00:06:35,450
to whatever discontinued is in the source. So I&#39;m taking

120
00:06:35,450 --> 00:06:39,240
the values in the source and updating the target to use those values.

121
00:06:39,240 --> 00:06:43,900
When not matched, and I haven&#39;t in this instance bothered to check

122
00:06:43,900 --> 00:06:48,720
to see which way it doesn&#39;t match. I&#39;m just saying if it doesn&#39;t match, then I&#39;m going to insert

123
00:06:48,720 --> 00:06:52,670
whatever there is in the source that isn&#39;t in the target

124
00:06:52,670 --> 00:06:56,630
and it&#39;s going to include all the values that I want to insert. And as I say, that could be more

125
00:06:56,630 --> 00:06:57,890
sophisticated because I could

126
00:06:57,890 --> 00:07:00,920
check which direction it doesn&#39;t match in and

127
00:07:00,920 --> 00:07:04,350
do something different accordingly. So you could actually have -- you don&#39;t have to have 

128
00:07:04,350 --> 00:07:05,850
update and an insert, you could have

129
00:07:05,850 --> 00:07:10,170
update and another update that does something different. 

130
00:07:10,170 --> 00:07:13,400
Yeah, exactly. And the classic example of that would be you typically

131
00:07:13,400 --> 00:07:14,060
wouldn&#39;t delete

132
00:07:14,060 --> 00:07:17,620
a row because the product was discontinued, but you&#39;d set it&#39;s discontinued flag.

133
00:07:17,620 --> 00:07:18,160
Mhmm. Yep.

134
00:07:18,160 --> 00:07:21,720
So we talked about 

135
00:07:21,720 --> 00:07:25,750
inserting and updating, and we briefly mentioned deleting data, I guess

136
00:07:25,750 --> 00:07:27,290
it&#39;s sensible to talk about well

137
00:07:27,290 --> 00:07:30,830
how do we delete rows from a table. The answer

138
00:07:30,830 --> 00:07:34,690
unsurprisingly is we use the DELETE statement. And 

139
00:07:34,690 --> 00:07:38,500
something to watch out for here is a really common, and I&#39;m sure you&#39;ll agree Jeff, 

140
00:07:38,500 --> 00:07:39,620
 

141
00:07:39,620 --> 00:07:43,320
if you don&#39;t specify a WHERE clause when you say DELETE FROM a table.

142
00:07:43,320 --> 00:07:47,310
What do you think it does? It deletes everything. It deletes every row in the table. 

143
00:07:47,310 --> 00:07:48,620
Hopefully not too common, but drastic 

144
00:07:48,620 --> 00:07:53,110
if done at all. Exactly, yeah. More than once I&#39;ve accidentally

145
00:07:53,110 --> 00:07:56,760
issued a DELETE statement and then realize I&#39;ve deleted all the rows in the table. So

146
00:07:56,760 --> 00:07:59,920
you know it can have fairly major consequences.

147
00:07:59,920 --> 00:08:04,520
Usually a DELETE statement has a WHERE clause that specifies which of the columns

148
00:08:04,520 --> 00:08:05,600
I want to delete. So

149
00:08:05,600 --> 00:08:08,440
in this case, sorry which of the rows I want to delete rather, so in this case

150
00:08:08,440 --> 00:08:11,000
I&#39;m specifying DELETE FROM the OrderDetails table

151
00:08:11,000 --> 00:08:15,150
WHERE the orderid is 10248. So I&#39;m very explicit about

152
00:08:15,150 --> 00:08:18,870
that order. It was maybe inserted in error or something like that so

153
00:08:18,870 --> 00:08:24,040
go ahead delete that out of the table.

154
00:08:24,040 --> 00:08:27,790
Now the alternative to deleting all of the rows using DELETE, I said if you use DELETE

155
00:08:27,790 --> 00:08:29,230
without a WHERE clause deletes all 

156
00:08:29,230 --> 00:08:32,250
the rows. Well SQL Server records

157
00:08:32,250 --> 00:08:35,630
transactions like that as they happened. So there&#39;s a little bit of overhead if I use

158
00:08:35,630 --> 00:08:36,920
a DELETE statement to do that.

159
00:08:36,919 --> 00:08:40,650
And I can eliminate that overhead by using this alternative approach called TRUNCATE

160
00:08:40,650 --> 00:08:41,930
TABLE.

161
00:08:41,929 --> 00:08:45,350
Which effectively deallocates the physical rows,

162
00:08:45,350 --> 00:08:50,710
it goes ahead and you know eliminates the data in the table but in a

163
00:08:50,710 --> 00:08:54,580
very efficient, very quick manner. So if you do actually want to go ahead and

164
00:08:54,580 --> 00:08:58,430
wipe the contents of a table, TRUNCATE TABLE is generally a more efficient way

165
00:08:58,430 --> 00:08:59,080
to do it.

166
00:08:59,080 --> 00:09:03,460
So we don&#39;t have a WHERE it&#39;s essentially doing that DELETE statement without a WHERE. 

167
00:09:03,460 --> 00:09:07,120
Yeah. Very, very quickly. Yes, it will go ahead and truncate the entire table.

168
00:09:07,120 --> 00:09:08,110
There&#39;s no way to 

169
00:09:08,110 --> 00:09:11,760
restrict which rows get deleted. TRUNCATE TABLE deletes all the data in the

170
00:09:11,760 --> 00:09:12,270
table.

171
00:09:12,270 --> 00:09:16,080
And the table itself, we&#39;ve still got the structure, we&#39;ve got the

172
00:09:16,080 --> 00:09:19,510
columns there empty? Yeah, table remains,

173
00:09:19,510 --> 00:09:22,620
the table definition itself remains, but all the data is wiped out of it.

174
00:09:22,620 --> 00:09:24,680
It&#39;s typically used in scenarios were

175
00:09:24,680 --> 00:09:28,140
perhaps you&#39;re creating a, you&#39;ve got staging tables that you load data from

176
00:09:28,140 --> 00:09:29,610
some external source into,

177
00:09:29,610 --> 00:09:32,839
and then when you ready load them  into their actual target tables

178
00:09:32,839 --> 00:09:36,470
you might use the MERGE statement that we talked about earlier on. So you might load

179
00:09:36,470 --> 00:09:40,450
new data into staging table, than use a MERGE statement to

180
00:09:40,450 --> 00:09:45,900
insert or update your actual production table using that, and then when you&#39;re finished doing that

181
00:09:45,900 --> 00:09:49,480
you want to empty the staging table but you want to keep it around for the next

182
00:09:49,480 --> 00:09:53,430
load, so you truncate the staging table. Because otherwise the next time it&#39;s going

183
00:09:53,430 --> 00:09:53,910
to

184
00:09:53,910 --> 00:09:57,050
try and stage the same data again if it&#39;s still in there. Yes, exactly.

185
00:09:57,050 --> 00:10:01,520
There are cases where it&#39;s used. It&#39;s not, in a typical 

186
00:10:01,520 --> 00:10:05,100
you know online transaction processing application database, it&#39;s not

187
00:10:05,100 --> 00:10:06,160
something you do a lot, 

188
00:10:06,160 --> 00:10:09,690
but there are scenarios where you want to truncate the table to get a rid of all the rows.

189
00:10:09,690 --> 00:10:13,180
Now there is a mention that the TRUNCATE TABLE will fail if the table is

190
00:10:13,180 --> 00:10:16,410
referenced by a foreign key constraint in another table.

191
00:10:16,410 --> 00:10:20,800
And the same is actually true with DELETE. So we haven&#39;t really 

192
00:10:20,800 --> 00:10:24,490
sort of talked much about defining tables and keys and constraints,

193
00:10:24,490 --> 00:10:27,980
but we did mention earlier on when we created our table there was a foreign key,

194
00:10:27,980 --> 00:10:29,250
there was a relationship

195
00:10:29,250 --> 00:10:33,209
between my CallLog table and my Customer table. There&#39;s a -- 

196
00:10:33,209 --> 00:10:37,030
the customer key is actually in the CallLog table. 

197
00:10:37,030 --> 00:10:40,640
So it must be a real customer that you&#39;re call logging for. Yes.

198
00:10:40,640 --> 00:10:44,830
So that foreign key constraint is used to make sure that whatever key-value I put in this table,

199
00:10:44,830 --> 00:10:49,080
there must be a record for it in the table that I&#39;m referencing.

200
00:10:49,080 --> 00:10:52,600
If you have foreign key constraints 

201
00:10:52,600 --> 00:10:55,089
in other tables that reference the table you&#39;re deleting from

202
00:10:55,089 --> 00:10:58,129
and you try and delete that row, 

203
00:10:58,129 --> 00:11:02,259
either using DELETE or TRUNCATE, you will get an error. So a good example that might

204
00:11:02,259 --> 00:11:02,749
be

205
00:11:02,749 --> 00:11:07,160
you&#39;ve got, well let&#39;s look at our call log. We&#39;ve got a CallLog table

206
00:11:07,160 --> 00:11:10,480
that references our Customer table. If I try to delete

207
00:11:10,480 --> 00:11:13,829
a customer for whom I had a call, that would fail

208
00:11:13,829 --> 00:11:18,069
because I&#39;ve got details about that customer in another table. But we could delete the

209
00:11:18,069 --> 00:11:18,860
call.

210
00:11:18,860 --> 00:11:22,009
We could delete the call. Because that&#39;s just one individual call for that customer.

211
00:11:22,009 --> 00:11:26,439
Right, yeah, exactly. So it&#39;s the foreign key side you can delete, the primary key side

212
00:11:26,439 --> 00:11:27,170
you can&#39;t delete

213
00:11:27,170 --> 00:11:30,379
if there are foreign keys still referencing it. So you don&#39;t get left 

214
00:11:30,379 --> 00:11:33,889
with these orphaned records that are not linked to anything. Exactly.

215
00:11:33,889 --> 00:11:37,139
And there are one or two techniques we can used to deal with

216
00:11:37,139 --> 00:11:39,069
that situation should I definitely need to delete

217
00:11:39,069 --> 00:11:43,180
a customer and all of their calls. I can do that. There are ways that we can do that,

218
00:11:43,180 --> 00:11:44,339
we don&#39;t really cover them 

219
00:11:44,339 --> 00:11:47,410
on this course. So there are ways around that, but just be aware if

220
00:11:47,410 --> 00:11:48,839
you&#39;re using a DELETE statement

221
00:11:48,839 --> 00:11:52,449
and the row you&#39;re deleting is referenced from

222
00:11:52,449 --> 00:11:56,220
to another table, then you won&#39;t be able to delete it. You&#39;ll get an error.

