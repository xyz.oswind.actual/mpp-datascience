0
00:00:02,300 --> 00:00:05,049
So let&#39;s dive into a demo of

1
00:00:05,049 --> 00:00:09,059
inserting data into a table. And to start off with I&#39;m going to a create table.

2
00:00:09,059 --> 00:00:13,120
The scenario here is that in our Adventure Works organization

3
00:00:13,120 --> 00:00:17,430
we want to keep a log of telephone calls that are made. Every time that we call

4
00:00:17,430 --> 00:00:18,550
a customer, we want to

5
00:00:18,550 --> 00:00:22,210
keep a log of when that call was and what was discussed and so on.

6
00:00:22,210 --> 00:00:26,580
So to do that, I&#39;m going to create table called CallLog in the SalesLT schema.

7
00:00:26,580 --> 00:00:31,830
And I&#39;m going to give each call an ID which is an identity column.

8
00:00:31,830 --> 00:00:34,989
So it&#39;s going to generate. I haven&#39;t specified a seed or an increment, so it&#39;s going to 

9
00:00:34,989 --> 00:00:37,090
start at one, and then two, and then three and so on.

10
00:00:37,090 --> 00:00:41,130
I&#39;m going to specify the CallTime and by that I mean the date and time that the call was

11
00:00:41,130 --> 00:00:41,960
placed.

12
00:00:41,960 --> 00:00:46,010
I&#39;m going to specify the SalesPerson that placed the call because were talking about sales

13
00:00:46,010 --> 00:00:46,790
people calling

14
00:00:46,790 --> 00:00:50,510
customers here. We&#39;ll specify the customer and

15
00:00:50,510 --> 00:00:54,730
notice this one references the Customer table. This is what we call a foreign key.

16
00:00:54,730 --> 00:00:58,640
So that&#39;s referencing an existing customer the Customer table.

17
00:00:58,640 --> 00:01:03,520
And we&#39;re going to specify the PhoneNumber that we actually called

18
00:01:03,520 --> 00:01:07,020
and some Notes about what was discussed on the call. So there&#39;s various different

19
00:01:07,020 --> 00:01:08,700
columns in the table that we&#39;re defining.

20
00:01:08,700 --> 00:01:12,200
The other thing to note here is that datetime column for CallTime

21
00:01:12,200 --> 00:01:16,280
as a default value based on the GETDATE function. So if I 

22
00:01:16,280 --> 00:01:20,189
don&#39;t specify the date and time, it&#39;s going to default to the current date and time.

23
00:01:20,189 --> 00:01:23,360
But you could override that by giving it a value. If you new the sale was 

24
00:01:23,360 --> 00:01:27,060
placed yesterday you could -- exactly if a call was placed yesterday, I could explicitly say 

25
00:01:27,060 --> 00:01:30,759
yeah this one was placed yesterday. If I don&#39;t do that, it will assume it was placed

26
00:01:30,759 --> 00:01:31,340
today.

27
00:01:31,340 --> 00:01:36,140
The other thing to note is that a lot of these columns don&#39;t allow nulls.

28
00:01:36,140 --> 00:01:40,159
So definitely don&#39;t want nulls in my primary key; I don&#39;t want nulls in my CallTime or 

29
00:01:40,159 --> 00:01:41,080
the SalesPerson,

30
00:01:41,080 --> 00:01:45,350
or the CustomerID or the PhoneNumber. But I will allow null in my Notes column. So if

31
00:01:45,350 --> 00:01:48,650
there weren&#39;t any particular notes, perhaps I called them and they weren&#39;t in and

32
00:01:48,650 --> 00:01:50,840
I just left a message. There&#39;s not really any point in

33
00:01:50,840 --> 00:01:55,280
making notes about that. So we maybe don&#39;t necessarily need notes.

34
00:01:55,280 --> 00:01:59,829
So we&#39;ll allow null in the Notes column. So let&#39;s create the table.

35
00:01:59,829 --> 00:02:03,030
And that&#39;s now ready for me to use.

36
00:02:03,030 --> 00:02:07,960
And the first thing I&#39;m going to do, I&#39;m just going to insert one single row into that table and I&#39;m going to 

37
00:02:07,960 --> 00:02:09,590
explicitly specify

38
00:02:09,590 --> 00:02:12,739
values for all of the columns other than the

39
00:02:12,739 --> 00:02:15,000
the CallID which is an identity, it gets

40
00:02:15,000 --> 00:02:18,540
generated automatically. So the syntax is INSERT INTO,

41
00:02:18,540 --> 00:02:21,890
the INTO is actual optional, you could just say INSERT and the name of the table,

42
00:02:21,890 --> 00:02:26,640
schema and table and then values because I&#39;m

43
00:02:26,640 --> 00:02:30,200
explicitly inserting some values into that table. 

44
00:02:30,200 --> 00:02:35,440
And in a parenthesis list I&#39;ve got the value for the first column that isn&#39;t an identity

45
00:02:35,440 --> 00:02:37,130
which in this case is the CallTime.

46
00:02:37,130 --> 00:02:42,320
So that&#39;s the specific date and time. The value for

47
00:02:42,320 --> 00:02:46,840
the salesperson, the value for the customer number, and this is customer number

48
00:02:46,840 --> 00:02:47,320
one,

49
00:02:47,320 --> 00:02:50,459
the telephone number that I dialed, and I&#39;m 

50
00:02:50,459 --> 00:02:54,310
inserting some notes about that call.  About what that call was about.

51
00:02:54,310 --> 00:02:57,340
So fairly straightforward. Insert into that table those values.

52
00:02:57,340 --> 00:03:03,100
Let&#39;s go ahead and do that. And I get back a message, 1 row affected.

53
00:03:03,100 --> 00:03:06,180
And that can be quite important when you&#39;re building applications, 

54
00:03:06,180 --> 00:03:09,630
the ability to figure out how many rows were affected by an INSERT and

55
00:03:09,630 --> 00:03:10,860
so on can be useful.

56
00:03:10,860 --> 00:03:14,440
Especially if your checking to see were no rows affected, did something wrong and

57
00:03:14,440 --> 00:03:15,920
nothing was inserted or whatever.

58
00:03:15,920 --> 00:03:19,140
And if I actually query that table now,

59
00:03:19,140 --> 00:03:23,480
we can see that that&#39;s been inserted into there. And I can see my identity

60
00:03:23,480 --> 00:03:25,920
number is working. Here it&#39;s generated number 1

61
00:03:25,920 --> 00:03:29,840
and I&#39;ve got all of the values that I inserted into the table. So far so good.

62
00:03:29,840 --> 00:03:33,019
What if

63
00:03:33,019 --> 00:03:36,430
I want to use nulls in default? So I 

64
00:03:36,430 --> 00:03:40,500
don&#39;t particularly have any notes that I want to keep a record of, so I&#39;m going to 

65
00:03:40,500 --> 00:03:44,000
insert null into that column. And I don&#39;t want to say this

66
00:03:44,000 --> 00:03:47,360
call was made yesterday, it was made right now. I just want to use the default

67
00:03:47,360 --> 00:03:48,329
value for that.

68
00:03:48,329 --> 00:03:51,910
Well I can do that by specifically saying

69
00:03:51,910 --> 00:03:56,040
insert the default value into this column and insert a null

70
00:03:56,040 --> 00:04:00,040
into that column. So it&#39;s very much like inserting values except I&#39;m explicitly

71
00:04:00,040 --> 00:04:00,570
saying

72
00:04:00,570 --> 00:04:04,329
insert the default value to the date and time column and insert this

73
00:04:04,329 --> 00:04:08,760
null value into the Notes column. But you know the order your putting

74
00:04:08,760 --> 00:04:13,100
those things in? Yes, so I know what order the columns are

75
00:04:13,100 --> 00:04:16,130
defined in, so it&#39;s really just the ordinal position in the list that&#39;s

76
00:04:16,130 --> 00:04:16,820
determining

77
00:04:16,820 --> 00:04:21,650
which column the data gets placed into. So if I go ahead and do that,

78
00:04:21,649 --> 00:04:25,770
we can actually see that it&#39;s inserted the default value which was

79
00:04:25,770 --> 00:04:28,770
today&#39;s date, so you can tell when we recorded this. 

80
00:04:28,770 --> 00:04:32,990
And you can see that it&#39;s inserted explicitly a null into the Notes column.

81
00:04:32,990 --> 00:04:36,350
But you raise a good point. 

82
00:04:36,350 --> 00:04:39,870
Up until now I&#39;ve assumed that the ordinal position of the columns

83
00:04:39,870 --> 00:04:44,550
is you know what&#39;s in my values clause. Well, what if I don&#39;t know what order

84
00:04:44,550 --> 00:04:47,520
the columns are defined in, or what if the  table has been modified and a new column has 

85
00:04:47,520 --> 00:04:48,290
been added

86
00:04:48,290 --> 00:04:52,230
the allows nulls, or has a default value, but I don&#39;t know where in the

87
00:04:52,230 --> 00:04:55,330
sequence of the table that column is?

88
00:04:55,330 --> 00:04:59,970
Well what I can do is I can explicitly say in the insert part of the statement

89
00:04:59,970 --> 00:05:03,130
which columns I want to insert values into.

90
00:05:03,130 --> 00:05:06,180
So I&#39;m inserting in this case just into the SalesPerson,

91
00:05:06,180 --> 00:05:09,450
CustomerID, and PhoneNumber columns.

92
00:05:09,450 --> 00:05:12,930
And I&#39;ve then provided the salesperson,

93
00:05:12,930 --> 00:05:17,250
customer ID, and phone number values. I haven&#39;t mentioned

94
00:05:17,250 --> 00:05:21,010
the CallID because that&#39;s an identity. I haven&#39;t mentioned

95
00:05:21,010 --> 00:05:24,700
the CallTime because the has a default value and that&#39;s what will be used.

96
00:05:24,700 --> 00:05:28,640
I haven&#39;t mentioned notes because I&#39;m not inserting any notes and it allows

97
00:05:28,640 --> 00:05:29,600
nulls so it will just

98
00:05:29,600 --> 00:05:33,190
place a null there. So if something didn&#39;t allow nulls

99
00:05:33,190 --> 00:05:36,410
and didn&#39;t have a default or an identity? I would get an error.

100
00:05:36,410 --> 00:05:40,360
You would have to supply it. Yeah we&#39;d get an error. You have to supply it, if it doesn&#39;t allow nulls,

101
00:05:40,360 --> 00:05:43,980
doesn&#39;t have a default and isn&#39;t an identity, you&#39;re going to have to supply the value.

102
00:05:43,980 --> 00:05:47,610
But if I go ahead and insert that

103
00:05:47,610 --> 00:05:51,980
there. One row affected, so it&#39;s been successful.

104
00:05:51,980 --> 00:05:56,590
And if I query the table, I can see that again even though I didn&#39;t explicitly say default

105
00:05:56,590 --> 00:06:00,570
because I didn&#39;t provide a value for the CallTime, it has used the default. And

106
00:06:00,570 --> 00:06:02,980
although I didn&#39;t explicitly say null for the Notes

107
00:06:02,980 --> 00:06:06,460
because I didn&#39;t provide any information about the Notes column,

108
00:06:06,460 --> 00:06:10,510
it&#39;s inserted a null.

109
00:06:10,510 --> 00:06:14,670
So far we&#39;ve inserted one row at a time.  I can actually use this VALUES clause to

110
00:06:14,670 --> 00:06:18,180
insert multiple rows. If I want to do that in a single INSERT statement rather

111
00:06:18,180 --> 00:06:18,650
than

112
00:06:18,650 --> 00:06:21,730
repeatedly running lots of INSERT statements to insert lots of rows,

113
00:06:21,730 --> 00:06:25,460
I could do one INSERT statement that inserts lots of rows. So

114
00:06:25,460 --> 00:06:28,720
in this case I&#39;ve got an INSERT statement here that

115
00:06:28,720 --> 00:06:33,040
for the first row adds an explicit date. And you&#39;ll notice that I&#39;m using a function.

116
00:06:33,040 --> 00:06:33,900
It doesn&#39;t have to be a

117
00:06:33,900 --> 00:06:37,980
a hardcoded value, a literal value. I&#39;m using a function to get

118
00:06:37,980 --> 00:06:41,300
two days ago as the value here. This is 

119
00:06:41,300 --> 00:06:44,960
actually DATEADD in. Oh, I beg your pardon, two minutes ago.

120
00:06:44,960 --> 00:06:48,319
DATEADD in minutes -2 to the current date and time.

121
00:06:48,319 --> 00:06:52,990
But that&#39;s actually dropping in the date or is it dropping in the function?

122
00:06:52,990 --> 00:06:56,620
Will it update itself afterwards? It will drop in the result of the function, it&#39;s

123
00:06:56,620 --> 00:06:57,530
the value.

124
00:06:57,530 --> 00:07:00,780
It won&#39;t then continue to update to always keep itself two minutes ago from

125
00:07:00,780 --> 00:07:01,199
now.

126
00:07:01,199 --> 00:07:05,180
Okay. It&#39;s whatever the result is that gets inserted. Then I&#39;ve got the

127
00:07:05,180 --> 00:07:08,770
salesperson, the customer, the telephone number, and there are no notes for this

128
00:07:08,770 --> 00:07:10,030
one, so I&#39;m inserting null. 

129
00:07:10,030 --> 00:07:14,830
And then for the second row, I&#39;m inserting the default value for

130
00:07:14,830 --> 00:07:18,199
our CallTime.

131
00:07:18,199 --> 00:07:21,900
This salesperson, this customer, this telephone number,

132
00:07:21,900 --> 00:07:25,539
and some notes in this one. So if I&#39;m inserting multiple rows,

133
00:07:25,539 --> 00:07:29,199
they don&#39;t all have to include all the same information.

134
00:07:29,199 --> 00:07:34,219
If I&#39;d had a list of columns after the INSERT, they would all have to have those columns and

135
00:07:34,219 --> 00:07:35,389
because I haven&#39;t

136
00:07:35,389 --> 00:07:40,210
they&#39;ve got to have something for all the columns other than the identity.

137
00:07:40,210 --> 00:07:45,650
But that could be a null or it could be a default. So I&#39;ll go ahead and insert that.

138
00:07:45,650 --> 00:07:49,009
I get two rows affected returned back.

139
00:07:49,009 --> 00:07:52,409
And if I have a look at what&#39;s in the table now,

140
00:07:52,409 --> 00:07:56,650
sure enough two new rows in there and I can see that one was

141
00:07:56,650 --> 00:08:00,550
two minutes previously and then I&#39;ve got the one that was just inserted

142
00:08:00,550 --> 00:08:04,919
at this current time. So

143
00:08:04,919 --> 00:08:08,400
far I&#39;ve been explicitly providing values. And actually that&#39;s fairly

144
00:08:08,400 --> 00:08:10,900
typical. If you&#39;re building an application, typically

145
00:08:10,900 --> 00:08:14,770
the application gathers the data from the user and then does an INSERT to insert it into the

146
00:08:14,770 --> 00:08:15,430
table.

147
00:08:15,430 --> 00:08:18,560
What I&#39;m doing now is I&#39;m going to

148
00:08:18,560 --> 00:08:21,690
look at inserting the results of a query. Perhaps I already have

149
00:08:21,690 --> 00:08:26,500
another table that&#39;s got some data in it. And I want to take that and insert it into 

150
00:08:26,500 --> 00:08:30,199
this table. So in this case I&#39;m taking data from 

151
00:08:30,199 --> 00:08:33,820
the sales Customer table,

152
00:08:33,820 --> 00:08:38,870
and what I&#39;m doing is I&#39;m saying that I did a sales promotion call

153
00:08:38,870 --> 00:08:42,610
and I&#39;m going to go ahead and do that just now. And I&#39;m going to do that for

154
00:08:42,610 --> 00:08:47,459
all of my contacts at the Big-Time Bike Store.

155
00:08:47,459 --> 00:08:52,339
So I&#39;m about to do a sales promotional call to there. I&#39;ve already got in that table, I&#39;ve got

156
00:08:52,339 --> 00:08:53,100
the

157
00:08:53,100 --> 00:08:56,180
relevant SalesPerson, the CustomerID, the PhoneNumber,

158
00:08:56,180 --> 00:09:00,790
and I&#39;m going to add a note that says this is a sales promotion call to there.

159
00:09:00,790 --> 00:09:05,120
So I don&#39;t need to explicitly provide those values. I can go and get them from that other table.

160
00:09:05,120 --> 00:09:08,340
So INSERT into that table SELECT 

161
00:09:08,340 --> 00:09:12,460
this from the other table. And it finds

162
00:09:12,460 --> 00:09:16,140
two, so I&#39;ve obviously got two records for the Big-Time Bike Store

163
00:09:16,140 --> 00:09:20,080
customer. So there must be two different contacts there. And if I go and

164
00:09:20,080 --> 00:09:25,110
have a look at that, sure enough I&#39;ve got two different

165
00:09:25,110 --> 00:09:29,580
sales promotion calls to that customer both made by the same salesperson

166
00:09:29,580 --> 00:09:32,900
but to different customers in the customer table presumably because

167
00:09:32,900 --> 00:09:35,010
there&#39;s two contacts. Perhaps it has two branches,

168
00:09:35,010 --> 00:09:38,350
two locations. So you actually supplied some information

169
00:09:38,350 --> 00:09:42,830
with that, the notes. And grabbed some information from existing data.

170
00:09:42,830 --> 00:09:46,800
Yup, so I&#39;ve combined the explicit values and data from a table.

171
00:09:46,800 --> 00:09:51,500
Now we talked about identities and we&#39;ve been inserting identities, you can see the

172
00:09:51,500 --> 00:09:52,740
CallID is being

173
00:09:52,740 --> 00:09:56,380
you know incrementally added each time I insert a row into here.

174
00:09:56,380 --> 00:10:00,470
Again like we spoke about when we had the slides, we talked about

175
00:10:00,470 --> 00:10:03,610
how do I get up that value back. And we saw a number functions.

176
00:10:03,610 --> 00:10:07,310
In this case I&#39;m going to use the SCOPE_IDENTITY function. So what I can do is do

177
00:10:07,310 --> 00:10:08,490
my INSERT

178
00:10:08,490 --> 00:10:11,720
and then get back the id

179
00:10:11,720 --> 00:10:16,170
of what I inserted. And so let&#39;s go ahead and do that and I&#39;m just going to

180
00:10:16,170 --> 00:10:19,720
do these one by one. I could --  typically an application would sort of 

181
00:10:19,720 --> 00:10:21,950
submit these in a batch and run them all in one go.

182
00:10:21,950 --> 00:10:25,180
Because I know I haven&#39;t got any other users at the moment, I&#39;m not going to worry about

183
00:10:25,180 --> 00:10:28,850
losing the identity. So I&#39;m going to go ahead and do the INSERT.

184
00:10:28,850 --> 00:10:31,910
And then if I have a look at SCOPE_IDENTITY, 

185
00:10:31,910 --> 00:10:36,450
I can see that actually the value 8 was used. That&#39;s the last identity within the

186
00:10:36,450 --> 00:10:36,900
current

187
00:10:36,900 --> 00:10:40,510
scope. And if I go and have a look at the data in the table,

188
00:10:40,510 --> 00:10:45,339
sure enough I can actually see that row number 8 is the one that I just inserted

189
00:10:45,339 --> 00:10:45,760
there.

190
00:10:45,760 --> 00:10:49,150
Now

191
00:10:49,150 --> 00:10:52,780
there might be occasions where I want to explicitly override

192
00:10:52,780 --> 00:10:56,590
the identity. Perhaps I have gone and deleted a call that I had made previously,

193
00:10:56,590 --> 00:11:00,210
and I then want to reinsert the details of that call.

194
00:11:00,210 --> 00:11:03,470
But I want it to still have the original number, so I want to override, I don&#39;t want to 

195
00:11:03,470 --> 00:11:04,750
generate a new identity. I want to 

196
00:11:04,750 --> 00:11:09,020
explicitly provide the identity. And there is a way of doing that. I can

197
00:11:09,020 --> 00:11:12,410
set an option called IDENTITY_INSERT ON

198
00:11:12,410 --> 00:11:15,470
for a particular table. So if the table has an identity column,

199
00:11:15,470 --> 00:11:18,890
I can do this SET IDENTITY_INSERT for that table ON.

200
00:11:18,890 --> 00:11:25,270
So I go ahead and do that. Then I can insert into the table and explicitly

201
00:11:25,270 --> 00:11:29,340
specify, I&#39;m inserting the CallID value here, and it&#39;s number 9 in this case.

202
00:11:29,340 --> 00:11:36,310
So I&#39;m going to go ahead and insert that. And what you probably immediately then want

203
00:11:36,310 --> 00:11:37,980
to do is turn IDENTITY_INSERT off  

204
00:11:37,980 --> 00:11:41,660
so that any subsequent inserts get the automatic identity.

205
00:11:41,660 --> 00:11:45,930
And now if I go and have a look at that table,

206
00:11:45,930 --> 00:11:50,390
sure enough it has explicitly inserted

207
00:11:50,390 --> 00:11:54,640
number 9. That&#39;s a 9 because you told it to be a number. Because I told it to be a 9.

208
00:11:54,640 --> 00:11:59,130
And it will now go on, the next time I do an insert, it will just carry on using the

209
00:11:59,130 --> 00:12:02,440
identity columns, so the next one will be 10 and so on 

210
00:12:02,440 --> 00:12:02,730
automatically.

