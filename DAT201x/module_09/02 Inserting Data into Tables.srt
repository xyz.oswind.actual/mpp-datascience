0
00:00:03,490 --> 00:00:05,890
Alright inserting data into tables then.

1
00:00:05,890 --> 00:00:11,330
Obviously I can just insert a row, and I can specify the values that I want to

2
00:00:11,330 --> 00:00:12,330
insert into that. 

3
00:00:12,330 --> 00:00:15,519
So I could explicitly say insert

4
00:00:15,519 --> 00:00:21,190
a product with this product id, this name, this price, this color and

5
00:00:21,190 --> 00:00:24,830
whatever. I can specify those values. 

6
00:00:24,830 --> 00:00:29,270
If the table that I&#39;m inserting into has and identity column -- do you want to just quickly remind us

7
00:00:29,270 --> 00:00:30,540
what an identity column is?

8
00:00:30,540 --> 00:00:33,739
So a lot of our ID&#39;s we&#39;ve seen have numbers

9
00:00:33,739 --> 00:00:36,980
and the unit -- they have to be unique  because that&#39;s what sort of the 

10
00:00:36,980 --> 00:00:39,390
primary key it uniquely identifies that row.

11
00:00:39,390 --> 00:00:44,520
One-way, a straightforward way of making sure that&#39;s unique, is making sure

12
00:00:44,520 --> 00:00:45,120
that

13
00:00:45,120 --> 00:00:48,500
we have a counter. So the first one is number 1, 

14
00:00:48,500 --> 00:00:52,270
next one is number 2, 3, 4, 5, 6, 7 and  automatically it would generate

15
00:00:52,270 --> 00:00:56,510
the next number. We can actually in SQL Server 

16
00:00:56,510 --> 00:01:01,850
tell it not to go in 1, 2, 3, 4, 5, we can make it jump a few as well if you wanted to.

17
00:01:01,850 --> 00:01:06,119
So you could actually deliberately make it do every 10 if that&#39;s more useful to you.

18
00:01:06,119 --> 00:01:09,850
Make it increment by a different amount. Or we could even start at a different number because

19
00:01:09,850 --> 00:01:11,329
we already have a hundred products,

20
00:01:11,329 --> 00:01:16,090
we&#39;re adding into that and I would like to start at 101. We can start different

21
00:01:16,090 --> 00:01:17,030
number if we wanted to.

22
00:01:17,030 --> 00:01:20,229
But I guess the point is when we create the table,

23
00:01:20,229 --> 00:01:23,310
we specify this column as an identity column,

24
00:01:23,310 --> 00:01:27,090
we tell it what they call the seed and the increment, you know where to start from

25
00:01:27,090 --> 00:01:30,840
and how much to count up in. And then every time I insert rows into that, I don&#39;t insert 

26
00:01:30,840 --> 00:01:32,340
an explicit value into that column, it

27
00:01:32,340 --> 00:01:36,069
automatically generates a unique number using that sequence of numbers for

28
00:01:36,069 --> 00:01:36,659
that table.

29
00:01:36,659 --> 00:01:40,929
Yeah? Yep. Just another point on that that we need to remember though.

30
00:01:40,929 --> 00:01:44,369
In the future we may well delete one of those rows. They&#39;re 

31
00:01:44,369 --> 00:01:48,479
not a counter. You can&#39;t go to the last one and say well we&#39;ve got 76 products because it&#39;s

32
00:01:48,479 --> 00:01:49,349
number 76.

33
00:01:49,349 --> 00:01:52,450
Because we may have deleted 73 or whatever, yeah. 

34
00:01:52,450 --> 00:01:57,020
So yes an identity is a fairly unique number that gets generated.

35
00:01:57,020 --> 00:02:00,719
We can insert into a table without  inserting explicitly into that column we just

36
00:02:00,719 --> 00:02:01,479
have

37
00:02:01,479 --> 00:02:06,090
the ID generated for us. And similarly we might have columns that allow null.

38
00:02:06,090 --> 00:02:09,250
We&#39;ve seen nulls quite a lot in this course. I might have a column that

39
00:02:09,250 --> 00:02:12,319
allows a null value, perhaps it&#39;s a middle name column or something like that.

40
00:02:12,319 --> 00:02:15,540
And I can you know explicitly insert 

41
00:02:15,540 --> 00:02:20,040
a null or I could just not insert into that column and SQL Server knows that well

42
00:02:20,040 --> 00:02:23,480
this column allows null, you didn&#39;t provide a value for it, therefore it&#39;s a null.

43
00:02:23,480 --> 00:02:27,760
And the other thing that we can do is we can create default

44
00:02:27,760 --> 00:02:32,739
value constraints. So I might, a classic example of this might be in orders table,

45
00:02:32,739 --> 00:02:34,069
a Sales Orders table,

46
00:02:34,069 --> 00:02:38,200
there&#39;s an order date. And I might set up the table so that well by default,

47
00:02:38,200 --> 00:02:41,159
if you don&#39;t tell me the day I&#39;m going to assume it&#39;s today. I&#39;m going to assume the order is

48
00:02:41,159 --> 00:02:41,669
right now.

49
00:02:41,669 --> 00:02:45,269
And it will insert the current date and perhaps the current date and time

50
00:02:45,269 --> 00:02:48,340
into that column. So I could specify

51
00:02:48,340 --> 00:02:51,409
default values that are inserted if the insert

52
00:02:51,409 --> 00:02:55,780
doesn&#39;t specify what that value should be. I can use nulls and I can insert nulls

53
00:02:55,780 --> 00:02:59,079
if that value isn&#39;t provided. And I could insert identity columns.

54
00:02:59,079 --> 00:03:03,040
And like I say, like it says on the slide, you can explicitly

55
00:03:03,040 --> 00:03:07,319
specify null or if you don&#39;t provide the value for that column

56
00:03:07,319 --> 00:03:11,879
then if the null is allowed, then the system will let the row in and just put a null

57
00:03:11,879 --> 00:03:12,590
in that

58
00:03:12,590 --> 00:03:17,540
location. So that&#39;s inserting values, if I&#39;m inserting a row of data I can 

59
00:03:17,540 --> 00:03:19,669
do that. I can also

60
00:03:19,669 --> 00:03:25,449
insert into a table using, not explicit values that I&#39;m providing as part of my

61
00:03:25,449 --> 00:03:28,979
application code or whatever, but I could insert the results of a query

62
00:03:28,979 --> 00:03:33,549
against another table. So if I want to transfer rows or duplicate rows in one

63
00:03:33,549 --> 00:03:35,049
table across into another table,

64
00:03:35,049 --> 00:03:39,659
then I can INSERT the results of a  SELECT statement against that table.

65
00:03:39,659 --> 00:03:43,389
And to be technical, I can also INSERT  the results

66
00:03:43,389 --> 00:03:47,259
of an execution of a stored procedure which we will talk about a little later on

67
00:03:47,259 --> 00:03:48,049
in this course

68
00:03:48,049 --> 00:03:52,030
which also returns rows that get inserted. So it&#39;s not just about inserting one

69
00:03:52,030 --> 00:03:53,370
row at a time, I can insert

70
00:03:53,370 --> 00:03:59,569
multiple rows that are the result of a query as well. There is one

71
00:03:59,569 --> 00:04:04,790
final option, SELECT INTO. What SELECT INTO does is it creates a brand

72
00:04:04,790 --> 00:04:05,659
new table

73
00:04:05,659 --> 00:04:09,209
from the results of a SELECT statement. So if I,

74
00:04:09,209 --> 00:04:13,459
we talked about INSERT from a SELECT, the table I&#39;m inserting into would already have

75
00:04:13,459 --> 00:04:14,639
to exist in that case.

76
00:04:14,639 --> 00:04:18,299
If I use SELECT INTO I&#39;m explicitly creating a new table

77
00:04:18,298 --> 00:04:23,610
and populating it with the results of the query. Now the new table won&#39;t have any

78
00:04:23,610 --> 00:04:27,449
constraints or indexes or primary keys or any of that kind of stuff. It&#39;s just a new table

79
00:04:27,449 --> 00:04:29,129
with a bunch of columns that contain

80
00:04:29,129 --> 00:04:32,719
the results of the query. And I guess one

81
00:04:32,719 --> 00:04:37,129
point we do need to make about that is that currently at the time that we&#39;re

82
00:04:37,129 --> 00:04:37,870
recording this, it

83
00:04:37,870 --> 00:04:41,689
is supported in SQL Sever but it&#39;s not supported in 

84
00:04:41,689 --> 00:04:46,449
Azure SQL Database. So you can&#39;t use SELECT INTO on Azure SQL Database. But you could create a table and then

85
00:04:46,449 --> 00:04:47,990
do an INSERT SELECT,

86
00:04:47,990 --> 00:04:51,719
yes, so you can get the same result. Yes if you know what the schema of the results is

87
00:04:51,719 --> 00:04:53,270
going to be, you could create a table

88
00:04:53,270 --> 00:04:57,599
ready for your results and then use a SELECT INTO. So it&#39;s not really a major

89
00:04:57,599 --> 00:05:03,930
blocking issue. So we talked a little bit about generating

90
00:05:03,930 --> 00:05:05,590
identifiers and we talked about

91
00:05:05,590 --> 00:05:10,550
this notion of an identity column. So in in an identity column we generate sequential

92
00:05:10,550 --> 00:05:11,219
numbers

93
00:05:11,219 --> 00:05:14,650
automatically every time a row is inserted.

94
00:05:14,650 --> 00:05:18,569
And you can specify like we said earlier and optionally the seed and the

95
00:05:18,569 --> 00:05:19,529
increment. So I could,

96
00:05:19,529 --> 00:05:24,469
by default it will start at 1 and go up in 1&#39;s, but I could start at 100 and go up 

97
00:05:24,469 --> 00:05:26,389
in 10s if I particularly wanted to

98
00:05:26,389 --> 00:05:29,949
have a sequence of IDs like that.

99
00:05:29,949 --> 00:05:33,990
One of the obvious questions is if I just inserted a row into a table,

100
00:05:33,990 --> 00:05:37,699
now that this might be inserting an order and the Order ID might be an

101
00:05:37,699 --> 00:05:39,440
identity column,

102
00:05:39,440 --> 00:05:43,229
how do I then get what the Order ID was back again?

103
00:05:43,229 --> 00:05:46,629
So if my app, if you think about an application on a website were you

104
00:05:46,629 --> 00:05:49,810
place your order, typically the first thing it then says is &quot;Thank you for your order. 

105
00:05:49,810 --> 00:05:52,060
Your order number is&quot; and it tells me.

106
00:05:52,060 --> 00:05:55,659
So would you just go for the highest order number? Well 

107
00:05:55,659 --> 00:05:59,069
yeah there&#39;s a bit of a problem there because of course multiple orders could be

108
00:05:59,069 --> 00:06:02,449
getting placed at the same time. So there&#39;s a risk that between your order being placed and 

109
00:06:02,449 --> 00:06:05,080
you going to find what was the highest one in the table,

110
00:06:05,080 --> 00:06:08,770
someone else might have placed an order. Yeah, right. So

111
00:06:08,770 --> 00:06:12,639
there are all sorts of issues with that. To help you that, there are a bunch of built in

112
00:06:12,639 --> 00:06:15,659
global variables and functions that you can use.

113
00:06:15,659 --> 00:06:18,939
And we&#39;ll talk about each of those here. There&#39;s a

114
00:06:18,939 --> 00:06:22,659
global system variable called IDENTITY  which gets the last

115
00:06:22,659 --> 00:06:25,949
identity value that was generated in the current session.

116
00:06:25,949 --> 00:06:30,099
So for this connection, what was the last identity value I generated?

117
00:06:30,099 --> 00:06:34,009
Now that might be useful if I&#39;m inserting 

118
00:06:34,009 --> 00:06:36,820
an order and getting back the Order ID.

119
00:06:36,820 --> 00:06:40,880
You kind of have to be careful though because if you insert

120
00:06:40,880 --> 00:06:46,300
an order, and that order includes order details, which you then subsequently insert,

121
00:06:46,300 --> 00:06:50,620
and those have identity columns as well, then actually the last identity

122
00:06:50,620 --> 00:06:52,070
that you inserted will be

123
00:06:52,070 --> 00:06:56,870
the last of the order details. It won&#39;t be the order number. So the last identity on any

124
00:06:56,870 --> 00:06:59,890
tables? Yeah it&#39;s the last identity that&#39;s connected -- 

125
00:06:59,890 --> 00:07:04,280
that was created in this connection. There&#39;s another

126
00:07:04,280 --> 00:07:07,420
similar, there&#39;s a function that returns something similar that&#39;s not

127
00:07:07,420 --> 00:07:11,220
scoped to the entire session, it&#39;s scoped to the current scope of

128
00:07:11,220 --> 00:07:12,890
the query, the current query that 

129
00:07:12,890 --> 00:07:16,690
you&#39;ve used. And that will bring you back, the SCOPE_IDENTITY is the name of 

130
00:07:16,690 --> 00:07:18,850
the function, it will bring you back the last identity

131
00:07:18,850 --> 00:07:22,220
that&#39;s in scope of the query that I&#39;ve just executed.

132
00:07:22,220 --> 00:07:25,470
Again it might not necessarily be the one you&#39;re looking for if you have a

133
00:07:25,470 --> 00:07:27,760
statement inserted into multiple tables and

134
00:07:27,760 --> 00:07:31,500
each of those had an identity. But that&#39;s possible as well.

135
00:07:31,500 --> 00:07:35,390
The other and I guess the most  explicit way of doing this is the IDENT_CURRENT

136
00:07:35,390 --> 00:07:39,460
function to which you pass a table name and it will pass you

137
00:07:39,460 --> 00:07:42,500
the last identity inserted into that table.

138
00:07:42,500 --> 00:07:46,800
Now, that might sound like that&#39;s exactly what we need, but again

139
00:07:46,800 --> 00:07:50,990
that&#39;s across all sessions. So if someone else has inserted in that table,

140
00:07:50,990 --> 00:07:54,570
it will get the identity they generated. So it&#39;s not an exact science.

141
00:07:54,570 --> 00:07:58,970
Typically in that type of scenario where I&#39;m inserting an order with an order

142
00:07:58,970 --> 00:08:00,350
ID and I want to get that back -- 

143
00:08:00,350 --> 00:08:03,419
well first of all I try and avoid having identity columns on my

144
00:08:03,419 --> 00:08:08,480
order details, and secondly I usually use SCOPE_IDENTITY to get back the value. 

145
00:08:08,480 --> 00:08:12,500
So you could grab that value before you go on to do other things that would

146
00:08:12,500 --> 00:08:16,790
then also use identity. Yeah, exactly. And particularly if it&#39;s within a transaction because you&#39;re going to

147
00:08:16,790 --> 00:08:17,230
get

148
00:08:17,230 --> 00:08:21,080
the value that&#39;s within your transaction before that transaction rolls back.

149
00:08:21,080 --> 00:08:24,220
And we will talk about transactions later on in the course if you don&#39;t

150
00:08:24,220 --> 00:08:25,020
already know what they are.

151
00:08:25,020 --> 00:08:29,400
So you can see the code example, I&#39;m  inserting into the Sales Orders table a

152
00:08:29,400 --> 00:08:30,950
CustomerID and an Amount.

153
00:08:30,950 --> 00:08:35,620
And I&#39;m explicitly specifying the values that are being inserted. So it&#39;s CustomerID 12

154
00:08:35,620 --> 00:08:40,159
and the Amount is 2056.99. And then once the insert

155
00:08:40,159 --> 00:08:43,880
is done, I&#39;m selecting the SCOPE_IDENTITY and that&#39;s the OrderID, that&#39;s

156
00:08:43,880 --> 00:08:45,290
the last identity that was

157
00:08:45,290 --> 00:08:49,019
inserted.

158
00:08:49,019 --> 00:08:50,810
Now there is actually another way of

159
00:08:50,810 --> 00:08:55,920
of dealing with identifiers. And I guess we first of all need to set up why we

160
00:08:55,920 --> 00:08:57,149
need another way to do this.

161
00:08:57,149 --> 00:09:02,069
If you think about perhaps a database where you have two types of orders,

162
00:09:02,069 --> 00:09:06,709
perhaps you have orders that are placed

163
00:09:06,709 --> 00:09:10,079
through your website and you have orders that are placed by

164
00:09:10,079 --> 00:09:14,089
customers that you explicitly ship to. So you&#39;ve got retailers that buy things

165
00:09:14,089 --> 00:09:14,930
from you and you&#39;ve got

166
00:09:14,930 --> 00:09:18,540
web customers that buy directly from you. Now you might

167
00:09:18,540 --> 00:09:21,850
choose to store those order details in two separate tables

168
00:09:21,850 --> 00:09:27,730
because you might want details from the retailers about the supplier contact ID,

169
00:09:27,730 --> 00:09:31,170
and the telephone number, and the address of the store, and all that kind of

170
00:09:31,170 --> 00:09:31,589
thing.

171
00:09:31,589 --> 00:09:34,690
And for your web customers, you might  have a different set of

172
00:09:34,690 --> 00:09:38,040
columns that you want to store about those. So you&#39;ve got different tables for

173
00:09:38,040 --> 00:09:39,019
your different types of customers,

174
00:09:39,019 --> 00:09:42,230
and you might have different details for the orders themselves.

175
00:09:42,230 --> 00:09:46,519
So your retailer orders might include some sort of purchase order number

176
00:09:46,519 --> 00:09:50,630
whereas your web orders generally won&#39;t have that because you wouldn&#39;t have raised 

177
00:09:50,630 --> 00:09:51,490
a purchase order.

178
00:09:51,490 --> 00:09:55,019
So in that case I&#39;m inserting

179
00:09:55,019 --> 00:09:58,410
into two different tables, but I quite possibly want to keep my

180
00:09:58,410 --> 00:10:04,009
order numbers unique across both tables. And an identity column won&#39;t do that. An identity column

181
00:10:04,009 --> 00:10:07,630
is specific to this table. If I create an identity column on another table

182
00:10:07,630 --> 00:10:11,240
then it will generate its own numbers independently.

183
00:10:11,240 --> 00:10:15,880
Now you could be clever and you could generate a seed and an increment on

184
00:10:15,880 --> 00:10:18,670
one table to make sure everyone is an odd number and everyone on the other one is

185
00:10:18,670 --> 00:10:19,490
an even number,

186
00:10:19,490 --> 00:10:24,310
but really that&#39;s not the best solution. So an alternative

187
00:10:24,310 --> 00:10:29,060
is to use something called sequences. And a sequence is an object that you create in a

188
00:10:29,060 --> 00:10:29,750
database

189
00:10:29,750 --> 00:10:33,709
that generates sequential numbers but it&#39;s not tied explicitly to any one

190
00:10:33,709 --> 00:10:34,889
particular table.

191
00:10:34,889 --> 00:10:38,279
It will generate, every time you need a new number you just ask it for 

192
00:10:38,279 --> 00:10:39,269
the next value,

193
00:10:39,269 --> 00:10:43,730
and it literally is SELECT NEXT VALUE FOR the sequence.

194
00:10:43,730 --> 00:10:47,540
And that will give you that number, and you can then use that in any table you like as

195
00:10:47,540 --> 00:10:48,329
an identifier.

196
00:10:48,329 --> 00:10:52,709
So that would get around this problem of wanting to have a unique order number

197
00:10:52,709 --> 00:10:54,709
across two different order tables

198
00:10:54,709 --> 00:10:59,209
or three or four. However many order table you&#39;ve got. You just have this central

199
00:10:59,209 --> 00:11:00,230
sequence

200
00:11:00,230 --> 00:11:04,880
that controls you know issuing new numbers. So we could then UNION those web

201
00:11:04,880 --> 00:11:06,190
queries with those

202
00:11:06,190 --> 00:11:10,600
retailer queries and it appears just as it should. These are all of our orders and 

203
00:11:10,600 --> 00:11:11,890
they&#39;ve all got unique numbers.

204
00:11:11,890 --> 00:11:13,480
Exactly.

