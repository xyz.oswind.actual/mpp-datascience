0
00:00:02,370 --> 00:00:06,399
So this module was a bit of a departure from the previous ones.

1
00:00:06,399 --> 00:00:10,750
We had previously talked about selecting from tables and really just

2
00:00:10,750 --> 00:00:15,410
focusing on getting data out of tables. In this module we&#39;ve talked about

3
00:00:15,410 --> 00:00:20,120
inserting new rows into tables and either individual rows or rows that 

4
00:00:20,120 --> 00:00:21,460
are results of queries

5
00:00:21,460 --> 00:00:26,600
into a table. We&#39;ve talked about updating existing rows in tables

6
00:00:26,600 --> 00:00:29,690
either by updating specific values

7
00:00:29,690 --> 00:00:33,480
in the columns or by using that MERGE statement that we talked about

8
00:00:33,480 --> 00:00:34,050
where you

9
00:00:34,050 --> 00:00:38,030
might want to check and see if the row  already exists then update it, if it

10
00:00:38,030 --> 00:00:39,180
doesn&#39;t then insert it. 

11
00:00:39,180 --> 00:00:43,320
And then finally we talked about deleting data from tables and we talked about using the

12
00:00:43,320 --> 00:00:47,180
WHERE clause to restrict which rows get deleted. And we talked about using

13
00:00:47,180 --> 00:00:48,350
TRUNCATE table

14
00:00:48,350 --> 00:00:52,739
if what you want to do is just completely empty the table. So

15
00:00:52,739 --> 00:00:56,210
some useful things there that you will use if you&#39;re 

16
00:00:56,210 --> 00:01:00,270
using Transact-SQL at all to work with databases. A lot of the time it is querying

17
00:01:00,270 --> 00:01:01,360
to get reports, but

18
00:01:01,360 --> 00:01:05,460
almost certainly you will be updating and inserting data at some point.

19
00:01:05,459 --> 00:01:09,590
So some useful stuff there. There&#39;s a lab for you to go and try.

20
00:01:09,590 --> 00:01:13,930
And as always on this course, we will get you to use the AdventureWorksLT

21
00:01:13,930 --> 00:01:14,780
database

22
00:01:14,780 --> 00:01:19,600
in Azure SQL Database to go and try out some of the lab exercises. So

23
00:01:19,600 --> 00:01:23,140
download those labs and make sure that you have a go at that. And when you&#39;re

24
00:01:23,140 --> 00:01:23,869
comfortable

25
00:01:23,869 --> 00:01:27,450
that your inserting, updating, and deleting table, data in tables quite

26
00:01:27,450 --> 00:01:28,020
happily,

27
00:01:28,020 --> 00:01:31,630
come back and join us for the next module when we will look at some

28
00:01:31,630 --> 00:01:32,610
techniques for

29
00:01:32,610 --> 00:01:36,900
programming with Transact-SQL. And doing some more sophisticated logic in your Transact-SQL

30
00:01:36,900 --> 00:01:37,270
code.

