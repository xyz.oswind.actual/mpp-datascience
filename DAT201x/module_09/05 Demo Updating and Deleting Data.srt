0
00:00:03,330 --> 00:00:06,100
So let&#39;s take a look at all these different types of modification

1
00:00:06,100 --> 00:00:07,279
we&#39;ve talked about here.

2
00:00:07,279 --> 00:00:10,670
And let&#39;s start with doing an UPDATE. So

3
00:00:10,670 --> 00:00:13,840
I&#39;ve got my SalesLog table you might remember, and had

4
00:00:13,840 --> 00:00:17,750
some data in it. So I&#39;ve got my

5
00:00:17,750 --> 00:00:21,960
various calls that I&#39;ve logged. And quite a lot of them because I&#39;m allowing

6
00:00:21,960 --> 00:00:26,020
nulls in the Notes column, quite a lot of them have a null value in there.

7
00:00:26,020 --> 00:00:30,100
And generally although I&#39;m allowing nulls, it&#39;s not really something that I&#39;m comfortable

8
00:00:30,100 --> 00:00:32,309
with having null. It kind of means unknown.

9
00:00:32,308 --> 00:00:35,840
I&#39;d like to be more explicit than that. So I&#39;m going to update this table

10
00:00:35,840 --> 00:00:39,329
so that where currently the notes is null,

11
00:00:39,329 --> 00:00:44,180
I want to change it to explicitly &quot;No notes&quot;. I want to actually put a string value in

12
00:00:44,180 --> 00:00:44,560
there.

13
00:00:44,560 --> 00:00:50,050
So if I go ahead and do that update, there are 5 rows affected.

14
00:00:50,050 --> 00:00:52,950
And then again this is where that row count that comes back is really

15
00:00:52,950 --> 00:00:53,620
useful.

16
00:00:53,620 --> 00:00:56,809
With an INSERT, you generally know how many rows you&#39;re inserting unless you&#39;re

17
00:00:56,809 --> 00:00:57,540
inserting 

18
00:00:57,540 --> 00:01:01,210
from a query. With an UPDATE, you know you might want to know well how many rows were

19
00:01:01,210 --> 00:01:02,840
updated. So that&#39;s pretty useful.

20
00:01:02,840 --> 00:01:05,960
And if I go and SELECT from that table,

21
00:01:05,960 --> 00:01:09,920
I can now see that where previously I had nulls, I&#39;ve now got &quot;No notes&quot;

22
00:01:09,920 --> 00:01:15,319
in that column. So it&#39;s updated that column. And as I said when we looked at the slides,

23
00:01:15,319 --> 00:01:19,119
you don&#39;t have to just update one column. I could update multiple columns, so I could

24
00:01:19,119 --> 00:01:19,569
set

25
00:01:19,569 --> 00:01:23,759
the SalesPerson equals an empty string and the PhoneNumber equals an empty

26
00:01:23,759 --> 00:01:24,819
string in this case.

27
00:01:24,819 --> 00:01:29,490
Now, if I do that, what do you spot immediately about that query Jeff?

28
00:01:29,490 --> 00:01:33,170
So yeah, we can see in there that you&#39;re updating

29
00:01:33,170 --> 00:01:37,810
the CallLog and setting the SalesPerson to nothing and the PhoneNumber to nothing,

30
00:01:37,810 --> 00:01:41,659
but you&#39;re doing it for everyone. So that&#39;s fine if you wanted to clear out, we don&#39;t 

31
00:01:41,659 --> 00:01:42,819
want phone numbers or sales 

32
00:01:42,819 --> 00:01:46,310
people anymore, but we&#39;re not going to  have those for anyone. Right, yeah. There&#39;s no

33
00:01:46,310 --> 00:01:48,159
WHERE clause, so it&#39;s going to affect every record. 

34
00:01:48,159 --> 00:01:51,450
Right? And just to prove the point, because I am I crazy,

35
00:01:51,450 --> 00:01:56,190
we&#39;ll run it. There were 9 rows affected. And if I now go and have a look at my 

36
00:01:56,190 --> 00:02:01,900
CallLog table, I can actually see that

37
00:02:01,900 --> 00:02:05,350
my SalesPerson column and my PhoneNumber column, 

38
00:02:05,350 --> 00:02:08,920
they don&#39;t have null in it, they have an empty string. Yeah. So

39
00:02:08,919 --> 00:02:12,690
well that&#39;s not really very good. What I might want to do is fix that. And I&#39;m going to 

40
00:02:12,690 --> 00:02:16,310
do that by updating from a query. So I&#39;m

41
00:02:16,310 --> 00:02:20,060
updating the CallLog table, I&#39;m setting the SalesPerson equal to

42
00:02:20,060 --> 00:02:23,770
whatever sales person value I get, PhoneNumber equal to whatever phone number I get

43
00:02:23,770 --> 00:02:28,090
from the Customer table WHERE its ID matches the

44
00:02:28,090 --> 00:02:31,510
CustomerID in my CallLog table. That&#39;s  handy that you&#39;ve still got the data that you

45
00:02:31,510 --> 00:02:32,130
just deleted.

46
00:02:32,130 --> 00:02:35,290
Isn&#39;t it handy that I&#39;ve still got that. Yeah. So I could go ahead and do an UPDATE

47
00:02:35,290 --> 00:02:39,190
and the point is it&#39;s an UPDATE FROM, so I&#39;m not explicitly setting the values. In

48
00:02:39,190 --> 00:02:42,460
the same I could do an INSERT FROM a query, I can do an UPDATE FROM

49
00:02:42,460 --> 00:02:46,860
a query. That&#39;s affected 9 rows and I can breathe a huge sigh of relief because

50
00:02:46,860 --> 00:02:49,100
I&#39;ve now got my sales person and my phone numbers

51
00:02:49,100 --> 00:02:52,100
back again. So

52
00:02:52,100 --> 00:02:56,950
updating data is pretty straightforward using the UPDATE statement like that.

53
00:02:56,950 --> 00:03:01,440
What about deleting data? Well I might want to say I want to delete all the older

54
00:03:01,440 --> 00:03:02,989
calls. I might want to archive off

55
00:03:02,989 --> 00:03:06,480
some of these calls and say I&#39;m not  really interested in calls that took

56
00:03:06,480 --> 00:03:10,400
to place quite a while ago. In this case I&#39;m only interested in the calls that happened this week.

57
00:03:10,400 --> 00:03:14,280
So, I want to get rid of any calls that happened before you know 7 days ago.

58
00:03:14,280 --> 00:03:17,610
And you can see the very first one I inserted actually was

59
00:03:17,610 --> 00:03:20,830
on the 1st of January which given that today

60
00:03:20,830 --> 00:03:25,040
according to this is the 27th of January, so that&#39;s an old call. I don&#39;t really care about

61
00:03:25,040 --> 00:03:25,600
it anymore.

62
00:03:25,600 --> 00:03:29,560
Let&#39;s go ahead and delete that.

63
00:03:29,560 --> 00:03:33,970
One row affected. And if I have a look and see what&#39;s in the table now,

64
00:03:33,970 --> 00:03:38,150
sure enough, call number 1 is now gone. So that&#39;s no longer

65
00:03:38,150 --> 00:03:41,790
kept. So our identity now as well doesn&#39;t actually -- it&#39;s not a 

66
00:03:41,790 --> 00:03:45,700
row count anymore. Yeah, exactly, yeah. The last identity is

67
00:03:45,700 --> 00:03:49,790
9, but there aren&#39;t 9 rows in the table. There&#39;s only 8 rows in the table.

68
00:03:49,790 --> 00:03:54,610
Yep. And then what I might want to do is eventually say well actually 

69
00:03:54,610 --> 00:03:58,080
let&#39;s wipe that call log and start again. Let&#39;s get rid of all the records in there. 

70
00:03:58,080 --> 00:04:00,000
So I can go ahead and I could TRUNCATE the table.

71
00:04:00,000 --> 00:04:04,590
I don&#39;t get a row count. It&#39;s just goes off and deallocates the table.

72
00:04:04,590 --> 00:04:09,300
And if I then do a SELECT, it&#39;s an empty table now. It&#39;s been emptied

73
00:04:09,300 --> 00:04:10,930
ready perhaps to load some more data into.

