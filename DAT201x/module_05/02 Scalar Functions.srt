0
00:00:02,850 --> 00:00:07,920
So the scalar functions to start off with.

1
00:00:07,920 --> 00:00:13,000
It&#39;s a single value output. So if we do scalar function it&#39;s going to return

2
00:00:13,000 --> 00:00:16,859
a value. One single value, it&#39;s not going to return a table

3
00:00:16,859 --> 00:00:20,249
of information, it&#39;s not going to return loads of rows or loads of columns. 

4
00:00:20,249 --> 00:00:24,069
It&#39;s a value. It&#39;s a single value and that&#39;s  gets returned. So today&#39;s date might 

5
00:00:24,069 --> 00:00:25,890
be a valid one. We might want to compare

6
00:00:25,890 --> 00:00:30,060
the due date with today&#39;s date. If it&#39;s greater than that

7
00:00:30,060 --> 00:00:34,330
we&#39;ve got a problem so. Using a function to return today&#39;s date,

8
00:00:34,330 --> 00:00:38,000
we can do that. Can be used like an expression in queries, so

9
00:00:38,000 --> 00:00:42,650
in the same we had things which were concatenating text,

10
00:00:42,650 --> 00:00:43,260
we had

11
00:00:43,260 --> 00:00:47,530
adding up numbers, and these things. We we&#39;re working with

12
00:00:47,530 --> 00:00:51,580
our data in the same way we can do that. So I said we could use the year and

13
00:00:51,580 --> 00:00:52,090
the date,

14
00:00:52,090 --> 00:00:55,810
well we can display that in our query. We can have year

15
00:00:55,810 --> 00:00:59,040
of GetDate gives the year the date now,

16
00:00:59,040 --> 00:01:02,220
and display that -- use an alias the same. So we could say 

17
00:01:02,220 --> 00:01:05,700
as year of today or something, whatever you want. 

18
00:01:05,700 --> 00:01:10,270
And that would display in the same way. Deterministic or 

19
00:01:10,270 --> 00:01:12,880
non-deterministic, right okay.

20
00:01:12,880 --> 00:01:16,200
This is a &lt;laughter&gt; this

21
00:01:16,200 --> 00:01:19,679
if you get onto 

22
00:01:19,679 --> 00:01:23,799
more of the design of databases, there are some things that will have rules about

23
00:01:23,799 --> 00:01:27,189
whether they can or can&#39;t be deterministic or non-deterministic. So we need to know what

24
00:01:27,189 --> 00:01:27,899
that means.

25
00:01:27,899 --> 00:01:32,219
Essentially if I run

26
00:01:32,219 --> 00:01:35,659
a function using

27
00:01:35,659 --> 00:01:40,679
certain data, I can either expect to get exactly the same value back every time,

28
00:01:40,679 --> 00:01:44,990
or there&#39;ll be scenarios we&#39;re I don&#39;t get exactly the same value back every time

29
00:01:44,990 --> 00:01:46,619
even though the data might not have changed.

30
00:01:46,619 --> 00:01:49,909
So I said an example would be what if we had

31
00:01:49,909 --> 00:01:53,859
the comparison with getdate and a due date.

32
00:01:53,859 --> 00:01:57,380
So would that always return the same

33
00:01:57,380 --> 00:02:00,999
value, do you think? Well no because getdate returns

34
00:02:00,999 --> 00:02:04,460
today&#39;s date, so it depends on which day you run it. 

35
00:02:04,460 --> 00:02:07,740
And even more accurate than that, it&#39;s going to be the time as well.

36
00:02:07,740 --> 00:02:10,739
So it almost always is going to be slightly different. Right. So 

37
00:02:10,739 --> 00:02:11,280
that&#39;s

38
00:02:11,280 --> 00:02:15,989
not deterministic. We can&#39;t say what the result will be based on the data going

39
00:02:15,989 --> 00:02:16,630
in.

40
00:02:16,630 --> 00:02:19,910
Right. Whereas if I was to

41
00:02:19,910 --> 00:02:24,709
and concatenate two columns of text, assuming the data hasn&#39;t changed,

42
00:02:24,709 --> 00:02:28,250
we know what the result will be. Right. It won&#39;t change over time.

43
00:02:28,250 --> 00:02:35,250
Ok, so that&#39;s deterministic. I see. And here&#39;s just an example of some

44
00:02:36,050 --> 00:02:39,830
items. There are categories there because there&#39;s a huge

45
00:02:39,830 --> 00:02:42,260
amount of functions in SQL Server. I was going to say yeah.

46
00:02:42,260 --> 00:02:45,560
We&#39;re looking a flavor of these.

47
00:02:45,560 --> 00:02:49,420
Looking in Books Online and the documentation, it has all the information

48
00:02:49,420 --> 00:02:51,180
about all the different functions. 

49
00:02:51,180 --> 00:02:55,709
Once you&#39;ve got an idea of running a few different functions, you&#39;ll get used to

50
00:02:55,709 --> 00:03:00,299
the concept of functions, what they look like, and with Books Online you can

51
00:03:00,299 --> 00:03:00,940
really,

52
00:03:00,940 --> 00:03:04,069
if you think &quot;Oh, I&#39;d like to be able to do this with that data&quot;,

53
00:03:04,069 --> 00:03:08,609
have a look. I&#39;m disappointed you haven&#39;t got a slide on every function. 

54
00:03:08,609 --> 00:03:10,560
I seem to remember

55
00:03:10,560 --> 00:03:13,890
that about and 12-15 years ago we did

56
00:03:13,890 --> 00:03:17,160
once have that sort of slide. &lt;laughter&gt; Now, it would

57
00:03:17,160 --> 00:03:21,940
be many, many slides. So we&#39;re going to have a look at

58
00:03:21,940 --> 00:03:25,549
scalar functions to start off with. So

59
00:03:25,549 --> 00:03:29,900
I&#39;ve mentioned the YEAR function. This will return the year of that value

60
00:03:29,900 --> 00:03:33,970
in there. So if I run this

61
00:03:33,970 --> 00:03:38,370
then we can output

62
00:03:38,370 --> 00:03:41,640
the selling start year

63
00:03:41,640 --> 00:03:45,230
of all of those. So we can see when that product was starting to be sold,

64
00:03:45,230 --> 00:03:48,829
the year that product was starting to be sold. And that&#39;s using a simple function just returning

65
00:03:48,829 --> 00:03:49,329
the year.

66
00:03:49,329 --> 00:03:54,049
Slightly more complex, expanding on that. We&#39;re now going to use year

67
00:03:54,049 --> 00:03:57,190
but also I want some more information. So I want the month

68
00:03:57,190 --> 00:04:00,970
and that&#39;s using DATENAME, that would actually supply the name

69
00:04:00,970 --> 00:04:05,069
the month rather than the number. So like March or 

70
00:04:05,069 --> 00:04:09,440
yeah, rather than just a number. And we have a MONTH function as well to return the 

71
00:04:09,440 --> 00:04:10,750
number so we can do either,

72
00:04:10,750 --> 00:04:12,560
whatever is preferred.

73
00:04:12,560 --> 00:04:16,829
And then the DAY as well we&#39;ve got in there so we can use day functions, month

74
00:04:16,829 --> 00:04:19,039
functions. And you see in there DATENAME as well.

75
00:04:19,039 --> 00:04:25,050
We&#39;ve got the dw, so you&#39;ve different  syntax we can use in here, mm for

76
00:04:25,050 --> 00:04:25,630
our month,

77
00:04:25,630 --> 00:04:29,449
and this is the weekdays is a dw. So you&#39;ve got 

78
00:04:29,449 --> 00:04:33,570
different syntax to provide different bits of your data. So the DATENAME 

79
00:04:33,570 --> 00:04:37,169
function then, depending on the parameters I pass in, I&#39;m going to get

80
00:04:37,169 --> 00:04:39,860
something different back. Absolutely, and you&#39;ll see that now because the

81
00:04:39,860 --> 00:04:43,120
same values going in

82
00:04:43,120 --> 00:04:46,650
on the SellStartMonth and SellStart Weekday, but

83
00:04:46,650 --> 00:04:50,080
we can see one of them is returning June&#39;s, July&#39;s. The other one&#39;s returning

84
00:04:50,080 --> 00:04:53,509
the actual day name of the week. 

85
00:04:53,509 --> 00:04:58,449
Yep, again the date name, we can return numbers or names for

86
00:04:58,449 --> 00:04:59,630
any of these functions have

87
00:04:59,630 --> 00:05:03,360
both. So we can, you see that we&#39;ve got a day,

88
00:05:03,360 --> 00:05:07,110
one there as well, we&#39;ve got SellStartDay, so we can 

89
00:05:07,110 --> 00:05:12,190
return values based on what we want in the output

90
00:05:12,190 --> 00:05:17,610
in there. Okay. Right, another useful one, I mentioned already about what happens if we&#39;ve got

91
00:05:17,610 --> 00:05:18,990
an order date

92
00:05:18,990 --> 00:05:22,820
and the current date. Well, here we&#39;re actually looking at the start date,

93
00:05:22,820 --> 00:05:23,150
but

94
00:05:23,150 --> 00:05:26,889
we might often want to compare two values.

95
00:05:26,889 --> 00:05:30,630
And if comparing dates DATEDIFF is a very useful one because we don&#39;t 

96
00:05:30,630 --> 00:05:33,740
just wanted to compare as a number.

97
00:05:33,740 --> 00:05:37,130
Well actually you can convert dates into numbers, they are

98
00:05:37,130 --> 00:05:43,070
held as numbers actually. But here I want to make sure, now I want to have a look

99
00:05:43,070 --> 00:05:44,180
at how many

100
00:05:44,180 --> 00:05:48,729
months, years, days are in between two values. So what&#39;s the time interval?

101
00:05:48,729 --> 00:05:52,270
So we can be a bit more specific and it has built-in

102
00:05:52,270 --> 00:05:55,690
date functions to allow you to deal with that

103
00:05:55,690 --> 00:06:00,080
specific type of data which behaves differently to anything else. I mean

104
00:06:00,080 --> 00:06:05,820
what else would have 365 of one unit, and 

105
00:06:05,820 --> 00:06:09,620
that unit is also split up into 12, but that doesn&#39;t quite fit into that one. And not 

106
00:06:09,620 --> 00:06:14,169
mention leap years and all of that. &lt;laughter&gt; So we have the specific functionality to

107
00:06:14,169 --> 00:06:14,979
deal with

108
00:06:14,979 --> 00:06:18,420
dates because of that complexity.

109
00:06:18,420 --> 00:06:22,610
So here, we&#39;re looking at the difference in years between the start date and the current date.

110
00:06:22,610 --> 00:06:26,880
So we can see how long we&#39;ve been selling these products for.

111
00:06:26,880 --> 00:06:30,920
We could see actually from the previous one, so this one we&#39;re saying it&#39;s 13 years, 10

112
00:06:30,920 --> 00:06:31,650
years

113
00:06:31,650 --> 00:06:34,720
from today&#39;s day, on the day we are actually running this demo.

114
00:06:34,720 --> 00:06:37,830
Absolute, yeah. And that&#39;s when we&#39;re saying deterministic/non-deterministic, well

115
00:06:37,830 --> 00:06:40,410
that will change. In a year&#39;s time those numbers will be one higher.

116
00:06:40,410 --> 00:06:44,810
I guess, I mean it&#39;s probably worth interjecting at this point and saying date

117
00:06:44,810 --> 00:06:47,130
functions, there&#39;s a huge array of

118
00:06:47,130 --> 00:06:51,380
very, very useful date functions. And you could easily spend a long time playing

119
00:06:51,380 --> 00:06:53,660
with the documentation and looking at what they all do.

120
00:06:53,660 --> 00:06:56,950
Absolutely, because if you didn&#39;t have date functions it&#39;s really

121
00:06:56,950 --> 00:06:57,770
complicated.

122
00:06:57,770 --> 00:07:02,190
Yes. As we said they don&#39;t, things don&#39;t, day&#39;s don&#39;t fit neatly into years, months

123
00:07:02,190 --> 00:07:05,290
don&#39;t fit neatly into years, weeks don&#39;t fit neatly into years, 

124
00:07:05,290 --> 00:07:10,460
so we have specific function because of that. A very simple one here dealing with a bit of text so

125
00:07:10,460 --> 00:07:11,440
we would like

126
00:07:11,440 --> 00:07:14,820
to convert the name in here 

127
00:07:14,820 --> 00:07:18,670
to uppercase. I&#39;m going to give you 

128
00:07:18,670 --> 00:07:22,180
a good example of where I might want to do that. Some people might think

129
00:07:22,180 --> 00:07:23,410
well is that just purely for

130
00:07:23,410 --> 00:07:27,700
you know for the purposes of displaying it neatly in uppercase or whatever.

131
00:07:27,700 --> 00:07:30,970
But actually we mentioned previously in this course,

132
00:07:30,970 --> 00:07:34,200
sometimes a database may have a collation where

133
00:07:34,200 --> 00:07:37,240
the data itself, the values, are case- sensitive.

134
00:07:37,240 --> 00:07:40,680
And we can make a database turn  case sensitive. 

135
00:07:40,680 --> 00:07:45,000
So it&#39;s quite common to have queries where you&#39;re comparing the

136
00:07:45,000 --> 00:07:46,260
value, you know where name

137
00:07:46,260 --> 00:07:51,410
equals, and to use an UPPER and then compare it. Just in case, it&#39;s a measure of case, because

138
00:07:51,410 --> 00:07:52,060
you won&#39;t

139
00:07:52,060 --> 00:07:56,720
get an equal equals true if it&#39;s a case-sensitive database and the case

140
00:07:56,720 --> 00:07:57,850
isn&#39;t exactly the same.

141
00:07:57,850 --> 00:08:02,070
So upper and the equivalent lower are quite commonly used just to make sure

142
00:08:02,070 --> 00:08:05,000
we&#39;re all starting from a level playing field when we&#39;re comparing strings.

143
00:08:05,000 --> 00:08:08,540
And you get some things which are always uppercase.

144
00:08:08,540 --> 00:08:12,390
In the UK our zip code, our postal codes are 

145
00:08:12,390 --> 00:08:16,160
always uppercase. So it would look strange to see them in any other way and you

146
00:08:16,160 --> 00:08:16,480
want to convert them.

147
00:08:16,480 --> 00:08:20,060
We have a slightly more

148
00:08:20,060 --> 00:08:23,940
elegant way of concatenating here. So we could do first name plus last name

149
00:08:23,940 --> 00:08:24,610
is full name 

150
00:08:24,610 --> 00:08:28,290
which we&#39;ve done already. This one is using the CONCAT function to get the same

151
00:08:28,290 --> 00:08:30,080
results.

152
00:08:30,080 --> 00:08:33,760
And we have exactly the same result as before. But just maybe looking a little bit more elegant 

153
00:08:33,760 --> 00:08:35,500
and using a function to do that.

154
00:08:35,500 --> 00:08:38,760
And you could have, I mean you could nest that CONCAT, we&#39;re actually using 

155
00:08:38,760 --> 00:08:39,800
both techniques there

156
00:08:39,799 --> 00:08:43,430
because you&#39;ve got first name plus the space. You could CONCAT 

157
00:08:43,429 --> 00:08:47,589
the CONCAT and get it that way. I think I might lose the elegance.

158
00:08:47,589 --> 00:08:50,850
It might, yes.

159
00:08:50,850 --> 00:08:54,959
and now again so interesting was no for some very simple

160
00:08:54,959 --> 00:08:58,279
so we&#39;re going to stay as we want to return the left

161
00:08:58,279 --> 00:09:01,440
two digits up the product number from the product able

162
00:09:01,440 --> 00:09:06,100
and us very straightforward thing today there we go

163
00:09:06,100 --> 00:09:09,360
FRFR HL days the first three digits

164
00:09:09,360 --> 00:09:14,220
from that table pretty Kachemak you specifically what a character data yeah

165
00:09:14,220 --> 00:09:19,880
characters and say that should we do have a right one as well we can say okay

166
00:09:19,880 --> 00:09:20,839
on the left to the right

167
00:09:20,839 --> 00:09:25,139
so in this situation got product numbers are actually made up of

168
00:09:25,139 --> 00:09:28,959
a number of different components we&#39;ve got the beginning but product I

169
00:09:28,959 --> 00:09:33,079
first two characters the end we&#39;ve got the site such as:

170
00:09:33,079 --> 00:09:37,399
the last 2 characters that 58 medium/large we&#39;ve got

171
00:09:37,399 --> 00:09:41,260
bits information which are actually stored in the product number and that&#39;s

172
00:09:41,260 --> 00:09:43,440
quite typical actually might want to strip

173
00:09:43,440 --> 00:09:46,760
out there with Zach I&#39;m interested in particular parts that

174
00:09:46,760 --> 00:09:51,800
information now it can get quite complicated to find that information

175
00:09:51,800 --> 00:09:52,940
you&#39;re interested

176
00:09:52,940 --> 00:09:56,279
if it&#39;s the left to characters its

177
00:09:56,279 --> 00:10:01,550
not a problem at all in got what he becomes a bit more complex what we have

178
00:10:01,550 --> 00:10:02,579
to search

179
00:10:02,579 --> 00:10:06,709
for values within the string and when we find a valiant

180
00:10:06,709 --> 00:10:09,730
then we can start retrieving information so

181
00:10:09,730 --> 00:10:13,920
here we&#39;re going to start with just the first two digits

182
00:10:13,920 --> 00:10:18,269
the product number as you did before but we&#39;re also going to search

183
00:10:18,269 --> 00:10:21,620
for some more information I weekend his car index to find

184
00:10:21,620 --> 00:10:25,839
dashes within their winning going to move across one

185
00:10:25,839 --> 00:10:30,540
so we&#39;re going to add 12 thats and use some string to retrieve

186
00:10:30,540 --> 00:10:34,260
not the left or the right but a portion 00

187
00:10:34,260 --> 00:10:37,800
text from within that string in this case you up four digits

188
00:10:37,800 --> 00:10:41,009
from within his we&#39;re going to look for the dash

189
00:10:41,009 --> 00:10:43,779
which is good for the first Test in this example hey we&#39;ve got

190
00:10:43,779 --> 00:10:47,000
fr-1 find that dash then retrieve the next

191
00:10:47,000 --> 00:10:52,019
to the starting order the substring function the starting point is

192
00:10:52,019 --> 00:10:55,170
wherever the first dashes plus what&#39;s one right

193
00:10:55,170 --> 00:10:59,079
to elect a dash five plus one and then returns for the day so that would be our

194
00:10:59,079 --> 00:11:04,089
ninety be for for this one that&#39;s the model code so that&#39;s okay that&#39;s where

195
00:11:04,089 --> 00:11:05,690
it&#39;s hidden within that product number:

196
00:11:05,690 --> 00:11:11,190
then we&#39;ve got a.m. a more even more complicated one now looking for

197
00:11:11,190 --> 00:11:14,949
the right hand select a size code no known how size

198
00:11:14,949 --> 00:11:18,670
in here so I&#39;m looking for the right hand dash

199
00:11:18,670 --> 00:11:23,259
in most scenarios so in the first one yet the right-hand dash in anything to

200
00:11:23,259 --> 00:11:24,709
the right that is the size

201
00:11:24,709 --> 00:11:28,870
however once we go down here we find actually there is no

202
00:11:28,870 --> 00:11:31,959
size the prom if I look to the right hand dash

203
00:11:31,959 --> 00:11:35,040
is also that Ash there are there is only

204
00:11:35,040 --> 00:11:39,110
the one that so I&#39;d end up finding the you 589 information which is not correct

205
00:11:39,110 --> 00:11:43,029
so it what we need to do is search

206
00:11:43,029 --> 00:11:48,000
for the right three characters

207
00:11:48,000 --> 00:11:51,459
we know that I&#39;ll if there is that that on the right hand side

208
00:11:51,459 --> 00:11:54,769
it&#39;s gonna be a maximum of three characters from the end well you and I

209
00:11:54,769 --> 00:11:56,800
know that that we have a four digit

210
00:11:56,800 --> 00:12:00,930
carried in the middle which is the product and code so I know if it&#39;s

211
00:12:00,930 --> 00:12:05,639
if it&#39;s more than three across then it&#39;s not the national interest in if it&#39;s

212
00:12:05,639 --> 00:12:06,779
less than that but it&#39;s

213
00:12:06,779 --> 00:12:10,209
right one of two so we&#39;re going to only look at the

214
00:12:10,209 --> 00:12:13,540
right now we with doing everything backwards so you have to think about

215
00:12:13,540 --> 00:12:14,560
things in river

216
00:12:14,560 --> 00:12:20,329
say we&#39;re going to reverse that rather than it being in the right order we&#39;re

217
00:12:20,329 --> 00:12:23,029
going from right to left mount so its rights being reversed

218
00:12:23,029 --> 00:12:27,430
so now we&#39;re looking for the car and except that daschle gain

219
00:12:27,430 --> 00:12:31,819
finding that&#39;s in this time where we&#39;re going to

220
00:12:31,819 --> 00:12:36,069
add see to that return too so essentially

221
00:12:36,069 --> 00:12:39,160
it&#39;s going to be looking at three reversing everything

222
00:12:39,160 --> 00:12:42,720
adding to that takes across then returning to that %uh 58

223
00:12:42,720 --> 00:12:46,740
from here so we going to be no caps

224
00:12:46,740 --> 00:12:51,190
what she knows rates looking at how length as voting link to product

225
00:12:51,190 --> 00:12:56,170
minus that by doing so we found that because they&#39;d be a flipped around two

226
00:12:56,170 --> 00:12:59,910
backwards and yet she saying I K which ones to be one all we want

227
00:12:59,910 --> 00:13:04,140
the substring if the link for the whole thing minus am an ear of their

228
00:13:04,140 --> 00:13:08,860
and weird were returning just two characters of that we should do

229
00:13:08,860 --> 00:13:13,730
is in this situation what we don&#39;t want is anything to be returned in his head

230
00:13:13,730 --> 00:13:18,560
its that&#39;s the case when I think when you do this the first time you do it you

231
00:13:18,560 --> 00:13:19,950
got it wrong because you go

232
00:13:19,950 --> 00:13:25,050
yet I call 858 I was fine but also got pay you $5 come out this

233
00:13:25,050 --> 00:13:28,320
because it&#39;s ezer yes course some

234
00:13:28,320 --> 00:13:32,030
come with outsized so many have to change in large cage type and thinking

235
00:13:32,030 --> 00:13:35,700
from left over from right works well so sometimes you&#39;re going from the left

236
00:13:35,700 --> 00:13:38,570
some time you came from the right and you go think I which way around you&#39;re

237
00:13:38,570 --> 00:13:39,180
thinking

238
00:13:39,180 --> 00:13:43,850
and its editor to price St you yeah rather it be okay with that sweet found

239
00:13:43,850 --> 00:13:47,470
want it for me as well as each of these because we&#39;ve got lots have bested

240
00:13:47,470 --> 00:13:50,860
functioned each one you can you could do a select team in the just select that

241
00:13:50,860 --> 00:13:51,680
bit

242
00:13:51,680 --> 00:13:55,560
yeah they don&#39;t ever as you&#39;re working all day so you can see what the right

243
00:13:55,560 --> 00:13:56,270
hand

244
00:13:56,270 --> 00:13:59,660
to go just that you can see what they&#39;re there for the thing looks like

245
00:13:59,660 --> 00:14:03,630
and something is going through the the process have independently run each of

246
00:14:03,630 --> 00:14:04,430
those sup

247
00:14:04,430 --> 00:14:08,330
um functions help you figure I why do I put all these together if

248
00:14:08,330 --> 00:14:13,130
if the thing reversed looks like that then i&#39;m looking for. like characters as

249
00:14:13,130 --> 00:14:13,660
the right

250
00:14:13,660 --> 00:14:17,260
yeah it take takes a little while to to working with you all the permutations

251
00:14:17,260 --> 00:14:20,950
I don&#39;t think with many people who could write

252
00:14:20,950 --> 00:14:24,410
days so string search queries stray off

253
00:14:24,410 --> 00:14:27,980
no had to take a lot of those working ahead

254
00:14:27,980 --> 00:14:32,070
say you can see on that right hand code they respond to that

255
00:14:32,070 --> 00:14:35,420
the and scenario we&#39;ve got you

256
00:14:35,420 --> 00:14:39,110
Hout 599 dash are is returned the always

257
00:14:39,110 --> 00:14:44,260
a chill touch you 589 which didn&#39;t have anything out with because

258
00:14:44,260 --> 00:14:49,060
that one has no you dash in those last four characters

259
00:14:49,060 --> 00:14:52,290
we don&#39;t want to return it so that was not finding you

260
00:14:52,290 --> 00:14:55,560
anybody&#39;s within this is not find any deductions win

261
00:14:55,560 --> 00:14:58,660
last 3 characters so we we elements in our size guide

262
00:14:58,660 --> 00:15:01,980
and that sucks the correct we we now know what the product type: is

263
00:15:01,980 --> 00:15:05,400
we know the model number is you know the size kate is sweet broken-down

264
00:15:05,400 --> 00:15:10,029
out product number which was a a number which were coming historically or from

265
00:15:10,029 --> 00:15:11,500
somewhere else and we touch the

266
00:15:11,500 --> 00:15:15,640
stripped out individual parts information products

267
00:15:15,640 --> 00:15:19,210
and I guess was important from this as well is just the importance of harming

268
00:15:19,210 --> 00:15:22,620
test data that is representative of the real deal with you lately to have

269
00:15:22,620 --> 00:15:26,710
so you can test these permutations I think the key thing about including the

270
00:15:26,710 --> 00:15:27,270
demo

271
00:15:27,270 --> 00:15:30,779
and this course is not that we expect you from this to know

272
00:15:30,779 --> 00:15:34,529
immediately be able to pay I could sit down I can write this this creative I&#39;ve

273
00:15:34,529 --> 00:15:38,350
learned how to do that but to come to show you how complex these things can

274
00:15:38,350 --> 00:15:42,260
become when you next one function with another function with another function

275
00:15:42,260 --> 00:15:46,110
especially the string handling functions it has to be said can be

276
00:15:46,110 --> 00:15:50,100
quite complex to a knife to work I is really about what you said is to is to

277
00:15:50,100 --> 00:15:51,100
start small

278
00:15:51,100 --> 00:15:55,310
because I think we&#39;re not showing students they saw things he ate you

279
00:15:55,310 --> 00:15:59,830
they just thing I cannot create that ya but could you create

280
00:15:59,830 --> 00:16:04,040
right product number: comments 3 can you find me the last 3 characters yet

281
00:16:04,040 --> 00:16:08,940
yeah I know you find was the co two&#39;s yeah will maybe even in turn that into a

282
00:16:08,940 --> 00:16:12,820
number you know although it did yes you know what that is okay just talkin

283
00:16:12,820 --> 00:16:16,670
weather could be fined the reverse is that type the code and then and then

284
00:16:16,670 --> 00:16:19,120
your place that I think I with the function to generate

285
00:16:19,120 --> 00:16:22,900
talked in code and you can build a build-up it up and ultimately fail well

286
00:16:22,900 --> 00:16:24,400
have created this thing with his

287
00:16:24,400 --> 00:16:28,460
really complex and but but breaking them so Eunice

288
00:16:28,460 --> 00:16:30,339
Monday in that way Rep

