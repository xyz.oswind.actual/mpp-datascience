0
00:00:02,460 --> 00:00:07,759
So now we&#39;re going to have a look at window functions. So these are applied to a set of

1
00:00:07,759 --> 00:00:10,790
rows, a set of data. So the window it refers to

2
00:00:10,790 --> 00:00:15,580
is a set. Nothing to do with Windows the product, this is purely

3
00:00:15,580 --> 00:00:18,860
a window of rows. It&#39;s not even anything to do with the thing you look out of

4
00:00:18,860 --> 00:00:23,560
made of glass, right. Ok, good. The other arcane use of the word. &amp;lt;laughter&amp;gt;That&#39;s right, yes.

5
00:00:23,560 --> 00:00:27,070
So here we&#39;re looking at

6
00:00:27,070 --> 00:00:31,730
ranking our data. So I would like to have

7
00:00:31,730 --> 00:00:35,760
a ranking by price. I want to know what most expensive product is, second, third

8
00:00:35,760 --> 00:00:38,899
ranked products. So we&#39;re going to use a

9
00:00:38,899 --> 00:00:45,429
RANK and it&#39;s OVER and then a set of data. So that ORDER BY list price descending, I

10
00:00:45,429 --> 00:00:46,219
want to get

11
00:00:46,219 --> 00:00:50,050
all of the list prices descending, and then I want to see

12
00:00:50,050 --> 00:00:53,129
a ranking of those 1, 2, 3 as it goes through.

13
00:00:53,129 --> 00:00:56,730
So the RANK function then RANK

14
00:00:56,730 --> 00:01:02,699
OVER is returning the ordinal number that indicates where abouts in

15
00:01:02,699 --> 00:01:06,310
the hierarchy of values this thing falls. Absolutely.

16
00:01:06,310 --> 00:01:10,649
And if you get draws, you&#39;ll have it in the same way you get draws

17
00:01:10,649 --> 00:01:16,729
in some sort of sport. You&#39;ll 1, 1 and then the next one, if you get the two in a draw at the top and a 1 and a 1.

18
00:01:16,729 --> 00:01:20,310
The next one is not a 2, the next one is  3 because it&#39;s the third down. 

19
00:01:20,310 --> 00:01:23,689
So you&#39;ll get that draws get the same number but

20
00:01:23,689 --> 00:01:30,040
then the next number is how far down you are. Right.

21
00:01:30,040 --> 00:01:32,729
Alright, I&#39;m going to move down now to have a look at

22
00:01:32,729 --> 00:01:36,020
some of those window functions. So to start off with

23
00:01:36,020 --> 00:01:40,070
the TOP 100 products. We&#39;ve got an ORDER BY 

24
00:01:40,070 --> 00:01:43,789
in here RANK by price. Now RANK by price that&#39;s actually going to be set up here.

25
00:01:43,789 --> 00:01:47,149
So there&#39;s two things going in, we&#39;ve got the RANK and then

26
00:01:47,149 --> 00:01:51,140
OVER and it&#39;s over the ORDER BY list price descending. So

27
00:01:51,140 --> 00:01:54,710
I&#39;m not ranking it by what we ultimately rank

28
00:01:54,710 --> 00:01:59,399
the query by. So we ultimately rank the query by, or order the query rather, by RANK by price.

29
00:01:59,399 --> 00:02:02,490
So I&#39;m going to say now in list price order  descending,

30
00:02:02,490 --> 00:02:06,920
I would like to have that ranked by price value so the

31
00:02:06,920 --> 00:02:09,970
rank in there will give me a number based

32
00:02:09,970 --> 00:02:14,090
on their list price. So over that whole window of 100 records.

33
00:02:14,090 --> 00:02:19,049
Now you&#39;ll see if we get equal to, they&#39;re all ranked 1. So the first

34
00:02:19,049 --> 00:02:23,200
five in here have all got the same number. They&#39;re all ranked 1. And then the next

35
00:02:23,200 --> 00:02:24,720
one is a 6 because

36
00:02:24,720 --> 00:02:27,769
you&#39;ve already got 5 ones preceding it. So it&#39;s 

37
00:02:27,769 --> 00:02:31,220
worth, I guess the way to think about this is

38
00:02:31,220 --> 00:02:35,170
3,578 dollars

39
00:02:35,170 --> 00:02:39,530
is the price of those first five

40
00:02:39,530 --> 00:02:42,540
rows that we&#39;ve got there. So what we&#39;re not saying is

41
00:02:42,540 --> 00:02:47,260
the Road 150 Red 62 is the most expensive product we have,

42
00:02:47,260 --> 00:02:50,380
were saying that the price Road 150 Red 62

43
00:02:50,380 --> 00:02:53,609
is the highest price that we have.

44
00:02:53,609 --> 00:02:58,319
The highest list price that we have, yes. Yep. So you&#39;re actually ranking

45
00:02:58,319 --> 00:02:59,209
the prices.

46
00:02:59,209 --> 00:03:02,709
Yes, and it&#39;s just giving us a number. It&#39;s not,

47
00:03:02,709 --> 00:03:05,370
you know we could have one thing in there which is a thousand times more

48
00:03:05,370 --> 00:03:07,100
expensive than the other, it would still be

49
00:03:07,100 --> 00:03:10,269
ranking 1 and the next one would be 2. Yeah. So it&#39;s not -- 

50
00:03:10,269 --> 00:03:14,280
and the number is just jumping up from 1 to 6 because we have five,

51
00:03:14,280 --> 00:03:18,470
they&#39;re the same product it&#39;s just they&#39;re different sizes. 

52
00:03:18,470 --> 00:03:21,829
So yeah, again you could argue maybe that

53
00:03:21,829 --> 00:03:25,980
in the future maybe we&#39;d actually  strip those out and have a products table that

54
00:03:25,980 --> 00:03:28,810
actually just had that as a product and somewhere separately had 

55
00:03:28,810 --> 00:03:30,639
details, the color and so on.

56
00:03:30,639 --> 00:03:35,160
Yeah, but you can what see it&#39;s doing in there. It&#39;s a way of ranking data. Because

57
00:03:35,160 --> 00:03:38,280
the problem with working with sets in SQL is that it&#39;s

58
00:03:38,280 --> 00:03:40,890
not something that runs down

59
00:03:40,890 --> 00:03:44,350
from row 1, to row 2, to row 3, to row 4, we don&#39;t have numberings

60
00:03:44,350 --> 00:03:48,010
in that way. We don&#39;t have row numbers or anything like that. It&#39;s a way

61
00:03:48,010 --> 00:03:51,810
of saying well I&#39;d like to have a number which goes down from the

62
00:03:51,810 --> 00:03:54,209
top to the bottom telling me what number they are in there.

63
00:03:54,209 --> 00:03:57,240
And because of the way it works,

64
00:03:57,240 --> 00:04:00,490
and the data is not ordered until we do an ORDER BY,

65
00:04:00,490 --> 00:04:03,620
we need to add that in afterwards.

66
00:04:03,620 --> 00:04:06,760
So now a little bit more than that. We want to actually,

67
00:04:06,760 --> 00:04:11,510
we&#39;re going to do, rather than the simple RANK OVER ORDER BY list price, we&#39;re going 

68
00:04:11,510 --> 00:04:12,470
to say

69
00:04:12,470 --> 00:04:16,579
add in a PARTITION BY and here

70
00:04:16,579 --> 00:04:19,640
c, c.name, c refers to

71
00:04:19,640 --> 00:04:22,740
the product category. So essentially we&#39;re going to say

72
00:04:22,740 --> 00:04:26,220
if you like GROUP BY product category, so for

73
00:04:26,220 --> 00:04:29,380
each product category, give me the ranking. So

74
00:04:29,380 --> 00:04:33,270
adding an extra level within that

75
00:04:33,270 --> 00:04:36,340
using this PETITION BY in here.

76
00:04:36,340 --> 00:04:39,540
So now what we&#39;ll see is

77
00:04:39,540 --> 00:04:43,710
that the first three that&#39;s fine, they&#39;re all number 1. They&#39;re 89.99, 

78
00:04:43,710 --> 00:04:47,340
well, that makes sense. But the next one we&#39;ve got is 120. And you think, we&#39;ll why

79
00:04:47,340 --> 00:04:48,479
isn&#39;t that above those ones?

80
00:04:48,479 --> 00:04:52,450
Well it&#39;s a different product category, the first three being 1 those are all the 

81
00:04:52,450 --> 00:04:56,520
the number 1 bib shorts. The next one

82
00:04:56,520 --> 00:05:00,450
is the number 1 bike rack, the next one is the number 1 bike stand,

83
00:05:00,450 --> 00:05:05,350
and then we&#39;ll go on to the number 1  bottle and cage, the number 2 bottle and cage,

84
00:05:05,350 --> 00:05:07,620
the number 3 bottle and cage. So we&#39;re going through each of

85
00:05:07,620 --> 00:05:11,580
the product categories and ranking them. And the point is, the reason that

86
00:05:11,580 --> 00:05:12,919
we&#39;ve got a number,

87
00:05:12,919 --> 00:05:16,620
well okay to break this down, the reason all the bib shorts are number 1 is

88
00:05:16,620 --> 00:05:17,950
because they&#39;re the same price,

89
00:05:17,950 --> 00:05:21,160
and there are no other types of bib shorts. So all of the bib shorts are

90
00:05:21,160 --> 00:05:22,900
same price, so they&#39;re all equal 1st.

91
00:05:22,900 --> 00:05:27,830
Yes. The reason we&#39;ve only got one bike rack is because we only actually sell

92
00:05:27,830 --> 00:05:28,270
one

93
00:05:28,270 --> 00:05:31,419
bike rack. We have

94
00:05:31,419 --> 00:05:34,720
an ORDER BY category, so yes we only have one bike rack. And then in that

95
00:05:34,720 --> 00:05:38,039
category, there&#39;s only one product, so obviously it&#39;s the most

96
00:05:38,039 --> 00:05:39,070
expensive one.

97
00:05:39,070 --> 00:05:42,430
Yeah. Where it gets more interesting, and the same with bike stands actually, but where 

98
00:05:42,430 --> 00:05:44,060
we get to bottles and cages,

99
00:05:44,060 --> 00:05:47,600
we have multiple bottles and cages that are different places and that&#39;s where we

100
00:05:47,600 --> 00:05:48,510
start to see

101
00:05:48,510 --> 00:05:50,000
the first, the second, the third.

102
00:05:50,000 --> 00:05:53,200
Yeah, and we&#39;ll talk about GROUP BY later on, but 

103
00:05:53,200 --> 00:05:56,270
GROUP BY is something that wouldn&#39;t do this because it&#39;s not going to look at

104
00:05:56,270 --> 00:06:00,740
where you sit in the queue. It&#39;s going to add up, it would give me a sum, or it would give me an

105
00:06:00,740 --> 00:06:03,170
average, or give me a count, but it won&#39;t

106
00:06:03,170 --> 00:06:05,990
say well, where&#39;s each of those individuals in the list.

