0
00:00:02,320 --> 00:00:07,339
So now we&#39;re going to have a look at logical functions. Now we&#39;re doing

1
00:00:07,339 --> 00:00:11,780
tests on our data in here and it&#39;s comparative logic. So we&#39;re saying

2
00:00:11,780 --> 00:00:15,049
true equals true. 

3
00:00:15,049 --> 00:00:19,210
We can test for things being the case. So the first one is numeric. So we want to

4
00:00:19,210 --> 00:00:19,840
know, 

5
00:00:19,840 --> 00:00:24,060
is this a number? So 101.99

6
00:00:24,060 --> 00:00:27,779
is it numeric? And basically that&#39;s just running it against

7
00:00:27,779 --> 00:00:32,430
something that&#39;s been typed in. But we can also run that obviously against

8
00:00:32,430 --> 00:00:35,820
columns in our database as well. So we can test for things being the numeric in 

9
00:00:35,820 --> 00:00:36,550
here.

10
00:00:36,550 --> 00:00:40,550
We&#39;ve got this, this is 

11
00:00:40,550 --> 00:00:44,300
you get in some applications as an IF

12
00:00:44,300 --> 00:00:47,980
in here, but an IIF is done as a function which is 

13
00:00:47,980 --> 00:00:51,079
in row or in here. And what&#39;s it&#39;s doing, 

14
00:00:51,079 --> 00:00:54,859
it&#39;s saying okay let&#39;s have a look at a test. So we&#39;ve got

15
00:00:54,859 --> 00:00:58,500
list price greater than fifty. If it is

16
00:00:58,500 --> 00:01:02,870
display high, if it isn&#39;t display low. So we&#39;ve got a test, what&#39;s if it&#39;s true, what&#39;s if 

17
00:01:02,870 --> 00:01:03,510
it&#39;s false.

18
00:01:03,510 --> 00:01:09,110
So, if list price greater than 50, display high. If list price is not greater than 50,

19
00:01:09,110 --> 00:01:12,310
including it being equal to 50, then display low.

20
00:01:12,310 --> 00:01:16,830
So we can output information using a logical test.

21
00:01:16,830 --> 00:01:20,060
And then we&#39;ve got choose in here. So this one, 

22
00:01:20,060 --> 00:01:23,130
we&#39;ve got multiple different

23
00:01:23,130 --> 00:01:28,530
category ID&#39;s within here. So the ProductCategoryID,

24
00:01:28,530 --> 00:01:32,010
if it&#39;s a 1 display bikes, if it&#39;s 2 display components, 

25
00:01:32,010 --> 00:01:35,740
3 clothing, 4 accessories. So we can turn a code

26
00:01:35,740 --> 00:01:39,140
into words which are more meaningful and

27
00:01:39,140 --> 00:01:42,530
and it does that without using another table that&#39;s being used to convert those

28
00:01:42,530 --> 00:01:43,540
values over.

29
00:01:43,540 --> 00:01:47,340
We could just do it in the function. But it&#39;s the ordinal position, I mean it&#39;s always 

30
00:01:47,340 --> 00:01:48,520
going to start from 1,

31
00:01:48,520 --> 00:01:52,090
2, 3, it&#39;s the ordinal position within that list. 

32
00:01:52,090 --> 00:01:55,660
Yeah. Which begs the question, what happens if the product category ID is

33
00:01:55,660 --> 00:01:59,060
5 in this instance? Because I&#39;ve only got 4 options which will be 1, 2, 3 

34
00:01:59,060 --> 00:01:59,570
and 4.

35
00:01:59,570 --> 00:02:02,650
Then we&#39;re going to get a null value if it&#39;s anything higher than that.

36
00:02:02,650 --> 00:02:05,010
This is not a

37
00:02:05,010 --> 00:02:08,000
solution to say, well I don&#39;t need to create

38
00:02:08,000 --> 00:02:11,780
categories table anymore. There are many advantages to having it as a table

39
00:02:11,780 --> 00:02:14,020
because what if one of those rows change or 

40
00:02:14,020 --> 00:02:17,600
anything happens. We can deal with it much more easily with a table.

41
00:02:17,600 --> 00:02:20,800
But it is a nice way of saying, okay well I just want to

42
00:02:20,800 --> 00:02:25,650
now do this. We always use it as codes, but actually someone&#39;s asked me to output these values

43
00:02:25,650 --> 00:02:25,980
instead of

44
00:02:25,980 --> 00:02:28,990
codes, and we can do that, we can do a conversion.

45
00:02:28,990 --> 00:02:33,230
So it just, it&#39;s rather than starting to say well couldn&#39;t you do that 

46
00:02:33,230 --> 00:02:33,970
with the

47
00:02:33,970 --> 00:02:38,520
IIF and then next lots of IIFs inside each other?

48
00:02:38,520 --> 00:02:43,120
Which you could but I mean I guess where I have seen it used is

49
00:02:43,120 --> 00:02:47,580
if you have something that&#39;s perhaps performed in a series of phases. So the

50
00:02:47,580 --> 00:02:49,660
status of an order might be 1 -

51
00:02:49,660 --> 00:02:53,830
it&#39;s been placed; 2 - it&#39;s been paid for; 3 - it&#39;s been delivered.

52
00:02:53,830 --> 00:02:57,500
And we know that it&#39;s always that  sequence of steps, 1, 2 and 3.

53
00:02:57,500 --> 00:03:01,830
Then it becomes very easy to assign some sort of label to what those things

54
00:03:01,830 --> 00:03:04,940
all. And we&#39;d never have any other values, we&#39;d never worry about the nulls, 

55
00:03:04,940 --> 00:03:09,959
then it would work fine. And doing that  nested IIF

56
00:03:09,959 --> 00:03:13,230
would be really complicated. When you start looking at it and you have

57
00:03:13,230 --> 00:03:17,340
nesting within nesting within nesting and you think about the logic it becomes complex.

58
00:03:17,340 --> 00:03:20,730
And earlier on in the course, we saw a case statement which is another way of doing

59
00:03:20,730 --> 00:03:23,970
this type of thing. It&#39;s not a function per se, but it&#39;s another way of

60
00:03:23,970 --> 00:03:27,480
comparing values and giving a result back out. So there&#39;s

61
00:03:27,480 --> 00:03:30,989
sometimes multiple ways of doing the same thing, and that the best one is

62
00:03:30,989 --> 00:03:31,630
going to depend

63
00:03:31,630 --> 00:03:34,650
specifically on the data that you&#39;re working with I guess. Yes, absolutely.

64
00:03:34,650 --> 00:03:38,700
So looking at some examples of

65
00:03:38,700 --> 00:03:42,310
logical functions. We&#39;re now going to

66
00:03:42,310 --> 00:03:46,160
have a filter through the Product table.

67
00:03:46,160 --> 00:03:52,180
Now the Product table has a mixture of different sizes in it, so we&#39;ve got 58

68
00:03:52,180 --> 00:03:53,590
in here, we&#39;ve also got M

69
00:03:53,590 --> 00:03:58,000
for medium, L for large in here. So what we&#39;re going to do is

70
00:03:58,000 --> 00:04:01,660
use the ISNUMERIC function

71
00:04:01,660 --> 00:04:06,160
in here because I don&#39;t want to 

72
00:04:06,160 --> 00:04:09,880
get in an error all the time. I just want to find the numeric

73
00:04:09,880 --> 00:04:13,540
sizes. I have no interest in small, medium, and large. I&#39;m interested in all these sizes of bikes in

74
00:04:13,540 --> 00:04:15,400
here. So I only want to find these numeric ones.

75
00:04:15,400 --> 00:04:17,920
So ISNUMERIC we&#39;re using in there to find things that 

76
00:04:17,920 --> 00:04:21,990
are numeric although obviously stored

77
00:04:21,990 --> 00:04:25,900
within probably a varchar I imagine,

78
00:04:25,900 --> 00:04:30,040
within there. But it is numeric data, so it&#39;s finding the numeric data within a

79
00:04:30,040 --> 00:04:34,220
text string data field. So in this context, the 1

80
00:04:34,220 --> 00:04:39,300
doesn&#39;t mean the number one it actually means true. Right. So we could find true/false 

81
00:04:39,300 --> 00:04:44,630
within there, so we could do a search for a 1 or a 0. Now the next one

82
00:04:44,630 --> 00:04:49,490
we&#39;re going to say if we have a product category of 5, 6 or 7, it&#39;s a bike.

83
00:04:49,490 --> 00:04:54,120
And if it&#39;s not, it&#39;s something else. So all  we want to do is display bike or other

84
00:04:54,120 --> 00:04:57,250
from there. So we&#39;ve got an IIF ProductCategoryID.

85
00:04:57,250 --> 00:05:00,890
So that&#39;s what we&#39;re looking at and testing it to say

86
00:05:00,890 --> 00:05:04,660
is it in this list numbers 5, 6, or 7? True

87
00:05:04,660 --> 00:05:08,070
is bike, false is other. So then we&#39;re just going to get

88
00:05:08,070 --> 00:05:12,900
a lot of products and whether they&#39;re in bike

89
00:05:12,900 --> 00:05:15,980
or not. And there will be some in bike as well down here.

90
00:05:15,980 --> 00:05:22,980
There you go. Yep.

91
00:05:23,870 --> 00:05:28,800
So now combining the two, we can use ISNUMERIC which we&#39;ve already seen.

92
00:05:28,800 --> 00:05:32,130
If ISNUMERIC size is 1

93
00:05:32,130 --> 00:05:35,670
then it returns a true, it is 1.

94
00:05:35,670 --> 00:05:39,240
So that would display numeric, if it&#39;s not it would display non-numeric. So we can

95
00:05:39,240 --> 00:05:43,350
display whether our sizes are numeric or not.

96
00:05:43,350 --> 00:05:47,210
So just expanding and combining the two previous ones.

97
00:05:47,210 --> 00:05:51,870
So we&#39;ve got an IIF and we&#39;ve got an ISNUMERIC, combining the two we can have a look and see what size type

98
00:05:51,870 --> 00:05:52,770
we&#39;ve got within there.

99
00:05:52,770 --> 00:05:56,130
And then using CHOOSE.

100
00:05:56,130 --> 00:06:00,810
So we&#39;ve got bikes, components, clothing, accessories is our first 4

101
00:06:00,810 --> 00:06:04,450
product categories. We&#39;re going to choose the category ID

102
00:06:04,450 --> 00:06:08,280
display bikes, components, clothing, or accessories as the product type

103
00:06:08,280 --> 00:06:12,120
from there. We&#39;ve a join on here as well because the data is coming from two tables.

104
00:06:12,120 --> 00:06:15,850
And you can see now we&#39;ve got bikes,

105
00:06:15,850 --> 00:06:20,700
we&#39;ve got components, clothing, accessories in there.

106
00:06:20,700 --> 00:06:24,680
And that&#39;s because we knew ahead of time that those categories were numbered 

107
00:06:24,680 --> 00:06:26,380
1, 2, 3 and 4. Yeah.

108
00:06:26,380 --> 00:06:31,080
So that&#39;s where long-term, if you&#39;re using this repeatedly and

109
00:06:31,080 --> 00:06:34,610
those things might change around and you&#39;re thinking well maybe we need another table at some

110
00:06:34,610 --> 00:06:34,970
point

111
00:06:34,970 --> 00:06:38,950
speak to the database designers and say could we have something actually listing that information

112
00:06:38,950 --> 00:06:40,169
and then we&#39;ll do a join.

113
00:06:40,169 --> 00:06:44,280
Because then it will always check the correct values. Right.

