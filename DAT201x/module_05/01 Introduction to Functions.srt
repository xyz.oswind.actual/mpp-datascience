0
00:00:05,350 --> 00:00:07,060
Welcome back once again.

1
00:00:07,060 --> 00:00:11,389
I hope you enjoyed the lab on using UNION. We&#39;re back now for module 5

2
00:00:11,389 --> 00:00:15,200
which is about using functions and aggregating data.

3
00:00:15,200 --> 00:00:18,560
So we&#39;re going to start to do queries  that don&#39;t involve just bringing back

4
00:00:18,560 --> 00:00:20,520
individual rows of data, we&#39;re going start to

5
00:00:20,520 --> 00:00:25,210
group those rows up and create aggregates, and count things and sum things, and all those

6
00:00:25,210 --> 00:00:28,930
great sorts of things. So Jeff, what are we going to cover in this module then?

7
00:00:28,930 --> 00:00:32,300
There&#39;s two broad things we&#39;re going to cover. 

8
00:00:32,299 --> 00:00:36,450
We&#39;ll look at built-in functions and they&#39;re broken down into scalar functions,

9
00:00:36,450 --> 00:00:39,660
aggregate functions, logical functions, window functions.

10
00:00:39,660 --> 00:00:43,100
And then we&#39;ll also look at queries where we can group up

11
00:00:43,100 --> 00:00:46,750
our data. So we&#39;ll look at a grouping clause where we&#39;ll 

12
00:00:46,750 --> 00:00:49,970
say let&#39;s just look at the totals for customer for example.

13
00:00:49,970 --> 00:00:53,579
And HAVING is connected to that as well as a way of filtering the information. Sounds useful.

14
00:00:53,579 --> 00:00:56,890
Ok, so to look into it in a bit more detail. The

15
00:00:56,890 --> 00:01:00,050
built-in function side if things is what we&#39;re looking at. We&#39;ll go into these in more

16
00:01:00,050 --> 00:01:00,720
detail, but 

17
00:01:00,720 --> 00:01:03,940
just briefly we&#39;ll look at scalar functions, we&#39;ll look at 

18
00:01:03,940 --> 00:01:07,100
one row in, one row out, one value out.

19
00:01:07,100 --> 00:01:10,740
Logical functions so we&#39;re just looking at using logic

20
00:01:10,740 --> 00:01:14,170
in there to return one single output.

21
00:01:14,170 --> 00:01:18,470
Aggregate, adding things up and we&#39;ll do that a bit more later on with GROUP BY as well.

22
00:01:18,470 --> 00:01:21,470
Window is set of rows. So we&#39;re looking at 

23
00:01:21,470 --> 00:01:25,400
returning these, it&#39;s working with the sets of data.

24
00:01:25,400 --> 00:01:28,620
And rowset as it says there, 

25
00:01:28,620 --> 00:01:32,200
return a virtual table. Effectively something we can use as a table

26
00:01:32,200 --> 00:01:36,600
in a Transact-SQL statement. So those are the types built-in functions that there are

27
00:01:36,600 --> 00:01:37,159
then.

28
00:01:37,159 --> 00:01:39,830
Just broadly categorized. Yeah, alright great.

