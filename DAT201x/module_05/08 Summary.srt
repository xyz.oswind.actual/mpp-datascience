0
00:00:02,029 --> 00:00:05,100
So we&#39;ve had a look at a number of different things. Two 

1
00:00:05,100 --> 00:00:09,110
sort of core differences. One is general functions which could be scalar, 

2
00:00:09,110 --> 00:00:12,690
dealing with individual values in and out, aggregate functions where we&#39;re 

3
00:00:12,690 --> 00:00:16,960
adding up, averaging, max, min working on a set of data.

4
00:00:16,960 --> 00:00:20,810
Logical functions if it&#39;s this do this, if it&#39;s something else do something else.

5
00:00:20,810 --> 00:00:24,980
Window functions, working with a set of data. And then those aggregate ones

6
00:00:24,980 --> 00:00:26,429
is another sort of

7
00:00:26,429 --> 00:00:30,689
subject, aggregating data. We&#39;re going to aggregate them, and then group up so we can

8
00:00:30,689 --> 00:00:31,849
have subtotals.

9
00:00:31,849 --> 00:00:35,770
And then filter the results using HAVING. Great.

10
00:00:35,770 --> 00:00:39,379
So we&#39;re really getting to the point now where we&#39;re starting to build much

11
00:00:39,379 --> 00:00:41,570
more complex queries than when we first started.

12
00:00:41,570 --> 00:00:45,399
We&#39;re starting to see how to  aggregate our data and use those

13
00:00:45,399 --> 00:00:46,460
functions to get

14
00:00:46,460 --> 00:00:49,809
lots of additional values over and above  what&#39;s already in the tables.

15
00:00:49,809 --> 00:00:54,520
There is again a lab that goes with this module. So we suggest once again that

16
00:00:54,520 --> 00:00:54,780
you

17
00:00:54,780 --> 00:00:58,510
download the lab documents and files for these labs,

18
00:00:58,510 --> 00:01:02,500
you use the Getting Started Guide to get your environment setup and get ready

19
00:01:02,500 --> 00:01:02,930
to go,

20
00:01:02,930 --> 00:01:06,880
and then do the lab that goes with this module on using functions and aggregating 

21
00:01:06,880 --> 00:01:07,420
data

22
00:01:07,420 --> 00:01:10,620
so that you&#39;re comfortable and you get a chance to try this out for yourself.

23
00:01:10,620 --> 00:01:13,840
And then when you&#39;re comfortable with that, you&#39;re ready to come back

24
00:01:13,840 --> 00:01:17,900
and we&#39;ll go on to the next module where, well, it gets exciting in the next module.

25
00:01:17,900 --> 00:01:20,110
In this module, we had functions within functions.

26
00:01:20,110 --> 00:01:22,900
In the next module, come back and join us to see queries within queries.

