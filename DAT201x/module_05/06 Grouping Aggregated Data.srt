0
00:00:02,409 --> 00:00:05,700
We&#39;re going to go on now to look at GROUP BY.

1
00:00:05,700 --> 00:00:10,599
Typically when doing aggregates, you&#39;re most likely to use GROUP BY because at the

2
00:00:10,599 --> 00:00:14,240
moment we&#39;ve only looked at grand totals and that&#39;s all we can display 

3
00:00:14,240 --> 00:00:18,410
without using GROUP BY. Actually what we want to do is add a bit context to this.

4
00:00:18,410 --> 00:00:22,440
So a total for a category for example. So we&#39;re going to add

5
00:00:22,440 --> 00:00:26,289
GROUP BY and we&#39;re going to say group up,

6
00:00:26,289 --> 00:00:30,160
and you have to group up everything that&#39;s in the select row that&#39;s not being

7
00:00:30,160 --> 00:00:31,369
aggregated. So

8
00:00:31,369 --> 00:00:34,629
you can&#39;t have something in there that is not grouped by and not

9
00:00:34,629 --> 00:00:39,260
aggregated, it has to be one or the other when we start to add it.

10
00:00:39,260 --> 00:00:44,469
And what we&#39;re going to do, we&#39;re going to have a look at summarizing for each of

11
00:00:44,469 --> 00:00:47,949
those things that we&#39;re grouping. So if we group by product id, we want for each

12
00:00:47,949 --> 00:00:49,429
product give me the total, 

13
00:00:49,429 --> 00:00:55,119
for each product give me the count. And the detail rows are lost, so we don&#39;t

14
00:00:55,119 --> 00:00:58,829
anymore, we don&#39;t see what goes into it, we don&#39;t see those individual values

15
00:00:58,829 --> 00:01:01,569
anymore. We&#39;ll see a much reduced record set. So we might have

16
00:01:01,569 --> 00:01:04,589
five rows returned from a thousand rows in the detail,

17
00:01:04,589 --> 00:01:08,810
we don&#39;t see the individual detail within there. So this one is saying group up by

18
00:01:08,810 --> 00:01:12,000
CustomerID and show me a count of star there, so how many rows are there

19
00:01:12,000 --> 00:01:17,670
in sales order header for each customer. 

20
00:01:17,670 --> 00:01:19,820
Alright, I&#39;ll just switch over here. Let&#39;s have a look then

21
00:01:19,820 --> 00:01:23,170
at grouping up. Now, so 

22
00:01:23,170 --> 00:01:27,210
we have GROUP BY. So if I were to start

23
00:01:27,210 --> 00:01:33,670
without my GROUP BY, then I get an error.

24
00:01:33,670 --> 00:01:37,240
So we&#39;ve got a column in there that&#39;s not contained in either an aggregate

25
00:01:37,240 --> 00:01:38,710
function or a GROUP BY clause.

26
00:01:38,710 --> 00:01:43,300
You can&#39;t have things that are in neither of those two. If I were to say the

27
00:01:43,300 --> 00:01:47,780
first salesperson or do an  aggregate on it, then it would work. But

28
00:01:47,780 --> 00:01:51,590
as it stands it&#39;s not being, it&#39;s not in either, so we have to do a GROUP BY or

29
00:01:51,590 --> 00:01:54,890
we have to do an aggregate on it.

30
00:01:54,890 --> 00:01:58,770
So let&#39;s add that. Now for each salesperson

31
00:01:58,770 --> 00:02:03,380
we&#39;re looking at the sales revenue. Okay, so we&#39;re doing an ISNULL on there as well.

32
00:02:03,380 --> 00:02:06,980
So it&#39;s going to sum the

33
00:02:06,980 --> 00:02:10,530
total in there, and basically display

34
00:02:10,530 --> 00:02:16,230
the grand total of each of those, sorry not the grand total, the subtotal for each of those,

35
00:02:16,230 --> 00:02:20,520
for each salesperson. And of course because we&#39;ve used the LEFT JOIN we&#39;ve included sales

36
00:02:20,520 --> 00:02:23,709
people who haven&#39;t sold anything. And so 

37
00:02:23,709 --> 00:02:24,680
they&#39;re the ISNULL.

38
00:02:24,680 --> 00:02:28,050
So now what we&#39;re going to do,

39
00:02:28,050 --> 00:02:31,920
again adding in --

40
00:02:31,920 --> 00:02:36,610
now you need to be careful if you said in here, well we&#39;ve got basically the

41
00:02:36,610 --> 00:02:38,110
same thing but we&#39;ve added in 

42
00:02:38,110 --> 00:02:44,180
this concatenated function in here to display the customer.

43
00:02:44,180 --> 00:02:47,950
And this tells me if I get it wrong is when I add another column, and you forget to add it

44
00:02:47,950 --> 00:02:50,480
you add it in the SELECT list, you forget to add it in the GROUP BY.

45
00:02:50,480 --> 00:02:54,270
So you see it&#39;s been added here, but exactly the same thing has been added here. Now you might say

46
00:02:54,270 --> 00:02:55,500
well actually, that&#39;s a function.

47
00:02:55,500 --> 00:02:58,850
And it is a function, but it&#39;s not an aggregate function.

48
00:02:58,850 --> 00:03:02,090
Right. So it&#39;s not just that you&#39;re in a function

49
00:03:02,090 --> 00:03:06,680
and that&#39;s again maybe more confusing, because you think, okay, let&#39;s have a look across here, there&#39;s only one thing that&#39;s not in a 

50
00:03:06,680 --> 00:03:08,090
a function and that&#39;s salesperson.

51
00:03:08,090 --> 00:03:12,030
Well that&#39;s in my GROUP BY. But this in a function,

52
00:03:12,030 --> 00:03:14,960
wrong sort of function. So it has to be in an aggregate 

53
00:03:14,960 --> 00:03:18,310
function. So I mean, the other thing is that we&#39;ve got

54
00:03:18,310 --> 00:03:22,750
concat first name last name, and so on. But we&#39;ve given it an alias,

55
00:03:22,750 --> 00:03:27,080
so can&#39;t I use the alias? So we&#39;re back to 

56
00:03:27,080 --> 00:03:30,480
the order that the things are run in as well. So

57
00:03:30,480 --> 00:03:34,350
we&#39;ve got the SELECT in here as Customer,

58
00:03:34,350 --> 00:03:37,570
but the SELECT remember runs right near the end.

59
00:03:37,570 --> 00:03:40,930
So GROUP BY happens before SELECT but

60
00:03:40,930 --> 00:03:45,510
the only thing running after ORDER BY and there we&#39;ve used it.

61
00:03:45,510 --> 00:03:50,950
It can use it. Again I think it&#39;s one of those things to keep in the back of your mind

62
00:03:50,950 --> 00:03:53,310
all the time because you&#39;re going to make that mistake sometimes.

63
00:03:53,310 --> 00:03:57,290
I think everyone makes that mistake sometimes. You think, oh of course, 

64
00:03:57,290 --> 00:04:01,400
yeah, it looks right because I&#39;ve declared it on the first line, actually it&#39;s not running it

65
00:04:01,400 --> 00:04:02,740
from the first line always.

66
00:04:02,740 --> 00:04:06,620
I mean it runs it as one line. Right. 

67
00:04:06,620 --> 00:04:11,610
It&#39;s converted into one thing. It&#39;s just one thing. So now we&#39;ve got

68
00:04:11,610 --> 00:04:15,840
for each salesperson, we&#39;ve got their name, we&#39;ve got sales revenue so

69
00:04:15,840 --> 00:04:19,390
although we&#39;ve used a function just remember it&#39;s not any function, it&#39;s

70
00:04:19,390 --> 00:04:22,940
aggregate functions only. So we&#39;re getting groups within groups here. You&#39;re getting, we&#39;re grouping 

71
00:04:22,940 --> 00:04:26,690
first of all by sales person and then for each sales person, we&#39;re grouping by customer

72
00:04:26,690 --> 00:04:30,410
and then we&#39;re seeing the sales revenue for each customer. We&#39;ve gotten, yes so that c.FirstName

73
00:04:30,410 --> 00:04:32,790
and c.LastName so that&#39;s coming from

74
00:04:32,790 --> 00:04:37,270
the Customer table. Yeah. Within there. So yeah, for each salesperson

75
00:04:37,270 --> 00:04:41,870
we&#39;ve got, the salesperson&#39;s also coming from the Customer table because each customer

76
00:04:41,870 --> 00:04:43,950
has a sales person is assigned to them. Yes.

77
00:04:43,950 --> 00:04:47,380
So each salesperson has been grouped up and within that we&#39;ve 

78
00:04:47,380 --> 00:04:50,300
grouped up each customer. So the order of GROUP BYs is relevant as well.

79
00:04:50,300 --> 00:04:53,840
We&#39;re grouping by salesperson first and then by the customer.

80
00:04:53,840 --> 00:04:57,560
So if we&#39;re building sort of cascading totals of things then

81
00:04:57,560 --> 00:05:01,550
you&#39;re going to group by each level that  you want to summarize I guess.

82
00:05:01,550 --> 00:05:04,880
Yes, so that then that becomes important, which order you do the GROUP BY within there.

83
00:05:04,880 --> 00:05:06,070
Okay.

