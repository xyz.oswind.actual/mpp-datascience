0
00:00:02,380 --> 00:00:06,440
So now we&#39;ll have a look at aggregate functions. And we&#39;re going to look at

1
00:00:06,440 --> 00:00:09,639
rather than ranking something or doing a

2
00:00:09,639 --> 00:00:13,889
scalar function of something, we&#39;re going to look at doing sum, counts,

3
00:00:13,889 --> 00:00:18,050
max, min, average. Again there&#39;s lots of  different aggregate functions, we&#39;re covering 

4
00:00:18,050 --> 00:00:19,279
some of the key ones.

5
00:00:19,279 --> 00:00:22,439
But if you want to do standard deviation,

6
00:00:22,439 --> 00:00:26,980
you could do that, and Graeme&#39;s obviously -- are you going to describe to me the difference 

7
00:00:26,980 --> 00:00:29,229
standard deviation and standard deviation of a population.

8
00:00:29,229 --> 00:00:34,340
No he&#39;s not, don&#39;t worry. I remember, come back for another course sometime.

9
00:00:34,340 --> 00:00:37,910
Yeah, there&#39;s really complicated things which I can vaguely remember from

10
00:00:37,910 --> 00:00:40,890
many years ago at University doing  statistical

11
00:00:40,890 --> 00:00:44,730
modeling, I can&#39;t remember what they are, but there&#39;s a huge number of different

12
00:00:44,730 --> 00:00:48,680
statistical functions we can do within there. So again once you&#39;re happy

13
00:00:48,680 --> 00:00:49,150
with

14
00:00:49,150 --> 00:00:52,770
something quite straightforward like a max or a sum, you can see that

15
00:00:52,770 --> 00:00:56,280
you can get out things like standard deviation values as well.

16
00:00:56,280 --> 00:01:00,110
In fact some really straightforward -- I can produce the information, I just can&#39;t

17
00:01:00,110 --> 00:01:01,120
tell you what it actually means.

18
00:01:01,120 --> 00:01:07,100
STDEV is the function. So we&#39;re looking at functions operating on

19
00:01:07,100 --> 00:01:11,990
a larger quantity of data again, not just one row. We&#39;re looking at aggregating

20
00:01:11,990 --> 00:01:13,609
data, so we&#39;re looking at sums, 

21
00:01:13,609 --> 00:01:16,859
we&#39;re looking at max, min of a set of rows of data. 

22
00:01:16,859 --> 00:01:21,299
And we&#39;re going to summarize that information however we define.

23
00:01:21,299 --> 00:01:25,659
And we&#39;ve also got a choice of GROUP BY which we&#39;ll come back to. But without the

24
00:01:25,659 --> 00:01:27,319
GROUP BY, we get the grand total.

25
00:01:27,319 --> 00:01:31,689
So if I do a SELECT COUNT star and a sum

26
00:01:31,689 --> 00:01:35,319
order quantity times unit price, those are grand totals for

27
00:01:35,319 --> 00:01:39,299
absolutely every record I have within there. So we can produce that

28
00:01:39,299 --> 00:01:46,299
total for the company and for all time, for all products, for all of everything.

29
00:01:46,430 --> 00:01:49,860
So to use some of these aggregate functions, here we&#39;re going to do a number

30
00:01:49,860 --> 00:01:53,020
different things. We&#39;ve got count star dollar that would just give us a simple count

31
00:01:53,020 --> 00:01:56,020
of products. We&#39;ve also got 

32
00:01:56,020 --> 00:01:59,400
in here a count of the distinct

33
00:01:59,400 --> 00:02:04,310
category ID. So the ProductCategoryID, how many distinct ones, how many different

34
00:02:04,310 --> 00:02:05,110
ones have we got

35
00:02:05,110 --> 00:02:09,470
within categories. And we&#39;ve got an average in here as well, average list price.

36
00:02:09,470 --> 00:02:10,440
And there are

37
00:02:10,440 --> 00:02:14,500
as I said, there&#39;s loads of different ones, we could do a 

38
00:02:14,500 --> 00:02:19,019
max, we could do a min, we could to standard deviation, we could do loads of

39
00:02:19,019 --> 00:02:20,630
things just replacing that value.

40
00:02:20,630 --> 00:02:24,959
So they&#39;re used in the same way, so we won&#39;t go through all of them, but it&#39;s really 

41
00:02:24,959 --> 00:02:25,680
simple, if you want

42
00:02:25,680 --> 00:02:28,910
to find the lowest ones put in min. Highest one max for example.

43
00:02:28,910 --> 00:02:33,610
Okay. In there, that&#39;s the grand total of all these things.

44
00:02:33,610 --> 00:02:37,220
So we can also filter this. Now what we&#39;re doing here

45
00:02:37,220 --> 00:02:40,950
is a count of product id, then display the

46
00:02:40,950 --> 00:02:44,200
average list price but only do that

47
00:02:44,200 --> 00:02:47,300
where the category name has the word bikes

48
00:02:47,300 --> 00:02:52,420
at the end in here. So it&#39;s filtering for that data.

49
00:02:52,420 --> 00:02:55,890
Ok, that&#39;s

50
00:02:55,890 --> 00:03:00,280
my grand totals. Again we&#39;re not doing any subtotaling. It&#39;s

51
00:03:00,280 --> 00:03:01,420
grand totals for 

52
00:03:01,420 --> 00:03:04,900
for things which end with the word bikes at the end of their category name.

53
00:03:04,900 --> 00:03:09,970
I guess wanting to pull out here, we&#39;re  counting the product id which we know

54
00:03:09,970 --> 00:03:11,060
is a unique value,

55
00:03:11,060 --> 00:03:15,049
so we know we&#39;re counting the distinct bikes in this case.

56
00:03:15,049 --> 00:03:16,640
Yeah the primary key is the

57
00:03:16,640 --> 00:03:21,030
typically the one that you would count. You can use count start to count the 

58
00:03:21,030 --> 00:03:24,280
rows, but of course that will include any duplicates that you have in their.

59
00:03:24,280 --> 00:03:28,370
So if you have a table or if the results the query included rows that

60
00:03:28,370 --> 00:03:28,790
were

61
00:03:28,790 --> 00:03:32,829
duplicates of one another, they&#39;d be  double counted. Or if you&#39;ve got nulls, 

62
00:03:32,829 --> 00:03:35,930
if you pick a column and it&#39;s got null values in there, it doesn&#39;t count them.

63
00:03:35,930 --> 00:03:40,299
Right. So count star can be useful if you know that you want to just count the

64
00:03:40,299 --> 00:03:41,150
number of rows, 

65
00:03:41,150 --> 00:03:44,540
then that sensible. If you want to count a specific business

66
00:03:44,540 --> 00:03:49,320
entity instance, like products, it&#39;s quite often better to explicitly count

67
00:03:49,320 --> 00:03:52,620
what you know is the unique key for that product. Yes

68
00:03:52,620 --> 00:03:56,120
and we know we&#39;ve got a join in here, so we could potentially get

69
00:03:56,120 --> 00:03:58,780
you know, once you start doing left joins and right joins,

70
00:03:58,780 --> 00:04:02,100
we&#39;re thinking okay what is it that I want to count? Because yeah

71
00:04:02,100 --> 00:04:05,500
we may well get nulls creeping in there. And the way 

72
00:04:05,500 --> 00:04:10,940
nulls are treated in aggregates as well. This changes our

73
00:04:10,940 --> 00:04:14,550
perception of how nulls work as well. You need to sort of check that because

74
00:04:14,550 --> 00:04:17,690
if we&#39;ve got nulls within our record set, they

75
00:04:17,690 --> 00:04:21,299
are normally ignored rather than being -- 

76
00:04:21,298 --> 00:04:24,190
So it doesn&#39;t return and error, it just ignores them and the count that we get

77
00:04:24,190 --> 00:04:25,400
back or whatever might

78
00:04:25,400 --> 00:04:28,700
not actually be right. Because our max 

79
00:04:28,700 --> 00:04:32,680
list price, you say well how can you say what max list price is because you&#39;ve got a null

80
00:04:32,680 --> 00:04:34,320
in there. That could be a higher value.

81
00:04:34,320 --> 00:04:38,730
Aggregates don&#39;t treat null they ignore nulls.

82
00:04:38,730 --> 00:04:42,590
That&#39;s a consideration in everything we said about nulls. Well

83
00:04:42,590 --> 00:04:43,870
you can&#39;t ignore them because they&#39;re 

84
00:04:43,870 --> 00:04:48,320
an unknown value. It could be millions of pounds worth of sales you&#39;re

85
00:04:48,320 --> 00:04:49,770
not counting because it&#39;s unknown.

86
00:04:49,770 --> 00:04:52,650
But, slightly different with aggregates.

