0
00:00:02,820 --> 00:00:06,480
Right, so we want to filter. We&#39;ve done a filter. We&#39;ve had a filter with a 

1
00:00:06,480 --> 00:00:10,009
WHERE in here and we can do that. We  can put a WHERE 

2
00:00:10,009 --> 00:00:13,379
into our aggregate functions, that works. And it is valid,

3
00:00:13,379 --> 00:00:17,620
but most of the time that we filter using an aggregate

4
00:00:17,620 --> 00:00:21,400
we filter using HAVING. What HAVING does is it will filter

5
00:00:21,400 --> 00:00:25,000
the results rather than the input. So

6
00:00:25,000 --> 00:00:28,230
we&#39;ve got a search condition that each group

7
00:00:28,230 --> 00:00:32,029
must satisfy. So we&#39;ve grouped up and then each of those groupings,

8
00:00:32,029 --> 00:00:35,820
we&#39;ve got an aggregate and we want to check those aggregates for each a certain value.

9
00:00:35,820 --> 00:00:39,519
And typically you&#39;re going to display that value.

10
00:00:39,519 --> 00:00:42,809
You actually don&#39;t have to display that value, but it

11
00:00:42,809 --> 00:00:47,489
is very unlikely that you wouldn&#39;t. But you could actually, so I want

12
00:00:47,489 --> 00:00:51,629
a list of customers that have more than 10

13
00:00:51,629 --> 00:00:54,870
orders. I don&#39;t want to see how many I&#39;ve got, I don&#39;t care.

14
00:00:54,870 --> 00:00:58,230
I just want to know what those CustomerIDs are. So you don&#39;t have to actually display the value

15
00:00:58,230 --> 00:00:58,890
you&#39;ve got HAVING in,

16
00:00:58,890 --> 00:01:02,059
most time you will, and almost certainly when you&#39;re testing it

17
00:01:02,059 --> 00:01:06,380
you would just to make sure it&#39;s correct. So, I guess I mean the point there is that the WHERE

18
00:01:06,380 --> 00:01:09,960
clause comes first and I think one way that&#39;s important to think about this is

19
00:01:09,960 --> 00:01:14,400
you use the WHERE clause to eliminate rows that you don&#39;t want to include in the groups. 

20
00:01:14,400 --> 00:01:18,270
And then once you&#39;ve done that, you use the HAVING clause to eliminate groups that you 

21
00:01:18,270 --> 00:01:19,460
don&#39;t want in the results.

22
00:01:19,460 --> 00:01:23,140
So there&#39;s sort of two phases of elimination going on here. The WHERE is happening before the

23
00:01:23,140 --> 00:01:24,380
aggregation,

24
00:01:24,380 --> 00:01:27,450
the HAVING is happening after the aggregation, on the results of the aggregation. 

25
00:01:27,450 --> 00:01:30,120
Yep.

26
00:01:30,120 --> 00:01:32,790
What we&#39;re doing here to start off with is a WHERE. 

27
00:01:32,790 --> 00:01:38,050
So it&#39;s working on the input. Before we get to aggregation in here we&#39;re looking

28
00:01:38,050 --> 00:01:38,420
at

29
00:01:38,420 --> 00:01:43,340
OrderDate of 2008. So we can run this and it&#39;s perfectly valid

30
00:01:43,340 --> 00:01:47,590
to filter using a WHERE because we&#39;re not filtering

31
00:01:47,590 --> 00:01:51,270
on the results, we&#39;re filtering on the inputs.

32
00:01:51,270 --> 00:01:54,680
Let&#39;s have a look at what happens. I want to have a look.  So hold on before we do that, let&#39;s

33
00:01:54,680 --> 00:01:58,340
explain what the query does. What we&#39;re getting from here is for each product

34
00:01:58,340 --> 00:01:59,390
id,

35
00:01:59,390 --> 00:02:03,130
we&#39;re getting the order quantity,

36
00:02:03,130 --> 00:02:06,940
and were filtering that for orders that were placed in 2008. So for each order in --

37
00:02:06,940 --> 00:02:10,290
each product that was ordered in 2008,

38
00:02:10,289 --> 00:02:13,650
tell me how many were actually ordered. Yeah, absolutely.

39
00:02:13,650 --> 00:02:18,850
Okay but I want to also filter and say no actually I only want the ones we ordered lots of,

40
00:02:18,850 --> 00:02:21,540
so I only want ones with a quantity higher than 50.

41
00:02:21,540 --> 00:02:25,200
Okay. So the first thing we might do in there is say well

42
00:02:25,200 --> 00:02:29,060
we&#39;ve already got a WHERE in here, so we could say well how about

43
00:02:29,060 --> 00:02:33,010
we had add an AND to that and say that

44
00:02:33,010 --> 00:02:39,480
the sum of order quantity is great than  50.

45
00:02:39,480 --> 00:02:42,940
Which looks fine, it reads okay

46
00:02:42,940 --> 00:02:46,720
if you read out, but you can&#39;t have that. An aggregate may not appear in a WHERE clause.

47
00:02:46,720 --> 00:02:50,480
in fact, if you carry on it kind of explains how to fix it. So it goes on and says

48
00:02:50,480 --> 00:02:56,000
about HAVINGs in here, it&#39;s quite a long error in there. And that&#39;s what we need. The key

49
00:02:56,000 --> 00:02:57,490
thing is that word HAVING in there.

50
00:02:57,490 --> 00:03:02,890
So if we remove it from here, we&#39;re going to leave the 2008 in, I still want to filter.

51
00:03:02,890 --> 00:03:06,910
I only want to find ones from 2008. But I want to add

52
00:03:06,910 --> 00:03:10,780
to this to say that it&#39;s

53
00:03:10,780 --> 00:03:16,090
HAVING a sum of order quantity greater than 50. So after we&#39;ve done the aggregation, now do the

54
00:03:16,090 --> 00:03:16,590
test.

55
00:03:16,590 --> 00:03:20,050
You can&#39;t do it on the WHERE because the WHERE ran before we aggregated. so it doesn&#39;t

56
00:03:20,050 --> 00:03:20,910
know what the sum of

57
00:03:20,910 --> 00:03:24,580
order quantity is. So it&#39;s all down to that

58
00:03:24,580 --> 00:03:29,090
order of execution again which is an important part. And now you&#39;ll see they&#39;re

59
00:03:29,090 --> 00:03:29,650
all over 

60
00:03:29,650 --> 00:03:32,770
50, it&#39;s filtered, they&#39;re all 2008 and they&#39;re all over 

61
00:03:32,770 --> 00:03:37,150
50. 2008 has to be done on the input because it&#39;s not an aggregate value.

62
00:03:37,150 --> 00:03:40,770
50 has to be done the output. So the WHERE clause

63
00:03:40,770 --> 00:03:46,070
filters my sales to only include the ones that were in 2008. That&#39;s

64
00:03:46,070 --> 00:03:47,700
individual rows that I&#39;m filtering out.

65
00:03:47,700 --> 00:03:51,490
It&#39;s then going to, group those rows by product id

66
00:03:51,490 --> 00:03:54,660
and then it&#39;s going to filter out the ones where the grouped

67
00:03:54,660 --> 00:03:58,560
row don&#39;t have an order quantity greater than than 50.

68
00:03:58,560 --> 00:04:02,080
It&#39;s key to kind of get your head around the idea that WHERE filters out rows

69
00:04:02,080 --> 00:04:05,530
that may then be grouped, and once you&#39;ve grouped them you can then filter out groups

70
00:04:05,530 --> 00:04:06,110
using

71
00:04:06,110 --> 00:04:09,790
HAVING. Yeah, yeah, and if you ever do you get it wrong that error message 

72
00:04:09,790 --> 00:04:10,860
that displays

73
00:04:10,860 --> 00:04:14,690
mentions about HAVING in the error, so that straightway light bulb,

74
00:04:14,690 --> 00:04:16,470
of course, must be a HAVING.

