0
00:00:02,920 --> 00:00:05,690
So we&#39;ve have had look at UNION queries.

1
00:00:05,690 --> 00:00:09,000
They are very useful queries in

2
00:00:09,000 --> 00:00:13,010
many situations where you do want to combine the output. So we&#39;ve looked at different,

3
00:00:13,010 --> 00:00:16,039
combining the outputs from different tables, but of course you can use a UNION

4
00:00:16,039 --> 00:00:16,880
query to

5
00:00:16,880 --> 00:00:20,109
have the output from the same table as well, which is an example we didn&#39;t do.

6
00:00:20,109 --> 00:00:23,710
When we talked about TOP, we might want the top 10 percent. Maybe I want like key 

7
00:00:23,710 --> 00:00:26,119
sales, that would be the top 10 percent

8
00:00:26,119 --> 00:00:30,669
and the bottom 20 percent. We could use two TOP queries sorted in

9
00:00:30,669 --> 00:00:35,250
reverse ways, combine the outputs from both, and that comes from same query.

10
00:00:35,250 --> 00:00:36,309
So, it doesn&#39;t have to be from

11
00:00:36,309 --> 00:00:40,109
different tables, those two queries can be from the same table.

12
00:00:40,109 --> 00:00:43,440
And also there can be more than one UNION. So there&#39;s no reason we can&#39;t keep

13
00:00:43,440 --> 00:00:46,789
unioning. And it could well be another example would be

14
00:00:46,789 --> 00:00:51,070
archive. Archives are really useful things in databases because they make

15
00:00:51,070 --> 00:00:51,710
your

16
00:00:51,710 --> 00:00:54,760
working set of records smaller. And

17
00:00:54,760 --> 00:00:59,039
if it&#39;s smaller, it&#39;s faster typically. And therefore, the things we&#39;re not interested in

18
00:00:59,039 --> 00:01:01,879
the majority of the time, we can archive that off

19
00:01:01,879 --> 00:01:05,979
to a separate table with potentially the  same structure. Well it probably has

20
00:01:05,979 --> 00:01:07,240
exactly the same structure,

21
00:01:07,240 --> 00:01:11,130
it&#39;s archived off, it&#39;s not used day to day. We&#39;d never insert new records in 

22
00:01:11,130 --> 00:01:12,729
there because it&#39;s last year&#39;s

23
00:01:12,729 --> 00:01:15,840
data, so nothing new goes in there, nothing gets updated in there.

24
00:01:15,840 --> 00:01:20,229
It&#39;s all happened in the past, but we now want to run a report

25
00:01:20,229 --> 00:01:23,250
over the last five years. So what we can do is

26
00:01:23,250 --> 00:01:28,200
UNION all of those yearly records with the current records and we&#39;ve got 

27
00:01:28,200 --> 00:01:31,430
the best of both worlds. We&#39;ve got a fast working set of data

28
00:01:31,430 --> 00:01:35,729
because we&#39;re only working with the current year, but also we can run queries

29
00:01:35,729 --> 00:01:38,880
on all of the data by unioning the other tables.

30
00:01:38,880 --> 00:01:42,759
Absolutely. And if we&#39;re doing union, and we&#39;re doing a UNION ALL and we&#39;re not removing 

31
00:01:42,759 --> 00:01:45,930
duplicates, then actually it&#39;s quite quick as well because it only has to

32
00:01:45,930 --> 00:01:50,689
return, it&#39;s not doing any filtering, so it&#39;s saying, return all of this, all of this, all of this, all of this.

33
00:01:50,689 --> 00:01:55,600
And that&#39;s a key learning point as well, if you know that the sets are distinct

34
00:01:55,600 --> 00:01:56,289
and you don&#39;t want

35
00:01:56,289 --> 00:01:59,670
to have the overhead of checking for distinctness, then

36
00:01:59,670 --> 00:02:04,090
just get into the habit of using UNION ALL and removing that overhead. Absolutely. It&#39;s a hard thing

37
00:02:04,090 --> 00:02:06,310
to do actually because we always calling them UNION queries. Yes.

38
00:02:06,310 --> 00:02:10,429
In the same way we call them SELECT queries, when we talk SELECT, we don&#39;t call them

39
00:02:10,429 --> 00:02:13,700
SELECT DISTINCTs. It would be nice actually, it if it was the other way around

40
00:02:13,700 --> 00:02:17,420
the extra word did the, yes indeed, yes did that the other way around.

41
00:02:17,420 --> 00:02:21,769
And then our INTERSECT and EXCEPT queries again it&#39;s an extension of the same idea

42
00:02:21,769 --> 00:02:22,730
but what it&#39;s doing is

43
00:02:22,730 --> 00:02:26,680
is dealing with those duplicates in different ways. Either by finding me which

44
00:02:26,680 --> 00:02:27,900
ones are the duplicates

45
00:02:27,900 --> 00:02:32,880
or by eliminating from the first set the ones are also in the second set.

46
00:02:32,880 --> 00:02:36,010
I can write some pretty sophisticated queries using those

47
00:02:36,010 --> 00:02:40,330
techniques. Yes and you will, I think that&#39;s an archive

48
00:02:40,330 --> 00:02:44,280
scenario, it is a real scenario. You do find a lot of people do that, 

49
00:02:44,280 --> 00:02:48,860
they move the data so they&#39;ve got a smaller -- because as we&#39;re going forward --

50
00:02:48,860 --> 00:02:52,860
many years ago, when I started in IT it  wasn&#39;t really an issue, we

51
00:02:52,860 --> 00:02:54,150
didn&#39;t have much data,

52
00:02:54,150 --> 00:02:58,010
now, it&#39;s vast. So 

53
00:02:58,010 --> 00:03:01,269
keeping your current working set manageable then,

54
00:03:01,269 --> 00:03:06,360
but we want to be able to combine it back again and UNIONs are ideal for that. So there you have it, UNION, 

55
00:03:06,360 --> 00:03:10,230
INTERSECT, and EXCEPT. Another way of joining multiple sets of data.

56
00:03:10,230 --> 00:03:13,489
This time the results of multiple queries joined together into one

57
00:03:13,489 --> 00:03:17,680
result set. Have a go at the lab for this one, I hope you&#39;ve already downloaded

58
00:03:17,680 --> 00:03:19,440
the lab documents and set up your

59
00:03:19,440 --> 00:03:22,970
environment, set up your Azure SQL Database. The lab in this one will

60
00:03:22,970 --> 00:03:24,420
challenge you with some

61
00:03:24,420 --> 00:03:28,130
business problems to return data that brings together 

62
00:03:28,130 --> 00:03:31,459
results from different queries. We&#39;ll get you to

63
00:03:31,459 --> 00:03:35,459
have a go at that. And once you&#39;re comfortable using UNION and INTERSECT 

64
00:03:35,459 --> 00:03:36,170
and EXCEPT, 

65
00:03:36,170 --> 00:03:39,530
come back and join us for the next module where we&#39;ll look at

66
00:03:39,530 --> 00:03:41,239
approaches to aggregating data.

