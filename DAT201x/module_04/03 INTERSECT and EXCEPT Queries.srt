0
00:00:02,020 --> 00:00:05,700
We talked about UNION queries and we&#39;ve looked at UNION ALL 

1
00:00:05,700 --> 00:00:09,660
and looking at whether we have records that occur in

2
00:00:09,660 --> 00:00:13,580
both queries or that distinct idea of

3
00:00:13,580 --> 00:00:17,390
just doing the UNION and finding ones and removing duplicates. We can do a little bit

4
00:00:17,390 --> 00:00:20,070
more with that sort of idea here using INTERSECT

5
00:00:20,070 --> 00:00:23,080
and then we&#39;ll go into EXCEPT as well. But INTERSECT,

6
00:00:23,080 --> 00:00:26,330
what we&#39;re looking at here is only

7
00:00:26,330 --> 00:00:31,699
rows that appear in both sets. So here we are looking overlapping them and saying okay

8
00:00:31,699 --> 00:00:36,060
we&#39;ve talked about removing the duplicates. I want to find those duplicates,

9
00:00:36,060 --> 00:00:37,860
I want to find the ones that do occur in both.

10
00:00:37,860 --> 00:00:42,380
So this is different in we are explicitly looking for those ones that occur

11
00:00:42,380 --> 00:00:47,430
in both of the queries. Now conceptually this sounds familiar and I

12
00:00:47,430 --> 00:00:49,000
can see a venn diagram

13
00:00:49,000 --> 00:00:52,360
oriented vertically rather than horizontally. 

14
00:00:52,360 --> 00:00:55,480
So this is kind of similar to an inner join.

15
00:00:55,480 --> 00:00:59,480
But just to emphasize the point that a join is where we&#39;re

16
00:00:59,480 --> 00:01:03,300
extending the number of columns by bringing columns back from multiple tables.

17
00:01:03,300 --> 00:01:06,580
In this case what we&#39;re doing is extending rows, except

18
00:01:06,580 --> 00:01:10,090
that because we&#39;re using INTERSECT, we&#39;re only finding the rows that exist in both

19
00:01:10,090 --> 00:01:11,110
datasets.

20
00:01:11,110 --> 00:01:15,330
Absolutely, yep. And so in the join scenario, 

21
00:01:15,330 --> 00:01:19,240
I couldn&#39;t actually add the data from

22
00:01:19,240 --> 00:01:23,280
one table to the data in another table in that same column. They would appear

23
00:01:23,280 --> 00:01:24,030
as separate columns.

24
00:01:24,030 --> 00:01:27,939
Right. So here I&#39;m doing, and that&#39;s why we put it vertically, because

25
00:01:27,939 --> 00:01:28,570
they appear

26
00:01:28,570 --> 00:01:33,509
in that same column. Okay. So that&#39;s explicitly looking for

27
00:01:33,509 --> 00:01:38,869
those overlapping ones. So we&#39;ve looked at INTERSECT, they&#39;re the ones that occur in

28
00:01:38,869 --> 00:01:41,909
both. But what about if you want to look at

29
00:01:41,909 --> 00:01:46,299
something that occurs in one record set but doesn&#39;t occur in the other record set.

30
00:01:46,299 --> 00:01:50,880
So we&#39;ve got those two again vertically overlapping, but this time we&#39;re saying OK

31
00:01:50,880 --> 00:01:55,909
you are an employee but I want you not to be a customer. So I want to remove

32
00:01:55,909 --> 00:01:59,869
anything that occurs in the other table from the results of the first table.

33
00:01:59,869 --> 00:02:02,899
So logically now

34
00:02:02,899 --> 00:02:06,250
if we look at the venn diagram, we can see that the top

35
00:02:06,250 --> 00:02:10,090
table there has had a chunk taken out of it by the bottom tables. So if you exist

36
00:02:10,090 --> 00:02:10,669
in

37
00:02:10,669 --> 00:02:14,579
that bottom table, which in this one it&#39;s customers, I don&#39;t want to see you in

38
00:02:14,579 --> 00:02:15,420
the employees

39
00:02:15,420 --> 00:02:18,930
set in here. I only want to see employees who are not customers as well.

40
00:02:18,930 --> 00:02:22,930
Right. So that the blue circle represents my employees,

41
00:02:22,930 --> 00:02:27,180
the white represents my customers, and where white overlaps blue we&#39;re saying don&#39;t

42
00:02:27,180 --> 00:02:30,390
include the employees who are also customers. Absolutely. And the 

43
00:02:30,390 --> 00:02:33,970
order is, when you were saying before, you were saying about with outer joins, the order is

44
00:02:33,970 --> 00:02:37,330
important. Again it&#39;s the same here, the order&#39;s important. If I&#39;d switched the order

45
00:02:37,330 --> 00:02:38,380
of the tables around,

46
00:02:38,380 --> 00:02:42,170
then it would be looking at customers who are not employees rather than employees who are not

47
00:02:42,170 --> 00:02:42,880
customers.

48
00:02:42,880 --> 00:02:46,590
So the order is completely relevant to what you get from there.

49
00:02:46,590 --> 00:02:47,480
Ok, it makes sense.

