0
00:00:02,009 --> 00:00:08,469
So I just switched to another set of queries, so we&#39;ve already seen

1
00:00:08,469 --> 00:00:11,790
that we have a slightly

2
00:00:11,790 --> 00:00:15,230
overlapping set of data. We have a Donna who sits there as

3
00:00:15,230 --> 00:00:18,579
both a customer and an employee. So the first thing I want to do because that

4
00:00:18,579 --> 00:00:20,589
just returns the union,

5
00:00:20,589 --> 00:00:24,130
is that we&#39;ve run this -- let&#39;s remove

6
00:00:24,130 --> 00:00:27,159
the extra bit information so that we

7
00:00:27,159 --> 00:00:32,090
get back our duplicate -- and we&#39;ve identified that when we run this

8
00:00:32,090 --> 00:00:36,250
as a UNION ALL, we get 441. We&#39;ve run this as a UNION,

9
00:00:36,250 --> 00:00:40,120
we get 440. We know there&#39;s a duplicate in there. Now

10
00:00:40,120 --> 00:00:44,600
fortunately, I had Graeme with me. And my psychic powers found us Donna. He knows 

11
00:00:44,600 --> 00:00:48,010
every single record in that table, he knew which one it would be. &lt;laughter&gt;

12
00:00:48,010 --> 00:00:52,210
Now, you&#39;re not always going to have Graeme with you when you do this.

13
00:00:52,210 --> 00:00:56,050
You can only hope. &lt;laughter&gt; So

14
00:00:56,050 --> 00:01:00,329
we do fortunately have a means of replicating Graeme

15
00:01:00,329 --> 00:01:03,980
in SQL code. So what we can do is use this

16
00:01:03,980 --> 00:01:07,590
INTERSECT to find what is that overlapping record?

17
00:01:07,590 --> 00:01:11,039
Because it might be a problem, there might be a reason why is that 440 and

18
00:01:11,039 --> 00:01:13,649
441 when we are expecting it to be the same. 

19
00:01:13,649 --> 00:01:17,020
So, when I run this one, we now find

20
00:01:17,020 --> 00:01:21,149
there we go, Donna Carreras is listed. So we can, 

21
00:01:21,149 --> 00:01:25,170
if it&#39;s a problem we&#39;ve now found that record and we can change that,

22
00:01:25,170 --> 00:01:30,009
deal with that, we can update that  record possibly. The next thing we want

23
00:01:30,009 --> 00:01:33,609
find is those EXCEPT queries. So

24
00:01:33,609 --> 00:01:36,780
now we&#39;re saying, well I want to find customers

25
00:01:36,780 --> 00:01:40,459
but not if they&#39;re an employee. I&#39;m not interested in that. It may be a relevant

26
00:01:40,459 --> 00:01:43,969
thing, I want my customers but if they&#39;re an employee I&#39;m not going to go out and try

27
00:01:43,969 --> 00:01:44,240
and

28
00:01:44,240 --> 00:01:49,090
sell them additional products. I just want customers who are not employees in

29
00:01:49,090 --> 00:01:49,600
this case.

30
00:01:49,600 --> 00:01:53,289
So we&#39;re not interested in the Donna Carreras record

31
00:01:53,289 --> 00:01:57,609
for customers, we want everyone else. Now, when I run this

32
00:01:57,609 --> 00:02:00,639
we&#39;ve only got 103 rows. It&#39;s a much lower number of rows

33
00:02:00,639 --> 00:02:03,729
because where only showing

34
00:02:03,729 --> 00:02:07,439
the customers. Before we were showing customers and the employees and removing duplicates. 

35
00:02:07,439 --> 00:02:10,940
Now, we&#39;re just showing customers and removing

36
00:02:10,940 --> 00:02:11,700
in this case

37
00:02:11,700 --> 00:02:17,150
Donna Carreras from the customer list. So I&#39;m expecting I&#39;ve got probably, well I must have 104

38
00:02:17,150 --> 00:02:17,849
customers,

39
00:02:17,849 --> 00:02:21,129
but I&#39;ve removed one because that&#39;s Donna Carreras.

40
00:02:21,129 --> 00:02:24,689
I was going to say, if you select the first SELECT query and run that, 

41
00:02:24,689 --> 00:02:28,670
that&#39;s all the customers, we know that and there&#39;s 104 rows. And we know that

42
00:02:28,670 --> 00:02:32,079
Donna Carreras is in that. I&#39;m not going to search, we could have done that again, but that&#39;s going to be the one. 

43
00:02:32,079 --> 00:02:35,879
So we&#39;ve now found removing that one

44
00:02:35,879 --> 00:02:40,859
we&#39;ve got 103 rows. Remember again, your getting it 

45
00:02:40,859 --> 00:02:44,040
to do a bit of work there. So we&#39;re having search, a matchup of all the records.

46
00:02:44,040 --> 00:02:48,549
So it&#39;s not something that I want to do just for the sake of doing it. I probably

47
00:02:48,549 --> 00:02:49,930
want to go and deal with,

48
00:02:49,930 --> 00:02:54,189
if it&#39;s really a problem then okay, then Donna Carreras needs to be updated to make sure

49
00:02:54,189 --> 00:02:55,400
she&#39;s distinct.

50
00:02:55,400 --> 00:02:58,519
But there may be, I mean there perfectly legitimate scenarios 

51
00:02:58,519 --> 00:03:00,489
where you want to find all the records from one

52
00:03:00,489 --> 00:03:03,840
query that aren&#39;t in the results from another. Exactly. If I&#39;m a mail shout

53
00:03:03,840 --> 00:03:07,209
to all my customers, I don&#39;t really want to mail shout all my employees as well

54
00:03:07,209 --> 00:03:10,209
who just happened to have bought something in the staff canteen.

