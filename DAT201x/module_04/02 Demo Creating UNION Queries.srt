0
00:00:02,270 --> 00:00:05,910
Alright, so let&#39;s have a look creating UNION queries. So we&#39;ve got

1
00:00:05,910 --> 00:00:09,759
a couple of queries here. We want to select the FirstName and the LastName

2
00:00:09,759 --> 00:00:13,080
from the Employees table. And we want to select the FirstName and the LastName from the

3
00:00:13,080 --> 00:00:14,019
Customers table.

4
00:00:14,019 --> 00:00:17,029
But we don&#39;t want to have 

5
00:00:17,029 --> 00:00:21,679
two separate record sets. We want one record set, all of the output in one place.

6
00:00:21,679 --> 00:00:26,699
So we can run these independently and they should work. I mean you can test these with 

7
00:00:26,699 --> 00:00:28,970
saying hey does this side, hey does that side works.

8
00:00:28,970 --> 00:00:32,559
There&#39;s 337 employees and we can test this side.

9
00:00:32,558 --> 00:00:36,570
And this side works, there&#39;s 104 customers that we&#39;ve got in there.

10
00:00:36,570 --> 00:00:41,270
If we UNION these two, then

11
00:00:41,270 --> 00:00:44,320
we&#39;ve got 440 rows which is

12
00:00:44,320 --> 00:00:48,830
both the customers and employees and anyone who&#39;s

13
00:00:48,830 --> 00:00:52,250
quick at math might have worked out

14
00:00:52,250 --> 00:00:56,380
the it doesn&#39;t quite add up. Because if I do a UNION ALL...

15
00:00:56,380 --> 00:01:01,200
So hold on, before we do the UNION ALL, we&#39;ve done a UNION. It&#39;s take the results from the Customers

16
00:01:01,200 --> 00:01:03,500
table and the results from the Employees table.

17
00:01:03,500 --> 00:01:08,649
Yep. But because it&#39;s a UNION, it behaves the same as a DISTINCT. So if I

18
00:01:08,649 --> 00:01:09,969
happen to have

19
00:01:09,969 --> 00:01:13,090
a customer and an employee with the same first and last name,

20
00:01:13,090 --> 00:01:16,950
I&#39;ve now only got one copy of that, it&#39;s removed the duplicates. Exactly, yeah.

21
00:01:16,950 --> 00:01:20,579
Okay, so if you&#39;d quickly added up the result numbers, actually we&#39;re one out

22
00:01:20,579 --> 00:01:24,069
because we don&#39;t have quite enough. But if I do a UNION ALL,

23
00:01:24,069 --> 00:01:28,490
then we see that we&#39;ve got 441 rows. So there&#39;s one row that occurs in both.

24
00:01:28,490 --> 00:01:31,819
Okay let me see if I can use my psychic powers to predict,

25
00:01:31,819 --> 00:01:35,810
I think it&#39;s a lady and I think her name begins with a D.

26
00:01:35,810 --> 00:01:39,740
A lady beginning with a D? And a last name beginning with

27
00:01:39,740 --> 00:01:43,560
it&#39;s coming to me. That would help, we&#39;re ordered by last name. C yeah,

28
00:01:43,560 --> 00:01:48,999
let&#39;s try that. Yeah, okay, okay.

29
00:01:48,999 --> 00:01:52,499
So if we scroll down, so no it&#39;s not Campbell.

30
00:01:52,499 --> 00:01:55,479
 

31
00:01:55,479 --> 00:01:58,560
But we do seem to have a Donna Carreras. It&#39;s almost as if,

32
00:01:58,560 --> 00:02:02,950
it&#39;s as if we had seeded the data to get that result. &lt;laughter&gt;

33
00:02:02,950 --> 00:02:06,600
So the point is we have someone called Donna Carreras and that&#39;s the name

34
00:02:06,600 --> 00:02:08,899
of both a customer and employee

35
00:02:08,899 --> 00:02:14,030
which may or may not be the same person. Okay. So I guess what would be useful then

36
00:02:14,030 --> 00:02:14,849
would be to know

37
00:02:14,849 --> 00:02:18,090
if we&#39;ve got those two duplicates, which one

38
00:02:18,090 --> 00:02:21,150
is coming from the customer table and which one from the employee table?

39
00:02:21,150 --> 00:02:25,620
So what I&#39;d do, is if I add a literal value because it may well be that Donna Carreras is a

40
00:02:25,620 --> 00:02:28,200
separate person, the customer and employee may well not be the same

41
00:02:28,200 --> 00:02:29,080
person anyway.

42
00:02:29,080 --> 00:02:32,090
So we can add literal values so

43
00:02:32,090 --> 00:02:35,840
I&#39;ll add one is here saying well let&#39;s say this one is an employee.

44
00:02:35,840 --> 00:02:40,920
Now worth using because it&#39;s essentially,

45
00:02:40,920 --> 00:02:44,239
that&#39;s not a calculated column, but it&#39;s not being derived from the table so it&#39;s always

46
00:02:44,239 --> 00:02:45,549
worth using an alias

47
00:02:45,549 --> 00:02:50,150
anyway. Okay. I&#39;m not going to call it employee because I&#39;m going to use the same column

48
00:02:50,150 --> 00:02:53,190
in the Customers table and it wouldn&#39;t work for both. So to I&#39;ll call it

49
00:02:53,190 --> 00:02:56,920
Type because it is the type of contact. 

50
00:02:56,920 --> 00:03:01,510
And then we&#39;ll do the same here, but the literal value we&#39;ll have in

51
00:03:01,510 --> 00:03:05,829
here will be customer. Now

52
00:03:05,829 --> 00:03:08,940
as we mentioned before with aliases, I don&#39;t need to alias this. 

53
00:03:08,940 --> 00:03:12,639
You can do it, it doesn&#39;t hurt because you may think well it looks neater and

54
00:03:12,639 --> 00:03:16,540
more aligned with the two. Actually it won&#39;t make any difference, it&#39;s going to use the alias from 

55
00:03:16,540 --> 00:03:17,410
the first query.

56
00:03:17,410 --> 00:03:21,130
So that column is always going to be called Type regardless of how many I&#39;m unioning. Yes.

57
00:03:21,130 --> 00:03:21,790
 

58
00:03:21,790 --> 00:03:26,400
They&#39;re always going to use that first reference in there. So if we execute

59
00:03:26,400 --> 00:03:29,840
this one, now let&#39;s do this, we&#39;ll start off with the UNION

60
00:03:29,840 --> 00:03:36,840
in here. So

61
00:03:37,000 --> 00:03:41,650
now we&#39;re getting 441 rows. Before we were getting 440.

62
00:03:41,650 --> 00:03:45,280
Because it&#39;s distinct including

63
00:03:45,280 --> 00:03:48,920
now this new column. Absolutely. So of course the customers different from the employee. It&#39;s distinct

64
00:03:48,920 --> 00:03:52,450
across all the columns. So if we scroll down

65
00:03:52,450 --> 00:03:56,340
to Donna what was her? 

66
00:03:56,340 --> 00:04:02,200
Carreras, Donna Carreras is her name. Donna Carreras, there 

67
00:04:02,200 --> 00:04:06,900
we go, customer employee, both in there because it&#39;s not distinct 

68
00:04:06,900 --> 00:04:10,290
those are separate to each other. They 

69
00:04:10,290 --> 00:04:15,060
are distinct. But we now know that the result sets from each of these

70
00:04:15,060 --> 00:04:18,410
will be DISTINCT because we&#39;re explicitly putting in an identifier of our own

71
00:04:18,410 --> 00:04:22,270
in there. Yeah. So we&#39;re, I guess from what we said earlier we&#39;re taking a

72
00:04:22,270 --> 00:04:24,630
performance hit here because we&#39;re using UNION,

73
00:04:24,630 --> 00:04:27,650
we know they&#39;re distinct so why don&#39;t we just UNION ALL.

74
00:04:27,650 --> 00:04:30,910
Yeah, it won&#39;t be so 

75
00:04:30,910 --> 00:04:34,040
noticeable if I run it again. Because

76
00:04:34,040 --> 00:04:37,960
it&#39;s a small amount of data. It gets cached. Actually, I don&#39;t know if you noticed, it did 

77
00:04:37,960 --> 00:04:39,229
quite a while to run that UNION query.

78
00:04:39,229 --> 00:04:43,000
Certainly, a lot longer than that. Yes. So yeah

79
00:04:43,000 --> 00:04:46,120
always ... and we still get our 441 rows. Yeah, 

80
00:04:46,120 --> 00:04:49,229
same records, faster performance. It&#39;s always worth

81
00:04:49,229 --> 00:04:53,430
defaulting to UNION ALL in the same way you to default to not using DISTINCT most of the

82
00:04:53,430 --> 00:04:54,110
time. Sure.

83
00:04:54,110 --> 00:04:58,810
And because we said, that Donna Carreras might well be a separate person.

84
00:04:58,810 --> 00:05:02,750
There&#39;s no reason we can&#39;t have a customer and employee with the same name, and we just strip them 

85
00:05:02,750 --> 00:05:03,419
out just

86
00:05:03,419 --> 00:05:06,479
for arbitrary reasons. Great, okay,

87
00:05:06,479 --> 00:05:06,729
thanks.

