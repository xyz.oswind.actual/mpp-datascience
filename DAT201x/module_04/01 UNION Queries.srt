0
00:00:05,720 --> 00:00:09,350
Welcome back! I hope you enjoyed completing the lab on joins

1
00:00:09,350 --> 00:00:12,600
and that you&#39;re now comfortable using them to join together

2
00:00:12,600 --> 00:00:13,390
data from

3
00:00:13,390 --> 00:00:16,960
multiple tables that you end up with rows that contain columns from

4
00:00:16,960 --> 00:00:17,750
multiple tables.

5
00:00:17,750 --> 00:00:21,960
In this module, module four, we&#39;re going to talk about something

6
00:00:21,960 --> 00:00:25,750
similar but slightly different. Instead of talking about joining

7
00:00:25,750 --> 00:00:29,170
tables together in a single query, we&#39;re going to talk about unioning the

8
00:00:29,170 --> 00:00:30,890
results of multiple queries together.

9
00:00:30,890 --> 00:00:34,820
And to talk us through that, Jeff is with us once again, so

10
00:00:34,820 --> 00:00:38,660
Jeff why don&#39;t you tell us we&#39;re going to  talk about in this module. Okay we&#39;re going to

11
00:00:38,660 --> 00:00:39,300
have a look

12
00:00:39,300 --> 00:00:43,739
at these union queries. We mentioned combining multiple queries into one and

13
00:00:43,739 --> 00:00:44,950
we&#39;re also going to have a look at

14
00:00:44,950 --> 00:00:48,160
INTERSCET and EXCEPT which are very much connected to

15
00:00:48,160 --> 00:00:51,160
UNIONs. They are a subset if you like of UNION queries. Sounds good. 

16
00:00:51,160 --> 00:00:55,250
So to start off with, UNION queries

17
00:00:55,250 --> 00:00:58,510
logically its two queries,

18
00:00:58,510 --> 00:01:01,679
or more than two queries, but let&#39;s start with two queries.

19
00:01:01,679 --> 00:01:05,840
And we&#39;re combining the results but not in the same way we do with join where

20
00:01:05,840 --> 00:01:07,580
we can have more columns

21
00:01:07,580 --> 00:01:11,610
of data. We&#39;re actually going to have more rows of data. So we say okay I want

22
00:01:11,610 --> 00:01:12,870
all the output from one,

23
00:01:12,870 --> 00:01:16,520
I want all the output from another one, and we&#39;re going to put the two one after another in

24
00:01:16,520 --> 00:01:17,270
the record set.

25
00:01:17,270 --> 00:01:20,930
So the example I&#39;ve got here, we&#39;re going to have a look at employee data and customer

26
00:01:20,930 --> 00:01:21,460
data.

27
00:01:21,460 --> 00:01:24,829
Because I want a list of all my contacts whether they be employees

28
00:01:24,829 --> 00:01:29,189
or customers as one list. So they&#39;re all going to appear here.

29
00:01:29,189 --> 00:01:33,290
We&#39;ve got the country/region, we&#39;ve got the city and we&#39;ve got that whether they&#39;re an employee or a

30
00:01:33,290 --> 00:01:35,439
customer because UNION combines the two.

31
00:01:35,439 --> 00:01:38,500
Now it does say there, only distinct rows.

32
00:01:38,500 --> 00:01:42,719
Okay. It&#39;s possible we might have an employee who is a customer.

33
00:01:42,719 --> 00:01:46,630
Right, yeah. We can have employees who buy things from our company.

34
00:01:46,630 --> 00:01:51,219
I don&#39;t want them twice. So I&#39;m not really, because I need a list contacts,

35
00:01:51,219 --> 00:01:51,820
they shouldn&#39;t

36
00:01:51,820 --> 00:01:55,020
occur in my list twice. So therefore it,

37
00:01:55,020 --> 00:01:58,149
by default with just UNION will

38
00:01:58,149 --> 00:02:03,439
remove those duplicate records. So I get the same behavior as a DISTINCT

39
00:02:03,439 --> 00:02:04,840
and a regular SELECT statement then.

40
00:02:04,840 --> 00:02:08,330
Like a DISTINCT, yep. So it&#39;s going to remove those excess rows.

41
00:02:08,330 --> 00:02:11,710
And that&#39;s why, when I&#39;ve drawn up the venn diagram, their touching,

42
00:02:11,710 --> 00:02:12,980
that overlapping there,

43
00:02:12,980 --> 00:02:16,660
it&#39;s basically saying that they&#39;re separate.

44
00:02:16,660 --> 00:02:19,880
If they were overlapping, it would get rid of,

45
00:02:19,880 --> 00:02:22,680
you would only have one of those middle ones, you wouldn&#39;t have them

46
00:02:22,680 --> 00:02:26,019
twice in there. So John Smith is a customer.

47
00:02:26,019 --> 00:02:29,510
If John Smith is an employee it would only display it once.

48
00:02:29,510 --> 00:02:33,799
Right, okay I see.

49
00:02:33,799 --> 00:02:37,409
That affects performance because it has to check.

50
00:02:37,409 --> 00:02:41,760
So it&#39;s got to check for every single row in one of 

51
00:02:41,760 --> 00:02:45,450
those tables it&#39;s got to check whether there&#39;s that value in the other one.

52
00:02:45,450 --> 00:02:50,849
So that&#39;s not insignificant performance task for it to do to go and make sure

53
00:02:50,849 --> 00:02:52,150
that that&#39;s not the case.

54
00:02:52,150 --> 00:02:56,700
And of course, if I&#39;m adding multiple UNION clauses for multiple queries it&#39;s

55
00:02:56,700 --> 00:03:00,000
going to just get worse. Yeah. Because it&#39;s going to have to check right across all of the different

56
00:03:00,000 --> 00:03:03,560
tables. It&#39;s got to check against all of the other tables and so it gets worse and worse and worse. 

57
00:03:03,560 --> 00:03:05,970
So we do have UNION ALL.

58
00:03:05,970 --> 00:03:09,690
So UNION ALL says okay, I&#39;m going to leave those duplicates in.

59
00:03:09,690 --> 00:03:14,840
So I&#39;m not going to have that additional workload, I&#39;m going to leave the duplicates

60
00:03:14,840 --> 00:03:15,280
in

61
00:03:15,280 --> 00:03:19,780
and then I would see, oh look, I&#39;ve got  John Smith the employee and I&#39;ve got John Smith the

62
00:03:19,780 --> 00:03:20,470
customer.

63
00:03:20,470 --> 00:03:23,500
They&#39;re the same person, I see them twice. 

64
00:03:23,500 --> 00:03:27,410
It may well be that want that as well. I want to know how many employees

65
00:03:27,410 --> 00:03:31,579
and customers I have. So I may have a column

66
00:03:31,579 --> 00:03:34,940
actually says whether this is a customer or employee and someone who&#39;s

67
00:03:34,940 --> 00:03:38,540
both should appear twice. You say may have, I think when we do these, you quite

68
00:03:38,540 --> 00:03:41,989
often do. You have a literal column, so we would add in there to what we&#39;ve got there

69
00:03:41,989 --> 00:03:42,359
 

70
00:03:42,359 --> 00:03:46,799
after the city, we would put comma, and then we would put employee in speech marks &lt;single quotation marks&gt;

71
00:03:46,799 --> 00:03:50,620
as type maybe. We could have, otherwise you don&#39;t know which one they

72
00:03:50,620 --> 00:03:53,250
came from, and it&#39;s quite useful to have that information.

73
00:03:53,250 --> 00:03:56,639
And so then we get to a situation saying well

74
00:03:56,639 --> 00:04:01,410
if we had a scenario with the venn diagram I have there, I know there&#39;s no duplicates 

75
00:04:01,410 --> 00:04:03,440
because they don&#39;t overlap.

76
00:04:03,440 --> 00:04:06,819
Right. We cannot have employee who is also a customer.

77
00:04:06,819 --> 00:04:12,010
They are separate entities, and I need treat them, and if there is such a case, I need

78
00:04:12,010 --> 00:04:14,349
to treat them separately. They are distinct things,

79
00:04:14,349 --> 00:04:17,400
they are separate. Does it matter?

80
00:04:17,399 --> 00:04:22,139
Well, I guess from a functional perspective no, but

81
00:04:22,139 --> 00:04:25,800
if we know there&#39;s no duplicates anyway, why would I have

82
00:04:25,800 --> 00:04:29,280
the querying engine do the additional work to remove the duplicates. I might as well

83
00:04:29,280 --> 00:04:32,500
use a UNION ALL. Precisely. I mean I think

84
00:04:32,500 --> 00:04:36,620
in practice actually, you probably see more unions than you should

85
00:04:36,620 --> 00:04:39,950
because it&#39;s easy, and it works,

86
00:04:39,950 --> 00:04:43,710
and it does everything you need.  And actually the results you get are the correct results.

87
00:04:43,710 --> 00:04:47,680
The thing is if you know there&#39;s no duplicates in there and

88
00:04:47,680 --> 00:04:52,130
you&#39;re running it as a UNION, yes it works. And you can say well the results of my query are exactly

89
00:04:52,130 --> 00:04:53,840
the results, there&#39;s no extra rows, there&#39;s no 

90
00:04:53,840 --> 00:04:59,210
rows missing, it&#39;s perfect. But if you think about logically what it&#39;s doing

91
00:04:59,210 --> 00:05:02,600
under the covers, it&#39;s having to do that quite complex

92
00:05:02,600 --> 00:05:05,930
search to go and check for duplicates and we don&#39;t need to do it.

93
00:05:05,930 --> 00:05:09,870
Okay. So UNION ALL should probably be your starting point

94
00:05:09,870 --> 00:05:14,540
and then thinking well actually maybe I need to go and do a UNION here because I do need 

95
00:05:14,540 --> 00:05:15,530
to check

96
00:05:15,530 --> 00:05:19,560
just in case. In the same way you don&#39;t tend to type SELECT DISTINCT,

97
00:05:19,560 --> 00:05:25,700
you should probably veer towards writing UNION ALL as you default

98
00:05:25,700 --> 00:05:30,030
start point. Right. And nd then you won&#39;t be tempted into accidentally writing unions.

99
00:05:30,030 --> 00:05:31,510
The problem is they work.

100
00:05:31,510 --> 00:05:34,740
Right, but with the overhead. Okay. 

101
00:05:34,740 --> 00:05:38,310
So we have a few guidelines.

102
00:05:38,310 --> 00:05:43,870
We can&#39;t have any queries or can&#39;t say well his two queries, I&#39;d like to combine

103
00:05:43,870 --> 00:05:45,140
the output from the two.

104
00:05:45,140 --> 00:05:48,180
So we have a couple guidelines for this.

105
00:05:48,180 --> 00:05:51,610
The first thing we&#39;ve got there is aliases.

106
00:05:51,610 --> 00:05:56,160
So we have multiple columns

107
00:05:56,160 --> 00:06:00,480
and those columns are referring to data that is in two or 

108
00:06:00,480 --> 00:06:04,830
three or four or more tables. Now because of that

109
00:06:04,830 --> 00:06:08,950
we may well want to alias them. I might not want to have it being called

110
00:06:08,950 --> 00:06:12,690
customer name for example because it&#39;s now got employee information in there as well.

111
00:06:12,690 --> 00:06:15,940
So what I would like to do

112
00:06:15,940 --> 00:06:20,390
is have an alias for that data. Now the alias, the key thing is,

113
00:06:20,390 --> 00:06:24,580
the alias is only defined in that first query. So

114
00:06:24,580 --> 00:06:27,880
when I&#39;ve got a query UNION another query

115
00:06:27,880 --> 00:06:31,540
the aliases in the second and subsequent queries

116
00:06:31,540 --> 00:06:35,910
basically are ignored. It&#39;s all down to the first query. So you set

117
00:06:35,910 --> 00:06:39,880
all of the aliases up in the first one. And then in the subsequent ones

118
00:06:39,880 --> 00:06:43,600
you don&#39;t need to worry about it at all. In fact if you did put in anything, it will  ignore it.

119
00:06:43,600 --> 00:06:44,760
So that&#39;s all done

120
00:06:44,760 --> 00:06:48,370
in the first one. So if I had an expression for example which I would obviously want to

121
00:06:48,370 --> 00:06:50,320
have a alias as otherwise there wouldn&#39;t be a column name,

122
00:06:50,320 --> 00:06:53,740
I only need to have the as clause  in the  first query and then subsequently

123
00:06:53,740 --> 00:07:00,740
it&#39;s going to inherit the alias. Absolutely.  Number of columns.

124
00:07:01,000 --> 00:07:04,090
We can&#39;t have a query with three columns

125
00:07:04,090 --> 00:07:09,140
UNION and then a query of four columns. It has to have the same number of columns.

126
00:07:09,140 --> 00:07:13,010
If you do you have that data, then you can put

127
00:07:13,010 --> 00:07:16,860
an explicit value or a null value in there.  So you can enter a null

128
00:07:16,860 --> 00:07:21,180
and that work, you can put an explicit value in and that would work. As long as we&#39;re matching them

129
00:07:21,180 --> 00:07:21,950
up

130
00:07:21,950 --> 00:07:26,310
they have to end up being the same. And the final thing

131
00:07:26,310 --> 00:07:31,840
is they need to be approximately similar datatypes. So there&#39;s things that we can do

132
00:07:31,840 --> 00:07:35,630
an implicit conversion. So we&#39;re not saying you can&#39;t have a

133
00:07:35,630 --> 00:07:38,990
smallint and an int. You know

134
00:07:38,990 --> 00:07:42,560
that would be happy with that, it can do that implicit conversion.

135
00:07:42,560 --> 00:07:46,080
But if you start having numbers and you start having text,

136
00:07:46,080 --> 00:07:49,790
then you need to do an explicit conversion. And you need to use

137
00:07:49,790 --> 00:07:53,470
CONVERT or CAST to get them into the same datatype first.

138
00:07:53,470 --> 00:07:57,280
And then you can then UNION them. But make sure that it&#39;s

139
00:07:57,280 --> 00:08:01,920
essentially similar queries really is what you need to generate

140
00:08:01,920 --> 00:08:03,640
to UNION that data.

