0
00:00:01,000 --> 00:00:03,590
The axioms of probability are mathematical

1
00:00:03,590 --> 00:00:06,410
rules that probability must satisfy.

2
00:00:06,410 --> 00:00:10,070
The first axiom of probability is that probability of an event

3
00:00:10,070 --> 00:00:11,700
is at least zero.

4
00:00:11,700 --> 00:00:16,190
This means that the smallest that a probability can ever be is zero.

5
00:00:16,190 --> 00:00:18,800
Well, this axiom says nothing about how large

6
00:00:18,800 --> 00:00:20,900
the probability of an event can be,

7
00:00:20,900 --> 00:00:24,240
it does eliminate the possibility of negative probabilities.

8
00:00:24,240 --> 00:00:27,420
It reflects notion that the smallest probability reserve for

9
00:00:27,420 --> 00:00:29,230
impossible events is zero.

10
00:00:30,330 --> 00:00:33,815
The second axiom of probability is that the probability of an entire

11
00:00:33,815 --> 00:00:35,880
sample space is one.

12
00:00:35,880 --> 00:00:40,410
The chance of something in that outcome space occurs is 100% because

13
00:00:40,410 --> 00:00:44,440
the outcome space contains every possible outcome.

14
00:00:44,440 --> 00:00:48,015
By itself, this axiom does not set an upper limit on the probabilities

15
00:00:48,015 --> 00:00:49,570
of an event and last of course,

16
00:00:49,570 --> 00:00:52,570
there is only one event that makes up that sample space.

17
00:00:52,570 --> 00:00:57,178
But it does say with certainty that something in the sample space will

18
00:00:57,178 --> 00:00:57,738
occur.

19
00:00:57,738 --> 00:01:03,660
Axiom 3 deals with the probability of mutually exclusive events.

20
00:01:03,660 --> 00:01:07,020
If A and B are mutually exclusive or disjoint,

21
00:01:07,020 --> 00:01:10,290
meaning that they share no outcomes, the probability that either of

22
00:01:10,290 --> 00:01:14,880
the events happens is the sum of the probabilities that each happens.

23
00:01:14,880 --> 00:01:19,140
And finally, axiom 4 is really the probability of a compliment.

24
00:01:19,140 --> 00:01:22,200
The fact in what it means is that compliment

25
00:01:22,200 --> 00:01:25,280
is one minus the probability of the event.

26
00:01:25,280 --> 00:01:27,400
This is known as the law of compliments,

27
00:01:27,400 --> 00:01:29,890
which we will discuss in more detail in the next lesson.

