0
00:00:00,950 --> 00:00:03,838
In this video we'll introduce you to the law of total probability which

1
00:00:03,838 --> 00:00:05,822
is critical to understanding Bayes' theorem.

2
00:00:05,822 --> 00:00:08,390
And again, Bayes' theorem is probably the single

3
00:00:08,390 --> 00:00:11,318
most important topic you can learn about in probability,

4
00:00:11,318 --> 00:00:14,950
cuz it explains how we learn about the world by observing things.

5
00:00:14,950 --> 00:00:17,990
So, the law of total probability is the idea that we can

6
00:00:17,990 --> 00:00:20,590
find the chance or probability of an event,

7
00:00:20,590 --> 00:00:24,070
by adding up all the mutually exclusive ways an event can happen.

8
00:00:24,070 --> 00:00:26,450
Remember, mutually exclusive means basically A and

9
00:00:26,450 --> 00:00:31,790
B are mutually exclusive, if A occurs then B can't occur, etc.

10
00:00:31,790 --> 00:00:33,906
So let's consider the following problem.

11
00:00:33,906 --> 00:00:37,253
Cars have flood damage or they don't.

12
00:00:37,253 --> 00:00:39,801
Cars have the engines fail or they don't.

13
00:00:39,801 --> 00:00:44,248
So let's suppose 5% of all cars have flood damage, and if they have flood

14
00:00:44,248 --> 00:00:47,934
damage, 80% of them have their engines eventually fail.

15
00:00:47,934 --> 00:00:52,580
95% of the cars don't have flood damage, and in that case,

16
00:00:52,580 --> 00:00:55,930
only 10% of the engines eventually fail.

17
00:00:55,930 --> 00:00:59,650
So the question is, what fraction of cars have their engine fail?

18
00:00:59,650 --> 00:01:01,470
Well, how can a car have its engine fail?

19
00:01:01,470 --> 00:01:04,810
It could have its engine fail and be a car with flood damage, or

20
00:01:04,810 --> 00:01:07,900
have its engine fail, and be a car without flood damage.

21
00:01:07,900 --> 00:01:10,485
Those are mutually exclusive, because in one case,

22
00:01:10,485 --> 00:01:13,191
the car had flood damage, in the other case it didn't.

23
00:01:13,191 --> 00:01:18,283
So if I want the probability an engine fails,

24
00:01:18,283 --> 00:01:24,045
it's the probability it can fail with flood damage

25
00:01:24,045 --> 00:01:31,150
plus the probability it could fail with no prior flood damage.

26
00:01:33,030 --> 00:01:35,400
Now let's try and figure out what probabilities we're given.

27
00:01:35,400 --> 00:01:37,960
You always wanna do this in a probability problem.

28
00:01:37,960 --> 00:01:39,860
Write down the probabilities you're given.

29
00:01:39,860 --> 00:01:42,309
Well, we have a 5% chance of flood damage, so

30
00:01:42,309 --> 00:01:44,163
5% of the cars have flood damage.

31
00:01:44,163 --> 00:01:48,044
And NFL doesn't mean National Football League, no flood damage,

32
00:01:48,044 --> 00:01:50,367
95% of the cars have no flood damage.

33
00:01:50,367 --> 00:01:53,554
And then, we have basically given there was flood damage,

34
00:01:53,554 --> 00:01:55,476
how likely it is we saw a failed car?

35
00:01:55,476 --> 00:01:57,265
80% chance.

36
00:01:57,265 --> 00:02:00,670
Given the car had flood damage, it's very likely the engine fails.

37
00:02:00,670 --> 00:02:02,394
Given the car didn't have flood damage,

38
00:02:02,394 --> 00:02:04,290
a 10% chance the engine fails.

39
00:02:04,290 --> 00:02:06,540
Now, where does conditional probability come in?

40
00:02:06,540 --> 00:02:08,494
See, we want A and B here.

41
00:02:08,494 --> 00:02:11,077
The A being the event that we have flood damage,

42
00:02:11,077 --> 00:02:13,024
B being the event the engine fails.

43
00:02:13,024 --> 00:02:17,614
And so, we can find the probability of two events occurring by taking

44
00:02:17,614 --> 00:02:21,891
the probability of one times the conditional probability given

45
00:02:21,891 --> 00:02:22,617
that one.

46
00:02:22,617 --> 00:02:25,920
So we start with the flood damage, because that's what we're given.

47
00:02:25,920 --> 00:02:29,094
So I want the probability we failed with flood damage, I take

48
00:02:29,094 --> 00:02:32,642
the probability of flood damage, and then I can multiply that times

49
00:02:32,642 --> 00:02:36,332
the probability that basically it would fail given the flood damage.

50
00:02:36,332 --> 00:02:38,976
Cuz I have all that information.

51
00:02:38,976 --> 00:02:41,160
So, that's the first part.

52
00:02:41,160 --> 00:02:44,120
Now, the second part, how can you fail with no flood damage?

53
00:02:44,120 --> 00:02:46,600
Well, you take the probability of no flood damage,

54
00:02:46,600 --> 00:02:48,580
because I know what that is.

55
00:02:48,580 --> 00:02:50,880
And once I know that happened,

56
00:02:50,880 --> 00:02:54,510
there's not much chance of the engine failing.

57
00:02:54,510 --> 00:02:57,180
I can say fail given no flood.

58
00:02:57,180 --> 00:03:00,210
And I've got all this stuff I just need to plug in.

59
00:03:00,210 --> 00:03:02,515
See, that's the law of total probability.

60
00:03:02,515 --> 00:03:06,570
Cuz the event of the engine failing is the total of these two

61
00:03:06,570 --> 00:03:10,700
probabilities, which I can compute with conditional probability.

62
00:03:10,700 --> 00:03:14,110
So what we can do here is take the probability of flood damage.

63
00:03:14,110 --> 00:03:19,708
And so that's going to be 0.05.

64
00:03:19,708 --> 00:03:24,148
Fail given flood damage is gonna be 0.8.

65
00:03:24,148 --> 00:03:27,736
Then the probability of no flood damage is 0.95.

66
00:03:30,105 --> 00:03:33,861
Fail given no flood damage is 0.1.

67
00:03:33,861 --> 00:03:36,633
We'll show you that formula.

68
00:03:36,633 --> 00:03:38,905
So that's 13.5%.

69
00:03:38,905 --> 00:03:41,496
And that is your answer, okay.

70
00:03:41,496 --> 00:03:44,600
So, 13.5%.

71
00:03:44,600 --> 00:03:48,510
Now, another way to look at this is what is called a contingency table.

72
00:03:48,510 --> 00:03:51,133
Every car falls into one of four categories.

73
00:03:51,133 --> 00:03:53,130
Flood damage and the engine fails.

74
00:03:53,130 --> 00:03:56,086
Flood damage and the engine doesn't fail.

75
00:03:56,086 --> 00:03:58,610
No flood damage, the engine fails.

76
00:03:58,610 --> 00:04:00,530
No flood damage and the engine doesn't fail.

77
00:04:00,530 --> 00:04:03,740
We can figure out all of these probabilities by using conditional

78
00:04:03,740 --> 00:04:04,720
probability.

79
00:04:04,720 --> 00:04:07,025
So, what fraction of the cars fall in here?

80
00:04:07,025 --> 00:04:10,220
Well, you would take the fraction of cars that flood,

81
00:04:10,220 --> 00:04:14,050
times given they flood, what's the chance that they'll fail?

82
00:04:14,050 --> 00:04:15,170
80%.

83
00:04:17,344 --> 00:04:19,088
So, 0.04.

84
00:04:19,088 --> 00:04:22,570
What's the chance that they will have flood damage, and not fail?

85
00:04:22,570 --> 00:04:26,913
Well, 5% of them flood, and the chance, okay,

86
00:04:26,913 --> 00:04:31,471
they won't fail given flood, is going to be 0.2.

87
00:04:31,471 --> 00:04:34,913
Cuz the chance they'd fail given flood is 0.8.

88
00:04:34,913 --> 00:04:37,900
Now, these probabilities all have to add up to 1 eventually.

89
00:04:37,900 --> 00:04:40,660
Notice those add up to 5%, which is good.

90
00:04:40,660 --> 00:04:42,570
Now, the chance of no flood and failing.

91
00:04:42,570 --> 00:04:47,631
Well, what fraction will have no flood damage, that's 95%.

92
00:04:47,631 --> 00:04:52,640
And then what's the chance they'll fail given no flood, 0.1.

93
00:04:52,640 --> 00:04:56,161
Okay, that's our 13.5% right there, by the way.

94
00:04:56,161 --> 00:04:57,551
Another way to see it.

95
00:04:57,551 --> 00:05:00,297
Now, the probabilities have to all add to 1, but, so

96
00:05:00,297 --> 00:05:02,825
I could figure this out by taking 1 minus the sum.

97
00:05:02,825 --> 00:05:06,410
But no flood and not fail, no flood is 0.95,

98
00:05:06,410 --> 00:05:08,600
this will be most of the cars.

99
00:05:10,160 --> 00:05:15,470
And not failing, okay, not failing given no flood.

100
00:05:15,470 --> 00:05:21,107
Failing given no flood is 0.01, so not failing is 1 minus that, 0.09.

101
00:05:21,107 --> 00:05:25,083
Now, these all add up to 1, cuz one of these events must happen for

102
00:05:25,083 --> 00:05:25,800
every car.

103
00:05:26,940 --> 00:05:30,960
But there's your 13.5%, as we got before.

104
00:05:30,960 --> 00:05:34,505
Now, you'll see how important this is when we talk about Bayes' theorem

105
00:05:34,505 --> 00:05:35,658
in the next two videos.

106
00:05:35,658 --> 00:05:39,053
Basically, if you understand the law of total probability and

107
00:05:39,053 --> 00:05:41,552
the definition of conditional probability,

108
00:05:41,552 --> 00:05:43,291
then you know Bayes' theorem.

109
00:05:43,291 --> 00:05:45,727
You don't even need to talk about Bayes' theorem.

110
00:05:45,727 --> 00:05:47,310
But it's critically important, and

111
00:05:47,310 --> 00:05:49,070
that's what we'll start on in the next video.

