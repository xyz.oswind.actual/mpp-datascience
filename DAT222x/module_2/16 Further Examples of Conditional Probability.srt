0
00:00:00,950 --> 00:00:03,880
In this video, we're gonna further our understanding of conditional

1
00:00:03,880 --> 00:00:06,210
probability by solving two interesting problems.

2
00:00:06,210 --> 00:00:08,460
So let's suppose you throw two dice,

3
00:00:08,460 --> 00:00:10,888
remember, 1 through 6 are equally likely on each dice.

4
00:00:10,888 --> 00:00:15,850
And we wanna know, if event A is the chance we get at least one 6, and

5
00:00:15,850 --> 00:00:18,050
event B is the event we get a total of 10,

6
00:00:18,050 --> 00:00:20,779
what's the probability of B given A?

7
00:00:20,779 --> 00:00:24,510
Okay, so what we need to get is the probability of A and B for

8
00:00:24,510 --> 00:00:25,170
the numerator.

9
00:00:27,200 --> 00:00:28,160
Now, what does that mean?

10
00:00:29,330 --> 00:00:32,370
That would mean we get at least one 6 and a total of 10.

11
00:00:32,370 --> 00:00:35,957
Now, I've highlighted here in xs everything with a 6.

12
00:00:35,957 --> 00:00:39,270
So to get a total of 10, if you get at least one 6,

13
00:00:39,270 --> 00:00:42,600
you're gonna have to get a 6 and a 4 or a 4 and a 6.

14
00:00:42,600 --> 00:00:44,710
If you got at least one 6.

15
00:00:44,710 --> 00:00:54,080
So the probability of A and B there is going to be, 2/36.

16
00:00:54,080 --> 00:00:56,180
Now, we want the probability of A and

17
00:00:56,180 --> 00:00:59,300
B divided by the probability of A.

18
00:00:59,300 --> 00:01:01,160
Okay, that's our definition there.

19
00:01:01,160 --> 00:01:01,990
I should write that down.

20
00:01:01,990 --> 00:01:09,430
So the probability of B given A, Is the probability of A and

21
00:01:09,430 --> 00:01:13,700
B, Divided by the probability of A.

22
00:01:14,870 --> 00:01:17,120
I've got the probability of A and B.

23
00:01:17,120 --> 00:01:18,680
Now, what's the probability of A?

24
00:01:20,580 --> 00:01:24,059
Well, the probability of A, okay, is at least one 6,

25
00:01:24,059 --> 00:01:25,900
those are the xs here.

26
00:01:25,900 --> 00:01:29,004
Now, you might think there's 12 points there, but there's 11,

27
00:01:29,004 --> 00:01:30,990
cuz you don't wanna double-count this 6.

28
00:01:30,990 --> 00:01:34,550
Okay, so you got 6 here and 6 here, but you're counting the 6-6 twice.

29
00:01:34,550 --> 00:01:37,234
So that would be 11/36.

30
00:01:40,480 --> 00:01:48,766
And so our answer would be 2/36, Divided by 11/36,

31
00:01:48,766 --> 00:01:53,405
and that's gonna be two-elevenths.

32
00:01:54,877 --> 00:01:57,180
So that's the answer to that problem.

33
00:01:57,180 --> 00:01:58,190
Now let's try this problem.

34
00:01:59,300 --> 00:02:03,410
Draw two cards without replacement, and we want even A to be the event

35
00:02:03,410 --> 00:02:07,080
the first card is an ace, event B the second card is an ace.

36
00:02:07,080 --> 00:02:10,586
So without replacement means, when you take the first card out,

37
00:02:10,586 --> 00:02:12,000
you don't put it back in.

38
00:02:12,000 --> 00:02:14,973
Of course, if you put the first card back in, and assume you

39
00:02:14,973 --> 00:02:19,160
shuffled the deck, then what happens on the first card doesn't matter.

40
00:02:19,160 --> 00:02:22,620
So you want the probability of B given A in this case.

41
00:02:22,620 --> 00:02:24,529
So let's apply our definition.

42
00:02:25,912 --> 00:02:28,247
That's the probability of A and

43
00:02:28,247 --> 00:02:34,820
B, Divided by the probability of A, okay.

44
00:02:34,820 --> 00:02:37,201
Actually, we don't even need that here, it turns out.

45
00:02:37,201 --> 00:02:39,744
Cuz if I tell you the first card's an ace,

46
00:02:39,744 --> 00:02:42,980
what's the chance the second card's an ace?

47
00:02:42,980 --> 00:02:45,225
We could do this formula, but we actually don't need it.

48
00:02:45,225 --> 00:02:47,817
Cuz, okay, if we're given that the first cards an ace,

49
00:02:47,817 --> 00:02:49,820
what happens after the first card?

50
00:02:49,820 --> 00:02:53,010
You have a new deck, if you think about it.

51
00:02:54,430 --> 00:02:58,850
And it's got 51 cards, and there's three aces, cuz you took one out.

52
00:03:00,840 --> 00:03:05,490
So the answer's just gonna be, 3/51,

53
00:03:05,490 --> 00:03:09,004
which is one-seventeenth.

54
00:03:09,004 --> 00:03:12,897
Originally, it was one-thirteenth, the chance of an ace, 4 over 52.

55
00:03:12,897 --> 00:03:16,240
And it drops after you've taken the first card out, it's an ace.

56
00:03:16,240 --> 00:03:18,750
So those were two simple problems which use

57
00:03:18,750 --> 00:03:21,380
the definition of conditional probability.

58
00:03:21,380 --> 00:03:24,835
Now, what we're driving for to close this module is an understanding of

59
00:03:24,835 --> 00:03:27,670
Bayes theorem, which is one of the most important concepts

60
00:03:27,670 --> 00:03:29,050
in probability and statistics.

61
00:03:29,050 --> 00:03:31,900
Cuz it shows you how people learn about the world and

62
00:03:31,900 --> 00:03:34,280
update their views of probabilities.

63
00:03:34,280 --> 00:03:36,770
And basically, before we can learn Bayes theorem,

64
00:03:36,770 --> 00:03:39,720
we need to understand the law of total probability.

65
00:03:39,720 --> 00:03:41,690
And that's what we're gonna discuss in the next video.

