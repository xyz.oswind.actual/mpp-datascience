0
00:00:00,710 --> 00:00:05,069
Remember our medical tests that was 95% accurate for those with the disease,

1
00:00:05,130 --> 00:00:08,977
97% accurate for those without and only 1% of the population

2
00:00:08,980 --> 00:00:10,515
having this particular disease.

3
00:00:11,040 --> 00:00:14,237
Let's change our question a little bit, What is the probability

4
00:00:14,333 --> 00:00:17,450
that you have the disease, if you get a positive test?

5
00:00:17,730 --> 00:00:21,122
This is a Bayesian question. What is the probability of something

6
00:00:21,170 --> 00:00:22,749
given something else?

7
00:00:23,303 --> 00:00:25,493
Let's consider the possible outcomes.

8
00:00:25,570 --> 00:00:27,392
This table shows what can happen,

9
00:00:27,608 --> 00:00:30,284
you can have the disease and the test is positive

10
00:00:30,355 --> 00:00:31,845
that's a true positive.

11
00:00:32,168 --> 00:00:34,802
You don't have the disease and the test is negative

12
00:00:34,868 --> 00:00:36,340
that is a true negative.

13
00:00:36,780 --> 00:00:41,001
You can have the disease and the test is negative, this is a false negative,

14
00:00:41,365 --> 00:00:45,300
you don't have the disease but the test is positive, a false positive.

15
00:00:45,740 --> 00:00:50,111
Now let's take a look at what this means in our medical test example.

16
00:00:50,200 --> 00:00:53,370
1% people have the disease,

17
00:00:53,434 --> 00:00:56,962
if you already have the disease you're in the first column.

18
00:00:57,330 --> 00:01:01,407
There's a 95% chance you will test positive, there's a 5% chance

19
00:01:01,511 --> 00:01:02,740
you will test negative.

20
00:01:03,330 --> 00:01:06,491
If you don't have the disease you're in the second column,

21
00:01:06,779 --> 00:01:12,071
there's a 3% chance you will test positive and a 97% chance you will test negative.

22
00:01:12,850 --> 00:01:17,840
Now, suppose you get a positive test result, What are the chances you have

23
00:01:17,905 --> 00:01:21,380
Cancer? 95%, 99%, 1%.

24
00:01:21,860 --> 00:01:25,306
It means we're somewhere in the top row of our table,

25
00:01:25,623 --> 00:01:28,874
it could be a true positive or a false positive.

26
00:01:29,223 --> 00:01:32,888
The chance of the true positives equal to the chance you have the disease

27
00:01:33,000 --> 00:01:35,400
multiplied by the chance the test caught it.

28
00:01:35,700 --> 00:01:40,974
In this case, 1% times 95% which is 0.95%.

29
00:01:41,700 --> 00:01:45,075
The chance of a false positive is equal to the chance you

30
00:01:45,108 --> 00:01:48,465
don't have the disease multiplied by the chance to test caught it.

31
00:01:48,470 --> 00:01:54,382
Anyway, in this case 99% times 3%, which is 2.97%.

32
00:01:55,223 --> 00:02:00,112
The chance of getting a real positive result is 0.95%,

33
00:02:00,586 --> 00:02:03,561
the chance of getting any type of positive result

34
00:02:03,610 --> 00:02:07,440
isn't chance of a true positive plus the chance of a false positive.

35
00:02:07,770 --> 00:02:13,537
0.95% plus 2.97%, which is 3.92%.

36
00:02:14,040 --> 00:02:16,838
So, if you get a positive result,

37
00:02:16,850 --> 00:02:22,186
your chance of having the disease is 0.95%, the real positive result

38
00:02:22,518 --> 00:02:27,090
divided by 3.92% the total possible positive results,

39
00:02:27,404 --> 00:02:32,151
which is about 24%. But this test is very accurate

40
00:02:32,171 --> 00:02:34,642
on both the positive and negative side.

41
00:02:34,891 --> 00:02:38,678
That's actually unusual in medicine, let's take a look at an example

42
00:02:38,755 --> 00:02:41,400
that might be a bit more realistic.

