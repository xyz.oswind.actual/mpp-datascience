0
00:00:00,640 --> 00:00:02,690
So, when we wanna solve probability problems,

1
00:00:02,690 --> 00:00:04,500
we often have to use formulas or rules.

2
00:00:04,500 --> 00:00:07,848
So, we're gonna start learning about some important rules that help you

3
00:00:07,848 --> 00:00:10,840
solve some fairly complicated probability problems.

4
00:00:10,840 --> 00:00:13,520
So, the first rule we're gonna talk about is the law of complements.

5
00:00:13,520 --> 00:00:15,112
So, what do we mean by complements?

6
00:00:15,112 --> 00:00:18,220
Well, we can have the event A and the event Not A.

7
00:00:18,220 --> 00:00:21,720
In other words, Not A includes everything that doesn't include A.

8
00:00:21,720 --> 00:00:22,730
Those are complements.

9
00:00:22,730 --> 00:00:24,530
Together, they include the whole sample space.

10
00:00:24,530 --> 00:00:26,890
So their probabilities add up to 1.

11
00:00:26,890 --> 00:00:31,743
The probability of an event A plus the probability of Not A = 1.

12
00:00:31,743 --> 00:00:35,063
And if you solve for Not A by subtracting probability of A from

13
00:00:35,063 --> 00:00:37,680
both sides, you get this key formula.

14
00:00:37,680 --> 00:00:40,840
The probability of Not A occurring

15
00:00:40,840 --> 00:00:43,540
is 1 minus the probability of A occurring.

16
00:00:43,540 --> 00:00:45,160
Now, why is this useful?

17
00:00:45,160 --> 00:00:48,639
Because it may be if you wanna find the probability of Not A,

18
00:00:48,639 --> 00:00:51,221
you could find the probability of A easier.

19
00:00:51,221 --> 00:00:54,340
And then you have the probability of Not A by this formula.

20
00:00:54,340 --> 00:00:56,560
So let's look at three examples of this.

21
00:00:56,560 --> 00:00:58,990
Okay, so here we have a sample space.

22
00:00:58,990 --> 00:01:01,960
You can see there are 16 squares here, it's 4 by 4.

23
00:01:01,960 --> 00:01:04,460
Assume they each have the same probability.

24
00:01:04,460 --> 00:01:07,125
Event A is yellow, the yellow squares, and

25
00:01:07,125 --> 00:01:08,933
event B are the blue squares.

26
00:01:08,933 --> 00:01:12,182
And that's Not A, because everything is either yellow or blue, so

27
00:01:12,182 --> 00:01:13,660
these are complements.

28
00:01:13,660 --> 00:01:18,503
So if I want the probability of Not A in this example,

29
00:01:18,503 --> 00:01:20,602
probability of Not A.

30
00:01:22,811 --> 00:01:25,420
Well, that would be 1 minus the probability of A.

31
00:01:25,420 --> 00:01:27,460
Now what's the probability of A, the yellow?

32
00:01:27,460 --> 00:01:30,150
Well, I can count the yellow easier than the blue.

33
00:01:30,150 --> 00:01:34,771
So the probability of A here would be 5/16, cuz there are five yellow.

34
00:01:38,378 --> 00:01:41,324
So the probability of Not A would simply be 1 minus

35
00:01:41,324 --> 00:01:42,668
the probability of A.

36
00:01:45,788 --> 00:01:50,039
And that would equal 11/16, which you can see is right

37
00:01:50,039 --> 00:01:54,130
because there are 11 blue squares in there out of 16.

38
00:01:54,130 --> 00:01:56,210
Let's look at another example.

39
00:01:56,210 --> 00:01:58,082
Suppose you throw two dice,

40
00:01:58,082 --> 00:02:01,208
we talked about this briefly in the last video.

41
00:02:01,208 --> 00:02:04,430
What's the chance you do not throw a total of 2?

42
00:02:04,430 --> 00:02:05,950
Okay, so not throwing a total of 2,

43
00:02:05,950 --> 00:02:08,380
there's lots of ways to not throw a 2.

44
00:02:08,380 --> 00:02:11,374
But there's only one way to throw a 2.

45
00:02:11,374 --> 00:02:14,810
Okay, see the only way you can get a 2 is with a 1 and a 1.

46
00:02:14,810 --> 00:02:17,700
Here are your 36 points in the sample space.

47
00:02:17,700 --> 00:02:20,885
So basically the probability of A,

48
00:02:20,885 --> 00:02:24,504
which is a total of 2, is 1 out of 36.

49
00:02:24,504 --> 00:02:30,805
So now the probability of Not A, which is everything but that X,

50
00:02:30,805 --> 00:02:36,654
is simply going to be 1- 1/36, which is 35/36.

51
00:02:36,654 --> 00:02:40,189
See, there it was much easier to use the law or rule of complements,

52
00:02:40,189 --> 00:02:44,310
than basically to count up everything in here that wasn't in X.

53
00:02:44,310 --> 00:02:47,630
So one final example here in this video.

54
00:02:47,630 --> 00:02:51,170
The probability at least 1 child is a girl if you have 3 children.

55
00:02:51,170 --> 00:02:54,420
Okay, there's lots of ways at least one child can be a girl.

56
00:02:54,420 --> 00:02:57,878
So A is the event at least one child is a girl

57
00:03:01,407 --> 00:03:03,822
So Not A, what's the complement of that?

58
00:03:03,822 --> 00:03:06,750
Either you have at least one girl, or you have No Girls.

59
00:03:08,590 --> 00:03:12,644
Well, No Girls is easy, because No Girls is boy, boy, boy.

60
00:03:14,990 --> 00:03:16,439
That's easy to figure out.

61
00:03:19,786 --> 00:03:25,540
So the probability of No Girls is 1/8.

62
00:03:25,540 --> 00:03:28,751
And so by the rule of complements,

63
00:03:28,751 --> 00:03:34,194
the probability of at least one girl would be 1- 1/8.

64
00:03:36,457 --> 00:03:37,759
Or 7/8.

65
00:03:37,759 --> 00:03:40,762
Now, you could have figured it out by counting everything here that has

66
00:03:40,762 --> 00:03:41,950
at least one girl.

67
00:03:41,950 --> 00:03:44,010
There'd be seven of those points in the sample space, but

68
00:03:44,010 --> 00:03:48,660
it was much easier to find the boy, boy, boy and subtract that from 1.

69
00:03:48,660 --> 00:03:51,920
So in the next video, we'll learn another important rule for

70
00:03:51,920 --> 00:03:53,590
computing probabilities.

71
00:03:53,590 --> 00:03:57,471
The rules involving mutually exclusive events, and also finding

72
00:03:57,471 --> 00:04:01,708
the probability of A or B, even when events are not mutually exclusive.

