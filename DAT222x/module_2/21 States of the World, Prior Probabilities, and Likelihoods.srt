0
00:00:00,660 --> 00:00:03,510
I can't overemphasize the importance of Bayes Theorem.

1
00:00:03,510 --> 00:00:07,120
You may have heard if you read websites like 538.com or

2
00:00:07,120 --> 00:00:09,630
the upshot of the New York Times,

3
00:00:09,630 --> 00:00:12,160
they'll talk about the Bayesian view of the world.

4
00:00:12,160 --> 00:00:12,850
And, basically,

5
00:00:12,850 --> 00:00:15,520
a lot of statisticians are really strong Bayesian.

6
00:00:15,520 --> 00:00:17,200
So, what does this sort of mean?

7
00:00:17,200 --> 00:00:20,599
We use information to update our view of the world or probabilities.

8
00:00:21,740 --> 00:00:25,384
So, to illustrate Bayes' theorem, we'll take a medical example which

9
00:00:25,384 --> 00:00:28,780
I think is really illustrative of how Bayes' theorem works.

10
00:00:28,780 --> 00:00:31,540
Let's consider a 40 year old woman with no risk factors,

11
00:00:31,540 --> 00:00:33,500
thankfully, for breast cancer.

12
00:00:33,500 --> 00:00:36,269
So, the framework for Bayes' theorem is we have states of the world.

13
00:00:37,390 --> 00:00:40,360
So, either the woman has cancer or she doesn't have cancer.

14
00:00:40,360 --> 00:00:42,480
There could be more states of the world, but

15
00:00:42,480 --> 00:00:44,780
in this case there's two states of the world.

16
00:00:44,780 --> 00:00:47,960
And then we have prior probabilities of the states of the world before we

17
00:00:47,960 --> 00:00:50,890
receive a signal, in this case, the results of a mammogram.

18
00:00:52,050 --> 00:00:54,300
And when you go to the doctor and you tell him you have symptoms,

19
00:00:54,300 --> 00:00:57,200
the doctor really has to deal with Bayes' theorem.

20
00:00:57,200 --> 00:01:01,080
Doctors do tests, they update their probabilities of what you have, and

21
00:01:01,080 --> 00:01:04,290
doctors really don't understand Bayes' theorem very well.

22
00:01:04,290 --> 00:01:07,720
Cuz the example I'm gonna show you was given to doctors to estimate

23
00:01:07,720 --> 00:01:10,930
probability that a woman would have breast cancer

24
00:01:10,930 --> 00:01:12,630
after getting a positive mammogram, and

25
00:01:12,630 --> 00:01:15,680
most doctors completely flubbed the answer.

26
00:01:15,680 --> 00:01:18,340
So, basically, I think doctors really need to

27
00:01:18,340 --> 00:01:20,520
get some training in Bayes' theorem.

28
00:01:20,520 --> 00:01:24,363
A great book, Gilbert Welch of Dartmouth Med School, has a book,

29
00:01:24,363 --> 00:01:26,972
Overdiagnosed, on how we basically have too

30
00:01:26,972 --> 00:01:31,023
many medical tests in this country, and he uses a lot of statistics and

31
00:01:31,023 --> 00:01:33,790
Bayes' theorem in that book, terrific book.

32
00:01:33,790 --> 00:01:34,430
Okay, so

33
00:01:34,430 --> 00:01:37,770
the prior probability is that the states of the world, it turns out or

34
00:01:37,770 --> 00:01:40,890
roughly the following for a woman with no breast cancer was 40.

35
00:01:40,890 --> 00:01:44,195
There's 4 chances in 1000 she has breast cancer, and

36
00:01:44,195 --> 00:01:46,080
99.6% chance she doesn't have cancer.

37
00:01:47,380 --> 00:01:50,430
Okay, so then you get a signal about the state of the world.

38
00:01:50,430 --> 00:01:52,180
You're always gathering information.

39
00:01:52,180 --> 00:01:55,170
Example, when I was a little kid, and maybe you did this,

40
00:01:55,170 --> 00:01:58,010
the burner was on, on the stove, my mother wasn't there.

41
00:01:58,010 --> 00:02:00,270
I put my finger on the stove and I burned it.

42
00:02:00,270 --> 00:02:04,200
That should give you sort of change your probability that the stove is

43
00:02:04,200 --> 00:02:07,130
dangerous and next time you wouldn't put your finger on the stove.

44
00:02:07,130 --> 00:02:10,020
Unfortunately, I put mg finger on the stove a second time and

45
00:02:10,020 --> 00:02:11,830
burned it twice, but then I was convinced.

46
00:02:12,920 --> 00:02:17,220
Some people in my class actually did put their finger on the stove twice.

47
00:02:17,220 --> 00:02:19,660
I was comforted by that fact.

48
00:02:19,660 --> 00:02:22,170
Okay, so the signal here is the mammogram result.

49
00:02:22,170 --> 00:02:24,120
And so, most medical tests, they come back positive or

50
00:02:24,120 --> 00:02:25,480
they come back negative.

51
00:02:25,480 --> 00:02:28,320
But the problem is they're not 100% accurate, but

52
00:02:28,320 --> 00:02:31,840
that's the signal that changes your view of the state of the world, and

53
00:02:31,840 --> 00:02:34,010
in different situations, you get different signals.

54
00:02:34,010 --> 00:02:36,740
Now, what we wanna be able to do is find what's called

55
00:02:36,740 --> 00:02:38,910
the posterior probability,

56
00:02:38,910 --> 00:02:41,890
the probability the woman has cancer given a positive test result.

57
00:02:41,890 --> 00:02:44,940
In other words the test result comes back positive,

58
00:02:44,940 --> 00:02:46,690
the woman would be very worried.

59
00:02:46,690 --> 00:02:48,330
And she should be a little bit worried, but

60
00:02:48,330 --> 00:02:51,180
probably not as worried as most people would think.

61
00:02:51,180 --> 00:02:53,230
Now, in order to get that posterior probability,

62
00:02:53,230 --> 00:02:55,020
we need to know likelihoods.

63
00:02:55,020 --> 00:02:55,930
What are likelihoods?

64
00:02:55,930 --> 00:02:58,500
It's how likely the signal or

65
00:02:58,500 --> 00:03:01,270
test result is given each state of the world.

66
00:03:01,270 --> 00:03:05,170
So, it turns out roughly speaking, given the woman has breast cancer,

67
00:03:05,170 --> 00:03:08,560
the chance of a positive test result is 80%.

68
00:03:08,560 --> 00:03:10,420
Given the women doesn't have cancer,

69
00:03:10,420 --> 00:03:13,100
the chance of a positive test result is much lower, 10%.

70
00:03:13,100 --> 00:03:14,249
So, the question is,

71
00:03:14,249 --> 00:03:17,332
after getting that test result which gives us information,

72
00:03:17,332 --> 00:03:20,122
what is our new estimate or the probability of cancer?

73
00:03:20,122 --> 00:03:22,625
And before you go on to the next video,

74
00:03:22,625 --> 00:03:26,396
write down your answer without doing any calculations.

75
00:03:26,396 --> 00:03:27,940
And if you're like most doctors,

76
00:03:27,940 --> 00:03:31,180
your answer will be completely wrong but you'll have to wait for

77
00:03:31,180 --> 00:03:33,660
the next video to find out what the answer is.

