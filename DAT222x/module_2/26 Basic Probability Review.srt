0
00:00:00,320 --> 00:00:04,840
You probability figured out by now that probability is a really hard subject.

1
00:00:04,860 --> 00:00:07,900
So in this video we've put together five problems to review

2
00:00:07,920 --> 00:00:10,690
the entire probability module so, let's get to it.

3
00:00:10,710 --> 00:00:14,700
So the first problem 40% of students get an A in marketing,

4
00:00:14,720 --> 00:00:18,160
30% get an A in statistic, 10% get an A in both.

5
00:00:18,180 --> 00:00:20,020
What fraction of students do not

6
00:00:20,040 --> 00:00:22,680
get an A in either course and what solve this two ways.

7
00:00:22,700 --> 00:00:28,480
Well probability of No A's is one minus the probability they get at least one A.

8
00:00:28,500 --> 00:00:31,130
Now what's the probability that's the role of complements,

9
00:00:31,150 --> 00:00:33,650
the probability get at least one A is

10
00:00:33,670 --> 00:00:37,300
the chance to get an A in marketing plus A in stats minus probability to

11
00:00:37,320 --> 00:00:43,500
get an A in both and A in marketing is 40% and A in stats as 30%, both is 10%,

12
00:00:43,520 --> 00:00:45,520
subtracted off we get 60%.

13
00:00:45,540 --> 00:00:50,079
So probability at least one A is 60%. So No A's is 40%, but another way

14
00:00:50,090 --> 00:00:51,909
to do this which is probability easier for you.

15
00:00:51,920 --> 00:00:57,540
Okay! Everybody either gets an A in marketing or not in A in marketing

16
00:00:57,560 --> 00:01:00,140
and A in stats or not an A in stats.

17
00:01:00,160 --> 00:01:03,180
So we know A in Marketing and A in stats is 10%

18
00:01:03,200 --> 00:01:03,879
Okay!

19
00:01:03,890 --> 00:01:07,856
Now total A in stats is 30% so this must be 20%

20
00:01:07,870 --> 00:01:11,950
to make 30% at A in stats. Now for the A in Marketing

21
00:01:11,970 --> 00:01:19,152
okay that's 40%, so this must be 30%. So this is 10%, this is 20%, this is 30%

22
00:01:19,170 --> 00:01:22,736
probability is have to add to one this one has got to be 40%

23
00:01:22,750 --> 00:01:24,304
which gives you the same answer.

24
00:01:24,320 --> 00:01:31,136
Problem 2, we toss two dice whether than A, B the event the first die is a three

25
00:01:31,150 --> 00:01:34,960
event be the event the total of six or these events are independent.

26
00:01:34,980 --> 00:01:36,896
Now how do you check for independence

27
00:01:37,160 --> 00:01:41,170
to events are independent if and only if the probability of them both happening

28
00:01:41,190 --> 00:01:44,650
is the products is of individual probabilities. Now what's the chance

29
00:01:44,670 --> 00:01:51,490
A happens one A would be basically 1/6, the first died being at 3 is going to be 1/6

30
00:01:51,510 --> 00:01:53,936
chance because every number has the same chance of coming up.

31
00:01:53,950 --> 00:02:00,656
Now the total being 6 how can you get a 6, 1 with a 5, 2 with the 4 etc 5 over 36.

32
00:02:00,670 --> 00:02:04,576
So probability of A times probability did B is 5 over 216.

33
00:02:04,590 --> 00:02:11,260
Okay! the probability of A and B is 1 over 36,

34
00:02:11,280 --> 00:02:15,136
to get it 3 on the first die in the total 6 you need to have three and three.

35
00:02:15,150 --> 00:02:18,300
So 1 over 36 doesn't equal 5 over 216

36
00:02:18,320 --> 00:02:20,496
so those events are not independent.

37
00:02:20,510 --> 00:02:24,624
But now if you met the event C be the event the total is 7

38
00:02:24,640 --> 00:02:26,800
then events A and C are independent.

39
00:02:26,820 --> 00:02:29,504
Because the probability of event C would be 1/6 now,

40
00:02:29,520 --> 00:02:33,360
there are 6 ways to get a 7 you can match the first die with anything up.

41
00:02:33,380 --> 00:02:36,080
with the number on the second die and the probability if A and C

42
00:02:36,100 --> 00:02:41,020
is still 136. So that probability of A would be 1/6, probability of C 1/6,

43
00:02:41,040 --> 00:02:44,900
1/6th times 1/6 is 136 OK.

44
00:02:44,920 --> 00:02:50,496
Problem 3! A Bowl has three red and five blue balls

45
00:02:50,510 --> 00:02:54,490
we draw two balls without replacement. So without replacement means once you

46
00:02:54,510 --> 00:02:57,600
take a ball out you can't get that same ball again.

47
00:02:57,620 --> 00:03:00,730
What's the chance to get at least one blue ball, when you see at

48
00:03:00,750 --> 00:03:02,560
least greater or equal to one blue ball

49
00:03:02,580 --> 00:03:06,040
you to think compliments. Because there are a lot of ways to get at least one

50
00:03:06,060 --> 00:03:10,192
blue ball will do it both ways the compliments I think is easier.

51
00:03:10,210 --> 00:03:12,704
The  probability is getting at least one blue ball

52
00:03:12,720 --> 00:03:15,310
is one minus the chance of getting no blue balls.

53
00:03:15,330 --> 00:03:18,970
Now what's probability getting no blue balls you need the first one to be red

54
00:03:18,990 --> 00:03:23,110
and the second one to be red the first one is red has a chance 3/8 because

55
00:03:23,130 --> 00:03:26,912
there's three red balls but then once you've got the first red one

56
00:03:26,930 --> 00:03:29,776
you pulled it out there's only two Reds out of 7.

57
00:03:29,790 --> 00:03:32,128
The chance the second one is red is 2/7.

58
00:03:32,140 --> 00:03:35,984
So, the probability of zero blue balls is  6/56

59
00:03:36,000 --> 00:03:41,168
the probability at least one blue ball is 1- 6/56 which is 50/56.

60
00:03:41,180 --> 00:03:45,040
Now the hard way is look at all the ways you could get at least

61
00:03:45,060 --> 00:03:49,270
one blue ball. You could get two blues or red followed by a blue or

62
00:03:49,290 --> 00:03:51,490
a blue followed by a red and if you add

63
00:03:51,510 --> 00:03:53,890
those up because they're mutually exclusive you should get the right

64
00:03:53,910 --> 00:03:58,060
answer but it's harder. How do you get two blues? well the chance of the first

65
00:03:58,080 --> 00:04:00,144
one being blue is 5/8,

66
00:04:00,160 --> 00:04:03,536
then when you pull the blue one out you got four blues two chance second one is

67
00:04:03,550 --> 00:04:08,640
blue is 4/7 you get 20/56 red blue or blue red have the same chance.

68
00:04:08,660 --> 00:04:11,980
First one red that's going to be 3/8, now once you take

69
00:04:12,000 --> 00:04:16,420
that out there are still five blues but seven balls you have a 5/7 chance of

70
00:04:16,440 --> 00:04:21,792
getting a blue that gives you 15/36 blue and red you just reversed the 5

71
00:04:21,810 --> 00:04:27,744
and you have the 3 here you get 15/36, add those all up you get 50/56.

72
00:04:27,760 --> 00:04:33,900
Now the fourth problem! Ten coins in a drawer, five are Two-headed,

73
00:04:33,920 --> 00:04:37,040
three are 2 tailed, two are fair coins.

74
00:04:37,060 --> 00:04:38,540
First of all what's the chance

75
00:04:38,560 --> 00:04:42,700
if you randomly draw a coin and flip it that you get heads

76
00:04:42,720 --> 00:04:47,470
okay and the second question given a coin toss comes up heads what's the

77
00:04:47,490 --> 00:04:48,928
chance the coin was fair,

78
00:04:48,940 --> 00:04:52,736
here you should be thinking Bayes theorem you're given states of the world

79
00:04:52,750 --> 00:04:54,496
what type of coin did you pick?

80
00:04:54,510 --> 00:04:58,208
You're given prior probabilities and then you're given a signal

81
00:04:58,220 --> 00:05:02,160
the fact that you got a head basically will change those prior probabilities

82
00:05:02,180 --> 00:05:05,250
So if you follow the Bayes theorem video you have to look at prior

83
00:05:05,270 --> 00:05:08,144
probabilities for states of the world and likelihoods

84
00:05:08,160 --> 00:05:11,712
and then it's pretty simple just multiply priors times likelihoods.

85
00:05:11,730 --> 00:05:14,940
So basically what are the prior probabilities?

86
00:05:14,960 --> 00:05:17,180
the probability of a two-headed coin is 5/10,

87
00:05:17,200 --> 00:05:18,440
there are 5 two-headed coins

88
00:05:18,460 --> 00:05:21,520
the probability the two tailed coin is 3/10,

89
00:05:21,540 --> 00:05:25,520
the probability of a one-headed coined or a fair coin is 2/10.

90
00:05:25,540 --> 00:05:26,848
Now the likelihoods

91
00:05:26,860 --> 00:05:30,352
we just look at the likelihoods of what we absorb which was a head,

92
00:05:30,370 --> 00:05:33,530
given you picked a two-headed coin what's the chance you get a head, one.

93
00:05:33,550 --> 00:05:37,600
Giving you pick the two tailed coin what's the chance to get ahead, zero.

94
00:05:37,620 --> 00:05:42,600
given basically you picked the fair coin what's the chance you got heads, 0.5.

95
00:05:42,620 --> 00:05:45,952
So now we can get by the law of total probability the

96
00:05:45,970 --> 00:05:48,960
chance of a head which is the denominator or Bayes Theorem.

97
00:05:48,980 --> 00:05:53,296
Remember Bayes Theorem probability of a fair coin given a head.

98
00:05:53,310 --> 00:05:55,800
Okay! It's probability of a fair coin

99
00:05:55,820 --> 00:05:59,050
and a head divided by the probability of a head. So the probability

100
00:05:59,070 --> 00:06:02,830
of a head you can get a head with the two-headed coin the two tail which

101
00:06:02,850 --> 00:06:04,880
really can't happen with the fair coin.

102
00:06:04,900 --> 00:06:06,880
So you could get a head with the

103
00:06:06,900 --> 00:06:09,960
two-headed coin take the prior times the likelihood of the head

104
00:06:09,980 --> 00:06:13,360
with a two-tailed coin take the prior times the likelihood of a head

105
00:06:13,380 --> 00:06:18,330
which is 0 or with the fair coin take the prior times the likelihood of a head.

106
00:06:18,350 --> 00:06:21,310
Probably of the two-headed coin is 0.5

107
00:06:21,330 --> 00:06:24,220
then you're sure to get ahead that's 0.5 times 1.

108
00:06:24,240 --> 00:06:27,200
Probably the head given to 2 tail is 0,

109
00:06:27,220 --> 00:06:31,440
Okay probably fair coin is 0.2 then there's half a chance to get ahead.

110
00:06:31,460 --> 00:06:35,940
So that's 0.6 that's your denominator, now the numerator is what's the

111
00:06:35,960 --> 00:06:38,890
chance you get a fair coin and a head, okay!

112
00:06:38,910 --> 00:06:42,670
So basically that one should say fair coin

113
00:06:45,630 --> 00:06:48,510
times the probably of head given fair

114
00:06:53,840 --> 00:06:57,000
and that's going to be the chance of a fair coin is point two and the head

115
00:06:57,020 --> 00:06:59,680
given fair is 0.5 that gives you a 0.1

116
00:06:59,700 --> 00:07:03,820
So the chance of getting that you got the fair coin giving you

117
00:07:03,840 --> 00:07:10,190
through a head is 1 over 6, 0.1 over 0.6, okay final problem. Okay

118
00:07:10,210 --> 00:07:13,960
let's take three events if we're given the probability of each of the events

119
00:07:13,980 --> 00:07:16,110
probability of event A is 0.1,

120
00:07:16,130 --> 00:07:20,140
probability of event B is 0.2, the probability of event C is 0.3

121
00:07:20,160 --> 00:07:23,290
What's the probability of A or B or C

122
00:07:23,310 --> 00:07:28,110
in two situations, first situation the events are mutually exclusive.

123
00:07:28,130 --> 00:07:30,730
The second situation the events are independent.

124
00:07:30,750 --> 00:07:32,470
Now the events are mutually exclusive

125
00:07:32,490 --> 00:07:35,880
you just add the probabilities take the probability of A plus

126
00:07:35,900 --> 00:07:38,840
probably B plus probably of C because they can't occur together.

127
00:07:38,860 --> 00:07:42,990
So you'd get 0.1 plus 0.2 plus 0.3 which is 60%

128
00:07:43,010 --> 00:07:46,890
now a harder problem, what's probability of A or B or C

129
00:07:46,910 --> 00:07:50,890
if they're independent. Now again use complements. So probably if A or B

130
00:07:50,910 --> 00:07:53,790
or C is 1 minus the probability none of them occur.

131
00:07:53,810 --> 00:07:57,160
Now what's the chance none of them occur, you need to probablility of

132
00:07:57,180 --> 00:08:00,080
node A times probability of node B times node C.

133
00:08:00,100 --> 00:08:02,210
Why can you multiply, because you have

134
00:08:02,230 --> 00:08:04,210
Independence. So we take the probability

135
00:08:04,230 --> 00:08:11,740
of node A as 1- 0.1, probability of node B 1- 0.2 the probability of node C 1-0.3

136
00:08:11,760 --> 00:08:15,120
and to multiply those together you get point .504

137
00:08:15,140 --> 00:08:21,630
So that probably if A or B or C is 1- 0.504 or 0.496

138
00:08:21,650 --> 00:08:24,550
Now that concludes our module on Probability.

139
00:08:24,570 --> 00:08:28,140
In the next module we'll start our study of Random variables

