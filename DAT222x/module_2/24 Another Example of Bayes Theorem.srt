0
00:00:00,630 --> 00:00:04,091
Let's see what happens, if we change the accuracy of our test.

1
00:00:04,394 --> 00:00:08,989
let's assume that the test is 80% accurate with a higher false positive rate,

2
00:00:08,998 --> 00:00:10,874
because that's more common in medicine.

3
00:00:11,463 --> 00:00:13,860
Now suppose you get a positive test result,

4
00:00:13,967 --> 00:00:15,750
what are the chances you have cancer?

5
00:00:15,750 --> 00:00:18,201
Eighty percent, twenty five percent, ten percent,

6
00:00:18,906 --> 00:00:22,080
the chance of a true positive is the chance you have the disease

7
00:00:22,091 --> 00:00:24,035
multiplied by the chance the test caught it.

8
00:00:24,130 --> 00:00:27,979
Point eight percent, the chance of a false positive is the chance

9
00:00:27,976 --> 00:00:31,668
you don't have the disease multiplied by the chance the tests caught it.

10
00:00:31,712 --> 00:00:34,340
Anyway fourteen point eight five percent.

11
00:00:34,939 --> 00:00:38,317
The chance of getting a real positive result is point eight percent,

12
00:00:38,430 --> 00:00:41,440
the chance of getting any type of positive result

13
00:00:41,448 --> 00:00:43,786
is the chance of a true positive plus

14
00:00:43,816 --> 00:00:47,274
the chance of a false positive, fifteen point six five percent.

15
00:00:47,850 --> 00:00:52,314
So if you get a positive result your chance of having the disease

16
00:00:52,320 --> 00:00:55,422
is point eight percent the real positive result divided

17
00:00:55,428 --> 00:00:59,697
by fifteen point six five percent the total possible positive results.

18
00:00:59,982 --> 00:01:02,008
Which is approximately five percent.

19
00:01:02,430 --> 00:01:06,874
In this example, the rate of actually having the disease with a positive test

20
00:01:06,888 --> 00:01:10,910
is much lower because of the higher rate of false positives.

21
00:01:11,333 --> 00:01:14,328
I think you can see how you might be able to apply something

22
00:01:14,325 --> 00:01:15,881
like this in the real world,

23
00:01:16,109 --> 00:01:19,750
these are very fun problems to solve.

