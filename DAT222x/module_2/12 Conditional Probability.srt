0
00:00:00,940 --> 00:00:03,120
The probability that event A occurs

1
00:00:03,200 --> 00:00:04,880
given that event B has occurred

2
00:00:04,940 --> 00:00:06,840
is called a conditional probability.

3
00:00:07,160 --> 00:00:08,660
For example imagine that

4
00:00:08,720 --> 00:00:12,340
the probability of being late to work is 5% on a typical day.

5
00:00:12,540 --> 00:00:14,060
However if it's raining,

6
00:00:14,120 --> 00:00:16,980
the probability of being late is 15%.

7
00:00:17,140 --> 00:00:19,040
This is a conditional probability.

8
00:00:19,140 --> 00:00:21,100
In other words conditional probability

9
00:00:21,140 --> 00:00:23,040
is the probability that you'll be late to work.

10
00:00:23,120 --> 00:00:25,960
given that it's raining in this case 15%.

11
00:00:26,520 --> 00:00:28,960
The conditional probability of event A

12
00:00:29,040 --> 00:00:31,020
given event B is denoted by the

13
00:00:31,120 --> 00:00:35,340
probability of A|B where the Pipe symbol means given.

14
00:00:35,580 --> 00:00:37,620
To calculate conditional probability

15
00:00:37,660 --> 00:00:40,040
of A given B you divide the probability

16
00:00:40,100 --> 00:00:42,660
of a A and B by the probability of B.

17
00:00:42,880 --> 00:00:45,280
Essentially what this formula is saying

18
00:00:45,320 --> 00:00:47,760
is that to calculate the conditional probability

19
00:00:47,840 --> 00:00:50,000
of event A given event B

20
00:00:50,140 --> 00:00:51,680
we change our sample space to

21
00:00:51,720 --> 00:00:53,540
consist of only the set B.

22
00:00:53,780 --> 00:00:56,800
In doing this we don't consider all of event A

23
00:00:56,860 --> 00:00:59,620
only the part of A that is also contained in B.

24
00:01:00,080 --> 00:01:03,080
This is known as the intersection of A and B

25
00:01:03,200 --> 00:01:05,560
or the joint probability of A and B.

26
00:01:06,020 --> 00:01:08,320
Be very careful to identify which event

27
00:01:08,420 --> 00:01:09,880
depends upon the other though.

28
00:01:10,160 --> 00:01:13,260
In general the probability of A given B

29
00:01:13,320 --> 00:01:16,300
is not equal to the probability of B given A.

30
00:01:16,640 --> 00:01:19,340
For example the probability of being late to work

31
00:01:19,400 --> 00:01:21,540
given that it's raining is not the same

32
00:01:21,620 --> 00:01:23,720
as the probability of that it's raining

33
00:01:23,820 --> 00:01:25,300
given that you are late to work.

34
00:01:26,300 --> 00:01:28,600
Let's take a closer look at how this works?

35
00:01:28,860 --> 00:01:31,020
Imagine that you have a deck of cards

36
00:01:31,180 --> 00:01:33,680
what is the probability of drawing an ace?

37
00:01:34,060 --> 00:01:36,840
This is a simple probability calculation.

38
00:01:36,940 --> 00:01:40,180
We know there are 4 aces in a standard deck of 52 cards.

39
00:01:40,280 --> 00:01:43,360
So the probability of drawing an ace is

40
00:01:43,426 --> 00:01:46,120
450 seconds or one thirteenth.

41
00:01:46,380 --> 00:01:50,100
Now imagine that you draw a queen on your first card.

42
00:01:50,220 --> 00:01:52,720
What is the probability that you will draw an ace

43
00:01:52,780 --> 00:01:54,300
the next time assuming that you

44
00:01:54,360 --> 00:01:56,080
do not replace the first card.

45
00:01:56,580 --> 00:01:58,260
The probability of drawing an ace

46
00:01:58,420 --> 00:02:01,626
given that you have already drawn a queen is four 54's.

47
00:02:01,660 --> 00:02:04,493
Because we still have all 4 aces in the deck.

48
00:02:04,580 --> 00:02:07,040
But one fewer card from which to select.

49
00:02:07,280 --> 00:02:10,520
This calculation is an example of a conditional probability.

50
00:02:10,800 --> 00:02:12,480
The probability of drawing an ace

51
00:02:12,560 --> 00:02:15,180
is dependent upon what happened in the first draw?

52
00:02:15,680 --> 00:02:17,520
Let's apply this logic of

53
00:02:17,620 --> 00:02:19,140
conditional probability formula to

54
00:02:19,220 --> 00:02:21,560
see if we end up with the same answer.

55
00:02:21,820 --> 00:02:24,480
We want to know the probability of drawing an ace

56
00:02:24,560 --> 00:02:26,820
given that the Queen has already been drawn.

57
00:02:26,910 --> 00:02:29,620
Thus event A is the we draw an ace,

58
00:02:29,820 --> 00:02:32,220
event B is the we draw a queen.

59
00:02:32,820 --> 00:02:34,940
The probability that both events happen

60
00:02:35,060 --> 00:02:38,020
that is we draw a queen and then an ace

61
00:02:38,140 --> 00:02:41,140
corresponds to the probability of A and B.

62
00:02:41,380 --> 00:02:46,800
The value of this probability is 16, 2650 seconds.

63
00:02:47,320 --> 00:02:49,240
The probability of event B

64
00:02:49,380 --> 00:02:51,870
that we draw a queen is 4, 50 seconds.

65
00:02:51,960 --> 00:02:54,440
Thus we use the conditional probability formula

66
00:02:54,480 --> 00:02:56,920
and see that the probability of drawing an ace

67
00:02:57,180 --> 00:03:00,520
given that a queen has been drawn is 4, 51st.

68
00:03:00,560 --> 00:03:02,460
The same is what we obtained above.

69
00:03:02,880 --> 00:03:04,860
Okay! This is complicated.

70
00:03:04,920 --> 00:03:06,580
So, let's look at another example.

71
00:03:06,980 --> 00:03:09,240
Let's imagine that we roll a pair of dice.

72
00:03:09,720 --> 00:03:11,860
The question that we would ask is

73
00:03:11,920 --> 00:03:14,880
what is the probability that we have rolled a 3

74
00:03:14,980 --> 00:03:18,340
given that we have rolled a sum less than 7.

75
00:03:18,540 --> 00:03:21,580
Here, event A is that we have rolled a 3

76
00:03:21,760 --> 00:03:25,780
and event B is that we have rolled a sum less than 7.

77
00:03:25,920 --> 00:03:27,940
There are 36 possible outcomes

78
00:03:28,000 --> 00:03:29,120
in this sample space.

79
00:03:29,280 --> 00:03:31,580
Of these 36 outcomes we can roll

80
00:03:31,640 --> 00:03:35,786
a sum less than 7 in15 different combinations of the two dice.

81
00:03:36,000 --> 00:03:39,760
So the probability of event B is 1536.

82
00:03:40,420 --> 00:03:43,180
Further, there are 5 combinations of two dice

83
00:03:43,240 --> 00:03:46,260
with the sum less than 7 where one of the die is 3.

84
00:03:46,460 --> 00:03:50,000
So the probability of A and B is 536.

85
00:03:50,320 --> 00:03:52,040
The conditional probability is

86
00:03:52,120 --> 00:03:56,500
536 divided by 1536 or 1/3rd.

87
00:03:57,200 --> 00:03:59,020
We can also demonstrate

88
00:03:59,120 --> 00:04:01,680
that-- that probability of A given B

89
00:04:01,780 --> 00:04:06,080
is not the same as the probability of B given A with this example.

90
00:04:06,520 --> 00:04:08,520
So what is the probability that

91
00:04:08,580 --> 00:04:10,500
we have a sum less than 7

92
00:04:10,600 --> 00:04:13,220
given that we rolled a 3.

93
00:04:13,880 --> 00:04:16,160
The probability of rolling a 3

94
00:04:16,260 --> 00:04:19,293
and a sum less than 7 is 536.

95
00:04:19,620 --> 00:04:23,760
The probability of rolling atleast one 3 is 1136.

96
00:04:24,080 --> 00:04:26,640
So the conditional probability in this case

97
00:04:26,746 --> 00:04:31,960
is 536 divided by 1136 or 5 eleven's.

