0
00:00:00,780 --> 00:00:04,050
There's one more relationship I want to briefly mention before we move on

1
00:00:04,050 --> 00:00:07,260
to The Law of Total Probability, and that is unions.

2
00:00:07,260 --> 00:00:10,820
What happens when you want to find the probability of A or B, and

3
00:00:10,820 --> 00:00:12,120
they overlap?

4
00:00:12,120 --> 00:00:15,914
The probability of the union of these events is denoted like you see

5
00:00:15,914 --> 00:00:19,030
here, with that U in between the A and B.

6
00:00:19,030 --> 00:00:23,510
I like to think of that U symbol as the bottom part of an o, as in or.

7
00:00:23,510 --> 00:00:27,300
When A and B overlap, you can see that the probability of A or

8
00:00:27,300 --> 00:00:31,150
B, is the sum of individual probability, minus the intersection,

9
00:00:31,150 --> 00:00:32,310
which gets added twice,

10
00:00:32,310 --> 00:00:35,350
if you were to simply sum the probability of each event together.

