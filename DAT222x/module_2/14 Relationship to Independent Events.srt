0
00:00:00,900 --> 00:00:03,630
As we just discussed, the conditional probability of event A,

1
00:00:03,630 --> 00:00:06,910
is a probability that the event will occur given knowledge that an event

2
00:00:06,910 --> 00:00:08,890
B has already occurred.

3
00:00:08,890 --> 00:00:11,790
What happens with conditional probability when events A and

4
00:00:11,790 --> 00:00:13,250
B are independent?

5
00:00:13,250 --> 00:00:16,770
Where event B has no effect on the probability of event A?

6
00:00:16,770 --> 00:00:19,550
In this case, the conditional probability of event A,

7
00:00:19,550 --> 00:00:23,870
given event B is simply the probability of event A.

8
00:00:23,870 --> 00:00:25,870
The same is true in the opposite direction.

9
00:00:25,870 --> 00:00:27,840
The probability of B, given A,

10
00:00:27,840 --> 00:00:31,220
is simply the probability of B when these events are independent.

