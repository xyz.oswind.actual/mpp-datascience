0
00:00:00,480 --> 00:00:03,900
In this video, we're gonna discuss independent events and

1
00:00:03,900 --> 00:00:08,160
also how to find the probability that two events occur together

2
00:00:08,160 --> 00:00:12,430
basically not A or B, but A and B if the events are independent.

3
00:00:12,430 --> 00:00:14,850
So what do we mean by independent events?

4
00:00:14,850 --> 00:00:18,370
Events A and B are independent if knowledge that A has occurred

5
00:00:18,370 --> 00:00:22,480
doesn't change your estimate of probability of B or knowledge that B

6
00:00:22,480 --> 00:00:25,260
has occurred doesn't change your estimate of the probability of A.

7
00:00:25,260 --> 00:00:27,330
So let's look at some examples.

8
00:00:27,330 --> 00:00:30,940
Let one event be the Dow is up at least 10% in a year and

9
00:00:30,940 --> 00:00:34,210
another event that Microsoft is down at least 30%.

10
00:00:34,210 --> 00:00:38,090
Those aren't independent because if I told you that Microsoft went down

11
00:00:38,090 --> 00:00:39,870
at least 30% in a year,

12
00:00:39,870 --> 00:00:43,020
it makes it highly unlikely that the Dow did very well.

13
00:00:43,020 --> 00:00:44,830
If I told you the Dow did very well,

14
00:00:44,830 --> 00:00:48,850
it makes it highly unlikely that Microsoft would do very poorly.

15
00:00:48,850 --> 00:00:50,480
Now, let's look at an example of two events that

16
00:00:50,480 --> 00:00:52,160
are probably independent.

17
00:00:52,160 --> 00:00:54,780
The Dow going up at least 10% in a year and

18
00:00:54,780 --> 00:00:58,060
the event that Manchester United wins the Premier League in soccer.

19
00:00:58,060 --> 00:00:59,746
Okay, those are probably independent.

20
00:00:59,746 --> 00:01:02,906
If I told you Manchester United won the Premiere League in soccer I

21
00:01:02,906 --> 00:01:06,296
don't think that tells you anything that would change your view of how

22
00:01:06,296 --> 00:01:07,343
the Dow's gonna do.

23
00:01:07,343 --> 00:01:11,033
If the Dow did really well or really bad I don't think you would change

24
00:01:11,033 --> 00:01:15,280
your view on Manchester United, so those events would be independent.

25
00:01:15,280 --> 00:01:18,610
Luckily, there's an easy test to see if events are independent.

26
00:01:18,610 --> 00:01:21,370
Two events, A and B, are independent, if the probability of

27
00:01:21,370 --> 00:01:25,750
them both occurring is the product of their individual probabilities.

28
00:01:25,750 --> 00:01:28,030
And so that's an easy test for independence, and

29
00:01:28,030 --> 00:01:32,070
we can also use independence, if we know it, to calculate probabilities.

30
00:01:32,070 --> 00:01:35,030
So let's look at some examples over here on the right.

31
00:01:35,030 --> 00:01:38,360
So here we have ten points in the sample space, fives times two,

32
00:01:38,360 --> 00:01:40,840
they each have chance one tenth.

33
00:01:40,840 --> 00:01:42,780
And so, basically, you have two events, A and B.

34
00:01:42,780 --> 00:01:44,830
And we wanna know if they're independent.

35
00:01:44,830 --> 00:01:46,840
So we'll try and check this condition.

36
00:01:46,840 --> 00:01:48,370
What's the chance that A would occur?

37
00:01:51,264 --> 00:01:52,627
So the chance that A would occur.

38
00:02:02,932 --> 00:02:05,990
Okay, there look to be five points in there that will be A.

39
00:02:05,990 --> 00:02:07,430
Five out of ten is one half.

40
00:02:09,020 --> 00:02:10,720
Now what's the chance that b will occur?

41
00:02:12,896 --> 00:02:15,343
There are six points in there for B, so

42
00:02:15,343 --> 00:02:19,640
the chance of B happening is six out of ten and if we multiply those,

43
00:02:26,475 --> 00:02:29,555
Okay, that's is going to be 30 over 100.

44
00:02:33,632 --> 00:02:35,288
And that's 0.3.

45
00:02:35,288 --> 00:02:37,500
Now does that equal the chance of A and B?

46
00:02:37,500 --> 00:02:40,740
The answer is no, because the chance of A and

47
00:02:40,740 --> 00:02:46,290
B happening when you look here, there's only one point out of ten.

48
00:02:48,060 --> 00:02:49,500
So these are not independent.

49
00:02:50,920 --> 00:02:53,270
Okay, now intuitively why is that?

50
00:02:53,270 --> 00:02:55,040
Because if I tell you that A happens,

51
00:02:55,040 --> 00:02:57,200
you're over here in this column.

52
00:02:57,200 --> 00:03:00,360
Then B is very unlikely cuz there's only one point with a B and

53
00:03:00,360 --> 00:03:02,400
you're missing all these other points with B.

54
00:03:02,400 --> 00:03:04,950
So that's why they are not independent.

55
00:03:04,950 --> 00:03:08,060
Now, in the second example here, A and B are independent.

56
00:03:08,060 --> 00:03:09,210
Let's see why.

57
00:03:09,210 --> 00:03:11,850
We have 20 points in the sample space 5 times 4.

58
00:03:11,850 --> 00:03:13,830
So how many As are there? 5.

59
00:03:13,830 --> 00:03:17,664
So the probability of A would

60
00:03:17,664 --> 00:03:20,387
equal 5 over 20.

61
00:03:20,387 --> 00:03:24,640
Now the probability of B, how many Bs are there?

62
00:03:24,640 --> 00:03:25,420
One, two, three, four.

63
00:03:25,420 --> 00:03:26,873
That's 4 over 20.

64
00:03:26,873 --> 00:03:30,921
And the probability of A and B,

65
00:03:30,921 --> 00:03:37,089
there's one point there, one out of 20. Okay.

66
00:03:37,089 --> 00:03:38,852
Now if I multiply these two

67
00:03:38,852 --> 00:03:42,090
probabilities, does that equal 1 over 20?

68
00:03:42,090 --> 00:03:42,900
Yes, it does.

69
00:03:48,960 --> 00:03:53,170
So we've got the probability of A times

70
00:03:53,170 --> 00:03:59,270
the probability of B.

71
00:03:59,270 --> 00:04:05,980
So that's gonna be 20 over 400 and that's 1/20.

72
00:04:07,020 --> 00:04:10,270
So A and B are independent in this situation.

73
00:04:10,270 --> 00:04:12,100
Cuz intuitively, if I told you that.

74
00:04:14,004 --> 00:04:16,273
A happened, basically,

75
00:04:16,273 --> 00:04:21,540
you can see there's one point in five that's gonna have a B.

76
00:04:21,540 --> 00:04:25,140
And that was the original chance of B was one point in five.

77
00:04:25,140 --> 00:04:29,020
So knowing that A happened basically didn't change the chance of B.

78
00:04:29,020 --> 00:04:34,304
So let's look at one or two more concrete examples here and then

79
00:04:34,304 --> 00:04:38,410
the next video will do two harder examples on independent events.

80
00:04:38,410 --> 00:04:42,258
So we have a deck of cards 13 diamonds,

81
00:04:42,258 --> 00:04:47,240
13 spades, 13 aces, 13 clubs and of course,

82
00:04:47,240 --> 00:04:52,920
sorry that should be 13 Hearts, and there are four aces.

83
00:04:56,730 --> 00:04:58,660
Four aces, four deuces, etc.

84
00:05:01,130 --> 00:05:03,700
Okay, so we draw a card from a deck of cards.

85
00:05:03,700 --> 00:05:06,375
Let the event A be the event the card is a spade,

86
00:05:06,375 --> 00:05:10,098
event B be the card is an ace, and these are independent events.

87
00:05:10,098 --> 00:05:13,040
In other words, knowing you got a spade doesn't change

88
00:05:13,040 --> 00:05:16,510
the chance that you're gonna get an ace and vice versa.

89
00:05:16,510 --> 00:05:18,160
Now, how can we show that?

90
00:05:18,160 --> 00:05:20,670
We can look at the probability of A.

91
00:05:20,670 --> 00:05:22,280
There are 13 spades out of 52.

92
00:05:22,280 --> 00:05:26,725
And B is an ace, there are four spades out of 52.

93
00:05:34,744 --> 00:05:39,320
And the probability that they both happen, we get a spade and an ace.

94
00:05:41,780 --> 00:05:43,980
That's only one card in the deck, the ace of spades.

95
00:05:45,580 --> 00:05:48,350
So if I multiply the probability of a times the probability of b,

96
00:05:48,350 --> 00:05:49,470
they'll come out the same.

97
00:05:55,586 --> 00:05:59,700
So that would be basically 1/4 times 1/13, if we reduce it.

98
00:06:04,512 --> 00:06:09,053
And that's what equals 1/52, so these events are independent.

99
00:06:09,053 --> 00:06:11,846
Remember, it's telling you you got a spade doesn't change the chance to

100
00:06:11,846 --> 00:06:13,370
get an ace or vice versa.

101
00:06:13,370 --> 00:06:15,510
Now, let's change this around a bit.

102
00:06:15,510 --> 00:06:18,580
Let's suppose event A is the event we get a spade and

103
00:06:18,580 --> 00:06:20,540
event B is the event we get the ace of spades.

104
00:06:20,540 --> 00:06:22,000
These are not independent.

105
00:06:22,000 --> 00:06:23,880
That should make sense to you because if I tell you you've got

106
00:06:23,880 --> 00:06:24,810
a spade,

107
00:06:24,810 --> 00:06:27,240
that makes it more likely you're going to get the ace of spades.

108
00:06:27,240 --> 00:06:31,754
So here are the probability of A,

109
00:06:31,754 --> 00:06:38,372
a spade is 13 over 52, which is one quarter.

110
00:06:41,130 --> 00:06:47,190
And the probability of B, ace of spades, is gonna be 1 over 52.

111
00:06:52,250 --> 00:06:53,770
Now the probability of A and B.

112
00:07:00,425 --> 00:07:05,268
Okay, we have to get a spade and the ace of spades, and

113
00:07:05,268 --> 00:07:08,078
that's gonna be 1 over 52.

114
00:07:08,078 --> 00:07:12,190
And I take the probability of A times the probability of B.

115
00:07:13,230 --> 00:07:15,010
That's just not gonna be the same thing.

116
00:07:19,800 --> 00:07:21,750
So A and B are not independent.

117
00:07:23,800 --> 00:07:27,100
In the next video, we'll solve two more complex problems involving

118
00:07:27,100 --> 00:07:28,180
independent events.

