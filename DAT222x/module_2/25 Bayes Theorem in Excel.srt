0
00:00:01,140 --> 00:00:03,500
So now, let's show you how base theorem works.

1
00:00:03,500 --> 00:00:06,190
Actually, you already know this cuz you know conditional probability,

2
00:00:06,190 --> 00:00:08,750
you know the law of total probability, and together,

3
00:00:08,750 --> 00:00:10,110
that gives you base theorem.

4
00:00:10,110 --> 00:00:12,820
So let's return to our breast cancer example.

5
00:00:12,820 --> 00:00:15,120
So the woman, unfortunately has just got,

6
00:00:15,120 --> 00:00:17,230
received a positive test result.

7
00:00:17,230 --> 00:00:19,000
What's the chance they have cancer?

8
00:00:19,000 --> 00:00:22,080
So it's a probability of cancer given a positive test result.

9
00:00:22,080 --> 00:00:25,139
So by definition, it's the probability of both of these events

10
00:00:25,139 --> 00:00:27,123
occurring in conditional probability,

11
00:00:27,123 --> 00:00:29,404
divided by the probability of the given event.

12
00:00:29,404 --> 00:00:33,020
The probability of the given event is the law of total probability.

13
00:00:33,020 --> 00:00:36,120
Now the key thing to understand here is to get all the terms and

14
00:00:36,120 --> 00:00:40,000
base term, you always take the prior probabilities times the likelihoods

15
00:00:40,000 --> 00:00:41,540
as you'll see.

16
00:00:41,540 --> 00:00:42,410
Now what's the numerator,

17
00:00:42,410 --> 00:00:45,060
the probability of cancer and a positive test result.

18
00:00:45,060 --> 00:00:49,929
Well you know the chance they had cancer that's the prior.

19
00:00:49,929 --> 00:00:54,174
And then you multiply that times the likelihood of the test result which

20
00:00:54,174 --> 00:00:55,810
is positive given cancer.

21
00:00:56,870 --> 00:00:59,143
Remember we said in conditional probability,

22
00:00:59,143 --> 00:01:02,435
to get the probability of two events you take the probably of one times

23
00:01:02,435 --> 00:01:04,560
the probably of the second given the first.

24
00:01:04,560 --> 00:01:07,169
So that'll be the numerator.

25
00:01:07,169 --> 00:01:08,109
Now, the denominator.

26
00:01:08,109 --> 00:01:10,351
How can they get a positive test result?

27
00:01:10,351 --> 00:01:13,020
Well, it's like the flooding, it's like the engine failing.

28
00:01:13,020 --> 00:01:17,723
You can get a positive test result from basically a person with

29
00:01:17,723 --> 00:01:21,075
cancer and from a person not having cancer.

30
00:01:21,075 --> 00:01:23,907
So you take the probability of cancer and

31
00:01:23,907 --> 00:01:28,270
plus test result which we already know from the top.

32
00:01:28,270 --> 00:01:31,690
But you could also take the probability of a plus test result

33
00:01:33,250 --> 00:01:34,150
and no cancer.

34
00:01:35,380 --> 00:01:37,600
And it turns out that's the bigger term,

35
00:01:37,600 --> 00:01:39,820
what we call the false positives.

36
00:01:39,820 --> 00:01:42,660
So you're gonna see the probability that the woman has cancer,

37
00:01:42,660 --> 00:01:45,820
thankfully, after the positive test result is not that high.

38
00:01:45,820 --> 00:01:49,840
It's only about 3% so let's go through the computations here.

39
00:01:49,840 --> 00:01:53,633
So we've got all the information we need here, okay, so

40
00:01:53,633 --> 00:01:59,419
the numerator here, We'll

41
00:01:59,419 --> 00:02:03,410
take the probability of cancer that's gonna be 0.996

42
00:02:03,410 --> 00:02:08,370
times the probability of a positive test result giving cancer is 0.8.

43
00:02:11,101 --> 00:02:13,878
Sorry, 0.004 is the probability of cancer.

44
00:02:13,878 --> 00:02:17,862
Boy, I hope that many people don't have cancer, we'd be in big trouble.

45
00:02:20,144 --> 00:02:22,737
So that's your numerator.

46
00:02:22,737 --> 00:02:29,760
Now the denominator, We should simplify this a bit first.

47
00:02:29,760 --> 00:02:33,978
See it's gonna be the probability of cancer which is the same term we

48
00:02:33,978 --> 00:02:37,311
had, times the probability of plus, given cancer.

49
00:02:40,560 --> 00:02:43,161
Plus the probability of no cancer.

50
00:02:48,593 --> 00:02:53,581
Times the likelihood probability of plus given, let's say, NC for

51
00:02:53,581 --> 00:02:56,180
no cancer, so we can fit it.

52
00:02:56,180 --> 00:02:58,310
See, it's always prior times likelihood.

53
00:02:58,310 --> 00:03:01,450
So to get that denominator we're just repeating the first term, but

54
00:03:01,450 --> 00:03:02,180
I'll type it out.

55
00:03:02,180 --> 00:03:04,240
You'll see I get exactly the same thing.

56
00:03:04,240 --> 00:03:06,110
Take the probability of cancer.

57
00:03:06,110 --> 00:03:13,356
0.004 times the probability of a plus given cancer, that's 0.8.

58
00:03:13,356 --> 00:03:19,579
Now the next term, the probability of no cancer is 0.996 okay,

59
00:03:19,579 --> 00:03:25,392
times the probability of plus given no cancer is 0.1, okay.

60
00:03:27,386 --> 00:03:29,448
I think I forgot a decimal point there.

61
00:03:33,329 --> 00:03:36,433
That should be a 0.004, sorry.

62
00:03:40,386 --> 00:03:43,227
Okay, so now denominator's spelled wrong there, but

63
00:03:43,227 --> 00:03:44,375
basically the ratio,

64
00:03:44,375 --> 00:03:48,120
the numerator to the denominator is our posterior probability.

65
00:03:48,120 --> 00:03:49,750
Posterior means after.

66
00:03:49,750 --> 00:03:52,810
What's the chance the woman has cancer after

67
00:03:52,810 --> 00:03:55,100
basically we see the positive mammogram?

68
00:03:55,100 --> 00:03:59,760
It's numerator divided by denominator and that's 3.1%.

69
00:03:59,760 --> 00:04:02,641
So it's very small, originally it was 4 in 1000.

70
00:04:02,641 --> 00:04:04,164
Now it's 3 in 100.

71
00:04:04,164 --> 00:04:06,053
Now, a much more intuitive way to see this and

72
00:04:06,053 --> 00:04:08,320
a lot of people like this approach better.

73
00:04:08,320 --> 00:04:11,950
Take a group of 10,000 people and classify them all by these

74
00:04:11,950 --> 00:04:15,514
probabilities into having cancer with a positive test result,

75
00:04:15,514 --> 00:04:17,880
cancer with a negative test result.

76
00:04:17,880 --> 00:04:22,350
No cancel positive test result, no cancel with a negative test result.

77
00:04:22,350 --> 00:04:24,260
And so let's do that.

78
00:04:24,260 --> 00:04:30,457
We can take 10,000 people, Just a nice big number there.

79
00:04:30,457 --> 00:04:32,791
Okay, how many of them have cancer?

80
00:04:32,791 --> 00:04:37,382
0.004, now of those people, how many are gonna test positive?

81
00:04:37,382 --> 00:04:40,452
Well, we know 80% are gonna test positive.

82
00:04:40,452 --> 00:04:42,165
So how many people would that be?

83
00:04:42,165 --> 00:04:43,517
That's 32.

84
00:04:43,517 --> 00:04:46,414
How many people would test positive with no cancer?

85
00:04:46,414 --> 00:04:49,554
We actually don't need the other two little probabilities, but

86
00:04:49,554 --> 00:04:51,280
we'll get them anyway.

87
00:04:51,280 --> 00:04:52,980
So how many people test positive,

88
00:04:52,980 --> 00:04:55,630
no cancer when you start with 10,000 people?

89
00:04:55,630 --> 00:04:58,250
Thankfully, this fraction have no cancer.

90
00:04:59,320 --> 00:05:02,620
And of those people, how many are gonna test positive?

91
00:05:02,620 --> 00:05:05,647
10%, okay.

92
00:05:05,647 --> 00:05:08,995
Now actually to find the other two it's easier to do it this way,

93
00:05:08,995 --> 00:05:12,040
see out of the 10th, well, we'll do it the normal way.

94
00:05:12,040 --> 00:05:15,051
But see 40 out of 10,000 will have cancer,

95
00:05:15,051 --> 00:05:19,823
0.04 times 10,000, but let's just say how many of them have cancer?

96
00:05:19,823 --> 00:05:22,448
That's gonna be 0.004.

97
00:05:22,448 --> 00:05:24,411
There's 10,000 of them.

98
00:05:24,411 --> 00:05:26,325
Now if they had cancer,

99
00:05:26,325 --> 00:05:31,418
the chance they're gonna test negative is going to be 0.2.

100
00:05:31,418 --> 00:05:32,326
So that's 8 people.

101
00:05:32,326 --> 00:05:34,374
That adds up to 40 as we'd think.

102
00:05:34,374 --> 00:05:37,314
Now how many people have no cancer and test negative?

103
00:05:37,314 --> 00:05:39,530
Well there's 10,000 people.

104
00:05:39,530 --> 00:05:41,810
Thankfully, that fraction have no cancer.

105
00:05:42,840 --> 00:05:46,717
Okay, and the chance they'll test negative, okay,

106
00:05:46,717 --> 00:05:50,272
given no cancer is gonna be 90% of them, okay.

107
00:05:50,272 --> 00:05:52,031
And that's gonna be a ton of people.

108
00:05:56,520 --> 00:05:59,835
And so basically, these should add up to all 10,000 people, I hope.

109
00:06:03,370 --> 00:06:07,108
And it does, now basically the probability of cancer given positive

110
00:06:07,108 --> 00:06:08,532
you just focus on the red.

111
00:06:08,532 --> 00:06:12,796
So now it's how many people cancer given positive?

112
00:06:12,796 --> 00:06:17,401
Well, you have 32 of those people in this column have cancer, so

113
00:06:17,401 --> 00:06:20,937
the answer would be 32 divided by the total in that

114
00:06:20,937 --> 00:06:26,420
column And that's exactly the same answer.

115
00:06:26,420 --> 00:06:30,483
Now, it's fine for you to do it this way without the formulas.

116
00:06:30,483 --> 00:06:34,026
Okay, you can just fill in this little matrix here for

117
00:06:34,026 --> 00:06:37,572
basically taking every possible state of the world,

118
00:06:37,572 --> 00:06:40,906
pair it with each possible outcome to the signal.

119
00:06:40,906 --> 00:06:44,020
And then use conditional probability on this column.

120
00:06:44,020 --> 00:06:47,320
That's fine you don't really need the formulas that are over here and

121
00:06:47,320 --> 00:06:50,210
I think anybody who teachers base theorem is fine with

122
00:06:50,210 --> 00:06:51,810
you doing it this way.

123
00:06:51,810 --> 00:06:54,640
So that concludes module two we'll go on to module three

124
00:06:54,640 --> 00:06:57,160
where we talk about random variables.

