0
00:00:00,233 --> 00:00:03,772
Now, let's take a look at the law of total probability.

1
00:00:03,772 --> 00:00:06,693
It is a powerful tool that relates marginal probabilities to

2
00:00:06,693 --> 00:00:08,752
conditional probabilities.

3
00:00:08,752 --> 00:00:11,250
As a result, if we want to find probability of A,

4
00:00:11,250 --> 00:00:14,970
we sum all of the probabilities of A, intersection B.

5
00:00:14,970 --> 00:00:18,580
Because intersection probabilities are calculating using multiplication

6
00:00:18,580 --> 00:00:21,630
through the magic of math it can be shown that this is the same as

7
00:00:21,630 --> 00:00:25,070
summing that conditional probability A, given B And

8
00:00:25,070 --> 00:00:27,030
the overall probability of B.

9
00:00:27,030 --> 00:00:29,720
Let's take a look at an example to illustrate this.

10
00:00:29,720 --> 00:00:32,120
Imagine that you purchase a pair of hiking boots.

11
00:00:32,120 --> 00:00:35,896
If the boots are manufactured in country A, the probability that they

12
00:00:35,896 --> 00:00:39,807
will last 1,000 miles before the water proofing result is 99%.

13
00:00:39,807 --> 00:00:45,209
However, if they're manufactured in country B, the probability is 95%.

14
00:00:45,209 --> 00:00:49,940
We know that 75% of these boots are manufactured in country A.

15
00:00:49,940 --> 00:00:53,060
What is the probability that your boots will last 1,000 miles?

16
00:00:53,060 --> 00:00:54,710
Well here's how this works.

17
00:00:54,710 --> 00:00:58,220
We simply plug these numbers in to this equation.

18
00:00:58,220 --> 00:01:01,360
What you can see after you do the math is that

19
00:01:01,360 --> 00:01:06,000
you have a 98% chance that your boots will last 1,000 miles.

20
00:01:06,000 --> 00:01:07,670
I wish mine would.

21
00:01:07,670 --> 00:01:11,200
Now, let's look at a more complicated example, one where we're

22
00:01:11,200 --> 00:01:14,700
estimating the probability of an event that occurs inside and

23
00:01:14,700 --> 00:01:17,180
outside the sample space of the second event.

24
00:01:17,180 --> 00:01:21,440
This requires a slightly more complicated formula to do it.

25
00:01:21,440 --> 00:01:25,602
Imagine that a particular medical test is 95% accurate.

26
00:01:25,602 --> 00:01:27,480
If someone has the disease,

27
00:01:27,480 --> 00:01:31,326
the test will report a positive result 95% of the time.

28
00:01:31,326 --> 00:01:34,593
Further, if someone doesn't have the disease,

29
00:01:34,593 --> 00:01:38,257
they will receive a negative result 97% of the time.

30
00:01:38,257 --> 00:01:42,845
Only 1% of the population has this particular disease.

31
00:01:42,845 --> 00:01:45,715
What is the probability that a particular person chosen

32
00:01:45,715 --> 00:01:48,360
at random will be tested positive?

33
00:01:48,360 --> 00:01:51,580
What we're going to do is we're going to let TP denote the event

34
00:01:51,580 --> 00:01:53,740
that a person is tested positive.

35
00:01:53,740 --> 00:01:57,110
D will denote the event that a person has the disease.

36
00:01:57,110 --> 00:02:00,832
D prime will denote that the event that the person does not have

37
00:02:00,832 --> 00:02:01,697
the disease.

38
00:02:01,697 --> 00:02:05,244
Then it follows, we do the math, and what we find is that

39
00:02:05,244 --> 00:02:09,319
the probability of having the disease, remember, is .01,

40
00:02:09,319 --> 00:02:14,160
then the probability of not having the disease, the complement is .99.

41
00:02:14,160 --> 00:02:18,787
The probability of testing positive when you have the disease is .95 and

42
00:02:18,787 --> 00:02:23,194
the probability of testing positive when you don't have the disease is

43
00:02:23,194 --> 00:02:23,790
.03.

44
00:02:23,790 --> 00:02:28,344
You just plug those pieces of data into the formula,

45
00:02:28,344 --> 00:02:30,525
and your result is 4%.

