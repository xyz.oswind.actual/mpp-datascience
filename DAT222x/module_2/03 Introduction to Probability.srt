0
00:00:00,560 --> 00:00:03,210
In this module, we're going to discuss probability, so

1
00:00:03,210 --> 00:00:05,910
let's start with an introduction to probability.

2
00:00:05,910 --> 00:00:09,380
So probability is the likelihood that an outcome occurs.

3
00:00:09,380 --> 00:00:12,390
And so there's some terms we always associate with probability so

4
00:00:12,390 --> 00:00:14,120
let's be clear on their meaning.

5
00:00:14,120 --> 00:00:16,080
The first one is an experiment.

6
00:00:16,080 --> 00:00:19,350
An experiment is a situation with uncertainty that can be repeated.

7
00:00:19,350 --> 00:00:21,690
For example, you toss two dice.

8
00:00:21,690 --> 00:00:24,639
You draw a poker hand, five cards from a deck of cards.

9
00:00:24,639 --> 00:00:26,560
You toss a coin 3 times.

10
00:00:26,560 --> 00:00:28,370
Those experiments can easily be repeated.

11
00:00:28,370 --> 00:00:30,230
And then, once you have an experiment,

12
00:00:30,230 --> 00:00:32,270
you talk about a sample space.

13
00:00:32,270 --> 00:00:35,355
You talk about the set of all possible outcomes for an experiment.

14
00:00:35,355 --> 00:00:38,609
And it makes our life easier if all the points in the sample space have

15
00:00:38,609 --> 00:00:39,760
the same probability.

16
00:00:39,760 --> 00:00:43,330
That doesn't have to be true but it does make our life easier.

17
00:00:43,330 --> 00:00:46,439
And the probability of all the possible outcomes to an experiment

18
00:00:46,439 --> 00:00:47,860
must add to 1, or 100%.

19
00:00:47,860 --> 00:00:51,338
So let's look at some examples of sample spaces.

20
00:00:51,338 --> 00:00:52,790
So we toss two coins.

21
00:00:52,790 --> 00:00:53,900
What might happen?

22
00:00:53,900 --> 00:00:56,840
You get heads or tails, not landing on its side.

23
00:00:56,840 --> 00:01:00,476
You could get head head, head tail, tail head or tail tail.

24
00:01:00,476 --> 00:01:02,684
So there are four points in that sample space.

25
00:01:02,684 --> 00:01:05,151
And there's no reason, if it's a fair coin,

26
00:01:05,151 --> 00:01:07,633
to think any one is more likely than the other.

27
00:01:07,633 --> 00:01:10,200
So those four points, the probability adds to 1.

28
00:01:10,200 --> 00:01:12,850
Each of those has a one-quarter chance of occurring.

29
00:01:12,850 --> 00:01:15,254
And we can solve lots of problems using that fact.

30
00:01:15,254 --> 00:01:16,375
Now we toss two dice.

31
00:01:16,375 --> 00:01:19,940
Dice, if you don't know, have the numbers one through six on them.

32
00:01:19,940 --> 00:01:22,580
We'll assume each face is equally likely to come up.

33
00:01:22,580 --> 00:01:24,830
So what are the points in the sample space?

34
00:01:24,830 --> 00:01:29,478
You can get a 1 with a 1, a 1 with a 2, a 1 with a 3, 1 with a 6,

35
00:01:29,478 --> 00:01:31,730
2 with a 1, 2 with a 6, etc.

36
00:01:31,730 --> 00:01:33,760
6 with a 1, 6 with a 6.

37
00:01:33,760 --> 00:01:36,750
The first die can come out six ways, so can the second.

38
00:01:36,750 --> 00:01:39,927
So, there are 36 possible points in the sample space.

39
00:01:39,927 --> 00:01:44,088
So each of them has a chance of one thirty-sixth of happening cuz

40
00:01:44,088 --> 00:01:48,401
there's no reason to expect that a 1, 2 is more likely than a 2,

41
00:01:48,401 --> 00:01:49,109
1, etc.

42
00:01:49,109 --> 00:01:51,011
Next we talk about an event.

43
00:01:51,011 --> 00:01:53,387
An event is a subset of the sample space.

44
00:01:53,387 --> 00:01:57,010
And we want to assign a probability to an event.

45
00:01:57,010 --> 00:02:00,440
And there are certain axioms probabilities must satisfy.

46
00:02:00,440 --> 00:02:02,661
And then we'll do a couple of examples.

47
00:02:02,661 --> 00:02:04,420
So axiom is a probability.

48
00:02:04,420 --> 00:02:08,003
Probabilities of events must satisfy the following.

49
00:02:08,003 --> 00:02:10,220
Probability of any event must be non-negative.

50
00:02:10,220 --> 00:02:13,440
It doesn't make sense to talk about a negative probability.

51
00:02:13,440 --> 00:02:16,699
If you take the event, that is all points in the sample space,

52
00:02:16,699 --> 00:02:18,340
it has to have probability 1.

53
00:02:18,340 --> 00:02:20,555
So let's look at a couple examples.

54
00:02:20,555 --> 00:02:21,641
So you toss two dice.

55
00:02:21,641 --> 00:02:23,920
What's the probability the total is 8?

56
00:02:23,920 --> 00:02:27,604
Remember, each point in the die sample space has a one thirty-sixth

57
00:02:27,604 --> 00:02:28,130
chance.

58
00:02:28,130 --> 00:02:29,832
What are the ways you can get an 8?

59
00:02:29,832 --> 00:02:32,109
Well, if you get a 1 on the first toss you can't get an 8.

60
00:02:32,109 --> 00:02:35,972
There's no way to take a 1 with the highest thing on the second toss and

61
00:02:35,972 --> 00:02:36,570
get an 8.

62
00:02:36,570 --> 00:02:39,740
But a 2 and a 6 can give you an 8.

63
00:02:39,740 --> 00:02:41,480
A 3 and a 5 can give you an 8.

64
00:02:41,480 --> 00:02:44,994
A 4 and a 4, a 5 and a 3, and a 6 and a 2 can give you an 8.

65
00:02:44,994 --> 00:02:48,855
There are five ways you can get an 8 out of the 36 possible outcomes in

66
00:02:48,855 --> 00:02:49,910
the sample space.

67
00:02:49,910 --> 00:02:53,150
So the chance of an 8 is 5 over 36.

68
00:02:53,150 --> 00:02:55,640
Let's look at another example here.

69
00:02:55,640 --> 00:02:58,790
Family with three children, assuming it's equally likely to be a boy or

70
00:02:58,790 --> 00:03:02,332
a girl, which is not totally exactly true, I think.

71
00:03:02,332 --> 00:03:05,820
The probability of exactly one daughter in that family.

72
00:03:05,820 --> 00:03:07,416
List the points in the sample space.

73
00:03:07,416 --> 00:03:10,194
So we can start with the first child being a boy.

74
00:03:10,194 --> 00:03:14,230
And then, we could pair boy-boy, boy-girl, girl-boy, girl-girl,

75
00:03:14,230 --> 00:03:15,181
with being a boy.

76
00:03:15,181 --> 00:03:17,431
We could start with the first child being a girl.

77
00:03:17,431 --> 00:03:21,120
Pair a boy-boy, boy-girl, girl-boy, and girl-girl.

78
00:03:21,120 --> 00:03:24,250
There are eight possible points in the sample space.

79
00:03:24,250 --> 00:03:26,682
Again, that's just 2 times 2 times 2,

80
00:03:26,682 --> 00:03:30,190
because each of those children can be a boy or a girl.

81
00:03:30,190 --> 00:03:34,052
So each of those points would have a one-eighth chance of occurring,

82
00:03:34,052 --> 00:03:37,522
cuz there's no more likelihood of getting boy-boy-boy than

83
00:03:37,522 --> 00:03:39,037
girl-girl-girl, etc.

84
00:03:39,037 --> 00:03:41,740
So if I want the chance of getting exactly one daughter,

85
00:03:41,740 --> 00:03:44,974
I can find the points in the sample space that give me one daughter.

86
00:03:44,974 --> 00:03:46,149
Boy-boy-girl.

87
00:03:46,149 --> 00:03:47,396
Boy-girl-boy.

88
00:03:47,396 --> 00:03:48,571
Girl-boy-boy.

89
00:03:48,571 --> 00:03:52,170
In other words, just pick the child basically who's the girl.

90
00:03:52,170 --> 00:03:55,350
It could be the first child, the second child, or the third child.

91
00:03:55,350 --> 00:03:58,927
Three points in the sample space yield a girl out of eight,

92
00:03:58,927 --> 00:04:00,314
one girl out of eight.

93
00:04:00,314 --> 00:04:03,040
So the chance of exactly one daughter is three eights.

94
00:04:03,040 --> 00:04:06,230
We'll see this another way in module three when we talk about

95
00:04:06,230 --> 00:04:08,520
the binomial random variable.

96
00:04:08,520 --> 00:04:10,597
Let's do one more example here, and

97
00:04:10,597 --> 00:04:13,819
then close with three interpretations of probability.

98
00:04:13,819 --> 00:04:15,717
So let's suppose you have a deck of cards.

99
00:04:15,717 --> 00:04:20,933
And a deck of cards has 13 of each suit, 13 diamonds,

100
00:04:20,933 --> 00:04:26,161
no jokers, 13 hearts, 13 clubs, and 13 spades.

101
00:04:26,161 --> 00:04:31,408
What's the probability you'll draw a diamond from the deck of cards?

102
00:04:31,408 --> 00:04:34,407
Well the points in the sample space would be each card you could draw,

103
00:04:34,407 --> 00:04:35,450
there's 52 points.

104
00:04:35,450 --> 00:04:37,707
They each have one chance in 52.

105
00:04:37,707 --> 00:04:39,295
And so 13 of them are diamonds.

106
00:04:39,295 --> 00:04:42,416
So the chance you'll get a diamond is 13 over 52.

107
00:04:42,416 --> 00:04:44,873
And now we can close with what we learned about.

108
00:04:44,873 --> 00:04:48,780
Three different interpretations of probability that people often use.

109
00:04:48,780 --> 00:04:50,261
One is the classical definition.

110
00:04:50,261 --> 00:04:52,524
That's what we have been discussing here.

111
00:04:52,524 --> 00:04:55,861
Use the sample space and experiment approach to come up with

112
00:04:55,861 --> 00:04:59,360
probabilities like we've done in these examples.

113
00:04:59,360 --> 00:05:02,139
A second approach is the relative frequency approach to

114
00:05:02,139 --> 00:05:03,565
estimating a probability.

115
00:05:03,565 --> 00:05:06,200
You use empirical data to estimate a probability.

116
00:05:06,200 --> 00:05:08,360
We'll see more of this later in the course.

117
00:05:08,360 --> 00:05:11,785
For instance, if a player made 80 of 100 foul shots in basketball,

118
00:05:11,785 --> 00:05:15,285
you would estimate their chance of making a foul shot is 80 over 100 or

119
00:05:15,285 --> 00:05:16,005
80%.

120
00:05:16,005 --> 00:05:19,450
Now if you add more data the relative frequency estimates of

121
00:05:19,450 --> 00:05:21,190
probability change.

122
00:05:21,190 --> 00:05:24,460
And finally, there's the subjective probability approach that people

123
00:05:24,460 --> 00:05:26,280
use to estimate probabilities.

124
00:05:26,280 --> 00:05:28,578
Use judgement to estimate a probability.

125
00:05:28,578 --> 00:05:30,514
For instance, at the end of 2020,

126
00:05:30,514 --> 00:05:33,430
what's the chance the Dow will be at least 20,000?

127
00:05:33,430 --> 00:05:37,240
You could get some experts in the room and try and estimate it.

128
00:05:37,240 --> 00:05:40,802
And what we've learned a lot in recent years is wisdom of crowds

129
00:05:40,802 --> 00:05:43,940
often helps us estimate subjective probability.

130
00:05:43,940 --> 00:05:47,090
If you have lots of people estimate a probability,

131
00:05:47,090 --> 00:05:49,600
then basically if you average their estimates, you'll probably get

132
00:05:49,600 --> 00:05:53,200
a good estimate of the probability of the event really happening.

133
00:05:53,200 --> 00:05:56,945
Now in our next video we'll talk about the law of complements which

134
00:05:56,945 --> 00:05:59,680
could help us solve some interesting probability problems.

