0
00:00:00,580 --> 00:00:03,760
Two events are independent if the outcome of one doesn't affect

1
00:00:03,760 --> 00:00:05,150
the outcome of the other.

2
00:00:05,150 --> 00:00:08,390
In other words, if the occurrence of event a does not

3
00:00:08,390 --> 00:00:13,360
change the probability of event b, then events a and b are independent.

4
00:00:13,360 --> 00:00:15,230
Otherwise, they are dependent.

5
00:00:15,230 --> 00:00:18,930
When tossing a fair dice twice, the result of the first toss doesn't

6
00:00:18,930 --> 00:00:23,060
affect the probability or the outcome of the second toss.

7
00:00:23,060 --> 00:00:27,590
Drawing two cards from a deck of 52, however, are not independent.

8
00:00:27,590 --> 00:00:31,210
The probability of drawing a king on the second card is dependent on what

9
00:00:31,210 --> 00:00:35,050
you drew the first time because there is one less card in the deck.

10
00:00:35,050 --> 00:00:37,690
However, if you return the first card to the deck,

11
00:00:37,690 --> 00:00:41,370
which we call with replacement, before drawing the second card,

12
00:00:41,370 --> 00:00:43,890
they are in fact independent.

13
00:00:43,890 --> 00:00:45,610
For two independent events A and

14
00:00:45,610 --> 00:00:49,880
B, the probability of both occurring together, the probability of A and

15
00:00:49,880 --> 00:00:53,400
B is the product of the probability of each event.

16
00:00:53,400 --> 00:00:56,360
This is more commonly represented with an upside down u

17
00:00:56,360 --> 00:00:58,150
between the A and B.

18
00:00:58,150 --> 00:01:02,710
Thus, this is indicating that both events A and B occur.

19
00:01:02,710 --> 00:01:04,340
When tossing a fair coin twice,

20
00:01:04,340 --> 00:01:07,940
the probability of getting a head on the first and then getting a tail

21
00:01:07,940 --> 00:01:12,620
on the second is the probability of both heads and tails.

22
00:01:12,620 --> 00:01:14,685
So you take the probability of the head and

23
00:01:14,685 --> 00:01:17,166
multiply it by the probability of getting a tail.

24
00:01:17,166 --> 00:01:21,890
So 0.5 multiplied by 0.5 which is 0.25.

25
00:01:21,890 --> 00:01:25,320
Remember, these events are mutually exclusive if

26
00:01:25,320 --> 00:01:27,750
they cannot occur at the same time.

27
00:01:27,750 --> 00:01:30,800
So to summarize the difference between mutually exclusive and

28
00:01:30,800 --> 00:01:32,590
dependent is this.

29
00:01:32,590 --> 00:01:35,850
Independent events means that the occurence or outcome of one event

30
00:01:35,850 --> 00:01:39,270
does not influence the occurrence of another vent.

31
00:01:39,270 --> 00:01:41,670
Mutually exclusive events means that the occurrence or

32
00:01:41,670 --> 00:01:46,060
presence of one event entails the non occurrence of the other.

33
00:01:46,060 --> 00:01:49,470
The probability of an independent event occurring at the same time

34
00:01:49,470 --> 00:01:53,040
is the product of the probability of each event.

35
00:01:53,040 --> 00:01:56,388
Whereas the probability of mutually exclusive events occurring at

36
00:01:56,388 --> 00:01:57,864
the same time is always zero.

