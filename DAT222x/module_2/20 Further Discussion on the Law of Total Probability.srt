0
00:00:00,480 --> 00:00:04,250
Another wa to think about this a variation on the law of

1
00:00:04,250 --> 00:00:07,640
probability in the form of a tree chart that lays out every

2
00:00:07,640 --> 00:00:09,780
possible combination of the outcomes.

3
00:00:09,780 --> 00:00:10,510
When you do this,

4
00:00:10,510 --> 00:00:13,560
you start with the marginal probability of having the disease.

5
00:00:13,560 --> 00:00:16,670
In this case, 1% chance of having the disease.

6
00:00:16,670 --> 00:00:19,430
Regardless of whether you have the disease or not though,

7
00:00:19,430 --> 00:00:22,100
you can test positive or you can test negative at

8
00:00:22,100 --> 00:00:25,410
rates indicated by the conditional probability.

9
00:00:25,410 --> 00:00:28,850
Then it's a simple matter of multiplying the marginal probability

10
00:00:28,850 --> 00:00:32,350
and the conditional probability for each of the outcomes.

11
00:00:32,350 --> 00:00:34,540
This is the joint probability.

12
00:00:34,540 --> 00:00:37,240
Remember we wanted to know the probability that someone selected at

13
00:00:37,240 --> 00:00:38,800
random would test positive?

14
00:00:38,800 --> 00:00:43,007
So we sum the probabilities of the two outcomes where testing positive

15
00:00:43,007 --> 00:00:44,726
can occur, and we get 4%.

16
00:00:44,726 --> 00:00:49,128
One more example just to make sure you completely understand

17
00:00:49,128 --> 00:00:52,190
the notion of total probability.

18
00:00:52,190 --> 00:00:55,080
Imagine that you asked your neighbor to water your plants while you're on

19
00:00:55,080 --> 00:00:56,100
vacation.

20
00:00:56,100 --> 00:00:59,894
You estimate the probability that she'll remember is about 85%.

21
00:00:59,894 --> 00:01:03,925
Your vacation is rather long, so you estimate the probability of

22
00:01:03,925 --> 00:01:07,591
the plant dying if it's not watered to be about 50%, and

23
00:01:07,591 --> 00:01:09,540
if it is watered about 10%.

24
00:01:09,540 --> 00:01:12,610
So what is the probability that you'll return to a plant that's

25
00:01:12,610 --> 00:01:13,790
still alive?

26
00:01:13,790 --> 00:01:16,400
Well, let's do the same thing we did before.

27
00:01:16,400 --> 00:01:19,430
We'll let R denote the event that your neighbor actually remembers to

28
00:01:19,430 --> 00:01:20,850
water the plant.

29
00:01:20,850 --> 00:01:23,610
R prime to denote the event that she forgot and

30
00:01:23,610 --> 00:01:26,250
DH to note the death of your plant.

31
00:01:26,250 --> 00:01:29,190
Then it follows that you get when you plug in the math that

32
00:01:29,190 --> 00:01:31,767
the probability that you'll remember Is 85.

33
00:01:31,767 --> 00:01:34,580
The probability that you'll forget is 15.

34
00:01:34,580 --> 00:01:39,900
The probability that you'll have a death even if she remembers is 10.

35
00:01:39,900 --> 00:01:44,180
And the probability that you'll have death if she forgets is 50.

36
00:01:44,180 --> 00:01:48,800
So we plug all of that in to the formula and guess what, there's

37
00:01:48,800 --> 00:01:52,460
a 16% chance that she'll return home to a dead plant regardless.

