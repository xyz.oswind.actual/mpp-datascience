0
00:00:00,590 --> 00:00:03,472
Let's dig a little deeper into independent events.

1
00:00:03,568 --> 00:00:05,810
In the previous video you learned how to calculate the

2
00:00:05,872 --> 00:00:09,340
probability that two independent events A and B occur.

3
00:00:09,424 --> 00:00:12,060
As we saw that is a multiplication problem,

4
00:00:12,192 --> 00:00:17,340
however how do you calculate the probability that either A or B occurs.

5
00:00:17,392 --> 00:00:18,670
Recall that if two events

6
00:00:18,736 --> 00:00:20,830
are mutually exclusive they cannot occur together.

7
00:00:20,912 --> 00:00:25,490
However independent events can occur together. So you need to account for the

8
00:00:25,568 --> 00:00:28,350
probability that each event could occur by itself

9
00:00:28,416 --> 00:00:30,220
and that they could occur together.

10
00:00:30,336 --> 00:00:34,200
As a result you sum the probability of each event occurring by itself

11
00:00:34,256 --> 00:00:37,340
and then you subtract the probability of both occurring together.

12
00:00:37,408 --> 00:00:41,630
When we say A or B occurs we mean one of three possibilities,

13
00:00:41,696 --> 00:00:48,090
A occurs but B does not, B occurs but A does not and A and B occur together.

14
00:00:48,176 --> 00:00:51,950
In this case or is technically called inclusive OR

15
00:00:52,016 --> 00:00:55,320
because it includes the case in which both A and B occur.

16
00:00:55,408 --> 00:00:57,580
If we did not include this possibility

17
00:00:57,648 --> 00:01:02,220
then we would be using an exclusive OR.  The latter is what we mean by

18
00:01:02,304 --> 00:01:06,280
mutually exclusive events, which we discussed earlier in this module.

19
00:01:06,352 --> 00:01:12,300
The inclusive OR is a joint probability which we will discuss in more detail

20
00:01:12,368 --> 00:01:17,630
later in this module. Let's look at an example if you flip a coin 2 times

21
00:01:17,696 --> 00:01:20,860
what is the probability that you will get a head on the first flip

22
00:01:20,928 --> 00:01:23,800
a head on the second flip or a head on both flips.

23
00:01:23,888 --> 00:01:27,160
Let event A be the head on the first flip

24
00:01:27,232 --> 00:01:31,310
and event B be a head on the second flip the probability of the head

25
00:01:31,344 --> 00:01:35,630
on the first flip is one-half the probability of a head on the second flip

26
00:01:35,680 --> 00:01:40,460
is one-half, from a previous video we know that the probability of A and B

27
00:01:40,512 --> 00:01:44,000
is the probability of A multiplied by the probability of B

28
00:01:44,064 --> 00:01:46,330
which is one quarter in this example.

29
00:01:46,384 --> 00:01:55,150
Thus the probability of A or B is  1/2 + 1/2 - 1/4, you have a 75%

30
00:01:55,216 --> 00:01:59,500
chance of flipping at least one head when you flip a fair coin twice.

31
00:01:59,568 --> 00:02:04,090
if you toss a six-sided die and then flip a coin

32
00:02:04,128 --> 00:02:08,030
what is the probability that you will get either a 4 on the die,

33
00:02:08,080 --> 00:02:10,600
a head on the coin flip or both.

34
00:02:10,688 --> 00:02:16,730
The probability of rolling of 4 is 1/6 the probability of flipping ahead is 1/2

35
00:02:16,784 --> 00:02:18,770
and the probability of both occurring

36
00:02:18,860 --> 00:02:24,800
together is 1/6 multiplied by 1/2 thus the result is 7/12.

