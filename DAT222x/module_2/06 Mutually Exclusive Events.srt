0
00:00:00,134 --> 00:00:04,770
Two events are mutually exclusive if they cannot occur at the same time.

1
00:00:04,770 --> 00:00:06,790
For example, when tossing a fair coin,

2
00:00:06,790 --> 00:00:10,430
you cannot get a head and a tail during the same toss.

3
00:00:10,430 --> 00:00:12,900
For two mutually exclusive events, A and B.

4
00:00:12,900 --> 00:00:15,490
The probability of either one occurring, A or

5
00:00:15,490 --> 00:00:19,250
B is the sum of the probability of each event.

6
00:00:19,250 --> 00:00:20,900
Imagine that you're rolling a die and

7
00:00:20,900 --> 00:00:23,740
you want to know the probability of rolling an odd number.

8
00:00:23,740 --> 00:00:26,639
To calculate this, you would sum the probabilities of each event.

9
00:00:26,639 --> 00:00:29,399
One-sixth plus one-sixth plus one-sixth.

10
00:00:29,399 --> 00:00:33,019
So the probability of rolling an odd number is one-half.

11
00:00:33,019 --> 00:00:34,839
Let's take another example.

12
00:00:34,839 --> 00:00:37,610
Imagine that you were grabbing a sock from a drawer.

13
00:00:37,610 --> 00:00:39,440
The drawer contains eight socks.

14
00:00:39,440 --> 00:00:42,480
Two that are blue, three that are purple, two that are green and

15
00:00:42,480 --> 00:00:43,610
one that is black.

16
00:00:43,610 --> 00:00:45,761
The probability of pulling out a blue or

17
00:00:45,761 --> 00:00:49,366
purple sock is one quarter plus three-eights or five-eights.

18
00:00:49,366 --> 00:00:52,810
It's a bit weird to think about it this way, but oddly,

19
00:00:52,810 --> 00:00:56,170
mutually exclusive events are dependent on each other.

20
00:00:56,170 --> 00:00:57,940
If one occurs, the other cannot.

