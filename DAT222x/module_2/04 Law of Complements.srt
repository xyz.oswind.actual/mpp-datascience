0
00:00:00,800 --> 00:00:04,425
The complement of an event is all outcomes that are not the event.

1
00:00:04,480 --> 00:00:07,550
When the event is heads the compliment is tails,

2
00:00:07,600 --> 00:00:11,650
when the event is hearts the complement is spades, clubs, diamonds,

3
00:00:11,728 --> 00:00:15,570
together the event and it's complement make all possible outcomes.

4
00:00:15,632 --> 00:00:20,300
So the Law of Complements is simply one minus the probability of the event.

5
00:00:20,368 --> 00:00:22,750
Why is the complement useful?

6
00:00:22,832 --> 00:00:25,920
It is sometimes easier to calculate the complement,

7
00:00:25,984 --> 00:00:29,070
first to determine the probability of some event of interest.

8
00:00:29,136 --> 00:00:32,870
For example, let's say that you are tossing two dice

9
00:00:32,944 --> 00:00:34,750
and want to know the probability,

10
00:00:34,816 --> 00:00:36,970
that each die will show a different value,

11
00:00:37,056 --> 00:00:39,900
such as a two and a three or four and a five.

12
00:00:39,968 --> 00:00:45,500
The number of possibilities is quite large would it be easier to ask

13
00:00:45,568 --> 00:00:49,620
what is the probability of obtaining the same value on both dice,

14
00:00:49,680 --> 00:00:53,420
this is the complement to the first question and in this case

15
00:00:53,488 --> 00:00:57,100
the complement only contains six possible outcomes

16
00:00:57,184 --> 00:01:05,170
so much simpler. The probability of A complement is 6 over 36 or 1/6,

17
00:01:05,232 --> 00:01:07,700
because we know that the probability

18
00:01:07,792 --> 00:01:11,050
of A and the the probability of A complement sum to 1.

19
00:01:11,104 --> 00:01:14,150
We can calculate the probability of having different values

20
00:01:14,224 --> 00:01:18,370
on each die by subtracting the probability of the complement from 1

21
00:01:18,448 --> 00:01:23,750
for a result of 5/6. Remember that it's sometimes easier to calculate,

22
00:01:23,824 --> 00:01:27,620
the complement to find the probability of what you're really interested in.

