0
00:00:01,070 --> 00:00:04,080
In this module, we will be introducing you to concepts related

1
00:00:04,080 --> 00:00:06,070
to basic probability.

2
00:00:06,070 --> 00:00:08,810
Probability is an estimation that some of that will

3
00:00:08,810 --> 00:00:10,830
occur in a random experiment.

4
00:00:10,830 --> 00:00:13,660
For example, imagine that you are flipping a coin,

5
00:00:13,660 --> 00:00:17,290
that's your experiment, and you want to toss heads, your event.

6
00:00:17,290 --> 00:00:20,270
The probability of tossing heads is based on the sample space,

7
00:00:20,270 --> 00:00:22,830
which is the set of all possible outcomes.

8
00:00:22,830 --> 00:00:27,280
In our coin flipping experiment, the sample space is heads or tails.

9
00:00:27,280 --> 00:00:29,580
The outcome of your experiment is simply the result,

10
00:00:29,580 --> 00:00:33,440
whether you flip a head or you flip a tail.

11
00:00:33,440 --> 00:00:36,230
When we repeat a random experiment several times,

12
00:00:36,230 --> 00:00:38,410
each iteration is called a trial.

13
00:00:38,410 --> 00:00:40,230
In the example of tossing a coin,

14
00:00:40,230 --> 00:00:43,510
each trial will result in either a head or a tail.

15
00:00:43,510 --> 00:00:46,530
Our goal then is to assign a probability of a certain

16
00:00:46,530 --> 00:00:47,530
events of interest.

17
00:00:47,530 --> 00:00:50,640
This is define by set E, the set of outcomes or

18
00:00:50,640 --> 00:00:53,270
results that we are hoping to achieve.

19
00:00:53,270 --> 00:00:57,260
So, in our case of flipping heads, that's 0.5.

20
00:00:57,260 --> 00:00:59,892
Similarly, if you're tossing die,

21
00:00:59,892 --> 00:01:03,683
your sample space is the set of all possible outcomes.

22
00:01:03,683 --> 00:01:07,471
If the outcome of interest is rolling an odd number,

23
00:01:07,471 --> 00:01:10,063
your site of events is 1, 3, 5.

24
00:01:10,063 --> 00:01:11,728
Assuming if they are die,

25
00:01:11,728 --> 00:01:15,221
the probability of rolling any number is one-sixth.

26
00:01:15,221 --> 00:01:19,734
So, the probability of rolling an odd number is the sum of

27
00:01:19,734 --> 00:01:23,880
the probabilities of rolling out 1, 3 or 5.5.

