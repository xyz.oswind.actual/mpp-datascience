0
00:00:01,120 --> 00:00:04,110
In this video, we're gonna introduce you to the very important concept of

1
00:00:04,110 --> 00:00:05,940
conditional probability.

2
00:00:05,940 --> 00:00:09,222
So if you look right here, it says, the probability of A given B,

3
00:00:09,222 --> 00:00:11,205
that's what the vertical line means.

4
00:00:11,205 --> 00:00:13,821
In other words, what's the probability that event A will occur

5
00:00:13,821 --> 00:00:15,940
once you're told that B has occurred?

6
00:00:15,940 --> 00:00:18,530
And the answer is, you take the probability of both A and

7
00:00:18,530 --> 00:00:21,130
B occurring divided by the probability of B.

8
00:00:21,130 --> 00:00:23,130
And I think we'll see this when we go through our examples,

9
00:00:23,130 --> 00:00:24,680
that it makes sense.

10
00:00:24,680 --> 00:00:27,070
Now, we can relate this to independence,

11
00:00:27,070 --> 00:00:30,210
which we just talked about in the previous videos.

12
00:00:30,210 --> 00:00:33,761
Events A and B are independent if the probability of, if and only if,

13
00:00:33,761 --> 00:00:36,469
the probability of A given B is the probability of A.

14
00:00:36,469 --> 00:00:38,273
In other words, if I tell you B occurs,

15
00:00:38,273 --> 00:00:40,969
it doesn't change your view of the chance of A occurring.

16
00:00:42,020 --> 00:00:45,318
And also, the probability of B given A is the probability of B.

17
00:00:45,318 --> 00:00:47,040
In other words, if I tell you A occurs,

18
00:00:47,040 --> 00:00:50,630
it doesn't change your view of the chance of B occurring.

19
00:00:50,630 --> 00:00:54,370
And you could rearrange this equation up here one of two ways.

20
00:00:54,370 --> 00:00:57,684
You can say the probability of A and B, if you multiply by probability

21
00:00:57,684 --> 00:01:00,787
of B, is the probability of B times the probability of A given B.

22
00:01:00,787 --> 00:01:03,902
In other words, to find the probability of two events occurring,

23
00:01:03,902 --> 00:01:07,129
take the probability of one of them times the probability of the other

24
00:01:07,129 --> 00:01:08,380
given the first to occur.

25
00:01:08,380 --> 00:01:10,190
And you could also flip it this way.

26
00:01:10,190 --> 00:01:11,312
The probability that A and

27
00:01:11,312 --> 00:01:13,913
B occurred is the probability that the first one occurs times

28
00:01:13,913 --> 00:01:15,959
the probability of the second given the first.

29
00:01:15,959 --> 00:01:18,564
And usually, one of these is easier to do than the other,

30
00:01:18,564 --> 00:01:21,512
that will be very important when we talk about Bayes' theorem.

31
00:01:21,512 --> 00:01:24,139
In other words, you want the probability that two events occur,

32
00:01:24,139 --> 00:01:26,814
you can take the probability of one of them times the probability of

33
00:01:26,814 --> 00:01:28,460
the second given the first.

34
00:01:28,460 --> 00:01:29,950
Or the probability of the second times

35
00:01:29,950 --> 00:01:31,840
the probability of the first given the second.

36
00:01:31,840 --> 00:01:35,290
Either way is fine, just work with the information you have.

37
00:01:35,290 --> 00:01:37,240
So let's look at two examples here.

38
00:01:37,240 --> 00:01:39,163
In this case, A and B are not independent.

39
00:01:39,163 --> 00:01:41,978
And suppose you want the probability of A given B.

40
00:01:41,978 --> 00:01:43,938
And so, again, the probability of A and

41
00:01:43,938 --> 00:01:46,000
B divided by the probability of B.

42
00:01:46,000 --> 00:01:49,150
So there are 20 points in the sample space, five times four.

43
00:01:49,150 --> 00:01:50,318
How many of these are As?

44
00:01:50,318 --> 00:01:51,785
One, two, three, four.

45
00:01:51,785 --> 00:01:53,183
One, two, three, four.

46
00:01:53,183 --> 00:01:59,860
So the probability of A Will be 8/20.

47
00:02:02,987 --> 00:02:04,750
Now, let's get the probability of B.

48
00:02:08,930 --> 00:02:10,786
How many Bs do we see here?

49
00:02:10,786 --> 00:02:14,180
I think we see four.

50
00:02:14,180 --> 00:02:17,339
Okay, so now what's the probability of a given B?

51
00:02:20,491 --> 00:02:24,715
In other words, I told you we're in B, okay, which is the yellow here,

52
00:02:24,715 --> 00:02:27,370
does that change your chance of A occurring?

53
00:02:30,650 --> 00:02:32,600
Well, we need the probability of A and B.

54
00:02:32,600 --> 00:02:34,110
That's the numerator here.

55
00:02:34,110 --> 00:02:35,300
We've got the probability of B.

56
00:02:38,240 --> 00:02:41,049
The probability of A and B, we just have to look here.

57
00:02:42,130 --> 00:02:44,338
Okay, there's one point that's A and B out of 20.

58
00:02:47,039 --> 00:02:50,069
So the probability of A given B is the probability they both occur,

59
00:02:50,069 --> 00:02:51,430
which is one of those points.

60
00:02:53,400 --> 00:02:56,600
Again, Excel loves to think it's a date, so I'll do it like this.

61
00:02:58,350 --> 00:03:01,598
Then I would divide by the probability of B, the given event.

62
00:03:01,598 --> 00:03:04,766
And that's 4/20.

63
00:03:04,766 --> 00:03:09,032
So 120 divided by 4/20 is gonna be 1/4.

64
00:03:15,355 --> 00:03:18,739
Okay, so now when I tell you that B occurs,

65
00:03:18,739 --> 00:03:22,150
the probability of A given B is now 1/4.

66
00:03:22,150 --> 00:03:26,525
Okay, but the probability of A originally was 0.4, 8/20.

67
00:03:26,525 --> 00:03:27,458
So what's changed?

68
00:03:27,458 --> 00:03:30,098
In other words, once I tell you that B has occurred,

69
00:03:30,098 --> 00:03:31,300
it makes A less likely.

70
00:03:31,300 --> 00:03:33,290
If you look at the picture you can see that.

71
00:03:33,290 --> 00:03:37,340
When I know it's yellow it's much less likely that it's orange.

72
00:03:37,340 --> 00:03:39,670
So basically, those are not independent events, and

73
00:03:39,670 --> 00:03:42,400
we've illustrated concept of conditional probability.

74
00:03:42,400 --> 00:03:43,931
Now, here, A and B are independent.

75
00:03:43,931 --> 00:03:47,740
And we'll be able to check that by using conditional probability.

76
00:03:47,740 --> 00:03:50,690
So lets do the same thing we did before.

77
00:03:50,690 --> 00:03:52,502
We want the probability of A and B.

78
00:03:55,518 --> 00:03:58,000
Or probably, we want the probability of A given B.

79
00:04:00,440 --> 00:04:03,040
Okay, so let's work from the probability of A.

80
00:04:04,950 --> 00:04:07,140
So how many As do I see there?

81
00:04:07,140 --> 00:04:09,289
I see 8 out of 20.

82
00:04:09,289 --> 00:04:12,961
Okay, again, there's 20 points in the sample space there, 4 times 5.

83
00:04:15,559 --> 00:04:17,593
Now let's find the probability of B.

84
00:04:20,451 --> 00:04:22,670
It looks like I've got five Bs.

85
00:04:24,150 --> 00:04:25,767
And how many times do I see A and B?

86
00:04:25,767 --> 00:04:27,179
Well, that is different here.

87
00:04:29,817 --> 00:04:32,360
There are two points that are As and Bs, right?

88
00:04:32,360 --> 00:04:34,289
You see them right here.

89
00:04:34,289 --> 00:04:37,012
Okay, so probability of A and B is 2/20.

90
00:04:39,390 --> 00:04:46,392
Okay, so now the probability of A given B Is the probability of both.

91
00:04:48,800 --> 00:04:52,087
Okay, so A and B is gonna be 2/20.

92
00:04:55,980 --> 00:04:58,812
Again, Excel thinks it's a date, February 20th.

93
00:05:01,314 --> 00:05:05,305
Divided by the probability of B there, which is going to be

94
00:05:08,516 --> 00:05:13,202
5/20.

95
00:05:13,202 --> 00:05:17,075
And that's 2/5, Okay?

96
00:05:17,075 --> 00:05:18,185
Which is 0.4.

97
00:05:20,764 --> 00:05:23,308
So probability of A given B is 0.4, but

98
00:05:23,308 --> 00:05:27,833
the original probability of A was 0.4, so the A and B are independent.

99
00:05:27,833 --> 00:05:30,356
In other words, when I told you that B occurred,

100
00:05:30,356 --> 00:05:32,720
it didn't change the chance of A occurring.

101
00:05:32,720 --> 00:05:35,810
So conditional probability can be used to show independence but

102
00:05:35,810 --> 00:05:38,580
it's actually a much more powerful concept.

103
00:05:38,580 --> 00:05:41,722
So in the next video we're gonna do two sort of

104
00:05:41,722 --> 00:05:45,619
more realistic examples of conditional probability.

