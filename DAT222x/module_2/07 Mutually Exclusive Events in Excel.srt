0
00:00:00,975 --> 00:00:04,750
In this video, we're going to discuss two Important Concepts and Probability.

1
00:00:04,750 --> 00:00:07,350
The concept of Mutually Exclusive Events,

2
00:00:07,350 --> 00:00:11,100
and then the Addition Rule for probability, which lets us determine

3
00:00:11,100 --> 00:00:15,125
the probability that Event A or Event B will occur.

4
00:00:15,120 --> 00:00:18,275
So, let's talk about Mutually Exclusive Events first.

5
00:00:18,270 --> 00:00:21,950
So two events are mutually exclusive, if the occurrence of one event

6
00:00:21,950 --> 00:00:23,725
precludes the occurrence of the other events.

7
00:00:23,720 --> 00:00:27,000
So some examples, let event A be the event that the

8
00:00:27,044 --> 00:00:32,400
2020 Houston Texans win the Super Bowl, let event B be the event that the

9
00:00:32,400 --> 00:00:35,125
Cleveland Browns win the 2020 Super Bowl.

10
00:00:35,120 --> 00:00:39,050
These are mutually exclusive, because of the Texans win the Browns can't win

11
00:00:39,050 --> 00:00:41,575
and if the Browns win the Texans can't win.

12
00:00:41,570 --> 00:00:44,500
Now, neither of them will probably win but that's a different story.

13
00:00:44,500 --> 00:00:47,780
Now here are two events that are not mutually exclusive.

14
00:00:47,780 --> 00:00:51,075
Let event A be the event that the Dow Jones Index

15
00:00:51,070 --> 00:00:53,800
goes up at least 20% in 2020,

16
00:00:53,800 --> 00:00:59,150
an event B the event that Microsoft Stock  goes up at least 15% in 2020.

17
00:00:59,150 --> 00:01:03,711
Now those can both happen, the Dow could do really well in 2020

18
00:01:03,820 --> 00:01:06,625
and probably that would mean Microsoft would also do pretty well,

19
00:01:06,625 --> 00:01:09,000
so these are certainly not mutually exclusive.

20
00:01:09,000 --> 00:01:13,125
So, let's do some more examples, the mutually understanding of this concept.

21
00:01:13,125 --> 00:01:16,320
So here we have 12 points in the sample space here,

22
00:01:16,320 --> 00:01:19,650
we've got four rows and three columns, assume each point has the same

23
00:01:19,650 --> 00:01:21,825
probability which would mean 1/12.

24
00:01:21,825 --> 00:01:26,600
So let event A be the Xs here, that has a chance 3/12,

25
00:01:26,600 --> 00:01:31,475
event B are the orange ones that has probability 3/12,

26
00:01:31,470 --> 00:01:34,950
because there's three orange cells and then Event C is the Blue.

27
00:01:34,950 --> 00:01:38,825
That's got probably two-twelfths because two of those cells are Blue.

28
00:01:38,820 --> 00:01:42,175
Now, which of these are vectors of events are mutually exclusive than

29
00:01:42,175 --> 00:01:45,900
which or not? Well, A and B are Not mutually exclusive, Why?

30
00:01:45,900 --> 00:01:49,600
Because if you look at the X and the oranges they overlap right here.

31
00:01:49,600 --> 00:01:53,550
Okay, they could both occur. Now A and C are mutually exclusive, Why?

32
00:01:53,550 --> 00:01:58,200
The Xs and the blue don't overlap. B and C mutually exclusive, Why?

33
00:01:58,200 --> 00:02:00,550
Because the orange and blue don't overlap.

34
00:02:00,550 --> 00:02:04,000
Okay, so now we can see how mutually exclusive this concept

35
00:02:04,000 --> 00:02:07,625
ties into the Addition Rule getting the probability of A or B.

36
00:02:07,625 --> 00:02:11,775
So for any two events A or B, how do you get the probability of A or B?

37
00:02:11,770 --> 00:02:15,400
Well, you take the probability of A plus probability of B, but you have to

38
00:02:15,400 --> 00:02:17,375
subtract off the probably they both happen

39
00:02:17,375 --> 00:02:20,300
which is called the intersection or else you'll double count things,

40
00:02:20,300 --> 00:02:23,350
Okay. Now if you have mutually exclusive events,

41
00:02:23,375 --> 00:02:26,875
then probably A and B is 0, because there is no intersection.

42
00:02:26,870 --> 00:02:29,825
So in that case if X and Y are mutually exclusive,

43
00:02:29,825 --> 00:02:34,125
the probability of X or Y is the probably the X plus the probability of Y.

44
00:02:34,120 --> 00:02:39,350
So again some examples, ok what's the probability of A or B

45
00:02:39,350 --> 00:02:44,950
with our definitions up here. Well, again A and B are not mutually exclusive.

46
00:02:44,950 --> 00:02:48,200
So you take the probably of A which we know is 3/12

47
00:02:48,200 --> 00:02:50,675
the probably it B which we know is 3/12

48
00:02:50,670 --> 00:02:53,125
you've got to subtract off the probably of A and B,

49
00:02:53,125 --> 00:02:56,500
so that's where there's an orange X and that's 1/12.

50
00:02:56,525 --> 00:02:58,525
So your answer there would be 5/12,

51
00:02:58,525 --> 00:03:02,325
In other words what's the chance that something would be an X or an orange,

52
00:03:02,325 --> 00:03:09,225
well you got to basically not double count this X right here which is orange

53
00:03:09,225 --> 00:03:13,425
okay. Now what's the probably of A or C, now those are mutually exclusive,

54
00:03:13,425 --> 00:03:17,800
so A and C can't occur together, so that last term drops out.

55
00:03:17,800 --> 00:03:22,150
So the probably of A or C would be the probably of A which is 3/12,

56
00:03:22,150 --> 00:03:27,850
supposed to probably of C which is 2/12. OK, two more  practical examples,

57
00:03:27,850 --> 00:03:31,550
If you throw two dice, what's the chance you get at least one four?

58
00:03:31,550 --> 00:03:34,250
So, here's the sample space, okay.

59
00:03:34,250 --> 00:03:37,600
This is what you throw in the first die, this is what you throw in the second die.

60
00:03:37,600 --> 00:03:41,125
So the first die being a four, is this row right here

61
00:03:41,125 --> 00:03:45,650
and the second die being a four, is this column right here

62
00:03:45,650 --> 00:03:49,500
and basically getting a four on both is where the X is.

63
00:03:49,500 --> 00:03:51,975
Okay, so what's probably getting a four on the first die

64
00:03:51,970 --> 00:03:55,225
or four on the second die? That's the chance of getting at least one four.

65
00:03:55,225 --> 00:03:58,070
So you add up the chance that you get a form the first die,

66
00:03:58,075 --> 00:04:01,050
which is 6 over 36, this row right here

67
00:04:01,050 --> 00:04:05,375
add on the chance that you get a 4 on the second die, that's 6 over 36

68
00:04:05,375 --> 00:04:09,000
and then subtract off the intersection the probably you get a 4 on both die

69
00:04:09,000 --> 00:04:11,150
which is this X here 1 over 36.

70
00:04:11,150 --> 00:04:16,950
And so, you get a final probability of at least one four being 11 over 36

71
00:04:16,950 --> 00:04:19,700
note if you hadn't subtracted of the 1 over 36

72
00:04:19,700 --> 00:04:22,600
you would have got 12 over 36 which would be incorrect.

73
00:04:22,600 --> 00:04:25,475
Okay, now one final example here,

74
00:04:25,470 --> 00:04:29,850
suppose you let Event A, be the event that it rains on Saturday

75
00:04:29,850 --> 00:04:33,650
and event B be the event that it rains on Sunday

76
00:04:33,650 --> 00:04:37,450
and let's suppose there's a 50% chance of rain on Saturday

77
00:04:37,450 --> 00:04:39,950
and a 50% chance of rain on Sunday.

78
00:04:39,950 --> 00:04:43,025
Is it sure to rain on the weekend? Now most people say yes,

79
00:04:43,025 --> 00:04:47,250
because 50% plus 50% is 100%. Well, that's not correct.

80
00:04:47,250 --> 00:04:49,325
What's the probably it rains on the weekend.

81
00:04:49,325 --> 00:04:51,800
It's probably it rains on Saturday or rains on Sunday

82
00:04:51,800 --> 00:04:56,875
That's a probably of rain on Saturday P(A)+P(B),

83
00:04:56,870 --> 00:05:00,350
which is 50% each of these is 50%

84
00:05:00,350 --> 00:05:03,375
minus the chance it rains on both Saturday and Sunday.

85
00:05:03,370 --> 00:05:07,000
So You've got 0.5 plus 0.5 minus something that's positive,

86
00:05:07,000 --> 00:05:08,950
because it could rain on both Saturday and Sunday.

87
00:05:08,950 --> 00:05:12,275
So basically this will be less than 1 because you have 1 minus,

88
00:05:12,275 --> 00:05:13,975
something which will be less than 1.

89
00:05:13,970 --> 00:05:16,530
So it's not sure to rain on the weekend although if you live in

90
00:05:16,530 --> 00:05:19,775
Seattle, you probably think it is sure to rain on the weekend.

91
00:05:19,770 --> 00:05:24,500
So in our next video we'll pick up with the study of Independent Events.

