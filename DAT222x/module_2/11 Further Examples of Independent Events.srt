0
00:00:00,430 --> 00:00:03,880
So let's talk about two more complicated examples

1
00:00:03,880 --> 00:00:06,770
of probabilities involving independent events.

2
00:00:06,770 --> 00:00:10,120
So remember we said the probability of A and B is probability of A times

3
00:00:10,120 --> 00:00:12,820
probability of B if the events are independent.

4
00:00:12,820 --> 00:00:15,470
You can generalize this if you have more than two events.

5
00:00:15,470 --> 00:00:18,460
So if you have four events, the probability of A, B, and C, and

6
00:00:18,460 --> 00:00:19,930
D happening together,

7
00:00:19,930 --> 00:00:22,400
you just multiply the individual probabilities.

8
00:00:22,400 --> 00:00:26,060
So that will be useful, that idea, in our second example.

9
00:00:26,060 --> 00:00:30,000
But let's solve this interesting problem two different ways.

10
00:00:30,000 --> 00:00:32,170
We have two independent systems, or machines,

11
00:00:32,170 --> 00:00:34,380
that each have a 90% chance of working.

12
00:00:34,380 --> 00:00:37,470
What's the chance at least one system works?

13
00:00:37,470 --> 00:00:41,910
Okay, so basically, we can do this as basically

14
00:00:41,910 --> 00:00:44,960
the probability of A is the event the first system works.

15
00:00:49,680 --> 00:00:51,910
And B is the event the second system works.

16
00:00:51,910 --> 00:00:53,140
We want the probability they both work.

17
00:00:56,260 --> 00:00:57,520
Or sorry, least one works.

18
00:00:57,520 --> 00:01:04,850
So the probability that at least one works, This is a little tricky.

19
00:01:04,850 --> 00:01:07,930
Use the law of complements, we can combine things.

20
00:01:07,930 --> 00:01:10,590
One minus the probability that no system works.

21
00:01:13,857 --> 00:01:16,250
Okay, now the probability no system works.

22
00:01:19,158 --> 00:01:23,940
By independence, we can multiply the probability that both systems fail.

23
00:01:29,321 --> 00:01:32,190
Times the probability system two fails.

24
00:01:34,643 --> 00:01:37,967
Okay, since each system has a 90% chance of working,

25
00:01:37,967 --> 00:01:40,870
they have a 1% chance of each failing.

26
00:01:40,870 --> 00:01:44,460
So that's gonna be 0.1 times 0.1 or 0.01.

27
00:01:44,460 --> 00:01:48,906
So the probability at least one system works is gonna be 1-0.1.

28
00:01:48,906 --> 00:01:52,653
Now we use the independence here and the role of compliments here but

29
00:01:52,653 --> 00:01:56,180
sometimes you gotta combine these things so you get 0.99.

30
00:01:56,180 --> 00:02:00,170
So there is a 99% chance at least one system works.

31
00:02:00,170 --> 00:02:04,030
Now we can do this a different way, hopefully we'll get the same answer.

32
00:02:04,030 --> 00:02:06,570
Okay, we can do the probability of A or B.

33
00:02:06,570 --> 00:02:09,050
So in other words, probability at least one system works,

34
00:02:09,050 --> 00:02:12,550
is the probability the first system works or the second system works.

35
00:02:15,712 --> 00:02:19,788
Now that would be basically the probability of A,

36
00:02:19,788 --> 00:02:23,970
by our addition rules, plus the probability of B,

37
00:02:27,007 --> 00:02:31,580
Minus the probability of A and B.

38
00:02:31,580 --> 00:02:34,304
Okay, now probability the first system works is .9.

39
00:02:34,304 --> 00:02:38,321
The probability of the second system works is .9.

40
00:02:38,321 --> 00:02:40,649
The probability they both work since they're independent,

41
00:02:40,649 --> 00:02:42,089
I can multiply their probabilities.

42
00:02:44,120 --> 00:02:50,342
So that's the 0.9 plus 0.9, a lot of 0.9s there, minus 0.9 times 0.9,

43
00:02:50,342 --> 00:02:56,510
that's 1.8 minus 0.81 which is 0.99, same answer thankfully.

44
00:02:56,510 --> 00:03:00,640
Let's do this final tricky problem here on independence.

45
00:03:00,640 --> 00:03:03,280
And then we'll talk about conditional probability, which

46
00:03:03,280 --> 00:03:07,270
is a generalization of independence, but very, very important.

47
00:03:07,270 --> 00:03:10,050
Okay, so we have three machines, A, B, and C.

48
00:03:10,050 --> 00:03:12,397
They each work the following fraction of the time.

49
00:03:12,397 --> 00:03:15,341
Machine A works 95% of the time, machine B,

50
00:03:15,341 --> 00:03:19,200
90% of the time, machine C, 92% of the time.

51
00:03:19,200 --> 00:03:21,540
What's the chance at least one machine is working?

52
00:03:21,540 --> 00:03:22,970
Again, law of compliments.

53
00:03:25,060 --> 00:03:28,141
Probably at least one working, that's complicated.

54
00:03:28,141 --> 00:03:32,010
Because it could be just A, it could be just B, it could be A and B.

55
00:03:32,010 --> 00:03:33,850
Probably at least one machine works.

56
00:03:35,950 --> 00:03:41,923
That's one minus the probability no machines work, and the probability

57
00:03:41,923 --> 00:03:47,800
no machines work, Would be the probability they all fail.

58
00:03:51,693 --> 00:03:56,341
And then I can just multiply the probably that each fail which is 1

59
00:03:56,341 --> 00:04:01,413
minus 0.95 for the first machine, independents lets me do this and

60
00:04:01,413 --> 00:04:05,240
we're assuming independents, times 1- .92.

61
00:04:05,240 --> 00:04:10,579
Now, what that would be would

62
00:04:10,579 --> 00:04:16,993
be .05 times .1 times .08.

63
00:04:16,993 --> 00:04:20,780
And so the probability is 0 machines work as this.

64
00:04:20,780 --> 00:04:22,541
I can show you that formula there.

65
00:04:25,299 --> 00:04:29,260
And then I would just take one minus that to get my answer probably at

66
00:04:29,260 --> 00:04:33,436
least one machine works as one minus the probability that none work.

67
00:04:33,436 --> 00:04:38,151
So there's 99.96% chance here at least one machine will work

68
00:04:38,151 --> 00:04:39,730
which is pretty good.

69
00:04:40,798 --> 00:04:43,250
So again in the next video will start talking about

70
00:04:43,250 --> 00:04:45,840
conditional probability which is very important and

71
00:04:45,840 --> 00:04:48,590
generalizes the concept of independent events.

