0
00:00:00,780 --> 00:00:03,670
Base theorem builds from the law of total probability.

1
00:00:03,670 --> 00:00:06,940
It describes the probability of an event based on prior knowledge

2
00:00:06,940 --> 00:00:09,540
of conditions that might be related to the event.

3
00:00:09,540 --> 00:00:13,430
For example, if cancer is related to age then using base theorem

4
00:00:13,430 --> 00:00:16,830
a person's age can be used to more accurately assess the probability

5
00:00:16,830 --> 00:00:19,870
that they have cancer compared to the assessment of the probability of

6
00:00:19,870 --> 00:00:23,020
cancer made without knowledge of the persons' age.

7
00:00:23,020 --> 00:00:26,040
So the formula looks like this.

8
00:00:26,040 --> 00:00:26,648
Did your head explode?

9
00:00:26,648 --> 00:00:28,930
Let's simplify this a bit.

10
00:00:28,930 --> 00:00:30,820
Remember our video game example?

11
00:00:30,820 --> 00:00:32,720
Let's take a closer look.

12
00:00:32,720 --> 00:00:36,600
So what is the probability that a randomly selected gamer is male?

13
00:00:36,600 --> 00:00:38,980
This time you're told that you have a gamer and

14
00:00:38,980 --> 00:00:42,500
ask to find the probability that the gamer is also male,

15
00:00:42,500 --> 00:00:46,780
there are 41 male gamers out of 69 total gamers.

16
00:00:46,780 --> 00:00:49,960
So 41 divide it by 69 is 59.42%.

17
00:00:49,960 --> 00:00:53,438
You have just worked Bayes' theorem problem.

18
00:00:53,438 --> 00:00:55,810
You didn't realize it, did you?

19
00:00:55,810 --> 00:00:57,441
A Bayes' problem can be set up so

20
00:00:57,441 --> 00:01:00,595
it appears to be just like another conditional probability.

