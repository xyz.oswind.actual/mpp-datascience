0
00:00:00,629 --> 00:00:03,019
Let&#39;s talk a little bit more about intersections and

1
00:00:03,019 --> 00:00:05,180
the associated joint probabilities.

2
00:00:05,180 --> 00:00:08,590
If the outcome is in both A and B, you have an intersection for

3
00:00:08,590 --> 00:00:11,180
which you can calculate a joint probability.

4
00:00:11,180 --> 00:00:14,670
The probability of an intersection is denoted as you see here.

5
00:00:14,670 --> 00:00:16,810
Do you see that little upside down U?

6
00:00:16,810 --> 00:00:19,430
I like to think of that as the A in and.

7
00:00:19,430 --> 00:00:22,470
Because this helps me remember that when I see this,

8
00:00:22,470 --> 00:00:25,950
it means that both A and B occur.

9
00:00:25,950 --> 00:00:27,660
Remember that if events A and

10
00:00:27,660 --> 00:00:31,130
B are mutually exclusive, one cannot occur if the other does.

11
00:00:31,130 --> 00:00:33,389
The joint probability in that case is zero.

12
00:00:33,389 --> 00:00:35,409
When calculating the joint probability,

13
00:00:35,409 --> 00:00:38,210
you multiply the probabilities of each event.

14
00:00:38,210 --> 00:00:41,610
For example, let&#39;s imagine we ask a question of 100 people,

15
00:00:41,610 --> 00:00:43,610
do you play video games?

16
00:00:43,610 --> 00:00:45,880
Here are the results that we obtained.

17
00:00:45,880 --> 00:00:48,640
So let&#39;s ask ourselves some questions of this data.

18
00:00:48,640 --> 00:00:51,200
What is the probability of a randomly selected individual

19
00:00:51,200 --> 00:00:52,120
being male?

20
00:00:52,120 --> 00:00:55,730
This is the total number of males divided by the total overall.

21
00:00:55,730 --> 00:00:57,630
So 60 divided by 100.

22
00:00:57,630 --> 00:00:59,900
Since no mention is made of game playing or

23
00:00:59,900 --> 00:01:02,250
not, it includes all cases.

24
00:01:02,250 --> 00:01:05,350
This is what we call a marginal probability.

25
00:01:05,349 --> 00:01:08,525
What is the probability of a randomly selected individual playing

26
00:01:08,525 --> 00:01:09,330
video games?

27
00:01:09,330 --> 00:01:13,120
Again, no mention is made of gender, so this is a marginal probability.

28
00:01:13,120 --> 00:01:15,386
69 divided by 100.

29
00:01:15,386 --> 00:01:18,520
Now, let&#39;s ask a more interesting question.

30
00:01:18,520 --> 00:01:21,560
What is the probability of a randomly selected individual being

31
00:01:21,560 --> 00:01:24,070
a male who plays video games?

32
00:01:24,070 --> 00:01:26,370
This is a joint probability.

33
00:01:26,370 --> 00:01:30,080
The number of male and yes, is divided by the total number.

34
00:01:30,080 --> 00:01:32,790
So 41 divided by 100.

35
00:01:32,790 --> 00:01:34,230
You can do the same for women.

36
00:01:34,230 --> 00:01:37,010
You would have 28 divide by 100.

37
00:01:37,010 --> 00:01:39,970
Now, the interesting thing here is,

38
00:01:39,970 --> 00:01:43,150
let&#39;s imagine we didn&#39;t have any of the numbers in the middle.

39
00:01:43,150 --> 00:01:45,148
How do we calculate joint probabilities?

40
00:01:45,148 --> 00:01:47,240
How do we get those middle bits?

41
00:01:47,240 --> 00:01:49,160
The rule of multiplication applies here.

42
00:01:50,210 --> 00:01:53,990
So what you have to do is you look at the intersection of two events.

43
00:01:53,990 --> 00:01:57,260
That is we want to know the probability that events A and

44
00:01:57,260 --> 00:01:58,340
B both occur.

45
00:01:59,370 --> 00:02:03,090
So, let&#39;s take a look at what happens here.

46
00:02:03,090 --> 00:02:06,132
Both yes and men, you get 0.41 or

47
00:02:06,132 --> 00:02:09,860
41% of the sample, as we saw in the original chart.

48
00:02:09,860 --> 00:02:12,470
We can fill out the remaining chart doing the same thing.

49
00:02:13,820 --> 00:02:17,013
So we get exactly what we started with.

