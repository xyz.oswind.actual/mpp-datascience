0
00:00:00,850 --> 00:00:03,735
In this video, we'll show you how to create a box plot that doesn't just

1
00:00:03,735 --> 00:00:05,850
summarize multiple populations,

2
00:00:05,850 --> 00:00:08,810
but each population can have more than one variable.

3
00:00:08,810 --> 00:00:13,176
So we have four high schools, Columbus East, Columbus North, BHSS,

4
00:00:13,176 --> 00:00:17,520
BHSN,, and we've tracked how the students did on achievement tests

5
00:00:17,520 --> 00:00:19,018
on English, Math and History.

6
00:00:19,018 --> 00:00:23,100
And we wanna see quickly visually a summary of how the students at these

7
00:00:23,100 --> 00:00:25,710
different schools, did on these different subjects.

8
00:00:25,710 --> 00:00:27,400
So we need to select all of the data.

9
00:00:27,400 --> 00:00:31,309
We could do Ctrl+Shift+right arrow down, nothing wrong with that.

10
00:00:31,309 --> 00:00:34,412
But a quicker way, if you want the whole range and

11
00:00:34,412 --> 00:00:38,285
you have your cursor in a cell in that range is to do Ctrl+*.

12
00:00:38,285 --> 00:00:40,300
So I am doing Ctrl+*.

13
00:00:40,300 --> 00:00:42,365
And it selects the whole range.

14
00:00:42,365 --> 00:00:47,070
That's a little quicker, so I can do insert and I can do a Box plot here.

15
00:00:47,070 --> 00:00:51,350
Statistics chart, you'll learn about this chart here the Pareto chart

16
00:00:51,350 --> 00:00:53,720
in a later lesson in this module.

17
00:00:53,720 --> 00:00:55,840
So now I'll box plot,

18
00:00:55,840 --> 00:00:58,440
now again I'll like to make this plot it shows up much better.

19
00:01:00,480 --> 00:01:04,389
So now you can see the blue is Columbia's East,

20
00:01:04,389 --> 00:01:09,277
the red is Columbia's North then we have orange for BHSN and

21
00:01:09,277 --> 00:01:11,644
a different color for BHSS.

22
00:01:11,644 --> 00:01:14,718
And then we have the English, Math and we have the History,

23
00:01:14,718 --> 00:01:16,150
things that we summarize.

24
00:01:16,150 --> 00:01:19,780
So we have four boxes for English, one for each high school,

25
00:01:19,780 --> 00:01:24,130
four boxes for Math, box plots for Math, four box plots for History.

26
00:01:24,130 --> 00:01:28,420
Now what we see here is the one that's Columbus North,

27
00:01:28,420 --> 00:01:32,410
the second one from the left, it always seems to be higher.

28
00:01:32,410 --> 00:01:33,740
That indicates in each subject,

29
00:01:33,740 --> 00:01:36,378
Columbus North seems to be the best school.

30
00:01:36,378 --> 00:01:37,840
And unfortunately,

31
00:01:37,840 --> 00:01:40,710
BHSN, which is in my home town, I should have changed the data.

32
00:01:42,030 --> 00:01:46,250
For each subject, English, Math, and History, BHSN appears to be lower,

33
00:01:46,250 --> 00:01:47,130
in other words,

34
00:01:47,130 --> 00:01:49,800
their scores are worse than the other three high schools.

35
00:01:49,800 --> 00:01:53,558
So we've got over here 500 rows times 4 columns of data which

36
00:01:53,558 --> 00:01:57,314
is difficult to make sense of, but when we put this in a box plot,

37
00:01:57,314 --> 00:02:01,307
we can seconds quickly visually see what's going on with the data.

38
00:02:01,307 --> 00:02:05,325
And I think this example really shows you the power of box plots,

39
00:02:05,325 --> 00:02:08,684
which again were add it to Microsoft Excel 2016.

40
00:02:08,684 --> 00:02:11,857
They were not present in earlier versions of Excel.

41
00:02:11,857 --> 00:02:16,230
So in our next lesson, we'll turn our attention to categorical data.

