0
00:00:00,960 --> 00:00:03,860
Statisticians follow a formal process to determine whether

1
00:00:03,860 --> 00:00:05,900
the result of their analysis is meaningful,

2
00:00:05,900 --> 00:00:09,260
often referred to as statistically significant.

3
00:00:09,260 --> 00:00:13,230
This process, called hypotheses testing, consists of five steps.

4
00:00:13,230 --> 00:00:16,550
First, you must state your null and alternative hypotheses.

5
00:00:16,550 --> 00:00:19,190
The hypotheses are stated in such a way that they are mutually

6
00:00:19,190 --> 00:00:20,030
exclusive.

7
00:00:20,030 --> 00:00:23,060
That is, if one is true, the other must be false.

8
00:00:23,060 --> 00:00:25,500
Second, you formulate an analysis plan.

9
00:00:25,500 --> 00:00:28,240
The analysis plan describes how to use the sample

10
00:00:28,240 --> 00:00:30,720
data to evaluate your null hypothesis.

11
00:00:30,720 --> 00:00:33,470
It should specify the level of significance.

12
00:00:33,470 --> 00:00:34,960
Which quantifies your tolerance for

13
00:00:34,960 --> 00:00:37,570
drawing the wrong conclusion about your hypothesis.

14
00:00:37,570 --> 00:00:41,706
Often researchers choose significance levels equal to .01,

15
00:00:41,706 --> 00:00:46,140
0.5, or .10 but any value between zero and one can be used.

16
00:00:46,140 --> 00:00:49,150
The closer to one, the less willing you are to be wrong but

17
00:00:49,150 --> 00:00:53,030
the more difficult it is to find a statically significant result.

18
00:00:53,030 --> 00:00:55,570
It should also specify the test statistics,

19
00:00:55,570 --> 00:00:58,650
such as a z-test, t-test, ANOVA, Regression,

20
00:00:58,650 --> 00:01:02,400
Chi- Square just to name a few and the sampling plan.

21
00:01:02,400 --> 00:01:05,660
Next, you conduct the study and gather your data.

22
00:01:05,660 --> 00:01:09,710
Then, you analyze your data from the data you obtained a test statistic

23
00:01:09,710 --> 00:01:12,030
that might be a mean score, a proportion,

24
00:01:12,030 --> 00:01:14,940
a difference between means, a difference between proportions,

25
00:01:14,940 --> 00:01:18,890
a z-score, a t statistic, a Chi- Square, etc.

26
00:01:18,890 --> 00:01:21,040
Finally, you interpret your results.

27
00:01:21,040 --> 00:01:24,760
Each statistic has certain probabilities associated with it.

28
00:01:24,760 --> 00:01:27,510
That is the likelihood that you will have obtained it if the null

29
00:01:27,510 --> 00:01:29,280
hypothesis is true.

30
00:01:29,280 --> 00:01:33,430
If the test statistic probability is less than the significance level,

31
00:01:33,430 --> 00:01:35,730
the null hypothesis is rejected.

