0
00:00:01,390 --> 00:00:05,010
Thinking back to the first module, it seems so long ago, you

1
00:00:05,010 --> 00:00:09,210
will recall that there are two types of data, numerical and categorical.

2
00:00:09,210 --> 00:00:09,900
Up to this point,

3
00:00:09,900 --> 00:00:13,390
we have been discussing statistics that applied to numerical data.

4
00:00:13,390 --> 00:00:16,260
Now let's talk about one that applies to categorical data,

5
00:00:16,260 --> 00:00:18,090
chi square for independence.

6
00:00:18,090 --> 00:00:20,860
This statistic is used to investigate whether distributions of

7
00:00:20,860 --> 00:00:23,670
categorical variables differ from one another.

8
00:00:23,670 --> 00:00:25,231
More specifically, it tests for

9
00:00:25,231 --> 00:00:28,420
independence between two categorical variables.

10
00:00:28,420 --> 00:00:30,570
As with all other prior statistical tests,

11
00:00:30,570 --> 00:00:33,352
we need to define the null and alternative hypotheses.

12
00:00:33,352 --> 00:00:37,030
With a chi square, we are interested in researching if two categorical

13
00:00:37,030 --> 00:00:41,450
variables are related or associated, that is, if they are dependent.

14
00:00:41,450 --> 00:00:45,034
Therefore, our null hypothesis will state that they are independent and

15
00:00:45,034 --> 00:00:48,790
our alternative hypothesis will state that they are dependent.

16
00:00:48,790 --> 00:00:51,590
Instead of using the words independent and dependent, one could

17
00:00:51,590 --> 00:00:55,600
say there is no relationship between two categorical variables versus

18
00:00:55,600 --> 00:00:58,290
there is a relationship between those variables.

19
00:00:58,290 --> 00:01:01,050
Next, of course, we select our alpha level.

20
00:01:01,050 --> 00:01:02,190
Once we have gathered our data,

21
00:01:02,190 --> 00:01:05,820
we summarize that data in a two way contingency table.

22
00:01:05,820 --> 00:01:08,460
This table represents the observed counts.

23
00:01:08,460 --> 00:01:10,480
The next question we need to ask is,

24
00:01:10,480 --> 00:01:14,440
how would this table look if the two variables were not related?

25
00:01:14,440 --> 00:01:17,320
That is, what would we expect to find in our data if the two

26
00:01:17,320 --> 00:01:20,950
variables, gender and gaming for example, were independent?

27
00:01:20,950 --> 00:01:23,610
We need a table that displays what the counts would be for

28
00:01:23,610 --> 00:01:26,850
our sample data if there's no association between the variables.

29
00:01:26,850 --> 00:01:29,430
These are expected values.

30
00:01:29,430 --> 00:01:33,260
Expected values are calculated by multiplying the marginal row and

31
00:01:33,260 --> 00:01:35,887
column totals and dividing by the overall total.

32
00:01:35,887 --> 00:01:40,160
Chi square is then the sum of the expected values subtracted from

33
00:01:40,160 --> 00:01:41,940
the observed value squared,

34
00:01:41,940 --> 00:01:44,710
divided by the expected value with the degrees of freedom,

35
00:01:44,710 --> 00:01:48,060
that is the product of the number of rows minus 1.

36
00:01:48,060 --> 00:01:50,270
And the number of columns minus 1.

37
00:01:50,270 --> 00:01:52,830
So let's a look at our gaming example.

38
00:01:52,830 --> 00:01:55,770
Is there a relationship between gaming and gender?

39
00:01:55,770 --> 00:01:59,630
Here's how you calculate the expected values for each cell.

40
00:01:59,630 --> 00:02:02,830
Now we need to square the difference between the observed and

41
00:02:02,830 --> 00:02:07,070
expected value and divide by the expected value like this.

42
00:02:07,070 --> 00:02:12,590
Finally, we sum these values, obtain our chi square value of .0312

43
00:02:12,590 --> 00:02:16,150
with 1 degree of freedom because we have 2 by 2 contingency table.

44
00:02:16,150 --> 00:02:20,180
2 rows minus 1 multiplied by 2 columns minus 1.

45
00:02:20,180 --> 00:02:23,540
1 multiplied by 1 Is one.

46
00:02:23,540 --> 00:02:25,190
Next we find the critical value for

47
00:02:25,190 --> 00:02:28,521
chi square with one degree of freedom, which is 3.84.

48
00:02:28,521 --> 00:02:32,930
Because 0.0312 is less than our critical value,

49
00:02:32,930 --> 00:02:35,380
we fail to reject the null hypothesis.

50
00:02:35,380 --> 00:02:38,350
Gaming and gender are independent.

51
00:02:38,350 --> 00:02:42,100
However, if you run a chi square on that very first contingency table

52
00:02:42,100 --> 00:02:45,700
that I showed, you obtain a chi square value of 24.78,

53
00:02:45,700 --> 00:02:49,160
much bigger than the critical value of 3.84.

54
00:02:49,160 --> 00:02:50,570
But looking at this table,

55
00:02:50,570 --> 00:02:53,810
it should be obvious that there's a relationship between the variables,

56
00:02:53,810 --> 00:02:57,970
given that there are only five observed values in one of the cells.

57
00:02:57,970 --> 00:02:58,750
And that is so

58
00:02:58,750 --> 00:03:02,470
dramatically different than the values in any other cell.

59
00:03:02,470 --> 00:03:05,461
So, your challenge, should you choose to accept it,

60
00:03:05,461 --> 00:03:09,167
is to have a little fun and see if there is a relationship between diet

61
00:03:09,167 --> 00:03:11,394
and illness in this contingency table.

