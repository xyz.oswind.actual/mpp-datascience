0
00:00:00,710 --> 00:00:03,650
Let's do an example of the paired sample T test.

1
00:00:03,650 --> 00:00:06,426
Let's focus on that insulation example.

2
00:00:06,426 --> 00:00:08,290
So basically we wanna see if the New Insulation,

3
00:00:08,290 --> 00:00:11,930
would improve heating bills over an Old Insulation.

4
00:00:11,930 --> 00:00:14,430
So basically we would take ten pairs of two homes.

5
00:00:14,430 --> 00:00:18,090
In each pair the homes are roughly the same size and

6
00:00:18,090 --> 00:00:20,140
have sort of the same design.

7
00:00:20,140 --> 00:00:22,760
And they have the same heating bill last year.

8
00:00:22,760 --> 00:00:23,800
So we flip a coin for

9
00:00:23,800 --> 00:00:27,704
each pair to determine which home in each pair gets the New Insulation,

10
00:00:27,704 --> 00:00:30,390
which home in each pair gets the Old Insulation.

11
00:00:30,390 --> 00:00:32,722
We look at the change in the heating bill.

12
00:00:32,722 --> 00:00:36,500
So -61 means the heating bill was reduced $61 for the tenth,

13
00:00:36,500 --> 00:00:38,430
home with the New Insulation.

14
00:00:38,430 --> 00:00:41,199
The plus 10 means for the fourth home with the Old Insulation,

15
00:00:41,199 --> 00:00:43,060
the heating bill went up $10.

16
00:00:43,060 --> 00:00:44,820
Now if you look at the AVERAGES,

17
00:00:44,820 --> 00:00:47,180
do they seem significantly different to you?

18
00:00:47,180 --> 00:00:48,040
Well it's hard to tell.

19
00:00:49,180 --> 00:00:51,390
So when we didn't change insulation,

20
00:00:51,390 --> 00:00:54,464
the heating bill went down $3 approximately.

21
00:00:54,464 --> 00:00:56,140
And maybe it was warmer winter.

22
00:00:56,140 --> 00:00:59,333
But when we gave them the New Insulation, it went down $14.

23
00:00:59,333 --> 00:01:01,250
So was that a significant difference?

24
00:01:01,250 --> 00:01:04,390
Well, it really depends on the variance within each group.

25
00:01:04,390 --> 00:01:07,840
But all we have to do is recognize that we've paired the samples.

26
00:01:07,840 --> 00:01:09,310
Again, what was the obvious pairing?

27
00:01:09,310 --> 00:01:13,360
They had the same heating bill last year,

28
00:01:13,360 --> 00:01:18,240
each pair in observations 1 through 10.

29
00:01:18,240 --> 00:01:20,380
So now, we do data analysis.

30
00:01:20,380 --> 00:01:22,337
It's gonna be paired sample two means.

31
00:01:23,934 --> 00:01:27,600
So variable one range can be the Old Insulation.

32
00:01:29,560 --> 00:01:33,940
Variable two range could be the New Insulation.

33
00:01:35,380 --> 00:01:37,730
The hypothesized mean difference, we always use 0.

34
00:01:37,730 --> 00:01:38,990
We don't have to.

35
00:01:38,990 --> 00:01:40,760
We have labels.

36
00:01:40,760 --> 00:01:45,950
We have the output range and that will be right there.

37
00:01:45,950 --> 00:01:48,090
So all we need is the PVALUE.

38
00:01:48,090 --> 00:01:50,680
Now excel doesn't make this font bigger automatically,

39
00:01:50,680 --> 00:01:51,950
I have to do it.

40
00:01:51,950 --> 00:01:54,758
When it puts in stop to the analysis toolpack,

41
00:01:54,758 --> 00:01:57,718
you'll gonna have to adjust font if you want to.

42
00:01:57,718 --> 00:01:58,587
But basically,

43
00:01:58,587 --> 00:02:01,946
we need to widen the column by double-clicking here at the top.

44
00:02:01,946 --> 00:02:07,600
The PVALUE is 0.53 for a two-tailed test.

45
00:02:07,600 --> 00:02:10,240
Now that is a big PVALUE.

46
00:02:10,240 --> 00:02:11,680
So we'd accept H0.

47
00:02:11,680 --> 00:02:12,611
What is the intuition here?

48
00:02:15,823 --> 00:02:20,094
Given the large amount of variance in the heating bills between the 10

49
00:02:20,094 --> 00:02:23,226
homes with Old Installation and New Installation,

50
00:02:23,226 --> 00:02:26,808
this difference of mean of $11 is just not significant.

51
00:02:26,808 --> 00:02:30,570
See these changes in the heating bill are all over the board.

52
00:02:30,570 --> 00:02:36,170
They go from -70 to 29 here,

53
00:02:36,170 --> 00:02:39,776
from 49 to -45.

54
00:02:39,776 --> 00:02:43,156
And that large amount of variance makes it hard for us to include,

55
00:02:43,156 --> 00:02:46,421
that the difference in the means here is due to the insulation.

56
00:02:46,421 --> 00:02:49,722
It could very well be due to the fact there's just still a lot of

57
00:02:49,722 --> 00:02:52,582
difference, basically in the hitting bills of these

58
00:02:52,582 --> 00:02:56,730
homes even thought we thought they had very similar designs.

59
00:02:56,730 --> 00:02:59,960
So basically, we accept in all hypothesis here.

60
00:02:59,960 --> 00:03:03,930
Now on our next video, we will talk about the chi-square test for

61
00:03:03,930 --> 00:03:07,360
independence, which is used on categorical data.

62
00:03:07,360 --> 00:03:10,968
Now note that all our hypothesis tests in this module have been for

63
00:03:10,968 --> 00:03:12,881
quantitative or numerical data.

64
00:03:12,881 --> 00:03:17,027
So it's only fair we have one hypothesis test at least,

65
00:03:17,027 --> 00:03:20,834
that deals with qualitative or categorical data.

66
00:03:20,834 --> 00:03:24,788
If you've been observant, you've noticed that every hypothesis test

67
00:03:24,788 --> 00:03:27,779
we've done in module five, involve numerical data.

68
00:03:27,779 --> 00:03:31,360
In our next set of videos, we'll talk about the chi-square test for

69
00:03:31,360 --> 00:03:34,760
independence, which is probably the most important hypothesis

70
00:03:34,760 --> 00:03:36,570
test used with categorical data.

