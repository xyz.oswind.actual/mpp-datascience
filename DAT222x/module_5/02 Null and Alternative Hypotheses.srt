0
00:00:01,020 --> 00:00:04,270
A statistical hypothesis is an assumption about a population

1
00:00:04,270 --> 00:00:05,200
parameter.

2
00:00:05,200 --> 00:00:07,750
This assumption may or may not be true.

3
00:00:07,750 --> 00:00:10,560
Hypothesis testing refers to the formal procedures used by

4
00:00:10,560 --> 00:00:14,340
statisticians to accept or reject statistical hypothesis.

5
00:00:14,340 --> 00:00:17,310
The best way to determine whether statistical hypothesis is true

6
00:00:17,310 --> 00:00:19,960
would be to examine the entire population.

7
00:00:19,960 --> 00:00:21,510
Since that is often impractical,

8
00:00:21,510 --> 00:00:25,550
researchers typically examine a random sample from that population.

9
00:00:25,550 --> 00:00:28,610
If the sample data are not consistent with the null hypothesis,

10
00:00:28,610 --> 00:00:30,920
the hypothesis is rejected.

11
00:00:30,920 --> 00:00:33,545
There are two types of statistical hypothesis,

12
00:00:33,545 --> 00:00:36,505
the null hypothesis denoted by H0 is usually

13
00:00:36,505 --> 00:00:40,875
the hypothesis that the result occurs purely from chance.

14
00:00:40,875 --> 00:00:44,549
The alternative hypothesis as denoted by H1 or Ha,

15
00:00:44,549 --> 00:00:49,224
is the hypothesis that sample observations are influenced by some

16
00:00:49,224 --> 00:00:50,737
non-random cause.

17
00:00:50,737 --> 00:00:54,585
In most cases we are hoping to reject the null hypothesis because

18
00:00:54,585 --> 00:00:57,436
the alternative hypothesis represents what we

19
00:00:57,436 --> 00:01:00,590
believe is true about the population.

20
00:01:00,590 --> 00:01:03,370
Some researchers say that a hypothesis test can have

21
00:01:03,370 --> 00:01:04,750
one of two outcomes.

22
00:01:04,750 --> 00:01:09,070
You accept the null hypothesis or you reject the null hypothesis.

23
00:01:09,070 --> 00:01:12,820
Many statisticians take umbrage with that notion of accepting

24
00:01:12,820 --> 00:01:14,230
a null hypothesis.

25
00:01:14,230 --> 00:01:17,450
Instead, we like to say you reject the null hypothesis or

26
00:01:17,450 --> 00:01:20,200
you fail to reject the null hypothesis.

27
00:01:20,200 --> 00:01:23,850
Why the distinction between acceptance and failure to reject?

28
00:01:23,850 --> 00:01:27,190
Acceptance implies that the null hypothesis is true.

29
00:01:27,190 --> 00:01:31,050
Failure to reject implies that the data are not sufficiently persuasive

30
00:01:31,050 --> 00:01:34,850
for us to prefer the alternate hypothesis over the null hypothesis.

