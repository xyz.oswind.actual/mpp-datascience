0
00:00:00,680 --> 00:00:02,170
In this video, we're going to try

1
00:00:02,190 --> 00:00:04,660
and give you a little bit more insight in how statisticians

2
00:00:04,680 --> 00:00:06,779
set up the null and alternative hypothesis.

3
00:00:07,250 --> 00:00:09,900
So again let's look at that Kernel just as example

4
00:00:09,920 --> 00:00:14,266
the null hypothesis was the status quo in the US the defendant is innocent.

5
00:00:14,740 --> 00:00:17,880
And the alternative hypothesis competes with the null hypothesis

6
00:00:18,200 --> 00:00:20,900
and the alternative hypothesis was the defendant was guilty

7
00:00:21,180 --> 00:00:23,946
and we needed lots of proof to reject the null hypothesis

8
00:00:24,270 --> 00:00:26,773
because then we really feel confident that we,

9
00:00:27,440 --> 00:00:29,964
proved our alternative view of the world.

10
00:00:31,000 --> 00:00:33,186
So here's a great example my opinion now

11
00:00:33,220 --> 00:00:35,880
now Penn State is arbitrary here it could really represent I think

12
00:00:35,910 --> 00:00:38,640
any State University in the United States.

13
00:00:38,660 --> 00:00:41,293
So I think you all know that a Harvard education

14
00:00:41,320 --> 00:00:43,820
is considered really top-notch maybe not the top

15
00:00:43,850 --> 00:00:47,881
in the United States but certainly one of the top schools in the United States.

16
00:00:48,270 --> 00:00:52,145
And so basically is a Harvard education really superior

17
00:00:52,170 --> 00:00:55,493
to a Penn State Education. Now the students that get into Harvard

18
00:00:55,510 --> 00:00:58,590
have higher test scores that's a fact.

19
00:00:59,000 --> 00:01:00,877
But it does the Harvard education really

20
00:01:00,900 --> 00:01:03,457
add value in addition to the fact that

21
00:01:04,080 --> 00:01:07,365
the students have higher test scores and higher GPAs.

22
00:01:07,820 --> 00:01:10,730
Well what you could look at is the average salary 10 years

23
00:01:10,760 --> 00:01:13,351
out of Harvard graduates versus Penn State graduates.

24
00:01:13,620 --> 00:01:17,370
And the average salary of the Harvard graduates is much higher.

25
00:01:17,760 --> 00:01:20,071
Does that mean that Harvard is adding a lot of value,

26
00:01:20,400 --> 00:01:24,515
not necessarily. So two economists came up with a really brilliant study.

27
00:01:24,980 --> 00:01:29,386
OK. What you do is you pick people who got into both Penn State and Harvard.

28
00:01:29,460 --> 00:01:32,773
OK. Now how they got the data I don't know because those people

29
00:01:32,790 --> 00:01:35,746
basically sort of have equal academic credentials in

30
00:01:35,790 --> 00:01:39,330
or equal probably and extracurricular activities et cetera.

31
00:01:39,670 --> 00:01:42,830
And then some of them went to Penn State and some of them went to Harvard.

32
00:01:42,950 --> 00:01:46,856
Look at their incomes 10 years out and see if there's a significant difference

33
00:01:47,160 --> 00:01:50,100
because basically the assumption is those students are basically identical

34
00:01:50,120 --> 00:01:54,173
and abilities because they got into both schools. So your null hypothesis

35
00:01:54,200 --> 00:01:57,040
would be the mean income of the Penn State grads 10 years out

36
00:01:57,200 --> 00:02:00,320
equals the mean income of the Harvard grads. In other words there's

37
00:02:00,340 --> 00:02:03,330
no difference and then the alternative hypothesis

38
00:02:03,360 --> 00:02:06,546
would be the mean income of the Penn State grads 10 years out is not equal

39
00:02:06,580 --> 00:02:08,740
to the mean income of the Harvard grads.

40
00:02:08,910 --> 00:02:12,660
In other words you've got to prove that basically for students of equal

41
00:02:12,680 --> 00:02:16,450
ability that Harvard education 10 years out causes that they make more money

42
00:02:16,760 --> 00:02:20,310
and when they did the test each rule was accepted there really wasn't a

43
00:02:20,330 --> 00:02:24,429
significant difference between these Penn State grads and Harvard grads

44
00:02:24,440 --> 00:02:27,253
who's sort of equal high school careers,

45
00:02:27,350 --> 00:02:29,280
and basically how much money they make.

46
00:02:29,560 --> 00:02:32,047
OK. So that's really quite amazing.

47
00:02:32,470 --> 00:02:34,669
Now I don't know if you've heard of Esther Duflo

48
00:02:34,690 --> 00:02:38,080
but I'll bet in the future, you will. She's an MIT economist.

49
00:02:38,300 --> 00:02:41,710
Oh, I think it has changed the world for the better similar to the Gates Foundation.

50
00:02:42,010 --> 00:02:45,016
And my guess it should win the Nobel Prize for economics for her great work

51
00:02:45,180 --> 00:02:50,230
and improving basically the well-being of people in third-world countries.

52
00:02:50,630 --> 00:02:54,705
So one thing she worked on was how to get teachers in India to be absent less.

53
00:02:54,950 --> 00:03:00,530
So some days according to the study I read 40% of teachers in India are absent

54
00:03:00,590 --> 00:03:02,909
and that's going to cause the students not to learn as much.

55
00:03:03,350 --> 00:03:06,936
So what Esther did is she divided the schools in India

56
00:03:06,950 --> 00:03:08,346
randomly in the two groups.

57
00:03:08,550 --> 00:03:12,830
So in half the school, she like flipped a coin to pick each school in her sample,

58
00:03:13,500 --> 00:03:17,200
either they would give them an extra dollar15 per day for teacher

59
00:03:17,230 --> 00:03:18,589
showing up for school or they wouldn't.

60
00:03:19,190 --> 00:03:22,760
OK. So the obvious question is if you give them an extra dollar 15

61
00:03:22,780 --> 00:03:25,420
the teachers for showing up, Will they show up more.

62
00:03:25,710 --> 00:03:29,090
So the null hypothesis would be the teacher attendance with the zero

63
00:03:29,110 --> 00:03:32,266
and no incentive would equal the teachers attendance

64
00:03:32,280 --> 00:03:33,920
with the dollar 15 incentive

65
00:03:34,370 --> 00:03:36,397
and the alternative could be set up two ways.

66
00:03:36,770 --> 00:03:40,841
OK. The attendance with the zero incentive minus the teacher attendance

67
00:03:40,870 --> 00:03:42,400
with the dollar 15 incentive.

68
00:03:42,470 --> 00:03:44,780
OK. Is that less than zero, in other words

69
00:03:45,030 --> 00:03:46,859
with no incentive today at 10 less

70
00:03:46,880 --> 00:03:48,373
than when you give them dollar 15

71
00:03:48,550 --> 00:03:51,420
or you could say do they attend more when you given the incentive.

72
00:03:51,660 --> 00:03:56,040
And basically the null hypothesis was resoundingly rejected

73
00:03:56,260 --> 00:03:58,960
showing that if you would pay the teachers an extra dollar 15

74
00:03:59,000 --> 00:04:01,110
for attending school a day. They'd show up

75
00:04:01,130 --> 00:04:03,327
more and I'm sure that would make the

76
00:04:03,510 --> 00:04:06,980
kids in India have better K-12 education

77
00:04:07,290 --> 00:04:09,339
and they would have better lives if you believe

78
00:04:09,380 --> 00:04:11,105
K-12 is important which I do.

79
00:04:11,400 --> 00:04:13,520
OK. Now let's take the following example.

80
00:04:13,940 --> 00:04:17,943
So I like to swim 100 yards every day on a speed test to see how fast I go

81
00:04:18,280 --> 00:04:21,680
and most days I do 88 seconds. Well, my son said

82
00:04:22,160 --> 00:04:25,460
he drinks coffee before he lifts weights and he really lifts weights better.

83
00:04:25,900 --> 00:04:27,186
So I mean I don't like coffee

84
00:04:27,200 --> 00:04:29,700
So I haven't tried this but let's suppose I wanted to try this.

85
00:04:29,830 --> 00:04:34,070
Does drinking coffee before practice make me swim faster or not?

86
00:04:34,380 --> 00:04:38,598
So the null hypothesis would be on the days where I drink coffee

87
00:04:38,620 --> 00:04:42,518
before doing my swim practice is my time equal to 88 seconds

88
00:04:42,560 --> 00:04:44,453
my average because I'm pretty consistent.

89
00:04:44,720 --> 00:04:47,736
The alternative would be do I swim better than my mean time.

90
00:04:47,860 --> 00:04:50,170
Mean time swimming with coffee is less

91
00:04:50,200 --> 00:04:53,060
than 88 seconds. So they basically that's a

92
00:04:53,120 --> 00:04:56,216
lower tailed alternative or left tailed alternative test

93
00:04:56,430 --> 00:05:00,200
and basically maybe if my sample mean if I did this 10 times

94
00:05:00,230 --> 00:05:02,693
and I averaged 85 seconds, 3 seconds better

95
00:05:02,710 --> 00:05:04,140
maybe that would prove drinking

96
00:05:04,160 --> 00:05:06,920
coffee makes me swim better. OK.

97
00:05:07,000 --> 00:05:09,730
In my speed test, I don't know I haven't done it.

98
00:05:09,930 --> 00:05:14,930
Now one example here ask Cola drinkers

99
00:05:14,950 --> 00:05:17,822
so they prefer Coke to Pepsi suppose you work for Coca Cola

100
00:05:18,040 --> 00:05:20,634
you want to say in an ad you won the taste test. OK.

101
00:05:20,760 --> 00:05:23,530
You want to say that basically Coca Cola tastes better than Pepsi.

102
00:05:23,790 --> 00:05:25,800
So how would you set up the hypothesis?

103
00:05:25,960 --> 00:05:29,740
You let p be the fraction of people in your taste test to prefer Coke to Pepsi.

104
00:05:29,900 --> 00:05:33,555
And you would make the null hypothesis not that Coca Cola is better

105
00:05:33,670 --> 00:05:36,170
because you want to prove Coca Cola is better.

106
00:05:36,220 --> 00:05:39,241
So the null hypothesis would be the fraction of people in your sample

107
00:05:39,260 --> 00:05:43,360
preferred Coke to get Pepsi is equal to 0.5 or less than equal to 0.5.

108
00:05:43,640 --> 00:05:46,177
And the alternative would be that basically

109
00:05:46,850 --> 00:05:50,260
the probability that somebody prefers coke to Pepsi is greater than 0.5.

110
00:05:50,610 --> 00:05:54,165
So maybe you'll find out 60% in your sample if you work for Coca Cola,

111
00:05:54,260 --> 00:05:58,109
prefer Coke to Pepsi and it is that significant proof, well you'll have to

112
00:05:58,540 --> 00:06:01,395
study later in the module how we evaluate that question.

113
00:06:01,740 --> 00:06:03,330
Okay one final example,

114
00:06:03,610 --> 00:06:07,710
A company- bottling company needs to produce bottles

115
00:06:08,070 --> 00:06:11,090
that will hold 12 ounces of liquid for a local beer maker.

116
00:06:11,420 --> 00:06:13,340
Ok, periodically the company gets

117
00:06:13,386 --> 00:06:15,610
complaints their bottles are not holding enough liquid.

118
00:06:16,000 --> 00:06:17,810
What hypothesis would you setup

119
00:06:17,820 --> 00:06:19,680
to help the beer company prove that they

120
00:06:19,910 --> 00:06:22,410
improve the process enough that they are no longer

121
00:06:22,720 --> 00:06:25,250
putting less than 12 ounces in a bottle of beer.

122
00:06:25,700 --> 00:06:28,520
Okay well the Null hypothesis would be

123
00:06:28,560 --> 00:06:30,260
that will not doing a good job

124
00:06:30,330 --> 00:06:32,580
because then when you reject it you really can say,

125
00:06:32,820 --> 00:06:35,390
well I'm really confident that we have

126
00:06:35,410 --> 00:06:40,900
are putting more than 12 ounces of beer in a can or bottle

127
00:06:40,946 --> 00:06:43,660
here I guess I switched it from can to bottle, but it's not that important.

128
00:06:44,160 --> 00:06:46,830
So the null hypothesis be the mean ounces in a can is

129
00:06:46,840 --> 00:06:48,510
less or equal to twelve ounces.

130
00:06:48,870 --> 00:06:50,690
And the alternative would be the mean ounces

131
00:06:50,690 --> 00:06:52,620
and a can is greater than twelve ounces.

132
00:06:52,820 --> 00:06:55,210
And if you reject the Null hypothesis there

133
00:06:55,540 --> 00:06:58,710
then you've really proven that basically you're putting enough

134
00:06:59,240 --> 00:07:03,080
beer in those cans. Okay, you don't want to start with the null hypothesis

135
00:07:03,350 --> 00:07:04,940
that the can has enough beer,

136
00:07:05,240 --> 00:07:08,440
because then if you had 11.95 ounces in your sample

137
00:07:08,690 --> 00:07:11,080
you probably accept that even though you had less beer.

138
00:07:11,380 --> 00:07:13,460
Again you want to set things up that when you

139
00:07:13,800 --> 00:07:17,000
reject the null hypothesis you really got a lot of evidence

140
00:07:17,010 --> 00:07:18,960
in favor of the alternative hypothesis.

141
00:07:19,430 --> 00:07:21,800
Now in our next video we'll show you

142
00:07:22,120 --> 00:07:25,140
what it means to talk about the Critical Region

143
00:07:25,170 --> 00:07:28,370
which basically is the region in which you reject the Null hypothesis.

