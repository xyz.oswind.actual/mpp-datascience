0
00:00:01,110 --> 00:00:02,971
In this video we'll discuss the t-Test for

1
00:00:02,971 --> 00:00:05,748
testing difference between means when we have equal variances.

2
00:00:05,748 --> 00:00:06,961
So here's our example,

3
00:00:06,961 --> 00:00:09,849
we've got students who took statistics in a hybrid class,

4
00:00:09,849 --> 00:00:13,156
which means they had online lectures and they took an in class test.

5
00:00:13,156 --> 00:00:16,021
And then we have students who took everything in person,

6
00:00:16,021 --> 00:00:17,890
they all took the same test.

7
00:00:17,890 --> 00:00:20,200
So, here are the scores of the hybrid students,

8
00:00:20,200 --> 00:00:22,780
here's the scores of the in person students.

9
00:00:22,780 --> 00:00:25,820
And we wanna know if there's a significant mean difference here.

10
00:00:25,820 --> 00:00:27,530
So, we could look at the averages,

11
00:00:27,530 --> 00:00:31,540
just to eyeball it, we won't really need that to do the test.

12
00:00:31,540 --> 00:00:33,220
But, what was the average score

13
00:00:34,570 --> 00:00:37,510
of our students who took it online mostly?

14
00:00:37,510 --> 00:00:44,273
It was 85 or 86, And for the student who took it totally in person,

15
00:00:44,273 --> 00:00:46,820
they got a little bit higher, they got an 87.6.

16
00:00:46,820 --> 00:00:50,020
But is that a significant difference?

17
00:00:50,020 --> 00:00:52,432
Okay, we should first test, we have sample size is less or

18
00:00:52,432 --> 00:00:53,281
equal to 30 here.

19
00:00:59,198 --> 00:01:03,476
So we're gonna have to use, not the two sample Z test,

20
00:01:03,476 --> 00:01:05,340
we just can't do that.

21
00:01:05,340 --> 00:01:10,670
So we have 14 there, we've got this many people in this class.

22
00:01:11,820 --> 00:01:14,343
And we should check do we have normal populations.

23
00:01:14,343 --> 00:01:16,938
So we could look at the kurtosis, and we could look at the skewness.

24
00:01:19,865 --> 00:01:27,349
So if I would get the skewness, We want that to be between plus one and

25
00:01:27,349 --> 00:01:32,551
minus one and it's minus 0.5 and the skewness over here

26
00:01:32,551 --> 00:01:37,450
is between plus one and minus one, so it looks normal.

27
00:01:37,450 --> 00:01:40,705
Check that kurtosis, sounds like we're going to the doctor.

28
00:01:43,780 --> 00:01:48,490
Kurtosis 0.1, very close to 0.

29
00:01:48,490 --> 00:01:52,934
Kurtosis, -0.34,

30
00:01:52,934 --> 00:01:57,110
okay, so looks like we have normal populations.

31
00:01:57,110 --> 00:02:00,150
The sample size is less than, equal to 30, the question is,

32
00:02:00,150 --> 00:02:02,180
do we have equal or unequal variances.

33
00:02:02,180 --> 00:02:04,428
So to test for equal variances, remember,

34
00:02:04,428 --> 00:02:06,017
we had that f.TEST function.

35
00:02:06,017 --> 00:02:10,812
So all we have to do is type in the first array, comma,

36
00:02:10,812 --> 00:02:12,527
the second array.

37
00:02:15,432 --> 00:02:19,895
And we get 0.22, so if our alpha's 0.05 for testing equal variances,

38
00:02:19,895 --> 00:02:21,530
that's much bigger.

39
00:02:21,530 --> 00:02:23,950
That means there's not much discrepancy, or

40
00:02:23,950 --> 00:02:27,530
not a statistically significant discrepancy in

41
00:02:27,530 --> 00:02:30,720
the variance of the scores in the hybrid and in person classes.

42
00:02:30,720 --> 00:02:33,010
So we can assume the variances are equal and

43
00:02:33,010 --> 00:02:37,950
use the t-Test cuz we have normal population, small sample size, for

44
00:02:37,950 --> 00:02:40,770
testing whether these means are significantly different.

45
00:02:40,770 --> 00:02:45,419
So again we go to the analysis tool back, so

46
00:02:45,419 --> 00:02:51,372
we'll go to the t-Test, assuming equal variances.

47
00:02:51,372 --> 00:02:54,997
Okay variable one range,

48
00:02:54,997 --> 00:02:59,443
we can have labels if we want, so

49
00:02:59,443 --> 00:03:04,550
we go right here variable two range,

50
00:03:04,550 --> 00:03:08,690
check the label right there.

51
00:03:12,048 --> 00:03:13,548
Hypothesis mean difference,

52
00:03:13,548 --> 00:03:16,998
we'll say zero we could put in a different value if we want.

53
00:03:16,998 --> 00:03:22,160
Now output range for this test just put it over to the right somewhere.

54
00:03:22,160 --> 00:03:25,060
That's the upper left hand corner of where the answer goes.

55
00:03:25,060 --> 00:03:26,370
And all we need is the p value,

56
00:03:26,370 --> 00:03:30,020
we don't need to see any complicated formulas okay.

57
00:03:30,020 --> 00:03:31,925
So the p value we had a two tail test here,

58
00:03:31,925 --> 00:03:33,562
we could've done a one tail test.

59
00:03:33,562 --> 00:03:37,452
P value for a two tailed test is always double for a one tailed test.

60
00:03:37,452 --> 00:03:39,735
So we have a p value of 35%.

61
00:03:43,051 --> 00:03:46,990
That means, if the two means were the same for

62
00:03:46,990 --> 00:03:51,862
these populations, there's a 35% chance of seeing

63
00:03:51,862 --> 00:03:56,470
a discrepancy basically this large or larger.

64
00:03:56,470 --> 00:03:59,854
So, that's a pretty big probability so we accept the null hypothesis.

65
00:04:02,650 --> 00:04:07,864
That Mean score In hybrid and In person classes are the same.

66
00:04:11,182 --> 00:04:14,476
And that's pretty good because that means I'm online,

67
00:04:14,476 --> 00:04:18,248
I know I can teach more students online than I can in person because

68
00:04:18,248 --> 00:04:20,220
there is no room capacity.

69
00:04:20,220 --> 00:04:21,170
And basically the students,

70
00:04:21,170 --> 00:04:24,780
they don't have to waste an hour each way driving to class.

71
00:04:24,780 --> 00:04:27,690
So basically this result would be very encouraging

72
00:04:27,690 --> 00:04:31,190
to show that basically if we teach people online we can get results

73
00:04:31,190 --> 00:04:33,790
that are basically similar to teaching them in person.

74
00:04:33,790 --> 00:04:38,438
Now in the next video we'll do t-Test with unequal variances.

75
00:04:38,438 --> 00:04:40,030
So what we'll have to do first is test for

76
00:04:40,030 --> 00:04:43,530
the variances of the populations, show that are unequal and

77
00:04:43,530 --> 00:04:44,780
then just push a different button.

