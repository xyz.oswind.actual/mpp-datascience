0
00:00:00,420 --> 00:00:04,520
In this video, we'll discuss a lower one sided alternative hypothesis.

1
00:00:04,520 --> 00:00:10,528
So the average US income, family income in 2015 was $79,263.

2
00:00:10,528 --> 00:00:13,670
So you wanna know whether your Congressional District has a lower

3
00:00:13,670 --> 00:00:16,570
average income than the US as a whole.

4
00:00:16,570 --> 00:00:19,370
So the parameter of interest to you is the average family income in your

5
00:00:19,370 --> 00:00:21,920
Congressional District, let's call that mu.

6
00:00:21,920 --> 00:00:25,809
So you might set a null hypothesis that your district is at least as

7
00:00:25,809 --> 00:00:27,943
well off as the rest of the country.

8
00:00:27,943 --> 00:00:34,163
Which would be mu = $79,263 or mu is greater and equal to $79,263.

9
00:00:34,163 --> 00:00:35,545
You could choose either one.

10
00:00:35,545 --> 00:00:40,302
Then your alternative is that your mean income in your district is

11
00:00:40,302 --> 00:00:42,406
less than $79,263.

12
00:00:42,406 --> 00:00:45,796
Now our null hypothesis there again is that we're no worse in income.

13
00:00:45,796 --> 00:00:49,430
And our alternative is that basically we are worse.

14
00:00:49,430 --> 00:00:52,190
So how would we choose between these hypotheses?

15
00:00:52,190 --> 00:00:55,120
We would take a simple random sample of families in our district, and

16
00:00:55,120 --> 00:00:56,790
look at the sample mean.

17
00:00:56,790 --> 00:00:59,760
And basically if the sample mean was really low,

18
00:00:59,760 --> 00:01:02,040
then we would probably go with the alternative hypothesis.

19
00:01:02,040 --> 00:01:03,851
Like if the sample mean was 60,000,

20
00:01:03,851 --> 00:01:05,776
we would probably say we're not as well off.

21
00:01:05,776 --> 00:01:10,000
If the sample mean was 75,000, we wouldn't be sure what to conclude.

22
00:01:10,000 --> 00:01:12,620
That's what hypothesis testing is all about.

23
00:01:12,620 --> 00:01:15,720
Now in this situation, the alternative hypothesis, the value of

24
00:01:15,720 --> 00:01:19,790
the population parameter is lower than it was in the null hypothesis,

25
00:01:19,790 --> 00:01:24,050
so this is called a lower one sided alternative hypothesis.

26
00:01:24,050 --> 00:01:27,695
In the next video, we'll discuss two tailed alternative hypotheses.

