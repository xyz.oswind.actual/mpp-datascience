0
00:00:01,000 --> 00:00:05,100
In this video will discuss  Upper One-Sided Alternative Hypothesis.

1
00:00:05,130 --> 00:00:07,925
Okay, so let's look at the following situation,

2
00:00:07,950 --> 00:00:10,525
let's suppose that with the current treatment or drug

3
00:00:10,550 --> 00:00:14,375
for pancreatic cancer 10% of all pancreatic cancer patients

4
00:00:14,400 --> 00:00:17,775
survive for five years. A new drug is being tested.

5
00:00:17,830 --> 00:00:21,050
Let P equal the fraction of pancreatic patients

6
00:00:21,080 --> 00:00:23,650
receiving the new drug, who survived for five years.

7
00:00:23,680 --> 00:00:26,900
So your company has come up with this new treatment which hopefully

8
00:00:26,930 --> 00:00:29,725
is better than the current treatment for this horrible disease.

9
00:00:29,750 --> 00:00:32,750
So what hypothesis test should you set up to

10
00:00:32,780 --> 00:00:35,000
prove to the Food and Drug Administration

11
00:00:35,050 --> 00:00:38,650
in the US or whatever organization is in charge of

12
00:00:38,680 --> 00:00:41,175
approving new drugs in your country that this drug

13
00:00:41,200 --> 00:00:43,325
should come to market and replace the old treatment.

14
00:00:43,350 --> 00:00:48,350
Well the null hypothesis is basically that that the drug is not effective.

15
00:00:48,380 --> 00:00:50,025
Now this may seem a bit strange,

16
00:00:50,050 --> 00:00:52,200
but I think you'll see why people do it this way.

17
00:00:52,230 --> 00:00:54,800
So the null hypothesis is the fraction of people would

18
00:00:54,830 --> 00:00:58,025
survive with the new drug is less  or equal to 0.1 the current percentage

19
00:00:58,050 --> 00:01:00,825
and the alternative is that more than 10 percent would

20
00:01:00,850 --> 00:01:03,700
survive of course they have to look at side-effects and stuff like that.

21
00:01:03,750 --> 00:01:10,475
So basically the reason we use phat being less than or equal to 0.1

22
00:01:10,544 --> 00:01:15,470
is basically, we need to show the drug is significantly better than

23
00:01:15,500 --> 00:01:18,925
what's out there or the government will just not approve it, okay.

24
00:01:18,950 --> 00:01:23,550
So this is basically an Upper One-Sided Alternative Hypothesis,

25
00:01:23,580 --> 00:01:27,550
because in the Alternative Hypothesis the value of the population

26
00:01:27,580 --> 00:01:30,750
parameter is larger than it is in the null hypothesis.

27
00:01:30,780 --> 00:01:35,575
And you can see why we do this again, let phat be the fraction of patients

28
00:01:35,600 --> 00:01:38,100
who get the new drug, who survived five years.

29
00:01:38,130 --> 00:01:41,250
Suppose it's less than or equal to  ten percent, nine percent survived,

30
00:01:41,280 --> 00:01:42,925
well then clearly the new drug,

31
00:01:42,950 --> 00:01:45,600
we can't prove it's better because it did worse than the old drug.

32
00:01:45,650 --> 00:01:49,150
But then the real place where the rubber hits the road where it's

33
00:01:49,180 --> 00:01:50,250
tough to figure it out.

34
00:01:50,280 --> 00:01:55,125
Suppose 13 percent survived with the new drug, is that enough proof to show that

35
00:01:55,150 --> 00:01:57,825
this drug is better than the current treatment.

36
00:01:57,850 --> 00:02:01,150
Maybe we need 15 percent it depends on the sample size

37
00:02:01,180 --> 00:02:05,950
and what fraction survived. But if we made the null hypothesis be

38
00:02:05,980 --> 00:02:10,175
that P was greater than 0.1 we would basically be assuming that

39
00:02:10,200 --> 00:02:14,925
the drug was good and we might accept the hypothesis with out real proof.

40
00:02:14,950 --> 00:02:17,950
So basically in pharmaceutical testing you always assume,

41
00:02:17,980 --> 00:02:20,400
basically the null hypothesis is the drug

42
00:02:20,430 --> 00:02:22,500
is not more effective than what's out there.

43
00:02:22,530 --> 00:02:26,875
And then you have to prove that the drug is more effective than what's out there.

44
00:02:26,900 --> 00:02:31,675
Now, in the next video we'll talk about a Lower One-Sided Alternative Test.

