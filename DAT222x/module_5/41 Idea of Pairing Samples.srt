0
00:00:00,640 --> 00:00:03,770
In this video, we'll discuss the T test paired two sample test,

1
00:00:03,770 --> 00:00:05,750
which is very important, very useful.

2
00:00:05,750 --> 00:00:07,570
So what do we mean by pairing samples?

3
00:00:07,570 --> 00:00:09,370
Let's look at three examples.

4
00:00:09,370 --> 00:00:13,240
Suppose you want to figure out if a drug is better at reducing

5
00:00:13,240 --> 00:00:16,290
cholesterol than the placebo, which we discussed in the last example.

6
00:00:17,350 --> 00:00:19,230
Well, the problem with the last example was,

7
00:00:19,230 --> 00:00:20,700
the ten people got the placebo and

8
00:00:20,700 --> 00:00:24,890
the ten people got the drug, they could differ drastically.

9
00:00:24,890 --> 00:00:27,560
One person might have a cholesterol of 200,

10
00:00:27,560 --> 00:00:30,540
the other might have a cholesterol of 95.

11
00:00:30,540 --> 00:00:34,600
Okay, so, what you wanna do is block out the effect of the differences

12
00:00:34,600 --> 00:00:35,860
between the people, and

13
00:00:35,860 --> 00:00:39,190
just focus on the difference between the drug and the placebo.

14
00:00:39,190 --> 00:00:43,180
So the way you do that is pick ten pairs of two people who are matched

15
00:00:43,180 --> 00:00:46,270
on their physical attributes like age, weight and cholesterol.

16
00:00:46,270 --> 00:00:49,230
Then you flip a coin to randomly choose one member

17
00:00:49,230 --> 00:00:52,920
of each pair to receive the drug and one member to receive the placebo.

18
00:00:52,920 --> 00:00:55,820
So the only difference in each pair should be the drug versus

19
00:00:55,820 --> 00:01:01,890
the placebo, cuz the two people were judged to be similar physically.

20
00:01:01,890 --> 00:01:03,110
Another example,

21
00:01:03,110 --> 00:01:07,040
you wanna test of a new type of insulation reduces heating bills.

22
00:01:07,040 --> 00:01:11,150
So you could randomly pick ten houses to give the new installation

23
00:01:11,150 --> 00:01:14,200
to and ten not to, but they could be different houses.

24
00:01:14,200 --> 00:01:17,040
So what you should do is find ten pairs of two houses

25
00:01:17,040 --> 00:01:18,970
that had the same heating bill last winter.

26
00:01:20,220 --> 00:01:24,720
Then you flip a coin and, basically, you can use the coin flip to

27
00:01:24,720 --> 00:01:28,470
determine which member of each pair gets the new type of insulation and

28
00:01:28,470 --> 00:01:31,120
the other member of the pair keeps their old insulation.

29
00:01:31,120 --> 00:01:34,820
So what would be the only difference in each pair on the heating bill?

30
00:01:34,820 --> 00:01:37,970
Well it would be the new type of insulation versus the old since

31
00:01:37,970 --> 00:01:41,840
the houses were controlled to have the same heating bill last winter.

32
00:01:41,840 --> 00:01:45,550
Final example, let's suppose I'm a big swimmer, I love swimming.

33
00:01:45,550 --> 00:01:47,070
So a test of cross training,

34
00:01:47,070 --> 00:01:49,250
not just swimming will improve a swimmer's time.

35
00:01:49,250 --> 00:01:52,360
In other words, if you do a lot of weight lifting, and

36
00:01:52,360 --> 00:01:55,470
you do elliptical machine, rowing machine maybe and

37
00:01:55,470 --> 00:01:57,260
stay out of the water for a while.

38
00:01:57,260 --> 00:01:59,290
Would that make you a faster swimmer?

39
00:01:59,290 --> 00:02:03,025
Okay, well, if you had 15 swimmers do cross training and 15 swimmers

40
00:02:03,025 --> 00:02:06,490
not do cross training, maybe you pick Michael Phelps as one of your

41
00:02:06,490 --> 00:02:09,930
swimmers that did the cross training and you pick me as the other one.

42
00:02:09,930 --> 00:02:13,530
Well, I’m no Michael Phelps, so that wouldn’t really work well.

43
00:02:13,530 --> 00:02:17,320
So, you pick 15 pairs of 2 swimmers who had identical best times in

44
00:02:17,320 --> 00:02:19,510
their event, they were sort of equal swimmers.

45
00:02:19,510 --> 00:02:22,200
Then, again, you flip a coin and, basically,

46
00:02:22,200 --> 00:02:27,119
maybe every time you get heads, the first swimmer in the pair gets

47
00:02:27,119 --> 00:02:31,400
cross training and the other swimmer gets no cross training.

48
00:02:31,400 --> 00:02:34,300
And then you look at the difference between their times and

49
00:02:34,300 --> 00:02:37,190
basically the only thing that should change their times,

50
00:02:37,190 --> 00:02:39,495
is basically whether they cross trained or not,

51
00:02:39,495 --> 00:02:42,490
cuz they're very much equal in swimming ability.

52
00:02:42,490 --> 00:02:45,410
So, what we see here is a very important ID in statistics.

53
00:02:45,410 --> 00:02:47,440
We have a blocking variable and a treatment variable.

54
00:02:47,440 --> 00:02:51,010
In other words, we want to sort of negate the effect of the blocking

55
00:02:51,010 --> 00:02:54,700
variable so the effect of any of the treatment variable will come shining

56
00:02:54,700 --> 00:02:56,120
through more clearly.

57
00:02:56,120 --> 00:02:59,490
So, basically, in the cholesterol example the physical characteristics

58
00:02:59,490 --> 00:03:01,660
of the patients are what we blocked on and

59
00:03:01,660 --> 00:03:04,030
the treatment was the difference between the drug and placebo.

60
00:03:05,440 --> 00:03:07,530
In the insulation example, the size and

61
00:03:07,530 --> 00:03:09,360
design of the home are what we blocked on.

62
00:03:10,450 --> 00:03:13,190
And then we wanted to concentrate on the difference in heating bills

63
00:03:13,190 --> 00:03:15,470
between the new and old insulation.

64
00:03:15,470 --> 00:03:18,090
In the swimming example, we wanted to block on the ability of

65
00:03:18,090 --> 00:03:22,380
the swimmer, and concentrate on the difference between cross training

66
00:03:22,380 --> 00:03:24,680
and basically just doing in water training.

67
00:03:24,680 --> 00:03:26,156
So in our next video,

68
00:03:26,156 --> 00:03:30,261
we'll do an example of how this paired sample T test works.

