0
00:00:00,930 --> 00:00:04,020
A controversial issue in hypothesis testing is,

1
00:00:04,020 --> 00:00:08,310
should use a one-tailed alternative or a two-tailed alternative test?

2
00:00:08,310 --> 00:00:11,080
And statisticians have different views on this matter, we'll try and

3
00:00:11,080 --> 00:00:13,920
clarify this through examples later in the module.

4
00:00:13,920 --> 00:00:17,519
But some statisticians believe you should always use a two-tailed test

5
00:00:17,519 --> 00:00:20,823
because a priori, before you collect any data, you have no idea of

6
00:00:20,823 --> 00:00:25,045
the direction in which deviations from the null hypothesis will occur.

7
00:00:25,045 --> 00:00:27,390
So, like in our congressional district example,

8
00:00:27,390 --> 00:00:31,800
you really don't have any idea of that district being richer or

9
00:00:31,800 --> 00:00:35,408
poorer than the national average without collecting any data.

10
00:00:35,408 --> 00:00:39,430
Now other statisticians believe that if

11
00:00:39,430 --> 00:00:43,270
a deviation from the null hypothesis in either direction is of interest,

12
00:00:43,270 --> 00:00:45,120
use a two-tailed alternative.

13
00:00:45,120 --> 00:00:47,920
But if you're only interested in deviation one direction,

14
00:00:47,920 --> 00:00:50,070
you could use a one-tailed alternative.

15
00:00:50,070 --> 00:00:53,780
So like if we spend more money in school districts it would be logical

16
00:00:53,780 --> 00:00:57,760
to assume people might think that we would have increased test scores.

17
00:00:57,760 --> 00:01:00,310
So there you might use a one-tailed test.

18
00:01:00,310 --> 00:01:02,890
But we'll see later using a two-tailed test is a more

19
00:01:02,890 --> 00:01:04,620
conservative approach and

20
00:01:04,620 --> 00:01:07,960
I would come down on the side of just about always using

21
00:01:07,960 --> 00:01:11,384
the alternative hypothesis being a two-tailed hypothesis.

22
00:01:11,384 --> 00:01:14,750
In the next video we'll talk about the two types of errors that can

23
00:01:14,750 --> 00:01:18,900
occur in a hypothesis testing, type one and type two error.

