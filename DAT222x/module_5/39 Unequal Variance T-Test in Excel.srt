0
00:00:00,000 --> 00:00:04,400
So in this video, we'll discuss the t test with unequal variances.

1
00:00:04,400 --> 00:00:07,650
So suppose we're doing a test to see if a new drug

2
00:00:07,650 --> 00:00:10,990
is more effective than a placebo, a very important example.

3
00:00:10,990 --> 00:00:13,950
So we have a drug we think can reduce cholesterol.

4
00:00:13,950 --> 00:00:16,540
So basically we've given that drug to,

5
00:00:18,280 --> 00:00:20,370
looks like there's 18 people there.

6
00:00:21,550 --> 00:00:23,900
And then we have 14 people we gave a placebo and

7
00:00:23,900 --> 00:00:26,290
we looked at the reductions in cholesterol.

8
00:00:26,290 --> 00:00:29,480
And the question is, is the mean reduction in cholesterol

9
00:00:29,480 --> 00:00:32,160
significantly higher for the drug than the placebo?

10
00:00:32,160 --> 00:00:34,250
And there always does turn out to be a placebo effect.

11
00:00:34,250 --> 00:00:36,100
You give somebody a sugar pill and

12
00:00:36,100 --> 00:00:38,760
it always does have some effect, don't ask me why.

13
00:00:38,760 --> 00:00:41,050
But if we look at the averages here,

14
00:00:41,050 --> 00:00:42,890
can we conclude that they're significantly different?

15
00:00:42,890 --> 00:00:43,990
Well, we don't know yet.

16
00:00:43,990 --> 00:00:45,540
Let's take a look at them.

17
00:00:45,540 --> 00:00:48,882
So the average there would be 2.13.

18
00:00:48,882 --> 00:00:55,640
The average there reduction is by 10.

19
00:00:55,640 --> 00:01:01,728
Okay, that's seems to be a lot bigger Okay,

20
00:01:01,728 --> 00:01:04,210
but the question is, is it significantly bigger.

21
00:01:04,210 --> 00:01:10,172
So the null would be, We'll just make our font bigger.

22
00:01:13,543 --> 00:01:19,469
The null hypothesis is mean reduction for

23
00:01:19,469 --> 00:01:23,540
placebo and drug are equal.

24
00:01:25,370 --> 00:01:26,907
And the alternative is that they're not equal.

25
00:01:33,501 --> 00:01:37,218
So the first thing is since we have small samples here, we should check

26
00:01:37,218 --> 00:01:40,750
for skewness and kurtosis and then check are the variances equal.

27
00:01:42,390 --> 00:01:50,580
So if I do kurtosis, That's close to zero, that's great.

28
00:01:51,610 --> 00:01:58,240
Kurtosis for the drug, very close to zero.

29
00:01:58,240 --> 00:02:02,298
Skewness for the placebo reductions, not slew.

30
00:02:02,298 --> 00:02:05,405
That's a.

31
00:02:08,847 --> 00:02:14,180
Skewness is near zero, not less than minus one.

32
00:02:14,180 --> 00:02:21,840
Skewness here, Between plus and minus one.

33
00:02:21,840 --> 00:02:25,390
So I can assume we have normal populations, we've got small sample

34
00:02:25,390 --> 00:02:28,630
size, we don't know the variance, so we're gonna do a t test.

35
00:02:28,630 --> 00:02:31,190
The question is are the variances equal or not?

36
00:02:31,190 --> 00:02:32,955
So I can do that f test.

37
00:02:35,329 --> 00:02:37,310
Okay, here is the first array.

38
00:02:39,040 --> 00:02:42,125
And then I put a comma, I point to the second array and

39
00:02:42,125 --> 00:02:45,790
if there's a low p-value, I can assume unequal variances.

40
00:02:45,790 --> 00:02:48,710
And there'll be a really low p-value.

41
00:02:48,710 --> 00:02:51,794
That's 3 in 100,000.

42
00:02:51,794 --> 00:02:56,799
Okay, so there's only 3 chances in 100,000 that you'd

43
00:02:56,799 --> 00:03:02,510
see, Let's look at what those variances were by the way.

44
00:03:02,510 --> 00:03:06,470
A discrepancy this large in the sample variances.

45
00:03:06,470 --> 00:03:08,680
For the placebo, we got a variance of 11.

46
00:03:12,851 --> 00:03:14,555
And for the drug,

47
00:03:18,979 --> 00:03:24,520
A variance of basically, One.

48
00:03:24,520 --> 00:03:27,290
Very small variance, the drug seemed to reduce almost

49
00:03:27,290 --> 00:03:30,230
everybody's cholesterol by the same amount, okay.

50
00:03:30,230 --> 00:03:33,950
The placebo was all over the place, sometimes it increased basically

51
00:03:33,950 --> 00:03:36,170
their cholesterol which was to be expected.

52
00:03:36,170 --> 00:03:39,100
So now we can do the unequal variance test

53
00:03:39,100 --> 00:03:42,300
because this p value for equal variance was very small.

54
00:03:43,990 --> 00:03:49,030
So I go data analysis, t test, unequal variances this time.

55
00:03:50,710 --> 00:03:54,858
Okay, so variable one range, I can use the label,

56
00:03:54,858 --> 00:04:01,461
variable two range, Hypothesis mean difference zero,

57
00:04:01,461 --> 00:04:04,450
check the labels box cuz I selected the labels.

58
00:04:04,450 --> 00:04:08,700
I could put this in a new worksheet but it's easier to put it here and

59
00:04:08,700 --> 00:04:12,470
we just want that p value and wow, it's pretty small.

60
00:04:12,470 --> 00:04:17,170
The t statistic is like minus p, so the t value basically here

61
00:04:17,170 --> 00:04:22,400
is going to be -8 and 10 million.

62
00:04:22,400 --> 00:04:25,665
So basically, that's very small.

63
00:04:25,665 --> 00:04:30,820
p-value less than or equal to any alpha.

64
00:04:32,360 --> 00:04:38,158
Reject the null hypothesis, Conclude,

65
00:04:40,557 --> 00:04:45,075
A significant difference in cholesterol

66
00:04:45,075 --> 00:04:49,210
reduction between placebo and drug.

67
00:04:51,970 --> 00:04:54,770
In the next video, we'll show you how to do what's called the paired

68
00:04:54,770 --> 00:04:57,510
sample or matched pairs test.

69
00:04:57,510 --> 00:05:00,480
To test for difference between means in two populations.

