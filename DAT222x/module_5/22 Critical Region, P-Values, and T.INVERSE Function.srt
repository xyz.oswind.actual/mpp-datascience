0
00:00:00,630 --> 00:00:03,614
In this video, we'll discuss the one sample hypothesis test for

1
00:00:03,614 --> 00:00:06,070
a mean when we have a small sample size,

2
00:00:06,070 --> 00:00:08,440
that means less than 30 observations.

3
00:00:08,440 --> 00:00:11,440
We have a normal population and the variance is unknown.

4
00:00:11,440 --> 00:00:14,880
So before we get to basically the cut offs with the critical region

5
00:00:14,880 --> 00:00:16,290
and the p values for this test,

6
00:00:16,290 --> 00:00:19,170
we need to know some new Excel functions.

7
00:00:19,170 --> 00:00:22,320
So just like we had the norm members function to get percentiles for

8
00:00:22,320 --> 00:00:23,740
the normal random variable.

9
00:00:23,740 --> 00:00:29,990
We have the T.INV to get percentiles with the T random variable.

10
00:00:29,990 --> 00:00:34,057
So if you want the 2.5%ile with 28 degrees of freedom,

11
00:00:34,057 --> 00:00:36,610
which would mean 29 observations,

12
00:00:36,610 --> 00:00:41,575
you just type in T.INV(0.025,28) and you get -2.048.

13
00:00:41,575 --> 00:00:45,207
That means with 28 degrees of freedom the T random variable has

14
00:00:45,207 --> 00:00:49,021
a 2.5% chance of being less than or equal to minus 2.048.

15
00:00:49,021 --> 00:00:52,668
Now, the T random variable is symmetric like the normal, and

16
00:00:52,668 --> 00:00:57,089
so basically the 97.5%ile would just be this number without the minus

17
00:00:57,089 --> 00:00:58,298
sign as you can see.

18
00:00:58,298 --> 00:01:01,270
To get the 0.5%ile, you type in 0.005,

19
00:01:01,270 --> 00:01:05,730
if you had 14 observations, you'd use 13 degrees of freedom.

20
00:01:05,730 --> 00:01:07,840
So there's one chance in 200,

21
00:01:07,840 --> 00:01:11,332
a T random variable with 13 degrees of freedom is less or

22
00:01:11,332 --> 00:01:16,453
equal to -3.01, and one chance in 200 it's greater or equal to 3.01.

23
00:01:16,453 --> 00:01:20,200
Now to get P-value is you need to get T distribution

24
00:01:20,200 --> 00:01:24,820
probabilities not percentiles, so we used to use norm disk for

25
00:01:24,820 --> 00:01:28,410
that, so we've got a T.DIST random variable.

26
00:01:28,410 --> 00:01:31,856
What's probably a T with 10 degrees of freedom is greater than or

27
00:01:31,856 --> 00:01:32,540
equal to 2.

28
00:01:32,540 --> 00:01:35,950
Well, again, you have to do 1 minus the probability, it's less or

29
00:01:35,950 --> 00:01:38,302
equal to 2 like we did with norm DIST.

30
00:01:38,302 --> 00:01:41,010
So, the probably a T random variable with 10 degrees of freedom is

31
00:01:41,010 --> 00:01:44,610
greater than or equal to 2 is 1 minus T.DIST,

32
00:01:44,610 --> 00:01:47,260
basically the value 2 with 10 degrees of freedom.

33
00:01:47,260 --> 00:01:49,190
And you need the word TRUE in there.

34
00:01:49,190 --> 00:01:53,217
What's probably a T random variable with basically-

35
00:01:55,901 --> 00:01:58,901
10 degrees of freedom is less than or equal to -2.

36
00:01:58,901 --> 00:02:02,661
That would simply be T.DIST- 2, 10 degrees of freedom true,

37
00:02:02,661 --> 00:02:05,490
which would be the same thing by symmetry.

38
00:02:05,490 --> 00:02:08,510
The chance the T is greater or equal to plus 2, and less to or

39
00:02:08,510 --> 00:02:10,580
equal to -2 should be the same.

40
00:02:10,580 --> 00:02:13,569
So, basically, what are the critical region cutoffs for

41
00:02:13,569 --> 00:02:15,840
the One Sample T-tests and the P-values?

42
00:02:15,840 --> 00:02:17,150
Well, it's really simple.

43
00:02:17,150 --> 00:02:21,770
All you do is replace the Z that we had before by the T.

44
00:02:21,770 --> 00:02:25,560
Okay, you just take here the absolute value of the T,

45
00:02:25,560 --> 00:02:28,080
alpha percentile with the right degrees of freedom.

46
00:02:28,080 --> 00:02:31,578
For the upper one-tailed test, for the lower one-tailed test,

47
00:02:31,578 --> 00:02:34,889
basically you don't take the absolute value cuz this will then

48
00:02:34,889 --> 00:02:37,824
be a negative number, and then for the absolute value,

49
00:02:37,824 --> 00:02:40,948
basically the two-tailed test, your cut off will be based

50
00:02:40,948 --> 00:02:44,383
on the absolute value of the t times the sample standard deviation

51
00:02:44,383 --> 00:02:46,858
divided by the square root of the sample size.

52
00:02:46,858 --> 00:02:48,086
So you change Z to T and

53
00:02:48,086 --> 00:02:51,470
replace sigma by the sample standard deviation.

54
00:02:51,470 --> 00:02:54,730
You do exactly the same thing in the P-values here.

55
00:02:54,730 --> 00:02:58,160
The little t here is the observed value of the t-statistic

56
00:02:58,160 --> 00:03:00,800
from basically your sample.

57
00:03:00,800 --> 00:03:05,910
And then basically you simply use the T distribution function,

58
00:03:05,910 --> 00:03:09,300
as we've talked about, to get the P-values here.

59
00:03:09,300 --> 00:03:13,566
And the little t will be the computed value of the T-statistic

60
00:03:13,566 --> 00:03:17,667
which is going to be the sample mean minus mu 0 divided by S,

61
00:03:17,667 --> 00:03:19,652
divided by square root of n.

62
00:03:19,652 --> 00:03:23,570
And in the next video, we'll do an example of this One Sample T-test.

