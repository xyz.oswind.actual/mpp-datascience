0
00:00:00,600 --> 00:00:03,950
In this video we'll do an example of the one sample T test.

1
00:00:03,950 --> 00:00:06,220
Let's talk about Cooley High again.

2
00:00:06,220 --> 00:00:10,048
So passing the state test required for graduation is necessary.

3
00:00:10,048 --> 00:00:13,550
And the average state score on the test is 75.

4
00:00:13,550 --> 00:00:17,680
So we wanna see if Cooley High is different in performance

5
00:00:17,680 --> 00:00:18,360
than the state.

6
00:00:18,360 --> 00:00:22,160
So we took a sample of 25 students at Cooley High and

7
00:00:22,160 --> 00:00:23,370
we found the sample mean was 81.

8
00:00:23,370 --> 00:00:25,860
The sample standard deviation is 15.

9
00:00:25,860 --> 00:00:28,830
We're assuming the test scores are normally distributed,

10
00:00:28,830 --> 00:00:30,450
that's usually true.

11
00:00:30,450 --> 00:00:34,149
Now for alpha equal 0.05, would you conclude that Cooley High students

12
00:00:34,149 --> 00:00:36,818
perform differently than the typical state student?

13
00:00:36,818 --> 00:00:40,234
We used a two-tail test because before doing the test we have no

14
00:00:40,234 --> 00:00:42,942
real view about whether Cooley High is superior or

15
00:00:42,942 --> 00:00:45,400
inferior to the rest of the State.

16
00:00:45,400 --> 00:00:49,825
So our null hypothesis is the mean is 75 for the Cooley High students,

17
00:00:49,825 --> 00:00:52,423
the alternative it's not equal to 75.

18
00:00:52,423 --> 00:00:53,974
So that's a two-tail test.

19
00:00:53,974 --> 00:00:57,348
So down here we put down basically the critical region for that.

20
00:00:57,348 --> 00:01:00,290
You take basically

21
00:01:00,290 --> 00:01:05,030
the absolute value of the sample mean minus Mu 0 which is 75 here.

22
00:01:05,030 --> 00:01:09,880
Compare it to t sub alpha over 2, the t0.025 within this case,

23
00:01:09,880 --> 00:01:13,680
24 degrees of freedom times the sample standard deviation divided by

24
00:01:13,680 --> 00:01:15,510
the square root of the sample size.

25
00:01:15,510 --> 00:01:19,462
Now how do I get that t 0.025 with 24 degrees of freedom?

26
00:01:19,462 --> 00:01:26,090
I used that T.INV function we discussed in the last video.

27
00:01:26,090 --> 00:01:29,171
If you type in T.INV 0.025, with 24 degrees of freedom,

28
00:01:29,171 --> 00:01:30,410
you'll get -2.06.

29
00:01:30,410 --> 00:01:33,860
So basically, we got a sample mean of 81.

30
00:01:33,860 --> 00:01:36,220
So basically we look at the absolute value of 81.

31
00:01:36,220 --> 00:01:40,470
Absolute value cuz it's a two-tail test minus 75, that's 6.

32
00:01:40,470 --> 00:01:45,140
We say is that greater than or equal to 2.06 times the standard sample

33
00:01:45,140 --> 00:01:48,970
deviation of 15 divided by the square root of the sample size 25.

34
00:01:48,970 --> 00:01:53,830
So basically that's 15 divided by 5 is 3, times 2.06 is 6.18.

35
00:01:53,830 --> 00:01:56,870
6 is not greater or equal to 6.18.

36
00:01:56,870 --> 00:02:00,820
We don't have sufficient proof to reject the null hypothesis.

37
00:02:00,820 --> 00:02:03,955
So we'd accept the null hypothesis that the Cooley High students

38
00:02:03,955 --> 00:02:06,241
are basically the same in ability as the state.

39
00:02:06,241 --> 00:02:08,751
Now we could do a p-value there, okay?

40
00:02:08,751 --> 00:02:11,858
Cuz that's always a preferred way to look at the test.

41
00:02:11,858 --> 00:02:13,192
So we have a two-tail test.

42
00:02:13,192 --> 00:02:14,660
What's the p-value?

43
00:02:14,660 --> 00:02:16,493
I've extracted it from our table above here.

44
00:02:16,493 --> 00:02:20,745
We take twice the probability, a T random variable with n-1,

45
00:02:20,745 --> 00:02:21,700
in this case.

46
00:02:21,700 --> 00:02:24,270
24 degrees of freedom is greater or

47
00:02:24,270 --> 00:02:28,020
equal to the absolute value of the T statistics from our sample.

48
00:02:28,020 --> 00:02:29,920
What is the T statistic from our sample?

49
00:02:29,920 --> 00:02:34,440
It's basically the sample mean 81 minus the hypothesis mean of 75

50
00:02:34,440 --> 00:02:38,807
divided by the sample standard deviation divided by the square root

51
00:02:38,807 --> 00:02:41,120
of the sample size, which is 25.

52
00:02:41,120 --> 00:02:46,120
So 15 divided by the square root of 25 is 15 divided by 5 is 3,

53
00:02:46,120 --> 00:02:50,950
81 minus 75 is 6, 6 divided by 3 comes out to be 2.

54
00:02:50,950 --> 00:02:54,810
So the p-value will be twice the probability of T ran variable

55
00:02:54,810 --> 00:02:58,720
with 24 degrees of freedom is greater than or equal to 2.

56
00:02:58,720 --> 00:03:01,900
So how do we find the probability that a T distribution with 24

57
00:03:01,900 --> 00:03:04,425
degrees of freedom is greater than or equal to 2?

58
00:03:04,425 --> 00:03:06,980
Well we can use the T.DIST function.

59
00:03:06,980 --> 00:03:10,963
We have to do 1- like we did with the NORM.DIST function.

60
00:03:10,963 --> 00:03:14,991
So basically 1-T.DIST, basically the value of 2 that we got from

61
00:03:14,991 --> 00:03:18,880
our sample, comma 24 comma TRUE is the probability that T random

62
00:03:18,880 --> 00:03:21,249
variable is greater than or equal to 2.

63
00:03:21,249 --> 00:03:26,340
Which we'll have to double, because of the factor of 2 here.

64
00:03:26,340 --> 00:03:27,133
So we get .028.

65
00:03:27,133 --> 00:03:30,830
So then the p-value for this test would be 2 times 0.028,

66
00:03:30,830 --> 00:03:32,134
which is 0.056.

67
00:03:32,134 --> 00:03:36,572
Now this is consistent with our accepting the null hypothesis

68
00:03:36,572 --> 00:03:39,815
because the p-value is greater than alpha or

69
00:03:39,815 --> 00:03:42,644
level of significance, 0.056.

70
00:03:42,644 --> 00:03:46,038
So in other words, if the level of significance had been 0.06,

71
00:03:46,038 --> 00:03:48,550
we would have had enough proof to reject the null.

72
00:03:48,550 --> 00:03:51,403
But since the level of significance was 0.05,

73
00:03:51,403 --> 00:03:54,728
we don't have enough proof to reject the null hypothesis.

74
00:03:54,728 --> 00:03:56,044
Now, in our next video,

75
00:03:56,044 --> 00:04:00,080
we'll talk about hypothesis testing involving population proportions.

