0
00:00:00,030 --> 00:00:03,911
In this video will discuss the important concepts

1
00:00:03,977 --> 00:00:05,622
of Type 1 and Type 2 error.

2
00:00:05,666 --> 00:00:07,244
So we are doing hypothesis test

3
00:00:07,288 --> 00:00:09,130
there are two types of mistakes you can make.

4
00:00:09,220 --> 00:00:11,044
You can make a Type 1 error which is

5
00:00:11,088 --> 00:00:14,510
rejecting the null hypothesis given the null hypothesis is true.

6
00:00:14,533 --> 00:00:17,466
We let alpha be the probability of making a Type 1 error,

7
00:00:17,533 --> 00:00:20,333
alpha is often called the level of significance of the test

8
00:00:20,377 --> 00:00:22,711
or we can make a Type 2 error we could accept

9
00:00:22,755 --> 00:00:24,955
the null hypothesis given it's not true.

10
00:00:25,000 --> 00:00:28,355
We defined data to be the probability of making a Type 2 error.

11
00:00:28,377 --> 00:00:32,466
So a situation that's helpful for understanding Type 1 and Type 2 errors

12
00:00:32,577 --> 00:00:35,355
has to do with the US criminal justice system.

13
00:00:35,422 --> 00:00:37,550
So in the US criminal justice system

14
00:00:37,550 --> 00:00:40,111
the defendant is innocent until proven guilty.

15
00:00:40,177 --> 00:00:43,311
So basically the null hypothesis is to defend in its innocent

16
00:00:43,311 --> 00:00:45,222
because it's always going to take a lot of proof

17
00:00:45,244 --> 00:00:47,066
to disprove the null hypothesis

18
00:00:47,111 --> 00:00:48,777
as you'll see throughout this module.

19
00:00:48,822 --> 00:00:51,755
The alternative hypothesis is the defendant is guilty.

20
00:00:51,800 --> 00:00:54,066
So if Type 1 error corresponds to

21
00:00:54,111 --> 00:00:55,866
rejecting the null hypothesis

22
00:00:55,866 --> 00:00:57,690
and basically accepting the alternative

23
00:00:57,690 --> 00:01:00,266
that's convicting an innocent defendant,

24
00:01:00,311 --> 00:01:04,888
okay. A Type 2 error corresponds to letting a guilty person to go free.

25
00:01:04,977 --> 00:01:08,933
Now  in the US system a 12-0 vote is needed for conviction,

26
00:01:09,000 --> 00:01:10,755
so the US  judicial system

27
00:01:10,800 --> 00:01:14,155
considers a Type 1 error to be costlier than a Type 2 error

28
00:01:14,200 --> 00:01:16,466
and the null hypothesis is indeed

29
00:01:16,533 --> 00:01:18,133
the defendant is innocent

30
00:01:18,177 --> 00:01:21,711
because it takes overwhelming proof to disprove that hypothesis.

31
00:01:21,755 --> 00:01:25,444
So for another example, let's return to our pancreatic

32
00:01:25,533 --> 00:01:27,488
cancer drug example.

33
00:01:27,511 --> 00:01:30,600
Okay, so you recall the null hypothesis there

34
00:01:30,711 --> 00:01:32,755
was that the probability

35
00:01:32,777 --> 00:01:34,244
that somebody taking the new drug

36
00:01:34,288 --> 00:01:35,688
with pancreatic cancer would survive

37
00:01:35,711 --> 00:01:38,600
was less frequent to 0.1 though better than the old drug.

38
00:01:38,688 --> 00:01:42,644
The alternative was their chance of survival was greater than 0.1

39
00:01:42,666 --> 00:01:45,777
What are the Type 1 and Type 2 errors in that situation,

40
00:01:45,844 --> 00:01:50,088
well Type 1 error would result when we reject the hypothesis that

41
00:01:50,110 --> 00:01:54,866
the change of survival for 5 years is less  than or equal to 0.1 when it was really-

42
00:01:54,888 --> 00:02:00,060
basically less than 0.1, okay. So that corresponds

43
00:02:00,060 --> 00:02:03,111
the including the drug as an improvement because when you reject

44
00:02:03,155 --> 00:02:05,266
p less than or equal to 0.1, you're saying

45
00:02:05,333 --> 00:02:07,288
basically P is greater than 0.1

46
00:02:07,377 --> 00:02:09,844
the drug is an improvement when reality it wasn't.

47
00:02:09,888 --> 00:02:11,844
Okay, so that's the Type 1 error there,

48
00:02:11,866 --> 00:02:15,977
now a Type 2 error results when we accept p less than or equal to 0.1

49
00:02:16,000 --> 00:02:18,533
which is essentially saying the drug is not an improvement

50
00:02:18,577 --> 00:02:20,555
when it actually was an improvement.

51
00:02:20,600 --> 00:02:22,977
So basically here the Type 2 error corresponds to

52
00:02:23,044 --> 00:02:25,066
concluding the drug is not an improvement

53
00:02:25,088 --> 00:02:27,577
when the drug is actually an improvement.

54
00:02:27,644 --> 00:02:29,288
Now in the next video we will

55
00:02:29,333 --> 00:02:33,000
return to the definition of Null and alternative hypothesis

56
00:02:33,088 --> 00:02:35,466
and basically give you lots of examples

57
00:02:35,533 --> 00:02:38,977
that hopefully will make it easier for you to understand how statisticians

58
00:02:39,000 --> 00:02:42,022
setup Null and alternative Hypothesis.

