0
00:00:00,975 --> 00:00:03,280
Z-tests assume that you know the population mean and

1
00:00:03,280 --> 00:00:04,650
standard deviation.

2
00:00:04,650 --> 00:00:07,090
And as we know this is rarely the case.

3
00:00:07,090 --> 00:00:10,172
When we don't know these parameters, we use a t-test.

4
00:00:10,172 --> 00:00:13,135
The t-test is based on student's t-distribution.

5
00:00:13,135 --> 00:00:16,448
Whereas a normal distribution describes a full population,

6
00:00:16,448 --> 00:00:20,568
t-distributions describe samples drawn from a full population.

7
00:00:20,568 --> 00:00:25,150
Accordingly, the t-distribution for each sample size is different, but

8
00:00:25,150 --> 00:00:28,760
the larger the sample the more the distribution resembles a normal

9
00:00:28,760 --> 00:00:30,460
distribution.

10
00:00:30,460 --> 00:00:32,970
Because the curve changes based on sample size,

11
00:00:32,970 --> 00:00:36,370
when you find the probability of a given t value, you need not

12
00:00:36,370 --> 00:00:40,480
only the alpha or significance level, but the sample size as well.

13
00:00:40,480 --> 00:00:42,795
However, it's not quite as simple as that.

14
00:00:42,795 --> 00:00:46,091
The t-distribution is based on samples, so

15
00:00:46,091 --> 00:00:51,320
we have to actually find the t-value for n-1 rather than just n.

16
00:00:51,320 --> 00:00:53,860
This is known as degrees of freedom.

17
00:00:53,860 --> 00:00:57,120
Degrees of freedom is a number of values in the final calculation of

18
00:00:57,120 --> 00:00:59,230
a statistic that are free to vary and

19
00:00:59,230 --> 00:01:01,700
still end up with the same value.

20
00:01:01,700 --> 00:01:06,600
For example, to get the same mean, we can vary all elements except one.

21
00:01:06,600 --> 00:01:08,230
If we can control that one element,

22
00:01:08,230 --> 00:01:10,930
we can always end up with the mean that we started with.

23
00:01:10,930 --> 00:01:14,297
This concept is applied to all statistical calculations based on

24
00:01:14,297 --> 00:01:14,854
samples.

