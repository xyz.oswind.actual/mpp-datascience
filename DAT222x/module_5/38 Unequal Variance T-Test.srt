0
00:00:00,960 --> 00:00:04,591
For two independent sample T test with unequal variances, you start by

1
00:00:04,591 --> 00:00:07,752
defining your hypothesis, again, taking on the same form.

2
00:00:07,752 --> 00:00:11,150
Equality, or one mean being greater or less than the other.

3
00:00:11,150 --> 00:00:13,590
You specify your significance level.

4
00:00:13,590 --> 00:00:17,450
The equation is the same as what you saw when the variances are equal,

5
00:00:17,450 --> 00:00:20,900
but the scary bit is what unequal variances does to how you

6
00:00:20,900 --> 00:00:22,930
calculate your degrees of freedom.

7
00:00:22,930 --> 00:00:24,460
It looks like this.

8
00:00:24,460 --> 00:00:26,870
So let's go back to our cupcake example.

9
00:00:26,870 --> 00:00:30,090
Recall that we are interested in comparing the number of cupcakes per

10
00:00:30,090 --> 00:00:32,550
just by men and women during the month of February.

11
00:00:32,550 --> 00:00:34,920
In this example, we leave the mean the same again,

12
00:00:34,920 --> 00:00:39,280
65.2 25 for men, 62.05 for women.

13
00:00:39,280 --> 00:00:43,600
But we'll change the sample sizes to 20 for men and 100 women.

14
00:00:43,600 --> 00:00:48,779
The sample variance for men is still 20.15 and for women it's 6.75.

15
00:00:48,779 --> 00:00:50,770
Using an alpha of 0.05 then,

16
00:00:50,770 --> 00:00:54,560
do men purchase more cupcakes in February than women?

17
00:00:54,560 --> 00:00:57,980
First, we need to confirm if our variances are equivalent.

18
00:00:57,980 --> 00:01:02,630
Using the process that Wayne showed, you see that the confidence interval

19
00:01:02,630 --> 00:01:06,210
does not contain one, so we reject the null hypothesis.

20
00:01:06,210 --> 00:01:08,362
These variances are unequal.

21
00:01:08,362 --> 00:01:10,790
Now that we have confirmed that they're unequal,

22
00:01:10,790 --> 00:01:14,490
let's plug the information in to the equation and see what we get.

23
00:01:14,490 --> 00:01:20,070
We obtain a T square of 2.79 with 21.61 degrees of freedom.

24
00:01:20,070 --> 00:01:24,000
I know that formula looks scary and honestly, it is, but it's really

25
00:01:24,000 --> 00:01:26,710
just a matter of plugging everything into the right spots.

26
00:01:26,710 --> 00:01:28,760
Because I like to err on the side of caution,

27
00:01:28,760 --> 00:01:32,460
I'm going to find the critical T value for 21 degrees of freedom.

28
00:01:32,460 --> 00:01:35,240
Looking at the T table, the critical value that we need to exceed for

29
00:01:35,240 --> 00:01:39,497
significance of alpha at 0.05 is 1.72.

30
00:01:39,497 --> 00:01:41,800
In this example, we do exceed that value so

31
00:01:41,800 --> 00:01:43,930
we reject the null hypothesis.

32
00:01:43,930 --> 00:01:47,250
Men purchase more cupcakes than women in the month of February.

33
00:01:47,250 --> 00:01:50,660
Let's take a look at how to run a two-independent sample T test with

34
00:01:50,660 --> 00:01:52,490
unequal variances in Excel.

35
00:01:52,490 --> 00:01:54,980
You don't have to worry about how to calculate the degrees of freedom,

36
00:01:54,980 --> 00:01:56,150
Excel does it for you.

