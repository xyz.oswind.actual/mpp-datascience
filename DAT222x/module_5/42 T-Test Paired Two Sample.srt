0
00:00:00,880 --> 00:00:03,480
Let's take a closer look at paired sample t-test.

1
00:00:03,480 --> 00:00:06,620
This is also known as a dependent samples t-test.

2
00:00:06,620 --> 00:00:08,360
As with every other test that we've discussed,

3
00:00:08,360 --> 00:00:11,340
you start by stating your null and alternative hypotheses.

4
00:00:11,340 --> 00:00:14,599
They look a little different in the case of a paired sample t-test,

5
00:00:14,599 --> 00:00:17,629
because we are concerned with new variable d, which is based on

6
00:00:17,629 --> 00:00:20,622
the difference between paired values from the two data sets.

7
00:00:20,622 --> 00:00:23,999
Where x1 is the value of the variable x in the first data set,

8
00:00:23,999 --> 00:00:27,643
and x2 is the value of the variable from the second data set that is

9
00:00:27,643 --> 00:00:29,230
paired with x1.

10
00:00:29,230 --> 00:00:31,365
The hypothesis looks like this,

11
00:00:31,365 --> 00:00:34,665
where mu d is the mean difference between the pairs.

12
00:00:34,665 --> 00:00:37,565
In most cases, you will likely hypothesize that there is no

13
00:00:37,565 --> 00:00:38,975
difference between the groups.

14
00:00:38,975 --> 00:00:41,817
But you could specify a difference in your hypotheses and test for

15
00:00:41,817 --> 00:00:43,365
that specifically.

16
00:00:43,365 --> 00:00:45,445
Next, of course, you select your confidence level and

17
00:00:45,445 --> 00:00:47,030
then you conduct your study.

18
00:00:47,030 --> 00:00:48,040
Once you have your data,

19
00:00:48,040 --> 00:00:51,720
you use this formula to determine if you have significant difference.

20
00:00:51,720 --> 00:00:52,779
In this formula,

21
00:00:52,779 --> 00:00:56,317
d bar is the mean difference between paired observations.

22
00:00:56,317 --> 00:01:00,484
To calculate this, you simply sum all the d values obtained for

23
00:01:00,484 --> 00:01:02,922
the difference between each pair and

24
00:01:02,922 --> 00:01:06,318
divide by the total number of d values in your data.

25
00:01:06,318 --> 00:01:08,160
D0 is the expected difference.

26
00:01:08,160 --> 00:01:11,610
In most cases, this will be zero, but it could be any value.

27
00:01:11,610 --> 00:01:15,542
The denominator is the standard error of the differences, or

28
00:01:15,542 --> 00:01:16,227
d values.

29
00:01:16,227 --> 00:01:19,892
sd is the standard deviation of your d values calculated in the exact

30
00:01:19,892 --> 00:01:23,440
same way that you calculate any standard deviation.

31
00:01:23,440 --> 00:01:26,170
Finally, the degrees of freedom are n minus,

32
00:01:26,170 --> 00:01:28,930
because the statistic is based on one mean.

33
00:01:28,930 --> 00:01:31,790
That is, the difference between your matched pairs.

34
00:01:31,790 --> 00:01:33,870
So let's take a look at an example.

35
00:01:33,870 --> 00:01:36,080
Imagine that we have matched pairs where some,

36
00:01:36,080 --> 00:01:39,770
those people that are circled, are interacting with interface A, and

37
00:01:39,770 --> 00:01:43,050
the other in the pair are interacting with interface B.

38
00:01:43,050 --> 00:01:46,070
We are trying to determine if either interface results in

39
00:01:46,070 --> 00:01:48,480
less time on task than the other.

40
00:01:48,480 --> 00:01:49,050
As a result,

41
00:01:49,050 --> 00:01:53,520
we have a two tailed hypothesis test with a significance level of 0.05.

42
00:01:53,520 --> 00:01:55,510
Our data looks like this.

43
00:01:55,510 --> 00:01:59,513
We calculate the differences between each pair, calculate the mean and

44
00:01:59,513 --> 00:02:02,138
standard deviation of those differences, and

45
00:02:02,138 --> 00:02:05,959
then we plug all of that into the formula for t value of 0.2544.

46
00:02:05,959 --> 00:02:09,427
Given the small sample size resulting in a small number of

47
00:02:09,427 --> 00:02:13,760
degrees of freedom, I don't have to even check a t table to determine if

48
00:02:13,760 --> 00:02:16,780
the result is statistically significant.

49
00:02:16,780 --> 00:02:17,587
I know it's not.

50
00:02:17,587 --> 00:02:20,480
And by now you probably recognize that as well.

51
00:02:20,480 --> 00:02:24,411
However, to formally draw our conclusion, the critical T value for

52
00:02:24,411 --> 00:02:28,156
a two-tailed t-test with 7 degrees of freedom is -2.36.

53
00:02:28,156 --> 00:02:30,700
And our t-value is nowhere near that.

54
00:02:30,700 --> 00:02:34,145
There doesn't appear to be any differences in the interfaces in

55
00:02:34,145 --> 00:02:35,369
terms of time on task.

56
00:02:35,369 --> 00:02:37,668
Now, let's take a look at how we do this in Excel.

