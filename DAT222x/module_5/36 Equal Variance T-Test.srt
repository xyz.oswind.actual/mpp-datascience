0
00:00:00,710 --> 00:00:04,430
For two independent sample t-tests with equal variances, you start by

1
00:00:04,430 --> 00:00:07,315
defining your hypotheses, just like you've always done.

2
00:00:07,315 --> 00:00:10,070
It may be for equality or for one mean being greater than or

3
00:00:10,070 --> 00:00:11,210
less than the other.

4
00:00:11,210 --> 00:00:12,747
You specify your significance level.

5
00:00:12,747 --> 00:00:15,621
What's new with the t-test is that you need to determine your degrees

6
00:00:15,621 --> 00:00:16,165
of freedom.

7
00:00:16,165 --> 00:00:20,280
Because this is needed to define the critical value for significance.

8
00:00:20,280 --> 00:00:23,814
When samples have equal variances the degrees of freedom are simply

9
00:00:23,814 --> 00:00:25,745
the sum of the sample sizes minus 2.

10
00:00:25,745 --> 00:00:27,747
Given what you've learned so far.

11
00:00:27,747 --> 00:00:31,285
Would it surprise you to learn that the equation for a two independent

12
00:00:31,285 --> 00:00:33,908
sample t test looked very much like the equation for

13
00:00:33,908 --> 00:00:34,949
a two sample z test?

14
00:00:34,949 --> 00:00:38,255
The only difference is that the sample variances are used because

15
00:00:38,255 --> 00:00:40,820
the population variances are unknown.

16
00:00:40,820 --> 00:00:42,750
Let's look at our cupcake example.

17
00:00:42,750 --> 00:00:44,741
Recall that we were interested in comparing the number

18
00:00:44,741 --> 00:00:47,257
of cupcakes purchased by men and women during the month of February.

19
00:00:47,257 --> 00:00:52,180
In this example, we'll leave the means as 65.25 for men,

20
00:00:52,180 --> 00:00:54,197
as 62.09 for women.

21
00:00:54,197 --> 00:00:57,437
But we'll change the sample sizes to five men and ten women.

22
00:00:57,437 --> 00:01:02,197
The sample variance is 20.15 and for women it's 26.75.

23
00:01:02,197 --> 00:01:03,834
Using an alpha of 0.05,

24
00:01:03,834 --> 00:01:07,560
do men purchase more cupcakes in February than women do?

25
00:01:07,560 --> 00:01:10,080
First we need to confirm that our variances are equal.

26
00:01:10,080 --> 00:01:12,090
Using the process that Wayne showed you,

27
00:01:12,090 --> 00:01:14,840
we see that the confidence interval does contain one.

28
00:01:14,840 --> 00:01:17,100
So we failed to reject the null hypothesis.

29
00:01:17,100 --> 00:01:19,500
These variances are equal.

30
00:01:19,500 --> 00:01:21,513
Now that we have confirmed that they're equal,

31
00:01:21,513 --> 00:01:23,587
let's plug this information into the equation.

32
00:01:23,587 --> 00:01:26,227
We obtain a t score of 1.22.

33
00:01:26,227 --> 00:01:30,538
With 5 plus 10 minus 2, or 13 degrees of freedom.

34
00:01:30,538 --> 00:01:34,808
Looking at a t table, the critical value that we need to exceed for

35
00:01:34,808 --> 00:01:38,317
significance at alpha equals 0.05 is 1.77.

36
00:01:38,317 --> 00:01:39,837
We do not exceed that value.

37
00:01:39,837 --> 00:01:43,140
So we fail to reject the null hypothesis in this case.

38
00:01:43,140 --> 00:01:44,570
There is no difference between men and

39
00:01:44,570 --> 00:01:46,885
women in the purchase of cupcakes.

40
00:01:46,885 --> 00:01:50,655
Now let's look at how to run a two independent sample t test with equal

41
00:01:50,655 --> 00:01:51,828
variances in Excel.

