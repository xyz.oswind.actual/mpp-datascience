0
00:00:00,730 --> 00:00:04,500
There are two types of errors that can occur during hypothesis testing.

1
00:00:04,500 --> 00:00:07,270
A type I error occurs when the researcher rejects

2
00:00:07,270 --> 00:00:09,400
a null hypothesis when it is true.

3
00:00:09,400 --> 00:00:11,920
The probability of committing a type I error

4
00:00:11,920 --> 00:00:13,540
is called the significance level.

5
00:00:13,540 --> 00:00:16,630
This is the probability that's also called alpha.

6
00:00:16,630 --> 00:00:19,280
A type II error occurs when the researcher fails to

7
00:00:19,280 --> 00:00:21,570
reject a null hypothesis that is false.

8
00:00:21,570 --> 00:00:25,660
The probability of committing a type II error is called beta.

9
00:00:25,660 --> 00:00:27,260
Here's another way to think about this.

10
00:00:27,260 --> 00:00:30,000
There are two possible decisions that can be made.

11
00:00:30,000 --> 00:00:33,210
You can fail to reject the null or you can reject it.

12
00:00:33,210 --> 00:00:35,350
Taking the truth into consideration,

13
00:00:35,350 --> 00:00:37,830
you can see how each of these errors plays out.

14
00:00:37,830 --> 00:00:41,460
If you reject the null when you shouldn't, that's a type I error,

15
00:00:41,460 --> 00:00:46,060
and its probability is equivalent to alpha or your significance level.

16
00:00:46,060 --> 00:00:49,200
If you fail to reject the null hypothesis when you should, you have

17
00:00:49,200 --> 00:00:53,530
committed a type II error, whose probability is equivalent to beta.

18
00:00:53,530 --> 00:00:55,460
Now, let's look at an example.

19
00:00:55,460 --> 00:00:57,590
Imagine that we manufacture hiking boots.

20
00:00:57,590 --> 00:01:00,210
We conduct a study comparing the quality of our boots that were

21
00:01:00,210 --> 00:01:02,450
manufactured in several different countries.

22
00:01:02,450 --> 00:01:05,780
Our null hypothesis is that there is no difference in quality based on

23
00:01:05,780 --> 00:01:08,150
where the boots are manufactured.

24
00:01:08,150 --> 00:01:10,800
Our alternative hypothesis is that country

25
00:01:10,800 --> 00:01:13,380
A produces the highest quality of boots.

26
00:01:13,380 --> 00:01:16,100
If the truth is that there are no differences, and

27
00:01:16,100 --> 00:01:19,360
we don't reject the null, we know that we can manufacture boots

28
00:01:19,360 --> 00:01:21,550
anywhere without sacrificing quality.

29
00:01:21,550 --> 00:01:23,700
This is a correct decision.

30
00:01:23,700 --> 00:01:28,160
If we reject the null, and quality in country A is actually better,

31
00:01:28,160 --> 00:01:30,760
we know that we should manufacture boots only in country

32
00:01:30,760 --> 00:01:32,980
A if we want to ensure the highest quality.

33
00:01:32,980 --> 00:01:34,817
Another correct decision.

34
00:01:34,817 --> 00:01:38,302
However, if quality is actually better in country A, and

35
00:01:38,302 --> 00:01:41,645
we do not reject the null, that there is no difference,

36
00:01:41,645 --> 00:01:43,653
we will make the wrong decision.

37
00:01:43,653 --> 00:01:46,283
And we will manufacture the boots in any country,

38
00:01:46,283 --> 00:01:50,120
which will likely result in lower quality boots overall.

39
00:01:50,120 --> 00:01:52,840
If we reject the null when there is no difference,

40
00:01:52,840 --> 00:01:56,270
we may decide to manufacture our boots only in country A,

41
00:01:56,270 --> 00:01:59,250
which might be a costly mistake if the cost of manufacturing in

42
00:01:59,250 --> 00:02:01,974
that country are higher than in other locations.

43
00:02:01,974 --> 00:02:05,330
There is one more aspect to this that I want to mention.

44
00:02:05,330 --> 00:02:08,723
The probability of not committing a type II error is what's called

45
00:02:08,723 --> 00:02:10,820
the power of the test.

46
00:02:10,820 --> 00:02:13,060
Power is affected by the sample size.

47
00:02:13,060 --> 00:02:14,710
Other things been equal,

48
00:02:14,710 --> 00:02:18,290
the greater the sample size the greater the power of the test.

49
00:02:18,290 --> 00:02:19,500
It should be obvious,

50
00:02:19,500 --> 00:02:24,310
as well, that power is also affected by the significance or alpha level.

51
00:02:24,310 --> 00:02:26,190
If you increase the significance level,

52
00:02:26,190 --> 00:02:29,680
you have reduced the region of acceptance.

53
00:02:29,680 --> 00:02:33,140
As a result, you are more likely to reject the null hypothesis.

54
00:02:33,140 --> 00:02:36,830
If you are more likely to reject the null hypothesis when it is false,

55
00:02:36,830 --> 00:02:39,230
you are less likely to make a type II error.

56
00:02:39,230 --> 00:02:41,500
Hence, the power of the test has increased.

57
00:02:41,500 --> 00:02:45,806
However, the probability of type I error increases because you are more

58
00:02:45,806 --> 00:02:48,358
likely to reject the null when it's true.

