0
00:00:00,640 --> 00:00:04,060
So the level of significance chosen for our test is rather arbitrary.

1
00:00:04,060 --> 00:00:07,384
We usually use 0.05, but why not 0.01, why not 0.1?

2
00:00:07,384 --> 00:00:11,250
For that reason, most statisticians use the concept of a probability

3
00:00:11,250 --> 00:00:15,090
value, or p-value, to report the outcome of a hypothesis test.

4
00:00:15,090 --> 00:00:18,490
So for any hypothesis test, not just this one sample z-test,

5
00:00:18,490 --> 00:00:22,275
the p-value for our hypothesis test is the smallest value of alpha for

6
00:00:22,275 --> 00:00:26,090
which the data indicates rejection of the null hypothesis.

7
00:00:26,090 --> 00:00:29,001
Thus, the p-value would be less than or equal alpha, if and

8
00:00:29,001 --> 00:00:30,324
only if we reject the null.

9
00:00:30,324 --> 00:00:33,354
The p-value would be greater than the alpha if it only if we accept

10
00:00:33,354 --> 00:00:34,820
the null hypothesis.

11
00:00:34,820 --> 00:00:38,076
Another interpretation of the p-value is the probability of

12
00:00:38,076 --> 00:00:41,160
observing, given the null hypothesis is true.

13
00:00:41,160 --> 00:00:45,190
A value of the test statistic at least as extreme as the observed

14
00:00:45,190 --> 00:00:46,370
value of the test statistic.

15
00:00:46,370 --> 00:00:48,398
So let’s go back to our Cooley High example.

16
00:00:48,398 --> 00:00:52,000
So that depended of XBAR, the results of the test.

17
00:00:52,000 --> 00:00:53,780
So XBAR is the random variable for

18
00:00:53,780 --> 00:00:56,610
the sample mean under the null hypothesis.

19
00:00:56,610 --> 00:01:00,740
And again our level of significance is rather arbitrary.

20
00:01:00,740 --> 00:01:04,560
So how would we determine the p-values for the one sample z-test?

21
00:01:04,560 --> 00:01:07,550
Well, they're right here, okay.

22
00:01:07,550 --> 00:01:12,140
So suppose we have a one-tailed upper alternative.

23
00:01:12,140 --> 00:01:14,710
In other words, the null as mu is less than or equal to mu 0.

24
00:01:14,710 --> 00:01:18,645
Well, basically, the p-value is the probability the sample mean is

25
00:01:18,645 --> 00:01:22,190
greater than or equal to the observed value of the sample mean.

26
00:01:22,190 --> 00:01:23,170
That's the little x there.

27
00:01:23,170 --> 00:01:28,950
In other words, being extreme would mean we have a big

28
00:01:28,950 --> 00:01:32,230
value of the sample mean cuz that's what the alternative indicates.

29
00:01:32,230 --> 00:01:36,540
Now, if the alternative is a one-tailed lowered alternative,

30
00:01:36,540 --> 00:01:39,750
then the p-value would be the chance that the sample mean is

31
00:01:39,750 --> 00:01:41,660
vertical to what we've observed.

32
00:01:41,660 --> 00:01:46,643
And then in a two-tailed test, the p-value would be the probability of

33
00:01:46,643 --> 00:01:51,463
the absolute value, basically, of the sample mean is greater than or

34
00:01:51,463 --> 00:01:56,121
equal to the absolute value of how far our observed sample mean value

35
00:01:56,121 --> 00:02:01,620
is from the hypothesized value of the population mean in the null.

36
00:02:01,620 --> 00:02:04,360
Let's get the p-values from our Cooley High example.

37
00:02:04,360 --> 00:02:08,110
The key thing to remember is all our probabilities will be computed

38
00:02:08,110 --> 00:02:11,730
under the assumption that the null hypothesis is true.

39
00:02:11,730 --> 00:02:15,612
So our null hypothesis was that the mean score was equal to the state

40
00:02:15,612 --> 00:02:16,663
average of 75.

41
00:02:16,663 --> 00:02:20,081
The alternative was that the mean score was not equal to 75.

42
00:02:20,081 --> 00:02:23,290
So we're using this third formula for p-value.

43
00:02:23,290 --> 00:02:25,950
Okay, so the p-value would be the probability

44
00:02:25,950 --> 00:02:28,530
of seeing something more extreme than what we observed.

45
00:02:28,530 --> 00:02:31,538
We observed a sample mean of 79, okay.

46
00:02:31,538 --> 00:02:34,190
So 79 minus 75 is 4.

47
00:02:34,190 --> 00:02:37,085
So the x is 79.

48
00:02:37,085 --> 00:02:38,030
The mu 0 is 75.

49
00:02:38,030 --> 00:02:43,400
So basically, you want to know, what's the chance that basically

50
00:02:44,740 --> 00:02:48,920
the absolute value of XBAR minus 75 would be greater than or equal to 4?

51
00:02:48,920 --> 00:02:52,947
Okay, well, if you go 75 plus 4, you have 79.

52
00:02:52,947 --> 00:02:57,289
So, basically, you could take the chance that XBAR is greater than or

53
00:02:57,289 --> 00:03:01,480
equal to 79, or go 4 below 75 to 71, where XBAR is less than or

54
00:03:01,480 --> 00:03:02,460
equal to 71.

55
00:03:02,460 --> 00:03:05,380
And, basically, you can just do one of those probabilities and

56
00:03:05,380 --> 00:03:08,799
double it because the normal random variable is symmetric.

57
00:03:08,799 --> 00:03:11,979
So, basically, what you can do is take what's the probability that

58
00:03:11,979 --> 00:03:14,290
XBAR is greater than or equal to 79?

59
00:03:14,290 --> 00:03:17,890
Well, it's one minus the chance that XBAR is less than or equal to 79, so

60
00:03:17,890 --> 00:03:19,830
we can use the NORM DIST function.

61
00:03:19,830 --> 00:03:22,470
So we put in NORM.DIST 70,

62
00:03:22,470 --> 00:03:26,980
not one minus NORM.DIST 79 is the chance where greater or equal to 79.

63
00:03:26,980 --> 00:03:28,990
Then the mean by the null is 75,

64
00:03:28,990 --> 00:03:32,135
but what's the standard deviation of XBAR?

65
00:03:32,135 --> 00:03:36,142
From module four, we know it's the population standard deviation of 15

66
00:03:36,142 --> 00:03:39,461
divided by the square root of the sample size, which is 49.

67
00:03:39,461 --> 00:03:44,935
So basically we can compute that and we'd get 0.030974

68
00:03:44,935 --> 00:03:50,116
from basically one minus the NORM.DIST of this situation.

69
00:03:50,116 --> 00:03:53,416
So 2 times 0.03 would be 0.06.

70
00:03:53,416 --> 00:03:58,120
So our p-value of 0.06 means basically hat's sort of the level of

71
00:03:58,120 --> 00:04:01,800
proof that our data gives us to reject the null.

72
00:04:01,800 --> 00:04:05,160
A lower p-value means you have more proof to reject the null.

73
00:04:05,160 --> 00:04:09,071
But that 0.06 was greater than the level of significance of 0.05.

74
00:04:09,071 --> 00:04:12,570
For a two-tailed test, we'd accept the null hypothesis.

75
00:04:12,570 --> 00:04:14,897
Now suppose we had the one-tailed test,

76
00:04:14,897 --> 00:04:19,028
in other words that Cooley High was better than mu greater than 0.75.

77
00:04:19,028 --> 00:04:22,829
Then the p-value would simply be the probability XBAR is greater or

78
00:04:22,829 --> 00:04:25,505
equal to 79, which we know is this 0.03.

79
00:04:25,505 --> 00:04:27,401
So we'd have the p-value of 0.03.

80
00:04:27,401 --> 00:04:31,288
And when the p-value is less than or equal to alpha, in this case 0.05,

81
00:04:31,288 --> 00:04:32,990
we reject the null.

82
00:04:32,990 --> 00:04:33,882
So, basically,

83
00:04:33,882 --> 00:04:37,260
that's very consistent with what we said up above here.

84
00:04:37,260 --> 00:04:41,450
Okay, if the p-value is less than or equal to alpha, we reject the null.

85
00:04:41,450 --> 00:04:44,388
P-value greater than alpha, we reject the null, and it's an if and

86
00:04:44,388 --> 00:04:45,750
only if relationship.

87
00:04:45,750 --> 00:04:48,930
So if alpha is 0.05 and you get a p-value of less than or

88
00:04:48,930 --> 00:04:50,988
equal to 0.05, you reject the null.

89
00:04:50,988 --> 00:04:54,938
If alpha is 0.05, you get a p-value of greater than 0.05,

90
00:04:54,938 --> 00:04:56,340
you accept the null.

91
00:04:56,340 --> 00:05:00,162
And this'll be crucial to our understanding of all hypothesis

92
00:05:00,162 --> 00:05:00,673
tests.

93
00:05:00,673 --> 00:05:04,068
And if you go on to study more data science or statistics,

94
00:05:04,068 --> 00:05:08,192
you'll see that the p-value concept is of incredible importance.

95
00:05:08,192 --> 00:05:11,747
In the next video, we'll discuss the one sample t-test,

96
00:05:11,747 --> 00:05:15,231
which is basically what you use when you have a small sample

97
00:05:15,231 --> 00:05:19,520
from a normal population and you don't know the population variance.

