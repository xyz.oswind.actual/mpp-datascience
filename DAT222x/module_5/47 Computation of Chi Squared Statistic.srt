0
00:00:00,500 --> 00:00:05,740
So what's the test statistic we used to determine whether eye color and

1
00:00:05,740 --> 00:00:08,520
gender are independent or not independent?

2
00:00:08,520 --> 00:00:12,170
Well what we have to do is under the assumption of independence, compute

3
00:00:12,170 --> 00:00:16,160
the expected number of people that would fall into each of these cells.

4
00:00:16,160 --> 00:00:20,270
Female blue eyes, female brown eyes through male hazel eyes.

5
00:00:21,340 --> 00:00:22,640
That's called EIJ,

6
00:00:22,640 --> 00:00:25,890
the expected number in the cell under Independence.

7
00:00:25,890 --> 00:00:30,590
And what you do to calculate that is take the total number of people,

8
00:00:30,590 --> 00:00:31,130
which we've got.

9
00:00:32,230 --> 00:00:36,303
We actually have the total number of people here.

10
00:00:38,570 --> 00:00:47,470
We can just add these up here, 2035 people.

11
00:00:47,470 --> 00:00:48,770
So you want to ask yourself,

12
00:00:48,770 --> 00:00:52,310
what fraction of people would you expect to be female with blue eyes,

13
00:00:52,310 --> 00:00:55,315
if there was independence between eye color and gender?

14
00:00:55,315 --> 00:00:58,630
Well take the total number of people time the fraction that would

15
00:00:58,630 --> 00:01:00,730
basically be female.

16
00:01:00,730 --> 00:01:03,150
That's P seven minus the total, and

17
00:01:03,150 --> 00:01:07,750
then multiply by the fraction that basically had blue eyes.

18
00:01:07,750 --> 00:01:08,850
Cuz we had independence,

19
00:01:08,850 --> 00:01:12,450
remember back to module two, you can get the probability

20
00:01:12,450 --> 00:01:16,400
of independent events by multiplying there respective probabilities.

21
00:01:16,400 --> 00:01:19,530
And so under independence, you can find the fraction of

22
00:01:19,530 --> 00:01:23,820
basically people who are female with blue eyes by taking the fraction of

23
00:01:23,820 --> 00:01:27,600
females times the fraction with blue eyes times the total.

24
00:01:27,600 --> 00:01:29,850
Now, if they weren't independent, this wouldn't work.

25
00:01:29,850 --> 00:01:32,200
So we'd expect when we get this expected values,

26
00:01:32,200 --> 00:01:34,480
they'd be pretty close to the observe values.

27
00:01:34,480 --> 00:01:36,610
So how do we statistically measure that?

28
00:01:36,610 --> 00:01:40,580
We take the observe value minus the expected value in each cell square,

29
00:01:40,580 --> 00:01:45,870
divided the normalized by the expected, by the expected value, and

30
00:01:45,870 --> 00:01:49,150
then we add those all up and that gets our test statistic.

31
00:01:49,150 --> 00:01:52,400
And that'll be compared to basically a critical value

32
00:01:52,400 --> 00:01:55,460
from the chi-squared table based on degrees of freedom.

33
00:01:55,460 --> 00:01:59,400
There's a function chi-squared dot inverse and the 95th percentile

34
00:01:59,400 --> 00:02:02,350
would be the critical value so I put some of those here.

35
00:02:02,350 --> 00:02:05,420
If you had one degree of freedom and you add up all these

36
00:02:05,420 --> 00:02:09,015
complicated things and it's greater than 3.84, you reject the null.

37
00:02:09,015 --> 00:02:11,660
Two degrees to freedom and

38
00:02:11,660 --> 00:02:14,065
it's greater than 5.99 you reject the null.

39
00:02:14,065 --> 00:02:16,040
Three degrees of freedom and it's greater or

40
00:02:16,040 --> 00:02:18,080
equal to 7.81, you reject the null.

41
00:02:19,210 --> 00:02:22,800
Four degrees of freedom greater or equal to 9.48 you reject the null.

42
00:02:22,800 --> 00:02:25,100
And in our example the number of rows is two and

43
00:02:25,100 --> 00:02:27,280
the number of columns is four.

44
00:02:27,280 --> 00:02:32,338
So basically with rows equal two and columns equal four we've

45
00:02:32,338 --> 00:02:38,590
got (2-1)x(4-1), we've got three degrees of freedom,or DF for short.

46
00:02:38,590 --> 00:02:42,250
So let's get those expected values in each cell, and

47
00:02:42,250 --> 00:02:46,170
then the test statistic would be the observed minus expected square

48
00:02:46,170 --> 00:02:49,640
divided by the expected in each cell, we add those together,

49
00:02:49,640 --> 00:02:52,200
we can get a total chi-square statistic.

50
00:02:52,200 --> 00:02:56,300
And then in the next video, we can conclude, basically should we

51
00:02:56,300 --> 00:02:59,250
accept the null hypothesis or reject the null hypothesis,

52
00:02:59,250 --> 00:03:02,800
and there'll be a couple of ways we can approach that final step.

53
00:03:02,800 --> 00:03:07,030
But we've gotta get these expected values in each cell, so

54
00:03:07,030 --> 00:03:08,340
that's the key step.

55
00:03:08,340 --> 00:03:10,420
So, again, how do we get the expected values?

56
00:03:10,420 --> 00:03:13,180
We take the total number of people here,

57
00:03:13,180 --> 00:03:15,060
we want a form that we can copy across.

58
00:03:15,060 --> 00:03:18,320
The total number there, times the fraction of women.

59
00:03:18,320 --> 00:03:25,190
Okay, so P7, Tells us how many women.

60
00:03:26,560 --> 00:03:28,640
Now I am gonna need to dollar-sign the P there,

61
00:03:28,640 --> 00:03:31,820
because I want to copy that across and not have the P change.

62
00:03:31,820 --> 00:03:35,050
And then I divide that by the total I've named that cell Total.

63
00:03:36,290 --> 00:03:38,285
We discussed that in module one how you can do that.

64
00:03:38,285 --> 00:03:40,960
Okay, so,

65
00:03:40,960 --> 00:03:45,110
that's the fraction of women times the total, and then I multiply times

66
00:03:45,110 --> 00:03:49,530
the fraction that basically have in this case blue eyes.

67
00:03:49,530 --> 00:03:58,105
OK, so blue eyes would be, Right there.

68
00:04:00,698 --> 00:04:05,141
Now I'm gonna need the dollar sign, the nine, because if you think about

69
00:04:05,141 --> 00:04:09,440
this when I copy this across and down what do I want to change?

70
00:04:09,440 --> 00:04:12,220
So basically I don't want the nine to change,

71
00:04:13,520 --> 00:04:19,280
because I've got my totals there.

72
00:04:19,280 --> 00:04:21,575
But I do want the L to change,

73
00:04:21,575 --> 00:04:26,770
cuz I want to get different color eyes as I go across.

74
00:04:26,770 --> 00:04:29,070
So that's L nine divided by the total.

75
00:04:31,550 --> 00:04:35,160
Okay so to recap what we have there, I mean, if there's independence,

76
00:04:35,160 --> 00:04:37,450
how many women would you expect with blue eyes?

77
00:04:37,450 --> 00:04:41,030
You take the total times the fraction of women that you have in

78
00:04:41,030 --> 00:04:45,990
your data set times the fraction of people who have blue eyes.

79
00:04:45,990 --> 00:04:50,330
And again, L9 has the blue-eyed people.

80
00:04:50,330 --> 00:04:52,560
And then basically you copy that across.

81
00:04:52,560 --> 00:04:56,890
Let's see what we get there, we get 396.56, that's right.

82
00:04:56,890 --> 00:05:02,100
So, when I copy that across, okay.

83
00:05:02,100 --> 00:05:04,560
Get 349 there, now I can drag this down.

84
00:05:07,711 --> 00:05:11,998
And then basically, these totals should match the observed totals,

85
00:05:11,998 --> 00:05:13,678
if we've done this right.

86
00:05:17,000 --> 00:05:19,280
So we get 1107, that's good.

87
00:05:19,280 --> 00:05:20,120
I copy that down.

88
00:05:20,120 --> 00:05:21,490
I get 928.

89
00:05:21,490 --> 00:05:25,560
So again, these are the expected number of, for instance,

90
00:05:25,560 --> 00:05:29,890
females with blue eyes, males with brown eyes, you'd expect if gender

91
00:05:29,890 --> 00:05:34,130
did not affect eye color cuz we multiply these fractions here.

92
00:05:34,130 --> 00:05:36,860
Now, what is the test statistic in each cell?

93
00:05:36,860 --> 00:05:42,290
It's going to be the observe value, that's going to be up here.

94
00:05:44,550 --> 00:05:47,050
Minus the expected value, which we just got.

95
00:05:49,970 --> 00:05:54,660
And we're going to square that, and

96
00:05:54,660 --> 00:05:59,040
then it says to divide by the expected, to normalize that.

97
00:06:01,690 --> 00:06:08,480
Now the expected is right there, and that should copy across fine.

98
00:06:08,480 --> 00:06:12,586
So I get a 1.78 there, drag that across and down.

99
00:06:16,873 --> 00:06:20,656
And we'll get a total for

100
00:06:20,656 --> 00:06:24,532
the chi square statistic, and then we'll analyze it in the next video.

101
00:06:27,868 --> 00:06:31,262
[INAUDIBLE] So we got a chi square statistic of 16.59, and

102
00:06:31,262 --> 00:06:34,655
in the next video, we'll show you whether or not you should,

103
00:06:34,655 --> 00:06:36,873
based on that test statistic, accept or

104
00:06:36,873 --> 00:06:40,680
reject the hypothesis that eye color, and gender are independent.

