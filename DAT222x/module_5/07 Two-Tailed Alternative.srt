0
00:00:00,870 --> 00:00:04,640
In this video, we'll discuss the Two-Tailed Alternative Hypothesis.

1
00:00:04,640 --> 00:00:05,850
So let's look at the example.

2
00:00:05,850 --> 00:00:09,080
Let's look at stock returns, annual stock returns in the U S and

3
00:00:09,080 --> 00:00:10,540
annual bond returns.

4
00:00:10,540 --> 00:00:12,660
And we'd be interested in the variance or

5
00:00:12,660 --> 00:00:15,180
the spread about the mean, for stock and bond returns.

6
00:00:15,180 --> 00:00:17,670
Well you probably would guess and

7
00:00:17,670 --> 00:00:21,390
you'd be right, that stock returns are more volatile than bond returns.

8
00:00:21,390 --> 00:00:25,070
So how would you set up hypotheses to test this view of the world?

9
00:00:25,070 --> 00:00:28,830
So the null hypothesis, would be the annual variance on stock returns

10
00:00:28,830 --> 00:00:31,519
would equal the annual variance on bond returns.

11
00:00:31,519 --> 00:00:35,057
And then you have no reason to think stocks would be more variable than

12
00:00:35,057 --> 00:00:38,231
bonds, so you'd set up the alternative hypothesis to be that

13
00:00:38,231 --> 00:00:40,854
the annual variance on stock returns is not equal,

14
00:00:40,854 --> 00:00:45,120
that's what this little sign is, to the annual variance on bond returns.

15
00:00:45,120 --> 00:00:47,790
So we don't have a directional alternative hypothesis here.

16
00:00:47,790 --> 00:00:50,510
We don't have a greater than sign or a less than sign.

17
00:00:50,510 --> 00:00:54,210
So this is called a Two-Tailed Alternative Hypothesis.

18
00:00:54,210 --> 00:00:56,390
And what we would do is look at let's say,

19
00:00:56,390 --> 00:01:00,830
the last ten years of annual stock returns and annual bond returns.

20
00:01:00,830 --> 00:01:03,610
Maybe take the ratio of the sample variances.

21
00:01:03,610 --> 00:01:06,010
And if that was significantly different than one,

22
00:01:06,010 --> 00:01:07,700
which we'll talk about later.

23
00:01:07,700 --> 00:01:11,160
We would conclude that stocks and bonds don't have the same annual

24
00:01:11,160 --> 00:01:15,390
variance, but the ratio of the sample variance of the stock returns

25
00:01:15,390 --> 00:01:18,555
the bond returns was like 0.95 close to 1.

26
00:01:18,555 --> 00:01:21,420
We'd probably accept the null hypothesis, that stocks and

27
00:01:21,420 --> 00:01:23,460
bonds are equally variable.

28
00:01:23,460 --> 00:01:26,210
So in summary we have a Two-Tailed Alternative Hypothesis of

29
00:01:26,210 --> 00:01:27,880
the alternative hypothesis,

30
00:01:27,880 --> 00:01:33,400
does not specify a directional deviation from the null hypothesis.

31
00:01:33,400 --> 00:01:35,930
In the next video, we'll talk about should you use

32
00:01:35,930 --> 00:01:38,598
a one-tailed alternative or a two-tailed alternative.

