0
00:00:00,669 --> 00:00:05,333
In this video, we'll talk about testing hypothesis involving whether

1
00:00:05,333 --> 00:00:09,200
or not two populations have equal variances.

2
00:00:09,200 --> 00:00:13,700
And so basically what we want to do is have a null hypothesis that

3
00:00:13,700 --> 00:00:16,230
the two populations have equal variance.

4
00:00:16,230 --> 00:00:19,180
An alternative hypothesis that their variances are not equal, so

5
00:00:19,180 --> 00:00:20,910
it would be a two tailed test.

6
00:00:20,910 --> 00:00:23,720
Now Excel will make this a snap, okay?

7
00:00:23,720 --> 00:00:27,400
There's in F.TEST function that we can use.

8
00:00:27,400 --> 00:00:30,120
We just put in the two sets of data,

9
00:00:30,120 --> 00:00:33,480
the samples from the two populations and we get the PVALUE.

10
00:00:33,480 --> 00:00:37,010
And again, if the PVALUE is less than or equal to alpha, we reject

11
00:00:37,010 --> 00:00:40,940
the null hypothesis, otherwise we accept the null hypothesis.

12
00:00:40,940 --> 00:00:44,510
We have to be careful though, we sort of convince ourselves that our

13
00:00:44,510 --> 00:00:47,610
data sets come from a normal population.

14
00:00:47,610 --> 00:00:50,729
One way to do that is get the skewness and kurtosis and

15
00:00:50,729 --> 00:00:54,450
most statisticians assume that they are between plus one and minus one.

16
00:00:54,450 --> 00:00:57,400
Then you have a fairly good confidence

17
00:00:57,400 --> 00:00:59,990
that your data comes from a normal population.

18
00:00:59,990 --> 00:01:04,010
So in the next video, we'll do a specific example of the F test for

19
00:01:04,010 --> 00:01:07,230
testing equal variances and you'll see Excel makes this so simple.

