0
00:00:00,860 --> 00:00:01,530
In this lesson,

1
00:00:01,530 --> 00:00:04,030
we will be discussing four different statistical tests

2
00:00:04,030 --> 00:00:06,470
that you can conduct when comparing two groups.

3
00:00:06,470 --> 00:00:10,543
The first, a two sample z-test is used when you have large sample

4
00:00:10,543 --> 00:00:15,310
sizes n or sample sizes greater than 30 from each of the populations.

5
00:00:15,310 --> 00:00:18,074
And the samples from the two populations are independent.

6
00:00:18,074 --> 00:00:21,512
Z-tests are preferred when you have large enough samples because it has

7
00:00:21,512 --> 00:00:23,080
a single critical value.

8
00:00:23,080 --> 00:00:26,590
For example, 1.964, an alpha of 0.05.

9
00:00:26,590 --> 00:00:29,970
Regardless of a sample size, which makes it much more convenient than

10
00:00:29,970 --> 00:00:34,400
a t-test which has separate critical values for each sample size.

11
00:00:34,400 --> 00:00:39,200
Second is a two sample t-test assuming equal variances.

12
00:00:39,200 --> 00:00:42,996
You can use this when you have small sample sizes less than 30 for

13
00:00:42,996 --> 00:00:46,725
at least one population, variances are unknown but equal, and

14
00:00:46,725 --> 00:00:50,790
the samples from the two populations are independent.

15
00:00:50,790 --> 00:00:54,170
Third is a two sample t-test assuming unequal variances,

16
00:00:54,170 --> 00:00:57,345
such as what might happen when one of your samples is much

17
00:00:57,345 --> 00:00:59,150
larger than the other.

18
00:00:59,150 --> 00:01:02,447
Remember, variances tend to become smaller as sample sizes increase.

19
00:01:02,447 --> 00:01:05,981
Use this when you have small sample sizes, less than 30 for

20
00:01:05,981 --> 00:01:09,032
at least one population, variances are unknown and

21
00:01:09,032 --> 00:01:13,900
unequal, and the samples from the two populations are independent.

22
00:01:13,900 --> 00:01:16,920
Finally, we'll discuss the paired two sample t-test.

23
00:01:16,920 --> 00:01:19,390
You use this when the two populations are normal.

24
00:01:19,390 --> 00:01:22,400
And the observations from the two populations can be paired in

25
00:01:22,400 --> 00:01:26,170
a natural fashion, such as the difference between Time 1 and

26
00:01:26,170 --> 00:01:29,520
Time 2 results on a pre and post-training assessment.

27
00:01:29,520 --> 00:01:32,690
By the way, all of these statistics that we've discussed including

28
00:01:32,690 --> 00:01:35,880
these, assume that the populations are normally distributed, which is

29
00:01:35,880 --> 00:01:38,940
another reason why the central limit theorem is so important.

