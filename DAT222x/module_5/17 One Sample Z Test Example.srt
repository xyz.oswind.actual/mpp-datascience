0
00:00:00,396 --> 00:00:03,520
Let's do an example of this one sample Z test.

1
00:00:03,520 --> 00:00:06,443
So let's suppose passing a standardized test is required for

2
00:00:06,443 --> 00:00:09,140
graduation in the state of Fredonia.

3
00:00:09,140 --> 00:00:13,048
And the average score in the state is 75 on that test.

4
00:00:13,048 --> 00:00:16,190
So you wanna know if Cooley High School, your high school,

5
00:00:16,190 --> 00:00:21,100
has basically a different ability on this test than the state as a whole.

6
00:00:21,100 --> 00:00:23,992
So you take a random sample of 49 students and

7
00:00:23,992 --> 00:00:27,563
you get the sample mean is 79 which is higher than 75.

8
00:00:27,563 --> 00:00:30,540
But is it significantly higher, that's the question.

9
00:00:30,540 --> 00:00:34,270
And supposed the sample standard deviation was 15 points on the test.

10
00:00:34,270 --> 00:00:37,520
For alpha equals 0.05, would you conclude Cooley High

11
00:00:37,520 --> 00:00:40,780
students performed differently than the typical state student.

12
00:00:40,780 --> 00:00:44,130
Okay, so the parameter of interest here is the mean score on the test

13
00:00:44,130 --> 00:00:46,810
at Cooley High and we don't have the data for everybody.

14
00:00:46,810 --> 00:00:49,070
We're just gonna sample 49 students.

15
00:00:49,070 --> 00:00:52,030
So there's no reason to believe Cooley High is better or

16
00:00:52,030 --> 00:00:54,990
worse than the typical person in the state.

17
00:00:54,990 --> 00:00:59,657
So our null hypothesis is the mean of 75, the alternative will be in

18
00:00:59,657 --> 00:01:02,960
this case, a two tale test, mu not equals 75.

19
00:01:02,960 --> 00:01:06,240
So I've written down here, what's the critical region in that case for

20
00:01:06,240 --> 00:01:07,770
the two tail test.

21
00:01:07,770 --> 00:01:11,023
You take the absolute value of the sample mean minus 75.

22
00:01:11,023 --> 00:01:14,648
In other words, how far did our sample mean deviate from 75 and

23
00:01:14,648 --> 00:01:18,208
compare it to 1.96 times the standard deviation divided by

24
00:01:18,208 --> 00:01:20,870
the square root of the sample size.

25
00:01:20,870 --> 00:01:23,410
Now we don't have the population standard deviation, so

26
00:01:23,410 --> 00:01:25,970
we'll use the sample standard deviation of 15.

27
00:01:25,970 --> 00:01:28,450
So what do we get in that case there?

28
00:01:28,450 --> 00:01:30,510
Well basically, x bar is 79.

29
00:01:30,510 --> 00:01:33,690
So we put in 79 for x bar.

30
00:01:33,690 --> 00:01:35,796
Mu 0, the hypothesized mean, is 75.

31
00:01:35,796 --> 00:01:40,471
So we have 79 minus 75, we'd say is that greater than equal to

32
00:01:40,471 --> 00:01:44,470
1.96 times 15 divided by the square root of 49.

33
00:01:44,470 --> 00:01:47,590
Okay, now you do the calculations on this basically.

34
00:01:47,590 --> 00:01:52,320
It's roughly 30 divided by 7 which is 4.2, okay.

35
00:01:52,320 --> 00:01:57,920
Now is 4 greater than 4.2, the left side is 4, the right side is 4.2.

36
00:01:57,920 --> 00:02:02,200
Four is not greater or equal to 4.2, so we're not in the critical region.

37
00:02:02,200 --> 00:02:06,285
We don't have enough proof to reject the null hypothesis.

38
00:02:06,285 --> 00:02:09,730
So we accept the null hypothesis and we conclude

39
00:02:09,730 --> 00:02:13,590
the average Cooley High score does not differ from the state average.

40
00:02:13,590 --> 00:02:16,740
But let's suppose that we spent a lot of money on Cooley High and

41
00:02:16,740 --> 00:02:19,600
we believe that Cooley High should be better than the state average cuz

42
00:02:19,600 --> 00:02:21,570
we spend tons of money there.

43
00:02:21,570 --> 00:02:23,560
So then we'd have a one tail test.

44
00:02:23,560 --> 00:02:28,120
The null hypothesis would be mu is equal to 75 and

45
00:02:28,120 --> 00:02:32,660
the alternative would be that mu is greater than 75 and

46
00:02:32,660 --> 00:02:34,950
we're using alpha equals 0.05 again.

47
00:02:34,950 --> 00:02:38,570
So what's the critical region in that case, copied from above?

48
00:02:38,570 --> 00:02:41,780
The sample means got to be really big the rejected null.

49
00:02:41,780 --> 00:02:43,380
Okay, how big?

50
00:02:43,380 --> 00:02:45,140
The hypothesized value plus 1.645,

51
00:02:45,140 --> 00:02:49,709
that comes from the alpha being 0.05 times

52
00:02:49,709 --> 00:02:53,500
the standard deviation divided by the square root of the sample size.

53
00:02:53,500 --> 00:02:57,194
So we say is x bar, that's 79, greater than or

54
00:02:57,194 --> 00:03:02,063
equal to mu zero, that's 75 plus 1.645 times the sample

55
00:03:02,063 --> 00:03:06,857
standard deviation of 15 divided by the square root of 49.

56
00:03:06,857 --> 00:03:11,710
Now that's roughly 75 + 15 times one and

57
00:03:11,710 --> 00:03:16,829
two-thirds would be about 25 divided by 7.

58
00:03:16,829 --> 00:03:21,526
That's about three and a half plus 75, that's 78.525.

59
00:03:21,526 --> 00:03:24,593
Now this 79 is greater and equal to the cut off point for

60
00:03:24,593 --> 00:03:26,270
the critical region.

61
00:03:26,270 --> 00:03:29,360
So for the one tailed test, we reject the null hypothesis.

62
00:03:29,360 --> 00:03:31,360
We get a different result.

63
00:03:31,360 --> 00:03:34,400
And basically, this is why I think people like the two tailed test if

64
00:03:34,400 --> 00:03:36,010
they wanna be conservative.

65
00:03:36,010 --> 00:03:38,480
So in other words, for the same alpha,

66
00:03:38,480 --> 00:03:41,240
it takes more proof to reject the null hypothesis.

67
00:03:41,240 --> 00:03:44,600
For two-tail test, we see here, for two-tailed test,

68
00:03:44,600 --> 00:03:46,030
we did not reject the null but

69
00:03:46,030 --> 00:03:49,950
for one-tail test, we did cuz a one-tail test requires less proof.

70
00:03:49,950 --> 00:03:53,894
That's why statisticians usually prefers two-tail test because it

71
00:03:53,894 --> 00:03:55,280
requires more proof.

72
00:03:55,280 --> 00:03:58,660
Now, in the next video, we'll talk about

73
00:03:58,660 --> 00:04:02,700
P values cuz it seems like the alpha level is fairly arbitrary.

74
00:04:02,700 --> 00:04:06,120
So probably a better way to state the results of a hypothesis tests is

75
00:04:06,120 --> 00:04:08,460
will see is the use of concept of P values.

