0
00:00:00,826 --> 00:00:02,430
Now that you have the basics,

1
00:00:02,480 --> 00:00:06,784
let's explore some statistics we'll start with a one sample z-test.

2
00:00:06,848 --> 00:00:09,504
You start by stating your hypothesis,

3
00:00:09,552 --> 00:00:11,648
let's imagine you are conducting a study about the

4
00:00:11,712 --> 00:00:14,960
average number of miles hiked by hikers in the Pacific Northwest in a

5
00:00:15,050 --> 00:00:19,664
given year. The population mean is 500 miles, but you think it might actually be

6
00:00:19,712 --> 00:00:22,860
higher than that. So, your null and alternate hypothesis are

7
00:00:22,928 --> 00:00:26,224
testing an upper one-tailed alternative hypothesis.

8
00:00:26,288 --> 00:00:28,380
Next you select your alpha or level of

9
00:00:28,448 --> 00:00:30,304
significance. In this case you select

10
00:00:30,368 --> 00:00:34,176
.05, how large does your sample mean need to be

11
00:00:34,272 --> 00:00:37,888
to reject the null hypothesis, to find the answer you add the

12
00:00:37,968 --> 00:00:39,980
population mean proposed in the

13
00:00:40,048 --> 00:00:41,820
null hypothesis to the product of the

14
00:00:41,920 --> 00:00:43,890
critical z-score at your desired level

15
00:00:44,000 --> 00:00:46,544
of alpha and the population standard error.

16
00:00:46,656 --> 00:00:49,020
When alpha equals .05,

17
00:00:49,072 --> 00:00:52,160
the critical z-score equals 1.645.

18
00:00:52,256 --> 00:00:56,848
If you know the population standard deviation is 52.63

19
00:00:56,912 --> 00:00:59,024
and we have sampled the 150 hikers

20
00:00:59,070 --> 00:01:04,464
we find that if our sample results in an average that is higher than 507.07

21
00:01:04,512 --> 00:01:06,992
we will reject the null hypothesis.

22
00:01:07,056 --> 00:01:10,260
More commonly though you calculate the z-score rather than

23
00:01:10,336 --> 00:01:12,400
the sample mean needed for significance.

24
00:01:12,460 --> 00:01:18,000
Imagine that we obtained a sample mean of 507.75

25
00:01:18,064 --> 00:01:19,500
using the critical value from the

26
00:01:19,568 --> 00:01:23,120
sample mean that we just calculated we know this would be significant

27
00:01:23,180 --> 00:01:29,104
because our obtained mean is larger than the required mean of 507.07.

28
00:01:29,184 --> 00:01:32,736
However, let's play it out just to see how it works when we use the

29
00:01:32,800 --> 00:01:38,128
z-score formula. In this case, you want to attain a value to be larger than

30
00:01:38,192 --> 00:01:41,488
the critical z-score of 1.645.

31
00:01:41,552 --> 00:01:44,784
To do this you subtract your obtained sample mean from the

32
00:01:44,864 --> 00:01:47,840
population mean and divide it by the standard error.

33
00:01:47,900 --> 00:01:54,320
As you can see the result 1.8 is greater than 1.645 so, we reject the

34
00:01:54,432 --> 00:01:58,170
null hypothesis. Now, let's take a look at

35
00:01:58,240 --> 00:02:00,400
this assuming a two-tailed test.

36
00:02:00,480 --> 00:02:04,080
In other words we're not sure if the average number of miles will be

37
00:02:04,128 --> 00:02:09,344
larger or smaller than 500, our alpha is still .05

38
00:02:09,376 --> 00:02:11,408
but because this is a two-tailed test

39
00:02:11,488 --> 00:02:13,520
we divide alpha by two and obtain a

40
00:02:13,600 --> 00:02:16,768
critical value for alpha equal to 0.25

41
00:02:16,832 --> 00:02:21,696
which is 1.96, one way to determine significance with the

42
00:02:21,776 --> 00:02:23,640
two-tailed test is to calculate how

43
00:02:23,712 --> 00:02:25,504
large the difference between the sample

44
00:02:25,536 --> 00:02:30,240
and population means need to be in order to reject the null hypothesis.

45
00:02:30,304 --> 00:02:34,480
To do this we multiply the critical z-score by the standard error.

46
00:02:34,544 --> 00:02:39,264
In our two-tailed test for the hiking example the difference must be

47
00:02:39,320 --> 00:02:42,112
greater than or equal to 8.42,

48
00:02:42,170 --> 00:02:49,520
our obtained mean 507.75 subtracted from the population mean is 7.75

49
00:02:49,584 --> 00:02:51,904
which is not greater than 8.42,

50
00:02:51,960 --> 00:02:55,360
so we fail to reject the null hypothesis.

51
00:02:55,456 --> 00:02:57,872
Using the z-score formula where we obtain

52
00:02:57,920 --> 00:03:01,136
the difference between the population and sample means and divide by the

53
00:03:01,210 --> 00:03:06,240
standard error. we again obtain a z-score of 1.8 which is not greater than

54
00:03:06,320 --> 00:03:10,256
1.96. This example illustrates the

55
00:03:10,304 --> 00:03:13,856
importance of selecting the right test, one or two tailed

56
00:03:13,920 --> 00:03:16,560
because what was significant in one tailed test

57
00:03:16,640 --> 00:03:19,712
is not always significant in a two-tailed test.

58
00:03:20,048 --> 00:03:22,050
Further, you must make this decision

59
00:03:22,128 --> 00:03:23,840
before you conduct your research.

60
00:03:23,904 --> 00:03:26,064
Ultimately, it is defined by the null

61
00:03:26,144 --> 00:03:30,256
and alternate hypothesis a priority before the analysis

62
00:03:30,330 --> 00:03:33,760
and you can't change it after seeing your results.

