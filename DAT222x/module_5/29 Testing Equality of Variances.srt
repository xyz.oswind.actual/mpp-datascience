0
00:00:00,370 --> 00:00:03,253
When testing two independent samples

1
00:00:03,270 --> 00:00:06,160
most statistics assume equality of variance.

2
00:00:06,230 --> 00:00:09,893
Thus, it is helpful to know if your samples have equal variances.

3
00:00:09,960 --> 00:00:12,426
When investigating equality of variance,

4
00:00:12,440 --> 00:00:14,520
we test the null hypothesis that both

5
00:00:14,540 --> 00:00:16,720
samples have the same variability.

6
00:00:16,750 --> 00:00:19,960
The alternate hypothesis is that they are not equivalent

7
00:00:20,000 --> 00:00:24,333
because this is a hypothesis test you also need to establish your alpha.

8
00:00:24,370 --> 00:00:26,560
There are several statistical tests

9
00:00:26,590 --> 00:00:28,186
that you can run to evaluate this.

10
00:00:28,220 --> 00:00:31,546
But the most common is Levene's test for equality.

11
00:00:31,590 --> 00:00:34,480
The formula looks like this.

12
00:00:34,540 --> 00:00:37,290
Did your brain explode? Luckily, this is

13
00:00:37,310 --> 00:00:40,026
something that you'll never need to calculate by hand.

14
00:00:40,140 --> 00:00:44,893
Most statistical packages have this solution built right in

15
00:00:45,000 --> 00:00:46,950
and we'll run the analysis for you.

16
00:00:47,022 --> 00:00:50,260
In fact Excel does two, more on that in a moment though.

17
00:00:50,340 --> 00:00:53,853
But let's imagine that you are a glutton for punishment.

18
00:00:53,910 --> 00:00:57,053
There is a formula that you can easily perform by hand.

19
00:00:57,080 --> 00:00:59,480
You can use Hartley's F max test

20
00:00:59,500 --> 00:01:01,986
rather than Levene's. You simply divide

21
00:01:02,000 --> 00:01:04,533
the larger variants by the smaller one.

22
00:01:04,560 --> 00:01:07,860
This gives you an F ratio. If the

23
00:01:07,880 --> 00:01:09,733
variances are similar to each other

24
00:01:09,750 --> 00:01:11,786
then the F ratio will be close to 1.

25
00:01:11,800 --> 00:01:16,400
The more the variances differ the larger the F ratio will be.

26
00:01:16,460 --> 00:01:20,266
To determine if the F ratio is statistically different from

27
00:01:20,280 --> 00:01:21,813
zero, you will need to find the

28
00:01:21,840 --> 00:01:24,426
probability of obtaining that F value.

29
00:01:24,510 --> 00:01:26,250
To do this you need to know the

30
00:01:26,270 --> 00:01:28,373
degrees of freedom which is the total number of

31
00:01:28,400 --> 00:01:32,911
people in one of the sample groups minus 1 and K the number of groups.

32
00:01:33,044 --> 00:01:36,260
In the case of F max test, K is

33
00:01:36,280 --> 00:01:39,280
always 2 because you are comparing two groups.

34
00:01:39,300 --> 00:01:43,506
Note that this test assumes that there are

35
00:01:43,520 --> 00:01:45,346
equal numbers of participants in

36
00:01:45,360 --> 00:01:48,680
each sample. Finding the critical value

37
00:01:48,720 --> 00:01:50,986
needed to reject the null hypothesis is

38
00:01:51,000 --> 00:01:52,733
very similar to how you would do this

39
00:01:52,760 --> 00:01:56,453
for a t-test except that we are using a different distribution.

40
00:01:56,510 --> 00:02:00,546
We are using the F distribution that is based on the two components

41
00:02:00,560 --> 00:02:02,666
that I just mentioned. The sample size

42
00:02:02,680 --> 00:02:04,013
and the number of groups.

43
00:02:04,040 --> 00:02:08,333
The F distribution takes on various shapes based on these two values

44
00:02:08,360 --> 00:02:12,866
and the alpha that was selected but it is always skewed to the right.

45
00:02:13,010 --> 00:02:15,540
You can find F tables online or at the

46
00:02:15,560 --> 00:02:17,386
back of many statistics books.

47
00:02:17,440 --> 00:02:21,253
How do you find the critical value though using one of those F tables?

48
00:02:21,300 --> 00:02:25,133
The first step is to select the table for the desired alpha.

49
00:02:25,180 --> 00:02:29,026
This is an example of an F table for alpha 0.05.

50
00:02:29,060 --> 00:02:31,290
Now you simply go over to the column

51
00:02:31,310 --> 00:02:32,670
that has the degrees of freedom for your

52
00:02:32,690 --> 00:02:35,546
sample size which is n minus 1 and then

53
00:02:35,560 --> 00:02:39,520
down to the row with the number of groups 2 in this case.

54
00:02:39,660 --> 00:02:42,540
If the obtained value for your analysis is larger than

55
00:02:42,560 --> 00:02:46,186
what it is in that cell you reject the null hypothesis.

56
00:02:46,220 --> 00:02:49,346
OK. Let's take a quick look at how this works

57
00:02:49,360 --> 00:02:52,160
for the equality of variance hypothesis test.

58
00:02:52,180 --> 00:02:54,660
Imagine that sample 1 has a variance

59
00:02:54,680 --> 00:02:57,946
of 12.65 and sample 2 has a variance of

60
00:02:57,960 --> 00:03:02,293
17.89. There are 12 people in each sample group.

61
00:03:02,320 --> 00:03:06,320
To calculate F max, you divide the larger variance

62
00:03:06,360 --> 00:03:13,826
by the smaller one. In this case, 17.89 divided by 12.65 which is 1.41.

63
00:03:13,960 --> 00:03:17,653
The alpha value of--the F value for alpha

64
00:03:17,680 --> 00:03:20,186
at 0.05 with eleven degrees of freedom

65
00:03:20,220 --> 00:03:22,440
for the sample size and two groups is

66
00:03:22,480 --> 00:03:26,760
19.4 which is clearly more than 1.41,

67
00:03:26,790 --> 00:03:28,773
meaning that we fail to reject the null.

68
00:03:28,820 --> 00:03:30,810
The variances are equal.

69
00:03:30,830 --> 00:03:34,466
You can also evaluate the equality of variances

70
00:03:34,480 --> 00:03:35,906
by leveraging a spreadsheet that

71
00:03:35,920 --> 00:03:39,666
Wayne created that estimates the confidence interval around this ratio.

72
00:03:39,710 --> 00:03:43,200
You don't have to assume equal sample sizes in this case.

73
00:03:43,240 --> 00:03:46,520
If you use a spreadsheet the variances are considered equal

74
00:03:46,540 --> 00:03:48,740
if the confidence interval contains 1.

75
00:03:48,780 --> 00:03:50,466
They are considered unequal,

76
00:03:50,480 --> 00:03:53,093
if the confidence interval does not contains 1.

77
00:03:53,180 --> 00:03:55,026
In addition, Wayne will show you

78
00:03:55,040 --> 00:03:57,613
how to use F test formula in Excel

79
00:03:57,630 --> 00:03:59,560
to determine if variances are equal.

80
00:03:59,590 --> 00:04:02,240
See Excel can make complicated statistics.

81
00:04:02,260 --> 00:04:03,800
Oh! So easy.

