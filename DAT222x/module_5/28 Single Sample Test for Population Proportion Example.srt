0
00:00:01,220 --> 00:00:04,800
In this video we're going to show you how to do hypothesis test

1
00:00:04,900 --> 00:00:07,333
involving a population proportion

2
00:00:07,420 --> 00:00:08,711
we'll do an example as a

3
00:00:08,770 --> 00:00:11,820
right-tailed test, a left-tailed test, and a two-tailed test.

4
00:00:11,840 --> 00:00:14,844
And basically the way we'll determine whether or not to

5
00:00:14,880 --> 00:00:18,466
accept or reject a null hypothesis is simply using p-values

6
00:00:18,570 --> 00:00:23,200
remember if the p-value is <=alpha you reject the null hypothesis,

7
00:00:23,260 --> 00:00:27,444
If the p-value > alpha you accept the null hypothesis.

8
00:00:27,480 --> 00:00:29,622
Okay so we'll go through three problems  here

9
00:00:29,700 --> 00:00:32,866
and you'll see in each of the worksheets for examples here

10
00:00:32,950 --> 00:00:36,511
we've got Right Tailed Test which means the alternative will be a

11
00:00:36,640 --> 00:00:40,688
proportion is > some number, we have a left tailed test

12
00:00:40,730 --> 00:00:44,355
the alternative is that proportion is < some number or two-tailed test

13
00:00:44,390 --> 00:00:48,022
where the proportion is not equal to the proportion the null hypothesis

14
00:00:48,080 --> 00:00:49,888
and then we've got the formulas for the

15
00:00:49,930 --> 00:00:53,311
p-values right here you don't have to do a single thing,

16
00:00:53,330 --> 00:00:56,100
okay. And we'll sort of explain how those formulas

17
00:00:56,120 --> 00:00:58,355
make sense as we do our examples here.

18
00:00:58,420 --> 00:01:01,160
So basically all you'll have to do in

19
00:01:01,180 --> 00:01:05,177
hypothesis testing problems involving a population proportion

20
00:01:05,280 --> 00:01:08,133
is basically put up how many trails you had,

21
00:01:08,170 --> 00:01:11,977
how many successes and what was the hypothesized value with

22
00:01:12,040 --> 00:01:14,266
population proportion in null hypothesis

23
00:01:14,370 --> 00:01:18,533
and the p-values pop right out pick up the one that corresponds to

24
00:01:18,640 --> 00:01:22,577
whether or not you have the right tailed test or left tailed test or two tailed test.

25
00:01:22,660 --> 00:01:24,866
So let's go through these three examples.

26
00:01:24,930 --> 00:01:28,680
Player makes 300 of 400 free throws, okay before they were

27
00:01:28,700 --> 00:01:32,060
70% foul shooter and they hired, let's say a new coach.

28
00:01:32,080 --> 00:01:36,488
So are they a better foul shooter after they hired the new coach

29
00:01:36,530 --> 00:01:40,088
300 out of 400 is 75% so they did better.

30
00:01:40,130 --> 00:01:42,133
The question is did they do significantly better

31
00:01:42,220 --> 00:01:44,140
we'll use alpha being 0.05,

32
00:01:44,160 --> 00:01:48,000
Ok so the Null hypothesis is that their chance of making

33
00:01:48,020 --> 00:01:49,955
a free-throw after the change which we call

34
00:01:50,040 --> 00:01:53,666
P would be less than or equal to 0.70 they weren't any better.

35
00:01:53,750 --> 00:01:57,622
The alternative hypothesis is their chance of making a free-throw after

36
00:01:57,730 --> 00:02:00,444
hiring the new code is >0.70

37
00:02:00,460 --> 00:02:03,044
so all we do is put that information over here

38
00:02:03,060 --> 00:02:07,688
p0 is 0.70, the number of successes or a success is making a free-throw

39
00:02:07,770 --> 00:02:10,111
300, number of trails is 400

40
00:02:10,130 --> 00:02:13,977
okay and it's a right tailed test because you have > sign here

41
00:02:14,040 --> 00:02:17,955
the p-VALUE is 0.016 that's <=0.05

42
00:02:18,040 --> 00:02:21,288
we reject the null hypothesis. Now how did I get that p-value

43
00:02:21,370 --> 00:02:23,220
I just simply came up with

44
00:02:23,240 --> 00:02:25,577
you were 70%foul shooter,

45
00:02:25,620 --> 00:02:27,022
what's the chance you'd make at least

46
00:02:27,080 --> 00:02:30,711
300 out of 400 and the answer is very small 0.016

47
00:02:30,770 --> 00:02:32,622
so we reject the hypothesis

48
00:02:32,680 --> 00:02:35,622
that you're 70% foul shooter and the new coach helped you.

49
00:02:35,680 --> 00:02:39,200
Okay, now let's do another example, okay.

50
00:02:39,260 --> 00:02:40,933
We want to know if a coin is fair

51
00:02:40,990 --> 00:02:46,822
we toss that coin 400 times, and 217 times, out of 400 it came up heads that's

52
00:02:46,840 --> 00:02:50,450
54% of the time. Now the null hypothesis

53
00:02:50,470 --> 00:02:53,355
involves the chance that the coin comes up heads

54
00:02:53,420 --> 00:02:55,933
that chance would be 0.5 in the null hypothesis

55
00:02:55,990 --> 00:02:58,488
the alternative the coin not being very simple means

56
00:02:58,570 --> 00:03:03,511
that basically the probably if the coin coming up heads is not 0.5,

57
00:03:03,570 --> 00:03:07,511
okay. So that's a two-tailed test so we'd use the p-value down here

58
00:03:07,590 --> 00:03:10,222
so basically p0 is 0.5

59
00:03:10,300 --> 00:03:12,822
because we're assuming a fair coin and the null hypothesis

60
00:03:12,900 --> 00:03:15,666
the number of success is 217 out of 400

61
00:03:15,750 --> 00:03:18,800
and then this probability sort of you have to double

62
00:03:18,880 --> 00:03:21,333
the probability from the smaller tail

63
00:03:21,420 --> 00:03:25,088
okay it's I don't know exactly which here would be smaller tail here

64
00:03:25,150 --> 00:03:28,911
okay but basically you get a p-value of 0.0988

65
00:03:29,020 --> 00:03:33,311
so basically for alpha 0.05 we would accept the null hypothesis

66
00:03:33,350 --> 00:03:35,333
that's not enough evidence that the coin

67
00:03:35,440 --> 00:03:38,688
is unfair even though we didn't get exactly 50%,

68
00:03:38,750 --> 00:03:41,010
there's roughly a 10% chance of getting

69
00:03:41,030 --> 00:03:44,040
a result this extreme 217 out of 400

70
00:03:44,060 --> 00:03:47,266
or more extreme. In other words 54%

71
00:03:47,300 --> 00:03:51,800
getting a result more extreme than 54% in the sample of

72
00:03:51,900 --> 00:03:54,777
400 coin tosses heads basically

73
00:03:54,840 --> 00:03:57,866
it's just that could happen with reasonable probability

74
00:03:57,970 --> 00:04:01,444
we don't reject the null hypothesis. Final example

75
00:04:01,570 --> 00:04:04,088
okay, so let P be the fraction of

76
00:04:04,170 --> 00:04:06,288
late flights on an airline after they change

77
00:04:06,390 --> 00:04:10,155
the boarding process in other words they have tried to change the boarding process

78
00:04:10,240 --> 00:04:14,822
to make the fraction of flights late to be less and there is a lot of

79
00:04:14,930 --> 00:04:18,888
scientific studies on what's the best way to board aircraft you can

80
00:04:18,990 --> 00:04:20,570
look that up on the internet that if you want to.

81
00:04:20,590 --> 00:04:24,600
So the null hypothesis would be basically

82
00:04:24,640 --> 00:04:28,444
that the fraction of late flights is going to be bad we haven't improved it

83
00:04:28,530 --> 00:04:31,800
so that the fraction of late flights will be greater  go to 30%

84
00:04:31,860 --> 00:04:34,866
let's assume the status scroll we got 30% late fights,

85
00:04:34,970 --> 00:04:38,711
the alternative hypothesis would be after changing the boarding process

86
00:04:38,770 --> 00:04:41,111
the chance of a late flight is less than 30%

87
00:04:41,150 --> 00:04:43,200
and let's write alpha be 0.01

88
00:04:43,260 --> 00:04:46,933
and what data do we have, suppose we tried this  new boarding process

89
00:04:47,020 --> 00:04:51,244
on 300 flights and 50 were late now that 17%

90
00:04:51,350 --> 00:04:54,555
that's a lot <30% is that enough to prove

91
00:04:54,590 --> 00:04:57,111
that we have a significantly better boarding process

92
00:04:57,150 --> 00:04:59,133
well the hypothesis test will tell us.

93
00:04:59,150 --> 00:05:04,822
So we put in the p0 here should be 30%

94
00:05:07,300 --> 00:05:12,333
and basically when we get 50 successes out of 300 we put that in here

95
00:05:12,350 --> 00:05:15,555
and now you look at this is a left tail test

96
00:05:15,660 --> 00:05:20,022
and the probability or p value is like 8 and 100 million

97
00:05:20,040 --> 00:05:22,622
okay that's way less than 0.01

98
00:05:22,640 --> 00:05:25,622
so we are quite sure that basically

99
00:05:25,730 --> 00:05:27,644
the new boarding system is

100
00:05:27,680 --> 00:05:29,000
better than the old  boarding system.

101
00:05:29,100 --> 00:05:33,733
Now if I put something like an 89 in there which is 29% or so

102
00:05:33,840 --> 00:05:37,333
okay see the p-value would be near 0.5

103
00:05:37,390 --> 00:05:40,511
we really wouldn't have any proof that the boarding system

104
00:05:40,570 --> 00:05:44,088
what we changed to was better with that 89 out of 300.

105
00:05:44,130 --> 00:05:47,622
Ok, even though 89 out of 300 is < 30%

106
00:05:47,750 --> 00:05:51,244
it's very close to 30% we just wouldn't have much proof.

107
00:05:51,300 --> 00:05:54,200
So I think in summary what you all you have to do here  is

108
00:05:54,350 --> 00:05:56,340
put in the null hypothesis but in

109
00:05:56,360 --> 00:05:58,688
number of successes the number of trials

110
00:05:58,900 --> 00:06:01,422
figure out it's a left tailed test right tailed test

111
00:06:01,550 --> 00:06:03,911
or a two-tailed test compare your p value

112
00:06:03,970 --> 00:06:06,888
from the spreadsheet you don't have to type these formulas in again

113
00:06:06,970 --> 00:06:09,400
to the hypothesized value of alpha

114
00:06:09,500 --> 00:06:13,533
and if the p-value is less than or equal to alpha reject the null hypothesis

115
00:06:13,640 --> 00:06:17,777
otherwise accept the null hypothesis.

116
00:06:17,860 --> 00:06:19,733
Now in our next video we'll talk about

117
00:06:19,790 --> 00:06:23,288
testing hypothesis concerning equality of variances.

