0
00:00:00,940 --> 00:00:04,180
Just like you do with all hypothesis testing, when you are preparing for

1
00:00:04,180 --> 00:00:07,040
a two-sample z-test, you start by defining your null and

2
00:00:07,040 --> 00:00:08,880
alternative hypothesis.

3
00:00:08,880 --> 00:00:12,110
Your null hypothesis might be to test for equality of means, or

4
00:00:12,110 --> 00:00:15,210
for one mean being greater or less than the other.

5
00:00:15,210 --> 00:00:18,960
What is unique about two-sample tests is that you can also test for

6
00:00:18,960 --> 00:00:21,510
an expected difference between the means.

7
00:00:21,510 --> 00:00:22,240
In most cases,

8
00:00:22,240 --> 00:00:25,360
you'll probably testing that the expected difference is 0,

9
00:00:25,360 --> 00:00:29,180
as you see here, but this could actually be any value of interest.

10
00:00:29,180 --> 00:00:32,620
For example, if previous research has indicated that the average time

11
00:00:32,620 --> 00:00:35,660
spent on a task differs by ten minutes for men versus women.

12
00:00:35,660 --> 00:00:38,320
You can specifically test if that size of difference was seen in

13
00:00:38,320 --> 00:00:39,320
your study.

14
00:00:39,320 --> 00:00:43,070
You can decide on your level of significance, as well.

15
00:00:43,070 --> 00:00:46,830
In two-sample t-tests, you are essentially using this formula.

16
00:00:46,830 --> 00:00:48,950
More formally, you see it written like this.

17
00:00:48,950 --> 00:00:50,160
Where the expected difference,

18
00:00:50,160 --> 00:00:53,250
represented by the second part of the numerator, is subtracted

19
00:00:53,250 --> 00:00:56,720
from the observed differences in the first part of the numerator.

20
00:00:56,720 --> 00:01:00,105
And then divided by what's called the pooled standard deviation.

21
00:01:00,105 --> 00:01:02,873
That is the square root of the variants of each population,

22
00:01:02,873 --> 00:01:06,335
weighted by it's associated sample size, and then summed.

23
00:01:06,335 --> 00:01:09,059
You will note that we are using sigma to denote the variants.

24
00:01:09,059 --> 00:01:11,761
Which implies that the population variances are known and

25
00:01:11,761 --> 00:01:13,715
used in this calculation.

26
00:01:13,715 --> 00:01:17,025
Now, imagine that we're interested in comparing the number of cupcakes

27
00:01:17,025 --> 00:01:20,110
purchased by men and women during the month of February.

28
00:01:20,110 --> 00:01:25,660
We obtained a mean of 65.25 for men, and a mean of 62.09 for

29
00:01:25,660 --> 00:01:29,407
women from a sample of 75 men and 50 women.

30
00:01:29,407 --> 00:01:34,068
In years past, we have seen that the variance for men is 20.15 and

31
00:01:34,068 --> 00:01:36,685
the variance for women is 26.75.

32
00:01:36,685 --> 00:01:38,492
Using an alpha of 0.05,

33
00:01:38,492 --> 00:01:42,710
do men purchase more cupcakes in February than women do?

34
00:01:42,710 --> 00:01:46,493
From this equation, we obtain a z-score of 3.52,

35
00:01:46,493 --> 00:01:50,126
which exceeds the required z-score of 1.645.

36
00:01:50,126 --> 00:01:52,829
To be significant at the alpha of 0.05,

37
00:01:52,829 --> 00:01:55,960
men purchase more cupcakes than women.

38
00:01:55,960 --> 00:01:59,050
Let's take a look at how to run a two-sample z-test in Excel.

