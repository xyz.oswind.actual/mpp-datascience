0
00:00:00,640 --> 00:00:01,140
In the world,

1
00:00:01,140 --> 00:00:04,740
we often need to make decisions based on population parameters.

2
00:00:04,740 --> 00:00:08,670
Hypothesis testing, which is the subject of Module 5,

3
00:00:08,670 --> 00:00:10,020
helps us make these decisions.

4
00:00:10,020 --> 00:00:13,810
So a few examples, we wanna know does a drug reduce blood pressure.

5
00:00:13,810 --> 00:00:17,440
Because if it does, then we would probably put it on the market.

6
00:00:17,440 --> 00:00:20,150
So we'd be interested in the mean reduction in blood pressure.

7
00:00:20,150 --> 00:00:24,155
Is it big enough to justify putting that drug on the market?

8
00:00:24,155 --> 00:00:27,968
Does a reduced class size increase test scores in elementary school?

9
00:00:27,968 --> 00:00:31,030
I mean, we would hope if we had less students in the class,

10
00:00:31,030 --> 00:00:32,580
the students would do better.

11
00:00:32,580 --> 00:00:35,430
So we would probably look at the change in test scores

12
00:00:35,430 --> 00:00:39,590
in schools that have less students in a classroom than more students.

13
00:00:39,590 --> 00:00:41,580
Is a person innocent or guilty of a crime?

14
00:00:41,580 --> 00:00:44,920
We make decisions on this in the court system every day.

15
00:00:44,920 --> 00:00:47,920
And so we need a preponderance of evidence

16
00:00:47,920 --> 00:00:50,250
to tell us if somebody is guilty.

17
00:00:50,250 --> 00:00:53,460
Does more money spent on education in low-income area

18
00:00:53,460 --> 00:00:55,640
improve student performance?

19
00:00:55,640 --> 00:00:58,960
In other words, if we would spend $15,000 per year on

20
00:00:58,960 --> 00:01:01,630
kids in a low-income area instead of 10,000,

21
00:01:01,630 --> 00:01:04,980
would the test scores be significantly higher?

22
00:01:04,980 --> 00:01:06,570
Those are the types of questions we want to

23
00:01:06,570 --> 00:01:08,810
analyze with hypothesis testing.

24
00:01:08,810 --> 00:01:12,580
So what we do is we set up a null and alternative hypothesis.

25
00:01:12,580 --> 00:01:15,610
So the null hypothesis is going to be the status quo.

26
00:01:15,610 --> 00:01:17,380
For example, in the court system,

27
00:01:17,380 --> 00:01:20,620
the defendant is innocent in the US until proven guilty.

28
00:01:20,620 --> 00:01:23,350
Then we have an alternative hypothesis that competes with

29
00:01:23,350 --> 00:01:24,650
the null hypothesis.

30
00:01:24,650 --> 00:01:25,950
And in our court system,

31
00:01:25,950 --> 00:01:29,010
the alternative hypothesis is the defendant is guilty.

32
00:01:29,010 --> 00:01:30,770
And we need lots of proof, as you know,

33
00:01:30,770 --> 00:01:34,110
in our US court system, to reject the null hypothesis.

34
00:01:34,110 --> 00:01:38,940
In other words we need a 12-0 jury vote to reject that null hypothesis.

35
00:01:38,940 --> 00:01:40,158
So in the next video,

36
00:01:40,158 --> 00:01:43,344
we're gonna start talking about types of hypotheses,

37
00:01:43,344 --> 00:01:47,561
upper one-tailed, lower one-tailed, and two-tailed hypotheses.

