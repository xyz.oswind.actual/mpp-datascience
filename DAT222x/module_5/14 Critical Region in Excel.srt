0
00:00:02,030 --> 00:00:04,380
In this video we'll discuss the crucial concept of

1
00:00:04,380 --> 00:00:05,990
the critical region.

2
00:00:05,990 --> 00:00:08,610
Basically the critical region is the range of values for

3
00:00:08,610 --> 00:00:12,800
a sample statistic that results in rejection of the null hypothesis.

4
00:00:12,800 --> 00:00:16,060
So basically the approach to hypothesis testing we usually use is

5
00:00:16,060 --> 00:00:19,660
we set a small probability alpha, usually 0.05 of

6
00:00:19,660 --> 00:00:23,300
making a type I error, and then choose a critical region that will

7
00:00:23,300 --> 00:00:26,600
minimize the probability of making a type II error.

8
00:00:26,600 --> 00:00:30,100
Okay, so let's look at two examples of

9
00:00:30,100 --> 00:00:32,430
what the critical region might look like.

10
00:00:32,430 --> 00:00:35,470
So remember example 1 about the cell phone defective chips?

11
00:00:35,470 --> 00:00:39,740
The null hypothesis was there's less or equal to 1% defective,

12
00:00:39,740 --> 00:00:42,440
the alternative is they're more than 1% defective.

13
00:00:42,440 --> 00:00:44,800
So when would you want to reject a null hypothesis?

14
00:00:44,800 --> 00:00:46,100
What would be sensible?

15
00:00:46,100 --> 00:00:48,810
If phat, the fraction of defectives is greater than or

16
00:00:48,810 --> 00:00:50,430
equal to some number.

17
00:00:50,430 --> 00:00:55,420
And that would give you a type I error of 0.05 if you pick

18
00:00:55,420 --> 00:00:58,080
the cut-off point to be a certain number.

19
00:00:58,080 --> 00:01:01,600
And then basically, that would give you the smallest chance of a type II

20
00:01:01,600 --> 00:01:07,070
error, because you're rejecting in a sensible way, only if the sample

21
00:01:07,070 --> 00:01:11,120
fraction of defectives is greater or equal to that critical value.

22
00:01:11,120 --> 00:01:13,850
You wouldn't wanna reject the null hypothesis if you had very few

23
00:01:13,850 --> 00:01:15,010
defectives.

24
00:01:15,010 --> 00:01:18,187
Now, let's look at example 2, where we're trying to see if

25
00:01:18,187 --> 00:01:21,927
a congressional district has a lower income than the country as a whole.

26
00:01:21,927 --> 00:01:24,630
Well, when would you want to reject the hypothesis?

27
00:01:24,630 --> 00:01:27,279
You take a sample and if the sample mean was too low,

28
00:01:27,279 --> 00:01:29,680
you would reject the null hypothesis.

29
00:01:29,680 --> 00:01:31,550
Now the question is how low?

30
00:01:31,550 --> 00:01:33,990
It depends on the level of proof you want, and

31
00:01:33,990 --> 00:01:37,370
if you want a 5% level of proof, which is the usual,

32
00:01:37,370 --> 00:01:41,080
you have a formula you can plug in that gives you the cutoff point

33
00:01:41,080 --> 00:01:45,120
on the sample mean that results in rejection of the null hypothesis.

34
00:01:45,120 --> 00:01:48,310
Now in our next video, we'll actually start doing hypothesis

35
00:01:48,310 --> 00:01:52,070
testing and we'll start with probably the most basic

36
00:01:52,070 --> 00:01:54,500
hypothesis test, the one sample z test.

