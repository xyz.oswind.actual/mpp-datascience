0
00:00:00,470 --> 00:00:03,290
Contingency tables display the cross-classification

1
00:00:03,290 --> 00:00:06,470
of observations by the levels of two categorical variables.

2
00:00:06,470 --> 00:00:09,574
The columns represent levels of one categorical variable and

3
00:00:09,574 --> 00:00:11,760
rows represent levels of the other.

4
00:00:11,760 --> 00:00:14,980
The values in the cells represent the observed frequency or

5
00:00:14,980 --> 00:00:17,980
number of observations at a given level of each variable.

6
00:00:17,980 --> 00:00:20,270
This is an example of what one looks like.

7
00:00:20,270 --> 00:00:22,970
Remember our video game example from module 1?

8
00:00:22,970 --> 00:00:25,670
That is also an example of a contingency table showing

9
00:00:25,670 --> 00:00:28,150
the relationship between gender and gaming.

10
00:00:28,150 --> 00:00:31,400
These tables are used in survey research, business intelligence,

11
00:00:31,400 --> 00:00:33,470
engineering and scientific research.

12
00:00:33,470 --> 00:00:36,600
They provide a basic picture of the relationship between two variables

13
00:00:36,600 --> 00:00:39,400
and can help find interactions between them.

14
00:00:39,400 --> 00:00:42,090
They're extremely helpful in understanding if two variables

15
00:00:42,090 --> 00:00:43,280
are independent.

16
00:00:43,280 --> 00:00:45,450
Here's one more example of a contingency table

17
00:00:45,450 --> 00:00:47,960
showing the relationship between illness and diet.

18
00:00:47,960 --> 00:00:50,020
These are not real data, I have presented this for

19
00:00:50,020 --> 00:00:51,900
illustrative purposes only.

20
00:00:51,900 --> 00:00:55,470
The point being that you can have different levels

21
00:00:55,470 --> 00:00:58,890
of different variables, and that you need to ask the question,

22
00:00:58,890 --> 00:01:01,540
is there a relationship between diet and disease?

23
00:01:01,540 --> 00:01:03,690
With the help of contingency tables and

24
00:01:03,690 --> 00:01:06,205
the Chi square statistic, we can do just that.

25
00:01:06,205 --> 00:01:07,984
[BLANK]

