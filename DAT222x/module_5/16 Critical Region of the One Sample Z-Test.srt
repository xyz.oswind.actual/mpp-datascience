0
00:00:00,540 --> 00:00:03,740
In this video, we&#39;ll begin our discussion of the one sample Z-test.

1
00:00:03,740 --> 00:00:06,690
Here we&#39;re testing a hypothesis about the population mean mu,

2
00:00:06,690 --> 00:00:10,090
and we&#39;ll assume the variance of the population is known.

3
00:00:10,090 --> 00:00:12,030
And if you don&#39;t know the variance of the population,

4
00:00:12,030 --> 00:00:15,840
you can plug in the sample variance for the population variance.

5
00:00:15,840 --> 00:00:19,790
Now we use this test when the sample size n is greater and equal to 30.

6
00:00:19,790 --> 00:00:21,000
Because by the central limit,

7
00:00:21,000 --> 00:00:23,660
there are men we know the sample mean will be normally

8
00:00:23,660 --> 00:00:27,530
distributed even if the population is not normally distributed.

9
00:00:27,530 --> 00:00:30,420
Okay, so what&#39;s the critical region for this test for

10
00:00:30,420 --> 00:00:32,600
all the types of hypothesis that we can have?

11
00:00:32,600 --> 00:00:37,326
We can have, again the one tailed up or the one tailed lower,

12
00:00:37,326 --> 00:00:39,570
the two-tailed test.

13
00:00:39,570 --> 00:00:42,530
Now here we need to talk about the standard normal a bit.

14
00:00:42,530 --> 00:00:44,560
We talked about this in module four.

15
00:00:44,560 --> 00:00:47,110
We&#39;ll use the variable z with the subscript to

16
00:00:47,110 --> 00:00:50,250
represent the given percentile of this standard normal.

17
00:00:50,250 --> 00:00:55,586
So z is a .025 is the two and a half percentile the standard normal,

18
00:00:55,586 --> 00:01:01,210
which we saw last module was minus 1.96 z .05 is minus 1.645.

19
00:01:01,210 --> 00:01:03,773
So, like for standard normal there&#39;s a two and

20
00:01:03,773 --> 00:01:07,070
a half percent chance you&#39;ll get -1.96 or less.

21
00:01:07,070 --> 00:01:10,980
So here are your critical regions in general, given alpha.

22
00:01:10,980 --> 00:01:14,490
Okay, so the null hypothesis here this is an upper one tail

23
00:01:14,490 --> 00:01:19,690
the alternative, mu is less mu zero or equal mu zero is the null.

24
00:01:19,690 --> 00:01:21,642
The alternative is mu is greater than mu zero.

25
00:01:21,642 --> 00:01:25,810
Well then you should reject the null if the sample mean it&#39;s too big.

26
00:01:25,810 --> 00:01:26,840
How big?

27
00:01:26,840 --> 00:01:32,070
You take the hypothesized version of mu and the null hypothesis.

28
00:01:32,070 --> 00:01:36,900
Then you add z sub alpha, in other words if you have the absent value

29
00:01:36,900 --> 00:01:43,450
of that, if alpha&#39;s .05 then you would take 1.645 okay, in that case.

30
00:01:43,450 --> 00:01:46,300
And you multiply by the standard deviation divided by the square root

31
00:01:46,300 --> 00:01:47,790
of the sample size.

32
00:01:47,790 --> 00:01:53,430
Now in a one tail lower alternative, it basically, you have mu greater

33
00:01:53,430 --> 00:01:59,290
than equal zero is the null, mu less than the zero is the alternative.

34
00:01:59,290 --> 00:02:02,960
You want to reject the null of the sample mean it&#39;s too low.

35
00:02:02,960 --> 00:02:05,330
So basically here the z&#39;s of alpha is negative, so

36
00:02:05,330 --> 00:02:07,370
this would be x bar less than or

37
00:02:07,370 --> 00:02:11,060
equal to the hypothesised value in each zero of the mean.

38
00:02:11,060 --> 00:02:14,288
In a two tail test you&#39;ll look at the absolute value,

39
00:02:14,288 --> 00:02:17,232
that&#39;s with the vertical lines mean a basically

40
00:02:17,232 --> 00:02:21,352
the value of x bar you observe minus the hypothesised value mu zero.

41
00:02:21,352 --> 00:02:23,139
And you say, is that greater than or

42
00:02:23,139 --> 00:02:26,357
equal to the z&#39;s of alpha over two times the population standard

43
00:02:26,357 --> 00:02:29,890
deviation divided by the square root of the sample size.

44
00:02:29,890 --> 00:02:32,070
Now the most common case where alpha&#39;s 0.05,

45
00:02:32,070 --> 00:02:35,690
I&#39;ve written down exactly what the critical region is.

46
00:02:35,690 --> 00:02:40,830
So if alpha&#39;s 0.05, that the z&#39;s of 0.05 is minus 1.645 and

47
00:02:40,830 --> 00:02:45,156
so then basically this becomes basically the cut off points for

48
00:02:45,156 --> 00:02:47,690
rejecting a null hypothesis.

49
00:02:47,690 --> 00:02:51,810
Notice in the two tail test, where we 0.05 divided by two.

50
00:02:51,810 --> 00:02:56,710
We get the absolute value of basically the z standard normal for

51
00:02:56,710 --> 00:03:01,252
basically 0.025, which becomes a 1.96 there.

