0
00:00:00,570 --> 00:00:03,199
Thinking back to our last example where

1
00:00:03,210 --> 00:00:05,546
the result was significant for a one-tailed test

2
00:00:05,560 --> 00:00:06,920
but not a two-tailed test.

3
00:00:06,940 --> 00:00:08,540
You can imagine how frustrating

4
00:00:08,560 --> 00:00:11,373
this must be for researchers. If only

5
00:00:11,390 --> 00:00:13,853
they had designed their hypothesis a bit differently.

6
00:00:13,870 --> 00:00:16,360
The level of significance chosen does

7
00:00:16,390 --> 00:00:18,293
seem rather arbitrary. Doesn't it?

8
00:00:18,310 --> 00:00:22,906
For that reason most statisticians use the concept of probability values

9
00:00:22,920 --> 00:00:26,653
or p-values to report the outcome of a hypothesis test.

10
00:00:26,680 --> 00:00:29,373
The p-value is simply the probability of

11
00:00:29,390 --> 00:00:31,426
observing the value of the test statistic

12
00:00:31,440 --> 00:00:33,240
that you obtain from your research.

13
00:00:33,260 --> 00:00:35,013
By doing this, it allows other

14
00:00:35,040 --> 00:00:38,693
researchers to make a judgment about how significant the result is.

15
00:00:38,750 --> 00:00:43,493
In our example, we obtained a z value of 1.8.

16
00:00:43,520 --> 00:00:47,693
What is the probability of obtaining that value from a z-test?

17
00:00:47,860 --> 00:00:52,200
Using the normal dias digest function in Excel,

18
00:00:52,333 --> 00:00:56,040
we calculate this probability to be 0.079.

19
00:00:56,120 --> 00:01:00,200
Other researchers can then decide if it's significant enough.

20
00:01:00,230 --> 00:01:02,986
Wayne will demonstrate how to use formulas in Excel

21
00:01:03,066 --> 00:01:05,460
to obtain the probability of the test statistic

22
00:01:05,480 --> 00:01:08,226
obtained from your research in the next video.

