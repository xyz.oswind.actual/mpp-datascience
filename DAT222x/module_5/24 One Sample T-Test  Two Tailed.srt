0
00:00:00,549 --> 00:00:02,889
Now let's look at this assuming a

1
00:00:02,960 --> 00:00:05,040
two-tailed test. In other words, we're not

2
00:00:05,104 --> 00:00:06,970
sure if the average number of miles will

3
00:00:07,024 --> 00:00:09,600
be larger or smaller than 500.

4
00:00:09,728 --> 00:00:12,670
Our alpha is still .05 but because

5
00:00:12,752 --> 00:00:14,860
this is a two-tailed test we divide by

6
00:00:14,944 --> 00:00:16,992
two and obtain the critical value

7
00:00:17,072 --> 00:00:19,392
for alpha equal to .025,

8
00:00:19,456 --> 00:00:24,016
with the t value of fifteen degrees of freedom for this level of significance

9
00:00:24,096 --> 00:00:29,136
is 2.13. How large must the difference be between the sample

10
00:00:29,184 --> 00:00:30,730
and population means for it to be

11
00:00:30,800 --> 00:00:33,152
significant. To determine this

12
00:00:33,200 --> 00:00:35,070
we multiply the critical t value by

13
00:00:35,150 --> 00:00:38,176
the standard error. In this example, the mean difference

14
00:00:38,224 --> 00:00:42,288
must be greater than or equal to 28.03

15
00:00:42,380 --> 00:00:45,920
our obtain mean 525.01

16
00:00:46,013 --> 00:00:48,030
subtracted from the population mean is

17
00:00:48,112 --> 00:00:53,424
25.01 which is not greater than 28.03.

18
00:00:53,472 --> 00:00:56,520
So we fail to reject the null hypothesis.

19
00:00:56,608 --> 00:01:00,240
Alternately, you can calculate the t value. To do this

20
00:01:00,336 --> 00:01:04,590
you subtract the population mean 500 from the sample mean

21
00:01:04,656 --> 00:01:08,976
525.01 and divided by the standard error.

22
00:01:09,056 --> 00:01:12,800
Doing this we obtain a t-score of 1.90

23
00:01:12,864 --> 00:01:16,704
which is not greater than the critical t value of 2.13

24
00:01:16,752 --> 00:01:18,768
and we fail to reject the null.

25
00:01:18,832 --> 00:01:23,456
By the way, you can also record, report probability values for the

26
00:01:23,536 --> 00:01:27,792
t-scores obtained from your analysis just like you can with z-scores.

27
00:01:27,840 --> 00:01:30,450
In fact you can do this with any statistic.

28
00:01:30,512 --> 00:01:33,270
In this example, we obtained a t value of

29
00:01:33,344 --> 00:01:38,128
1.90. What is the probability of obtaining that value from

30
00:01:38,176 --> 00:01:40,352
a t-test given our degrees of freedom

31
00:01:40,416 --> 00:01:44,480
using the T.DIST function in excel

32
00:01:44,560 --> 00:01:50,880
we calculate this probability to be .0384 for a one tailed using the

33
00:01:50,976 --> 00:01:55,248
T.DIST.2T function for the two-tailed probability,

34
00:01:55,296 --> 00:01:58,140
we obtain a probability of .0768.

35
00:01:58,192 --> 00:02:01,424
Wayne will demonstrate how to do this in Excel.

