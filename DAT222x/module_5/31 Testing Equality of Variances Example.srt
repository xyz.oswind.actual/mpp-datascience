0
00:00:00,580 --> 00:00:04,610
So I taught a statistics class with two versions, a hybrid version which

1
00:00:04,610 --> 00:00:08,490
was online test in person and a totally in-person version.

2
00:00:08,490 --> 00:00:12,340
So the question would be, is the variance of the test scores in

3
00:00:12,340 --> 00:00:14,790
the hybrid class and in-person class different?

4
00:00:14,790 --> 00:00:16,040
So we have the sample,

5
00:00:16,040 --> 00:00:20,050
we have 14 people's test scores on the same test in the hybrid class,

6
00:00:20,050 --> 00:00:23,150
and we have 18 people's test scores in the in person class.

7
00:00:23,150 --> 00:00:26,730
And so our null hypothesis is that the variance of the hybrid scores,

8
00:00:26,730 --> 00:00:29,250
would equal the variance of the in-class scores.

9
00:00:29,250 --> 00:00:31,390
The alternative is that they're significantly different.

10
00:00:32,470 --> 00:00:35,720
Well, what we can use is the F.TEST function which we'll show you how to

11
00:00:35,720 --> 00:00:38,120
use to get a PVALUE for these hypotheses.

12
00:00:38,120 --> 00:00:39,290
And that's about it.

13
00:00:39,290 --> 00:00:41,990
But we should check that we have normal populations.

14
00:00:41,990 --> 00:00:44,519
So we can put in a skewness function here,

15
00:00:44,519 --> 00:00:46,845
which we learned about in module one.

16
00:00:49,651 --> 00:00:51,865
The skewness is between plus and minus one, that's good.

17
00:00:51,865 --> 00:00:58,360
The skewness for the in person, is also between plus and minus one.

18
00:00:58,360 --> 00:00:59,870
In kurtosis, between plus and

19
00:00:59,870 --> 00:01:02,086
minus one is also consistent within normal.

20
00:01:04,943 --> 00:01:07,440
Ctrl+Shift+arrow down is moving me down.

21
00:01:07,440 --> 00:01:08,207
That's very close to 0.

22
00:01:11,341 --> 00:01:16,050
And that kurtosis should be between +1 and -1, and it is.

23
00:01:16,050 --> 00:01:19,980
Let's just get those sample variances to see if,

24
00:01:19,980 --> 00:01:22,030
by an eyeball test, they look to be similar.

25
00:01:23,440 --> 00:01:26,300
So I could do what VAR to get the sample variance.

26
00:01:28,323 --> 00:01:31,110
Excel will use these in its hypothesis test.

27
00:01:31,110 --> 00:01:37,321
So the variance and the hybrid was 34 and the variance in person.

28
00:01:40,616 --> 00:01:41,457
Was 18.

29
00:01:41,457 --> 00:01:43,472
The question is, is that a significant difference.

30
00:01:43,472 --> 00:01:46,847
And you just can't tell by eyeballing at that's what

31
00:01:46,847 --> 00:01:48,990
a hypothesis test is for.

32
00:01:48,990 --> 00:01:52,630
That's like a standard deviation of almost six versus four, and

33
00:01:52,630 --> 00:01:55,490
are those significantly different with these sample sizes?

34
00:01:55,490 --> 00:01:58,080
Well only the PVALUE knows for sure.

35
00:01:58,080 --> 00:02:02,450
So what we can do is do F.TEST.

36
00:02:02,450 --> 00:02:05,720
We can go F and then we see .TEST, there's a lot of F things there.

37
00:02:07,040 --> 00:02:08,540
Just highlight your first array.

38
00:02:11,348 --> 00:02:13,202
And then you can highlight your second array and

39
00:02:13,202 --> 00:02:14,830
you'll get a PVALUE.

40
00:02:14,830 --> 00:02:18,335
That's all you have to do, you don't have to know some complicated

41
00:02:18,335 --> 00:02:21,913
formula, take my word for it, the formula here is really horrible.

42
00:02:21,913 --> 00:02:25,932
So just to show you what we did there with FORMULATEXT, so

43
00:02:25,932 --> 00:02:28,125
we've got a P VALUE of 22%.

44
00:02:28,125 --> 00:02:32,271
So what that means is, if the two populations had equal variance,

45
00:02:32,271 --> 00:02:35,760
basically there's a 22% chance you'd see.

46
00:02:35,760 --> 00:02:40,030
This big a discrepancy here or bigger between the variances.

47
00:02:40,030 --> 00:02:42,170
Now, that's a pretty big probability.

48
00:02:42,170 --> 00:02:43,890
It's not less than 0.05.

49
00:02:43,890 --> 00:02:44,950
So basically here,

50
00:02:44,950 --> 00:02:48,660
we'd accept the null hypothesis, that the variances are equal.

51
00:02:48,660 --> 00:02:51,170
And we'll see later that that's really important.

52
00:02:51,170 --> 00:02:51,880
So we accept.

53
00:02:53,977 --> 00:02:55,348
The font size is getting small there.

54
00:03:01,454 --> 00:03:07,372
Null hypothesis, that the population variances are equal.

55
00:03:10,800 --> 00:03:13,530
Now in the next lesson, we're gonna talk

56
00:03:13,530 --> 00:03:17,830
about probably the most important hypothesis test there is.

57
00:03:17,830 --> 00:03:19,660
Testing two populations to see

58
00:03:19,660 --> 00:03:22,060
if their means are significantly different.

59
00:03:22,060 --> 00:03:25,940
And you will see that this will be important sometimes,

60
00:03:25,940 --> 00:03:29,730
to test first if the variances are the same before we test for

61
00:03:29,730 --> 00:03:32,030
differences in means, that's why we did this test

