0
00:00:00,410 --> 00:00:02,840
In this video, we'll discuss an important random variable,

1
00:00:02,840 --> 00:00:04,290
the T random variable.

2
00:00:04,290 --> 00:00:07,860
When do you use the T random variable or the T distribution?

3
00:00:07,860 --> 00:00:11,640
Well, basically, if your population is normal and has a mean mu, and

4
00:00:11,640 --> 00:00:15,093
the standard deviation is unknown, and usually you have a sample size

5
00:00:15,093 --> 00:00:19,980
that's small, less than 30, then what has a T distribution?

6
00:00:19,980 --> 00:00:24,390
Take the sample mean minus the population mean mu, divide

7
00:00:24,390 --> 00:00:27,870
by the sample standard deviation divided by the square root of n.

8
00:00:27,870 --> 00:00:29,870
That will have a T distribution.

9
00:00:29,870 --> 00:00:33,330
Now, if you knew the standard deviation and you would plug in this

10
00:00:33,330 --> 00:00:37,460
population standard deviation here, this would have a standard normal.

11
00:00:37,460 --> 00:00:39,340
Okay, we've learned about the standard normal in

12
00:00:39,340 --> 00:00:40,820
the previous module.

13
00:00:40,820 --> 00:00:43,760
Okay, now the problem with the T random variable is it has

14
00:00:43,760 --> 00:00:45,720
what's called degrees of freedom.

15
00:00:45,720 --> 00:00:49,370
If you have n observations you have n minus one degrees of freedom.

16
00:00:49,370 --> 00:00:51,460
So you sort of would need a different table for

17
00:00:51,460 --> 00:00:55,320
every sample size, but luckily, Excel has functions built in

18
00:00:55,320 --> 00:00:58,730
to basically help you calculate percentiles and

19
00:00:58,730 --> 00:01:01,260
probabilities for the T random variable.

20
00:01:01,260 --> 00:01:04,470
So, what does the T random variable look like compared to the normal?

21
00:01:04,470 --> 00:01:06,550
Well, we have a little chart here.

22
00:01:06,550 --> 00:01:07,900
So, the blue is the normal, and

23
00:01:07,900 --> 00:01:09,850
you can see it's the highest one in the middle.

24
00:01:09,850 --> 00:01:12,670
So, it's concentrated less in the tails.

25
00:01:12,670 --> 00:01:14,330
The smaller the degrees of freedom,

26
00:01:14,330 --> 00:01:17,840
the more the T random variables is concentrated in the extremes.

27
00:01:17,840 --> 00:01:21,650
And as the number of degrees of freedom or observations increases,

28
00:01:21,650 --> 00:01:25,270
then the T random variable sort of looks like the normal density, and

29
00:01:25,270 --> 00:01:26,890
really, when the sample size is greater or

30
00:01:26,890 --> 00:01:29,410
equal to 30, we probably don't worry about the T.

31
00:01:29,410 --> 00:01:33,510
We use the normal like we did in the one sample Z test.

32
00:01:33,510 --> 00:01:37,890
Now, in our next video, we'll structure how you do a one sample T

33
00:01:37,890 --> 00:01:41,840
test, give you the cut-offs for a critical region and the P values and

34
00:01:41,840 --> 00:01:44,620
then do an example of the one sample T test.

