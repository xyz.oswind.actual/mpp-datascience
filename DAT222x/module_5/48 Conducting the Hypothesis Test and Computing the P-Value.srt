0
00:00:01,020 --> 00:00:02,530
In this video, we'll show you

1
00:00:02,550 --> 00:00:07,070
how to determine whether Eye Color and Gender are independent.

2
00:00:07,090 --> 00:00:09,719
And as you may guess they'll turn out not to be independent.

3
00:00:09,730 --> 00:00:12,120
In other words Gender does have an

4
00:00:12,140 --> 00:00:14,340
influence on Eye Color. Okay so you'll

5
00:00:14,360 --> 00:00:16,379
recall in the last video we came up with

6
00:00:16,390 --> 00:00:18,780
a Chi Square value of 16.59

7
00:00:18,800 --> 00:00:21,210
and the question is sort of is that

8
00:00:21,230 --> 00:00:23,340
consistent with the null hypothesis of

9
00:00:23,360 --> 00:00:24,850
independents are not consistent.

10
00:00:24,870 --> 00:00:26,790
So basically there are two functions

11
00:00:26,810 --> 00:00:28,830
in Excel that can help us here, the first

12
00:00:28,850 --> 00:00:32,399
one is CHISQ.DIST.RT

13
00:00:32,410 --> 00:00:34,350
basically if you just point to and we'll

14
00:00:34,370 --> 00:00:36,300
type this in here your Chi Squared

15
00:00:36,320 --> 00:00:37,890
statistic and put the degrees of Freedom

16
00:00:37,910 --> 00:00:40,230
in it'll give you the p-value for the

17
00:00:40,250 --> 00:00:42,210
test and the p-value is lesser good

18
00:00:42,230 --> 00:00:44,040
alpha you reject the null hypothesis

19
00:00:44,060 --> 00:00:45,610
now how many degrees of freedom

20
00:00:45,630 --> 00:00:48,780
recall from the last video you take the number

21
00:00:48,800 --> 00:00:51,059
of rows minus one times columns minus one.

22
00:00:51,070 --> 00:00:54,850
So rows is 2 that's 2 - 1 there are 4 Eye Colors here

23
00:00:54,870 --> 00:00:56,750
Blue, Brown, Green and Hazel

24
00:00:56,770 --> 00:01:02,090
So that's 4-1, so 4-1 times 2-1 is 3 Degrees of Freedom.

25
00:01:02,110 --> 00:01:03,750
So what we could do is we could type in

26
00:01:03,770 --> 00:01:07,369
Chi Squared distribution right tailed

27
00:01:07,380 --> 00:01:09,750
okay we can point to that test statistic

28
00:01:09,770 --> 00:01:13,049
and put it in a 3, that will give us the

29
00:01:13,060 --> 00:01:17,140
p-value there which is a bad I guess 8, 9 and 10,000.

30
00:01:17,160 --> 00:01:20,340
So in other words if Eye Color and Gender independent

31
00:01:20,360 --> 00:01:22,200
there's only 9 chances in 10,000 to get a

32
00:01:22,220 --> 00:01:24,420
Chi Square total this big. So you reject

33
00:01:24,440 --> 00:01:29,340
the null hypothesis here P value is 0.05 from even point 0.1

34
00:01:29,360 --> 00:01:33,420
Now another way to do this okay if you

35
00:01:33,440 --> 00:01:38,610
look at L7 through O8 here, okay we've

36
00:01:38,630 --> 00:01:40,799
got the observed and then we have the

37
00:01:40,810 --> 00:01:51,320
expected value down here. We did those in the last video.

38
00:01:51,340 --> 00:01:53,180
What you can do is put in the observed

39
00:01:53,200 --> 00:01:56,960
values that you had and you can put in

40
00:01:56,980 --> 00:02:00,350
the expected values which are right here

41
00:02:00,370 --> 00:02:05,620
and then basically you don't even compute that 16.59

42
00:02:05,640 --> 00:02:07,880
Excel does that for you it doesn't show

43
00:02:07,900 --> 00:02:09,979
it to you but it'll get you the p-value

44
00:02:09,990 --> 00:02:11,300
there and you don't have to put in the

45
00:02:11,320 --> 00:02:12,800
degrees of freedom, so you could do

46
00:02:12,820 --> 00:02:18,740
equals Chi Square test and then you go

47
00:02:18,760 --> 00:02:25,730
to your expected sorry your observed

48
00:02:25,750 --> 00:02:28,489
there and then you go down to your

49
00:02:28,500 --> 00:02:36,800
expected which is right there and you'll

50
00:02:36,820 --> 00:02:38,930
get the p-value instantly there you get

51
00:02:38,950 --> 00:02:41,810
exactly the same p-value there. So again

52
00:02:41,830 --> 00:02:43,790
there I just pointed to the observed

53
00:02:43,810 --> 00:02:45,950
values in each of the cells in the

54
00:02:45,970 --> 00:02:48,350
contingency tables it's called then the

55
00:02:48,370 --> 00:02:50,420
expected value and I got the p-value

56
00:02:50,440 --> 00:02:52,519
right away now I don't know what the

57
00:02:52,530 --> 00:02:54,769
Chi Square statistic is but basically it

58
00:02:54,780 --> 00:02:56,390
saves me a step that I'm not have to

59
00:02:56,410 --> 00:02:58,580
compute that number which was a bit of a

60
00:02:58,600 --> 00:03:01,910
hassle in the last video. So in summary

61
00:03:01,930 --> 00:03:04,150
we've projected the null hypothesis that

62
00:03:04,170 --> 00:03:06,560
Gender and Eye Color independent we

63
00:03:06,580 --> 00:03:08,209
concluded that Gender does have an

64
00:03:08,220 --> 00:03:10,010
influence on Eye Color I'm not a

65
00:03:10,030 --> 00:03:11,600
biologist so I don't really know the

66
00:03:11,620 --> 00:03:13,730
mechanism there but basically we've

67
00:03:13,750 --> 00:03:15,200
shown you that basically it's pretty

68
00:03:15,220 --> 00:03:17,360
easy to conduct this very important

69
00:03:17,770 --> 00:03:20,200
Chi Square test for independence in Excel.

70
00:03:20,220 --> 00:03:23,700
This video concludes our course on

71
00:03:23,720 --> 00:03:26,730
Data Analysis with Excel. I hope Liberty and myself

72
00:03:26,750 --> 00:03:28,280
have shown you that it's really

73
00:03:28,300 --> 00:03:30,739
fun and easy to do statistics in Excel

74
00:03:30,750 --> 00:03:33,230
and I hope that we've inspired you to go

75
00:03:33,250 --> 00:03:35,680
on in your study of Data analysis and Statistics

76
00:03:35,700 --> 00:03:37,280
because the world needs many

77
00:03:37,300 --> 00:03:39,860
more data analysts to analyze the huge

78
00:03:39,880 --> 00:03:42,880
amounts of data that are being created every day in the world.

79
00:03:42,900 --> 00:03:48,100
So good luck to all of you and I hope you'll keep studying statistics.

