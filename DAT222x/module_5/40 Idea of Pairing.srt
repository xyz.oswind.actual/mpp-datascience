0
00:00:00,940 --> 00:00:05,110
Two data sets are paired when a one to one relationship exists between

1
00:00:05,110 --> 00:00:06,930
values in the two data sets.

2
00:00:06,930 --> 00:00:10,390
As a result, each data set has the same number of data points, and

3
00:00:10,390 --> 00:00:13,200
each data point in one data set is related to one and

4
00:00:13,200 --> 00:00:16,140
only one data point in another set.

5
00:00:16,140 --> 00:00:19,916
An example of a paired test would be before and after drug test.

6
00:00:19,916 --> 00:00:23,039
The researcher might record the cholesterol levels of each

7
00:00:23,039 --> 00:00:26,790
subject in the study before and after the drug is administered.

8
00:00:26,790 --> 00:00:30,750
These measurements would be paired data because each before-measure

9
00:00:30,750 --> 00:00:35,110
is related to only one after-measure for that same subject.

10
00:00:35,110 --> 00:00:38,080
Another example would be evaluating the effectiveness of training by

11
00:00:38,080 --> 00:00:41,880
comparing paired scores on pre and post training assessments.

12
00:00:41,880 --> 00:00:44,810
Obviously, you can create matched pairs on any element of interest.

13
00:00:44,810 --> 00:00:48,390
For example, you might look at miles per gallon of vehicles before and

14
00:00:48,390 --> 00:00:52,100
after you modify an engine part or add some chemical to your gas.

15
00:00:52,100 --> 00:00:54,480
Or you could compare the average heating bill for

16
00:00:54,480 --> 00:00:57,590
some homes before and after new windows were installed.

17
00:00:57,590 --> 00:00:59,190
In these examples, the individuals or

18
00:00:59,190 --> 00:01:03,070
subjects must be present at both times or both measurements.

19
00:01:03,070 --> 00:01:05,320
As a result, from a research perspective,

20
00:01:05,320 --> 00:01:08,650
a match pairs design is likely more practical.

21
00:01:08,650 --> 00:01:11,560
Because it allows you to randomly assign individuals

22
00:01:11,560 --> 00:01:15,140
to one of two treatment conditions and compare the results.

23
00:01:15,140 --> 00:01:15,990
In this design,

24
00:01:15,990 --> 00:01:20,210
individuals are grouped into pairs based on some blocking variable.

25
00:01:20,210 --> 00:01:21,430
Then, within each pairs,

26
00:01:21,430 --> 00:01:24,420
subjects are randomly assigned to different treatments.

27
00:01:24,420 --> 00:01:27,440
A common example of this would be when you went to evaluate

28
00:01:27,440 --> 00:01:29,910
the effectiveness of a new drug or vaccine.

29
00:01:29,910 --> 00:01:32,250
Because we know the power of the placebo effect,

30
00:01:32,250 --> 00:01:35,370
we always want to include people in the study who get placebo while

31
00:01:35,370 --> 00:01:37,640
others get the actual job.

32
00:01:37,640 --> 00:01:40,810
A matched pair would match individuals in your study by

33
00:01:40,810 --> 00:01:44,170
specific characteristics that might confound your results.

34
00:01:44,170 --> 00:01:45,730
In medicine and other fields,

35
00:01:45,730 --> 00:01:50,500
this is often characteristic such as age, gender, race and ethnicity.

36
00:01:50,500 --> 00:01:54,220
So let's imagine that we decide to create a matched pairs based on

37
00:01:54,220 --> 00:01:55,580
age and gender.

38
00:01:55,580 --> 00:01:59,567
In this example, pair 1 might be 2 women both aged 21.

39
00:01:59,567 --> 00:02:02,787
Pair 2 might be 2 men both aged 21.

40
00:02:02,787 --> 00:02:07,575
Pair 3 might be 2 women aged 22, and so on.

41
00:02:07,575 --> 00:02:10,195
When that happens, the matched pairs design is

42
00:02:10,195 --> 00:02:13,465
actually an improvement over completely randomized design.

43
00:02:13,465 --> 00:02:16,605
Like the completely randomized design, the matched pairs design

44
00:02:16,605 --> 00:02:20,585
uses randomization to control for confounding variables.

45
00:02:20,585 --> 00:02:24,701
However, unlike other designs, the matched pairs design explicitly

46
00:02:24,701 --> 00:02:28,406
controls for two potential extraneous variables in this case,

47
00:02:28,406 --> 00:02:32,263
age and gender that could explain any relationship that is found.

