0
00:00:00,960 --> 00:00:05,490
So it's only fair we do a hypothesis test involving categorical data.

1
00:00:05,490 --> 00:00:09,220
And the most important hypothesis test involving categorical data

2
00:00:09,220 --> 00:00:11,200
is the chi square test for independence.

3
00:00:11,200 --> 00:00:13,750
So let's start with a interesting example.

4
00:00:13,750 --> 00:00:16,190
Are eye color and gender interdependent?

5
00:00:16,190 --> 00:00:18,210
They're both categorical variables.

6
00:00:18,210 --> 00:00:21,120
We'll assume everybody's gender is male or female.

7
00:00:21,120 --> 00:00:23,240
And the eye colors we'll look at will be blue,

8
00:00:23,240 --> 00:00:26,150
brown, green and hazel.

9
00:00:26,150 --> 00:00:30,471
And so basically, what we will do is look at a bunch of students and

10
00:00:30,471 --> 00:00:33,301
count how many females had each eye color,

11
00:00:33,301 --> 00:00:35,598
how many males had each eye color.

12
00:00:35,598 --> 00:00:37,478
And this is the data that we have.

13
00:00:37,478 --> 00:00:41,410
And so what does it mean to say eye color and gender are independent?

14
00:00:41,410 --> 00:00:43,310
Well, you think back to module two,

15
00:00:43,310 --> 00:00:46,950
it means basically the chance that you would have blue eyes, or

16
00:00:46,950 --> 00:00:51,940
brown eyes, or green eyes, or hazel eyes, doesn't depend on your gender.

17
00:00:51,940 --> 00:00:54,440
So we should probably look at what fraction of these people

18
00:00:55,730 --> 00:00:57,470
in each gender had each eye color.

19
00:00:57,470 --> 00:00:58,978
So we can do that over here.

20
00:00:58,978 --> 00:01:03,870
So we could say the fraction of the females with blue eyes, we've got

21
00:01:03,870 --> 00:01:07,520
the total number of females here, it would be this fraction have blue.

22
00:01:08,610 --> 00:01:11,409
And we would divide by P7.

23
00:01:11,409 --> 00:01:14,220
But now when we copy that down to get it for males,

24
00:01:14,220 --> 00:01:16,079
we want the 7 to change to an 8.

25
00:01:16,079 --> 00:01:19,818
When we copy this across, we don't want the P to change.

26
00:01:19,818 --> 00:01:23,319
So I'll dollar sign that, okay.

27
00:01:23,319 --> 00:01:27,519
So roughly 33% of the women have blue eyes.

28
00:01:31,019 --> 00:01:33,259
We need to widen the columns, I guess, there.

29
00:01:35,859 --> 00:01:38,399
And if I've done this right, that should add to 100%.

30
00:01:42,859 --> 00:01:49,339
Now, if I do that for the males here, Those should also add to 100%.

31
00:01:53,099 --> 00:01:57,165
Okay, so now the question about the independence in our hypothesis

32
00:01:57,165 --> 00:01:59,000
are gonna be as follows.

33
00:01:59,000 --> 00:02:05,589
The null hypothesis, Is that eye color and gender are independent.

34
00:02:09,089 --> 00:02:12,456
And the alternative hypothesis is eye color and

35
00:02:12,456 --> 00:02:14,569
gender are not independent.

36
00:02:17,910 --> 00:02:20,740
Okay, now what would be perfect independence?

37
00:02:20,740 --> 00:02:24,251
Well, perfect independence would be, for females and males,

38
00:02:24,251 --> 00:02:27,696
we should see exactly the same fraction of them with blue eyes,

39
00:02:27,696 --> 00:02:29,919
brown eyes, green eyes and hazel eyes.

40
00:02:29,919 --> 00:02:33,191
Of course, it's never gonna be exactly the same, but the percentage

41
00:02:33,191 --> 00:02:36,290
with hazel eyes in men and women is pretty much the same.

42
00:02:36,290 --> 00:02:38,100
The percentage of blue eyes seems to differ.

43
00:02:38,100 --> 00:02:40,250
The brown eyes doesn't differ much.

44
00:02:40,250 --> 00:02:43,480
But the green and the blue eyes seem to differ a lot.

45
00:02:43,480 --> 00:02:46,420
The question is, is that a significant difference when you look

46
00:02:46,420 --> 00:02:50,400
over all the eye colors and all the genders involved in the problem?

47
00:02:50,400 --> 00:02:53,723
And that's what the chi square test for independence can tell you,

48
00:02:53,723 --> 00:02:58,749
is, The numbers that we see in this table,

49
00:02:58,749 --> 00:03:02,841
are they illustrating a significant deviation from independence?

50
00:03:02,841 --> 00:03:03,749
Which would mean,

51
00:03:03,749 --> 00:03:07,360
basically, in each column the two numbers would be the same.

52
00:03:07,360 --> 00:03:10,080
So if you think about this, what we're gonna have to do in the next

53
00:03:10,080 --> 00:03:14,690
video is figure out, basically, we know the observed number in

54
00:03:14,690 --> 00:03:17,970
each of these cells of this thing called the contingency table.

55
00:03:17,970 --> 00:03:21,280
We need to figure out the expected number under independence, and

56
00:03:21,280 --> 00:03:24,473
then see is there a significant difference between the observed

57
00:03:24,473 --> 00:03:26,141
number and the expected number.

58
00:03:26,141 --> 00:03:30,151
And there'll be a test statistic based on the observed number minus

59
00:03:30,151 --> 00:03:32,822
the expected number squared, in each cell,

60
00:03:32,822 --> 00:03:34,810
divided by the expected number.

61
00:03:34,810 --> 00:03:38,250
You add those all up and that won't be a normal random variable,

62
00:03:38,250 --> 00:03:41,050
it won't be an f test, it won't be a t test.

63
00:03:41,050 --> 00:03:44,290
It's something called the chi squared random variable.

64
00:03:44,290 --> 00:03:45,660
And like the t random variable,

65
00:03:45,660 --> 00:03:47,910
it has a certain number of degrees of freedom.

66
00:03:47,910 --> 00:03:52,330
The number of rows minus 1 times the number of columns minus 1.

67
00:03:52,330 --> 00:03:53,090
And then, basically,

68
00:03:53,090 --> 00:03:56,560
if you get a big value with the statistic, you reject the null.

69
00:03:56,560 --> 00:03:59,773
Because, basically, if all these percentages in

70
00:03:59,773 --> 00:04:04,020
each column were the same, you would get a statistic of 0.

71
00:04:04,020 --> 00:04:05,400
So the bigger the statistic,

72
00:04:05,400 --> 00:04:08,410
results in a rejection of the null hypothesis.

73
00:04:08,410 --> 00:04:11,810
So in the next video we'll go about computing this test statistic.

74
00:04:11,810 --> 00:04:14,945
And then we'll get the results of the hypothesis, which will tell us

75
00:04:14,945 --> 00:04:18,000
are eye color and gender independent or not independent.

