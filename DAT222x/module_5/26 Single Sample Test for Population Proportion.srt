0
00:00:00,590 --> 00:00:02,186
When it comes to evaluating the

1
00:00:02,240 --> 00:00:03,580
statistical significance of

2
00:00:03,648 --> 00:00:05,620
probabilities, we follow the same logic.

3
00:00:05,790 --> 00:00:08,510
As always you start by stating your

4
00:00:08,592 --> 00:00:11,130
hypothesis as some of you may know the

5
00:00:11,184 --> 00:00:13,210
National Football League made it more

6
00:00:13,264 --> 00:00:15,620
difficult to kick the extra point after

7
00:00:15,696 --> 00:00:17,440
touchdown by making it longer.

8
00:00:17,550 --> 00:00:19,740
Let's imagine you are invested in the

9
00:00:19,792 --> 00:00:22,040
proportion of extra kicks made on

10
00:00:22,112 --> 00:00:24,010
Sundays during football season.

11
00:00:24,320 --> 00:00:27,060
The population proportion is 0.75, but you

12
00:00:27,136 --> 00:00:28,500
think it might actually be lower than

13
00:00:28,560 --> 00:00:31,540
that with this change. So your null hypothesis

14
00:00:31,600 --> 00:00:32,920
and alternate hypothesis

15
00:00:32,960 --> 00:00:36,600
would be evaluating a lower one-tailed hypothesis test.

16
00:00:36,790 --> 00:00:39,866
Next you select your alpha or level of significance.

17
00:00:39,920 --> 00:00:42,933
In this case, you select 0.05 because

18
00:00:43,008 --> 00:00:45,960
you're investigating the left tail. The Z obtained

19
00:00:46,048 --> 00:00:47,640
should be smaller than negative

20
00:00:47,728 --> 00:00:51,840
1.645, you watch 50 games counting the

21
00:00:51,904 --> 00:00:53,640
number of extra points attempted,

22
00:00:53,712 --> 00:00:56,100
those are your number of trials and the number

23
00:00:56,160 --> 00:00:58,813
actually made. These are your successors.

24
00:00:58,900 --> 00:01:01,560
You note that 150 kicks were attempted

25
00:01:01,616 --> 00:01:03,600
and 96 were made resulting in a

26
00:01:03,680 --> 00:01:06,710
proportion of 0.64. To determine if the

27
00:01:06,800 --> 00:01:08,100
proportion that you obtained is

28
00:01:08,192 --> 00:01:09,420
statistically different from the

29
00:01:09,472 --> 00:01:11,620
population proportion you subtract

30
00:01:11,680 --> 00:01:15,400
the obtained proportion 0.64 from the

31
00:01:15,472 --> 00:01:18,570
population proportion 0.75 and divided by the

32
00:01:18,624 --> 00:01:20,700
standard deviation. Recall that

33
00:01:20,752 --> 00:01:22,490
the standard deviation of a proportion

34
00:01:22,560 --> 00:01:25,740
is the square root of p multiplied by q

35
00:01:25,866 --> 00:01:27,620
divided by the sample size.

36
00:01:27,670 --> 00:01:30,213
In this example, our standard deviation is

37
00:01:30,272 --> 00:01:33,590
0.035. We obtain a Z score

38
00:01:33,664 --> 00:01:37,250
of negative 3.14 which is less than the

39
00:01:37,296 --> 00:01:39,920
required value of 1. of negative

40
00:01:40,000 --> 00:01:43,520
1.645 meaning we reject the null.

41
00:01:43,600 --> 00:01:45,450
It appears that making the extra kick

42
00:01:45,488 --> 00:01:47,210
longer has resulted in a lower

43
00:01:47,264 --> 00:01:48,690
percentage of success.

44
00:01:48,760 --> 00:01:51,573
Wayne will walk you through how to do this using Excel

45
00:01:51,632 --> 00:01:55,200
and the binomial distribution range formula in the next video.

