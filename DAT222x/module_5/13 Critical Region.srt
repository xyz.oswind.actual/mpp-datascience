0
00:00:00,690 --> 00:00:04,605
You'll recall that you will need to decide if your hypothesis is one- or

1
00:00:04,605 --> 00:00:05,530
two-tailed.

2
00:00:05,530 --> 00:00:08,800
This matters because you are less likely to reject the null if you use

3
00:00:08,800 --> 00:00:11,585
a two-tailed hypothesis test because you are splitting the risk that

4
00:00:11,585 --> 00:00:14,690
you're willing to take between both sides of the distribution.

5
00:00:14,690 --> 00:00:17,510
If you can predict the direction of the expected relationship,

6
00:00:17,510 --> 00:00:21,170
then you can put all of your risk into that side of the distribution.

7
00:00:21,170 --> 00:00:23,240
Of course, if you are wrong about the direction,

8
00:00:23,240 --> 00:00:26,980
you will never reject the null hypothesis.

9
00:00:26,980 --> 00:00:28,150
Here's how this works.

10
00:00:28,150 --> 00:00:29,950
When you formulate your analysis plan,

11
00:00:29,950 --> 00:00:32,180
you will select the level of significance, or alpha,

12
00:00:32,180 --> 00:00:35,870
that the statistic resulting from your analysis must achieve

13
00:00:35,870 --> 00:00:39,580
in order to be sufficiently confident in rejecting the null.

14
00:00:39,580 --> 00:00:41,720
Many people use alphas of 0.05, so

15
00:00:41,720 --> 00:00:44,970
I'll illustrate my point using that level of significance.

16
00:00:44,970 --> 00:00:48,458
In a one-tailed upper hypothesis, the test statistic must be greater

17
00:00:48,458 --> 00:00:51,800
than the value associated with its significance level.

18
00:00:51,800 --> 00:00:57,190
In z-tests and large sample t-tests, this value is 1.645.

19
00:00:57,190 --> 00:01:03,270
In one-tailed lower hypothesis, this value is negative 1.645.

20
00:01:03,270 --> 00:01:07,450
However, in a two-tailed test, you divide your desired level of

21
00:01:07,450 --> 00:01:10,630
significance by two, meaning that you will need to obtain a test

22
00:01:10,630 --> 00:01:15,290
statistic that exceeds the value of 0.025, which in z-tests and

23
00:01:15,290 --> 00:01:19,476
large sample t-tests is negative 1.96 at the lower end and

24
00:01:19,476 --> 00:01:21,190
1.96 at the upper end,

25
00:01:21,190 --> 00:01:25,440
making it much more difficult to assign statistical significance.

26
00:01:25,440 --> 00:01:28,910
The area above this value is known as the critical region.

27
00:01:28,910 --> 00:01:31,960
The critical value needed is based on the statistic that you're using

28
00:01:31,960 --> 00:01:33,440
and the sample size.

29
00:01:33,440 --> 00:01:37,443
As I just suggested, however, the z-score and t-score critical values

30
00:01:37,443 --> 00:01:41,141
are the same in large samples, thanks to the central limit theorem.

31
00:01:41,141 --> 00:01:44,993
As samples get sufficiently large, remember we said that a good rule of

32
00:01:44,993 --> 00:01:47,495
thumb is 30 or more, the critical values for

33
00:01:47,495 --> 00:01:50,530
t-tests start to look like those needed for z-tests.

34
00:01:50,530 --> 00:01:55,050
In such cases z and t critical values are essentially the same.

35
00:01:55,050 --> 00:01:56,850
However, the critical values needed for

36
00:01:56,850 --> 00:02:01,030
other types of statistical tests, such as f-tests, will be different.

37
00:02:01,030 --> 00:02:03,390
You can find those values in tables through a quick search of

38
00:02:03,390 --> 00:02:07,820
the Internet or, in most cases, by using the formulas right in Excel.

39
00:02:07,820 --> 00:02:09,940
Or you can obtain the critical values for

40
00:02:09,940 --> 00:02:12,890
a z-test by using some simple math, as shown here.

41
00:02:12,890 --> 00:02:14,912
You can do the same for

42
00:02:14,912 --> 00:02:19,060
a t-test by replacing the z values with the appropriate t values.

43
00:02:19,060 --> 00:02:19,950
Don't freak out,

44
00:02:19,950 --> 00:02:22,280
I'm gonna walk you through how to do this in the next two modules.

