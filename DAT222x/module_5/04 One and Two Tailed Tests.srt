0
00:00:01,190 --> 00:00:04,500
A test of a statistical hypothesis where the region of rejection is on

1
00:00:04,500 --> 00:00:07,020
only one side of the sampling distribution is called the one

2
00:00:07,020 --> 00:00:08,200
tailed test.

3
00:00:08,200 --> 00:00:11,814
For example, suppose the null hypothesis states the mean is less

4
00:00:11,814 --> 00:00:13,202
than or equal to 500.

5
00:00:13,202 --> 00:00:17,250
The alternative hypothesis would be that the mean is greater than 500.

6
00:00:17,250 --> 00:00:20,430
The region of rejection for the null hypothesis then consists of

7
00:00:20,430 --> 00:00:23,700
the numbers located on the right side of the sampling distribution.

8
00:00:23,700 --> 00:00:26,880
That is a set of numbers that is considered to be greater than 500

9
00:00:26,880 --> 00:00:28,960
from a statistical perspective.

10
00:00:28,960 --> 00:00:32,020
Essentially anything to the left of that blue line is considered

11
00:00:32,020 --> 00:00:36,090
equivalent to 500 or less for the purposes of hypothesis testing.

12
00:00:36,090 --> 00:00:38,300
Even if that number is greater than 500.

13
00:00:38,300 --> 00:00:41,380
At some point that value will become large enough to be statistically

14
00:00:41,380 --> 00:00:42,260
significant.

15
00:00:42,260 --> 00:00:45,090
This will be determined by your significance level, sample size, and

16
00:00:45,090 --> 00:00:46,490
standard deviation.

17
00:00:46,490 --> 00:00:49,750
This version is more specifically called an upper

18
00:00:49,750 --> 00:00:52,360
one sided alternative hypothesis.

19
00:00:52,360 --> 00:00:55,530
Suppose the null hypothesis states that the mean is greater than or

20
00:00:55,530 --> 00:00:56,830
equal to 500.

21
00:00:56,830 --> 00:01:00,070
The alternative hypothesis would then be that the mean is less than

22
00:01:00,070 --> 00:01:00,800
500.

23
00:01:00,800 --> 00:01:02,540
Because the critical region for

24
00:01:02,540 --> 00:01:05,450
significance is to the left of the distribution,

25
00:01:05,450 --> 00:01:09,320
this is known as a lower one-sided alternative hypothesis.

26
00:01:09,320 --> 00:01:12,680
Now, imagine that we know that there is a difference but

27
00:01:12,680 --> 00:01:14,768
we're not sure which direction it is.

28
00:01:14,768 --> 00:01:18,930
We simply wanna test if the result is of a particular value or not.

29
00:01:18,930 --> 00:01:19,870
In this example,

30
00:01:19,870 --> 00:01:23,850
our alternative hypothesis does not specify a particular direction.

31
00:01:23,850 --> 00:01:26,750
The region of rejection would consist of numbers located on both

32
00:01:26,750 --> 00:01:28,730
sides of the sampling distribution.

33
00:01:28,730 --> 00:01:31,660
That is the region of rejection would consist partly of

34
00:01:31,660 --> 00:01:34,790
numbers that are considered less than 500 statistically.

35
00:01:34,790 --> 00:01:37,770
And partly of numbers that are considered greater than 500

36
00:01:37,770 --> 00:01:38,820
statistically.

37
00:01:38,820 --> 00:01:42,933
Everything in between the blue lines is considered equivalent to 500.

38
00:01:42,933 --> 00:01:46,404
Therefore, the alternative hypothesis is called a two sided

39
00:01:46,404 --> 00:01:47,949
alternative hypothesis.

