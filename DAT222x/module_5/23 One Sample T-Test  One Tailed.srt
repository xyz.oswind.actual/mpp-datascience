0
00:00:01,140 --> 00:00:04,000
We call that thanks to the Central Limit Theorem

1
00:00:04,020 --> 00:00:07,225
with sufficiently large samples the T-distribution

2
00:00:07,240 --> 00:00:09,100
starts to look like the Z distribution.

3
00:00:09,120 --> 00:00:11,850
So the difference between a Z-score and a T-score

4
00:00:11,870 --> 00:00:14,950
is only noticeable on samples less than 30,

5
00:00:14,970 --> 00:00:18,450
much of what you learned about Z test applies to T tests.

6
00:00:18,470 --> 00:00:22,100
Even the formulas to calculate this should look very much the same,

7
00:00:22,120 --> 00:00:25,825
just like with Z test you start by stating your hypotheses.

8
00:00:25,840 --> 00:00:29,700
Let's imagine you are conducting a study about the average number of miles

9
00:00:29,720 --> 00:00:32,600
hiked by hikers in the Pacific Northwest in a given year.

10
00:00:32,620 --> 00:00:37,475
The mean is 500 miles, but you think it actually might be higher.

11
00:00:37,520 --> 00:00:40,150
So your null and alternate hypotheses would be

12
00:00:40,170 --> 00:00:42,925
for upper one tailed hypothesis test.

13
00:00:42,940 --> 00:00:47,250
Next, you select your alpha or level of significance in this case,

14
00:00:47,270 --> 00:00:49,200
you select 0.5.

15
00:00:49,220 --> 00:00:53,275
What is the critical value that you'll need to reject the null hypothesis?

16
00:00:53,290 --> 00:00:57,975
To do this the population mean proposed in the null hypothesis is added to the

17
00:00:58,020 --> 00:01:00,775
product of the critical T-score at the alpha level

18
00:01:00,790 --> 00:01:03,625
given the degrees of freedom and the standard error.

19
00:01:03,640 --> 00:01:06,775
In this version of the study you sample 16 people,

20
00:01:06,820 --> 00:01:09,975
so your degrees of freedom is 15 and minus 1.

21
00:01:09,990 --> 00:01:16,700
When alpha equals 0.5, T with 15 degrees of freedom is 1.753

22
00:01:16,720 --> 00:01:20,950
If we know the sample standard deviation is 52.63

23
00:01:20,970 --> 00:01:23,425
and we have sampled 16 hikers,

24
00:01:23,440 --> 00:01:27,675
we find that the critical value is 523.07.

25
00:01:27,690 --> 00:01:33,500
If our sample results in an average that is greater than 523.07

26
00:01:33,520 --> 00:01:35,760
we reject the null hypothesis.

27
00:01:35,780 --> 00:01:41,025
This is much larger than the 507.07, we calculated with the z-score

28
00:01:41,040 --> 00:01:44,825
and that is entirely a function of the sample size in this example.

29
00:01:44,840 --> 00:01:47,725
Because I used the same standard deviation in both

30
00:01:47,740 --> 00:01:50,300
the T test and Z test examples.

31
00:01:50,340 --> 00:01:54,525
Increasing the sample standard deviation which is more likely in a

32
00:01:54,540 --> 00:01:56,975
small sample when compared to the population.

33
00:01:56,990 --> 00:01:59,900
You would see this value increase even more.

34
00:01:59,940 --> 00:02:03,900
More commonly you calculate the T score by subtracting the

35
00:02:03,920 --> 00:02:07,950
population mean from the sample mean and dividing by the standard error.

36
00:02:07,970 --> 00:02:12,830
Imagine that we obtained an average of 525.01.

37
00:02:12,870 --> 00:02:16,450
using the critical value that we just calculated, we know that

38
00:02:16,470 --> 00:02:20,650
this will be significant. However let's play this out just to see how it works

39
00:02:20,670 --> 00:02:24,250
using this formula. Here you want the resulting value

40
00:02:24,270 --> 00:02:29,050
to be larger than the critical T-score of 1.753.

41
00:02:29,070 --> 00:02:34,320
The difference between the population and sample mean is 25.01

42
00:02:34,340 --> 00:02:38,350
which divided by the standard error results in 1.90

43
00:02:38,370 --> 00:02:42,690
hich is greater than the critical T value of 1.753,

44
00:02:42,710 --> 00:02:45,475
So we reject the null hypothesis.

