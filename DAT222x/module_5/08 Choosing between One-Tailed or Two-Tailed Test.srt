0
00:00:00,740 --> 00:00:03,370
So the question is, should you plan for a one or

1
00:00:03,370 --> 00:00:05,350
two tailed hypothesis test?

2
00:00:05,350 --> 00:00:08,730
Some statisticians believe you should always use a two tailed test

3
00:00:08,730 --> 00:00:12,720
because a priori, you have no idea of the direction in which deviations

4
00:00:12,720 --> 00:00:14,890
from the null hypothesis will occur.

5
00:00:14,890 --> 00:00:17,960
However, most statisticians feel that if you know the direction of

6
00:00:17,960 --> 00:00:19,860
the deviation from the null hypothesis,

7
00:00:19,860 --> 00:00:23,310
you should use a one tailed alternative hypothesis test.

8
00:00:23,310 --> 00:00:26,530
Making the right decision here would increase the probability of finding

9
00:00:26,530 --> 00:00:27,820
a significant result.

10
00:00:27,820 --> 00:00:30,730
We'll talk more about why this matters later in this module.

