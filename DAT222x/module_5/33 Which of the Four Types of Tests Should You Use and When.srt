0
00:00:00,700 --> 00:00:04,240
Most statisticians believe the most important hypothesis test you can

1
00:00:04,240 --> 00:00:08,360
learn is how to test the differences between population means.

2
00:00:08,360 --> 00:00:11,130
Unfortunately there are four different tests that you sort of

3
00:00:11,130 --> 00:00:12,540
need to be familiar with.

4
00:00:12,540 --> 00:00:16,880
Fortunately Excel has, in the data analysis add-in,

5
00:00:16,880 --> 00:00:18,600
an easy way to do each of these tests.

6
00:00:18,600 --> 00:00:22,277
So let's go through the four situations and the tests that you

7
00:00:22,277 --> 00:00:25,822
can use to test the differences between population means.

8
00:00:25,822 --> 00:00:29,949
Suppose you have a large sample size (n>=30) from each population and

9
00:00:29,949 --> 00:00:32,980
the samples from the two populations are independent,

10
00:00:32,980 --> 00:00:36,525
in other words knowing which gap from one population in the sample

11
00:00:36,525 --> 00:00:38,030
does not affect the other.

12
00:00:38,030 --> 00:00:41,320
Then you use the z-test, z meaning normal cuz you have the central

13
00:00:41,320 --> 00:00:44,580
limit there Two samples for mean tests from Excel.

14
00:00:44,580 --> 00:00:48,000
Now let's suppose that you have a small sample size and

15
00:00:48,000 --> 00:00:52,090
less than 30 for at least one population.

16
00:00:52,090 --> 00:00:53,470
Your populations are normal.

17
00:00:53,470 --> 00:00:55,230
Your variances are unknown.

18
00:00:55,230 --> 00:00:58,790
Variances unknown should tip you off we need the t random variable

19
00:00:58,790 --> 00:01:01,250
as we discussed earlier in the module.

20
00:01:01,250 --> 00:01:04,620
So if you have sample size is small for at least one population,

21
00:01:04,620 --> 00:01:08,280
populations normal, variances unknown but equal.

22
00:01:08,280 --> 00:01:10,680
That's why we have to do that test and equal variances.

23
00:01:10,680 --> 00:01:13,410
And the samples from the two populations are independent.

24
00:01:13,410 --> 00:01:17,060
You do the t-Test two sample assuming equal variances.

25
00:01:17,060 --> 00:01:20,790
Now the next test is exactly the same situation as the previous test

26
00:01:20,790 --> 00:01:24,800
but the variances are unknown but unequal.

27
00:01:24,800 --> 00:01:26,846
So again we'll do that two sample test for

28
00:01:26,846 --> 00:01:29,253
variance first when we're gonna use a t-Test.

29
00:01:29,253 --> 00:01:33,815
Now we accept equal variances we'll use this test from the analysis tool

30
00:01:33,815 --> 00:01:35,200
pack addin.

31
00:01:35,200 --> 00:01:39,230
If we accept that the variances are unequal, we'll use this test.

32
00:01:39,230 --> 00:01:43,090
And finally our two populations might be normal, the observations

33
00:01:43,090 --> 00:01:46,370
from the two populations can be paired in a natural fashion.

34
00:01:46,370 --> 00:01:48,220
We'll talk more about that later.

35
00:01:48,220 --> 00:01:52,000
Then you use the t-Test paired two sample test per means.

36
00:01:52,000 --> 00:01:55,266
So in the next video we'll start showing you how the two sample z

37
00:01:55,266 --> 00:01:55,950
tests work.

