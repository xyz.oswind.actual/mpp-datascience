0
00:00:00,510 --> 00:00:03,730
In this video, we'll show you how to do the two sample Z test for

1
00:00:03,730 --> 00:00:05,340
difference in means.

2
00:00:05,340 --> 00:00:07,140
So here's the example we'll deal with.

3
00:00:07,140 --> 00:00:11,340
Let's suppose you're the placement director at a major MBA program, and

4
00:00:11,340 --> 00:00:14,770
you wanna know, do marketing graduates or

5
00:00:14,770 --> 00:00:18,250
finance graduates get different starting salaries?

6
00:00:18,250 --> 00:00:20,940
So your null hypothesis would be the mean.

7
00:00:20,940 --> 00:00:25,470
Starting salary for a marketing MBA equals the mean for a finance MBA.

8
00:00:25,470 --> 00:00:27,980
The alternative is that they're not equal.

9
00:00:27,980 --> 00:00:31,565
So you collect some data, we have 227 marketing

10
00:00:31,565 --> 00:00:36,340
students' starting salaries, COUNT function tells you how many,

11
00:00:36,340 --> 00:00:40,910
we have 211 finance students' starting salaries.

12
00:00:40,910 --> 00:00:44,160
And if you look at the average of these salaries, the marketing

13
00:00:44,160 --> 00:00:48,775
majors average about $99,000, the finance majors, about $109,000.

14
00:00:48,775 --> 00:00:52,110
The question is, are those sample means significantly different?

15
00:00:52,110 --> 00:00:55,550
But that really depends on the variance within each population.

16
00:00:55,550 --> 00:00:58,410
If every marketing student made 99,000 and

17
00:00:58,410 --> 00:01:02,770
every finance student made 109,000, you'd be sure they're different.

18
00:01:02,770 --> 00:01:06,330
But since there's variability here, we need to include that in our test.

19
00:01:06,330 --> 00:01:11,270
So we've computed the sample variance using the VAR function for

20
00:01:11,270 --> 00:01:16,150
basically the marketing graduate and the finance graduates.

21
00:01:16,150 --> 00:01:18,000
And we'll need that as an input, and

22
00:01:18,000 --> 00:01:20,090
it won't take cell references to test here.

23
00:01:20,090 --> 00:01:23,990
So you have to have those numbers in your mind before you go to

24
00:01:23,990 --> 00:01:25,640
the data analysis add-in.

25
00:01:25,640 --> 00:01:29,380
But then, all you do is punch in the data to the data analysis add-in,

26
00:01:29,380 --> 00:01:32,200
click OK, you'll get the P-Value for this test.

27
00:01:32,200 --> 00:01:35,570
And if it's less than or equal to 0.05 in the usual situation,

28
00:01:35,570 --> 00:01:39,300
you reject the null, if it's greater than 0.05, you accept the null.

29
00:01:39,300 --> 00:01:41,480
So that's all there is to it.

30
00:01:41,480 --> 00:01:44,560
So we go Data Analysis, and if you don't remember how

31
00:01:44,560 --> 00:01:47,170
to get the Data Analysis add-in, go back to Module One.

32
00:01:48,220 --> 00:01:53,067
Okay, so the Variable 1 range, you go here for marketing,

33
00:01:53,067 --> 00:01:55,169
row 5 down to row 231.

34
00:01:55,169 --> 00:02:00,442
The second variable, click on the little button, scroll up.

35
00:02:03,445 --> 00:02:04,280
Go to the bottom.

36
00:02:06,690 --> 00:02:08,690
Hypothesized Mean Difference, we said 0.

37
00:02:08,690 --> 00:02:11,671
You can have a different Hypothesized Mean Difference

38
00:02:11,671 --> 00:02:12,390
if you want.

39
00:02:12,390 --> 00:02:17,281
Now Variance 1 null, and I wrote this down, 131.66,

40
00:02:17,281 --> 00:02:22,855
cuz it won't let you point to the cell, Variance 2, 144.02.

41
00:02:22,855 --> 00:02:25,120
Okay, I didn't put the labels in.

42
00:02:25,120 --> 00:02:29,268
I could have, but basically, Output Range, we can put this near the top

43
00:02:29,268 --> 00:02:32,540
here, and we'll get the P-Value really quickly here.

44
00:02:35,540 --> 00:02:37,130
And the P-Value is 0.

45
00:02:38,910 --> 00:02:41,620
Okay, for the two-tailed test or the one-tailed test.

46
00:02:41,620 --> 00:02:44,600
We did a two-tailed test, so 0 is less than or

47
00:02:44,600 --> 00:02:46,170
equal to any value of alpha.

48
00:02:46,170 --> 00:02:49,786
P-Value is 0, so reject the null hypothesis, in other words,

49
00:02:49,786 --> 00:02:52,878
if the two means were different, there's basically no

50
00:02:52,878 --> 00:02:56,290
chance we get this big a discrepancy between the means.

51
00:02:56,290 --> 00:02:58,770
So we conclude that the two means are different.

52
00:02:58,770 --> 00:03:02,080
In other words, the average starting salary for marketing majors

53
00:03:02,080 --> 00:03:05,885
is different than the average starting salary for finance majors.

54
00:03:05,885 --> 00:03:06,865
In the next video,

55
00:03:06,865 --> 00:03:12,140
we'll discuss basically how to do the T test with equal variances.

