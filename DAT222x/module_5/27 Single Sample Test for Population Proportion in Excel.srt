0
00:00:00,670 --> 00:00:01,520
Okay. In this video,

1
00:00:01,520 --> 00:00:05,640
we're gonna talk about hypothesis involving a population proportion,

2
00:00:05,640 --> 00:00:09,430
like the probability of success in a binomial random variable,

3
00:00:09,430 --> 00:00:12,510
or where we talked about the fraction of defective chips in

4
00:00:12,510 --> 00:00:14,770
example one in this module.

5
00:00:14,770 --> 00:00:18,353
So basically, the hypothesis we can have involving a proportion would be

6
00:00:18,353 --> 00:00:20,609
basically, the left-tailed alternative,

7
00:00:20,609 --> 00:00:24,480
the right-tailed alternative, or the two-tailed alternative.

8
00:00:24,480 --> 00:00:27,430
So, we might have the null hypothesis that the population

9
00:00:27,430 --> 00:00:30,000
proportion is less than or equal to an amount p0.

10
00:00:30,000 --> 00:00:33,560
The alternative is that it's greater than p0.

11
00:00:33,560 --> 00:00:36,060
That would be the right-tailed test.

12
00:00:36,060 --> 00:00:40,890
We might have the null hypothesis be that the probability.

13
00:00:40,890 --> 00:00:41,850
Sorry, I messed that up.

14
00:00:41,850 --> 00:00:43,626
Let's start over.

15
00:00:43,626 --> 00:00:44,206
Just do it.

16
00:00:44,206 --> 00:00:46,026
>> Maybe a start over.

17
00:00:46,026 --> 00:00:48,186
>> Yeah, it's just easier.

18
00:00:48,186 --> 00:00:48,966
>> Are you ready to go?

19
00:00:48,966 --> 00:00:49,766
>> Yes, sure.

20
00:00:49,766 --> 00:00:51,987
>> All right, we gonna go in three.

21
00:00:51,987 --> 00:00:52,925
>> In this video,

22
00:00:52,925 --> 00:00:57,540
we'll discuss hypothesis involving a population proportion.

23
00:00:57,540 --> 00:01:01,530
Like the probability of success in a binomial random variable.

24
00:01:01,530 --> 00:01:04,890
So, there are three types of hypotheses we can have.

25
00:01:04,890 --> 00:01:08,220
We can have a right-tailed alternative that knows that p equals

26
00:01:08,220 --> 00:01:10,930
or is less or or equal to p0 hypothesized value.

27
00:01:10,930 --> 00:01:13,930
And the alternative is that p is greater than p zero.

28
00:01:13,930 --> 00:01:15,830
That's a right-tailed alternative.

29
00:01:15,830 --> 00:01:19,160
We could have a left-tailed alternative that the null hypothesis

30
00:01:19,160 --> 00:01:22,600
is that p is greater than or equal to p0, or p equal p0.

31
00:01:22,600 --> 00:01:25,100
The alternative is p less than p0.

32
00:01:26,140 --> 00:01:30,714
We can also have a two-tailed alternative, that p equals p0 is

33
00:01:30,714 --> 00:01:36,410
the null, and p is not equal to p0 would be the two-tail alternative.

34
00:01:36,410 --> 00:01:39,330
And so we setup the spreadsheet that you can

35
00:01:39,330 --> 00:01:41,040
easily do your hypotheses test.

36
00:01:41,040 --> 00:01:45,060
All you do is put in how many trials are your sample size.

37
00:01:45,060 --> 00:01:47,030
How many successes you observed.

38
00:01:47,030 --> 00:01:50,810
And then the hypothesized value of the probability of success.

39
00:01:50,810 --> 00:01:55,500
And your P-values for each test will be spit out here by the formulas.

40
00:01:55,500 --> 00:01:57,240
And basically, if your P-value is less or

41
00:01:57,240 --> 00:01:59,690
equal to alpha, you reject the null.

42
00:01:59,690 --> 00:02:01,340
And if your P-value is greater than alpha,

43
00:02:01,340 --> 00:02:03,310
you accept the null hypothesis.

44
00:02:03,310 --> 00:02:06,750
And that's the theme you'll see throughout the rest of this module

45
00:02:06,750 --> 00:02:10,520
and throughout the rest of your career studying hypothesis tests.

46
00:02:10,520 --> 00:02:13,320
So, in the next video we'll do a specific example

47
00:02:13,320 --> 00:02:15,690
of a hypothesis test involving a proportion.

