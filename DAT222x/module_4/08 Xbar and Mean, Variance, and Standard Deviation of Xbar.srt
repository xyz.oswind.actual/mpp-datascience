0
00:00:00,720 --> 00:00:03,820
Let's suppose we wanna estimate the unknown mean of a population.

1
00:00:03,820 --> 00:00:05,330
We'll call this unknown mean Mu.

2
00:00:05,330 --> 00:00:07,220
And we just don't know what it is.

3
00:00:07,220 --> 00:00:09,140
How can we try and estimate it?

4
00:00:09,140 --> 00:00:13,270
Well we would probably take a sample and so what would be a logical way

5
00:00:13,270 --> 00:00:16,210
to estimate that unknown population mean Mu?

6
00:00:16,210 --> 00:00:20,910
Use the sample mean which we'll call xbar, as an estimated Mu.

7
00:00:20,910 --> 00:00:24,635
So we take a sample of n independent observations, x1, x2,

8
00:00:24,635 --> 00:00:25,970
xn from a population,

9
00:00:25,970 --> 00:00:30,830
then xbar is simply the average of the n observations in this sample.

10
00:00:30,830 --> 00:00:33,720
The sample mean turns out to be an unbiased estimated mu.

11
00:00:33,720 --> 00:00:35,410
What does that say?

12
00:00:35,410 --> 00:00:39,720
That on the average, the expected value of that x bar,

13
00:00:39,720 --> 00:00:41,340
that sample mean is a random variable,

14
00:00:41,340 --> 00:00:44,420
will be different every sample, will equal the population mean.

15
00:00:44,420 --> 00:00:46,130
How can we show that?

16
00:00:46,130 --> 00:00:49,970
Ok, well we have the expected value of x bar is the expected value of

17
00:00:49,970 --> 00:00:52,950
x 1 over n, plus x 2 over n, etc.

18
00:00:52,950 --> 00:00:55,958
Well you can factor out a constant in the expected values so

19
00:00:55,958 --> 00:01:00,120
you'd get expected value of x1 over n is one over n times the expected

20
00:01:00,120 --> 00:01:04,870
value of x1, which is mu, plus the expected value of x2 over n which is

21
00:01:04,870 --> 00:01:10,300
one over n times the expected value of x2, which is mu over n there.

22
00:01:10,300 --> 00:01:13,750
So you'd have n times mu over n which averages out to mu.

23
00:01:13,750 --> 00:01:16,730
So that proves that the sample mean is an unbiased estimator of

24
00:01:16,730 --> 00:01:17,600
the population mean.

25
00:01:18,620 --> 00:01:21,460
Well, since the sample mean is a random variable, take a sample of

26
00:01:21,460 --> 00:01:25,510
size 3 for example, you'll get a different sample mean each time.

27
00:01:25,510 --> 00:01:27,013
It must have a variance.

28
00:01:27,013 --> 00:01:30,087
So let's suppose the population we're sampling from has an unknown

29
00:01:30,087 --> 00:01:31,680
variance sigma squared.

30
00:01:31,680 --> 00:01:33,780
What would be the variance of x bar?

31
00:01:33,780 --> 00:01:36,060
Well we need one more rule here we haven't discussed.

32
00:01:36,060 --> 00:01:38,580
If you take a constant times a random variable,

33
00:01:38,580 --> 00:01:39,800
what's its variance?

34
00:01:39,800 --> 00:01:43,160
It's the constant squared times the variance of the random variable, and

35
00:01:43,160 --> 00:01:46,100
that's because variance involves square root deviations.

36
00:01:46,100 --> 00:01:49,570
So we have x bar is basically the sample mean.

37
00:01:49,570 --> 00:01:51,760
What would be the variance of x bar?

38
00:01:51,760 --> 00:01:53,220
Well assuming these are independent,

39
00:01:53,220 --> 00:01:55,880
the variants of the sum is the sum of the variances.

40
00:01:55,880 --> 00:01:58,310
What's the variance of the x1 over n?

41
00:01:58,310 --> 00:01:59,820
Well it's 1 over n, squared.

42
00:01:59,820 --> 00:02:00,850
The c is one over n,

43
00:02:00,850 --> 00:02:05,720
times the variance of the x1, which is gonna be Sigma squared.

44
00:02:05,720 --> 00:02:07,630
Plus the variance of x2 over n,

45
00:02:07,630 --> 00:02:10,950
which is one over n squared times the variance of x2.

46
00:02:10,950 --> 00:02:14,745
And then finally the variance of xn over n is 1 over n squared times

47
00:02:14,745 --> 00:02:16,560
the variance of xn.

48
00:02:16,560 --> 00:02:21,280
So you have n terms, each of which is sigma over n squared.

49
00:02:21,280 --> 00:02:24,440
So if you add those up, you get sigma squared over n.

50
00:02:24,440 --> 00:02:27,150
So the variance of the sample mean is

51
00:02:27,150 --> 00:02:31,240
the population mean divided by the sample size, very important.

52
00:02:31,240 --> 00:02:34,250
And then the standard deviation of the sample mean would be the square

53
00:02:34,250 --> 00:02:35,560
root of that variance,

54
00:02:35,560 --> 00:02:38,370
which would be sigma divided by the square root of n.

55
00:02:38,370 --> 00:02:41,920
That's often called the standard error of x bar.

56
00:02:41,920 --> 00:02:42,750
In the next video,

57
00:02:42,750 --> 00:02:45,680
we'll illustrate with a concrete example how these formulas work.

