0
00:00:00,560 --> 00:00:03,333
Statisticians use confidence intervals to describe

1
00:00:03,377 --> 00:00:05,800
the amount of uncertainty associated with a sample

2
00:00:05,822 --> 00:00:07,844
estimate of a population parameter

3
00:00:07,911 --> 00:00:14,000
often researchers choose 90%, 95%, or 99% confidence levels

4
00:00:14,088 --> 00:00:16,333
but any percentage can be used.

5
00:00:16,377 --> 00:00:19,266
Confidence intervals consist of two parts

6
00:00:19,288 --> 00:00:22,533
the first part is the estimate of the population parameter

7
00:00:22,577 --> 00:00:25,488
sample statistic and the second is the margin of

8
00:00:25,533 --> 00:00:27,755
error which is derived from our desired level

9
00:00:27,800 --> 00:00:30,977
confidence in the standard error of the sample statistic.

10
00:00:31,022 --> 00:00:34,933
The sample statistic is in the center of the interval

11
00:00:34,955 --> 00:00:37,600
we then add and subtract the margin of error

12
00:00:37,688 --> 00:00:39,422
from this statistic to obtain

13
00:00:39,488 --> 00:00:41,688
a range of values for the parameter

14
00:00:41,733 --> 00:00:45,177
to calculate a confidence interval you need to first

15
00:00:45,244 --> 00:00:47,155
choose a sample statistic

16
00:00:47,222 --> 00:00:51,711
x-bar or P-hat that you will use to estimate a population parameter,

17
00:00:51,777 --> 00:00:58,111
second you will select a confidence level 90%, 95%, or 99%.

18
00:00:58,111 --> 00:01:00,822
You calculate the margin of error next

19
00:01:00,822 --> 00:01:03,755
the margin of error is the product of the critical value

20
00:01:03,755 --> 00:01:06,088
which is associated with the z-score

21
00:01:06,133 --> 00:01:10,622
for our  desired level of confidence and the standard error of the statistic,

22
00:01:10,640 --> 00:01:13,488
to find the critical Z value subtract

23
00:01:13,511 --> 00:01:15,755
to desired level of confidence from one

24
00:01:15,844 --> 00:01:20,466
for 95% confidence interval this would be 5%.

25
00:01:20,555 --> 00:01:22,822
This is the percentage of confidence intervals

26
00:01:22,866 --> 00:01:25,088
based on the various samples that could be drawn

27
00:01:25,133 --> 00:01:28,311
from the population that would not contain the population parameter,

28
00:01:28,350 --> 00:01:31,377
for a 95% confidence interval

29
00:01:31,377 --> 00:01:34,800
this means that 5% percent of those confidence interval would not

30
00:01:34,888 --> 00:01:36,822
contain the population parameter

31
00:01:36,933 --> 00:01:41,200
you divide that result by two because we are estimating

32
00:01:41,288 --> 00:01:43,155
an interval around a center point

33
00:01:43,220 --> 00:01:45,711
so some of the error needs to be above that value

34
00:01:45,755 --> 00:01:47,530
and some needs to be below.

35
00:01:47,600 --> 00:01:50,333
in this example it's 2.5 %

36
00:01:50,422 --> 00:01:52,800
in terms of the standard normal distribution

37
00:01:52,888 --> 00:01:54,960
this means that we're looking for a z-score where

38
00:01:54,960 --> 00:01:59,022
2.5 percent of the area under the curve is above that value

39
00:01:59,044 --> 00:02:01,466
and 2.5 percent is below it.

40
00:02:01,488 --> 00:02:03,710
It turns out that 95 percent of

41
00:02:03,719 --> 00:02:06,570
the area under a curve is between

42
00:02:06,570 --> 00:02:10,377
z-score of + and - 1.96

43
00:02:10,444 --> 00:02:13,511
which is slightly less than 2  this is why I became

44
00:02:13,533 --> 00:02:18,066
so precise in my numbers when discussing z-scores in the previous module

45
00:02:18,133 --> 00:02:21,688
and when introducing the standard normal curve earlier in this module,

46
00:02:21,733 --> 00:02:24,780
you can certainly use 2 as your critical value

47
00:02:24,780 --> 00:02:26,940
but it will actually result in a

48
00:02:26,940 --> 00:02:31,133
confidence interval that is slightly bigger than 95%.

