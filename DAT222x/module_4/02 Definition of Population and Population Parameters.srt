0
00:00:00,950 --> 00:00:03,230
In this video we're gonna discuss populations and

1
00:00:03,230 --> 00:00:04,970
population parameters.

2
00:00:04,970 --> 00:00:07,650
So a population is the collection of all objects of interest,

3
00:00:07,650 --> 00:00:09,970
so here are some examples of populations.

4
00:00:09,970 --> 00:00:13,510
All voters registered for a US Presidential election.

5
00:00:13,510 --> 00:00:15,970
All Americans who have a CPA or

6
00:00:15,970 --> 00:00:18,780
Certified Public Accountant certification.

7
00:00:18,780 --> 00:00:20,760
All the cows in India.

8
00:00:20,760 --> 00:00:24,000
All the customers shopping in a department store on a given day.

9
00:00:24,000 --> 00:00:27,560
All the computer chips produced this month at a semiconductor plant.

10
00:00:27,560 --> 00:00:30,490
All the families in, let's say, Houston, Texas.

11
00:00:30,490 --> 00:00:33,320
So, often we wanna estimate a numerical characteristic of

12
00:00:33,320 --> 00:00:34,520
a population.

13
00:00:34,520 --> 00:00:36,380
These are called population parameters.

14
00:00:36,380 --> 00:00:37,790
Here are some examples.

15
00:00:37,790 --> 00:00:38,410
The fraction of

16
00:00:38,410 --> 00:00:41,610
voters that prefer the Democratic candidate for president.

17
00:00:41,610 --> 00:00:43,090
The average age of all CPA's.

18
00:00:43,090 --> 00:00:46,852
The average weight of all the cows in India.

19
00:00:46,852 --> 00:00:50,741
The standard deviation of the amount spent by a department store customer

20
00:00:50,741 --> 00:00:51,563
on a given day.

21
00:00:51,563 --> 00:00:53,348
The fraction of all computer chips that are defective.

22
00:00:53,348 --> 00:00:57,980
The median income of the families in Houston Texas.

23
00:00:57,980 --> 00:01:00,420
Now in the next video we'll discuss samples and

24
00:01:00,420 --> 00:01:04,500
sample statistics which are used to estimate population perimeters.

