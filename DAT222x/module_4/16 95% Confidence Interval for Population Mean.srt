0
00:00:00,960 --> 00:00:03,570
In this video, we'll discuss one of the most important

1
00:00:03,570 --> 00:00:07,340
formulas in statistics, finding a 95% confidence interval for

2
00:00:07,340 --> 00:00:08,930
the mean of a population.

3
00:00:08,930 --> 00:00:11,300
We don't know the population mean mu.

4
00:00:11,300 --> 00:00:15,200
So we take a sample, let's say the sample's size is at least 30.

5
00:00:15,200 --> 00:00:16,770
Now, why at least 30?

6
00:00:16,770 --> 00:00:19,120
Because of the central limit theorem.

7
00:00:19,120 --> 00:00:21,712
We know that x bar will be normally distributed.

8
00:00:21,712 --> 00:00:23,610
It's adding up random variables.

9
00:00:23,610 --> 00:00:26,560
Even if the individual population observations are not

10
00:00:26,560 --> 00:00:28,210
normally distributed.

11
00:00:28,210 --> 00:00:32,555
So then by some mathematical manipulations we have in the notes,

12
00:00:32,555 --> 00:00:34,925
the 95% confidence interval for

13
00:00:34,925 --> 00:00:37,693
the population mean is centered at x bar.

14
00:00:37,693 --> 00:00:41,425
We go 1.96, that's the 2 and a half percentile, half of 5% for

15
00:00:41,425 --> 00:00:45,098
the standard normal that we just talked about, times the population

16
00:00:45,098 --> 00:00:49,470
standard deviation, divided by the square root of the sample size.

17
00:00:49,470 --> 00:00:51,580
And then we just change the minus to a plus.

18
00:00:51,580 --> 00:00:55,890
We take the sample mean plus 1.96 times the population standard

19
00:00:55,890 --> 00:00:59,010
deviation divided by the square root of the sample size.

20
00:00:59,010 --> 00:01:01,250
So what are some properties of this confidence interval?

21
00:01:01,250 --> 00:01:02,987
It's centered at x bar.

22
00:01:02,987 --> 00:01:06,750
Because the minus and pluses, if you averaged them, would cancel out.

23
00:01:06,750 --> 00:01:10,001
And as the sample size gets bigger, the width of this interval shrinks,

24
00:01:10,001 --> 00:01:12,684
cuz you're dividing by the square root of the sample size.

25
00:01:12,684 --> 00:01:14,860
As the standard deviation gets bigger,

26
00:01:14,860 --> 00:01:18,252
basically the width of the confidence interval would get wider

27
00:01:18,252 --> 00:01:20,710
because the sigma is in the numerator.

28
00:01:20,710 --> 00:01:24,110
Now, what do we do if we don't know the population standard deviation?

29
00:01:24,110 --> 00:01:27,120
We plug in the sample standard deviation.

30
00:01:27,120 --> 00:01:30,255
So let's do an example of this very important formula.

31
00:01:30,255 --> 00:01:34,551
You're told the standard deviation of invoice values is $500.

32
00:01:34,551 --> 00:01:39,110
A sample of 100 invoices taken from a large population of invoices.

33
00:01:39,110 --> 00:01:41,910
Now, why do I say a large population of invoices?

34
00:01:41,910 --> 00:01:44,991
Cuz suppose there were only 100 invoices in the population.

35
00:01:44,991 --> 00:01:49,757
If you took a sample size of 100 you would exactly know the mean

36
00:01:49,757 --> 00:01:50,910
invoice size.

37
00:01:50,910 --> 00:01:54,110
And we'll talk more about finite correction factor in a couple of

38
00:01:54,110 --> 00:01:55,170
videos from now.

39
00:01:55,170 --> 00:01:58,727
But let's suppose we have a sample mean in those 100 invoices of

40
00:01:58,727 --> 00:01:59,626
$4,500.

41
00:01:59,626 --> 00:02:03,090
Then you're 95% sure the mean size of an invoice is between blank

42
00:02:03,090 --> 00:02:04,380
and blank.

43
00:02:04,380 --> 00:02:07,308
So basically our sample mean is 4,500.

44
00:02:10,110 --> 00:02:11,488
Make this a little bigger.

45
00:02:15,700 --> 00:02:20,168
The population standard deviation, reading the problem,

46
00:02:20,168 --> 00:02:22,101
is supposedly $500.

47
00:02:22,101 --> 00:02:24,134
The sample size is 100.

48
00:02:25,680 --> 00:02:28,308
And we know that the 2.5 percentile,

49
00:02:28,308 --> 00:02:31,492
we know that's -1.96 from our last video.

50
00:02:31,492 --> 00:02:34,087
The 97.5 percentile, 1.96, and

51
00:02:34,087 --> 00:02:36,542
that's where these numbers come from.

52
00:02:36,542 --> 00:02:39,420
Now, what would be the lower limit on the confidence interval?

53
00:02:39,420 --> 00:02:41,180
Just plug in this formula.

54
00:02:41,180 --> 00:02:47,654
So what we would do is, we would take the sample mean right there,

55
00:02:47,654 --> 00:02:52,601
minus 1.96, times the standard deviation,

56
00:02:52,601 --> 00:02:56,386
divided by the square root of 100.

57
00:02:59,454 --> 00:03:08,990
And that's 4402, if you want to see that formula, There it is.

58
00:03:08,990 --> 00:03:13,135
And then I could just change the minus to a plus or I could, say,

59
00:03:13,135 --> 00:03:14,632
take the sample mean.

60
00:03:17,698 --> 00:03:24,844
Plus 1.96 times the population standard deviation,

61
00:03:24,844 --> 00:03:29,518
divided by the square root of 100.

62
00:03:29,518 --> 00:03:32,764
And so we're 95% sure that basically,

63
00:03:32,764 --> 00:03:37,010
the population mean is between 4402 and 4598.

64
00:03:37,010 --> 00:03:39,795
Now, if you're an inquisitive person, you might say,

65
00:03:39,795 --> 00:03:41,601
that doesn't make that much sense.

66
00:03:41,601 --> 00:03:45,530
Because either the population mean is in that interval, or it's not.

67
00:03:45,530 --> 00:03:49,499
In the next video, we'll make more precise the interpretation of a 95%

68
00:03:49,499 --> 00:03:50,763
confidence interval.

