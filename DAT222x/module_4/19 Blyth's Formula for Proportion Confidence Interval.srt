0
00:00:01,000 --> 00:00:04,496
In the last video we computed the Confidence Interval

1
00:00:04,520 --> 00:00:07,200
for a population proportion. But there is a slight problem

2
00:00:07,230 --> 00:00:09,312
that can occur that we did not discuss.

3
00:00:09,340 --> 00:00:14,016
What happens if on every trial you get success or failure

4
00:00:14,040 --> 00:00:17,200
then the Confidence Interval formula breaks down. So here's the

5
00:00:17,230 --> 00:00:19,440
formula you might recall from the last video here.

6
00:00:19,470 --> 00:00:23,699
Okay! So if you look at the second term here if every trials of

7
00:00:23,720 --> 00:00:28,704
success Phat is equal to 1 and then one times 1-1 is 0

8
00:00:28,752 --> 00:00:32,650
or if every trial is a failure Phat is 0,

9
00:00:32,680 --> 00:00:36,150
when you get a 0 here and the confidence of all shrinks to a single point

10
00:00:36,180 --> 00:00:37,584
but that doesn't make sense.

11
00:00:37,610 --> 00:00:41,040
So fortunately the formula for Blyth's Confidence Interval

12
00:00:41,070 --> 00:00:44,688
solves this problem, all you have to do is put in the number of successes

13
00:00:44,710 --> 00:00:49,072
you've seen every trial being a success and put in your level of confidence

14
00:00:49,100 --> 00:00:53,344
0.05 for 95%, 0.01 for 99% et cetra.

15
00:00:53,370 --> 00:00:57,152
And then basically the spreadsheet computes the Confidence Interval for you.

16
00:00:57,180 --> 00:01:00,080
Ok! You don't have to worry about how the formulas

17
00:01:00,110 --> 00:01:04,100
are derived but you just have to use the formula. So here's an example

18
00:01:04,130 --> 00:01:08,480
my son is driven to work 500 times thankfully without an accident

19
00:01:08,510 --> 00:01:10,640
so he's going to drive to work tomorrow.

20
00:01:10,670 --> 00:01:14,250
So I'm 95% sure the chance that it doesn't have an accident

21
00:01:14,280 --> 00:01:18,300
between blank and blank. So success is no accident and we had 500

22
00:01:18,330 --> 00:01:24,210
successes there. Ok! And so all we have to do is put in 500 here in other words it

23
00:01:24,240 --> 00:01:29,820
just said 100, whatever it said you just change it to the current situation

24
00:01:29,850 --> 00:01:33,344
95% Confidence Interval this is 0.05

25
00:01:33,370 --> 00:01:38,448
and then there's your Confidence Interval right there between 0.994 and 1.

26
00:01:38,470 --> 00:01:42,608
So we're 95% sure that the chance he doesn't have an accident tomorrow's

27
00:01:42,630 --> 00:01:44,729
between 0.994 and 1.

28
00:01:44,750 --> 00:01:48,432
Now we could turn this around and basically say what's the chance of

29
00:01:48,460 --> 00:01:53,296
having an accident. So if we let a success be an accident even though that

30
00:01:53,320 --> 00:01:58,649
seems a bit perverted. Ok! We had zero successes ok and 500 trials

31
00:01:58,670 --> 00:02:00,544
because he had no accidents thankfully.

32
00:02:00,570 --> 00:02:03,770
So we go right here and this would be our Confidence Interval

33
00:02:03,800 --> 00:02:09,200
okay, so we're 95% sure the chance he has an accident tomorrow

34
00:02:09,230 --> 00:02:12,950
is between 0 and 0.006.

35
00:02:12,980 --> 00:02:17,030
So what this video shows you how to do is resolve the problem basically you

36
00:02:17,060 --> 00:02:22,350
have every trial in your sample estimating a population proportion

37
00:02:22,380 --> 00:02:24,400
result in success or failure.

38
00:02:24,430 --> 00:02:28,160
Now in the next video we'll begin teaching you how to determine

39
00:02:28,190 --> 00:02:30,416
what's the appropriate sample size to

40
00:02:30,832 --> 00:02:33,500
come up with a desired level of Confidence.

