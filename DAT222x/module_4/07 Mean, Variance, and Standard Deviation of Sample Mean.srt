0
00:00:00,470 --> 00:00:03,210
Sample statistics are point estimates of

1
00:00:03,230 --> 00:00:05,310
population parameters to find the

2
00:00:05,330 --> 00:00:07,020
unknown mean of a population we

3
00:00:07,040 --> 00:00:10,110
calculate X-bar. We find this mean using

4
00:00:10,130 --> 00:00:11,990
the calculations we discussed in Module 01.

5
00:00:12,010 --> 00:00:15,000
The sample Mean is an unbiased

6
00:00:15,020 --> 00:00:17,910
estimate of MU, in other words if we

7
00:00:17,930 --> 00:00:20,100
average the x-bar values obtained from

8
00:00:20,120 --> 00:00:21,810
our multiple samples pulled from our

9
00:00:21,830 --> 00:00:24,359
population we should get mu this can

10
00:00:24,370 --> 00:00:26,910
actually be demonstrated mathematically,

11
00:00:26,930 --> 00:00:29,760
the expected value of x-bar is the sum

12
00:00:29,780 --> 00:00:31,619
of the expected value of sample one

13
00:00:31,630 --> 00:00:33,359
divided by its sample size plus the

14
00:00:33,370 --> 00:00:35,550
expected value of sample two divided by

15
00:00:35,570 --> 00:00:38,579
its sample size and so on. If we multiply

16
00:00:38,590 --> 00:00:41,100
that value by n we have mathematically

17
00:00:41,120 --> 00:00:43,530
demonstrated that x-bar is an unbiased

18
00:00:43,550 --> 00:00:46,440
estimate of MU, remember I told you that

19
00:00:46,460 --> 00:00:48,899
expected values were important this is

20
00:00:48,910 --> 00:00:51,569
why. It's the basis of our ability to say

21
00:00:51,580 --> 00:00:55,079
x-bar is a good estimation of MU, we can

22
00:00:55,090 --> 00:00:57,059
do the same for the sample variance.

23
00:00:57,070 --> 00:00:59,940
Recall that the variance is of the sum

24
00:00:59,960 --> 00:01:02,069
is found by summing the variances of

25
00:01:02,080 --> 00:01:04,229
each element that went into calculating

26
00:01:04,240 --> 00:01:07,049
that sum. However notice that the

27
00:01:07,060 --> 00:01:08,820
expected variance of a sampling

28
00:01:08,840 --> 00:01:11,159
distribution of the mean is equivalent

29
00:01:11,170 --> 00:01:13,320
to the population variance divided by

30
00:01:13,340 --> 00:01:15,740
the sample size because the mean is

31
00:01:15,760 --> 00:01:19,530
1/n th times the sum as we just demonstrated,

32
00:01:19,550 --> 00:01:22,500
thus when we take the square root of the

33
00:01:22,520 --> 00:01:24,539
expected variance to obtain the standard

34
00:01:24,550 --> 00:01:26,969
deviation we end up with the standard

35
00:01:26,980 --> 00:01:28,859
error which is the standard deviation

36
00:01:28,870 --> 00:01:31,420
divided by the square root of the sample size.

37
00:01:31,440 --> 00:01:34,350
Now you may be wondering why we

38
00:01:34,370 --> 00:01:37,229
divide by n - 1 well it turns out

39
00:01:37,240 --> 00:01:39,780
that by subtracting 1 we end up with an

40
00:01:39,800 --> 00:01:41,460
unbiased estimate of the population

41
00:01:41,480 --> 00:01:44,789
variance. If we don't do this we are

42
00:01:44,800 --> 00:01:46,920
slightly under estimating the variance

43
00:01:46,940 --> 00:01:49,109
of the population because the observed

44
00:01:49,120 --> 00:01:52,350
values fall on average closer to the

45
00:01:52,370 --> 00:01:54,539
sample mean than they do the population

46
00:01:54,550 --> 00:01:58,170
mean, using n * 1, n - 1 instead of n as

47
00:01:58,190 --> 00:02:00,539
the divisor corrects that for that bias

48
00:02:00,550 --> 00:02:05,380
by making the result just a little bit bigger.

