0
00:00:00,480 --> 00:00:04,460
Suppose we wanna estimate an unknown population proportion P for

1
00:00:04,460 --> 00:00:08,070
example we might let P be the fraction of registered voters

2
00:00:08,070 --> 00:00:10,530
in Seattle Washington or Independents.

3
00:00:10,530 --> 00:00:14,860
To estimate that unknown proportion what might we do?

4
00:00:14,860 --> 00:00:17,890
We might ask n randomly chosen Seattle voters if they

5
00:00:17,890 --> 00:00:18,990
are independents.

6
00:00:18,990 --> 00:00:21,540
Then what would be the logical way to estimate the fraction of

7
00:00:21,540 --> 00:00:22,640
independents?

8
00:00:22,640 --> 00:00:25,260
Take the number of independents in the sample divided by

9
00:00:25,260 --> 00:00:26,750
the sample size.

10
00:00:26,750 --> 00:00:30,230
So for example if a 100 of 400 voters in the sample say they're

11
00:00:30,230 --> 00:00:34,170
independents we would estimate p by what we call Phat, okay?

12
00:00:34,170 --> 00:00:37,600
This Phat is the estimator from the sample.

13
00:00:37,600 --> 00:00:40,648
It's the fraction in the sample with the attribute divided by

14
00:00:40,648 --> 00:00:41,533
the sample size.

15
00:00:41,533 --> 00:00:45,571
So, in this case, Phat would be 100 out of 400, or 0.25.

16
00:00:45,571 --> 00:00:48,787
Now, of course, that may not equal the population mean because if we

17
00:00:48,787 --> 00:00:52,700
looked at another sample 400 voters, we get a different value of p hat.

18
00:00:52,700 --> 00:00:54,800
So p hat is a random variable.

19
00:00:54,800 --> 00:00:58,500
But you can show that on the average P hat comes out to the population

20
00:00:58,500 --> 00:00:59,830
proportion.

21
00:00:59,830 --> 00:01:04,260
And the standard deviation of p hat would be the square root of

22
00:01:04,260 --> 00:01:08,630
the unknown proportion p times 1 minus the unknown proportion p,

23
00:01:08,630 --> 00:01:10,580
divided by the sample size.

24
00:01:10,580 --> 00:01:12,960
But the problem is we don't know p.

25
00:01:12,960 --> 00:01:14,960
Okay, because that's what we're trying to estimate.

26
00:01:14,960 --> 00:01:18,630
So, if we want to get the standard deviation of phat, what can we do?

27
00:01:18,630 --> 00:01:21,970
Well, what we do is we just plug in phat from the sample, and

28
00:01:21,970 --> 00:01:24,570
assume it's p to get the standard deviation.

29
00:01:24,570 --> 00:01:27,400
There isn't really much else we can do about that.

30
00:01:27,400 --> 00:01:30,680
Now, this here is called the Standard Deviation of phat or

31
00:01:30,680 --> 00:01:31,920
the standard error of phat.

32
00:01:33,440 --> 00:01:36,580
And so phat, again, is an unbiased estimator of the population

33
00:01:36,580 --> 00:01:41,340
proportion p, and it can be shown, phat has the smallest variance of

34
00:01:41,340 --> 00:01:45,450
all the estimators are unbiased we can come up with a p.

35
00:01:45,450 --> 00:01:47,570
So we'll use phat as a point estimate for

36
00:01:47,570 --> 00:01:52,310
p, just like we'll use xbar as a point estimate from you.

37
00:01:52,310 --> 00:01:56,340
Now the problem is that basically, every time we get

38
00:01:56,340 --> 00:01:59,716
a different sample, we get a different p-hat or x-bar.

39
00:01:59,716 --> 00:02:02,280
So we're going to have to talk about, in the subsequent videos,

40
00:02:02,280 --> 00:02:05,650
about interval estimation of parameters.

41
00:02:05,650 --> 00:02:07,620
But one more example, here.

42
00:02:07,620 --> 00:02:12,050
Okay, we have a sample of 600 of 1500 United Kingdom adults who

43
00:02:12,050 --> 00:02:14,100
are in favor of driverless cars.

44
00:02:14,100 --> 00:02:17,700
How would you estimate the fraction of United Kingdom adults in favor

45
00:02:17,700 --> 00:02:18,710
of driverless cars?

46
00:02:18,710 --> 00:02:23,813
Well, you'd say 600 over 1500, which is the phat from the sample, 0.4.

47
00:02:23,813 --> 00:02:26,990
What would be the standard deviation of phat?

48
00:02:26,990 --> 00:02:31,278
Well, you would simply take the square root of the fraction in

49
00:02:31,278 --> 00:02:35,155
the sample that said they believed in driverless cars,

50
00:02:35,155 --> 00:02:40,617
0.4 times 1 minus that 0.6, divided by the sample size of 1,500.

51
00:02:40,617 --> 00:02:44,643
So that standard deviation of P hat is about 1.2%.

52
00:02:44,643 --> 00:02:48,051
So again we'll need to talk about interval estimation of

53
00:02:48,051 --> 00:02:51,248
the population mean and the population proportion.

54
00:02:51,248 --> 00:02:53,300
And that'll be what we'll talk about next.

