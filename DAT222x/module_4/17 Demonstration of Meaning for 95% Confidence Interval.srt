0
00:00:00,810 --> 00:00:05,650
So in the last video, we talked about a fairly complex issue.

1
00:00:05,650 --> 00:00:09,890
When you compute a 95% confidence interval for a population mean,

2
00:00:09,890 --> 00:00:12,700
either the population mean is in that interval or it's not.

3
00:00:12,700 --> 00:00:14,900
So how does the 95% come in?

4
00:00:14,900 --> 00:00:15,980
Here's how.

5
00:00:15,980 --> 00:00:19,920
The true interpretation of a 95% confidence interval is as follows.

6
00:00:19,920 --> 00:00:22,134
You take 100 samples, and for

7
00:00:22,134 --> 00:00:26,510
each sample you compute the 95% confidence interval.

8
00:00:26,510 --> 00:00:30,710
Around 95 of those samples will contain the population mean.

9
00:00:30,710 --> 00:00:36,162
So I've tried to replicate that idea in this file here.

10
00:00:36,162 --> 00:00:38,960
So I took 36 IQs.

11
00:00:38,960 --> 00:00:41,877
So this norm inverse random, don't worry too much about it,

12
00:00:41,877 --> 00:00:43,610
it generates random IQs.

13
00:00:43,610 --> 00:00:47,130
If I hit F9, basically those numbers change.

14
00:00:47,130 --> 00:00:53,380
Okay, so there's a sample of 36 IQs in each row and I've got 100 rows.

15
00:00:53,380 --> 00:00:57,700
And so basically based on those 36 IQs,

16
00:00:57,700 --> 00:01:01,190
I could have a sample mean, x bar,

17
00:01:01,190 --> 00:01:04,670
a sample standard deviation from the standard deviation function.

18
00:01:04,670 --> 00:01:05,450
And then I can use

19
00:01:05,450 --> 00:01:08,270
the confidence interval formula from the last video.

20
00:01:08,270 --> 00:01:11,050
So I would take the lower limit of the confidence

21
00:01:11,050 --> 00:01:13,984
interval as the sample mean for the sample of 36 IQs,

22
00:01:13,984 --> 00:01:19,190
minus 1.96 times the sample standard deviation from the 36 IQs,

23
00:01:19,190 --> 00:01:21,900
divided by the square root of the sample size, 36.

24
00:01:21,900 --> 00:01:25,190
For the upper limit I changed the minus to a plus and

25
00:01:25,190 --> 00:01:28,420
then with the word yes, that indicates that the true

26
00:01:28,420 --> 00:01:33,090
IQ value which is a true mean IQ, which is 100 is in the interval.

27
00:01:33,090 --> 00:01:35,980
And basically, the word no indicates it's not, so

28
00:01:35,980 --> 00:01:37,210
there should be some no's here.

29
00:01:38,320 --> 00:01:39,400
There is a no.

30
00:01:39,400 --> 00:01:43,530
So in other words, our confidence symbol here went 94, 88 to 99,

31
00:01:43,530 --> 00:01:45,299
which did not include 100.

32
00:01:45,299 --> 00:01:48,898
So I've counted up here how many of the 100 intervals will actually

33
00:01:48,898 --> 00:01:51,690
include the mean of 100, the true mean?

34
00:01:51,690 --> 00:01:54,929
And if I hit F9 you'll see it's almost always close to 95.

35
00:01:54,929 --> 00:01:56,193
There it's 95,

36
00:01:56,193 --> 00:02:01,490
97, 93 so that's the true meaning of a 95% confidence interval.

37
00:02:01,490 --> 00:02:04,880
If you would take your samples 100 times and

38
00:02:04,880 --> 00:02:08,287
do 100 different 95% confidence intervals,

39
00:02:08,287 --> 00:02:12,360
then roughly 95 times out of 100, the true value of the population

40
00:02:12,360 --> 00:02:15,540
mean would be in the confidence interval you construct.

41
00:02:15,540 --> 00:02:20,470
Although, for one single interval either the population mean is there.

42
00:02:20,470 --> 00:02:23,890
When it says yes here or it's not there when you say no.

43
00:02:23,890 --> 00:02:25,129
Now, on our next video,

44
00:02:25,129 --> 00:02:28,566
we\re start talking about confidence intervals for proportions.

