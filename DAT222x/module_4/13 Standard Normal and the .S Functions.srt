0
00:00:00,900 --> 00:00:02,610
In our study of confidence intervals and

1
00:00:02,610 --> 00:00:05,950
hypothesis testing, we'll have to make a lot of use of what's called

2
00:00:05,950 --> 00:00:08,140
the standard normal random variable.

3
00:00:08,140 --> 00:00:10,260
So, what is a standard normal random variable?

4
00:00:10,260 --> 00:00:11,580
It's often called Z,

5
00:00:11,580 --> 00:00:15,340
like in the Z scores we discussed in the last module.

6
00:00:15,340 --> 00:00:18,317
The standard normal is a normal random variable with a mean of 0 and

7
00:00:18,317 --> 00:00:19,606
a standard deviation of 1.

8
00:00:19,606 --> 00:00:22,546
And what we'll need to be able to find easily are the percentiles of

9
00:00:22,546 --> 00:00:24,100
such a random variable.

10
00:00:24,100 --> 00:00:26,850
So we could do that with NORM.INV, but there's an easier way.

11
00:00:26,850 --> 00:00:30,920
There's a function NORM.S, for standardized, .INV.

12
00:00:30,920 --> 00:00:33,770
So if we want to know the 2.5 percentile, in other words

13
00:00:33,770 --> 00:00:37,970
the chance that this normal random variable, 2.5% chance the normal

14
00:00:37,970 --> 00:00:40,780
random variable that's standardized is less or equal to what?

15
00:00:40,780 --> 00:00:47,070
We could do NORM.INV(0.025), mean of 0, standard deviation of 1.

16
00:00:47,070 --> 00:00:48,772
We get -1.96.

17
00:00:48,772 --> 00:00:54,419
Or we could just do NORM.S.INV and do 0.025.

18
00:00:54,419 --> 00:00:59,780
The 97.5 percentile, by symmetry, should be 1.96, which it is.

19
00:00:59,780 --> 00:01:00,886
Let's do some examples.

20
00:01:00,886 --> 00:01:06,286
The 95th percentile, if I wanted to do that for a standard normal,

21
00:01:06,286 --> 00:01:11,142
I could use that NORM.INV, okay, and I could say 0.95.

22
00:01:13,100 --> 00:01:16,850
And I could say mean 0, standard deviation 1.

23
00:01:16,850 --> 00:01:19,487
And I would get 1.645 there.

24
00:01:21,882 --> 00:01:24,590
We can copy with formula text those formulas down.

25
00:01:26,170 --> 00:01:29,532
And if I wanted to use that NORM.S, which is a little bit easier.

26
00:01:32,407 --> 00:01:36,714
And then I could say the 95th percentile, 0.95.

27
00:01:39,580 --> 00:01:40,980
That gives the same answer.

28
00:01:40,980 --> 00:01:42,580
Now if I want the 5th percentile,

29
00:01:42,580 --> 00:01:45,040
it's gonna come out to minus these numbers by symmetry.

30
00:01:46,880 --> 00:01:50,280
But if I would say NORM.INV, okay, and

31
00:01:50,280 --> 00:01:53,090
I would say I want the 5th percentile.

32
00:01:53,090 --> 00:02:00,240
And the mean is going to be 0, the standard deviation 1, -1.64.

33
00:02:00,240 --> 00:02:05,504
And basically if I would do that with the NORM.S.INV,

34
00:02:05,504 --> 00:02:08,306
okay, I could say 0.05.

35
00:02:09,868 --> 00:02:11,700
Should get the same thing.

36
00:02:11,700 --> 00:02:16,615
And these numbers, 1.96, 1.64, -1.96,

37
00:02:16,615 --> 00:02:21,027
-1.64 will pop, -1.645 to be precise,

38
00:02:21,027 --> 00:02:26,840
these will pop up a lot in the remaining topics in this course.

39
00:02:26,840 --> 00:02:32,100
Now, in our next video, we'll use the standard normal to show you how

40
00:02:32,100 --> 00:02:35,680
to find a 95% confidence interval for the population mean

41
00:02:35,680 --> 00:02:38,590
based on a sample, which is a very, very important formula.

