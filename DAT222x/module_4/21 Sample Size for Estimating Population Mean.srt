0
00:00:00,610 --> 00:00:03,290
So often you wanna know how big a sample

1
00:00:03,290 --> 00:00:06,440
size you need to get a desired level of accuracy.

2
00:00:06,440 --> 00:00:10,950
So let's first approach the problem of what sample size we need to

3
00:00:10,950 --> 00:00:14,360
get a desired level of accuracy when we're estimating a population mean.

4
00:00:15,390 --> 00:00:19,150
And the formula derived in the notes is, basically, your sample size need

5
00:00:19,150 --> 00:00:24,290
is 1.96 times the standard deviation, assuming it's known,

6
00:00:24,290 --> 00:00:28,430
divided by the level of error you're going to accept, squared.

7
00:00:28,430 --> 00:00:30,230
Now does this formula make sense?

8
00:00:30,230 --> 00:00:32,620
As the standard deviation gets bigger,

9
00:00:32,620 --> 00:00:35,710
you have more uncertainty and the sample size goes up.

10
00:00:35,710 --> 00:00:37,450
As the error drops,

11
00:00:37,450 --> 00:00:40,446
the desired error drops, it's in the denominator, so

12
00:00:40,446 --> 00:00:44,330
a smaller value of e would also result in a bigger sample size.

13
00:00:44,330 --> 00:00:48,010
So let's use this formula in an example, okay.

14
00:00:48,010 --> 00:00:51,160
Suppose we know the standard deviation of a large population of

15
00:00:51,160 --> 00:00:52,420
uncashed checks.

16
00:00:52,420 --> 00:00:54,400
Now, again, why do we say large?

17
00:00:54,400 --> 00:00:56,960
Because, basically, if we got a sample size of 100, and

18
00:00:56,960 --> 00:01:00,520
the population is 50, that wouldn't make any sense.

19
00:01:00,520 --> 00:01:04,484
So we have to assume, basically, the rule we use is, if the sample size

20
00:01:04,484 --> 00:01:08,517
comes out bigger than 10% of the population size, we need a different

21
00:01:08,517 --> 00:01:12,573
formula for sample size, which we'll come up with later in the module.

22
00:01:12,573 --> 00:01:15,936
Okay, suppose we know the standard deviation of a large population of

23
00:01:15,936 --> 00:01:19,532
uncashed checks has a standard deviation of 100, that's your sigma.

24
00:01:19,532 --> 00:01:23,621
We wanna be 95% sure the 1.96, basically,

25
00:01:23,621 --> 00:01:28,600
comes from the 95% using that standard normal idea.

26
00:01:28,600 --> 00:01:31,590
The average sized of an uncashed check we want to estimate

27
00:01:31,590 --> 00:01:32,360
within $20.

28
00:01:32,360 --> 00:01:35,570
What that means is the half width of the confidence interval should

29
00:01:35,570 --> 00:01:36,150
be $20.

30
00:01:36,150 --> 00:01:41,512
In other words, if we go X-bar plus or minus whatever, okay,

31
00:01:41,512 --> 00:01:47,298
that 1.96 thing, it should add up that plus or minus is $20.

32
00:01:47,298 --> 00:01:49,610
That's the margin of error, okay,

33
00:01:49,610 --> 00:01:53,422
so the question is what sample size do we need to ensure that?

34
00:01:53,422 --> 00:01:56,666
Well, all we do is plug in this formula.

35
00:01:56,666 --> 00:02:02,960
We take 1.96 times our standard deviation we assume we knew was 100.

36
00:02:02,960 --> 00:02:08,342
We want to put this all squared, so it's 1.96 times

37
00:02:08,342 --> 00:02:13,504
the standard deviation of 100, divided by 20.

38
00:02:13,504 --> 00:02:15,250
And now we square that whole thing.

39
00:02:18,984 --> 00:02:23,738
We get a sample size of 96.04, we could round up to 97.

40
00:02:23,738 --> 00:02:28,915
So that would mean we'd need to sample 97 checks to be 95% sure we

41
00:02:28,915 --> 00:02:33,931
can estimate the average size of an uncashed check within $20.

42
00:02:33,931 --> 00:02:36,490
Now, that population was, let's say,

43
00:02:36,490 --> 00:02:39,865
500 then the sample size would be more than 10%.

44
00:02:39,865 --> 00:02:42,040
We would need a different formula.

45
00:02:42,040 --> 00:02:44,920
Now, in the next video we'll talk about the sample

46
00:02:44,920 --> 00:02:48,120
size needed when you're estimating a population proportion.

47
00:02:48,120 --> 00:02:51,230
And that's the formula pollsters use because they're trying to estimate

48
00:02:51,230 --> 00:02:53,940
the fraction of voters who prefer a particular candidate.

