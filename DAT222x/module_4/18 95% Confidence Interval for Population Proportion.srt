0
00:00:00,720 --> 00:00:03,930
So in this video we are gonna discuss how to find a 95% confidence

1
00:00:03,930 --> 00:00:07,110
interval for an unknown population proportion.

2
00:00:07,110 --> 00:00:09,933
And this basically applies to basically what we've seen in

3
00:00:09,933 --> 00:00:12,933
presidential election polls of whatever country you live in.

4
00:00:12,933 --> 00:00:16,922
The polls, I'm sure, are taken in your country for

5
00:00:16,922 --> 00:00:18,742
the elective offices.

6
00:00:18,742 --> 00:00:22,265
And lots of countries have had important elections this year, or

7
00:00:22,265 --> 00:00:24,643
every year there are important elections.

8
00:00:24,643 --> 00:00:27,628
So 800 of 1,500 were sampled, let's say, and

9
00:00:27,628 --> 00:00:30,580
they preferred the Democrat for President.

10
00:00:30,580 --> 00:00:34,990
So let's get a 95% confidence interval for the true fraction of

11
00:00:34,990 --> 00:00:37,380
people who would prefer the Democrat for President.

12
00:00:37,380 --> 00:00:40,365
And we don't know that cuz we just sampled 1,500 people.

13
00:00:40,365 --> 00:00:44,173
Though it's quite amazing, whether you're dealing with your small town

14
00:00:44,173 --> 00:00:47,801
that may have 50,000 people, the city of Houston with 4 million

15
00:00:47,801 --> 00:00:51,308
people, or the city of New York with 8 million people, or Tokyo, or

16
00:00:51,308 --> 00:00:54,240
Mexico City with more than 8 million people.

17
00:00:54,240 --> 00:00:58,470
A sample size with 1,500 will give roughly the same degree of accuracy,

18
00:00:58,470 --> 00:01:00,020
that's quite amazing.

19
00:01:00,020 --> 00:01:03,410
The accuracy of a confidence interval

20
00:01:03,410 --> 00:01:05,210
doesn't depend on the population size,

21
00:01:05,210 --> 00:01:08,520
it really mostly depends on the size of the sample.

22
00:01:08,520 --> 00:01:12,080
So what would be our 95% confidence interval formula?

23
00:01:12,080 --> 00:01:13,680
It would be centered at phat,

24
00:01:13,680 --> 00:01:17,565
which is the fraction in the sample that say they're for the Democrat.

25
00:01:17,565 --> 00:01:21,638
And then we go minus 1.96 which is the two and a half percent of

26
00:01:21,638 --> 00:01:25,109
the standard normal, times the standard deviation,

27
00:01:25,109 --> 00:01:28,806
standard error of phat, which is phat times one minus phat

28
00:01:28,806 --> 00:01:31,911
divided by the sample size take the square root.

29
00:01:31,911 --> 00:01:34,415
Then we change the minus to a plus.

30
00:01:34,415 --> 00:01:37,150
So, let's go ahead and figure that out.

31
00:01:37,150 --> 00:01:41,971
Okay, so the sample size was 1,500.

32
00:01:41,971 --> 00:01:44,026
Now phat would be?

33
00:01:44,026 --> 00:01:45,828
Should make this a little bigger here.

34
00:01:48,982 --> 00:01:52,045
Phat would be the fraction in the sample who say they're for

35
00:01:52,045 --> 00:01:52,846
the Democrat.

36
00:01:52,846 --> 00:01:55,589
That would be 800 out of 1500.

37
00:01:56,970 --> 00:02:00,060
Now, the standard error of phat is gonna be this square

38
00:02:00,060 --> 00:02:01,510
root thing here.

39
00:02:01,510 --> 00:02:05,230
So, square root Excel is sqrt, and

40
00:02:05,230 --> 00:02:10,698
then we take our phat, we go one minus phat times it.

41
00:02:12,860 --> 00:02:15,767
And then we would divide by the sample size.

42
00:02:18,280 --> 00:02:21,115
And we get 1.3%.

43
00:02:21,115 --> 00:02:25,867
So, the lower limit to our confidence interval would be that

44
00:02:25,867 --> 00:02:29,036
phat, that's where we're centered,

45
00:02:29,036 --> 00:02:34,181
minus 1.96 times that standard error of phat, 50.8%.

46
00:02:34,181 --> 00:02:39,253
And the upper limit would be the phat,

47
00:02:39,253 --> 00:02:45,760
plus 1.96 times the standard error of phat.

48
00:02:47,490 --> 00:02:52,808
So essentially we are 95% sure that a 53% of the sample,

49
00:02:52,808 --> 00:02:58,180
in a sample size 1,500, preferred the Democrat.

50
00:02:58,180 --> 00:03:05,720
We're 95% sure that the true percentage is between 51% and 56%.

51
00:03:05,720 --> 00:03:09,677
Now, in the next video we'll talk about how we determine sample size.

52
00:03:09,677 --> 00:03:11,436
We'll sort of turn things on its head.

53
00:03:11,436 --> 00:03:13,560
All right, so we know how accurate we wanna be.

54
00:03:13,560 --> 00:03:15,223
How big a sample do we need?

