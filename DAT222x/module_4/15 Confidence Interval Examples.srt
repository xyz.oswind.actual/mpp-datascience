0
00:00:00,880 --> 00:00:05,510
Because the critical values of z-scores don't change it's an easy matter to

1
00:00:05,530 --> 00:00:08,816
remember that the critical value for 90% confidence intervals is

2
00:00:08,830 --> 00:00:16,672
1.645, for 95% it's 1.96 and for 99% it's 2.576.

3
00:00:16,690 --> 00:00:20,460
You multiply this value by the standard error of your sample statistic

4
00:00:20,480 --> 00:00:22,032
to obtain the margin of error.

5
00:00:22,050 --> 00:00:24,000
Margin of error is added to

6
00:00:24,000 --> 00:00:26,000
and subtracted from the sample statistic

7
00:00:26,020 --> 00:00:29,344
to provide the upper and lower values of your confidence interval.

8
00:00:29,360 --> 00:00:33,120
Let's take a look at an example using the P(hat) we obtained

9
00:00:33,140 --> 00:00:34,840
from our hiking example point 3

10
00:00:34,860 --> 00:00:36,840
with a confidence level of 95%.

11
00:00:37,860 --> 00:00:44,480
For P(hat) the margin of error will be 1.96 multiplied by point .0374

12
00:00:44,500 --> 00:00:47,296
which is the standard error we obtained in that lesson

13
00:00:47,310 --> 00:00:51,264
giving us a margin of error of .0733

14
00:00:51,280 --> 00:00:58,559
If we add that to point 3 we get 0.3733 for the upper end and if we subtract it

15
00:00:58,570 --> 00:01:03,440
from point three we see that the lower bound is 0.2267.

16
00:01:03,460 --> 00:01:08,490
That's our confidence interval for a P(hat) of 0.3. For fun

17
00:01:08,510 --> 00:01:10,380
and for x-bar let's say that the

18
00:01:10,400 --> 00:01:15,472
average number of miles hiked by those samples was 425.47

19
00:01:15,490 --> 00:01:18,528
with a standard deviation of 52.63

20
00:01:18,540 --> 00:01:23,719
We calculate the standard error 52.63 divided by the square root

21
00:01:23,730 --> 00:01:28,976
of 150 which was our sample size and we get a value of 4.30

22
00:01:28,990 --> 00:01:33,539
We multiply that by 1.96 and then add and subtract that value

23
00:01:33,550 --> 00:01:40,069
which is 8.42 from 425.47 to obtain a confidence interval

24
00:01:40,080 --> 00:01:45,472
of 417.05  and 433.89

25
00:01:45,490 --> 00:01:49,619
Finally I want to conclude by clarifying a common

26
00:01:49,630 --> 00:01:52,544
misperception about what a confidence interval is.

27
00:01:52,560 --> 00:01:57,008
There's a lot of confusion about this, many people think that they provide

28
00:01:57,020 --> 00:02:00,929
the level of confidence that a population parameter falls within the

29
00:02:00,940 --> 00:02:02,896
range provided by the interval.

30
00:02:02,910 --> 00:02:07,552
However they actually reflect the  level of confidence that we have

31
00:02:07,570 --> 00:02:09,632
in the sampling method.

32
00:02:09,650 --> 00:02:13,650
Remember that with each sample you draw from a population you will get

33
00:02:13,670 --> 00:02:16,368
slightly different means and variances.

34
00:02:16,380 --> 00:02:19,550
As a result some confidence intervals would include the

35
00:02:19,570 --> 00:02:22,576
true population parameter and some would not.

36
00:02:22,590 --> 00:02:25,744
A 90% confidence level means that we would expect

37
00:02:25,760 --> 00:02:29,680
90% of the intervals to include the population parameter.

38
00:02:29,700 --> 00:02:32,690
A 95% confidence level means

39
00:02:32,730 --> 00:02:34,690
that 95% of the intervals would include

40
00:02:35,270 --> 00:02:40,020
the parameter and soon, which is why higher levels of confidence lead to

41
00:02:40,040 --> 00:02:41,584
wider intervals.

