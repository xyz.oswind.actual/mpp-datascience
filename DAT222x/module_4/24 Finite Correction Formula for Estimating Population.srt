0
00:00:00,540 --> 00:00:04,280
If your sample size is more than 10% of your population size,

1
00:00:04,280 --> 00:00:08,441
you could actually shrink the width of your confidence intervals using

2
00:00:08,441 --> 00:00:11,785
something known as the finite correction factor, FCF.

3
00:00:11,785 --> 00:00:17,510
We could use as our Acronym there.

4
00:00:17,510 --> 00:00:19,380
And so basically, it's very simple.

5
00:00:19,380 --> 00:00:21,380
You just take the confidence interval you had for

6
00:00:21,380 --> 00:00:23,230
either the mean or proportion and

7
00:00:23,230 --> 00:00:27,160
then you multiply the part you subtract or add by the following.

8
00:00:27,160 --> 00:00:30,300
The square root of capital and the population size,

9
00:00:30,300 --> 00:00:34,540
minus the sample size, divided by the population size, minus one.

10
00:00:34,540 --> 00:00:37,480
Now does this have the properties that you'd expect?

11
00:00:37,480 --> 00:00:40,550
Suppose your sample size equals the population size.

12
00:00:40,550 --> 00:00:42,720
Well then the width of your confidence interval should be zero

13
00:00:42,720 --> 00:00:44,550
because you've sampled 100%.

14
00:00:44,550 --> 00:00:47,800
So if little n equals capital N, this becomes a zero and

15
00:00:47,800 --> 00:00:50,960
your confidence interval shrinks to the sample mean.

16
00:00:50,960 --> 00:00:52,330
Okay so it makes sense.

17
00:00:52,330 --> 00:00:54,210
You could always use this formula, but

18
00:00:54,210 --> 00:00:58,020
honestly if the sample size is less than 10% of the population size,

19
00:00:58,020 --> 00:00:59,690
people just don't worry about it.

20
00:00:59,690 --> 00:01:02,970
So suppose you're trying to come up with the average salary of Fortune

21
00:01:02,970 --> 00:01:04,210
500 CEOs.

22
00:01:04,210 --> 00:01:05,410
And, guess what, there's 500 of them.

23
00:01:06,480 --> 00:01:11,322
Okay, so you take a sample size of 100, which is 20% of the population.

24
00:01:11,322 --> 00:01:13,239
100 is 20% of 500.

25
00:01:14,310 --> 00:01:17,782
Suppose you know the standard deviation of the CEO salaries is $5

26
00:01:17,782 --> 00:01:21,387
million, or maybe your sample standard deviation is $5 million.

27
00:01:21,387 --> 00:01:24,480
100 observation should be enough to give you a good estimate.

28
00:01:24,480 --> 00:01:26,550
And the sample mean of the salary is 40 million.

29
00:01:27,770 --> 00:01:32,753
Let's get a 95% confidence interval for the mean salary of a Fortune 500

30
00:01:32,753 --> 00:01:37,227
CEO, and again we haven't talked to every one, just 20% of them.

31
00:01:37,227 --> 00:01:41,600
Okay, so we've got a sample size of 100, a population size of 500.

32
00:01:41,600 --> 00:01:46,105
We've got the standard deviation is 5 and the sample mean was 40.

33
00:01:46,105 --> 00:01:50,864
Okay, so basically we want the lower limit for a confidence interval,

34
00:01:50,864 --> 00:01:55,749
basically It would be what we have here without

35
00:01:55,749 --> 00:01:58,320
the finite correction factor.

36
00:01:58,320 --> 00:02:00,180
Okay, that's what we had before.

37
00:02:00,180 --> 00:02:02,770
That's the upper limit without the finite correction factor.

38
00:02:02,770 --> 00:02:05,850
And all we're gonna do is throw that finite correction factor

39
00:02:05,850 --> 00:02:06,980
into the formula.

40
00:02:06,980 --> 00:02:09,741
So let's get that finite correction factor.

41
00:02:09,741 --> 00:02:15,067
All right, so that finite correction factor would be the square

42
00:02:15,067 --> 00:02:20,800
root of the population size, 500, which I've named that cell.

43
00:02:20,800 --> 00:02:23,363
We talked about naming things in the first module.

44
00:02:23,363 --> 00:02:24,945
Minus 100.

45
00:02:28,136 --> 00:02:32,480
Then we divide by the population size -1)).

46
00:02:32,480 --> 00:02:37,279
We want the square root there.

47
00:02:37,279 --> 00:02:40,910
We get 0.89 as the finite correction factor.

48
00:02:40,910 --> 00:02:45,269
In other words we're shrinking the width of that confidence interval by

49
00:02:45,269 --> 00:02:46,845
1-0.89 or 11%.

50
00:02:46,845 --> 00:02:51,451
So our confidence interval would be =xbar minus the finite correction

51
00:02:51,451 --> 00:02:52,236
factor.

52
00:02:52,236 --> 00:02:57,571
Times that 1.96 times the standard deviation

53
00:02:57,571 --> 00:03:02,533
divided by the square root of our sample size.

54
00:03:10,321 --> 00:03:13,382
And that should be the lower limit of our confidence interval.

55
00:03:13,382 --> 00:03:15,087
That's 39.12.

56
00:03:15,087 --> 00:03:17,890
Then I can just change the minus to a plus.

57
00:03:17,890 --> 00:03:20,920
And a little tip, I could retype that formula, but

58
00:03:20,920 --> 00:03:24,680
it might be easier just to copy that with Ctrl+C, and

59
00:03:24,680 --> 00:03:28,739
do equals Ctrl+V, and paste it in, and just change the minus to a plus.

60
00:03:29,940 --> 00:03:32,100
I mean, I don't care, you could retype it, but

61
00:03:32,100 --> 00:03:33,680
that's a little bit easier.

62
00:03:33,680 --> 00:03:37,500
So that's our 95% confidence role with the finite correction factor.

63
00:03:37,500 --> 00:03:40,140
Now if you did it without the finite correction factor,

64
00:03:40,140 --> 00:03:44,560
your interval goes 39.02 to 40.98 which is a little bit wider.

65
00:03:44,560 --> 00:03:46,350
Not a big deal.

66
00:03:46,350 --> 00:03:49,440
So in the next video, which will conclude this module,

67
00:03:49,440 --> 00:03:53,454
we'll talk about how you modify the sample size formulas when basically

68
00:03:53,454 --> 00:03:55,853
you wanna use a finite correction factor.

