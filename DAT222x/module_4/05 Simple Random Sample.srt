0
00:00:00,540 --> 00:00:03,550
In this video, we'll discuss what a simple random sample is and

1
00:00:03,550 --> 00:00:07,340
how you can use Excel to take a simple random sample.

2
00:00:07,340 --> 00:00:10,580
So suppose a population has capital N individuals and

3
00:00:10,580 --> 00:00:13,520
we want to take a sample of size little n.

4
00:00:13,520 --> 00:00:17,238
When will the sample be a simple random sample of each set of little

5
00:00:17,238 --> 00:00:20,153
n individuals has the same chance of being chosen?

6
00:00:20,153 --> 00:00:24,352
So, for example, let capital N be 5, so our population has 5 elements,

7
00:00:24,352 --> 00:00:25,370
1 through 5.

8
00:00:25,370 --> 00:00:28,720
And little n is 2, so we take a sample of size 2.

9
00:00:28,720 --> 00:00:31,610
So what would define a simple random sample?

10
00:00:31,610 --> 00:00:34,610
If every possible combination

11
00:00:34,610 --> 00:00:37,880
of two items out of five had the same chance of being chosen.

12
00:00:37,880 --> 00:00:41,571
Below we've listed every possible combination of two items out

13
00:00:41,571 --> 00:00:42,124
of five.

14
00:00:42,124 --> 00:00:46,161
1 with 2, 1 with 3, 1 with 4, 1 with 5.

15
00:00:46,161 --> 00:00:52,107
2 with 3, 2 with 4, 2 with 5, 3 with 4, 3 with 5, 4 with 5.

16
00:00:52,107 --> 00:00:55,580
So there are ten possible ways to pick two items out of five.

17
00:00:55,580 --> 00:00:59,090
So if each of those combinations has a one tenth chance of occurring

18
00:00:59,090 --> 00:01:02,230
in your sampling, then you would have a simple random sample.

19
00:01:02,230 --> 00:01:05,260
So now it's easy to use Excel to take a simple random sample.

20
00:01:05,260 --> 00:01:06,440
Here's an example.

21
00:01:06,440 --> 00:01:08,690
We have a list of NBA players.

22
00:01:08,690 --> 00:01:11,470
And basically, suppose we want to take a random sample of ten

23
00:01:11,470 --> 00:01:13,010
NBA players.

24
00:01:13,010 --> 00:01:17,810
There's about 285 here, so how could we do that so that every

25
00:01:17,810 --> 00:01:21,380
combination of ten NBA players has the same chance of being chosen?

26
00:01:21,380 --> 00:01:23,620
Well, we'll use Excel's Rand function.

27
00:01:23,620 --> 00:01:26,754
If you type rand in a cell you're equally likely to get any number

28
00:01:26,754 --> 00:01:27,690
between 0 and 1.

29
00:01:29,820 --> 00:01:34,120
And if you copy that down, and I hit F9, even those numbers change.

30
00:01:34,120 --> 00:01:37,020
Now, what I wanna do is freeze those random numbers so

31
00:01:37,020 --> 00:01:38,430
that they don't change.

32
00:01:38,430 --> 00:01:42,342
And then if I sort on this column and I just assign as my sample

33
00:01:42,342 --> 00:01:46,490
the 10 players that have the 10 largest numbers in the row,

34
00:01:46,490 --> 00:01:49,070
I have a simple random sample.

35
00:01:49,070 --> 00:01:50,100
Because, basically,

36
00:01:50,100 --> 00:01:53,690
every combination of 10 players has the same chance of being chosen.

37
00:01:53,690 --> 00:01:55,710
So I'll select these random numbers.

38
00:01:55,710 --> 00:01:58,190
Copy them, then I can go right-click.

39
00:01:58,190 --> 00:02:01,688
And there's this 1, 2, 3 that freezes the random numbers, so

40
00:02:01,688 --> 00:02:03,068
now they're not random.

41
00:02:03,068 --> 00:02:06,423
And now all I have to do is select these two columns, and if I sort on

42
00:02:06,423 --> 00:02:10,327
the random numbers I could take the 10 largest through the 10 smallest,

43
00:02:10,327 --> 00:02:12,240
it doesn't matter.

44
00:02:12,240 --> 00:02:15,100
So, basically I would say, sort on the random numbers.

45
00:02:15,100 --> 00:02:16,520
We'll say the 10 largest.

46
00:02:16,520 --> 00:02:17,710
Largest to smallest.

47
00:02:19,450 --> 00:02:21,910
That puts 10 different players on top.

48
00:02:21,910 --> 00:02:24,615
And basically that would be my random sample of size 10.

49
00:02:25,700 --> 00:02:30,050
Now, if I wanted a random sample of size 20 I would go down 20 rows.

50
00:02:30,050 --> 00:02:33,320
Now, clearly every combination of 10 players have the same chance

51
00:02:33,320 --> 00:02:34,780
of being chosen here.

52
00:02:34,780 --> 00:02:37,890
So, basically, this simple procedure enables you to easily

53
00:02:37,890 --> 00:02:41,560
take a simple random sample from any population.

54
00:02:41,560 --> 00:02:42,500
Now, on the next video,

55
00:02:42,500 --> 00:02:46,010
we'll talk about problems that can occur when people take samples.

56
00:02:46,010 --> 00:02:48,410
And we've seen some of these problems in election polls.

