0
00:00:00,930 --> 00:00:03,810
So the question we need to ask ourselves is how do we identify

1
00:00:03,810 --> 00:00:05,810
the right sample, because if we're wrong,

2
00:00:05,810 --> 00:00:09,040
we will draw incorrect conclusions about a population.

3
00:00:09,040 --> 00:00:09,730
The key is for

4
00:00:09,730 --> 00:00:13,420
that sample to be representative of the population of interest.

5
00:00:13,420 --> 00:00:16,310
We could take a simple random sample where every element in

6
00:00:16,310 --> 00:00:19,480
the population has an equal probability of being selected.

7
00:00:19,480 --> 00:00:22,060
It's very much like putting all of the elements in a hat and

8
00:00:22,060 --> 00:00:23,940
pulling out the desired number.

9
00:00:23,940 --> 00:00:26,890
We could also do some more sophisticated types of sampling,

10
00:00:26,890 --> 00:00:30,560
although some of these strategies will lead to some inherent biases.

11
00:00:30,560 --> 00:00:33,410
Let's look at some of the other options starting with stratified

12
00:00:33,410 --> 00:00:34,920
random sampling.

13
00:00:34,920 --> 00:00:37,727
With stratified random sampling, the population is divided into

14
00:00:37,727 --> 00:00:40,091
groups called strata, based on some characteristic.

15
00:00:40,091 --> 00:00:45,360
Then within each group, a sample, usually random is selected.

16
00:00:45,360 --> 00:00:48,940
How many are selected from each strata depends on the purpose for

17
00:00:48,940 --> 00:00:50,720
creating the strata initially.

18
00:00:50,720 --> 00:00:53,690
As an example, suppose, we want to make sure that the percentage of

19
00:00:53,690 --> 00:00:57,210
genders in our study is equal to that of the population.

20
00:00:57,210 --> 00:00:59,988
We planned to sample 100 people from the population, and

21
00:00:59,988 --> 00:01:01,464
the percent of women is 45%.

22
00:01:01,464 --> 00:01:04,636
In this case, we'd randomly select 45 women and

23
00:01:04,636 --> 00:01:07,540
55 men to participate in our study.

24
00:01:07,540 --> 00:01:08,390
In most cases,

25
00:01:08,390 --> 00:01:11,510
stratification is done to ensure the same sample percentages

26
00:01:11,510 --> 00:01:15,390
match the population percentages on some key characteristic.

27
00:01:15,390 --> 00:01:18,110
However, imagine that you are studying something that tends to be

28
00:01:18,110 --> 00:01:20,758
more of an issue for some part of your population.

29
00:01:20,758 --> 00:01:23,580
For example, let's imagine you're studying something that is more of

30
00:01:23,580 --> 00:01:27,450
an issue for women than it is for men, like the glass ceiling effect.

31
00:01:27,450 --> 00:01:30,210
In this case, you might use a stratified

32
00:01:30,210 --> 00:01:33,050
random sampling strategy to over-select women.

33
00:01:33,050 --> 00:01:36,240
You might select 60 rather than 45 women and 40 men.

34
00:01:37,410 --> 00:01:40,680
Cluster sampling involves dividing the population into clusters and

35
00:01:40,680 --> 00:01:43,570
then selecting clusters to be part of the sample.

36
00:01:43,570 --> 00:01:47,160
Every cluster should represent the population on a small scale and

37
00:01:47,160 --> 00:01:49,340
be as heterogeneous as possible.

38
00:01:49,340 --> 00:01:53,650
Every population element must belong to one and only one cluster.

39
00:01:53,650 --> 00:01:56,990
If the researcher decides to include all of individuals from

40
00:01:56,990 --> 00:02:01,480
a given cluster in that sample, this is called a one stage clustering.

41
00:02:01,480 --> 00:02:05,000
If the researcher randomly selects people from those clusters,

42
00:02:05,000 --> 00:02:07,410
this is called multi-stage clustering.

43
00:02:07,410 --> 00:02:10,170
How is this different from stratified random sampling?

44
00:02:10,170 --> 00:02:14,280
In stratified, some members from each group, or strata, are selected.

45
00:02:14,280 --> 00:02:18,160
But in cluster, all or part of some clusters are used, but

46
00:02:18,160 --> 00:02:20,750
not all clusters are included.

47
00:02:20,750 --> 00:02:22,480
If the clusters are heterogeneous and

48
00:02:22,480 --> 00:02:26,560
representative, this sampling strategy works fairly well.

49
00:02:27,900 --> 00:02:30,870
In systematic random sampling, the researcher first

50
00:02:30,870 --> 00:02:34,340
randomly picks the first item or subject from the population,

51
00:02:34,340 --> 00:02:38,070
then the researcher will select each nth subject from the list.

52
00:02:38,070 --> 00:02:40,590
The results are usually representative of population

53
00:02:40,590 --> 00:02:43,350
unless certain characteristics of the population are repeated for

54
00:02:43,350 --> 00:02:46,950
every nth individual, which is highly unlikely.

55
00:02:46,950 --> 00:02:48,474
Finally, we have convenient sampling.

56
00:02:48,474 --> 00:02:51,889
This is a technique where subjects are selected based on their

57
00:02:51,889 --> 00:02:55,770
convenient accessibility or proximity to the researcher.

58
00:02:55,770 --> 00:02:58,520
For example, professors who have their students participate

59
00:02:58,520 --> 00:03:01,800
in the research projects are using convenient sampling.

60
00:03:01,800 --> 00:03:04,800
Clearly, there are a lot of problems with this type of sampling because

61
00:03:04,800 --> 00:03:08,370
it's very unlikely to represent the population, unless of course,

62
00:03:08,370 --> 00:03:10,160
your population is those that are close to you.

