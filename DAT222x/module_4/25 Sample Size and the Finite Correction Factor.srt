0
00:00:00,800 --> 00:00:04,300
So, in this video, we're gonna talk about a more accurate way to come up

1
00:00:04,300 --> 00:00:05,980
with your sample size.

2
00:00:05,980 --> 00:00:09,100
So we basically, in previous videos, said if we're trying to estimate

3
00:00:09,100 --> 00:00:12,580
a mean, here's your sample size, given your level of error and

4
00:00:12,580 --> 00:00:15,080
your standard deviation of your population.

5
00:00:15,080 --> 00:00:18,510
Estimating proportion there is your sample size.

6
00:00:18,510 --> 00:00:21,390
Now, basically, if your sample size turns out to be a big

7
00:00:21,390 --> 00:00:23,819
percentage of your population, you really don't need

8
00:00:25,220 --> 00:00:28,310
is big as sample size as this formula would indicate.

9
00:00:28,310 --> 00:00:31,290
And so what you can do is just fudge these formulas when you get

10
00:00:31,290 --> 00:00:33,500
the sample size from one of these formulas.

11
00:00:33,500 --> 00:00:37,156
Just multiply that sample size that you

12
00:00:37,156 --> 00:00:42,630
get which will be N0 times N divided by N0+N-1.

13
00:00:42,630 --> 00:00:46,711
Now, if you forget about the N0 part, since N0 is bigger than 1,

14
00:00:46,711 --> 00:00:49,002
this denominator is bigger than N, so

15
00:00:49,002 --> 00:00:51,383
you're gonna shrink the value of N0.

16
00:00:51,383 --> 00:00:54,644
Okay, so you get a smaller sample size due to the fact that your

17
00:00:54,644 --> 00:00:58,000
sample size is a big fraction of the population.

18
00:00:58,000 --> 00:01:00,736
So, let's go back to our Fortune 500 example.

19
00:01:00,736 --> 00:01:05,289
Suppose the standard deviation of Fortune 500 CEOs salaries is known

20
00:01:05,289 --> 00:01:06,577
to be $5 million.

21
00:01:06,577 --> 00:01:11,725
And you wanna be 95% sure you can estimate the Fortune 500 CEOs

22
00:01:11,725 --> 00:01:16,820
salaries average within $1 million.

23
00:01:16,820 --> 00:01:18,890
Okay, so your level of error would be 1 million.

24
00:01:20,180 --> 00:01:27,135
Okay, this population size, capital N would be basically 500.

25
00:01:27,135 --> 00:01:29,006
The standard deviation is 5.

26
00:01:29,006 --> 00:01:31,817
And if you plug in this formula here,

27
00:01:31,817 --> 00:01:36,452
you get you need a sample of 96 Fortune 500 CEOs, okay?

28
00:01:36,452 --> 00:01:37,484
Now basically,

29
00:01:37,484 --> 00:01:41,830
we need less than that because what does this formula say to do?

30
00:01:41,830 --> 00:01:43,260
Take the sample size you got.

31
00:01:43,260 --> 00:01:44,760
This is our N0.

32
00:01:44,760 --> 00:01:51,436
And then multiply by this thing here, N divided by N0+N-1.

33
00:01:51,436 --> 00:01:55,694
So what we could do is take the sample size that we got before,

34
00:01:55,694 --> 00:02:00,371
with no finite correction, and then we can multiply by capital N,

35
00:02:00,371 --> 00:02:02,560
which is our population size.

36
00:02:03,890 --> 00:02:08,532
And then we could divide by the sample size that we got before.

37
00:02:10,790 --> 00:02:18,370
Plus the population size, Minus 1.

38
00:02:18,370 --> 00:02:22,614
And that brings us to a sample size of 81 instead of 96.

39
00:02:22,614 --> 00:02:26,610
Which would mean 15 less phone calls or 15 less plane trips,

40
00:02:26,610 --> 00:02:31,150
depending on how we're doing it, or 15 less Skype calls, okay.

41
00:02:31,150 --> 00:02:35,260
And so basically this formula can reduce your burden of sampling,

42
00:02:35,260 --> 00:02:39,530
using the fact that basically that 96 is almost 20% of the population,

43
00:02:39,530 --> 00:02:42,280
so we really don't need that many people.

44
00:02:42,280 --> 00:02:43,590
Now in our final module,

45
00:02:43,590 --> 00:02:47,650
we'll talk about hypothesis testing which is how we sort of decide

46
00:02:47,650 --> 00:02:51,160
between competing hypotheses about population parameters.

