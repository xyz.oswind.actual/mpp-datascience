0
00:00:00,510 --> 00:00:03,150
The standard normal distribution is special case

1
00:00:03,150 --> 00:00:04,880
of the normal distribution.

2
00:00:04,880 --> 00:00:07,590
It is a distribution that occurs when a normal random

3
00:00:07,590 --> 00:00:11,140
variable has a mean of zero and a standard deviation of one.

4
00:00:11,140 --> 00:00:13,980
Recall from module three that a normal random variable can be

5
00:00:13,980 --> 00:00:18,780
transformed into a z score, or a standard score using this equation.

6
00:00:18,780 --> 00:00:20,940
As I mentioned when we were discussing z scores,

7
00:00:20,940 --> 00:00:24,080
one of the benefits of them is that you know the proportion of

8
00:00:24,080 --> 00:00:26,110
observations below your score.

9
00:00:26,110 --> 00:00:30,200
This is actually the cumulative probability associated with that

10
00:00:30,200 --> 00:00:34,710
score which is the area under the curve to the left of that value.

11
00:00:34,710 --> 00:00:38,180
These probabilities never change in a standard normal distribution and

12
00:00:38,180 --> 00:00:41,830
can be found online in z tables or more readily through Excel.

13
00:00:41,830 --> 00:00:46,095
As you recall from module three, in a normal distribution,

14
00:00:46,095 --> 00:00:49,715
68.26% of the data lie between plus 1 and minus 1 standard deviations,

15
00:00:49,715 --> 00:00:54,064
95.45% within plus or minus 2 and

16
00:00:54,064 --> 00:00:58,355
99.73% within plus or minus 3.

