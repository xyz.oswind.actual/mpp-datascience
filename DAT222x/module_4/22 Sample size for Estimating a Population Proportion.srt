0
00:00:00,920 --> 00:00:03,808
In this video we'll discuss how we determine the sample size when

1
00:00:03,808 --> 00:00:07,450
you're trying to estimate an unknown population proportion.

2
00:00:07,450 --> 00:00:11,340
And of course, this is important in presidential election polls,

3
00:00:11,340 --> 00:00:15,100
because you're trying to estimate the fraction of voters who'll be for

4
00:00:15,100 --> 00:00:18,450
the Democratic candidate, the Republican candidate, Libertarian

5
00:00:18,450 --> 00:00:22,470
candidate or whatever country you're in, the parties for your country.

6
00:00:22,470 --> 00:00:25,375
Suppose you want to estimate a population portion p, and

7
00:00:25,375 --> 00:00:30,140
be 95% sure that your estimate of p which is p hat

8
00:00:30,140 --> 00:00:33,650
the sample proportion is accurate to within a given amount, e.

9
00:00:33,650 --> 00:00:35,830
How large a sample size is needed.

10
00:00:35,830 --> 00:00:38,232
Well basically you just take 1.96 squared,

11
00:00:38,232 --> 00:00:41,350
the 1.96 comes from that standard normal.

12
00:00:41,350 --> 00:00:44,050
The 97 and a haf percentile, basically,

13
00:00:44,050 --> 00:00:47,220
divided by four times the amount of error you'll accept squared.

14
00:00:47,220 --> 00:00:48,970
And this error will be a decimal,

15
00:00:48,970 --> 00:00:52,620
because you're trying to estimate a number between zero and one.

16
00:00:52,620 --> 00:00:55,870
So as an example, suppose you want to estimate the fraction of

17
00:00:55,870 --> 00:00:59,624
registered voters preferring the Republican Senatorial candidate in

18
00:00:59,624 --> 00:01:01,660
the 2525 Texas Senatorial race.

19
00:01:01,660 --> 00:01:06,475
We'd like our estimate to have a 95% chance of being accurate within 3%.

20
00:01:06,475 --> 00:01:07,923
How large a sample is needed?

21
00:01:07,923 --> 00:01:10,888
Now what does it mean to be accurate within 3%.

22
00:01:10,888 --> 00:01:15,834
It means like your interval should be like point 37 to point 53.

23
00:01:15,834 --> 00:01:19,345
Now you might say that 0.06 level of accuracy, but

24
00:01:19,345 --> 00:01:23,695
since your estimate's tin the middle 0.5 your margin of error is

25
00:01:23,695 --> 00:01:27,072
0,.53 minus 0.5 or 0.5 minus 0.47.

26
00:01:27,072 --> 00:01:30,630
So we can simply plug in this formula really quickly.

27
00:01:30,630 --> 00:01:33,644
So we can take 1.96 squared.

28
00:01:33,644 --> 00:01:39,378
Caret squares divided by 4 times

29
00:01:39,378 --> 00:01:44,503
the error would be 0.03.

30
00:01:44,503 --> 00:01:46,346
And we wanna square that.

31
00:01:49,339 --> 00:01:53,520
And so we get 1,067.1 there.

32
00:01:53,520 --> 00:01:58,100
So we'd need a sample of 1,067 and we wanna be accurate within 3%.

33
00:01:58,100 --> 00:01:59,535
Now if you follow the election polls,

34
00:01:59,535 --> 00:02:02,000
they'll always say the margin of errors is 3%.

35
00:02:02,000 --> 00:02:03,511
That's what they referring to.

36
00:02:03,511 --> 00:02:06,890
And the sample size they usually used is 1500.

37
00:02:06,890 --> 00:02:11,555
Now, right here, if we will change this error percentage to

38
00:02:11,555 --> 00:02:15,408
2.5% I've used the same formula basically.

39
00:02:15,408 --> 00:02:19,720
You get the 1500, which is what most pollsters use in their sample size.

40
00:02:19,720 --> 00:02:20,770
In other words,

41
00:02:20,770 --> 00:02:25,358
the margin of error in most US presidential election polls is 2.5%.

42
00:02:25,358 --> 00:02:29,438
Now, in the next video we'll talk about how you adjust your confidence

43
00:02:29,438 --> 00:02:33,042
interval formulas when the sample size is a large percentage of

44
00:02:33,042 --> 00:02:34,410
the population.

45
00:02:34,410 --> 00:02:38,057
The good news is the intervals get smaller.

46
00:02:38,057 --> 00:02:41,463
Because basically if you have a small population,

47
00:02:41,463 --> 00:02:45,750
you don't need as big a sample as when you have a big population.

