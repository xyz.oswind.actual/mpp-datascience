0
00:00:00,760 --> 00:00:04,620
Bias is a systematic error that is introduced into your study that can

1
00:00:04,620 --> 00:00:06,310
prejudice your results in some way.

2
00:00:06,310 --> 00:00:09,230
You should be aware of the most common biases

3
00:00:09,230 --> 00:00:11,720
as you consider your sampling strategy.

4
00:00:11,720 --> 00:00:13,960
In most cases, bias is unintentional and

5
00:00:13,960 --> 00:00:17,210
happens because a sampling procedure wasn't well thought out.

6
00:00:17,210 --> 00:00:20,790
A classic example of this is a company who wants information about

7
00:00:20,790 --> 00:00:22,635
one of their products in a particular area and

8
00:00:22,635 --> 00:00:24,820
decides to do a phone survey.

9
00:00:24,820 --> 00:00:27,826
Even if they do a random sample, there's still a problem.

10
00:00:27,826 --> 00:00:30,587
They are missing input from customers who don't have a phone.

11
00:00:30,587 --> 00:00:33,880
Yes, those people really do exist, even today.

12
00:00:33,880 --> 00:00:36,480
There may be something systematically different about

13
00:00:36,480 --> 00:00:40,320
people who use the product and own a phone from those who use it but

14
00:00:40,320 --> 00:00:42,200
do not have a phone.

15
00:00:42,200 --> 00:00:45,600
This is actually an example of what we call selection bias.

16
00:00:45,600 --> 00:00:48,810
Selection bias occurs when each element in the population does

17
00:00:48,810 --> 00:00:51,520
not have the same chance of being chosen.

18
00:00:51,520 --> 00:00:55,220
As an example, imagine that you are reaching into a bag of marbles, but

19
00:00:55,220 --> 00:00:57,390
one marble is a lot bigger than the others.

20
00:00:57,390 --> 00:01:00,030
The probability of pulling out that larger marble is

21
00:01:00,030 --> 00:01:02,755
actually higher than pulling out a smaller marble.

22
00:01:02,755 --> 00:01:05,340
Non-response bias occurs when

23
00:01:05,340 --> 00:01:08,640
respondents differ in meaningful ways from non-respondents.

24
00:01:08,640 --> 00:01:12,320
Similarly, voluntary response bias occurs when sample members

25
00:01:12,320 --> 00:01:14,200
are self-selected volunteers.

26
00:01:14,200 --> 00:01:16,700
The resulting sample in both of these tends to

27
00:01:16,700 --> 00:01:20,710
over represent individuals who have strong opinions.

28
00:01:20,710 --> 00:01:23,980
Publication bias occurs because studies with positive results

29
00:01:23,980 --> 00:01:27,560
are more likely to be published than those with negative or null results.

30
00:01:27,560 --> 00:01:30,460
This is actually a big problem in many fields because

31
00:01:30,460 --> 00:01:33,020
the failure to publish these types of studies

32
00:01:33,020 --> 00:01:36,150
leads us to draw conclusions without having all of the information.

33
00:01:37,242 --> 00:01:40,510
Similarly, survivorship bias occurs when a meaningful

34
00:01:40,510 --> 00:01:43,490
part of a population is not considered in your sample.

35
00:01:43,490 --> 00:01:47,410
For example, in World War II engineers noticed that after

36
00:01:47,410 --> 00:01:50,940
a mission bullet holes tended to be clustered in the wing,

37
00:01:50,940 --> 00:01:53,370
rear gunner and body of returned planes.

38
00:01:53,370 --> 00:01:56,520
They started reinforcing those areas without giving a thought to where

39
00:01:56,520 --> 00:01:59,650
the bullets were doing the most damage on the planes that did not

40
00:01:59,650 --> 00:02:00,890
return.

41
00:02:00,890 --> 00:02:04,750
Today, we see this in finance when mutual fund companies drop poorly

42
00:02:04,750 --> 00:02:08,860
performing mutual funds, which overinflates their past returns.

43
00:02:08,860 --> 00:02:11,690
Or when you look at successful people, identifying interesting

44
00:02:11,690 --> 00:02:14,120
characteristics, such as dropping out of school, and

45
00:02:14,120 --> 00:02:17,480
assuming that all successful people must have dropped out of school.

46
00:02:17,480 --> 00:02:19,810
Essentially, you're ignoring failures.

47
00:02:19,810 --> 00:02:22,480
All those people who dropped out of school and went nowhere

48
00:02:22,480 --> 00:02:25,140
when you're interpreting your results of your study.

49
00:02:25,140 --> 00:02:28,240
Response bias refers to the bias that results from problems

50
00:02:28,240 --> 00:02:30,380
in the measurement process, itself.

51
00:02:30,380 --> 00:02:32,540
For example, when you ask leading questions,

52
00:02:32,540 --> 00:02:35,760
when people tell you what they think you want to hear, or

53
00:02:35,760 --> 00:02:38,670
when they want to present themselves in a favorable way.

54
00:02:38,670 --> 00:02:41,887
This is actually known as social desirability bias.

