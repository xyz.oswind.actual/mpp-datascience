0
00:00:00,660 --> 00:00:03,540
All right, let's take a closer look at how we estimate population

1
00:00:03,540 --> 00:00:05,300
proportions from samples.

2
00:00:05,300 --> 00:00:08,010
To calculate p hat, our point estimate

3
00:00:08,010 --> 00:00:11,230
of the population proportion, we simply take the number of successes

4
00:00:11,230 --> 00:00:15,270
in our sample and divide by the total number of sample observations.

5
00:00:15,270 --> 00:00:18,050
Imagine that we want to know what proportion of hikers

6
00:00:18,050 --> 00:00:20,450
hiked more than 500 miles in a year.

7
00:00:20,450 --> 00:00:24,840
We ask a random sample of 150 hikers, find that 45 do.

8
00:00:24,840 --> 00:00:28,325
To find p hat, we simply divide 45 by 150 and

9
00:00:28,325 --> 00:00:30,961
find that the proportion is 0.3.

10
00:00:30,961 --> 00:00:34,470
To calculate the standard deviation, we use this formula.

11
00:00:34,470 --> 00:00:37,440
You will notice that the standard deviation is actually the standard

12
00:00:37,440 --> 00:00:40,430
error, because we are dividing by the standard deviation of the square

13
00:00:40,430 --> 00:00:41,690
root of n.

14
00:00:41,690 --> 00:00:44,170
But the numerator in this formula should look familiar.

15
00:00:44,170 --> 00:00:47,100
It's the same as the variants of a binomial variable.

16
00:00:47,100 --> 00:00:51,407
In this case, we've plugged the numbers in and we get 0.0374.

17
00:00:51,407 --> 00:00:54,595
I hope you're starting to see how everything's connected in

18
00:00:54,595 --> 00:00:55,337
statistics.

