0
00:00:00,450 --> 00:00:03,350
How many responses do you really need?

1
00:00:03,424 --> 00:00:05,990
I get this question a lot. This simple

2
00:00:06,048 --> 00:00:08,240
question is a never-ending quandary

3
00:00:08,304 --> 00:00:10,740
for researchers. A large sample can

4
00:00:10,800 --> 00:00:13,070
yield more accurate results but depending on

5
00:00:13,120 --> 00:00:14,710
the research that you're doing getting

6
00:00:14,784 --> 00:00:16,490
more and more responses can be quite

7
00:00:16,576 --> 00:00:19,010
expensive. So we want a sample that will

8
00:00:19,088 --> 00:00:20,890
get us the most band for our buck.

9
00:00:21,008 --> 00:00:23,080
One that limits data collection expenses

10
00:00:23,136 --> 00:00:25,000
but still has sufficient power to draw

11
00:00:25,072 --> 00:00:27,320
accurate conclusions about a population.

12
00:00:27,360 --> 00:00:29,453
Based on what you just learned about

13
00:00:29,520 --> 00:00:31,460
confidence intervals you might be able

14
00:00:31,520 --> 00:00:33,620
to see how we can reverse engineer

15
00:00:33,664 --> 00:00:35,420
that equation to get a formula that will

16
00:00:35,472 --> 00:00:38,160
yield the sample size needed to achieve

17
00:00:38,240 --> 00:00:39,760
your desired level of confidence

18
00:00:39,824 --> 00:00:41,420
and that reflects the level of error

19
00:00:41,488 --> 00:00:43,050
that you're willing to tolerate.

20
00:00:43,150 --> 00:00:48,386
Suppose we want to be 95% sure that X bar is an accurate estimate of a

21
00:00:48,448 --> 00:00:52,000
population with an acceptable level of error or E.

22
00:00:52,260 --> 00:00:54,853
How large does the sample size need to be?

23
00:00:54,900 --> 00:00:57,190
We simply multiply the standard deviation

24
00:00:57,248 --> 00:00:59,050
and the critical value associated with

25
00:00:59,136 --> 00:01:00,710
our desired level of confidence for

26
00:01:00,784 --> 00:01:03,820
example 1.96 divided by the error

27
00:01:03,888 --> 00:01:06,660
that we are willing to accept and then square the result.

28
00:01:06,740 --> 00:01:08,750
Note that this requires knowing the

29
00:01:08,800 --> 00:01:10,960
population standard deviation which

30
00:01:11,056 --> 00:01:13,620
we rarely do. If you don't use the

31
00:01:13,680 --> 00:01:16,740
standard deviation from previous research if available.

32
00:01:16,790 --> 00:01:18,320
What standard deviations are

33
00:01:18,384 --> 00:01:19,880
people who are conducting similar

34
00:01:19,952 --> 00:01:24,180
research reporting use that information to guide the standard deviation that you

35
00:01:24,272 --> 00:01:28,600
use in this calculation. Let's look at an example.

36
00:01:28,680 --> 00:01:31,226
How large does our sample size

37
00:01:31,296 --> 00:01:35,530
need to be if we want to be within 20 miles of the yearly mean

38
00:01:35,584 --> 00:01:39,890
for the typical hiker with a confidence level of 99%.

39
00:01:39,990 --> 00:01:41,826
We know that the standard deviation

40
00:01:41,904 --> 00:01:45,300
from previous studies is 52.63 using a

41
00:01:45,424 --> 00:01:47,840
confidence level of 99% our critical

42
00:01:47,904 --> 00:01:51,840
value is 2.576. So we multiply this value

43
00:01:51,904 --> 00:01:54,340
and the standard deviation divided by 20

44
00:01:54,416 --> 00:01:56,980
our error and then square that value

45
00:01:57,024 --> 00:02:01,600
for a result of 45.95 or 46 people.

46
00:02:01,650 --> 00:02:05,213
But if you want to be within ten miles

47
00:02:05,280 --> 00:02:07,760
this number increases to a 184.

48
00:02:07,840 --> 00:02:09,810
The more precise you want to be

49
00:02:09,860 --> 00:02:12,266
the larger the sample needs to be.

50
00:02:12,580 --> 00:02:14,706
Here's how this works for proportions.

51
00:02:14,750 --> 00:02:18,560
You multiply the square of the critical value by p and q

52
00:02:18,624 --> 00:02:22,880
and divide by the error squared. Again this assumes that you know the

53
00:02:22,944 --> 00:02:26,740
proportions in your population. But in most cases because the

54
00:02:26,800 --> 00:02:28,780
research hasn't been conducted there is

55
00:02:28,816 --> 00:02:30,920
no value for the sample proportion.

56
00:02:30,990 --> 00:02:34,973
P and q can then be taken from a previous study if one is available

57
00:02:35,040 --> 00:02:38,310
if there is no previous study or estimate that can be

58
00:02:38,368 --> 00:02:41,560
used, use 0.5 for p and q because

59
00:02:41,648 --> 00:02:43,310
these proportions will result in the

60
00:02:43,360 --> 00:02:46,690
largest sample size estimate from this formula.

61
00:02:46,752 --> 00:02:48,840
Erroring on the size-- side of a

62
00:02:48,896 --> 00:02:51,660
larger sample is always better.

63
00:02:51,728 --> 00:02:54,720
Let's imagine that we want our results to be

64
00:02:54,768 --> 00:02:58,200
within 5% with a confidence level of 95%.

65
00:02:58,288 --> 00:03:01,360
Given that our confidence level is 95%,

66
00:03:01,440 --> 00:03:05,100
we know that the z is 1.96. Let's assume

67
00:03:05,140 --> 00:03:07,320
we don't know the population proportion

68
00:03:07,392 --> 00:03:10,370
or the sample proportion so we'll use 0.5.

69
00:03:10,420 --> 00:03:14,800
In this case, 1.96 squared multiplied by

70
00:03:14,880 --> 00:03:18,050
0.5 times 0.5 and then divided by

71
00:03:18,112 --> 00:03:24,100
0.05 squared gives us a result of 384.16

72
00:03:24,150 --> 00:03:26,840
meaning that we will need 385 people

73
00:03:26,880 --> 00:03:31,893
to be 95% confident that our result is plus or minus 5%.

