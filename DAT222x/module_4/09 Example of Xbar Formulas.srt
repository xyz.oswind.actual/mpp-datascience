0
00:00:00,920 --> 00:00:04,050
Recall in the last video we found out that the expected value of

1
00:00:04,050 --> 00:00:06,551
the sample mean should equal the population mean, and

2
00:00:06,551 --> 00:00:10,210
the variance of the sample mean should equal to the variance

3
00:00:10,210 --> 00:00:13,690
of the population divided by the sample size.

4
00:00:13,690 --> 00:00:16,470
Let's get a concrete illustration of this idea.

5
00:00:16,470 --> 00:00:19,940
Let's suppose our population is the result of when we toss a die.

6
00:00:19,940 --> 00:00:25,090
Basically we could take sample size two by tossing a die twice.

7
00:00:25,090 --> 00:00:27,700
And basically what are the possible

8
00:00:27,700 --> 00:00:29,810
sample results we could get if we toss the die twice.

9
00:00:29,810 --> 00:00:33,071
We could get a one with a one, a one with a two, et cetera.,

10
00:00:33,071 --> 00:00:34,460
through a six with a six.

11
00:00:34,460 --> 00:00:39,540
There are 36 equally likely samples we could get.

12
00:00:39,540 --> 00:00:41,660
They each have a chance of one over 36.

13
00:00:41,660 --> 00:00:44,370
We learned about that in module two.

14
00:00:44,370 --> 00:00:47,430
What would be the sample mean in each situation.

15
00:00:47,430 --> 00:00:51,410
What would just be the average of the two tosses of the die.

16
00:00:54,160 --> 00:00:55,884
So Xbar's a random variable.

17
00:00:55,884 --> 00:00:57,573
In this sample it was one.

18
00:00:57,573 --> 00:00:59,977
In this sample it was two and a half.

19
00:00:59,977 --> 00:01:03,180
So what would be the average value of Xbar?

20
00:01:03,180 --> 00:01:06,450
Take all the values of Xbar times the probability of them occurring.

21
00:01:07,590 --> 00:01:16,980
So, sum of product times the values of Xbar times the probabilities.

22
00:01:20,160 --> 00:01:21,860
And lo and behold, we get three and a half.

23
00:01:21,860 --> 00:01:25,360
And we know that's the population mean because when we toss a die

24
00:01:25,360 --> 00:01:27,870
the average number of dots showing is three and a half.

25
00:01:27,870 --> 00:01:30,380
Now what about the variance of Xbar.

26
00:01:30,380 --> 00:01:35,250
Well for each sample we could take the Xbar we got, minus the mean,

27
00:01:35,250 --> 00:01:40,210
which we know is 3.5 for Xbar.

28
00:01:40,210 --> 00:01:41,070
We could square that.

29
00:01:42,800 --> 00:01:43,350
Double click.

30
00:01:43,350 --> 00:01:45,000
You can copy that down.

31
00:01:45,000 --> 00:01:48,810
And so the variance of Xbar would be taking each of these squared

32
00:01:48,810 --> 00:01:51,130
deviations times the probability of getting that.

33
00:01:55,240 --> 00:01:56,230
So we'd sum a product.

34
00:01:56,230 --> 00:02:05,079
The square deviations with those probabilities.

35
00:02:06,930 --> 00:02:08,150
And we get 1.458.

36
00:02:08,150 --> 00:02:11,650
Now does that match this formula.

37
00:02:11,650 --> 00:02:13,200
Well, the variance of Xbar.

38
00:02:14,450 --> 00:02:18,104
Okay, the variance of when you toss one die the number of dots showing

39
00:02:18,104 --> 00:02:21,150
is 2.91 we've talked about several times.

40
00:02:21,150 --> 00:02:25,746
So basically the variance of Xbar in this case should equal

41
00:02:25,746 --> 00:02:28,710
2.91 divided by 2.

42
00:02:31,331 --> 00:02:36,430
Looking here, we have same as square was 2.91, sample size is 2.

43
00:02:36,430 --> 00:02:42,310
And if you work that out, that's exactly what we got.

44
00:02:42,310 --> 00:02:45,560
So that's an illustration of how these formulas work.

45
00:02:45,560 --> 00:02:48,530
In summary, remember Xbar, the sample mean,

46
00:02:48,530 --> 00:02:50,410
is gonna be a random variable.

47
00:02:50,410 --> 00:02:53,340
On the average, it's gonna come out to the population mean, but

48
00:02:53,340 --> 00:02:55,560
it's not always gonna come out to that.

49
00:02:55,560 --> 00:02:57,130
The population mean is 3.5.

50
00:02:57,130 --> 00:03:00,890
We often don't come out with a sample mean of 3 and a half.

51
00:03:00,890 --> 00:03:05,340
But the variance of Xbar gives us an idea of how sort of accurate

52
00:03:05,340 --> 00:03:09,210
the sample is In estimating the population mean.

53
00:03:09,210 --> 00:03:09,730
And note that,

54
00:03:09,730 --> 00:03:13,910
as the sample size gets bigger, the variance of export gets smaller.

55
00:03:13,910 --> 00:03:15,836
And since on the average it's getting the right answer,

56
00:03:15,836 --> 00:03:18,500
we should think we have a better shot at estimating

57
00:03:18,500 --> 00:03:20,100
the population mean.

58
00:03:20,100 --> 00:03:24,110
And we'll see as we go through this module, that's indeed the case.

59
00:03:24,110 --> 00:03:26,870
In the next video, we'll talk about how we can

60
00:03:26,870 --> 00:03:30,130
use a sample to estimate a population proportion, or

61
00:03:30,130 --> 00:03:33,040
the probability of success in a binomial random variable.

