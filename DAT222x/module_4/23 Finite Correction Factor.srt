0
00:00:00,780 --> 00:00:03,020
Formulas used to compute standard errors

1
00:00:03,060 --> 00:00:04,680
are based on the idea that samples

2
00:00:04,720 --> 00:00:06,780
are selected from an  infinite population

3
00:00:06,800 --> 00:00:08,980
or that samples are selected with replacement.

4
00:00:09,200 --> 00:00:10,560
In most studies,however

5
00:00:10,660 --> 00:00:12,020
neither of these is correct.

6
00:00:12,340 --> 00:00:14,380
This does not present much of a problem

7
00:00:14,420 --> 00:00:15,860
when the sample size is small

8
00:00:15,920 --> 00:00:17,740
relative to the population size.

9
00:00:17,960 --> 00:00:20,540
However when the sample is larger

10
00:00:20,640 --> 00:00:23,580
say greater than 10% of the population

11
00:00:23,760 --> 00:00:25,400
it is best to apply a correction

12
00:00:25,480 --> 00:00:27,540
to the formulas used to compute standard error.

13
00:00:27,820 --> 00:00:31,420
This correction is called the finite population correction.

14
00:00:31,840 --> 00:00:34,680
The formula for the finite population correction

15
00:00:34,760 --> 00:00:37,200
is simply the square root of the sample size

16
00:00:37,220 --> 00:00:39,240
subtracted from the population size

17
00:00:39,320 --> 00:00:40,660
divided by the degrees of freedom

18
00:00:40,700 --> 00:00:42,400
of the population size.

19
00:00:42,740 --> 00:00:44,980
you simply multiply the standard error

20
00:00:45,040 --> 00:00:48,220
by this result, to obtain a better estimate of the variability

21
00:00:48,260 --> 00:00:50,659
in your sample this estimate should then

22
00:00:50,720 --> 00:00:53,230
be used in your statistical calculations

23
00:00:53,420 --> 00:00:55,940
in all cases this factor will reduce

24
00:00:56,020 --> 00:00:58,460
the standard error estimate and

25
00:00:58,540 --> 00:01:00,440
increase the likelihood of obtaining

26
00:01:00,480 --> 00:01:03,920
statistical significance. When the sample size

27
00:01:03,980 --> 00:01:07,600
is very small relative to the population size.

28
00:01:07,660 --> 00:01:09,560
The finite population correction

29
00:01:09,620 --> 00:01:12,600
is almost equal to 1. And it is only has

30
00:01:12,660 --> 00:01:14,560
a small effect on the standard error.

31
00:01:14,960 --> 00:01:17,220
However when the sample size

32
00:01:17,280 --> 00:01:19,780
is large relative to  the population size

33
00:01:19,840 --> 00:01:21,900
The finite population correction can have

34
00:01:22,000 --> 00:01:24,020
a big effect on the standard error.

35
00:01:24,340 --> 00:01:26,320
Let's take a look at how this works?

36
00:01:26,620 --> 00:01:29,360
Imagine that the population is 1,000

37
00:01:29,420 --> 00:01:33,760
we sample 150. Using the same standard error

38
00:01:33,900 --> 00:01:35,880
from our hiking example  we multiply that

39
00:01:35,960 --> 00:01:38,120
correction factor by the standard error.

40
00:01:38,220 --> 00:01:42,040
and obtain 3.96. A smaller standard error

41
00:01:42,060 --> 00:01:44,080
then we would have used otherwise

42
00:01:44,300 --> 00:01:47,140
If we have a sample of 50 though

43
00:01:47,200 --> 00:01:50,140
you can see that this has a very small effect.

44
00:01:50,400 --> 00:01:53,340
The resulting standard error 4.19

45
00:01:53,400 --> 00:01:55,500
isn't that much smaller than the

46
00:01:55,540 --> 00:01:57,060
uncorrected standard error.

47
00:01:57,380 --> 00:02:00,340
we can also apply the correction factor

48
00:02:00,420 --> 00:02:03,340
to confidence intervals by including the correction factor

49
00:02:03,420 --> 00:02:05,380
in the calculation of  the margin of error.

50
00:02:05,860 --> 00:02:09,700
And it can be applied to the sample size calculation.

51
00:02:10,100 --> 00:02:13,480
N0 is the sample size obtained without the correction

52
00:02:13,700 --> 00:02:16,760
that value is multiplied to the population size

53
00:02:16,820 --> 00:02:19,600
and then divided by the sum of the sample size

54
00:02:19,700 --> 00:02:21,620
obtained without the correction plus

55
00:02:21,720 --> 00:02:23,560
the population size minus one.

56
00:02:24,120 --> 00:02:25,720
Remember our hiking example

57
00:02:26,000 --> 00:02:29,240
where we learned that we'd need a sample of 184 hikers

58
00:02:29,360 --> 00:02:31,680
if we really wanted a confidence interval

59
00:02:31,780 --> 00:02:33,440
to be within 10 miles

60
00:02:33,720 --> 00:02:35,720
Do we really need that many

61
00:02:35,840 --> 00:02:38,720
let's apply the finite correction factor to see.

62
00:02:39,040 --> 00:02:42,220
Imagine that we know that the population of hikers is 1,000.

63
00:02:42,440 --> 00:02:46,020
in this case we multiply 184 by 1,000

64
00:02:46,080 --> 00:02:48,100
and divide by 1183.

65
00:02:48,360 --> 00:02:51,340
We then apply the correction factor to our sample size.

66
00:02:51,600 --> 00:02:56,820
Calculation we see that we have a need for 155.54

67
00:02:56,920 --> 00:02:59,680
or 156 hikers in our sample

68
00:02:59,680 --> 00:03:03,440
to be within 10 miles with the confidence of 95%.

