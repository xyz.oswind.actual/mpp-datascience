0
00:00:00,750 --> 00:00:02,210
As you'll recall from module one,

1
00:00:02,210 --> 00:00:04,900
the main difference between a population and a sample has to do

2
00:00:04,900 --> 00:00:07,530
with the observations included in the dataset.

3
00:00:07,530 --> 00:00:09,740
A population includes all elements,

4
00:00:09,740 --> 00:00:14,040
a sample consists of a subset of the observations from the population.

5
00:00:14,040 --> 00:00:18,080
More than one sample can be derived from the same population.

6
00:00:18,080 --> 00:00:21,050
Populations can be all employees in an organization,

7
00:00:21,050 --> 00:00:23,010
all the cupcakes that I make in a day, or

8
00:00:23,010 --> 00:00:25,700
all the trees in the north Cascade Mountains.

9
00:00:25,700 --> 00:00:29,380
A measurable characteristic of a population such as an average

10
00:00:29,380 --> 00:00:31,810
job satisfaction of all employees at Microsoft or

11
00:00:31,810 --> 00:00:34,960
the total amount of frosting on all cupcakes made in one day.

12
00:00:34,960 --> 00:00:36,640
It's called a parameter.

13
00:00:36,640 --> 00:00:40,870
If we are able to survey the entire population, we have a census.

14
00:00:40,870 --> 00:00:43,930
What's interesting about populations is that if you measure

15
00:00:43,930 --> 00:00:48,490
every element the result of your statistical analysis is the truth.

16
00:00:48,490 --> 00:00:49,490
There is no error.

17
00:00:49,490 --> 00:00:51,750
So why would we ever take a sample?

18
00:00:51,750 --> 00:00:54,410
Sometimes it's impractical to measure all the elements of

19
00:00:54,410 --> 00:00:55,580
a population.

20
00:00:55,580 --> 00:00:58,700
For example, if I want to insure the quality of my cupcakes,

21
00:00:58,700 --> 00:01:02,450
I don't need to, and probably shouldn't, eat all of them.

22
00:01:02,450 --> 00:01:05,500
I can assess the quality of the population by eating one, or

23
00:01:05,500 --> 00:01:07,400
two or three.

24
00:01:07,400 --> 00:01:10,500
A measurable characteristic of a sample is called a statistic.

25
00:01:10,500 --> 00:01:13,400
It's intended to represent or estimate the population.

