0
00:00:00,670 --> 00:00:04,480
Okay, in this video, we'll discuss samples and sample statistics.

1
00:00:04,480 --> 00:00:08,020
So, a complete enumeration of a population is a census.

2
00:00:08,020 --> 00:00:10,960
A sample is a part of a population that we observe

3
00:00:10,960 --> 00:00:13,830
in an attempt to get insights about the population.

4
00:00:13,830 --> 00:00:15,720
Now, why do we use samples?

5
00:00:15,720 --> 00:00:18,260
Well, here's the most common reason and most important.

6
00:00:18,260 --> 00:00:21,160
If the population is large, like we have over 200 million

7
00:00:21,160 --> 00:00:23,300
registered voters in the United States,

8
00:00:23,300 --> 00:00:26,170
it's impractical to ask every voter who they're for.

9
00:00:26,170 --> 00:00:29,600
I mean, we just can't do that, it's just too complex.

10
00:00:29,600 --> 00:00:32,880
So sampling involves examining fewer items than a census.

11
00:00:32,880 --> 00:00:34,530
That's another reason we sample.

12
00:00:34,530 --> 00:00:38,120
And that reduces measurement error, because you don't have as many

13
00:00:40,060 --> 00:00:42,940
points of information to analyze for measuring error.

14
00:00:42,940 --> 00:00:46,990
And finally, sampling cold involve destroying elements of a population.

15
00:00:46,990 --> 00:00:49,570
For example, if you test a computer chip to see if the chip is

16
00:00:49,570 --> 00:00:51,650
defective, you may destroy the chip.

17
00:00:51,650 --> 00:00:53,880
So if you did that to every chip that you produced,

18
00:00:53,880 --> 00:00:55,880
you'd have no chips to sell.

19
00:00:55,880 --> 00:00:59,240
So how do we estimate population parameters?

20
00:00:59,240 --> 00:01:03,930
We use a function of the sample data called a sample statistic

21
00:01:03,930 --> 00:01:05,670
to estimate a population parameter.

22
00:01:05,670 --> 00:01:07,400
So here are some examples.

23
00:01:07,400 --> 00:01:10,020
If you want to estimate the median income of Houston

24
00:01:10,020 --> 00:01:13,670
families you might take a sample of 100 Houston families and

25
00:01:13,670 --> 00:01:16,830
use the median income in the sample to estimate the population

26
00:01:16,830 --> 00:01:18,700
of Houston's median family income.

27
00:01:19,860 --> 00:01:22,370
If you want to estimate the fraction with defective chips in

28
00:01:22,370 --> 00:01:24,960
a population, you might test 100 chips and

29
00:01:24,960 --> 00:01:27,362
look at the fraction of defective chips in the sample,

30
00:01:27,362 --> 00:01:30,930
to estimate the fraction of defective chips in the population.

31
00:01:30,930 --> 00:01:34,511
So for example, if you tested 100 chips and five are defective, maybe

32
00:01:34,511 --> 00:01:37,879
your best guess is 5% of the chips produced that day are defective.

33
00:01:38,950 --> 00:01:41,900
You could estimate the average weight of all the cows in India

34
00:01:41,900 --> 00:01:44,070
perhaps weighing 100 cows.

35
00:01:44,070 --> 00:01:46,880
And using the average weight of all the cows in the sample

36
00:01:46,880 --> 00:01:50,090
to estimate the average weight of all the Indian cows.

37
00:01:50,090 --> 00:01:52,500
You could estimate the fraction of registered voters for

38
00:01:52,500 --> 00:01:56,220
Hillary Rodham Clinton 2016 By looking at the fraction of

39
00:01:56,220 --> 00:01:59,960
registered voters in a sample that were for Hillary Rodham Clinton.

40
00:01:59,960 --> 00:02:02,670
And I think we probably saw plenty of polls

41
00:02:02,670 --> 00:02:06,630
during the election particularly if you looked at realclearpolitics.com

42
00:02:06,630 --> 00:02:08,630
that summarized the polls.

43
00:02:08,630 --> 00:02:10,710
Now in the next video,

44
00:02:10,710 --> 00:02:13,830
we'll talk about the definition of a simple random sample, and

45
00:02:13,830 --> 00:02:16,830
how you can take a simple random sample from the population.

