0
00:00:01,230 --> 00:00:02,280
Hi, I'm Wayne Winston.

1
00:00:02,280 --> 00:00:05,240
I taught statistics, Excel modeling and analytics for

2
00:00:05,240 --> 00:00:09,400
over 40 years at the Indiana University Kelley School of Business

3
00:00:09,400 --> 00:00:12,770
and the University of Houston Bauer College of Business.

4
00:00:12,770 --> 00:00:16,740
I've won over 40 teaching awards and written more than a dozen textbooks.

5
00:00:16,740 --> 00:00:20,750
I also used data to help the Dallas Mavericks win an NBA championship

6
00:00:20,750 --> 00:00:24,000
and help companies improve their forecasting ability.

7
00:00:24,000 --> 00:00:27,150
I'm honored to partner with Microsoft to bring you our course

8
00:00:27,150 --> 00:00:29,900
on basic data analysis and probability.

9
00:00:29,900 --> 00:00:32,540
Recently, there has been an explosion of interest in the use of

10
00:00:32,540 --> 00:00:36,680
data to analyze many situations in arena such as Wall Street,

11
00:00:36,680 --> 00:00:40,380
the athletics field, medicine and retail marketing.

12
00:00:40,380 --> 00:00:43,840
Our course will equip you with the basic knowledge of data analysis

13
00:00:43,840 --> 00:00:46,550
that will help you understand many of the current uses of data in

14
00:00:46,550 --> 00:00:50,120
the world and will also set you up for further study in data science.

