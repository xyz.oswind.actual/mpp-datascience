0
00:00:00,730 --> 00:00:01,970
Hi, I'm Wayne Winston.

1
00:00:01,970 --> 00:00:03,200
>> Hi, I'm Liberty Munson.

2
00:00:03,200 --> 00:00:06,720
Welcome to Essential Statistics for Data Analysis Using Excel.

3
00:00:06,720 --> 00:00:09,080
So Wayne, when you think about this course,

4
00:00:09,080 --> 00:00:12,040
what is the one thing you want people to get out of it?

5
00:00:12,040 --> 00:00:14,570
>> Well, I think most people are afraid of data and

6
00:00:14,570 --> 00:00:15,600
afraid of statistics.

7
00:00:15,600 --> 00:00:17,370
And they really shouldn't be.

8
00:00:17,370 --> 00:00:19,569
They mostly have Excel on their desktop.

9
00:00:19,569 --> 00:00:22,469
And I think we can show people in this course how they can

10
00:00:22,469 --> 00:00:26,255
use Excel's powerful data analysis features to really understand and

11
00:00:26,255 --> 00:00:30,160
analyze the explosion of data that they're seeing in the world.

12
00:00:30,160 --> 00:00:32,470
>> Yes, and certainly in the newest release of Excel,

13
00:00:32,470 --> 00:00:36,240
they've made it much, much easier to do a lot of these statistics.

14
00:00:36,240 --> 00:00:38,490
So speaking of statistics, what's your favorite one?

15
00:00:38,490 --> 00:00:41,580
>> Well, I guess that I would say the median income is a measure of

16
00:00:41,580 --> 00:00:42,690
how well off a country is.

17
00:00:42,690 --> 00:00:45,830
The median income is the income of sort of the middle person,

18
00:00:45,830 --> 00:00:48,010
the 50th percentile person.

19
00:00:48,010 --> 00:00:50,380
And the mean income is the average of everybody.

20
00:00:50,380 --> 00:00:53,610
So we look at the average income in the United States the last 30 years

21
00:00:53,610 --> 00:00:55,640
adjusted for inflation, it's gone up,

22
00:00:55,640 --> 00:00:58,110
because the people at the top have made more money.

23
00:00:58,110 --> 00:00:59,836
But the median income has gone down,

24
00:00:59,836 --> 00:01:01,962
which means the typical person is worse off.

25
00:01:01,962 --> 00:01:04,535
And I think we're seeing in American politics and

26
00:01:04,535 --> 00:01:08,027
politics around the world, that the median income is probably more

27
00:01:08,027 --> 00:01:10,978
important to understand the world than the mean income.

28
00:01:10,978 --> 00:01:13,681
>> And that's actually a perfect reason why people should take

29
00:01:13,681 --> 00:01:14,640
this course.

30
00:01:14,640 --> 00:01:17,650
Understanding the statistics that are driving the news that is making

31
00:01:17,650 --> 00:01:19,190
the headlines today.

32
00:01:19,190 --> 00:01:20,640
Understanding that if you look at it and

33
00:01:20,640 --> 00:01:23,330
someone reports the average, America is doing better.

34
00:01:23,330 --> 00:01:26,187
When in fact if you look at the right statistic, the median,

35
00:01:26,187 --> 00:01:27,540
we're in fact doing worse.

36
00:01:27,540 --> 00:01:28,880
>> No, I totally agree.

37
00:01:28,880 --> 00:01:33,160
And, again, I hope you'll join us in this course and see how much fun and

38
00:01:33,160 --> 00:01:36,740
how easy it is to use Excel to understand how to analyze data and

39
00:01:36,740 --> 00:01:37,780
understand uncertainty.

