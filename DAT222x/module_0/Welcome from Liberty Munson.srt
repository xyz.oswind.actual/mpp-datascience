0
00:00:00,507 --> 00:00:04,810
Hi, welcome to essential statistics for data analysis using Excel.

1
00:00:04,810 --> 00:00:05,730
I'm Liberty Munson,

2
00:00:05,730 --> 00:00:09,050
I'm a pyschometrician in Microsoft's Learning organization.

3
00:00:09,050 --> 00:00:11,580
Essentially what that means is that I'm responsible for

4
00:00:11,580 --> 00:00:12,720
ensuring the validity and

5
00:00:12,720 --> 00:00:16,420
reliability of all the processes we use to validate skills.

6
00:00:16,420 --> 00:00:19,500
While some of that is done during design and development, a lot more

7
00:00:19,500 --> 00:00:23,810
of it is done actually using data, and data science, and analysis.

8
00:00:23,810 --> 00:00:26,780
Much of what you will learn in this course will introduce you to key

9
00:00:26,780 --> 00:00:28,770
concepts and statistics.

10
00:00:28,770 --> 00:00:31,890
And will help you better evaluate information that's being presented

11
00:00:31,890 --> 00:00:32,725
to you in the news.

12
00:00:32,725 --> 00:00:36,753
As well as make you more effective in making the right decisions after

13
00:00:36,753 --> 00:00:38,355
you analyze your own data.

14
00:00:38,355 --> 00:00:39,288
I hope you'll join us.

