0
00:00:00,880 --> 00:00:03,520
If you recall from module one.

1
00:00:03,520 --> 00:00:07,990
Data sets had a mean, a variance, and a standard deviation.

2
00:00:07,990 --> 00:00:10,220
The same is true of random variables.

3
00:00:10,220 --> 00:00:14,000
So let's show you in this video how to find the expected value.

4
00:00:14,000 --> 00:00:16,140
Or the mean of a discrete random variable.

5
00:00:16,140 --> 00:00:18,200
We'll go through two examples.

6
00:00:18,200 --> 00:00:21,660
Let's consider a stock that has a 40% chance of going up 20% in

7
00:00:21,660 --> 00:00:25,390
a year, 30% chance of staying unchanged, and

8
00:00:25,390 --> 00:00:28,180
a 30% chance of going down 20%.

9
00:00:28,180 --> 00:00:33,300
How will we find the meaner expected value for that stock?

10
00:00:33,300 --> 00:00:36,140
Well basically you simply wait the probability of each outcome

11
00:00:36,140 --> 00:00:36,815
times the outcome.

12
00:00:36,815 --> 00:00:43,240
You take .4 times .2, .3 times zero plus .3 times minus .2.

13
00:00:43,240 --> 00:00:48,034
Now we could multiply those by saying B4 times C4, B5 times C5,

14
00:00:48,034 --> 00:00:50,990
B6 times C6 but there's an easier way.

15
00:00:50,990 --> 00:00:53,098
There's a function called sum product.

16
00:00:53,098 --> 00:00:56,100
Sum product in Excel multiplies a row times a row or

17
00:00:56,100 --> 00:00:57,770
a column times a column.

18
00:00:57,770 --> 00:01:00,530
So if I highlight the values and probabilities.

19
00:01:00,530 --> 00:01:07,560
It basically takes B4 times C4, plus B5 times C5, plus B6 times C6.

20
00:01:07,560 --> 00:01:10,180
And there we get the average is 2%.

21
00:01:10,180 --> 00:01:13,200
Notice that's not even a possible value of the random variable, but

22
00:01:13,200 --> 00:01:13,880
that's okay.

23
00:01:15,510 --> 00:01:19,338
So that means, basically, if we would repeat this experiment,

24
00:01:25,188 --> 00:01:29,929
Very often, on the average, we would gain 2% each time we repeated this

25
00:01:29,929 --> 00:01:31,770
experiment with the stock.

26
00:01:32,990 --> 00:01:35,260
Now let's do another example of an expected value.

27
00:01:35,260 --> 00:01:37,250
Let's look at tossing a die.

28
00:01:37,250 --> 00:01:38,840
On the average, how many dots show up?

29
00:01:38,840 --> 00:01:42,900
Well we know the chance of getting a one, two, three, four, five or

30
00:01:42,900 --> 00:01:44,220
six dots is one sixth.

31
00:01:45,750 --> 00:01:48,990
So we can write those probabilities right there, and

32
00:01:48,990 --> 00:01:51,880
then the expected value would be one times one sixth plus

33
00:01:51,880 --> 00:01:54,700
two times one sixth plus three times one sixth.

34
00:01:54,700 --> 00:01:57,820
We can also do that with sum of product.

35
00:01:57,820 --> 00:02:00,706
Sum of product, the number of dots showing on the die

36
00:02:02,774 --> 00:02:07,770
With the probabilities and you'll get three and a half.

37
00:02:07,770 --> 00:02:11,444
On the average when you toss a die, three and a half dot show up.

38
00:02:15,463 --> 00:02:18,792
But you know every time you toss a die you'll never see exactly three

39
00:02:18,792 --> 00:02:20,130
and a half dots.

40
00:02:20,130 --> 00:02:23,090
Now in the next video we'll teach you how to find the variance and

41
00:02:23,090 --> 00:02:25,830
standard deviation of a discrete random variable.

