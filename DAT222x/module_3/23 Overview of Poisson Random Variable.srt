0
00:00:00,820 --> 00:00:03,219
Now let's talk about the Poisson random variables.

1
00:00:03,219 --> 00:00:06,621
A Poisson random variable is a discrete random variable that counts

2
00:00:06,621 --> 00:00:10,740
the number of times a certain event occurs in a specific interval.

3
00:00:10,740 --> 00:00:14,170
I suspect you will never guess how this distribution was first used.

4
00:00:14,170 --> 00:00:17,220
It was first used to estimate the probability of the number of deaths

5
00:00:17,220 --> 00:00:20,150
by horse-kicking in the Prussian army in a given year.

6
00:00:20,150 --> 00:00:22,600
Well, I guess these things have to start somewhere.

7
00:00:22,600 --> 00:00:23,610
More relevantly,

8
00:00:23,610 --> 00:00:26,490
Poisson random variables would be obtained by counting the number

9
00:00:26,490 --> 00:00:30,580
of car accidents at a busy intersection between 6 and 9 AM.

10
00:00:30,580 --> 00:00:32,740
The number of typing errors on a page, or

11
00:00:32,740 --> 00:00:36,310
the defect rate of a machine in one day or a month.

12
00:00:36,310 --> 00:00:40,050
The Poisson distribution applies when the event is something that can

13
00:00:40,050 --> 00:00:41,780
be counted in whole numbers.

14
00:00:41,780 --> 00:00:44,160
Occurences are independent.

15
00:00:44,160 --> 00:00:46,470
The average frequency of occurence for

16
00:00:46,470 --> 00:00:48,562
a time period in question is known.

17
00:00:48,562 --> 00:00:51,438
In Poisson distributions, this is known as lambda.

18
00:00:51,438 --> 00:00:54,356
And it possible to count how many events have occurred,

19
00:00:54,356 --> 00:00:57,560
such as the number of car accidents on Hollywood Boulevard.

20
00:00:57,560 --> 00:01:00,720
But meaningless to try to determine how many such events have

21
00:01:00,720 --> 00:01:01,940
not occurred.

22
00:01:01,940 --> 00:01:05,610
It doesn't make any sense to ask how many accidents did not occur during

23
00:01:05,610 --> 00:01:06,580
that time frame, or

24
00:01:06,580 --> 00:01:10,020
how many people weren't killed by horse-kicking in a given year.

25
00:01:10,020 --> 00:01:12,780
The last point illustrates the contrast with the binomial

26
00:01:12,780 --> 00:01:13,610
distribution.

27
00:01:13,610 --> 00:01:16,990
Where the probability of each of two mutually exclusive events, P,

28
00:01:16,990 --> 00:01:19,280
the probability of something happening, and Q,

29
00:01:19,280 --> 00:01:22,940
the probability of something not happening, is known.

30
00:01:22,940 --> 00:01:25,790
The Poisson distribution is essentially the binomial

31
00:01:25,790 --> 00:01:29,460
distribution without Q, without that failure rate.

32
00:01:29,460 --> 00:01:33,227
The Poisson distribution gives the expected frequency profile for

33
00:01:33,227 --> 00:01:35,745
events based on a known average occurrence.

