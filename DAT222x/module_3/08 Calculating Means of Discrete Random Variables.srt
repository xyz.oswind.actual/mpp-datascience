0
00:00:00,020 --> 00:00:03,733
The mean of a Discrete random variable

1
00:00:03,790 --> 00:00:07,000
X is also called the expected value of X.

2
00:00:07,020 --> 00:00:10,688
The Expected Value of X is simply the sum of the value of

3
00:00:10,730 --> 00:00:13,800
the random variable multiply by its probability.

4
00:00:13,930 --> 00:00:17,155
Imagine that you are flipping a coin three times

5
00:00:17,240 --> 00:00:20,933
what is the expected value of the number of heads that we will toss?

6
00:00:20,990 --> 00:00:25,155
We know that you can toss a 0,1, 2 or 3 heads

7
00:00:25,170 --> 00:00:27,911
if you think throught each possible combination of

8
00:00:27,950 --> 00:00:32,066
tosses you can obtain the probability of each possible outcome.

9
00:00:32,130 --> 00:00:36,800
For example there is one combination of tosses that results and no heads,

10
00:00:36,880 --> 00:00:41,488
the case where all three tosses result in tails. But there are three

11
00:00:41,570 --> 00:00:43,844
different ways to toss only one head

12
00:00:43,860 --> 00:00:47,488
you can toss head on  the first, second or third trail.

13
00:00:47,500 --> 00:00:51,777
There are three ways to toss two heads and one way to toss all heads.

14
00:00:51,790 --> 00:00:53,977
To calculate the mean, we multiply

15
00:00:53,990 --> 00:00:57,822
each outcome in the  sample space with its probability and SumNum.

16
00:00:57,840 --> 00:01:01,800
The probability of tossing no heads or all heads is the same

17
00:01:01,820 --> 00:01:06,911
at .125, 1 of the 8 possible outcomes.

18
00:01:06,990 --> 00:01:10,570
The probability of tossing one or two heads is the same

19
00:01:10,590 --> 00:01:14,822
.375, 3 out of the 8 possible outcomes.

20
00:01:14,840 --> 00:01:18,755
We multiply each possible outcome by it's probability

21
00:01:18,770 --> 00:01:21,711
so 0 times .125=0,

22
00:01:21,730 --> 00:01:26,688
1 times .375=.375 and so on.

23
00:01:26,700 --> 00:01:29,177
Once we have all of those products across

24
00:01:29,190 --> 00:01:33,511
all of the outcomes we SumNum for a mean of 1.5

