0
00:00:00,430 --> 00:00:03,834
To enhance our understanding of the concepts of a mean variance, and

1
00:00:03,834 --> 00:00:05,889
standard deviation of random variable,

2
00:00:05,889 --> 00:00:07,606
let's go back to our die example.

3
00:00:07,606 --> 00:00:09,595
So if recall from the previous video,

4
00:00:09,595 --> 00:00:12,960
the average number of dots showing when you toss a die is 3.5.

5
00:00:12,960 --> 00:00:17,165
The variance is 2.91, and the standard deviation is 1.71.

6
00:00:17,165 --> 00:00:19,164
So to illustrate what this really means,

7
00:00:19,164 --> 00:00:20,942
how about we toss a die a lot of times?

8
00:00:20,942 --> 00:00:22,962
Let's try 10,000 times and

9
00:00:22,962 --> 00:00:26,519
you can do this in Excel with a function called RANDBETWEEN.

10
00:00:26,519 --> 00:00:30,607
Sp, RANDBETWEEN(1,6) is equally likely to give you any

11
00:00:30,607 --> 00:00:32,419
integer between 1 and 6.

12
00:00:32,419 --> 00:00:36,434
So here we have the dice being tossed 10,000 times.

13
00:00:36,434 --> 00:00:39,714
And so, we can find the average to be near 3.5, in other words,

14
00:00:39,714 --> 00:00:43,172
if we perform the experiment over and over, we should get basically is

15
00:00:43,172 --> 00:00:46,532
the average value of our outcome, the mean of the random variable.

16
00:00:46,532 --> 00:00:50,605
We got 3.499 here and the variance was 2.93.

17
00:00:50,605 --> 00:00:53,365
And the standard deviation was 1.71.

18
00:00:53,365 --> 00:00:58,180
Now, I can hit the F9 key and make Excel do this again.

19
00:00:58,180 --> 00:01:00,700
So if I hit F9, these numbers will change.

20
00:01:00,700 --> 00:01:02,396
Remember here it was 2.64, but

21
00:01:02,396 --> 00:01:04,664
that will probably change as aI hit the F9 key.

22
00:01:04,664 --> 00:01:06,945
But no matter how many times I hit F9,

23
00:01:06,945 --> 00:01:11,509
the average should be close to 3.5, the variance close to 2.91, and

24
00:01:11,509 --> 00:01:14,168
the standard deviation close to 1.71.

25
00:01:14,168 --> 00:01:16,239
So I'll hit the F9 key here.

26
00:01:19,102 --> 00:01:21,789
And Excel is now trying to change those numbers.

27
00:01:27,824 --> 00:01:31,344
And you can see we got a 4, 3, and 2 instead of the 2, 6, 4,

28
00:01:31,344 --> 00:01:32,825
so Excel did recalculate.

29
00:01:32,825 --> 00:01:37,436
Our mean was still around 3.5, our variance was around 2.91,

30
00:01:37,436 --> 00:01:41,440
and our standard deviation was around 1.71.

31
00:01:41,440 --> 00:01:44,370
Now, in the next three videos, we'll turn our attention to the important

32
00:01:44,370 --> 00:01:47,560
topic of trying to understand how to find the mean variance, and

33
00:01:47,560 --> 00:01:50,300
standard deviation of the sums of random variables.

