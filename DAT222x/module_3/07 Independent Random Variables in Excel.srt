0
00:00:01,280 --> 00:00:03,870
In module two, we talked about independent events.

1
00:00:03,870 --> 00:00:06,270
Now let's talk about independent random variables.

2
00:00:06,270 --> 00:00:08,500
So when are two random variables independent?

3
00:00:08,500 --> 00:00:11,450
If the knowledge of one random variable tells you nothing about

4
00:00:11,450 --> 00:00:13,120
the value of the other random variable.

5
00:00:13,120 --> 00:00:15,290
So for example, pick a given year.

6
00:00:15,290 --> 00:00:17,560
Look at how many games Real Madrid soccer wins and

7
00:00:17,560 --> 00:00:20,670
the Dallas Cowboys Football team win in a year.

8
00:00:20,670 --> 00:00:23,790
They happen to be the two most valuable franchises in sports.

9
00:00:23,790 --> 00:00:25,990
Well, I think those are independent random variables.

10
00:00:25,990 --> 00:00:27,944
Cuz if Real Madrid has a really great year,

11
00:00:27,944 --> 00:00:30,457
I don't think it tells me anything about the Cowboys.

12
00:00:30,457 --> 00:00:31,960
And if the Cowboys have a great year,

13
00:00:31,960 --> 00:00:34,900
I don't think it tells me anything about Real Madrid.

14
00:00:34,900 --> 00:00:37,610
Now let's look at two random variables that are absolutely,

15
00:00:37,610 --> 00:00:40,220
positively not independent.

16
00:00:40,220 --> 00:00:42,600
Let's look at the return on Exxon stock in a year and

17
00:00:42,600 --> 00:00:43,530
Shell stock in a year.

18
00:00:43,530 --> 00:00:45,200
They're both oil companies.

19
00:00:45,200 --> 00:00:46,860
Okay, they're not independent, why?

20
00:00:46,860 --> 00:00:50,827
Cuz if I told you, Exxon did great, that means, basically,

21
00:00:50,827 --> 00:00:54,510
one main thing, oil prices really were high.

22
00:00:54,510 --> 00:00:57,210
And that would mean that Shell would probably do great.

23
00:00:57,210 --> 00:01:00,890
So basically you can't say the returns on Exxon and

24
00:01:00,890 --> 00:01:04,320
Shell are not related cuz the key determinant of those returns is

25
00:01:04,320 --> 00:01:05,410
simply the price of oil.

26
00:01:06,450 --> 00:01:08,400
Now what if we have more than two random variables.

27
00:01:08,400 --> 00:01:10,390
Random variable X1, X2 through XN.

28
00:01:10,390 --> 00:01:12,440
How are they independent?

29
00:01:12,440 --> 00:01:15,581
If the knowledge of the values of any subset of the random variables

30
00:01:15,581 --> 00:01:18,980
tells you nothing about the value of the other random variables.

31
00:01:18,980 --> 00:01:22,920
For example, if x sub i is the pizza sold at a pizza shop during the ith

32
00:01:22,920 --> 00:01:23,971
day of the month.

33
00:01:23,971 --> 00:01:27,873
And we have x1, x2 through x30 in a 30 day month, say June, and

34
00:01:27,873 --> 00:01:29,640
we say they're independent.

35
00:01:29,640 --> 00:01:32,914
What that means is if I told you the number of pizzas sold the first 10

36
00:01:32,914 --> 00:01:36,131
days of the month, tells you nothing about the number of pizzas that

37
00:01:36,131 --> 00:01:39,765
would be sold during the remaining days, 11 through 30, of the month.

38
00:01:39,765 --> 00:01:42,424
Now a very important example of independence is what's called

39
00:01:42,424 --> 00:01:45,080
the efficient market hypothesis in finance.

40
00:01:45,080 --> 00:01:48,570
So suppose X sub i is the return on a given stock on

41
00:01:48,570 --> 00:01:50,090
ith day of the year.

42
00:01:50,090 --> 00:01:52,800
The efficient market hypothesis says

43
00:01:52,800 --> 00:01:55,710
X sub i is independent of the previous dates.

44
00:01:55,710 --> 00:01:56,628
In other words,

45
00:01:56,628 --> 00:02:00,432
if you know what happens to a stock on its returns on previous days, it

46
00:02:00,432 --> 00:02:04,058
gives you absolutely no information on how it's gonna do today.

47
00:02:04,058 --> 00:02:07,327
There's a lot of debate on whether this is true or not, but

48
00:02:07,327 --> 00:02:09,931
Eugene Fama won the Nobel Prize for finance for

49
00:02:09,931 --> 00:02:14,190
a lot of research showing the efficient market hypothesis is true.

50
00:02:14,190 --> 00:02:17,520
And Robert Shiller won the Nobel Prize for finance for

51
00:02:17,520 --> 00:02:20,280
showing the efficient market hypothesis is false.

52
00:02:20,280 --> 00:02:21,790
So figure that out while you wait for

53
00:02:21,790 --> 00:02:25,650
the video, which will discuss how to find the mean, standard deviation,

54
00:02:25,650 --> 00:02:27,700
and variance of random variables.

