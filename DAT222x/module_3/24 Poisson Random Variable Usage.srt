0
00:00:00,930 --> 00:00:03,930
The interesting feature about the Gaussian distribution is that it's

1
00:00:03,930 --> 00:00:05,100
asymmetrical.

2
00:00:05,100 --> 00:00:07,820
The range of variation ends with zero on one side.

3
00:00:07,820 --> 00:00:11,130
You can never have an event that happens less than zero times, but

4
00:00:11,130 --> 00:00:12,910
is unlimited on the other side.

5
00:00:12,910 --> 00:00:16,520
If a machine is creating defective parts, it could do so forever.

6
00:00:16,520 --> 00:00:19,540
As a result, the distribution is always skewed to the right.

7
00:00:19,540 --> 00:00:22,960
Further, as you can see as the average expected occurrence are,

8
00:00:22,960 --> 00:00:26,220
gets larger, the degree of SKU units become smaller.

9
00:00:26,220 --> 00:00:28,545
And approaches what appears to be normal.

10
00:00:28,545 --> 00:00:31,580
Gaussian is most often used for rare events but

11
00:00:31,580 --> 00:00:33,490
it can be used with any number.

12
00:00:33,490 --> 00:00:36,360
At about 20 though, the distribution starts to lose some of its

13
00:00:36,360 --> 00:00:39,620
properties that make it so cool because it becomes more symmetrical

14
00:00:39,620 --> 00:00:42,700
as it moves away from that hard zero lower boundary.

15
00:00:42,700 --> 00:00:45,240
Let's take a closer look at a distribution for something that

16
00:00:45,240 --> 00:00:48,740
occurs on average of two times in a specified time period.

17
00:00:48,740 --> 00:00:49,350
For example,

18
00:00:49,350 --> 00:00:53,430
that a machine generates on average two defective parts every day.

19
00:00:53,430 --> 00:00:55,930
You will see that the probability of three defective parts

20
00:00:55,930 --> 00:00:59,620
on a particular day is 18%, while the probability of one or

21
00:00:59,620 --> 00:01:04,730
two defective parts occurring is actually the same at about 27% each.

22
00:01:04,730 --> 00:01:08,110
So you're wondering, when will you ever use this distribution?

23
00:01:08,110 --> 00:01:10,140
When is it ever gonna be valuable?

24
00:01:10,140 --> 00:01:13,380
As I mentioned this is particularly effective when wanting to understand

25
00:01:13,380 --> 00:01:15,220
the probability of rare events.

26
00:01:15,220 --> 00:01:18,240
So it's quite commonly used in fields related to biology,

27
00:01:18,240 --> 00:01:21,570
chemistry and medicine when dealing with rare diseases, pathogens and

28
00:01:21,570 --> 00:01:23,740
chemical reactions and so on.

29
00:01:23,740 --> 00:01:27,110
More practically you might use it to assign resources to projects when

30
00:01:27,110 --> 00:01:30,030
creating employee schedules based on the probability of certain rare

31
00:01:30,030 --> 00:01:33,570
events happening, how many surgeons do you need on the graveyard shift

32
00:01:33,570 --> 00:01:36,220
or to estimate the probability of receiving a certain number of

33
00:01:36,220 --> 00:01:38,770
customer complaints during a particular time frame.

34
00:01:38,770 --> 00:01:41,524
How many customer service reps do you need to have on call between

35
00:01:41,524 --> 00:01:42,085
4 and 6 AM?

36
00:01:42,085 --> 00:01:45,055
You can also use it to calculate the probability of running out of

37
00:01:45,055 --> 00:01:47,915
inventory based on the average number of sales in a given day,

38
00:01:47,915 --> 00:01:49,050
week, or month.

39
00:01:49,050 --> 00:01:51,810
Or even the odds that the oversold flight that you're on will

40
00:01:51,810 --> 00:01:52,940
run out of seats.

41
00:01:52,940 --> 00:01:54,560
Wayne will walk you through how this is done in Excel.

