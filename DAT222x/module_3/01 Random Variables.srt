0
00:00:00,950 --> 00:00:03,570
Now let's look at different types of variables starting with the random

1
00:00:03,570 --> 00:00:04,420
variable.

2
00:00:04,420 --> 00:00:07,220
A random variable is a set of possible values from

3
00:00:07,220 --> 00:00:08,840
a random experiment.

4
00:00:08,840 --> 00:00:12,120
If we flip a coin we can either get a head or a tail.

5
00:00:12,120 --> 00:00:14,740
We need to assign values to each possible outcome.

6
00:00:14,740 --> 00:00:18,760
Let's give heads a value of zero and tails a value of one.

7
00:00:18,760 --> 00:00:22,960
For each flip of the result will be X, which is our random variable

8
00:00:22,960 --> 00:00:26,850
because it can be either zero or one randomly.

9
00:00:26,850 --> 00:00:30,820
When the value is a variable is determined by chance event,

10
00:00:30,820 --> 00:00:34,020
that variable is called a random variable.

11
00:00:34,020 --> 00:00:35,440
Let's look at another example.

12
00:00:35,440 --> 00:00:38,130
Imagine that we're flipping a coin three times.

13
00:00:38,130 --> 00:00:40,365
We are interested in the number of tosses that result in heads.

14
00:00:40,365 --> 00:00:44,861
In this example, the random variable X is equal to zero,

15
00:00:44,861 --> 00:00:47,080
one, two, or three.

16
00:00:47,080 --> 00:00:51,460
We could throw no heads, one head, two heads, or three.

17
00:00:51,460 --> 00:00:53,880
Any of these could occur and do so randomly.

18
00:00:53,880 --> 00:00:56,450
What's interesting about random variables is that

19
00:00:56,450 --> 00:00:59,310
the probability of each event occurring could be different

20
00:00:59,310 --> 00:01:01,120
as is the case in this example.

21
00:01:01,120 --> 00:01:05,295
The probability of tossing no heads is different from the probability of

22
00:01:05,295 --> 00:01:07,286
tossing one head in three tosses.

