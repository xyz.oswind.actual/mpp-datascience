0
00:00:00,220 --> 00:00:03,070
So a Poisson random variable is characterized by it's mean.

1
00:00:03,070 --> 00:00:06,693
We often use the Greek letter Lambda to represent the mean of a Poisson

2
00:00:06,693 --> 00:00:07,701
random variable.

3
00:00:07,701 --> 00:00:10,512
It turns out the variance of a Poisson random variable equals

4
00:00:10,512 --> 00:00:11,172
the mean, and

5
00:00:11,172 --> 00:00:14,700
then the standard deviation will be the square root of the mean.

6
00:00:14,700 --> 00:00:17,840
So a quick example, if we have an average of 200 people per

7
00:00:17,840 --> 00:00:21,470
hour arriving at a bank, then in two hours, what would be the mean

8
00:00:21,470 --> 00:00:24,740
variance in standard deviation of the number of people showing up?

9
00:00:24,740 --> 00:00:27,809
Well, if it's 200 people per hour arriving on average,

10
00:00:27,809 --> 00:00:30,106
then in two hours, it's 400 on average.

11
00:00:30,106 --> 00:00:32,968
And the variance would equal the mean, so that's 400, and

12
00:00:32,968 --> 00:00:35,668
the standard deviation would be the square root of 400,

13
00:00:35,668 --> 00:00:36,700
which is 20 people.

14
00:00:37,890 --> 00:00:39,201
Now in our next few videos,

15
00:00:39,201 --> 00:00:41,768
we'll talk about probably the most important random

16
00:00:41,768 --> 00:00:44,460
variable in statistics, the normal random variable.

17
00:00:44,460 --> 00:00:48,255
And you'll see later in this module when we discuss the central limit

18
00:00:48,255 --> 00:00:51,540
theorem, why the normal random variable is so important.

