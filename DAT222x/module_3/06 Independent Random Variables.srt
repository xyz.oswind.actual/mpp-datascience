0
00:00:00,990 --> 00:00:04,080
Finally, let's quickly discuss independent random variables.

1
00:00:04,080 --> 00:00:06,920
Think back to our lesson on independent events.

2
00:00:06,920 --> 00:00:09,520
Two events are independent if, and only if,

3
00:00:09,520 --> 00:00:13,120
the occurrence of event A does not change the probability of event B,

4
00:00:13,120 --> 00:00:14,630
otherwise they are dependent.

5
00:00:14,630 --> 00:00:16,840
This extends to variables in the same way.

6
00:00:16,840 --> 00:00:20,770
In other words, two random variables are independent if, and only if,

7
00:00:20,770 --> 00:00:24,170
the events related to those random variables are independent events.

8
00:00:24,170 --> 00:00:26,880
Meaning that they convey no information about each other and

9
00:00:26,880 --> 00:00:30,430
as consequence receiving information about one does not

10
00:00:30,430 --> 00:00:33,740
change assessment about the probability of the other.

11
00:00:33,740 --> 00:00:36,230
There is no relationship between them.

12
00:00:36,230 --> 00:00:40,140
Flipping a coin is an example of an independent random variable,

13
00:00:40,140 --> 00:00:42,400
as is rolling a dice.

14
00:00:42,400 --> 00:00:46,377
The independence between two random variables is also called

15
00:00:46,377 --> 00:00:48,295
statistical independence.

