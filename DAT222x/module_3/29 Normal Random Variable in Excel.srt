0
00:00:00,690 --> 00:00:03,560
The normal random variable is a continuous random variable,

1
00:00:03,560 --> 00:00:05,280
it's specified by two parameters.

2
00:00:05,280 --> 00:00:08,890
It's mean, we usually use the Greek letter mu for the mean.

3
00:00:08,890 --> 00:00:12,010
And the standard deviation sometimes called sigma we use the Greek

4
00:00:12,010 --> 00:00:12,592
letter sigma.

5
00:00:12,592 --> 00:00:16,450
And we'll see soon that the central limit theorem explains why

6
00:00:16,450 --> 00:00:19,620
the normal random variable is really relevant in the world for

7
00:00:19,620 --> 00:00:22,060
computing lots of probabilities.

8
00:00:22,060 --> 00:00:24,980
As an example of the normal random variable let's look at IQs which

9
00:00:24,980 --> 00:00:26,490
we've looked at before.

10
00:00:26,490 --> 00:00:29,750
IQs are often modeled as a normal random variable with a mean of 100,

11
00:00:29,750 --> 00:00:32,800
a standard deviation of 15 points.

12
00:00:32,800 --> 00:00:37,310
Now for a normal random variable 68% of the time you'll see a value

13
00:00:37,310 --> 00:00:41,920
within 1 sigma of the mean, 95% of the time within 2 sigma, and

14
00:00:41,920 --> 00:00:45,360
that's where the outlier definition came from in module one.

15
00:00:45,360 --> 00:00:48,100
Remember we said anything that was more than two standard deviations

16
00:00:48,100 --> 00:00:49,986
from the mean was an outlier.

17
00:00:49,986 --> 00:00:54,874
And 99.7% of the time a normal random variable will be within three

18
00:00:54,874 --> 00:00:57,980
standard deviations of the mean.

19
00:00:57,980 --> 00:01:00,000
And so, we have some illustrations to this,

20
00:01:00,000 --> 00:01:02,883
courtesy of Palisade's @Risk package, some nice pictures.

21
00:01:02,883 --> 00:01:07,045
So, here's the density function, PDF, as we talked about earlier for

22
00:01:07,045 --> 00:01:09,172
the normal random variable for IQs.

23
00:01:09,172 --> 00:01:15,445
Between 85 and 115, if you want 68%, you go 85.1 to 114.9.

24
00:01:15,445 --> 00:01:19,124
68% of the area under normal density is in that range,

25
00:01:19,124 --> 00:01:21,280
that's 1 standard deviation.

26
00:01:21,280 --> 00:01:24,483
If we wanna go within 3 standard deviations of the mean,

27
00:01:24,483 --> 00:01:27,510
we'd go from 55 to 145, we'd get 99.7%.

28
00:01:27,510 --> 00:01:33,080
And if we want within 2 standard deviations of the mean

29
00:01:33,080 --> 00:01:37,930
roughly 70 to 130 would give you're 95%.

30
00:01:37,930 --> 00:01:41,430
Couple more quick comments about the normal random variable.

31
00:01:41,430 --> 00:01:43,980
The normal probability density function is symmetric

32
00:01:43,980 --> 00:01:45,060
about the mean.

33
00:01:45,060 --> 00:01:47,550
What that means is if you look at the mean here of 100.

34
00:01:47,550 --> 00:01:48,113
And go,

35
00:01:48,113 --> 00:01:52,786
let's say, ten points to the left and ten points to the right in IQs.

36
00:01:52,786 --> 00:01:55,518
Or 20 points to the left or 20 points to the right,

37
00:01:55,518 --> 00:01:58,027
the density function will have the same height.

38
00:01:58,027 --> 00:02:01,333
So like at 80 and 120, this density has the same height.

39
00:02:01,333 --> 00:02:05,110
At 90 and 110 it has the same height, what does that mean?

40
00:02:05,110 --> 00:02:08,460
Roughly as many people have IQs near 80 as 120.

41
00:02:08,460 --> 00:02:13,740
Roughly as many people have IQs near 90 as 110, so symmetric means it

42
00:02:13,740 --> 00:02:18,290
looks the same again to the left of the mean and the right of the mean.

43
00:02:18,290 --> 00:02:22,983
You can see this side of the density looks

44
00:02:22,983 --> 00:02:23,510
sorta like this side of the density.

45
00:02:23,510 --> 00:02:26,160
And for a normal random variable the mean here would be 100.

46
00:02:26,160 --> 00:02:30,210
Now the median would be the 50th percentile like we talked about in

47
00:02:30,210 --> 00:02:30,840
module one.

48
00:02:30,840 --> 00:02:34,783
That's gonna be 100 because half the area is gonna be to the left of 100,

49
00:02:34,783 --> 00:02:37,890
half the area to the right of 100 by the symmetry.

50
00:02:37,890 --> 00:02:38,867
And then if we say,

51
00:02:38,867 --> 00:02:41,865
the mode is the most frequently occurring value of a random

52
00:02:41,865 --> 00:02:45,924
variable, the mode would also be 100 cuz the density is highest at 100.

53
00:02:45,924 --> 00:02:49,551
So, the normal random variable has the mean equal to the median equals

54
00:02:49,551 --> 00:02:50,113
the mode.

55
00:02:50,113 --> 00:02:53,890
And, there are several great functions to help us calculate

56
00:02:53,890 --> 00:02:58,420
normal probabilities, and we'll learn about those in the next video.

