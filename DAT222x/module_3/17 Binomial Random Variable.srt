0
00:00:00,670 --> 00:00:02,210
A binomial random variable,

1
00:00:02,210 --> 00:00:05,150
is a specific type of discrete random variable that counts how

2
00:00:05,150 --> 00:00:08,490
often a particular event occurs in a fixed number of trials.

3
00:00:08,490 --> 00:00:11,110
For a variable to be a binomial random variable,

4
00:00:11,110 --> 00:00:13,490
all of the following conditions must be met.

5
00:00:13,490 --> 00:00:16,870
There must be a fixed number of trials, a fixed sample size.

6
00:00:16,870 --> 00:00:20,610
So for example, we're going to toss a coin six times.

7
00:00:20,610 --> 00:00:24,950
Next, on each trial, the event of interest either occurs or does not.

8
00:00:24,950 --> 00:00:28,430
Imagine we are interested in tails, which we will assign a value of one.

9
00:00:28,430 --> 00:00:29,973
If we roll heads, we will sign a value of zero.

10
00:00:29,973 --> 00:00:34,880
The probability of occurrence or not, is the same on each trial.

11
00:00:34,880 --> 00:00:35,903
We have a fair coin so

12
00:00:35,903 --> 00:00:38,199
the probability of tossing a tail is 50%.

13
00:00:38,199 --> 00:00:41,702
This is often referred to as P in binomial formulas.

14
00:00:41,702 --> 00:00:46,190
The probability of P not occurring is referred to as Q.

15
00:00:46,190 --> 00:00:49,050
Next, trials are independent of one another.

16
00:00:49,050 --> 00:00:52,600
A coin toss doesn't affect the outcome of the next.

17
00:00:52,600 --> 00:00:57,630
So you've just tossed three heads, that does not mean that the next

18
00:00:57,630 --> 00:01:00,130
coin that you'll toss would become a tail.

19
00:01:00,130 --> 00:01:03,140
It could still be a heads as is in this case.

20
00:01:03,140 --> 00:01:07,010
We can apply the same logic to the roll of a die, or multiple dice.

21
00:01:07,010 --> 00:01:08,760
We can roll one of six numbers, and

22
00:01:08,760 --> 00:01:12,310
the probability of rolling any given number or a combination of numbers,

23
00:01:12,310 --> 00:01:15,230
with multiple die, doesn't change from one roll to the next.

