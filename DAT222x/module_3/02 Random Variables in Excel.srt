0
00:00:00,930 --> 00:00:03,336
In this module, we're gonna discuss random variables.

1
00:00:03,336 --> 00:00:06,930
So of course, we have to start out by defining a random variable.

2
00:00:06,930 --> 00:00:08,590
So first of all, a variable in algebra,

3
00:00:08,590 --> 00:00:10,820
you probably know, is an unknown quantity.

4
00:00:10,820 --> 00:00:14,120
So we usually use x or y or something in algebra.

5
00:00:14,120 --> 00:00:15,590
So what's a random variable?

6
00:00:15,590 --> 00:00:18,750
Well, we associate a numerical value with each possible sample

7
00:00:18,750 --> 00:00:19,470
space outcome.

8
00:00:19,470 --> 00:00:21,660
So it's essentially an uncertain variable.

9
00:00:21,660 --> 00:00:23,160
So we need some examples.

10
00:00:24,260 --> 00:00:25,505
Suppose you toss a die and

11
00:00:25,505 --> 00:00:28,657
you get paid the square of the number of dots showing on the die.

12
00:00:28,657 --> 00:00:34,061
So if you get a 1, you get $1, a 2, you get $4, a 3, you get $9,

13
00:00:34,061 --> 00:00:39,328
a 4, you get 4 squared, or 16, 5, 5 squared, 25, or 36.

14
00:00:39,328 --> 00:00:40,175
So, basically,

15
00:00:40,175 --> 00:00:43,510
that would be the possible values of the random variable in that case.

16
00:00:44,610 --> 00:00:45,698
Suppose you have a light bulb.

17
00:00:45,698 --> 00:00:47,410
How long does it last before burning out?

18
00:00:47,410 --> 00:00:48,850
That's a random variable.

19
00:00:48,850 --> 00:00:51,471
You don't know in advance how long a light bulb's gonna

20
00:00:51,471 --> 00:00:52,709
last before burning out.

21
00:00:52,709 --> 00:00:56,590
Our repair guy said he got us light bulbs that'll last 22 years on

22
00:00:56,590 --> 00:00:57,430
the average.

23
00:00:57,430 --> 00:00:59,201
I'll belive that when I see it.

24
00:00:59,201 --> 00:01:04,100
Okay, percentage return on Microsoft stock, well, I'll say the year 2525.

25
00:01:04,100 --> 00:01:06,890
There was a famous song about that when I was a kid.

26
00:01:06,890 --> 00:01:08,410
Okay, so what might that be?

27
00:01:08,410 --> 00:01:10,846
Well, Microsoft might go up 50%.

28
00:01:10,846 --> 00:01:12,821
It might go up 70%.

29
00:01:12,821 --> 00:01:14,848
It might even go down 5%.

30
00:01:14,848 --> 00:01:16,965
We're trying to be optimistic here.

31
00:01:16,965 --> 00:01:19,610
And look at the first year market share for a new drug.

32
00:01:19,610 --> 00:01:22,220
It might get a 40% share of the market.

33
00:01:22,220 --> 00:01:24,168
It might get a 10% share.

34
00:01:24,168 --> 00:01:27,720
Maybe nobody wants to use it or it gets taken off the market.

35
00:01:29,270 --> 00:01:32,280
The number of aces when you draw 5 cards from a deck of cards,

36
00:01:32,280 --> 00:01:34,310
basically without replacement.

37
00:01:34,310 --> 00:01:38,910
Well, there's only 4 aces in the decks, so you might get 0 aces,

38
00:01:38,910 --> 00:01:42,150
1 ace, 2 ace, 3 aces, or 4 aces.

39
00:01:42,150 --> 00:01:42,820
Now, if you recall,

40
00:01:42,820 --> 00:01:47,550
when we talked in our first video about, basically, variables.

41
00:01:47,550 --> 00:01:51,557
We've talked about numerical variables could be discreet or

42
00:01:51,557 --> 00:01:53,336
they could be continuous.

43
00:01:53,336 --> 00:01:56,117
And so we're also gonna talk now about random variables

44
00:01:56,117 --> 00:01:57,715
being discrete or continuous,

45
00:01:57,715 --> 00:02:00,210
and give you some examples in the next few videos.

