0
00:00:00,340 --> 00:00:02,950
So, in this video, we're gonna learn how to find the variance for

1
00:00:02,950 --> 00:00:05,500
the sum of random variables that are independent.

2
00:00:05,500 --> 00:00:09,160
So, these formulas only hold if the random variables are independent.

3
00:00:09,160 --> 00:00:10,890
In other words, the value of one doesn't affect

4
00:00:10,890 --> 00:00:12,100
the value of the other.

5
00:00:12,100 --> 00:00:14,480
So given random variables X1, X2 through Xn,

6
00:00:14,480 --> 00:00:16,240
how do I find the variance of the sum?

7
00:00:16,240 --> 00:00:18,650
VAR for short for variance.

8
00:00:18,650 --> 00:00:21,600
The variance of the sum of n independent random variables

9
00:00:21,600 --> 00:00:24,320
is the sum of the individual variances.

10
00:00:24,320 --> 00:00:27,470
And so, the variances add of the random variables are independent but

11
00:00:27,470 --> 00:00:29,600
the standards deviations don't add.

12
00:00:29,600 --> 00:00:32,700
You have to find the standard deviation of the sum of n

13
00:00:32,700 --> 00:00:35,670
random variables by taking the square root of the variance.

14
00:00:35,670 --> 00:00:38,150
So, let's do two examples of this.

15
00:00:38,150 --> 00:00:41,520
We tossed a die 100 times and we wanna find the variance in standard

16
00:00:41,520 --> 00:00:43,890
deviation of how many total dots show up.

17
00:00:43,890 --> 00:00:47,184
Well we know the variance on one die is 2.91.

18
00:00:47,184 --> 00:00:53,136
So the variance on 100 dice would just be 100 times 2.91 or 291.

19
00:00:53,136 --> 00:00:57,959
And then the standard deviation, Would

20
00:00:57,959 --> 00:01:02,220
be the square root of 291 which is about 17.

21
00:01:02,220 --> 00:01:06,000
Okay, now how about if we toss a coin 100 times, get the variance and

22
00:01:06,000 --> 00:01:08,490
standard deviation of the total number of heads?

23
00:01:08,490 --> 00:01:10,630
Well, we need the variance on one coin toss, and

24
00:01:10,630 --> 00:01:11,970
we don't have that yet.

25
00:01:11,970 --> 00:01:15,650
So, that's the average squared deviation from the mean.

26
00:01:15,650 --> 00:01:17,110
So, if we get zero heads,

27
00:01:17,110 --> 00:01:21,200
we know the mean is 0.5 heads on a coin toss, we've talked about that.

28
00:01:21,200 --> 00:01:25,534
So, here the squared deviation would be -0.5

29
00:01:25,534 --> 00:01:29,469
squared cuz we have 0 minus 0.5 squared.

30
00:01:29,469 --> 00:01:34,770
Here, the squared deviation would be 0.5 squared, 1 minus 0.5 squared.

31
00:01:37,980 --> 00:01:43,054
And so, the variance would just be really 0.5 times 0.25 plus 0.5

32
00:01:43,054 --> 00:01:47,744
times 0.25, which we can figure out pretty straightforwardly.

33
00:01:52,173 --> 00:01:54,840
So that's the variance on one toss.

34
00:01:54,840 --> 00:01:59,050
Now, what's the variance on 100 tosses of the coin?

35
00:01:59,050 --> 00:02:02,029
Well, it's the variance on the first toss plus the second toss.

36
00:02:02,029 --> 00:02:05,261
That's 0.25 plus 0.25 through the 100th toss.

37
00:02:05,261 --> 00:02:07,074
It's 100 times 0.25.

38
00:02:09,370 --> 00:02:13,045
And that would be 25 and then the standard deviation,

39
00:02:16,692 --> 00:02:21,120
Would be the square root of that which is 5.

40
00:02:21,120 --> 00:02:24,610
Now remember the rule of thumb from module one that 95%

41
00:02:24,610 --> 00:02:27,600
of the time you're within 2 standard deviations of the mean?

42
00:02:27,600 --> 00:02:31,470
Well, that holds pretty well when we talk about tossing a coin.

43
00:02:31,470 --> 00:02:35,190
We know the average number of heads, we toss a coin 100 times, it's 50.

44
00:02:35,190 --> 00:02:38,020
So we go plus or minus 2 standard deviations,

45
00:02:38,020 --> 00:02:41,398
that would take us 10 below, and 10 below 50.

46
00:02:41,398 --> 00:02:45,782
So, roughly, 95% of the time,

47
00:02:45,782 --> 00:02:49,728
if you toss a coin 100 times,

48
00:02:49,728 --> 00:02:54,712
you will get between 40 and 60 hits.

49
00:02:57,634 --> 00:03:01,510
And in a couple of videos, we'll actually verify that computation.

50
00:03:03,170 --> 00:03:04,210
In the next video,

51
00:03:04,210 --> 00:03:06,670
we're gonna start talking about a specific random variable,

52
00:03:06,670 --> 00:03:09,740
the binomial random variable, which has many uses in the real world.

