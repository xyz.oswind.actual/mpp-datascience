0
00:00:00,650 --> 00:00:03,380
So I've taught statistics for many, many years.

1
00:00:03,380 --> 00:00:08,230
And before I started using Excel to teach statistics in, I guess, 1992,

2
00:00:08,230 --> 00:00:11,370
we would use the stat books and they would always have these

3
00:00:11,370 --> 00:00:15,020
things called z tables that compute normal probabilities.

4
00:00:15,020 --> 00:00:17,740
And it was absolutely horrible to teach from the z tables

5
00:00:17,740 --> 00:00:21,000
cuz every book had a different z table.

6
00:00:21,000 --> 00:00:25,630
Well, Excel really makes the z tables apps obsolete and

7
00:00:25,630 --> 00:00:28,280
basically we can, using the norm dysfunction and

8
00:00:28,280 --> 00:00:32,020
norm inverse functions in Excel, solve many interesting probability

9
00:00:32,020 --> 00:00:34,750
problems involving the normal random variable.

10
00:00:34,750 --> 00:00:38,180
So what is the syntax of these two functions and what do they do.

11
00:00:38,180 --> 00:00:43,200
So if you do NORM.DIST and you put in a value of x, okay,

12
00:00:43,200 --> 00:00:46,330
and a mean and standard deviation and put in the word True.

13
00:00:46,330 --> 00:00:48,020
You do need that word true.

14
00:00:48,020 --> 00:00:50,580
It gives you the probability a normal random variable with given

15
00:00:50,580 --> 00:00:54,080
mean and sigma is basically less than or equal to x.

16
00:00:54,080 --> 00:00:58,170
And it's also less than x, so because we said before,

17
00:00:58,170 --> 00:01:00,380
what's the chance somebody's exactly six feet tall?

18
00:01:00,380 --> 00:01:01,390
It's zero.

19
00:01:01,390 --> 00:01:04,210
So whether we say less than or equal to or less than, when we talk about

20
00:01:04,210 --> 00:01:07,030
a normal random variable, we really get the same answer.

21
00:01:07,030 --> 00:01:09,160
Because it's a continuous random variable.

22
00:01:09,160 --> 00:01:11,230
Now you could change true to false here, but

23
00:01:11,230 --> 00:01:13,010
we'll not do that in this course.

24
00:01:13,010 --> 00:01:15,960
But if you change true to false, you get the height of the normal density

25
00:01:15,960 --> 00:01:17,340
if you're interested in that.

26
00:01:17,340 --> 00:01:20,400
But make sure you put the word true in there, you get the probability of

27
00:01:20,400 --> 00:01:22,870
normal random variable less than or equal to x.

28
00:01:22,870 --> 00:01:23,700
So as an example,

29
00:01:23,700 --> 00:01:26,740
what's the fraction of people with IQ's less than or equal to 90?

30
00:01:26,740 --> 00:01:30,841
We'll do some more complex examples in the next example.

31
00:01:30,841 --> 00:01:36,410
So do norm disk, we would put a 90, the mean is 100,

32
00:01:36,410 --> 00:01:41,516
standard deviation is 15 and we need that word true

33
00:01:41,516 --> 00:01:46,760
to let our true probabilities come shining through.

34
00:01:47,870 --> 00:01:51,970
Okay, we get 25% of people have IQs less than or equal to 90.

35
00:01:51,970 --> 00:01:53,670
Now, here's a picture to show you that again,

36
00:01:53,670 --> 00:01:56,260
courtesy of Palisades At Risk package.

37
00:01:56,260 --> 00:02:00,090
See, if I look at the normal density for IQs here, mean 100,

38
00:02:00,090 --> 00:02:03,780
standard deviation 15, the area under this whole thing is one.

39
00:02:03,780 --> 00:02:06,420
What fraction of the area is less than or equal to 90?

40
00:02:06,420 --> 00:02:09,119
It's the shaded area, and it's 25%.

41
00:02:10,700 --> 00:02:14,777
Okay, now we can also get percentiles of a normal random

42
00:02:14,777 --> 00:02:15,656
variable.

43
00:02:15,656 --> 00:02:20,922
Norm.inv(p, mu, sigma) gives the pth percentile.

44
00:02:20,922 --> 00:02:24,261
Like if p is 0.9, the 90th percentile of a normal random

45
00:02:24,261 --> 00:02:26,355
variable with a given mean and sigma,

46
00:02:26,355 --> 00:02:28,810
you don't need that true or false.

47
00:02:28,810 --> 00:02:31,910
If you put p as 0.1, you get the 10th percentile.

48
00:02:31,910 --> 00:02:35,272
You put p equals 0.5, you get the median, okay?

49
00:02:35,272 --> 00:02:38,561
P equals 0.6, you get the 60th percentile.

50
00:02:38,561 --> 00:02:41,464
So if I want the 90th percentile of IQs, and of course,

51
00:02:41,464 --> 00:02:44,500
that should be higher than 100, use your common sense.

52
00:02:44,500 --> 00:02:48,080
That should be a thoroughly smart person on the IQ test.

53
00:02:48,080 --> 00:02:51,055
So I would do NORM.INV.

54
00:02:51,055 --> 00:02:54,030
Okay, probability would 0.9.

55
00:02:54,030 --> 00:03:00,901
The mean is 100 and the standard deviation is 15 and I get 119.2.

56
00:03:00,901 --> 00:03:02,881
So if we look at this picture,

57
00:03:02,881 --> 00:03:06,923
it should be 90% of the area to the left of 119.2, and

58
00:03:06,923 --> 00:03:11,175
that's exactly true and 10% is to the right of 119.2.

59
00:03:11,175 --> 00:03:14,015
So in next video we'll continue with the applications

60
00:03:14,015 --> 00:03:16,745
of these functions to compute normal probabilities.

