0
00:00:00,780 --> 00:00:03,240
Now, let's talk about one of the most important variables in

1
00:00:03,240 --> 00:00:06,010
statistics, the normal random variable.

2
00:00:06,010 --> 00:00:09,710
Measurements collected of continuous variables, such as height, weight,

3
00:00:09,710 --> 00:00:13,190
temperature, distance traveled, are considered continuous random

4
00:00:13,190 --> 00:00:17,040
variables because these variables can take on any value across a wide,

5
00:00:17,040 --> 00:00:20,300
possibly infinite range, and can vary randomly.

6
00:00:20,300 --> 00:00:21,540
The distribution of many,

7
00:00:21,540 --> 00:00:25,390
if not most continuous variables is bell-shaped or normal.

8
00:00:25,390 --> 00:00:29,190
As a result these variables are called normal random variables.

9
00:00:29,190 --> 00:00:32,420
As you recall from module one, normal curves are symmetrical with

10
00:00:32,420 --> 00:00:35,060
the mean median and mode clustered together.

11
00:00:35,060 --> 00:00:38,620
This variable will play a role in your statistics future.

12
00:00:38,620 --> 00:00:42,100
Odds are that much of what you do in statistics will be based on normal

13
00:00:42,100 --> 00:00:43,110
random variables.

