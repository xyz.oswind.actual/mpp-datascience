0
00:00:00,970 --> 00:00:03,950
Sometimes it is necessary to add or subtract random variables, and

1
00:00:03,950 --> 00:00:06,750
it is useful to know the mean and variance of the result.

2
00:00:06,750 --> 00:00:09,890
The mean of the sum of two random variables, X and

3
00:00:09,890 --> 00:00:12,605
Y, is simply the sum of their means.

4
00:00:12,605 --> 00:00:15,245
Imagine that we know the joint probabilities of x and y, and

5
00:00:15,245 --> 00:00:16,925
they are presented in this table.

6
00:00:16,925 --> 00:00:19,695
To calculate the mean of the sum of these two variables,

7
00:00:19,695 --> 00:00:23,645
find the expected value of x by summing across the probabilities for

8
00:00:23,645 --> 00:00:25,315
0, and then multiplying by 0.

9
00:00:25,315 --> 00:00:27,195
Repeat for x equals 1 and

10
00:00:27,195 --> 00:00:30,365
then sum that with what the value you obtained from 0.

11
00:00:30,365 --> 00:00:33,630
In that case, you see that the result is 0.3.

12
00:00:33,630 --> 00:00:35,990
You repeat this process for Y and

13
00:00:35,990 --> 00:00:40,182
you see the result is 1.4, then you sum those expected values.

14
00:00:40,182 --> 00:00:43,052
0.3+1.4 and you get 1.7.

15
00:00:43,052 --> 00:00:45,752
Now, let&#39;s imagine that you&#39;re playing

16
00:00:45,752 --> 00:00:50,300
two games with different probabilities of financial outcomes.

17
00:00:50,300 --> 00:00:53,620
Calculate the expected value for each game and sum them, and

18
00:00:53,620 --> 00:00:54,750
then sum the sums.

19
00:00:54,750 --> 00:00:59,210
As you can see, if you play both of these games, you should break even.

20
00:00:59,210 --> 00:01:00,500
But why would you do that?

21
00:01:00,500 --> 00:01:02,720
Why wouldn&#39;t you play just game B?

