0
00:00:00,500 --> 00:00:03,420
In this video, we'll introduce you to the Poisson Random Variable,

1
00:00:03,420 --> 00:00:06,170
which is another important discrete random variable.

2
00:00:06,170 --> 00:00:09,500
So when is the Poisson Random Variable applicable?

3
00:00:09,500 --> 00:00:12,070
It can be used to approximate the binomial random variable,

4
00:00:12,070 --> 00:00:14,160
where the number of trials n is large and

5
00:00:14,160 --> 00:00:17,300
the chance of success p on each trial is small.

6
00:00:17,300 --> 00:00:20,655
If you at least a 100 trials, the probably success is less root

7
00:00:20,655 --> 00:00:23,774
the 0.1 and the product dependent p is less or equal 20.

8
00:00:23,774 --> 00:00:26,254
Now would you expect that most 20 successes,

9
00:00:26,254 --> 00:00:29,960
the Poisson would do a nice job of approximating binomial.

10
00:00:29,960 --> 00:00:32,000
We also use the Poisson when there,

11
00:00:32,000 --> 00:00:35,940
this situation we have many opportunities for an event to occur.

12
00:00:35,940 --> 00:00:39,000
And for each opportunity event has small chance of occurring.

13
00:00:39,000 --> 00:00:40,750
Some examples follow.

14
00:00:40,750 --> 00:00:42,600
So let's think about a Starbucks.

15
00:00:42,600 --> 00:00:45,490
Let's assume people arrive one at a time at a Starbucks.

16
00:00:45,490 --> 00:00:48,550
So the number of people arriving at a Starbucks in an hour,

17
00:00:48,550 --> 00:00:51,780
will usually be governed by a Poisson Random Variable, Y.

18
00:00:51,780 --> 00:00:55,050
Cuz each second there is a small chance that a person arrives,

19
00:00:55,050 --> 00:00:58,230
again ignoring people arriving in groups.

20
00:00:58,230 --> 00:01:01,570
So you have many opportunities, many seconds in an hour, for

21
00:01:01,570 --> 00:01:03,120
somebody to arrive at a Starbucks.

22
00:01:03,120 --> 00:01:06,530
And there's a small chance in a given second, that somebody arrives.

23
00:01:06,530 --> 00:01:10,850
You don't expect 3,600 people an hour to arrive at a Starbucks,

24
00:01:10,850 --> 00:01:13,610
although sometimes it sort of looks that way.

25
00:01:13,610 --> 00:01:15,870
Now, let's think about the number of misprints in a book.

26
00:01:15,870 --> 00:01:18,510
That also follows a Poisson Random Variable.

27
00:01:18,510 --> 00:01:19,265
Why?

28
00:01:19,265 --> 00:01:22,641
Look at each page of a book, maybe its a 500 page book.

29
00:01:22,641 --> 00:01:24,900
So you have many opportunities for a misprint.

30
00:01:24,900 --> 00:01:27,720
Each page has a small chance of having a misprint.

31
00:01:27,720 --> 00:01:30,600
So again, we have this situation where we have many opportunities for

32
00:01:30,600 --> 00:01:32,010
an event to happen.

33
00:01:32,010 --> 00:01:35,210
In each opportunity there is a small chance the event happens.

34
00:01:35,210 --> 00:01:37,280
We ignore the possibility there could be two or

35
00:01:37,280 --> 00:01:39,280
more misprints on one page.

36
00:01:39,280 --> 00:01:42,240
Which honestly in some of my books I think that's happened.

37
00:01:42,240 --> 00:01:44,740
Let's talk about the number of goals a premier league

38
00:01:44,740 --> 00:01:46,300
team scores in a soccer game.

39
00:01:47,820 --> 00:01:49,858
Now, there aren't that many goals in soccer.

40
00:01:49,858 --> 00:01:53,190
I think those of you who are soccer fans know, and you have to think

41
00:01:53,190 --> 00:01:56,599
about each second of a soccer game there is a small chance of a goal.

42
00:01:56,599 --> 00:01:58,840
There'll either be zero goals or one goal.

43
00:01:58,840 --> 00:02:00,930
And there are many 0-0 ties in soccer,

44
00:02:00,930 --> 00:02:04,730
so I think most seconds we see 0 goals being scored.

45
00:02:04,730 --> 00:02:08,750
So often, when people are analyzing soccer scores,

46
00:02:08,750 --> 00:02:11,590
they'll assume the number of goals a team scores in a game,

47
00:02:11,590 --> 00:02:14,080
follows a Poisson random variable.

48
00:02:14,080 --> 00:02:17,320
Now how do we find Poisson probabilities using Excel?

49
00:02:17,320 --> 00:02:20,010
Thankfully we don't have to use these ugly tables in the back of

50
00:02:20,010 --> 00:02:21,740
many stat books.

51
00:02:21,740 --> 00:02:24,710
So there's a nice function POISSON.DIST.

52
00:02:24,710 --> 00:02:26,970
No quite as good as BINOM.DISTrange.

53
00:02:26,970 --> 00:02:27,785
But it's pretty good.

54
00:02:27,785 --> 00:02:30,080
POISSON.DIST.

55
00:02:30,080 --> 00:02:32,040
X would be your number of successes.

56
00:02:32,040 --> 00:02:35,190
And all you have to note for the Poisson is the mean.

57
00:02:35,190 --> 00:02:37,290
You don't have to know n and p.

58
00:02:37,290 --> 00:02:38,770
If you know the mean for a Poisson,

59
00:02:38,770 --> 00:02:40,672
that determines all the probabilities.

60
00:02:40,672 --> 00:02:44,780
But POISSON.DIST, where x is your number of successes, and

61
00:02:44,780 --> 00:02:46,320
you're given the mean, and you put it in as 0,

62
00:02:46,320 --> 00:02:50,910
you get the chance of exactly x of n sort of successes happening.

63
00:02:50,910 --> 00:02:52,800
Now if you change that last argument to a 1,

64
00:02:52,800 --> 00:02:57,300
you get the probability of less than or equal to x successes.

65
00:02:57,300 --> 00:03:02,150
And basically a false can be used for 0 and a 1 could be used for

66
00:03:02,150 --> 00:03:02,718
true.

67
00:03:02,718 --> 00:03:06,136
And in our next video we'll show you how to solve some probability

68
00:03:06,136 --> 00:03:08,757
problems, using this POISSON.DIST function.

