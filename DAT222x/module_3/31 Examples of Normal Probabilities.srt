0
00:00:01,110 --> 00:00:04,838
So let's do two probability calculations using that NORMDIST

1
00:00:04,838 --> 00:00:05,548
function.

2
00:00:05,548 --> 00:00:08,461
Again, focusing on IQs which have a mean of 100 and

3
00:00:08,461 --> 00:00:10,157
a standard deviation of 15.

4
00:00:10,157 --> 00:00:11,057
Suppose I ask you,

5
00:00:11,057 --> 00:00:14,327
what's the probability somebody's IQ is between 90 and 120?

6
00:00:14,327 --> 00:00:16,430
And I have less-than signs there.

7
00:00:16,430 --> 00:00:19,118
But, if it were less-than or equal-tos it would be the same

8
00:00:19,118 --> 00:00:22,350
answer because we're assuming IQs are continuous.

9
00:00:22,350 --> 00:00:29,758
So basically 90<= IQ less<=120.

10
00:00:29,758 --> 00:00:31,880
Okay, so these two are the same.

11
00:00:31,880 --> 00:00:35,720
Now, how do I find the chance of being between 90 and 120?

12
00:00:35,720 --> 00:00:39,120
This is tricky, cuz that NORMDIST does only area to the left.

13
00:00:39,120 --> 00:00:44,344
So what I have to do is take the chance the IQ is less than or equal

14
00:00:44,344 --> 00:00:50,384
to 120 and then take away the chance it's less than or equal to 90.

15
00:00:50,384 --> 00:00:54,293
Because that'll leave everybody between 120 and 90.

16
00:00:54,293 --> 00:00:57,757
So this NORMDIST function only does area to the left under the normal

17
00:00:57,757 --> 00:00:58,621
distribution.

18
00:00:58,621 --> 00:01:00,870
So you have to sort of think about it that way.

19
00:01:00,870 --> 00:01:04,830
So the chance of being between 90 and 120, we have a picture here.

20
00:01:06,210 --> 00:01:10,370
You take the area to the left of 120 minus the area to the left of 90,

21
00:01:10,370 --> 00:01:13,790
and that would leave what's in the middle, which is the dark blue area,

22
00:01:13,790 --> 00:01:16,670
which we'll see is 65.6%.

23
00:01:16,670 --> 00:01:19,760
But since we can only do area to the left with this function,

24
00:01:19,760 --> 00:01:23,524
we have to sometimes take the differences of norm dist.

25
00:01:23,524 --> 00:01:26,120
So to solve this problem, okay,

26
00:01:26,120 --> 00:01:32,148
we're gonna take the area to the left of 120, so that's gonna be 120,

27
00:01:32,148 --> 00:01:36,538
mean is 100, sigma is 15, you need the word true.

28
00:01:39,620 --> 00:01:43,850
So that's the fraction of people with IQs less than or equal to 120.

29
00:01:43,850 --> 00:01:47,962
And now I'll take the fraction of people who have IQs less than or

30
00:01:47,962 --> 00:01:48,964
equal to 90.

31
00:01:53,425 --> 00:01:56,341
And that should duplicate that 65.6%.

32
00:01:56,341 --> 00:01:58,387
There's the formula right there.

33
00:01:58,387 --> 00:02:00,700
And remember, looking at this picture,

34
00:02:00,700 --> 00:02:02,490
that's what we got right here.

35
00:02:02,490 --> 00:02:05,980
The area to the left of 120 minus the area to the left of 90.

36
00:02:05,980 --> 00:02:07,561
Now one final example,

37
00:02:07,561 --> 00:02:11,365
what fraction of people have IQ's greater than 120?

38
00:02:11,365 --> 00:02:15,254
It would be the same as this, okay, cuz we're assuming the chance of

39
00:02:15,254 --> 00:02:18,891
exactly 120 is zero cuz we're assuming IQs are continuous.

40
00:02:18,891 --> 00:02:22,208
So I want the area to the right of 120.

41
00:02:22,208 --> 00:02:27,066
That's gotta be 1-Prob<120,

42
00:02:27,066 --> 00:02:30,259
or less than or equal to.

43
00:02:30,259 --> 00:02:38,273
So then, I can do =1-NORM DIST 120.

44
00:02:38,273 --> 00:02:40,920
Here's your mean, here's your sigma.

45
00:02:42,996 --> 00:02:43,714
Okay.

46
00:02:47,270 --> 00:02:52,178
So basically, the chance of being greater than or equal to 120.

47
00:02:54,718 --> 00:02:58,101
We've got that right here in this picture.

48
00:02:58,101 --> 00:02:59,625
9.1 percent.

49
00:02:59,625 --> 00:03:03,255
You can see right here the chance of being at least 120 is that

50
00:03:03,255 --> 00:03:05,270
crosshatched area.

51
00:03:05,270 --> 00:03:09,888
The area to the right of 120 is one minus the area to the left of 120,

52
00:03:09,888 --> 00:03:11,268
and we get 9.1%.

53
00:03:11,268 --> 00:03:15,065
In the next video, we'll show how to use the norm inverse function to get

54
00:03:15,065 --> 00:03:17,440
percentiles of the normal random variable.

