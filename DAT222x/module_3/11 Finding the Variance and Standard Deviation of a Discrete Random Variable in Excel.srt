0
00:00:00,410 --> 00:00:02,600
In this video, we'll teach you how to find the variance and

1
00:00:02,600 --> 00:00:05,540
standard deviation of a discrete random variable.

2
00:00:05,540 --> 00:00:08,700
Now the variance is simply the average squared deviation

3
00:00:08,700 --> 00:00:10,740
from the mean, where you take the average,

4
00:00:10,740 --> 00:00:13,330
weighting each outcome by the probabilities.

5
00:00:13,330 --> 00:00:16,560
So let's do two examples of variance and standard deviation.

6
00:00:16,560 --> 00:00:17,970
The standard deviation, of course,

7
00:00:17,970 --> 00:00:19,450
will be the square root of the variance,

8
00:00:19,450 --> 00:00:23,340
like it was with data in module one.

9
00:00:23,340 --> 00:00:25,580
So let's go back to our stock example.

10
00:00:25,580 --> 00:00:28,440
For every possible value here, okay,

11
00:00:28,440 --> 00:00:32,530
of the return on the stock we take the squared deviation from the mean.

12
00:00:32,530 --> 00:00:34,815
So here we would take the value.

13
00:00:37,860 --> 00:00:42,440
Minus the mean, and we need the dollar sign, the mean there.

14
00:00:45,690 --> 00:00:48,003
And we wanna square that, that's the caret over the two.

15
00:00:48,003 --> 00:00:53,969
So that would be 0.2 minus 0.02.

16
00:00:53,969 --> 00:00:55,490
We wanna copy that down.

17
00:00:57,730 --> 00:01:02,331
We can show you that formula with formula text.

18
00:01:05,168 --> 00:01:08,487
Okay, now we simply wanna weight those squared deviations by the mean

19
00:01:08,487 --> 00:01:10,020
by the probabilities.

20
00:01:10,020 --> 00:01:13,020
So again we wanna use some product, column C times column D.

21
00:01:15,730 --> 00:01:18,560
So we take some product to probabilities

22
00:01:18,560 --> 00:01:20,590
times the squared deviations from the mean.

23
00:01:20,590 --> 00:01:24,350
That takes 0.4 times this 0.0324 etc.

24
00:01:24,350 --> 00:01:27,100
And that will be out variance there, which is 0.0276.

25
00:01:27,100 --> 00:01:32,120
And we can see that formula, if I drag down the formula text.

26
00:01:33,190 --> 00:01:36,480
Now, the standard deviation is just the square root of that variance,

27
00:01:36,480 --> 00:01:37,960
SQRT is square root.

28
00:01:39,990 --> 00:01:44,295
So the standard deviation there would be about 16% there.

29
00:01:47,350 --> 00:01:50,358
Now let's find the variance and standard deviation for

30
00:01:50,358 --> 00:01:53,460
the number of dots showing when we toss a die.

31
00:01:53,460 --> 00:01:56,965
So the variance we need the square deviation again from the mean.

32
00:02:00,216 --> 00:02:03,620
So we get a 1, that's our outcome.

33
00:02:03,620 --> 00:02:07,740
And then we minus the mean there and we need to dollar sign that to copy

34
00:02:07,740 --> 00:02:12,340
this down, carat two squares it.

35
00:02:13,690 --> 00:02:18,881
Now we drag that down, and if you want

36
00:02:18,881 --> 00:02:24,750
to see the formula, spelled that wrong.

37
00:02:28,263 --> 00:02:32,540
See again it's the value minus the mean squared.

38
00:02:33,830 --> 00:02:38,069
And then to get the variance we would sum a product of these

39
00:02:38,069 --> 00:02:41,515
probabilities times the squared deviation,

40
00:02:41,515 --> 00:02:46,919
multiply the probability of each outcome times the squared deviation.

41
00:02:49,244 --> 00:02:50,683
2.91.

42
00:02:52,915 --> 00:02:55,719
And the standard deviation's the square root.

43
00:02:58,786 --> 00:02:59,750
About 1.7.

44
00:02:59,750 --> 00:03:03,920
Now, another way to get the variance which sometimes you can find easier.

45
00:03:03,920 --> 00:03:06,840
The variance of a random variable is the expected value of the square

46
00:03:06,840 --> 00:03:09,380
of the random variable minus the mean squared.

47
00:03:09,380 --> 00:03:11,270
So let's try that on the dice example and

48
00:03:11,270 --> 00:03:12,900
see that we get the same answer.

49
00:03:12,900 --> 00:03:15,950
So what's the expected value of the random variable squared?

50
00:03:15,950 --> 00:03:19,140
Where you square the random variable, square the number of dots.

51
00:03:20,680 --> 00:03:26,060
That's a new random variable which will be either 1,4,9,16, 25 or 36.

52
00:03:26,060 --> 00:03:29,020
Now if I want to find the expected

53
00:03:29,020 --> 00:03:31,370
value of that random variable squared.

54
00:03:31,370 --> 00:03:37,961
I can just multiply those squared values, Times our probabilities.

55
00:03:45,865 --> 00:03:48,700
Now the square there, oops I did not square that there.

56
00:03:48,700 --> 00:03:51,768
This did not get a 2, sorry about that.

57
00:03:51,768 --> 00:03:57,080
Easily fixed.

58
00:03:57,080 --> 00:04:02,180
Okay, now to get the variance, we would take the expected value of

59
00:04:02,180 --> 00:04:06,070
the random variable squared which is right there, minus the mean squared,

60
00:04:06,070 --> 00:04:09,280
and I know what the mean is, it's 3.5.

61
00:04:09,280 --> 00:04:14,220
And there's our 2.91 by a different approach.

62
00:04:15,320 --> 00:04:19,430
And sometimes it's easier to find the variances, we'll see later,

63
00:04:19,430 --> 00:04:23,100
by using this formula which looks more complicated but

64
00:04:23,100 --> 00:04:25,020
actually can be simpler.

65
00:04:25,020 --> 00:04:28,400
So now we know how to find the expected value, the variance, and

66
00:04:28,400 --> 00:04:30,400
standard deviation of random variable.

67
00:04:30,400 --> 00:04:32,900
We'll illustrate, basically,

68
00:04:32,900 --> 00:04:37,110
the meaning of these by tossing a die 10,000 times in the next video.

