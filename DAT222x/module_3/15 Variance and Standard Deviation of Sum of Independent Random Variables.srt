0
00:00:00,740 --> 00:00:05,300
For independent random variables X and Y, the variance of their sum or

1
00:00:05,300 --> 00:00:08,130
difference is the sum of their variances.

2
00:00:08,130 --> 00:00:11,300
Variances are added for both the sum and difference for

3
00:00:11,300 --> 00:00:14,240
two independent random variables, because the variation in

4
00:00:14,240 --> 00:00:17,980
each variable contributes to the variation in each case.

5
00:00:17,980 --> 00:00:22,394
Looking at our gaming example, here is the variance of the game A and

6
00:00:22,394 --> 00:00:23,580
game B.

7
00:00:23,580 --> 00:00:26,991
Summing them together, we get 26.31,

8
00:00:26,991 --> 00:00:32,710
which is the variance of the sum of these independent random variables.

9
00:00:32,710 --> 00:00:36,627
We take the square root to get the standard deviation, 5.13.

10
00:00:36,627 --> 00:00:40,040
Now, you are probably asking yourself,

11
00:00:40,040 --> 00:00:42,940
when am I ever going to use this?

12
00:00:42,940 --> 00:00:45,445
And it turns out this is actually something you're gonna use a lot.

13
00:00:45,445 --> 00:00:48,530
And we're gonna talk more about that in module four.

