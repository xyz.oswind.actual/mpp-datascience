0
00:00:00,750 --> 00:00:04,480
Now let's look at how you compute the variance of a discrete random variable

1
00:00:04,500 --> 00:00:08,189
it is very similar to calculating variance as we described in module 1

2
00:00:08,200 --> 00:00:12,509
where we subtract the observed value from the mean in this case the mean is

3
00:00:12,520 --> 00:00:14,896
the expected value and square that difference.

4
00:00:14,910 --> 00:00:16,448
However we go one step

5
00:00:16,460 --> 00:00:20,000
further by multiplying that value by its probability before

6
00:00:20,020 --> 00:00:23,520
summing across all outcomes possible in the sample space.

7
00:00:23,540 --> 00:00:28,448
Let's calculate the variance in our coin toss experiment first we subtract

8
00:00:28,460 --> 00:00:31,190
each of the possible values in the sample space

9
00:00:31,210 --> 00:00:33,264
from the mean of the random variable.

10
00:00:33,280 --> 00:00:36,630
Second we square that difference, third we multiply each

11
00:00:36,650 --> 00:00:38,992
of these squared deviations by  the probability of

12
00:00:39,010 --> 00:00:42,300
that outcome occurring. Finally we sum these weighted squared

13
00:00:42,320 --> 00:00:45,568
deviations to get a variance of 0.75.

14
00:00:45,660 --> 00:00:48,912
The standard deviation then is quite simply the square root

15
00:00:48,930 --> 00:00:52,910
of the variance 0.86.

