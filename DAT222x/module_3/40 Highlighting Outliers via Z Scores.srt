0
00:00:00,590 --> 00:00:02,600
So to close this lesson,

1
00:00:02,600 --> 00:00:06,360
let's highlight all the outliers on the demographics here.

2
00:00:06,360 --> 00:00:10,241
So remember, something will be an outlier if it's standardized if it's

3
00:00:10,241 --> 00:00:13,390
bigger than plus 2 or less than minus 2.

4
00:00:13,390 --> 00:00:16,370
So I can select either the standardized columns or

5
00:00:16,370 --> 00:00:18,960
the calculated columns, it doesn't matter.

6
00:00:18,960 --> 00:00:21,059
Then I do Home > Conditional Formatting.

7
00:00:22,830 --> 00:00:24,870
And then I can do Highlight Cells.

8
00:00:24,870 --> 00:00:27,390
I can say greater than 2.

9
00:00:27,390 --> 00:00:30,507
And we'll make that red, and

10
00:00:30,507 --> 00:00:36,622
then I can say less than -2, and let's make that green.

11
00:00:41,916 --> 00:00:45,108
And now we have all the outlier characteristics of cities

12
00:00:45,108 --> 00:00:45,980
highlighted.

13
00:00:45,980 --> 00:00:47,190
So if I look at age for

14
00:00:47,190 --> 00:00:51,160
instance, okay, we can see Honolulu is an older city.

15
00:00:51,160 --> 00:00:54,180
More than 2.5 standard deviations above average.

16
00:00:54,180 --> 00:00:56,260
Miami, not surprisingly, is an old city.

17
00:00:58,110 --> 00:01:01,970
And San Francisco, maybe more surprisingly, is an old city.

18
00:01:01,970 --> 00:01:05,950
And we can see the cities with a high percentage of Hispanics.

19
00:01:05,950 --> 00:01:11,188
We can see El Paso is a highly Hispanic City, Miami also.

20
00:01:13,130 --> 00:01:16,936
And there we have San Antonio is a highly Hispanic city.

21
00:01:16,936 --> 00:01:21,182
So I think the z scores together with the conditional formatting

22
00:01:21,182 --> 00:01:25,425
really sort of make the data come to life, and we can compare data

23
00:01:25,425 --> 00:01:29,850
across different variables that are really not in the same units.

24
00:01:29,850 --> 00:01:33,780
I mean age and unemployment rate are not in the same units, and

25
00:01:33,780 --> 00:01:35,640
income is not in the same units, but

26
00:01:35,640 --> 00:01:39,460
we can understand how unusual the demographic characteristics

27
00:01:39,460 --> 00:01:42,140
of various cities are by using z scores.

28
00:01:42,140 --> 00:01:44,730
And they're very important, and used all the time.

29
00:01:44,730 --> 00:01:48,450
Now, in our next module, we'll talk about sampling, estimation, and

30
00:01:48,450 --> 00:01:49,718
confidence intervals.

