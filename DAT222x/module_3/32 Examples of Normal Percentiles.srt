0
00:00:00,790 --> 00:00:03,090
So in this video, we're going to illustrate how to use the nor

1
00:00:03,090 --> 00:00:05,440
members function to calculate percentiles for

2
00:00:05,440 --> 00:00:06,740
a normal random variable.

3
00:00:06,740 --> 00:00:08,570
Suppose you're selling Prozac, and

4
00:00:08,570 --> 00:00:11,390
you have an estimate that during the next year.

5
00:00:11,390 --> 00:00:14,827
The average number of units sold will be 60,000 with

6
00:00:14,827 --> 00:00:16,620
a standard deviation of 5,000.

7
00:00:16,620 --> 00:00:20,190
And you feel the demand will be normally distributed.

8
00:00:20,190 --> 00:00:23,650
You wanna have a 1% chance of running out, how much did you make?

9
00:00:23,650 --> 00:00:26,670
Well, you should make the 99th percentile, so there's only a 1%

10
00:00:26,670 --> 00:00:30,590
chance that people want more than basically what you made.

11
00:00:30,590 --> 00:00:34,390
So we can use that nor members function to find the 99th percentile

12
00:00:34,390 --> 00:00:36,690
basically for the demand for Prozac.

13
00:00:36,690 --> 00:00:37,880
Now using your common sense,

14
00:00:37,880 --> 00:00:40,850
you better get an answer that's more than the mean.

15
00:00:40,850 --> 00:00:43,010
Okay, if you wanna have small chance of running out,

16
00:00:43,010 --> 00:00:47,440
you better make more than the mean, so we could do norm inverse.

17
00:00:47,440 --> 00:00:50,576
I can double click to put that function in, okay,

18
00:00:50,576 --> 00:00:52,676
so I want the 99th percentile.

19
00:00:52,676 --> 00:00:57,323
The mean is 60 and this is 5, the units could millions or thousands,

20
00:00:57,323 --> 00:01:00,130
it doesn't matter, so I get 71.63.

21
00:01:00,130 --> 00:01:02,337
Okay, now again we have a picture for this.

22
00:01:05,570 --> 00:01:07,258
Let's show you that formula.

23
00:01:15,143 --> 00:01:18,867
There we go, okay, so the 99th percentile, with the mean of 60 and

24
00:01:18,867 --> 00:01:21,760
standard deviation of 5 let's look down here.

25
00:01:21,760 --> 00:01:25,970
The mean is 60, you can see that's where the normal distribution peaks.

26
00:01:25,970 --> 00:01:29,500
The standard deviation is 5 so most of it is between 50 and 70.

27
00:01:29,500 --> 00:01:34,071
And if you go to 71.63 the area to the right of that is 1%,

28
00:01:34,071 --> 00:01:36,678
the area to the left of it is 99%.

29
00:01:36,678 --> 00:01:39,361
So that means that's the 99th percentile,

30
00:01:39,361 --> 00:01:41,090
now this is another example.

31
00:01:41,090 --> 00:01:44,603
Lets suppose in a city with a mean income of 30,000 and

32
00:01:44,603 --> 00:01:46,509
standard deviation 8000.

33
00:01:46,509 --> 00:01:50,397
That is basically about 10% of the people get through stance or

34
00:01:50,397 --> 00:01:52,160
some form of welfare.

35
00:01:52,160 --> 00:01:53,860
So, what would be the carry over for the welfare?

36
00:01:53,860 --> 00:01:57,960
That will be the 10% or this mean income are normally distributed.

37
00:01:57,960 --> 00:02:02,320
So there you would go NORM.INV and say,

38
00:02:02,320 --> 00:02:07,347
0.1 for the tenth percentile, the mean is 30,000.

39
00:02:07,347 --> 00:02:10,700
Standard deviation 8,000,

40
00:02:10,700 --> 00:02:15,700
like that, and you can see the formula right there.

41
00:02:18,780 --> 00:02:21,260
And again, if we had a picture of that,

42
00:02:21,260 --> 00:02:24,140
we could look at a normal random variable with a mean of 30,000.

43
00:02:24,140 --> 00:02:25,927
So it peaks at 30,000,

44
00:02:25,927 --> 00:02:31,018
standard deviation 8,000 means it's mostly between 14 and 46,000.

45
00:02:31,018 --> 00:02:38,907
And if you look at 19,748 there is a 10% chance of being less than that,

46
00:02:38,907 --> 00:02:43,998
and that's what we got I believe, 19,748.

47
00:02:43,998 --> 00:02:46,718
So now in the next lesson we are going to talk about,

48
00:02:46,718 --> 00:02:48,239
the central limit theorem,

49
00:02:48,239 --> 00:02:51,818
which is probably about the most important result in statistics.

50
00:02:51,818 --> 00:02:53,920
It says basically, when you have 30 or

51
00:02:53,920 --> 00:02:57,880
more random variables, even if they are not normal,

52
00:02:57,880 --> 00:03:01,930
the sum of them will be very close to a normal random variable.

53
00:03:01,930 --> 00:03:05,880
This enables us to solve an incredible amount of important

54
00:03:05,880 --> 00:03:07,660
probability problems in the real world.

