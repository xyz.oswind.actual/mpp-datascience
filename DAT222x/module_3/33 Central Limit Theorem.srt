0
00:00:00,520 --> 00:00:02,370
Let's talk about the central limit theorem.

1
00:00:02,370 --> 00:00:05,080
It concerns the sampling distributions of the sample means

2
00:00:05,080 --> 00:00:08,268
that could be obtained from various random samples of a population.

3
00:00:08,268 --> 00:00:10,102
Think of it this way.

4
00:00:10,102 --> 00:00:12,853
Each time you select a sample from a population,

5
00:00:12,853 --> 00:00:15,840
you obtain a slightly different mean and variance.

6
00:00:15,840 --> 00:00:18,960
What the central limit theorem says though is that the sample size

7
00:00:18,960 --> 00:00:19,790
increases.

8
00:00:19,790 --> 00:00:22,910
The sample means that could be obtained from these various samples

9
00:00:22,910 --> 00:00:25,940
tend to be normally distributed around the population mean.

10
00:00:25,940 --> 00:00:28,990
And its variance shrinks in size as n increases.

11
00:00:28,990 --> 00:00:32,330
Thus, the mean of the sample means should be approximately

12
00:00:32,330 --> 00:00:35,620
equal to the mean of the population and the variance approximately

13
00:00:35,620 --> 00:00:39,010
equal to the population variance, divided by the sample size.

14
00:00:39,010 --> 00:00:40,560
Why do we divide by the sample size?

15
00:00:40,560 --> 00:00:42,510
Because the normality assumption,

16
00:00:42,510 --> 00:00:45,842
the central limit theorem is based on sample size.

17
00:00:45,842 --> 00:00:49,760
At its simplest, the central limit theorem says the bigger the sample,

18
00:00:49,760 --> 00:00:53,630
the more likely the distribution is to be normal.

19
00:00:53,630 --> 00:00:57,310
Even cooler, this tendency towards normality of the distribution of

20
00:00:57,310 --> 00:01:00,450
the sample means happens regardless of the initial shape of

21
00:01:00,450 --> 00:01:03,430
the distribution if the sample is large enough.

22
00:01:03,430 --> 00:01:05,470
So the question you need to be asking is,

23
00:01:05,470 --> 00:01:07,430
how large is large enough?

24
00:01:07,430 --> 00:01:11,920
Generally speaking, a sample of size of 30 or more, it's considered

25
00:01:11,920 --> 00:01:15,680
to be large enough for the central limit theorem to take effect.

26
00:01:15,680 --> 00:01:17,720
The closer the population distribution is,

27
00:01:17,720 --> 00:01:19,170
to the normal distribution, however,

28
00:01:19,170 --> 00:01:23,050
the smaller that sample size is needed to demonstrate this theorem.

29
00:01:23,050 --> 00:01:26,210
Populations that are heavily skewed, or have several modes,

30
00:01:26,210 --> 00:01:29,110
may require larger sample sizes.

31
00:01:29,110 --> 00:01:30,470
Why does this matter?

32
00:01:30,470 --> 00:01:33,450
Well, it's impractical to collect all of the data

33
00:01:33,450 --> 00:01:35,320
from an entire population, so

34
00:01:35,320 --> 00:01:38,750
we rely on samples to draw conclusions about the population.

35
00:01:38,750 --> 00:01:41,922
The central limit theorem gives us confidence in the statistics

36
00:01:41,922 --> 00:01:45,269
we obtain from our samples as being good estimates of our population

37
00:01:45,269 --> 00:01:45,983
parameters.

