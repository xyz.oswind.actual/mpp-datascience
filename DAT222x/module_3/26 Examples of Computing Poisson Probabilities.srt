0
00:00:00,430 --> 00:00:03,990
Okay, let's illustrate the POISSON random variable

1
00:00:03,990 --> 00:00:05,510
with the following example.

2
00:00:05,510 --> 00:00:10,308
Let's suppose a technical support center that receives calls

3
00:00:10,308 --> 00:00:14,300
receives on the average, 30 calls per hour.

4
00:00:14,300 --> 00:00:15,020
We wanna know,

5
00:00:15,020 --> 00:00:18,430
what's the chance they'll get exactly 60 calls in 2 hours.

6
00:00:18,430 --> 00:00:21,950
The chance they'll get less than or equal to 60 calls in 2 hours.

7
00:00:21,950 --> 00:00:24,510
And the chance they'll get between 50 and 100 calls,

8
00:00:24,510 --> 00:00:26,350
inclusive, in 2 hours.

9
00:00:26,350 --> 00:00:29,080
So we'll use that POISSON dysfunction.

10
00:00:29,080 --> 00:00:31,019
So, if they get 30 calls per hour,

11
00:00:31,019 --> 00:00:33,595
what's the average number of calls in 2 hours?

12
00:00:33,595 --> 00:00:35,550
Well, it'll be 30 times 2, or 60.

13
00:00:35,550 --> 00:00:37,990
All we need to know is the average.

14
00:00:37,990 --> 00:00:45,880
So the probability is 60 calls in 2 hours, we can do POISSON, .DIST.

15
00:00:45,880 --> 00:00:50,310
Okay, now x is 60, cuz we want chance of 60 calls.

16
00:00:50,310 --> 00:00:53,790
The mean is 60, and remember, we use 0 to

17
00:00:53,790 --> 00:00:57,970
get what's called the probability mass function, or a false, okay?

18
00:00:57,970 --> 00:01:01,600
And there's a chance of exactly 60 as the mass function.

19
00:01:01,600 --> 00:01:04,280
Okay, if we want less than or equal to 60, it's what's called,

20
00:01:04,280 --> 00:01:07,790
we haven't used the term but it's really the cumulative mass function.

21
00:01:07,790 --> 00:01:10,080
So there we would get, basically,

22
00:01:10,080 --> 00:01:13,190
a 5% chance of exactly 60 calls in two hours.

23
00:01:13,190 --> 00:01:15,650
Now for less than or equal to 60 calls,

24
00:01:15,650 --> 00:01:18,320
all we have to do is change that 0 to a 1.

25
00:01:18,320 --> 00:01:21,966
The third problem will be a harder problem.

26
00:01:21,966 --> 00:01:26,561
Okay so we want 60, the mean is 60 and

27
00:01:26,561 --> 00:01:30,344
we want basically we can put a 1 or

28
00:01:30,344 --> 00:01:34,416
a true there, it doesn't matter.

29
00:01:34,416 --> 00:01:37,584
So we get there as a 53% chance there'll be 60 or

30
00:01:37,584 --> 00:01:39,110
fewer calls in 2 hours.

31
00:01:39,110 --> 00:01:41,998
Now the chance of between 50 and 100 calls that's tricky.

32
00:01:41,998 --> 00:01:45,530
Cuz the POISSON.DIST only does the probability of less than or

33
00:01:45,530 --> 00:01:46,720
equal to something.

34
00:01:46,720 --> 00:01:50,670
So if I want the chance of between 50 and 100 calls, inclusive.

35
00:01:52,420 --> 00:01:56,010
I've gotta take the chance of less than or equal to 100 and

36
00:01:56,010 --> 00:02:00,709
subtract off the chance of less than or equal to 49.

37
00:02:01,920 --> 00:02:03,734
Because what does that leave me with?

38
00:02:03,734 --> 00:02:08,211
See, if I go 0, 1, 2,

39
00:02:08,211 --> 00:02:14,130
we'll not type everything here.

40
00:02:16,210 --> 00:02:19,241
I go 49, 50, 51, 52, 99, 100.

41
00:02:19,241 --> 00:02:25,130
What I want is basically everything between 50 and 100.

42
00:02:25,130 --> 00:02:29,240
So if I do POISSON.DIS 100, I get everything that's less or

43
00:02:29,240 --> 00:02:30,390
equal to 100.

44
00:02:30,390 --> 00:02:33,830
And if I do POISSON.DIS 49, I take away 0 through 49.

45
00:02:33,830 --> 00:02:35,740
That leaves 50 through 100.

46
00:02:35,740 --> 00:02:38,500
Now if you took away less from equal to 50,

47
00:02:38,500 --> 00:02:40,410
You would only get 51 through 100.

48
00:02:40,410 --> 00:02:42,010
People have trouble with that.

49
00:02:42,010 --> 00:02:45,620
But so I'm gonna start out with a chance of less than or equal to 100.

50
00:02:45,620 --> 00:02:51,660
So I put 100 there, the mean is 60, and I could use a true, whatever.

51
00:02:52,860 --> 00:02:56,531
Okay, that's less or equal to 60,

52
00:02:56,531 --> 00:03:00,559
minus the chance of less or equal to 49.

53
00:03:00,559 --> 00:03:02,303
Put a 49 there.

54
00:03:02,303 --> 00:03:04,840
The mean is 60 and again, I put a true.

55
00:03:04,840 --> 00:03:08,320
So the first term is the chance of 0 through 100 calls.

56
00:03:08,320 --> 00:03:11,100
The second term is 0 through 49 calls.

57
00:03:11,100 --> 00:03:15,904
So the difference there should be 50 through 100 calls, which is 92%.

58
00:03:16,950 --> 00:03:20,470
That concludes our examples of Poisson probabilities.

59
00:03:20,470 --> 00:03:23,450
In the next video, we'll quickly tell you what the mean variants and

60
00:03:23,450 --> 00:03:26,390
standard deviation of the Poisson random variable are.

