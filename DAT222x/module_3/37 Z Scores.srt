0
00:00:00,012 --> 00:00:03,424
A z-score or standard score, indicates how many

1
00:00:03,424 --> 00:00:05,980
standard deviations something is from the mean.

2
00:00:05,980 --> 00:00:09,470
It's a simple calculation that involves subtracting the population

3
00:00:09,470 --> 00:00:11,430
mean mu, from the observed value x and

4
00:00:11,430 --> 00:00:13,435
then dividing by standard deviation.

5
00:00:13,435 --> 00:00:17,270
z-scores are important because they allow us to do several things.

6
00:00:17,270 --> 00:00:19,940
First, a z-score provides a frame of reference for

7
00:00:19,940 --> 00:00:22,670
how a value compares to a population mean.

8
00:00:22,670 --> 00:00:25,360
If you know a product was rated by customers as a 70, and

9
00:00:25,360 --> 00:00:29,960
the average is 55, you might assume that the product was highly rated.

10
00:00:29,960 --> 00:00:32,640
However, without knowing the variability in the ratings,

11
00:00:32,640 --> 00:00:36,010
you can't know for sure if it really was highly rated.

12
00:00:36,010 --> 00:00:38,000
If the standard deviation of the ratings was 10,

13
00:00:38,000 --> 00:00:41,630
then the product was highly rated, with a z-score of 1.5.

14
00:00:41,630 --> 00:00:46,680
Meaning that the rating was 1.5 standard deviations above the mean.

15
00:00:46,680 --> 00:00:50,340
However, if the standard deviation is 20, then the product was rated

16
00:00:50,340 --> 00:00:54,640
good, but perhaps not great, with a z-score of 0.75.

17
00:00:54,640 --> 00:00:57,490
Second, we can determine what proportion of the population has

18
00:00:57,490 --> 00:01:00,540
scores above and below, the one of interest.

19
00:01:00,540 --> 00:01:03,260
Let's be a little more precise than we were when we introduced this

20
00:01:03,260 --> 00:01:04,780
concept in module one.

21
00:01:04,780 --> 00:01:10,210
In a normal distribution, 68.26% of the data lie between plus 1 and

22
00:01:10,210 --> 00:01:12,275
minus 1 standard deviations.

23
00:01:12,275 --> 00:01:15,970
95.45% within plus or minus two.

24
00:01:15,970 --> 00:01:20,010
And 99.73% within plus or minus three.

25
00:01:20,010 --> 00:01:22,000
If someone has a z-score of one,

26
00:01:22,000 --> 00:01:26,760
that means that 84.13% of the other scores are below.

27
00:01:26,760 --> 00:01:27,990
And 15.87% are above.

28
00:01:27,990 --> 00:01:33,844
Does a z-score of 1.5 would have 93.32% below and

29
00:01:33,844 --> 00:01:38,180
6.68% of the scores would be above.

30
00:01:38,180 --> 00:01:39,890
I know you're wondering.

31
00:01:39,890 --> 00:01:42,180
Why I should be in so precise?

32
00:01:42,180 --> 00:01:45,800
We will be leveraging z scores in a module four,

33
00:01:45,800 --> 00:01:47,830
in a way that requires this precision.

34
00:01:47,830 --> 00:01:49,710
Stay tuned, you'll see.

35
00:01:49,710 --> 00:01:52,660
Finally, you can use z-scores to compare values obtained

36
00:01:52,660 --> 00:01:54,490
from different samples and distributions,

37
00:01:54,490 --> 00:01:58,420
because they put all the values on a common metric, or yard stick.

38
00:01:58,420 --> 00:01:59,030
In other words,

39
00:01:59,030 --> 00:02:02,495
you can compare apples to oranges as the saying goes.

40
00:02:02,495 --> 00:02:05,690
Z-scores allow you to compare company financial health, customer

41
00:02:05,690 --> 00:02:09,765
satisfaction, defect rates, even if they are in different scales.

42
00:02:09,765 --> 00:02:12,540
Z-scores have the power to level the playing field.

43
00:02:12,540 --> 00:02:14,590
If someone claims that something is better or

44
00:02:14,590 --> 00:02:18,410
worse than something else, look to the z-scores to know for certain.

45
00:02:18,410 --> 00:02:20,740
By the way, ever heard of six sigma?

46
00:02:20,740 --> 00:02:23,420
It's based on the concept of z-scores where six sigma

47
00:02:23,420 --> 00:02:26,330
representing six standard deviations from the mean.

48
00:02:26,330 --> 00:02:28,463
The higher the number the fewer the defects.

