0
00:00:00,710 --> 00:00:03,920
Random variables can be discrete or continuous.

1
00:00:03,920 --> 00:00:08,300
Discrete variables can take on only certain values within a range.

2
00:00:08,300 --> 00:00:10,860
Suppose for example, that we flip a coin three times and

3
00:00:10,860 --> 00:00:12,300
count the number of heads.

4
00:00:12,300 --> 00:00:14,622
The number of heads will be a value between zero and

5
00:00:14,622 --> 00:00:17,786
the total number of times that we flip the coin in this case, three.

6
00:00:17,786 --> 00:00:20,750
Further, the number of heads can only be a whole number zero, one,

7
00:00:20,750 --> 00:00:21,480
two, or three.

8
00:00:21,480 --> 00:00:26,040
And can never be a fraction because the number of heads results from

9
00:00:26,040 --> 00:00:31,140
a random process, flipping a coin, it is a discreet random variable.

10
00:00:31,140 --> 00:00:34,270
Continuous variables in contrast can take on any value in

11
00:00:34,270 --> 00:00:35,670
a range of values.

12
00:00:35,670 --> 00:00:38,500
For example, suppose we randomly select an individual from

13
00:00:38,500 --> 00:00:41,460
a population, then we measure the weight of that person.

14
00:00:41,460 --> 00:00:44,770
In theory, his or her weight can take on any value between zero and

15
00:00:44,770 --> 00:00:45,720
infinity.

16
00:00:45,720 --> 00:00:47,690
I hope not, but it's possible.

17
00:00:47,690 --> 00:00:49,900
So weight is a continuous variable.

18
00:00:49,900 --> 00:00:52,430
Because we are randomly selecting individuals,

19
00:00:52,430 --> 00:00:55,270
weight is a continuous random variable.

20
00:00:55,270 --> 00:00:57,470
Other example includes things like time,

21
00:00:57,470 --> 00:01:01,190
temperature, the actual amount of Pepsi in a 12 ounce Pepsi can.

22
00:01:01,190 --> 00:01:03,130
It's probably not exactly 12 ounces.

