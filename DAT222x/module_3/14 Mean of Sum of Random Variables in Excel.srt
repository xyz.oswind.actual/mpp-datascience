0
00:00:00,021 --> 00:00:03,733
In this video, we'll learn quickly how to find the expected value of

1
00:00:03,733 --> 00:00:05,340
the sum of random variables.

2
00:00:05,340 --> 00:00:08,800
And you'll see later in the course that many times we have to add

3
00:00:08,800 --> 00:00:11,710
up random variables to solve important problems.

4
00:00:11,710 --> 00:00:15,100
So let's suppose we have random variables X1, X2, through Xn, so

5
00:00:15,100 --> 00:00:16,060
we have n of them.

6
00:00:16,060 --> 00:00:17,730
They don't have to be independent.

7
00:00:17,730 --> 00:00:19,870
Independence will be important in the next video.

8
00:00:19,870 --> 00:00:21,700
So these could be any random variables.

9
00:00:21,700 --> 00:00:25,000
What's the mean or expected value of the sum of random variables?

10
00:00:25,000 --> 00:00:26,660
So if I want the expected value or

11
00:00:26,660 --> 00:00:30,090
the mean of adding up n random variables, all I have to do is

12
00:00:30,090 --> 00:00:32,960
add up the expected value of each of the individual ones.

13
00:00:32,960 --> 00:00:35,030
Boy does that make our life easy.

14
00:00:35,030 --> 00:00:37,039
So suppose I toss a die a hundred times.

15
00:00:37,039 --> 00:00:39,500
On average, how many dots will show up?

16
00:00:39,500 --> 00:00:41,913
Well, you know on each die 3.5 dots will show up.

17
00:00:41,913 --> 00:00:43,520
We just talked about that.

18
00:00:43,520 --> 00:00:48,447
So the expected value on 100 dice would be 3.5 plus 3.5 100 times.

19
00:00:48,447 --> 00:00:53,270
And so the answer there would be 100 times 3.5.

20
00:00:53,270 --> 00:00:54,436
350.

21
00:00:54,436 --> 00:00:57,803
One more example, toss a coin 100 times on average.

22
00:00:57,803 --> 00:00:58,820
How many heads will show up?

23
00:00:58,820 --> 00:01:01,224
Well you can guess the answer to that, I think.

24
00:01:01,224 --> 00:01:04,953
It's 50/50 a head will show up on one coin toss.

25
00:01:04,953 --> 00:01:05,950
What's the expected value?

26
00:01:08,302 --> 00:01:10,989
You would sum a product.

27
00:01:10,989 --> 00:01:15,138
You'd either get 0 or 1 heads with this probability.

28
00:01:15,138 --> 00:01:17,888
So on the average you'll see 0.5 a head on one coin toss,

29
00:01:17,888 --> 00:01:19,650
although you never see 0.5 a head.

30
00:01:19,650 --> 00:01:26,307
But on 100 coin tosses, The average number of heads to show up

31
00:01:26,307 --> 00:01:31,977
would just be 0.5 plus 0.5, 100 times, which would be 50.

32
00:01:31,977 --> 00:01:34,953
So in the next video, we'll talk about how to find the variance and

33
00:01:34,953 --> 00:01:37,080
standard deviation for random variables, but

34
00:01:37,080 --> 00:01:39,917
they have to be independent or it's a very difficult problem.

35
00:01:39,917 --> 00:01:43,350
So we'll assume the random variables we're adding up are independent.

