0
00:00:00,500 --> 00:00:03,190
Now let's talk about continuous random variables.

1
00:00:03,190 --> 00:00:04,990
So when is a random variable continuous?

2
00:00:04,990 --> 00:00:08,320
If it can assume any value from one or more intervals of numbers.

3
00:00:08,320 --> 00:00:09,940
So let's look at some examples.

4
00:00:09,940 --> 00:00:13,303
The market share for new product can be any number between 0 and 1 or

5
00:00:13,303 --> 00:00:14,640
0 and 100%.

6
00:00:14,640 --> 00:00:16,670
So that's an interval, so that's continuous.

7
00:00:16,670 --> 00:00:19,190
The height of a male can be, let's say, I don't know,

8
00:00:19,190 --> 00:00:23,890
between 20 inches, and maybe 9 feet tall, I don't know, 108 inches.

9
00:00:23,890 --> 00:00:27,521
But you could be any number, not just 21 inches,

10
00:00:27,521 --> 00:00:32,433
22 inches, you could be 21.5, you could be 61.5553.

11
00:00:32,433 --> 00:00:36,637
The time it takes to process you at the airline ticket counter,

12
00:00:36,637 --> 00:00:40,682
that could be anything between 1 second, 5.8 seconds,

13
00:00:40,682 --> 00:00:45,160
seems to me it's like 605 seconds for me if I'm lucky.

14
00:00:45,160 --> 00:00:48,330
And the definition of the longest line is the line that I'm in.

15
00:00:48,330 --> 00:00:51,160
Okay, so how do you describe a continuous random variable?

16
00:00:51,160 --> 00:00:52,800
We have the probability of mass function for

17
00:00:52,800 --> 00:00:55,160
discrete random variables, but that doesn't work for

18
00:00:55,160 --> 00:00:56,950
continuous random variables.

19
00:00:56,950 --> 00:00:59,640
A continuous random variable is described by a probability

20
00:00:59,640 --> 00:01:03,182
density function or PDF, not like Adobe PDF.

21
00:01:03,182 --> 00:01:09,890
Okay, the height of a PDF tells us the relative likelihood

22
00:01:09,890 --> 00:01:13,200
that a continuous random variable assumes different possible values.

23
00:01:13,200 --> 00:01:16,382
So what are some properties of PDF, so probability density functions?

24
00:01:16,382 --> 00:01:18,280
Then, we'll look at two examples.

25
00:01:18,280 --> 00:01:18,808
First of all,

26
00:01:18,808 --> 00:01:21,185
a probability density function must always be non-negative,

27
00:01:21,185 --> 00:01:22,940
just like a probability.

28
00:01:22,940 --> 00:01:26,590
The total area under our probability density function equals one, and

29
00:01:26,590 --> 00:01:28,360
this is so important.

30
00:01:28,360 --> 00:01:29,790
If you wanna get probabilities for

31
00:01:29,790 --> 00:01:33,990
a continuous random variable that is between a and b you take the area

32
00:01:33,990 --> 00:01:37,280
under the probability density function between a and b.

33
00:01:37,280 --> 00:01:41,410
So let's look at two examples of probability density functions,

34
00:01:41,410 --> 00:01:45,280
again courtesy of Palisade's @RISK package, we have some nice graphs.

35
00:01:45,280 --> 00:01:48,228
Suppose market share is equally likely to be anywhere between

36
00:01:48,228 --> 00:01:49,450
0 and 1.

37
00:01:49,450 --> 00:01:51,540
So the PDF would have the same height everywhere.

38
00:01:51,540 --> 00:01:55,080
That's called the uniform PDF because it's equally likely to be

39
00:01:55,080 --> 00:01:56,870
anywhere between zero and one.

40
00:01:56,870 --> 00:01:58,770
Now, the area under this is the height of one.

41
00:01:58,770 --> 00:02:02,530
It's a rectangle, the area is one times one for an area of one, and

42
00:02:02,530 --> 00:02:04,300
probability is area.

43
00:02:04,300 --> 00:02:08,039
So what would be the chance your market share's between 20% and 80%?

44
00:02:08,039 --> 00:02:12,555
You would take the area under this rectangle, between 20% and 80%,

45
00:02:12,555 --> 00:02:16,854
and that would 0.6, or 60% area, cuz the base would be 0.6 and

46
00:02:16,854 --> 00:02:21,930
the height would be 1, and the area of a rectangle is base times height.

47
00:02:21,930 --> 00:02:25,454
Now, suppose you look at the height of an American male which follows

48
00:02:25,454 --> 00:02:27,400
basically a normal random variable,

49
00:02:27,400 --> 00:02:31,160
which we'll talk about later, on average, 69.4 inches.

50
00:02:31,160 --> 00:02:32,840
So the density will look something like this.

51
00:02:32,840 --> 00:02:34,850
You'll see more about this later.

52
00:02:34,850 --> 00:02:38,565
But basically what fraction of American men are between 65 and

53
00:02:38,565 --> 00:02:39,673
74 inches tall?

54
00:02:39,673 --> 00:02:43,786
Well that would be the fraction of the area under this probability

55
00:02:43,786 --> 00:02:47,766
density function between 65 and 74, it's about 89%.

56
00:02:47,766 --> 00:02:52,207
Okay, and again, the density function gives you, dense can mean

57
00:02:52,207 --> 00:02:56,920
not very smart but here it means how massive is the probability.

58
00:02:56,920 --> 00:03:01,520
If you look at the density at 66 inches and the density at 69 inches,

59
00:03:01,520 --> 00:03:05,760
basically 66 inches has roughly half the density of 69 inches.

60
00:03:05,760 --> 00:03:09,608
This density curve has really roughly half the height

61
00:03:09,608 --> 00:03:11,180
at 66 as 69.4.

62
00:03:11,180 --> 00:03:15,664
That means roughly half as many men are around 66.1

63
00:03:15,664 --> 00:03:19,378
inches tall as around 69.4 inches tall.

64
00:03:19,378 --> 00:03:22,099
Now, one thing you have to understand about continuous random

65
00:03:22,099 --> 00:03:22,668
variables.

66
00:03:22,668 --> 00:03:26,840
Suppose I ask you the chance of a man being exactly six feet tall.

67
00:03:26,840 --> 00:03:29,900
So I would always ask this to my class and a bunch of hands go up.

68
00:03:29,900 --> 00:03:32,720
Well, basically nobody is exactly six feet tall.

69
00:03:32,720 --> 00:03:36,669
You could be between 5'11'' and a half and 6 feet and a half and

70
00:03:36,669 --> 00:03:39,250
you consider yourself 6 feet tall.

71
00:03:39,250 --> 00:03:44,220
But to be exactly six feet tall, you need to be 6.00, lots of zeroes,

72
00:03:44,220 --> 00:03:48,030
feet tall and nobody is really exactly that feet tall.

73
00:03:48,030 --> 00:03:50,170
See, if you look at being six feet tall,

74
00:03:50,170 --> 00:03:53,760
the area of a rectangle with the base of zero is equal to zero.

75
00:03:53,760 --> 00:03:56,620
So that's why nobody can be exactly six feet tall.

76
00:03:56,620 --> 00:03:58,190
So, for a continuous random variable,

77
00:03:58,190 --> 00:04:01,770
the chance of assuming any particular value is zero.

78
00:04:01,770 --> 00:04:04,980
That's not true for a discreet random variable.

79
00:04:04,980 --> 00:04:05,650
In the next video,

80
00:04:05,650 --> 00:04:09,240
we're gonna talk about a related topic, independent random variables.

