0
00:00:00,700 --> 00:00:03,450
So, in this video, we'll discuss how to find the mean, variance and

1
00:00:03,450 --> 00:00:06,430
standard deviation of the binomial random variable.

2
00:00:06,430 --> 00:00:09,390
We'll use what we learned about means and variance and

3
00:00:09,390 --> 00:00:12,130
some standard deviations of sums of random variables.

4
00:00:12,130 --> 00:00:15,939
So on one trial the mean number's success is its P because with

5
00:00:15,939 --> 00:00:18,657
probably p you get one success and 1- p0.

6
00:00:18,657 --> 00:00:21,380
Now, what's the variance on one binomial trial?

7
00:00:21,380 --> 00:00:23,267
With probably p, you get a 1.

8
00:00:23,267 --> 00:00:24,730
You get one success.

9
00:00:24,730 --> 00:00:27,138
Subtract off the mean, and you square that.

10
00:00:27,138 --> 00:00:29,800
And with probably 1- p you get zero successes.

11
00:00:29,800 --> 00:00:31,673
Subtract off the mean, and square that.

12
00:00:31,673 --> 00:00:33,084
And if you simplify that,

13
00:00:33,084 --> 00:00:36,490
you get the variance is p times 1- p on one trial.

14
00:00:36,490 --> 00:00:39,920
And then the variance on n trials, remember you can get the variance

15
00:00:39,920 --> 00:00:43,270
of the sum of random variables by adding up the individual variances.

16
00:00:43,270 --> 00:00:45,271
You just multiply this by n.

17
00:00:45,271 --> 00:00:47,960
And then the standard deviation would be the square root.

18
00:00:47,960 --> 00:00:50,680
Perhaps an easier way to get the variance on one trial is to

19
00:00:50,680 --> 00:00:54,130
use our formula the variance of a random variable is the expected

20
00:00:54,130 --> 00:00:57,950
value of the random variable squared minus the mean squared.

21
00:00:57,950 --> 00:01:02,809
Because what's the expected value of the number of successes

22
00:01:02,809 --> 00:01:03,935
on one trial?

23
00:01:03,935 --> 00:01:07,920
Well, with probably p you get one success and one squared as one.

24
00:01:07,920 --> 00:01:11,270
So this would be, expected value of x squared is just p, and

25
00:01:11,270 --> 00:01:12,170
what's the mean squared?

26
00:01:12,170 --> 00:01:13,380
It's p squared.

27
00:01:13,380 --> 00:01:16,420
So you have p- p squared, the same thing.

28
00:01:16,420 --> 00:01:19,522
Okay, so again, the variance on n trials will be n times

29
00:01:19,522 --> 00:01:23,352
the probability success times 1 minus the probability of success,

30
00:01:23,352 --> 00:01:26,700
and the standard deviation is the square root of that.

31
00:01:26,700 --> 00:01:28,960
Again, we're looking at the number of successes.

32
00:01:28,960 --> 00:01:32,760
So, we toss a coin a hundred times, what's the mean and

33
00:01:32,760 --> 00:01:35,000
standard deviation of the number of hits?

34
00:01:35,000 --> 00:01:39,562
Well, n is a 100, that's the number of trials, probably success is .5,

35
00:01:39,562 --> 00:01:41,410
so on the average how many hits?

36
00:01:41,410 --> 00:01:44,380
So a 100 times .5 or a 50, what's the variance?

37
00:01:46,310 --> 00:01:49,701
It's gonna be 100 times .5 times 1- .5,

38
00:01:49,701 --> 00:01:54,705
that's a 100 times .5 is 50 times 1- .5, half of 50 is 25,

39
00:01:54,705 --> 00:01:59,263
and the standard deviation would be the square root of 25 or a 5.

40
00:01:59,263 --> 00:02:00,516
Now, in the next video,

41
00:02:00,516 --> 00:02:03,740
we'll start learning about a new random variable, the Poisson

42
00:02:03,740 --> 00:02:07,337
random variable, which has many exciting real-life applications.

