0
00:00:00,580 --> 00:00:03,380
So now let's give you a powerful demonstration of the central

1
00:00:03,380 --> 00:00:04,300
limit theorem.

2
00:00:04,300 --> 00:00:06,690
Let's toss a die 30 times.

3
00:00:06,690 --> 00:00:09,500
Now the die tosses will be independent.

4
00:00:09,500 --> 00:00:12,650
Now as each die toss, the number of dots showing normal.

5
00:00:12,650 --> 00:00:15,610
No, it's equally likely to be between one and six.

6
00:00:15,610 --> 00:00:17,610
We can do a RANDBETWEEN function to do that.

7
00:00:17,610 --> 00:00:20,480
Whenever you do RANDBETWEEN between 1 through 6,

8
00:00:20,480 --> 00:00:24,740
1,6 it is equally likely to give a 1 through 6.

9
00:00:24,740 --> 00:00:28,670
So I've got 30 of those and I add those together.

10
00:00:28,670 --> 00:00:30,440
And then using something called a data table,

11
00:00:30,440 --> 00:00:33,504
I trick Excel into doing this 10,000 times.

12
00:00:33,504 --> 00:00:37,310
And then I count how many times I get a total less than or

13
00:00:37,310 --> 00:00:40,050
equal to 90, between 91 and 95, etc.

14
00:00:40,050 --> 00:00:44,103
For example, I got 635 times out of 10,000,

15
00:00:44,103 --> 00:00:47,550
the total that I was less than or equal to 90.

16
00:00:47,550 --> 00:00:51,890
977 between 91 and 95, and then we use the histogram

17
00:00:51,890 --> 00:00:55,210
tool that we talked about in module one to summarize this data.

18
00:00:55,210 --> 00:00:57,750
That looks almost like a perfect bell curve.

19
00:00:58,930 --> 00:01:00,160
Now if I hit the F9 key,

20
00:01:00,160 --> 00:01:05,290
I would get 10,000 more repetitions of this experiment.

21
00:01:05,290 --> 00:01:07,530
Now, the 635 would change a little bit, so

22
00:01:07,530 --> 00:01:12,160
I'll hit the F9 key to change this and generate the 10,000 more

23
00:01:12,160 --> 00:01:15,770
times that we tossed 30 dice and added up the total dots.

24
00:01:15,770 --> 00:01:19,180
Okay, and it's gonna still look like a bell curve.

25
00:01:19,180 --> 00:01:20,090
So I'll hit the F9 key.

26
00:01:20,090 --> 00:01:21,490
It'll take it a second here.

27
00:01:23,870 --> 00:01:26,610
But that 635 should change and be a little bit different, but

28
00:01:26,610 --> 00:01:27,830
it should be close to 635.

29
00:01:27,830 --> 00:01:30,979
But the key is that this chart will basically,

30
00:01:30,979 --> 00:01:33,819
histogram, will look like a bell curve.

31
00:01:37,063 --> 00:01:41,375
Okay, so you can see this time we got 619 times that the total was

32
00:01:41,375 --> 00:01:43,377
less than or equal to 90, but

33
00:01:43,377 --> 00:01:46,760
it still looks pretty much like a bell curve.

34
00:01:46,760 --> 00:01:50,740
Now, it looks like roughly 6% of the time your total

35
00:01:50,740 --> 00:01:52,850
number of dots would be less or equal to 90.

36
00:01:52,850 --> 00:01:55,530
Can we verify that with the central limit theorem?

37
00:01:55,530 --> 00:01:56,830
Yes we can.

38
00:01:56,830 --> 00:01:59,380
So remember we know a lot about tossing a die.

39
00:01:59,380 --> 00:02:01,130
We've talked about this a lot.

40
00:02:01,130 --> 00:02:03,380
What's the average number of dots on each die?

41
00:02:03,380 --> 00:02:04,559
It's three and a half.

42
00:02:04,559 --> 00:02:06,110
What's the variance? 2.916.

43
00:02:06,110 --> 00:02:08,008
What's the standard deviation?

44
00:02:08,008 --> 00:02:08,763
1.7.

45
00:02:08,763 --> 00:02:11,700
So, what's the mean for 30 dice?

46
00:02:11,700 --> 00:02:13,410
It's 30 times 3.5.

47
00:02:13,410 --> 00:02:15,380
What's the variance?

48
00:02:15,380 --> 00:02:17,790
It's 30 times the variance for one die.

49
00:02:17,790 --> 00:02:19,230
What's the standard deviation?

50
00:02:19,230 --> 00:02:20,980
The square root of the variance.

51
00:02:20,980 --> 00:02:24,110
So now I know the mean and I know the standard deviation.

52
00:02:26,070 --> 00:02:27,300
There is your mean.

53
00:02:27,300 --> 00:02:29,630
There is your standard deviation for the 30 die.

54
00:02:29,630 --> 00:02:32,480
I can treat the sum of the 30 dice as if it's normal,

55
00:02:32,480 --> 00:02:35,410
even though each die itself is not normal.

56
00:02:35,410 --> 00:02:38,230
So if I want the chance of getting a total of less than or equal to 90,

57
00:02:38,230 --> 00:02:40,450
can I approximate that 6%?

58
00:02:40,450 --> 00:02:41,640
Yes, I can.

59
00:02:41,640 --> 00:02:46,297
You find with a normal, the chance of less than or equal to 90.5.

60
00:02:46,297 --> 00:02:47,910
Now, why not 90?

61
00:02:47,910 --> 00:02:51,470
Because basically the die total is discreet and

62
00:02:51,470 --> 00:02:53,240
the normal is continuous.

63
00:02:53,240 --> 00:02:57,260
And it makes sense to say what should I use to approximate

64
00:02:57,260 --> 00:03:01,090
the probability of getting exactly 90 total with 30 dice?

65
00:03:01,090 --> 00:03:03,720
Well it should be between 89 and a half and 90 and

66
00:03:03,720 --> 00:03:06,070
a half because that's centered at 90.

67
00:03:06,070 --> 00:03:08,310
So if I want the chance of getting less than or

68
00:03:08,310 --> 00:03:13,270
equal to 90.5, I do a norm disc on 90.5 with the mean and

69
00:03:13,270 --> 00:03:17,270
standard deviation from 30 dice, and put the word true there and

70
00:03:17,270 --> 00:03:21,330
I get 6%, which is exactly very close to what happened.

71
00:03:21,330 --> 00:03:24,165
So that's a really powerful demonstration of the central limit

72
00:03:24,165 --> 00:03:24,910
theorem.

73
00:03:24,910 --> 00:03:27,930
So in the next video, we'll do another example of the central limit

74
00:03:27,930 --> 00:03:31,420
theorem to cement your understanding of this important concept.

