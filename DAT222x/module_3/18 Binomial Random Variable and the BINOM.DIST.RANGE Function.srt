0
00:00:00,460 --> 00:00:03,355
So now we're gonna turn our attention to three important random

1
00:00:03,355 --> 00:00:05,330
variables, the binomial random variable,

2
00:00:05,330 --> 00:00:07,970
the Poisson random variable, and the normal random variable.

3
00:00:07,970 --> 00:00:10,710
And Excel makes it really easy to compute probabilities for

4
00:00:10,710 --> 00:00:11,750
these random variables.

5
00:00:11,750 --> 00:00:13,590
If you took statistics before,

6
00:00:13,590 --> 00:00:16,510
you probably had to look at tables in the back of your book, and

7
00:00:16,510 --> 00:00:20,500
that was just, to me at least, a horrible experience as a professor.

8
00:00:20,500 --> 00:00:25,040
But Excel basically has all the tables in any stat book built into

9
00:00:25,040 --> 00:00:28,250
Excel, and we'll spend a lot of the remainder of this course showing you

10
00:00:28,250 --> 00:00:34,050
how Excel functions, sort of make tables and stat books obsolete.

11
00:00:34,050 --> 00:00:36,890
Okay, so let's talk about binomial random variable.

12
00:00:36,890 --> 00:00:38,380
It's a discrete random variable.

13
00:00:38,380 --> 00:00:39,950
You have repeated trials.

14
00:00:39,950 --> 00:00:42,330
Each trial has two outcomes, success or failure.

15
00:00:42,330 --> 00:00:46,810
You can define success to be something bad like a defective chip

16
00:00:47,820 --> 00:00:49,360
for a cell phone, whatever.

17
00:00:49,360 --> 00:00:52,820
But as long as you are consistent in how you define success, you're fine.

18
00:00:52,820 --> 00:00:55,437
There has to be the same chance of success on each trial.

19
00:00:55,437 --> 00:00:57,244
We call that p.

20
00:00:57,244 --> 00:01:00,200
The chance of success cannot change from trial to trial.

21
00:01:00,200 --> 00:01:02,360
And the outcomes of the trials must be independent.

22
00:01:02,360 --> 00:01:05,850
In other words, if you know what happened on the first five trials,

23
00:01:05,850 --> 00:01:08,260
it doesn't affect what happens on the sixth trial.

24
00:01:08,260 --> 00:01:10,130
So what are some examples where the binomial

25
00:01:10,130 --> 00:01:11,960
random variable is relevant?

26
00:01:11,960 --> 00:01:15,600
You toss a coin 100 times, okay, what are the outcomes?

27
00:01:15,600 --> 00:01:16,390
Heads or tails?

28
00:01:16,390 --> 00:01:17,930
We've talked about this already.

29
00:01:17,930 --> 00:01:19,560
Let a success be heads,

30
00:01:19,560 --> 00:01:21,855
then what's the probability of success on each trial?

31
00:01:21,855 --> 00:01:22,818
0.5.

32
00:01:22,818 --> 00:01:25,379
And I think the trials are independent, cuz I don't think

33
00:01:25,379 --> 00:01:28,096
if you toss the coin one time it affects what happens on the next

34
00:01:28,096 --> 00:01:31,820
toss, unless there's something I don't know about in the world.

35
00:01:31,820 --> 00:01:34,220
And the number of trials would be 100.

36
00:01:34,220 --> 00:01:36,330
Let's look at another example here.

37
00:01:36,330 --> 00:01:39,220
We sample 50 chips from a large batch of chips.

38
00:01:39,220 --> 00:01:40,910
Now, why does it have to be a large batch?

39
00:01:40,910 --> 00:01:42,068
We'll come back to that in a minute.

40
00:01:42,068 --> 00:01:45,519
Let's suppose 2% of the chips are defective, and

41
00:01:45,519 --> 00:01:47,564
a success is a defective chip.

42
00:01:47,564 --> 00:01:52,265
Well then p = 0.02, okay?

43
00:01:52,265 --> 00:01:54,850
The probability of success being a defective chip.

44
00:01:54,850 --> 00:01:56,982
And n, the sample size, was 50.

45
00:01:56,982 --> 00:01:58,937
We're looking at 50 chips being drawn,

46
00:01:58,937 --> 00:02:02,031
we wanna know the chance there'll be 5 chips that were defective,

47
00:02:02,031 --> 00:02:04,310
2 chips that were defective, etc.

48
00:02:04,310 --> 00:02:07,270
Now, why do I need to make the assumption that it's a large batch?

49
00:02:07,270 --> 00:02:08,280
Because if the batch were,

50
00:02:08,280 --> 00:02:12,370
let's say 51 chips, then basically as I take the chips out,

51
00:02:12,370 --> 00:02:15,330
I change the chance that there's a defective chip in there.

52
00:02:15,330 --> 00:02:17,720
Okay, so basically if I have a large batch,

53
00:02:17,720 --> 00:02:20,580
I can assume taking out one of the chips doesn't affect

54
00:02:20,580 --> 00:02:23,980
from trial to trial the chance of getting a defective chip.

55
00:02:23,980 --> 00:02:27,200
So how do we compute binomial probabilities in Excel?

56
00:02:27,200 --> 00:02:28,320
It's really easy.

57
00:02:28,320 --> 00:02:32,538
There's a great function, BINOM.DIST.RANGE.

58
00:02:32,538 --> 00:02:34,480
So what you do, as you'll see in the next video,

59
00:02:34,480 --> 00:02:37,300
is you put in the number of trials, you put in the probability of

60
00:02:37,300 --> 00:02:40,360
success in each trial, you put in the lower limit on

61
00:02:40,360 --> 00:02:43,670
how many successes you want in the upper limit, inclusive.

62
00:02:43,670 --> 00:02:46,950
And you get the probability that between S1 and

63
00:02:46,950 --> 00:02:49,570
S2 successes will occur.

64
00:02:49,570 --> 00:02:50,780
And that's all you need to know.

65
00:02:50,780 --> 00:02:53,530
You don't have to look at these crazy tables that are in your

66
00:02:53,530 --> 00:02:54,480
stat book.

67
00:02:54,480 --> 00:02:57,950
So in the next video, we'll show you how easy it is to compute

68
00:02:57,950 --> 00:03:02,506
binomial probabilities using the BINOM.DIST.RANGE function.

