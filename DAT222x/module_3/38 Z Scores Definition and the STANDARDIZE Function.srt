0
00:00:00,520 --> 00:00:02,190
Well, often we look at data sets and

1
00:00:02,190 --> 00:00:05,825
we wanna compare how unusual data points in different data sets are.

2
00:00:05,825 --> 00:00:09,180
Z-scores provides a convenient way to do this.

3
00:00:09,180 --> 00:00:12,780
So for any data set in any observation, what is the Z score?

4
00:00:12,780 --> 00:00:14,820
You take the value of the data point,

5
00:00:14,820 --> 00:00:18,464
x sub i minus the mean divided by the standard deviation.

6
00:00:18,464 --> 00:00:22,140
Essentially a Z score is how many standard deviations above or

7
00:00:22,140 --> 00:00:23,890
below average is a data point?

8
00:00:23,890 --> 00:00:26,280
And there's an Excel function to do this called

9
00:00:26,280 --> 00:00:27,980
the standardize function.

10
00:00:27,980 --> 00:00:30,140
I usually just do it directly.

11
00:00:30,140 --> 00:00:33,350
But for any data set, what will happen with the Z scores?

12
00:00:33,350 --> 00:00:37,560
The average will be zero and the standard deviation will be one, and

13
00:00:37,560 --> 00:00:40,240
that's we mean by sort of standardizing.

14
00:00:40,240 --> 00:00:44,770
And so If you have a value of the z score greater than +2 or

15
00:00:44,770 --> 00:00:47,340
less than -2, that sort of denotes an outlier.

16
00:00:47,340 --> 00:00:52,030
That again comes from the normal random variable only 5% of the time

17
00:00:52,030 --> 00:00:54,680
or more than two standard deviations away from the mean.

18
00:00:54,680 --> 00:00:57,470
We can use conditional formatting to easily highlight

19
00:00:57,470 --> 00:00:58,880
outliers using the Z scores.

20
00:00:58,880 --> 00:01:01,400
And we'll do that in a later video.

21
00:01:01,400 --> 00:01:04,460
And, again, Z scores allow us to compare data sets measured in

22
00:01:04,460 --> 00:01:05,570
different units.

23
00:01:05,570 --> 00:01:08,310
So the average score on the SAT used to be about 500,

24
00:01:08,310 --> 00:01:09,860
with a standard deviation of 100.

25
00:01:09,860 --> 00:01:14,460
So if you've got an SAT of 700, that's a Z score of +2.

26
00:01:14,460 --> 00:01:16,812
You're two standard deviations better than average.

27
00:01:16,812 --> 00:01:21,160
Now an IQ of 130 is also a z score of plus two, because basically

28
00:01:21,160 --> 00:01:25,824
the standard deviation is 15, so you can sort of equate in some sense

29
00:01:25,824 --> 00:01:30,749
an SAT score of 700 at an IQ of 130 if you assume the two populations.

30
00:01:30,749 --> 00:01:35,480
Basically if everybody took the SAT and everybody took an IQ test.

31
00:01:35,480 --> 00:01:38,730
Another example of a Z score to show how unusual something is,

32
00:01:38,730 --> 00:01:43,180
there was a day in October 1987 when the market dropped 22%.

33
00:01:43,180 --> 00:01:48,090
Now the standard deviation daily returns is about 1.5%.

34
00:01:48,090 --> 00:01:50,428
The mean daily return is about zero.

35
00:01:50,428 --> 00:01:55,461
So the Z score would be just -22% divided by 1.5%.

36
00:01:55,461 --> 00:01:57,822
That's about -14.

37
00:01:57,822 --> 00:02:03,660
So that day in 1987 the Dow sort of changed by 14 standard deviations,

38
00:02:03,660 --> 00:02:06,000
which just should not happen.

39
00:02:06,000 --> 00:02:09,700
That probably tells you stock prices don't follow a normal random

40
00:02:09,700 --> 00:02:11,700
variable, which I think is true.

41
00:02:11,700 --> 00:02:15,140
Now, in the next video we'll look at demographics of US cities and

42
00:02:15,140 --> 00:02:16,800
try and compute Z scores for

43
00:02:16,800 --> 00:02:20,280
each city based on different demographic characteristics.

