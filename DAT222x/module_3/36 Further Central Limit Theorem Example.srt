0
00:00:00,190 --> 00:00:02,530
In this video we will apply the

1
00:00:02,550 --> 00:00:05,009
Central Limit theorem to solve a really

2
00:00:05,020 --> 00:00:07,740
practical probability problem. So let's

3
00:00:07,760 --> 00:00:10,920
suppose you're running a pizza shop in

4
00:00:10,940 --> 00:00:13,769
your town and let's suppose you know on

5
00:00:13,780 --> 00:00:15,809
average each day in a 30 day month

6
00:00:15,820 --> 00:00:18,539
you'll sell 45 pizzas and the standard

7
00:00:18,550 --> 00:00:21,119
duration is 12 and again we don't need

8
00:00:21,130 --> 00:00:24,840
to assume that each day the number of

9
00:00:24,860 --> 00:00:26,490
pizzas sold it follows a normal random

10
00:00:26,510 --> 00:00:28,230
variable by the Central Limit theorem it

11
00:00:28,250 --> 00:00:31,740
doesn't really matter as long as the

12
00:00:31,760 --> 00:00:34,950
pizza sales each day are independent

13
00:00:34,970 --> 00:00:36,899
and we'll assume that's true. So the question

14
00:00:36,910 --> 00:00:39,390
would be if we have a 30 day month

15
00:00:39,410 --> 00:00:41,280
what's the chance we'll sell more than

16
00:00:41,300 --> 00:00:43,170
1,400 pizzas, now what you have to

17
00:00:43,190 --> 00:00:45,960
understand more than 1,400 means greater

18
00:00:45,980 --> 00:00:51,660
than equal to 1401, okay. More than 1401,

19
00:00:51,680 --> 00:00:53,850
I would say at least 1,400 if I wanted to

20
00:00:53,870 --> 00:00:56,340
include 1,400 and that's important when

21
00:00:56,360 --> 00:00:57,750
you read our tests in our homework

22
00:00:57,770 --> 00:01:00,300
problems to understand that. Okay, so

23
00:01:00,320 --> 00:01:01,949
what's the chance we'll sell more than

24
00:01:01,960 --> 00:01:04,589
1,400 pizzas in 30 days and then if we

25
00:01:04,600 --> 00:01:06,479
want to have a 1% chance of running out

26
00:01:06,490 --> 00:01:07,950
of pizza dough, let's suppose we have to

27
00:01:07,970 --> 00:01:10,110
buy all the pizza dough at the beginning

28
00:01:10,130 --> 00:01:10,619
of the month,

29
00:01:10,630 --> 00:01:14,070
how many pizza dough's should we buy?

30
00:01:14,090 --> 00:01:17,820
Okay, so we need to understand is what is

31
00:01:17,840 --> 00:01:19,650
the random variable that sort of governs

32
00:01:19,670 --> 00:01:22,560
the 30 day sales of pizza, so we need the

33
00:01:22,580 --> 00:01:24,750
mean and the standard deviation for 30

34
00:01:24,770 --> 00:01:26,830
days and we can then just apply the

35
00:01:27,050 --> 00:01:29,189
NORM.DIST and the NORM.INV functions by

36
00:01:29,200 --> 00:01:31,259
the Central Limit theorem. So what is the

37
00:01:31,270 --> 00:01:32,790
mean number of pizzas that will be sold

38
00:01:32,810 --> 00:01:35,280
in 30 days, it's 30 times the mean for

39
00:01:35,300 --> 00:01:39,479
each day. So here we have 30 times 45 you

40
00:01:39,490 --> 00:01:41,909
can see the formula here 1,350 pizzas on

41
00:01:41,920 --> 00:01:44,189
average will be sold, what's the variance?

42
00:01:44,200 --> 00:01:46,409
Now remember for independent random

43
00:01:46,420 --> 00:01:48,119
variables the variance of the sum is the

44
00:01:48,130 --> 00:01:49,979
sum of the variance. It's not true that

45
00:01:49,990 --> 00:01:52,409
the standard deviation of the sum is

46
00:01:52,420 --> 00:01:54,119
the sum of the standard deviations, but that's

47
00:01:54,130 --> 00:01:56,159
very important. So the variance of the

48
00:01:56,170 --> 00:01:58,200
sum here would be the variance of the

49
00:01:58,220 --> 00:02:01,740
first day which would be 12 squared 144

50
00:02:01,760 --> 00:02:03,210
plus the variance for the second day

51
00:02:03,230 --> 00:02:06,810
which is 144 so it's 30 times 144. That's

52
00:02:06,830 --> 00:02:08,970
what we have here, so that's the variance

53
00:02:08,990 --> 00:02:11,489
in the sales to pizza for the 30 days.

54
00:02:11,500 --> 00:02:13,720
Now to get the standard deviation you

55
00:02:13,740 --> 00:02:14,800
always take the square root of the variance.

56
00:02:14,820 --> 00:02:19,000
Square root of 4320 is 6572, so now we

57
00:02:19,020 --> 00:02:20,560
know when we want to compute

58
00:02:20,580 --> 00:02:22,420
probabilities or answer questions about

59
00:02:22,440 --> 00:02:24,820
the demand for pizzas for the whole

60
00:02:24,840 --> 00:02:26,950
month of 30 days we can just basically

61
00:02:26,970 --> 00:02:29,920
assume the demand for pizzas is normally

62
00:02:29,940 --> 00:02:31,960
distributed with a mean of 1350 and a

63
00:02:31,980 --> 00:02:34,240
standard deviation of 65.73.

64
00:02:34,260 --> 00:02:36,370
So what's the chance that more

65
00:02:36,390 --> 00:02:39,640
than 1,400 gets sold well that's a

66
00:02:39,660 --> 00:02:41,320
discrete random variable you can't sort

67
00:02:41,340 --> 00:02:43,660
of sell half a pizza as far as I know. So

68
00:02:43,680 --> 00:02:45,280
basically what you got to do is start at

69
00:02:45,300 --> 00:02:52,900
1400.5 and then really 1401.5 that range sort of estimates

70
00:02:52,920 --> 00:02:55,480
the probability of exactly 1401.

71
00:02:55,500 --> 00:02:57,730
You could start at 1401 it's not going

72
00:02:57,750 --> 00:02:59,440
to make much difference, but this is

73
00:02:59,460 --> 00:03:01,150
called the Continuity correction

74
00:03:01,170 --> 00:03:03,220
and it's basically a little bit more precise.

75
00:03:03,240 --> 00:03:06,040
So the chance you would sell atleast

76
00:03:06,060 --> 00:03:08,740
1401 is the chance that you would have

77
00:03:08,760 --> 00:03:10,180
normal random variable greater than or

78
00:03:10,200 --> 00:03:12,730
equal to 1400.5, so you just

79
00:03:12,750 --> 00:03:15,640
take one minus NORM DIST 1400.5

80
00:03:15,660 --> 00:03:17,470
put in the mean and standard deviation

81
00:03:17,490 --> 00:03:20,410
don't forget the word TRUE, you get a 22

82
00:03:20,430 --> 00:03:21,880
percent chance you'd sell more than

83
00:03:21,900 --> 00:03:24,130
1,400 pizzas. Now to have a one

84
00:03:24,150 --> 00:03:25,660
percent chance of running out you just

85
00:03:25,680 --> 00:03:27,970
need the 99th percentile. So you use NORM

86
00:03:27,990 --> 00:03:29,800
members for percentile since we

87
00:03:29,820 --> 00:03:35,230
discussed earlier, so you take NORM- INV point, 0.99 the mean is 1350.

88
00:03:35,250 --> 00:03:37,120
The standard deviation is 65.72

89
00:03:37,140 --> 00:03:41,730
So if you ordered 1502.09 pizzas you'd have a one

90
00:03:41,750 --> 00:03:43,299
percent chance of running out probably

91
00:03:43,310 --> 00:03:45,270
round that up to 1403

92
00:03:45,290 --> 00:03:46,840
and you can see a picture here

93
00:03:46,860 --> 00:03:48,730
basically where we got that twenty two

94
00:03:48,750 --> 00:03:53,200
percent here, okay. Basically this is the

95
00:03:53,220 --> 00:03:57,420
area to the right of 1400.5 and that's the chance that

96
00:03:57,440 --> 00:04:00,640
you'd sell at least 1401 pizzas in a

97
00:04:00,660 --> 00:04:05,620
month and that says 22.1 percent, which is exactly what we got. Now

98
00:04:05,640 --> 00:04:07,750
in the next video we'll begin our study

99
00:04:07,770 --> 00:04:09,570
of the important concept of z-scores

100
00:04:09,590 --> 00:04:12,519
which is a concept that statisticians

101
00:04:12,530 --> 00:04:14,709
use to compare different data sets for

102
00:04:14,720 --> 00:04:20,370
instance, how do you compare a GMAT score to an SAT score.

