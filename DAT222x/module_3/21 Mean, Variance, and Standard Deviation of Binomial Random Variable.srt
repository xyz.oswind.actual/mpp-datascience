0
00:00:00,930 --> 00:00:04,700
Recall that a binomial random variable is the number of successes

1
00:00:04,700 --> 00:00:08,340
x, in n-repeated trials of a binomial experiment.

2
00:00:08,340 --> 00:00:11,910
The probability distribution of a binomial random variable is called

3
00:00:11,910 --> 00:00:13,970
a binomial distribution.

4
00:00:13,970 --> 00:00:16,930
Remember our example of tossing a coin three times and

5
00:00:16,930 --> 00:00:18,590
counting the number of heads?

6
00:00:18,590 --> 00:00:22,133
Binomial random variable is the number of heads can take on

7
00:00:22,133 --> 00:00:24,673
the values of zero, one, two and three.

8
00:00:24,673 --> 00:00:27,520
This is what a binomial distribution looks like.

9
00:00:27,520 --> 00:00:31,240
The mean of the distribution is equal to P times n or

10
00:00:31,240 --> 00:00:34,178
3 times 0.5 in this case, 1.5.

11
00:00:34,178 --> 00:00:39,981
The variance is P times q times n or 3 times 0.5 times 0.5.

12
00:00:39,981 --> 00:00:42,110
In this case, 0.75.

13
00:00:42,110 --> 00:00:43,988
The standard deviation then is,

14
00:00:43,988 --> 00:00:47,755
simply the square root of the variance, in this case, 0.85.

15
00:00:47,755 --> 00:00:49,410
Do these numbers look familiar?

16
00:00:49,410 --> 00:00:50,040
They should.

17
00:00:50,040 --> 00:00:52,840
They are the same as those calculated in the lesson describing

18
00:00:52,840 --> 00:00:55,560
expected values of discrete random variables.

19
00:00:55,560 --> 00:00:58,530
Binomial is just a special case of that concept.

20
00:00:58,530 --> 00:01:01,880
The results are the same because a coin toss, or roll of a die for

21
00:01:01,880 --> 00:01:04,160
that matter, has two possible outcomes.

22
00:01:04,160 --> 00:01:07,290
There is a probability that can be associated with success.

23
00:01:07,290 --> 00:01:09,170
And one that can be associated with failure,

24
00:01:09,170 --> 00:01:11,120
regardless of how you define it.

25
00:01:11,120 --> 00:01:14,370
Remember that game with the financial outcomes examples from

26
00:01:14,370 --> 00:01:15,990
earlier in this module?

27
00:01:15,990 --> 00:01:17,800
If you selected one outcome of interest, for

28
00:01:17,800 --> 00:01:21,300
example winning $5, and collapsed the other three outcomes together,

29
00:01:21,300 --> 00:01:25,370
summing their probabilities, you would have a binomial probability.

30
00:01:25,370 --> 00:01:29,928
Recall the probability of winning $5 in that game was 0.1,

31
00:01:29,928 --> 00:01:32,835
that means the probability of success is 0.1, and

32
00:01:32,835 --> 00:01:35,045
your probability of failure is 0.9.

33
00:01:35,045 --> 00:01:38,675
Assume you played the game 10 times, that would mean that you

34
00:01:38,675 --> 00:01:42,345
would have 10 times 0.1, or 1, so that's your mean.

35
00:01:42,345 --> 00:01:43,135
Your variance is 0.9.

36
00:01:43,135 --> 00:01:47,384
And the standard deviation is 0.95.

