0
00:00:01,970 --> 00:00:05,080
In video, we'll again discuss discrete random variable.

1
00:00:05,080 --> 00:00:06,905
So, when is a random variable discrete?

2
00:00:06,905 --> 00:00:10,642
If the number of possible values is finite or countably infinite,

3
00:00:10,642 --> 00:00:14,777
so countably infinite means it could be an integer usually, zero, one,

4
00:00:14,777 --> 00:00:15,460
two, etc.

5
00:00:16,480 --> 00:00:19,530
So, every discrete random variable has what's called the probability

6
00:00:19,530 --> 00:00:20,620
mass function.

7
00:00:20,620 --> 00:00:24,260
That gives the probability of each of the possible values, and

8
00:00:24,260 --> 00:00:27,660
we can usually express those by heights in a column graph, and

9
00:00:27,660 --> 00:00:29,900
of course those heights must add to one.

10
00:00:29,900 --> 00:00:33,740
So lets look at two examples of discrete random variables.

11
00:00:33,740 --> 00:00:37,000
Now these graphs are courtesy of Palisade Corporation who make at

12
00:00:37,000 --> 00:00:38,920
risk money simulation, but

13
00:00:38,920 --> 00:00:43,760
they really do help us understand what random variables look like.

14
00:00:43,760 --> 00:00:46,350
So one example would be the number of dots showing on a die.

15
00:00:46,350 --> 00:00:48,796
You know you could have one through six dots showing and

16
00:00:48,796 --> 00:00:51,254
they're equally likely, each probability 1/6.

17
00:00:51,254 --> 00:00:55,390
So the mass function would be here 1/6 chance of a 1.

18
00:00:55,390 --> 00:00:56,756
One-sixth chance of a 2.

19
00:00:56,756 --> 00:00:58,156
One-sixth chance of a 3.

20
00:00:58,156 --> 00:00:59,635
One-sixth chance of a 4.

21
00:00:59,635 --> 00:01:02,930
One-sixth chance of a 5, one-sixth chance of a 6.

22
00:01:02,930 --> 00:01:05,030
I guess we have three examples here.

23
00:01:05,030 --> 00:01:08,454
Okay, let's look at the number of accidents your teenager has

24
00:01:08,454 --> 00:01:09,041
in a year.

25
00:01:09,041 --> 00:01:13,241
Hopefully zero, but probably for not all of you, zero.

26
00:01:13,241 --> 00:01:17,119
Okay, so the number of accidents a teenager might have in a year most

27
00:01:17,119 --> 00:01:18,760
likely zero, I would hope.

28
00:01:18,760 --> 00:01:22,756
And so here we have about a 73% chance of zero, maybe a 22% chance

29
00:01:22,756 --> 00:01:27,226
of one accident, a 5% chance of two, 4% chance of two accidents, three,

30
00:01:27,226 --> 00:01:31,180
four, five and God help us six accidents, very unlikely.

31
00:01:31,180 --> 00:01:34,020
This comes from what's called the random variable, but

32
00:01:34,020 --> 00:01:37,330
the sum of the heights of all these spikes would equal one and

33
00:01:37,330 --> 00:01:38,830
this is your mass function.

34
00:01:38,830 --> 00:01:42,630
Because the word mass measures sort of how much weight you have.

35
00:01:42,630 --> 00:01:44,280
Basically, the height of the bar

36
00:01:44,280 --> 00:01:46,540
here indicates how much probability you have.

37
00:01:46,540 --> 00:01:49,670
So one final example here, if you toss a coin ten times,

38
00:01:49,670 --> 00:01:52,890
that's a discrete random variable, you'll get zero through ten heads.

39
00:01:52,890 --> 00:01:54,940
What does the mass function look like?

40
00:01:54,940 --> 00:01:57,650
Well what's most likely is you'll see five heads about 24%

41
00:01:57,650 --> 00:01:58,420
of the time.

42
00:01:58,420 --> 00:02:02,342
We'll see more on this when we study the binomial random variable later.

43
00:02:02,342 --> 00:02:05,800
But zero is very unlikely, ten is very unlikely.

44
00:02:05,800 --> 00:02:08,600
But the height of these spikes add to one, and if it looks like

45
00:02:08,600 --> 00:02:11,750
a symmetric histogram to you, your eyes aren't deceiving you.

46
00:02:11,750 --> 00:02:13,430
It is pretty much a symmetric histogram,

47
00:02:13,430 --> 00:02:15,770
and we'll talk more about that later.

48
00:02:15,770 --> 00:02:18,820
Now in the next video, we'll talk about continuous random variables.

