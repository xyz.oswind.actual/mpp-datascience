0
00:00:00,820 --> 00:00:05,570
So, let's do in this video, four examples of how the BINOM.DIST.RANGE

1
00:00:05,570 --> 00:00:09,700
function makes it easy to compute binomial probabilities.

2
00:00:09,700 --> 00:00:12,110
So, let's suppose you ask 100 people,

3
00:00:12,110 --> 00:00:13,450
do they prefer Coke to Pepsi?

4
00:00:13,450 --> 00:00:16,510
And suppose half the people prefer Coke to Pepsi or Pepsi to Coke.

5
00:00:16,510 --> 00:00:21,470
What's the probability exactly 60 people will prefer Coke to Pepsi?

6
00:00:21,470 --> 00:00:25,750
Well, you have a binomial with 100 trials, each person being asked Coke

7
00:00:25,750 --> 00:00:30,700
or Pepsi is a trial and basically there's a 50/50 chance, probably P,

8
00:00:30,700 --> 00:00:35,265
that they'll prefer Coke to Pepsi so I can use that binom disc range.

9
00:00:35,265 --> 00:00:40,783
Okay, now the number of trials goes first,

10
00:00:40,783 --> 00:00:45,700
that's 100, okay, the probability is 0.5.

11
00:00:45,700 --> 00:00:50,810
And the success is the range would be 60 to 60 cuz I want exactly 60.

12
00:00:50,810 --> 00:00:55,420
So the formula shows up right up there, it's 1%.

13
00:00:55,420 --> 00:00:59,861
Okay there's 1 chance in 100 that exactly 60 people will prefer Coke

14
00:00:59,861 --> 00:01:01,010
to Pepsi.

15
00:01:01,010 --> 00:01:03,110
Now what's the chance that between 40 and

16
00:01:03,110 --> 00:01:05,870
60 people prefer Coke to Pepsi.

17
00:01:05,870 --> 00:01:08,040
So I would do BINOM DIST.RANGE again.

18
00:01:10,070 --> 00:01:14,250
Okay I had 100 trials, 0.5 chance of success

19
00:01:16,160 --> 00:01:21,250
and I would say between 40 and 60, that's 40 and 60 inclusive.

20
00:01:21,250 --> 00:01:24,886
So that counts 40, 41 through 59 and 60 and

21
00:01:24,886 --> 00:01:29,320
that'll be just about everybody, 96% of the time.

22
00:01:29,320 --> 00:01:32,935
By the way this would still apply if I was doing coin tosses instead of

23
00:01:32,935 --> 00:01:36,395
doing Coke vs Pepsi cuz there would be 50% chance of success.

24
00:01:36,395 --> 00:01:39,600
So basically there would be 96% chance of getting 40 and

25
00:01:39,600 --> 00:01:43,000
60 heads if you toss a coin 100 times.

26
00:01:43,000 --> 00:01:45,980
Now let's suppose you have a batch of 10,000 elevator rails and

27
00:01:45,980 --> 00:01:47,910
you know 3% are defective.

28
00:01:47,910 --> 00:01:51,230
You sample 100 elevator rails without replacement.

29
00:01:51,230 --> 00:01:54,260
Okay see you can't get the same elevator rail twice.

30
00:01:54,260 --> 00:01:57,460
What's probability two or fewer of those rails are defective.

31
00:01:57,460 --> 00:02:00,990
Now I assume the batch was a large size relative to the sample.

32
00:02:00,990 --> 00:02:02,820
So, when I take elevator rails out,

33
00:02:02,820 --> 00:02:05,220
it doesn't really change the probability.

34
00:02:05,220 --> 00:02:08,870
So, I have 100 trials that will be n, and

35
00:02:08,870 --> 00:02:11,900
the success is being defective, and

36
00:02:11,900 --> 00:02:17,040
basically, we'll use a 0.03 chance for p of being defective.

37
00:02:17,040 --> 00:02:19,416
So I do BINOM.DIST.RANGE.

38
00:02:22,731 --> 00:02:28,426
Okay, now the trials were 100, the chance of a defective was 0.03.

39
00:02:28,426 --> 00:02:31,961
Okay, 2 or fewer means between 0 and 2 defective.

40
00:02:35,165 --> 00:02:37,824
So I get a 42% chance we'd find 2 or

41
00:02:37,824 --> 00:02:41,320
fewer elevator rails defective in that sample.

42
00:02:41,320 --> 00:02:43,280
How about airline overbooking?

43
00:02:43,280 --> 00:02:47,480
We've all been at the airport when they say somebody

44
00:02:47,480 --> 00:02:50,650
take $300 voucher because the flight is overbooked.

45
00:02:50,650 --> 00:02:53,190
And the airlines really pay attention to this.

46
00:02:53,190 --> 00:02:54,560
They'll always book more people for

47
00:02:54,560 --> 00:02:57,840
the flight than there are seats cuz they know some people won't show up,

48
00:02:57,840 --> 00:03:00,030
and they don't want empty seats on that flight.

49
00:03:00,030 --> 00:03:03,057
Although, we passengers love those empty seats in the middle,

50
00:03:03,057 --> 00:03:04,020
if we're in aisle.

51
00:03:04,020 --> 00:03:08,920
Okay, so what's probability a flight is overbooked if it has basically

52
00:03:08,920 --> 00:03:13,034
105 tickets are sold and it's 100 seat plane, okay?

53
00:03:13,034 --> 00:03:15,540
And 95% of the people show up.

54
00:03:15,540 --> 00:03:16,970
So why is this binomial?

55
00:03:16,970 --> 00:03:18,780
Each person shows up or not.

56
00:03:18,780 --> 00:03:22,000
A success is they show up with probably 0.95.

57
00:03:22,000 --> 00:03:24,632
We have 105 trials we'll be over booked but

58
00:03:24,632 --> 00:03:26,980
there's at a 101 successes.

59
00:03:26,980 --> 00:03:30,561
So I do BINOM.DIST.RANGE, okay so

60
00:03:30,561 --> 00:03:36,457
I have 105 trials probably success we said was 0.95.

61
00:03:36,457 --> 00:03:38,552
I want the chance of between 101 and

62
00:03:38,552 --> 00:03:41,250
105 showing up cuz that would be overbooked.

63
00:03:41,250 --> 00:03:44,890
The most that can show up is 105, 100 would not be overbooked,

64
00:03:44,890 --> 00:03:46,400
everybody would have a seat.

65
00:03:46,400 --> 00:03:48,780
But, 101 showing up basically,

66
00:03:48,780 --> 00:03:53,460
we'd be overbooked because we have more people showing up than seats.

67
00:03:53,460 --> 00:03:58,270
So, that comes out to be 39% chance that flight will be overbooked.

68
00:03:58,270 --> 00:04:01,775
So like in five minutes, we did four really simple problems,

69
00:04:01,775 --> 00:04:06,530
non trivial problems, to be honest using the binomial random

70
00:04:06,530 --> 00:04:09,460
variable we never once had to look at a table in a book,

71
00:04:09,460 --> 00:04:12,220
we just used this binom disrange function.

72
00:04:12,220 --> 00:04:15,700
In the next video we'll show you what the mean variance in standard

73
00:04:15,700 --> 00:04:17,840
deviation of the binomial random variable are.

