0
00:00:00,600 --> 00:00:03,960
As an example of how we can use z-scores to better understand data,

1
00:00:03,960 --> 00:00:05,610
let's look at the demographics.

2
00:00:05,610 --> 00:00:09,460
This is probably about 10 years ago of 49 US cities.

3
00:00:09,460 --> 00:00:14,470
So from Albuquerque through, alphabetically, Virginia Beach.

4
00:00:16,200 --> 00:00:17,350
What information do we have?

5
00:00:18,930 --> 00:00:21,320
We have the percentage of African Americans in the city.

6
00:00:21,320 --> 00:00:24,880
For example, Albuquerque does not have that high a percentage of

7
00:00:24,880 --> 00:00:27,900
African Americans but Atlanta has a very high percentage.

8
00:00:27,900 --> 00:00:30,720
We have the percentage of Hispanics, percentage of Asians.

9
00:00:30,720 --> 00:00:32,540
We have the median age,

10
00:00:32,540 --> 00:00:35,620
which measures if it's sort of like an old city or a young city.

11
00:00:35,620 --> 00:00:37,394
We have the unemployment rate,

12
00:00:37,394 --> 00:00:40,421
we have the per capita income in thousands of dollars.

13
00:00:40,421 --> 00:00:43,180
So it's like comparing apples and oranges,

14
00:00:43,180 --> 00:00:46,373
I mean how far above average is a city on median age for

15
00:00:46,373 --> 00:00:49,941
African Americans, z-scores really help us with this.

16
00:00:49,941 --> 00:00:53,761
So, I've computed the mean for each of these six variables here, and

17
00:00:53,761 --> 00:00:57,870
the standard deviation, with average and standard deviation function.

18
00:00:57,870 --> 00:01:02,110
For example, of our cities on the list, there's an average of 24%

19
00:01:02,110 --> 00:01:06,835
African Americans in each city, with a standard deviation of 18%.

20
00:01:06,835 --> 00:01:09,625
But if you look at the median, the median age is 32, but

21
00:01:09,625 --> 00:01:12,485
the standard deviation is really low, it's only 2 years.

22
00:01:12,485 --> 00:01:15,245
I mean there isn't that much difference in the median age in

23
00:01:15,245 --> 00:01:16,475
different cities.

24
00:01:16,475 --> 00:01:18,797
So if you wanna really sort of compare,

25
00:01:18,797 --> 00:01:23,147
if a city is unusual on age versus unusual relative to other cities on

26
00:01:23,147 --> 00:01:26,737
percentage of African Americans, z-score really helps us.

27
00:01:26,737 --> 00:01:28,567
So there is two ways you can do z-scores,

28
00:01:28,567 --> 00:01:31,787
there is a standardized function in Excel that if you have the mean and

29
00:01:31,787 --> 00:01:35,967
standard deviation for each column of data, you can just use that or

30
00:01:35,967 --> 00:01:37,847
you can basically just compute them directly.

31
00:01:37,847 --> 00:01:40,587
I'll do it both ways, you can pick what you like.

32
00:01:40,587 --> 00:01:46,640
So I'll do STANDARDIZE, okay, so now I need the value, that's right here.

33
00:01:46,640 --> 00:01:49,311
Now the mean would be in C1.

34
00:01:49,311 --> 00:01:51,768
Now when I copy this, I want the C to change, but

35
00:01:51,768 --> 00:01:54,330
I don't want the 1 to change.

36
00:01:54,330 --> 00:01:58,190
See, when I copy that down, I want to be pulling the mean from there.

37
00:01:58,190 --> 00:02:01,420
And then the standard deviation would be C, and

38
00:02:01,420 --> 00:02:05,000
again I want the 2 to not change but I want the C to change.

39
00:02:05,000 --> 00:02:09,400
So when I copy that across, it's gonna pull stuff from column D etc.

40
00:02:09,400 --> 00:02:11,780
So that will be the z-score for African Americans.

41
00:02:11,780 --> 00:02:14,756
Now I can copy that, do Ctrl+C, and

42
00:02:14,756 --> 00:02:17,736
then I can go down to the bottom here.

43
00:02:23,560 --> 00:02:27,880
Ctrl+V, okay, and there is my z-scores.

44
00:02:27,880 --> 00:02:30,300
And we'll do this directly in a minute, but

45
00:02:30,300 --> 00:02:34,155
the average of the z-scores should always be 0.

46
00:02:35,700 --> 00:02:38,380
So we can sort of compare things in each column.

47
00:02:40,080 --> 00:02:43,971
E to the -17 means point 16 zeroes, and then a 4.

48
00:02:43,971 --> 00:02:46,701
And the standard deviation should always be 1.

49
00:02:51,640 --> 00:02:56,525
And if I copy that across, we'll see now every demographic characteristic

50
00:02:56,525 --> 00:02:58,595
has sort of been standardized.

51
00:02:58,595 --> 00:03:02,190
Again, these -16s mean 0, basically.

52
00:03:02,190 --> 00:03:04,570
Now I could do this by calculation if I wanted to.

53
00:03:04,570 --> 00:03:09,361
I could take basically for Albuquerque,

54
00:03:11,846 --> 00:03:16,475
The percentage of African-Americans minus the mean.

55
00:03:16,475 --> 00:03:19,526
And that would be C$1.

56
00:03:24,070 --> 00:03:25,150
There's the $ sign.

57
00:03:26,900 --> 00:03:31,263
Divided by the standard deviation, C$2.

58
00:03:31,263 --> 00:03:34,700
Again I $ sign the 1 and the 2, cuz I don't want those to change.

59
00:03:34,700 --> 00:03:37,700
But I do want the columns to change so I pulled the mean and

60
00:03:37,700 --> 00:03:39,840
standard deviation for the appropriate column.

61
00:03:39,840 --> 00:03:42,670
Notice that's exactly what I got here.

62
00:03:42,670 --> 00:03:47,410
Now if I drag that across, an easier way to copy it down, to be honest,

63
00:03:47,410 --> 00:03:50,090
is double click the left mouse, it'll match it.

64
00:03:50,090 --> 00:03:54,760
So basically, what we did by standardization here, and

65
00:03:54,760 --> 00:03:56,240
it just doesn't matter, really.

66
00:03:57,780 --> 00:03:59,610
So we did that there.

67
00:03:59,610 --> 00:04:03,430
The numbers are exactly the same, so what do they tell us?

68
00:04:03,430 --> 00:04:04,689
That's the important thing.

69
00:04:10,225 --> 00:04:14,500
Okay, so we can get a snapshot of how, for instance, Atlanta looks.

70
00:04:14,500 --> 00:04:19,170
Highly African-American city with not that many Hispanics, and

71
00:04:19,170 --> 00:04:23,088
basically it's got a fairly low unemployment rate.

72
00:04:23,088 --> 00:04:27,340
Okay, now if I would look at Omaha, basically,

73
00:04:28,760 --> 00:04:31,765
that's a city that's low on all the minorities there.

74
00:04:31,765 --> 00:04:37,320
Okay, it's basically a city that's mostly Caucasian.

75
00:04:38,440 --> 00:04:40,283
And, if I look at Seattle,

76
00:04:44,329 --> 00:04:50,870
Basically Seattle's a high income and it's an older city.

77
00:04:50,870 --> 00:04:52,750
Okay, basically right there.

78
00:04:53,880 --> 00:04:58,839
Honolulu, Would be basically

79
00:04:58,839 --> 00:05:02,191
6 standard deviations above average on Asians there.

80
00:05:02,191 --> 00:05:05,529
And then it's basically older city.

81
00:05:08,505 --> 00:05:11,040
And also a bit above average on income.

82
00:05:11,040 --> 00:05:13,820
But this gives us a really quick way with the z-scores,

83
00:05:13,820 --> 00:05:18,130
to really understand what's going on with datasets that really can't

84
00:05:18,130 --> 00:05:21,300
be compared based on their numerical value.

85
00:05:21,300 --> 00:05:25,090
So in other words the median ages basically don't have much spread but

86
00:05:25,090 --> 00:05:28,600
when basically I take a z-score on age, then we have spread.

87
00:05:28,600 --> 00:05:30,780
Now we could easily highlight outliers which we'll do in

88
00:05:30,780 --> 00:05:31,970
the next video.

89
00:05:31,970 --> 00:05:36,262
Any z-score greater than +2 or less than -2 will be an outlier.

