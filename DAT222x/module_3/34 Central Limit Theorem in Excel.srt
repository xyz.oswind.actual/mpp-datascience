0
00:00:01,320 --> 00:00:02,990
Through all my years of studying Mathematics,

1
00:00:02,990 --> 00:00:05,220
I think the Central Limit Theorem is the most elegant and

2
00:00:05,220 --> 00:00:07,420
beautiful result that I've seen.

3
00:00:07,420 --> 00:00:10,210
Basically what the Central Limit Theorem says is if you add

4
00:00:10,210 --> 00:00:13,510
together 30 or more independent random variables.

5
00:00:13,510 --> 00:00:16,380
Even if none of the individual random variables follow a normal

6
00:00:16,380 --> 00:00:17,440
random variable,

7
00:00:17,440 --> 00:00:20,970
the sum of the random variables will come very close to being normal.

8
00:00:20,970 --> 00:00:24,200
And this lets us solve a lot of probability problems involving

9
00:00:24,200 --> 00:00:25,920
adding up random variables.

10
00:00:25,920 --> 00:00:29,513
So if we add up X1 + X2 + Xn as the sum of n independent random

11
00:00:29,513 --> 00:00:33,602
variables, the sum of those random variables will be approximately

12
00:00:33,602 --> 00:00:37,408
normal, look like a bell curve or normal density, with the mean

13
00:00:37,408 --> 00:00:41,093
equal to sum of the means of the individual random variables.

14
00:00:41,093 --> 00:00:43,982
And the variance of the sum being the sum of the variances.

15
00:00:43,982 --> 00:00:46,215
And then the standard deviation being the square root of

16
00:00:46,215 --> 00:00:47,190
the variance.

17
00:00:47,190 --> 00:00:50,130
And then we can calculate probabilities for this sum of random

18
00:00:50,130 --> 00:00:54,900
variables even if each one is not normal by using the norm dysfunction

19
00:00:54,900 --> 00:00:58,500
and we can calculate percentiles by using the norm inverse function.

20
00:00:58,500 --> 00:01:01,070
And so, on the next video, we'll give you a really neat

21
00:01:01,070 --> 00:01:05,970
demonstration of the central limit theorem by tossing a die 30 times.

