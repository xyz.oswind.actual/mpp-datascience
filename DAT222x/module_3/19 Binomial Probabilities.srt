0
00:00:00,730 --> 00:00:04,726
A binomial probability is a probability that an n trial binomial

1
00:00:04,726 --> 00:00:07,900
experiment results in exactly x successes.

2
00:00:07,900 --> 00:00:09,161
Before getting into the math,

3
00:00:09,161 --> 00:00:12,550
let's talk about the notation that's used in binomial probabilities.

4
00:00:12,550 --> 00:00:15,280
Imagine that we are tossing a coin six times.

5
00:00:15,280 --> 00:00:19,200
This is n or the number of trials in our binomial experiment.

6
00:00:19,200 --> 00:00:21,030
Imagine that we are interested in tails.

7
00:00:21,030 --> 00:00:23,990
We toss the coin and note the result of each toss.

8
00:00:23,990 --> 00:00:26,710
In this example we obtained two tails.

9
00:00:26,710 --> 00:00:29,920
This is x or the number of successes in our experiment.

10
00:00:29,920 --> 00:00:32,722
Further, we know that the probability of success

11
00:00:32,722 --> 00:00:34,867
on an individual trial P, is 0.5.

12
00:00:34,867 --> 00:00:40,870
And the probability of failure 1-P, is known as Q and is 0.5.

13
00:00:40,870 --> 00:00:44,780
As another example, imagine that we are rolling a die four times.

14
00:00:44,780 --> 00:00:48,250
We are interested in rolling threes so on n equals 4,

15
00:00:48,250 --> 00:00:51,111
P equals one sixth and Q equals five sixths.

16
00:00:51,111 --> 00:00:53,888
To calculate a binomial probability, or

17
00:00:53,888 --> 00:00:57,777
the probability of an n trial binomial experiment results

18
00:00:57,777 --> 00:01:01,480
in exactly x success as we use this formula.

19
00:01:01,480 --> 00:01:05,060
See this value, that with the n and the c and the r?

20
00:01:05,060 --> 00:01:07,740
This is is the number of combinations of n things,

21
00:01:07,740 --> 00:01:11,430
taken r at a time, which translates into factorial math.

22
00:01:11,430 --> 00:01:11,930
Yay!

23
00:01:11,930 --> 00:01:12,850
But don't freak out.

24
00:01:12,850 --> 00:01:15,150
It's actually super easy to calculate this.

25
00:01:15,150 --> 00:01:18,610
I'll show you an example, but Excel makes it very, very easy.

26
00:01:18,610 --> 00:01:21,800
So, what is the probability that we will toss exactly two tails

27
00:01:21,800 --> 00:01:24,340
in exactly six tosses of a coin?

28
00:01:24,340 --> 00:01:25,033
Here's what we know.

29
00:01:25,033 --> 00:01:29,653
In this example, N = 6, X = 2, P = 0.5,

30
00:01:29,653 --> 00:01:33,352
Q = 0.5, 6 factorial is 720,

31
00:01:33,352 --> 00:01:38,130
2 factorial is 2, and 4 factorial is 24.

32
00:01:38,130 --> 00:01:42,440
So we plug all those numbers into the math, and we get this.

33
00:01:42,440 --> 00:01:44,038
The probability is 0.234.

34
00:01:44,038 --> 00:01:46,910
What about our die example?

35
00:01:46,910 --> 00:01:50,920
What is a probability that we will roll one, three in four rolls.

36
00:01:50,920 --> 00:01:56,015
In this case, N=4, X=1, P is one sixth, Q is five sixth,

37
00:01:56,015 --> 00:02:01,128
4 factorial is 24, 1 factorial is 1, 3 factorial is 6.

38
00:02:01,128 --> 00:02:05,232
We plug that all into our magical equation and

39
00:02:05,232 --> 00:02:08,588
we get a probability of 0.388.

