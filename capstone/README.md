DAT102x: Predicting County-Level Rents
================

## Housing costs and income inequality

[![America-Housing-Assistance-Lottery](capstone_thumb.jpg)](https://www.youtube.com/watch?v=Bd1HszxR_6I "One in Four: America's Housing Assistance Lottery")

Over the past five decades, [housing costs have risen faster than
incomes](http://www.jchs.harvard.edu/state-nations-housing-2018),
low-cost housing has been disappearing from the market, and racial
disparities in homeownership rates have deepened.

A [recent study by the economists at Apartment
Lab](https://www.apartmentlist.com/rentonomics/housing-markets-and-income-inequality/)
explains:

> Housing markets are exacerbating the gap between the rich and poor…As
> incomes are growing fastest at the top of the income distribution,
> housing costs are doing the opposite. Americans in the bottom ten
> percent of the income distribution have experienced the most rapid
> growth in housing costs over the past ten years. Moving up the income
> ladder, the richer a household gets the less it has seen rents and
> mortgage payments spike. At the top, we find that the top quartile of
> income earners has actually seen their housing costs fall in the last
> decade.

Housing markets place disproportionate financial stress on low-income
households, who tend to face similar housing costs as middle-income
Americans, despite lower paychecks.

> Those in the lowest quartile make only 27% as much as the median
> household, but they still need to pay 79% of what the median household
> does each month to housing. America’s poor earn just a fraction of the
> median family’s income, but their housing costs are not too different.

Today, most poor renting families spend at least half of their income on
housing costs, [according to the Eviction
Lab](https://evictionlab.org/why-eviction-matters/#affordable-housing-crisis).

Housing issues are intertwined with many other social problems,
including poverty, educational disparities, and health care. Digging
into underlying patterns in housing costs can be an important first step
toward greater visibility and better policies.

In previous capstone challenges, we looked at how poverty, evictions,
and heart health are distributed across US counties. Now, we are
challenging you to consider how poverty, income, health, ethnicity, and
other sociodemographic factors are related to rent costs, and to use
your skills to build a model for predicting median gross rent values in
counties across the United States.
