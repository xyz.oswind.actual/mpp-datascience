0
00:00:00,360 --> 00:00:04,565
>> The universe is made of stories not items.

1
00:00:04,565 --> 00:00:07,855
How true it is? We all love stories,

2
00:00:07,855 --> 00:00:10,510
stories have been part of our existence.

3
00:00:10,510 --> 00:00:12,705
Since very early childhood days,

4
00:00:12,705 --> 00:00:15,670
we have been listening and telling stories.

5
00:00:15,670 --> 00:00:19,860
But what happens when you enter in this corporate world?

6
00:00:19,860 --> 00:00:23,350
How many times we all have been in those meetings,

7
00:00:23,350 --> 00:00:25,085
in those conference rooms,

8
00:00:25,085 --> 00:00:27,725
where everyone is just buried in their laptops,

9
00:00:27,725 --> 00:00:29,890
and then they have got this magical device

10
00:00:29,890 --> 00:00:31,750
in their hand called cellphone,

11
00:00:31,750 --> 00:00:33,850
to which they are tweeting, re-tweeting,

12
00:00:33,850 --> 00:00:36,713
Facebooking, snap-chatting what not?

13
00:00:36,713 --> 00:00:39,460
All in all, the attention span of

14
00:00:39,460 --> 00:00:43,440
the audience has reduced.

15
00:00:43,440 --> 00:00:47,980
So this is what happens when you start to tell a story.

16
00:00:47,980 --> 00:00:50,120
You can see that the eyes

17
00:00:50,120 --> 00:00:52,325
slowly popping out and listening to you.

18
00:00:52,325 --> 00:00:54,660
This is the time when you make the pitch.

19
00:00:54,660 --> 00:00:57,170
This is the time to share the insights.

20
00:00:57,170 --> 00:00:58,646
You might say, yes,

21
00:00:58,646 --> 00:01:00,965
we all know that storytelling is important,

22
00:01:00,965 --> 00:01:03,455
but how do we tell stories with data?

23
00:01:03,455 --> 00:01:08,400
So that's what we'll try to talk in today's session.

24
00:01:08,400 --> 00:01:11,375
However, before I go into that,

25
00:01:11,375 --> 00:01:13,230
let me tell you a story.

26
00:01:13,230 --> 00:01:16,235
Few weeks back, I was already occupied with

27
00:01:16,235 --> 00:01:19,615
work and was hardly paying any attention at home.

28
00:01:19,615 --> 00:01:22,030
So naturally, my wife and

29
00:01:22,030 --> 00:01:24,805
my daughter were not very happy with that.

30
00:01:24,805 --> 00:01:27,305
So one fine day I said, all right,

31
00:01:27,305 --> 00:01:29,575
today I'm going to rest my neural network,

32
00:01:29,575 --> 00:01:31,855
and have quality time with the family.

33
00:01:31,855 --> 00:01:34,420
Listening to this, my wife gave me

34
00:01:34,420 --> 00:01:36,980
a smile back and my daughter yelled,

35
00:01:36,980 --> 00:01:41,065
yeah, today we gonna play with Legos, Legos?

36
00:01:41,065 --> 00:01:42,490
I thought I wanted to rest my neural network,

37
00:01:42,490 --> 00:01:45,275
but I didn't have any option.

38
00:01:45,275 --> 00:01:47,590
I said yes, let's play with Legos.

39
00:01:47,590 --> 00:01:49,585
So she brought all these different colors

40
00:01:49,585 --> 00:01:51,010
of Legos, the reds, the greens,

41
00:01:51,010 --> 00:01:52,540
the yellows, the whites,

42
00:01:52,540 --> 00:01:55,383
and all in different shapes and sizes.

43
00:01:55,383 --> 00:01:57,460
So I started massaging and

44
00:01:57,460 --> 00:02:00,020
then transformation and all those things.

45
00:02:00,020 --> 00:02:03,330
And I spend around 30 minutes doing that.

46
00:02:03,330 --> 00:02:06,055
After 30 minutes into the Lego's,

47
00:02:06,055 --> 00:02:08,090
here are my inventions.

48
00:02:08,090 --> 00:02:15,230
So, you've got Mykea sofa, an apple tree.

49
00:02:15,230 --> 00:02:17,109
And ladies and gentlemen,

50
00:02:17,109 --> 00:02:19,320
presenting to you for the first time

51
00:02:19,320 --> 00:02:22,790
the much rumored , Surface Phone.

52
00:02:22,790 --> 00:02:27,340
So, looking at my inventions, my daughter giggled.

53
00:02:27,340 --> 00:02:31,706
She laughed, and then she asked me a question, Baba,

54
00:02:31,706 --> 00:02:34,950
does this phone sleep on the sofa,

55
00:02:34,950 --> 00:02:38,344
or did someone leave it in the park?

56
00:02:38,344 --> 00:02:40,645
She was craving for a story.

57
00:02:40,645 --> 00:02:42,525
I didn't have any story.

58
00:02:42,525 --> 00:02:44,875
I spent all this much time in

59
00:02:44,875 --> 00:02:47,565
creating all these structures with the Legos,

60
00:02:47,565 --> 00:02:48,975
all these massaging,

61
00:02:48,975 --> 00:02:51,195
eight years and all these transformations,

62
00:02:51,195 --> 00:02:54,660
but I didn't pay any attention to the story.

63
00:02:54,660 --> 00:02:56,545
I told her, Baba spends

64
00:02:56,545 --> 00:02:59,199
so much time in creating these beautiful structures,

65
00:02:59,199 --> 00:03:01,385
you can create any story you want.

66
00:03:01,385 --> 00:03:05,360
So it was almost 8:00 pm by that time,

67
00:03:05,360 --> 00:03:07,520
and my wife called us for dinner.

68
00:03:07,520 --> 00:03:09,800
After dinner, my daughter went to

69
00:03:09,800 --> 00:03:13,100
bed and I went back to work.

70
00:03:13,100 --> 00:03:16,400
And there I was, looking at my dashboards,

71
00:03:16,400 --> 00:03:18,722
looking at my reports,

72
00:03:18,722 --> 00:03:21,080
but her question kept coming back to me,

73
00:03:21,080 --> 00:03:23,370
Baba, where is the story?

74
00:03:23,370 --> 00:03:25,935
So, as you could see,

75
00:03:25,935 --> 00:03:29,480
you've got all these Lego structures which are beautiful.

76
00:03:29,480 --> 00:03:31,715
And similar to that, I've got my dashboard,

77
00:03:31,715 --> 00:03:34,490
some of my reports, they look beautiful.

78
00:03:34,490 --> 00:03:36,890
I spent a lot of time in churning the data,

79
00:03:36,890 --> 00:03:39,475
doing all those ETS transformations.

80
00:03:39,475 --> 00:03:41,058
Using as your data factory,

81
00:03:41,058 --> 00:03:43,735
you've got as your stream analytics through which I'm

82
00:03:43,735 --> 00:03:47,405
pumping the data and then pushing it into our dashboard.

83
00:03:47,405 --> 00:03:50,240
But again, where is the story?

84
00:03:50,240 --> 00:03:52,901
Are these structures talking to each other?

85
00:03:52,901 --> 00:03:55,166
Is this form related to the sofa or

86
00:03:55,166 --> 00:03:57,765
is it related to the apple Tree?

87
00:03:57,765 --> 00:03:59,810
What I've done is, I've left it to

88
00:03:59,810 --> 00:04:03,000
the audience to figure it out the story part.

