0
00:00:04,663 --> 00:00:05,399
Welcome.

1
00:00:05,399 --> 00:00:08,000
And what you're seeing here is powerbi.com,

2
00:00:08,000 --> 00:00:09,520
it's the online service.

3
00:00:09,520 --> 00:00:11,480
So we're gonna learn how to load up our data set and

4
00:00:11,480 --> 00:00:15,160
get that ready for your analysis and your work for this course.

5
00:00:15,160 --> 00:00:18,950
So if you see here, we have this three dash thing up in the upper

6
00:00:18,950 --> 00:00:22,760
left and that will expand to allow you to see your data sets

7
00:00:22,760 --> 00:00:25,650
as well your reports and dashboards for Power BI.

8
00:00:25,650 --> 00:00:30,200
And we're gonna start with this data set tab here and if haven't

9
00:00:30,200 --> 00:00:34,140
downloaded your dataset for the course already please do so and

10
00:00:34,140 --> 00:00:36,610
we're gonna reference that for this portion.

11
00:00:36,610 --> 00:00:40,770
So adding our dataset I'll press that little plus button and

12
00:00:40,770 --> 00:00:42,920
we're going to import a file.

13
00:00:42,920 --> 00:00:46,120
So in our get data thing import git file.

14
00:00:46,120 --> 00:00:50,520
And this is local file so this would be on our computer and

15
00:00:50,520 --> 00:00:54,440
we'll go to our desktop and load up our sample dataset.

16
00:00:54,440 --> 00:01:00,080
And you'll see here in our local file section it says import

17
00:01:00,080 --> 00:01:01,480
Excel data into PowerBI or

18
00:01:01,480 --> 00:01:02,760
upload your Excel file to PowerBI.

19
00:01:02,760 --> 00:01:05,150
We're gonna take the left choice here.

20
00:01:05,150 --> 00:01:09,620
It will just be embedded in Power BI whereas this one is

21
00:01:09,620 --> 00:01:11,990
a combined functionality if you install on Power BI.

22
00:01:13,830 --> 00:01:15,230
Already importing our dataset.

23
00:01:15,230 --> 00:01:16,138
This will take a minute.

24
00:01:18,175 --> 00:01:20,850
The up right saying importing datas can take a little while.

25
00:01:20,850 --> 00:01:21,469
Let's just wait.

26
00:01:25,299 --> 00:01:28,211
Depending on your connection it can take longer obviously.

27
00:01:30,323 --> 00:01:32,260
Okay, our data says ready.

28
00:01:32,260 --> 00:01:35,330
And we can view our data set here.

29
00:01:35,330 --> 00:01:38,200
And you'll notice that we have a Visualizations tab,

30
00:01:38,200 --> 00:01:42,990
a Field tab and some page here It's essentially a blank canvas

31
00:01:42,990 --> 00:01:45,340
where we can pull up our data and visualize it.

32
00:01:45,340 --> 00:01:47,720
So just to make sure that we're all set here,

33
00:01:47,720 --> 00:01:49,460
you can click on a few things.

34
00:01:49,460 --> 00:01:52,593
Okay, looks like our data's loaded and we're ready for

35
00:01:52,593 --> 00:01:54,948
analysis and visualization, thank you.

