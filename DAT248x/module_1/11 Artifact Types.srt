0
00:00:00,430 --> 00:00:03,250
With these three insight types we can make a generalization

1
00:00:03,250 --> 00:00:05,200
about which artifact goes with which insight.

2
00:00:05,200 --> 00:00:08,409
In the case of business visibility, we have reports and

3
00:00:08,409 --> 00:00:10,467
reports are generally one-offs.

4
00:00:10,467 --> 00:00:12,172
They're meant to answer a specific question that

5
00:00:12,172 --> 00:00:12,830
the business has.

6
00:00:12,830 --> 00:00:14,400
And they end up in PowerPoint or email.

7
00:00:15,500 --> 00:00:17,047
With performance improvement,

8
00:00:17,047 --> 00:00:18,927
we're making things like scorecards so

9
00:00:18,927 --> 00:00:21,202
that teams can level up on a given set of metrics.

10
00:00:21,202 --> 00:00:25,188
Or what they most care about in terms of their values as a team

11
00:00:25,188 --> 00:00:26,320
organization.

12
00:00:27,360 --> 00:00:29,957
With opportunity discovery we're creating things like dashboards.

13
00:00:29,957 --> 00:00:32,656
And these are just more complex artifacts meant to be

14
00:00:32,656 --> 00:00:34,312
delivered to stakeholders, so

15
00:00:34,312 --> 00:00:36,660
that they can get insights on their own time.

16
00:00:37,690 --> 00:00:40,582
And with all these artifacts, we're gonna learn how to craft

17
00:00:40,582 --> 00:00:42,874
them into a compelling story in the next module.

18
00:00:42,874 --> 00:00:46,190
And then perfect them as we get to Module 3.

19
00:00:46,190 --> 00:00:46,790
Thank you.

