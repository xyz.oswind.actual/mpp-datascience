0
00:00:00,554 --> 00:00:04,412
Telling an effective story, is your best way to

1
00:00:04,412 --> 00:00:08,952
drive influence and covey information as a presenter.

2
00:00:08,952 --> 00:00:12,236
So it's pretty easy to see why stories matter.

3
00:00:12,236 --> 00:00:15,615
The deeper questions are, how do they work?

4
00:00:15,615 --> 00:00:17,650
What do they comprise?

5
00:00:17,650 --> 00:00:19,810
How do you actually build them?

6
00:00:19,810 --> 00:00:22,980
That's what we're going to do through this course.

7
00:00:22,980 --> 00:00:26,130
And to start, let's get into definitions.

8
00:00:26,130 --> 00:00:27,860
What is the story?

9
00:00:27,860 --> 00:00:32,116
A really good place to begin is to define what a story is not.

10
00:00:32,116 --> 00:00:34,730
Because this is often where we fall off track.

11
00:00:34,730 --> 00:00:38,686
A story is not the description of the thing that you do,

12
00:00:38,686 --> 00:00:43,870
the thing what you did, you're process, a chronology.

13
00:00:43,870 --> 00:00:45,900
These are not necessarily stories,

14
00:00:45,900 --> 00:00:48,830
they're usually bad prescriptions for stories.

15
00:00:48,830 --> 00:00:51,230
Stories aren't your marketing buzzwords.

16
00:00:51,230 --> 00:00:53,760
They're not the pillars in your plan.

17
00:00:53,760 --> 00:00:56,550
They're not the cure for world hunger.

18
00:00:56,550 --> 00:00:59,790
Stories are a much simpler thing.

19
00:00:59,790 --> 00:01:04,749
A story fundamentally is simply a way to connect.

20
00:01:04,749 --> 00:01:07,865
To make a connection with another human being and

21
00:01:07,865 --> 00:01:10,835
the importance of this cannot be overstated.

22
00:01:10,835 --> 00:01:14,450
In a world where people are inundated with information and

23
00:01:14,450 --> 00:01:18,373
our brains and constantly trying to filter through the noise.

24
00:01:18,373 --> 00:01:22,347
Your ability to open the door and get into the brain and

25
00:01:22,347 --> 00:01:26,790
in the mind of someone else very, very powerful.

26
00:01:26,790 --> 00:01:29,201
And once you've done that,

27
00:01:29,201 --> 00:01:34,425
then you have the ability to teach, to land new information,

28
00:01:34,425 --> 00:01:38,154
to transfer knowledge in a way that sticks.

29
00:01:38,154 --> 00:01:42,981
Finally, story is a way to influence what people think and

30
00:01:42,981 --> 00:01:44,070
feel and do.

31
00:01:45,130 --> 00:01:49,713
Stories can help you win people's hearts and minds and

32
00:01:49,713 --> 00:01:51,511
change what they do.

33
00:01:51,511 --> 00:01:54,038
This is the ultimate influence model and

34
00:01:54,038 --> 00:01:55,752
it cuts through the noise.

35
00:01:55,752 --> 00:01:57,960
It gives you the ability to standout,

36
00:01:57,960 --> 00:02:01,410
grab people's attention in a way that you can then take them

37
00:02:01,410 --> 00:02:02,930
where you want them to go.

38
00:02:03,980 --> 00:02:05,830
Whether it's to a point of influence or

39
00:02:05,830 --> 00:02:08,780
just a point of insight.

40
00:02:08,780 --> 00:02:11,612
So up next we'll go deeper into what a story really is and

41
00:02:11,612 --> 00:02:13,840
take a look at what kind of work they can do for

42
00:02:13,840 --> 00:02:15,674
you as an analytics professional.

