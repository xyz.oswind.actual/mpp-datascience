0
00:00:00,016 --> 00:00:04,200
The Nobel Prize-winning economist Daniel Kahneman has

1
00:00:04,200 --> 00:00:07,140
done groundbreaking research into the human mind that has

2
00:00:07,140 --> 00:00:10,300
strong implications for the data storyteller.

3
00:00:10,300 --> 00:00:12,800
Kahneman believes that the human is of two minds,

4
00:00:12,800 --> 00:00:15,480
the first what he calls the narrating self.

5
00:00:15,480 --> 00:00:18,620
This is the background default talk track happening in

6
00:00:18,620 --> 00:00:19,280
our brains.

7
00:00:19,280 --> 00:00:20,810
What did I have for breakfast?

8
00:00:20,810 --> 00:00:22,750
What am I gonna be doing tonight?

9
00:00:22,750 --> 00:00:25,400
A place of semi-consciousness.

10
00:00:25,400 --> 00:00:29,160
The second is what he calls the experiencing self and

11
00:00:29,160 --> 00:00:30,460
this is the place of immersion.

12
00:00:30,460 --> 00:00:34,480
This is the place you're looking to get people as a storyteller,

13
00:00:34,480 --> 00:00:37,386
the here and now, the dynamics of reality.

14
00:00:37,386 --> 00:00:40,230
Kahneman has discovered that when people are in

15
00:00:40,230 --> 00:00:41,475
the experiencing self,

16
00:00:41,475 --> 00:00:44,730
their cognitive processes are very different,

17
00:00:44,730 --> 00:00:49,130
what they walk away from in an experience is very different.

18
00:00:49,130 --> 00:00:52,080
Your job as data storyteller is to get people

19
00:00:52,080 --> 00:00:54,950
out of the narrating self and through the quality of

20
00:00:54,950 --> 00:00:57,780
the experience that you're delivering in your story

21
00:00:57,780 --> 00:01:00,150
to get them into the experiencing self,

22
00:01:00,150 --> 00:01:03,600
where you're taking them to your insights and your outcomes.

