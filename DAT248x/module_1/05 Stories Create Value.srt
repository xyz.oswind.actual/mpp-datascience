0
00:00:00,645 --> 00:00:03,931
Beyond the ability to engage people emotionally and

1
00:00:03,931 --> 00:00:06,693
change what they think, and feel, and do,

2
00:00:06,693 --> 00:00:10,730
stories play another very powerful role in business.

3
00:00:10,730 --> 00:00:14,980
Stories create value, stories make money, lots and

4
00:00:14,980 --> 00:00:16,215
lots of money.

5
00:00:16,215 --> 00:00:20,029
And this was demonstrated not too long ago in something called

6
00:00:20,029 --> 00:00:22,240
the significant objects project.

7
00:00:22,240 --> 00:00:26,736
A group of artists in New York wanted to test the hypothesis

8
00:00:26,736 --> 00:00:31,417
that the attachment of a story could create monetary value to

9
00:00:31,417 --> 00:00:35,109
a worthless piece of junk, like this trinket.

10
00:00:35,109 --> 00:00:39,009
Figurine of St. Vralkomir purchased at a New York City

11
00:00:39,009 --> 00:00:43,330
thrift store for probably too much money at $3.

12
00:00:43,330 --> 00:00:46,600
But what they did where they took dozens of these strange

13
00:00:46,600 --> 00:00:49,740
little artifacts and they gave them the writers, and they said,

14
00:00:49,740 --> 00:00:54,650
write a story and we'll post it on eBay and see what happens.

15
00:00:54,650 --> 00:00:57,520
Now this particular piece, the figure of St.

16
00:00:57,520 --> 00:01:01,273
Vralkomir here, was given to a novelist named Doug Durst and

17
00:01:01,273 --> 00:01:04,732
he wrote a hilarious story about a 13th century figure

18
00:01:04,732 --> 00:01:08,721
Saint Vralkomir, the patron saint of extremely fast dancing.

19
00:01:08,721 --> 00:01:12,769
He wrote a 660 word essay, ridiculous, 660 that's a lot of

20
00:01:12,769 --> 00:01:15,990
words, it takes a long time to read that much.

21
00:01:15,990 --> 00:01:19,700
And he told the story about a little frozen village in 13th

22
00:01:19,700 --> 00:01:23,320
century eastern Europe where an evil Tsar had outlawed fire.

23
00:01:24,330 --> 00:01:24,936
The people,

24
00:01:24,936 --> 00:01:27,860
the villagers in the middle of a cold winter night were huddling

25
00:01:27,860 --> 00:01:30,361
freezing in a church when they looked out the window.

26
00:01:30,361 --> 00:01:32,674
And they saw the strange figure of Vralkomir,

27
00:01:32,674 --> 00:01:35,223
the odd little man who lived on the edge of town, and

28
00:01:35,223 --> 00:01:37,613
who was always shuffling his feet and dancing.

29
00:01:37,613 --> 00:01:41,002
And they watched as he dragged a log into the town square and

30
00:01:41,002 --> 00:01:44,630
he cut it into discs and he placed each disc down.

31
00:01:44,630 --> 00:01:46,500
And on that disk, he stood up and

32
00:01:46,500 --> 00:01:50,240
he started dancing, humming to himself and dancing so fast that

33
00:01:50,240 --> 00:01:54,710
the sparks from his heel ignited the wood, burst into flame.

34
00:01:54,710 --> 00:01:55,753
He took the burning and disk and

35
00:01:55,753 --> 00:01:56,965
handed it to a family in the church.

36
00:01:56,965 --> 00:02:00,068
And one after another, he cut disks and

37
00:02:00,068 --> 00:02:02,616
completely saved the village.

38
00:02:02,616 --> 00:02:06,048
Because they all have fire nobody can duplicate

39
00:02:06,048 --> 00:02:08,880
his feet of terpsichorean ignition and

40
00:02:08,880 --> 00:02:11,930
he died in mid April a beloved martyr.

41
00:02:11,930 --> 00:02:15,460
Ridiculous story posted on Ebay and

42
00:02:15,460 --> 00:02:20,638
they let the bidding begin and by the time the bidding

43
00:02:20,638 --> 00:02:25,581
was done, the figurine of St. Vralkomir sold for

44
00:02:25,581 --> 00:02:30,653
the grand total of $193.50 collectively.

45
00:02:30,653 --> 00:02:35,732
These guys saw a 2700% increase in the value of otherwise

46
00:02:35,732 --> 00:02:40,810
worthless junk simply by virtue of the attachment of a story,

47
00:02:40,810 --> 00:02:45,595
and now you might say well that seems a little extreme, but

48
00:02:45,595 --> 00:02:46,983
its really not.

49
00:02:46,983 --> 00:02:50,508
If you have a mole skin notebook, if you have an Iphone,

50
00:02:50,508 --> 00:02:54,108
odds are you've paid a lot of money, a lot more money than

51
00:02:54,108 --> 00:02:56,365
something's worth on a cog basis.

52
00:02:56,365 --> 00:03:00,126
And yet you see the value because there's a story and

53
00:03:00,126 --> 00:03:04,680
all of the intended emotions and value that story brings.

54
00:03:04,680 --> 00:03:08,260
This is the heart of branding.

55
00:03:08,260 --> 00:03:12,400
Now in the world of data analytics this is critical,

56
00:03:12,400 --> 00:03:17,105
because your ability to tell a story that takes you to a place

57
00:03:17,105 --> 00:03:20,792
of value is fundamentally you doing your job.

58
00:03:20,792 --> 00:03:25,333
Because businesses demand that you translate raw data into

59
00:03:25,333 --> 00:03:29,963
something that is going to have value to the organization and

60
00:03:29,963 --> 00:03:31,158
the business.

