0
00:00:00,852 --> 00:00:02,671
Hi I'm Mario Juarez.

1
00:00:02,671 --> 00:00:03,611
>> And I'm Ben Olsen.

2
00:00:03,611 --> 00:00:06,064
>> Welcome to analytic storytelling for impact

3
00:00:06,064 --> 00:00:07,363
>> The goal of this course is to

4
00:00:07,363 --> 00:00:10,357
turn you into a more effective analytics professional through

5
00:00:10,357 --> 00:00:12,897
proven methods and techniques for taking raw data and

6
00:00:12,897 --> 00:00:14,604
turning it into stories of impact.

7
00:00:14,604 --> 00:00:17,685
>> The problem that we have with data, is the data is

8
00:00:17,685 --> 00:00:20,766
fundamentally inert, and has no real meaning or

9
00:00:20,766 --> 00:00:23,484
value until you give it meaning and value.

10
00:00:23,484 --> 00:00:26,444
Storytelling is the most powerful way to do that.

11
00:00:26,444 --> 00:00:29,066
Effective storytelling creates emotional and

12
00:00:29,066 --> 00:00:32,737
intellectual engagement like no other form of presentation, and

13
00:00:32,737 --> 00:00:35,504
we're here to unlock that for you in this course.

14
00:00:35,504 --> 00:00:38,838
>> Through videos, exercises, and real world scenarios, you'll

15
00:00:38,838 --> 00:00:42,064
have what you need to become an analytics storytelling master.

16
00:00:42,064 --> 00:00:42,964
>> Welcome.

17
00:00:42,964 --> 00:00:43,665
>> Thank you.

