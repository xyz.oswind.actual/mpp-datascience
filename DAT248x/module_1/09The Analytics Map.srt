0
00:00:01,060 --> 00:00:05,040
Analytics stories have their origins in business context.

1
00:00:05,040 --> 00:00:08,360
That is the questions and the insights the business is trying

2
00:00:08,360 --> 00:00:12,060
to get out of the analytics work that they've asked you to do.

3
00:00:12,060 --> 00:00:14,540
That can be what is my revenue this year, or

4
00:00:14,540 --> 00:00:17,660
what are my customers thinking, doing, and feeling?

5
00:00:17,660 --> 00:00:21,750
And that context is what we bring to bear in our data work.

6
00:00:21,750 --> 00:00:24,470
And that's where our stories start.

7
00:00:24,470 --> 00:00:26,780
So we begin to do this data work.

8
00:00:26,780 --> 00:00:29,900
We define and collect new data sets.

9
00:00:29,900 --> 00:00:31,760
We transform and calculate and

10
00:00:31,760 --> 00:00:34,520
create metrics off of those data sets.

11
00:00:34,520 --> 00:00:38,230
And we analyze and explore them in order to derive some form

12
00:00:38,230 --> 00:00:42,960
of insight and artifact that might be useful to the business

13
00:00:42,960 --> 00:00:45,430
to drive it forward in the form of answers or

14
00:00:45,430 --> 00:00:50,560
recommendations or new insights that the decisionmaker might

15
00:00:50,560 --> 00:00:53,190
find from using the artifacts themselves.

16
00:00:53,190 --> 00:00:54,671
And often times they'll go back and say no,

17
00:00:54,671 --> 00:00:55,440
we need to do it again.

18
00:00:55,440 --> 00:00:57,820
We need to analyze and explore again.

19
00:00:57,820 --> 00:01:01,560
We need to change the metric cuz that's actually not what

20
00:01:01,560 --> 00:01:04,380
gets me to the heart of the matter or we need to even look

21
00:01:04,380 --> 00:01:07,440
at different data sets cuz it's the wrong place to look.

22
00:01:07,440 --> 00:01:10,320
Or in fact, I asked the wrong question in the first place and

23
00:01:10,320 --> 00:01:13,330
that's even in itself a new form of insight, right?

24
00:01:13,330 --> 00:01:16,030
And drives you forward in understanding and

25
00:01:16,030 --> 00:01:18,240
making better business decisions.

26
00:01:18,240 --> 00:01:20,545
And in this data map,

27
00:01:20,545 --> 00:01:23,640
in this is data lifecycle, we also have these different roles.

28
00:01:23,640 --> 00:01:24,520
So if you're in

29
00:01:24,520 --> 00:01:26,790
a small business you might be doing all this yourself.

30
00:01:26,790 --> 00:01:30,120
But inside of a larger business context, PMs are generally

31
00:01:30,120 --> 00:01:32,950
the ones who have to both listen to the story of the business and

32
00:01:32,950 --> 00:01:36,370
translate that into actionable stuff for engineers.

33
00:01:36,370 --> 00:01:39,320
And engineers are the ones creating the pipeline and

34
00:01:39,320 --> 00:01:43,210
doing all the munging of data to tee it up for those analysts

35
00:01:43,210 --> 00:01:46,410
who have to then analyze and find the nuggets of gold,

36
00:01:46,410 --> 00:01:49,330
the insights to deliver back to the business so

37
00:01:49,330 --> 00:01:52,120
that decisionmakers can again make those right decisions.

38
00:01:52,120 --> 00:01:54,982
So while the story's flavors might look differently,

39
00:01:54,982 --> 00:01:57,190
it would depend on your role.

40
00:01:57,190 --> 00:02:00,870
The analytics map shows you that really your story can dive into

41
00:02:00,870 --> 00:02:04,580
any point along the path here in this process.

42
00:02:04,580 --> 00:02:07,390
And the stories will craft,

43
00:02:07,390 --> 00:02:10,090
will touch every part of this analytics map.

