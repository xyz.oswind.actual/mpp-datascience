0
00:00:01,380 --> 00:00:04,301
Now that we've learned what a story is, what it does,

1
00:00:04,301 --> 00:00:05,826
and what makes it valuable,

2
00:00:05,826 --> 00:00:08,762
let's jump into what makes an analytic story unique.

3
00:00:08,762 --> 00:00:11,529
An analytic story is a process of bringing data to life through

4
00:00:11,529 --> 00:00:14,193
the combination of visuals and narrative that result in new

5
00:00:14,193 --> 00:00:17,000
understandings, new insights for others.

6
00:00:17,000 --> 00:00:20,190
And this maps directly to a process of framework that

7
00:00:20,190 --> 00:00:21,690
we're calling the analytics map.

8
00:00:21,690 --> 00:00:22,381
We'll show it to you now.

