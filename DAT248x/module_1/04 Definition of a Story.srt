0
00:00:01,890 --> 00:00:06,569
Let's go deeper into this fundamental question of

1
00:00:06,569 --> 00:00:08,098
what is a story.

2
00:00:08,098 --> 00:00:10,690
What does a story comprise?

3
00:00:10,690 --> 00:00:13,910
How do you define what a story is?

4
00:00:13,910 --> 00:00:14,925
Well here's our definition.

5
00:00:14,925 --> 00:00:22,020
A story is an experience of human transformation.

6
00:00:23,100 --> 00:00:25,430
An experience of human transformation.

7
00:00:25,430 --> 00:00:29,255
Every one of these words matter, and if you don't have all of

8
00:00:29,255 --> 00:00:33,461
these elements, the presentation you're giving is not a story.

9
00:00:33,461 --> 00:00:35,370
Let's walk through them one at a time.

10
00:00:35,370 --> 00:00:36,640
An experience.

11
00:00:36,640 --> 00:00:38,690
A story is an experience.

12
00:00:39,800 --> 00:00:42,950
Means that it is grounded in a physical place,

13
00:00:42,950 --> 00:00:48,560
a physical space, a point in time and space, that your job

14
00:00:48,560 --> 00:00:53,880
as a storyteller is to create in the minds of your listeners.

15
00:00:54,880 --> 00:00:56,880
Think about it, if you're a storyteller,

16
00:00:56,880 --> 00:01:00,430
you're in the experience creation business.

17
00:01:00,430 --> 00:01:03,930
And it's your job that at least at one point ideally,

18
00:01:03,930 --> 00:01:05,280
multiple points.

19
00:01:05,280 --> 00:01:08,060
Through your data presentation, you're thinking in terms of

20
00:01:08,060 --> 00:01:10,700
experience and ways that transport people

21
00:01:10,700 --> 00:01:13,870
out of their chairs into the place that you wanna take them.

22
00:01:14,950 --> 00:01:18,994
Second factor, stories are human.

23
00:01:18,994 --> 00:01:22,990
Stories have to have people, your data is not human.

24
00:01:22,990 --> 00:01:25,620
The data has no actually story value.

25
00:01:25,620 --> 00:01:28,774
It's what humans do as a result of the data or

26
00:01:28,774 --> 00:01:32,458
because of the data that makes thing interesting.

27
00:01:32,458 --> 00:01:35,379
This means you must find character that

28
00:01:35,379 --> 00:01:39,901
come to life in very and vivid ways as part of the experience.

29
00:01:39,901 --> 00:01:44,871
And then third transformation, and a good story, a human being,

30
00:01:44,871 --> 00:01:49,666
often multiple human beings are changed either for the better or

31
00:01:49,666 --> 00:01:50,819
the worse, and

32
00:01:50,819 --> 00:01:55,381
it's in the change in the nature of the change that happens.

33
00:01:55,381 --> 00:02:00,408
Where people change themselves because they live the story,

34
00:02:00,408 --> 00:02:04,576
because they feel and live that human experience.

35
00:02:04,576 --> 00:02:06,810
A story has to have all of these.

36
00:02:06,810 --> 00:02:10,227
Now let's take a closer look at the kinds of stories that we

37
00:02:10,227 --> 00:02:13,580
tell in business because this is very important.

38
00:02:13,580 --> 00:02:16,090
Again we often tell the wrong stories.

39
00:02:16,090 --> 00:02:19,788
In business there's two kinds of stories.

40
00:02:19,788 --> 00:02:23,270
The first story the one we hear too much of is what we call

41
00:02:23,270 --> 00:02:25,020
the transactional story.

42
00:02:25,020 --> 00:02:29,999
The transactional story is the description of the transaction,

43
00:02:29,999 --> 00:02:34,490
the process what we built, how the thing works.

44
00:02:34,490 --> 00:02:39,320
The sequencing of events is very boring and it's usually framed

45
00:02:39,320 --> 00:02:42,440
in terms that don't really resonate with your audience,

46
00:02:42,440 --> 00:02:44,720
they resonate with you and people like like you,

47
00:02:44,720 --> 00:02:48,290
and if you stay there you're not making the connections.

48
00:02:48,290 --> 00:02:51,530
The other kinds of business story are the transcendent

49
00:02:51,530 --> 00:02:52,520
stories.

50
00:02:52,520 --> 00:02:55,830
Transcendent stories are the stories of how

51
00:02:55,830 --> 00:03:01,270
human life changed because of the phenomenon

52
00:03:01,270 --> 00:03:04,230
that you're examining in your data analysis.

53
00:03:04,230 --> 00:03:08,700
Transcendent stories go to the place of human experience and

54
00:03:08,700 --> 00:03:11,120
human values and human meaning.

55
00:03:11,120 --> 00:03:13,480
And this is where stories have power.

