0
00:00:00,000 --> 00:00:04,630
Storytelling with data is like writing a comic book.

1
00:00:04,630 --> 00:00:07,270
What happens in a comic book is like,

2
00:00:07,270 --> 00:00:10,208
you've got a free which the author is using.

3
00:00:10,208 --> 00:00:13,515
So, you'll start with a beginning

4
00:00:13,515 --> 00:00:15,200
and then you've got at the end

5
00:00:15,200 --> 00:00:17,385
like how the transformation happens.

6
00:00:17,385 --> 00:00:19,380
Often, what happens is that

7
00:00:19,380 --> 00:00:22,065
when you are doing visualization,

8
00:00:22,065 --> 00:00:25,620
the single visualization not

9
00:00:25,620 --> 00:00:28,125
necessarily provide all the details.

10
00:00:28,125 --> 00:00:30,840
So, can we do something similar to writing

11
00:00:30,840 --> 00:00:34,230
a comic book for data visualization?

12
00:00:34,230 --> 00:00:37,910
And that's one example that I see where it is.

13
00:00:37,910 --> 00:00:41,315
And this is someone who did it on time.

14
00:00:41,315 --> 00:00:43,890
This was crocodile sightings in Queensland,

15
00:00:43,890 --> 00:00:47,595
Australia from 2010 to 2015.

16
00:00:47,595 --> 00:00:50,160
And here, how beautifully the author has

17
00:00:50,160 --> 00:00:52,520
created some text which talks

18
00:00:52,520 --> 00:00:57,675
about what about the crocodile sightings in Australia,

19
00:00:57,675 --> 00:01:00,240
and how beautifully the pictures are used.

20
00:01:00,240 --> 00:01:04,805
And then you see, there were 2,077 reported sightings,

21
00:01:04,805 --> 00:01:07,840
what areas are the most sightings,

22
00:01:07,840 --> 00:01:09,750
five found to be imminent threat,

23
00:01:09,750 --> 00:01:12,180
and then further are the crocodiles

24
00:01:12,180 --> 00:01:15,655
moving to south, and further on.

25
00:01:15,655 --> 00:01:19,320
And we'll examine this with a demo.

