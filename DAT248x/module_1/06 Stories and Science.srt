0
00:00:00,480 --> 00:00:02,093
It's not lost on business or

1
00:00:02,093 --> 00:00:05,261
government leaders that stories have enormous power.

2
00:00:05,261 --> 00:00:08,363
A lot of people are trying to understand exactly what

3
00:00:08,363 --> 00:00:11,113
the dynamics of storytelling are that wield so

4
00:00:11,113 --> 00:00:13,860
much influence over human beings.

5
00:00:13,860 --> 00:00:17,488
The interesting news on this is that in the last 20 years,

6
00:00:17,488 --> 00:00:20,247
there's been an explosion of scientific and

7
00:00:20,247 --> 00:00:24,240
sociological research to give us insights into what is happening

8
00:00:24,240 --> 00:00:26,726
in the human body when a story gets told.

9
00:00:26,726 --> 00:00:28,010
And this valuable for

10
00:00:28,010 --> 00:00:31,646
us, because what we learn here can give us clues and insights

11
00:00:31,646 --> 00:00:35,088
into what it takes for us to become better storytellers.

12
00:00:35,088 --> 00:00:39,689
Let's start here by taking a look at what goes on in

13
00:00:39,689 --> 00:00:43,770
the human brain, the storytelling brain.

14
00:00:43,770 --> 00:00:47,640
Now as a point of contrast, let's take a quick look at what

15
00:00:47,640 --> 00:00:52,100
happens in the brains of people that are suffering through

16
00:00:52,100 --> 00:00:56,710
a boring day of PowerPoint presentations and emails.

17
00:00:56,710 --> 00:01:00,474
That brain is functioning in a very specific and

18
00:01:00,474 --> 00:01:01,893
particular way.

19
00:01:01,893 --> 00:01:06,579
We see in this slide here that the brain basically is focused

20
00:01:06,579 --> 00:01:09,963
on processing language and information.

21
00:01:09,963 --> 00:01:13,589
These two areas that are constantly trying to keep

22
00:01:13,589 --> 00:01:17,731
other parts of the brain shut down so that they can focus on

23
00:01:17,731 --> 00:01:22,393
digesting words, converting them into some sense of meaning,

24
00:01:22,393 --> 00:01:27,316
jumping across the amygdala into long-term memory into something

25
00:01:27,316 --> 00:01:29,280
that will stick.

26
00:01:29,280 --> 00:01:31,370
Brain does not like this.

27
00:01:31,370 --> 00:01:35,470
The brain prefers not to spend too much time doing this.

28
00:01:35,470 --> 00:01:38,294
And yet, it's the thing that, as students and

29
00:01:38,294 --> 00:01:41,411
knowledge workers, we've become accustomed to.

30
00:01:41,411 --> 00:01:43,839
But it's not our preferred state.

31
00:01:43,839 --> 00:01:48,473
The preferred state is the state of the brain when a story

32
00:01:48,473 --> 00:01:49,761
is being told.

33
00:01:49,761 --> 00:01:53,499
And what we know from scans, and FMRIs, and other

34
00:01:53,499 --> 00:01:58,990
research is that when a story gets told, the brain lights up.

35
00:01:58,990 --> 00:02:03,535
The brain suddenly becomes very much like the brain of

36
00:02:03,535 --> 00:02:07,474
a person in physical motion if only sitting and

37
00:02:07,474 --> 00:02:09,399
listening to a story.

38
00:02:09,399 --> 00:02:13,338
And the parts of the brains that we see that are interesting and

39
00:02:13,338 --> 00:02:17,525
new here are the parts that are associated with physical motion.

40
00:02:17,525 --> 00:02:23,205
Moving, running, feeling, actually being sensory.

41
00:02:23,205 --> 00:02:27,358
And secondarily, the emotional parts of the brain that have

42
00:02:27,358 --> 00:02:29,721
to do with feelings internal, and

43
00:02:29,721 --> 00:02:32,497
the things that we actually care about.

44
00:02:32,497 --> 00:02:36,487
And when the brain is lit up like this,

45
00:02:36,487 --> 00:02:42,090
it opens the door to the remembering of details.

46
00:02:42,090 --> 00:02:45,320
In other words, information sitting in short-term memory

47
00:02:45,320 --> 00:02:50,130
jumps up into the cortex where it then can be accessed readily.

48
00:02:50,130 --> 00:02:53,485
And many times, fill your brain with all kinds of information

49
00:02:53,485 --> 00:02:55,850
that you're not even sure why you remember it,

50
00:02:55,850 --> 00:02:59,900
because there it is, through a story.

51
00:02:59,900 --> 00:03:03,140
Now concurrent with this, we also know

52
00:03:03,140 --> 00:03:06,120
about some interesting things that happen in the bloodstream.

53
00:03:06,120 --> 00:03:08,310
When you listen to a story,

54
00:03:08,310 --> 00:03:12,140
when your brain recognizes that a story is coming, two somewhat

55
00:03:12,140 --> 00:03:14,970
contradictory things happen in the bloodstream.

56
00:03:14,970 --> 00:03:18,678
First, you get a shot of a neuro chemical called oxytocin,

57
00:03:18,678 --> 00:03:21,052
sometimes called the love chemical,

58
00:03:21,052 --> 00:03:23,963
which is associated with empathy and warmth.

59
00:03:23,963 --> 00:03:27,580
A mother holding a newborn baby, a person talking to a friend,

60
00:03:27,580 --> 00:03:30,667
a lot of oxytocin floating through the bloodstream.

61
00:03:30,667 --> 00:03:34,727
Concurrent with it, though, is a stress hormone called cortisol,

62
00:03:34,727 --> 00:03:38,590
which is associated with something's going wrong.

63
00:03:38,590 --> 00:03:39,580
Do I fly?

64
00:03:39,580 --> 00:03:40,860
Do I fight?

65
00:03:40,860 --> 00:03:44,400
That stress hormone is something that's often associated with

66
00:03:44,400 --> 00:03:44,995
trouble.

67
00:03:44,995 --> 00:03:48,019
And when these two substances become present in

68
00:03:48,019 --> 00:03:51,118
the bloodstream, as they do automatically when

69
00:03:51,118 --> 00:03:53,775
a story gets told, it changes behavior.

70
00:03:53,775 --> 00:03:56,785
In fact, research shows that simply injecting those two

71
00:03:56,785 --> 00:03:59,405
things into people's bloodstream will make

72
00:03:59,405 --> 00:04:03,395
them 20% more likely to donate to a cause when asked to,

73
00:04:03,395 --> 00:04:05,575
without even understanding why.

74
00:04:05,575 --> 00:04:09,855
So as a storyteller, you can make all of these things happen

75
00:04:09,855 --> 00:04:13,793
and open the door to people's ability to hear you, and

76
00:04:13,793 --> 00:04:18,175
then think and feel in ways that drive impact and influence.

