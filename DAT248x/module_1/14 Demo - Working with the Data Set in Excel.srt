0
00:00:04,521 --> 00:00:05,351
Welcome and

1
00:00:05,351 --> 00:00:09,671
today we're gonna look at how to load up our data set in Excel.

2
00:00:09,671 --> 00:00:11,117
And if you haven't done so

3
00:00:11,117 --> 00:00:14,274
already, please download the data set from our course,

4
00:00:14,274 --> 00:00:18,051
and also download Excel, if you haven't done so, and install it.

5
00:00:18,051 --> 00:00:21,918
So once you open up Excel, you'll see this window pane so

6
00:00:21,918 --> 00:00:24,991
you'll see, you can look at some examples.

7
00:00:24,991 --> 00:00:26,653
You have a blank workbook here.

8
00:00:26,653 --> 00:00:30,630
Open other workbooks is probably the way that you can get to your

9
00:00:30,630 --> 00:00:34,609
downloaded workbook and our data says already on our desktop so

10
00:00:34,609 --> 00:00:36,874
I'm just gonna double click on it.

11
00:00:36,874 --> 00:00:41,935
And it's opening it up, and you'll see our data set.

12
00:00:41,935 --> 00:00:45,078
So we have a bunch of rows and columns, something to

13
00:00:45,078 --> 00:00:48,661
familiarize yourself with as we go throughout this course

14
00:00:48,661 --> 00:00:52,536
this is what you'll be using to create your visualizations and

15
00:00:52,536 --> 00:00:54,490
your story for each module.

16
00:00:54,490 --> 00:00:57,540
So, there's many different ways to go about analysis in Excel

17
00:00:57,540 --> 00:00:58,410
and visualization.

18
00:00:58,410 --> 00:01:01,150
Just showing you one method quickly to

19
00:01:01,150 --> 00:01:04,040
get to some visualizations as fast as possible.

20
00:01:04,040 --> 00:01:05,670
You can insert a pivot table.

21
00:01:05,670 --> 00:01:07,902
So this is a way to sort of condense and

22
00:01:07,902 --> 00:01:10,862
combine your data in interesting and easy ways.

23
00:01:10,862 --> 00:01:14,202
Let's just pull up a couple fields here.

24
00:01:14,202 --> 00:01:16,763
So Vendor Site, Time To Close.

25
00:01:16,763 --> 00:01:19,454
Ignore it for right now with the necessary,

26
00:01:19,454 --> 00:01:21,630
how you interpret those columns.

27
00:01:21,630 --> 00:01:27,220
So the value, I'm gonna change it to average of time to close.

28
00:01:27,220 --> 00:01:28,773
This is how long things take to close.

29
00:01:28,773 --> 00:01:32,753
And I'm going to insert a pivot chart.

30
00:01:32,753 --> 00:01:36,724
So just a quick way to show you how to visualize data and

31
00:01:36,724 --> 00:01:41,312
Excel going from just opening up Excel to already having a nice

32
00:01:41,312 --> 00:01:45,918
chart here with visualization you can use for your analysis.

