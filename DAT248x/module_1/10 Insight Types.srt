0
00:00:00,180 --> 00:00:02,430
We've been talking a lot about insights.

1
00:00:02,430 --> 00:00:06,190
And insights actually come in a few different flavors.

2
00:00:06,190 --> 00:00:07,790
Let's take a look.

3
00:00:07,790 --> 00:00:10,630
Insights first come in the form of business visibility.

4
00:00:10,630 --> 00:00:13,540
That's where we're trying to just provide some sort of

5
00:00:13,540 --> 00:00:17,530
insight around what's happening, in a given metric,

6
00:00:17,530 --> 00:00:19,190
in a single visualization.

7
00:00:19,190 --> 00:00:20,040
It keeps things simple.

8
00:00:20,040 --> 00:00:20,760
So, in this case,

9
00:00:20,760 --> 00:00:23,050
we have a bar chart with mean time to customer,

10
00:00:23,050 --> 00:00:25,940
a single metric displayed by a bar chart.

11
00:00:25,940 --> 00:00:27,095
That way, you can see what's happening.

12
00:00:27,095 --> 00:00:29,394
But there's not a lot after that, that you can ask or

13
00:00:29,394 --> 00:00:31,440
interrogate from the visualization itself.

14
00:00:32,520 --> 00:00:34,320
Then moving forward, we have performance improvement.

15
00:00:34,320 --> 00:00:35,750
That's one step deeper.

16
00:00:35,750 --> 00:00:37,140
We put a target on it.

17
00:00:37,140 --> 00:00:40,430
Let's say we're trying to drive to this certain threshold, and

18
00:00:40,430 --> 00:00:43,170
we can change certain behaviors to move the needle on that

19
00:00:43,170 --> 00:00:43,760
metric and

20
00:00:43,760 --> 00:00:46,690
use the visualizations themselves to move that forward.

21
00:00:46,690 --> 00:00:50,920
And thirdly, the type of insight that you have in business is

22
00:00:50,920 --> 00:00:52,010
opportunity discovery.

23
00:00:52,010 --> 00:00:53,590
This is your aha moment,

24
00:00:53,590 --> 00:00:57,200
perhaps the most exciting portion of our analytics work

25
00:00:57,200 --> 00:01:00,330
will be to discover something new that we hadn't known before.

26
00:01:00,330 --> 00:01:03,860
And we're always trying to drive to this more complex version

27
00:01:03,860 --> 00:01:05,850
with the analyses we deliver.

28
00:01:05,850 --> 00:01:07,890
And it's not that these come in onesies, twosies.

29
00:01:07,890 --> 00:01:11,611
Hopefully the artifacts that we develop and the presentations

30
00:01:11,611 --> 00:01:15,472
that we're giving can provide a number of insights in succession

31
00:01:15,472 --> 00:01:17,859
and different combinations thereof and

32
00:01:17,859 --> 00:01:20,760
in order to provide extra value to the business.

