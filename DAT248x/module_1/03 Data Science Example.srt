0
00:00:00,070 --> 00:00:03,080
>> Alright. So this is one example

1
00:00:03,080 --> 00:00:06,788
off one dashboard that I had created,

2
00:00:06,788 --> 00:00:08,660
and as you can see like these are

3
00:00:08,660 --> 00:00:11,870
all these beautiful structures which I was talking about.

4
00:00:11,870 --> 00:00:17,660
And you've got all these different frames that I have.

5
00:00:17,660 --> 00:00:20,935
Let's see what we can do to make them relevant.

6
00:00:20,935 --> 00:00:25,515
Like let's use panels and do it more like a comic book.

7
00:00:25,515 --> 00:00:27,515
The first thing that you would see is,

8
00:00:27,515 --> 00:00:32,080
maybe we try to remove the tar junks and what we do is,

9
00:00:32,080 --> 00:00:33,650
like the first thing that just comes to

10
00:00:33,650 --> 00:00:35,640
my eyes is the "Borders."

11
00:00:35,640 --> 00:00:38,030
Like is there a use of these borders?

12
00:00:38,030 --> 00:00:43,775
So maybe we'll just start removing them one by one.

13
00:00:43,775 --> 00:00:50,020
And as I go here,

14
00:00:50,020 --> 00:00:53,040
you'll see the difference it makes.

15
00:00:53,040 --> 00:00:57,012
So the "Border" is off over here,

16
00:00:57,012 --> 00:01:06,491
"Border" is off here and the "Border" is off here,

17
00:01:06,491 --> 00:01:10,030
and the last piece.

18
00:01:10,890 --> 00:01:16,785
Okay, I think I've got the "Filters" as well.

19
00:01:16,785 --> 00:01:21,040
And I'll talk about the filters in a few seconds.

20
00:01:21,040 --> 00:01:24,300
Okay. Just by removing the borders you could see

21
00:01:24,300 --> 00:01:27,530
there is a little bit difference like your eyesight,

22
00:01:27,530 --> 00:01:30,415
they are not being caught into the borders.

23
00:01:30,415 --> 00:01:32,920
So you have removed that chunk piece.

24
00:01:32,920 --> 00:01:34,230
Now further what you could do

25
00:01:34,230 --> 00:01:36,031
is you've got these "Filters."

26
00:01:36,031 --> 00:01:38,235
First of all the place of the "Filters"

27
00:01:38,235 --> 00:01:41,010
is on the right side and for

28
00:01:41,010 --> 00:01:43,772
the English speaking audience what normally

29
00:01:43,772 --> 00:01:46,860
happens is we look to the screen from,

30
00:01:46,860 --> 00:01:48,265
either left to right,

31
00:01:48,265 --> 00:01:49,875
or top to bottom,

32
00:01:49,875 --> 00:01:52,895
and filters should be placed in such place.

33
00:01:52,895 --> 00:01:55,260
Like either they should be in the left or on the top.

34
00:01:55,260 --> 00:01:57,075
But first what we'll do is,

35
00:01:57,075 --> 00:01:58,950
you have this capability of

36
00:01:58,950 --> 00:02:01,625
making these filters as drop down.

37
00:02:01,625 --> 00:02:03,471
So I've done that.

38
00:02:03,471 --> 00:02:12,499
And further let's go back and I do that again over here,

39
00:02:12,499 --> 00:02:19,167
same thing, maybe I will have to increase and drop down.

40
00:02:19,167 --> 00:02:25,415
Okay. Back to report and changing the size,

41
00:02:25,415 --> 00:02:29,344
and moving these guys all over here,

42
00:02:29,344 --> 00:02:31,851
moving these guys all over here.

43
00:02:31,851 --> 00:02:34,655
And further what you could also do over here is,

44
00:02:34,655 --> 00:02:38,285
you don't have to limit yourself to just this canvas.

45
00:02:38,285 --> 00:02:40,426
You go over here,

46
00:02:40,426 --> 00:02:43,000
and this is the "Page

47
00:02:43,000 --> 00:02:46,195
setting" and change the "Page size".

48
00:02:46,195 --> 00:02:49,775
Right now there's a custom 16 by 9.

49
00:02:49,775 --> 00:02:53,385
And then when we change it to this,

50
00:02:53,385 --> 00:02:57,320
maybe just increase it by 1720

51
00:02:57,320 --> 00:03:06,847
and change the "View" to "Page view" and "Actual size".

52
00:03:06,847 --> 00:03:11,660
And further, what we could do is,

53
00:03:11,660 --> 00:03:17,120
we can move this stuff at the bottom.

54
00:03:17,580 --> 00:03:29,374
Not too many things on, over here.

55
00:03:29,374 --> 00:03:37,051
Okay. So, this goes down again,

56
00:03:37,051 --> 00:03:42,120
here and okay, so here.

57
00:03:42,120 --> 00:03:43,680
And further what we could do is,

58
00:03:43,680 --> 00:03:45,215
what are we talking about?

59
00:03:45,215 --> 00:03:49,295
Are we talking about bananas?

60
00:03:49,295 --> 00:03:51,430
Or are we talking about like what

61
00:03:51,430 --> 00:03:53,710
exactly this visual is about?

62
00:03:53,710 --> 00:03:56,650
So maybe we can add a "Text box" which talks

63
00:03:56,650 --> 00:04:00,165
about what this data is about.

64
00:04:00,165 --> 00:04:01,810
So this data is about

65
00:04:01,810 --> 00:04:07,683
the searches for data science in a search engine.

66
00:04:07,683 --> 00:04:13,429
"Searches for data science."

67
00:04:13,429 --> 00:04:17,220
And you can change this from

68
00:04:17,220 --> 00:04:21,778
"Light" to "Regular" and then further "44".

69
00:04:21,778 --> 00:04:32,525
"Searches for Data Science",

70
00:04:32,525 --> 00:04:34,050
and you should be able to see

71
00:04:34,050 --> 00:04:35,490
like what difference this has

72
00:04:35,490 --> 00:04:39,340
made into the "Visualization" that we started with.

73
00:04:39,340 --> 00:04:41,310
Initially when we started with,

74
00:04:41,310 --> 00:04:44,485
we just dumped everything in a single view

75
00:04:44,485 --> 00:04:46,380
and we left it to the audience to

76
00:04:46,380 --> 00:04:48,390
understand what are we talking about?

77
00:04:48,390 --> 00:04:50,520
Are we talking about

78
00:04:50,520 --> 00:04:53,940
a particular graph or

79
00:04:53,940 --> 00:04:56,960
is it like we just left it to the audience?

80
00:04:56,960 --> 00:04:59,670
So, here you can see clearly like

81
00:04:59,670 --> 00:05:02,775
after removing the tar junk what has happened.

82
00:05:02,775 --> 00:05:06,165
And this is just the initial part of it.

83
00:05:06,165 --> 00:05:10,270
And I can spend more time on it to modify this further,

84
00:05:10,270 --> 00:05:13,020
but I've already done a part of it.

85
00:05:13,020 --> 00:05:17,045
And let me show you what I've got over here.

86
00:05:17,045 --> 00:05:19,980
So what you see here is,

87
00:05:19,980 --> 00:05:22,725
and again this is what I'm doing in the Power BI.

88
00:05:22,725 --> 00:05:24,925
And the same data that I was showing you

89
00:05:24,925 --> 00:05:27,885
in the previous the scratch portion.

90
00:05:27,885 --> 00:05:29,820
And here what you see is,

91
00:05:29,820 --> 00:05:31,590
" Data Science, Analytic,

92
00:05:31,590 --> 00:05:33,300
Big data, Machine learning."

93
00:05:33,300 --> 00:05:38,670
These are the key words since 2015 and onwards in Bing.

94
00:05:38,670 --> 00:05:42,810
So here, what you would see is, I'm using text.

95
00:05:42,810 --> 00:05:44,850
So first of all, breaking it like

96
00:05:44,850 --> 00:05:47,010
a comic book, panel by panel.

97
00:05:47,010 --> 00:05:48,540
What is data science?

98
00:05:48,540 --> 00:05:51,364
When people are searching for data science,

99
00:05:51,364 --> 00:05:53,900
if you look into the top three one like

100
00:05:53,900 --> 00:05:56,563
you see Wikipedia and again Wikipedia,

101
00:05:56,563 --> 00:05:57,960
the difference is HTTPS.

102
00:05:57,960 --> 00:06:02,360
So, whenever people are searching for data science,

103
00:06:02,360 --> 00:06:05,555
they're actually going to want to learn about it.

104
00:06:05,555 --> 00:06:07,865
And further these Wikipedia articles

105
00:06:07,865 --> 00:06:10,645
are followed by Coursera,

106
00:06:10,645 --> 00:06:14,210
and some of the other MOOC providers.

107
00:06:14,210 --> 00:06:16,040
So, basically there is a thirst about

108
00:06:16,040 --> 00:06:18,905
people wanting to learn about data science.

109
00:06:18,905 --> 00:06:20,610
As we go further down,

110
00:06:20,610 --> 00:06:22,460
what you would see is,

111
00:06:22,460 --> 00:06:25,340
these are the places where the searches are being made.

112
00:06:25,340 --> 00:06:29,530
And then, what are the top sites people are clicking?

113
00:06:29,530 --> 00:06:31,820
And then, when people are searching with

114
00:06:31,820 --> 00:06:34,250
data science and a degree or certification,

115
00:06:34,250 --> 00:06:36,050
you'll see an uptake on it.

116
00:06:36,050 --> 00:06:38,665
So already, we see a good uptake

117
00:06:38,665 --> 00:06:43,385
in interest in people searching for these things.

118
00:06:43,385 --> 00:06:46,958
Further, when they go and search for these terms,

119
00:06:46,958 --> 00:06:50,797
these are the top destination URLs they are going,

120
00:06:50,797 --> 00:06:54,200
and followed by top universities visited.

121
00:06:54,200 --> 00:06:57,155
Just from the URL which people are visiting,

122
00:06:57,155 --> 00:06:59,265
we are able to figure out that a lot bunch of

123
00:06:59,265 --> 00:07:02,510
searches are going to Berkeley,

124
00:07:02,510 --> 00:07:06,275
New York University and other places.

125
00:07:06,275 --> 00:07:08,975
Further as we go down you'll see,

126
00:07:08,975 --> 00:07:10,970
these are the places which people are

127
00:07:10,970 --> 00:07:13,520
going to look for data science jobs.

128
00:07:13,520 --> 00:07:16,580
And like that we can go further and further.

129
00:07:16,580 --> 00:07:19,753
But look at where we have come.

130
00:07:19,753 --> 00:07:22,395
We started with just four or five chats

131
00:07:22,395 --> 00:07:24,401
similar to the Lego structures,

132
00:07:24,401 --> 00:07:26,090
and now we are actually

133
00:07:26,090 --> 00:07:28,425
talking about data. There is a story.

134
00:07:28,425 --> 00:07:30,275
There is a flow which you could follow.

135
00:07:30,275 --> 00:07:31,655
So, similar to that,

136
00:07:31,655 --> 00:07:35,540
you can do that for any data set that you have got.

137
00:07:35,540 --> 00:07:37,160
You don't have to limit yourself

138
00:07:37,160 --> 00:07:38,945
with just the interaction piece,

139
00:07:38,945 --> 00:07:40,565
that if I click on this chart,

140
00:07:40,565 --> 00:07:43,075
this chart should get updated or not.

141
00:07:43,075 --> 00:07:45,485
But what you have to think of it

142
00:07:45,485 --> 00:07:48,920
as storytelling with data.

143
00:07:48,920 --> 00:07:52,500
Like what is your data telling about?

