0
00:00:00,000 --> 00:00:02,270
>> Another component, of

1
00:00:02,270 --> 00:00:05,709
storytelling element with data, is text.

2
00:00:05,709 --> 00:00:08,240
So, I'll give one example of

3
00:00:08,240 --> 00:00:12,370
a traffic violation data set that I looked on.

4
00:00:12,370 --> 00:00:14,585
So basically, few weeks back,

5
00:00:14,585 --> 00:00:18,058
I was issued a parking violation ticket.

6
00:00:18,058 --> 00:00:20,160
And I went to Seattle Police Department site,

7
00:00:20,160 --> 00:00:22,775
and then got the data about all of

8
00:00:22,775 --> 00:00:28,520
parking violation tickets that have been issued in 2016.

9
00:00:28,520 --> 00:00:31,960
So, we'll see what we have in that data set.

10
00:00:31,960 --> 00:00:33,889
So what you see over here is,

11
00:00:33,889 --> 00:00:36,070
this is the data that I got in

12
00:00:36,070 --> 00:00:39,085
the CSV format from the Seattle Police Department,

13
00:00:39,085 --> 00:00:42,213
which talks about different cases,

14
00:00:42,213 --> 00:00:46,615
and then, parking violation tickets that have been given,

15
00:00:46,615 --> 00:00:49,555
the time when the ticket was issued,

16
00:00:49,555 --> 00:00:54,610
the location, the district, and all those latitude,

17
00:00:54,610 --> 00:00:57,130
longitude which is useful for a map,

18
00:00:57,130 --> 00:01:00,390
if you want to plot a map I'm sure,

19
00:01:00,390 --> 00:01:03,734
which area had the most of the parking violation tickets.

20
00:01:03,734 --> 00:01:05,530
And then they have got some type

21
00:01:05,530 --> 00:01:07,850
of groupings they are doing, date,

22
00:01:07,850 --> 00:01:11,470
and then further what I've done is,

23
00:01:11,470 --> 00:01:14,210
I've calculated the hour of the incident,

24
00:01:14,210 --> 00:01:17,208
what hour the ticket was issued.

25
00:01:17,208 --> 00:01:20,205
And then I've got this index field.

26
00:01:20,205 --> 00:01:22,585
And then you also have the days of the week.

27
00:01:22,585 --> 00:01:25,645
So, Monday, Tuesday, and then the month.

28
00:01:25,645 --> 00:01:28,120
So further, what we will do is,

29
00:01:28,120 --> 00:01:32,320
let's go here, and the only change

30
00:01:32,320 --> 00:01:34,280
that you would see in this probably a format

31
00:01:34,280 --> 00:01:35,970
is the color right now.

32
00:01:35,970 --> 00:01:39,805
So, I've chosen page background as black

33
00:01:39,805 --> 00:01:44,278
but the default one that you see is the white one.

34
00:01:44,278 --> 00:01:47,101
So maybe just use the white one.

35
00:01:47,101 --> 00:01:48,220
And what we'll do is,

36
00:01:48,220 --> 00:01:51,390
we'll start seeing how many tickets were issued.

37
00:01:51,390 --> 00:01:54,355
And one thing that I've done is,

38
00:01:54,355 --> 00:01:55,995
I have a Page Filter over here,

39
00:01:55,995 --> 00:01:58,215
which says "Show me

40
00:01:58,215 --> 00:02:04,345
all that data after 31 of December 2015."

41
00:02:04,345 --> 00:02:07,915
And then let's see what we have.

42
00:02:07,915 --> 00:02:09,990
So, Seattle Police Department,

43
00:02:09,990 --> 00:02:13,850
and then I drag this counting over here,

44
00:02:13,850 --> 00:02:17,280
and change this to the number,

45
00:02:17,280 --> 00:02:22,180
so around 254,000 tickets were issued.

46
00:02:22,180 --> 00:02:24,000
And when I got this ticket,

47
00:02:24,000 --> 00:02:27,730
I was worried and then now with this data I can

48
00:02:27,730 --> 00:02:29,430
at least tell to my wife that,

49
00:02:29,430 --> 00:02:35,565
"Hey, last year it's 254,000 tickets were issued".

50
00:02:35,565 --> 00:02:37,840
I think there is something wrong in this data,

51
00:02:37,840 --> 00:02:39,940
these were not so many tickets,

52
00:02:39,940 --> 00:02:42,625
but since this 911 log data

53
00:02:42,625 --> 00:02:45,490
has got some of the other data set as well,

54
00:02:45,490 --> 00:02:47,670
so I think we'll have to use a filter.

55
00:02:47,670 --> 00:02:52,125
So, what they have got here is, event.

56
00:02:52,125 --> 00:02:58,385
Let's see, "Page filter" and okay.

57
00:02:58,385 --> 00:02:59,870
So it removed my page filter,

58
00:02:59,870 --> 00:03:03,225
go back and okay.

59
00:03:03,225 --> 00:03:05,468
So, I've got a page filter for a date right now,

60
00:03:05,468 --> 00:03:07,556
and what will have is,

61
00:03:07,556 --> 00:03:12,445
even clearance group as a page level filter.

62
00:03:12,445 --> 00:03:15,420
And, further, you would see,

63
00:03:15,420 --> 00:03:16,830
all the different type of

64
00:03:16,830 --> 00:03:19,590
incidents that are captured in this data set.

65
00:03:19,590 --> 00:03:21,585
But right now, we are interested

66
00:03:21,585 --> 00:03:26,050
in just traffic-related incidents.

67
00:03:26,050 --> 00:03:29,490
Because, I've got this ticket issued so I

68
00:03:29,490 --> 00:03:33,895
want to see how many of these things are happening.

69
00:03:33,895 --> 00:03:38,040
So first thing that you would do is like 44.58.

70
00:03:38,040 --> 00:03:42,640
You don't necessarily need all this decimal accuracy.

71
00:03:42,640 --> 00:03:44,428
So, in the "Data Label",

72
00:03:44,428 --> 00:03:45,700
what you could do is,

73
00:03:45,700 --> 00:03:48,990
you've go to "Display" is Auto.

74
00:03:48,990 --> 00:03:51,445
So, in "Value Decimal",

75
00:03:51,445 --> 00:03:55,835
I can change this to, zero, and as you can see

76
00:03:55,835 --> 00:03:59,755
45,000 tickets were issued last year.

77
00:03:59,755 --> 00:04:02,605
So I was not the only guy who got the ticket.

78
00:04:02,605 --> 00:04:08,733
And then, let's start with something heading.

79
00:04:08,733 --> 00:04:12,435
And you've got a "Textbox" that I can get.

80
00:04:12,435 --> 00:04:16,220
And here I can write,

81
00:04:16,220 --> 00:04:29,690
Parking violations in Seattle.

82
00:04:29,690 --> 00:04:32,270
And change from light to UI,

83
00:04:32,270 --> 00:04:39,167
and a big number, 44 or something so it's visible.

84
00:04:39,167 --> 00:04:45,649
Parking Violations in Seattle.

85
00:04:45,649 --> 00:04:47,385
And further, since we are

86
00:04:47,385 --> 00:04:49,545
doing some kind of storytelling,

87
00:04:49,545 --> 00:04:52,785
I also want to add some kind of a text,

88
00:04:52,785 --> 00:04:58,980
so I can add another stuff text box over here,

89
00:04:58,980 --> 00:05:04,250
which is some story about Seattle.

90
00:05:04,250 --> 00:05:16,280
So in Seattle there are a number of

91
00:05:16,280 --> 00:05:23,040
parking violation tickets.

92
00:05:23,040 --> 00:05:27,180
So you can add a big story over here,

93
00:05:27,180 --> 00:05:29,310
which goes underneath the heading,

94
00:05:29,310 --> 00:05:31,725
which talks basically about what we are doing,

95
00:05:31,725 --> 00:05:33,025
what this data is about,

96
00:05:33,025 --> 00:05:35,430
and if a reader just comes and walks in

97
00:05:35,430 --> 00:05:38,770
and wants to see what this data is about,

98
00:05:38,770 --> 00:05:40,530
there is some kind of a story.

99
00:05:40,530 --> 00:05:44,193
So what's our sample size or sample sizes?

100
00:05:44,193 --> 00:05:47,925
45,000 and then here the count,

101
00:05:47,925 --> 00:05:51,505
maybe we don't need the category label, so.

102
00:05:51,505 --> 00:05:55,175
And I can add here another title

103
00:05:55,175 --> 00:06:00,490
which says number of tickets issued, maybe.

104
00:06:00,610 --> 00:06:11,139
How many tickets were issued?

105
00:06:11,139 --> 00:06:17,940
And change the size from eight to 15.

106
00:06:17,940 --> 00:06:21,040
Okay 13.

107
00:06:21,040 --> 00:06:23,925
So how many tickets are issued?

108
00:06:23,925 --> 00:06:26,495
45,000 tickets were issued.

109
00:06:26,495 --> 00:06:29,730
And then further, what you could do is,

110
00:06:29,730 --> 00:06:33,335
when I got the ticket, I started thinking like,

111
00:06:33,335 --> 00:06:34,695
what's the big time?

112
00:06:34,695 --> 00:06:37,620
How can I save myself from not

113
00:06:37,620 --> 00:06:41,250
getting caught again if I'm parking illegally?

114
00:06:41,250 --> 00:06:44,645
So I want to see the time of the day,

115
00:06:44,645 --> 00:06:48,030
or the hours where people get most tickets.

116
00:06:48,030 --> 00:06:50,455
So let's drag this thing over here,

117
00:06:50,455 --> 00:06:54,375
and we want this hour to be axis.

118
00:06:54,375 --> 00:06:57,195
And then you've got the count over here.

119
00:06:57,195 --> 00:07:01,875
And maybe I can just have a line graph.

120
00:07:01,875 --> 00:07:08,167
So, this number size is small at this point,

121
00:07:08,167 --> 00:07:08,260
but we necessarily don't need the axis.

122
00:07:08,260 --> 00:07:15,325
But you would see that around 8:00 a.m. is the time when,

123
00:07:15,325 --> 00:07:18,930
8:00 a.m. And then there is this 1:00 p.m.

124
00:07:18,930 --> 00:07:21,330
So that's kind of morning,

125
00:07:21,330 --> 00:07:22,858
and then the lunch hour,

126
00:07:22,858 --> 00:07:25,585
where most of the tickets are issued.

127
00:07:25,585 --> 00:07:28,080
So if you're trying, parking illegally,

128
00:07:28,080 --> 00:07:30,100
try to avoid those peak hours.

129
00:07:30,100 --> 00:07:33,540
And further, what you could do is-

130
00:07:33,540 --> 00:07:37,130
Do I need X-Axis, Y-axis?

131
00:07:37,130 --> 00:07:40,820
Maybe not. I'm just looking into trying to understand.

132
00:07:40,820 --> 00:07:46,725
And, further, in the title you have

133
00:07:46,725 --> 00:08:02,672
got time of the day, you need to be

134
00:08:02,672 --> 00:08:06,040
careful and then there is

135
00:08:06,040 --> 00:08:10,200
a peak so I can have this thing.

136
00:08:10,200 --> 00:08:12,095
OK? So I've got that.

137
00:08:12,095 --> 00:08:15,945
So I know what are the rationales. Be careful.

138
00:08:15,945 --> 00:08:19,220
And now, further, what I want to see is that,

139
00:08:19,220 --> 00:08:21,410
is there a particular day of

140
00:08:21,410 --> 00:08:24,050
the week that I should be more careful?

141
00:08:24,050 --> 00:08:28,823
So, let's drop the day of the week over here.

142
00:08:28,823 --> 00:08:31,345
And let's do the same thing.

143
00:08:31,345 --> 00:08:33,490
Axis is the day of the week and then I'm

144
00:08:33,490 --> 00:08:36,570
going to count over here.

145
00:08:36,570 --> 00:08:41,390
What you see is something interesting.

146
00:08:41,390 --> 00:08:43,363
The interesting stuff is, like,

147
00:08:43,363 --> 00:08:47,705
zero is Sunday and then six is Saturday.

148
00:08:47,705 --> 00:08:50,540
And then you've got Monday,

149
00:08:50,540 --> 00:08:52,895
Tuesday, Wednesday, Thursday, Friday.

150
00:08:52,895 --> 00:08:55,100
So you'll see that Saturday and

151
00:08:55,100 --> 00:08:58,755
Sunday have a low number of tickets being issued.

152
00:08:58,755 --> 00:09:00,113
What's the reason?

153
00:09:00,113 --> 00:09:03,020
So the reason that I thought upon is,

154
00:09:03,020 --> 00:09:04,370
it's like the weekend.

155
00:09:04,370 --> 00:09:07,070
Some areas of the parking is free,

156
00:09:07,070 --> 00:09:12,605
so maybe that's the reason it's less so,

157
00:09:12,605 --> 00:09:14,075
"I can kind of

158
00:09:14,075 --> 00:09:17,090
afford when I'm trying to park and other stuff."

159
00:09:17,090 --> 00:09:21,070
Again, I can remove the X-axis and the Y-axis.

160
00:09:21,070 --> 00:09:27,440
Further, I could just add a text here which says,

161
00:09:27,440 --> 00:09:29,895
"The Maxion Fundam exam is happening."

162
00:09:29,895 --> 00:09:32,690
And then I can add text which shows these two points,

163
00:09:32,690 --> 00:09:34,535
that the weekends parking is free.

164
00:09:34,535 --> 00:09:39,840
So you get that. So, I'm getting here, and then further.

165
00:09:39,840 --> 00:09:42,488
So I got how many tickets were issued,

166
00:09:42,488 --> 00:09:44,210
what hours should I be avoiding,

167
00:09:44,210 --> 00:09:46,465
what days I should be more careful.

168
00:09:46,465 --> 00:09:47,840
And then the months.

169
00:09:47,840 --> 00:09:50,990
Like, is there a seasonality that

170
00:09:50,990 --> 00:09:55,235
we see when these tickets are issued more?

171
00:09:55,235 --> 00:09:57,580
So, let's check that out.

172
00:09:57,580 --> 00:10:00,995
And I've got to a month column as well.

173
00:10:00,995 --> 00:10:04,870
So I've got the month as an Axis.

174
00:10:04,870 --> 00:10:09,210
And then I can add count,

175
00:10:09,210 --> 00:10:13,880
and, let's do a different graph this time.

176
00:10:13,880 --> 00:10:19,765
So what do you see?

177
00:10:19,765 --> 00:10:22,090
You see that there are

178
00:10:22,090 --> 00:10:26,120
certain months where you've got more of these things.

179
00:10:26,120 --> 00:10:27,855
So let me expand this;

180
00:10:27,855 --> 00:10:29,710
it's more clear now.

181
00:10:29,710 --> 00:10:34,650
And then, as you see, like this is one,

182
00:10:34,650 --> 00:10:37,220
two is like a January, February.

183
00:10:37,220 --> 00:10:39,190
Let's see if I've got the name of

184
00:10:39,190 --> 00:10:42,765
the months by any chance.

185
00:10:42,765 --> 00:10:47,110
So, instead of the month,

186
00:10:47,110 --> 00:11:01,205
I take this guy, okay now so go back.

187
00:11:01,205 --> 00:11:03,630
Okay. And remove this one, okay. So I'll have to do

188
00:11:03,630 --> 00:11:04,950
a little bit of formatting in

189
00:11:04,950 --> 00:11:08,100
terms of the ordering of this thing.

190
00:11:08,100 --> 00:11:10,200
Right now it's alphabetically ordered.

191
00:11:10,200 --> 00:11:11,940
But you can still make sense that,

192
00:11:11,940 --> 00:11:17,790
February, you see less number of tickets being issued.

193
00:11:17,790 --> 00:11:19,805
Same thing goes for November.

194
00:11:19,805 --> 00:11:23,160
But you see peaks happening in June, August.

195
00:11:23,160 --> 00:11:24,990
So in summer months,

196
00:11:24,990 --> 00:11:27,000
you see the peak.

197
00:11:27,000 --> 00:11:30,145
So I can also get the seasons over here.

198
00:11:30,145 --> 00:11:34,210
So Fall, Spring and Summer Winter.

199
00:11:34,210 --> 00:11:38,250
So Winter is when I can be more happy, like,

200
00:11:38,250 --> 00:11:40,275
if I am not parked correctly,

201
00:11:40,275 --> 00:11:41,520
I have less chances of

202
00:11:41,520 --> 00:11:42,924
getting tickets looking at the trend.

203
00:11:42,924 --> 00:11:45,315
But Summer, you need to be careful.

204
00:11:45,315 --> 00:11:48,960
So you have got that data.

205
00:11:48,960 --> 00:11:52,115
Now, further, what areas I should be avoiding.

206
00:11:52,115 --> 00:11:55,680
So that's where our graph could be useful.

207
00:11:55,680 --> 00:11:58,155
So here what I'm using is I'm using

208
00:11:58,155 --> 00:12:07,355
this ArcGIS map for Power BI.

209
00:12:07,355 --> 00:12:10,685
And here what we'll be adding is

210
00:12:10,685 --> 00:12:14,801
all these different latitude and longitude,

211
00:12:14,801 --> 00:12:17,075
the data which I showed you earlier.

212
00:12:17,075 --> 00:12:20,971
So Latitude over here,

213
00:12:20,971 --> 00:12:26,917
Longitude over here and then you've got the Count.

214
00:12:26,917 --> 00:12:31,115
So, I'm dragging the count over

215
00:12:31,115 --> 00:12:43,110
here to the size.

216
00:12:43,110 --> 00:12:46,080
I can stretch this guy, stretch this

217
00:12:46,080 --> 00:12:59,570
further and I can maximize this guy a little.

218
00:12:59,570 --> 00:13:04,880
I'll maybe let 's show that in a bigger screen.

219
00:13:05,610 --> 00:13:11,810
Okay. So which area should I be avoiding?

220
00:13:11,810 --> 00:13:16,970
I see everywhere, like where the tickets are issued,

221
00:13:16,970 --> 00:13:18,470
like, all over Seattle.

222
00:13:18,470 --> 00:13:21,825
And now I do see some big circles,

223
00:13:21,825 --> 00:13:24,805
so let's see what's going on.

224
00:13:24,805 --> 00:13:28,610
And if you look into this big circle over here,

225
00:13:28,610 --> 00:13:31,710
you'll see this is the area of the Alki Beach

226
00:13:31,710 --> 00:13:35,475
where a bunch of tickets are issued.

227
00:13:35,475 --> 00:13:37,430
So I should definitely be

228
00:13:37,430 --> 00:13:40,620
avoiding illegally parking over there.

229
00:13:40,620 --> 00:13:46,495
Just with this data set as you could see, like,

230
00:13:46,495 --> 00:13:47,590
what we have done is,

231
00:13:47,590 --> 00:13:50,680
we started doing an analysis

232
00:13:50,680 --> 00:13:53,650
looking at what data was provided.

233
00:13:53,650 --> 00:13:56,790
So, I got a parking violation ticket. Let's check it out.

234
00:13:56,790 --> 00:14:00,835
So 45,000 tickets were issued and then,

235
00:14:00,835 --> 00:14:03,075
what day of the week,

236
00:14:03,075 --> 00:14:05,260
what time of the day,

237
00:14:05,260 --> 00:14:06,945
I need to be more careful?

238
00:14:06,945 --> 00:14:09,190
8:00 a.m. and then 11:00

239
00:14:09,190 --> 00:14:12,135
a.m. That's where I need to be more careful.

240
00:14:12,135 --> 00:14:15,870
What day of the week? So weekend, I can afford.

241
00:14:15,870 --> 00:14:18,685
And then you see by seasons.

242
00:14:18,685 --> 00:14:20,385
You see summer is when

243
00:14:20,385 --> 00:14:22,880
most number of tickets are issued, winter

244
00:14:22,880 --> 00:14:29,665
I can be not so much thinking about the parking stuff.

245
00:14:29,665 --> 00:14:32,890
And what area I should be avoiding?

246
00:14:32,890 --> 00:14:36,003
So definitely avoid the Alki Beach Part

247
00:14:36,003 --> 00:14:37,900
but it's all over

248
00:14:37,900 --> 00:14:39,815
the place so you should be more careful,

249
00:14:39,815 --> 00:14:44,480
park legally and not get tickets. All right.

