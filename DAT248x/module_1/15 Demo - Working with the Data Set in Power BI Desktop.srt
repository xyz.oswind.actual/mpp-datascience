0
00:00:04,836 --> 00:00:05,993
Well, welcome and

1
00:00:05,993 --> 00:00:09,270
in this video we're going to be loading our data set.

2
00:00:09,270 --> 00:00:12,740
So if you could open up Power BI Desktop and

3
00:00:12,740 --> 00:00:15,730
if you haven't downloaded it yet, please look at the intro

4
00:00:15,730 --> 00:00:19,350
where you can look at how you can download these tools.

5
00:00:19,350 --> 00:00:21,940
And the first step we're going to do is we're going to

6
00:00:21,940 --> 00:00:22,940
get some data.

7
00:00:22,940 --> 00:00:26,110
So, when you open it up, this is the first screen you will see

8
00:00:26,110 --> 00:00:28,800
and this little section here, get data, click on that.

9
00:00:29,940 --> 00:00:31,500
And you'll have many options.

10
00:00:31,500 --> 00:00:33,320
We're going to simply stick with Excel and

11
00:00:33,320 --> 00:00:35,930
that's the format that our data set is in.

12
00:00:35,930 --> 00:00:38,202
And you can see that there's a lot of other connections,

13
00:00:38,202 --> 00:00:40,670
Power BI gives you tons of options, but

14
00:00:40,670 --> 00:00:42,310
today is just Excel.

15
00:00:42,310 --> 00:00:42,810
Then Connect.

16
00:00:44,120 --> 00:00:46,822
And if you've downloaded the data set, it's okay,

17
00:00:46,822 --> 00:00:48,350
just put it in your desktop.

18
00:00:48,350 --> 00:00:50,180
In this case, that's what I've done as well.

19
00:00:50,180 --> 00:00:52,730
The sample data set and load that up.

20
00:00:52,730 --> 00:00:56,879
You just double click on it and it's thinking about it.

21
00:00:56,879 --> 00:00:59,731
Okay, so in this navigator screen here,

22
00:00:59,731 --> 00:01:02,890
we'll wanna click on requests and surveys.

23
00:01:02,890 --> 00:01:05,300
These are our two tables that we'll be bringing in.

24
00:01:05,300 --> 00:01:07,390
And it also gives you a preview of these, so that's nice.

25
00:01:07,390 --> 00:01:09,310
And that looks correct.

26
00:01:09,310 --> 00:01:10,160
And we'll say, load.

27
00:01:12,340 --> 00:01:14,730
And it's thinking about it, loading.

28
00:01:16,860 --> 00:01:19,280
And, we're almost done but

29
00:01:19,280 --> 00:01:22,060
this last step, what we're going to do, is we have two tables,

30
00:01:22,060 --> 00:01:24,380
but right now they're not really related to one another.

31
00:01:24,380 --> 00:01:26,780
You won't be able to slice and dice them.

32
00:01:26,780 --> 00:01:29,370
They won't talk to each other so,

33
00:01:29,370 --> 00:01:32,710
in order to do that click on your modeling tab in Power BI.

34
00:01:32,710 --> 00:01:37,050
Click on manage relationships and it says there are no active

35
00:01:37,050 --> 00:01:39,980
relationships defined yet, so we're gonna do that.

36
00:01:39,980 --> 00:01:42,530
We're gonna click on the tab that says New.

37
00:01:42,530 --> 00:01:44,370
Select tables and columns that are related to one another.

38
00:01:44,370 --> 00:01:46,459
So, I'm gonna start with Request table.

39
00:01:46,459 --> 00:01:50,880
And there's a column called service request id in request.

40
00:01:50,880 --> 00:01:53,390
And in survey there's a column called SR number.

41
00:01:53,390 --> 00:01:55,480
And these are the two numbers that will join on.

42
00:01:55,480 --> 00:01:58,500
You can see that they're very similar data types and values.

43
00:01:58,500 --> 00:02:02,600
That means that those two data sets will link up and

44
00:02:02,600 --> 00:02:05,250
we'll be able to slice and dice them by each other.

45
00:02:05,250 --> 00:02:07,480
Don't worry about cardinality or cross filter,

46
00:02:07,480 --> 00:02:09,210
just leave it as it is.

47
00:02:09,210 --> 00:02:12,086
And select OK.

48
00:02:12,086 --> 00:02:12,598
There we go.

49
00:02:12,598 --> 00:02:14,630
Now we have that relationship.

50
00:02:14,630 --> 00:02:16,700
We can select Close.

51
00:02:16,700 --> 00:02:20,710
And we're ready to do some data modeling and data visualization.

52
00:02:20,710 --> 00:02:24,690
So let's just, you can save this as you like

53
00:02:24,690 --> 00:02:26,888
with whatever name you'd like and you're all set.

54
00:02:26,888 --> 00:02:27,752
Thank you.

