0
00:00:00,440 --> 00:00:03,688
Now it's time to get creative and do the job of actually

1
00:00:03,688 --> 00:00:06,736
finding and building a story that's compelling.

2
00:00:06,736 --> 00:00:08,570
And for many people this is where it gets a little bit

3
00:00:08,570 --> 00:00:09,820
frightening.

4
00:00:09,820 --> 00:00:11,852
It's one thing to do some analysis,

5
00:00:11,852 --> 00:00:15,136
it's a lot of fun to hear about stories and story theories.

6
00:00:15,136 --> 00:00:18,393
It can be a little bit different when it's time to actually find

7
00:00:18,393 --> 00:00:21,833
your original story that's gonna land where a story didn't exist

8
00:00:21,833 --> 00:00:22,337
before.

9
00:00:22,337 --> 00:00:23,960
But how do you do this?

10
00:00:23,960 --> 00:00:27,245
I use the metaphor of going out and prospecting for gold,

11
00:00:27,245 --> 00:00:29,778
going out and exploring the landscape, and

12
00:00:29,778 --> 00:00:32,998
finding those little gold nuggets those little kernels

13
00:00:32,998 --> 00:00:36,351
that are gonna be at the heart of your story, that are gonna

14
00:00:36,351 --> 00:00:40,007
bring your story to life, and make it feel original and fresh.

15
00:00:40,007 --> 00:00:43,770
So, this is actually really just a process of kind of looking at

16
00:00:43,770 --> 00:00:46,468
your experience through a different lens and

17
00:00:46,468 --> 00:00:48,600
you do this through questionings.

18
00:00:48,600 --> 00:00:50,950
You do this with the kinds of questions,

19
00:00:50,950 --> 00:00:54,610
a process of questions that you ask that are kind of all

20
00:00:54,610 --> 00:00:57,610
keyed on this one fundamental notion of finding out what was

21
00:00:57,610 --> 00:01:01,960
interesting about the analytics experience.

22
00:01:01,960 --> 00:01:04,943
Remember that story map, that analytics map that we had

23
00:01:04,943 --> 00:01:07,872
earlier that showed you all those different aspects.

24
00:01:07,872 --> 00:01:11,332
Every analytics experience has its own process,

25
00:01:11,332 --> 00:01:14,890
its own sequence of events, its own players.

26
00:01:14,890 --> 00:01:18,560
Your job is to look at that with a different point of view.

27
00:01:18,560 --> 00:01:22,294
You do this by describing it as a journey of discovery,

28
00:01:22,294 --> 00:01:24,081
because fundamentally,

29
00:01:24,081 --> 00:01:27,934
every analytics experience is a journey of discovery.

30
00:01:27,934 --> 00:01:31,590
May be successful, may not be successful, but

31
00:01:31,590 --> 00:01:34,570
in all those cases there is something to be gained in

32
00:01:34,570 --> 00:01:38,460
terms of lessons learned of knowledge that you take away.

33
00:01:38,460 --> 00:01:40,370
You wanna describe that,

34
00:01:40,370 --> 00:01:43,590
you wanna put it in terms of an experience, and

35
00:01:43,590 --> 00:01:48,400
ideally the experience that led to your key insights.

36
00:01:48,400 --> 00:01:52,150
So a lot of questions, questions that you can ask around this.

37
00:01:52,150 --> 00:01:57,054
Where did you find conflict in that experience?

38
00:01:57,054 --> 00:02:00,082
Odds are that your analytics experience was right with all

39
00:02:00,082 --> 00:02:02,310
kinds of little brands of conflict.

40
00:02:02,310 --> 00:02:03,240
What were they?

41
00:02:03,240 --> 00:02:03,910
Write them done.

42
00:02:03,910 --> 00:02:05,510
Where were the surprises?

43
00:02:05,510 --> 00:02:08,530
It's almost always something surprising,

44
00:02:08,530 --> 00:02:11,200
a point where something unexpected happened.

45
00:02:11,200 --> 00:02:14,740
Sometimes game changing ways, sometimes in just a way that

46
00:02:14,740 --> 00:02:17,680
adds to the annoyance or the stress.

47
00:02:17,680 --> 00:02:19,140
Where were the stresses,

48
00:02:19,140 --> 00:02:21,200
the team, the people that were involved?

49
00:02:21,200 --> 00:02:23,845
Whether they were the people that were trying to solve

50
00:02:23,845 --> 00:02:26,792
a problem or people for whom the problems are being solved?

51
00:02:26,792 --> 00:02:30,500
Where were the stresses, and who were the characters?

52
00:02:30,500 --> 00:02:32,285
Stories are full of characters.

53
00:02:32,285 --> 00:02:34,995
Your analytics experience is full of characters,

54
00:02:34,995 --> 00:02:36,615
data is full of characters.

55
00:02:36,615 --> 00:02:38,115
And this isn't just the people that

56
00:02:38,115 --> 00:02:40,225
are involved in the analytics processing,

57
00:02:40,225 --> 00:02:43,095
the number crunching, or the artifact creation.

58
00:02:43,095 --> 00:02:46,085
It could be the actual people who are involved in the data

59
00:02:46,085 --> 00:02:46,598
itself.

60
00:02:46,598 --> 00:02:49,641
Sometimes great data stories are about the customers that

61
00:02:49,641 --> 00:02:53,340
are affected through some process represented by the data.

62
00:02:53,340 --> 00:02:54,740
Think of it in terms of people.

63
00:02:54,740 --> 00:02:56,656
Think about the people that are involved and

64
00:02:56,656 --> 00:02:58,535
the human experience that's happening.

65
00:02:58,535 --> 00:03:02,363
And then, really powerful one, think of those

66
00:03:02,363 --> 00:03:06,584
ah-ha moments when things actually became clear.

67
00:03:06,584 --> 00:03:09,930
When did the moment of insight occur?

68
00:03:09,930 --> 00:03:11,910
These are the points where you can look at.

69
00:03:11,910 --> 00:03:15,090
And in the lab you'll see a set of questions that are designed

70
00:03:15,090 --> 00:03:17,870
to get you to just think about these things, and

71
00:03:17,870 --> 00:03:19,490
spark them as ideas.

72
00:03:19,490 --> 00:03:22,795
Your job here is not to write a complete story.

73
00:03:22,795 --> 00:03:25,738
You're not looking for the complete story arc or

74
00:03:25,738 --> 00:03:26,970
a broad narrative.

75
00:03:26,970 --> 00:03:30,150
No, at this point all you're looking for are the elements.

76
00:03:30,150 --> 00:03:33,310
Sparks that fly when you think about these things.

77
00:03:33,310 --> 00:03:35,260
And then later, we'll take those,

78
00:03:35,260 --> 00:03:37,550
put them through a process of evaluation.

79
00:03:37,550 --> 00:03:42,410
So that we can then see which of them actually qualifies

80
00:03:42,410 --> 00:03:45,200
as pieces that will go into the story and

81
00:03:45,200 --> 00:03:46,800
into the artifacts that you build.

