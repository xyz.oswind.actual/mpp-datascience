0
00:00:01,070 --> 00:00:03,860
Let's continue the process of creating an original story that

1
00:00:03,860 --> 00:00:05,560
will land with impact.

2
00:00:05,560 --> 00:00:08,690
In our prior exercise, you identified some potential

3
00:00:08,690 --> 00:00:10,550
elements, potential gold nuggets,

4
00:00:10,550 --> 00:00:14,290
that could serve as compelling components of your story.

5
00:00:14,290 --> 00:00:18,020
Now it's the time to go through a process of evaluating

6
00:00:18,020 --> 00:00:21,100
those pieces to see which ones actually have a place in

7
00:00:21,100 --> 00:00:22,220
this story that you can tell.

8
00:00:22,220 --> 00:00:27,020
Now this is fundamentally a process of evaluating

9
00:00:27,020 --> 00:00:31,010
the nature of your story in terms of what is compelling.

10
00:00:31,010 --> 00:00:34,220
And by compelling, I kinda want you to think not in terms of

11
00:00:34,220 --> 00:00:38,880
maybe some formal thing, but at a very human level

12
00:00:38,880 --> 00:00:42,200
of what actually feels compelling to you.

13
00:00:42,200 --> 00:00:45,110
A really good lens to put on this is

14
00:00:45,110 --> 00:00:46,370
what would you gossip about?

15
00:00:46,370 --> 00:00:49,240
What are the kinds of things that you would share?

16
00:00:49,240 --> 00:00:52,160
What are the things that feel like there's an urgency to them

17
00:00:52,160 --> 00:00:55,580
because they're so interesting at a human level?

18
00:00:55,580 --> 00:00:59,100
That's the lens to put on, and when you think about this,

19
00:00:59,100 --> 00:01:00,590
you think in terms of a few things.

20
00:01:00,590 --> 00:01:04,310
You think in terms of the emotion that surround

21
00:01:04,310 --> 00:01:05,020
those nuggets.

22
00:01:05,020 --> 00:01:07,190
So look at that list of nuggets that you have and

23
00:01:07,190 --> 00:01:09,160
ask what are the emotions that surround these.

24
00:01:09,160 --> 00:01:10,810
Are these sad things?

25
00:01:10,810 --> 00:01:12,656
Are these happy things?

26
00:01:12,656 --> 00:01:15,690
And you kinda wanna think in terms of that audience analysis

27
00:01:15,690 --> 00:01:18,520
that you did and the impact that you defined,

28
00:01:18,520 --> 00:01:21,050
just make sure that it's harmonious.

29
00:01:21,050 --> 00:01:23,600
If you have people who are not

30
00:01:23,600 --> 00:01:26,650
predisposed to liking bad news or negative

31
00:01:26,650 --> 00:01:29,540
emotions, you might wanna think differently about how you

32
00:01:29,540 --> 00:01:31,990
would handle those elements that fall into those categories.

33
00:01:31,990 --> 00:01:33,210
It doesn't mean you don't wanna use them.

34
00:01:33,210 --> 00:01:35,911
It means you wanna use them with care and in some cases,

35
00:01:35,911 --> 00:01:38,220
there's some you'll just put off the table.

36
00:01:38,220 --> 00:01:40,270
What are the emotions surrounding it?

37
00:01:40,270 --> 00:01:41,430
That's one thing,

38
00:01:41,430 --> 00:01:44,425
also is what's compelling is where is their conflict?

39
00:01:44,425 --> 00:01:48,055
What is there conflict on, where do things go wrong?

40
00:01:48,055 --> 00:01:51,690
Where's the stress surrounding those?

41
00:01:51,690 --> 00:01:53,850
Where did things resolve?

42
00:01:53,850 --> 00:01:57,245
That ha moment that you had, when was that ha moment?

43
00:01:57,245 --> 00:02:00,800
And ask yourself, is there something that's so

44
00:02:00,800 --> 00:02:01,550
compelling in that,

45
00:02:01,550 --> 00:02:04,259
that it can find a place in the story that your telling.

46
00:02:05,480 --> 00:02:08,638
As you think about these things again you wanna put that lens of

47
00:02:08,638 --> 00:02:11,990
kind of the filter of gossip,

48
00:02:11,990 --> 00:02:14,460
the filter of what's really interesting.

49
00:02:14,460 --> 00:02:18,308
You're gonna look to get the set of those some subset of what you

50
00:02:18,308 --> 00:02:20,269
did out of the prior exercise and

51
00:02:20,269 --> 00:02:23,842
then from that really think about the nature of the story.

52
00:02:23,842 --> 00:02:25,837
Look at it as a whole, and

53
00:02:25,837 --> 00:02:29,741
then ask yourself how would I distill this down.

54
00:02:29,741 --> 00:02:32,852
Because once you have that set of core elements that comprise

55
00:02:32,852 --> 00:02:35,780
an interesting story it's time to turn the the corner and

56
00:02:35,780 --> 00:02:38,464
to begin to actually think about how you're going to

57
00:02:38,464 --> 00:02:39,950
build it out.

58
00:02:39,950 --> 00:02:43,990
Now yes, you may be building a story that is as long as five or

59
00:02:43,990 --> 00:02:46,910
ten minute presentation or that's gonna be in an elaborate

60
00:02:46,910 --> 00:02:50,510
report or some other complex artifact.

61
00:02:50,510 --> 00:02:53,536
But at this point the process what you're trying to do is

62
00:02:53,536 --> 00:02:54,886
distill it down.

63
00:02:54,886 --> 00:02:58,990
Distill it down, boil it down to it's essence.

64
00:02:58,990 --> 00:03:02,410
Go through the hard work and the discipline of saying that I'm

65
00:03:02,410 --> 00:03:07,304
going to take that full story and say what is essential.

66
00:03:08,395 --> 00:03:11,815
What is the essential point, the one single point?

67
00:03:11,815 --> 00:03:13,865
And you wanna get it down to one single point.

68
00:03:13,865 --> 00:03:18,449
And ideally, what you're looking to do is to get that point

69
00:03:18,449 --> 00:03:20,750
down to the six words or less.

70
00:03:20,750 --> 00:03:24,500
So this is an exercise, you've done that work of evaluating it

71
00:03:24,500 --> 00:03:26,354
and you found those elements.

72
00:03:26,354 --> 00:03:30,955
You need to sit down and get it down to one sentence,

73
00:03:30,955 --> 00:03:33,680
one six word sentence.

74
00:03:33,680 --> 00:03:36,180
It's an art form, a six word story.

75
00:03:36,180 --> 00:03:39,870
The six word story has actually become a bit of a meme.

76
00:03:39,870 --> 00:03:43,530
In fact, The Boston Globe not long ago came up with the story,

77
00:03:43,530 --> 00:03:47,210
a contest where they wanted to find the greatest novel ever

78
00:03:47,210 --> 00:03:49,560
written in six words.

79
00:03:49,560 --> 00:03:53,480
Here it is, greatest novel ever written in six words, For

80
00:03:53,480 --> 00:03:56,420
sale, baby shoes, never worn.

81
00:03:58,040 --> 00:04:01,780
This kind of distillation is full of power.

82
00:04:01,780 --> 00:04:04,260
You look at this and you can see the emotion and

83
00:04:04,260 --> 00:04:08,240
you can see the way that the story begs to be explored.

84
00:04:08,240 --> 00:04:11,100
You wanna learn more, you wanna find more.

85
00:04:11,100 --> 00:04:13,150
And in your process of distillation,

86
00:04:13,150 --> 00:04:15,210
that's what you're looking to do here.

87
00:04:15,210 --> 00:04:17,120
Get it down into six words or less.

88
00:04:17,120 --> 00:04:20,720
And this can be as basic as, it doesn't have to be colorful and

89
00:04:20,720 --> 00:04:22,396
poetic like the Boston Globe one.

90
00:04:22,396 --> 00:04:23,733
It could be,

91
00:04:23,733 --> 00:04:28,208
hey, our launch failed to improve customer perceptions.

92
00:04:28,208 --> 00:04:31,540
Or the competitor's

93
00:04:31,540 --> 00:04:35,676
move into the services category damaged our profitability.

94
00:04:35,676 --> 00:04:40,500
Or travel costs declined as targeted with the new policy.

95
00:04:41,540 --> 00:04:44,690
What you're looking to do is to get to the place where you get

96
00:04:44,690 --> 00:04:47,900
to the heart of the story in one sentence.

97
00:04:47,900 --> 00:04:51,660
Doesn't have to be 6 words, you can get it as much as 10 or 12.

98
00:04:51,660 --> 00:04:54,890
But you wanna bring it down to that because when you get to

99
00:04:54,890 --> 00:04:58,450
this place, then you're in position to do the work of

100
00:04:58,450 --> 00:05:02,550
building and shaping a narrative that will land with real impact.

