0
00:00:01,240 --> 00:00:04,020
Now that you know the nature of the story that you wanna tell,

1
00:00:04,020 --> 00:00:06,630
what's at the heart of it, where you ultimately wanna take

2
00:00:06,630 --> 00:00:09,530
people, it's time to construct the narrative.

3
00:00:10,530 --> 00:00:13,150
How are you gonna tell a story that captures people's attention

4
00:00:13,150 --> 00:00:14,410
from the outside?

5
00:00:14,410 --> 00:00:17,070
Carries them through to the place of insight and

6
00:00:17,070 --> 00:00:19,590
action that you defined at the beginning.

7
00:00:19,590 --> 00:00:23,540
Well, it helps again to look at classic architectures.

8
00:00:23,540 --> 00:00:24,350
In this case,

9
00:00:24,350 --> 00:00:27,970
from this guy, Gustav Freytag, 19th century writer and

10
00:00:27,970 --> 00:00:32,230
philosopher who gave us this concept of the dramatic arc.

11
00:00:32,230 --> 00:00:37,590
The dramatic arc defines the elements of a typical story,

12
00:00:37,590 --> 00:00:39,500
those elements unfold like this.

13
00:00:39,500 --> 00:00:41,970
You have an exposition where you have a character going through

14
00:00:41,970 --> 00:00:44,870
life and then typically something big happens.

15
00:00:44,870 --> 00:00:49,430
Something typically goes either very wrong or very right and

16
00:00:49,430 --> 00:00:52,060
our character finds himself or herself through a course of

17
00:00:52,060 --> 00:00:56,140
rising action, to a place of climax where the story breaks.

18
00:00:56,140 --> 00:00:58,120
The dust settles in the falling action.

19
00:00:58,120 --> 00:01:00,130
And at the end, our character finds himself or

20
00:01:00,130 --> 00:01:03,840
herself transformed at a different level.

21
00:01:03,840 --> 00:01:07,210
This is kind of a classic architecture, an overall

22
00:01:07,210 --> 00:01:11,180
structure that every story needs to comprise in order for

23
00:01:11,180 --> 00:01:14,170
people to feel satisfied that it is a complete story.

