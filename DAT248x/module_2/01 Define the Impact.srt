0
00:00:01,830 --> 00:00:04,330
Now that we know what a story is,

1
00:00:04,330 --> 00:00:08,420
what a story comprises, why it works how it works,

2
00:00:08,420 --> 00:00:11,360
it's time to get to the business of actually building a story.

3
00:00:11,360 --> 00:00:14,575
But before we get to the creative work involved.

4
00:00:14,575 --> 00:00:18,295
It's very important to spend a little time on the analytical,

5
00:00:18,295 --> 00:00:20,915
understanding some of the core elements that

6
00:00:20,915 --> 00:00:22,865
underlie the storytelling process so

7
00:00:22,865 --> 00:00:25,875
that the story you create actually lands.

8
00:00:25,875 --> 00:00:27,885
First here is the question of impact,

9
00:00:27,885 --> 00:00:32,250
defining the impact of the story that you're telling.

10
00:00:32,250 --> 00:00:33,810
Why are you telling a story?

11
00:00:33,810 --> 00:00:35,610
One of the questions that you need to consider.

12
00:00:35,610 --> 00:00:37,310
What is the point?

13
00:00:37,310 --> 00:00:39,000
Where are you going with the story?

14
00:00:39,000 --> 00:00:42,590
If you don't have clarity on why and the work that you want your

15
00:00:42,590 --> 00:00:46,030
story to do, odds are you're gonna be on some thin ice,

16
00:00:46,030 --> 00:00:48,800
you have to get very explicit on this.

17
00:00:48,800 --> 00:00:52,300
And the way that you do this is to think about the problems

18
00:00:52,300 --> 00:00:55,560
that are being addresses in the analytics work itself.

19
00:00:55,560 --> 00:00:58,270
What problems are being addressed in the analytics work?

20
00:00:58,270 --> 00:01:01,180
And then what problems out of the insights that you're

21
00:01:01,180 --> 00:01:04,770
bringing, are being solved through your story?

22
00:01:06,090 --> 00:01:09,230
Ask yourself what impact you want the story to have.

23
00:01:09,230 --> 00:01:11,305
Bottomline on the business.

24
00:01:11,305 --> 00:01:14,283
Your story's a tool that's going to work for you and

25
00:01:14,283 --> 00:01:17,123
it's gotta go to work for you in a way that's gonna

26
00:01:17,123 --> 00:01:19,709
actually benefit the business bottom line.

27
00:01:19,709 --> 00:01:21,030
So how do you do this?

28
00:01:21,030 --> 00:01:22,850
We have some exercises in the lab but

29
00:01:22,850 --> 00:01:25,850
a really core way to think about this from the beginning is just

30
00:01:25,850 --> 00:01:27,580
filling in some blanks here.

31
00:01:27,580 --> 00:01:28,660
Filling in some blanks.

32
00:01:28,660 --> 00:01:32,597
We need to tell a story about blank so that we can blank.

33
00:01:32,597 --> 00:01:35,740
And you just think about this in very simple terms.

34
00:01:35,740 --> 00:01:38,620
Could be as simple as saying, we need to tell a story about

35
00:01:38,620 --> 00:01:41,440
our competitors arrival in the marketplace so

36
00:01:41,440 --> 00:01:44,970
that we can advise management on changes that we can make in

37
00:01:44,970 --> 00:01:46,530
our go to market strategy.

38
00:01:46,530 --> 00:01:50,830
Any of a variety of these based core at the core

39
00:01:50,830 --> 00:01:53,810
around the nature of the problems that you're solving.

40
00:01:53,810 --> 00:01:57,890
What you define in this process will be the thing that tells you

41
00:01:57,890 --> 00:02:00,850
what your story is intended to do, act as

42
00:02:00,850 --> 00:02:04,360
a kind of a North Star for you to the rest of the process so

43
00:02:04,360 --> 00:02:06,730
that you're very clear on where you're going.

