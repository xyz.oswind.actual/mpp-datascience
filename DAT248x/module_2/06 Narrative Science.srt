0
00:00:00,908 --> 00:00:03,741
By this point, you have a sense of what your story's all about.

1
00:00:03,741 --> 00:00:05,484
The elements that make it interesting and

2
00:00:05,484 --> 00:00:08,440
a core statement of what is at the heart of your story.

3
00:00:08,440 --> 00:00:11,758
Now it's time to move to the fun work of crafting your story.

4
00:00:11,758 --> 00:00:14,954
How do you build a story that's original, that feels fresh, and

5
00:00:14,954 --> 00:00:16,998
interesting, and new to your audience.

6
00:00:16,998 --> 00:00:21,435
A good way to begin is to look to classic story structures.

7
00:00:21,435 --> 00:00:24,401
In other words, the shapes, and the nature, and

8
00:00:24,401 --> 00:00:28,226
the construction architecture of story narratives over time.

9
00:00:28,226 --> 00:00:29,028
What does this look like?

10
00:00:29,028 --> 00:00:29,922
Well, let's have a look.

11
00:00:29,922 --> 00:00:32,022
Kurt Vonnegut, the great author,

12
00:00:32,022 --> 00:00:34,892
gave us a sense with a presentation that he gave in

13
00:00:34,892 --> 00:00:37,552
which he plotted out stories on an x.y axis.

14
00:00:37,552 --> 00:00:41,889
With the y axis being good fortune and ill fortune and

15
00:00:41,889 --> 00:00:46,040
the x axis being time, the beginning and the end.

16
00:00:46,040 --> 00:00:49,002
And he said, well every story has a shape,

17
00:00:49,002 --> 00:00:51,396
you can plot it out over this axis.

18
00:00:51,396 --> 00:00:54,668
A good way to look at this is maybe start with a character,

19
00:00:54,668 --> 00:00:58,292
start with a character that's benefiting from good fortune.

20
00:00:58,292 --> 00:01:01,709
He goes through time, everything is going well and

21
00:01:01,709 --> 00:01:03,623
then something goes wrong.

22
00:01:03,623 --> 00:01:06,750
He finds himself mired in ill fortune.

23
00:01:06,750 --> 00:01:10,941
He's in pain, he's suffering, and then the narrative unfolds,

24
00:01:10,941 --> 00:01:14,911
and for whatever reason, whether it's good luck or good work,

25
00:01:14,911 --> 00:01:16,161
pulls himself out,

26
00:01:16,161 --> 00:01:19,342
finds himself in a place of good fortune at the end.

27
00:01:19,342 --> 00:01:23,868
Vonnegut calls this shape the Man in a Hole.

28
00:01:23,868 --> 00:01:27,102
This is just but one story shape of which there are many.

29
00:01:27,102 --> 00:01:30,760
And in fact, Vonnegut in his presentation rather jokingly

30
00:01:30,760 --> 00:01:31,810
said, these shapes are so

31
00:01:31,810 --> 00:01:34,790
beautiful they should be put into a computer.

32
00:01:34,790 --> 00:01:38,280
Well recently, couple of years ago, a group of researchers at

33
00:01:38,280 --> 00:01:42,250
the University of Romonce embarked on doing just that.

34
00:01:42,250 --> 00:01:44,837
They took hundreds and hundreds of stories,

35
00:01:44,837 --> 00:01:49,062
almost 2,000 eBooks and put them through an analytics processing,

36
00:01:49,062 --> 00:01:52,418
where they did sentiment analysis using these same axes.

37
00:01:52,418 --> 00:01:54,524
Here's a look at Romeo and Juliet.

38
00:01:54,524 --> 00:01:57,565
The shape of the story of Romeo and Juliet.

39
00:01:57,565 --> 00:02:01,044
Starts off pretty good, our characters fall in love and

40
00:02:01,044 --> 00:02:02,160
then it goes bad.

41
00:02:03,180 --> 00:02:05,720
They did this again with hundreds and hundreds of books

42
00:02:05,720 --> 00:02:08,650
using big data and a whole bunch of cool number crunching.

43
00:02:08,650 --> 00:02:12,091
And they came up, as they noticed at the end of this,

44
00:02:12,091 --> 00:02:14,532
they came up with a set, a set of six.

45
00:02:14,532 --> 00:02:16,420
You see them here in the lower right.

46
00:02:16,420 --> 00:02:20,907
Six story shapes residually that they noticed kind of came out of

47
00:02:20,907 --> 00:02:25,073
all of these hundred of stories, six classic story forms.

48
00:02:25,073 --> 00:02:26,499
Here's another look at them.

49
00:02:26,499 --> 00:02:30,429
Rags to riches, tragedy Man in the hole as we've just seen,

50
00:02:30,429 --> 00:02:34,669
Icarus, which is the kind of the inverse of that, Cinderella and

51
00:02:34,669 --> 00:02:36,320
Oedipus.

52
00:02:36,320 --> 00:02:37,536
These are six story shapes.

53
00:02:37,536 --> 00:02:40,527
Now the important thing to understand is that these are not

54
00:02:40,527 --> 00:02:41,936
the only six story shapes.

55
00:02:41,936 --> 00:02:44,019
This is a no way a definitive list.

56
00:02:44,019 --> 00:02:47,932
What it is, is a list of a set of patterns.

57
00:02:47,932 --> 00:02:51,783
The human mind loves patterns and as you're building your

58
00:02:51,783 --> 00:02:55,789
story, it's a great idea to look at kind of what is the nature

59
00:02:55,789 --> 00:02:59,400
of your story against these six or maybe others?

60
00:02:59,400 --> 00:03:02,180
And it's also important to remember that a really good

61
00:03:02,180 --> 00:03:05,470
story need not have only one of these.

62
00:03:05,470 --> 00:03:08,532
A longer story can have a variety of these as chapters.

63
00:03:08,532 --> 00:03:11,250
Think of them as building blocks.

64
00:03:11,250 --> 00:03:13,960
Another important building blocks is human archetypes.

65
00:03:13,960 --> 00:03:16,848
These are classic story forms that we all recognize.

66
00:03:16,848 --> 00:03:18,210
We all recognize a hero.

67
00:03:18,210 --> 00:03:20,628
We all recognize an outlaw.

68
00:03:20,628 --> 00:03:23,265
Look at the characters that populate your story and

69
00:03:23,265 --> 00:03:25,720
ask yourself, what do they remind you of?

70
00:03:25,720 --> 00:03:28,350
What does your story remind you of?

71
00:03:28,350 --> 00:03:30,651
This does two very important things.

72
00:03:30,651 --> 00:03:33,714
When you begin to become familiar with the nature of your

73
00:03:33,714 --> 00:03:36,452
story and the nature of your characters it informs

74
00:03:36,452 --> 00:03:39,142
the pattern that you're going to build toward.

75
00:03:39,142 --> 00:03:42,703
And secondarily, as you unfold that story well constructed,

76
00:03:42,703 --> 00:03:46,198
your listeners will recognize it, ether overtly because you

77
00:03:46,198 --> 00:03:50,310
tell them or just through the quality of your construction.

78
00:03:50,310 --> 00:03:53,990
Using classic story forms and understanding human archetypes

79
00:03:53,990 --> 00:03:57,531
is the great first step in the process of crafting a story that

80
00:03:57,531 --> 00:03:59,765
will really land with your audience.

