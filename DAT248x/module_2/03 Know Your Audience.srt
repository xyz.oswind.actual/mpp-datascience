0
00:00:00,750 --> 00:00:03,030
Now that we've defined the impact that we want our stories

1
00:00:03,030 --> 00:00:06,720
to have, it's time to move on to the next step of analysis

2
00:00:06,720 --> 00:00:09,760
which is understanding the audience.

3
00:00:09,760 --> 00:00:12,230
No good marketer would ever go to market without knowing

4
00:00:12,230 --> 00:00:15,170
an audience, and it's the same with a storyteller.

5
00:00:15,170 --> 00:00:18,398
It's critical that you are aware of, and very,

6
00:00:18,398 --> 00:00:22,589
very familiar with the people that you're telling a story to.

7
00:00:22,589 --> 00:00:25,741
This may sound obvious, but it's actually where business

8
00:00:25,741 --> 00:00:28,586
storytellers often make a really critical mistake.

9
00:00:28,586 --> 00:00:31,670
They think they're telling stories to people just like

10
00:00:31,670 --> 00:00:33,900
them, and it's simply not the case.

11
00:00:33,900 --> 00:00:37,380
If you don't recast the terms that you use and

12
00:00:37,380 --> 00:00:39,100
what you emphasize in your story,

13
00:00:39,100 --> 00:00:42,420
your story's simply not going to land.

14
00:00:42,420 --> 00:00:46,118
You have to be very clear on who it is that you're telling

15
00:00:46,118 --> 00:00:47,151
your story to.

16
00:00:47,151 --> 00:00:50,060
And there's a lot of different kinds of people that

17
00:00:50,060 --> 00:00:52,166
are involved here, think about it.

18
00:00:52,166 --> 00:00:55,547
Analytic stories are likely to be told to any of a wide range

19
00:00:55,547 --> 00:00:57,479
of people, including a novice,

20
00:00:57,479 --> 00:01:01,067
somebody who doesn't know much of anything about depth, but

21
00:01:01,067 --> 00:01:04,594
just wants to know things on simple terms, the bottom line.

22
00:01:04,594 --> 00:01:08,360
Generalists, who tend to be a mile wide and an inch deep,

23
00:01:08,360 --> 00:01:10,478
who are interested in trends and

24
00:01:10,478 --> 00:01:13,860
knowing things again at a somewhat topical level.

25
00:01:15,060 --> 00:01:18,280
You get to managers, they start to want a lot more details,

26
00:01:18,280 --> 00:01:20,930
typically in a very granular way,

27
00:01:20,930 --> 00:01:23,810
focused on some aspect of the business.

28
00:01:23,810 --> 00:01:27,436
Experts, technical experts especially are gonna be very

29
00:01:27,436 --> 00:01:31,136
interested in drilling down on the technical specifics and

30
00:01:31,136 --> 00:01:33,066
going very deep on the arcane.

31
00:01:33,066 --> 00:01:36,208
And, of course, executives are the people that are running

32
00:01:36,208 --> 00:01:38,486
things, that have to make the hard calls and

33
00:01:38,486 --> 00:01:41,627
need information on specific terms that are gonna let them be

34
00:01:41,627 --> 00:01:44,170
able to make decisions quickly and accurately.

35
00:01:44,170 --> 00:01:47,364
This is the landscape of people that you need to tell

36
00:01:47,364 --> 00:01:48,116
a story to.

37
00:01:48,116 --> 00:01:49,743
So there's a process involved,

38
00:01:49,743 --> 00:01:51,834
and saying how do you get into their heads so

39
00:01:51,834 --> 00:01:55,720
that you tell stories that are gonna actually land with them.

40
00:01:55,720 --> 00:01:59,119
That process involves first looking at the demographics and

41
00:01:59,119 --> 00:02:02,382
these are the kinds of things that you might find on a resume

42
00:02:02,382 --> 00:02:05,397
or some sheet of just specific, basic information.

43
00:02:05,397 --> 00:02:07,994
And you wanna think about the kinda people that you're telling

44
00:02:07,994 --> 00:02:09,032
your analytic story to.

45
00:02:09,032 --> 00:02:10,560
What's their job title?

46
00:02:10,560 --> 00:02:11,119
What's their age?

47
00:02:11,119 --> 00:02:12,594
What's their gender?

48
00:02:12,594 --> 00:02:13,130
Are they men?

49
00:02:13,130 --> 00:02:14,113
Are they women?

50
00:02:14,113 --> 00:02:16,392
Are they Native English speakers?

51
00:02:16,392 --> 00:02:19,353
Are they people that come from different countries?

52
00:02:19,353 --> 00:02:20,930
What's their experience?

53
00:02:20,930 --> 00:02:21,830
Are they young?

54
00:02:21,830 --> 00:02:23,170
Are they old?

55
00:02:23,170 --> 00:02:26,008
You can have a very wide range and when you spend time

56
00:02:26,008 --> 00:02:29,374
imagining these and actually putting pictures in your minds

57
00:02:29,374 --> 00:02:31,890
of these kinds of people, that will inform.

58
00:02:31,890 --> 00:02:33,750
That will begin to inform the way that you'd

59
00:02:33,750 --> 00:02:35,780
think of the stories that you're gonna build and

60
00:02:35,780 --> 00:02:37,074
the stories that you're gonna land.

61
00:02:37,074 --> 00:02:38,728
But it's much deeper than that.

62
00:02:38,728 --> 00:02:41,750
You have to go into the places of psychographics as well, and

63
00:02:41,750 --> 00:02:44,205
psychographics is about empathy.

64
00:02:44,205 --> 00:02:46,736
Empathy is about getting into the skin,

65
00:02:46,736 --> 00:02:49,864
getting kind of deep into what makes people tick,

66
00:02:49,864 --> 00:02:53,675
having clarity on these people and what their world view is.

67
00:02:53,675 --> 00:02:56,512
Once you spend time and you realise that you're selling

68
00:02:56,512 --> 00:02:59,768
a story to an executive versus say a millennial who's a novice,

69
00:02:59,768 --> 00:03:02,075
their world view is gonna be very different.

70
00:03:02,075 --> 00:03:05,755
It's gonna be informed by a much wider range of experiences.

71
00:03:05,755 --> 00:03:09,080
You wanna spend time thinking about their values.

72
00:03:09,080 --> 00:03:12,327
What is it that they value, both from a business point of view

73
00:03:12,327 --> 00:03:14,181
and from a personal point of view?

74
00:03:14,181 --> 00:03:15,905
What do they think is right and wrong?

75
00:03:15,905 --> 00:03:17,841
What do they think is good and bad?

76
00:03:17,841 --> 00:03:21,196
You wanna spend time and go in there and ask yourself questions

77
00:03:21,196 --> 00:03:23,400
about what is perfect success for them?

78
00:03:23,400 --> 00:03:25,600
What does greatness look like for them?

79
00:03:25,600 --> 00:03:27,240
How do you help them get there?

80
00:03:27,240 --> 00:03:30,480
How does what you are dealing with in your analytics story,

81
00:03:30,480 --> 00:03:33,080
going to inform either positive or

82
00:03:33,080 --> 00:03:37,345
negatively, their journey onto a road of success.

83
00:03:37,345 --> 00:03:38,495
And then conversely and

84
00:03:38,495 --> 00:03:42,485
quite powerfully, what is disaster for them?

85
00:03:42,485 --> 00:03:44,734
What does failure look like to them?

86
00:03:44,734 --> 00:03:46,253
How do you think about that?

87
00:03:46,253 --> 00:03:49,796
What are the terms that come with failure and

88
00:03:49,796 --> 00:03:52,198
complete disaster for them?

89
00:03:52,198 --> 00:03:54,295
There's some land mines involved here too.

90
00:03:54,295 --> 00:03:57,896
You wanna be careful of a few things especially about what

91
00:03:57,896 --> 00:04:01,656
their view is of you, and the data that you're working on.

92
00:04:01,656 --> 00:04:04,534
What are the prejudices they have about you, about who you

93
00:04:04,534 --> 00:04:08,180
are, who are you to be bringing in data and information?

94
00:04:08,180 --> 00:04:11,630
And then stories about analytics that

95
00:04:11,630 --> 00:04:14,410
they may not think that you're qualified for.

96
00:04:14,410 --> 00:04:18,590
They may not have the right idea of your qualifications.

97
00:04:18,590 --> 00:04:22,470
And then concurrently with that, think of the data themself.

98
00:04:22,470 --> 00:04:25,770
When you talk about data, data is powerful stuff.

99
00:04:25,770 --> 00:04:28,790
Data is ultimately very sensitive, and

100
00:04:28,790 --> 00:04:31,650
every bit of data, every bit of important data that you work

101
00:04:31,650 --> 00:04:33,660
with, odds are your stakeholders,

102
00:04:33,660 --> 00:04:35,660
the people that you're telling a story to,

103
00:04:35,660 --> 00:04:39,820
they're all gonna have strong feelings about what the data is.

104
00:04:39,820 --> 00:04:40,820
And in some level,

105
00:04:40,820 --> 00:04:45,858
that can be either a threat or it can be an opportunity.

106
00:04:45,858 --> 00:04:50,079
So you wanna be aware of those psychographic elements that

107
00:04:50,079 --> 00:04:53,274
surround the story that you're building.

108
00:04:53,274 --> 00:04:55,760
And this will help you in a variety of ways.

109
00:04:55,760 --> 00:04:59,770
It will help you in terms of thinking about the right story

110
00:04:59,770 --> 00:05:03,861
to tell, out of this vast set of options that exist when you

111
00:05:03,861 --> 00:05:07,202
tell a story about some big analytics process,

112
00:05:07,202 --> 00:05:09,145
as we saw with the data map.

113
00:05:09,145 --> 00:05:13,278
And then it will also inform the way that you actually craft and

114
00:05:13,278 --> 00:05:14,800
then land that story.

115
00:05:14,800 --> 00:05:17,374
The more you know about your audience,

116
00:05:17,374 --> 00:05:20,728
the higher your chances of success in having it land

117
00:05:20,728 --> 00:05:22,760
with the impact that you want.

