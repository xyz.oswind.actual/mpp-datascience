0
00:00:00,380 --> 00:00:03,371
In analytic storytelling, it's easy to get stuck.

1
00:00:03,371 --> 00:00:05,462
What I'd like to give you are a couple cheat sheets to help you

2
00:00:05,462 --> 00:00:06,410
through those obstacles.

3
00:00:07,990 --> 00:00:09,390
So I will call this the chart selector.

4
00:00:09,390 --> 00:00:11,620
On the left, you have a bunch of chart types.

5
00:00:11,620 --> 00:00:13,872
On the right, descriptions of how to use those charts.

6
00:00:13,872 --> 00:00:16,882
And you can take what you built in the previous modules and

7
00:00:16,882 --> 00:00:19,480
stick it up against that, and see what you get.

8
00:00:19,480 --> 00:00:20,865
See if it's the right thing.

9
00:00:20,865 --> 00:00:24,062
And not that you have to memorize all this, it's just

10
00:00:24,062 --> 00:00:27,702
a nice check on your work to help you perfect your artifacts.

11
00:00:27,702 --> 00:00:30,605
And a subset of these are really essential to great story

12
00:00:30,605 --> 00:00:31,630
telling.

13
00:00:31,630 --> 00:00:32,730
Master your line chart,

14
00:00:32,730 --> 00:00:36,890
master your bar chart, pareto, heat map, scatter plot,

15
00:00:36,890 --> 00:00:40,200
these are really the core of great analytic storytelling.

16
00:00:40,200 --> 00:00:43,712
The rest can be confusing and perhaps distract your audience,

17
00:00:43,712 --> 00:00:46,363
just like those preattentive attributes can if

18
00:00:46,363 --> 00:00:47,431
used improperly.

19
00:00:47,431 --> 00:00:50,337
And lastly, if you're really stuck, maybe just start in

20
00:00:50,337 --> 00:00:53,480
the center of this diagram and think, what am I actually trying

21
00:00:53,480 --> 00:00:56,770
to accomplish and workflow your way to the right visualization.

