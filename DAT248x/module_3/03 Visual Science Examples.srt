0
00:00:00,265 --> 00:00:02,905
To prove to you that preattentive processing

1
00:00:02,905 --> 00:00:06,307
happens without you fully being aware of it, Colin Ware,

2
00:00:06,307 --> 00:00:07,766
this visual scientist,

3
00:00:07,766 --> 00:00:11,880
put together this visualization to show us just that.

4
00:00:11,880 --> 00:00:16,342
And if you notice there are two circles in this visual,

5
00:00:16,342 --> 00:00:19,452
one on the left, and one on the right.

6
00:00:19,452 --> 00:00:24,884
And if you can believe it, these are same size, shape, and really

7
00:00:24,884 --> 00:00:29,649
color and shading, everything about them is identical.

8
00:00:29,649 --> 00:00:32,883
But it's impossible for your brain to actually see that,

9
00:00:32,883 --> 00:00:35,985
if you just take a second, the left one looks bigger than

10
00:00:35,985 --> 00:00:39,089
the right one and that's because of what surrounds it.

11
00:00:39,089 --> 00:00:42,060
So your narrative brain can't even kick in here.

12
00:00:42,060 --> 00:00:45,287
The preattentive processing's happening in real time and

13
00:00:45,287 --> 00:00:46,714
you can't reconcile it.

14
00:00:46,714 --> 00:00:49,842
Just a perfect example of that happening, and

15
00:00:49,842 --> 00:00:53,850
if you know these brain tricks that I'm gonna show you on how

16
00:00:53,850 --> 00:00:56,818
to deal with these preattentive processing

17
00:00:56,818 --> 00:01:00,185
principles it will help you to be more effective,

18
00:01:00,185 --> 00:01:04,460
and let you avoid the pitfalls of using them inappropriately.

19
00:01:04,460 --> 00:01:07,697
The first one we'll see is a set of numbers here and

20
00:01:07,697 --> 00:01:10,050
it's really black and white.

21
00:01:10,050 --> 00:01:12,739
Your preattentive processing is kind of going nowhere,

22
00:01:12,739 --> 00:01:15,042
now it's switching to the top down processing.

23
00:01:15,042 --> 00:01:18,387
But if you add color It'll pop right out if I ask you to look

24
00:01:18,387 --> 00:01:21,340
for number 5, how many times number 5 shows up.

25
00:01:22,490 --> 00:01:26,530
And this can be used with great effect, or it can be used all

26
00:01:26,530 --> 00:01:29,915
over the place if you're not thinking about it.

27
00:01:29,915 --> 00:01:33,657
The next one, we have a number of circles, right?

28
00:01:33,657 --> 00:01:36,164
All of them are pretty much the same except the one outlier,

29
00:01:36,164 --> 00:01:38,020
which is a different shape.

30
00:01:38,020 --> 00:01:39,270
And that jumps out at you,

31
00:01:39,270 --> 00:01:41,900
this square versus all the other circles.

32
00:01:41,900 --> 00:01:45,390
Then that's using a principle of curvature to distinguish

33
00:01:45,390 --> 00:01:46,650
one object from many.

34
00:01:46,650 --> 00:01:51,090
And if you don't think about the types of curvature you're using

35
00:01:51,090 --> 00:01:53,914
in the data points that you're showing,

36
00:01:53,914 --> 00:01:57,868
you might fall into a similar problem of making one item pop

37
00:01:57,868 --> 00:02:00,632
out out of many that you didn't mean to.

38
00:02:02,671 --> 00:02:06,713
Next, there's these clusters of dots and when you put things

39
00:02:06,713 --> 00:02:09,835
together you tend to think that they are alike.

40
00:02:09,835 --> 00:02:12,517
And if you don't use the principle of proximity

41
00:02:12,517 --> 00:02:15,936
appropriately you might end up with clusters that are seem to

42
00:02:15,936 --> 00:02:17,812
the viewer to be of the same kind,

43
00:02:17,812 --> 00:02:20,848
when in fact they have different data points in them.

44
00:02:20,848 --> 00:02:24,881
So another thing to be keenly aware of.

45
00:02:24,881 --> 00:02:28,160
Next we have these rows of circles and squares.

46
00:02:28,160 --> 00:02:31,313
Our brain is automatically gonna fall into understanding

47
00:02:31,313 --> 00:02:33,565
the narrative about what comes before and

48
00:02:33,565 --> 00:02:36,021
after, we expect certain shapes to pop out.

49
00:02:36,021 --> 00:02:40,482
And this is using similarity as a preattentive processing in

50
00:02:40,482 --> 00:02:42,000
principle.

51
00:02:42,000 --> 00:02:44,590
And again, if you want to avoid

52
00:02:44,590 --> 00:02:48,360
having people think that circles are the next in line it's best

53
00:02:48,360 --> 00:02:52,580
to try to use this judiciously in your data work.

54
00:02:54,190 --> 00:02:58,155
For this next one we've stuck some dots into a box and it'll

55
00:02:58,155 --> 00:03:02,040
make those dots pop out and the ones behind that seem more or

56
00:03:02,040 --> 00:03:05,873
less important, and the ones in the box more important.

57
00:03:05,873 --> 00:03:09,569
And a simple process of enclosure will allow you to

58
00:03:09,569 --> 00:03:14,740
highlight what you like to highlight in a visualization.

59
00:03:14,740 --> 00:03:17,698
Or you could potentially lead the viewer to ignore the things

60
00:03:17,698 --> 00:03:20,485
that are outliers, when in fact they're interesting.

61
00:03:20,485 --> 00:03:23,170
So you gotta be careful with that.

62
00:03:23,170 --> 00:03:24,892
And if you'd like to learn more,

63
00:03:24,892 --> 00:03:27,414
if you look in the further reading section this is

64
00:03:27,414 --> 00:03:29,890
your cheat sheet of preattentive attributes.

65
00:03:29,890 --> 00:03:32,310
And mastering these will just make you that much more

66
00:03:32,310 --> 00:03:34,785
effective as an analytics professional and allow you

67
00:03:34,785 --> 00:03:37,480
to build the perfect artifact for the perfect situation.

