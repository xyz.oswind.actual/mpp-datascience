0
00:00:00,000 --> 00:00:02,190
>> Storytelling with Data,

1
00:00:02,190 --> 00:00:05,029
while we are using all these different components,

2
00:00:05,029 --> 00:00:07,495
one important aspect of it is,

3
00:00:07,495 --> 00:00:12,325
you need to have telling the data in the correct way.

4
00:00:12,325 --> 00:00:14,430
Like, there should be a truthful art.

5
00:00:14,430 --> 00:00:18,025
And what you would see is that,

6
00:00:18,025 --> 00:00:19,735
sometimes, you would find

7
00:00:19,735 --> 00:00:22,530
these visualization all over the internet.

8
00:00:22,530 --> 00:00:27,740
Many would see that some of the graphs can be misleading.

9
00:00:27,740 --> 00:00:29,995
A few examples of that could be: You've

10
00:00:29,995 --> 00:00:32,425
got this particular chart

11
00:00:32,425 --> 00:00:37,915
which starts at 5.175 and goes 'til 5.21.

12
00:00:37,915 --> 00:00:41,305
But when it is present this graph in this format,

13
00:00:41,305 --> 00:00:45,430
you just see that this one is way higher than this one.

14
00:00:45,430 --> 00:00:49,360
So, it doesn't provide the correct information.

15
00:00:49,360 --> 00:00:51,540
Same thing for this chart.

16
00:00:51,540 --> 00:00:55,300
It starts as 44.5 and 49.

17
00:00:55,300 --> 00:00:59,325
But if the audience is looking into this picture,

18
00:00:59,325 --> 00:01:03,060
they would see that this particular bar

19
00:01:03,060 --> 00:01:07,167
is three times more bigger than the other one.

20
00:01:07,167 --> 00:01:08,770
Same thing goes for the other stuff.

21
00:01:08,770 --> 00:01:12,935
Sometimes, other ways of misleading graphs is like,

22
00:01:12,935 --> 00:01:16,776
here you see one of the pool,

23
00:01:16,776 --> 00:01:19,650
and here you see, "Yes" is 58 percent,

24
00:01:19,650 --> 00:01:21,335
"No" is 52 percent.

25
00:01:21,335 --> 00:01:22,380
When you do the sum,

26
00:01:22,380 --> 00:01:24,484
it's 110 percent. How's that possible?

27
00:01:24,484 --> 00:01:26,895
And similar thing, so here,

28
00:01:26,895 --> 00:01:30,670
you've got the miles per hour,

29
00:01:30,670 --> 00:01:36,480
75.3 in 2013, 2012 it was 77.3.

30
00:01:36,480 --> 00:01:39,040
So, looking at it, it shows that the speed

31
00:01:39,040 --> 00:01:42,400
has reduced to more than half what this,

32
00:01:42,400 --> 00:01:46,175
from 77.3 to 75.3.

33
00:01:46,175 --> 00:01:48,910
So, it's always very easy to kind of

34
00:01:48,910 --> 00:01:52,690
lie with the data or with the statistics as well.

35
00:01:52,690 --> 00:01:55,480
So, even when you're creating all these visualization,

36
00:01:55,480 --> 00:01:57,490
telling stories, try to

37
00:01:57,490 --> 00:01:59,835
think of from that perspective as well.

38
00:01:59,835 --> 00:02:02,410
So, a few of the things that you can think of is

39
00:02:02,410 --> 00:02:06,265
a scale are not starting at zero, so check with that.

40
00:02:06,265 --> 00:02:08,280
Scale values or labels,

41
00:02:08,280 --> 00:02:10,420
sometimes they could be missing as well.

42
00:02:10,420 --> 00:02:12,945
So, that's why it's very important.

43
00:02:12,945 --> 00:02:15,240
Like, what are the scale?

44
00:02:15,240 --> 00:02:16,600
Sometimes, you would hide

45
00:02:16,600 --> 00:02:19,625
the scales when you just want to show the trend.

46
00:02:19,625 --> 00:02:21,770
But before you show the trend,

47
00:02:21,770 --> 00:02:23,890
you should be checking it out.

48
00:02:23,890 --> 00:02:28,390
Like, will it be misleading for the audience?

49
00:02:28,390 --> 00:02:31,360
And then, incorrect scale placed on the graph.

50
00:02:31,360 --> 00:02:34,105
Sometimes, you would see some of that thing as well.

51
00:02:34,105 --> 00:02:35,875
And then, size of images.

52
00:02:35,875 --> 00:02:37,570
Sometimes, what people do is,

53
00:02:37,570 --> 00:02:41,575
instead of using the bar showing the other things,

54
00:02:41,575 --> 00:02:43,990
they'll use the images to

55
00:02:43,990 --> 00:02:46,540
show kind of a infographic which is kind of cool,

56
00:02:46,540 --> 00:02:48,550
but they might not pay

57
00:02:48,550 --> 00:02:51,465
attention to the size of the image.

58
00:02:51,465 --> 00:02:52,885
For example, in this case,

59
00:02:52,885 --> 00:02:56,080
maybe the intention was not to mislead,

60
00:02:56,080 --> 00:02:59,050
but the image which the author got was

61
00:02:59,050 --> 00:03:00,425
not of a proper size,

62
00:03:00,425 --> 00:03:03,155
and that's why you see it in a different scale.

63
00:03:03,155 --> 00:03:05,900
So, I think that's very important,

64
00:03:05,900 --> 00:03:07,460
that you pay attention to

65
00:03:07,460 --> 00:03:10,260
those things when you tell stories.

