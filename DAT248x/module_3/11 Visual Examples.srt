0
00:00:01,170 --> 00:00:04,383
Let's take a look at a few more real world examples that we can

1
00:00:04,383 --> 00:00:05,328
be inspired by and

2
00:00:05,328 --> 00:00:08,796
learn from in order to continue perfecting our visualizations.

3
00:00:11,447 --> 00:00:14,360
In this slide you just have one visualization.

4
00:00:14,360 --> 00:00:19,360
This is a line chart with an acronym MRR on it.

5
00:00:20,670 --> 00:00:22,400
Simple enough, right?

6
00:00:22,400 --> 00:00:25,820
Well, CEO Manny Medina of Outreach

7
00:00:25,820 --> 00:00:29,960
used this in 2015 to do a bunch of fundraising.

8
00:00:29,960 --> 00:00:32,430
He had one slide that he'd bring to investors, just one, and

9
00:00:32,430 --> 00:00:33,530
it was this one,

10
00:00:33,530 --> 00:00:36,870
and he would say, this is our monthly recurring revenue.

11
00:00:37,890 --> 00:00:39,470
Any questions?

12
00:00:39,470 --> 00:00:40,840
And he just basically let the mic drop.

13
00:00:41,870 --> 00:00:44,550
And the fascinating part was it was super effective.

14
00:00:44,550 --> 00:00:46,620
Outreach raised a ton of money and

15
00:00:46,620 --> 00:00:49,490
the reason was because when investors see monthly recurring

16
00:00:49,490 --> 00:00:53,040
revenue basically doubling every month or every few months.

17
00:00:53,040 --> 00:00:56,800
That's a great sign of healthy business, a great business model

18
00:00:56,800 --> 00:00:59,780
and it effectively gets the point across in something that's

19
00:00:59,780 --> 00:01:05,200
so simple as one little line, or area chart increasing over time.

20
00:01:05,200 --> 00:01:07,840
And what investors really see in their mind is hockey

21
00:01:07,840 --> 00:01:08,820
stick growth.

22
00:01:08,820 --> 00:01:10,830
This is a company that's gonna take off.

23
00:01:10,830 --> 00:01:12,436
All that comes from one slide.

24
00:01:12,436 --> 00:01:14,080
Really knowing your audience,

25
00:01:14,080 --> 00:01:18,920
and being able to be that concise, is a great bonus and

26
00:01:18,920 --> 00:01:20,760
something that master storytellers love to do.

