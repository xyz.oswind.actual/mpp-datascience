0
00:00:01,110 --> 00:00:03,820
So I'd like to demonstrate great data storytelling

1
00:00:03,820 --> 00:00:05,540
through some before and afters.

2
00:00:05,540 --> 00:00:09,020
This is where we'll take a story that is pretty good and run it

3
00:00:09,020 --> 00:00:12,560
through some best practices and principles to make it great.

4
00:00:12,560 --> 00:00:14,570
And we're calling this Data Stories Makeover.

5
00:00:14,570 --> 00:00:15,660
So let's begin.

