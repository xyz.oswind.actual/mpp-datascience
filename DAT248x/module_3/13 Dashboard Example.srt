0
00:00:00,470 --> 00:00:02,430
Lastly, I wanted to show you a dashboard that

1
00:00:02,430 --> 00:00:03,410
does a lot of things well.

2
00:00:04,790 --> 00:00:08,340
There's this computer scientist by the name of Ben Shneiderman,

3
00:00:08,340 --> 00:00:13,140
who has this saying of a well-formed dashboard,

4
00:00:13,140 --> 00:00:16,420
that it should provide an overview first, then allow

5
00:00:16,420 --> 00:00:19,236
you to zoom and filter, and then provide you details on demand.

6
00:00:19,236 --> 00:00:23,010
So kinda a three-step process of an excellently crafted

7
00:00:23,010 --> 00:00:27,270
visualization or dashboard would that it provide an overview,

8
00:00:27,270 --> 00:00:29,200
allow you to zoom and filter, and

9
00:00:29,200 --> 00:00:32,330
tease out insights, and then get those specific details in order

10
00:00:32,330 --> 00:00:35,030
to find those nuggets of gold, those nuggets of insights.

11
00:00:35,030 --> 00:00:36,620
And this dashboard does that well.

12
00:00:36,620 --> 00:00:39,140
So if you see in the top there, it says selective measure.

13
00:00:39,140 --> 00:00:41,687
You have profits, quantity, and revenue.

14
00:00:41,687 --> 00:00:44,405
And then you can select a perspective, love that take.

15
00:00:44,405 --> 00:00:47,580
Perspective on product category, order type, and year.

16
00:00:47,580 --> 00:00:49,900
All these things you can filter down, and you zoom and filter.

17
00:00:51,260 --> 00:00:53,120
And lastly, the details on demand.

18
00:00:53,120 --> 00:00:54,964
There's lots of stuff here that you can take away from.

19
00:00:54,964 --> 00:00:58,194
Top five products would pop up in the bottom right.

20
00:00:58,194 --> 00:01:01,556
In the top, you see these high-level metrics,

21
00:01:01,556 --> 00:01:05,656
total profit and profit margin, average order quantity,

22
00:01:05,656 --> 00:01:08,038
total revenue, and their trend.

23
00:01:08,038 --> 00:01:10,765
And then you get some specifics in terms of geographically,

24
00:01:10,765 --> 00:01:12,580
how is the revenue distributed?

25
00:01:12,580 --> 00:01:14,410
So just a lot of things here.

26
00:01:14,410 --> 00:01:17,290
Maybe that small enhancement on this would be to provide those

27
00:01:17,290 --> 00:01:18,420
really specific details,

28
00:01:18,420 --> 00:01:20,830
maybe even like row level details at the bottom.

29
00:01:20,830 --> 00:01:24,790
Take away some space from the large bar chart, perhaps.

30
00:01:24,790 --> 00:01:27,380
But in any case, not a lot to say in terms of criticisms,

31
00:01:27,380 --> 00:01:31,520
just to say that this is a well-formed dashboard,

32
00:01:31,520 --> 00:01:33,751
nice use of Shneiderman's mantra there.

33
00:01:34,880 --> 00:01:37,665
Lastly, color is used decently well.

34
00:01:37,665 --> 00:01:40,513
So as a preattentive attribute, you have year over year change

35
00:01:40,513 --> 00:01:42,790
and total revenue on the right here.

36
00:01:42,790 --> 00:01:45,290
It looks like in 2017 there's a decrease.

37
00:01:45,290 --> 00:01:48,290
And the rest of it is pretty gray.

38
00:01:48,290 --> 00:01:50,830
So I think in terms of things that pop out,

39
00:01:50,830 --> 00:01:53,673
obviously my eye goes all the way to the right.

40
00:01:53,673 --> 00:01:56,848
And in heat map studies, says that the most important stuff in

41
00:01:56,848 --> 00:01:59,800
your visualization is kind of in that top left area.

42
00:01:59,800 --> 00:02:02,620
So the total profit amount, selecting a measure,

43
00:02:02,620 --> 00:02:06,280
the Executive Insights title itself, super important.

44
00:02:06,280 --> 00:02:09,788
So definitely when you're crafting your dashboard or

45
00:02:09,788 --> 00:02:14,058
your visualization, keep these in mind that you'd wanna provide

46
00:02:14,058 --> 00:02:18,188
some form of Shneiderman-like interactivity in a dashboard.

47
00:02:18,188 --> 00:02:20,590
But you also wanna keep in mind those fundamentals.

48
00:02:20,590 --> 00:02:22,997
Never lose track of those fundamentals, and

49
00:02:22,997 --> 00:02:26,431
you're always pretty well set to tee up a great analytic story.

