0
00:00:02,470 --> 00:00:04,130
Acme is at it again.

1
00:00:04,130 --> 00:00:06,920
This time what happened is there was a crash.

2
00:00:06,920 --> 00:00:11,200
And the crash caused a bunch of social media activity and

3
00:00:11,200 --> 00:00:14,150
ended up with the company apologizing and certain

4
00:00:14,150 --> 00:00:17,830
things happening with sentiment and volume of social media.

5
00:00:17,830 --> 00:00:20,150
So that's the story in a nutshell, and

6
00:00:20,150 --> 00:00:22,380
this visualization could be quite complex.

7
00:00:22,380 --> 00:00:25,304
So this is a dual access chart, some text on the top,

8
00:00:25,304 --> 00:00:27,640
some bubbles explaining what happened.

9
00:00:27,640 --> 00:00:29,027
There's no real reveal.

10
00:00:29,027 --> 00:00:32,797
And additionally, there's things like the dates on the bottom,

11
00:00:32,797 --> 00:00:33,402
have AM and

12
00:00:33,402 --> 00:00:36,785
PM repeating to the point where your eyes kinda go crossed.

13
00:00:36,785 --> 00:00:39,889
And it's usually a number of predetermined attributes in

14
00:00:39,889 --> 00:00:42,168
a very strange way like the box enclosure,

15
00:00:42,168 --> 00:00:43,573
not sure what that means.

16
00:00:43,573 --> 00:00:46,728
Perhaps we could talk to it, but as well as the legend in

17
00:00:46,728 --> 00:00:50,730
the bottom, sentiment ratio and brand volume, kinda tucked away.

18
00:00:50,730 --> 00:00:53,180
I don't know exactly what those bars and

19
00:00:53,180 --> 00:00:54,916
lines are from the get-go.

20
00:00:54,916 --> 00:00:57,627
If I were to take a crack at this, that's probably

21
00:00:57,627 --> 00:01:00,340
where I'd start is to take the bar of sentiment and

22
00:01:00,340 --> 00:01:03,181
turn it into a line additionally with brand volume so

23
00:01:03,181 --> 00:01:06,218
that I can see comparisons between the two values as well

24
00:01:06,218 --> 00:01:09,478
as move the legend above the axis that they're related to.

25
00:01:09,478 --> 00:01:12,950
So sentiment ratio can be identified as the orange line.

26
00:01:12,950 --> 00:01:14,230
Brand volume is the blue.

27
00:01:14,230 --> 00:01:16,320
And make those dates much cleaner.

28
00:01:16,320 --> 00:01:18,210
And just remove a lot of that noise and

29
00:01:18,210 --> 00:01:21,140
perhaps add some reveals so that I can make the story

30
00:01:21,140 --> 00:01:23,950
come alive with this chart that's essentially static.

31
00:01:23,950 --> 00:01:27,651
Say there was a crush that happened on March 23rd, and

32
00:01:27,651 --> 00:01:31,288
there was a tweet storm that happened on March 24th.

33
00:01:31,288 --> 00:01:35,369
As you can see, the sentiment dropped into below average

34
00:01:35,369 --> 00:01:39,300
territory as identified by that box and unhappy face.

35
00:01:39,300 --> 00:01:42,330
Customers were quite unhappy about this being the case.

36
00:01:42,330 --> 00:01:44,820
So we then apologized and said we're sorry.

37
00:01:44,820 --> 00:01:49,380
And sentiments started to go back up and volume started to

38
00:01:49,380 --> 00:01:53,465
taper down, and things were going back to normal.

39
00:01:53,465 --> 00:02:00,510
So in summary, try to use two lines in dual-axis charts.

40
00:02:00,510 --> 00:02:03,680
I think using bar versus line can oftentimes confuse.

41
00:02:03,680 --> 00:02:06,260
Cuz the main goal you're trying to get somebody to do is compare

42
00:02:06,260 --> 00:02:08,090
two values over time.

43
00:02:08,090 --> 00:02:13,050
And that essential perspective on dual access charts is a bit

44
00:02:13,050 --> 00:02:14,010
against the norm.

45
00:02:14,010 --> 00:02:16,680
You'd most of the time see bar with line, so

46
00:02:16,680 --> 00:02:17,590
that might seem foreign.

47
00:02:17,590 --> 00:02:19,840
But definitely play with it and see where it takes you.

48
00:02:19,840 --> 00:02:22,815
As well, embrace that white space again,

49
00:02:22,815 --> 00:02:26,205
and just make every little bit count.

50
00:02:26,205 --> 00:02:29,661
The legend themselves can enhance your visualization or

51
00:02:29,661 --> 00:02:30,817
take away from it.

