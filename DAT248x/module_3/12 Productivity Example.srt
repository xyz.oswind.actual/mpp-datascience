0
00:00:00,690 --> 00:00:04,300
Companies Levo and Microsoft together did a survey

1
00:00:04,300 --> 00:00:07,410
on how important productivity is to happiness.

2
00:00:07,410 --> 00:00:10,320
And what they did is they collected a bunch of data from

3
00:00:10,320 --> 00:00:14,600
a fairly young age to later stage career folks,

4
00:00:14,600 --> 00:00:19,360
and created some visualizations in Power BI off of it.

5
00:00:19,360 --> 00:00:20,970
And what's great about this is,

6
00:00:20,970 --> 00:00:22,780
it's really a survey come to life.

7
00:00:22,780 --> 00:00:24,060
And they've drawn out the nuggets,

8
00:00:24,060 --> 00:00:26,570
they've done the work of creating some insights and

9
00:00:26,570 --> 00:00:29,270
putting them into a format that people can readily understand.

10
00:00:29,270 --> 00:00:30,640
In this case,

11
00:00:30,640 --> 00:00:34,800
this nice kind of grid chart is a custom visualization, sure.

12
00:00:34,800 --> 00:00:38,505
But what jumps out is basically, boom, one statistic,

13
00:00:38,505 --> 00:00:41,200
93% believe productivity is important to happiness.

14
00:00:41,200 --> 00:00:43,325
And you could filter by age range to look at what other

15
00:00:43,325 --> 00:00:44,255
age ranges feel like.

16
00:00:44,255 --> 00:00:46,877
But this is on average across all the people surveyed,

17
00:00:46,877 --> 00:00:49,830
this is how many people believe that's important.

18
00:00:49,830 --> 00:00:52,754
A super sticky statistic that is a punchy

19
00:00:52,754 --> 00:00:56,115
kind of visualization and comes across well.

20
00:00:56,115 --> 00:00:58,945
And there's only one point to this slide pretty much, and

21
00:00:58,945 --> 00:01:00,170
it comes across nicely.

22
00:01:01,430 --> 00:01:04,680
And a level deeper is the next question,

23
00:01:04,680 --> 00:01:07,120
what is the number one challenge of being productive?

24
00:01:07,120 --> 00:01:10,720
And looks like there are a few answers to this.

25
00:01:10,720 --> 00:01:12,990
The biggest one is distractions.

26
00:01:12,990 --> 00:01:17,153
And then comes time management, interruptions, other, and

27
00:01:17,153 --> 00:01:18,710
wrong tools.

28
00:01:18,710 --> 00:01:21,411
Interestingly enough, it could have been done a little bit

29
00:01:21,411 --> 00:01:23,798
better in terms of what is the number one challenge?

30
00:01:23,798 --> 00:01:28,359
[LAUGH] Instead, the visualization gives us five.

31
00:01:28,359 --> 00:01:31,918
Yeah, I don't know if that was thought through in a very clear

32
00:01:31,918 --> 00:01:34,870
way, but it is nifty to have these other examples.

33
00:01:34,870 --> 00:01:37,136
So the misalignment between title and

34
00:01:37,136 --> 00:01:40,691
what the visualizations are showing is a bit distracting.

35
00:01:40,691 --> 00:01:44,180
But there's more here to unpack that they could have done.

36
00:01:44,180 --> 00:01:47,531
And then they dig into this next one, well, okay, well, what

37
00:01:47,531 --> 00:01:50,832
would help you then if you have these issues to productivity?

38
00:01:50,832 --> 00:01:52,176
And it's like creating a to-do list,

39
00:01:52,176 --> 00:01:53,406
everybody thinks that would help.

40
00:01:53,406 --> 00:01:55,908
And calendar tasks, taking breaks.

41
00:01:55,908 --> 00:01:59,922
Because they've used the black background and not white again,

42
00:01:59,922 --> 00:02:02,420
sure, it alternates from the previous slide or

43
00:02:02,420 --> 00:02:05,490
the previous visualizations to this one.

44
00:02:05,490 --> 00:02:07,130
But ultimately I think it does distract,

45
00:02:07,130 --> 00:02:11,410
because it's not keeping the context that we had before,

46
00:02:11,410 --> 00:02:13,405
the same pre-attentive attributes of color.

47
00:02:13,405 --> 00:02:16,133
And nice that it's kinda one takeaway,

48
00:02:16,133 --> 00:02:20,308
it's not saying the number one challenge is saying in general

49
00:02:20,308 --> 00:02:21,760
what would help you.

50
00:02:21,760 --> 00:02:24,600
And the visualization does support that question,

51
00:02:24,600 --> 00:02:27,320
allowing you to come to your own insight about it.

52
00:02:28,630 --> 00:02:31,010
And lastly, this is taking yet another slice and saying,

53
00:02:31,010 --> 00:02:32,980
what time of day are you most productive?

54
00:02:32,980 --> 00:02:34,447
And not surprisingly,

55
00:02:34,447 --> 00:02:37,340
mid-morning is when you should do a lot of your work and

56
00:02:37,340 --> 00:02:40,590
be very focused, whereas lunchtime, [LAUGH] not a lot of

57
00:02:40,590 --> 00:02:43,120
people say they're very productive around lunchtime.

58
00:02:43,120 --> 00:02:46,230
And the same with evening and late night.

59
00:02:46,230 --> 00:02:47,310
And this all makes sense.

60
00:02:47,310 --> 00:02:50,454
A lot of people front load their day in their workload.

61
00:02:50,454 --> 00:02:53,540
And then there's the couple of night owls that hang out there.

62
00:02:53,540 --> 00:02:55,080
But this is the end.

63
00:02:55,080 --> 00:02:59,480
So Levo and Microsoft kind of left it at that.

64
00:02:59,480 --> 00:03:03,055
And the story itself didn't feel like it had any conclusion or

65
00:03:03,055 --> 00:03:04,576
wrap up or narrative arc.

66
00:03:04,576 --> 00:03:07,741
As we've learned in the previous module, we learned all about how

67
00:03:07,741 --> 00:03:10,270
important it is to land a great story.

68
00:03:10,270 --> 00:03:13,040
We are going somewhere, and ultimately, it's kind of

69
00:03:13,040 --> 00:03:17,110
a bummer that we didn't end up in a more clear and concise way.

70
00:03:17,110 --> 00:03:20,450
Maybe you could take this to say, and then given all these

71
00:03:20,450 --> 00:03:24,210
insights and these productivity discoveries that we've had,

72
00:03:24,210 --> 00:03:26,522
maybe here are some recommendations.

73
00:03:26,522 --> 00:03:27,540
Here are some takeaways.

