0
00:00:02,020 --> 00:00:04,073
This is a one slide report,

1
00:00:04,073 --> 00:00:08,548
it's talking about how Twitter is driving Share of Voice.

2
00:00:08,548 --> 00:00:12,645
And how before an event there was a lot of talk on social

3
00:00:12,645 --> 00:00:15,570
media, and a couple of other things.

4
00:00:15,570 --> 00:00:18,911
But what's hard to tell is the point of the story without

5
00:00:18,911 --> 00:00:21,339
reading a giant paragraph, first off.

6
00:00:21,339 --> 00:00:23,491
So you see there's a bunch that says Twitter drove

7
00:00:23,491 --> 00:00:26,363
Acme Corporation's conversations about the event on this day and

8
00:00:26,363 --> 00:00:27,400
this day leading up to.

9
00:00:27,400 --> 00:00:30,585
It'd be very hard for someone to present this and

10
00:00:30,585 --> 00:00:33,309
not end up reading the paragraph itself.

11
00:00:33,309 --> 00:00:37,485
Similarly, these charts aren't in any sort of tangible

12
00:00:37,485 --> 00:00:38,540
sequence.

13
00:00:38,540 --> 00:00:42,680
I'm not sure why the bottom chart is hiding out middle and

14
00:00:42,680 --> 00:00:44,090
center of the visualization,

15
00:00:44,090 --> 00:00:46,850
whereas the other two are side by side.

16
00:00:46,850 --> 00:00:48,677
Is that a third and different visualization?

17
00:00:48,677 --> 00:00:52,314
I'm going to have to spend some time thinking about it, and

18
00:00:52,314 --> 00:00:55,094
I may be explaining it in the meeting itself.

19
00:00:55,094 --> 00:00:57,767
So if we take a crack at unpacking this,

20
00:00:57,767 --> 00:01:01,817
creating some more white space, removing some borders and

21
00:01:01,817 --> 00:01:05,948
cleaning up the axis, we're gonna find that there is a story

22
00:01:05,948 --> 00:01:08,950
here that we can bring to bear.

23
00:01:08,950 --> 00:01:12,170
So the first point really that the previous slide was trying to

24
00:01:12,170 --> 00:01:15,300
make was that over and above these other channels, Twitter

25
00:01:15,300 --> 00:01:19,990
accounted for over 85% of Share of Voice on this specific date.

26
00:01:19,990 --> 00:01:24,369
And changing the visualization from what was a complex doughnut

27
00:01:24,369 --> 00:01:27,705
chart, perhaps it takes a little explanation.

28
00:01:27,705 --> 00:01:30,233
It's easy to see right off the bat, using those preattentive

29
00:01:30,233 --> 00:01:32,900
attributes, that Twitter is just jumping out straight ahead.

30
00:01:32,900 --> 00:01:35,680
The visualization supports the words that you're saying.

31
00:01:36,730 --> 00:01:39,530
And secondly, I move this previous chart under

32
00:01:39,530 --> 00:01:41,870
the middle center chart to the right here so

33
00:01:41,870 --> 00:01:44,785
that you have the two side by side,

34
00:01:44,785 --> 00:01:48,130
looking slightly differently, but really the bottom chart had

35
00:01:48,130 --> 00:01:51,320
been supporting the chart in the previous slide on the right.

36
00:01:51,320 --> 00:01:55,145
So this is percent of brand conversations before the event

37
00:01:55,145 --> 00:01:56,322
and also volume.

38
00:01:56,322 --> 00:01:58,900
Really two pivots on the same kind of thing,

39
00:01:58,900 --> 00:02:02,805
ultimately to support, again, some text that says more than 1%

40
00:02:02,805 --> 00:02:05,816
of all brand conversations were about this event.

41
00:02:05,816 --> 00:02:08,384
1% of total conversions about the brand itself.

42
00:02:08,384 --> 00:02:13,305
And who knows what was before or after this story, but really

43
00:02:13,305 --> 00:02:18,419
this one slide only had a couple points and could be supported by

44
00:02:18,419 --> 00:02:23,472
much more simple visualizations and embrace of white space.

45
00:02:23,472 --> 00:02:27,251
And here's the summary slide before and after with a few more

46
00:02:27,251 --> 00:02:30,968
tidbits and recommendations, please feel free to dig in.

47
00:02:30,968 --> 00:02:35,102
But in summary, remove those borders, embrace white space,

48
00:02:35,102 --> 00:02:36,584
reduce that text, and

49
00:02:36,584 --> 00:02:40,250
you'll create that perfect one pager visualization.

