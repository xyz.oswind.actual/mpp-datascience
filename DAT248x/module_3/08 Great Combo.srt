0
00:00:00,680 --> 00:00:03,762
In this story, Acme Service is putting on an event and

1
00:00:03,762 --> 00:00:06,400
this event has stuff leading up to it.

2
00:00:06,400 --> 00:00:08,387
As we see in the chart is the pre period,

3
00:00:08,387 --> 00:00:10,435
there's the event day, the day of, and

4
00:00:10,435 --> 00:00:13,160
then there is post period, which is after the event.

5
00:00:13,160 --> 00:00:17,570
And as we can see there's volume and sentiment of

6
00:00:17,570 --> 00:00:21,130
customers sharing their thoughts and feelings online.

7
00:00:21,130 --> 00:00:23,380
Before, during and after the event.

8
00:00:23,380 --> 00:00:25,370
And if you look in the bottom here,

9
00:00:25,370 --> 00:00:27,810
we have some numbers supporting the chart.

10
00:00:27,810 --> 00:00:30,250
But really it's kinda hard to tell what those numbers

11
00:00:30,250 --> 00:00:33,040
are telling us above and beyond the chart itself.

12
00:00:33,040 --> 00:00:34,988
Are they exactly aligned to the specific period?

13
00:00:34,988 --> 00:00:36,620
What are these annotations doing?

14
00:00:36,620 --> 00:00:39,920
Should I care about them before I really unpack the chart?

15
00:00:39,920 --> 00:00:41,645
There's an average volume line there.

16
00:00:41,645 --> 00:00:44,030
Is that really timely a lot as well?

17
00:00:44,030 --> 00:00:46,940
Obviously during the event it make sense that more

18
00:00:46,940 --> 00:00:48,900
people are sharing about the event.

19
00:00:48,900 --> 00:00:51,464
When I did a revision on this I took a crack at aligning

20
00:00:51,464 --> 00:00:53,225
the stuff on the bottom to the left.

21
00:00:53,225 --> 00:00:55,936
And removing some of the outlines and

22
00:00:55,936 --> 00:01:00,135
some of the backgrounds to just sure up the pre, during,

23
00:01:00,135 --> 00:01:02,672
and post in this visualization and

24
00:01:02,672 --> 00:01:05,760
make the main chart the hero of the story.

25
00:01:05,760 --> 00:01:07,400
Similarly, I've also moved the annotations and

26
00:01:07,400 --> 00:01:08,450
some of the notes to the right.

27
00:01:08,450 --> 00:01:10,260
So that the eye can start,

28
00:01:10,260 --> 00:01:14,590
the viewer can start from the left and move to the right and

29
00:01:14,590 --> 00:01:18,210
have an experience of this chart that is sequential.

30
00:01:18,210 --> 00:01:22,610
And lastly, I've also changed the average line to be a trend

31
00:01:22,610 --> 00:01:26,370
line, and interestingly you see that there's a decrease in

32
00:01:26,370 --> 00:01:31,650
the volume overall in terms of what the events did for

33
00:01:31,650 --> 00:01:35,180
total volume shared and perhaps even sentiment as well.

34
00:01:35,180 --> 00:01:38,177
So there could be a story there in terms of, actually,

35
00:01:38,177 --> 00:01:40,130
we did all this work to put on event,

36
00:01:40,130 --> 00:01:42,997
but perhaps volume hasn't really been impacted and

37
00:01:42,997 --> 00:01:46,536
customers aren't happier or less happy than they were before.

38
00:01:46,536 --> 00:01:48,348
And that's where I would take this story next.

39
00:01:48,348 --> 00:01:53,830
So to summarize say rely on that main chart to be the hero

40
00:01:53,830 --> 00:01:56,900
of your story instead of having all the supporting data and

41
00:01:56,900 --> 00:01:58,670
jumbly stuff around it.

42
00:01:58,670 --> 00:02:01,830
And again the white space make that jump out and

43
00:02:01,830 --> 00:02:04,465
be really linear and sequential if you're showing a time series

44
00:02:04,465 --> 00:02:06,790
unpacking that chart in real time.

