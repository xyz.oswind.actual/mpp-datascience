0
00:00:01,930 --> 00:00:04,690
Let's put our healthcare hat on, and

1
00:00:04,690 --> 00:00:07,490
imagine a business that's trying to put together a scorecard to

2
00:00:07,490 --> 00:00:11,440
track its healthcare goals and missions and ambitions.

3
00:00:11,440 --> 00:00:14,380
So in this case, this healthcare provider is looking

4
00:00:14,380 --> 00:00:16,500
to save lives through digital transformation.

5
00:00:16,500 --> 00:00:20,110
And more than likely, this metric that they're tracking is

6
00:00:20,110 --> 00:00:22,170
a combination of a bunch of different metrics, and

7
00:00:22,170 --> 00:00:24,640
ends up in these main scores here that are either going up or

8
00:00:24,640 --> 00:00:27,510
down, as you can see by the green or red arrows.

9
00:00:27,510 --> 00:00:30,200
Similarly with ambition to create more personal healthcare

10
00:00:30,200 --> 00:00:32,250
or build intelligent healthcare apps.

11
00:00:32,250 --> 00:00:34,740
These are combos of a bunch of different metrics.

12
00:00:34,740 --> 00:00:36,730
What you can tell from this visualization is,

13
00:00:36,730 --> 00:00:39,190
of course there's volume, there's positive sentiments.

14
00:00:39,190 --> 00:00:42,800
They're doing some sort of comparison between volume and

15
00:00:42,800 --> 00:00:46,380
sentiment, but it's not clear to me really how they're doing it.

16
00:00:46,380 --> 00:00:49,281
And I can't tell the difference between some of these numbers.

17
00:00:49,281 --> 00:00:50,548
My eyes go all across,

18
00:00:50,548 --> 00:00:53,830
it's also not super intelligent in terms of an artifact.

19
00:00:53,830 --> 00:00:57,200
Score cards have a lot of work to go into creating them.

20
00:00:57,200 --> 00:01:01,350
So it's worth it to put a thought behind every little

21
00:01:01,350 --> 00:01:04,760
aspect of the score card itself, so that an executive looking at

22
00:01:04,760 --> 00:01:08,000
these differences that they need to make, and make the changes in

23
00:01:08,000 --> 00:01:10,910
the business in order to drive the metrics forward.

24
00:01:10,910 --> 00:01:12,880
So if I were to reenvision this,

25
00:01:12,880 --> 00:01:15,610
it would look more like a complex artifact.

26
00:01:15,610 --> 00:01:18,150
It should be complex in the sense that it's trying to

27
00:01:18,150 --> 00:01:19,270
do a lot.

28
00:01:19,270 --> 00:01:22,060
But I should give it a sense of overall impact and

29
00:01:22,060 --> 00:01:23,840
score of those metrics.

30
00:01:23,840 --> 00:01:26,470
So in this case, I've created an overall score.

31
00:01:26,470 --> 00:01:27,190
So it says,

32
00:01:27,190 --> 00:01:29,990
how I'm doing on saving lives through digital transformation,

33
00:01:29,990 --> 00:01:33,240
or how am I doing this week compared to last week, or

34
00:01:33,240 --> 00:01:35,270
the last 90 days to this week?

35
00:01:35,270 --> 00:01:39,240
As well as include some of these bar charts inside of

36
00:01:39,240 --> 00:01:40,320
the values themselves.

37
00:01:40,320 --> 00:01:42,888
So I need to understand the difference between

38
00:01:42,888 --> 00:01:43,550
60,000 and 1,000.

39
00:01:43,550 --> 00:01:46,110
Visually it's quite easy to do in this visualization.

40
00:01:46,110 --> 00:01:51,270
Along with trends and heat maps of sentiments, targets.

41
00:01:51,270 --> 00:01:55,190
There's a lot here, but I think a good score card should

42
00:01:55,190 --> 00:01:57,700
have things like scales of goodness and

43
00:01:57,700 --> 00:02:01,900
easily identifiable good versus okay versus bad.

44
00:02:01,900 --> 00:02:04,250
And provides some intelligence, so

45
00:02:04,250 --> 00:02:08,230
that this becomes actionable instead of what we saw before,

46
00:02:08,230 --> 00:02:13,150
which is really just kind of a throw away state of affairs.

47
00:02:13,150 --> 00:02:17,820
So just as a general takeaway, make sure you give your audience

48
00:02:17,820 --> 00:02:20,730
that overview score, that sense of how we're doing in general

49
00:02:20,730 --> 00:02:23,490
against each one of these targets and missions.

50
00:02:23,490 --> 00:02:26,800
As well, put some thought into things like scale of goodness,

51
00:02:26,800 --> 00:02:29,660
and how you're showing things,like trying to enable

52
00:02:29,660 --> 00:02:33,240
additional decision making to happen and

53
00:02:33,240 --> 00:02:34,690
deeper insights to occur.

