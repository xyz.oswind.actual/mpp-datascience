0
00:00:00,840 --> 00:00:04,418
In data visualization, there is an actual science behind how we

1
00:00:04,418 --> 00:00:06,351
perceive artifacts themselves.

2
00:00:06,351 --> 00:00:08,490
There are two systems at play.

3
00:00:08,490 --> 00:00:11,309
First is the preattentive processing system, and this is

4
00:00:11,309 --> 00:00:14,597
bottoms-up, happening almost instantly without us knowing it.

5
00:00:14,597 --> 00:00:15,703
Related, of course,

6
00:00:15,703 --> 00:00:18,567
to the experiencing self as we talked about in module one.

7
00:00:18,567 --> 00:00:21,615
And there's a second serial processing that's happening,

8
00:00:21,615 --> 00:00:24,721
this top-down narrative self on top of the visualization that

9
00:00:24,721 --> 00:00:26,210
you're seeing.

10
00:00:26,210 --> 00:00:27,390
That's the conscious mind

11
00:00:27,390 --> 00:00:29,720
making up a story about what you're seeing.

12
00:00:29,720 --> 00:00:32,180
And it's important to note that preattentive processing in

13
00:00:32,180 --> 00:00:33,800
particular happens without us knowing it,

14
00:00:33,800 --> 00:00:38,330
happens like a first impression, within 200 to 250 milliseconds

15
00:00:38,330 --> 00:00:40,220
as you're looking at a visualization.

16
00:00:40,220 --> 00:00:43,130
And if you don't apply preattentive processing

17
00:00:43,130 --> 00:00:46,170
principles appropriately, you can create a visualization that

18
00:00:46,170 --> 00:00:50,750
misleads viewers and potentially distracts them from

19
00:00:50,750 --> 00:00:52,170
the insight that you were trying to give them.

