0
00:00:01,009 --> 00:00:02,038
In the previous module,

1
00:00:02,038 --> 00:00:04,737
you learned some methods for how to tell an excellent data story.

2
00:00:04,737 --> 00:00:07,219
That's through narrative arc types and

3
00:00:07,219 --> 00:00:08,974
discovering insight gold.

4
00:00:08,974 --> 00:00:11,330
All of these things you can kind of think of as helping you

5
00:00:11,330 --> 00:00:12,999
create your first draft of your story.

6
00:00:12,999 --> 00:00:15,116
Now, we're gonna learn how to perfect it,

7
00:00:15,116 --> 00:00:16,384
through visual science.

8
00:00:16,384 --> 00:00:18,339
I'm gonna give you some visual cheat sheets in case

9
00:00:18,339 --> 00:00:18,981
you're stuck.

10
00:00:18,981 --> 00:00:22,314
Also some data story makeovers, as I'm calling it, before and

11
00:00:22,314 --> 00:00:25,030
after shots of stories that have been perfected, or

12
00:00:25,030 --> 00:00:27,207
has gotten at least close to perfection.

13
00:00:27,207 --> 00:00:30,565
And lastly, some real world examples, so that you can take

14
00:00:30,565 --> 00:00:33,400
these and apply them to tell your best data story.

