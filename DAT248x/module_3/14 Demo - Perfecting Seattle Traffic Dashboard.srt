0
00:00:00,000 --> 00:00:02,415
>> All right. So we looked into

1
00:00:02,415 --> 00:00:05,075
this parking violation data in Seattle,

2
00:00:05,075 --> 00:00:07,575
and we created this visualization,

3
00:00:07,575 --> 00:00:09,300
kind of a story that you

4
00:00:09,300 --> 00:00:11,520
see about how many tickets are issued,

5
00:00:11,520 --> 00:00:15,240
what time, what day, what season, and then which part.

6
00:00:15,240 --> 00:00:20,190
But I think it's useful information,

7
00:00:20,190 --> 00:00:21,390
but maybe we can make

8
00:00:21,390 --> 00:00:23,810
a little touch ups and make it more beautiful,

9
00:00:23,810 --> 00:00:27,090
and let's see what other things we could do.

10
00:00:27,090 --> 00:00:28,500
So first of all,

11
00:00:28,500 --> 00:00:33,210
you've got this parking violation in Seattle.

12
00:00:33,210 --> 00:00:35,250
One thing that I could quickly do

13
00:00:35,250 --> 00:00:37,575
is maybe change the background.

14
00:00:37,575 --> 00:00:41,155
We'll just play around and see what we have.

15
00:00:41,155 --> 00:00:45,350
And you have got this page background from white.

16
00:00:45,350 --> 00:00:49,260
Let's make it a little bit dark.

17
00:00:49,260 --> 00:00:52,920
And here in Esri graphs,

18
00:00:52,920 --> 00:00:55,390
what you could do is you can edit this,

19
00:00:55,390 --> 00:00:58,525
and then there are multiple things you could choose from.

20
00:00:58,525 --> 00:01:00,765
So as you could see,

21
00:01:00,765 --> 00:01:04,390
you've got, let me make more space.

22
00:01:04,390 --> 00:01:08,495
Okay. So here, there are these different options.

23
00:01:08,495 --> 00:01:18,725
You've got a base map, map theme.

24
00:01:18,725 --> 00:01:20,865
So I'll turn back. So I'll redo that.

25
00:01:20,865 --> 00:01:24,020
I'll click on this guy again.

26
00:01:24,020 --> 00:01:26,295
I don't want the focus mode.

27
00:01:26,295 --> 00:01:30,485
Sometimes it just takes a while,

28
00:01:30,485 --> 00:01:34,145
so let's give it few minutes.

29
00:01:34,145 --> 00:01:37,815
Okay.

30
00:01:37,815 --> 00:01:41,395
All right.

31
00:01:41,395 --> 00:01:42,764
So here,

32
00:01:42,764 --> 00:01:44,135
you have got the Esri graph,

33
00:01:44,135 --> 00:01:48,390
and what we could do is I can

34
00:01:48,390 --> 00:01:53,870
click over here and change that.

35
00:01:53,870 --> 00:01:56,170
More options, where are the more options?

36
00:01:56,170 --> 00:01:58,056
Okay.

37
00:01:58,056 --> 00:02:01,850
So here, you could see you have got this base map,

38
00:02:01,850 --> 00:02:05,600
map theme, symbols, style, and other stuff.

39
00:02:05,600 --> 00:02:08,450
So I've clicked over here on the map theme.

40
00:02:08,450 --> 00:02:13,880
And I could see all the visualization type.

41
00:02:13,880 --> 00:02:16,280
So I'm using this size over here.

42
00:02:16,280 --> 00:02:21,720
And base map, let's make it a dark grey canvas.

43
00:02:21,720 --> 00:02:23,655
So when I clicked over here,

44
00:02:23,655 --> 00:02:27,335
you'd see that this should change.

45
00:02:27,335 --> 00:02:29,615
Maybe it takes awhile.

46
00:02:29,615 --> 00:02:33,110
And once it does that,

47
00:02:33,110 --> 00:02:35,700
I go back to the report,

48
00:02:35,700 --> 00:02:42,945
and we just wait for this thing to finish.

49
00:02:42,945 --> 00:02:45,590
Okay, all right.

50
00:02:45,590 --> 00:02:48,560
So I think it has made the change,

51
00:02:48,560 --> 00:02:50,485
and now you could see

52
00:02:50,485 --> 00:02:54,015
all this thing in a dark background.

53
00:02:54,015 --> 00:02:56,795
It will take a while to load this stuff,

54
00:02:56,795 --> 00:02:58,295
but while this is happening,

55
00:02:58,295 --> 00:03:01,385
we could do some of the other stuff.

56
00:03:01,385 --> 00:03:07,165
And I didn't want to move over here.

57
00:03:07,165 --> 00:03:10,800
And let's see.

58
00:03:10,800 --> 00:03:13,960
Okay, stop, parking violations.

59
00:03:13,960 --> 00:03:28,120
All

60
00:03:28,120 --> 00:03:28,810
right.

61
00:03:28,810 --> 00:03:30,355
So here, as you can see,

62
00:03:30,355 --> 00:03:32,680
after I've changed the layout,

63
00:03:32,680 --> 00:03:41,390
you see that the Esri map is in the black background,

64
00:03:41,390 --> 00:03:44,125
and you have got these parking violations.

65
00:03:44,125 --> 00:03:47,625
But still, you don't see that in the same thing.

66
00:03:47,625 --> 00:03:50,265
You see there is this big line which is coming up.

67
00:03:50,265 --> 00:03:52,549
There are these plus and minus things

68
00:03:52,549 --> 00:03:55,026
here to zoom, and other stuff.

69
00:03:55,026 --> 00:03:57,810
Let's see what other things we could do.

70
00:03:57,810 --> 00:04:01,590
Parking violations in Seattle,

71
00:04:01,590 --> 00:04:06,228
Control Z, I don't want to queue with that.

72
00:04:06,228 --> 00:04:11,980
Okay, and change this to maybe a different color.

73
00:04:11,980 --> 00:04:15,966
Parking violations in Seattle,

74
00:04:15,966 --> 00:04:18,865
so since it's violations,

75
00:04:18,865 --> 00:04:22,950
something bad, we change that to a red color,

76
00:04:22,950 --> 00:04:27,775
and we'll do the same thing for the line graph.

77
00:04:27,775 --> 00:04:33,780
And over here, and the data colors as they

78
00:04:33,780 --> 00:04:42,416
are popping up, one, two, okay.

79
00:04:42,416 --> 00:04:48,605
So the data color came over here,

80
00:04:48,605 --> 00:04:53,150
and as soon as it opens,

81
00:05:00,600 --> 00:05:11,835
we change the color over here.

82
00:05:11,835 --> 00:05:20,400
And further, we do the same over here.

83
00:05:20,400 --> 00:05:24,410
And now for the seasons,

84
00:05:24,410 --> 00:05:29,860
change the color as soon as it opens.

85
00:05:29,860 --> 00:05:31,744
Change the color over here,

86
00:05:31,744 --> 00:05:34,115
and I think we should have

87
00:05:34,115 --> 00:05:39,200
changed the color over here as well.

88
00:05:43,530 --> 00:05:47,415
So edit, as soon as it opens.

89
00:05:47,415 --> 00:05:56,595
And you have got the map theme opening up.

90
00:05:56,595 --> 00:06:01,370
Let's go to

91
00:06:01,370 --> 00:06:01,460
the

92
00:06:01,460 --> 00:06:01,740
symbol

93
00:06:01,740 --> 00:06:14,160
style.

94
00:06:14,160 --> 00:06:19,585
Okay, so the symbol style,

95
00:06:19,585 --> 00:06:22,010
you have got that color over here,

96
00:06:22,010 --> 00:06:24,285
and then we will try to match

97
00:06:24,285 --> 00:06:30,920
this guy to something that we had.

98
00:06:45,380 --> 00:06:50,490
Yes, sometimes it takes a while to load actually.

99
00:06:50,490 --> 00:06:55,135
So what I've done is I've got another one,

100
00:06:55,135 --> 00:06:57,115
which I have got open,

101
00:06:57,115 --> 00:07:00,260
which will show the final visualization that I've got.

102
00:07:00,260 --> 00:07:02,935
So let me just show whether,

103
00:07:02,935 --> 00:07:04,900
and we'll real quickly tell like

104
00:07:04,900 --> 00:07:07,570
what other things I did over there,

105
00:07:07,570 --> 00:07:15,985
as soon as it opens.

106
00:07:15,985 --> 00:07:22,490
All right.

107
00:07:22,490 --> 00:07:25,436
So here, what I've got is here I've got the same data

108
00:07:25,436 --> 00:07:28,735
set and kind of a final version,

109
00:07:28,735 --> 00:07:30,580
but I'll quickly tell what

110
00:07:30,580 --> 00:07:33,615
all things are happening underneath it.

111
00:07:33,615 --> 00:07:35,740
And what you see over here

112
00:07:35,740 --> 00:07:39,563
is I've got a label that I've added.

113
00:07:39,563 --> 00:07:41,860
So when I'm doing this at the time of the day,

114
00:07:41,860 --> 00:07:44,255
which time of the day I should be avoiding,

115
00:07:44,255 --> 00:07:46,675
I did a text box,

116
00:07:46,675 --> 00:07:51,070
which is the maximum time that this event happened.

117
00:07:51,070 --> 00:07:54,135
And then I've got other things over here,

118
00:07:54,135 --> 00:07:56,427
so let me change the view to what, yeah.

119
00:07:56,427 --> 00:08:04,530
So kind of those things,

120
00:08:04,530 --> 00:08:06,615
so peak time to get a ticket.

121
00:08:06,615 --> 00:08:09,261
And then just adding a few labels.

122
00:08:09,261 --> 00:08:11,580
So this is the text box that I added,

123
00:08:11,580 --> 00:08:12,900
which shows there are free

124
00:08:12,900 --> 00:08:14,970
weekend parking which are happening.

125
00:08:14,970 --> 00:08:17,675
And then Harbor Avenue Southwest,

126
00:08:17,675 --> 00:08:20,665
where we get a number of parking violation tickets,

127
00:08:20,665 --> 00:08:21,870
so I did something on

128
00:08:21,870 --> 00:08:24,820
those lines as well, peak in summer months.

129
00:08:24,820 --> 00:08:27,900
And then every time

130
00:08:27,900 --> 00:08:30,420
you're doing any kind of visualization,

131
00:08:30,420 --> 00:08:33,825
it's very good to have a source.

132
00:08:33,825 --> 00:08:36,180
What is a source of your data set?

133
00:08:36,180 --> 00:08:40,005
Is it something that you've done on your cell phone?

134
00:08:40,005 --> 00:08:42,375
Is this data set available outside,

135
00:08:42,375 --> 00:08:46,135
which someone else can also kind of replicate?

136
00:08:46,135 --> 00:08:48,190
So this was about

137
00:08:48,190 --> 00:08:53,880
the data visualization for the parking violation tickets.

