0
00:00:00,880 --> 00:00:03,724
Now it's time to make sure that you finish strong.

1
00:00:03,724 --> 00:00:06,539
You've taken your audience through a story, you've gotten

2
00:00:06,539 --> 00:00:09,910
them engaged, and it's time to really bring it home.

3
00:00:09,910 --> 00:00:13,417
Do this by reiterating your key insights.

4
00:00:13,417 --> 00:00:17,803
After your story's done you really need to take the occasion

5
00:00:17,803 --> 00:00:22,310
to explain to them what it was that you felt was important.

6
00:00:22,310 --> 00:00:25,120
What mattered in the story that you delivered.

7
00:00:25,120 --> 00:00:28,487
Earlier we did the exercise of boiling your story essentials

8
00:00:28,487 --> 00:00:30,290
down into one key sentence.

9
00:00:30,290 --> 00:00:32,071
This may be the time to do that.

10
00:00:32,071 --> 00:00:35,353
Reiterate your key insights and then be clear on

11
00:00:35,353 --> 00:00:38,780
the recommendations that come out of that.

12
00:00:38,780 --> 00:00:41,460
Again, you've taken them through this story.

13
00:00:41,460 --> 00:00:43,325
You've won them over.

14
00:00:43,325 --> 00:00:45,875
At this point you're in the position to be able to drive

15
00:00:45,875 --> 00:00:46,595
influence.

16
00:00:46,595 --> 00:00:49,215
And this is your moment to do that clearly and

17
00:00:49,215 --> 00:00:50,865
with great articulation.

18
00:00:50,865 --> 00:00:53,805
And finally, if you've got a call to action,

19
00:00:53,805 --> 00:00:56,335
if there are next steps that you want them to follow,

20
00:00:56,335 --> 00:00:58,825
this is the time to spell those out.

21
00:00:58,825 --> 00:01:02,285
Many people assume that at this point, everything that they

22
00:01:02,285 --> 00:01:05,700
wanted their listeners to get has landed and

23
00:01:05,700 --> 00:01:06,770
they've gotten it.

24
00:01:06,770 --> 00:01:09,000
That's not necessarily the case.

25
00:01:09,000 --> 00:01:12,471
So be sure that you don't miss your opportunity and

26
00:01:12,471 --> 00:01:16,587
be very clear and specific on these points to drive the impact

27
00:01:16,587 --> 00:01:17,891
that you defined.

