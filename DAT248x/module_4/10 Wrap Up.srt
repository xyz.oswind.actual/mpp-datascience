0
00:00:00,420 --> 00:00:03,010
>> The world's data is said to be

1
00:00:03,010 --> 00:00:05,055
doubling every couple of years.

2
00:00:05,055 --> 00:00:06,880
And if you look into these open datasets

3
00:00:06,880 --> 00:00:08,034
which are out there,

4
00:00:08,034 --> 00:00:10,235
I already got business and datasets,

5
00:00:10,235 --> 00:00:12,180
every data has a story.

6
00:00:12,180 --> 00:00:14,490
If you listen carefully, it talks.

7
00:00:14,490 --> 00:00:16,375
It will tell you all these stories.

8
00:00:16,375 --> 00:00:18,730
And as you have learned all these skills,

9
00:00:18,730 --> 00:00:21,085
telling stories with data,

10
00:00:21,085 --> 00:00:23,720
it is very exciting time and don't just stop over here.

11
00:00:23,720 --> 00:00:27,970
We'll be starting a hashtag in Twitter called

12
00:00:27,970 --> 00:00:30,520
#datastory where we'll be

13
00:00:30,520 --> 00:00:33,250
sharing more of these things that we have learned.

14
00:00:33,250 --> 00:00:35,320
And you can also share your stuff,

15
00:00:35,320 --> 00:00:38,020
and together as a community, we can grow.

16
00:00:38,020 --> 00:00:40,150
>> Thank you, Gorez. And as well,

17
00:00:40,150 --> 00:00:41,410
you can contribute more on

18
00:00:41,410 --> 00:00:43,800
the discussion boards as the course goes on.

19
00:00:43,800 --> 00:00:46,510
And through Mario and through

20
00:00:46,510 --> 00:00:49,390
my content and through Gorez's contributions, hopefully,

21
00:00:49,390 --> 00:00:52,510
you do have what it takes now to craft

22
00:00:52,510 --> 00:00:54,430
that perfect data story and

23
00:00:54,430 --> 00:00:56,860
bring that to your business or to the world at large.

24
00:00:56,860 --> 00:01:00,020
So, thank you. And we hope to see you again.

