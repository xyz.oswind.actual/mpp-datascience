0
00:00:00,950 --> 00:00:02,300
So serving our customers,

1
00:00:02,300 --> 00:00:04,440
welcome to the Support CSAT Briefing.

2
00:00:04,440 --> 00:00:07,580
So on market leadership, Shelly, thank you for having me here.

3
00:00:07,580 --> 00:00:11,640
And here to present today some of the findings that we've

4
00:00:11,640 --> 00:00:14,130
discovered as we're looking through our

5
00:00:14,130 --> 00:00:19,200
customer support data and also provide some recommendations.

6
00:00:19,200 --> 00:00:22,490
So without further ado, so first off,

7
00:00:22,490 --> 00:00:25,520
globally customer satisfaction with support is high.

8
00:00:25,520 --> 00:00:29,720
So I think it's a great thing to understand and

9
00:00:29,720 --> 00:00:32,000
begin with our time together.

10
00:00:32,000 --> 00:00:35,450
So on a scale of 1 to 9, we're near an 8 for

11
00:00:35,450 --> 00:00:39,110
all of 2015 for our customer satisfaction overall on average.

12
00:00:39,110 --> 00:00:42,235
Of course their are outliers and there are some months we'll

13
00:00:42,235 --> 00:00:46,040
analyze a little bit going in the next little bit.

14
00:00:46,040 --> 00:00:51,120
But being near 8 out of 9 on average is something that

15
00:00:51,120 --> 00:00:53,400
is great from our perspective,

16
00:00:53,400 --> 00:00:56,020
and historically what I hope we continue to see.

17
00:00:57,180 --> 00:01:01,180
So for 2015 we noticed that there were some variations,

18
00:01:01,180 --> 00:01:03,210
January, February maybe a little low point,

19
00:01:03,210 --> 00:01:07,495
November a low point, but again on average we are riding near 8.

20
00:01:08,620 --> 00:01:11,550
The next chart, so we're looking at how long it took

21
00:01:11,550 --> 00:01:14,540
us to respond to our customer with a solution.

22
00:01:14,540 --> 00:01:19,041
We can see, in terms of hours, it was definitely,

23
00:01:19,041 --> 00:01:23,241
maybe between 2.5, 3, up to near 4.5.

24
00:01:23,241 --> 00:01:26,830
So in terms of turn around time it's actually globally on

25
00:01:26,830 --> 00:01:28,470
average pretty good.

26
00:01:28,470 --> 00:01:32,550
So you can think of that as enough time to watch a movie or

27
00:01:32,550 --> 00:01:34,580
take a break at work, right?

28
00:01:34,580 --> 00:01:38,520
Have a long lunch break, one thing I was looking into was,

29
00:01:38,520 --> 00:01:40,980
where there any patterns in the data.

30
00:01:40,980 --> 00:01:44,820
Do we see any correlation perhaps, between the 2 sets?

31
00:01:44,820 --> 00:01:48,040
Maybe there's a little bit, but on the whole I think it's either

32
00:01:48,040 --> 00:01:51,560
inverse or there's maybe perhaps a little correlation.

33
00:01:51,560 --> 00:01:53,160
Without going deep into the math,

34
00:01:53,160 --> 00:01:57,180
that's what I've seen from our perspective.

35
00:01:57,180 --> 00:01:59,630
So, let's dig into a little month that maybe is more

36
00:01:59,630 --> 00:02:02,740
interesting than others, February being a place where

37
00:02:02,740 --> 00:02:05,820
CSAT dropped and the time to respond rose.

38
00:02:06,980 --> 00:02:09,010
What we saw in February where there were issues primarily

39
00:02:09,010 --> 00:02:10,590
around billing and payments, and

40
00:02:10,590 --> 00:02:12,290
this is consistent throughout the data set.

41
00:02:12,290 --> 00:02:15,210
So we saw those being some customer issues that we were

42
00:02:15,210 --> 00:02:19,190
experiencing as that relates to paying for our service.

43
00:02:19,190 --> 00:02:22,930
Also an interesting point is the poor or no performance for

44
00:02:22,930 --> 00:02:23,670
their advertising.

45
00:02:23,670 --> 00:02:27,560
So customers coming in, buying this advertising base from us,

46
00:02:27,560 --> 00:02:28,784
there were no poor or

47
00:02:28,784 --> 00:02:31,870
no performance as it relates to their purchase.

48
00:02:31,870 --> 00:02:36,160
So that was significant I think, and perhaps even

49
00:02:36,160 --> 00:02:38,995
particular to February as one of the major issues.

50
00:02:38,995 --> 00:02:41,120
We move on to June,

51
00:02:41,120 --> 00:02:43,920
we see that there was a high point in CSAT, right?

52
00:02:43,920 --> 00:02:46,930
And there was perhaps it took a little bit less to respond,

53
00:02:46,930 --> 00:02:48,760
about a half an hour,

54
00:02:48,760 --> 00:02:52,570
and the top five issues definitely didn't change.

55
00:02:52,570 --> 00:02:55,210
So we can see that poor performance is now being

56
00:02:55,210 --> 00:02:56,880
flipped by editorial.

57
00:02:56,880 --> 00:02:58,530
And this might mean that

58
00:02:59,570 --> 00:03:02,080
their ads were performing more to expectations,

59
00:03:02,080 --> 00:03:06,510
and lesser problems were actually prevalent in June.

60
00:03:06,510 --> 00:03:10,440
So seeing that kind of pattern is

61
00:03:10,440 --> 00:03:13,720
something that rose from the data analysis.

62
00:03:13,720 --> 00:03:18,458
So is there a particular CSAT relationship between this flip,

63
00:03:18,458 --> 00:03:22,850
or can we draw some more insights from that?

64
00:03:22,850 --> 00:03:25,280
At this point, my analysis

65
00:03:25,280 --> 00:03:29,500
has shown that although these are top five issues, billing and

66
00:03:29,500 --> 00:03:31,270
payments is consistent throughout.

67
00:03:31,270 --> 00:03:35,310
And we see different variations in the other top four, but

68
00:03:35,310 --> 00:03:39,230
something to focus on in terms of our 80-20 analysis.

69
00:03:39,230 --> 00:03:41,330
It'd be nice to solve that problem, right?

70
00:03:41,330 --> 00:03:42,230
So billing and payments,

71
00:03:42,230 --> 00:03:44,460
let's focus on that, see where we can go.

72
00:03:45,750 --> 00:03:48,120
Also to note, most MTTR pain points,

73
00:03:48,120 --> 00:03:50,765
again, going back to correlation between MTTR and

74
00:03:50,765 --> 00:03:54,010
CSAT are outside the main geographic areas of service.

75
00:03:54,010 --> 00:03:57,911
So we have United States, India, and UK as the primary places

76
00:03:57,911 --> 00:04:00,463
that service requests are coming into,

77
00:04:00,463 --> 00:04:02,802
so these are our main areas of focus.

78
00:04:02,802 --> 00:04:06,851
And then MTTR by region, we can see that you have a lot of

79
00:04:06,851 --> 00:04:10,374
non-U.S., UK, India country regions, so

80
00:04:10,374 --> 00:04:15,780
you have the Seychelles, you have Sudan, Brunei, Netherlands.

81
00:04:15,780 --> 00:04:18,260
So, all of these places that are a little bit

82
00:04:18,260 --> 00:04:20,620
outside of the main service request areas.

83
00:04:20,620 --> 00:04:24,940
So again, just to draw the point that CSAT and

84
00:04:24,940 --> 00:04:28,890
MTTR in terms of volume, in terms of service,

85
00:04:28,890 --> 00:04:32,240
are perhaps not as related as we had previously thought.

86
00:04:32,240 --> 00:04:36,120
So some other issues that we're seeing, dropped calls and

87
00:04:36,120 --> 00:04:41,118
misroutes provide maybe perhaps some of the largest pain points.

88
00:04:41,118 --> 00:04:44,361
So the quality support drops definitely when you're on

89
00:04:44,361 --> 00:04:45,970
a support call, and

90
00:04:45,970 --> 00:04:48,510
the person you're talking to drops the call.

91
00:04:48,510 --> 00:04:52,090
So that makes sense, I don't know if we actually

92
00:04:52,090 --> 00:04:54,520
were observing that or knew that prior to the analysis.

93
00:04:54,520 --> 00:04:57,070
So that may be something to keep in mind,

94
00:04:57,070 --> 00:05:00,980
perhaps it's a networking issue we should figure that one out.

95
00:05:00,980 --> 00:05:05,036
Also misroutes, so this seems like an easy one to solve.

96
00:05:05,036 --> 00:05:08,440
So sending customers to the wrong spot and letting

97
00:05:08,440 --> 00:05:11,642
them sit there, or maybe they hangup cuz nobody's answering.

98
00:05:11,642 --> 00:05:13,890
Could be a staffing thing,

99
00:05:13,890 --> 00:05:16,080
something that we should investigate.

100
00:05:17,450 --> 00:05:21,287
Another thing with MTTR to keep in mind is that most of these

101
00:05:21,287 --> 00:05:22,800
top five like we said,

102
00:05:22,800 --> 00:05:25,840
billion payments, poor performance editorial,

103
00:05:25,840 --> 00:05:29,310
are taking roughly the same amount of time to respond to.

104
00:05:29,310 --> 00:05:31,840
So, it's a little bit difficult to read, but the MTTR,

105
00:05:31,840 --> 00:05:34,590
you can see it stays about the same and

106
00:05:34,590 --> 00:05:37,140
then it shoots up in some of these exceptions.

107
00:05:37,140 --> 00:05:41,080
So you have, let's say, reserved or, let's see,

108
00:05:41,080 --> 00:05:43,810
it looks like invalid clicks or leads.

109
00:05:43,810 --> 00:05:46,800
So some of these MTTR takes longer but on average,

110
00:05:46,800 --> 00:05:49,580
it looks like those top issues are taking about

111
00:05:49,580 --> 00:05:51,542
the same time to respond to.

112
00:05:51,542 --> 00:05:54,900
So again, that said

113
00:05:54,900 --> 00:05:59,130
there are a few specific ones in terms of affecting CSAT as it

114
00:05:59,130 --> 00:06:02,050
relates to these top issues that we can deal with.

115
00:06:02,050 --> 00:06:04,630
For your reference we also have a CSAT dashboard and

116
00:06:04,630 --> 00:06:06,090
you can look at this after our meeting.

117
00:06:06,090 --> 00:06:08,060
And it's something that you can just takeaway and

118
00:06:08,060 --> 00:06:10,700
think about and poke at, and look at yourself and

119
00:06:10,700 --> 00:06:14,130
investigate as it relates to the stuff we've talked about.

120
00:06:14,130 --> 00:06:18,070
And finally I'd like to provide some recommendations, so we saw

121
00:06:18,070 --> 00:06:24,150
that near 9 was close, like a CSAT average, or near 8, sorry.

122
00:06:24,150 --> 00:06:28,400
And it's 1 through 9, but in recent months we were not

123
00:06:28,400 --> 00:06:32,460
hitting 8 at all, we were down near closer to your 7.5.

124
00:06:32,460 --> 00:06:36,610
We think that it was set at a reasonable target is important.

125
00:06:36,610 --> 00:06:40,680
We'd say that 8 is the highest amount that we saw, just over 8,

126
00:06:40,680 --> 00:06:42,660
so let's begin there.

127
00:06:42,660 --> 00:06:46,070
And perhaps with each support center country region, you can

128
00:06:46,070 --> 00:06:49,190
provide it's own target and we can help you put that together.

129
00:06:50,550 --> 00:06:51,846
But as a final point,

130
00:06:51,846 --> 00:06:55,235
it would be great to know how these targets will be used.

131
00:06:55,235 --> 00:06:57,861
Because I think that that will provide some context for

132
00:06:57,861 --> 00:07:00,716
us to understand how much rigor should we put in into setting

133
00:07:00,716 --> 00:07:01,530
these targets.

134
00:07:01,530 --> 00:07:04,354
Are they gonna be used for compensation, and

135
00:07:04,354 --> 00:07:07,474
that kind of a context will help us project plan for

136
00:07:07,474 --> 00:07:09,349
this next period of our work.

137
00:07:09,349 --> 00:07:13,019
So thank you, and I look forward to working with you some more,

138
00:07:13,019 --> 00:07:14,000
any questions?

