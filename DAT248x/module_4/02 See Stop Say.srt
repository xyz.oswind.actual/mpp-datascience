0
00:00:00,790 --> 00:00:04,567
Now that you've engaged your audience with a killer hook and

1
00:00:04,567 --> 00:00:08,788
your story is under way, use the See-Stop-Say approach to making

2
00:00:08,788 --> 00:00:10,565
sure that your analytics and

3
00:00:10,565 --> 00:00:13,840
the findings that you're delivering really land.

4
00:00:13,840 --> 00:00:16,767
When you're giving a presentation, it's just you and

5
00:00:16,767 --> 00:00:18,696
the data on a monitor or an artifact,

6
00:00:18,696 --> 00:00:21,310
and your job is to bring that to life.

7
00:00:21,310 --> 00:00:24,883
As you're engage your audience with a good story and you get to

8
00:00:24,883 --> 00:00:28,251
that point where you really want them to focus on the data,

9
00:00:28,251 --> 00:00:31,413
this is the point where you physically use your eyes and

10
00:00:31,413 --> 00:00:33,427
your body to draw attention to it.

11
00:00:33,427 --> 00:00:37,350
If it's in a monitor stop, turn and look at those data.

12
00:00:37,350 --> 00:00:40,810
Look at that details, make sure that people are looking at it.

13
00:00:40,810 --> 00:00:45,280
Get them engaged in the artifact and the data first.

14
00:00:46,470 --> 00:00:48,630
Walk them through what they need to, and

15
00:00:48,630 --> 00:00:52,390
then refocus your attention on them.

16
00:00:52,390 --> 00:00:54,620
Make sure that the engagement is there,

17
00:00:54,620 --> 00:00:57,440
and then at that point, land your point.

18
00:00:57,440 --> 00:01:00,290
Really, bring it home, deliver the essential thing that you

19
00:01:00,290 --> 00:01:05,190
want them to know in this moment so that they're fully engaged

20
00:01:05,190 --> 00:01:08,680
and on the terms that you need them to be in order for

21
00:01:08,680 --> 00:01:10,370
your story to unfold.

22
00:01:10,370 --> 00:01:14,289
And as appropriate, repeat this process as the heart and

23
00:01:14,289 --> 00:01:17,797
the essence of your story to drive them to that place

24
00:01:17,797 --> 00:01:19,524
of insight and closure.

