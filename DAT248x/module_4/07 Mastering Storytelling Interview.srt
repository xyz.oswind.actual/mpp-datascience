0
00:00:00,320 --> 00:00:04,149
Mario, as we're wrapping up the course, I'd love to hear

1
00:00:04,149 --> 00:00:08,217
your thoughts on what it takes to become a master storyteller.

2
00:00:08,217 --> 00:00:11,121
And I think what we often see is, in the day-to-day

3
00:00:11,121 --> 00:00:13,959
work of analytics, we're stuck doing data work,

4
00:00:13,959 --> 00:00:17,138
we're creating artifacts, we may present sometimes.

5
00:00:17,138 --> 00:00:21,337
But how does someone get to where you are?

6
00:00:21,337 --> 00:00:24,634
>> Well, a critical point is of course practice, work it,

7
00:00:24,634 --> 00:00:28,412
work it, work it, which is just a baseline standard that you're

8
00:00:28,412 --> 00:00:31,112
not gonna be successful if you don't do that.

9
00:00:31,112 --> 00:00:34,364
But some insights that may help you kinda go a little bit

10
00:00:34,364 --> 00:00:37,897
further and deeper on that have to do with the mindset of a lot

11
00:00:37,897 --> 00:00:40,371
of the people that work in engineering and

12
00:00:40,371 --> 00:00:42,300
they work in technical fields.

13
00:00:42,300 --> 00:00:46,241
I do a lot of storytelling for impact workshops in Microsoft.

14
00:00:46,241 --> 00:00:48,143
And I worked with a lot of engineering teams.

15
00:00:48,143 --> 00:00:51,537
And one of the most remarkable things to me is I ask people to

16
00:00:51,537 --> 00:00:55,348
self-rate, ask them how do you rate yourself as a storyteller,

17
00:00:55,348 --> 00:00:59,229
the vast majority of engineers, engineering technical people at

18
00:00:59,229 --> 00:01:02,195
Microsoft view themselves as less than average.

19
00:01:02,195 --> 00:01:04,405
>> Wow. >> Which I always a certain

20
00:01:04,405 --> 00:01:06,095
humor in because of course they're wrong,

21
00:01:07,115 --> 00:01:08,725
of course they are good storytellers.

22
00:01:08,725 --> 00:01:11,810
I mean, human beings are good storytellers.

23
00:01:11,810 --> 00:01:12,975
>> Mm-hm. >> People are telling stories

24
00:01:12,975 --> 00:01:16,597
all the time, or daydreaming, or dreaming at night.

25
00:01:16,597 --> 00:01:22,040
And stories, it's actually just integral to the human condition.

26
00:01:22,040 --> 00:01:24,100
What happens though is in our business lives,

27
00:01:24,100 --> 00:01:27,260
in our technical lives, we sort of think that there's this other

28
00:01:27,260 --> 00:01:30,730
land, the land of business storytelling.

29
00:01:30,730 --> 00:01:33,430
And fundamentally, what we try to do in this course and

30
00:01:33,430 --> 00:01:35,640
in the work that I do is to break that wall down and

31
00:01:35,640 --> 00:01:40,230
say no, it's okay to bring in these areas of meaning.

32
00:01:40,230 --> 00:01:43,155
And by meaning, I think, we have our work lives, but

33
00:01:43,155 --> 00:01:46,470
then there's our private lives, and our family lives, and

34
00:01:46,470 --> 00:01:48,625
our lives as members of the community.

35
00:01:48,625 --> 00:01:52,051
And we have all of these experience that resonate.

36
00:01:52,051 --> 00:01:54,857
And what's your job as a storyteller is to sort of break

37
00:01:54,857 --> 00:01:58,090
that down and become comfortable with those other parts of your

38
00:01:58,090 --> 00:01:59,310
life and bring them in.

39
00:01:59,310 --> 00:02:02,220
If you're doing analytics work, you're struggling,

40
00:02:02,220 --> 00:02:03,450
you're running into problems.

41
00:02:03,450 --> 00:02:04,790
There are unexpected issues.

42
00:02:04,790 --> 00:02:06,570
There are people that create problems.

43
00:02:06,570 --> 00:02:08,540
There's all this human drama.

44
00:02:08,540 --> 00:02:10,940
And chances are that in the challenge you have with that you

45
00:02:10,940 --> 00:02:13,630
have with analytics, you're running into a problem that

46
00:02:13,630 --> 00:02:16,570
resonates greatly with something else that you have in your life.

47
00:02:16,570 --> 00:02:19,780
Some other place where you have somebody just like that causing

48
00:02:19,780 --> 00:02:23,540
problems that remind you of that and I say, bring those in.

49
00:02:23,540 --> 00:02:25,710
Bring those in, use them as metaphors,

50
00:02:25,710 --> 00:02:30,060
use anecdotes and scenarios that are resonant with

51
00:02:30,060 --> 00:02:32,880
what you're trying to get people to understand.

52
00:02:32,880 --> 00:02:36,070
And bring those in in a way that feels natural and

53
00:02:36,070 --> 00:02:37,750
naturally woven in.

54
00:02:37,750 --> 00:02:41,300
It makes things human and it makes things feel like

55
00:02:41,300 --> 00:02:44,610
they are relatable to people at a level outside.

56
00:02:44,610 --> 00:02:46,820
It almost feels like gossip.

57
00:02:46,820 --> 00:02:49,920
Gossip and jokes are two great kinds of stories.

58
00:02:49,920 --> 00:02:53,060
Look at the dynamics of those and try to bring those in.

59
00:02:53,060 --> 00:02:54,110
>> That's a great challenge.

60
00:02:54,110 --> 00:02:55,678
Yeah, that's not an easy job.

61
00:02:55,678 --> 00:02:56,447
>> Yeah. >> Yeah.

62
00:02:56,447 --> 00:02:59,300
>> Yeah, there's one other tip that I would also raise,

63
00:02:59,300 --> 00:03:00,610
story is an experience.

64
00:03:00,610 --> 00:03:04,029
And as as storyteller, you're really in the job of creating

65
00:03:04,029 --> 00:03:07,794
a human experience, painting a picture in other people's mind.

66
00:03:07,794 --> 00:03:11,135
And so the implications here from a practical point of view

67
00:03:11,135 --> 00:03:14,894
are really, first and foremost you need to use active language.

68
00:03:14,894 --> 00:03:18,729
Active, physical, descriptive language, and

69
00:03:18,729 --> 00:03:21,202
this is both labels for things.

70
00:03:21,202 --> 00:03:24,393
For example one thing that people commonly forget to do is

71
00:03:24,393 --> 00:03:28,200
to tell you the names of the characters in their story.

72
00:03:28,200 --> 00:03:31,170
They just simply forget it because it's so close, so

73
00:03:31,170 --> 00:03:34,570
much on their mind that they assume that someone else does.

74
00:03:34,570 --> 00:03:37,510
Names and other details often get left out because of this

75
00:03:37,510 --> 00:03:39,530
cursive knowledge that we have.

76
00:03:39,530 --> 00:03:42,470
I know it, I assume that you also know it.

77
00:03:42,470 --> 00:03:43,150
And typically,

78
00:03:43,150 --> 00:03:45,430
your audience doesn't, take the time to do that.

79
00:03:45,430 --> 00:03:46,060
>> That's great. >> And

80
00:03:46,060 --> 00:03:50,510
then use physical language, use physical, descriptive language.

81
00:03:50,510 --> 00:03:54,760
Brain science shows us the power of just simply using a word.

82
00:03:54,760 --> 00:03:57,870
We now know that if I offer up a smell, I had

83
00:03:57,870 --> 00:04:01,885
one woman in a workshop say, the room smelled like beer and feet.

84
00:04:01,885 --> 00:04:02,710
>> [LAUGH] >> And

85
00:04:02,710 --> 00:04:06,730
when you hear that, two simple words that take almost no time,

86
00:04:06,730 --> 00:04:07,800
the brain lights up.

87
00:04:07,800 --> 00:04:12,295
The olfactory regions actually fire the neurons as though they

88
00:04:12,295 --> 00:04:14,423
were smelling bear and feet.

89
00:04:14,423 --> 00:04:16,076
Use that language to your advantage.

90
00:04:16,076 --> 00:04:17,044
>> Yeah.

91
00:04:17,044 --> 00:04:20,980
>> But focus it on a point and a place in time.

92
00:04:20,980 --> 00:04:22,150
Be very descriptive.

93
00:04:22,150 --> 00:04:26,150
If you can slow things down so that we're on the ground,

94
00:04:26,150 --> 00:04:28,740
create that virtual reality in my mind so

95
00:04:28,740 --> 00:04:31,700
that I'm experiencing what you're experiencing.

96
00:04:31,700 --> 00:04:34,670
And then you can take them where you want them to go.

97
00:04:34,670 --> 00:04:38,064
Now, for some engineer types, this is a hard thing.

98
00:04:38,064 --> 00:04:40,512
I've worked with a lot of executives and

99
00:04:40,512 --> 00:04:44,112
executive communications roles and coached them and say,

100
00:04:44,112 --> 00:04:46,272
you've got a 20 minute keynote,

101
00:04:46,272 --> 00:04:49,379
I want you to spend four minutes minimum on a story.

102
00:04:49,379 --> 00:04:52,632
And they'll say, well, I'm not gonna spend four minutes out of

103
00:04:52,632 --> 00:04:54,417
20, that's just too much time.

104
00:04:54,417 --> 00:04:57,259
And honestly, I say, if you don't land that four minutes,

105
00:04:57,259 --> 00:04:59,392
well, the other 16 are going to be wasted.

106
00:04:59,392 --> 00:05:00,295
>> Wow. >> Land it well and

107
00:05:00,295 --> 00:05:02,730
you can take them wherever you wanna go.

108
00:05:02,730 --> 00:05:03,490
>> Brilliant.

109
00:05:03,490 --> 00:05:04,050
Thank you, Mario.

110
00:05:04,050 --> 00:05:04,930
>> Thank you.

111
00:05:04,930 --> 00:05:05,430
>> Thank you.

