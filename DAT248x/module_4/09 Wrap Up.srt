0
00:00:01,110 --> 00:00:03,600
Thank you for spending time with us in this course on

1
00:00:03,600 --> 00:00:05,940
analytic storytelling for impact.

2
00:00:05,940 --> 00:00:08,602
>> We hope that along the way you've gained new skills in not

3
00:00:08,602 --> 00:00:10,433
only becoming a better storyteller but

4
00:00:10,433 --> 00:00:13,221
ultimately that more effective analytics professional.

5
00:00:13,221 --> 00:00:14,725
>> One thought to leave you with.

6
00:00:14,725 --> 00:00:18,328
Becoming a good storyteller is a long-term proposition.

7
00:00:18,328 --> 00:00:20,828
So we invite you to use the labs, materials and

8
00:00:20,828 --> 00:00:23,472
resources that we've provided in this course.

9
00:00:23,472 --> 00:00:27,345
And continue practicing and becoming a master storyteller.

10
00:00:27,345 --> 00:00:29,286
>> And finally, we'd love to hear from you.

11
00:00:29,286 --> 00:00:32,083
So please feel free to contact us if you'd like to drop us

12
00:00:32,083 --> 00:00:33,332
a line or tell us a story or

13
00:00:33,332 --> 00:00:35,847
make suggestions on improvements to the course.

14
00:00:35,847 --> 00:00:36,729
>> Thank you.

15
00:00:36,729 --> 00:00:37,780
>> Thanks.

