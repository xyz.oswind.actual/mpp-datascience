0
00:00:01,450 --> 00:00:02,850
This has been a course on

1
00:00:02,850 --> 00:00:05,370
analytic storytelling for impact.

2
00:00:05,370 --> 00:00:07,870
How do you know if you actually had impact?

3
00:00:07,870 --> 00:00:10,370
How do you measure whether your story did

4
00:00:10,370 --> 00:00:12,390
what you wanted it to do?

5
00:00:12,390 --> 00:00:14,780
To begin with, ask a simple question.

6
00:00:14,780 --> 00:00:16,810
Was the story shared?

7
00:00:16,810 --> 00:00:19,970
Because a good story is always shared.

8
00:00:19,970 --> 00:00:23,825
A story that is not shared is a story that probably shouldn't

9
00:00:23,825 --> 00:00:24,893
have been told.

10
00:00:24,893 --> 00:00:29,110
At a fundamental level, this is the primary measure.

11
00:00:29,110 --> 00:00:31,381
Of course it's nice to get more granular on that, and

12
00:00:31,381 --> 00:00:33,190
we have plenty of tools to do this.

13
00:00:33,190 --> 00:00:36,500
You can do this through proxies to see where and

14
00:00:36,500 --> 00:00:39,330
how the story was actually registered.

15
00:00:39,330 --> 00:00:41,838
How many times it got read?

16
00:00:41,838 --> 00:00:43,350
How many times it got picked up?

17
00:00:43,350 --> 00:00:47,510
Whether it got shared in email or on social media.

18
00:00:47,510 --> 00:00:50,330
So these metrics that we can gather through things like

19
00:00:50,330 --> 00:00:54,890
dashboards and our IT tools, are very powerful ways to do it.

20
00:00:54,890 --> 00:00:57,490
Of course in a fundamental way,

21
00:00:57,490 --> 00:01:00,230
sometimes we ask our story to help us drive business.

22
00:01:00,230 --> 00:01:02,580
So we can measure whether we have repeat customers or

23
00:01:02,580 --> 00:01:04,580
request for more information.

24
00:01:04,580 --> 00:01:06,910
Things that drive things down the funnel.

25
00:01:06,910 --> 00:01:09,840
An attachment of story is one way to see whether or

26
00:01:09,840 --> 00:01:13,080
not your story work helped to drive these things.

27
00:01:14,430 --> 00:01:17,651
Directly, you can also measure storytelling through surveys,

28
00:01:17,651 --> 00:01:21,400
interviews, follow-up conversations that you have,

29
00:01:21,400 --> 00:01:22,630
testimonials.

30
00:01:22,630 --> 00:01:25,490
These are ways for you to see whether the story actually got

31
00:01:25,490 --> 00:01:26,910
picked up and shared,

32
00:01:26,910 --> 00:01:31,480
whether people remember it is another really critical element.

33
00:01:31,480 --> 00:01:33,775
At a fundamental level though, when you tell a story,

34
00:01:33,775 --> 00:01:37,060
especially face to face, you typically are going to know in

35
00:01:37,060 --> 00:01:38,980
the responses that you've gotten.

36
00:01:38,980 --> 00:01:41,860
These follow-up capabilities are what you can use to

37
00:01:41,860 --> 00:01:44,940
determine whether or not they actually landed and

38
00:01:44,940 --> 00:01:48,880
drove the impact that you wanted to have when you began

39
00:01:48,880 --> 00:01:50,850
the process of building your story.

