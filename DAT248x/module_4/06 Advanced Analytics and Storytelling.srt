0
00:00:01,130 --> 00:00:03,670
Hi, as we prepare to conclude this course,

1
00:00:03,670 --> 00:00:06,565
we thought we'd take few moments to talk about a couple of topics

2
00:00:06,565 --> 00:00:10,250
of special interests that you can keep in mind as you continue

3
00:00:10,250 --> 00:00:13,250
on the journey of becoming a better analytic storyteller.

4
00:00:13,250 --> 00:00:14,030
Ben, so far we've

5
00:00:14,030 --> 00:00:16,800
talked about analytics in a kind of a basic way.

6
00:00:16,800 --> 00:00:17,890
>> Yeah. >> And of course there's a lot

7
00:00:17,890 --> 00:00:19,750
of different flavors and

8
00:00:19,750 --> 00:00:24,510
far more complex iterations of data science and data work.

9
00:00:24,510 --> 00:00:28,910
Can you give us a review of what those new realms are and

10
00:00:28,910 --> 00:00:31,980
talk a little bit about the implications of storytelling

11
00:00:31,980 --> 00:00:32,760
in them?

12
00:00:32,760 --> 00:00:35,120
>> Yes, thanks Mario, it's a great question.

13
00:00:35,120 --> 00:00:38,440
I do think to date we've been talking about

14
00:00:38,440 --> 00:00:40,730
things like the scripted statistics.

15
00:00:40,730 --> 00:00:44,390
Visualizations that are fairly simple of flavor in nature for

16
00:00:44,390 --> 00:00:45,520
good reason.

17
00:00:45,520 --> 00:00:49,310
Crafting a good story is hard, as we've found, but so valuable.

18
00:00:49,310 --> 00:00:53,026
And it usually takes the form of things like Powerpoints, and

19
00:00:53,026 --> 00:00:55,885
that's where 90% of even data scientists,

20
00:00:55,885 --> 00:00:58,611
or big data engineers end up working anyway.

21
00:00:58,611 --> 00:01:02,665
But as we increase in, these axis of scale and

22
00:01:02,665 --> 00:01:07,692
complexity, we get into realms like big data, right?

23
00:01:07,692 --> 00:01:11,675
Where we have just a lot more data and it's coming faster,

24
00:01:11,675 --> 00:01:13,963
it's coming in greater volume.

25
00:01:13,963 --> 00:01:17,027
And the reality is that most of the techniques we learn

26
00:01:17,027 --> 00:01:19,590
are applicable there and that's great.

27
00:01:19,590 --> 00:01:21,344
So, there's not going to be too much of a difference.

28
00:01:21,344 --> 00:01:23,162
You're still going to use the same visual types.

29
00:01:23,162 --> 00:01:26,255
You're still going to use a lot of descriptive statistics to

30
00:01:26,255 --> 00:01:28,015
describe what's happening, but

31
00:01:28,015 --> 00:01:30,150
you might be doing things like sampling.

32
00:01:30,150 --> 00:01:32,720
And you might be doing things like real-time analytics,

33
00:01:32,720 --> 00:01:35,830
trying to find signal versus the noise.

34
00:01:35,830 --> 00:01:38,288
And this can get pretty difficult, pretty fast.

35
00:01:38,288 --> 00:01:41,082
So, more advanced techniques are used, and

36
00:01:41,082 --> 00:01:44,770
just require that extra care in a story that's being told.

37
00:01:44,770 --> 00:01:47,149
But, I think it's just gonna be bigger stories and

38
00:01:47,149 --> 00:01:47,988
faster stories.

39
00:01:47,988 --> 00:01:50,127
It's not gonna be too different from what we've seen.

40
00:01:50,127 --> 00:01:53,120
But, when we move into more complex scenarios of data

41
00:01:53,120 --> 00:01:56,522
science, we're often times called to predict the future.

42
00:01:56,522 --> 00:01:58,352
And tell people what they should do.

43
00:01:58,352 --> 00:02:01,468
And telling people what they should do can get hairy, right?

44
00:02:01,468 --> 00:02:03,122
>> [LAUGH] >> It's not so

45
00:02:03,122 --> 00:02:06,740
simple to convert somebody's mind in that regard.

46
00:02:06,740 --> 00:02:10,996
So, in fact storytelling becomes that much more central.

47
00:02:10,996 --> 00:02:12,877
In those kind of stories, and

48
00:02:12,877 --> 00:02:15,294
may be worth another course someday.

49
00:02:15,294 --> 00:02:18,410
But as you increase, again, in scale and complexity,

50
00:02:18,410 --> 00:02:21,935
we're then getting to the place of artificial intelligence.

51
00:02:21,935 --> 00:02:25,525
And that's a wild world right now, and a lot of stuff has been

52
00:02:25,525 --> 00:02:28,074
discovered and hasn't yet been laid out.

53
00:02:28,074 --> 00:02:30,640
But we have issues of transparency, and

54
00:02:30,640 --> 00:02:34,640
explainability, interpretability and of course ethical and

55
00:02:34,640 --> 00:02:37,289
legal issues involved with that as well.

56
00:02:37,289 --> 00:02:40,868
I'd say the kind of stories you're telling there,

57
00:02:40,868 --> 00:02:44,283
often require maybe a wave of the hand with math and

58
00:02:44,283 --> 00:02:47,619
talking to the researchers here at Microsoft and

59
00:02:47,619 --> 00:02:51,304
beyond metaphor becomes really important, right?

60
00:02:51,304 --> 00:02:55,735
The analogy and just making it understandable to the normal

61
00:02:55,735 --> 00:02:57,868
person is quite difficult.

62
00:02:57,868 --> 00:03:00,321
>> So the principles of storytelling,

63
00:03:00,321 --> 00:03:02,929
do they go away as you move farther up and

64
00:03:02,929 --> 00:03:06,108
to the right on that scale and complexity axis?

65
00:03:06,108 --> 00:03:10,100
>> No I mean, that they become more essential I'd say in that

66
00:03:10,100 --> 00:03:13,603
if you lose touch with storytelling when you become

67
00:03:13,603 --> 00:03:16,291
more complex or you increase in scale,

68
00:03:16,291 --> 00:03:20,210
you'll lose your audience even that much more quickly.

69
00:03:21,800 --> 00:03:22,460
>> Great.

70
00:03:22,460 --> 00:03:23,260
>> Thank you. >> Thank you.

