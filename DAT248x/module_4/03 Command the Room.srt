0
00:00:01,250 --> 00:00:04,250
One of the hardest parts about being a presenter is knowing

1
00:00:04,250 --> 00:00:05,640
how to command the room.

2
00:00:06,790 --> 00:00:09,600
It's lonely up there, just you and your data and

3
00:00:09,600 --> 00:00:13,460
your monitor and presentation, and all those people sitting in

4
00:00:13,460 --> 00:00:15,740
the room watching and looking at you.

5
00:00:15,740 --> 00:00:19,990
How can you become a master at knowing how to command the room,

6
00:00:19,990 --> 00:00:23,780
take people through that great story that you've built?

7
00:00:23,780 --> 00:00:25,570
And first and foremost,

8
00:00:25,570 --> 00:00:29,090
understand your physical presence, control your body.

9
00:00:30,220 --> 00:00:33,660
This is often really just a matter of standing still.

10
00:00:33,660 --> 00:00:36,330
When people are nervous and a little unconscious, they tend to

11
00:00:36,330 --> 00:00:39,460
dance and fidget and that movement creates a distraction

12
00:00:39,460 --> 00:00:42,330
that could undermine the impact of your story.

13
00:00:42,330 --> 00:00:47,080
Learn how to use your posture, your hands, and your feet, where

14
00:00:47,080 --> 00:00:51,150
you're standing and how you move to pace your story and bring

15
00:00:51,150 --> 00:00:54,760
people through the experience of enjoying what you're delivering.

16
00:00:55,760 --> 00:00:57,240
Next, make eye contact.

17
00:00:58,300 --> 00:01:00,170
Physically look people in the eye, and

18
00:01:00,170 --> 00:01:02,070
it's important that you mix this up.

19
00:01:02,070 --> 00:01:05,890
You don't wanna spend too much time on any one person.

20
00:01:05,890 --> 00:01:08,760
You wanna make sure that you're making eye contact

21
00:01:08,760 --> 00:01:09,650
across the room.

22
00:01:10,910 --> 00:01:14,290
Maintain eye contact with a given individual long enough to

23
00:01:14,290 --> 00:01:17,650
land a sentence or an idea, and then move on.

24
00:01:17,650 --> 00:01:20,160
You do this well, and everyone in the room will think that

25
00:01:20,160 --> 00:01:22,900
you're telling your story directly to him or her.

26
00:01:24,320 --> 00:01:28,220
Next, be mindful of that verbal punctuation that we do when

27
00:01:28,220 --> 00:01:31,380
we say and.

28
00:01:31,380 --> 00:01:33,090
Start to pay attention to this.

29
00:01:33,090 --> 00:01:35,760
Pay attention to this in your own speech.

30
00:01:35,760 --> 00:01:38,105
Pay attention to this in the presentations that you'll see

31
00:01:38,105 --> 00:01:41,620
and you'll be amazed at how frequent and common they are.

32
00:01:41,620 --> 00:01:45,550
Often these are used as bridges from one sentence to another as

33
00:01:45,550 --> 00:01:48,380
though you have this long string of consciousness.

34
00:01:48,380 --> 00:01:51,290
Learn to eradicate those and your story will be

35
00:01:51,290 --> 00:01:54,430
much stronger and will feel much better structured.

36
00:01:56,050 --> 00:01:59,210
Manage the pace and volume of what you do.

37
00:01:59,210 --> 00:02:02,300
A common mistake in storytelling, verbal

38
00:02:02,300 --> 00:02:05,920
presentation storytelling, is that people use the same tone,

39
00:02:05,920 --> 00:02:08,830
the same volume, and the same pace.

40
00:02:08,830 --> 00:02:10,201
You need inflections.

41
00:02:10,201 --> 00:02:12,862
You need to know when to use a pause.

42
00:02:12,862 --> 00:02:16,030
You need to know when to slow down and when to speed up.

43
00:02:16,030 --> 00:02:19,180
And these nonverbal techniques

44
00:02:19,180 --> 00:02:23,610
are what help the audience follow you emotionally, and

45
00:02:23,610 --> 00:02:25,830
through the course of the content that you're delivering,

46
00:02:25,830 --> 00:02:29,830
and the experience that you're creating as a storyteller.

47
00:02:29,830 --> 00:02:32,396
Bonus tip here, record yourself.

48
00:02:32,396 --> 00:02:35,640
Take out your camera, take out your phone, and

49
00:02:35,640 --> 00:02:36,820
do a recording of yourself.

50
00:02:36,820 --> 00:02:38,260
Now this is painful.

51
00:02:38,260 --> 00:02:40,950
No emotionally healthy human being enjoys

52
00:02:40,950 --> 00:02:43,780
watching a recording of himself or herself.

53
00:02:43,780 --> 00:02:46,480
And yet it's essential because when you do this, you'll see how

54
00:02:46,480 --> 00:02:50,010
many times you're doing these things, and how common or

55
00:02:50,010 --> 00:02:53,720
frequent one or two of these little glitches are, which will

56
00:02:53,720 --> 00:02:58,340
go away simply by you seeing them and becoming aware of them.

57
00:02:58,340 --> 00:03:01,130
Do that, follow these steps and you can command a room.

