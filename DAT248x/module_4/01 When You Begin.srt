0
00:00:00,610 --> 00:00:03,100
Now let's talk about how to land your story.

1
00:00:03,100 --> 00:00:06,630
Specifically, face-to-face in a presentation, whether it's in

2
00:00:06,630 --> 00:00:10,320
a large room, one-on-one, or just to your team.

3
00:00:10,320 --> 00:00:14,150
There's some principles that apply on how to begin and

4
00:00:14,150 --> 00:00:17,420
those principles have to do with time.

5
00:00:17,420 --> 00:00:20,700
Conventional wisdom says you have seven seconds, seven

6
00:00:20,700 --> 00:00:25,100
seconds during which time your audience is making the decision

7
00:00:25,100 --> 00:00:27,840
about whether they're gonna really engage with you or not.

8
00:00:27,840 --> 00:00:30,070
And that's really about whether they recognize that you're

9
00:00:30,070 --> 00:00:33,300
telling a compelling story or not.

10
00:00:33,300 --> 00:00:34,660
How do you do that?

11
00:00:34,660 --> 00:00:38,670
Well, to begin with, I recommend that you start with a good,

12
00:00:38,670 --> 00:00:39,930
deep breath.

13
00:00:39,930 --> 00:00:41,790
Start with a pause.

14
00:00:41,790 --> 00:00:43,240
This does a few things.

15
00:00:44,590 --> 00:00:48,450
Grounds you, gets you ready for the presentation.

16
00:00:48,450 --> 00:00:51,460
And it creates a moment of silence in the room.

17
00:00:51,460 --> 00:00:53,810
Pauses and inflections are critical.

18
00:00:53,810 --> 00:00:56,740
Opening with a pause tells your audience

19
00:00:56,740 --> 00:00:58,800
that the story's going to begin.

20
00:00:58,800 --> 00:01:02,060
And then you move on with a bang.

21
00:01:02,060 --> 00:01:04,500
Previously, we talked about the killer hook,

22
00:01:04,500 --> 00:01:09,050
that one compelling element that is gonna open your story.

23
00:01:09,050 --> 00:01:13,120
Do that well, your audience in that seven seconds will know

24
00:01:13,120 --> 00:01:14,560
that a story is coming.

25
00:01:14,560 --> 00:01:18,650
And from that point forward, you can carry them on to the journey

26
00:01:18,650 --> 00:01:20,770
that you want your story to comprise.

