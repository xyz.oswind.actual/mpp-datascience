0
00:00:05,049 --> 00:00:10,349
Alright well, you’ve seen that we&#39;ve been able to analyze some data using some formulae,

1
00:00:10,349 --> 00:00:15,760
some aggregate functions in there and then we added the few visualizations to the individual

2
00:00:15,760 --> 00:00:19,800
columns. Well what do you think of the visualizations? &gt;&gt; So I love Sparkline, I think that’s a

3
00:00:19,800 --> 00:00:23,640
great place to start but it’s really just the tip of the ice berg in understanding what’s

4
00:00:23,640 --> 00:00:27,619
in the data and what’s going on there. So I think that probably the next step is actually

5
00:00:27,619 --> 00:00:33,190
to create some charts and some graphs and some figures and do a true visual representation

6
00:00:33,190 --> 00:00:37,420
of what’s going on in the data. &gt;&gt; Yeah absolutely, I think one of the things we&#39;ve

7
00:00:37,420 --> 00:00:42,550
done is we&#39;ve created visualization on an individual column basis. It really the power

8
00:00:42,550 --> 00:00:47,059
of charts has been able to combine columns together and see how one thing compares to

9
00:00:47,059 --> 00:00:51,379
the other. So really important that we can get a handle on how to use chart. Before we

10
00:00:51,379 --> 00:00:55,579
do that though, I guess we need to do is to understand some of the basic concepts that

11
00:00:55,579 --> 00:01:00,410
are in charts, just some of the common features that you see. So let’s take a look at that.

12
00:01:00,410 --> 00:01:04,530
So here is Rosie and Rosie wants to learn a little bit about charts and the first thing

13
00:01:04,530 --> 00:01:09,140
that I would probably say to Rosie is that usually when we&#39;re creating charts we use

14
00:01:09,140 --> 00:01:16,080
axis, we&#39;ll be plotting values along axis and normally there’s an X-axis and a Y-axis

15
00:01:16,080 --> 00:01:19,280
in most of the charts; not all of them, by any matter of means, but in most charts there’s

16
00:01:19,280 --> 00:01:24,430
an X and a Y and the easy way to remember is X goes across, because an X is across,

17
00:01:24,430 --> 00:01:29,780
so the X value was across the way and Y or I reach for the sky, so the Y value goes up

18
00:01:29,780 --> 00:01:34,840
the way. So we got our two axes and what we probably going to want to do with these is

19
00:01:34,840 --> 00:01:39,580
label them so we know what they are or what they represent; the values along my X axis,

20
00:01:39,580 --> 00:01:43,610
in this case, it’s going to be temperature and the value is at my Y axis in this case

21
00:01:43,610 --> 00:01:49,720
it’s going to be the sales. So I’m comparing two different things along these axes and

22
00:01:49,720 --> 00:01:52,360
one of the things you&#39;ll do is – we talk about plots sometimes, we&#39;re talking about

23
00:01:52,360 --> 00:01:56,850
creating a plot rather than a chart actually a specific type of chart. What I’m going

24
00:01:56,850 --> 00:02:01,480
to do is plot values. The values that I’m going to plot in this case are Sales vs Temperature.

25
00:02:01,480 --> 00:02:05,240
So a title along the top is going to help me to understand what this chart actually

26
00:02:05,240 --> 00:02:09,619
shows and quite often people forget the importance of a title and then you&#39;ll look at the chart

27
00:02:09,619 --> 00:02:12,890
later on and find out – well I don’t know what that means. So we&#39;re going to plot our

28
00:02:12,890 --> 00:02:18,980
values and I’m going to plot them along this scale. So the scale along the axis indicates

29
00:02:18,980 --> 00:02:23,880
the value where the axis would be plotted as coordinate within that space and that is

30
00:02:23,880 --> 00:02:28,540
really important. The scale I think is probably one of the things that it is the most important

31
00:02:28,540 --> 00:02:32,970
consideration of designing a chart. &gt;&gt; It absolutely is in fact there’s a notion in

32
00:02:32,970 --> 00:02:39,000
the industry called G-Whiz chart. So you can really make a difference between two variables

33
00:02:39,000 --> 00:02:44,480
in your dataset, look surprisingly large depending on the scale with what you use to display

34
00:02:44,480 --> 00:02:49,280
the data. So be very aware of the scale that you select. &gt;&gt; Yeah I think that is something

35
00:02:49,280 --> 00:02:54,060
it bears a little bit of discussion I think the scale that you choose, it should be appropriate

36
00:02:54,060 --> 00:02:57,530
for the data that you are plotting, the X and Y scales don’t really have to be the

37
00:02:57,530 --> 00:03:02,050
same. But they do have to be appropriate for the data that you are plotting along those

38
00:03:02,050 --> 00:03:06,370
axes and sometimes one of the things you&#39;ll find is we get more and more into that kind

39
00:03:06,370 --> 00:03:10,500
of data science aspects of this and we start plotting lots of different values in order

40
00:03:10,500 --> 00:03:14,980
to find statistical correlations. Sometimes what you want to do is actually scale your

41
00:03:14,980 --> 00:03:19,300
data, so that all fits within the same scale. You might want to normalize the values so

42
00:03:19,300 --> 00:03:23,959
that they all appear as a value between zero and one for example. So that, there’s some

43
00:03:23,959 --> 00:03:28,380
new considerations we need to go into scaling data and how you represent it. We’re leaving

44
00:03:28,380 --> 00:03:32,160
that side though, we&#39;ll carry on with our simple explanation of a chart. I’m going

45
00:03:32,160 --> 00:03:36,700
to actually going to talk about this O here, is the origin if you like; we talk about the

46
00:03:36,700 --> 00:03:42,610
origin as being the point where the X axis meets the Y axis as zero usually, not always

47
00:03:42,610 --> 00:03:48,880
but usually we have a zero origin there and it&#39;ll plot our value. So here I’m plotting

48
00:03:48,880 --> 00:03:54,900
my sales on the Y axis so the measure up the way to measure sales and the temperature on

49
00:03:54,900 --> 00:04:00,670
the X axis. So we measure along the way to measure temperature. So the further along

50
00:04:00,670 --> 00:04:05,640
the plot on the chart is the higher the temperature and the higher up is on the Y scale, the more

51
00:04:05,640 --> 00:04:10,270
sales that were. And I can kind of see bit of a pattern and those not a lot of data point

52
00:04:10,270 --> 00:04:13,910
really to draw conclusions from, there’s only four, but I can kind of see a pattern

53
00:04:13,910 --> 00:04:20,509
in there as the temperature goes up then sales correspondingly seem to go up and so that’s

54
00:04:20,509 --> 00:04:24,529
one interesting aspect of this. Typically what you&#39;ll do is, if you got plots like this

55
00:04:24,529 --> 00:04:28,979
is actually draw some sort of line through them, so you can start to see a line that

56
00:04:28,979 --> 00:04:34,309
lets me see that as a linear chart. And line chart as it turns out is a really common chart,

57
00:04:34,309 --> 00:04:38,279
especially over time. We tend to use line charts to show out things change over, over

58
00:04:38,279 --> 00:04:44,840
time in the Y axis. But I’m not limited to just one line, I could actually add something

59
00:04:44,840 --> 00:04:49,559
else in there and so I can add in a different plot here. And that makes things confusing

60
00:04:49,559 --> 00:04:54,400
because I don’t know which is which and maybe what I’ve done here is that my blue

61
00:04:54,400 --> 00:04:59,569
line there represents sales of lemonade and my orange line represents sales of orangeade.

62
00:04:59,569 --> 00:05:04,449
Maybe what I’ve done is instead of just all my sales are up, I’ve plotted two different

63
00:05:04,449 --> 00:05:10,419
series of plots along there. So when I add these multiple series we call them in Excel

64
00:05:10,419 --> 00:05:14,669
when we’re creating charts. I probably also want to add a Legend. This is going to let

65
00:05:14,669 --> 00:05:18,599
me figure out which is which. So I’ve added in this case something that just tells the

66
00:05:18,599 --> 00:05:22,509
person looking at the chart look the blue dots and the blue line represents sales of

67
00:05:22,509 --> 00:05:28,400
lemonade. The orange dots and the orange line represent sales of the orangeade. Now that’s

68
00:05:28,400 --> 00:05:32,409
all fine and well and we&#39;ve got our nice line chart that gives us a bit of information.

69
00:05:32,409 --> 00:05:37,379
Of course, line charts are not the only game in time; I might want to consider doing this

70
00:05:37,379 --> 00:05:42,770
same type of chart as a column chart. In this case, it’s a clustered column chart, where

71
00:05:42,770 --> 00:05:48,499
I’ve got two different columns for each distinct value along the bottom. One showing

72
00:05:48,499 --> 00:05:52,059
the corresponding sales of lemonade and one showing the sales of orangeade so the legend

73
00:05:52,059 --> 00:05:56,370
is still required. An important point is there are different types of chart and depending

74
00:05:56,370 --> 00:06:01,379
upon the specific data values that you&#39;re plotting, one or more of those types of chart

75
00:06:01,379 --> 00:06:03,589
might be more relevant than others.

