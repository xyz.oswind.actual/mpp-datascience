0
00:00:05,120 --> 00:00:10,240
Well hi, and welcome back. I’m joined by Liberty Munson. So Liberty if you&#39;re perhaps

1
00:00:10,240 --> 00:00:13,980
just introduce yourself to the nice people at home. &gt;&gt; Hi, I’m Liberty Munson I work

2
00:00:13,980 --> 00:00:18,400
at Microsoft, I deal a lot of work with data science and data in general and statistics.

3
00:00:18,400 --> 00:00:23,749
So I’m going to talk to Graeme a little bit about data analysis and get you started.

4
00:00:23,749 --> 00:00:29,950
&gt;&gt; Fantastic, ok. So we have spoken to some data scientists, you&#39;ve seen earlier on the

5
00:00:29,950 --> 00:00:35,149
videos running of what they think about data science and it seems Liberty, that data science

6
00:00:35,149 --> 00:00:39,489
is mostly about exploring data. I think people have this idea that it’s all about building

7
00:00:39,489 --> 00:00:42,890
machine learning models and predictive stuff. But actually it’s really about the working

8
00:00:42,890 --> 00:00:46,850
with the data. Is that your experience? &gt;&gt; It absolutely is. I mean I think the best thing

9
00:00:46,850 --> 00:00:50,940
that you can do with data is just understand what you have and start asking yourself questions

10
00:00:50,940 --> 00:00:57,079
and just start exploring and from there you can make incredible – do incredible things

11
00:00:57,079 --> 00:01:00,839
for businesses and organizations to help them drive improvements and whatever it is, that

12
00:01:00,839 --> 00:01:04,339
their problems are trying to solve. &gt;&gt; Absolutely, so we&#39;re going to have a look

13
00:01:04,339 --> 00:01:09,270
at some ways that we can work with data and I guess one of the things we want to accomplish

14
00:01:09,270 --> 00:01:14,150
in this orientation class really is to make sure that you have the skills you need as

15
00:01:14,150 --> 00:01:18,400
a base to build from as you work through the rest of the program. So we&#39;ll be looking very

16
00:01:18,400 --> 00:01:22,430
quickly at some basic concepts that you&#39;re going to build on, in the courses that come

17
00:01:22,430 --> 00:01:28,640
along later. Okay so I guess maybe let’s talk a little bit about the business of actually

18
00:01:28,640 --> 00:01:32,560
getting the data, first of all. I mean data comes in all sorts of shapes and sizes and

19
00:01:32,560 --> 00:01:36,140
all different places. So that’s probably give place to start right? &gt;&gt; It absolutely

20
00:01:36,140 --> 00:01:39,750
is I mean it can come from anywhere. So, why don’t you talk to us a little bit about

21
00:01:39,750 --> 00:01:44,090
where it could come from? &gt;&gt; Alright let’s take a look at that. Alright, so yeah data

22
00:01:44,090 --> 00:01:48,530
can come from a variety of places as you say, so it could be a simple as files; you might

23
00:01:48,530 --> 00:01:53,170
have some text files, or some documents that are sitting around in a folder or somewhere.

24
00:01:53,170 --> 00:01:58,200
Or increasingly commonly if you working in a large organization, your data is more likely

25
00:01:58,200 --> 00:02:02,000
to be in somewhere like a database. So you&#39;re going to have to go and get it out from there

26
00:02:02,000 --> 00:02:05,980
and of course we live in a connected world now, where there are devices running, that

27
00:02:05,980 --> 00:02:10,590
are emitting data all the time, in real time. You’ve got sensors, and this so-called Internet

28
00:02:10,590 --> 00:02:14,629
of things is out there. So we got all this data from all these types of places that you

29
00:02:14,629 --> 00:02:17,909
might want to get it from. And as a data scientist, you’re kind of

30
00:02:17,909 --> 00:02:21,959
sitting in the middle of all of this world and you&#39;re ready to go and grab that data

31
00:02:21,959 --> 00:02:26,719
and start working on it. And that might be a case of just going and grabbing the files;

32
00:02:26,719 --> 00:02:29,920
maybe you’re going to get your text files from a file share on the network or something

33
00:02:29,920 --> 00:02:34,909
like that or maybe you have to learn a little bit of programming skills in the sense of

34
00:02:34,909 --> 00:02:39,730
SQL. So maybe you write some SQL to go and query a database and bring back a table, a

35
00:02:39,730 --> 00:02:45,579
dataset of data from there or maybe there’s an application you can use that can capture

36
00:02:45,579 --> 00:02:49,420
the data in real time. That real time data streams and bring them into your environment

37
00:02:49,420 --> 00:02:53,780
to work from maybe you&#39;re using something like Azure stream analytics to do that. Either

38
00:02:53,780 --> 00:02:58,170
way you’re going to bring that data back and you&#39;re going use some sort of tool to

39
00:02:58,170 --> 00:03:03,269
go and manipulate that data, work with that data, clean up and then start to analyze that

40
00:03:03,269 --> 00:03:08,109
data. And that tool might be something that is very commonly used like excel, that’s

41
00:03:08,109 --> 00:03:12,400
a really powerful tool that a lot of people use to analyze data. Or maybe it’s a bit

42
00:03:12,400 --> 00:03:16,200
more specialized; may be you&#39;re writing some R code or Python or something like that and

43
00:03:16,200 --> 00:03:21,060
you&#39;re manipulating the data in that language. So you&#39;re going to be working with that data,

44
00:03:21,060 --> 00:03:25,659
I guess what we should probably do is let’s scale things back a bit, and let’s look

45
00:03:25,659 --> 00:03:30,200
at a very simple example and just try and get some of these concepts.

46
00:03:30,200 --> 00:03:38,079
So I want to introduce you to Rosie. Here’s Rosie, Rosie is a middle-school pupil. She

47
00:03:38,079 --> 00:03:42,709
spent her summer holidays, as you know most children do, industriously trying to figure

48
00:03:42,709 --> 00:03:47,189
out how to make money, that’s what she does as the way forward and Rosie has chosen to

49
00:03:47,189 --> 00:03:52,439
do this with a lemonade stand. So Rosie makes this lovely lemonade and she makes it available

50
00:03:52,439 --> 00:03:58,739
for people to buy and because Rosie is quite a smart, switched-on person, she knows that

51
00:03:58,739 --> 00:04:04,379
if she makes use of the data about her lemonade sales then she might have more success going

52
00:04:04,379 --> 00:04:09,659
forward. So she painstakingly records all of the data to do with her lemonade sales

53
00:04:09,659 --> 00:04:14,689
and she stores that in an excel spreadsheet as a csv file. So we&#39;re going to be looking

54
00:04:14,689 --> 00:04:19,530
at how Rosie worked with that data and is able to analyze, so that we can look at a

55
00:04:19,529 --> 00:04:24,050
few basic concepts of exploring data. So there we go, we now know that we&#39;re going to be

56
00:04:24,050 --> 00:04:28,169
looking at Rosie&#39;s data and I guess when you come back in the next session, we&#39;ll have

57
00:04:28,169 --> 00:04:30,939
a look at an example of what Rosie might to do work with that data.

