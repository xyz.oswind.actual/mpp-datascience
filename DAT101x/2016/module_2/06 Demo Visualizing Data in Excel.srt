0
00:00:05,049 --> 00:00:10,059
Here we&#39;re back in Excel and we&#39;ve looked at our data. Now we probably want to visualize

1
00:00:10,059 --> 00:00:13,459
it by creating some of these charts we&#39;ve talked about. So the first thing I’m going

2
00:00:13,459 --> 00:00:16,619
to do is I’m going to look at what – really what my revenues been over time. That’s

3
00:00:16,619 --> 00:00:21,109
a pretty common thing to I want to look at. So I’m going to go and select the dates

4
00:00:21,109 --> 00:00:26,419
here, it is the dates over which I sold stuff. I’ll just hold the Control key and also

5
00:00:26,419 --> 00:00:34,300
select the revenue. I’ve got those two sets of data selected and I’m going to go and

6
00:00:34,300 --> 00:00:39,690
insert, I got a variety of different types of chart here, it’ll even recommend a chart

7
00:00:39,690 --> 00:00:43,090
for me based on what I’ve selected. What I’m going to do is I’m going to select

8
00:00:43,090 --> 00:00:49,960
a Line chart here and here we have a classic financial chart that shows me along the X

9
00:00:49,960 --> 00:00:55,769
axis I’ve got my dates and up the Y axis I’ve got my revenue and is picked up the

10
00:00:55,769 --> 00:01:00,350
formatting. So I can see that’s in dollars, that’s all very nice. Now I’ll just make

11
00:01:00,350 --> 00:01:05,000
that bigger so I can see what’s going on and I might want to change the title here

12
00:01:05,000 --> 00:01:11,890
to show that this is Revenue over Time. So that chart makes it a bit of sense and I might

13
00:01:11,890 --> 00:01:16,390
also want to go ahead and just add some Axis Titles to the chart. So we&#39;ll go ahead and

14
00:01:16,390 --> 00:01:22,250
add those. I get my axis title, so I can see that along there but I’m going to make that

15
00:01:22,250 --> 00:01:30,500
Date and this up here, I’ll make that Revenue – a bit hard to type on that side, but there

16
00:01:30,500 --> 00:01:35,350
we go. So I’ve got my chart here and that is showing me exactly what I expected. Now

17
00:01:35,350 --> 00:01:38,700
it looks to me – it’s difficult to tell that peaks and troughs will looks to me that

18
00:01:38,700 --> 00:01:43,130
as a gradual kind of upward slope there. And what I might want to do is just bear that

19
00:01:43,130 --> 00:01:47,980
out is out of Trend line in my chart. So I can go ahead and do that and I get this dotted

20
00:01:47,980 --> 00:01:52,040
line appearing here, that shows here, if we look at that this linearly, there’s a gradual

21
00:01:52,040 --> 00:01:57,750
sort of trend upwards which is kind of useful to know. And what I might want to also do

22
00:01:57,750 --> 00:02:01,880
with this is maybe compare revenue to something. So just remember when we looked to the data

23
00:02:01,880 --> 00:02:07,120
earlier on, I thought there might be some relationship between temperatures, revenue

24
00:02:07,120 --> 00:02:11,569
and so may be in as temperature goes up, and revenue also goes up.

25
00:02:11,569 --> 00:02:16,180
Let’s take a look at that and see if that’s potential with the case with a bit more investigation.

26
00:02:16,180 --> 00:02:20,860
So what I’m going to do is I’m going to go ahead and I’ve got my chart selected,

27
00:02:20,860 --> 00:02:26,510
I’m going to select the data for this chart and I can see there’s Series1 in here, is

28
00:02:26,510 --> 00:02:32,350
obviously my temperature. So I’m going to add another series, so I’ll just add a second

29
00:02:32,350 --> 00:02:36,090
series. In the second series, I’m going to add – we’ll get its name – I’m

30
00:02:36,090 --> 00:02:39,990
going to just point to the temperature column here, there’s the heading. So it’s going

31
00:02:39,990 --> 00:02:43,920
to pick up the name from there and the values I’m going to put in there. I’m going to

32
00:02:43,920 --> 00:02:48,890
replace this place holder this there with the actual temperature column. I’m going

33
00:02:48,890 --> 00:02:54,970
to select that column. If I Ok that, I’ve now go one series called temperature, one

34
00:02:54,970 --> 00:02:58,770
called series1. So that series name is potentially confusing so I’m just going to add to that

35
00:02:58,770 --> 00:03:04,650
as well and point that at the Revenue title sales. So I can easily see, I’ve got one

36
00:03:04,650 --> 00:03:10,900
series of Revenue and one series of Temperature and Ok to that and now I got my temperature

37
00:03:10,900 --> 00:03:15,540
appearing. It actually looks like a relatively fat line if I’m honest. So it maybe my idea,

38
00:03:15,540 --> 00:03:19,850
that there was a correlation isn’t there now by this chart. What I’ve now got of

39
00:03:19,850 --> 00:03:24,600
course from the chart are these two different colors and I’m not really sure about what

40
00:03:24,600 --> 00:03:32,180
they represent. So I’ll go ahead and I’ll add a Legend to my chart and I get it, it

41
00:03:32,180 --> 00:03:36,849
also picks up the file, I’ve got linear Trend line there as well. So I add that Legend

42
00:03:36,849 --> 00:03:43,910
into here and to be able to close that, there we go, I got away from it. And I’ve got

43
00:03:43,910 --> 00:03:47,349
my Legend telling me that the blue line is Revenue and the orange line is the Temperature.

44
00:03:47,349 --> 00:03:51,590
So line chart is pretty useful for that type of data.

45
00:03:51,590 --> 00:03:55,770
&gt;&gt; And to your point about maybe there’s not a correlation just looking at this graph

46
00:03:55,770 --> 00:04:00,930
or chart. Keep in mind that temperature is on a different scale than your prices and

47
00:04:00,930 --> 00:04:07,480
so it’s actually moving and a much narrower range. So that’s might be a good example

48
00:04:07,480 --> 00:04:11,560
where you might want to normalize your data and in some ways you can get a more accurate

49
00:04:11,560 --> 00:04:15,480
picture of what’s really going on. But more importantly there are statistics and they

50
00:04:15,480 --> 00:04:18,139
will automatically be able to tell you if there’s a relationship between the two.

51
00:04:18,139 --> 00:04:21,620
&gt;&gt; That’s exactly right and all of the things we said earlier on about the scaling of the

52
00:04:21,620 --> 00:04:25,210
data, it really becomes important when you start plotting multiple things on the same

53
00:04:25,210 --> 00:04:29,069
chart because of their different scales then visually they will look very different when

54
00:04:29,069 --> 00:04:34,669
you try and compare them. Okay so we&#39;ve got down revenue with over time.

55
00:04:34,669 --> 00:04:38,809
Let’s have a look at some other types of data that I might want to look at and it might

56
00:04:38,809 --> 00:04:45,279
be interested in the sales of lemonade and orangeade over time. So we might want to be

57
00:04:45,279 --> 00:04:53,249
look at that. So again we&#39;ll just select our dates and got selected there, oh we missed

58
00:04:53,249 --> 00:04:57,400
– no we got the first one, there we go. And I want my lemonade and my orangeade as

59
00:04:57,400 --> 00:05:00,590
well. So we&#39;ll just again just control and I’ll just select both of those columns like

60
00:05:00,590 --> 00:05:08,330
that. So now we were plotting over time my lemon and my orange sales and we&#39;ll use a

61
00:05:08,330 --> 00:05:13,389
different type of chart list and I think I’ll just insert a column chart and I want to go

62
00:05:13,389 --> 00:05:19,589
with one of these what we called a Clustered Column chart. And I’ll just move this one

63
00:05:19,589 --> 00:05:26,300
out the way and focus on this one I think and make that out a bit bigger. So I’m looking

64
00:05:26,300 --> 00:05:34,249
at Flavors over Time, remember that I’m in America so there’s no U in flavors. So

65
00:05:34,249 --> 00:05:40,110
we’ve got our Flavors over Time and I can see a different colored, I’ve got this whole,

66
00:05:40,110 --> 00:05:43,389
it automatically put Legend unfortunately, it picked up Series1 and Series2. So I probably

67
00:05:43,389 --> 00:05:46,999
want to do something about that but I can see that I’m getting a different color of

68
00:05:46,999 --> 00:05:51,120
columns for each of the different flavors. So let’s go and sort out that problem with

69
00:05:51,120 --> 00:05:56,889
the Legends that was by selecting our data, our Series1 data well I can see if I go and

70
00:05:56,889 --> 00:06:02,819
edit that is looking in column D, the column D is the lemon, so we&#39;ll just go and pick

71
00:06:02,819 --> 00:06:11,710
up the heading from column D and we&#39;ll do the same with Series2, and this one is from

72
00:06:11,710 --> 00:06:17,569
Column E, so we&#39;ll pick up the heading from the column E and that changes my Legend down

73
00:06:17,569 --> 00:06:21,619
here. So now I can clearly see that I’m comparing lemons and oranges, it is better

74
00:06:21,619 --> 00:06:25,669
than comparing apples and oranges I guess. So I’m looking at my Clustered Column chart,

75
00:06:25,669 --> 00:06:31,139
I’ve got my title in there and what I might want to do is I can see obviously I can compare

76
00:06:31,139 --> 00:06:35,979
side by side sales of lemons and oranges and that in itself is quite useful. But it could

77
00:06:35,979 --> 00:06:40,080
be difficult to judge total sales now you know looking in to it that way. If I wanted

78
00:06:40,080 --> 00:06:45,809
to view slightly differently what I can maybe do is go and change that chart type. So then

79
00:06:45,809 --> 00:06:51,719
instead of being a Clustered Column chart, I might make it a Stacked Column chart and

80
00:06:51,719 --> 00:06:56,419
what I actually get then are the height of the column indicates the total sales and it’s

81
00:06:56,419 --> 00:07:00,879
broken down into the different colors and I can actually hover over and see the different

82
00:07:00,879 --> 00:07:05,569
values within there. So even within one type of chart, a Column chart, there are different

83
00:07:05,569 --> 00:07:10,479
variants that show you different types of information that might be useful ways of working

84
00:07:10,479 --> 00:07:16,580
with your data. Okay, so from Column charts to something that

85
00:07:16,580 --> 00:07:21,469
perhaps a little more contentious I think one of the data scientists in the room might

86
00:07:21,469 --> 00:07:24,680
be rolling their eyes a little bit, but it’ll keep the marking people happy, we&#39;re going

87
00:07:24,680 --> 00:07:29,969
to select these average number of lemons and oranges or lemonades and orangeades that we&#39;ve

88
00:07:29,969 --> 00:07:36,729
sold and we&#39;re going to insert a Pie chart, just make it interesting when we get 3D and

89
00:07:36,729 --> 00:07:41,680
you can see that the image doesn’t look quite right, so the reason for that is up

90
00:07:41,680 --> 00:07:45,479
until now we&#39;ve been selecting columns of data and that’s we&#39;ve been charting and

91
00:07:45,479 --> 00:07:49,360
that’s kind of the default. Here we&#39;ve actually selected data along the way we would so I’ve

92
00:07:49,360 --> 00:07:55,139
selected a row of data. So the values are left to right in this case instead of top

93
00:07:55,139 --> 00:07:58,229
to bottom. So the chart doesn’t look right because it is trying to plot it as if it were

94
00:07:58,229 --> 00:08:03,189
columns and what we&#39;re going to do is just switch through one column over. And now we

95
00:08:03,189 --> 00:08:07,219
get a pie chart as we probably know it. I can do all the usual things, I could add a

96
00:08:07,219 --> 00:08:17,479
title in here, so we could add the title here that is Flavors Average Sales and I could

97
00:08:17,479 --> 00:08:22,509
go and I could change the series names and so on as well. You get the idea Pie charts

98
00:08:22,509 --> 00:08:26,909
are one of these things – I mean, coming as a statistician, it is not probably something

99
00:08:26,909 --> 00:08:31,289
that you use a lot. &gt;&gt; I almost never used that actually. &gt;&gt; Yeah, I think that’s the

100
00:08:31,289 --> 00:08:36,630
case in most kind of data science type roles, Pie charts are very limited. But they’re

101
00:08:36,630 --> 00:08:43,019
very commonly used in kind of marketing organization or where you want to show comparative sales

102
00:08:43,019 --> 00:08:46,779
typically it is the type of thing you do. So this is worth knowing that you can create

103
00:08:46,779 --> 00:08:52,089
those in Excel. We’re not going to worry too much about them any more in this course

104
00:08:52,089 --> 00:08:56,279
or, usually in any other courses in the series but it’s worth knowing that you can create

105
00:08:56,279 --> 00:08:59,060
those. &gt;&gt; Because you might be asked to do it. &gt;&gt; Indeed,

106
00:08:59,060 --> 00:09:04,339
yeah then you can show me that as a Pie chart as a common request. Okay we&#39;ve seen we got

107
00:09:04,339 --> 00:09:08,319
a Pie chart. Now we&#39;re going to look at something that is commonly used when we&#39;re looking at

108
00:09:08,319 --> 00:09:11,639
sort of data science where all the, that’s a Scatter Plot chart, where we&#39;re going to

109
00:09:11,639 --> 00:09:16,819
look at plots on a map that compare two different values. This is really commonly used to find

110
00:09:16,819 --> 00:09:20,630
correlation between different data values. So we&#39;re going to do very simply, we&#39;re going

111
00:09:20,630 --> 00:09:24,600
to do with our number of Leaflets that we&#39;ve distributed which we&#39;ve got here. So I’ll

112
00:09:24,600 --> 00:09:31,329
just go and select those and then the number of sales on a given day and so let’s go

113
00:09:31,329 --> 00:09:38,149
and select the number of sales. There are two columns we selected here and again we’re

114
00:09:38,149 --> 00:09:42,699
just going to go and insert a chart and the chart we&#39;re going to create is a Scatter chart.

115
00:09:42,699 --> 00:09:50,440
So let’s go ahead and choose the first of those and what we&#39;ve got here is our Sales

116
00:09:50,440 --> 00:10:01,240
vs Leaflets and the thing we&#39;re looking at, you can kind of see again this very few data

117
00:10:01,240 --> 00:10:04,410
points here, then that’s a really important thing to think about what we are working with

118
00:10:04,410 --> 00:10:10,160
a very limited sample dataset. Here it just make things manageable for the demos but even

119
00:10:10,160 --> 00:10:14,680
within that dataset we can see there’s kind of some structure here, just looks arguably

120
00:10:14,680 --> 00:10:19,589
like sort of tag on a line and we can see that data is sort of clustered around that

121
00:10:19,589 --> 00:10:25,300
line. So as you get more and more data to, at your dataset and plot, if you start to

122
00:10:25,300 --> 00:10:29,480
see structure like that. Then that is usually an indication of the some sort of relationship

123
00:10:29,480 --> 00:10:34,490
between the data, we were plotting it like that. So that’s a really useful chart when

124
00:10:34,490 --> 00:10:39,949
it comes to that associative type of question that we&#39;ve talked about earlier on.

125
00:10:39,949 --> 00:10:45,550
Alright well, I’ve looked at few chart types there, they are pretty common and you know

126
00:10:45,550 --> 00:10:49,610
use of sort of business analysis world, if you like or at least you know business users

127
00:10:49,610 --> 00:10:53,389
analyzing data. We were starting now with we looked at Scatter plot; we&#39;ll start getting

128
00:10:53,389 --> 00:10:57,759
into that more data science statistical sort of world, now that’s really your world.

129
00:10:57,759 --> 00:11:02,250
So what are the some other chart types as a data scientist or statistician I might be

130
00:11:02,250 --> 00:11:06,709
interested in looking at? &gt;&gt; There’s two, that are, I think they are particularly important

131
00:11:06,709 --> 00:11:10,120
when we start looking and visualizing data. The first is the Histogram, so why don’t

132
00:11:10,120 --> 00:11:14,149
I walk you through how we actually do that and we&#39;ll look at the history and looking

133
00:11:14,149 --> 00:11:19,769
at our Leaflets. Alright let’s take a look at this with the Leaflets data. So we have

134
00:11:19,769 --> 00:11:26,759
to highlight the data that we&#39;re interested in. We&#39;ll do that and then we go into Insert,

135
00:11:26,759 --> 00:11:33,329
select our chart type, Histogram... ta da.....it’s the easiest that eezy-peezy.... So this is

136
00:11:33,329 --> 00:11:38,529
actually broken into four bins as we call it but you can actually be more creative and

137
00:11:38,529 --> 00:11:43,089
get a little bit finer insight into what’s going on in your data and the reason why we

138
00:11:43,089 --> 00:11:46,990
do this is because we really want to understand the shape of the data. And this is important

139
00:11:46,990 --> 00:11:52,100
from a statistical perspective because almost all of the statistics we use assume a normal

140
00:11:52,100 --> 00:11:57,209
distribution. So with the Histogram we can get a finer look into what the shape of our

141
00:11:57,209 --> 00:12:00,800
data is. &gt;&gt; This is what I’m looking at, before we change the bins what I’m looking

142
00:12:00,800 --> 00:12:05,629
at here that if I understand this, bins really are ways of sort of lumping different values

143
00:12:05,629 --> 00:12:11,860
together onto categories if you like and I’ve got what I’m seeing here is four columns

144
00:12:11,860 --> 00:12:16,560
that show me the number of instances where the first one the value is between 75 and

145
00:12:16,560 --> 00:12:20,629
100, the second number between 100 and 125. So it’s basically just created those kinds

146
00:12:20,629 --> 00:12:25,420
of categorical ranges of values and set how many of each one there are. Is that right?

147
00:12:25,420 --> 00:12:28,670
&gt;&gt; Absolutely and what’s really cool is excel did it for you automatically. Back in

148
00:12:28,670 --> 00:12:33,470
the old days, we used to count up between 75 and 100, how many days did I have Leaflets

149
00:12:33,470 --> 00:12:39,000
like that and so it’s a lot of manual work. &gt;&gt; Okay&gt;&gt; Alright, so let’s take a look

150
00:12:39,000 --> 00:12:44,410
at how we might actually change the bin width and so to do that we&#39;re going to highlight

151
00:12:44,410 --> 00:12:50,040
the access that we care about which is our X axis here. And then we click Format Axis

152
00:12:50,040 --> 00:12:55,250
and up pops our options. Now you could do this by Bin width or I’m actually just kind

153
00:12:55,250 --> 00:13:00,389
of curious to do by the Number of bins and I’m going to increase this to ten. Is that

154
00:13:00,389 --> 00:13:05,009
seems like a pretty good distribution to give me a look at what’s going on, what the status

155
00:13:05,009 --> 00:13:11,959
actually shape like. So change it to ten and tada....it actually goes ahead and makes those

156
00:13:11,959 --> 00:13:17,019
changes for us and so we can see now that the bins range from 75 to 85, 85 to 95, and

157
00:13:17,019 --> 00:13:25,279
95 to 105 and so on. We actually don’t have any days where we actually had 155 to 165

158
00:13:25,279 --> 00:13:30,500
delivered or put out. So that is, that bin is empty, but you can see now what the shape

159
00:13:30,500 --> 00:13:34,370
of the state actually looks like. Alright we&#39;ll talk a little bit more about this and

160
00:13:34,370 --> 00:13:39,610
we talked a about that bit introduction statistics about and kind of what the shape might represent

161
00:13:39,610 --> 00:13:45,420
in terms of is this normal or not. We actually can start to get a feel for how close to normal

162
00:13:45,420 --> 00:13:52,290
our data actually is. So that is the Histogram. &gt;&gt; Fantastic, you said Histogram is one of

163
00:13:52,290 --> 00:13:56,639
two types of chart that I might be interested in from a kind of more statistical perspective,

164
00:13:56,639 --> 00:14:00,939
what was the other one you had in mind? &gt;&gt; It’s the Box-and-Whisker chart, a favorite of many

165
00:14:00,939 --> 00:14:05,850
because it’s got to great name for starters. &gt;&gt; Yeah. &gt;&gt; So let’s actually do this with

166
00:14:05,850 --> 00:14:11,180
and compare sales of lemonades and orangeades. So just like we’ve been doing in the past,

167
00:14:11,180 --> 00:14:14,269
I’m going to go ahead and I’m going to highlight these cells like I care about and

168
00:14:14,269 --> 00:14:19,860
it to include into my charts. So we got orangeade and lemonade, I’m going to go to Insert,

169
00:14:19,860 --> 00:14:25,180
I’m going to click on, it’s actually in the same area, the same option list as the

170
00:14:25,180 --> 00:14:31,660
Histogram, so you see Box and Whisker. So that is this the cool chart that we&#39;ve here.

171
00:14:31,660 --> 00:14:35,339
Now of course which one is which we don’t know, so we&#39;re going to click on the little

172
00:14:35,339 --> 00:14:41,980
plus sign and we&#39;re going to have it to a Legend, so we can see which of these two data

173
00:14:41,980 --> 00:14:47,129
points has captured by each of the colors, so lemon is the blue box and orange is the

174
00:14:47,129 --> 00:14:53,939
orange box. If that happened naturally it is kind of ironic and cool at the same time.

175
00:14:53,939 --> 00:15:00,649
So we have our Box and Whisker chart that shows us the range with which the data for

176
00:15:00,649 --> 00:15:04,810
each of these variables. So what you have here in the traditional box and whisker is

177
00:15:04,810 --> 00:15:10,970
the blue box in the middle that X is the average. &gt;&gt; Okay. &gt;&gt; The top of the box represents

178
00:15:10,970 --> 00:15:16,939
the third quartile and the bottom of the box represents the first quartile. So it is showing

179
00:15:16,939 --> 00:15:21,910
you where most of the data that should be clustering and then the whiskers show you

180
00:15:21,910 --> 00:15:29,529
out to the maximum value and the minimum value. &gt;&gt; I’m seeing almost the outliers I guess,

181
00:15:29,529 --> 00:15:33,529
so the ends of the whiskers. &gt;&gt; Yeah if they are considered outliers, they may or may not

182
00:15:33,529 --> 00:15:37,550
be depending on the shape and distribution of your data but the whole idea here is it’s

183
00:15:37,550 --> 00:15:43,410
giving you a visual representation in a slightly different way of where your data lies. &gt;&gt; So

184
00:15:43,410 --> 00:15:48,149
I can see that the average for my lemon is the blue one, it&#39;s kind of a shame it isn’t

185
00:15:48,149 --> 00:15:52,649
actually yellow, but the blue one represents the lemonade, I can see that the average is

186
00:15:52,649 --> 00:15:56,290
higher, than it is for the orange which we kind of knew because we&#39;ll lead to those values

187
00:15:56,290 --> 00:16:01,129
earlier on. But I can see a little bit of distribution around the average as well like

188
00:16:01,129 --> 00:16:05,959
I can see there’s slightly larger range of values for lemon and then there’s for

189
00:16:05,959 --> 00:16:09,749
orange. &gt;&gt; Right, so it’s a little bit more, there’s little more variability in our lemon

190
00:16:09,749 --> 00:16:14,639
data than in our orange data and what’s interesting about this particular chart is

191
00:16:14,639 --> 00:16:19,240
that you can see that there’s some overlap in the values across these two variables.

192
00:16:19,240 --> 00:16:26,079
So it’s not like they are distinctly different that they, in other words what can happen

193
00:16:26,079 --> 00:16:30,889
is it looks like there’s a difference, certainly the means tell us that there’s a different

194
00:16:30,889 --> 00:16:35,759
number of lemonades and orangeades being sold. But the real question is, is it statistically

195
00:16:35,759 --> 00:16:40,829
significant? Is it meaningful? And if these two boxes and their whiskers were completely

196
00:16:40,829 --> 00:16:44,670
separated, there’s no overlap, you probably wouldn’t even need to do this statistics

197
00:16:44,670 --> 00:16:49,920
to know that this is statistically different but there’s an overlap. So the question

198
00:16:49,920 --> 00:16:54,290
is what statistics can we use? Or should we run to actually understand if the difference

199
00:16:54,290 --> 00:17:00,420
between those two averages is statistically significant or meaningful? &gt;&gt; Right, so in

200
00:17:00,420 --> 00:17:05,520
this case it’s not like I’m selling a gazillion lemonades and five orangeades. They’re

201
00:17:05,520 --> 00:17:08,480
kind of close to each other but I’m going to have to apply some statistical methods

202
00:17:08,480 --> 00:17:10,880
to get deeper into what the differences are?&gt;&gt; Yes. &gt;&gt; Great.

