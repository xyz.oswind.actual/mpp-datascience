0
00:00:05,069 --> 00:00:09,250
Alright Liberty, so here I am in Excel. I’m sure this is a familiar tool to you, I’m

1
00:00:09,250 --> 00:00:12,469
sure you’ve seen this before; and let’s – let’s do one of the things we talked

2
00:00:12,469 --> 00:00:15,820
about earlier on. First of all we’re going to get that data; we need to ingest that data

3
00:00:15,820 --> 00:00:20,619
or acquire the data from somewhere. And I’m – this is actually just a comma-delimited

4
00:00:20,619 --> 00:00:23,949
file so I could just open it in Excel, but I wanted to take a look at these different

5
00:00:23,949 --> 00:00:28,199
ways of getting data from different places. So there’s a whole bunch of sources that

6
00:00:28,199 --> 00:00:32,870
I could use to go and find my data and get it in, and the point of that is that your

7
00:00:32,870 --> 00:00:37,410
data that you want to analyze could come from pretty much anywhere and Excel is going to

8
00:00:37,410 --> 00:00:39,660
let me get it. Okay so in this case I’m going to get it

9
00:00:39,660 --> 00:00:45,550
from a text file, so we’ll go and find our text file; it’s in my documents folder here,

10
00:00:45,550 --> 00:00:50,460
and its lemonade stand csv so it’s just a Comma-Separated Values file so we’ll go

11
00:00:50,460 --> 00:00:55,890
and import that. And here’s the first thing that I get, is a wizard that’s going to

12
00:00:55,890 --> 00:01:00,079
import the text. It says it’s delimited; well yes, we know that it is since it is a

13
00:01:00,079 --> 00:01:05,539
comma-delimited text file, and I get a little preview and I can see that the first row contains

14
00:01:05,539 --> 00:01:09,420
what&#39;s obviously the headers of the columns, so my data clearly has headers so I’m going

15
00:01:09,420 --> 00:01:12,700
to select the option there. And that’s going to be important; sometimes your data will

16
00:01:12,700 --> 00:01:15,700
sometimes your data won’t, and you don’t want to get mixed up with what’s actually

17
00:01:15,700 --> 00:01:21,610
a row of just headers as opposed to actual data that you want to work with. To bring

18
00:01:21,610 --> 00:01:25,250
it in, now the preview doesn’t look quite right; everything is kind of spread out at

19
00:01:25,250 --> 00:01:28,130
different lengths along here and that’s because it is assuming that we are delimiting

20
00:01:28,130 --> 00:01:32,509
with a tab and we’re not, we’re actually delimiting with a comma and now – there

21
00:01:32,509 --> 00:01:36,549
we go – that looks much nicer. One of the key things you’ll find is regardless of

22
00:01:36,549 --> 00:01:40,509
the format of the data you’re bringing in, usually when you want to analyze it, it’s

23
00:01:40,509 --> 00:01:44,930
going to end up in some sort of tabular structure, it usually ends up in nice neat rows and columns;

24
00:01:44,930 --> 00:01:48,600
it’s the way that we want to bring it in. So we’re bringing our text here in our rows

25
00:01:48,600 --> 00:01:52,850
and columns, and I can then deal with the different types of data that I’ve got. You

26
00:01:52,850 --> 00:01:57,060
see that the first row here is actually the date – the column heading tells me that’s

27
00:01:57,060 --> 00:02:01,070
the date – and I can see that it’s in what I would think of as the US format; I’m

28
00:02:01,070 --> 00:02:04,770
sure it’s familiar to you, to me that’s the 7th of January but to you that’s the

29
00:02:04,770 --> 00:02:09,289
1st of July so I can see that the dates are there. And I can actually mess around with

30
00:02:09,288 --> 00:02:13,269
that – I can go into this date option here and change the structure of the date depending

31
00:02:13,269 --> 00:02:18,269
on where I’m working; as it happens we are in the US so I will leave it that way, and

32
00:02:18,269 --> 00:02:22,389
I could do the same with the other data types along here. Actually, Excel is pretty smart

33
00:02:22,389 --> 00:02:25,420
at figuring out what things are so we’ll just go ahead and finish there.

34
00:02:25,420 --> 00:02:29,859
&gt;&gt; But that is actually why it’s so important to identify the first row as labels, because

35
00:02:29,859 --> 00:02:34,230
then Excel can figure out what your data is in each of those columns. &gt;&gt; Exactly yeah,

36
00:02:34,230 --> 00:02:37,199
and if I hadn’t done that, I mean you can see this for sure that the word Date is clearly

37
00:02:37,199 --> 00:02:41,919
not actually a date itself, so it wouldn’t have figured that out. So I’ve got my data

38
00:02:41,919 --> 00:02:45,269
in here and now I can see that I’ve got my nice tabular structure of data from the

39
00:02:45,269 --> 00:02:49,669
sales that Rosie’s been making over the summer over lemonade, and first thing I might

40
00:02:49,669 --> 00:02:54,180
want to do is, just to make it easier to work with, Excel has this really neat idea of just

41
00:02:54,180 --> 00:02:58,030
seeing these things as tables so I’m going to format it as a table. And it’s not just

42
00:02:58,030 --> 00:03:03,160
a question of the formatting; if I go into the formula and see that it has headers, not

43
00:03:03,160 --> 00:03:09,729
only do I get this nice kind of visual representation of the data, I also get some additional functionality.

44
00:03:09,729 --> 00:03:14,130
I get these drop-downs for a start that I can work with, so I can do things like filter

45
00:03:14,130 --> 00:03:19,630
on the price or sort the price; I could sort from smallest to largest and I’m getting

46
00:03:19,630 --> 00:03:23,130
the lowest-priced ones going in there and the highest price ones at the bottom, or I

47
00:03:23,130 --> 00:03:26,940
can do the other way around as well. And I probably actually in this case want to sort

48
00:03:26,940 --> 00:03:30,419
by the date so I’ll go ahead and sort by oldest to newest and get it back into that

49
00:03:30,419 --> 00:03:34,549
date order. So I’m getting some functionality now because this is a table, and under the

50
00:03:34,549 --> 00:03:38,900
covers Excel recognizes that this is a table so there’s some things that it’ll do later

51
00:03:38,900 --> 00:03:42,979
on when we’re going to write formulas, where because it’s in a table it’s easier to

52
00:03:42,979 --> 00:03:46,260
work with. Okay well, the next thing I might want to

53
00:03:46,260 --> 00:03:51,449
think about: the data is kind of interesting but there’s a few kind of obvious things

54
00:03:51,449 --> 00:03:56,010
missing here. I can see the number of lemonades and orangeades, I’ve got two different flavors

55
00:03:56,010 --> 00:04:00,030
that I sell. I can see the number of sales of those on the individual days. But what

56
00:04:00,030 --> 00:04:04,370
I haven’t got is the total number of sales for the day. So sometimes, you’ll get your

57
00:04:04,370 --> 00:04:09,570
data and it won’t be quite complete; sometimes you have to derive columns or create derived

58
00:04:09,570 --> 00:04:12,720
data from the data you’ve got, and in this case it’s quite easy. I’m just going to

59
00:04:12,720 --> 00:04:18,190
create a formula here that goes ahead and adds the number of lemons to the number of

60
00:04:18,190 --> 00:04:24,720
oranges, and again, because it’s a table I only have to write the formula in one cell

61
00:04:24,720 --> 00:04:28,950
and it automatically fills it out and I get my total sales for those two things here,

62
00:04:28,950 --> 00:04:32,980
so I can see that the number of drinks in total that were sold is listed here. Now because

63
00:04:32,980 --> 00:04:36,930
we’ve got the price that Rosie was selling these things at, there’s also another obvious

64
00:04:36,930 --> 00:04:40,900
thing I can do; at the end of the day, it’s all about the buck, right? [laughs] &gt;&gt; Right.

65
00:04:40,900 --> 00:04:46,600
&gt;&gt;So, yeah let’s stick in our revenue column here, and again we just put in a very simple

66
00:04:46,600 --> 00:04:51,570
formula here and this time it’s just going to be the number of sales multiplied by the

67
00:04:51,570 --> 00:04:56,560
price I sold them at and then that comes. And that looks pretty good, I get my revenue

68
00:04:56,560 --> 00:05:02,060
coming in there. Now, you’ll notice I’ve added those columns onto the end of the table,

69
00:05:02,060 --> 00:05:06,130
maybe I want to add one somewhere in the middle of the table so maybe I want to add one just

70
00:05:06,130 --> 00:05:10,320
in here. So I’ll go insert a new column and it automatically takes on the formatting

71
00:05:10,320 --> 00:05:14,460
of the table. Now what I might be interested in is the different days of the week that

72
00:05:14,460 --> 00:05:18,590
I’m working on, so a slightly more complicated formula this time just to kind of show that

73
00:05:18,590 --> 00:05:23,780
we can write more interesting formulae here. This time I want to display whatever it is

74
00:05:23,780 --> 00:05:28,090
as text – we use the text operator – and what I actually want to do is get the weekday

75
00:05:28,090 --> 00:05:35,470
and it’s going to be the weekday of whatever the date was that we sold the thing on, and

76
00:05:35,470 --> 00:05:40,760
I want to format that as a long date, or long day rather. The way I do that is four d’s,

77
00:05:40,760 --> 00:05:45,320
which is the formatting code for giving the full name of the day. So that looks a little

78
00:05:45,320 --> 00:05:49,000
bit of a more complex query – it’s not actually all that complicated a formula but

79
00:05:49,000 --> 00:05:52,600
it just shows it’s a little bit more complicated than just adding a couple of things together,

80
00:05:52,600 --> 00:05:57,310
and if I enter that sure enough I get my days of the week appearing along here. So I’m

81
00:05:57,310 --> 00:06:02,450
already enriching the data, I’m adding additional fields and additional columns that would make

82
00:06:02,450 --> 00:06:06,889
it easier for me to work with. What other things can I do with this? Well

83
00:06:06,889 --> 00:06:10,190
there’s a couple of issues with this data, you might notice. If you have a sort of really

84
00:06:10,190 --> 00:06:16,340
close look at this you’ll see a few interesting things that are happening. One is that I can

85
00:06:16,340 --> 00:06:24,180
see there is two rows that are the 6th of July there, and they seem to be exactly the

86
00:06:24,180 --> 00:06:28,680
same as one another. So, maybe Rosie’s entered the same data twice, maybe there’s been

87
00:06:28,680 --> 00:06:32,210
some sort of mistake and that’s pretty common that you’ll end up with duplicate entries

88
00:06:32,210 --> 00:06:36,139
in your data. And usually that’s a bad thing; if I total this up the totals aren’t going

89
00:06:36,139 --> 00:06:40,370
to be right because I’m double-counting the sales for that day, for example. So one

90
00:06:40,370 --> 00:06:43,480
of the things you’re quite often left to do is clean up your data, and what I might

91
00:06:43,480 --> 00:06:48,860
do is use whatever tool you want to use; there’s a variety of tools you could use to do this.

92
00:06:48,860 --> 00:06:53,840
One of the things that I can do in my Excel environment here is I can use the remove duplicates

93
00:06:53,840 --> 00:06:58,810
option here and it’s going to compare based on the columns that I select – If all of

94
00:06:58,810 --> 00:07:02,760
the columns are identical, then it must be a duplicate, so let’s go ahead and remove

95
00:07:02,760 --> 00:07:07,110
that. And sure enough, it says one duplicate value found and removed, 31 unique values

96
00:07:07,110 --> 00:07:11,300
remain; so we’re dealing with a pretty small data set here, there only was one duplicate

97
00:07:11,300 --> 00:07:16,250
which you can see that it’s gone now and I’ve actually got my dates on here. Now

98
00:07:16,250 --> 00:07:20,370
the other thing that’s interesting here is that you can see there’s a missing value

99
00:07:20,370 --> 00:07:23,730
here in the number of leaflets; this is the number of leaflets that Rosie distributed

100
00:07:23,730 --> 00:07:27,750
that day that tell people about her lemonade stand, and you can see that there’s a missing

101
00:07:27,750 --> 00:07:32,210
value there. Now if that missing value were something like the date, let’s say that

102
00:07:32,210 --> 00:07:37,320
this date here had been missing so we’d have a data that looked like that. Actually

103
00:07:37,320 --> 00:07:40,010
it’d have been quite easy for me to look at that and figure out; well look, I’ve

104
00:07:40,010 --> 00:07:43,280
got the first, the second, the third, the fourth, blank, the sixth, the seventh, the

105
00:07:43,280 --> 00:07:47,230
eighth, the ninth – in fact there’s a row for the eighth missing there as well – so

106
00:07:47,230 --> 00:07:52,389
you can see that there’s some missing data there. What I could do in this case is just

107
00:07:52,389 --> 00:07:56,980
interpolate that data; I can say well let’s just continue that series and go ahead and

108
00:07:56,980 --> 00:08:02,300
get the fifth appearing in there and it’s automatically filled that gap in for me. So

109
00:08:02,300 --> 00:08:06,199
interpolating missing data is a strategy when the data makes sense to do that, when it’s

110
00:08:06,199 --> 00:08:11,419
running in a series. Here however, that’s not really the case; here I can see that there’s

111
00:08:11,419 --> 00:08:15,669
a gap, it goes a hundred, a hundred, a hundred and fifteen, a hundred and fifty – there’s

112
00:08:15,669 --> 00:08:20,850
not really a pattern there that I could follow. I could try to find one, I could just leave

113
00:08:20,850 --> 00:08:25,190
it blank or I could put a zero in there as a placeholder, or I could find the minimum

114
00:08:25,190 --> 00:08:29,680
value or maximum value, or the other thing I could do is if I just select that entire

115
00:08:29,680 --> 00:08:35,579
column there, down here I can see the average is appearing down here in the status bar as

116
00:08:35,578 --> 00:08:40,339
120.5, but we haven’t gotten any fractions in here just full numbers. So what I might

117
00:08:40,339 --> 00:08:45,240
do here is just fill that in with 120, and I don’t know if that’s how many leaflets

118
00:08:45,240 --> 00:08:49,320
she gave out, but rather than having none it would be better just to have what she probably

119
00:08:49,320 --> 00:08:53,310
gave out, an average number of leaflets for that day. There’s a variety of ways that

120
00:08:53,310 --> 00:08:57,210
we could deal with missing values. &gt;&gt; You should know though, if you put a zero in there

121
00:08:57,210 --> 00:09:00,450
and you start running your descriptors when we start talking to the statists about the

122
00:09:00,450 --> 00:09:05,960
statistics, it will actually have an effect there, so think very consciously about putting

123
00:09:05,960 --> 00:09:09,279
another number in there that doesn’t make sense with relationship to the other parts

124
00:09:09,279 --> 00:09:13,900
of your data. &gt;&gt; Yeah, absolutely yeah. So a lot of it is knowing the data and understanding

125
00:09:13,900 --> 00:09:18,980
the context of the data, and like I said there’s all sorts of strategies for dealing with that.

126
00:09:18,980 --> 00:09:24,420
So we’ve dealt with a couple of things there; we’ve seen that we can sort the data using

127
00:09:24,420 --> 00:09:28,200
these things at the top. The other thing you might occasionally want to do is think about

128
00:09:28,200 --> 00:09:33,390
filtering the data, so for example, I might only be interested – you see that occasionally

129
00:09:33,390 --> 00:09:38,120
Rosie sold her drinks at the park or sometimes she went to the beach – and I might just

130
00:09:38,120 --> 00:09:42,940
want to show me the days when she went to the park. So I’ll just go and filter that

131
00:09:42,940 --> 00:09:46,080
and sure enough, we filter that list so now we’ve just got the days she spent at the

132
00:09:46,080 --> 00:09:50,750
park. And that can be a useful way to just quickly eyeball the data and, you know, see

133
00:09:50,750 --> 00:09:55,610
how many were at the park versus how many were at the beach or whatever. And the other

134
00:09:55,610 --> 00:10:00,270
way that I can filter is actually a couple of interesting techniques you can use in Excel

135
00:10:00,270 --> 00:10:04,580
here. I can add something called a slicer to my workbook and I’ll base this on the

136
00:10:04,580 --> 00:10:10,360
day, let’s see… I’m going to bring up my slicer here; I can go ahead and I can select

137
00:10:10,360 --> 00:10:14,370
individual days of the week that I want to filter on, so just show me the Sundays or

138
00:10:14,370 --> 00:10:19,060
just show me the Saturdays or whatever. So I can use that filter there in my slicer to

139
00:10:19,060 --> 00:10:23,490
go and further filter that data if you like. So there are two techniques, and then I can

140
00:10:23,490 --> 00:10:29,540
remove the filter from here and I can remove the filter just by clearing it and get all

141
00:10:29,540 --> 00:10:33,610
my data back. So that’s a couple of basic techniques for

142
00:10:33,610 --> 00:10:39,029
exploring data in Excel. When you come back, we’ll talk a little bit more about some

143
00:10:39,029 --> 00:10:43,260
of the techniques we can use to analyze that data and get some information from them.

