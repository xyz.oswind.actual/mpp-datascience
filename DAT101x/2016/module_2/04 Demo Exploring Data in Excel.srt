0
00:00:04,890 --> 00:00:08,870
Okay, so we’re back in Excel; here’s our Rosie’s lemonade sales, and we’ve got

1
00:00:08,870 --> 00:00:13,000
the individual daily records in here. But what I might want to do is aggregate some

2
00:00:13,000 --> 00:00:16,129
of that, so let’s go down to the bottom here – it’s usually a sensible place to

3
00:00:16,129 --> 00:00:20,990
put aggregations – and I’m going to use the count function in here, and I’m going

4
00:00:20,990 --> 00:00:27,429
to count the number of days on which Rosie actually sold drinks. So we’ll just select

5
00:00:27,429 --> 00:00:30,849
that column and you can see that I get this formula “equals count date”. This is another

6
00:00:30,849 --> 00:00:34,790
advantage of it being in a table; I don’t have to select the individual cells, it knows

7
00:00:34,790 --> 00:00:39,359
what the date column is so it’s just going to add up all the values in that column. And

8
00:00:39,359 --> 00:00:44,879
when I do that, I get this kind of weird result here it says formatted as a date, and that’s

9
00:00:44,879 --> 00:00:49,449
because in Excel, dates are really just numbers – they are just measured from a particular

10
00:00:49,449 --> 00:00:53,269
point in time. So what I’ve got there is a number, it’s just I previously told it

11
00:00:53,269 --> 00:00:57,420
I wanted to format everything in this column as a date. So I’m going to go up and I’m

12
00:00:57,420 --> 00:01:02,059
going to look at my home tab; I’m just going to pin this to the screen so I can see it

13
00:01:02,059 --> 00:01:08,200
a bit more clearly and maybe change it so I’m using a mouse, and rather than have

14
00:01:08,200 --> 00:01:11,579
that cell there – we’ve got the cell selected – rather than have that formatted as a date,

15
00:01:11,579 --> 00:01:16,470
I just want to go ahead and format that just as a general number. So I get my count appearing

16
00:01:16,470 --> 00:01:23,140
there, there were 31 days in which Rosie sold some lemonade, so that’s pretty good. Now

17
00:01:23,140 --> 00:01:26,450
this formatting thing is actually quite useful; I can do the same thing with something like

18
00:01:26,450 --> 00:01:31,600
my revenues, so I can come in here and I can go format that in dollars, and I begin to

19
00:01:31,600 --> 00:01:35,790
make some more sense of my data by adding some formatting to it as well. So we’ve

20
00:01:35,790 --> 00:01:38,520
counted up the number of days in which we’ve sold, we’ve formatted that as a number,

21
00:01:38,520 --> 00:01:42,890
we can see that there are 31 days of sales and we’ve got our sales there in dollars.

22
00:01:42,890 --> 00:01:48,390
Okay well, that’s the COUNT function. I might be interested in knowing – I can see

23
00:01:48,390 --> 00:01:52,560
that on different days I sold different numbers of lemonades and orangeades, and I might want

24
00:01:52,560 --> 00:01:57,200
to figure out on an average day how many of each of those did I sell. So let’s stick

25
00:01:57,200 --> 00:02:05,789
in an “= average” formula here, and we’ll go in and again just select that column, we’ll

26
00:02:05,789 --> 00:02:14,970
finish off the formula, and we can see that on average there were a 129.3 lemonades sold.

27
00:02:14,970 --> 00:02:19,150
Now I could enter the same formula for orange or I could just copy the formula along here,

28
00:02:19,150 --> 00:02:23,519
and it uses what it calls relative reference; it knows that the column next to lemon is

29
00:02:23,519 --> 00:02:28,860
orange so now the formula is “average orange”, and we get our figure there appearing – we

30
00:02:28,860 --> 00:02:33,690
get the average of number of oranges. So on average it looks like at this point, one of

31
00:02:33,690 --> 00:02:39,319
our comparative questions was lemonades versus oranges; on average, we tended to sell more

32
00:02:39,319 --> 00:02:42,420
lemonade than orangeade, we can say that at least.

33
00:02:42,420 --> 00:02:46,310
Okay, so what else might I want to do here? Well, we’ve got the temperature that was

34
00:02:46,310 --> 00:02:50,090
recorded that day so obviously this was during the summer, so hopefully the temperatures

35
00:02:50,090 --> 00:02:56,340
are reasonably high. We might want to find out what was the highest, the maximum temperature,

36
00:02:56,340 --> 00:03:02,190
so I might just want to “equals max” and go in there and just grab that maximum temperature;

37
00:03:02,190 --> 00:03:07,870
again, we’ll just finish the formula, and we can see that the maximum, the hottest day

38
00:03:07,870 --> 00:03:12,220
that we had during that period was 85 degrees. Given that I’m from Edinburgh that seems

39
00:03:12,220 --> 00:03:17,150
pretty hot to me, but that’s our maximum temperature. So I can go through and add these

40
00:03:17,150 --> 00:03:21,140
various types of aggregate functions, there are actually a whole bunch of them. And there’s

41
00:03:21,140 --> 00:03:25,040
a bit of a shortcut to this; because I formatted this as a table, when I select the cell underneath

42
00:03:25,040 --> 00:03:30,209
a column, you see a little drop-down here and if I click that I get a list of commonly

43
00:03:30,209 --> 00:03:32,650
used functions and you can see that there’s loads of them and there’s more. I could

44
00:03:32,650 --> 00:03:36,480
actually go in and drill for more functions here, so I can do things like here. I guess

45
00:03:36,480 --> 00:03:41,120
the most common one is “sum”; you usually want to total things up. I could go and sum

46
00:03:41,120 --> 00:03:46,569
the number of sales that we had in total over there, and I could then drag that along again

47
00:03:46,569 --> 00:03:51,439
– that’s relative referencing – so I can drag that along to the next column and

48
00:03:51,439 --> 00:03:56,720
I can get the sum of revenue that we made, which again we’ll format as dollars. And

49
00:03:56,720 --> 00:04:01,230
it looks to me that Rosie’s doing pretty well; if my middle school daughter was doing

50
00:04:01,230 --> 00:04:06,280
that over the summer I’d be pretty pleased I guess, so maybe our data’s not as realistic

51
00:04:06,280 --> 00:04:10,510
as we would like, but we can see that we’re getting some more information. We’re starting

52
00:04:10,510 --> 00:04:15,870
to not just clean the data, but actually explore the data and get some information out of it.

53
00:04:15,870 --> 00:04:20,790
Alright, well what else do you think might help me here? I’m going through adding formulae,

54
00:04:20,790 --> 00:04:24,380
what might be a useful thing do you think in terms of working with data? At the moment,

55
00:04:24,380 --> 00:04:30,400
it’s just a big spreadsheet of numbers and text. &gt;&gt; You know what I think would be great?

56
00:04:30,400 --> 00:04:35,010
Is there some way that we can look at this data more visually? Maybe it can help us identify

57
00:04:35,010 --> 00:04:39,650
outliers, or the day where she made the most money or the least money. &gt;&gt; Yeah, I think

58
00:04:39,650 --> 00:04:44,100
that’s a really good point, is that sometimes just looking at a list of numbers like this…

59
00:04:44,100 --> 00:04:47,080
it can be quite daunting, especially when there’s a huge number of them, it’s easier

60
00:04:47,080 --> 00:04:51,940
to see visualizations. So what we’ll maybe do is just select the cells in the revenue

61
00:04:51,940 --> 00:04:54,190
column; I’m going to select them individually because I don’t want to select the total,

62
00:04:54,190 --> 00:04:59,250
I just want the individual cells; and I’m going to apply some – what we call conditional

63
00:04:59,250 --> 00:05:03,200
formatting – here. There’s a number of different ways that I can apply formatting;

64
00:05:03,200 --> 00:05:07,230
in this case, I’m going to add something called data bars, and I could use different

65
00:05:07,230 --> 00:05:10,590
colors of those depending on your mood but I’m going to go with a nice bright yellow.

66
00:05:10,590 --> 00:05:15,970
And I can immediately see once I’ve done that, I get these comparative bars appearing.

67
00:05:15,970 --> 00:05:20,290
So those comparative questions we talked about, it’s very easy for me to look through those

68
00:05:20,290 --> 00:05:24,510
and see the days where I sold the most, I made the most revenue, and the days where

69
00:05:24,510 --> 00:05:29,950
I made the least because the bars are obviously lower. So I’m able to use that visualization

70
00:05:29,950 --> 00:05:35,050
to quickly let me see things that might be interesting in my data. And I can apply the

71
00:05:35,050 --> 00:05:39,120
same sort of technique to something like temperature here, so our temperature values – they’re

72
00:05:39,120 --> 00:05:44,570
all two-digit numbers so just looking at those is kind of… you know, it’s not immediately

73
00:05:44,570 --> 00:05:49,340
clear which days are hotter than others, and again we’ll apply some conditional formatting;

74
00:05:49,340 --> 00:05:54,080
I can look at my color scales and what I might do is go with something like – actually,

75
00:05:54,080 --> 00:05:58,850
that one I want to use there, which is the red-white color scheme here – and the white

76
00:05:58,850 --> 00:06:04,070
cells are the days that are the coolest, and the darker the red the hotter the days. So

77
00:06:04,070 --> 00:06:09,780
I get these nice gradient scales, and now I can see comparatives of temperature, and

78
00:06:09,780 --> 00:06:13,930
I could also possibly – there’s nothing definitely conclusive here, but I can start

79
00:06:13,930 --> 00:06:19,470
to develop a hypothesis that on days when it’s particularly hot like this 85 here,

80
00:06:19,470 --> 00:06:24,060
there’s quite a high bar there for the sales and the same there, and as the temperature

81
00:06:24,060 --> 00:06:28,470
cools… So maybe, there’s a relationship there. We need to do some further exploration

82
00:06:28,470 --> 00:06:33,250
to prove that, but I’ve started to get not just comparative but associative information

83
00:06:33,250 --> 00:06:37,210
out of that as well. Now the other thing we’ve got is the leaflets

84
00:06:37,210 --> 00:06:43,710
that were distributed; I said that our intrepid Rosie the salesperson is out there distributing

85
00:06:43,710 --> 00:06:49,270
leaflets, trying to drive people to come and buy her lemonade, and some days she does better

86
00:06:49,270 --> 00:06:52,220
than others. Some days she hands out more leaflets than other days, and I might want

87
00:06:52,220 --> 00:06:57,930
to score those days effectively. So we could do maybe an icon for this one, we could go

88
00:06:57,930 --> 00:07:02,400
and add an icon, and there’s a nice kind of rating thing here with stars so let’s

89
00:07:02,400 --> 00:07:08,080
go with that one. And I can see that on days where she handed out most of her flyers – so

90
00:07:08,080 --> 00:07:13,110
145, 150 – we get a gold star. Well done, Rosie, you handed out a whole bunch of leaflets.

91
00:07:13,110 --> 00:07:18,310
But on the days where perhaps, maybe she didn’t feel like walking around so much or maybe

92
00:07:18,310 --> 00:07:22,150
she didn’t have as many leaflets to hand out, then we get a different icon there; so

93
00:07:22,150 --> 00:07:27,240
again, that comparative visualization. And it just draws the eye, it makes it easier

94
00:07:27,240 --> 00:07:32,660
for me to see, oh look, on that day obviously there were more leaflets handed out. And then

95
00:07:32,660 --> 00:07:35,620
the other thing you mentioned was outliers, and that, as it turns out, that’s a really

96
00:07:35,620 --> 00:07:40,280
important thing to be able to find in a data set. Sometimes, just because you want to know

97
00:07:40,280 --> 00:07:45,210
what the days are where, you know, amazingly I sold much more. Is there a correlation of

98
00:07:45,210 --> 00:07:50,180
selling more things on a day, much more than any other day; or sometimes because the data

99
00:07:50,180 --> 00:07:54,740
might be wrong, outliers quite often indicate that you’ve added an extra zero or something.

100
00:07:54,740 --> 00:07:59,030
So to find those outliers, what I might want to do is look at my sales, and we’ll just

101
00:07:59,030 --> 00:08:04,680
go and select the individual cells here, and what I can do is I can go to my conditional

102
00:08:04,680 --> 00:08:08,520
formatting and I want to look at some top and bottom rules. So let’s look at the top

103
00:08:08,520 --> 00:08:13,830
ten percent of sales, and generally the top ten percent are going to be good things so

104
00:08:13,830 --> 00:08:18,270
we’ll go with a green fill with dark green text – green usually meaning go for traffic

105
00:08:18,270 --> 00:08:23,220
light, kind of thing. And it highlights in green these as the days when we sold more

106
00:08:23,220 --> 00:08:28,520
than other days or the top ten percent of sales days are highlighted in green here.

107
00:08:28,520 --> 00:08:32,430
And I can apply another conditional formatting to the same column – you’re not limited

108
00:08:32,429 --> 00:08:36,010
to just one. So I can apply another top and bottom rule and this time, we’ll go with

109
00:08:36,010 --> 00:08:41,870
the bottom ten percent and we’ll make that a light red fill with our red text, and I

110
00:08:41,870 --> 00:08:46,450
get them appearing as well. So you can see the days where I sold more and the days where

111
00:08:46,450 --> 00:08:51,400
I sold less; not just more or less but actually the top ten percent and the bottom ten percent.

112
00:08:51,400 --> 00:08:58,060
Now they all appear to be reasonably sensible values; 308 is not disproportionate. But if

113
00:08:58,060 --> 00:09:02,630
I had a value in here, like maybe I typed an extra zero in here, so instead of 204 I

114
00:09:02,630 --> 00:09:10,200
typed 2040, well that would immediately show up as a highlighted cell here and I can very

115
00:09:10,200 --> 00:09:13,360
quickly look at that and see that well, that’s obviously an outlier – there’s something

116
00:09:13,360 --> 00:09:18,720
wrong with that, maybe I need to go back and check sales for that day and fix that data.

117
00:09:18,720 --> 00:09:25,140
So, I can go ahead and just put that back to 204, and it’s all back to normal. So

118
00:09:25,140 --> 00:09:30,300
actually, what I’ve done there is overtyped a formula – I’m getting a little warning

119
00:09:30,300 --> 00:09:33,410
there so I’m just going to copy the formula down. But you get the idea; if there’s an

120
00:09:33,410 --> 00:09:37,450
outlier that’s completely wrong, then I want to know about it and that’s a pretty

121
00:09:37,450 --> 00:09:41,840
good way of finding it. There’s one other little visualization I

122
00:09:41,840 --> 00:09:46,130
want to look at at this point, and I want to look at something called Sparkline. &gt;&gt; Oh,

123
00:09:46,130 --> 00:09:50,710
one of my favorites. &gt;&gt; Sparkline, there we go yes. So Sparkline is like a miniature chart,

124
00:09:50,710 --> 00:09:54,840
a miniature graph if you like, applied to one column. So what I might want to do is

125
00:09:54,840 --> 00:10:00,280
I might want to go down to my price – see, the price varied over time, clearly Rosie

126
00:10:00,280 --> 00:10:04,270
was quite experimental with her pricing as the summer went on, so let’s find out what

127
00:10:04,270 --> 00:10:07,550
she did with the pricing. And I can go ahead – I’m just going to widen this column

128
00:10:07,550 --> 00:10:12,120
slightly just so we get a better view of the line that’s going to appear in here – and

129
00:10:12,120 --> 00:10:18,630
I’m going to insert the Sparkline. I’m going to insert a line, and the data range

130
00:10:18,630 --> 00:10:26,490
that I want to use is the price column so I’m just going to select that column and

131
00:10:26,490 --> 00:10:30,330
I get the line appearing here, and you can see that it’s fairly faint in there; I can

132
00:10:30,330 --> 00:10:34,490
do a little bit of formatting with that, I can do things like add in the high point and

133
00:10:34,490 --> 00:10:38,250
the low point so we can actually see them a bit more clearly on here. And I can see

134
00:10:38,250 --> 00:10:42,210
that there are really three phases of pricing: it started off fairly low and then it peaked,

135
00:10:42,210 --> 00:10:46,070
and then it went back down to sort of medium value towards the end of the summer here,

136
00:10:46,070 --> 00:10:51,440
so I’m starting to get information there about that price. Now I can do a similar sort

137
00:10:51,440 --> 00:10:57,340
of thing under sales, so let’s go with sales and again we’re just going to insert a sparkline

138
00:10:57,340 --> 00:11:01,370
but this time I’ll do it as a column just because I can, do something a bit different,

139
00:11:01,370 --> 00:11:05,890
and again we’ll select the data range so I’m just going to go and grab those individual

140
00:11:05,890 --> 00:11:13,370
sales, daily sales, and totals; and I get this column appearing and I can see that I

141
00:11:13,370 --> 00:11:17,880
get sort of peaks and troughs of when there were high sales and when there were low sales.

142
00:11:17,880 --> 00:11:23,650
So, it’s possible; again, it bears further investigation, but you can see that we’ve

143
00:11:23,650 --> 00:11:27,010
got… we started off pretty slow but then we peaked up – we’ve got this Manhattan

144
00:11:27,010 --> 00:11:30,860
skyline type thing happening in the beginning, these big tall buildings – and then there’s

145
00:11:30,860 --> 00:11:37,260
a noticeable dip, which might or might not correlate with the price going up, so it’s

146
00:11:37,260 --> 00:11:41,400
possible that there’s again some sort of associative information. I’m being very

147
00:11:41,400 --> 00:11:45,760
careful not to draw conclusions from this; I’m looking at the data and really just

148
00:11:45,760 --> 00:11:50,320
identifying some hypotheses that I might want to investigate further, but by creating these

149
00:11:50,320 --> 00:11:55,040
little visualizations, you can start to visually compare values in the data and perhaps see

150
00:11:55,040 --> 00:11:59,130
things that there might be an interesting correlation there that I can do a bit more

151
00:11:59,130 --> 00:12:00,070
investigation into.

