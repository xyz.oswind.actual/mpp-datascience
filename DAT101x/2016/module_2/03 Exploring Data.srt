0
00:00:04,970 --> 00:00:09,420
So Graham, that was a very great example; a very simple one but a good one of showing

1
00:00:09,420 --> 00:00:14,270
how we can acquire data, clean it and work with data in Excel. Excel is actually a very

2
00:00:14,270 --> 00:00:18,680
powerful tool; I don’t know if people really realize it but it really helps us explore

3
00:00:18,680 --> 00:00:23,150
what we have in our data. So, I think the next step is really understanding the kinds

4
00:00:23,150 --> 00:00:27,199
of questions and how we start to think about our data a little bit differently so we can

5
00:00:27,199 --> 00:00:32,310
actually do a more thorough exploration so we can learn something from what we have.

6
00:00:32,310 --> 00:00:36,200
&gt;&gt; Yeah, that’s absolutely right, I think that it’s all very well being able to take

7
00:00:36,200 --> 00:00:41,180
the data and clean it up and so on but if you haven’t got a goal in mind, if you don’t

8
00:00:41,180 --> 00:00:46,020
know what it is you’re looking for, then you really need a place to start. So let’s

9
00:00:46,020 --> 00:00:49,090
start looking at the types of questions we can ask of the data.

10
00:00:49,090 --> 00:00:54,940
So, here’s Rosie. She’s currently at the park as you can see, and she wants to start

11
00:00:54,940 --> 00:00:57,920
asking questions of the data that she’s gathered and the types of questions that she

12
00:00:57,920 --> 00:01:02,830
might want to ask. First of all, descriptive questions: she might want to know things like

13
00:01:02,830 --> 00:01:08,860
how many drinks did I sell, or how many lemonades, or – those types of things – how much

14
00:01:08,860 --> 00:01:12,880
revenue did I make is probably the first &gt;&gt; [laughs] &gt;&gt; question she wants to ask. So that’s

15
00:01:12,880 --> 00:01:18,900
descriptive type of information that she can get from the data. Then, going forward, she

16
00:01:18,900 --> 00:01:24,380
may want to do some associative analysis. Maybe she sees: is there a correlation between

17
00:01:24,380 --> 00:01:28,900
days when it’s hot and days when it’s cold? Is the temperature a factor in how many

18
00:01:28,900 --> 00:01:36,830
drinks I sell? Is there an association between temperature and sales in this instance? So,

19
00:01:36,830 --> 00:01:39,979
maybe she suspects there might be something there and she wants to see if the data bears

20
00:01:39,979 --> 00:01:46,580
out that hypothesis. And then, there might be comparative aspects to that; she might

21
00:01:46,580 --> 00:01:51,030
want to figure out, well, how many oranges do I sell versus lemonades? Maybe lemonades

22
00:01:51,030 --> 00:01:54,909
are more popular so I maybe need to make more lemonade and less orangeade, so maybe I want

23
00:01:54,909 --> 00:02:00,369
to look at a comparison of the sales of those two flavors. And then finally, if she’s

24
00:02:00,369 --> 00:02:05,500
really smart and she really wants to make a go of this, she might want to look at predictive

25
00:02:05,500 --> 00:02:10,340
aspects of her data. She might want to see, is there ways in which we can use these associations

26
00:02:10,340 --> 00:02:15,110
that we’ve identified and the comparisons that we’ve identified to somehow predict

27
00:02:15,110 --> 00:02:19,420
the number of lemonades that I might sell in any given day and that way I can be efficient

28
00:02:19,420 --> 00:02:23,450
about how much lemonade I make and how much I carry, how many leaflets I hand out, all

29
00:02:23,450 --> 00:02:29,620
that type of thing. So, there’s these kind of four general categories, I guess, of questions

30
00:02:29,620 --> 00:02:31,560
that we might want to ask of the data.

