0
00:00:05,080 --> 00:00:09,750
Okay, so we’ve had a chance to look at some of our data visually using these charts, and

1
00:00:09,750 --> 00:00:14,059
it’s great that we can see that kind of more traditional – you know, business-based

2
00:00:14,059 --> 00:00:18,250
charting alongside the more kind of statistical data science stuff; there is a bit of overlap

3
00:00:18,250 --> 00:00:24,500
into those two worlds. I guess what we’re going to look at now is more of that traditional

4
00:00:24,500 --> 00:00:28,220
business analytics – it’s quite a common thing in what we call Business Intelligence,

5
00:00:28,220 --> 00:00:33,379
or BI – to slice and dice your data, to work with what we call data cubes. So, we’ve

6
00:00:33,379 --> 00:00:36,960
been thinking of data kind of two-dimensionally up to this point, and now we’re going to

7
00:00:36,960 --> 00:00:40,159
start looking at it in three dimensions. Does that make sense? &gt;&gt; It does… it makes my

8
00:00:40,159 --> 00:00:43,839
head explode, but let’s do it. &gt;&gt; Okay, well let’s try and keep the number of dimensions

9
00:00:43,839 --> 00:00:47,860
down so our heads don’t explode, and let’s take a look at what we mean by this.

10
00:00:47,860 --> 00:00:53,860
So, here’s Rosie, and Rosie – as we’ve just said – has been looking at her data.

11
00:00:53,860 --> 00:00:57,049
Think of it – think of the way that we’ve been visualizing the data as being on a two-dimensional

12
00:00:57,049 --> 00:01:04,000
plane. So we’ve looked at things like the sum of our sales against an individual weekday,

13
00:01:04,000 --> 00:01:08,320
so I might want to plot – for each of the days of the week, I might count up how many

14
00:01:08,320 --> 00:01:13,390
sales were made on a Monday versus a Tuesday versus a Wednesday, and I might want to plot

15
00:01:13,390 --> 00:01:17,810
that value in there. And that’s a coordinate that is in this two-dimensional plane, so

16
00:01:17,810 --> 00:01:23,850
I can see that that represents the number of sales made on Thursdays, and in this case

17
00:01:23,850 --> 00:01:28,380
150 were made on Thursdays. So that’s quite easy to grasp; we’re looking at the thing

18
00:01:28,380 --> 00:01:33,690
effectively in two dimensions. But of course, data can go beyond more than two dimensions;

19
00:01:33,690 --> 00:01:37,960
there’s more than two things that I might want to slice my data by. So I might want

20
00:01:37,960 --> 00:01:41,970
to consider going into three-dimensional space with this; I might want to look at – well,

21
00:01:41,970 --> 00:01:46,480
not only the sum of the sales against weekday, but also factor in the location of where that

22
00:01:46,480 --> 00:01:51,770
sale was made, so there’s a third dimension to the data. So now when I plot the value,

23
00:01:51,770 --> 00:01:56,920
well, what does that plot represent? It’s a location in three dimensions of space if

24
00:01:56,920 --> 00:02:02,150
you think of it as being like that, so we have these kind of multiple axis that we’re

25
00:02:02,150 --> 00:02:06,850
looking at that data on, and I can see that what this represents is the number of sales

26
00:02:06,850 --> 00:02:11,319
that were made on a Wednesday at the park, because that’s where that corresponds to

27
00:02:11,319 --> 00:02:15,370
in that kind of cube space. And of course, we can go up the number of dimensions, we

28
00:02:15,370 --> 00:02:20,500
can – you know, it’s easy in a cartoon like this to visualize three dimensions because

29
00:02:20,500 --> 00:02:24,140
we’re used to thinking in three dimensions. Once we get into four dimensions, five dimensions,

30
00:02:24,140 --> 00:02:28,780
and up into multiple dimensions, it becomes more complicated, first of all, to represent,

31
00:02:28,780 --> 00:02:32,600
and secondly, just to think about, because you’re starting to deal with data against

32
00:02:32,600 --> 00:02:37,720
lots of different dimensions. But fundamentally, we can add all the dimensions we need and

33
00:02:37,720 --> 00:02:42,780
we can slice and dice the data around this point. So I guess, let’s take a look at

34
00:02:42,780 --> 00:02:44,830
how we might want to do that in Excel.

