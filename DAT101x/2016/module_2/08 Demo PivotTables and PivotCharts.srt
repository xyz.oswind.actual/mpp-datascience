0
00:00:05,089 --> 00:00:10,780
Alright, well let’s see how Excel can help us look at our data in multiple dimensions;

1
00:00:10,780 --> 00:00:14,070
and the way that we do that is to use this technology called “pivot tables”. So I’ve

2
00:00:14,070 --> 00:00:21,800
got my regular table here of my data, and I’m going to insert a pivot table, and to

3
00:00:21,800 --> 00:00:25,270
do that, I’m going to select the table or range. Remember, right at the beginning we

4
00:00:25,270 --> 00:00:29,380
formatted this as a table and I said it was more than just the formatting, it recognizes

5
00:00:29,380 --> 00:00:32,780
that actually as a table – it’s called “table 1” by default; I could change that,

6
00:00:32,780 --> 00:00:36,440
I don’t really need to do that right now. But it’s selecting that table, and I’m

7
00:00:36,440 --> 00:00:39,800
going to create a new pivot table and I’m going to put it into a new worksheet, so we’ll

8
00:00:39,800 --> 00:00:45,590
just go ahead and do that. So I get a new worksheet being created in my Excel workbook,

9
00:00:45,590 --> 00:00:49,420
and I’ve got this thing here called “pivot table 2” in this case, because I’ve obviously

10
00:00:49,420 --> 00:00:54,329
previously created one, and I’ve got my pivot table fields over here. These are the

11
00:00:54,329 --> 00:00:59,370
fields – these are the columns that I’ve got in that original table. And I said that

12
00:00:59,370 --> 00:01:04,879
I was interested in looking at the day and the location, and potentially the numbers

13
00:01:04,879 --> 00:01:11,490
of lemonade and orangeade sales on those days. So as I go and select those values in my field,

14
00:01:11,490 --> 00:01:15,479
it starts to build up this pivot table here and I can see that I can expand and collapse

15
00:01:15,479 --> 00:01:21,229
the days of the week, and within each of those days I get, for the beach and the park, the

16
00:01:21,229 --> 00:01:26,170
total sum of sales of lemonade and the total sum of sales of orangeade. And over here,

17
00:01:26,170 --> 00:01:29,630
I can see the different areas that I’ve got; so I can take, for example, location,

18
00:01:29,630 --> 00:01:35,959
and I could drag that across and put it over on the columns so that now it pivots the data

19
00:01:35,959 --> 00:01:38,409
around – as it’s where we get the name from – it pivots the data so I’ve got

20
00:01:38,409 --> 00:01:44,149
my days of the week down here in this first column, and then along here I can see for

21
00:01:44,149 --> 00:01:49,049
the beach, the sum of lemonades and the sum of orangeades; and for the park I can see

22
00:01:49,049 --> 00:01:52,829
the sum of lemonades and the sum of orangeades, and then I get the totals along the end as

23
00:01:52,829 --> 00:02:00,249
well. So, I can move the data around and I can pivot that data to be in different ways.

24
00:02:00,249 --> 00:02:05,849
Now, the other thing that I might want to do is visualize this data, and to do that

25
00:02:05,849 --> 00:02:12,510
I’m going to use something called a “pivot chart”. So I select the pivot chart, and

26
00:02:12,510 --> 00:02:14,969
I can choose the same types of chart we’ve been looking at up until now – I’m just

27
00:02:14,969 --> 00:02:19,819
going to go with a column chart in this case – and if I go and select that, sure enough,

28
00:02:19,819 --> 00:02:25,739
I see my days of the week along here and I’ve got the combination of location and value

29
00:02:25,739 --> 00:02:30,379
appearing here on my chart as well, so I can see that. That, to me, is a little bit more

30
00:02:30,379 --> 00:02:34,129
complicated than I want; I’ve got four colors to deal with and they represent two different

31
00:02:34,129 --> 00:02:40,400
things, so I might just want to move my location back again over to here, and now my chart

32
00:02:40,400 --> 00:02:44,069
shows me the days of the week along the bottom – which kind of makes sense – and then

33
00:02:44,069 --> 00:02:48,439
within those days I’ve got my totals for “beach” and for “park”, and the colors

34
00:02:48,439 --> 00:02:53,370
on the chart represent the orangeade and the lemonade so I can view my data slightly differently

35
00:02:53,370 --> 00:02:59,140
there and start to kind of tease out information about differences between sales of orangeade

36
00:02:59,140 --> 00:03:05,370
depending on the location of where I actually am on a given day. So that’s a quick intro

37
00:03:05,370 --> 00:03:09,659
to pivot tables and pivot charts; they’re very commonly used when you’re in a business

38
00:03:09,659 --> 00:03:15,189
context and you want to analyze data by multiple dimensions and move it around like that, and

39
00:03:15,189 --> 00:03:18,470
it can be useful just for exploring the data and finding those relationships.

