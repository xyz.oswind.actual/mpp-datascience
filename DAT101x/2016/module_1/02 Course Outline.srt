0
00:00:04,722 --> 00:00:07,742
Hopefully, you’re ready to get started, so

1
00:00:07,742 --> 00:00:11,700
let’s take a look at what we're gonna cover in this course.

2
00:00:11,700 --> 00:00:14,337
The first thing we're gonna do is an overview of the data

3
00:00:14,337 --> 00:00:16,070
science curriculum.

4
00:00:16,070 --> 00:00:18,800
This courses is slightly unusual in some ways in that it is

5
00:00:18,800 --> 00:00:20,230
an orientation for

6
00:00:20,230 --> 00:00:23,260
a whole bunch of other courses that are gonna come later on.

7
00:00:23,260 --> 00:00:25,130
Really, this course is about setting the scene for

8
00:00:25,130 --> 00:00:27,790
those later courses and helping you plan how you're going to

9
00:00:27,790 --> 00:00:29,260
progress through that curriculum.

10
00:00:29,260 --> 00:00:30,800
We'll give you an overview on how that works.

11
00:00:32,590 --> 00:00:34,790
Then just to make sure you're ready to start the rest of

12
00:00:34,790 --> 00:00:37,540
the courses in the curriculum, we need to make sure that you

13
00:00:37,540 --> 00:00:39,960
are up to speed on some of the fundamental things.

14
00:00:39,960 --> 00:00:43,090
We've got a module in here on some data science fundamentals.

15
00:00:43,090 --> 00:00:45,210
Really, just the basics of working with data and

16
00:00:45,210 --> 00:00:49,120
getting you up to speed with how to do that in tools like Excel.

17
00:00:49,120 --> 00:00:53,120
And then we've got a basic introduction to statistics.

18
00:00:53,120 --> 00:00:55,970
Statistics, you'll find as you progress through the curriculum,

19
00:00:55,970 --> 00:00:58,770
is a core skill that underpins a lot of

20
00:00:58,770 --> 00:01:01,810
the data science work that we're gonna do with all the different

21
00:01:01,810 --> 00:01:03,850
technologies that are covered in the curriculum.

22
00:01:03,850 --> 00:01:06,510
So we'll cover a basic introduction to statistics here.

23
00:01:06,510 --> 00:01:08,718
This won't be the last of the statistics that you learn and

24
00:01:08,718 --> 00:01:11,120
we're certainly not gonna cover everything you would ever need

25
00:01:11,120 --> 00:01:12,880
to know about statistics in this course.

26
00:01:12,880 --> 00:01:15,650
But we'll give you enough to get a flavor of the types of

27
00:01:15,650 --> 00:01:18,260
statistical operations that you have to do as you work

28
00:01:18,260 --> 00:01:19,940
with data throughout the rest of the curriculum.

29
00:01:22,170 --> 00:01:24,010
Finally, at the end of this course,

30
00:01:24,010 --> 00:01:25,400
there is a practical lab.

31
00:01:25,400 --> 00:01:27,370
It's a chance to put into practice some of

32
00:01:27,370 --> 00:01:29,295
those fundamental skills that we've talked about and

33
00:01:29,295 --> 00:01:31,500
you'll be able to use Excel and

34
00:01:31,500 --> 00:01:34,270
test your knowledge of what you've learned in this course.

35
00:01:34,270 --> 00:01:37,730
Now the lab in this course isn't graded, it's really just

36
00:01:37,730 --> 00:01:40,370
a chance for you to put things into practice and try out for

37
00:01:40,370 --> 00:01:43,620
yourself, just to check your own learning, if you like.

38
00:01:43,620 --> 00:01:45,770
But I do encourage you to complete the lab and

39
00:01:45,770 --> 00:01:46,950
to work through.

40
00:01:46,950 --> 00:01:49,100
If you do find that you have problems in answering some

41
00:01:49,100 --> 00:01:52,220
of the questions in the lab, dig back into the videos and

42
00:01:52,220 --> 00:01:54,570
look at the information that we've provided.

43
00:01:54,570 --> 00:01:56,620
Make sure that you're comfortable with everything

44
00:01:56,620 --> 00:01:59,420
that's in this course before you move onto the next course.

45
00:01:59,420 --> 00:02:02,460
So plenty to look forward to in this course.

46
00:02:02,460 --> 00:02:05,010
Now the next thing to do is to move to the next topic in

47
00:02:05,010 --> 00:02:07,580
this section where there's an important survey that we'd like

48
00:02:07,580 --> 00:02:09,285
you to fill in before you start the course.

49
00:02:09,285 --> 00:02:11,950
You'll find there's another survey at the end of the course

50
00:02:11,950 --> 00:02:14,110
and it's really useful if you can fill in that information and

51
00:02:14,110 --> 00:02:15,830
help us improve the course and

52
00:02:15,830 --> 00:02:18,220
improve the whole curriculum as we move forward.

53
00:02:18,220 --> 00:02:21,081
Once you've done that, you're ready to move on to the next

54
00:02:21,081 --> 00:02:24,188
section and start digging into learning about the curriculum.

