0
00:00:06,759 --> 00:00:13,680
If you are an aspiring data scientist, don&#39;t wait for a job or your study or work

1
00:00:13,680 --> 00:00:19,340
to start a project; you just go take some courses and what you would do is look at all

2
00:00:19,340 --> 00:00:26,449
the available data sets out there so there’s data.gov where government has opened hundreds

3
00:00:26,449 --> 00:00:31,840
of datasets out there which you could use and start playing around with data, create

4
00:00:31,840 --> 00:00:39,079
those models and can write blogs and also take part in various competitions which lot

5
00:00:39,079 --> 00:00:46,559
of people, big companies are doing about machine learning, so take part on it, get noticed

6
00:00:46,559 --> 00:00:51,120
by the big companies and that&#39;s how you will succeed. &gt;&gt; So, I think just starting out

7
00:00:51,120 --> 00:00:56,030
in your career the advice I’d have is learn from other people, work on projects that you

8
00:00:56,030 --> 00:01:00,489
are excited about so that you actually want to solve that problem, you are curious about

9
00:01:00,489 --> 00:01:05,479
that problem and you can learn from other individuals. So I would say work on projects

10
00:01:05,479 --> 00:01:10,790
with people that inspire you, learn from other individuals and really seek to solve problems;

11
00:01:10,790 --> 00:01:15,350
if you seek to solve problems, build things yourself. You’ll really, naturally come

12
00:01:15,350 --> 00:01:20,370
to learn that, just because it&#39;s fun and working with other people and that would get you to

13
00:01:20,370 --> 00:01:24,040
where you want to be as a data scientist. &gt;&gt; Find an area that you are passion about

14
00:01:24,040 --> 00:01:28,800
and try to use data science to make a difference in that field. &gt;&gt; During the course work,

15
00:01:28,800 --> 00:01:34,670
during your work; if you came across a problem and found a clever way to solve it, share

16
00:01:34,670 --> 00:01:39,030
it with the community, put it out there. Your experience – your work is your currency,

17
00:01:39,030 --> 00:01:45,700
share it with the community. It just makes you a better data scientist, a better worker.

18
00:01:45,700 --> 00:01:52,240
&gt;&gt; So one of the most important things to learn is the methodology of data science and

19
00:01:52,240 --> 00:01:58,610
there is a very clear winner in what methodologies work and what one&#39;s don&#39;t and the reason you

20
00:01:58,610 --> 00:02:06,780
need to know those kind of things is there is no way to determine a right answer from

21
00:02:06,780 --> 00:02:13,269
a lucky guess. So, if I go off and I play the lottery once and I happen to win it, I&#39;ve

22
00:02:13,269 --> 00:02:19,230
got strong evidence that I must have ESP yet of course that sounds nonsense because it

23
00:02:19,230 --> 00:02:25,999
is nonsense and if you don&#39;t have a good understanding of the methodologies and what the tools are

24
00:02:25,999 --> 00:02:31,969
capable of you come up with nonsense answers like that. So one of the things you really

25
00:02:31,969 --> 00:02:37,969
want to focus on is having the good process that’s repeatable and that leads to good

26
00:02:37,969 --> 00:02:44,310
answers most of the time. There is nothing at will make it all of the time but you can

27
00:02:44,310 --> 00:02:47,030
get it to the point where the odds are in your favour.

