0
00:00:07,359 --> 00:00:11,530
What kind of person succeeds as a data scientist? You have to be a little bit

1
00:00:11,530 --> 00:00:17,800
crazy I think. You have to actually like data. &gt;&gt; So you need to love the data, dream about

2
00:00:17,800 --> 00:00:24,750
the data – that’s very important piece and once you do that, everything just falls

3
00:00:24,750 --> 00:00:30,890
in place. Like curiosity is an important thing, you would want to be curious to see what is

4
00:00:30,890 --> 00:00:36,680
it that my users are looking for? How do I improve the product quality by looking at

5
00:00:36,680 --> 00:00:44,310
just a raw CSV file or GSL file and then you put that data into visualization, so imagination

6
00:00:44,310 --> 00:00:48,950
is the another thing which is an important aspect that you would need. &gt;&gt; So you need

7
00:00:48,950 --> 00:00:54,760
to be genuinely curious about the data and how that that is going to be used by the business.

8
00:00:54,760 --> 00:01:00,280
So that is a key trait; another is being able to work with many individuals. So you need

9
00:01:00,280 --> 00:01:06,060
to be curious, you need to be collaborative, you also need to be a problem solver to take

10
00:01:06,060 --> 00:01:11,060
different techniques , tips and tricks that you&#39;ve learned along the way, combine them

11
00:01:11,060 --> 00:01:15,280
together to figure how you can actually solve a problem for the business. &gt;&gt; You have to

12
00:01:15,280 --> 00:01:21,450
be a quick learner. You have to be smart and inquisitive; you have to be able to know how

13
00:01:21,450 --> 00:01:26,150
to ask lots of relevant questions, get the answers that you are looking for. You should

14
00:01:26,150 --> 00:01:31,250
be able to adopt and adapt to new technologies because this is an ever-growing field with

15
00:01:31,250 --> 00:01:36,299
new methodologies coming up every day, new and faster algorithms so you have to keep

16
00:01:36,299 --> 00:01:42,220
up, be interested in finding about what’s out there current, just learn new technologies.

17
00:01:42,220 --> 00:01:49,049
&gt;&gt; I really enjoy just analysing data, to the point where I’ll sometimes do very strange

18
00:01:49,049 --> 00:01:55,100
experiments – like, one I’ve got going on right now is “Is band 1 or band 2 a better

19
00:01:55,100 --> 00:02:01,590
tracking device for steps as tracked against a telephone and a Fitbit”, so which is the

20
00:02:01,590 --> 00:02:09,159
best device for tracking my steps? And what you’ll find is I often do things like that.

21
00:02:09,158 --> 00:02:14,890
So I do a lot of very strange experiments just for fun because I think they’re fun.

22
00:02:14,890 --> 00:02:21,390
It&#39;s really interesting to go and do these kind of things. If you find it fun to analyse

23
00:02:21,390 --> 00:02:24,060
a bunch of numbers then you’ll make a good data scientist.

