0
00:00:00,729 --> 00:00:04,486
[MUSIC]

1
00:00:04,486 --> 00:00:07,783
So now it's time to turn our attention to the curriculum as

2
00:00:07,783 --> 00:00:08,830
a whole.

3
00:00:08,830 --> 00:00:10,723
Obviously, this is the orientation class.

4
00:00:10,723 --> 00:00:13,235
We're designing this to help you learn a little bit about

5
00:00:13,235 --> 00:00:14,073
the curriculum and

6
00:00:14,073 --> 00:00:16,128
get ready to start with the rest of the course.

7
00:00:16,128 --> 00:00:18,975
So it's probably time we looked at what's actually involved in

8
00:00:18,975 --> 00:00:20,831
the curriculum and what the courses are.

9
00:00:20,831 --> 00:00:21,799
So let's take a look.

10
00:00:24,131 --> 00:00:27,028
So we have a curriculum, you are the start of that just now.

11
00:00:27,028 --> 00:00:30,044
And you have a journey ahead of you to get through to learn all

12
00:00:30,044 --> 00:00:32,712
of the skills that are involved in the data science.

13
00:00:32,712 --> 00:00:33,930
And what we've done is,

14
00:00:33,930 --> 00:00:36,660
we've split that curriculum into four units.

15
00:00:36,660 --> 00:00:39,580
So Unit 1, we're gonna cover the basic fundamental

16
00:00:39,580 --> 00:00:41,170
skills that you need to work with data.

17
00:00:42,340 --> 00:00:45,510
Unit 2, we're gonna look at some core data science concepts and

18
00:00:45,510 --> 00:00:46,540
principles, so

19
00:00:46,540 --> 00:00:49,190
you can learn how to engage in a data science project.

20
00:00:50,910 --> 00:00:52,711
Unit 3, we're gonna look at applied data science.

21
00:00:52,711 --> 00:00:54,739
We're gonna look at taking some of those concepts and

22
00:00:54,739 --> 00:00:56,055
principles that we've learned.

23
00:00:56,055 --> 00:00:58,999
And actually applying them through real scenarios and

24
00:00:58,999 --> 00:01:00,474
building real solutions.

25
00:01:00,474 --> 00:01:03,245
And in Unit 4, you've got the chance to put together all

26
00:01:03,245 --> 00:01:06,209
of the things you've learned and engage in a final project.

27
00:01:06,209 --> 00:01:09,073
Where you're gonna have to solve some problems on your own, and

28
00:01:09,073 --> 00:01:11,900
you're gonna have to submit your solution to those problems.

29
00:01:13,210 --> 00:01:13,900
Well, let's dive in and

30
00:01:13,900 --> 00:01:16,080
take a look at Unit 1 in a bit more detail.

31
00:01:17,560 --> 00:01:20,410
So Unit 1, there are several skills that you're gonna learn.

32
00:01:20,410 --> 00:01:23,820
You are gonna, first of all, get started with the curriculum.

33
00:01:23,820 --> 00:01:25,860
Then you're gonna learn to query relational data.

34
00:01:25,860 --> 00:01:29,200
That's a core skill for working with data for data scientists,

35
00:01:29,200 --> 00:01:31,400
and for any form of data professional or developer.

36
00:01:31,400 --> 00:01:33,410
So we're gonna look at how to query relational data.

37
00:01:33,410 --> 00:01:36,380
And then we're gonna look at some skills for analyzing and

38
00:01:36,380 --> 00:01:37,410
visualizing data.

39
00:01:37,410 --> 00:01:40,180
So get some of those techniques and that learn some of those

40
00:01:40,180 --> 00:01:43,400
technologies for working with data to analyze and visualize.

41
00:01:43,400 --> 00:01:45,990
And then finally we're gonna understand statistics.

42
00:01:45,990 --> 00:01:48,563
We're gonna give you a chance to get to grips with some of

43
00:01:48,563 --> 00:01:51,470
the statistics that you need in order to work

44
00:01:51,470 --> 00:01:53,290
with data in a data science environment.

45
00:01:54,780 --> 00:01:56,620
So for the getting started skill,

46
00:01:56,620 --> 00:01:59,490
well the good news is you're actually already in that course.

47
00:01:59,490 --> 00:02:01,780
We have an orientation course, this is it.

48
00:02:01,780 --> 00:02:04,060
And that's gonna help you get started and ready to go.

49
00:02:04,060 --> 00:02:08,120
Now the next course you're gonna have to look at in curriculum

50
00:02:08,120 --> 00:02:10,600
is one that helps you learn to query relational data.

51
00:02:10,600 --> 00:02:13,593
And the course that we're offering is a Querying Data with

52
00:02:13,593 --> 00:02:14,883
Transact-SQL course.

53
00:02:14,883 --> 00:02:18,126
And that's a course where you can learn to work with

54
00:02:18,126 --> 00:02:20,777
Transact-SQL which is the SQL variant.

55
00:02:20,777 --> 00:02:24,331
The SQL language that's used with Microsoft SQL Server and

56
00:02:24,331 --> 00:02:27,340
Azure SQL Database and Azure SQL Data Wearhouse.

57
00:02:27,340 --> 00:02:30,934
So those are SQL based databases on the Microsoft platform.

58
00:02:30,934 --> 00:02:32,142
But it's a fairly generic language.

59
00:02:32,142 --> 00:02:34,832
So you'll learn the skills that you would need to use SQL with

60
00:02:34,832 --> 00:02:36,589
pretty much any relational database.

61
00:02:38,790 --> 00:02:40,320
Then something interesting happens.

62
00:02:40,320 --> 00:02:42,990
We have a choice of two courses for you.

63
00:02:42,990 --> 00:02:45,150
The skill that you're gonna learn is how to analyze and

64
00:02:45,150 --> 00:02:46,290
visualize data.

65
00:02:46,290 --> 00:02:49,364
But there are two different courses that we're providing

66
00:02:49,364 --> 00:02:51,253
that you can use to meet that skill.

67
00:02:51,253 --> 00:02:53,574
One is Analyzing and Visualizing Data with Excel.

68
00:02:53,574 --> 00:02:57,362
Which is very much focused on using Excel to create models of

69
00:02:57,362 --> 00:02:58,119
the data and

70
00:02:58,119 --> 00:03:02,146
then visualize the data within the Excel environment itself.

71
00:03:02,146 --> 00:03:03,987
And then the other is Analyzing and

72
00:03:03,987 --> 00:03:05,773
Visualizing Data with Power BI.

73
00:03:05,773 --> 00:03:09,157
And Power BI are a wide base service from Microsoft, for

74
00:03:09,157 --> 00:03:11,965
taking data from lots of different sources and

75
00:03:11,965 --> 00:03:16,035
visualising and creating reports and dashboards through there.

76
00:03:16,035 --> 00:03:18,852
Now in order to complete the curriculum, you choose one or

77
00:03:18,852 --> 00:03:21,267
other of this courses, you can do either of them,

78
00:03:21,267 --> 00:03:22,602
you don't have to do both.

79
00:03:22,602 --> 00:03:26,227
However, if you are planning to work seriously with data and

80
00:03:26,227 --> 00:03:30,152
to be analyzing and visualizing data on the Microsoft platform.

81
00:03:30,152 --> 00:03:32,887
You can choose to do both courses if you wish, and

82
00:03:32,887 --> 00:03:34,660
we do recommend that.

83
00:03:34,660 --> 00:03:37,150
You might want to choose the option of doing both courses

84
00:03:37,150 --> 00:03:39,200
in order to get to grips with both technologies.

85
00:03:39,200 --> 00:03:40,540
But to complete your curriculum,

86
00:03:40,540 --> 00:03:41,829
you only need to choose one of those.

87
00:03:44,250 --> 00:03:46,390
And then we have a course on Statistical Thinking for

88
00:03:46,390 --> 00:03:47,690
Data Science.

89
00:03:47,690 --> 00:03:50,340
And that's a slightly different course from the others,

90
00:03:50,340 --> 00:03:52,740
in that it's offered by an external partner.

91
00:03:52,740 --> 00:03:56,110
It's offered by ColumbiaX, on the edX platform.

92
00:03:56,110 --> 00:03:57,130
So that course is there for

93
00:03:57,130 --> 00:04:00,400
you to take, to understand statistics, and to learn how to

94
00:04:00,400 --> 00:04:03,640
apply statistical thinking to data science project.

95
00:04:04,950 --> 00:04:07,700
Now once you've completed all of those courses in Unit 1,

96
00:04:07,700 --> 00:04:09,325
you're ready to move on to Unit 2.

97
00:04:10,560 --> 00:04:13,895
So let's take a look at what's in Unit 2.

98
00:04:13,895 --> 00:04:17,021
Well Unit 2 is our Core Data Science unit.

99
00:04:17,021 --> 00:04:20,147
And in this unit you're gonna learn how to explore data using

100
00:04:20,147 --> 00:04:24,210
code, using the languages that data scientists prefer to use.

101
00:04:24,210 --> 00:04:27,330
You're gonna learn some core data science concepts.

102
00:04:27,330 --> 00:04:32,250
So the basics of a process for performing a data

103
00:04:32,250 --> 00:04:35,400
science project and for working with data and preparing it.

104
00:04:35,400 --> 00:04:37,061
And then you're gonna learn a little bit about

105
00:04:37,061 --> 00:04:37,790
machine learning.

106
00:04:37,790 --> 00:04:39,249
So we're gonna get the concepts and

107
00:04:39,249 --> 00:04:40,664
principles of machine learning.

108
00:04:40,664 --> 00:04:44,280
So that you're ready to build predictive solutions using

109
00:04:44,280 --> 00:04:46,058
a variety of technologies.

110
00:04:46,058 --> 00:04:48,088
Now the courses that are in this unit, for

111
00:04:48,088 --> 00:04:50,640
exploring data with code again, you have a choice.

112
00:04:50,640 --> 00:04:52,980
There's a couple of course you can choose from.

113
00:04:52,980 --> 00:04:55,310
One is an Introduction to R for Data Science.

114
00:04:55,310 --> 00:04:58,300
R is a statistical language that's used by lot of data

115
00:04:58,300 --> 00:05:00,900
scientists to manipulate and work with data.

116
00:05:00,900 --> 00:05:03,357
And the other is in introduction of Python for data science, and

117
00:05:03,357 --> 00:05:06,610
Python's also a very popular language for working with data.

118
00:05:06,610 --> 00:05:09,657
It's by both developers and by data scientists.

119
00:05:09,657 --> 00:05:11,866
You can choose one or other of those courses,

120
00:05:11,866 --> 00:05:13,202
if you like you can do both,

121
00:05:13,202 --> 00:05:16,900
you only need to complete one to complete the curriculum.

122
00:05:16,900 --> 00:05:18,860
An important thing about these courses is,

123
00:05:18,860 --> 00:05:21,580
we're not at this point teaching you everything you'll ever need

124
00:05:21,580 --> 00:05:22,980
to know about R or Python.

125
00:05:22,980 --> 00:05:24,793
This is not a comprehensive end-to-end,

126
00:05:24,793 --> 00:05:25,921
learn everything course.

127
00:05:25,921 --> 00:05:28,982
It's gonna give you enough knowledge of working with R or

128
00:05:28,982 --> 00:05:31,980
working with Python to do the basic data operations that

129
00:05:31,980 --> 00:05:32,570
you need.

130
00:05:32,570 --> 00:05:35,952
In order to get started looking at data science concepts.

131
00:05:35,952 --> 00:05:37,597
So we'll return to these languages later in

132
00:05:37,597 --> 00:05:38,267
the curriculum.

133
00:05:38,267 --> 00:05:41,363
But for the moment, this is just an introduction,

134
00:05:41,363 --> 00:05:44,675
just a primer to get you started with those languages.

135
00:05:44,675 --> 00:05:48,140
After you've got those basics of the languages under your belt,

136
00:05:48,140 --> 00:05:50,345
we have a Data Science Essentials class.

137
00:05:50,345 --> 00:05:52,505
And this is really where we get into the core of the data

138
00:05:52,505 --> 00:05:53,427
science curriculum.

139
00:05:53,427 --> 00:05:55,670
And we're gonna look at the processes involved for

140
00:05:55,670 --> 00:05:57,662
working with data in a data science project.

141
00:05:57,662 --> 00:06:01,259
We're gonna look at some of the foundational principles for

142
00:06:01,259 --> 00:06:04,516
working with data, for applying statistics to data.

143
00:06:04,516 --> 00:06:08,250
And for cleaning data and preparing data for modeling.

144
00:06:08,250 --> 00:06:10,160
So a lot of skills that are involved in there that will use

145
00:06:10,160 --> 00:06:11,140
some of your R or

146
00:06:11,140 --> 00:06:13,490
Python skills that you've learned up til this point.

147
00:06:13,490 --> 00:06:15,630
Some of the visualization techniques that you've learned

148
00:06:15,630 --> 00:06:16,730
previously.

149
00:06:16,730 --> 00:06:19,710
And also introduce Azure Machine Learning,

150
00:06:19,710 --> 00:06:21,885
which is a platform for doing machine learning in the cloud.

151
00:06:21,885 --> 00:06:25,460
And once you've completed the Data Science Essentials class,

152
00:06:25,460 --> 00:06:27,240
you're ready to move on to the next one,

153
00:06:27,240 --> 00:06:29,340
which is Principles of Machine Learning.

154
00:06:29,340 --> 00:06:32,400
So we'll take those ideas that we've learned to prepare data

155
00:06:32,400 --> 00:06:33,250
for modelling, and

156
00:06:33,250 --> 00:06:36,630
then we'll actually start building, evaluating, refining,

157
00:06:36,630 --> 00:06:38,860
and validating machine learning models.

158
00:06:38,860 --> 00:06:42,124
And we'll create those models again using R, Python and

159
00:06:42,124 --> 00:06:43,627
Azure Machine Learning.

160
00:06:43,627 --> 00:06:45,760
So there's a whole bunch of technologies we'll apply.

161
00:06:45,760 --> 00:06:48,791
But the core thing that you're going to learn here, the core

162
00:06:48,791 --> 00:06:51,537
skill, is the techniques for creating those models.

163
00:06:51,537 --> 00:06:54,320
And then evaluating and improving those models.

164
00:06:55,520 --> 00:06:57,059
And once you've learned that,

165
00:06:57,059 --> 00:06:59,057
you're ready to move on to the next unit.

166
00:07:01,742 --> 00:07:04,250
So Unit 3 is about applied data science.

167
00:07:04,250 --> 00:07:07,620
It's about applying some of the principles we've learned so far.

168
00:07:07,620 --> 00:07:10,150
And the first thing we'll look at is using code to manipulate

169
00:07:10,150 --> 00:07:10,820
and model data.

170
00:07:10,820 --> 00:07:12,510
And then we'll look at

171
00:07:12,510 --> 00:07:14,019
developing intelligent solutions.

172
00:07:15,982 --> 00:07:18,628
And for using code, we're gonna build on that knowledge we've

173
00:07:18,628 --> 00:07:21,084
been building up all the way through the curriculum so far.

174
00:07:21,084 --> 00:07:23,581
We're gonna return to R or to Python.

175
00:07:23,581 --> 00:07:25,807
And we're gonna focus on applying some of those

176
00:07:25,807 --> 00:07:28,146
principles we've learned about data science and

177
00:07:28,146 --> 00:07:30,530
machine learning using R or using Python.

178
00:07:30,530 --> 00:07:33,669
So, again, you can choose one or other of those classes.

179
00:07:35,580 --> 00:07:36,740
Then moving on from there,

180
00:07:36,740 --> 00:07:39,340
we have a choice of three classes that you can take.

181
00:07:39,340 --> 00:07:41,100
You can take anyone of these three.

182
00:07:41,100 --> 00:07:43,210
There is an Applied Machine Learning class which

183
00:07:43,210 --> 00:07:45,700
really continues that discovery of using R,

184
00:07:45,700 --> 00:07:48,040
Python and Azure Machine Learning.

185
00:07:48,040 --> 00:07:50,960
And starts to bring in some common scenarios and

186
00:07:50,960 --> 00:07:53,290
common name used cases for machine learning.

187
00:07:53,290 --> 00:07:56,288
So we'll look at things like time-series forecasting,

188
00:07:56,288 --> 00:07:58,856
or working with images, or working with text, or

189
00:07:58,856 --> 00:08:00,335
working with spatial data.

190
00:08:00,335 --> 00:08:03,407
So we'll look at those specific scenarios and

191
00:08:03,407 --> 00:08:06,086
how we apply machine learning to working

192
00:08:06,086 --> 00:08:08,617
in those particular fields of study.

193
00:08:08,617 --> 00:08:11,813
Now alternatively, if you prefer to work with big data, if you've

194
00:08:11,813 --> 00:08:14,802
perhaps got some experience doing some big data engineering.

195
00:08:14,802 --> 00:08:17,803
Maybe you've worked with or maybe you've done some data

196
00:08:17,803 --> 00:08:21,160
warehousing, you've worked with large quantities of data.

197
00:08:21,160 --> 00:08:24,170
You might want to look at our Predictive Models with

198
00:08:24,170 --> 00:08:25,710
Spark Course.

199
00:08:25,710 --> 00:08:29,040
And a Spark is an open-source distributed system for

200
00:08:29,040 --> 00:08:30,340
working with data.

201
00:08:30,340 --> 00:08:32,201
And we offer a solution for

202
00:08:32,201 --> 00:08:34,830
Spark that runs in Azure HDInsight.

203
00:08:34,830 --> 00:08:38,328
So we'll teach you in this class how to spin up a Spark cluster

204
00:08:38,328 --> 00:08:39,572
in Azure HDInsight.

205
00:08:39,572 --> 00:08:41,838
And how to use Spark to manipulate data,

206
00:08:41,838 --> 00:08:43,846
to build machine learning models.

207
00:08:43,846 --> 00:08:47,717
And to work with Python and Scala, which is a language for

208
00:08:47,717 --> 00:08:48,288
SPARK.

209
00:08:48,288 --> 00:08:52,535
And also using R Server to work with R in a distributed fashion

210
00:08:52,535 --> 00:08:53,960
on a SPARK cluster.

211
00:08:53,960 --> 00:08:56,248
So if you're into that big data scenario,

212
00:08:56,248 --> 00:08:58,799
then that might be the course for you to look at.

213
00:08:58,799 --> 00:09:00,541
And then the other option you've got here,

214
00:09:00,541 --> 00:09:02,009
maybe at heart you're a developer.

215
00:09:02,009 --> 00:09:05,051
Maybe you're interested in building applications that

216
00:09:05,051 --> 00:09:07,147
consume machine learning solutions.

217
00:09:07,147 --> 00:09:10,574
So maybe you wanna build those applications for enterprises or

218
00:09:10,574 --> 00:09:14,130
for mobile applications or whatever it is you wanna build.

219
00:09:14,130 --> 00:09:16,356
Maybe the programming side of things is what you're

220
00:09:16,356 --> 00:09:17,069
interested in.

221
00:09:17,069 --> 00:09:20,301
So the Developing Intelligent Applications course is about

222
00:09:20,301 --> 00:09:22,401
building those client applications.

223
00:09:22,401 --> 00:09:25,965
That consume the backend machine learning processes that we've

224
00:09:25,965 --> 00:09:28,290
talked about building until now.

225
00:09:28,290 --> 00:09:32,022
And providing applications that exhibit intelligence and

226
00:09:32,022 --> 00:09:35,611
that engage with their users in an intelligent fashion.

227
00:09:35,611 --> 00:09:37,952
And whichever one of those you choose to complete,

228
00:09:37,952 --> 00:09:39,592
you can choose any ibe of the three.

229
00:09:39,592 --> 00:09:42,239
And then you're ready to move on to the final unit in

230
00:09:42,239 --> 00:09:43,170
the curriculum.

231
00:09:44,980 --> 00:09:47,700
Now the final unit, it says it's called project.

232
00:09:47,700 --> 00:09:51,130
And so the only thing that's in here is the final project.

233
00:09:51,130 --> 00:09:53,638
And this is where you get a chance to prove that you've

234
00:09:53,638 --> 00:09:56,539
learned and to practice all of the things that you've learned

235
00:09:56,539 --> 00:09:58,452
throughout the rest of the curriculum.

236
00:09:58,452 --> 00:09:59,738
And what we've got here is

237
00:09:59,738 --> 00:10:01,678
a Data Science Professional Project.

238
00:10:01,678 --> 00:10:04,990
And what that actually does, it's a course that you take just

239
00:10:04,990 --> 00:10:07,990
like any of the other courses, you start of in a course.

240
00:10:07,990 --> 00:10:09,801
But then we'll send you off to

241
00:10:09,801 --> 00:10:13,003
the Cortana Intelligence Competitions Platform.

242
00:10:13,003 --> 00:10:16,144
Where you get a chance to take a data set that we've got for you.

243
00:10:16,144 --> 00:10:19,198
We've prepared the data, you'll then have to work with that

244
00:10:19,198 --> 00:10:22,382
data, clean it up, examine it, find the relationships in it.

245
00:10:22,382 --> 00:10:25,930
And build a machine learning solution based on that data that

246
00:10:25,930 --> 00:10:27,113
you will publish and

247
00:10:27,113 --> 00:10:29,980
that we will grade using some private test data.

248
00:10:29,980 --> 00:10:32,872
And the grading that will apply to that data will determine

249
00:10:32,872 --> 00:10:36,160
whether or not you pass the professional project.

250
00:10:36,160 --> 00:10:40,700
So it's a chance really for you to work with data in a real

251
00:10:40,700 --> 00:10:43,240
situation where you really are gonna have to build a solution.

252
00:10:43,240 --> 00:10:45,870
It's not a step by step lab where we give you everything.

253
00:10:45,870 --> 00:10:47,810
You're actually gonna have to take the data,

254
00:10:47,810 --> 00:10:49,650
make your own decisions, and build your own model.

255
00:10:51,220 --> 00:10:54,590
And once you've completed that you're done.

256
00:10:54,590 --> 00:10:56,420
We get the checkered flag, and

257
00:10:56,420 --> 00:10:57,820
you have completed the curriculum.

258
00:10:57,820 --> 00:11:01,133
And by then, you should find that you've managed to amass all

259
00:11:01,133 --> 00:11:03,809
the skills that are involved in data science, and

260
00:11:03,809 --> 00:11:05,801
different aspects of data science.

261
00:11:05,801 --> 00:11:08,300
So right from the basics of working with data,

262
00:11:08,300 --> 00:11:10,931
creating relational databases, using Excel,

263
00:11:10,931 --> 00:11:12,803
using Power BI to visualize data.

264
00:11:12,803 --> 00:11:16,317
Right through programming with R and Python and using tools like

265
00:11:16,317 --> 00:11:19,257
Azure machine learning to build predictive models.

266
00:11:19,257 --> 00:11:21,379
And perhaps even building client applications or

267
00:11:21,379 --> 00:11:22,880
working with big data in SPARK.

268
00:11:22,880 --> 00:11:25,120
So there's a lot of things that you're gonna learn as we go

269
00:11:25,120 --> 00:11:25,940
through the curriculum.

270
00:11:25,940 --> 00:11:27,760
And then when we get to the end,

271
00:11:27,760 --> 00:11:30,960
you're ready to move on with your data science career.

272
00:11:30,960 --> 00:11:32,490
So we hope you enjoy the curriculum.

273
00:11:32,490 --> 00:11:35,170
We hope you find it useful.

274
00:11:35,170 --> 00:11:37,120
Like I said, there are places in the curriculum where you

275
00:11:37,120 --> 00:11:40,030
can choose one course of a number of options.

276
00:11:40,030 --> 00:11:42,085
If you want, there's nothing stopping you taking multiple

277
00:11:42,085 --> 00:11:44,270
courses if you particularly want to.

278
00:11:44,270 --> 00:11:45,195
But you only need to take one.

279
00:11:45,195 --> 00:11:47,830
Whenever there's a choice you only need to take one

280
00:11:47,830 --> 00:11:49,830
to complete the curriculum.

281
00:11:49,830 --> 00:11:52,030
So good luck with continuing the courses.

282
00:11:52,030 --> 00:11:54,706
In the next session we'll talk a little bit about the schedule

283
00:11:54,706 --> 00:11:56,672
for those courses, when they are gonna run.

284
00:11:56,672 --> 00:11:59,325
And the things you have to do to make sure that you complete each

285
00:11:59,325 --> 00:12:00,559
course in a timely fashion.

