0
00:00:00,000 --> 00:00:04,700
[Music]

1
00:00:04,770 --> 00:00:09,670
Hi and welcome to this Microsoft course on EdX. My name is Graeme Malcolm; I’m going

2
00:00:09,670 --> 00:00:13,820
to be taking you through this course over next 2 modules and it&#39;s great to have you

3
00:00:13,820 --> 00:00:18,119
join us. If it’s the first time you have been to an Edx course, I just want to spend

4
00:00:18,119 --> 00:00:22,410
some time in the beginning helping you find your way around so that we’re ready to start.

5
00:00:22,410 --> 00:00:27,910
So, if you are watching this video you should be watching it in the EdX player and you should

6
00:00:27,910 --> 00:00:34,120
see to your left there is a list of the modules. Now if you’re viewing the course live, the

7
00:00:34,120 --> 00:00:38,030
module will appear week by week as they get released; if you’re viewing the course on

8
00:00:38,030 --> 00:00:42,270
demand as a self-paced course they may already all be there. But you can flick through the

9
00:00:42,270 --> 00:00:46,760
modules and in each module, you will find different content. Now, when you are in the

10
00:00:46,760 --> 00:00:50,450
module and you are viewing the content, if you look above me you will see there is a

11
00:00:50,450 --> 00:00:55,320
bar with some icons on it. Each of the topics is listed along there and you can click through

12
00:00:55,320 --> 00:01:00,530
that bar to work your way through the topics in the module. There is also, down below me,

13
00:01:00,530 --> 00:01:04,329
a pair of buttons for going forward and back so you can use those to navigate the course

14
00:01:04,328 --> 00:01:08,119
as well. So now that you know little bit of the environment in which we are going to do

15
00:01:08,119 --> 00:01:11,939
the course, you are ready to get started. Let&#39;s go and have a look of what we’re actually

16
00:01:11,939 --> 00:01:13,079
going to cover in this course.

