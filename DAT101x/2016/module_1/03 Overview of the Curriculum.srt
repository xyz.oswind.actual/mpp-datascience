0
00:00:00,000 --> 00:00:05,200
[Music]

1
00:00:05,210 --> 00:00:09,370
Let&#39;s take a look at how the curriculum works. Here you are, your sleeves are rolled

2
00:00:09,370 --> 00:00:13,120
up and you&#39;re ready to start work. And that work is gonna involve learning all of the

3
00:00:13,120 --> 00:00:18,750
skills that are required to be a data scientist. You can think of these skills as being stops

4
00:00:18,750 --> 00:00:24,110
along a subway map, where you travel through the subway map attaining the skills at these

5
00:00:24,110 --> 00:00:28,020
various different stops and eventually learning all of the skills that you are required to

6
00:00:28,020 --> 00:00:33,690
be successful as a data scientist. For each of these stops, we&#39;ll have one or more courses

7
00:00:33,690 --> 00:00:38,600
that you can take. And in these courses, we&#39;ll have lectures from experts in their field

8
00:00:38,600 --> 00:00:44,570
who&#39;ll explain the different concepts and principles. There&#39;ll also be hands-on activities

9
00:00:44,570 --> 00:00:49,489
in these courses, to give you a chance to actually try things over yourself. And there&#39;ll

10
00:00:49,489 --> 00:00:53,440
be assessments you can take in order to validate the skills that you&#39;ve learned

11
00:00:53,440 --> 00:00:59,570
in each of those stops. Each course will run for six weeks. So you have six weeks to complete

12
00:00:59,570 --> 00:01:03,359
all of the exercises in the course and earn the verified certificate that you need to

13
00:01:03,359 --> 00:01:07,820
complete the curriculum. You can do multiple courses in parallel if you like and you can

14
00:01:07,820 --> 00:01:12,520
do in less than six weeks. But you must complete that course within the six weeks while its

15
00:01:12,520 --> 00:01:18,200
running. After each six week block, the courses will be removed, they may be updated and then

16
00:01:18,200 --> 00:01:22,390
they&#39;ll be re-released for the next run. So if you fail to complete a course during

17
00:01:22,390 --> 00:01:27,070
its first run of six weeks, you can then have another go at completing it before the curriculum

18
00:01:27,070 --> 00:01:31,899
ends in its next run. And you&#39;ll be able to consult the course schedule to figure out

19
00:01:31,899 --> 00:01:38,539
when the best time is to take each course you&#39;re going to require to complete the curriculum.

20
00:01:38,539 --> 00:01:42,420
Now the final stop in the curriculum is different from the others, rather than a course, it&#39;s

21
00:01:42,420 --> 00:01:46,270
a professional project. Its a chance for you to apply the skills you&#39;ve learnt through

22
00:01:46,270 --> 00:01:53,409
the rest of the curriculum and take on a data science challenge with some real world data.

23
00:01:53,409 --> 00:01:58,170
You need to complete all of the courses in the curriculum and the final project in order

24
00:01:58,170 --> 00:02:04,179
to verify that you have the skills it takes to be a data scientist. Now, you&#39;re not on

25
00:02:04,179 --> 00:02:07,619
your own as you complete the curriculum. There are lots of resources that you can use to

26
00:02:07,619 --> 00:02:13,060
help you. There is the dashboard which provides lots of information about the curriculum as

27
00:02:13,060 --> 00:02:18,220
a whole. And of course, you&#39;re not on your own in the courses. There are students from

28
00:02:18,220 --> 00:02:22,000
all around the world who&#39;ll be working alongside you and you&#39;ll be able to engage with these

29
00:02:22,000 --> 00:02:26,280
students in the course forums, asking questions and answering other students where you can

30
00:02:26,280 --> 00:02:32,110
help. In addition to your fellow students, there are teaching assistants who monitor

31
00:02:32,110 --> 00:02:38,040
the forums and they&#39;ll be able to answer questions and to provide guidance. So the key thing

32
00:02:38,040 --> 00:02:42,400
to take from this is that the curriculum is a collaborative space where you can work with

33
00:02:42,400 --> 00:02:46,910
your fellow students as you move towards your common goal of becoming a data scientist.

