0
00:00:06,930 --> 00:00:13,410
There are a few things that are fundamental that you must know. So, for example, you need

1
00:00:13,410 --> 00:00:20,859
a good handle on the statistics that you&#39;ll use every day. &gt;&gt; Maths is one thing that

2
00:00:20,859 --> 00:00:26,000
you definitely need, statistics. &gt;&gt; You also need soft skills, so you would need to be

3
00:00:26,000 --> 00:00:31,270
able to communicate your findings to people who might not necessarily have the mathematical

4
00:00:31,270 --> 00:00:36,660
background you have and you need to justify your findings to people who are outside your

5
00:00:36,660 --> 00:00:42,410
field of expertise. &gt;&gt; And a couple of programming languages would help. &gt;&gt; One is the R language

6
00:00:42,410 --> 00:00:48,190
and the other is Python. Those – the industry has pretty much said those are the big tools

7
00:00:48,190 --> 00:00:53,969
of the data science. &gt;&gt; Apart from it, the presentation layer may be Microsoft Power

8
00:00:53,969 --> 00:01:00,710
BI, Tableau, click or some of these tools would help. &gt;&gt; There&#39;s lot of great tools that are

9
00:01:00,710 --> 00:01:05,289
out there in the community that you can leverage – visualization tools; you need to be able

10
00:01:05,289 --> 00:01:11,280
to explore data and understand data well and then there is the different ways you need

11
00:01:11,280 --> 00:01:17,490
to transform the data whether it&#39;s even using things like Excel, or R, or Python; and then

12
00:01:17,490 --> 00:01:21,700
there is the modelling techniques that could be done with tools like Azure machine learning

13
00:01:21,700 --> 00:01:27,920
or Spark or again R, Python types of tools. So being able to transform data and be able

14
00:01:27,920 --> 00:01:31,439
to create models on top of the data; those are some key skills that you need as a data

15
00:01:31,439 --> 00:01:38,390
scientist. &gt;&gt; So, you&#39;re starting to see people developing tools that put together all the

16
00:01:38,390 --> 00:01:45,170
different parts of – for example Azure ML is an attempt to put together a set of tools

17
00:01:45,170 --> 00:01:52,770
for gathering data, clean data and then analysing it in a way that is repeatable and sharable

18
00:01:52,770 --> 00:01:58,990
and you like to call it “democratizing” the data science. We’re not there yet, as

19
00:01:58,990 --> 00:02:04,439
an industry, that anybody can do it but we&#39;re trying to develop tools that will make it

20
00:02:04,439 --> 00:02:10,389
possible so that most people can do it without needing to have a PhD in statistics.

