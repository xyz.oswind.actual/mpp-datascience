0
00:00:00,000 --> 00:00:07,259
alright let&#39;s talk a little bit about the timetable for the curriculum

1
00:00:07,259 --> 00:00:11,639
obviously that are a lot of courses to get through so you&#39;re not going to do

2
00:00:11,639 --> 00:00:12,900
them all at once

3
00:00:12,900 --> 00:00:15,509
let&#39;s talk about the approach that we can take to get through them and let&#39;s

4
00:00:15,509 --> 00:00:19,980
take a look at our schedule now the way that the the schedule works is we have

5
00:00:19,980 --> 00:00:27,330
four quarters in a year and for the purposes of the the program our quarter

6
00:00:27,330 --> 00:00:31,710
star in july so we have we have conceptually something like an academic

7
00:00:31,710 --> 00:00:36,840
year that starts in july and we run from july to $MONTH june that&#39;s a year of the

8
00:00:36,840 --> 00:00:43,710
program within that year we have these four quarters and each course is run

9
00:00:43,710 --> 00:00:49,320
once a quarter so therefore runs of each course throughout the entire year that

10
00:00:49,320 --> 00:00:53,579
means that you can choose to take all of the courses in the first quarter if you

11
00:00:53,579 --> 00:00:58,800
wish or you can plan out your schedule to spread across the entire time

12
00:00:58,800 --> 00:01:02,940
everything&#39;s completely self paced so there&#39;s there&#39;s no requirement for you

13
00:01:02,940 --> 00:01:08,280
to sign up for for particular dates but you do have to complete a course if you

14
00:01:08,280 --> 00:01:11,460
start a course in one quarter to get credit for like course you have to

15
00:01:11,460 --> 00:01:15,570
completely successfully within the same quarter and if you don&#39;t achieve a

16
00:01:15,570 --> 00:01:18,750
passing grade or if you don&#39;t complete the course then you have to retake the

17
00:01:18,750 --> 00:01:23,189
entire course and one of the later quarters so there&#39;s four chances to take

18
00:01:23,189 --> 00:01:27,390
each course a year and you have a quarter your three months within which

19
00:01:27,390 --> 00:01:33,270
to complete the course so the other thing that&#39;s worth noting about this

20
00:01:33,270 --> 00:01:36,720
schedule here you can see that we have courses starting and quarter one and

21
00:01:36,720 --> 00:01:40,470
there be run again quarter-to-quarter to inquire for you&#39;ll notice that the

22
00:01:40,470 --> 00:01:43,950
professional project to the end is slightly different it doesn&#39;t run for

23
00:01:43,950 --> 00:01:49,200
the food quarter it starts part of the way through the the at the third month

24
00:01:49,200 --> 00:01:53,880
in each quarter and it carries on over into the next quarter so in other words

25
00:01:53,880 --> 00:01:57,990
you have three months to finish all of the courses and then the professional

26
00:01:57,990 --> 00:02:02,820
project starts before the quarter ends and and there are a couple weeks into

27
00:02:02,820 --> 00:02:06,719
the next quarter in which to finish that professional projects is a little bit of

28
00:02:06,719 --> 00:02:10,950
overlap for that now the other thing we&#39;re thinking about as you take these

29
00:02:10,949 --> 00:02:13,410
courses each quarter is

30
00:02:13,410 --> 00:02:17,250
what are the the actions that you have to take one of the specific requirements

31
00:02:17,250 --> 00:02:21,540
for you and order to take these classes successfully well the first thing is

32
00:02:21,540 --> 00:02:26,280
that you have to register for each class and registration opens one week before

33
00:02:26,280 --> 00:02:31,800
class begins so you can register any time from when registration opens right

34
00:02:31,800 --> 00:02:35,790
up until it closes a week before the class ends and you can take the class

35
00:02:35,790 --> 00:02:41,040
self-paced anytime during that quarter registration opens one week before the

36
00:02:41,040 --> 00:02:46,350
beginning of each quarter and then you can start registering for courses week

37
00:02:46,350 --> 00:02:50,970
after registration opens the the courses start and again you don&#39;t have to start

38
00:02:50,970 --> 00:02:54,420
the course on that specific date you can you can dip into that course as often as

39
00:02:54,420 --> 00:02:58,260
you like between the beginning of the quarter in the end of the quarter so you

40
00:02:58,260 --> 00:03:03,030
can do everything immediately or you can register any point during that quarter

41
00:03:03,030 --> 00:03:08,760
and take the course as long as you completed before the course ends now

42
00:03:08,760 --> 00:03:11,880
what&#39;s going to happen is a week before the core the course finishes

43
00:03:11,880 --> 00:03:15,360
registration will close so although the course is still running you&#39;ll no longer

44
00:03:15,360 --> 00:03:18,660
be able to register for that course if you missed that you have to wait for the

45
00:03:18,660 --> 00:03:23,010
next I&#39;m course but that&#39;s okay because registration for the same course in the

46
00:03:23,010 --> 00:03:26,790
next quarter opens at the same time as it closes for the first quarter if you

47
00:03:26,790 --> 00:03:31,350
didn&#39;t take the course in the first three months registration closes it&#39;ll

48
00:03:31,350 --> 00:03:34,830
open for the next three months and you can then start registering to take the

49
00:03:34,830 --> 00:03:41,250
course in the next quarter at the end of that first quarter the the course ends

50
00:03:41,250 --> 00:03:47,430
and the validated certificate deadline happens then as well so any point during

51
00:03:47,430 --> 00:03:51,000
that three-month period when the course is running you can take the course you

52
00:03:51,000 --> 00:03:54,060
can finish all the work you could finish in a week if you like or you could take

53
00:03:54,060 --> 00:03:57,720
your time and spread across the entire three months but any point you can you

54
00:03:57,720 --> 00:04:01,290
can go and request your validated certificate for completing that course

55
00:04:01,290 --> 00:04:05,160
but if you leave that too late if you wait until the quarters ended you might

56
00:04:05,160 --> 00:04:08,220
miss your opportunity so the most important thing you have to remember is

57
00:04:08,220 --> 00:04:11,520
when you&#39;ve completed the course and you&#39;ve got a passing grade you want to

58
00:04:11,520 --> 00:04:15,570
go and get your validated certificate so make sure you leave yourself time to do

59
00:04:15,570 --> 00:04:20,880
that of course the other thing that happens is the the second run of the

60
00:04:20,880 --> 00:04:24,900
course starts the second quarter starts so the new runs of the courses start if

61
00:04:24,900 --> 00:04:27,100
you partially completed a course in the first

62
00:04:27,100 --> 00:04:30,790
order you can&#39;t carry forward any work that you&#39;ve done so if you don&#39;t

63
00:04:30,790 --> 00:04:32,530
complete the course in the first quarter

64
00:04:32,530 --> 00:04:36,550
you have to do the whole thing again and in a subsequent quarter in the second

65
00:04:36,550 --> 00:04:41,740
third or fourth quarter so I&#39;m there&#39;s no partial credit that carried over is

66
00:04:41,740 --> 00:04:47,350
basically retake the course from the star and of course the same thing

67
00:04:47,350 --> 00:04:51,130
happens at the end of the second quarter registration will close around a week

68
00:04:51,130 --> 00:04:54,670
before the the course finishes and at that point registration for the next one

69
00:04:54,670 --> 00:04:58,570
of the course will open and then we clear the course ends and you have the

70
00:04:58,570 --> 00:05:04,540
validated certificate deadline for that run of the course as well now the time

71
00:05:04,540 --> 00:05:09,580
for all of these deadlines is zero hundred hours that&#39;s midnight and right

72
00:05:09,580 --> 00:05:14,200
at the very start of every day so when you see a deadline for the date that the

73
00:05:14,200 --> 00:05:19,270
deadline is zero hundred hours at the start of that date now of course that&#39;s

74
00:05:19,270 --> 00:05:22,750
only useful if you think about it in your local time and of course these

75
00:05:22,750 --> 00:05:28,390
courses are offered worldwide so everybody&#39;s in a different timezone so

76
00:05:28,390 --> 00:05:32,260
the convention with chosen is that our deadlines are specified at 0:00 hours

77
00:05:32,260 --> 00:05:39,040
in UTC as universal coordinated time and that is the the time in London is the

78
00:05:39,040 --> 00:05:44,080
same as GMT so the time in London during the winter months so that&#39;s the year the

79
00:05:44,080 --> 00:05:46,090
deadline whenever you see a date

80
00:05:46,090 --> 00:05:50,500
the deadline is actually 0:00 hours at the start of that date in the

81
00:05:50,500 --> 00:05:56,650
UTC time zone, in GMT effectively, so you&#39;ll have to figure out depending on

82
00:05:56,650 --> 00:06:01,720
what time zone you&#39;re in across the world -  what time that is actually locally for

83
00:06:01,720 --> 00:06:06,640
you. You should bear in mind that the deadline is always midnight zero hundred

84
00:06:06,640 --> 00:06:12,310
hours of the start of the date that specified UTC just make sure that you&#39;re

85
00:06:12,310 --> 00:06:15,640
aware of what that is in your local timezone and then you&#39;ll avoid missing

86
00:06:15,640 --> 00:06:20,620
any deadlines so hopefully that explains a little bit about some of the key

87
00:06:20,620 --> 00:06:24,340
responsibilities the you as the student have from making sure you do things on

88
00:06:24,340 --> 00:06:27,880
time make sure you register for the quarter on that you want to take make

89
00:06:27,880 --> 00:06:31,420
sure that you complete all of the great work before the deadline remember that

90
00:06:31,420 --> 00:06:35,710
the deadline is expressed in universal coordinated time and it&#39;s midnight at

91
00:06:35,710 --> 00:06:39,370
the start of the day that&#39;s the deadline for getting all the work done and also

92
00:06:39,370 --> 00:06:40,370
the deadline for upgrading

93
00:06:40,370 --> 00:06:44,780
to a verified certificate and the registration remains open all the

94
00:06:44,780 --> 00:06:48,080
way through the course until a week before the course ends and then it opens

95
00:06:48,080 --> 00:06:51,680
for the next one of the core so at that point if you haven&#39;t registered to do

96
00:06:51,680 --> 00:06:55,550
the course you have to do the second run the third run whichever run as you

97
00:06:55,550 --> 00:06:57,350
waiting for

98
00:06:57,350 --> 00:07:00,199
so now you are you know a little bit of license jest you going to take a look at

99
00:07:00,199 --> 00:07:03,410
the the course materials that we have in this course and have a look at the the

100
00:07:03,410 --> 00:07:07,729
actual timetable and get a look at the dates of when these course runs actually

101
00:07:07,729 --> 00:07:11,720
a star and when they run and start to plan out your schedule for the

102
00:07:11,720 --> 00:07:12,220
curriculum

