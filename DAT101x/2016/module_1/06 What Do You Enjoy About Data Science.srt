0
00:00:06,609 --> 00:00:11,760
What do I enjoy about being data scientist? I love being a problem solver. I get lots

1
00:00:11,760 --> 00:00:17,450
of complicated problems from business users. They want to forecast something; they want

2
00:00:17,450 --> 00:00:22,470
to understand how data can improve their business. I love working with them to figure out – how

3
00:00:22,470 --> 00:00:27,880
can we utilise that data to actually provide value to the business? &gt;&gt; I&#39;ve always loved

4
00:00:27,880 --> 00:00:33,489
playing around with data, it amazes me how you could put data into different formats,

5
00:00:33,489 --> 00:00:38,810
different visualizations and then making sense out of that data. &gt;&gt; I think what I enjoy

6
00:00:38,810 --> 00:00:44,000
most about being a data scientist is the challenge. It’s almost like being an investigative

7
00:00:44,000 --> 00:00:49,320
journalist, investigating a story; you never know what are you going to find when get your

8
00:00:49,320 --> 00:00:55,190
hands on this new set of data. &gt;&gt; I really enjoy just going through and solving the puzzles

9
00:00:55,190 --> 00:01:01,820
and questions necessary to come up with new knowledge about product. &gt;&gt; It feels at times

10
00:01:01,820 --> 00:01:07,149
like working on an open end of puzzle, you get to work with terabytes of data that might

11
00:01:07,149 --> 00:01:13,080
look meaningless on the surface but as you dig deeper, you’ll find patterns and insights

12
00:01:13,080 --> 00:01:17,950
that would have lethal effects throughout the company. &gt;&gt; If you listen carefully to

13
00:01:17,950 --> 00:01:24,110
the data, it talks, it will tell you stories. So it is very interesting to see – watch

14
00:01:24,110 --> 00:01:30,890
out the user trends and see what all the users are doing, and then it’s not someone&#39;s opinion

15
00:01:30,890 --> 00:01:38,080
in the business or something; it&#39;s actually the fact you are looking into. &gt;&gt; I also love

16
00:01:38,080 --> 00:01:43,460
working with people from a variety of different backgrounds. So Data scientists can come from

17
00:01:43,460 --> 00:01:47,150
chemistry, they can come from biology, they can come from psychology, they can come from

18
00:01:47,150 --> 00:01:52,100
economics or finance or computer science and so I love working with people with really

19
00:01:52,100 --> 00:01:58,450
diverse backgrounds, bringing together those different ideas and really solving problems

20
00:01:58,450 --> 00:02:03,570
for different businesses. &gt;&gt; It is a very exciting time to be a data scientist because

21
00:02:03,570 --> 00:02:08,329
the world&#39;s data is said to be doubling every couple of years, so it’s a big challenge

22
00:02:08,329 --> 00:02:10,220
out here, which I enjoy.

