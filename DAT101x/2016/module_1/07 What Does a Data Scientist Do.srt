0
00:00:07,080 --> 00:00:12,070
So, typically as a data scientist, the majority of our time is spent dealing

1
00:00:12,070 --> 00:00:16,820
with data, munging data; doing what we call feature engineering, so taking that raw input

2
00:00:16,820 --> 00:00:22,160
data and transforming it in a way that we can actually provide value. &gt;&gt; A great deal

3
00:00:22,160 --> 00:00:29,140
of my time is spent cleaning data and getting data ready to actually be good enough to be

4
00:00:29,140 --> 00:00:34,510
trust worthy. So one of the things that you always want to do with your data is make sure

5
00:00:34,510 --> 00:00:39,269
that you can trust anything that comes out of it and a huge percentage of my time is

6
00:00:39,269 --> 00:00:45,690
spent trying to make the data trustworthy and usable and that is probably the biggest

7
00:00:45,690 --> 00:00:53,540
challenge. &gt;&gt; So, I spend most of time in understanding and prepping the data and sometimes

8
00:00:53,540 --> 00:01:02,479
it also has to do accessing data from multiple data sources and see what kind of data formulations

9
00:01:02,479 --> 00:01:07,960
that we need to do. Sometimes it would be a big data problem which you&#39;ll have to parse

10
00:01:07,960 --> 00:01:15,280
the data into various algorithms and then make sense out of it. So this is on a day-to-day

11
00:01:15,280 --> 00:01:21,790
basis, but apart from it, every now and then I would take part into this online discussions,

12
00:01:21,790 --> 00:01:28,970
online competitions and what&#39;s going on in technology to improve myself; how do I make

13
00:01:28,970 --> 00:01:33,100
myself more efficient, keeping abreast with the latest technologies?

