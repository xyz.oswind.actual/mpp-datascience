0
00:00:03,870 --> 00:00:09,600
so those are actually example values of what we call central tendency is about

1
00:00:09,600 --> 00:00:15,089
finding the middle of the data basically it&#39;s understanding kind of what the how

2
00:00:15,089 --> 00:00:18,660
the data shapes what they sort of looks like but it&#39;s really only the beginning

3
00:00:18,660 --> 00:00:23,130
of the story the other part of that is variability or variance so let&#39;s take a

4
00:00:23,130 --> 00:00:27,989
look at some of those measurements now that we know something about the center

5
00:00:27,989 --> 00:00:31,559
tendencies of the values of our data set we can start to calculate some of the

6
00:00:31,559 --> 00:00:35,850
measures of variability in other words how spread apart are the data values the

7
00:00:35,850 --> 00:00:39,660
very simplest way to think about dispersion of your data is to look at

8
00:00:39,660 --> 00:00:45,120
something that we call range and range is very simply the maximum value and the

9
00:00:45,120 --> 00:00:49,199
bad it&#39;s the minimum value subtracted from the maximum value so if we take a

10
00:00:49,199 --> 00:00:53,730
look at this particular data what we can see is that the maximum value is ten the

11
00:00:53,730 --> 00:00:58,890
minimum value is to us the Rangers eight so it starts to tell the story about how

12
00:00:58,890 --> 00:01:04,080
disperse our data is so in this case that the most lemonade&#39;s that she sold

13
00:01:04,080 --> 00:01:07,080
on anyone he was tangible issues to the range is the difference between those

14
00:01:07,080 --> 00:01:08,370
two cracks right

15
00:01:08,370 --> 00:01:11,820
next let&#39;s get a little more sophisticated and let&#39;s talk about this

16
00:01:11,820 --> 00:01:17,520
idea called variance so how variable is the data to do this we actually create

17
00:01:17,520 --> 00:01:22,230
this calculation that we call uh that looks like this it&#39;s essentially each

18
00:01:22,230 --> 00:01:26,070
individual value subtracted from the mean and then divided by the total

19
00:01:26,070 --> 00:01:30,270
number of observations again here this is actually the calculation for the

20
00:01:30,270 --> 00:01:34,920
population you notice we have the Greek symbol for Sigma squared that&#39;s what we

21
00:01:34,920 --> 00:01:39,330
know as a variance let&#39;s take a look at what this calculation looks like for a

22
00:01:39,330 --> 00:01:44,220
sample exactly the same except there&#39;s a minor difference in the parameters that

23
00:01:44,220 --> 00:01:48,330
we use because now we&#39;re using the sample parameters where s lower case is

24
00:01:48,330 --> 00:01:52,350
an indicator of the variance so essentially what we&#39;re doing is we&#39;re

25
00:01:52,350 --> 00:01:56,310
taking the sum of each individual observation and subtracting it and

26
00:01:56,310 --> 00:02:00,090
subtracting the mean from that when we use the sample will be divided by n

27
00:02:00,090 --> 00:02:05,220
minus 1 whereas we just divided by n for the full population so is that just

28
00:02:05,220 --> 00:02:09,629
because when I&#39;m gonna sample have got all the data so there may be some bias

29
00:02:09,628 --> 00:02:10,880
and thereby what is

30
00:02:10,880 --> 00:02:15,050
actually one hundred percent correct its to correct for some bias if we don&#39;t

31
00:02:15,050 --> 00:02:20,480
subtract the one we end up with variances that are a little bit bigger

32
00:02:20,480 --> 00:02:26,600
and so that&#39;s why we take care of that that bias by subtracting x 1 but what&#39;s

33
00:02:26,600 --> 00:02:29,540
interesting here and what I love about this particular calculation is that

34
00:02:29,540 --> 00:02:32,930
often times people ask me why are you making it so complicated why do we have

35
00:02:32,930 --> 00:02:36,290
to take this square but why do we have to sum of the squared differences

36
00:02:36,290 --> 00:02:39,830
well it turns out when it comes to the mean if you subtract every individual

37
00:02:39,830 --> 00:02:44,690
observation from the mean and you don&#39;t square it you end up with zero right

38
00:02:44,690 --> 00:02:48,710
because by definition the mean is the average and you got that by summing up

39
00:02:48,710 --> 00:02:51,470
everything and then dividing by the total number of observations so if we

40
00:02:51,470 --> 00:02:54,800
don&#39;t take care of that we end up with zero so how do we manage that we take

41
00:02:54,800 --> 00:02:58,910
the sum of the squared differences so that&#39;s where we end up with the variance

42
00:02:58,910 --> 00:03:02,810
so if we run that calculation here this is what it essentially looks like in

43
00:03:02,810 --> 00:03:07,010
this case the variances six it&#39;s not uncommon when you&#39;re dealing with very

44
00:03:07,010 --> 00:03:10,970
large datasets however that the variance becomes very large and in some ways it

45
00:03:10,970 --> 00:03:15,230
becomes unwieldy we&#39;re talking about variances that are in that get close to

46
00:03:15,230 --> 00:03:19,910
hundreds and if the data is is very big and very dispersed you might have even

47
00:03:19,910 --> 00:03:24,410
multiple hundreds of values that were looking at in terms of the variance so

48
00:03:24,410 --> 00:03:29,240
reason we do that to calculate the variance to we have to get the squared

49
00:03:29,240 --> 00:03:33,860
value to manage the difference because of the way the mean works but it is

50
00:03:33,860 --> 00:03:38,000
unwieldy at times thus what we are almost always more interested in is what

51
00:03:38,000 --> 00:03:41,900
we call the standard deviation and the standard deviation is essentially just

52
00:03:41,900 --> 00:03:48,770
the square root of the variance so what we do we have six the square root of six

53
00:03:48,770 --> 00:03:54,290
is 2.45 that is our standard deviation now one of the things that&#39;s really cool

54
00:03:54,290 --> 00:03:58,190
about standard deviation is that it puts on a common metric and that if you have

55
00:03:58,190 --> 00:04:03,410
a normal distribution we all understand what that metric means so in this case

56
00:04:03,410 --> 00:04:07,850
what you&#39;ve seen plotted here is what the standard deviation from the mean as

57
00:04:07,850 --> 00:04:14,780
a plus and minus 1 plus and minus 2 in a normal distribution the a percent of

58
00:04:14,780 --> 00:04:21,979
your data that falls +1 between plus 1 and minus 1 is actually 68.2 percent the

59
00:04:21,978 --> 00:04:24,560
percent of your data that falls between plus

60
00:04:24,560 --> 00:04:30,620
two and minus two standard deviations is 95.4 percent and then within 3 plus or

61
00:04:30,620 --> 00:04:35,180
minus standard deviations is almost ninety nine point seven percent that&#39;s

62
00:04:35,180 --> 00:04:39,590
just like a universal constants always track of a normal distribution yeah it&#39;s

63
00:04:39,590 --> 00:04:42,410
always true of normal distribution which is really awesome and why we love

64
00:04:42,410 --> 00:04:46,550
dealing with normal distributions and that&#39;s why the the standard deviation is

65
00:04:46,550 --> 00:04:50,419
so helpful is because once you couldn&#39;t do that with variance because the the

66
00:04:50,419 --> 00:04:53,510
metrics that are used to calculate the variance or so difference but once you

67
00:04:53,510 --> 00:04:57,650
take the square root of that you can apply that logic and understand how your

68
00:04:57,650 --> 00:05:03,979
data is distributed around the mean accordingly then the last measure of

69
00:05:03,979 --> 00:05:07,639
dispersion that I want to talk about is what we call the standard error this one

70
00:05:07,639 --> 00:05:12,080
is actually pretty important because you almost always you will always use this

71
00:05:12,080 --> 00:05:17,000
in terms of the sample parameters and it&#39;s particularly important because it

72
00:05:17,000 --> 00:05:23,180
helps us understand that variance around what we call that the true mean so if

73
00:05:23,180 --> 00:05:27,770
you think about it every time you take a sample you&#39;re going to get a slightly

74
00:05:27,770 --> 00:05:32,750
different mean so if you go and you randomly sample from the population 10

75
00:05:32,750 --> 00:05:35,479
observations and then you take another 10 observations and then you take

76
00:05:35,479 --> 00:05:39,890
another 10 observations the odds of you getting the same mean are pretty low-key

77
00:05:39,890 --> 00:05:40,880
slight difference

78
00:05:40,880 --> 00:05:45,590
Rosie might sell anything between 0 and a hundred eliminates a day and be taking

79
00:05:45,590 --> 00:05:48,620
any 10 observations it&#39;s not always gonna up things out with the same it&#39;s

80
00:05:48,620 --> 00:05:51,950
not gonna always average to be the same its they&#39;re going to be close probably

81
00:05:51,950 --> 00:05:56,030
especially normal distribution and assuming that you randomly pulling from

82
00:05:56,030 --> 00:06:00,080
that but there will be times but by random even you would end up with some

83
00:06:00,080 --> 00:06:02,570
significantly different means

84
00:06:02,570 --> 00:06:06,710
so the idea with the standard error is to help you evaluate how close you are

85
00:06:06,710 --> 00:06:12,620
to the true measure the true mean given the sample that you you pulled so when

86
00:06:12,620 --> 00:06:17,300
you look at this you will actually see that we often use this in our statistics

87
00:06:17,300 --> 00:06:21,410
that are to follow we use the standard error of measurement rather than the

88
00:06:21,410 --> 00:06:24,590
standard deviation because we&#39;re taking that into account that it&#39;s really just

89
00:06:24,590 --> 00:06:27,979
one of many possible outcomes we could have got obtained

90
00:06:29,400 --> 00:06:34,110
so in this case the calculation for the standard air looks like this you have

91
00:06:34,110 --> 00:06:37,650
the standard deviation divided by the square root of the total number of

92
00:06:37,650 --> 00:06:42,270
observations you have which is nine in this case 3 so that comes out to be . a2

