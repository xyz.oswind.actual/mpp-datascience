0
00:00:05,040 --> 00:00:09,120
We’ve done a number of different tests up to this point, comparing different sets of

1
00:00:09,120 --> 00:00:14,320
data, but usually we’ve sort of focused on one thing; we’ve focused on sales or

2
00:00:14,320 --> 00:00:19,939
we’ve focused on oranges with lemons or whatever. I think really what I’m interested

3
00:00:19,939 --> 00:00:23,840
in now is, could we do a slightly more sophisticated comparison where perhaps I’m looking at

4
00:00:23,840 --> 00:00:30,180
a combination of – perhaps she sells her lemonade and her orangeade, but she sells

5
00:00:30,180 --> 00:00:34,570
them at the park and also at the beach, so I want to kind of compare all four of those

6
00:00:34,570 --> 00:00:39,750
factors and see what I can find. Is that possible? &gt;&gt; It is possible, and we can do that using

7
00:00:39,750 --> 00:00:44,890
something called “analysis of variance” or an ANOVA. So, if you think about it, we

8
00:00:44,890 --> 00:00:50,250
have her sales at the park of orangeade and lemonade, and we also have her sales at the

9
00:00:50,250 --> 00:00:54,850
beach of orangeade and lemonade. So is there a difference depending on where she sells

10
00:00:54,850 --> 00:01:01,110
her product? &gt;&gt; So, how does the ANOVA approach then help me understand that then? &gt;&gt; Well,

11
00:01:01,110 --> 00:01:06,259
the really cool thing about an ANOVA is that it allows you to do multiple t-tests at one

12
00:01:06,259 --> 00:01:09,460
time, so essentially what you’re doing is you’re taking a whole bunch of data and

13
00:01:09,460 --> 00:01:13,270
you’re running one single analysis to figure out what’s going on in a bigger picture.

14
00:01:13,270 --> 00:01:17,400
And the reason – from a statistical perspective – that that’s particularly important is

15
00:01:17,400 --> 00:01:22,700
that I can run multiple different t-tests and I could get that some of them will be

16
00:01:22,700 --> 00:01:27,280
statistically significant and some of them won’t be; but just kind of one of the rules

17
00:01:27,280 --> 00:01:31,979
of thumb is, the more analyses I run, the more likely I am to find something that’s

18
00:01:31,979 --> 00:01:37,080
statistically significant; and so to control for that, to manage some of the error of doing

19
00:01:37,080 --> 00:01:41,670
multiple analyses, the ANOVA really takes that into account and really portions the

20
00:01:41,670 --> 00:01:47,290
error and the variance in such a way that you really understand what is truly statistically

21
00:01:47,290 --> 00:01:50,500
significant when you’re taking all sorts of analyses into consideration.

22
00:01:50,500 --> 00:01:54,909
In Excel, we can actually run a couple of different variations of an ANOVA, so let’s

23
00:01:54,909 --> 00:02:00,170
start with the basic one. Before you start doing an ANOVA in Excel though, you do have

24
00:02:00,170 --> 00:02:05,119
to do some reformatting of your data. Now, Graham talked a little bit about pivot tables,

25
00:02:05,119 --> 00:02:09,580
and you might be able to leverage pivot tables to get the data in the format that you need,

26
00:02:09,580 --> 00:02:15,049
but essentially what you need for the basic ANOVA is that it needs to be formatted in

27
00:02:15,049 --> 00:02:21,010
such a manner as this, where you can see that the number of sales that she had for day one

28
00:02:21,010 --> 00:02:25,870
in the park for lemonade is this, in the park for day one with 74, in the beach, and the

29
00:02:25,870 --> 00:02:29,980
beach; and so you can see how this data is laid out a little bit differently than what

30
00:02:29,980 --> 00:02:36,450
we have been looking at previously. To do an ANOVA then, you click on Data Analysis

31
00:02:36,450 --> 00:02:42,879
of course, we’re going to do a single-factor ANOVA in this case, and then we’re going

32
00:02:42,879 --> 00:02:48,700
to tell it where our data lives, and we’re going to give it – the data lives right

33
00:02:48,700 --> 00:02:53,769
here. It’s grouped by columns, there’s labels in the first row, and I’m just going

34
00:02:53,769 --> 00:02:59,639
to keep things simple and I’m going to actually just have it put the data into the Excel spreadsheet

35
00:02:59,639 --> 00:03:04,739
that I’m working at right now, just so that you can kind of keep everything altogether

36
00:03:04,739 --> 00:03:11,049
here. And so what you get is output that looks like this – I’m going to make this just

37
00:03:11,049 --> 00:03:16,279
a touch bigger so we can see it; you can see that all of the different elements to the

38
00:03:16,279 --> 00:03:21,309
output, you can see the number of observations for the lemonade in the park, the orangeade

39
00:03:21,309 --> 00:03:24,930
in the park, the lemonade at the beach, the orangeade at the beach, the averages for each

40
00:03:24,930 --> 00:03:31,349
of those. So the question is, is there a statistically significant difference between any of these

41
00:03:31,349 --> 00:03:36,900
groups? So we scroll down here to the ANOVA output, and we can see that our ‘p’ value

42
00:03:36,900 --> 00:03:43,749
is very very small, less than 0.05, and so there is a statistically significant difference.

43
00:03:43,749 --> 00:03:49,069
Now, the question is, where is it? Unfortunately, Excel doesn’t have the capabilities to do

44
00:03:49,069 --> 00:03:54,260
that particular analysis, but you can actually do it yourself using some of the other statistical

45
00:03:54,260 --> 00:03:57,419
tools that we certainly have talked about and some of the formulas that are already

46
00:03:57,419 --> 00:04:02,620
built into Excel. We have the mean, we have the standard deviation right here – I’m

47
00:04:02,620 --> 00:04:06,799
sorry, the mean and the variance right here, and so you can actually do these calculations

48
00:04:06,799 --> 00:04:11,729
using the t-test statistics that I have provided you previously.

49
00:04:11,729 --> 00:04:16,940
So that’s one version of an ANOVA; you can also do other versions of an ANOVA that are

50
00:04:16,940 --> 00:04:21,170
kind of fun to play with and I just want to show you real briefly. The first is an ANOVA

51
00:04:21,170 --> 00:04:27,470
with no replication; again, the data has to be set up at a little bit different format.

52
00:04:27,470 --> 00:04:33,340
So what we want to know in this case is, does the number of sales – are they affected

53
00:04:33,340 --> 00:04:38,750
at the beach or the park more or less when we have leaflets or no leaflets? So that’s

54
00:04:38,750 --> 00:04:42,940
really – the question is are leaflets having any effect on whether or not we’re selling

55
00:04:42,940 --> 00:04:48,130
more at either the beach or the park, and if so, where? And so what we have done here

56
00:04:48,130 --> 00:04:51,380
is I’ve essentially just laid out the data to show you how I came up with these numbers

57
00:04:51,380 --> 00:04:57,400
so you can see what total sales are at the beach when we have leaflets, so we have total

58
00:04:57,400 --> 00:05:02,470
sales at the park, total sales at the beach with no leaflets; so essentially that’s

59
00:05:02,470 --> 00:05:08,180
just the data that I used to populate these values so I could show you this analysis.

60
00:05:08,180 --> 00:05:17,060
So we’re going to do data ANOVA with no replication, and we’re going to input our

61
00:05:17,060 --> 00:05:23,690
values, we have labels again, and again I’m going to just keep this simple because I want

62
00:05:23,690 --> 00:05:27,440
to show this to you really quickly in the same spreadsheet, so we’re just going to

63
00:05:27,440 --> 00:05:34,950
have the data show up in m1, and we’re going to take a look at what the output shows. So

64
00:05:34,950 --> 00:05:39,870
again you get all of the summary statistics that you would normally expect from an analysis,

65
00:05:39,870 --> 00:05:44,210
but the important thing is to go down here and see and understand what the data is telling

66
00:05:44,210 --> 00:05:51,050
us. So if we go back and we look at our data – I’m going to just move that over so

67
00:05:51,050 --> 00:06:05,900
we can see this, I did not mean to make that smaller; I’ll make this big again –  what we can see is that we have a statistically

68
00:06:05,900 --> 00:06:12,120
significant difference by both rows and columns. Now our rows are the leaflet rows, so there

69
00:06:12,120 --> 00:06:16,300
is a statistical significantly different based on whether or not we’re putting leaflets

70
00:06:16,300 --> 00:06:21,060
out; and we also see that there’s a statistically significant different by whether or not we’re

71
00:06:21,060 --> 00:06:26,710
selling at the beach or at the park. Where exactly are the differences? Again, Excel

72
00:06:26,710 --> 00:06:31,800
does not provide that level of detail of analysis, however you have all the information here

73
00:06:31,800 --> 00:06:37,360
that you need so that you can run those – essentially a t-test – to determine where those differences

74
00:06:37,360 --> 00:06:42,840
lie. And then the final ANOVA option in Excel is

75
00:06:42,840 --> 00:06:48,480
the ANOVA with replication. Now the idea here is that you have gathered data across multiple

76
00:06:48,480 --> 00:06:53,560
different periods of time or multiple different observations or for multiple different groups.

77
00:06:53,560 --> 00:06:57,700
And so let’s just take – let’s just imagine this is imaginary data, but let’s

78
00:06:57,700 --> 00:07:01,910
say that we took and we looked at the number of sales when we had no leaflets and a bucket

79
00:07:01,910 --> 00:07:05,910
that we called “low temperatures” and a bucket that we called “moderate temperatures”

80
00:07:05,910 --> 00:07:10,050
and a bucket that we called “high temperatures”. We’re essentially just creating those – something

81
00:07:10,050 --> 00:07:16,450
like a bin in a histogram as it were. So let’s see if there’s anything here in this data

82
00:07:16,450 --> 00:07:22,370
that might show us whether sales are driven by leaflets or not, and by these different

83
00:07:22,370 --> 00:07:26,280
temperature categories. Now the really great thing about me adding a third column here

84
00:07:26,280 --> 00:07:31,160
and looking at data with replication is that we can actually start to look at things called

85
00:07:31,160 --> 00:07:39,360
an interaction. And so, is there some combination of factors that actually is – has a more

86
00:07:39,360 --> 00:07:44,730
effect on our outcome of interests, so what might that be? And that’s what essentially

87
00:07:44,730 --> 00:07:50,190
an interaction is. So to do that, we’re going to select the ANOVA two-factor with

88
00:07:50,190 --> 00:07:54,930
replication, and then we’re going to select our data – where our data are, which is

89
00:07:54,930 --> 00:08:00,970
right here; there are four rows per sample – we have to tell Excel how many rows there

90
00:08:00,970 --> 00:08:04,980
are in each of our samples – and then I’m going to just go ahead and have it add this

91
00:08:04,980 --> 00:08:10,470
to this output to this page, so that it’s easier to find, and then we’re going to

92
00:08:10,470 --> 00:08:16,150
run the analysis. So just like before, you have the summary statistics where you have

93
00:08:16,150 --> 00:08:21,010
the different values for each of your groups – for your leaflets, for no leaflets, the

94
00:08:21,010 --> 00:08:26,930
overall total – but the thing that we care about is really down here in the ANOVA analysis

95
00:08:26,930 --> 00:08:33,820
results. So you can see that our sample, in this case which is to find as the leaflets

96
00:08:33,820 --> 00:08:39,409
or no leaflets, is statistically significant; the columns, which are related to temperature,

97
00:08:39,409 --> 00:08:45,829
are also statistically significant, and then we also have a statistically significant interaction.

98
00:08:45,829 --> 00:08:53,009
Now I personally love interacts because they actually make some really great charts. So

99
00:08:53,009 --> 00:08:58,540
let’s take a look at what this chart might look like really quickly, when we do these

100
00:08:58,540 --> 00:09:04,930
line charts. So here we have the average sales for each of the low, moderate, and high temperatures

101
00:09:04,930 --> 00:09:09,680
when we have no leaflets – I’m just going to do this really quickly by copy and pasting

102
00:09:09,680 --> 00:09:16,819
these values over here into some rows so that we can make this pretty handy – no leaflets,

103
00:09:16,819 --> 00:09:24,680
yes leaflets… and we’re just going to do a quick line chart to show us what exactly

104
00:09:24,680 --> 00:09:31,160
is going on in this data, and you can see that there is a cool interaction effect – another

105
00:09:31,160 --> 00:09:37,300
great thing for charts. Now, what is driving this interaction? That’s something that

106
00:09:37,300 --> 00:09:40,800
you would have to dig a little bit deeper potentially into the data to better understand

107
00:09:40,800 --> 00:09:45,199
that, but there is one and it’s interesting and it’s worth further exploration.

