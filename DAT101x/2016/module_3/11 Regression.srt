0
00:00:05,080 --> 00:00:09,710
Okay, so we’ve seen a lot of ways that we can use statistics in Excel to examine the

1
00:00:09,710 --> 00:00:14,830
data; we can find descriptive statistics, we can find correlations in the data, those

2
00:00:14,830 --> 00:00:19,250
associations that we can do, we can do tests that compare values – I guess for a lot

3
00:00:19,250 --> 00:00:25,070
of people, the kind of goal is to get to that predictive point where I can take some information

4
00:00:25,070 --> 00:00:28,820
that I’ve got and I can apply statistics to that to predict something that I don’t

5
00:00:28,820 --> 00:00:32,619
know. So, I mean classically an example we’re looking at – we might want to look at those

6
00:00:32,619 --> 00:00:37,760
various variables we have about past sales, and use them to predict future sales. So,

7
00:00:37,760 --> 00:00:41,769
how can we approach that sort of thing from a statistical perspective? &gt;&gt; It’s called

8
00:00:41,769 --> 00:00:46,019
regression, and it’s one of the best statistics out there. It’s so powerful in helping you

9
00:00:46,019 --> 00:00:50,299
understand what variables in your data can be used to predict some outcome of interest.

10
00:00:50,299 --> 00:00:58,470
So let’s talk a little bit about what regression is. So if we think about this, the idea here

11
00:00:58,470 --> 00:01:04,580
is that we have some variables or features that we are curious about. In this case, we

12
00:01:04,580 --> 00:01:10,170
might have the flyers, the price, the temperature, and of course what we’re most interested

13
00:01:10,170 --> 00:01:16,170
in understanding is: does that predict sales? So what we have then is we’re essentially

14
00:01:16,170 --> 00:01:22,900
creating a function, as it were. So it’s a function that has a set of these variables

15
00:01:22,900 --> 00:01:29,440
which we call features that predict this outcome which we call a label – or more generically,

16
00:01:29,440 --> 00:01:34,530
what we say, “f of x equals y”. So let’s take a little bit of a closer look at what

17
00:01:34,530 --> 00:01:39,800
we have in terms of some data. So if we plot some data and we see these data points like

18
00:01:39,800 --> 00:01:44,860
this, we can see that there is a pattern in that data. And so what regression is hopefully

19
00:01:44,860 --> 00:01:52,520
going to tell us is, what is the function of x that will best fit what that representation

20
00:01:52,520 --> 00:01:58,530
of the data is? In this case, what we have is a line, and so what we’re going to talk

21
00:01:58,530 --> 00:02:06,060
about is linear regression. And you also have what’s known as an intercept, or that ‘I’

22
00:02:06,060 --> 00:02:10,030
that appears on your screen. That intercept is essentially the constant that is an important

23
00:02:10,030 --> 00:02:16,100
part of the equation that we need to use to predict the outcome or label of interest.

24
00:02:16,100 --> 00:02:22,030
One final analysis that I want to show you in Excel is the regression analysis, so data

25
00:02:22,030 --> 00:02:27,360
analysis pack again – we’re going to find our lovely regression here, and we’re going

26
00:02:27,360 --> 00:02:33,800
to input our y range, which is those labels that we were mentioning in our previous conversation,

27
00:02:33,800 --> 00:02:38,440
in this case, what we want to predict. The outcome variable of interest is going to be

28
00:02:38,440 --> 00:02:43,880
total sales, so we’re going to scroll down here and select the data. Now I do want to

29
00:02:43,880 --> 00:02:49,180
mention one little quirk about using regression in Excel, is that you cannot – you can’t

30
00:02:49,180 --> 00:02:54,060
just click on the columns, you have to actually select exactly where your data lives. If you

31
00:02:54,060 --> 00:02:58,230
select the whole column, you will get an error, and then you’ll remember what Liberty said

32
00:02:58,230 --> 00:03:02,040
and then you’d go in and then select exactly where the data lives for each of your analyses.

33
00:03:02,040 --> 00:03:09,989
So this is where our outcome variable is, and then we’re going to enter our input

34
00:03:09,989 --> 00:03:17,849
variables, and we want to look at: how well does temperature, leaflets, and price predict

35
00:03:17,849 --> 00:03:24,020
our outcome of interest, which is sales? Again, you notice that I selected the top row which

36
00:03:24,020 --> 00:03:28,120
has the labels of the variable names, so I’m going to make sure that I select that; I’m

37
00:03:28,120 --> 00:03:32,180
going to go ahead and just have this put it in a new worksheet because the output for

38
00:03:32,180 --> 00:03:37,120
this particular analysis is quite robust – there’s a lot of it, and so it just makes sense to

39
00:03:37,120 --> 00:03:42,660
put it in a new worksheet. I also am going to have it provide the residuals, the standardized

40
00:03:42,660 --> 00:03:47,569
residuals, the residual plots, and the line fit plots. These are sometimes fun to look

41
00:03:47,569 --> 00:03:52,900
– to see what’s happening within the data, both in terms of the numbers I’m getting,

42
00:03:52,900 --> 00:03:57,900
but also in terms of some of the graphs that are possible with this analysis.

43
00:03:57,900 --> 00:04:03,180
So, we come to our separate new worksheet and we’re going to go ahead – I’m going

44
00:04:03,180 --> 00:04:06,410
to just expand this a little bit so we can take a closer look at what we’re seeing

45
00:04:06,410 --> 00:04:10,540
here – and let’s start at the top where we look at our regression statistics and the

46
00:04:10,540 --> 00:04:15,500
summary output. We have these words called “multiple R”, “R square”, “adjusted

47
00:04:15,500 --> 00:04:20,699
R square”; essentially what R is and what R square is doing is telling you the percent

48
00:04:20,699 --> 00:04:26,610
of variance that is explained by the regression model itself. So the closer to one it is,

49
00:04:26,610 --> 00:04:30,919
the better it is in terms of explaining the variance, so you can see here that we have

50
00:04:30,919 --> 00:04:37,059
quite a good – quite a high number here, 0.91 with an adjusted R square, which shows

51
00:04:37,059 --> 00:04:40,490
that we have a pretty good indicator that the variables that we have included in this

52
00:04:40,490 --> 00:04:45,620
model are explaining quite a bit of the variance that we’re seeing in sales. The next table

53
00:04:45,620 --> 00:04:50,830
that we see is the ANOVA table, which is essentially just telling us if the regression in and of

54
00:04:50,830 --> 00:04:56,370
itself is significant. I got to tell you, it’s explaining over 90 percent of the variance;

55
00:04:56,370 --> 00:05:00,529
you almost don’t even need to look at this table to know it’s going to be significant,

56
00:05:00,529 --> 00:05:06,099
but we do see that the significance is – the significance f, which is the statistics that

57
00:05:06,099 --> 00:05:11,999
is used for regression, is actually less than our p value of 0.05 by quite a margin, so

58
00:05:11,999 --> 00:05:16,849
we do have a statistically significant regression. Now let’s take a closer look at what’s

59
00:05:16,849 --> 00:05:21,889
going on in that regression. You can see that we have the intercept, temperature, leaflets,

60
00:05:21,889 --> 00:05:28,349
and price, and we have this thing called coefficients. This is essentially the weight with which

61
00:05:28,349 --> 00:05:36,789
we would weight that variable when we do – when we create and run our function. So the coefficient

62
00:05:36,789 --> 00:05:42,789
for temperature is 1.42, the coefficient for leaflets is 1.92, and the coefficient for

63
00:05:42,789 --> 00:05:50,569
price is -88.89. You have some other information here that tells you whether or not that element

64
00:05:50,569 --> 00:05:58,020
of the model is actually statistically significant, so you can see that temperature is not by

65
00:05:58,020 --> 00:06:03,180
our definition; it is not less than 0.05, it is actually greater than that. Some people

66
00:06:03,180 --> 00:06:08,770
might argue, well, it’s marginally significant because it’s close, but if you’re going

67
00:06:08,770 --> 00:06:14,110
to do this and you’re going to do it right, if it says 0.05 and it’s not less than 0.05

68
00:06:14,110 --> 00:06:19,330
you cannot call that significant. You could see that the other two variables in our model,

69
00:06:19,330 --> 00:06:24,719
however, are – leaflets and price, and they’re both very – have very small p values so

70
00:06:24,719 --> 00:06:30,189
they’re very significant. As we scroll down the page, you can see that we have what’s

71
00:06:30,189 --> 00:06:35,059
called “residual output”; remember I asked for it to provide me with residuals. So what

72
00:06:35,059 --> 00:06:39,659
you can see actually is that you get this predicted total sales, and then you have this

73
00:06:39,659 --> 00:06:46,369
value that’s the residual. This is actually the difference between the predicted amount

74
00:06:46,369 --> 00:06:51,479
and what you actually saw for sales for that first observation. So how does this work?

75
00:06:51,479 --> 00:06:56,210
Well, the really really cool thing about regression is there truly is a function or an equation

76
00:06:56,210 --> 00:07:00,659
that you can use and you can calculate yourself, because the idea is that you want to be able

77
00:07:00,659 --> 00:07:06,029
to use this equation to predict what future sales might be if you change some of these

78
00:07:06,029 --> 00:07:12,860
variables around. So, the way that this works is like this: you essentially are going to

79
00:07:12,860 --> 00:07:19,520
do – to take the intercept, and you’re going to add it to the weighted temperature

80
00:07:19,520 --> 00:07:24,169
that you obtained for each of your observations. So let’s just take the first observation

81
00:07:24,169 --> 00:07:30,219
to show you how this works. So the weighted temperature is 1.42, we’re going to multiply

82
00:07:30,219 --> 00:07:35,059
that by the temperature we actually saw on day one – so let’s make sure we get to

83
00:07:35,059 --> 00:07:43,210
day one – which was 71, we’re going to add that to the weighted leaflets, which is

84
00:07:43,210 --> 00:07:49,889
multiplied by the number of leaflets that we actually distributed on day one, which

85
00:07:49,889 --> 00:08:00,559
is added to the weighted price that we actually sold our lemonades and orangeades for on day

86
00:08:00,559 --> 00:08:11,339
one; so you can see that I got the same value that was obtained from Excel. Now, if we go

87
00:08:11,339 --> 00:08:17,009
back and we look at this, we see that on day one, we had sales of 182. So this is how we

88
00:08:17,009 --> 00:08:29,509
get the residual: 182 – so we’re going to subtract 182, 177, and we get our residual;

89
00:08:29,509 --> 00:08:36,089
so that’s essentially how the output works. Now you don’t have to do all that math because

90
00:08:36,089 --> 00:08:41,520
the Excel program has done it for you, but the idea is now I have this equation; what

91
00:08:41,520 --> 00:08:48,240
if I wanted to change the number of leaflets or I wanted to change the number of – or

92
00:08:48,240 --> 00:08:51,210
I wanted to see what would happen if there was a different temperature that day or if

93
00:08:51,210 --> 00:08:55,900
I wanted to change the price? I then – by just calculating it and plugging it into this

94
00:08:55,900 --> 00:09:00,390
function – I can actually get a pretty good estimate of what my sales are likely to be

95
00:09:00,390 --> 00:09:08,210
under a variety of different conditions. Now, I also asked it to create some line – some

96
00:09:08,210 --> 00:09:13,960
plots. The plots that are, kind of, of most interest to me are the ones of the line plots,

97
00:09:13,960 --> 00:09:18,960
so let’s take a look at a couple of those – I’m going to move them over here so

98
00:09:18,960 --> 00:09:26,080
you can see them a little bit better. So what you can see is actually, it aligns very nicely

99
00:09:26,080 --> 00:09:31,280
with what we saw when we looked at the statistics and what was significant. You can see the

100
00:09:31,280 --> 00:09:37,340
temperature – the predicted values, the orange values, are kind of a little scattered

101
00:09:37,340 --> 00:09:42,030
along the line, and if you could see the blue values just a little bit better, you can see

102
00:09:42,030 --> 00:09:46,900
that they’re a little more scattered; especially when you compare them to the leaflets line.

103
00:09:46,900 --> 00:09:52,020
This is much more structured and tightly bound, and so you do see that there’s a stronger

104
00:09:52,020 --> 00:09:57,460
relationship for leaflets than there is for temperature, which is just another way to

105
00:09:57,460 --> 00:10:02,950
really illustrate why one is a little more significant than the other. Now there’s

106
00:10:02,950 --> 00:10:09,180
one other kind of neat thing that I want to show you related to regressions, and that

107
00:10:09,180 --> 00:10:14,750
is what we call a “standardized regression equation”. And to do that, you actually

108
00:10:14,750 --> 00:10:20,220
need to know what the variance is, for each of the variables that you have in your model

109
00:10:20,220 --> 00:10:25,570
and for the outcome. So we do still have the descriptor’s data that we had from before;

110
00:10:25,570 --> 00:10:32,560
so, remember this. We can actually calculate the standardized beta coefficients by using

111
00:10:32,560 --> 00:10:37,920
the variance and the standard deviation obtained from this analysis, and so this is kind of

112
00:10:37,920 --> 00:10:44,240
a cool thing that I want to show you. So, what is the standardized regression coefficient

113
00:10:44,240 --> 00:10:50,610
for leaflets and price? The reason why this matters is because: which one is actually

114
00:10:50,610 --> 00:10:57,880
more important in the prediction of our outcome? You really can’t tell that just looking

115
00:10:57,880 --> 00:11:03,810
at this data because these are on different scales; it’s a different metric. So, to

116
00:11:03,810 --> 00:11:09,750
understand if one is more important than the other, we need to look at the standardized

117
00:11:09,750 --> 00:11:17,970
coefficients. And it’s a really simple math conversion, where you take the leaflet coefficient,

118
00:11:17,970 --> 00:11:30,150
you multiply it by its standard deviation, and you divide it by the standard deviation

119
00:11:30,150 --> 00:11:36,540
of the outcome of interest, and so you can see that the standardized beta coefficient

120
00:11:36,540 --> 00:11:54,780
for leaflets is 0.88 – or 0.83. Now let’s do this for price as well; and we can see

121
00:11:54,780 --> 00:12:03,540
that the price is -0.26. So, what this is telling me is that leaflets are actually more

122
00:12:03,540 --> 00:12:08,070
important in the prediction of our outcome than is price.

123
00:12:08,070 --> 00:12:13,970
&gt;&gt; Well, there you have it, that’s your orientation to the data science program. Hopefully

124
00:12:13,970 --> 00:12:18,610
you found that useful and interesting, and hopefully you’ve enjoyed following along

125
00:12:18,610 --> 00:12:24,070
with Liberty and I as we’ve taken a little look at some of the basics of exploring data

126
00:12:24,070 --> 00:12:27,810
and some statistics. For the rest of the program, you’re going to build on that; you’re

127
00:12:27,810 --> 00:12:31,490
going to be looking at a lot more interesting technologies and interesting techniques that

128
00:12:31,490 --> 00:12:35,380
you could use to explore data and to build predictive models from data, and I hope you’re

129
00:12:35,380 --> 00:12:40,110
going to enjoy the rest of the program. But from Liberty and I for just now, goodbye,

130
00:12:40,110 --> 00:12:40,760
and we’ll see you next time.

