0
00:00:03,940 --> 00:00:08,020
I think the next part we&#39;re going to do is we&#39;re going to run people

1
00:00:08,020 --> 00:00:10,750
through a basic introduction to statistics

2
00:00:10,750 --> 00:00:15,250
yeah it&#39;s not a full-on stats course but i think one of the important things if

3
00:00:15,250 --> 00:00:18,070
you&#39;re going to use there any of the techniques we&#39;ve talked about in terms

4
00:00:18,070 --> 00:00:22,030
of using Excel to explore data it&#39;s good to have an idea of some statistical

5
00:00:22,030 --> 00:00:25,780
underlying methodologies and thinking that you can apply to data science yeah

6
00:00:25,780 --> 00:00:30,039
yeah so let&#39;s take a look a little bit at some of the more basic statistics

7
00:00:30,039 --> 00:00:34,000
that you can run and understand what&#39;s going on with your data so i guess we

8
00:00:34,000 --> 00:00:38,379
might be wondering why we would be interested in statistics and their

9
00:00:38,379 --> 00:00:42,670
Rosie&#39;s perhaps wondering about her lemonade sales and how she can apply

10
00:00:42,670 --> 00:00:46,600
statistics to that to improve the way that she performs and she&#39;s selling

11
00:00:46,600 --> 00:00:51,819
eliminate so the thing about statistics is it&#39;s all about the variables that

12
00:00:51,819 --> 00:00:55,359
you&#39;re dealing with this all about working primarily with numbers and

13
00:00:55,359 --> 00:00:58,300
working with those variables and I guess what the first thing we need to

14
00:00:58,300 --> 00:01:01,809
understand the types of variables that we can be working with when we use

15
00:01:01,809 --> 00:01:07,299
statistics so the first type of variable that we we might want to think about our

16
00:01:07,299 --> 00:01:11,050
something we call continuous variables so think about numbers that are

17
00:01:11,050 --> 00:01:15,610
continuous there on a scale and there we typically measure them so good example

18
00:01:15,610 --> 00:01:20,200
maybe temperature and Rosie might have the the temperature in each day when

19
00:01:20,200 --> 00:01:24,940
she&#39;s selling her lemonade and that can vary between several different levels

20
00:01:24,940 --> 00:01:30,700
over the course of the the summer so we&#39;ve gotta add a numerical value that

21
00:01:30,700 --> 00:01:36,490
is on a continuous scale and it can be any value on that scale between my so we

22
00:01:36,490 --> 00:01:40,420
could be below zero i guess between 0 &amp; and let&#39;s say a hundred for for the sake

23
00:01:40,420 --> 00:01:45,280
of argument know it as well as those kind of continuous numbers there might

24
00:01:45,280 --> 00:01:49,960
be numbers that are discreet and what we mean by there are things that are still

25
00:01:49,960 --> 00:01:53,800
numerical in nature we can still do calculations and so on with them but you

26
00:01:53,800 --> 00:01:57,550
you count them rather than measure them so they&#39;re their individual discrete

27
00:01:57,550 --> 00:02:02,080
units that you kind good example lightmap maybe the number of leaflets

28
00:02:02,080 --> 00:02:07,450
that the rosy distributes so she she can reach distribute our number of leaflets

29
00:02:07,450 --> 00:02:10,360
but they&#39;re always going to be a unit of one leaflet she couldn&#39;t distribute a

30
00:02:10,360 --> 00:02:15,730
1.5 leaflets or 1.76 three leaflets or something like that so it&#39;s a discrete

31
00:02:15,730 --> 00:02:17,080
number that you count

32
00:02:17,080 --> 00:02:20,650
rather than measure that&#39;s the the kinect key difference between continuous

33
00:02:20,650 --> 00:02:25,900
variables and discrete variables and both of these are examples of numerical

34
00:02:25,900 --> 00:02:30,760
variables and that the other thing you might have our are categorical variables

35
00:02:30,760 --> 00:02:37,090
know in some senses the this could just be strings so for example we&#39;ve got Lucy

36
00:02:37,090 --> 00:02:43,600
selling her lemonade at both the beach and the park and we can see in our r XL

37
00:02:43,600 --> 00:02:47,290
father be working but just got the word beach or park to tell us where which of

38
00:02:47,290 --> 00:02:50,620
those two things it was those are the categories of sales that are seals that

39
00:02:50,620 --> 00:02:54,640
are sales at the beach and skills are sales at the park now as it turns out

40
00:02:54,640 --> 00:02:58,990
most statistical functions that we work with and most of the kind of machine

41
00:02:58,990 --> 00:03:03,610
learning things that the data scientists do and tend to prefer to work with

42
00:03:03,610 --> 00:03:07,840
numbers rather than strings are with characters so what we might actually do

43
00:03:07,840 --> 00:03:12,340
is apply numerical labels to these different things we might say that will

44
00:03:12,340 --> 00:03:15,970
whenever we sell something the beach will record a one and whenever we sell

45
00:03:15,970 --> 00:03:21,850
something at the the park will record a too so they&#39;re still categorical

46
00:03:21,850 --> 00:03:25,750
variables we&#39;ve applied numbers to them but the numbers aren&#39;t and numbers in

47
00:03:25,750 --> 00:03:29,350
the sense that we do any numerical calculation with them they&#39;re just

48
00:03:29,350 --> 00:03:35,380
simply labels for those two different types of things so these are the three

49
00:03:35,380 --> 00:03:40,660
different types of variable that the data that we&#39;re working with might fall

50
00:03:40,660 --> 00:03:45,400
into and this is important because you can do different types of analyses based

51
00:03:45,400 --> 00:03:49,989
on what kind of data you&#39;re actually dealing with so with that in mind let&#39;s

52
00:03:49,989 --> 00:03:54,519
take a look at one more key concept that we need to kind of master before we step

53
00:03:54,519 --> 00:03:57,820
into the basics of statistics and that&#39;s the notion of the difference between a

54
00:03:57,820 --> 00:04:05,019
population and a sample so in this case if you were to look at keeping track of

55
00:04:05,019 --> 00:04:09,820
all of the sales of drinks that sheet as takes in every single day you&#39;ll end up

56
00:04:09,820 --> 00:04:14,230
with the complete set of data values that represent the daily sales over time

57
00:04:14,230 --> 00:04:18,609
the roses kept track of every lemonade she&#39;s ever sold and we know exactly what

58
00:04:18,608 --> 00:04:23,410
the total number of the the total data for all time yep so let&#39;s let&#39;s just you

59
00:04:23,410 --> 00:04:26,860
know take this little one step further and think about this let&#39;s say you kept

60
00:04:26,860 --> 00:04:29,620
track of i sold this lemonade at this time

61
00:04:29,620 --> 00:04:32,590
was this temperature because those are the things we are carrying about in this

62
00:04:32,590 --> 00:04:38,949
dataset and so that is what you call the population of data now it&#39;s very

63
00:04:38,949 --> 00:04:43,660
unlikely that you&#39;re ever going to have access to that kind of information and

64
00:04:43,660 --> 00:04:46,449
can you imagine just trying to keep track of that every time she sold

65
00:04:46,449 --> 00:04:49,600
lemonade or jets have to mark that down on a sheet and then she had the

66
00:04:49,600 --> 00:04:54,310
population so typically when we talk about statistics were actually dealing

67
00:04:54,310 --> 00:05:00,940
with what we call a sample before we are and in that sample is it&#39;s just one

68
00:05:00,940 --> 00:05:06,400
small piece of the data now one thing you&#39;ll notice about this and the slide

69
00:05:06,400 --> 00:05:10,090
actually is that you&#39;ll notice that their big x&#39;s or capital X is

70
00:05:10,090 --> 00:05:13,990
representing the population and you look at the there&#39;s little axis representing

71
00:05:13,990 --> 00:05:18,880
the sample what&#39;s interesting here and when we look at this is the in

72
00:05:18,880 --> 00:05:24,039
statistics we used slightly different letters or abbreviations when we&#39;re

73
00:05:24,039 --> 00:05:27,220
talking about statistics that are related to the population versus those

74
00:05:27,220 --> 00:05:32,020
that are related to the sample generally when you see a Greek symbol that&#39;s

75
00:05:32,020 --> 00:05:36,610
referring to the population and when you see a non-greek symbol that&#39;s referring

76
00:05:36,610 --> 00:05:41,650
to the sample in general and in this case is the only example in this

77
00:05:41,650 --> 00:05:46,270
particular case what we&#39;re saying is when up with the food population a large

78
00:05:46,270 --> 00:05:49,900
n is the total number of things in the population of the total number of

79
00:05:49,900 --> 00:05:53,800
observations I guess is the proper term for the total number of bits of data

80
00:05:53,800 --> 00:05:58,360
that I&#39;ve got and then we we we identify which one of these things within the

81
00:05:58,360 --> 00:06:02,020
population looking out by saying this is x1 or x2 x3 and so on all the way up to

82
00:06:02,020 --> 00:06:08,440
xn all right correct and then if I&#39;ve gotta a sample of that sort of taken and

83
00:06:08,440 --> 00:06:11,199
presently when you take a sample you you&#39;re going to do it so that it&#39;s

84
00:06:11,199 --> 00:06:14,139
representative of the whole population you know just the first five you take

85
00:06:14,139 --> 00:06:18,490
something that&#39;s gonna representative and again identify the individual

86
00:06:18,490 --> 00:06:23,139
members as X 1 X 2 X the butt but the convention is I use a lowercase a tract

87
00:06:23,139 --> 00:06:27,520
and the Lower Keys end to tell me that the total number so the principle is the

88
00:06:27,520 --> 00:06:31,990
same but who because we&#39;re dealing with the difference between up the population

89
00:06:31,990 --> 00:06:36,520
and a sample that the the actual where the syntax we use a few like this is

90
00:06:36,520 --> 00:06:39,760
slightly different all right that is correct and we do that so that the

91
00:06:39,760 --> 00:06:42,110
average person is looking at a rope

92
00:06:42,110 --> 00:06:45,020
we&#39;re looking at the statistics knows whether we&#39;re talking about population

93
00:06:45,020 --> 00:06:47,600
statistics or if we&#39;re talking about a sample statistic green

