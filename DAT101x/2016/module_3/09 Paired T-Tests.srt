0
00:00:03,900 --> 00:00:10,350
alright so we were to look at a number of ways are comparing using t-test I

1
00:00:10,350 --> 00:00:13,889
guess there&#39;s there&#39;s slightly more complicated and check that might want to

2
00:00:13,889 --> 00:00:19,289
make so for example if historically rosie has been selling lemonade every

3
00:00:19,289 --> 00:00:23,759
year and she&#39;s this year she&#39;s handing out leaflets to help people find maybe

4
00:00:23,759 --> 00:00:26,400
she didn&#39;t hand out leaflets last year so it might be interesting to find out

5
00:00:26,400 --> 00:00:31,439
as that changing meter statistically viable difference to what she&#39;s doing

6
00:00:31,439 --> 00:00:36,600
yeah so that we can actually do with what&#39;s called a paired samples T test if

7
00:00:36,600 --> 00:00:41,280
we take a look then last year she wasn&#39;t delivering any flyers she didn&#39;t post

8
00:00:41,280 --> 00:00:45,060
anything like that and so her sails might have looked like this this year

9
00:00:45,060 --> 00:00:48,810
she does start posting Flyers handing out leaflets in her sales look a little

10
00:00:48,810 --> 00:00:50,040
like this

11
00:00:50,040 --> 00:00:55,440
so is this a statistically significant difference does posting flyers and

12
00:00:55,440 --> 00:00:59,160
handing out these leaflets make a difference and specifically i&#39;m

13
00:00:59,160 --> 00:01:03,570
interested in looking at comparing the individual days last year with the same

14
00:01:03,570 --> 00:01:08,190
days this year so for example in Liberty last year i sold so many lemonade&#39;s and

15
00:01:08,190 --> 00:01:11,370
I didn&#39;t have any leaflets and on Labor Day this year I did handouts and

16
00:01:11,370 --> 00:01:14,700
leaflets and i sold a different amount of of lemonade and that&#39;s to freeze days

17
00:01:14,700 --> 00:01:18,900
I want to pair up the days from last year with this year and compare the

18
00:01:18,900 --> 00:01:23,520
seals reach the last year with the seals each day this year to do that we do a

19
00:01:23,520 --> 00:01:29,159
paired sample t-test which is the math like this that d with the bar above it

20
00:01:29,159 --> 00:01:33,659
is essentially just the average difference between the two two time

21
00:01:33,659 --> 00:01:38,430
periods that were looking at again / a version of the standard error and we

22
00:01:38,430 --> 00:01:40,730
also have our degrees of freedom

23
00:01:40,730 --> 00:01:45,020
before we actually do the paired sample t-test I want to talk a little bit about

24
00:01:45,020 --> 00:01:48,290
the data structure now there&#39;s a couple of different ways that you can structure

25
00:01:48,290 --> 00:01:52,910
the data in excel to run this analysis what I have chosen to do is actually

26
00:01:52,910 --> 00:01:58,910
append the data from last year to the bottom of my file for the 2015 data so

27
00:01:58,910 --> 00:02:03,470
if you scroll down here you can see that i have added 2014 that those data

28
00:02:03,470 --> 00:02:05,570
results to the bottom of this file

29
00:02:05,570 --> 00:02:11,600
you could also do this so that you put the 2015 data right here and your 2014

30
00:02:11,600 --> 00:02:15,920
data or your previous year&#39;s data over and other columns that extend the data

31
00:02:15,920 --> 00:02:18,950
to the right this is wide

32
00:02:18,950 --> 00:02:21,650
however you want to do it it&#39;s your choice whatever you&#39;re most comfortable

33
00:02:21,650 --> 00:02:22,580
with

34
00:02:22,580 --> 00:02:27,110
just understand that both parts of the data need to be available to you to run

35
00:02:27,110 --> 00:02:32,870
the analysis so to do the paired sample t-test you go to data analysis you

36
00:02:32,870 --> 00:02:38,330
select your paired to sample for means click ok and then what I&#39;m going to do

37
00:02:38,330 --> 00:02:41,480
is I&#39;m going to make sure that i have selected the right data so I&#39;m going to

38
00:02:41,480 --> 00:02:46,070
put this year&#39;s data in the first cells i&#39;m going to scroll down until I get to

39
00:02:46,070 --> 00:02:51,860
the bottom of my 2015 and that will be the end of that data and then I&#39;m going

40
00:02:51,860 --> 00:02:56,989
to add the 2014 data to the variable number to the end of that

41
00:02:58,860 --> 00:03:02,550
have a hypothesized difference again just like with the previous examples i

42
00:03:02,550 --> 00:03:06,120
can throw in here any number i want we&#39;re going to say that there is no

43
00:03:06,120 --> 00:03:09,750
difference we don&#39;t we didn&#39;t think there would be one we&#39;re trying this out

44
00:03:09,750 --> 00:03:12,600
we don&#39;t know what the difference might be so we&#39;re just gonna go with the

45
00:03:12,600 --> 00:03:17,220
simple there is no difference in this case I don&#39;t have labels so I&#39;m going to

46
00:03:17,220 --> 00:03:22,320
leave that blank and i&#39;m going to click OK and just to be clear what we are

47
00:03:22,320 --> 00:03:27,209
actually comparing here are the individual payers of days from each year

48
00:03:27,209 --> 00:03:30,540
so the first Johnny for the first year against the first of January for the

49
00:03:30,540 --> 00:03:34,500
second year and so on we&#39;re not comparing the the overall mean for the

50
00:03:34,500 --> 00:03:38,310
year against the overwhelming for the previous year we&#39;re comparing on an

51
00:03:38,310 --> 00:03:44,640
individual pair day basis and then we change this or at least the major change

52
00:03:44,640 --> 00:03:48,300
between those time periods that for one of the years there were the number of

53
00:03:48,300 --> 00:03:52,290
the leaflets that we distributed on each day and in the previous year there were

54
00:03:52,290 --> 00:03:54,420
no leaflets distributed on that day

55
00:03:54,420 --> 00:04:00,180
exactly so what we can see here is we get the data back and we get the same

56
00:04:00,180 --> 00:04:05,160
format of the data that we got previously now variable one is that

57
00:04:05,160 --> 00:04:08,790
variable that I first put in and I know that was 2015 so I&#39;m just going to

58
00:04:08,790 --> 00:04:12,060
change that so I keep this all straight in my head in terms of understanding

59
00:04:12,060 --> 00:04:18,359
what this analysis shows so is there a difference in terms of what happens when

60
00:04:18,358 --> 00:04:20,220
she starts posting flyers

61
00:04:20,220 --> 00:04:24,030
well we can see that the means are different and if we go down here and we

62
00:04:24,030 --> 00:04:28,500
have our look at our outcomes and our output in our analysis we can see that

63
00:04:28,500 --> 00:04:33,060
both a one-tailed and a two-tailed tests are statistically significant

