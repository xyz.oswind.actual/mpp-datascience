0
00:00:05,120 --> 00:00:09,660
So we find ourselves back in our data with Rosie’s lemonade stand, and we want to start

1
00:00:09,660 --> 00:00:15,160
doing some statistical analysis on this. The way we start, though, is we want to make sure

2
00:00:15,160 --> 00:00:19,980
we have installed the data analysis pack – that’s one of the add-ins within Excel. To do that,

3
00:00:19,980 --> 00:00:28,020
we go to File, we click Options, we go to Add-Ins; down here where it says Excel Add-ins,

4
00:00:28,020 --> 00:00:34,420
click Go; we’re going to add the Analysis ToolPack, and then the VBA pack just in case

5
00:00:34,420 --> 00:00:40,440
– I’m not entirely sure why we want to do that except that I always do. Now, once

6
00:00:40,440 --> 00:00:44,719
we have installed that, what we can do is we can click on the Data ribbon and then we

7
00:00:44,719 --> 00:00:50,170
will now see we have another option called “Data Analysis”. Let’s take a look at

8
00:00:50,170 --> 00:00:54,199
the Data Analysis options that we have for those descriptive statistics that we just

9
00:00:54,199 --> 00:01:00,050
talked about. To do that, what I’m going to do is I’m going to click Data Analysis,

10
00:01:00,050 --> 00:01:04,940
and it’s almost always set up so that when you first launch it, it does – it links

11
00:01:04,940 --> 00:01:08,840
right to Descriptive Statistics Because that’s where you generally start when it comes to

12
00:01:08,840 --> 00:01:14,940
looking at your data. So I’m going to click OK, and then I am going to input the range

13
00:01:14,940 --> 00:01:19,500
with which I’m curious about learning some of these descriptive statistics. So let’s

14
00:01:19,500 --> 00:01:25,470
go ahead and put lemonade, orangeade, temperature, leaflets, price, sales, and revenue into our

15
00:01:25,470 --> 00:01:30,820
Descriptive Statistics analysis. Now, you saw that I highlighted each of those columns

16
00:01:30,820 --> 00:01:35,890
and each of those columns have labels in the first row, so I’m going to check that box.

17
00:01:35,890 --> 00:01:40,280
The other thing you need to do is you need to also check “Summary Statistics”. I’m

18
00:01:40,280 --> 00:01:44,040
going to show you what happens if you don’t, so you can see what this error looks like:

19
00:01:44,040 --> 00:01:49,420
it’s going to basically say that you haven’t told it to do anything. So click OK, it’s

20
00:01:49,420 --> 00:01:53,700
going to pop back up, and you have to select at least one of these boxes. Well you want

21
00:01:53,700 --> 00:01:59,820
to see the descriptive statistics, we’re going to click summary statistics, click OK,

22
00:01:59,820 --> 00:02:03,350
and then it’s going to populate a separate window or separate sheet – because I told

23
00:02:03,350 --> 00:02:09,459
it to – with each of these values; descriptive statistics for each of our variables. So let’s

24
00:02:09,459 --> 00:02:12,580
take a closer look at what we see in this data.

25
00:02:12,580 --> 00:02:16,469
Starting with lemonade, you can see that we have that the mean or the average sales across

26
00:02:16,469 --> 00:02:24,010
31 days in July was 129; for orangeade, it was 89, and so on. We also have the standard

27
00:02:24,010 --> 00:02:29,689
error – remember, the standard error is essentially how close we are to the population

28
00:02:29,689 --> 00:02:33,730
mean, because every time we collect a sample, we’re going to have a slightly different

29
00:02:33,730 --> 00:02:37,590
mean that is a result and so the standard error gives us an estimate of how close we

30
00:02:37,590 --> 00:02:44,359
are. We have the median – in this case it is 128; the mode in this case is 109. If you

31
00:02:44,359 --> 00:02:49,439
scroll over here which is kind of interesting, is that you see for revenue, you get a “not

32
00:02:49,439 --> 00:02:55,120
applicable”. That’s because there’s no one single value in our revenue data that

33
00:02:55,120 --> 00:03:00,170
is more common than anything else, so it doesn’t know what to calculate so it doesn’t give

34
00:03:00,170 --> 00:03:05,749
you anything. We have the standard deviation, the variance, and then we start getting into

35
00:03:05,749 --> 00:03:12,379
a couple of – we see two indicators of statistics that are related to the shape of the distribution:

36
00:03:12,379 --> 00:03:19,579
Kurtosis and Skewness. Kurtosis is an estimate of the normality of the data; the closer it

37
00:03:19,579 --> 00:03:25,049
is to 0, the more likely the data is normally distributed. Skewness has to do with those

38
00:03:25,049 --> 00:03:30,340
tails that we saw in our previous example. If you see a positive number, that means that

39
00:03:30,340 --> 00:03:34,450
it’s right-skewed; if you see a negative number, it is left-skewed, so it’s just

40
00:03:34,450 --> 00:03:40,519
telling you the direction of the tails. In both cases, values between minus 2 and plus

41
00:03:40,519 --> 00:03:46,290
2 are considered okay for statistical analysis. So just a quick look here to make sure that

42
00:03:46,290 --> 00:03:51,519
everything seems appropriate so that you’re dealing with a normally distributed data,

43
00:03:51,519 --> 00:03:56,900
and nothing seems out of whack here. We have the range, and then we have the min-max, the

44
00:03:56,900 --> 00:03:59,230
sum, and then the count or the number of observations in our data.

