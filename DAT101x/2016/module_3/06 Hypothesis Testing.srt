0
00:00:03,900 --> 00:00:09,930
alright so we got a good idea about how we can use correlation to see potential

1
00:00:09,930 --> 00:00:14,610
relationships between our various data points that we have in

2
00:00:14,610 --> 00:00:18,539
our data set and a good point about correlation not necessarily being

3
00:00:18,539 --> 00:00:22,349
causation and it was like everything we&#39;ve talked about so far a lot of this is

4
00:00:22,349 --> 00:00:27,689
about finding hypotheses and then trying to bear that out. let&#39;s talk about where the

5
00:00:27,689 --> 00:00:31,919
statistical analysis actually starts and that&#39;s what we call hypothesis testing

6
00:00:31,919 --> 00:00:36,960
check and see rules here and she&#39;s a clearly selling her lemonade and I guess

7
00:00:36,960 --> 00:00:40,050
she might be looking at the amount of lemonade she&#39;s sold so maybe one

8
00:00:40,050 --> 00:00:43,980
month&#39;s you saw the whole whole bunch and maybe another month and a slightly

9
00:00:43,980 --> 00:00:45,030
different amount

10
00:00:45,030 --> 00:00:51,060
so how would she apply statistics to figure out is there some explanation for

11
00:00:51,060 --> 00:00:53,880
the difference in these months or is it just random chance sometimes you

12
00:00:53,880 --> 00:00:56,880
sell more something you said last well that&#39;s actually the question we&#39;re

13
00:00:56,880 --> 00:01:00,510
trying to answer with statistics actually and so what you start with is

14
00:01:00,510 --> 00:01:04,530
the question which forms what we call hypothesis and we actually have two

15
00:01:04,530 --> 00:01:07,830
different types of hypotheses when it comes to statistics we talk about the

16
00:01:07,830 --> 00:01:11,460
null hypothesis which is essentially there is no difference

17
00:01:11,460 --> 00:01:14,550
so even though it looks like there&#39;s a difference I mean clearly she looks she

18
00:01:14,550 --> 00:01:18,090
sold six lemonade&#39;s in one month and five so there&#39;s a difference but the

19
00:01:18,090 --> 00:01:22,710
question is is it statistically meaningful does it really matter and so

20
00:01:22,710 --> 00:01:26,550
the null hypothesis is proposing is that it doesn&#39;t there&#39;s no difference what

21
00:01:26,550 --> 00:01:31,800
that one lemonade difference is due to chance to just randomly about Bo you&#39;re

22
00:01:31,800 --> 00:01:34,950
going to get someone to you some more than others but there&#39;s no real sort of

23
00:01:34,950 --> 00:01:39,180
factor lip that cause that it&#39;s just sometimes think we have a chance

24
00:01:39,180 --> 00:01:42,360
absolutely and then you have the alternate hypothesis where you&#39;re

25
00:01:42,360 --> 00:01:48,570
actually asking yourself i was there a difference that was is there truly a

26
00:01:48,570 --> 00:01:52,380
significant difference is it meaningful the alternate hypothesis is proposing

27
00:01:52,380 --> 00:01:56,130
that in fact is you can actually three different variations of an alternate

28
00:01:56,130 --> 00:02:01,110
hypothesis you might propose that there a difference is actually greater it

29
00:02:01,110 --> 00:02:04,229
might be less and then you might not actually know what direction the

30
00:02:04,229 --> 00:02:08,220
difference is just that there should be a difference and so you can have the

31
00:02:08,220 --> 00:02:12,180
alternate hypothesis can actually take on three different flavors as it were

32
00:02:12,719 --> 00:02:16,109
so I guess my challenge then is the figure what what&#39;s the probability that

33
00:02:16,109 --> 00:02:17,460
this was this was it

34
00:02:17,460 --> 00:02:20,580
the null hypothesis is true what&#39;s the probability that this is just chance

35
00:02:20,580 --> 00:02:23,940
well that&#39;s a that&#39;s the person X part of the equation you have to kind of

36
00:02:23,940 --> 00:02:27,570
consider is what is the probability you&#39;re willing to accept that she could

37
00:02:27,570 --> 00:02:31,860
be wrong but the statistic that you get let&#39;s assume that it is significant that

38
00:02:31,860 --> 00:02:36,090
that sir tips that statistic is actually meaningful how much risk are you willing

39
00:02:36,090 --> 00:02:39,900
to assume that it&#39;s wrong because there&#39;s always a little bit of wiggle

40
00:02:39,900 --> 00:02:43,470
room when it comes to statistics it could be that you get a stitch

41
00:02:43,470 --> 00:02:48,030
statistically significant result but in fact it really was due to chance

42
00:02:48,030 --> 00:02:51,540
so that&#39;s what we call a p-value is how much risk are you willing to assume that

43
00:02:51,540 --> 00:02:52,890
it could be wrong

44
00:02:52,890 --> 00:02:58,380
most statisticians look at p values of . 05 so they&#39;re willing to say that I&#39;m

45
00:02:58,380 --> 00:03:02,430
willing to go about five like five percent that&#39;s how much risk I&#39;m willing

46
00:03:02,430 --> 00:03:05,910
to assume that there&#39;s a five percent probability that the statistical

47
00:03:05,910 --> 00:03:11,610
significant result that I obtained was in fact actually due to chance now in

48
00:03:11,610 --> 00:03:14,280
some areas that actually matters a lot

49
00:03:14,280 --> 00:03:17,580
so let&#39;s imagine you&#39;re in the medical field and you&#39;re deciding whether you

50
00:03:17,580 --> 00:03:21,390
should recommend a drug for somebody and so you might not think five percent wow

51
00:03:21,390 --> 00:03:27,060
that&#39;s a pretty big space to be wrong and so you might choose a p-value . on

52
00:03:27,060 --> 00:03:30,480
one you&#39;re willing to accept a one-percent chance that she could be

53
00:03:30,480 --> 00:03:35,520
wrong or . 001 so the p-value is actually really important to decide on

54
00:03:35,520 --> 00:03:39,360
before you run your statistical analysis because it&#39;s essentially setting the

55
00:03:39,360 --> 00:03:43,260
criteria with which you&#39;re going to say something is statistically significant

56
00:03:43,260 --> 00:03:48,180
and that&#39;s meaningful to in this case we&#39;re saying that that we we accept that

57
00:03:48,180 --> 00:03:52,680
if it turns out to be five percent were saying there&#39;s a five percent chance the

58
00:03:52,680 --> 00:03:55,680
five percent probability this this difference is to just really done a

59
00:03:55,680 --> 00:03:56,400
chance

60
00:03:56,400 --> 00:04:00,270
what we&#39;re actually saying is that if we get a statistical significant result

61
00:04:00,270 --> 00:04:05,220
that it&#39;s a five percent chance that we were wrong that in fact is due to chance

62
00:04:05,220 --> 00:04:10,410
we have said that it is statistically significant but it actually isn&#39;t right

63
00:04:10,410 --> 00:04:12,480
that&#39;s what that the p-value actually means

