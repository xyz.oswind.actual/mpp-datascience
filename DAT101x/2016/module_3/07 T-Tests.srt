0
00:00:03,910 --> 00:00:08,440
so i guess one of the things we might want to do is is we&#39;ve talked about

1
00:00:08,440 --> 00:00:12,130
comparative statistics a little bit and we want to talk about perhaps comparing

2
00:00:12,130 --> 00:00:15,190
two different sets of data from different samples

3
00:00:15,190 --> 00:00:19,240
oh my approach that well we can do this in a number of different ways there&#39;s

4
00:00:19,240 --> 00:00:22,449
essentially four different statistics that we can talk about that allow

5
00:00:22,449 --> 00:00:26,769
comparisons of different groups of data so let&#39;s start with the simplest one

6
00:00:26,769 --> 00:00:32,080
which is what we call a one-sample t-test or even a z-test let&#39;s take an

7
00:00:32,080 --> 00:00:38,710
example for a one-sample t-test or Z test in this case let&#39;s imagine that we

8
00:00:38,710 --> 00:00:43,690
have a we know the historical sales that rosie has had over the last say five

9
00:00:43,690 --> 00:00:48,100
years and we want to know if what she got this year was actually different

10
00:00:48,100 --> 00:00:52,480
than what she&#39;s seen historically so over time what we would think is

11
00:00:52,480 --> 00:00:56,859
potentially that over time she&#39;s had let&#39;s say a hundred and twenty classes

12
00:00:56,859 --> 00:01:00,129
that&#39;s what she sold on average for the month of july over the last five years

13
00:01:00,699 --> 00:01:04,269
so what kind of statistic can we determine used to determine if this is

14
00:01:04,269 --> 00:01:08,290
this year&#39;s number of lemonade or orangeade sales is actually different

15
00:01:08,290 --> 00:01:13,720
than that and so this is what we call a one-sample t-test you&#39;re simply taking

16
00:01:13,720 --> 00:01:18,880
the I mean that you got for your current sample and subtracting what we had from

17
00:01:18,880 --> 00:01:22,480
our previous samples and we&#39;re dividing it by the standard error

18
00:01:22,990 --> 00:01:26,290
remember we talked about that which essentially the standard deviation of

19
00:01:26,290 --> 00:01:30,310
the sample means then you notice here that i actually am introducing something

20
00:01:30,310 --> 00:01:35,170
new it&#39;s called the degrees of freedom degrees of freedom are important because

21
00:01:35,170 --> 00:01:40,000
this is actually the number you use to determine if your value is statistically

22
00:01:40,000 --> 00:01:41,050
significant

23
00:01:41,050 --> 00:01:43,960
so you&#39;re going to run this calculation you&#39;re going to get what&#39;s called the t

24
00:01:43,960 --> 00:01:47,530
statistic and then what you&#39;re going to do is you&#39;re going to apply a

25
00:01:47,530 --> 00:01:52,000
t-distribution to determine if that T statistic is statistically significant

26
00:01:52,000 --> 00:01:56,470
and to do that one piece of information you need is the degrees of freedom now

27
00:01:56,470 --> 00:02:00,430
it&#39;s called degrees of freedom because it is essentially the number of items in

28
00:02:00,430 --> 00:02:04,990
the data that you can change and you can still end up with the same mean so in

29
00:02:04,990 --> 00:02:09,399
this case i can literally change all of the numbers but one and then I just can

30
00:02:09,399 --> 00:02:12,790
make that adjustment for that last number to get back to the same mean so

31
00:02:12,790 --> 00:02:15,300
that&#39;s why we call it degrees of freedom

32
00:02:15,300 --> 00:02:19,590
in this case we&#39;re actually going to use a formula that&#39;s built into Excel

33
00:02:19,590 --> 00:02:22,860
already rather than to use the data analysis back to do the one-sample

34
00:02:22,860 --> 00:02:28,470
t-test in fact we&#39;re going to use is something called a z-test now you&#39;re

35
00:02:28,470 --> 00:02:32,130
probably saying Liberty did you just say we&#39;re doing a t-test and in fact i did

36
00:02:32,130 --> 00:02:36,660
but it turns out that the math is exactly the same for a z-test and a

37
00:02:36,660 --> 00:02:41,790
one-sample t-test the big difference is that a z-test assumes that you have some

38
00:02:41,790 --> 00:02:46,470
population information which we don&#39;t always have with the atty test so we&#39;re

39
00:02:46,470 --> 00:02:50,190
going to use the z-test because the math is close enough for us to get what we

40
00:02:50,190 --> 00:02:53,360
need to know from this analysis

41
00:02:53,360 --> 00:02:57,230
what would be interesting is let&#39;s imagine that we know that historically

42
00:02:57,230 --> 00:03:03,950
over the last five years that rosie has seen an average of sales that have been

43
00:03:03,950 --> 00:03:08,930
200 that&#39;s been her average sales so what we want to know is this year

44
00:03:08,930 --> 00:03:13,250
did she sell more than she has historically to do that we&#39;ll run our

45
00:03:13,250 --> 00:03:18,350
z-test will select the input the variable of interest which is our total

46
00:03:18,350 --> 00:03:21,350
sales are going to select that array

47
00:03:21,980 --> 00:03:26,060
we&#39;re going to enter in the value we want hurt that we want the analysis to

48
00:03:26,060 --> 00:03:31,010
be compared to which in this case is 200 now if you know what you&#39;re yours

49
00:03:31,010 --> 00:03:35,390
standard deviation is for your historical values or whatever values

50
00:03:35,390 --> 00:03:39,140
you&#39;re doing for this comparison you can plug that in as well we don&#39;t in this

51
00:03:39,140 --> 00:03:41,810
case so we&#39;re just going to leave it blank and we&#39;re going to run the

52
00:03:41,810 --> 00:03:47,599
analysis and we get a result of point zero two seven two so the question is

53
00:03:47,599 --> 00:03:52,819
what is that that&#39;s actually the probability of the t results or Z result

54
00:03:52,819 --> 00:03:59,239
that was obtained from this test most statisticians use a p-value of less than

55
00:03:59,239 --> 00:04:05,090
$MONEY . of five as the criteria with which they will it look at a statistic

56
00:04:05,090 --> 00:04:09,739
as being significant so the p-value is essentially the probability of being

57
00:04:09,739 --> 00:04:13,040
wrong if i say something is statistically significant or something

58
00:04:13,040 --> 00:04:13,970
is different

59
00:04:13,970 --> 00:04:18,680
what is the probability that i might be wrong somo statisticians say . 05 or

60
00:04:18,680 --> 00:04:24,590
less of five percent or less in this case you can see that . 02 is clearly

61
00:04:24,590 --> 00:04:29,419
less than $MONEY . 5 meaning that our result is statistically significant she

62
00:04:29,419 --> 00:04:31,580
in fact sold more lemonade

63
00:04:31,580 --> 00:04:35,539
I&#39;m sorry she had backed had more sales this year then she had in previous years

