0
00:00:05,009 --> 00:00:09,440
So, we saw how we could do that t-test or z-test, whichever letter you want to pick

1
00:00:09,440 --> 00:00:15,039
to do it, and we were able to compare our total sales versus – from this year to last

2
00:00:15,039 --> 00:00:18,930
year, so we’re really just looking at sales in that case; we’re looking at one thing.

3
00:00:18,930 --> 00:00:23,530
What if there’s two elements in my data? What if I’m interested in comparing my sales

4
00:00:23,530 --> 00:00:27,670
of lemonade and my sales of orangeade for example? &gt;&gt; Right, so this is actually another

5
00:00:27,670 --> 00:00:32,300
statistic that we use called the “two-sample T-test”, where we can actually compare different

6
00:00:32,299 --> 00:00:37,899
sales – or different variables within the data set. So, in this case, imagine if you’re

7
00:00:37,899 --> 00:00:44,289
curious – like, am I selling more lemonade or am I selling more orangeade? So to do that,

8
00:00:44,289 --> 00:00:50,229
we do the two-sample T-test. This is – it seems like a lot of math, but the formula

9
00:00:50,229 --> 00:00:54,210
is actually pretty straightforward. Essentially what its saying is it’s comparing the mean

10
00:00:54,210 --> 00:00:58,519
of your orangeade sales and the mean of your lemonade sales and it’s dividing it by the

11
00:00:58,519 --> 00:01:04,239
pooled standard deviation; that’s essentially what this equation all boils down to. And

12
00:01:04,239 --> 00:01:10,040
so if you think back to the t-test equation that I just showed you, that was a very simple

13
00:01:10,040 --> 00:01:14,970
version of this but it’s always the difference between two values divided by some version

14
00:01:14,970 --> 00:01:18,640
of the standard deviation, so that’s what we have here. And we also have degrees of

15
00:01:18,640 --> 00:01:23,190
freedom as well; in this case, we have two different means that we’re controlling for

16
00:01:23,190 --> 00:01:27,000
instead of subtracting one, we’re actually subtracting two.

17
00:01:27,000 --> 00:01:32,380
Back in Excel in Rosie’s data, let’s take a look at how we would do a two-sample T-test.

18
00:01:32,380 --> 00:01:37,010
We again get to visit our friend, the data analysis pack; we’re going to scroll down

19
00:01:37,010 --> 00:01:42,310
and we’re going to see that there’s several different options for a two-sample t-test:

20
00:01:42,310 --> 00:01:47,920
one assumes equal variances and one assumes unequal variances. For the most part, you’re

21
00:01:47,920 --> 00:01:52,510
going to assume equal variances. Unless you have reason to suspect that your two samples

22
00:01:52,510 --> 00:01:58,760
have very very different variances, then you will select the unequal variances, but by

23
00:01:58,760 --> 00:02:03,000
and large you’ll find yourself using the “assuming equal variances” version of

24
00:02:03,000 --> 00:02:08,300
this analysis. So you select that, and then you tell it where you want each of your – where

25
00:02:08,300 --> 00:02:15,599
each of your data lives. So, I think we’re curious: does she sell more lemonade or orangeade?

26
00:02:15,599 --> 00:02:24,810
So let’s select that data for each of those variables and enter it into our analysis.

27
00:02:24,810 --> 00:02:34,489
Again, I selected the top row because I want to have the labels or the variable names associated

28
00:02:34,489 --> 00:02:39,810
with it, so I’m going to make sure that I select the labels button. Now, the really

29
00:02:39,810 --> 00:02:44,870
neat thing about this analysis is that you can actually propose that there is a difference,

30
00:02:44,870 --> 00:02:48,299
so if you think that it’s something other than zero – if you think that maybe the

31
00:02:48,299 --> 00:02:52,829
difference is five or ten lemonades – you could put those numbers in here and it will

32
00:02:52,829 --> 00:02:57,700
actually test to see if your hypothesized mean difference is actually the true difference

33
00:02:57,700 --> 00:03:02,650
that you were expecting. We’re going to say, because we don’t know, we assume that

34
00:03:02,650 --> 00:03:08,659
she’s selling the same; for example, we’re going to assume that the difference is zero.

35
00:03:08,659 --> 00:03:13,529
Select okay, and we’re going to see that the data shows up in another window, in another

36
00:03:13,529 --> 00:03:18,889
sheet, as I said it should, and you can see that what you get out of this output is you

37
00:03:18,889 --> 00:03:23,140
see the mean or the average, the variance, the number of observations, the pooled variance

38
00:03:23,140 --> 00:03:28,299
– which is the denominator in the equation that I showed you just a moment ago. We said

39
00:03:28,299 --> 00:03:34,430
our hypothesized difference is zero, our degrees of freedom – remember 31 plus 31 is 62 minus

40
00:03:34,430 --> 00:03:39,829
2 is 60, so that’s how you get your degrees of freedom. Now the more important bit is

41
00:03:39,829 --> 00:03:45,650
actually the stuff down here at the bottom. The t-statistic is actually the number obtained

42
00:03:45,650 --> 00:03:50,809
once you run the math behind that formula that I showed you. What you can see is that

43
00:03:50,809 --> 00:03:55,329
you have a number of different values below that that are of particular importance; you

44
00:03:55,329 --> 00:04:00,549
have the ‘p’ for one-tail test, the critical value needed for one-tail test, a ‘p’

45
00:04:00,549 --> 00:04:05,749
for the two-tail test, and then the critical ‘t’ for the two-tail test. So what’s

46
00:04:05,749 --> 00:04:12,879
the difference? Essentially, if you have proposed or hypothesized that there is a direction

47
00:04:12,879 --> 00:04:17,859
that you think that – for example, she is selling more lemonade than orangeade – you

48
00:04:17,858 --> 00:04:22,850
have what’s called a one-tailed test. If you weren’t sure – you’re like, I don&#39;t

49
00:04:22,850 --> 00:04:26,590
know, she might be selling more lemonade or she might be selling more orangeade but I’m

50
00:04:26,590 --> 00:04:31,040
not sure which direction this difference could go – then you actually have a two-tailed

51
00:04:31,040 --> 00:04:35,920
test. Now the reason this is important is because, remember what we said is that the

52
00:04:35,920 --> 00:04:41,560
‘p’ value of 0.05 is our measure for determining if something is statistically significant?

53
00:04:41,560 --> 00:04:47,660
Well, if you have a one-tailed test, you can put all of that chance on one side; so it

54
00:04:47,660 --> 00:04:52,140
actually makes it a little bit easier for you to find something that’s statistically

55
00:04:52,140 --> 00:04:57,760
significant. And you can see that because the critical value that you need to obtain

56
00:04:57,760 --> 00:05:03,350
for a one-tailed test is only 1.67; anything larger than that is going to be statistically

57
00:05:03,350 --> 00:05:08,340
significant. With the two-tailed test, because you’re not sure which side of the distribution

58
00:05:08,340 --> 00:05:13,140
the difference could land on, you have to divide the error and put a little on each

59
00:05:13,140 --> 00:05:18,460
side, so a little goes in each of the tails. So as a result, you need a larger critical

60
00:05:18,460 --> 00:05:24,820
t-value to obtain, so you can see that 2 is larger than 1.67. So, the t-stat that you

61
00:05:24,820 --> 00:05:31,570
did get though is 5.9; in both cases, this is statistically significant. There is a difference

62
00:05:31,570 --> 00:05:35,080
between the number of lemonades she sells and the number of orangeades she sells.

