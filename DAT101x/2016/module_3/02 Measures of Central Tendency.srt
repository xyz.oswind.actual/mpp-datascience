0
00:00:03,790 --> 00:00:07,990
now let&#39;s take a look at some of the descriptive statistics and we&#39;re going

1
00:00:07,990 --> 00:00:12,820
to start with the basics that we call central tendency which are the mean

2
00:00:12,820 --> 00:00:16,390
median and mode so let&#39;s imagine we have this data

3
00:00:16,390 --> 00:00:19,540
these are some of these are the number of sales that she had over the course of

4
00:00:19,540 --> 00:00:24,040
a series of days so let&#39;s take a look at some of the the basic information or the

5
00:00:24,040 --> 00:00:29,349
descriptive statistics we can obtain about this data so if we do this the

6
00:00:29,349 --> 00:00:32,920
first thing we might want to do is actually plot the data in a histogram

7
00:00:32,920 --> 00:00:34,690
that looks like this

8
00:00:34,690 --> 00:00:39,610
so you&#39;ve created this histogram and and really what I&#39;m seeing here is I&#39;ve got

9
00:00:39,610 --> 00:00:42,730
the seals on the left-hand side i can see that the first day of school to the

10
00:00:42,730 --> 00:00:46,060
second day sold for the third day so forth and so on and you put that i&#39;m

11
00:00:46,060 --> 00:00:52,900
showing me the the number of items sold and then the number of times that they

12
00:00:52,900 --> 00:00:57,130
were sort of so there were one instance of me selling two items one day when i

13
00:00:57,130 --> 00:01:01,120
did that there&#39;s one day when I saw pain item so those are the lore values i see

14
00:01:01,120 --> 00:01:05,110
my histogram but there were three days where i sold six items so that gives me

15
00:01:05,110 --> 00:01:08,530
that big spike in the middle right I&#39;m looking at right so that&#39;s essentially

16
00:01:08,530 --> 00:01:13,210
what a histogram dies in this case we&#39;re just creating buckets of the sales and

17
00:01:13,210 --> 00:01:17,440
how many days you speak hit that particular sale marker got it

18
00:01:17,440 --> 00:01:21,820
so one thing you might want to do actually you should do the very first

19
00:01:21,820 --> 00:01:24,580
thing you should do when it comes to data and understanding the descriptive

20
00:01:24,580 --> 00:01:29,409
nature of it is to calculate what we call the mean or average as you can see

21
00:01:29,409 --> 00:01:32,200
in this case we actually have the formula here and this is what you call

22
00:01:32,200 --> 00:01:36,970
the population mean you notice that we have the greek new as the the character

23
00:01:36,970 --> 00:01:40,270
that were using here to describe this particular statistic because it

24
00:01:40,270 --> 00:01:44,619
represents the population when we talk about sample statistics though which is

25
00:01:44,619 --> 00:01:47,950
probably what most of you will be dealing with is you&#39;ll be looking at

26
00:01:47,950 --> 00:01:52,180
what we call the sample mean and in that case we&#39;re looking at x bar you notice

27
00:01:52,180 --> 00:01:57,310
that the formula is exactly the same it&#39;s just that the the notation that&#39;s

28
00:01:57,310 --> 00:02:00,070
used as a little bit different because we&#39;re representing the difference

29
00:02:00,070 --> 00:02:04,330
between a population and I mean generally when it comes to the mean

30
00:02:04,330 --> 00:02:08,799
though we&#39;re talking about something over the total number of values you have

31
00:02:08,799 --> 00:02:12,849
in your data set and dividing it by the number the total number of values that

32
00:02:12,849 --> 00:02:14,670
you have

33
00:02:14,670 --> 00:02:19,830
in this case then you just do that math it comes out that the Sun is 54 you have

34
00:02:19,830 --> 00:02:25,140
nine observations you&#39;re going to divide bus 54 by nine and you have six so that

35
00:02:25,140 --> 00:02:30,660
is your sample mean next that the next piece of information you&#39;re probably

36
00:02:30,660 --> 00:02:34,590
interested in is what we call the median and this is essentially the middlemost

37
00:02:34,590 --> 00:02:39,720
value in a median the way you calculate that is you actually take the total

38
00:02:39,720 --> 00:02:46,680
number of observations add 1/2 in this case you get a value of i-5 when you do

39
00:02:46,680 --> 00:02:49,920
that math now this does not mean that the median is five

40
00:02:49,920 --> 00:02:54,630
what it means is that the position of the median is the fifth value in the

41
00:02:54,630 --> 00:02:58,950
data set so you countdown because we have ordered the data from smallest to

42
00:02:58,950 --> 00:03:04,170
largest to the fifth value and you see that six and that is your median so the

43
00:03:04,170 --> 00:03:07,440
data has to be ordered for me to get that mean I&#39;ve got in the order and then

44
00:03:07,440 --> 00:03:11,069
I just kind that I I can&#39;t do the calculation and then kind of that number

45
00:03:11,069 --> 00:03:14,910
of places into the the collection of data and the value at that position is

46
00:03:14,910 --> 00:03:16,709
is the median correct right

47
00:03:16,709 --> 00:03:22,320
yep then the third central tendency statistic that we are interested in is

48
00:03:22,320 --> 00:03:26,519
what we call the mode and this is the most frequent value in the data set so

49
00:03:26,519 --> 00:03:30,690
just looking at not only the histogram which is probably even more telling but

50
00:03:30,690 --> 00:03:34,590
looking at the data you can see that the most frequent value is six

51
00:03:34,590 --> 00:03:39,120
there&#39;s no calculation for this it&#39;s simply just the count number of

52
00:03:39,120 --> 00:03:43,739
observations that are all the same and where that number is the highest is it

53
00:03:43,739 --> 00:03:49,440
most easily seen if you do a histogram so in this case we have all the mean

54
00:03:49,440 --> 00:03:56,459
median and mode are six days I mean just the way that data is organized then it

55
00:03:56,459 --> 00:04:00,329
looks like by chance it turns out the the mean the median on the motorola

56
00:04:00,329 --> 00:04:04,890
exactly the same value is not a common thing is not normal it&#39;s definitely

57
00:04:04,890 --> 00:04:09,150
normal that&#39;s actually the definition of normal to a statistician side this is

58
00:04:09,150 --> 00:04:12,900
very pleasing this is what we call a normal distribution we make a

59
00:04:12,900 --> 00:04:16,200
distribution of the data is really important because we make certain

60
00:04:16,200 --> 00:04:19,799
assumptions with our statistics that assume normality so it doesn&#39;t have to

61
00:04:19,798 --> 00:04:23,669
be perfectly normal for statistics to work but it should be close to normal so

62
00:04:23,669 --> 00:04:27,270
that&#39;s why we like to look at the mean median and mode because we want to see

63
00:04:27,270 --> 00:04:27,900
how far

64
00:04:27,900 --> 00:04:32,699
off our data is from normal to me sooner normal distribution and most things

65
00:04:32,699 --> 00:04:35,940
going to gravitate towards the middle value and it&#39;s a pretty even

66
00:04:35,940 --> 00:04:40,229
distribution I word from ya feeling either way yep so we have pretty much

67
00:04:40,229 --> 00:04:48,479
the same number of values on each side of the average or the mean so let&#39;s take

68
00:04:48,479 --> 00:04:52,710
another look at a little more complicated data set so this one is the

69
00:04:52,710 --> 00:04:57,600
number of fliers that she posted on a number of different days over the course

70
00:04:57,600 --> 00:05:01,560
of a month and so if we take a look at this again one of the first things you

71
00:05:01,560 --> 00:05:06,570
should consider doing to understand the true look and feel of your data is to

72
00:05:06,570 --> 00:05:11,639
actually plot in a histogram so I similar to before you see that on one

73
00:05:11,639 --> 00:05:16,229
day she had one flyer on one day she had three flyers and so on so we&#39;ve plotted

74
00:05:16,229 --> 00:05:20,130
the data in that histogram format to looks a little different maybe we talked

75
00:05:20,130 --> 00:05:23,220
about normal distribution the previous one that this looks like it might my

76
00:05:23,220 --> 00:05:27,090
suspect looking at that shape it might not be the same kind of normal

77
00:05:27,090 --> 00:05:30,780
distribution that we saw the last time it&#39;s probably not normal it&#39;s probably

78
00:05:30,780 --> 00:05:34,979
deviates from normal weather it&#39;s a significant important deviation is

79
00:05:34,979 --> 00:05:37,979
something that you will look at other statistics to determine but it&#39;s

80
00:05:37,979 --> 00:05:44,370
definitely differently shaped than a normal distribution just like before the

81
00:05:44,370 --> 00:05:48,449
first statistic will want to calculate is the average and we do that in the

82
00:05:48,449 --> 00:05:52,380
same way where we some i would take the sum across all the observations and

83
00:05:52,380 --> 00:05:56,099
divided by the total number of observations so in this case we see that

84
00:05:56,099 --> 00:06:01,889
the average is 6.75 we will look at the median where is the middle most value we

85
00:06:01,889 --> 00:06:05,880
calculate that out what&#39;s interesting about this particular one is that you

86
00:06:05,880 --> 00:06:10,830
notice that we have an odd number of observations and so you end up going

87
00:06:10,830 --> 00:06:16,500
counting down to the 8.5 position well there&#39;s no number at 8.5 so what is the

88
00:06:16,500 --> 00:06:22,229
immediate the middle value what you do in this case is you actually take six

89
00:06:22,229 --> 00:06:28,650
and seven you add them together and divide by two so you get a 6.5

90
00:06:28,650 --> 00:06:34,889
essentially and that becomes your median value that&#39;s the most value where half

91
00:06:34,889 --> 00:06:39,000
of the data appear above that value and half of the data pair below that value

92
00:06:39,880 --> 00:06:43,360
and then of course you have the model which is the most frequent and it in

93
00:06:43,360 --> 00:06:47,980
this case it&#39;s very easy to see from the histogram that that value is five that&#39;s

94
00:06:47,980 --> 00:06:52,750
the most frequently commonly occurring value in the data so it&#39;s interesting

95
00:06:52,750 --> 00:06:58,120
about this particular data set then is such as Graham noted is that the the

96
00:06:58,120 --> 00:07:02,500
mode and the median are actually less than the mean and so that tells us

97
00:07:02,500 --> 00:07:07,480
something about the distribution so let&#39;s take a look at the three different

98
00:07:07,480 --> 00:07:12,490
types of distributions we can have with data then the first as you can see is

99
00:07:12,490 --> 00:07:17,110
what we call a normal distribution where the mean median and mode are clustered

100
00:07:17,110 --> 00:07:20,650
together and you can look at this distribution and you can see that the

101
00:07:20,650 --> 00:07:24,370
data are pretty evenly divided on each side of the mean median and mode got

102
00:07:24,370 --> 00:07:29,320
that nice bell curve if I try and draw a line effectively exactly a bell curve or

103
00:07:29,320 --> 00:07:33,910
a normal distribution the next kind of distribution we can have is when in fact

104
00:07:33,910 --> 00:07:38,440
the mode and the median are less than the mean and you can see here that the

105
00:07:38,440 --> 00:07:42,790
tail actually extends out to the right and this as a result is what we call a

106
00:07:42,790 --> 00:07:48,760
right skate skewed distribution so it&#39;s skewed to the right the third kind of

107
00:07:48,760 --> 00:07:54,010
distribution we can have is actually where the mean is less than the mode and

108
00:07:54,010 --> 00:07:57,820
the median in this case you can see the tail is actually on the other side it&#39;s

109
00:07:57,820 --> 00:08:01,840
to the left and so this is what we call a left skewed distribution

