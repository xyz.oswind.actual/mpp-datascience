0
00:00:05,190 --> 00:00:10,170
So great, you showed us how we can get our descriptive statistics, and we can see some

1
00:00:10,170 --> 00:00:14,340
information about the data we’ve got; the next thing on our list is to start looking

2
00:00:14,340 --> 00:00:20,109
at associative statistics; how can we see relationships between things in our data?

3
00:00:20,109 --> 00:00:24,820
&gt;&gt; Well, it all starts with Correlation. &gt;&gt; Okay. &gt;&gt; So let’s take a look at what correlation

4
00:00:24,820 --> 00:00:27,590
is. Alright, let’s take a look at Rosie’s

5
00:00:27,590 --> 00:00:32,780
data with her lemonade stands, so we can better understand correlation. So let’s start with

6
00:00:32,780 --> 00:00:37,120
some basic concepts related to correlations before we get into what we might look for

7
00:00:37,120 --> 00:00:44,719
in correlations with her data. First, correlations always range from negative 1 to positive 1.

8
00:00:44,719 --> 00:00:50,789
The closer the relationship is to 1, the more in-alignment those variables are, so they

9
00:00:50,789 --> 00:00:56,129
move together either in the same direction if it’s a positive correlation, or in opposite

10
00:00:56,129 --> 00:01:00,260
directions if it’s a negative correlation. So that’s kind of the fundamental piece

11
00:01:00,260 --> 00:01:04,500
of a correlation; there’s two key aspects: the number itself – which always ranges

12
00:01:04,500 --> 00:01:09,159
from plus 1 to minus 1 – and then the direction it’s headed, which is the sign, the plus

13
00:01:09,159 --> 00:01:13,600
or the negative. Now let’s take a look at Rosie’s data and understand what things

14
00:01:13,600 --> 00:01:18,720
might be helping her drive sales. So there might be three variables of interest that

15
00:01:18,720 --> 00:01:23,750
we’re interested in to determine if there’s a relationship with sales. The first is price,

16
00:01:23,750 --> 00:01:27,970
the second is temperature, and the third is the number of leaflets or flyers that she

17
00:01:27,970 --> 00:01:32,500
hands out at a particular – on a particular day. So let’s take a closer look at what

18
00:01:32,500 --> 00:01:37,060
might be happening when she does certain things to understand if there’s a relationship

19
00:01:37,060 --> 00:01:43,310
with that variable and her sales. So let’s start with price. Let’s imagine

20
00:01:43,310 --> 00:01:48,520
she increases the price; what happens? We see sales go down. That’s an example of

21
00:01:48,520 --> 00:01:55,700
a negative correlation. In other words, price increases, sales decrease. Now, what happens

22
00:01:55,700 --> 00:02:00,960
when temperatures change? Imagine the temperatures go up and go down, but we don’t see any

23
00:02:00,960 --> 00:02:05,210
difference in the number of sales based on the temperature that she sees over a course

24
00:02:05,210 --> 00:02:12,510
of time. That is essentially no correlation, or correlation next to 0. Finally, we have

25
00:02:12,510 --> 00:02:17,040
the number of leaflets. What happens if she starts putting more and more leaflets out

26
00:02:17,040 --> 00:02:23,480
and she starts posting them? What we see is that sales increase. This is a positive correlation;

27
00:02:23,480 --> 00:02:26,730
as the number of leaflets increase, so does sales.

28
00:02:26,730 --> 00:02:32,730
Alright, let’s take a look at how we actually do a correlation matrix within Excel using

29
00:02:32,730 --> 00:02:37,670
the data analysis pack. We’re going to be in our data ribbon, we’re going to click

30
00:02:37,670 --> 00:02:44,209
data analysis, we’re going to click “correlation”, click OK, and then what we’re going to do

31
00:02:44,209 --> 00:02:47,590
is we’re going to identify the range of the variables that we’re interested in including

32
00:02:47,590 --> 00:02:53,500
in our correlation matrix. I think we probably are interested in looking at the relationship

33
00:02:53,500 --> 00:02:58,370
between lemonades, orangeades, temperatures, leaflets, price, total sales, and revenue.

34
00:02:58,370 --> 00:03:03,750
So let’s go ahead and highlight all of those columns; because again I highlighted the columns

35
00:03:03,750 --> 00:03:08,310
and we have our labels or variable names in the first column, I’m going to check that

36
00:03:08,310 --> 00:03:13,900
box and I’m going to click OK. Now it’s going to generate the correlation matrix in

37
00:03:13,900 --> 00:03:18,290
a separate worksheet because I told it to, and so that’s what happened here. What we

38
00:03:18,290 --> 00:03:24,819
can see is that we have a wide range of correlations in this matrix. You can see, for example,

39
00:03:24,819 --> 00:03:32,430
that the number of leaflets and the correlation with temperature is 0.36 for example; the

40
00:03:32,430 --> 00:03:37,030
number of – the correlation between price and the number of lemonades sold is negative

41
00:03:37,030 --> 00:03:42,209
0.39. So let’s take a little bit closer look at what each of these correlations potentially

42
00:03:42,209 --> 00:03:45,730
mean. So, if we look at the one that I have highlighted

43
00:03:45,730 --> 00:03:50,300
right now – it’s the negative correlation between lemonade and price – what you see

44
00:03:50,300 --> 00:03:55,550
is that as price increases, the number of lemonade sales are decreasing. That’s why

45
00:03:55,550 --> 00:04:01,340
it’s a negative correlation. What you can see then is you also have some very strong

46
00:04:01,340 --> 00:04:08,319
positive correlations in here. For example, let’s take a look at the price and – sorry,

47
00:04:08,319 --> 00:04:15,599
at leaflets, and revenue. That’s a very strong positive correlation, 0.57. As the

48
00:04:15,599 --> 00:04:22,320
number of leaflets increase, so does her revenue. Now it’s interesting with correlations is

49
00:04:22,320 --> 00:04:31,160
that they’re not causation, and a good example of this is that if we take a look at the relationship

50
00:04:31,160 --> 00:04:36,870
that we see between temperatures and leaflets. It’s a strong positive correlation, and

51
00:04:36,870 --> 00:04:42,160
so if correlation meant causation, one would say – well, as temperature increases, so

52
00:04:42,160 --> 00:04:47,850
does the number of leaflets; or, more practically, you might say – well if I put more leaflets

53
00:04:47,850 --> 00:04:52,980
out, then the temperature must be increasing. Those are what we call “spurious correlations”;

54
00:04:52,980 --> 00:04:57,910
there is a relationship, but odds are good that there’s a third factor that’s driving

55
00:04:57,910 --> 00:05:03,740
that relationship to be so strong. So keep in mind just because you have any sizable

56
00:05:03,740 --> 00:05:09,060
correlation, does not necessarily mean that there is a causal relationship between those

57
00:05:09,060 --> 00:05:15,470
two variables. It could be a spurious relationship that is caused by a third, yet-to-be-identified

58
00:05:15,470 --> 00:05:20,410
factor that’s in your data. The other thing I should mention about this that Excel doesn’t

59
00:05:20,410 --> 00:05:26,150
do is it doesn’t tell you the significance of these correlations. So yes, we have some

60
00:05:26,150 --> 00:05:30,650
big correlations in here, but they may or may not be significant. A quick web search

61
00:05:30,650 --> 00:05:35,780
will give you a table that shows you what level of correlation needs to be in order

62
00:05:35,780 --> 00:05:40,639
for it to be significant given the size of your data. I encourage you to check that out

63
00:05:40,639 --> 00:05:45,139
before you draw too many conclusions based on the correlations obtained from this correlation

64
00:05:45,139 --> 00:05:45,699
matrix.

