0
00:00:02,250 --> 00:00:07,330
Your R skills are growing by the minute, I can feel it! The most important data structures

1
00:00:07,330 --> 00:00:12,700
that we&#39;ve covered up to now are vectors and matrices. Remember that the vector is a one

2
00:00:12,700 --> 00:00:19,109
dimensional array that can only hold elements of the same type. Similarly, matrices can

3
00:00:19,109 --> 00:00:25,220
only hold elements of the same type, but this time they&#39;re in a two-dimensional array.

4
00:00:25,220 --> 00:00:29,430
Vectors and matrices are great, but there are cases in which you want to store different

5
00:00:29,430 --> 00:00:35,350
data types in the same data structures. This is where lists come in. A list can contain

6
00:00:35,350 --> 00:00:40,840
all kinds of R objects, such as vectors and matrices, but also other R objects, such as

7
00:00:40,840 --> 00:00:47,070
dates, data frames, factors and many more. All of this can be stored in a single list

8
00:00:47,070 --> 00:00:53,059
without R having to perform coercion to enforce the same type. That&#39;s pretty cool right? Because

9
00:00:53,059 --> 00:00:57,579
lists can contain practically anything you can think of in R terms, you do lose some

10
00:00:57,579 --> 00:01:03,510
functionality that vectors and matrices offered. Most importantly, performing calculus with

11
00:01:03,510 --> 00:01:08,340
lists is far less straightforward because there&#39;s no predefined structure that lists

12
00:01:08,340 --> 00:01:08,570
have to follow.

13
00:01:08,570 --> 00:01:15,880
Enough for the theory, let&#39;s build some lists! Suppose that as a music artist on the rise

14
00:01:15,880 --> 00:01:20,869
to fame and fortune, you regularly record some new songs, and keep some details for

15
00:01:20,869 --> 00:01:27,439
each of these songs. Your latest creation is called &quot;Rsome times&quot;, is 190 seconds long

16
00:01:27,439 --> 00:01:30,580
and should be the 5th number on your record.

17
00:01:30,580 --> 00:01:35,159
Trying to store this information in a vector using the `c()` function, ..., Inevitably

18
00:01:35,159 --> 00:01:40,770
leads to coercion. However, you can also store this information in a list, using the `list()`

19
00:01:40,770 --> 00:01:43,360
function

20
00:01:43,360 --> 00:01:48,460
This time, all the elements have kept their original type. The printout is pretty different

21
00:01:48,460 --> 00:01:53,259
from what you&#39;re used to. We can see that the first element in the list is the string

22
00:01:53,259 --> 00:01:58,560
&quot;Rsome times&quot; for example, which actually is a character vector. To continue working

23
00:01:58,560 --> 00:02:02,670
with this list we&#39;ll store the list in a new variable, `song`:

24
00:02:02,670 --> 00:02:09,679
We can assert that this `song` variable is a list using the is-dot-list function:

25
00:02:09,679 --> 00:02:15,090
Now, storing the song information like this, without any names, is not really clear, so

26
00:02:15,090 --> 00:02:20,340
let&#39;s assign some labels with the names() function. To assign the names, you still use

27
00:02:20,340 --> 00:02:25,680
a character vector, even though we&#39;re working with lists now:

28
00:02:25,680 --> 00:02:29,950
Printing `song` again shows that the indices in double square brackets have changed to

29
00:02:29,950 --> 00:02:35,310
the names of the list elements, this looks much nicer! As was the case with vectors,

30
00:02:35,310 --> 00:02:40,870
you can also directly specify the names in a list at the time of creation. To create

31
00:02:40,870 --> 00:02:43,760
the exact same variable `songs`, you can use this command

32
00:02:43,760 --> 00:02:50,280
You&#39;ve already figured out that the standard way of printing the contents of a list is

33
00:02:50,280 --> 00:02:55,640
pretty bulky. I suggest you use the `str()` function for this: this function compactly

34
00:02:55,640 --> 00:03:00,209
displays the structure of the `song` list:

35
00:03:00,209 --> 00:03:06,629
As I told you before, lists can contain practically anything. They can even contain other lists!

36
00:03:06,629 --> 00:03:11,370
Suppose you want to add a list containing the title and duration of a very similar but

37
00:03:11,370 --> 00:03:16,360
less catchy song that you&#39;ve also recorded in the last weeks. Let&#39;s first create a list

38
00:03:16,360 --> 00:03:20,760
to contain this information, `similar_song`

39
00:03:20,760 --> 00:03:24,980
We can now create the `song` list again, as follows:

40
00:03:24,980 --> 00:03:30,080
The structure of this list reveals that it is perfectly possible to store lists inside

41
00:03:30,080 --> 00:03:30,510
lists:

42
00:03:30,510 --> 00:03:36,330
If you want to go totally crazy, you can even store a list inside a list and then store

43
00:03:36,330 --> 00:03:41,450
that list in another list, but let&#39;s not make ourselves dizzy here. It&#39;s time to work your

44
00:03:41,450 --> 00:03:46,670
way around some of the interactive exercises before I introduce some techniques to subset

45
00:03:46,670 --> 00:03:48,340
and extend lists. Have fun!

