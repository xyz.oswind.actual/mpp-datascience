0
00:00:02,320 --> 00:00:08,109
Creating a list is not so different from creating a vector, is it? Unfortunately, subsetting

1
00:00:08,109 --> 00:00:14,450
lists and subsetting vectors are not that similar. Let&#39;s retake the music artist example.

2
00:00:14,450 --> 00:00:18,210
Remember the list `song` that contained both information on a song, as well as on a similar

3
00:00:18,210 --> 00:00:20,380
song?

4
00:00:20,380 --> 00:00:24,199
You might have noticed that the printout of the `song` list is not what it should be.

5
00:00:24,199 --> 00:00:29,460
I actually included the output of calling `str(song)` here instead, which is more compact

6
00:00:29,460 --> 00:00:35,190
and readable. I&#39;ll also do this in the list printouts that follow. Anyways, you see that

7
00:00:35,190 --> 00:00:41,170
the list contains 4 elements, a character, two numerics and another list.

8
00:00:41,170 --> 00:00:46,239
Suppose now you want to extract the song title, &quot;Rsome times&quot;, from this list. When you were

9
00:00:46,239 --> 00:00:51,390
working with vectors and matrices, this was dead simple. You simply put the index of the

10
00:00:51,390 --> 00:00:56,010
song title, which is 1, in square brackets and you get the elements. Well, let&#39;s try

11
00:00:56,010 --> 00:00:56,890
it out!

12
00:00:56,890 --> 00:01:03,090
That&#39;s not what we want! This is not a character string. It&#39;s a list, containing only the element

13
00:01:03,090 --> 00:01:08,250
that corresponds to the title information. This is a very important detail when trying

14
00:01:08,250 --> 00:01:13,750
to subset lists. If you use single brackets on lists, you subset the list, but also a

15
00:01:13,750 --> 00:01:18,590
list gets returned. If you want to select the actual title from `song`, so the character

16
00:01:18,590 --> 00:01:24,210
string Rsome times, you&#39;ll need double square brackets instead of single ones:

17
00:01:24,210 --> 00:01:28,759
That looks more like it! The difference between single brackets and double brackets is not

18
00:01:28,759 --> 00:01:34,829
big on your keyboard, but it sure is important from an R perspective. Just remember this:

19
00:01:34,829 --> 00:01:40,189
subsetting lists using single brackets results in lists, while you can only access a single

20
00:01:40,189 --> 00:01:45,219
element using double brackets. This difference doesn&#39;t limit you from supercharging your

21
00:01:45,219 --> 00:01:50,340
subsetting operations of course. Suppose for example you want to select both the title

22
00:01:50,340 --> 00:01:56,290
and the track from `song`. You can use a vector with the indices 1 and 3 inside single brackets

23
00:01:56,290 --> 00:01:59,159
to subset the list:

24
00:01:59,159 --> 00:02:04,770
We end up with a list of length 2 this time, only containing the title and the track. Could

25
00:02:04,770 --> 00:02:09,509
this also work with double brackets? Let&#39;s try it out.

26
00:02:09,508 --> 00:02:15,070
It generates an index out of bounds errror. That&#39;s pretty strange? Well, not really. It&#39;s

27
00:02:15,070 --> 00:02:20,220
because the double brackets are only to select single elements from a list. In fact, this

28
00:02:20,220 --> 00:02:24,100
command is actually equivalent to this one:

29
00:02:24,100 --> 00:02:28,800
This command means: take the first element from the song list, and from that list, take

30
00:02:28,800 --> 00:02:33,660
the third element. But the first element from song is simply a character vector of length

31
00:02:33,660 --> 00:02:39,470
1, so there&#39;s no way of selecting the third element from it. Nonetheless, you use this

32
00:02:39,470 --> 00:02:44,010
approach to select for example the title of the similar song, whose information is stored

33
00:02:44,010 --> 00:02:49,670
in the fourth element of `song`. In this case you first want to take the fourth element,

34
00:02:49,670 --> 00:02:52,020
and then the first element:

35
00:02:52,020 --> 00:03:00,870
This indeed is the title we were looking for! This could also be coded as follows:

36
00:03:00,870 --> 00:03:05,890
What about subsetting by name and by logicals, I hear you asking? Well, subsetting by name

37
00:03:05,890 --> 00:03:10,920
is super straightforward. To select the second element from song, for example, you can just

38
00:03:10,920 --> 00:03:15,310
as well use the string &quot;duration&quot; inside double brackets:

39
00:03:15,310 --> 00:03:19,850
This subsetting by names of course also works with single brackets, both to build a one-element

40
00:03:19,850 --> 00:03:25,030
list as well as to select multiple elements:

41
00:03:25,030 --> 00:03:29,080
Subsetting by logicals is only possible for the single-bracket version. To select the

42
00:03:29,080 --> 00:03:34,720
second and third element from song, this works, ..., but this doesn&#39;t.

43
00:03:34,720 --> 00:03:38,060
That&#39;s because the second line is interpreted as follows:

44
00:03:38,060 --> 00:03:41,970
and this makes no sense whatsoever.

45
00:03:41,970 --> 00:03:45,870
Another way that is totally new here, is the use of the dollar sign to select an element

46
00:03:45,870 --> 00:03:51,460
from a list. It works just the same as the double brackets but only works on named lists.

47
00:03:51,460 --> 00:03:56,700
If you want to select the duration string from `song` for example, you can use song,

48
00:03:56,700 --> 00:03:59,980
dollar sign, duration:

49
00:03:59,980 --> 00:04:04,430
This seamlessly brings me to adding elements to your list. Before you want to release your

50
00:04:04,430 --> 00:04:09,840
new song online, suppose you first want to have some of your friends to check it out.

51
00:04:09,840 --> 00:04:14,260
You decide to add a vector of friends names to the song list to remember which ones you&#39;ve

52
00:04:14,260 --> 00:04:20,100
sent it to. Let&#39;s first create a new vector `friends`.

53
00:04:20,100 --> 00:04:25,620
To add this information to the list `song` under the name `sent`, you have diferent options.

54
00:04:25,620 --> 00:04:31,130
Why not start with the dollar notation first? You simply type song, dollar sign, sent and

55
00:04:31,130 --> 00:04:32,490
then assign friends to it:

56
00:04:32,490 --> 00:04:37,640
If you now have a look at song again, you&#39;ll see that there is a fifth list element in

57
00:04:37,640 --> 00:04:45,000
song now. The same result could have been reached using the double square brackets:

58
00:04:45,000 --> 00:04:49,810
It&#39;s even possible to add elements to embedded lists. To add a reason why you decided to

59
00:04:49,810 --> 00:04:56,180
remove the similar song from your album, you could write something like this:

60
00:04:56,180 --> 00:05:01,210
That&#39;s all you need to know about lists for now. There are two main take-aways here: first,

61
00:05:01,210 --> 00:05:05,340
you use double square brackets to select a list element, while single square brackets

62
00:05:05,340 --> 00:05:11,380
results in a sublist containing the specified elements. Second, you can use the double square

63
00:05:11,380 --> 00:05:15,980
bracket and its dollar sign equivalent both to subset as well as to add elements to your

64
00:05:15,980 --> 00:05:18,620
list. Up to the exercises, chop chop!

