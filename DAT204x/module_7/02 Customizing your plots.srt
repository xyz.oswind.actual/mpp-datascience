0
00:00:02,300 --> 00:00:06,040
The plots that I&#39;ve shown to you in the previous video and the ones you&#39;ve created yourself

1
00:00:06,040 --> 00:00:10,810
in the interactive exercises look quite nice, but there are certainly more things we can

2
00:00:10,810 --> 00:00:16,740
do to make the plots fancier and more informative. What about setting a title, or specifying

3
00:00:16,740 --> 00:00:22,210
the labels of the axes? All of this is possible from inside R. Throughout our experiments,

4
00:00:22,210 --> 00:00:27,269
we will be using the `mercury` data frame, thats lists pressure versus temperature measurements

5
00:00:27,269 --> 00:00:34,649
of mercury. It contains two variables: temperature and pressure. Let&#39;s start with a simple plot:

6
00:00:34,649 --> 00:00:39,550
We can clearly see that the pressure rises dramatically once the temperature exceeds

7
00:00:39,550 --> 00:00:45,370
200 degrees celsius. But this plot is still kind of dull, isn&#39;t it? Have a look at this

8
00:00:45,370 --> 00:00:49,660
code, that specifies a bunch of arguments inside the `plot()` function.

9
00:00:49,660 --> 00:00:55,570
The result looks like this. Can you tell which arguments led to which changes in the plot?

10
00:00:55,570 --> 00:01:01,280
`xlab` and `ylab` changed the horizontal and vertical axis labels, respectively, while

11
00:01:01,280 --> 00:01:06,240
`main` specified the plot title. If you set the `type` argument to `o`, you will have

12
00:01:06,240 --> 00:01:11,470
both points and a line through these points on your plot. If you only want a line, you

13
00:01:11,470 --> 00:01:15,000
can use type = &quot;l&quot;, which looks like this:

14
00:01:15,000 --> 00:01:20,990
Finally, the `col` argument specifies the plot color.

15
00:01:20,990 --> 00:01:25,960
Most of the arguments that are used here, such as `xlab`, `ylab`, `main` and `type`,

16
00:01:25,960 --> 00:01:30,960
are specified in the documentation of the `plot()` function. However, the `plot()` function

17
00:01:30,960 --> 00:01:36,430
also allows you to set a bunch of other graphical parameters. An example of such a graphical

18
00:01:36,430 --> 00:01:41,260
parameter is `col`, which specifies the color. But there are many others.

19
00:01:41,260 --> 00:01:45,810
You can specify these graphical parameters straight inside the `plot()` function, as

20
00:01:45,810 --> 00:01:50,890
you did with `col`. In this case here, the graphical parameter only has an effect on

21
00:01:50,890 --> 00:01:56,320
this specific graph. If you now plot the same graph, without the col argument, the green

22
00:01:56,320 --> 00:01:58,390
color is not there anymore.

23
00:01:58,390 --> 00:02:04,680
You can also inspect and control these same graphical parameters with the `par()` function.

24
00:02:04,680 --> 00:02:09,780
Typing question mark par opens up its documentation, with information on all the parameters that

25
00:02:09,780 --> 00:02:16,270
you can specify. Simply calling `par()` gives you the actual values of these parameters.

26
00:02:16,270 --> 00:02:21,750
You can also use `par()` with arguments to specify session-wide graphical parameters:

27
00:02:21,750 --> 00:02:26,530
Suppose you set the color to blue using the `par()` function, ..., and now create a plot.

28
00:02:26,530 --> 00:02:32,690
It&#39;s blue. If you next create another plot, ..., the plot is still blue! That&#39;s because

29
00:02:32,690 --> 00:02:38,140
parameters specified with `par()` are maintained for different plotting operations. If you

30
00:02:38,140 --> 00:02:43,330
list all graphical parameters again and select the `col` element, ..., you&#39;ll see that indeed,

31
00:02:43,330 --> 00:02:44,900
it&#39;s still set to blue.

32
00:02:44,900 --> 00:02:51,600
For the rest of this video, let me focus on some of the most important graphical parameters.

33
00:02:51,600 --> 00:02:55,710
I&#39;ll do this by adding arguments to the plot function, instead of using the par function

34
00:02:55,710 --> 00:02:57,730
for this.

35
00:02:57,730 --> 00:03:03,750
You already know the first 5 graphical parameters. Similar to the `col` argument, the `col.main`

36
00:03:03,750 --> 00:03:08,959
argument specifies the color of the main title. There are also other col dot arguments to

37
00:03:08,959 --> 00:03:15,990
set the color of other elements in your plot. Next, the `cex dot axis` argument specifies

38
00:03:15,990 --> 00:03:20,400
with which ratio the original font size of the axis tick marks should be multiplied.

39
00:03:20,400 --> 00:03:29,010
With cex dot axis equal 0.6, we have small labels, with cex.axis to 1.5, the labels become

40
00:03:29,010 --> 00:03:31,810
huge.

41
00:03:31,810 --> 00:03:37,200
Just as the col parameter has col dot variants for other elements in the plot, the cex parameter

42
00:03:37,200 --> 00:03:40,510
also has its cex dot variants.

43
00:03:40,510 --> 00:03:46,819
The `lty` argument specifies the line type. a line type of 1 is a full line, and the types

44
00:03:46,819 --> 00:03:52,959
2 to 6 are all different types of lines, like you can see here. And last but not least,

45
00:03:52,959 --> 00:03:57,690
we have the `pch` argument, which specifies a plot symbol for the points you are plotting.

46
00:03:57,690 --> 00:04:02,980
There are more than 35 different symbols for plotting, going from plusses and small octogonals

47
00:04:02,980 --> 00:04:05,760
to stars and hashtags.

48
00:04:05,760 --> 00:04:11,140
Like I said, all of these arguments are just the tip of the iceberg. One of R&#39;s main powers

49
00:04:11,140 --> 00:04:15,739
is visualization and this is clear from the numerous ways in which you can make your plots

50
00:04:15,739 --> 00:04:21,590
ready for a report. Just make sure you don&#39;t overdo things; interpretability should be

51
00:04:21,589 --> 00:04:23,090
the main goal at all times!

