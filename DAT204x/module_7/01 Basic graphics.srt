0
00:00:03,179 --> 00:00:07,970
One of the main reasons that both academic and professional workers turn to R, is because

1
00:00:07,970 --> 00:00:12,889
of its very strong graphical capabilities. Walking away from graphical point and click

2
00:00:12,889 --> 00:00:18,410
programs such as Excel and Sigmaplot can be quite scary, but you&#39;ll soon see that R has

3
00:00:18,410 --> 00:00:24,130
much to offer. The big difference is that you create plots with lines of R code. You

4
00:00:24,130 --> 00:00:30,770
can thus perfectly replicate and modify plots inside R. This idea nicely fits into the spirit

5
00:00:30,770 --> 00:00:36,579
of reproducibility. The graphics package, that is loaded by default in R, already provides

6
00:00:36,579 --> 00:00:42,030
quite some functionality to create beautiful, publication-quality plots. Of course, throughout

7
00:00:42,030 --> 00:00:47,260
time, also a bunch of R packages have been developed to create visualizations differently

8
00:00:47,260 --> 00:00:52,579
or to build visualizations in very specific fields of study. Examples of very popular

9
00:00:52,579 --> 00:00:59,329
packages are ggplot2, ggvis and lattice, but I won&#39;t talk about those here.

10
00:00:59,329 --> 00:01:04,360
Among many others, the graphics package contains two functions that I&#39;ll talk about: `plot()`

11
00:01:04,360 --> 00:01:10,479
and `hist()`. I&#39;ll start with the `plot()` function first. This function is very generic,

12
00:01:10,479 --> 00:01:15,670
meaning that it can plot many different things. Depending on the type of data you give it,

13
00:01:15,670 --> 00:01:20,880
`plot()` will generate different plots. Typically, you&#39;ll want to plot one or two columns from

14
00:01:20,880 --> 00:01:25,860
a data frame, but you can also plot linear models, kernel density estimates and many

15
00:01:25,860 --> 00:01:28,540
more.

16
00:01:28,540 --> 00:01:32,729
Suppose we have a data frame `countries`, containing information on all of the countries

17
00:01:32,729 --> 00:01:37,890
in the world. There&#39;s of course the name of the country, but also the area, the continent

18
00:01:37,890 --> 00:01:43,240
it belongs to and other information such as population and religion. If you have a look

19
00:01:43,240 --> 00:01:48,049
at the structure of this data frame, you can see that there are numerical variables, characters,

20
00:01:48,049 --> 00:01:53,680
but also categorical variables, such as continent and religion. How would `plot()` handle these

21
00:01:53,680 --> 00:01:56,430
different data types? Let&#39;s find out!

22
00:01:56,430 --> 00:02:01,119
Let&#39;s try to plot the continent column of `countries`. We do the selection using the

23
00:02:01,119 --> 00:02:02,600
dollar sign:

24
00:02:02,600 --> 00:02:07,759
Cool. It looks like R figured out that continent is a factor, and that you&#39;ll probably want

25
00:02:07,759 --> 00:02:13,590
a bar chart, containing the number of countries there are in each continent. It also automatically

26
00:02:13,590 --> 00:02:19,109
assigned labels to the different bars. Now, what if you decide to plot a continuous variable,

27
00:02:19,109 --> 00:02:21,760
such as the population:

28
00:02:21,760 --> 00:02:26,620
This time, the populations of the countries are shown on an indexed plot. The first country

29
00:02:26,620 --> 00:02:31,450
corresponds to the index 1, while the population of the fiftieth observation in the data frame

30
00:02:31,450 --> 00:02:37,329
corresponds to index 50. Of course it&#39;s also possible to plot two continuous variables,

31
00:02:37,329 --> 00:02:39,620
such as area versus population:

32
00:02:39,620 --> 00:02:45,529
The result is a scatter plot, showing a dot for each country. There are some huge countries

33
00:02:45,529 --> 00:02:50,980
with many people living there, but also many smaller countries with also less people. It

34
00:02:50,980 --> 00:02:55,809
makes perfect sense that area and population are somewhat related, right? To make this

35
00:02:55,809 --> 00:03:00,040
relationship more clear, we can apply the logarithm function on both of the area and

36
00:03:00,040 --> 00:03:04,819
population vectors. You can use the `log()` function twice:

37
00:03:04,819 --> 00:03:11,889
Looks better already. When you try to plot two categorical variables, R handles it differently:

38
00:03:11,889 --> 00:03:17,790
For every continent now, a stacked bar chart of the different religions is depicted. On

39
00:03:17,790 --> 00:03:22,589
the right, you conveniently see the axis showing the proportions of each religion in each continent.

40
00:03:22,589 --> 00:03:27,370
If you switch the two variables inside the plot function, for every religion, a stacked

41
00:03:27,370 --> 00:03:32,329
bar chart of the different continents is depicted. This means that the first element in the `plot()`

42
00:03:32,329 --> 00:03:37,489
function is the variable on the horizontal axis, the x axis, while the second element

43
00:03:37,489 --> 00:03:42,739
is the element on the vertical axis, the y axis.

44
00:03:42,739 --> 00:03:47,139
All these examples show that the plot function is very capable at visualizing different kinds

45
00:03:47,139 --> 00:03:53,499
of information and manages to display the information in an interpretable way.

46
00:03:53,499 --> 00:03:58,239
To better understand your numerical data, it&#39;s often a good idea to have a look at its

47
00:03:58,239 --> 00:04:03,040
distribution. You can do this with the `hist()` function, which is short for histogram. Basically,

48
00:04:03,040 --> 00:04:08,669
a histogram is a visual representation of the distribution of your dataset by placing

49
00:04:08,669 --> 00:04:14,579
all the values in bins and plotting how many values there are in each bin. Say we want

50
00:04:14,579 --> 00:04:19,489
to get a first impression on the population in all of Africa&#39;s countries. With a logical

51
00:04:19,488 --> 00:04:24,639
vector, africa_obs, you can perform a selection on the countries data frame, to create sub

52
00:04:24,639 --> 00:04:27,710
data frame that contains only african countries.

53
00:04:27,710 --> 00:04:33,419
Now, we can call the `hist()` function on the `population` column of `africa`.

54
00:04:33,419 --> 00:04:38,229
It appears that around 40 african countries have less than 10 million inhabitants, and

55
00:04:38,229 --> 00:04:42,900
that there are only a few with more than 20 million inhabitants. R has chosen a number

56
00:04:42,900 --> 00:04:49,460
of bins, 6 in this case, itself. If you like a more detailed representation, you can increase

57
00:04:49,460 --> 00:04:55,830
the number of bins in the histogram, for example to 10, by setting the `breaks` argument:

58
00:04:55,830 --> 00:05:01,280
It appears that still more than 25 countries have a population under 5 million people.

59
00:05:01,280 --> 00:05:05,289
Apart from the `breaks` argument, there are many more options you can specify in the `hist()`

60
00:05:05,289 --> 00:05:11,080
function. If you are curious, you can already check out the documentation for these!

61
00:05:11,080 --> 00:05:15,870
Apart from plots and histograms, there are so much more things you can do in R. You can

62
00:05:15,870 --> 00:05:20,120
create bar plots with the `barplot()` function, boxplots with the `boxplot()` function and

63
00:05:20,120 --> 00:05:24,870
even create a bunch of plots all together with functions such as `pairs()`. But let&#39;s

64
00:05:24,870 --> 00:05:30,050
stick to `plot()` and `hist()` for now. Head over to the exercises and experience the magic

65
00:05:30,050 --> 00:05:31,860
of creating graphs in R yourself!

