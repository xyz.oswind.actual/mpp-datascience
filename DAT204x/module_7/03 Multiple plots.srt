0
00:00:02,230 --> 00:00:08,790
By now, you should know how to build and customize plots in R. That&#39;s already pretty nice! However,

1
00:00:08,790 --> 00:00:13,059
you might have noticed that all the examples were limited to the plotting of a single source

2
00:00:13,059 --> 00:00:18,660
of data, for example: a histogram of a data frame column, or a scatter plot of two variables

3
00:00:18,660 --> 00:00:23,820
in a data frame. But what about a histogram next to a boxplot in the same graphical window,

4
00:00:23,820 --> 00:00:28,949
or a scatter plot with a linear regression line on top of that? In this last video, I&#39;ll

5
00:00:28,949 --> 00:00:34,239
give you a sneak peek in all possibilities to create multiple plots or to add more information

6
00:00:34,239 --> 00:00:35,789
to your plots.

7
00:00:35,789 --> 00:00:41,850
We&#39;ll be working with the shop data set: it contains 27 observations of a company&#39;s sales

8
00:00:41,850 --> 00:00:46,590
figures in relation to the amount of money spent on advertising, the number of nearby

9
00:00:46,590 --> 00:00:51,420
competitors, the current inventory and the overall size of the district. Have a look

10
00:00:51,420 --> 00:00:54,399
at its structure.

11
00:00:54,399 --> 00:00:58,559
Suppose now that we want to get a first idea of the correlation between the net sales,

12
00:00:58,559 --> 00:01:05,630
in the `sales` column, and the other 4 variables, `ads`, `comp`, `inv` and `size_dist`. We could

13
00:01:05,630 --> 00:01:10,680
build four separate plots, but it can be a good idea to create a grid of plots, to compare

14
00:01:10,680 --> 00:01:12,920
the correlations more easily.

15
00:01:12,920 --> 00:01:19,080
The easiest way to do is, is by specifying the graphical parameter `mfrow` with the `par()`

16
00:01:19,080 --> 00:01:24,320
function. Remember from before that you could use the `par()` function to list all the parameters,

17
00:01:24,320 --> 00:01:25,470
like this:

18
00:01:25,470 --> 00:01:31,650
But you can also use it to set graphical parameters. Let&#39;s set the `mfrow` parameter to a vector

19
00:01:31,650 --> 00:01:38,220
containing two times two, to tell R that you want to build 4 subplots on a 2 by 2 grid:

20
00:01:38,220 --> 00:01:43,250
Nothing really happens. It&#39;ll become clear once you start building some plots though.

21
00:01:43,250 --> 00:01:46,960
Let&#39;s start with net sales versus ad spending:

22
00:01:46,960 --> 00:01:52,210
The plot shows up in the top left corner. If you now also plot `sales` versus `comp`,

23
00:01:52,210 --> 00:01:56,780
..., it appears next to it. Adding the third plot, the relation with the inventory, and

24
00:01:56,780 --> 00:02:01,200
the fourth plot, that shows the relation with the size of the district, we end up with four

25
00:02:01,200 --> 00:02:01,540
figures:

26
00:02:01,540 --> 00:02:06,660
Pretty cool huh! You saw here that the plots got added in a row-wise fashion. If you instead

27
00:02:06,660 --> 00:02:12,750
specify the graphical parameter `mfcol`, instead of `mfrow`, with the same specification, ..., and

28
00:02:12,750 --> 00:02:17,930
do the four plots again, one by one, you&#39;ll see that the graphs are added in a column-wise

29
00:02:17,930 --> 00:02:27,030
fashion. This vector c(2,2) denotes that you want to have a layout of two rows and two

30
00:02:27,030 --> 00:02:30,510
columns, in this order.

31
00:02:30,510 --> 00:02:35,290
To reset the graphical parameters such that R plots a single figure per layout, you can

32
00:02:35,290 --> 00:02:41,019
set either mfrow or mfcol to a vector that denotes that you want a 1 by 1 grid:

33
00:02:41,019 --> 00:02:47,010
If you now plot something, it looks like it did before:

34
00:02:47,010 --> 00:02:52,689
Apart from specifying the mfrow or mfcol parameters using the `par()` function, there&#39;s also the

35
00:02:52,689 --> 00:02:57,920
`layout()` function, that allows you to specify more complex plot arrangements. This function

36
00:02:57,920 --> 00:03:03,959
expects a matrix, in which you specify the location of the figures on the output device.

37
00:03:03,959 --> 00:03:09,129
Have a look at this matrix, `grid`, that specifies the locations that 3 figures can take:

38
00:03:09,129 --> 00:03:14,569
If you pass this matrix to the `layout` function, and do three plot calls afterwards, ..., You&#39;ll

39
00:03:14,569 --> 00:03:19,930
see that the first figure spans the entire width of the first row of the layout. You

40
00:03:19,930 --> 00:03:24,230
can go pretty far with customizing your layouts, but that&#39;s something for more advanced visualization

41
00:03:24,230 --> 00:03:30,139
courses. To reset this layout, you can simply call this command to specify a 1 by 1 layout,

42
00:03:30,139 --> 00:03:36,969
or you can again use the mfcol or mfrow parameter to reset the grid

43
00:03:36,969 --> 00:03:41,219
Resetting this layout afterwards, every time you have been adapting the graphical parameters,

44
00:03:41,219 --> 00:03:46,519
can be quite tedious. Another, often easier way is to copy the old graphical parameters

45
00:03:46,519 --> 00:03:52,249
to a new variable first, like this. If you then do your customizations, for example setting

46
00:03:52,249 --> 00:03:57,540
the overall color to red and make a plot, you can easily clean up afterwards by restoring

47
00:03:57,540 --> 00:04:04,950
the old graphical parameters. If you now the same plot again, the red color is gone again.

48
00:04:04,950 --> 00:04:09,430
Apart from building layouts to show different plots next to each other, you can also choose

49
00:04:09,430 --> 00:04:13,889
to stack different graphical elements on top of each other, as if you are adding layers

50
00:04:13,889 --> 00:04:20,600
to the same plot. Let&#39;s see how this works with an example. Let&#39;s first try to plot the

51
00:04:20,600 --> 00:04:25,470
sales versus the advertisement spending with some basic customization:

52
00:04:25,470 --> 00:04:29,230
Can you still tell what the meaning is of all these different parameters?

53
00:04:29,230 --> 00:04:34,920
Next, let&#39;s try to build a model that models sales based on advertisement spending with

54
00:04:34,920 --> 00:04:40,850
the `lm()` function. The `lm()` function returns an lm object, which contains the coefficients

55
00:04:40,850 --> 00:04:44,750
of the line representating the linear fit.

56
00:04:44,750 --> 00:04:49,740
You can now use these coefficients to plot a straight line on the plot. The `abline()`

57
00:04:49,740 --> 00:04:55,010
function helps us out: This function can take the coefficients of a straight line as a vector,

58
00:04:55,010 --> 00:04:57,220
or as separate values.

59
00:04:57,220 --> 00:05:03,120
The `lwd` argument stands for line width here. Instead of building a new plot with simply

60
00:05:03,120 --> 00:05:08,200
a line, the previous plot is kept, and the straight line is added to the plot. This is

61
00:05:08,200 --> 00:05:12,440
a great way of adding more information to your plot!

62
00:05:12,440 --> 00:05:16,920
Next to `abline()`, you can also use functions like `points()`, `segments()`, `lines()` and

63
00:05:16,920 --> 00:05:22,830
`text()` to supercharge your plots. You&#39;ll learn all about them in the exercises. Most

64
00:05:22,830 --> 00:05:27,120
of them work out of the box; only for the `lines()` function it&#39;s important that you

65
00:05:27,120 --> 00:05:32,380
pass the data points in the right order. If we simply use `lines()` on the `ads` and `sales`

66
00:05:32,380 --> 00:05:37,940
columns, the lines connecting the points will be all over the place:

67
00:05:37,940 --> 00:05:41,680
You have to use the `order()` function to get a `ranks` vector, just like you did to

68
00:05:41,680 --> 00:05:47,300
order data frames. If you now rebuild the plot, with the abline, and the lines function

69
00:05:47,300 --> 00:05:53,520
that uses reshuffled data points, it looks much better.

70
00:05:53,520 --> 00:05:57,680
Notice from this last example that once you add something to your plot, such as the mess

71
00:05:57,680 --> 00:06:02,960
of lines going from left to right, you cannot remove it anymore. Plotting in R works like

72
00:06:02,960 --> 00:06:07,880
painting things on a canvas; you can&#39;t take it off again. If you want to remove elements,

73
00:06:07,880 --> 00:06:12,740
you&#39;ll have to start over with a plot generating function like `plot()` and generally add more

74
00:06:12,740 --> 00:06:17,310
layers to it with the functions I&#39;ve just introduced.

75
00:06:17,310 --> 00:06:21,640
With this new knowledge on how to build multiple plots on a grid as well as to add different

76
00:06:21,640 --> 00:06:26,030
layers to the same plot, I think you&#39;re totally ready for the final set of interactive exercises

77
00:06:26,030 --> 00:06:28,720
of this introduction to R course. Thanks for watching!

