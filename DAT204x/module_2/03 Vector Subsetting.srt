0
00:00:02,790 --> 00:00:07,210
In the previous exercises, you have created and worked with vectors of different types.

1
00:00:07,210 --> 00:00:12,070
You have even computed their length, and seen how to operate with them on a basic level.

2
00:00:12,070 --> 00:00:18,470
A last important element to make your experience with vectors complete is vector subsetting.

3
00:00:18,470 --> 00:00:23,230
As the name reveals, it basically comes down to selecting parts of your vector to end up

4
00:00:23,230 --> 00:00:28,239
with a new vector, which is a subset of the original vector.

5
00:00:28,239 --> 00:00:32,119
Remember the `remain` vector that we built in one of the previous videos? Here it is

6
00:00:32,119 --> 00:00:35,360
again, as a named vector.

7
00:00:35,360 --> 00:00:39,300
Suppose you know want to select the first element from this vector, corresponding to

8
00:00:39,300 --> 00:00:45,110
the number of spades that are left. You can use square brackets for this. We write remain,

9
00:00:45,110 --> 00:00:48,460
open brackets, 1, close brackets.

10
00:00:48,460 --> 00:00:52,780
The number one inside the square brackets indicates that you want to get the first element

11
00:00:52,780 --> 00:00:58,110
from the `remain` vector. The result is again a vector, because a single number is actually

12
00:00:58,110 --> 00:01:05,439
a vector of length 1. This new vector contains the number 11. The name of the only element,

13
00:01:05,438 --> 00:01:11,170
`spades`, is conveniently kept. If you instead wanted to select the third element, corresponding

14
00:01:11,170 --> 00:01:18,460
to the remaining diamonds, you could code remain followed by 3 in square brackets.

15
00:01:18,460 --> 00:01:22,640
This was subsetting using an index, but if you&#39;re dealing with named vectors, you can

16
00:01:22,640 --> 00:01:27,830
also use the names to perform the selection. Instead of using the index 1 to select the

17
00:01:27,830 --> 00:01:33,619
first element, you can use the name `spades`. You type remain followed by spades as a character

18
00:01:33,619 --> 00:01:36,040
string inside square brackets.

19
00:01:36,040 --> 00:01:42,350
The result is exactly the same as using the numeric index 1. Can you tell how you can

20
00:01:42,350 --> 00:01:47,409
refer to the third element in the vector with a name? You&#39;ve probably figured that you simply

21
00:01:47,409 --> 00:01:52,119
have to type diamonds inside square brackets this time.

22
00:01:52,119 --> 00:01:56,350
Suppose now you want to select the elements in the vector that give the remaining spades

23
00:01:56,350 --> 00:02:02,600
and clubs in a one-liner, and store them in a new variable, remain_black. Instead of using

24
00:02:02,600 --> 00:02:07,450
a single number inside the square brackets, you can use a vector to specify which indices

25
00:02:07,450 --> 00:02:14,090
you want to select. Because spades are at index 1 and clubs at index 4, you use vector

26
00:02:14,090 --> 00:02:18,689
containing 1 and 4 inside the square brackets.

27
00:02:18,689 --> 00:02:23,409
How the resulting vector is ordered depends on the order of the indices inside the selection

28
00:02:23,409 --> 00:02:32,169
vector. If you change c 1 4 to c 4 1, you will get a vector where the clubs come first.

29
00:02:32,169 --> 00:02:37,239
Of course, subsetting multiple elements can also be done by using names, at least if you&#39;re

30
00:02:37,239 --> 00:02:43,109
dealing with a named vector. To get the same result as the command above, we write remain,

31
00:02:43,109 --> 00:02:50,069
open brackets, then a vector containing the character strings &quot;clubs&quot; and &quot;spades&quot;.

32
00:02:50,069 --> 00:02:54,540
There&#39;s yet another way to subset vectors, that&#39;s specifically useful if you want to

33
00:02:54,540 --> 00:03:00,169
select all the elements from a vector, except one. Suppose you want to create a vector that

34
00:03:00,169 --> 00:03:05,389
contains all the information that&#39;s in the remain vector, except for the spades count.

35
00:03:05,389 --> 00:03:10,819
You can write remain, open brackets, minus 1, close brackets.

36
00:03:10,819 --> 00:03:14,879
This command removes the first index from the remain vector. Of course, you can also

37
00:03:14,879 --> 00:03:18,370
remove multiple elements, like this for example.

38
00:03:18,370 --> 00:03:23,849
This minus operator does not work with names though. This command, for example, throws

39
00:03:23,849 --> 00:03:27,159
an error.

40
00:03:27,159 --> 00:03:31,129
These subsetting techniques will enable you to select only the elements of interest from

41
00:03:31,129 --> 00:03:36,799
your vector and continue your analyses with these. Before I unleash you to the last set

42
00:03:36,799 --> 00:03:42,099
of exercises of this chapter, I want to talk about one last way to perform vector subsetting:

43
00:03:42,099 --> 00:03:45,040
using a logical vector.

44
00:03:45,040 --> 00:03:49,409
To do this, you typically use a logical vector that has the same length as the vector you

45
00:03:49,409 --> 00:03:54,529
try to subset. The elements for which the corresponding value in the selecting vector

46
00:03:54,529 --> 00:03:59,239
is TRUE, will be kept in the subset. The vector elements that correspond to FALSE, will not

47
00:03:59,239 --> 00:04:04,969
be kept. Let&#39;s try to select the second and fourth element from the `remain` vector using

48
00:04:04,969 --> 00:04:11,219
a logical vector. To this end, we construct a vector containing FALSE, TRUE, FALSE and

49
00:04:11,219 --> 00:04:13,809
TRUE, and put it inside the square brackets.

50
00:04:13,809 --> 00:04:19,350
Ofcourse, you could also have created a new vector first and then use it to perform the

51
00:04:19,350 --> 00:04:21,260
selection, like this.

52
00:04:21,260 --> 00:04:26,880
Now, you might expect that R throws an error if you try to use a logical vector that is

53
00:04:26,880 --> 00:04:32,120
shorter than the vector on which you want to perform subsetting. Trying this out shows

54
00:04:32,120 --> 00:04:37,790
a different reality. Suppose you use a vector containing only two logicals instead of a

55
00:04:37,790 --> 00:04:38,450
4

56
00:04:38,450 --> 00:04:44,290
No error whatsoever! That&#39;s because R performs something called &#39;recycling&#39;. R is smart enough

57
00:04:44,290 --> 00:04:49,030
to see that the vector of logicals you passed it is shorter than the `remain` vector, so

58
00:04:49,030 --> 00:04:54,260
it repeats the contents of the vector until it has the same length as `remain`. This means

59
00:04:54,260 --> 00:04:58,790
that, behind the scenes, this line of code is executed, giving the result that we&#39;ve

60
00:04:58,790 --> 00:05:04,530
observed before. Even if you use a vector of length 3 to do the selection, the vector

61
00:05:04,530 --> 00:05:09,950
is recycled to end up with a vector of length 4, thus appending the first element again.

62
00:05:09,950 --> 00:05:14,790
So this statement, ..., gets converted to this statement behind the scenes. That&#39;s why

63
00:05:14,790 --> 00:05:17,590
their outputs are the same.

64
00:05:17,590 --> 00:05:22,570
Up to you now to solve the challenges and use your new-found knowledge to beat the Casino.

65
00:05:22,570 --> 00:05:25,890
In the next chapter I&#39;ll be talking about matrices, see you there!

