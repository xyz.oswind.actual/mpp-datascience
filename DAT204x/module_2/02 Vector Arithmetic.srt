0
00:00:03,290 --> 00:00:03,820
Vector arithmetic

1
00:00:03,820 --> 00:00:08,670
Before, you learned that you can use variables to perform arithmetic Remember how you summed

2
00:00:08,670 --> 00:00:13,780
apples and oranges? From the previous video, you also know that actually these variables,

3
00:00:13,780 --> 00:00:19,399
`my_apples` and `my_oranges`, are simply vectors. This means that you can perform arithmetic

4
00:00:19,399 --> 00:00:24,910
with vectors in R; you already did it! However, there are still some things I want to discuss

5
00:00:24,910 --> 00:00:29,590
about calculations with vectors that contain more than 1 element.

6
00:00:29,590 --> 00:00:33,870
The most important thing to remember about operations with vectors in R, is that they

7
00:00:33,870 --> 00:00:38,980
will be applied _element by element_. This means that standard mathematics is extended

8
00:00:38,980 --> 00:00:44,410
to vectors in an element-wise fashion. To illustrate what this means, let&#39;s have a look

9
00:00:44,410 --> 00:00:50,880
at an example. Imagine you have a vector containing your gambling earnings for the past 3 days.

10
00:00:50,880 --> 00:00:55,760
Not bad for a few days in the desert, is it? Imagine a well-dressed gentleman approaches

11
00:00:55,760 --> 00:01:00,120
you and offers to triple your earnings for the past three days, if you beat him in one

12
00:01:00,120 --> 00:01:04,909
round of poker. If you want to calculate the expected earnings for each of the past three

13
00:01:04,909 --> 00:01:08,710
days, you can easily do it in R.

14
00:01:08,710 --> 00:01:13,700
As you can see, R multiplies each element in the `earnings` vector with 3, resulting

15
00:01:13,700 --> 00:01:18,950
in 150 dollars of promised earnings in the first day, 300 in the second day and 90 in

16
00:01:18,950 --> 00:01:23,580
the third day. Sounds like a sweet deal if you ask me!

17
00:01:23,580 --> 00:01:28,640
Performing these kinds of calculations works just the same with other mathetemical operators.

18
00:01:28,640 --> 00:01:35,229
Likewise, division, ... , subtraction, ... summation and many more are all carried out element

19
00:01:35,229 --> 00:01:42,119
wise, just as if you are carrying out the operation between two scalars three times.

20
00:01:42,119 --> 00:01:46,270
From these lines of code you don&#39;t see anything different from what we&#39;ve done before, because

21
00:01:46,270 --> 00:01:51,450
of course, you were working with vectors all along. The mathematics naturally extend to

22
00:01:51,450 --> 00:01:55,570
vectors that contain more than one element.

23
00:01:55,570 --> 00:02:00,189
Let&#39;s go back to your Vegas adventures. To enjoy your earnings, you also decided to go

24
00:02:00,189 --> 00:02:05,080
shopping and spend some money every day on the Las Vegas Strip. You recorded a vector

25
00:02:05,080 --> 00:02:08,369
of expenses.

26
00:02:08,369 --> 00:02:12,340
Because you are a very conscious programmer in training, you decide to compute whether

27
00:02:12,340 --> 00:02:17,439
your luck in the casino was sufficient to pay for your expenses. How would you go about

28
00:02:17,439 --> 00:02:23,599
this? Once again, this is fairly easy in R. You can simply subtract `expenses` from `earnings`

29
00:02:23,599 --> 00:02:25,340
to find your daily balance.

30
00:02:25,340 --> 00:02:31,629
This time, the mathematics were also calculated element-wise: the first element of expenses

31
00:02:31,629 --> 00:02:36,159
was subtracted from the first element of earnings, the second element of expenses was subtracted

32
00:02:36,159 --> 00:02:41,269
from the second element of earnings, and so on. It seems that for days 1 and 2, you are

33
00:02:41,269 --> 00:02:46,709
on the positive side, while on day 3 you spent more than you earned.

34
00:02:46,709 --> 00:02:51,730
Also here, the operations between two vectors that contain more than 1 element each, naturally

35
00:02:51,730 --> 00:02:56,529
extend in an element-wise fashion. Have a look at these three examples.

36
00:02:56,529 --> 00:03:02,569
Pay attention here: multiplication and division in R are different from the traditional matrix

37
00:03:02,569 --> 00:03:07,389
and vector operations, where the multiplication of two vectors can result in a single scalar

38
00:03:07,389 --> 00:03:09,340
or a matrix.

39
00:03:09,340 --> 00:03:14,639
A last question you might ask yourself is how your bank account progressed overall after

40
00:03:14,639 --> 00:03:19,409
these three mad days in the city of Sins. You can save the previous calculation in a

41
00:03:19,409 --> 00:03:25,609
new vector called bank and use the sum() function on it to calculate the sum of all its elements.

42
00:03:25,609 --> 00:03:30,889
Wow, that was pretty close! You still managed to make a thirty dollar profit in Vegas, that&#39;s

43
00:03:30,889 --> 00:03:32,999
quite an accomplishment!

44
00:03:32,999 --> 00:03:37,049
Instead of subtracting expenses from earnings to compare them, which gives you positive

45
00:03:37,049 --> 00:03:42,269
and negative numbers, you could also use relational operators to know when earnings were higher

46
00:03:42,269 --> 00:03:47,719
than expenses. You can use the greater than operator for this. This operator compares

47
00:03:47,719 --> 00:03:50,409
the numbers in the vectors element-wise:

48
00:03:50,409 --> 00:03:56,790
The result is a vector of TRUE&#39;s and FALSE&#39;s: on days 1 and 2, your earnings exceeded your

49
00:03:56,790 --> 00:04:01,639
expenses, so the corresponding elements are `TRUE`. On the last day you spent more than

50
00:04:01,639 --> 00:04:06,919
you earned so the result is `FALSE`. There are much more relational operators, but that&#39;s

51
00:04:06,919 --> 00:04:09,629
something for the intermediate R course.

52
00:04:09,629 --> 00:04:14,229
For now, you know enough about vector arithmetic to do some Vegas accounting; hopefully they

53
00:04:14,229 --> 00:04:16,780
can help you to earn some cash!

