0
00:00:02,500 --> 00:00:08,600
Hi again! In this video I&#39;ll be talking about Vectors. A vector is nothing more than a sequence

1
00:00:08,600 --> 00:00:13,960
of data elements of the _same_ basic data type. Remember the atomic vector types I discussed

2
00:00:13,960 --> 00:00:20,880
before? You can have character vectors, numeric vectors, logical vectors, and many more.

3
00:00:20,880 --> 00:00:25,449
First things first: creating a vector in R! You use the `c()` function for this, which

4
00:00:25,449 --> 00:00:30,869
allows you to combine values into a vector. Suppose you&#39;re playing a basic card game,

5
00:00:30,869 --> 00:00:36,320
and record the suit of 5 cards you draw from a deck. A possible outcome and corresponding

6
00:00:36,320 --> 00:00:40,640
vector to contain this information could be this one

7
00:00:40,640 --> 00:00:44,610
Of course we could also assign this character vector to a new variable, drawn_suits for

8
00:00:44,610 --> 00:00:46,110
example.

9
00:00:46,110 --> 00:00:49,289
We now have a character vector, drawn_suits.

10
00:00:49,289 --> 00:00:55,750
We can assert that it is a vector, by typing is dot vector drawn_suits

11
00:00:55,750 --> 00:01:01,579
Likewise, you could create a vector of integers for example to store how much cards of each

12
00:01:01,579 --> 00:01:09,970
suit remain after you drew the 5 cards. Let&#39;s call this vector remain. There are 11 more

13
00:01:09,970 --> 00:01:15,640
spades, 12 more hearts, 11 diamonds, and all 13 clubs still remain.

14
00:01:15,640 --> 00:01:21,000
If you print remain to the console, ..., it looks ok, but it&#39;s not very informative. How

15
00:01:21,000 --> 00:01:25,830
does somebody else know that the first value corresponds to spades? Wouldn&#39;t it be useful

16
00:01:25,830 --> 00:01:32,530
if you could attach labels to the vector elements? You can do this in R by naming the vector.

17
00:01:32,530 --> 00:01:36,860
You can use the `names()` function for this. Let&#39;s first create another character vector,

18
00:01:36,860 --> 00:01:41,409
`suits`, that contains the strings &quot;spades&quot;, &quot;hearts&quot;, &quot;diamonds&quot;, and &quot;clubs&quot;, the names

19
00:01:41,409 --> 00:01:43,829
you want to give your vector elements.

20
00:01:43,829 --> 00:01:49,259
Now, this line of code, ..., sets the names of the elements in `remain` to the strings

21
00:01:49,259 --> 00:01:54,890
in suits. If you now print remain to the console, ... , you&#39;ll see that the suits information

22
00:01:54,890 --> 00:02:00,130
is accompanied by the proper labels. Great! If you don&#39;t want to bother with setting the

23
00:02:00,130 --> 00:02:05,899
names afterwards, you could just as well create a named vector with a one liner. You can use

24
00:02:05,899 --> 00:02:08,860
equals signs inside the `c()` function:

25
00:02:08,860 --> 00:02:12,940
Notice that here, it&#39;s not necessary to surround the names, &quot;spades&quot;, &quot;hearts&quot;, &quot;diamonds&quot;

26
00:02:12,940 --> 00:02:19,959
and &quot;clubs&quot;, with double quotes, although this also works perfectly fine.

27
00:02:19,959 --> 00:02:25,349
In all three cases, the result is exactly the same. Under the hood, R vectors have attributes

28
00:02:25,349 --> 00:02:30,379
associated with them. What you did when you set the names of the remain vector, is actually

29
00:02:30,379 --> 00:02:35,349
setting the names attributes of the remains object. The `str()` function, that compactly

30
00:02:35,349 --> 00:02:38,970
displays the the structure of an R object, shows this.

31
00:02:38,970 --> 00:02:44,360
You&#39;ll have plenty of fun creating and naming variables in all sorts of ways, but before

32
00:02:44,360 --> 00:02:48,310
I let you to it, there are two more things I want to discuss with you.

33
00:02:48,310 --> 00:02:53,290
First of all, remember the variables you&#39;ve created in the previous chapter? These variables,

34
00:02:53,290 --> 00:02:57,910
such as `my_apples`, equal to 5, and `my_oranges`, equal to the character string &quot;six&quot; at some

35
00:02:57,910 --> 00:03:03,670
point, are actually all vectors themselves. R does not provide a data structure to hold

36
00:03:03,670 --> 00:03:08,430
a single number or a single character string or any other basic data type: they&#39;re all

37
00:03:08,430 --> 00:03:14,269
just vectors of length 1. You can check this by typing is dot vector my_apples, which is

38
00:03:14,269 --> 00:03:19,430
TRUE, and is dot vector my_oranges, which is TRUE as well.

39
00:03:19,430 --> 00:03:25,200
That these variables are actually vectors of length 1, can be checked using the length()

40
00:03:25,200 --> 00:03:26,879
function.

41
00:03:26,879 --> 00:03:31,140
This contrasts with the other vectors we&#39;ve created in this video: the drawn_suits vector,

42
00:03:31,140 --> 00:03:35,440
for example, has length 5.

43
00:03:35,440 --> 00:03:41,330
The last important thing is that in R, a vector can only hold elements of the same type. They&#39;re

44
00:03:41,330 --> 00:03:46,209
also often called *atomic vectors*, to differentiate them from *lists*, another data structure

45
00:03:46,209 --> 00:03:51,110
which can hold elements of different types. This means that you cannot have a vector that

46
00:03:51,110 --> 00:03:57,599
contains both logicals and numerics, for example. If you do try to build such a vector, R automatically

47
00:03:57,599 --> 00:04:02,110
performs coercion to make sure that you end up with a vector that contains elements of

48
00:04:02,110 --> 00:04:06,629
the same type. Let&#39;s see how that works with an example.

49
00:04:06,629 --> 00:04:11,530
In contrast to recording the suits you draw from a deck of cards, suppose now you&#39;re recording

50
00:04:11,530 --> 00:04:16,180
the _ranks_ of the cards. You might want to combine the result of drawing 8 cards like

51
00:04:16,180 --> 00:04:18,690
this, creating a vector drawn_ranks.

52
00:04:18,690 --> 00:04:24,210
If you now inspect this vector, you&#39;ll see that the numeric vector elements have been

53
00:04:24,210 --> 00:04:30,259
coerced to characters, to end up with a homogeneous character vector.

54
00:04:30,259 --> 00:04:35,220
This is also what the class function from before tells us.

55
00:04:35,220 --> 00:04:40,210
The fact that R handles this for us automatically, &#39;upgrading&#39; logicals to numerics and numerics

56
00:04:40,210 --> 00:04:45,690
to characters when necessary along the way, is useful but can also be dangerous, so be

57
00:04:45,690 --> 00:04:51,030
aware of this. If you want to store elements of different types in the same data structure,

58
00:04:51,030 --> 00:04:53,919
you&#39;ll want to use a list. But that&#39;s something for later.

59
00:04:53,919 --> 00:04:58,580
Now, it&#39;s time to step up your betting game in the interactive exercises!

