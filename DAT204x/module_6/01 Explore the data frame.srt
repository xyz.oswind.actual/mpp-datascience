0
00:00:02,780 --> 00:00:08,280
By now, you already learned quite some things in R. Data structures such as vectors, matrices

1
00:00:08,280 --> 00:00:14,400
and lists have no secrets for you anymore. However, R is a statistical programming language,

2
00:00:14,400 --> 00:00:19,970
and in statistics you&#39;ll often be working with data sets. Such data sets are typically

3
00:00:19,970 --> 00:00:26,110
comprised of observations, or instances. All these observations have some variables associated

4
00:00:26,110 --> 00:00:31,710
with them. You can have for example, a data set of 5 people. Each person is an instance,

5
00:00:31,710 --> 00:00:37,510
and the properties about these people, such as for example their name, their age and whether

6
00:00:37,510 --> 00:00:42,539
they have children are the variables. How could you store such information in R? In

7
00:00:42,539 --> 00:00:48,129
a matrix? Not really, because the name would be a character and the age would be a numeric,

8
00:00:48,129 --> 00:00:53,530
these don&#39;t fit in a matrix. In a list maybe? This could work, because you can put practically

9
00:00:53,530 --> 00:00:58,809
anything in a list. You could create a list of lists, where each sublist is a person,

10
00:00:58,809 --> 00:01:04,530
with a name, an age and so on. However, the structure of such a list is not really useful

11
00:01:04,530 --> 00:01:09,140
to work with. What if you want to know all the ages for example? You&#39;d have to write

12
00:01:09,140 --> 00:01:14,979
a lot of R code just to get what you want. But what data structure could we use then?

13
00:01:14,979 --> 00:01:20,890
Meet the data frame. It&#39;s the fundamental data structure to store typical data sets.

14
00:01:20,890 --> 00:01:26,600
It&#39;s pretty similar to a matrix, because it also has rows and columns. Also for data frames,

15
00:01:26,600 --> 00:01:30,939
the rows correspond to the observations, the persons in our example, while the columns

16
00:01:30,939 --> 00:01:36,219
correspond to the variables, or the properties of each of these persons. The big difference

17
00:01:36,219 --> 00:01:41,590
with matrices is that a data frame can contain elements of different types. One column can

18
00:01:41,590 --> 00:01:47,179
contain characters, another one numerics and yet another one logicals. That&#39;s exactly what

19
00:01:47,179 --> 00:01:51,759
we need to store our persons&#39; information in the dataset, right? We could have a column

20
00:01:51,759 --> 00:01:56,299
for the name, which is character, one for the age, which is numeric, and one logical

21
00:01:56,299 --> 00:02:00,619
column to denote whether the person has children.

22
00:02:00,619 --> 00:02:05,079
There still is a restriction on the data types, though. Elements in the same column should

23
00:02:05,079 --> 00:02:10,679
be of the same type. That&#39;s not really a problem, because in one column, the age column for

24
00:02:10,679 --> 00:02:15,519
example, you&#39;ll always want a numeric, because an age is always a number, regardless of the

25
00:02:15,519 --> 00:02:17,150
observation.

26
00:02:17,150 --> 00:02:23,980
So, for the practical part now: creating a data.frame. In most cases, you don&#39;t create

27
00:02:23,980 --> 00:02:29,870
a data frame yourself. Instead, you typically import data from another source. This could

28
00:02:29,870 --> 00:02:34,870
be a csv file, a relational database, but also come from other software packages like

29
00:02:34,870 --> 00:02:37,659
Excel or SPSS.

30
00:02:37,659 --> 00:02:43,500
Of course, R provides ways to manually create data frames as well. You use the data dot

31
00:02:43,500 --> 00:02:49,290
frame function for this. To create our people data frame that has 5 observations and 3 variables,

32
00:02:49,290 --> 00:02:55,299
we&#39;ll have to pass the data frame function 3 vectors that are all of length five. The

33
00:02:55,299 --> 00:03:01,250
vectors you pass correspond to the columns. Let&#39;s create these three vectors first: `name`,

34
00:03:01,250 --> 00:03:03,459
`age` and `child`.

35
00:03:03,459 --> 00:03:08,849
Now, calling the data frame function is simple:

36
00:03:08,849 --> 00:03:13,129
The printout of the data frame already shows very clearly that we&#39;re dealing with a data

37
00:03:13,129 --> 00:03:18,010
set. Notice how the data frame function inferred the names of the columns from the variable

38
00:03:18,010 --> 00:03:23,720
names you passed it. To specify the names explicitly, you can use the same techniques

39
00:03:23,720 --> 00:03:29,989
as for vectors and lists. You can use the names function, ... , or use equals sings

40
00:03:29,989 --> 00:03:36,129
inside the data frame function to name the data frame columns right away.

41
00:03:36,129 --> 00:03:40,810
Like in matrices, it&#39;s also possible to name the rows of the data frame, but that&#39;s generally

42
00:03:40,810 --> 00:03:46,280
not a good idea so I won&#39;t detail on that here.

43
00:03:46,280 --> 00:03:50,230
Before you head over to some exercises, let me shortly discuss the structure of a data

44
00:03:50,230 --> 00:03:51,870
frame some more.

45
00:03:51,870 --> 00:03:57,129
If you look at this structure, ..., there are two things you can see here: First, the

46
00:03:57,129 --> 00:04:01,959
printout looks suspiciously similar to that of a list. That&#39;s because, under the hood,

47
00:04:01,959 --> 00:04:07,709
the data frame actually is a list. In this case, it&#39;s a list with three elements, corresponding

48
00:04:07,709 --> 00:04:13,180
to each of the columns in the data frame. Each list element is a vector of length 5,

49
00:04:13,180 --> 00:04:18,799
corresponding to the number of observations. A requirement that is not present for lists

50
00:04:18,798 --> 00:04:23,600
is that the length of the vectors you put in the list has to be equal. If you try to

51
00:04:23,600 --> 00:04:28,880
create a data frame with 3 vectors that are not all of the same length, you&#39;ll get an

52
00:04:28,880 --> 00:04:29,840
error.

53
00:04:29,840 --> 00:04:35,680
Second, the name column, which you expect to be a character vector, is actually a factor.

54
00:04:35,680 --> 00:04:41,330
That&#39;s because R by default stores the strings as factors. To suppress this behaviour, you

55
00:04:41,330 --> 00:04:45,240
can set the stringsAsFactors argument of the data.frame function to FALSE

56
00:04:45,240 --> 00:04:53,090
Now, the name column actually contains characters.

57
00:04:53,090 --> 00:04:57,310
With this new knowledge, you&#39;re ready for some first exercises on this extremely useful

58
00:04:57,310 --> 00:05:02,639
and powerful data structure. Whenever you&#39;re experimenting with data frames, remember that

59
00:05:02,639 --> 00:05:07,639
they&#39;re actually lists. This gives the data.frame the ability to store vectors of different

60
00:05:07,639 --> 00:05:13,530
types. On top, R also has some additional functionality built in to easily extend and

61
00:05:13,530 --> 00:05:17,060
subset data frames. I&#39;ll talk more about that in the next video!

