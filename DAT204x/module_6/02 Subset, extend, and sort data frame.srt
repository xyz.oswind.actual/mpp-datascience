0
00:00:02,460 --> 00:00:08,100
The data frame is somewhere on the intersection between matrices and lists. To subset a dataframe

1
00:00:08,100 --> 00:00:13,690
you can thus use subsetting syntax from both matrices and lists. On the one hand, you can

2
00:00:13,690 --> 00:00:18,960
use the single brackets from matrix subsetting, while you can also use the double brackets

3
00:00:18,960 --> 00:00:23,810
and dollar sign notation that you use to select list elements.

4
00:00:23,810 --> 00:00:28,680
We&#39;ll continue with the data frame that contained some information on 5 persons. Have another

5
00:00:28,680 --> 00:00:31,989
look at its definition here.

6
00:00:31,989 --> 00:00:36,500
Let&#39;s start with selecting single elements from a data frame. To select the age of Frank,

7
00:00:36,500 --> 00:00:42,440
who is on row 3 in the data frame, you can use the exact same syntax as for matrix subsetting:

8
00:00:42,440 --> 00:00:48,559
single brackets with two indices inside. The row, index 3, comes first, and the column,

9
00:00:48,559 --> 00:00:50,539
index 2, comes second:

10
00:00:50,539 --> 00:00:56,320
Indeed, Frank is 21 years old. You can also use the column names to refer to the columns

11
00:00:56,320 --> 00:00:59,129
of course:

12
00:00:59,129 --> 00:01:03,969
Just as for matrices, you can also choose to omit one of the two indices or names, to

13
00:01:03,969 --> 00:01:09,360
end up with an entire row or an entire column. If you want to have all information on Frank,

14
00:01:09,360 --> 00:01:10,540
you can use this command:

15
00:01:10,540 --> 00:01:16,119
The result is a data frame with a single observation, because there has to be a way to store the

16
00:01:16,119 --> 00:01:19,700
different types. On the other hand, to get the entire age column,

17
00:01:19,700 --> 00:01:21,409
you could use this command:

18
00:01:21,409 --> 00:01:29,460
Here, the result is a vector, because columns contain elements of the same type.

19
00:01:29,460 --> 00:01:33,939
Subsetting the data frame to end up with a sub data frame that contains multiple observations

20
00:01:33,939 --> 00:01:39,110
also works just as you&#39;d expect. Have a look at this command, that selects the age and

21
00:01:39,110 --> 00:01:44,439
parenting information on Frank and Cath:

22
00:01:44,439 --> 00:01:49,780
All of these examples show that you can subset data frames exactly as you did with matrices.

23
00:01:49,780 --> 00:01:55,060
The only difference occurs when you specify only one index inside `people`. In the matrix

24
00:01:55,060 --> 00:01:59,579
case, R would go through each column from left to right to find the index you specified.

25
00:01:59,579 --> 00:02:04,450
In the data frame case, you simply end up with a new data frame, that only contains

26
00:02:04,450 --> 00:02:09,990
the column you specified. This command, for example, gives the age column as a data.frame.

27
00:02:09,990 --> 00:02:13,940
I repeat: a data.frame, not a vector!

28
00:02:13,940 --> 00:02:20,290
Why so? Let me talk about subsetting data.frames with list syntax and it&#39;ll all become clear.

29
00:02:20,290 --> 00:02:24,580
Remember when I told that a data frame is actually a list containing all vectors of

30
00:02:24,580 --> 00:02:30,240
the same length? This means that you can also use the list syntax to select elements. Say,

31
00:02:30,240 --> 00:02:34,280
for example, you typed people dollar sign age:

32
00:02:34,280 --> 00:02:39,050
The age vector inside the data frame gets returned, so you end up with the age column.

33
00:02:39,050 --> 00:02:44,250
Likewise, you can use the double brackets notation with a name ... or with an index.

34
00:02:44,250 --> 00:02:48,000
In all cases, the result is a vector.

35
00:02:48,000 --> 00:02:53,730
You can also use single brackets to subset lists, but this generates a new list, containing

36
00:02:53,730 --> 00:02:57,740
only the specified elements. Take this command for example:

37
00:02:57,740 --> 00:03:02,560
The result is still a data frame, which is a list, but this time containing only the

38
00:03:02,560 --> 00:03:09,720
&quot;age&quot; element. This explains why before, this command gave a data frame instead of vector.

39
00:03:09,720 --> 00:03:15,500
Again, using single brackets or double brackets to subset data structures can have serious

40
00:03:15,500 --> 00:03:22,510
consequences, so always think about what you&#39;re dealing with and how you should handle it.

41
00:03:22,510 --> 00:03:27,010
Once you know how to correctly subset data frames, extending those data frames is pretty

42
00:03:27,010 --> 00:03:32,520
simple. Sometimes, you&#39;ll want to add a column, a new variable, to your data frame. Other

43
00:03:32,520 --> 00:03:38,400
times, it&#39;s also useful to add new rows, so new observations, to your data frame.

44
00:03:38,400 --> 00:03:42,370
To add a column, which actually comes down to adding a new element to the list, you can

45
00:03:42,370 --> 00:03:47,380
use the dollar sign or the double square brackets. Suppose you want to add a column `height`,

46
00:03:47,380 --> 00:03:53,220
the information of which is already in a vector `height`. This call ... Or this call ... Will

47
00:03:53,220 --> 00:03:55,520
do the trick.

48
00:03:55,520 --> 00:04:00,240
You can also use the `cbind()` function that you&#39;ve learned to build and extend matrices.

49
00:04:00,240 --> 00:04:06,070
It works just the same for data.frames. To add a weight column, in kilograms, for example.

50
00:04:06,070 --> 00:04:12,710
If `cbind()` works, than surely `rbind()` will work fine as well. Indeed, you can use

51
00:04:12,710 --> 00:04:18,350
`rbind()` to add new rows to your observations. Suppose you want to add the information of

52
00:04:18,350 --> 00:04:24,469
another person, Tom, to the data frame. Simply creating a vector with the name, age, height

53
00:04:24,469 --> 00:04:30,039
etc, won&#39;t work, because a vector can&#39;t contain elements of different types. You&#39;ll have to

54
00:04:30,039 --> 00:04:35,129
create a new data frame containing only a single observation, and add that to the data

55
00:04:35,129 --> 00:04:39,270
frame using rbind. Let&#39;s call this mini data frame `tom`.

56
00:04:39,270 --> 00:04:44,000
Now, we can use `rbind()` to bind `people` and `tom` together:

57
00:04:44,000 --> 00:04:49,780
Wait, what? R throws an error. Names do not match previous names. This means that the

58
00:04:49,780 --> 00:04:54,889
names in `people` and `tom` do not match. We&#39;ll have to improve our definition of `tom`

59
00:04:54,889 --> 00:04:56,379
to make the merge successful:

60
00:04:56,379 --> 00:05:01,919
Now, `rbind()` will work as you&#39;d want it to work.

61
00:05:01,919 --> 00:05:06,060
So adding a column to a data frame is pretty easy, but adding new observations requires

62
00:05:06,060 --> 00:05:08,990
some care.

63
00:05:08,990 --> 00:05:13,460
Last but not least, there are also ways of reordering your data frame. This comes down

64
00:05:13,460 --> 00:05:17,680
to reshuffling the order of the observations in your data frame, to get some more insight

65
00:05:17,680 --> 00:05:22,740
in your data. Suppose you want to sort the data frame by age, such that the youngest

66
00:05:22,740 --> 00:05:27,069
person is on top, and the oldest on the bottom; how would this work?

67
00:05:27,069 --> 00:05:31,419
A first guess would be to use the `sort()` function, which sorts a vector into ascending

68
00:05:31,419 --> 00:05:36,460
or descending order. Let&#39;s experiment a bit with this, and sort the age column:

69
00:05:36,460 --> 00:05:42,199
What gets returned is an ordered version of the age column. Looks nice, but it&#39;s not really

70
00:05:42,199 --> 00:05:47,330
what we need. There&#39;s not a clear way in which we could use these values, to change the ordering

71
00:05:47,330 --> 00:05:51,729
of the rows in the data.frame. Instead of the sort() function, we&#39;ll need the `order()`

72
00:05:51,729 --> 00:05:57,659
function to help us out. Let&#39;s test the order function on the age column:

73
00:05:57,659 --> 00:06:02,629
Order returns a vector `ranks` with the ranked position of each element. To see what this

74
00:06:02,629 --> 00:06:07,400
means, let&#39;s print the age column and try to reproduce the result.

75
00:06:07,400 --> 00:06:13,900
So, the lowest value in the age column is 21, it&#39;s in third position. This index, 3,

76
00:06:13,900 --> 00:06:19,210
thus comes first in the `ranks` vector. The second lowest value in the age column is 28,

77
00:06:19,210 --> 00:06:24,650
and is in first position in the age column. The index 1 thus comes second in the rank

78
00:06:24,650 --> 00:06:29,990
vector. The highest value is 39 at the fourth position of the age column. This index comes

79
00:06:29,990 --> 00:06:34,139
in the last place in rank. Makes sense?

80
00:06:34,139 --> 00:06:39,020
The ranks vector contains indices that are now used to perform a selection in a scrambled

81
00:06:39,020 --> 00:06:44,419
order, such that the result is an ordered data frame. Cool, right?

82
00:06:44,419 --> 00:06:51,210
To sort in descending order, you can set the decreasing argument to TRUE:

83
00:06:51,210 --> 00:06:55,770
These were just the basics on data frame subsetting and manipulation: know that there are tons

84
00:06:55,770 --> 00:07:00,309
of packages out there, such as data.table and dplyr, that will help you manipulate,

85
00:07:00,309 --> 00:07:05,559
filter, merge and sort your data frames. Head over to the exercises and become a data frame

86
00:07:05,559 --> 00:07:06,129
hero!

