0
00:00:02,300 --> 00:00:08,269
Hello! Welcome to the first video of the introduction to R course. My name is Filip, I&#39;m a content

1
00:00:08,269 --> 00:00:12,670
creator at DataCamp and I will help you take your first steps in this fascinating programming

2
00:00:12,670 --> 00:00:13,690
language.

3
00:00:13,690 --> 00:00:18,849
R, also called the Language for Statistical Computing, was developed by Ross Ihaka and

4
00:00:18,849 --> 00:00:23,509
Robert Gentleman at the University of Auckland in the nineties. It is considered an open

5
00:00:23,509 --> 00:00:27,800
source implementation of the S language, which was developed by John Chambers in the Bell

6
00:00:27,800 --> 00:00:30,739
Laboratories in the eighties.

7
00:00:30,739 --> 00:00:36,120
R provides a wide variety of statistical techniques and visualization capabilities. Another very

8
00:00:36,120 --> 00:00:41,649
important feature about R is that it is higly extensible. Because of this and more importantly

9
00:00:41,649 --> 00:00:46,510
because R is open source, it actually was the vehicle to bring the power of S to a larger

10
00:00:46,510 --> 00:00:49,269
community.

11
00:00:49,269 --> 00:00:53,829
Like in every programming language, there are pros and cons. Because I like R, let&#39;s

12
00:00:53,829 --> 00:00:56,379
start with the pros first.

13
00:00:56,379 --> 00:01:02,280
First of all, it&#39;s open source, so it&#39;s free! Next, R&#39;s graphical capabilities are top notch

14
00:01:02,280 --> 00:01:07,650
and it&#39;s very easy to build publication quality plots. In comparison to many other statistical

15
00:01:07,650 --> 00:01:12,980
software packages, R uses a command line interface, which means that you have to actually code

16
00:01:12,980 --> 00:01:17,830
things in your console and in scripts. While this might be frustrating at first, it makes

17
00:01:17,830 --> 00:01:22,830
your work reproducible. You can now wrap your work in R scripts, which you can then easily

18
00:01:22,830 --> 00:01:28,030
share with your colleagues. Because of these advantages, R appeals to a large audience

19
00:01:28,030 --> 00:01:32,390
both in academia and in business. Moreover, it&#39;s fairly easy to create

20
00:01:32,390 --> 00:01:38,110
R packages, which are extensions of R, aimed at solving particular problems. R&#39;s

21
00:01:38,110 --> 00:01:42,920
very active community has created thousands of well-documented R packages for a very broad

22
00:01:42,920 --> 00:01:48,520
range of applications in the financial sector, health care and for cutting edge research.

23
00:01:48,520 --> 00:01:53,790
However, as with anything, there are also some disadvantages. R seems to be relatively

24
00:01:53,790 --> 00:01:58,930
easy to learn at first, but it is hard to really master it. Also the fact that R is

25
00:01:58,930 --> 00:02:02,610
command-based is a frightening detail for statisticians that are used to the typical

26
00:02:02,610 --> 00:02:07,670
point and click programs out there. This steep learning curve sometimes results in poorly

27
00:02:07,670 --> 00:02:13,630
written R code that can be hard, both to read and to maintain. Furthermore, poorly written

28
00:02:13,630 --> 00:02:17,900
R code can become slow if you&#39;re working with large data sets.

29
00:02:17,900 --> 00:02:22,850
But fear not! DataCamp is here to help you master R in no time! Use our courses to get

30
00:02:22,850 --> 00:02:28,040
a grip on R&#39;s fundamental concepts, and you can always consult RDocumentation for documentation

31
00:02:28,040 --> 00:02:34,640
on all publicly available R packages. Next up: your first steps in R!

32
00:02:34,640 --> 00:02:38,840
One of the most important components of R, and where most of the action happens, is the

33
00:02:38,840 --> 00:02:43,950
R console. In DataCamp&#39;s interactive exercises, the console will be your companion during

34
00:02:43,950 --> 00:02:49,210
your entire learning journey! It&#39;s a place where you can execute R commands. You simply

35
00:02:49,210 --> 00:02:54,069
type something at the prompt in the console, hit Enter, and R interprets and executes your

36
00:02:54,069 --> 00:02:55,660
command.

37
00:02:55,660 --> 00:02:59,740
Let&#39;s start our experiments by having R do some basic arithmetic; we&#39;ll calculate the

38
00:02:59,740 --> 00:03:06,730
sum of 1 and 2. We simply type 1 + 2 in the console and hit Enter. R compiles what you

39
00:03:06,730 --> 00:03:11,380
typed, calculates the result and prints that result as a numerical value.

40
00:03:11,380 --> 00:03:16,060
Now let&#39;s try to type some text in the console. We use double quotes for this.

41
00:03:16,060 --> 00:03:20,480
You can also simply type a number and hit Enter.

42
00:03:20,480 --> 00:03:26,060
R understood your character string and numerical value, but simply printed that string as an

43
00:03:26,060 --> 00:03:31,990
output. This brings me to the first super important concept in R: the variable. A variable

44
00:03:31,990 --> 00:03:36,599
allows you to store a value or an object in R. You can then later use this variable&#39;s

45
00:03:36,599 --> 00:03:41,950
name to easily access the value or the object that is stored within this variable. You can

46
00:03:41,950 --> 00:03:47,599
use the less than sign followed by a dash to create a variable. Suppose the number 2

47
00:03:47,599 --> 00:03:52,560
is the height of a rectangle. Let&#39;s assign this value 2 to a variable height. We type

48
00:03:52,560 --> 00:03:56,470
height, less than sign, dash, 2:

49
00:03:56,470 --> 00:04:00,580
This time, R does not print anything, because it assumes that you will be using this variable

50
00:04:00,580 --> 00:04:07,069
in the future. If we now simply type and execute height in the console, R returns 2:

51
00:04:07,069 --> 00:04:11,099
We can do a similar thing for the width of our imaginary rectangle. We assign the value

52
00:04:11,099 --> 00:04:12,720
4 to a variable width.

53
00:04:12,720 --> 00:04:18,359
If we type width, we see that indeed, it contains the value 4.

54
00:04:18,358 --> 00:04:23,789
As you&#39;re assigning variables in the R console, you&#39;re actually accumulating an R workspace.

55
00:04:23,789 --> 00:04:28,289
It&#39;s the place where variables and information is stored in R. You can access the objects

56
00:04:28,289 --> 00:04:33,759
in the workspace with the ls() function. Simply type ls followed by empty parentheses and

57
00:04:33,759 --> 00:04:35,689
hit enter.

58
00:04:35,689 --> 00:04:39,349
This shows you a list of all the variables you have created in the R session. If you

59
00:04:39,349 --> 00:04:44,110
have followed all the examples up to now, you should see &quot;height&quot; and &quot;width&quot;. This

60
00:04:44,110 --> 00:04:48,419
tells you that there are two objects in your workspace at the moment. When you type height

61
00:04:48,419 --> 00:04:53,159
in the console, R looks for the variable height in the workspace, finds it, and prints the

62
00:04:53,159 --> 00:04:58,650
corresponding value. If, however, we try to print a non-existing variable, depth for example,

63
00:04:58,650 --> 00:05:04,990
R throws an error, because depth is not defined in the workspace and thus not found.

64
00:05:04,990 --> 00:05:09,139
The principle of accumulating a workspace through variable assignment makes these variables

65
00:05:09,139 --> 00:05:14,779
available for further use. Suppose we want to find out the area of our imaginary rectangle,

66
00:05:14,779 --> 00:05:20,520
which is height multiplied by width. Let&#39;s go ahead and type height asterisk width.

67
00:05:20,520 --> 00:05:26,289
The result is 8, as you&#39;d expect. We can take it one step further and also assign the result

68
00:05:26,289 --> 00:05:31,289
of this calculation to a new variable, area. We again use the assignment operator.

69
00:05:31,289 --> 00:05:36,279
If you now type area, you&#39;ll see that it contains 8 as well.

70
00:05:36,279 --> 00:05:41,800
Inspecting the workspace again with ls, shows that the workspace contains three objects

71
00:05:41,800 --> 00:05:44,860
now: area, height and width.

72
00:05:44,860 --> 00:05:49,529
Now, this is all great, but what if you want to recalculate the area of your imaginary

73
00:05:49,529 --> 00:05:54,520
rectangle when the height is 3 and the width is 6? You&#39;d have to reassign the variables

74
00:05:54,520 --> 00:05:58,919
width and height, and then recalculate the area. That&#39;s quite some coding you have to

75
00:05:58,919 --> 00:06:01,610
redo, isn&#39;t it?

76
00:06:01,610 --> 00:06:06,089
This is the place where R scripts come in! An R script is simply a text file, containing

77
00:06:06,089 --> 00:06:11,439
a collection of succesive lines of R code that solve a particular task. When using R,

78
00:06:11,439 --> 00:06:15,469
you will build a lot of scripts to make things easier for you, and hopefully automate parts

79
00:06:15,469 --> 00:06:16,650
of your work.

80
00:06:16,650 --> 00:06:21,330
We can create a new script, &quot;rectangle.R&quot;, that contains the code that we&#39;ve written

81
00:06:21,330 --> 00:06:22,879
up to now.

82
00:06:22,879 --> 00:06:27,529
Next, you can run this script. Depending on the software you&#39;re using, there&#39;s several

83
00:06:27,529 --> 00:06:32,110
ways to do this. In the DataCamp interface, you simply have to hit Submit Answer button

84
00:06:32,110 --> 00:06:37,210
to submit your script. In RStudio, R&#39;s most popular IDE, there&#39;s a &quot;Source&quot; button to

85
00:06:37,210 --> 00:06:43,069
run the script you&#39;re editing. In either case, R goes through your code, line by line, executing

86
00:06:43,069 --> 00:06:48,439
every command one by one, just as if you are manually typing each command in the console.

87
00:06:48,439 --> 00:06:52,389
The cool thing is, that if you want to change some variables here and there, you can simply

88
00:06:52,389 --> 00:06:57,949
do this in the script, source your code again and see how the output has changed. Let&#39;s

89
00:06:57,949 --> 00:07:02,520
change the height to 3 and the width to 6, and rerun the script.

90
00:07:02,520 --> 00:07:06,759
This is the true power of reproducibility at work here! This also makes sharing your

91
00:07:06,759 --> 00:07:11,520
scripts very easy; you just send your R file and the recipient can simply run it on his

92
00:07:11,520 --> 00:07:16,759
or her own system. However, you&#39;ll sometimes have to help people understand what you&#39;ve

93
00:07:16,759 --> 00:07:21,749
written and clarify your code here and there. You can use comments for this, which are lines

94
00:07:21,749 --> 00:07:26,559
of code that start with a pound sign, like this one. Let&#39;s extend the script with some

95
00:07:26,559 --> 00:07:31,819
more information. We add three lines of comments; one line before the assignment of height and

96
00:07:31,819 --> 00:07:36,259
width, another one before the calculation of area, and a last one just before the printout

97
00:07:36,259 --> 00:07:37,009
of area.

98
00:07:37,009 --> 00:07:41,990
If you would run this script again, the lines starting with the pound sign do not affect

99
00:07:41,990 --> 00:07:47,559
R&#39;s execution. But beware! Perfectly valid R code that you place behind a comment sign

100
00:07:47,559 --> 00:07:52,939
is interpreted as a comment and won&#39;t be considered by R!

101
00:07:52,939 --> 00:07:56,879
Let&#39;s have a final look at the workspace: as before, there are three variables in your

102
00:07:56,879 --> 00:07:59,580
workspace: area, height and width.

103
00:07:59,580 --> 00:08:03,969
If you&#39;re working on big projects that have tons of data involved, this workspace can

104
00:08:03,969 --> 00:08:08,659
consume a lot of your computer&#39;s resources. It&#39;s a good idea to clean up your workspace

105
00:08:08,659 --> 00:08:13,610
from time to time to make sure it does not get bloated. You can use the rm() function

106
00:08:13,610 --> 00:08:19,929
for this. To remove the area variable, you can type rm followed by area inside parenthesis.

107
00:08:19,929 --> 00:08:26,869
If you now check your workspace again, using ls(), you will see that only height and width

108
00:08:26,869 --> 00:08:27,710
remain.

109
00:08:27,710 --> 00:08:31,999
Trying to access the contents of area now results in an error, because it&#39;s no longer

110
00:08:31,999 --> 00:08:35,630
in your workspace.

111
00:08:35,630 --> 00:08:39,959
Now it&#39;s time for some interactive exercises! Try to solve the different instructions and

112
00:08:39,958 --> 00:08:43,190
let DataCamp&#39;s tailored feedback guide you to R mastery!

