0
00:00:03,030 --> 00:00:08,700
In the previous video you saw that R is also know as the Language for Statistical Computing.

1
00:00:08,700 --> 00:00:13,790
Data is the center of any statistical analysis, so let me introduce you to some of R&#39;s fundamental

2
00:00:13,790 --> 00:00:19,990
data types, also called atomic vector types. Throughout our experiments, we will use the

3
00:00:19,990 --> 00:00:26,010
function class(). This is a useful way to see what type a variable is. Let&#39;s head over

4
00:00:26,010 --> 00:00:30,570
to the console and start with TRUE, in capital letters.

5
00:00:30,570 --> 00:00:36,730
TRUE is a logical. That&#39;s also what class(TRUE) tells us. Logicals are so-called boolean values,

6
00:00:36,730 --> 00:00:39,579
and can be either `TRUE` or `FALSE`.

7
00:00:39,579 --> 00:00:45,769
Well, actually, `NA`, to denote missing values, is also a logical, but I won&#39;t go into detail

8
00:00:45,769 --> 00:00:52,079
on that here. `TRUE` and `FALSE` can be abbreviated to `T` and `F` respectively, as you can see

9
00:00:52,079 --> 00:00:59,239
here. However, I want to strongly encourage you to use the full versions, `TRUE` and `FALSE`.

10
00:00:59,239 --> 00:01:06,490
Next, let&#39;s experiment with numbers. The values 2 and 2.5 are called numerics in R. You can

11
00:01:06,490 --> 00:01:11,290
perform all sorts of operations on them such as addition, subtraction, multiplication,

12
00:01:11,290 --> 00:01:17,100
division and many more. A special type of numeric is the integer. It is a way to represent

13
00:01:17,100 --> 00:01:22,680
natural numbers like 1 and 2. To specify that a number is integer, you can add a capital

14
00:01:22,680 --> 00:01:24,000
L to them.

15
00:01:24,000 --> 00:01:29,869
You don&#39;t see the difference between the integer 2 and the numeric 2 from the output. However,

16
00:01:29,869 --> 00:01:33,520
the `class()` function reveals the difference.

17
00:01:33,520 --> 00:01:38,180
Instead of asking for the class of a variable, you can also use the is-dot-functions to see

18
00:01:38,180 --> 00:01:43,719
whether variables are actually of a certain type. To see if a variable is a numeric, we

19
00:01:43,719 --> 00:01:47,090
can use the is-dot-numeric function.

20
00:01:47,090 --> 00:01:54,340
It appears that both are numerics. To see if a variable is integer, we can use is-dot-integer.

21
00:01:54,340 --> 00:01:59,070
This shows us that integers are numerics, but that not all numerics are integers, so

22
00:01:59,070 --> 00:02:02,280
there&#39;s some kind of type hierarchy going on here.

23
00:02:02,280 --> 00:02:08,990
Last but not least, there&#39;s the character string. The class of this type of object is

24
00:02:08,990 --> 00:02:10,619
&quot;character&quot;.

25
00:02:10,619 --> 00:02:14,640
It&#39;s important to note that there are other data types in R, such as double for higher

26
00:02:14,640 --> 00:02:20,950
precision numerics, complex for handling complex numbers, and raw to store raw bytes. However,

27
00:02:20,950 --> 00:02:26,060
you will have tons of fun working with numerics, integers, logicals and characters in the remainder

28
00:02:26,060 --> 00:02:29,470
of this introductory course so we&#39;ll leave these alone for now.

29
00:02:29,470 --> 00:02:33,879
There are cases in which you want to change the type of a variable to another one. How

30
00:02:33,879 --> 00:02:40,090
would that work? This is where coercion comes into play! By using the as dot functions one

31
00:02:40,090 --> 00:02:45,250
can coerce the type of a variable to another type. Many ways of transformation between

32
00:02:45,250 --> 00:02:48,989
types are possible. Have a look at these examples.

33
00:02:48,989 --> 00:02:54,720
The first command here coerces the logical TRUE to a numeric. FALSE, however, coerces

34
00:02:54,720 --> 00:03:01,400
to the numeric zero. We can also coerce numerics to characters. But what about the other way

35
00:03:01,400 --> 00:03:05,879
around? Can you also coerce characters to numerics? Sure you can!

36
00:03:05,879 --> 00:03:11,299
You can even convert this character string, &quot;4.5&quot;, to an integer, but this implies some

37
00:03:11,299 --> 00:03:17,150
information loss, because you cannot keep the decimal part here. But beware: coercion,

38
00:03:17,150 --> 00:03:21,849
as in converting data types, is not always possible. Let&#39;s try to convert the character

39
00:03:21,849 --> 00:03:27,730
&quot;Hello&quot; to a numeric. This conversion outputs an NA, a missing value.

40
00:03:27,730 --> 00:03:32,220
R doesn&#39;t understand how to transform &quot;Hello&quot; into a numeric, and decides to return a Not

41
00:03:32,220 --> 00:03:34,230
Available instead.

42
00:03:34,230 --> 00:03:40,000
You already have the essentials on what R is, how to use its basic features and what

43
00:03:40,000 --> 00:03:44,549
are the most important data types you will encounter in your R quest. Now head over to

44
00:03:44,549 --> 00:03:47,129
the exercises and I&#39;ll see you in the next chapter!

