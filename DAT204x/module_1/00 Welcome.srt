0
00:00:00,000 --> 00:00:02,916
[MUSIC PLAYING]

1
00:00:12,650 --> 00:00:16,120
JONATHAN CORNELISSEN: Hi, my name is Jonathan, and I am the CEO of DataCamp.

2
00:00:16,120 --> 00:00:18,570
DataCamp is an online data science school,

3
00:00:18,570 --> 00:00:21,980
and together with Microsoft, we have created a great introduction

4
00:00:21,980 --> 00:00:23,840
to R course.

5
00:00:23,840 --> 00:00:28,190
You might wonder, what is R, and why should I learn it?

6
00:00:28,190 --> 00:00:32,450
R is the lingua franca of data science, used by millions of data experts

7
00:00:32,450 --> 00:00:35,720
around the globe to map marketing trends, model financial data,

8
00:00:35,720 --> 00:00:38,500
model pharmaceutical data, and so on.

9
00:00:38,500 --> 00:00:41,420
It is literally used by professionals in every industry

10
00:00:41,420 --> 00:00:44,780
for both small and big data applications.

11
00:00:44,780 --> 00:00:47,310
And the best thing is, R is free.

12
00:00:47,310 --> 00:00:49,480
R is an open source statistical programming

13
00:00:49,480 --> 00:00:55,350
language supported by a great community that develops new tools every day.

14
00:00:55,350 --> 00:00:58,380
Now the best way to learn R is by doing.

15
00:00:58,380 --> 00:01:00,790
And that&#39;s why you&#39;ll spend most of your course time

16
00:01:00,790 --> 00:01:03,510
inside our interactive learning interface.

17
00:01:03,510 --> 00:01:05,880
You start with a short video lesson, and then

18
00:01:05,880 --> 00:01:08,580
move into the interactive coding environments,

19
00:01:08,580 --> 00:01:10,440
where you&#39;ll learn through fun challenges

20
00:01:10,440 --> 00:01:15,260
how to use R for statistics, visualizations, and much more.

21
00:01:15,260 --> 00:01:19,710
Solve real data problems by receiving instance and personalized feedback that

22
00:01:19,710 --> 00:01:22,660
guides you towards the solution.

23
00:01:22,660 --> 00:01:24,920
This introduction to R course is the best way

24
00:01:24,920 --> 00:01:27,640
to start learning R for data science.

25
00:01:27,640 --> 00:01:30,640
No prior knowledge or technical backgrounds is required.

26
00:01:30,640 --> 00:01:35,200
So enroll now and get yourself ready for the skill of the future.

