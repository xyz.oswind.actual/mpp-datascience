0
00:00:02,139 --> 00:00:07,060
If you have some background in statistics, you&#39;ll have heard about categorical variables.

1
00:00:07,060 --> 00:00:12,289
Unlike numerical variables, categorical variables can only take on a limited number of different

2
00:00:12,289 --> 00:00:19,380
values. Otherwise put, a categorical variable can only belong to a limited number of categories.

3
00:00:19,380 --> 00:00:23,919
As R is a statistical programming language, it&#39;s not a surprise that there exists a specific

4
00:00:23,919 --> 00:00:29,730
data structure for this: factors. If you store categorical data as factors, you can rest

5
00:00:29,730 --> 00:00:34,570
assured that all the statistical modelling techniques will handle such data correctly.

6
00:00:34,570 --> 00:00:41,809
A good example of a categorical variable is a person&#39;s blood type: it can be A, B, AB

7
00:00:41,809 --> 00:00:46,929
or O. Suppose we have asked 8 people what their bloodtype is and recorded the information

8
00:00:46,929 --> 00:00:48,840
as a vector `blood`.

9
00:00:48,840 --> 00:00:54,329
Now, for R it is not yet clear that you&#39;re dealing with categorical variables, or factors,

10
00:00:54,329 --> 00:01:00,260
here. To convert this vector to a factor, you can use the `factor()` function.

11
00:01:00,260 --> 00:01:04,140
The printout looks somewhat different than the original one: there are no double quotes

12
00:01:04,140 --> 00:01:09,860
anymore and also the factor levels, corresponding to the different categories, are printed.

13
00:01:09,860 --> 00:01:14,610
R basically does two things when you call the factor function on a character vector:

14
00:01:14,610 --> 00:01:18,500
first of all, it scans through the vector to see the different categories that are in

15
00:01:18,500 --> 00:01:24,710
there. In this case, that&#39;s &quot;A&quot;, &quot;AB&quot;, &quot;B&quot; and &quot;O&quot;. Notice here that R sorts the levels

16
00:01:24,710 --> 00:01:30,409
alphabetically. Next, it converts the character vector, blood in this example, to a vector

17
00:01:30,409 --> 00:01:36,079
of integer values. These integers correspond to a set of character values to use when the

18
00:01:36,079 --> 00:01:41,469
factor is displayed. Inspecting the structure reveals this:

19
00:01:41,469 --> 00:01:45,689
We&#39;re dealing with a factor with 4 levels. The &quot;A&quot;&#39;s are encoded as 1, because it&#39;s the

20
00:01:45,689 --> 00:01:53,070
first level, &quot;AB&quot; is encoded as 2, &quot;B&quot; as 3 and &quot;O&quot; as 4. Why this conversion? Well,

21
00:01:53,070 --> 00:01:57,780
it can be that your categories are very long character strings. Each time repeating this

22
00:01:57,780 --> 00:02:03,020
string per observation can take up a lot of memory. By using this simple encoding, much

23
00:02:03,020 --> 00:02:08,680
less space is necessary. Just remember that factors are actually integer vectors, where

24
00:02:08,680 --> 00:02:13,610
each integer corresponds to a category, or a level.

25
00:02:13,610 --> 00:02:18,810
As I said before, R automatically infers the factor levels from the vector you pass it

26
00:02:18,810 --> 00:02:23,650
and orders them alphabetically. If you want a different order in the levels, you can specify

27
00:02:23,650 --> 00:02:28,090
the levels argument in the factor function.

28
00:02:28,090 --> 00:02:32,930
If you compare the structures of `blood_factor` and `blood_factor2`, you&#39;ll see that the encoding

29
00:02:32,930 --> 00:02:35,819
is different now.

30
00:02:35,819 --> 00:02:40,420
Next to changing the order of the levels, it is possible to manually specify the level

31
00:02:40,420 --> 00:02:46,129
names, instead of letting R choose them. Suppose that for clarity, you want to display the

32
00:02:46,129 --> 00:02:53,599
blood types as `BT_A`, `BT_AB`, `BT_B` and `BT_O`. To name the factor afterwards, you

33
00:02:53,599 --> 00:02:58,280
can use the `levels()` function. Similar to the names function to name vectors, you can

34
00:02:58,280 --> 00:03:01,370
pass a vector to levels blood_factor.

35
00:03:01,370 --> 00:03:07,519
You can also specify the category names, or levels, by specifying the `labels` argument

36
00:03:07,519 --> 00:03:09,319
in `factor()`.

37
00:03:09,319 --> 00:03:15,370
I admit it, it&#39;s a bit confusing. For both of these approaches, it&#39;s important to follow

38
00:03:15,370 --> 00:03:21,720
the same order as the order of the factor levels: first A, then AB, then B and then

39
00:03:21,720 --> 00:03:27,599
O. But this can be pretty dangerous: you might have mistakenly changed the order.

40
00:03:27,599 --> 00:03:32,260
To solve this, you can use a combination of manually specifying the `levels` and the `labels`

41
00:03:32,260 --> 00:03:38,330
argument when creating a factor. With `levels`, you specify the order, just like before, while

42
00:03:38,330 --> 00:03:42,470
with the labels, you specify a new name for the categories:

43
00:03:42,470 --> 00:03:48,840
In the world of categorical variables, there&#39;s also a difference between nominal categorical

44
00:03:48,840 --> 00:03:54,230
variables and ordinal categorical variables. The nominal categorical variables has no implied

45
00:03:54,230 --> 00:03:59,260
order. For example, you can&#39;t really say the the blood type &quot;O&quot; is greater or less than

46
00:03:59,260 --> 00:04:05,540
the blood type &quot;A&quot;. &quot;O&quot; is not worth more than &quot;A&quot; in any sense I can think of. Trying

47
00:04:05,540 --> 00:04:10,519
such a comparison with factors will generate a warning, telling you that less than is not

48
00:04:10,519 --> 00:04:11,549
meaningful:

49
00:04:11,549 --> 00:04:17,250
However, there are examples for which such a natural ordering does exist. Consider for

50
00:04:17,250 --> 00:04:22,910
example this tshirt vector. It has codes ranging from from small to large. Here, you could

51
00:04:22,910 --> 00:04:27,890
say that extra large indeed is greater than, say, a small, right?

52
00:04:27,890 --> 00:04:32,700
Of course, R provides a way to impose this kind of order on a factor, thus making it

53
00:04:32,700 --> 00:04:38,780
an ordered factor. Inside the factor() function, you simply set the argument ordered to TRUE,

54
00:04:38,780 --> 00:04:42,190
and specify the levels in ascending order.

55
00:04:42,190 --> 00:04:46,420
Can you so how these less then signs appear between the different factor levels? This

56
00:04:46,420 --> 00:04:51,100
compactly shows that we&#39;re dealing with an ordered factor now. If we now try to perform

57
00:04:51,100 --> 00:04:57,890
a comparison, this call for example, ..., evaluates to TRUE, without a warning message, because

58
00:04:57,890 --> 00:05:02,170
a medium was specified to be less than a large.

59
00:05:02,170 --> 00:05:07,550
Before you can start with the exercises, let&#39;s shortly wrap up on factors. First of all,

60
00:05:07,550 --> 00:05:13,200
factors are used to store categorical variables in R. Second, these factors are simply integer

61
00:05:13,200 --> 00:05:18,550
vectors that have factor levels associated. Third, you can change the factor levels using

62
00:05:18,550 --> 00:05:23,860
the levels function or by using the labels argument inside the factor function. And last

63
00:05:23,860 --> 00:05:28,920
but not least, R allows you to make the difference between ordered and unordered factors, thus

64
00:05:28,920 --> 00:05:31,750
catering to both nominal and ordinal variables.

