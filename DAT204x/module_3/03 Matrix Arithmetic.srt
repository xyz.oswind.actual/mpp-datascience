0
00:00:02,840 --> 00:00:02,840
Matrix arithmetic

1
00:00:02,840 --> 00:00:07,340
Throughout the exercises on matrices, you have already bumped into the `colSums()` and

2
00:00:07,340 --> 00:00:12,280
`rowSums()` functions. The `colSums()` function, for example, took the sum of each column,

3
00:00:12,280 --> 00:00:17,800
and stored the result in a vector. Apart from these matrix-specifc math functions, you can

4
00:00:17,800 --> 00:00:21,920
also do standard arithmetic with functions. Remember how you could do all the typical

5
00:00:21,920 --> 00:00:27,869
arithmetic operations on vectors? Again, it&#39;s exactly the same thing for matrices: all computations

6
00:00:27,869 --> 00:00:30,890
are done element-wise.

7
00:00:30,890 --> 00:00:35,820
Ever heard about that other classic trilogy, the Lord of the Rings? As for Star Wars, you

8
00:00:35,820 --> 00:00:41,570
can build a matrix of box office revenue for both US and non-US regions. The information

9
00:00:41,570 --> 00:00:48,730
is saved in the the matrix, lotr_matrix, which has been constructed as follows:

10
00:00:48,730 --> 00:00:53,940
These are astronomical numbers, we&#39;re talking about millions of US dollars here. What if

11
00:00:53,940 --> 00:00:59,270
you wanted to convert this to Euros? At the time of writing, 1 euro converts to 1 point

12
00:00:59,270 --> 00:01:04,219
12 US dollars, so to convert the figures to euros, we&#39;ll have to divide the figures by

13
00:01:04,218 --> 00:01:09,859
1.12. We can simply use the division operator as if we were performing the division on a

14
00:01:09,859 --> 00:01:12,459
single number:

15
00:01:12,459 --> 00:01:19,039
R performs the operation element-wise: each element is divided by 1.12. This works just

16
00:01:19,039 --> 00:01:25,259
the same for multiplication, summation and subtraction. Say, for example, that the theaters

17
00:01:25,259 --> 00:01:29,639
world-wide claim 50 million dollars on the box office revenue. How much is left for the

18
00:01:29,639 --> 00:01:37,029
Lord of the rings concern itself then? We simply subtract 50 from LOTR matrix:

19
00:01:37,029 --> 00:01:41,329
Operating on matrices with single numbers looks pretty straightforward, and this actually

20
00:01:41,329 --> 00:01:46,529
also holds when you&#39;re performing calculations with two matrices. Suppose that instead of

21
00:01:46,529 --> 00:01:51,849
demanding the same sum of money for every release, theaters worldwide ask for 50 million

22
00:01:51,849 --> 00:01:57,849
for the first release, 80 for the second and 100 for the third. We could build a matrix,

23
00:01:57,849 --> 00:02:03,020
say, `theater_cut`, with the same dimensions as LOTR matrix, that looks as follows

24
00:02:03,020 --> 00:02:08,929
If we now subtract theater_cut from LOTR matrix:

25
00:02:08,929 --> 00:02:13,989
This time, the substraction was also performed element wise: the figures for the Two towers

26
00:02:13,989 --> 00:02:19,819
lowered by 80, while the figures for the return of the king were lowered by 100. Makes perfect

27
00:02:19,819 --> 00:02:21,930
sense right?

28
00:02:21,930 --> 00:02:28,500
Now, what would happen if we use a vector, containing 50, 80 and 100 to subtract from

29
00:02:28,500 --> 00:02:31,510
the lord of the rings matrix? Let&#39;s try it out.

30
00:02:31,510 --> 00:02:38,540
The result is exactly the same! That&#39;s because once again, R performed recycling. R realizes

31
00:02:38,540 --> 00:02:43,170
that the dimensions of the matrix and the vector don&#39;t match. Therefore, the vector

32
00:02:43,170 --> 00:02:47,689
is extended to a matrix of the same size, and is filled up with the vector elements

33
00:02:47,689 --> 00:02:53,170
column by column. The matrix that is thus actually subtracted from the lotr matrix is

34
00:02:53,170 --> 00:02:55,840
the following:

35
00:02:55,840 --> 00:03:00,859
This matrix is the exact same one as we built manually before, so the result is the same.

36
00:03:00,859 --> 00:03:05,969
Again, blindly trusting R that it performs recycling just the way you want it can be

37
00:03:05,969 --> 00:03:10,549
quite a dangerous practice. You should be fully aware of how this recycling is actually

38
00:03:10,549 --> 00:03:11,329
happening.

39
00:03:11,329 --> 00:03:17,040
If you are familiar with linear algebra, you might wonder how matrix multiplication would

40
00:03:17,040 --> 00:03:23,340
work. Well, in R, multiplication is simply performed element wise. Suppose you want to

41
00:03:23,340 --> 00:03:28,189
convert the US dollar figures to euros with the exchange rate at the time of the release,

42
00:03:28,189 --> 00:03:33,329
you can again create a new matrix, this time with the amount of euros you should pay for

43
00:03:33,329 --> 00:03:34,489
1 dollar.

44
00:03:34,489 --> 00:03:40,389
Now, we can simply multiply the lord of the rings matrix with the rates matrix:

45
00:03:40,389 --> 00:03:46,299
Again, every calculation is done element-by-element. The top left figure has been multiplied by

46
00:03:46,299 --> 00:03:50,579
one point 11, while the bottom right figure was multiplied by point 82.

47
00:03:50,579 --> 00:03:57,420
To end this last video on matrices, I want to stress once more that matrices and vectors

48
00:03:57,420 --> 00:04:03,519
are very similar: they simply are data structures that can store elements of the same type.

49
00:04:03,519 --> 00:04:08,109
The vector does this in a one-dimensional sequence, while the matrix uses a two-dimensional

50
00:04:08,109 --> 00:04:12,980
grid structure. Both of them perform coercion when you want to store elements of different

51
00:04:12,980 --> 00:04:19,870
types and both of them perform recycling when necessary. Similarly, vector and matrix arithmetic

52
00:04:19,870 --> 00:04:25,270
are straightforward: all calculations are performed element-wise. That&#39;s all folks.

53
00:04:25,270 --> 00:04:30,330
Time to wrap up on matrices in the following exercises and I&#39;ll be awaiting you to explain

54
00:04:30,330 --> 00:04:31,470
all about factors!

