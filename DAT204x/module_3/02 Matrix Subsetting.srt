0
00:00:02,389 --> 00:00:06,430
Just as for vectors, there are situations in which you want to select single elements

1
00:00:06,430 --> 00:00:12,020
or entire parts of a matrix to continue your analysis with. Again, you can use square brackets

2
00:00:12,020 --> 00:00:16,820
for this, but the fact that you&#39;re dealing with two dimensions now, complicates things

3
00:00:16,820 --> 00:00:18,090
a bit.

4
00:00:18,090 --> 00:00:22,570
Have a look at this matrix containing some random numbers.

5
00:00:22,570 --> 00:00:26,859
If you want to select a single element from this matrix, you&#39;ll have to specify both the

6
00:00:26,859 --> 00:00:33,120
row and the column of the element of interest. Suppose we want to select the number 15, located

7
00:00:33,120 --> 00:00:39,500
at the first row and the third column. We type m, open brackets, 1, comma, 3, comma,

8
00:00:39,500 --> 00:00:41,920
close brackets.

9
00:00:41,920 --> 00:00:46,300
As you can probably tell, the first index refers to the row, the second one refers to

10
00:00:46,300 --> 00:00:52,870
the column. Likewise, to select the number 1, at row 3 and column 2, we write the following

11
00:00:52,870 --> 00:00:54,160
line:

12
00:00:54,160 --> 00:01:00,280
Works like a charm! Notice that the results are single values, so vectors of length 1.

13
00:01:00,280 --> 00:01:06,380
Now, what if you want to select an entire row or column from this matrix? You can do

14
00:01:06,380 --> 00:01:12,000
this by letting out some of the indices between square brackets. Instead of writing 3, comma,

15
00:01:12,000 --> 00:01:17,380
2 inside square brackets to select the element at row 3 and column 2, you can leave out the

16
00:01:17,380 --> 00:01:23,610
2 and keep the 3, comma part. Now, you select all elements that are in row 3, namely 6,

17
00:01:23,610 --> 00:01:26,729
1, 4 and 2.

18
00:01:26,729 --> 00:01:31,490
Notice here that the result is not a matrix anymore! It&#39;s also a vector, but this time

19
00:01:31,490 --> 00:01:37,049
one that contains more than 1 element. You selected a single row from the matrix so a

20
00:01:37,049 --> 00:01:44,060
vector suffises to store this one-dimensional information. To select columns, you can work

21
00:01:44,060 --> 00:01:48,899
similarly, but this time the index that comes before the comma should be removed. To select

22
00:01:48,899 --> 00:01:55,170
the entire 3rd column, you should write m, open brackets, comma, 3, close brackets.

23
00:01:55,170 --> 00:02:01,299
Again, a vector results, this time of length 3, corresponding to the third column of `m`.

24
00:02:01,299 --> 00:02:06,780
Now, what happens when you decide not to include a comma to clearly discern between column

25
00:02:06,780 --> 00:02:12,870
and row indices? Let&#39;s simply try it out and see if we can explain it. Suppose you simply

26
00:02:12,870 --> 00:02:15,730
type m and then 4 inside brackets.

27
00:02:15,730 --> 00:02:23,370
The result is 11. How did R get to that? Well, when you pass a single index to subset a matrix,

28
00:02:23,370 --> 00:02:28,590
R simply goes through the matrix column by column from left to right. The first index

29
00:02:28,590 --> 00:02:35,690
is then 5, the second one 12, the third one 6 and the fourth one is 11, in the next column.

30
00:02:35,690 --> 00:02:41,690
This means that if we pass m[9], we should get 4, in the third row and third column.

31
00:02:41,690 --> 00:02:47,430
Correct! There aren&#39;t a lot of cases in which using a single index without commas in a matrix

32
00:02:47,430 --> 00:02:54,640
is useful, but I just wanted to point out that the comma is really crucial here.

33
00:02:54,640 --> 00:02:59,090
In vector subsetting, you also learned how to select multiple elements. In matrices,

34
00:02:59,090 --> 00:03:05,519
this is of course also possible and the principles are just the same. Say, for example, you want

35
00:03:05,519 --> 00:03:10,450
to select the values 14 and 8, in the middle of the matrix. This command will do that for

36
00:03:10,450 --> 00:03:10,450
you:

37
00:03:10,450 --> 00:03:16,830
You select elements that are on the second row and on the second and third column. Again,

38
00:03:16,830 --> 00:03:23,989
the result is a vector, because 1 dimension suffises. But you can&#39;t select elements that

39
00:03:23,989 --> 00:03:29,879
don&#39;t have one of row or column index in common. If you want to select the 11, on row 1 and

40
00:03:29,879 --> 00:03:34,049
column 2, and 8, on row 2 and column 3, this call

41
00:03:34,049 --> 00:03:38,870
will not give the wanted result. Instead, a submatrix gets returned, that spans the

42
00:03:38,870 --> 00:03:45,510
elements on row 1 and 2 and column 2 and 3. These submatrices can also be built up from

43
00:03:45,510 --> 00:03:50,810
disjoint places in your matrix. Creating a submatrix that contains elements on row 1

44
00:03:50,810 --> 00:03:54,659
and 3 and on columns 1 , 3 and 4, for example, would look like this

45
00:03:54,659 --> 00:04:03,010
Now, remember these other ways of performing subsetting, by using names and with logical

46
00:04:03,010 --> 00:04:08,870
vectors? These work just as well for matrices. Let&#39;s have a look at subsetting by names first.

47
00:04:08,870 --> 00:04:12,379
First, though, we&#39;ll have to name the matrix:

48
00:04:12,379 --> 00:04:17,160
In fact subsetting by name works exactly the same as by index, but you just replace the

49
00:04:17,160 --> 00:04:23,470
indices with the corresponding names. To select 8, you could use the row index 2 and column

50
00:04:23,470 --> 00:04:28,540
3, or use the row name r2 and column name column c:

51
00:04:28,540 --> 00:04:31,440
You can even use a combination of both:

52
00:04:31,440 --> 00:04:37,030
Just remember to surround the row and column names with quotes Selecting multiple elements

53
00:04:37,030 --> 00:04:42,860
and submatrices from a matrix is straightforward as well. To select elements on row r3 and

54
00:04:42,860 --> 00:04:45,390
in the last two columns, you can use:

55
00:04:45,390 --> 00:04:52,420
Finally, you can also use logical vectors. Again, the same rules apply: rows and columns

56
00:04:52,420 --> 00:04:58,440
corresponding to a TRUE are kept, while those corresponding to FALSE are left out. To select

57
00:04:58,440 --> 00:05:02,130
the same elements as in the previous call, you can use:

58
00:05:02,130 --> 00:05:08,510
The rules of vector recycling also apply here. Suppose that you only pass a vector of length

59
00:05:08,510 --> 00:05:12,310
2 to perform a selection on the columns:

60
00:05:12,310 --> 00:05:17,500
The column selection vector gets recycled to FALSE, TRUE, FALSE, TRUE:

61
00:05:17,500 --> 00:05:22,110
Giving the same result. As you can see, there&#39;s not nothing new under

62
00:05:22,110 --> 00:05:26,910
the matrix sun: apart from the additional dimension when you compare to vectors, all

63
00:05:26,910 --> 00:05:30,970
different techinques to perform subsetting and all of the technicalities remain the same.

