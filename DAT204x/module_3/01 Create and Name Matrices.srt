0
00:00:02,570 --> 00:00:08,260
Hello again! In the next few videos, I&#39;ll be talking about matrices. As before, I&#39;ll

1
00:00:08,260 --> 00:00:12,150
take you through all the basics you need to know about this new data structure.

2
00:00:12,150 --> 00:00:17,980
So, what is a matrix. Well, a matrix is kind of like the big brother of the vector. Where

3
00:00:17,980 --> 00:00:22,759
a vector is a _sequence_ of data elements, which is one-dimensional, a matrix is a similar

4
00:00:22,759 --> 00:00:29,009
collection of data elements, but this time arranged into a fixed number of rows and columns.

5
00:00:29,009 --> 00:00:34,510
Since you are only working with rows and columns, a matrix is called two-dimensional. As with

6
00:00:34,510 --> 00:00:40,090
the vector, the matrix can contain only one atomic vector type. This means that you can&#39;t

7
00:00:40,090 --> 00:00:44,980
have logicals and numerics in a matrix for example. There&#39;s really not much more theory

8
00:00:44,980 --> 00:00:50,620
about matrices than this: it&#39;s really a natural extension of the vector, going from one to

9
00:00:50,620 --> 00:00:56,609
two dimensions. Of course, this has its implications for manipulating and subsetting matrices,

10
00:00:56,609 --> 00:01:00,359
but let&#39;s start with simply creating and naming them.

11
00:01:00,359 --> 00:01:05,860
To build a matrix, you use the matrix function. Most importantly, it needs a vector, containing

12
00:01:05,860 --> 00:01:11,130
the values you want to place in the matrix, and at least one matrix dimension. You can

13
00:01:11,130 --> 00:01:16,650
choose to specify the number of rows or the number of columns. Have a look at the following

14
00:01:16,650 --> 00:01:22,820
example, that creates a 2-by-3 matrix containing the values 1 to 6, by specifying the vector

15
00:01:22,820 --> 00:01:27,060
and setting the nrow argument to 2:

16
00:01:27,060 --> 00:01:31,920
R sees that the input vector has length 6 and that there have to be two rows. It then

17
00:01:31,920 --> 00:01:36,689
infers that you&#39;ll probably want 3 columns, such that the number of matrix elements matches

18
00:01:36,689 --> 00:01:42,649
the number of input vector elements. You could just as well specify ncol instead of nrow;

19
00:01:42,649 --> 00:01:48,619
in this case, R infers the number of _rows_ automatically.

20
00:01:48,619 --> 00:01:53,899
In both these examples, R takes the vector containing the values 1 to 6, and fills it

21
00:01:53,899 --> 00:01:59,039
up, column by column. If you prefer to fill up the matrix in a row-wise fashion, such

22
00:01:59,039 --> 00:02:04,090
that the 1, 2 and 3 are in the first row, you can set the `byrow` argument of matrix

23
00:02:04,090 --> 00:02:05,939
to `TRUE`

24
00:02:05,939 --> 00:02:11,020
Can you spot the difference? Remember how R did recycling when you were

25
00:02:11,020 --> 00:02:16,060
subsetting vectors using logical vectors? The same thing happens when you pass the matrix

26
00:02:16,060 --> 00:02:22,220
function a vector that is too short to fill up the entire matrix. Suppose you pass a vector

27
00:02:22,220 --> 00:02:27,580
containing the values 1 to 3 to the matrix function, and explicitly say you want a matrix

28
00:02:27,580 --> 00:02:31,000
with 2 rows and 3 columns:

29
00:02:31,000 --> 00:02:36,250
R fills up the matrix column by column and simply repeats the vector. If you try to fill

30
00:02:36,250 --> 00:02:40,800
up the matrix with a vector whose multiple does not nicely fit in the matrix, for example

31
00:02:40,800 --> 00:02:45,970
when you want to put a 4-element vector in a 6-element matrix, R generates a warning

32
00:02:45,970 --> 00:02:48,000
message.

33
00:02:48,000 --> 00:02:54,490
Actually, apart from the `matrix()` function, there&#39;s yet another easy way to create matrices

34
00:02:54,490 --> 00:02:59,720
that is more intuitive in some cases. You can paste vectors together using the `cbind()`

35
00:02:59,720 --> 00:03:02,800
and `rbind()` functions. Have a look at these calls

36
00:03:02,800 --> 00:03:09,700
`cbind()`, short for column bind, takes the vectors you pass it, and sticks them together

37
00:03:09,700 --> 00:03:15,470
as if they were columns of a matrix. The `rbind()` function, short for row bind, does the same

38
00:03:15,470 --> 00:03:21,900
thing but takes the input as rows and makes a matrix out of them. These functions can

39
00:03:21,900 --> 00:03:27,510
come in pretty handy, because they&#39;re often more easy to use than the `matrix()` function.

40
00:03:27,510 --> 00:03:32,000
The `bind` functions I just introduced can also handle matrices actually, so you can

41
00:03:32,000 --> 00:03:37,760
easily use them to paste another row or another column to an already existing matrix. Suppose

42
00:03:37,760 --> 00:03:42,510
you have a matrix `m`, containing the elements 1 to 6:

43
00:03:42,510 --> 00:03:48,090
If you want to add another row to it, containing the values 7, 8, 9, you could simply run this

44
00:03:48,090 --> 00:03:49,490
command:

45
00:03:49,490 --> 00:03:56,640
You can do a similar thing with `cbind()`:

46
00:03:56,640 --> 00:04:01,310
Next up is naming the matrix. In the case of vectors, you simply used the names() function,

47
00:04:01,310 --> 00:04:06,750
but in the case of matrices, you could assign names to both columns and rows. That&#39;s why

48
00:04:06,750 --> 00:04:12,620
R came up with the rownames() and colnames() functions. Their use is pretty straightforward.

49
00:04:12,620 --> 00:04:14,510
Retaking the matrix `m` from before,

50
00:04:14,510 --> 00:04:19,070
we can set the row names just the same way as we named vectors, but this time with the

51
00:04:19,070 --> 00:04:21,630
rownames function.

52
00:04:21,630 --> 00:04:24,630
Printing m shows that it worked:

53
00:04:24,630 --> 00:04:30,850
Setting the column names with a vector of length 3 gives us a fully named matrix

54
00:04:30,850 --> 00:04:35,540
Just as with vectors, there are also one-liner ways of naming matrices while you&#39;re building

55
00:04:35,540 --> 00:04:42,180
it. You use the dimnames argument of the matrix function for this. Check this out.

56
00:04:42,180 --> 00:04:46,590
You need to specify a list which has a vector of rownames as the first element and a vector

57
00:04:46,590 --> 00:04:50,850
of column names as the second element. Don&#39;t panic if you&#39;ve never seen this list() function

58
00:04:50,850 --> 00:04:55,190
before, you&#39;ll learn all about that later on!

59
00:04:55,190 --> 00:05:00,500
As I explained in the beginning of this video, matrices are just an extension of vectors.

60
00:05:00,500 --> 00:05:05,180
This means that they can also contain only a single atomic vector type. If you try to

61
00:05:05,180 --> 00:05:10,310
store different types in a matrix, coercion automatically takes place. Have a look at

62
00:05:10,310 --> 00:05:16,310
these two matrices, one containing numerics, the other one containing characters.

63
00:05:16,310 --> 00:05:22,790
Let&#39;s now try to bind these two matrices together in a column-wise fashing using `cbind()`.

64
00:05:22,790 --> 00:05:27,810
Did you see what happened? The numeric matrix elements were coerced to characters to end

65
00:05:27,810 --> 00:05:33,190
up with a matrix that is only comprised of characters. To have a multi-dimensional data

66
00:05:33,190 --> 00:05:39,060
structure that can contain different elements, you&#39;ll have to use to lists or more specifically:

67
00:05:39,060 --> 00:05:40,680
data.frames.

68
00:05:40,680 --> 00:05:44,800
Up to you now to become a jedi in the world of matrices, enjoy it you shall!

