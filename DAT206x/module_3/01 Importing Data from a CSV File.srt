0
00:00:00,050 --> 00:00:04,420
[MUSIC]

1
00:00:04,421 --> 00:00:06,080
Hello, welcome back.

2
00:00:06,081 --> 00:00:10,250
This is module three from the course about analyzing and

3
00:00:10,251 --> 00:00:11,430
visualizing data in Excel.

4
00:00:12,510 --> 00:00:18,010
In this module I will introduce you to queries in Excel

5
00:00:18,011 --> 00:00:22,030
that are done using a technology that was previous to 2016,

6
00:00:22,031 --> 00:00:26,880
which I’m using, known as Power Query.

7
00:00:26,881 --> 00:00:31,560
If you’re using Excel 2013, or even Excel 2010, you’ll be able to

8
00:00:31,561 --> 00:00:36,170
follow it and see the functionality that I am going to use here.

9
00:00:36,171 --> 00:00:40,980
You’ll see it as functionality of the Power Query add-in in Excel.

10
00:00:40,981 --> 00:00:46,090
In 2016 what we did is include all this functionality in Excel,

11
00:00:47,230 --> 00:00:49,670
and now there is no add-in anymore.

12
00:00:49,671 --> 00:00:52,560
There’s no Power Query add-in.

13
00:00:52,561 --> 00:00:55,600
So you’ll see that the same functionality is now folded

14
00:00:55,601 --> 00:00:57,970
into the regular ribbons of Excel.

15
00:00:57,971 --> 00:01:00,410
And it’s there side by side with

16
00:01:00,411 --> 00:01:02,880
older functionality to import data in Excel.

17
00:01:04,870 --> 00:01:08,640
Further down in the next versions of Excel we

18
00:01:08,641 --> 00:01:13,240
plan to make this technology that came from Power Query, it is now

19
00:01:13,241 --> 00:01:17,610
not called Power Query any more, the default technology to import data.

20
00:01:17,611 --> 00:01:19,440
But for now it’s there side by side and

21
00:01:19,441 --> 00:01:23,440
I’m going to use it exclusively to bring data into Excel.

22
00:01:23,441 --> 00:01:27,400
My first example in bringing data from using the query

23
00:01:27,401 --> 00:01:28,350
will be very simple.

24
00:01:28,351 --> 00:01:33,060
I will just be importing a single flat CSV file, and

25
00:01:33,061 --> 00:01:34,980
make it into my model.

26
00:01:34,981 --> 00:01:36,518
This table has everything I need,

27
00:01:36,519 --> 00:01:41,080
not necessarily ready in the shape and the form that I need it but

28
00:01:41,081 --> 00:01:45,040
I will form it and shape it using the query.

29
00:01:45,041 --> 00:01:45,670
So let’s start.

30
00:01:46,700 --> 00:01:47,820
We switch to Excel.

31
00:01:47,821 --> 00:01:53,000
In the data ribbon, there at top in the ribbon, you see there’s

32
00:01:53,001 --> 00:01:58,065
an area here what we call a chunk and it’s title is Get and Transform.

33
00:01:58,066 --> 00:02:02,175
This is where you are looking for what was previously, or

34
00:02:02,176 --> 00:02:08,075
is still in 2013, known as the Power Query functionality.

35
00:02:08,076 --> 00:02:11,927
And most of it is under here, the new query.

36
00:02:11,928 --> 00:02:14,812
Here, you start your queries.

37
00:02:14,813 --> 00:02:19,897
Power Query is a technology that is designed to bring the data

38
00:02:19,898 --> 00:02:21,517
into Excel.

39
00:02:21,518 --> 00:02:25,600
It can bring data into the Excel grid as tables, and

40
00:02:25,601 --> 00:02:30,010
you could then use them for any use that you want in Excel.

41
00:02:30,011 --> 00:02:33,460
Or it can bring data directly into the Excel data model that was

42
00:02:33,461 --> 00:02:39,310
introduced in the previous part of this course.

43
00:02:39,311 --> 00:02:42,630
And it can actually bring data to both, which I would not recommend

44
00:02:42,631 --> 00:02:46,320
you to do, but if you want to it’s possible.

45
00:02:46,321 --> 00:02:51,420
Now, first thing I need to do is to pick the source that I want to use.

46
00:02:51,421 --> 00:02:52,540
What kind of source is it?

47
00:02:52,541 --> 00:02:53,500
It’s a file.

48
00:02:53,501 --> 00:02:54,360
What file is it?

49
00:02:54,361 --> 00:02:55,780
It’s a CSV file.

50
00:02:55,781 --> 00:03:01,020
So I would say I want to query, to create a new query from CSV file.

51
00:03:01,021 --> 00:03:04,300
Now, I’m going to browse for the file.

52
00:03:04,301 --> 00:03:09,979
The file is here and it’s called onetable.csv and

53
00:03:09,980 --> 00:03:12,290
I just double click.

54
00:03:12,291 --> 00:03:17,370
And now what I will see next is the query editor, so

55
00:03:17,371 --> 00:03:21,868
this is an environment in which I am going to

56
00:03:21,869 --> 00:03:26,230
enhance the query, create the query and

57
00:03:26,231 --> 00:03:30,200
then I can save the query, and every time I need I can run it again

58
00:03:30,201 --> 00:03:33,922
to refresh the data to bring the data from the same source again.

59
00:03:33,923 --> 00:03:36,260
So here I see the data.

60
00:03:36,261 --> 00:03:38,570
Looks pretty good to me.

61
00:03:40,480 --> 00:03:43,300
But there’s still a few things which I need to do

62
00:03:43,301 --> 00:03:45,700
in order to prepare this data.

63
00:03:48,120 --> 00:03:51,400
So first of all there are some columns which I don’t need.

64
00:03:51,401 --> 00:03:54,200
So let’s say I don’t need the district, so

65
00:03:54,201 --> 00:03:57,961
I can just go right-click and remove it.

66
00:03:59,420 --> 00:04:02,200
This one here, maybe also I don’t want it.

67
00:04:02,201 --> 00:04:03,990
I can also go and remove it.

68
00:04:03,991 --> 00:04:08,600
So I can decide which columns do I want to

69
00:04:08,601 --> 00:04:14,290
leave as they are or otherwise others that I can remove.

70
00:04:14,291 --> 00:04:18,650
Let’s watch now the column named city.

71
00:04:19,880 --> 00:04:24,870
You see that it is formatted as a city colon comma 

72
00:04:24,871 --> 00:04:28,350
State comma the country which is USA for everyone.

73
00:04:28,351 --> 00:04:34,310
I also have a separate columns for the state and for the country.

74
00:04:34,311 --> 00:04:37,760
So, actually, I don’t need all this superfluous data.

75
00:04:37,761 --> 00:04:41,320
I don’t need it both in the city and in the other column, so

76
00:04:41,321 --> 00:04:45,240
I would like to actually, leave here just the city.

77
00:04:45,241 --> 00:04:48,480
So, what I can do, I can select this column and then I use,

78
00:04:48,481 --> 00:04:53,160
from the ribbon this tool, from the transformations available for

79
00:04:53,161 --> 00:04:55,750
me, I will use split columns.

80
00:04:55,751 --> 00:04:58,870
So I say yes, I want to split it be using the delimiter,

81
00:04:58,871 --> 00:05:00,410
the delimiter is a comma.

82
00:05:00,411 --> 00:05:05,300
I want just to use it once, so I say the left most delimiter, click OK.

83
00:05:07,910 --> 00:05:10,910
And the result is that actually now I have two columns.

84
00:05:10,911 --> 00:05:14,386
Column 1 called City.1 and Column 2 called City.2.

85
00:05:14,387 --> 00:05:17,834
City.2 is the one I don’t need, so I will remove it.

86
00:05:17,835 --> 00:05:22,120
City.1, I can double-click the header and

87
00:05:22,121 --> 00:05:24,790
just rename it to be just City.

88
00:05:24,791 --> 00:05:30,160
So I’m doing a series of simple steps to make this data ready

89
00:05:30,161 --> 00:05:34,910
for its purpose to be used later as the basis for my pivot tables, so

90
00:05:34,911 --> 00:05:36,970
far nothing really fancy.

91
00:05:38,045 --> 00:05:41,355
Now another thing is that the state, I actually only have data from two

92
00:05:41,356 --> 00:05:46,795
states here, New York and Texas, but the state comes as abbreviations.

93
00:05:46,796 --> 00:05:51,635
And I want this column to have the full name of the state.

94
00:05:51,636 --> 00:05:56,005
So I can do right-click and say Replace Values.

95
00:05:57,050 --> 00:05:58,940
And I can do something do very simple, I say,

96
00:05:58,941 --> 00:06:02,800
when it’s TX, I want it to show Texas.

97
00:06:04,270 --> 00:06:06,160
And here it goes.

98
00:06:06,161 --> 00:06:11,482
And again, I will say Replace Values

99
00:06:11,483 --> 00:06:17,508
when it says NY, I want it to say New York.

100
00:06:17,509 --> 00:06:19,880
My mistake there, all right.

101
00:06:23,740 --> 00:06:29,090
Now this looks like very manual work, I’m just going to replace.

102
00:06:29,091 --> 00:06:34,710
But the good thing is if you look at the right side here, you see here

103
00:06:34,711 --> 00:06:40,170
the query settings here, one is that name of the query.

104
00:06:40,171 --> 00:06:44,320
And I can change this to be, let’s say, I don’t know, SalesData.

105
00:06:44,321 --> 00:06:48,260
Because this name here will become eventually the name of the table

106
00:06:48,261 --> 00:06:50,650
in the model once I import it.

107
00:06:50,651 --> 00:06:53,960
So I want to give it a nicer name than just OneTable.

108
00:06:53,961 --> 00:06:57,080
And then below that I have a list of applied steps.

109
00:06:57,081 --> 00:06:59,730
This is actually each one of them

110
00:06:59,731 --> 00:07:04,310
is one of the steps that I did while I working on this data.

111
00:07:04,311 --> 00:07:07,740
So I removed columns and each time I click on one of them,

112
00:07:07,741 --> 00:07:12,600
I actually see the way the data looks after the step.

113
00:07:12,601 --> 00:07:16,320
So I can go and retrace my steps and see exactly where I was and

114
00:07:16,321 --> 00:07:18,070
how did it look.

115
00:07:18,071 --> 00:07:22,260
This series of steps is actually creating some kind of a script

116
00:07:22,261 --> 00:07:25,460
that will be saved as part of the query.

117
00:07:25,461 --> 00:07:29,450
Next time, I will run the query by clicking refresh on the table that I

118
00:07:29,451 --> 00:07:30,780
will create.

119
00:07:30,781 --> 00:07:34,650
This series of steps will be repeated, starting from the source

120
00:07:34,651 --> 00:07:39,180
from exactly the location of the source when I picked it up, and

121
00:07:39,181 --> 00:07:41,390
going forward with all the steps.

122
00:07:41,391 --> 00:07:45,250
And this is why I’m not really worried about manual steps and

123
00:07:45,251 --> 00:07:47,520
repeating them because I do it once and

124
00:07:47,521 --> 00:07:52,760
then they will be repeated again and again for me.

125
00:07:52,761 --> 00:07:56,185
So here we saw a list of very simple steps,

126
00:07:56,186 --> 00:08:02,405
later on we’re going to see there’s a few more parts about using queries.

127
00:08:02,406 --> 00:08:08,735
And we’re going to see more sophisticated steps they can apply.

128
00:08:08,736 --> 00:08:11,545
Now I’m done with preparing the data.

129
00:08:11,546 --> 00:08:14,735
It looks ready to me, so it can go and from

130
00:08:16,620 --> 00:08:21,820
here I have Close and Load or Close and Load To.

131
00:08:21,821 --> 00:08:26,670
Close and Load To enables me to choose the target for

132
00:08:26,671 --> 00:08:31,240
this data when it’s going to be and there’s two parts to it.

133
00:08:31,241 --> 00:08:34,049
One is, do I want to see it in the workbook itself?

134
00:08:35,080 --> 00:08:38,810
So do I want it to create a table for me or I only want it as

135
00:08:38,811 --> 00:08:43,020
a connection that then can be used for refresh and so on?

136
00:08:43,020 --> 00:08:45,662
So in this case I leave it as the default for me here,

137
00:08:45,663 --> 00:08:47,020
Only Create Connection.

138
00:08:48,050 --> 00:08:52,860
The second part is about this checkbox,

139
00:08:52,861 --> 00:08:54,260
do I want it in the data model?

140
00:08:54,261 --> 00:08:58,210
The fact the default comes this way is because I configured before.

141
00:08:58,211 --> 00:09:01,320
I can configure it this will be different for me.

142
00:09:01,321 --> 00:09:03,630
Because this is the way usually I do it.

143
00:09:03,631 --> 00:09:06,120
But anyhow, you see here the two parts and

144
00:09:06,121 --> 00:09:09,880
now I don’t need to change anything, I can click Load.

145
00:09:09,881 --> 00:09:15,290
And on the right side you see the progress of this query and

146
00:09:15,291 --> 00:09:20,110
it says that they have 33,000 and something loaded.

147
00:09:20,111 --> 00:09:23,630
When I hover over it I see a preview of the data and

148
00:09:23,631 --> 00:09:27,590
we don’t see any of the data because it’s actually in the data model,

149
00:09:27,591 --> 00:09:30,220
this is where I asked it to go to the data model.

150
00:09:30,221 --> 00:09:35,580
But I can go in here, insert a pivot table, again the default for

151
00:09:35,581 --> 00:09:38,800
2016 is use this workbook from the data model.

152
00:09:38,801 --> 00:09:41,860
If you are on previous version, I would recommend it’s much easier to

153
00:09:41,861 --> 00:09:45,680
go to the Powerpivot add-in and create the pivot table from there.

154
00:09:45,681 --> 00:09:46,790
You can do it from Excel but

155
00:09:46,791 --> 00:09:51,100
it will take you about eight clicks instead of one.

156
00:09:51,101 --> 00:09:54,319
And now you see on the right side that actually the field list for

157
00:09:54,320 --> 00:09:55,550
the pivot.

158
00:09:55,551 --> 00:09:57,015
It has only one table with everything

159
00:09:57,016 --> 00:09:58,066
in it.

160
00:09:58,067 --> 00:10:05,210
And you see here that I have the revenue which I can click on.

161
00:10:05,211 --> 00:10:06,550
I have the countries.

162
00:10:06,551 --> 00:10:10,920
Country is not very useful because it’s all from one country so,

163
00:10:10,921 --> 00:10:11,640
I take it out.

164
00:10:11,641 --> 00:10:13,860
State, within State, the City.

165
00:10:13,861 --> 00:10:16,180
So I’m having pivot off all the cities.

166
00:10:16,181 --> 00:10:20,450
And I can actually go and create a pivot based on this data.

167
00:10:20,451 --> 00:10:25,740
So that will conclude this part,

168
00:10:25,741 --> 00:10:29,410
in which we saw the first example of using a query.

169
00:10:29,411 --> 00:10:37,850
Previously known as Power Query in Excel 2016 to bring flat CSV file.

170
00:10:37,851 --> 00:10:42,270
Applying some steps to it and bringing into the model and

171
00:10:42,271 --> 00:10:45,270
then creating a pivot table from it.

172
00:10:45,271 --> 00:10:49,610
In the next modules we’ll see building of more

173
00:10:49,611 --> 00:10:52,730
complex data models also using queries.

174
00:10:52,731 --> 00:10:58,610
And then eventually we’ll also go dive into DAX and

175
00:10:58,611 --> 00:11:03,480
create more sophisticated logic within these data models.

176
00:11:03,481 --> 00:11:05,670
So bye for now and see you in the next one.

