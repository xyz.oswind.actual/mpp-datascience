0
00:00:00,001 --> 00:00:04,430
[MUSIC]

1
00:00:04,431 --> 00:00:05,700
Hello.

2
00:00:05,701 --> 00:00:06,520
Welcome back.

3
00:00:06,521 --> 00:00:12,460
This is module two in the course for Excel, analyze and visualize data.

4
00:00:12,461 --> 00:00:17,710
And in this module, I will introduce you to something really important

5
00:00:17,711 --> 00:00:19,480
which is the Excel data model.

6
00:00:19,481 --> 00:00:23,770
The Excel data model was available in

7
00:00:23,771 --> 00:00:27,810
Excel 2010 and was introduced together with an add-in to Excel called

8
00:00:27,811 --> 00:00:30,880
Power Pivot that you probably have heard about.

9
00:00:30,881 --> 00:00:34,785
It has the data model in it and the next version of Excel 2013,

10
00:00:34,786 --> 00:00:38,235
the model was actually absorbed into Excel and

11
00:00:38,236 --> 00:00:40,445
now it’s part of Excel itself.

12
00:00:40,446 --> 00:00:45,375
Although the add-in has still its role and we are going to see it.

13
00:00:45,376 --> 00:00:47,915
Going forward the Excel model will take a more and

14
00:00:47,916 --> 00:00:53,900
more important part in everything to do with data in Excel.

15
00:00:53,901 --> 00:00:57,660
So we’ll look into a model that I prepared.

16
00:00:57,661 --> 00:01:02,280
In this module, we’re not going to see exactly how did I build

17
00:01:02,281 --> 00:01:06,770
the model, but this will come in subsequent modules.

18
00:01:06,771 --> 00:01:10,610
So, let’s dive into it and see this example.

19
00:01:10,611 --> 00:01:11,840
When you switch to Excel,

20
00:01:11,841 --> 00:01:13,760
what you see is actually an Excel that looks empty.

21
00:01:13,761 --> 00:01:15,430
There is nothing in the grid,

22
00:01:15,431 --> 00:01:18,720
there’s only one sheet, and everything is empty.

23
00:01:18,721 --> 00:01:22,061
But, it’s not really empty, because the data is already here.

24
00:01:22,062 --> 00:01:25,723
And, to see the data, I can either, from the data ribbon,

25
00:01:25,724 --> 00:01:29,094
click on manage data model, this is new for 2016,

26
00:01:29,095 --> 00:01:33,051
you won’t see it in previous versions, or, from the ribbon for

27
00:01:33,052 --> 00:01:37,195
the power pivot you can also switch to the manage window.

28
00:01:37,196 --> 00:01:42,269
And this is an independent window 

29
00:01:42,270 --> 00:01:46,870
that is used to see what’s in the data model.

30
00:01:46,871 --> 00:01:49,690
The data model, as you can see, is made of a series of tables.

31
00:01:49,691 --> 00:01:51,490
Each one of them is a table.

32
00:01:51,491 --> 00:01:55,738
It looks a little bit as if it’s Excel in the grid but it’s not.

33
00:01:55,739 --> 00:02:01,270
There’s no formulas just in the rows.

34
00:02:01,271 --> 00:02:07,110
Every row has the same data type, has a name and there’s no

35
00:02:07,111 --> 00:02:11,740
exceptions, it’s a little bit like tables in Excel but more strict.

36
00:02:11,741 --> 00:02:16,720
In this  37 00:02:16,721 --&gt; 00:02:21,240 case the largest table only has about 60,000 rows but

37
00:02:21,241 --> 00:02:25,730
we’ll see in later models with like two million rows.

38
00:02:26,730 --> 00:02:29,130
We can watch this way as tables.

39
00:02:29,131 --> 00:02:32,080
I can switch to another view by clicking here.

40
00:02:32,081 --> 00:02:34,422
In the diagram view in the ribbon and

41
00:02:34,423 --> 00:02:38,260
I will just make it a little bit smaller so all the tables will fit

42
00:02:38,261 --> 00:02:43,420
and here you’ll see like a diagram of all the tables and

43
00:02:43,421 --> 00:02:47,040
this line is connecting them which represent relations.

44
00:02:47,041 --> 00:02:48,640
Which are very important.

45
00:02:48,641 --> 00:02:50,680
The relationships between tables.

46
00:02:50,681 --> 00:02:54,050
This may look a little bit familiar to you if you’ve ever used Access.

47
00:02:55,200 --> 00:02:58,460
Now, so you see the tables and

48
00:02:58,461 --> 00:03:01,280
we see the lines which as I said represents relationships.

49
00:03:01,281 --> 00:03:07,110
Every relationship has a direction, goes from one table that’s in here.

50
00:03:07,111 --> 00:03:07,630
It shows one.

51
00:03:07,631 --> 00:03:10,470
And the other table it shows an asterisk.

52
00:03:10,471 --> 00:03:13,790
This represents the fact that these relationships are one

53
00:03:13,791 --> 00:03:15,480
to many relationships.

54
00:03:15,481 --> 00:03:17,970
So for example, this FactInternetSales,

55
00:03:17,971 --> 00:03:20,880
which is the main table in this model that contains the actual sales

56
00:03:20,881 --> 00:03:23,880
transactions has many rows.

57
00:03:23,881 --> 00:03:28,280
For each row in the DimProduct, which contains a row for

58
00:03:28,281 --> 00:03:29,260
each product.

59
00:03:29,261 --> 00:03:33,810
So one product, many rows in the FactInternetSales table.

60
00:03:33,811 --> 00:03:37,520
The same with customers, many rows in the sales,

61
00:03:37,521 --> 00:03:41,790
many transactions the customers have purchased multiple times so

62
00:03:41,791 --> 00:03:45,500
it will have multiple rows in the Internet sales and

63
00:03:45,501 --> 00:03:50,050
one row representing this customer in the customer table.

64
00:03:50,051 --> 00:03:52,870
In this example there’s a table called DimGeography which actually

65
00:03:52,871 --> 00:03:54,770
represents cities.

66
00:03:54,771 --> 00:03:59,280
So a customer lives in a city, city has many customers.

67
00:03:59,281 --> 00:04:01,140
So there’s a one to many relationship between customers and

68
00:04:01,141 --> 00:04:03,810
the city in the DimGeography.

69
00:04:03,811 --> 00:04:08,640
So the same table of customers play the role of one against

70
00:04:08,641 --> 00:04:14,320
the five Internet sales and the role of a many against the DimGeography.

71
00:04:14,321 --> 00:04:19,030
And relationships are what make this model behave as one.

72
00:04:19,031 --> 00:04:25,330
I can use this model to, people in Excel can use this model.

73
00:04:25,331 --> 00:04:30,150
As one, as if it was one table and we will see it in a moment.

74
00:04:30,151 --> 00:04:32,670
Let’s switch back to the data view and

75
00:04:32,671 --> 00:04:36,120
look at the content of the fact internet sales.

76
00:04:36,121 --> 00:04:40,760
So, this table has some information about the product that was

77
00:04:40,761 --> 00:04:45,290
purchased, the order, date, the customer, the quantity, the list

78
00:04:45,291 --> 00:04:50,720
price and the product cost for each unit of product that was purchased.

79
00:04:50,721 --> 00:04:53,480
In order to do some analysis, I obviously missed something here.

80
00:04:53,481 --> 00:04:55,280
I missed the actual revenue for

81
00:04:55,281 --> 00:04:59,240
this line, which is the order quantity times the list price.

82
00:04:59,241 --> 00:05:01,970
So, actually I can add calculated

83
00:05:01,971 --> 00:05:07,160
columns to this table that use other columns in the table.

84
00:05:07,161 --> 00:05:11,080
So, I just go to an empty column and I start typing an expression in

85
00:05:11,081 --> 00:05:13,590
a very similar way to what I would do in Excel.

86
00:05:13,591 --> 00:05:17,440
I click Equals, and I click on the order quantity,

87
00:05:19,010 --> 00:05:23,440
asterisk or times list price, enter.

88
00:05:23,441 --> 00:05:27,750
Now this will be now calculated for each row in the table.

89
00:05:27,751 --> 00:05:31,260
And now I can also go and give it a proper name.

90
00:05:31,261 --> 00:05:32,990
Let’s say it’s revenue.

91
00:05:34,370 --> 00:05:36,390
So, now we have the revenue calculated.

92
00:05:36,391 --> 00:05:43,144
And, if I want the cost, I will also go and create myself a cost.

93
00:05:51,440 --> 00:05:56,903
And I will say all the quantity times product cost,

94
00:05:56,904 --> 00:06:01,740
and again, I will have a calculated column.

95
00:06:03,150 --> 00:06:08,030
So, by having these calculated columns, I have completed

96
00:06:08,031 --> 00:06:12,548
the data that I need in this table and I can switch back to Excel,

97
00:06:12,549 --> 00:06:17,290
I will just, go back to Excel and in Excel I will say insert pivot table.

98
00:06:18,330 --> 00:06:21,840
This dialog is a little bit different in Excel 2016 than

99
00:06:21,841 --> 00:06:25,100
it was in previous versions because I have this very important button

100
00:06:25,101 --> 00:06:27,690
that says use this workbook’s data model.

101
00:06:27,691 --> 00:06:31,530
So the default for Excel to create a pivot table, or

102
00:06:31,531 --> 00:06:35,970
a pivot chart, if there is already a model in this file,

103
00:06:35,971 --> 00:06:40,070
is to use the data model as a basis for the pivot.

104
00:06:40,071 --> 00:06:41,730
So I click end, okay.

105
00:06:41,731 --> 00:06:43,290
And now I have my pivot.

106
00:06:43,291 --> 00:06:46,540
Now if you look on the right side, you’ll see that.

107
00:06:46,541 --> 00:06:49,150
This is not like,

108
00:06:49,151 --> 00:06:53,650
any other pivot that you’ve seen before because it

109
00:06:53,651 --> 00:06:57,700
contains all the tables that were in this model.

110
00:06:57,701 --> 00:07:01,540
In each one there are multiple columns, I can open it and

111
00:07:01,541 --> 00:07:06,790
then start dragging and using columns for any one of this tables.

112
00:07:06,791 --> 00:07:10,000
For example, I have the revenue, I can

113
00:07:10,001 --> 00:07:12,000
drag it to the values area.

114
00:07:12,001 --> 00:07:17,160
And I see the value coming here.

115
00:07:17,161 --> 00:07:21,780
If I want to take let’s say the country.

116
00:07:21,781 --> 00:07:27,120
Put it on rows and I go to another table here, which is about

117
00:07:27,121 --> 00:07:31,580
product categories and I take the product category and drag it too and

118
00:07:31,581 --> 00:07:36,100
now we have a full pivot and also I can create in the same way,

119
00:07:36,101 --> 00:07:42,630
I can create from, let’s say from the year I want to create a slicer.

120
00:07:42,631 --> 00:07:45,930
So I do create the slicer, and the slicer is now connected

121
00:07:45,931 --> 00:07:50,860
to the pivot and it’s able to slice the pivot.

122
00:07:50,861 --> 00:07:55,810
So I had my model with many tables with the relationship.

123
00:07:55,811 --> 00:07:58,530
The fact that I’m clicking here and I’m filtering on a year,

124
00:07:58,531 --> 00:08:02,220
because the year is coming from a table called date, and

125
00:08:02,221 --> 00:08:06,470
this table is connected or related to the main table,

126
00:08:06,471 --> 00:08:10,850
it means that when I’m clicking here it actually applies the filter to

127
00:08:10,851 --> 00:08:15,670
the main table and I see only rows that correspond to the year 2006 in

128
00:08:15,671 --> 00:08:21,030
the same way that this number here, for Australia bikes,

129
00:08:21,031 --> 00:08:25,490
show just data, the sum of data, for Australia and bikes.

130
00:08:25,491 --> 00:08:30,060
So I will close here, this part, and we’ll continue in the next

131
00:08:31,440 --> 00:08:35,370
video, actually it will be a direct continuation of where we stopped,

132
00:08:35,371 --> 00:08:38,280
and I will show you a little bit more of the options and

133
00:08:38,280 --> 00:08:41,170
what can I do with this model.

134
00:08:41,171 --> 00:08:42,711
So see you on the next part of this module.

