0
00:00:00,001 --> 00:00:04,413
[MUSIC]

1
00:00:04,414 --> 00:00:09,442
Hello, this is part two of module two and in the previous

2
00:00:09,443 --> 00:00:14,330
part we have seen the content of the Excel Data model.

3
00:00:14,331 --> 00:00:17,850
What’s in it, and that it’s made of tables and relationships.

4
00:00:17,851 --> 00:00:20,720
If you haven’t watched it yet, I would recommend going back and

5
00:00:20,721 --> 00:00:21,620
watching it.

6
00:00:21,621 --> 00:00:24,160
Because everything that we going to do in this part

7
00:00:24,161 --> 00:00:28,360
is based on what we just started to do in the previous video.

8
00:00:29,590 --> 00:00:36,250
So, we finished by having a pivot table populated from the data model.

9
00:00:36,251 --> 00:00:40,000
We’ve seen that the data, the field list that’s here on the right side,

10
00:00:40,001 --> 00:00:43,350
contained multiple tables and I was able to drag

11
00:00:43,351 --> 00:00:47,570
things from each one of the tables, and to create my first pivot.

12
00:00:47,571 --> 00:00:51,567
One more thing I want to show you here, before we dive into the data

13
00:00:51,568 --> 00:00:55,706
module using the Power Pivot add in, is, I dragged, and by default

14
00:00:55,707 --> 00:00:59,503
because I dragged something which is numeric, the revenue.

15
00:00:59,504 --> 00:01:04,466
It created a value of sum of revenue which is the same behavior

16
00:01:04,467 --> 00:01:09,260
as Excel would do with native pivot table from grid data.

17
00:01:09,261 --> 00:01:12,960
But there’s, for example, there’s things which are possible here which

18
00:01:12,961 --> 00:01:18,180
are not possible with a regular native pivot table.

19
00:01:18,181 --> 00:01:22,340
So, for example, if I drag the customer key to the values area,

20
00:01:23,860 --> 00:01:27,610
by default it will do something that is not really very useful,

21
00:01:27,611 --> 00:01:31,080
which is the sum of customer key, but I can go and

22
00:01:31,081 --> 00:01:34,390
change it to use another aggregation.

23
00:01:34,391 --> 00:01:38,810
And, if you see the bottom one the very last one of the aggregations

24
00:01:38,811 --> 00:01:40,580
is very very important.

25
00:01:40,581 --> 00:01:42,373
This is the Distinct Count.

26
00:01:42,374 --> 00:01:45,510
Distinct Count, so now, what I see is for

27
00:01:45,511 --> 00:01:49,726
each combination of country and category of products,

28
00:01:49,727 --> 00:01:54,479
I see the number of customers that actually have purchased this

29
00:01:54,480 --> 00:01:58,890
combination,  bikes in a country, unique customers.

30
00:01:58,891 --> 00:02:02,380
In the same way, I can drag now a product and show you

31
00:02:02,381 --> 00:02:06,730
how many different products were bought in a combination and so on.

32
00:02:06,731 --> 00:02:09,400
And this is something that is impossible to really do with

33
00:02:09,401 --> 00:02:13,020
a regular pivot and in general in many other tools is very,

34
00:02:13,021 --> 00:02:14,480
very difficult to do.

35
00:02:14,481 --> 00:02:18,850
And you can see that, with the same speed I can immediately

36
00:02:18,851 --> 00:02:23,970
calculate these results for any combination of slicers and so on.

37
00:02:23,971 --> 00:02:26,130
So, after we saw these two,

38
00:02:26,131 --> 00:02:31,350
let’s go back to the window that we saw on the previous model.

39
00:02:31,351 --> 00:02:35,620
The window into the data model, the manage, and

40
00:02:35,621 --> 00:02:38,409
in here continue to do a few more things.

41
00:02:39,660 --> 00:02:42,840
So, first of all, if you notice

42
00:02:42,841 --> 00:02:47,900
the revenue that I use was not really formatted nicely.

43
00:02:47,901 --> 00:02:51,662
I can, for each one of the columns, I can apply a format.

44
00:02:51,663 --> 00:02:55,610
I can go, for example, and say that this is currency.

45
00:02:55,611 --> 00:03:01,053
So, from now on every time I use this column,

46
00:03:01,054 --> 00:03:07,226
it will be formatted appropriately as a currency.

47
00:03:07,227 --> 00:03:11,390
But what I want to do now, is I want to create something.

48
00:03:11,391 --> 00:03:15,760
In the previous video, we created two calculated columns.

49
00:03:15,761 --> 00:03:18,810
Now, the syntax for these columns, maybe you wonder,

50
00:03:18,811 --> 00:03:20,080
what is the syntax?

51
00:03:20,081 --> 00:03:21,630
It looks very much like Excel.

52
00:03:21,631 --> 00:03:24,220
Is it really just an Excel formula?

53
00:03:24,221 --> 00:03:26,780
Actually, it’s not.

54
00:03:26,781 --> 00:03:29,300
This expression, which is a very simple one, but

55
00:03:29,301 --> 00:03:33,221
still, is written in language called DAX, D-A-X.

56
00:03:33,222 --> 00:03:39,090
And DAX was created together with the data model.

57
00:03:39,091 --> 00:03:42,830
And it starts by being really very simple but it goes way,

58
00:03:42,831 --> 00:03:46,760
way beyond that and into some very sophisticated options for

59
00:03:46,761 --> 00:03:48,320
calculations.

60
00:03:48,321 --> 00:03:52,790
Now, DAX can be used in Excel in the data model in two

61
00:03:52,791 --> 00:03:53,530
different positions.

62
00:03:53,531 --> 00:03:56,949
One is for calculated columns and one is for measures.

63
00:03:57,960 --> 00:04:01,520
Measures are, there are multiple ways to do them.

64
00:04:01,521 --> 00:04:06,110
One way is to use this area here below the rows

65
00:04:06,111 --> 00:04:07,770
in order to create the measures.

66
00:04:07,771 --> 00:04:12,730
So, let me start creating some simple

67
00:04:12,731 --> 00:04:16,850
measures and show you, for example.

68
00:04:16,851 --> 00:04:21,410
If I want to create a measure for the total revenue,

69
00:04:21,411 --> 00:04:22,930
I will call it totrev.

70
00:04:23,970 --> 00:04:27,500
And the syntax is name:=.

71
00:04:27,501 --> 00:04:29,255
And now we start to create an expression.

72
00:04:29,256 --> 00:04:36,140
Sum of revenue and I complete it with tab.

73
00:04:36,141 --> 00:04:37,037
Close parentheses.

74
00:04:39,661 --> 00:04:43,712
And I have the results here, and I can also go and

75
00:04:43,713 --> 00:04:48,260
format it, let’s say, to be a currency.

76
00:04:48,261 --> 00:04:52,320
This is my first example of creating a, what is called,

77
00:04:52,321 --> 00:04:56,140
an explicit measure.

78
00:04:56,141 --> 00:04:59,760
Actually, I created already measures behind the scenes, if you go for

79
00:04:59,761 --> 00:05:01,730
a moment back to the pivot.

80
00:05:01,731 --> 00:05:04,800
When I dragged anything to the values area,

81
00:05:04,801 --> 00:05:08,820
it actually was creating for me a measure, behind the scenes.

82
00:05:08,821 --> 00:05:10,840
This is called an implicit measure.

83
00:05:10,841 --> 00:05:14,220
But, what I just did was to replace this

84
00:05:14,221 --> 00:05:17,670
implicit functionality by explicitly creating measures.

85
00:05:17,671 --> 00:05:20,430
So, this one is not more sophisticated than the other that I

86
00:05:20,431 --> 00:05:24,792
was creating by dragging, but there are reasons even, for

87
00:05:24,793 --> 00:05:30,660
this kind of simple example, to create explicit measures and

88
00:05:30,661 --> 00:05:35,040
it’s a good practice to use them exclusively.

89
00:05:35,041 --> 00:05:37,530
Now, I can also create another one.

90
00:05:37,531 --> 00:05:39,919
For example, for the total cost.

91
00:05:42,007 --> 00:05:47,732
And I will say totcost, sum of,

92
00:05:47,733 --> 00:05:54,316
and this is sum of the cost column in.

93
00:05:57,047 --> 00:06:00,940
And again, format it as currency.

94
00:06:01,950 --> 00:06:05,190
Now I will create a third measure

95
00:06:05,191 --> 00:06:08,750
which is a little bit more sophisticated.

96
00:06:08,751 --> 00:06:11,320
And this one will be calculating the margin percent.

97
00:06:11,321 --> 00:06:12,540
So once I have the revenue and

98
00:06:12,541 --> 00:06:15,970
the cost, I can calculate the margin percent.

99
00:06:15,971 --> 00:06:20,099
So margin percent:=, and

100
00:06:20,100 --> 00:06:24,744
here I will say open parenthesis

101
00:06:24,745 --> 00:06:30,097
revenue minus cost divided by cost.

102
00:06:30,098 --> 00:06:35,740
So I’m not referring to columns in the table, but instead

103
00:06:35,741 --> 00:06:39,450
I’m referring to other calculated measures that are already created.

104
00:06:39,451 --> 00:06:41,390
I will format this as a percent.

105
00:06:43,050 --> 00:06:48,419
And going back to my pivot,

106
00:06:48,420 --> 00:06:53,118
I will take out the two.

107
00:06:53,119 --> 00:06:55,920
I still haven’t replaced it, so for

108
00:06:55,921 --> 00:06:58,580
now I can leave it or I can take it out.

109
00:06:58,581 --> 00:07:03,843
But, I want just to show you that now, when I go back to the list

110
00:07:03,844 --> 00:07:09,034
of columns in this table, the main table, I see the totrev.

111
00:07:09,035 --> 00:07:14,304
I can click on it, the totcost and the margin which is a percent.

112
00:07:14,305 --> 00:07:18,314
And, I can still, of course, filter, slice it by year and

113
00:07:18,315 --> 00:07:20,880
calculate it every time.

114
00:07:20,881 --> 00:07:24,270
Now let’s stop here for a moment, and actually understand what has

115
00:07:24,271 --> 00:07:26,890
happened when I created this measure.

116
00:07:26,891 --> 00:07:31,560
I created this measure once in here.

117
00:07:31,561 --> 00:07:36,560
And I just said that this is the sum of, so this is like the margin,

118
00:07:36,561 --> 00:07:41,140
but this is one this is the sum of the column revenue.

119
00:07:41,141 --> 00:07:44,100
Although I defined it once,

120
00:07:44,101 --> 00:07:48,360
every time I see its use it gives me a different number.

121
00:07:48,361 --> 00:07:51,320
And the reason it’s giving me a different number is

122
00:07:51,321 --> 00:07:56,330
that it’s being calculated fresh every time with a different context.

123
00:07:56,331 --> 00:08:01,320
And the context, like in this cell here is Australia bikes in 2006.

124
00:08:01,321 --> 00:08:05,460
So, this is the sum of revenue and this is the sum of cost.

125
00:08:05,461 --> 00:08:06,700
And this is the margin for

126
00:08:06,701 --> 00:08:11,470
this combination of Australia, bikes and the year 2006.

127
00:08:11,471 --> 00:08:17,890
If I go here, this would be another calculation.

128
00:08:17,891 --> 00:08:19,960
Again, every time it’s another calculation.

129
00:08:19,961 --> 00:08:24,158
If I go to the grand total, again it’s a calculation of this measure.

130
00:08:24,159 --> 00:08:27,060
It’s not a sum of this column here,

131
00:08:27,061 --> 00:08:31,400
it’s actually a new fresh calculation with a new context.

132
00:08:31,401 --> 00:08:32,870
So, this is a very,

133
00:08:32,871 --> 00:08:37,660
very important principle for the DAX language that we’re going to see,

134
00:08:37,659 --> 00:08:42,300
and I will repeat a few times for you to grasp the importance of,

135
00:08:42,301 --> 00:08:45,160
that every measure is calculated with a context.

136
00:08:45,161 --> 00:08:48,980
And the context, by default, comes from if it’s used in the pivot,

137
00:08:48,981 --> 00:08:53,283
there’s something on columns, something on rows, some slicers,

138
00:08:53,284 --> 00:08:56,320
a filter maybe for the pivot and so on.

139
00:08:56,321 --> 00:09:00,610
So all this gives a context in which the calculation is calculated.

140
00:09:00,611 --> 00:09:02,280
So this is for measures.

141
00:09:02,281 --> 00:09:04,720
Now, what about, let’s switch back for

142
00:09:04,721 --> 00:09:08,230
a moment to understand more about the calculated columns.

143
00:09:08,231 --> 00:09:11,930
Do they also have the same filter context?

144
00:09:11,931 --> 00:09:13,010
Well, actually they don’t.

145
00:09:13,011 --> 00:09:14,720
They have a different context.

146
00:09:14,721 --> 00:09:20,340
When I calculate this calculated column, it doesn’t

147
00:09:20,341 --> 00:09:25,400
see the pivot that will eventually be used or built from that.

148
00:09:25,401 --> 00:09:28,800
It actually sees the row that it lives in.

149
00:09:28,801 --> 00:09:33,650
And you can use any other column in this row of data.

150
00:09:33,651 --> 00:09:37,880
So, in this case, it’s used the other quantity and the list price.

151
00:09:37,881 --> 00:09:44,100
It could also use columns in other tables, like an extended row.

152
00:09:44,101 --> 00:09:45,020
And why is that?

153
00:09:45,021 --> 00:09:47,210
It is because of the relationships.

154
00:09:47,211 --> 00:09:53,300
The relationships make other columns to be kind of related to this one.

155
00:09:53,301 --> 00:09:56,420
So for example, for each row here, I have the product key.

156
00:09:56,421 --> 00:10:00,070
So every property of the product,

157
00:10:00,071 --> 00:10:03,840
I can use here, or I have the customer key.

158
00:10:03,841 --> 00:10:06,360
So, because of the relationship with customers,

159
00:10:06,361 --> 00:10:12,200
I can use data from the customers in here if I need to.

160
00:10:12,201 --> 00:10:17,280
Let me give you one example to create such a calculated column that

161
00:10:17,281 --> 00:10:23,150
is using the context which is called the row context to,

162
00:10:23,151 --> 00:10:28,750
let’s go, and for example into the product table.

163
00:10:28,751 --> 00:10:32,520
And in the product table I have this data and I want,

164
00:10:32,521 --> 00:10:35,850
let’s say that you saw that I have product category table and

165
00:10:35,851 --> 00:10:37,480
product sub category table.

166
00:10:37,481 --> 00:10:41,490
But those two tables are actually used just for one column each.

167
00:10:41,491 --> 00:10:43,750
The dimproductcategory has the category name and

168
00:10:43,751 --> 00:10:45,780
the subcategory has the subcategory name.

169
00:10:46,980 --> 00:10:50,820
I may have a goal to reduce the number of tables that my user sees

170
00:10:50,821 --> 00:10:52,500
when he creates a pivot table.

171
00:10:52,501 --> 00:10:55,910
In order to do that, I can actually create a column here,

172
00:10:55,911 --> 00:10:59,770
in the products table that will have the category name, for example.

173
00:10:59,771 --> 00:11:02,840
So, in order to do that I create a new column.

174
00:11:02,841 --> 00:11:05,340
And I use a DAX function called related.

175
00:11:05,341 --> 00:11:09,620
Related means I’m going to use a column from another table but

176
00:11:09,621 --> 00:11:13,160
that is related to this one.

177
00:11:13,161 --> 00:11:15,840
And, immediately it gives me the list of options of things which

178
00:11:15,841 --> 00:11:17,610
are related to where I am.

179
00:11:17,611 --> 00:11:18,950
And the first one is what I want.

180
00:11:18,951 --> 00:11:20,520
I want a category name.

181
00:11:20,521 --> 00:11:23,130
So give me the category name for this product.

182
00:11:25,360 --> 00:11:29,570
So it goes, and it brings the necessary data from the other.

183
00:11:30,900 --> 00:11:35,528
Now it seems as if it didn’t bring anything, but actually it did,

184
00:11:35,529 --> 00:11:39,745
because there are some products in the beginning that did not

185
00:11:39,746 --> 00:11:43,561
have the right connection to subcategories and so on.

186
00:11:43,562 --> 00:11:46,910
So, it seems empty but it’s not.

187
00:11:46,911 --> 00:11:48,750
And I can call it category.

188
00:11:49,950 --> 00:11:58,640
So now I have a category column in the product table.

189
00:11:58,641 --> 00:12:02,800
And this will also enable me, so I have here, see the list of bikes,

190
00:12:02,801 --> 00:12:05,830
clothing, components, and some products which will actually

191
00:12:05,831 --> 00:12:08,540
have a blank for the name of the category.

192
00:12:08,541 --> 00:12:09,870
So, once I have this,

193
00:12:09,871 --> 00:12:12,510
I can actually say that I don’t need the product category.

194
00:12:12,511 --> 00:12:16,560
I can go here, right-click and say just hide it.

195
00:12:16,561 --> 00:12:18,380
I don’t delete it, but I hide it.

196
00:12:18,381 --> 00:12:21,640
So, when I go back to my pivot table,

197
00:12:21,641 --> 00:12:25,570
I will see that the product category table have disappeared.

198
00:12:25,571 --> 00:12:30,510
And if I opened a dimproduct table, I will see that it has now

199
00:12:30,511 --> 00:12:34,740
a new column that wasn’t there that is called a category.

200
00:12:34,741 --> 00:12:38,460
And I can use this instead of the original one that came from

201
00:12:38,461 --> 00:12:39,320
the category table.

202
00:12:39,321 --> 00:12:41,850
So I reduce the number of tables and

203
00:12:41,851 --> 00:12:46,610
I added a calculated column from a related table.

204
00:12:46,611 --> 00:12:50,670
So this ends the second part of this module.

205
00:12:50,671 --> 00:12:52,700
We’ve seen the data model.

206
00:12:52,701 --> 00:12:54,260
We’ve seen relationships.

207
00:12:54,261 --> 00:12:58,510
We’ve started to see calculated measures using DAX.

208
00:12:58,511 --> 00:13:01,350
And we saw the use of calculated columns.

209
00:13:01,351 --> 00:13:05,230
The measures are calculated within a filter context,

210
00:13:05,231 --> 00:13:09,260
while the calculated columns are calculated within a row context.

211
00:13:09,261 --> 00:13:11,850
We’re going to use a lot more of the measures, not so

212
00:13:11,851 --> 00:13:15,500
much calculated columns, in subsequent modules.

213
00:13:15,501 --> 00:13:16,385
I hope to see you there.

