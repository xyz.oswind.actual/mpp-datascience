0
00:00:00,001 --> 00:00:03,995
[MUSIC]

1
00:00:03,996 --> 00:00:05,396
All right. Welcome back.

2
00:00:05,397 --> 00:00:09,790
We’re now in the second part of module four.

3
00:00:09,791 --> 00:00:14,900
Remember in the previous part, I imported data from SQL server,

4
00:00:14,901 --> 00:00:16,350
bringing in four tables,

5
00:00:17,830 --> 00:00:19,690
making sure that they have relationship between them.

6
00:00:19,691 --> 00:00:22,360
Some of the relationships were identified automatically,

7
00:00:22,361 --> 00:00:24,300
detected by the tool.

8
00:00:24,301 --> 00:00:25,800
One of them I needed to do manually.

9
00:00:27,050 --> 00:00:32,990
And in this part I will add some aspects to this model,

10
00:00:32,991 --> 00:00:37,950
bringing some additional data, combining it with existing data, and

11
00:00:37,951 --> 00:00:40,560
making it a more complete model.

12
00:00:40,561 --> 00:00:45,350
So let me just first jump into the window, the manage window,

13
00:00:46,490 --> 00:00:50,960
and one of the things that in almost every model we do we need,

14
00:00:50,961 --> 00:00:52,170
is a calendar table.

15
00:00:53,180 --> 00:00:56,120
Calendar tables are required

16
00:00:56,121 --> 00:01:01,120
we have a date In the cell’s table we do have a date column, but

17
00:01:01,121 --> 00:01:02,050
this is just a date.

18
00:01:02,051 --> 00:01:07,130
What if I want to filter on year, months, week or so on.

19
00:01:07,131 --> 00:01:10,730
And also, what if I want to do some more sophisticated calculations,

20
00:01:10,731 --> 00:01:14,990
that I will actually show you in a later module,

21
00:01:14,991 --> 00:01:17,350
like year over year comparisons.

22
00:01:17,351 --> 00:01:22,190
For all these purposes, we do need a calendar table.

23
00:01:22,191 --> 00:01:27,390
Now in Excel 2016, we actually added a very easy way to do this.

24
00:01:27,391 --> 00:01:31,510
With previous versions you will need to either create the table in Excel

25
00:01:31,511 --> 00:01:32,650
and bring data in Excel.

26
00:01:32,651 --> 00:01:33,770
By the way, I didn’t mention but

27
00:01:33,771 --> 00:01:38,290
you can actually create tables in Excel and bring them into the model.

28
00:01:38,291 --> 00:01:41,480
So one of the sources available to you is just a table in Excel.

29
00:01:41,481 --> 00:01:45,910
But here, you see that I go to the design tab

30
00:01:45,911 --> 00:01:49,460
in the window of the manage window.

31
00:01:49,461 --> 00:01:51,980
And I see that there is a Date Table here, and

32
00:01:51,981 --> 00:01:55,640
I can say create me a new Date Table.

33
00:01:55,641 --> 00:01:57,630
So this is just one click, and

34
00:01:57,631 --> 00:02:03,500
it added the table to my model, called calendar.

35
00:02:03,501 --> 00:02:07,060
It has one column of type date, and additional columns,

36
00:02:07,061 --> 00:02:10,770
calculated columns, that add all kind of properties to it.

37
00:02:10,770 --> 00:02:15,930
The only thing I still need to do is to create

38
00:02:15,931 --> 00:02:20,160
a relationship between this new table and the rest of the tables.

39
00:02:20,161 --> 00:02:25,340
Oh actually, because I had to redo it, I didn’t do the, so

40
00:02:25,341 --> 00:02:30,766
let me very quickly do the relationship between

41
00:02:30,767 --> 00:02:35,713
locations and the locations here so,

42
00:02:35,714 --> 00:02:41,430
cuz this is, we need it for the rest of it, so.

43
00:02:42,670 --> 00:02:48,740
And, the new table, which is just hiding here because of the scale.

44
00:02:48,741 --> 00:02:50,580
Let me bring it back.

45
00:02:50,581 --> 00:02:55,102
And, now I can just create a relationship between the date here

46
00:02:55,103 --> 00:02:56,731
and the date in sales.

47
00:02:56,732 --> 00:03:01,855
It created everything I needed, only it didn’t create the relationship.

48
00:03:01,856 --> 00:03:07,270
It actually created the necessary data in the data,

49
00:03:07,271 --> 00:03:09,855
in the calendar table for

50
00:03:09,856 --> 00:03:14,668
all the dates it found in any one of the tables.

51
00:03:14,669 --> 00:03:18,344
It analyzes all the tables, found, in this case there is

52
00:03:18,345 --> 00:03:22,188
only one date column and it goes from 2011 to 2015.

53
00:03:22,189 --> 00:03:26,768
I have data in the calendar table for all these years.

54
00:03:26,769 --> 00:03:31,568
If I want to I can go to the Date Table and actually, I can extend it.

55
00:03:31,569 --> 00:03:38,489
Once I am on it I can extend the dates and make dates for the years.

56
00:03:38,490 --> 00:03:41,000
But let’s go because we have work to do.

57
00:03:41,001 --> 00:03:44,500
Let’s go back to Excel and I want to create another query.

58
00:03:46,040 --> 00:03:50,500
This query is actually, the reason that I need to do another query is

59
00:03:50,501 --> 00:03:54,840
that this company has the sales data for

60
00:03:54,841 --> 00:03:58,872
the US in this SQL server database but sales data from other countries

61
00:03:58,873 --> 00:04:05,360
are sent as text files to this guy who’s doing this analysis.

62
00:04:05,361 --> 00:04:08,640
And I am this guy in this case, and

63
00:04:08,641 --> 00:04:12,500
I need to combine the data from the text files to

64
00:04:12,501 --> 00:04:16,610
the data from the SQL server to be able to create some global analysis.

65
00:04:17,660 --> 00:04:19,500
So, how do I do that?

66
00:04:19,500 --> 00:04:22,330
I go to another query from file.

67
00:04:22,331 --> 00:04:25,736
And here, It’s not a single CSV, but

68
00:04:25,737 --> 00:04:29,500
actually it’s a folder full of CSV data.

69
00:04:29,501 --> 00:04:31,020
Here is this folder.

70
00:04:31,021 --> 00:04:34,780
You see that there are a bunch of, in this case four, files.

71
00:04:34,781 --> 00:04:37,740
I copy the path.

72
00:04:37,741 --> 00:04:39,590
I paste it here.

73
00:04:39,591 --> 00:04:44,290
And now I will create a query from the entire content of this query.

74
00:04:44,291 --> 00:04:48,670
I have all kinds of columns of properties of the file,

75
00:04:48,671 --> 00:04:50,960
which I’m not interested in at this point

76
00:04:50,961 --> 00:04:54,390
I’m just interested in the first column, which is the content.

77
00:04:54,391 --> 00:04:57,560
I can click on this icon here and

78
00:04:57,561 --> 00:05:02,750
get myself into the Editor with all the data in

79
00:05:02,751 --> 00:05:08,490
all these files combined together, as if they were one.

80
00:05:08,491 --> 00:05:13,580
The headers from the first file were used to become the headers for

81
00:05:13,581 --> 00:05:17,150
the entire file, which is fine for me, but

82
00:05:17,151 --> 00:05:20,260
there are a few cleanup things that I need to do.

83
00:05:20,261 --> 00:05:22,730
First of all, I need to create what is

84
00:05:22,731 --> 00:05:26,415
called the custom column here in the query.

85
00:05:26,416 --> 00:05:29,055
Remember from country and zip I need to do it also here.

86
00:05:30,115 --> 00:05:32,785
Because this will be actually the extension

87
00:05:32,786 --> 00:05:35,365
of the sales table which already have this column.

88
00:05:35,366 --> 00:05:37,355
So merge column,

89
00:05:37,356 --> 00:05:41,805
the same way we did before and it’s going to be again the location.

90
00:05:41,806 --> 00:05:44,180
Here it’s very important to give it the same name.

91
00:05:44,181 --> 00:05:48,570
Because this is how, when we append this data to the previous data,

92
00:05:48,571 --> 00:05:52,480
each column here would find its matching in the existing data.

93
00:05:53,860 --> 00:05:54,960
That’s fine.

94
00:05:54,961 --> 00:05:58,870
Now, there’s another little problem here, which is that each one of

95
00:05:58,871 --> 00:06:03,680
the files, and remember there were four of them, has as its first row

96
00:06:03,681 --> 00:06:08,250
the headers and I need to get rid of all the other headers except the one

97
00:06:08,251 --> 00:06:11,510
that was used for giving the headers for the entire file.

98
00:06:11,511 --> 00:06:14,250
Now I cannot filter it from here.

99
00:06:14,251 --> 00:06:19,759
I can actually go from here, so for example I can say,

100
00:06:19,760 --> 00:06:24,899
in the zip column, it’s a text field and I can say I want

101
00:06:24,900 --> 00:06:31,040
to filter out all the rows which does not equal the word zip.

102
00:06:32,430 --> 00:06:36,910
But I would like to do it before, no actually now it’s,

103
00:06:36,911 --> 00:06:41,120
I didn’t filter the date yet so it’s actually a good time.

104
00:06:41,121 --> 00:06:42,750
I can say does not equal.

105
00:06:44,100 --> 00:06:47,850
So the zip column does not equal the value zip itself

106
00:06:47,851 --> 00:06:50,590
assuming there is no zip code called zip.

107
00:06:50,591 --> 00:06:55,150
And I’m filtering it out so by filtering the value of zip I

108
00:06:55,151 --> 00:06:58,140
actually filter the rows which were including the headers for

109
00:06:58,141 --> 00:06:59,270
the other files.

110
00:06:59,271 --> 00:07:03,760
Now that I got rid of those, I can actually go to the date field and

111
00:07:03,761 --> 00:07:08,790
apply the filter on the date field to include only years after 2011.

112
00:07:08,791 --> 00:07:12,020
Remember, I did the same for the sales and

113
00:07:12,021 --> 00:07:15,690
there’s no point in bringing me the international data from 1999 when

114
00:07:15,691 --> 00:07:20,222
I’m actually interested in 2011.

115
00:07:20,223 --> 00:07:23,960
All right so now we have these four files and

116
00:07:23,961 --> 00:07:28,890
I’m going to the close and load to dialogue, because I actually

117
00:07:28,891 --> 00:07:33,820
don’t want this query to be loaded to the data module as yet.

118
00:07:33,821 --> 00:07:37,050
I just want to define it, create it,

119
00:07:37,051 --> 00:07:40,250
and it will be ready for the next step.

120
00:07:40,251 --> 00:07:44,420
So I am going to load it, but not really load it.

121
00:07:44,421 --> 00:07:45,370
It’s just a connection.

122
00:07:45,371 --> 00:07:50,330
It’s not loaded to the data model, and also not loaded into Excel.

123
00:07:50,331 --> 00:07:53,640
So, here it is Query1, it says connection only.

124
00:07:53,641 --> 00:07:55,300
Now I go back to the sales table.

125
00:07:56,680 --> 00:07:59,760
And go again into the Editor.

126
00:07:59,761 --> 00:08:02,560
And I am going to add another step to this.

127
00:08:02,561 --> 00:08:03,810
This is the table.

128
00:08:03,811 --> 00:08:07,080
By the way, I want to draw your attention to these two columns,

129
00:08:07,081 --> 00:08:09,920
location and products here.

130
00:08:09,921 --> 00:08:14,590
Those are very special columns that were added by the query,

131
00:08:14,591 --> 00:08:17,875
because it detected the relationship between this table and others.

132
00:08:17,876 --> 00:08:21,540
There’s actually an option that I will not use here to select

133
00:08:21,541 --> 00:08:25,120
columns for these other tables, and add them automatically to here.

134
00:08:25,121 --> 00:08:27,040
So, you can just ignore them, and

135
00:08:27,041 --> 00:08:30,470
they will not do anything in the model, if you don’t use them.

136
00:08:31,550 --> 00:08:36,900
So, I will use another step, which is called the append query.

137
00:08:36,900 --> 00:08:41,160
I will append another query to this query, so

138
00:08:41,159 --> 00:08:43,960
the query to append will be the one called Query1.

139
00:08:43,961 --> 00:08:45,460
I just left it as a default name.

140
00:08:45,461 --> 00:08:48,590
I didn’t bother to change the name to something else,

141
00:08:48,591 --> 00:08:52,520
because I knew that this would not become a table in the database,

142
00:08:52,521 --> 00:08:54,660
even in the model eventually, so

143
00:08:54,661 --> 00:08:59,570
I didn’t care much about giving it a meaningful name, and I click OK.

144
00:09:00,760 --> 00:09:02,410
So now we have another step.

145
00:09:02,411 --> 00:09:06,460
I want to also draw your attention to another element and

146
00:09:06,461 --> 00:09:10,879
this is the fact that here, I have some

147
00:09:10,880 --> 00:09:17,420
expressions that, this is actually the actual

148
00:09:17,421 --> 00:09:21,990
script line that is doing, in this case, the append query.

149
00:09:21,991 --> 00:09:26,180
Each step here will have its corresponding line in the script.

150
00:09:26,181 --> 00:09:27,272
And this script is,

151
00:09:27,273 --> 00:09:30,302
we don’t really are going to look at this at this point.

152
00:09:30,303 --> 00:09:32,402
We’re not interested in the actual syntax.

153
00:09:32,403 --> 00:09:37,047
In more advanced use of the queries you can actually start

154
00:09:37,048 --> 00:09:42,005
getting more to know the language this script is written on.

155
00:09:42,006 --> 00:09:45,450
The language is called the M language, but for now,

156
00:09:45,451 --> 00:09:50,610
in our course here, we’re not going to touch it for anything.

157
00:09:50,611 --> 00:09:54,270
So, but it’s there, and actually if you know what you’re doing you can

158
00:09:54,271 --> 00:09:58,250
go and actually apply some tweaks to the syntax.

159
00:09:58,251 --> 00:10:01,920
But for now we are happy with that, and we are going to close and

160
00:10:01,921 --> 00:10:06,950
load, and now what will happen is that

161
00:10:06,951 --> 00:10:12,270
the sales query will fire again, repeat everything, all the steps,

162
00:10:12,271 --> 00:10:16,690
which starts by reading this two million rows from a SQL server.

163
00:10:16,691 --> 00:10:19,900
Once it’s done reading the two million rows form SQL server,

164
00:10:19,901 --> 00:10:23,580
it will start reading the rows from the Query1.

165
00:10:23,581 --> 00:10:28,610
Which is in itself four CSV files combined together.

166
00:10:28,611 --> 00:10:32,260
And the end result of the sales table is going to be

167
00:10:33,260 --> 00:10:35,750
the entire content of the sales table.

168
00:10:35,751 --> 00:10:38,620
Starting filtering to only 2011,

169
00:10:38,621 --> 00:10:43,960
plus the text files appended to it and all

170
00:10:43,961 --> 00:10:48,740
of them will become one big happy file, happy table in the model.

171
00:10:49,870 --> 00:10:54,950
And then subsequently if I get these

172
00:10:54,951 --> 00:11:00,210
text files every month, I can just repeat this entire process and

173
00:11:00,211 --> 00:11:02,590
it will give me the entire global analysis.

174
00:11:02,591 --> 00:11:04,110
But what happened here?

175
00:11:04,111 --> 00:11:07,690
We got some yellow notification here, and

176
00:11:07,691 --> 00:11:10,330
the model looks very funny.

177
00:11:10,331 --> 00:11:13,202
It shows me the same value for every cell.

178
00:11:13,203 --> 00:11:15,600
What happened?

179
00:11:15,601 --> 00:11:20,230
Actually, because now the sales table is not directly a table from

180
00:11:20,231 --> 00:11:25,280
the database, the tool recognized that the previously identified,

181
00:11:25,281 --> 00:11:28,670
detected relationship between this sales stable and

182
00:11:28,671 --> 00:11:31,610
the other tables are not valid anymore.

183
00:11:31,611 --> 00:11:33,480
And so it decided to delete it.

184
00:11:33,481 --> 00:11:36,040
This is a functionality that we probably are going to change in

185
00:11:36,041 --> 00:11:37,020
the future.

186
00:11:37,021 --> 00:11:41,506
But it’s actually very easy to fix because we have another, new for

187
00:11:41,507 --> 00:11:46,241
2016, functionality to detect the relationships from the data.

188
00:11:46,242 --> 00:11:51,429
So now when I clicked on it, it actually brought back the numbers

189
00:11:51,430 --> 00:11:56,342
to where they were and we now have the data from all countries.

190
00:11:56,343 --> 00:11:59,928
And if I go to the location and pick the country and

191
00:11:59,929 --> 00:12:05,001
make it into a slicer, I see all the countries from which I have data,

192
00:12:05,002 --> 00:12:09,285
and I can see the data from France and I can control click and

193
00:12:09,286 --> 00:12:13,070
see the data from France and Germany and so on.

194
00:12:13,071 --> 00:12:16,520
So now we have a truly global analysis of the data for

195
00:12:16,521 --> 00:12:17,036
this company.

196
00:12:17,037 --> 00:12:21,461
So to conclude, we added a calendar table using

197
00:12:21,462 --> 00:12:26,882
a functionality that was added to Power Pivot or to Excel,

198
00:12:26,883 --> 00:12:31,550
Power Pivot in Excel in this version of 2016.

199
00:12:31,551 --> 00:12:36,620
We also created another query from a folder of text files, added

200
00:12:36,621 --> 00:12:41,340
all the contents of these text files to the sales table, and eventually

201
00:12:41,341 --> 00:12:46,060
we got a model that has data both from SQL and from the text files.

202
00:12:46,061 --> 00:12:48,080
And it’s now a global file.

203
00:12:48,081 --> 00:12:49,375
Thank you, and see you in the next module.

