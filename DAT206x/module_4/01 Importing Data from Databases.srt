0
00:00:00,001 --> 00:00:04,333
[MUSIC]

1
00:00:04,334 --> 00:00:06,230
Hello, welcome back.

2
00:00:06,231 --> 00:00:10,200
In this module we’re going to actually

3
00:00:10,201 --> 00:00:12,430
again it will be in two parts and

4
00:00:12,431 --> 00:00:17,760
we’re going to dive in more depth into the query technology.

5
00:00:17,761 --> 00:00:22,630
In this case we’re going to bring data from a database, in this

6
00:00:22,631 --> 00:00:25,940
case a SQL Server database which has multiple tables that I’m interested

7
00:00:25,941 --> 00:00:31,190
in, and I want to create a model made of this collection of tables.

8
00:00:31,191 --> 00:00:35,567
And then also we’re going to, in the second part, mash the data,

9
00:00:35,568 --> 00:00:40,790
create a mash-up of this data with data coming from text files,

10
00:00:40,791 --> 00:00:44,270
make them seem like as if they come from the same source.

11
00:00:44,271 --> 00:00:48,560
So there’s a lot of query technology which we’re going to see in

12
00:00:48,561 --> 00:00:49,990
this part.

13
00:00:49,991 --> 00:00:52,750
So again, we are looking at Excel.

14
00:00:52,751 --> 00:00:55,300
Currently there is really nothing in this Excel.

15
00:00:55,301 --> 00:00:59,020
There’s no data model, there are no tables, nothing.

16
00:00:59,021 --> 00:01:04,541
So I’m going again to the data and in there I will start a new query.

17
00:01:04,542 --> 00:01:07,710
In this case it’s not a file, it’s a database.

18
00:01:07,711 --> 00:01:10,360
You see the list of databases is pretty long.

19
00:01:10,361 --> 00:01:15,890
If you browse further down you will see that they’re really a long list,

20
00:01:15,891 --> 00:01:18,380
an interesting list of data sources available.

21
00:01:20,040 --> 00:01:24,821
In these other sources, you will see Active Directory,

22
00:01:24,822 --> 00:01:29,510
you’ll see Exchange, CRM, Facebook, Salesforce.

23
00:01:29,511 --> 00:01:32,446
But we are going to use something more common,

24
00:01:32,447 --> 00:01:34,640
which is the SQL Server Database.

25
00:01:35,810 --> 00:01:37,890
So I’m connected to the database,

26
00:01:37,891 --> 00:01:41,850
and it first of all asks me what is the name the server.

27
00:01:41,851 --> 00:01:44,190
The server, this is the name of the server.

28
00:01:44,191 --> 00:01:46,310
Actually it’s the machine I’m using myself.

29
00:01:47,680 --> 00:01:48,870
And I’m clicking OK.

30
00:01:50,690 --> 00:01:54,263
Now the first time I’m using this source, it asks me for

31
00:01:54,264 --> 00:01:56,860
what kind of credential do I want to use.

32
00:01:56,861 --> 00:02:02,344
Do I want to use my current credential, which is regular Windows

33
00:02:02,345 --> 00:02:08,148
authentication, give me a warning about encryption, I click OK.

34
00:02:08,149 --> 00:02:12,400
And it will now actually present me with this navigation dialogue

35
00:02:12,401 --> 00:02:15,230
showing me the list of databases.

36
00:02:15,231 --> 00:02:17,570
This is the database that I’m going to use.

37
00:02:17,571 --> 00:02:23,550
There’s a checkbox here that allows me to select multiple tables,

38
00:02:23,551 --> 00:02:27,180
and actually I want to select all the tables.

39
00:02:27,181 --> 00:02:29,910
Every time I’m in a table, as you can see,

40
00:02:29,911 --> 00:02:35,000
it shows me a preview of the content of this table in the database.

41
00:02:35,001 --> 00:02:38,310
In some cases, it could be even be the entire table but

42
00:02:38,311 --> 00:02:43,460
mostly it’s just a subset of the rows of this database.

43
00:02:43,461 --> 00:02:47,630
And I’m not ready to use these tables as they are, so

44
00:02:47,631 --> 00:02:50,290
I’m going to click here the Edit, and

45
00:02:50,291 --> 00:02:55,160
it will bring me into the same editor window that we saw before.

46
00:02:55,161 --> 00:03:00,960
It tells me that there is a preview already cached,

47
00:03:00,961 --> 00:03:02,500
and I can refresh it, and

48
00:03:02,501 --> 00:03:06,920
then I’m sure that actually I am looking at the up-to-date data.

49
00:03:06,921 --> 00:03:11,080
On the left side I see the list of tables that I imported, and

50
00:03:11,081 --> 00:03:13,120
I can switch between each one of them and

51
00:03:13,121 --> 00:03:17,290
apply some steps to each one of these.

52
00:03:18,580 --> 00:03:22,020
So what do I want to do?

53
00:03:22,021 --> 00:03:28,500
I want to go to the sales table, and first of all,

54
00:03:28,501 --> 00:03:35,210
I see here that I have a zip code and a country.

55
00:03:35,211 --> 00:03:40,410
In this case the data is just for one country, the USA, but

56
00:03:40,411 --> 00:03:44,260
if I go to the locations table, the locations table

57
00:03:44,261 --> 00:03:48,510
have actually information about each one of these zips of this region.

58
00:03:48,511 --> 00:03:52,430
So again, have here the zip code and a country, but

59
00:03:52,431 --> 00:03:56,430
I also have the city, the state, the region, so on.

60
00:03:56,431 --> 00:03:58,800
And if I go here and see the list of countries,

61
00:03:58,801 --> 00:04:01,300
I see that there’s actually more than one country.

62
00:04:01,301 --> 00:04:04,360
It actually shows me just the US and France.

63
00:04:04,361 --> 00:04:06,870
There’s more but actually in the preview.

64
00:04:06,871 --> 00:04:10,120
That it reads so far, because now I’m looking at the preview

65
00:04:10,121 --> 00:04:14,250
of the data, not all the data, and so only USA and France.

66
00:04:14,251 --> 00:04:18,470
So the problem with that is that I would like to create a relationship

67
00:04:18,471 --> 00:04:22,020
between this sales table and the locations table.

68
00:04:22,021 --> 00:04:25,330
Only that relation, and I didn’t mention before,

69
00:04:25,331 --> 00:04:31,110
a relationship can be done only based on one column in each side.

70
00:04:31,111 --> 00:04:36,550
So I can connect zip to zip, but the problem is that the same zip code

71
00:04:36,551 --> 00:04:39,470
can actually appear in multiple countries.

72
00:04:39,471 --> 00:04:44,486
And so it means that the zip code is not unique in the locations table.

73
00:04:44,487 --> 00:04:47,693
And it could potentially not be unique on the sales, well,

74
00:04:47,694 --> 00:04:51,608
obviously it’s not unique in the sales table because there’s multiple

75
00:04:51,609 --> 00:04:53,240
rows for each zip.

76
00:04:53,241 --> 00:04:57,180
And the relationship has to be unique at least on one side.

77
00:04:57,181 --> 00:04:59,700
We said that each relation is one to many, so

78
00:04:59,701 --> 00:05:01,680
on the one side has to be unique.

79
00:05:01,681 --> 00:05:02,660
So what can I do?

80
00:05:02,661 --> 00:05:06,972
I can create a combination between Country and Zip.

81
00:05:06,973 --> 00:05:08,100
I click on it.

82
00:05:08,101 --> 00:05:09,570
I Ctrl-click on that.

83
00:05:09,571 --> 00:05:12,030
And now I want to say that I want to add a column.

84
00:05:12,031 --> 00:05:13,370
Another option to create a column.

85
00:05:13,371 --> 00:05:15,900
Remember we had calculated columns in the model, but

86
00:05:15,901 --> 00:05:23,990
this one is a column that is created in the query.

87
00:05:25,550 --> 00:05:28,820
It says, okay, do you want a separator between them?

88
00:05:28,821 --> 00:05:31,750
No, I don’t want, I just want the data itself.

89
00:05:31,751 --> 00:05:35,350
And I will call the result column, I will call it Location.

90
00:05:37,020 --> 00:05:40,670
And here it is, I see a location coming here.

91
00:05:40,671 --> 00:05:44,360
I have to do the same on the other side, so that

92
00:05:44,361 --> 00:05:48,950
eventually I will be able to create the relationship between those two.

93
00:05:48,951 --> 00:05:52,350
So merge those two the same way.

94
00:05:54,180 --> 00:05:55,150
Call it Location.

95
00:05:57,570 --> 00:05:58,260
Here we go.

96
00:05:58,261 --> 00:06:00,860
Now we have Location here and Location there

97
00:06:00,861 --> 00:06:05,790
which I will be able eventually to create a relationship from.

98
00:06:05,791 --> 00:06:07,530
What else do I want to do?

99
00:06:07,531 --> 00:06:10,607
I have data here from 2008.

100
00:06:10,608 --> 00:06:13,830
I don’t need all this data, this historic data.

101
00:06:13,831 --> 00:06:17,500
I only want to start my analysis starting from 2010.

102
00:06:17,501 --> 00:06:22,540
So I can go here and apply a filter,

103
00:06:22,541 --> 00:06:26,980
it recognizes as a date, and I say you can apply a date filter.

104
00:06:26,981 --> 00:06:28,960
And I want to apply a filter that says,

105
00:06:28,961 --> 00:06:34,342
I just want data where the date is after

106
00:06:34,343 --> 00:06:39,200
12/31/2010.

107
00:06:39,201 --> 00:06:43,236
So starting from January of 2011.

108
00:06:43,237 --> 00:06:46,460
So this would just fill it now.

109
00:06:46,461 --> 00:06:49,400
Everything I filter here is actually going to

110
00:06:49,401 --> 00:06:51,820
import just the data that I filter.

111
00:06:51,821 --> 00:06:55,710
It’s not just a matter of showing me right here, the editor, less or

112
00:06:55,711 --> 00:06:56,430
more data.

113
00:06:56,431 --> 00:07:00,379
It’s actually about going and later on,

114
00:07:00,380 --> 00:07:03,260
when it goes to input an entire dataset,

115
00:07:03,261 --> 00:07:08,840
it will input only the data which comply with this filter.

116
00:07:08,841 --> 00:07:10,540
So now I think I’m ready to go.

117
00:07:10,541 --> 00:07:13,450
The other tables I don’t need to do anything to, so

118
00:07:13,451 --> 00:07:16,230
I can go back here and I say Close &amp; Load.

119
00:07:16,231 --> 00:07:20,090
I know that my default is already to put the data into the model, so

120
00:07:20,091 --> 00:07:24,300
you know that I will actually get multiple tables into the model.

121
00:07:24,301 --> 00:07:27,042
I click Load and now it starts loading.

122
00:07:27,043 --> 00:07:30,310
You see that it’s doing some loading in parallel.

123
00:07:30,311 --> 00:07:32,420
First it has to read them from the database,

124
00:07:32,421 --> 00:07:38,170
then it needs to put them into the data model.

125
00:07:38,171 --> 00:07:42,878
Now I see that it’s making progress on the sales table.

126
00:07:44,810 --> 00:07:50,790
Even the subset of data from 2011 is almost 2 million rows,

127
00:07:50,791 --> 00:07:55,160
so we’re waiting a little bit to this data to finish.

128
00:07:59,432 --> 00:08:05,090
Because I import from multiple tables, it will look to see if there

129
00:08:05,091 --> 00:08:08,840
are relationships that could be recognized from the database.

130
00:08:08,841 --> 00:08:13,620
In the database we have something called referential integrity,

131
00:08:13,621 --> 00:08:18,110
relationship between tables, and let’s now see what indeed

132
00:08:18,111 --> 00:08:22,210
it was recognizing, so I can go to the manage table.

133
00:08:22,211 --> 00:08:26,220
I can see my multiple tables.

134
00:08:30,623 --> 00:08:33,150
Manage, okay, here’s the tables.

135
00:08:33,150 --> 00:08:36,020
I see that the sales table in the bottom here,

136
00:08:36,020 --> 00:08:42,580
it said it has 1 million and 800-something rows 137 00:08:42,581 --&gt; 00:08:46,270 If I switch to diagram view, you’ll see that the tables,

137
00:08:46,271 --> 00:08:49,170
some of them are connected and some of them are not.

138
00:08:49,171 --> 00:08:51,610
The Sales is connected to the Products table.

139
00:08:51,611 --> 00:08:54,390
The Products table is connected to a little table that has just

140
00:08:54,391 --> 00:08:57,050
information about the Manufacturers.

141
00:08:57,051 --> 00:09:02,505
But the Locations table is not automatically connected to

142
00:09:02,506 --> 00:09:05,855
the Sales table, and this is because 

143
00:09:05,856 --> 00:09:09,725
the actual relationship exists only between

144
00:09:09,726 --> 00:09:12,465
the additional custom column that I created.

145
00:09:12,466 --> 00:09:14,345
Remember, they were called location.

146
00:09:14,346 --> 00:09:19,655
So now I can actually drag the connection here and

147
00:09:19,656 --> 00:09:23,620
create the relationship manually.

148
00:09:23,621 --> 00:09:25,920
Every time you need the relationship and

149
00:09:25,921 --> 00:09:28,880
the relationship is not recognized automatically,

150
00:09:28,881 --> 00:09:33,470
you can actually go here and just do this dragging.

151
00:09:33,471 --> 00:09:36,760
And create the relationship is pretty simple.

152
00:09:36,761 --> 00:09:40,340
So once I’ve done that, actually my model is ready.

153
00:09:40,341 --> 00:09:45,120
I can go to the Insert &gt; Pivot Table,

154
00:09:45,121 --> 00:09:46,970
like we did a few times before.

155
00:09:46,971 --> 00:09:51,480
I can close this tab for the workbook queries for now.

156
00:09:51,481 --> 00:09:57,240
And I can start using data coming from this.

157
00:09:57,241 --> 00:09:58,860
I have the Revenue.

158
00:09:58,861 --> 00:10:04,075
Again, from Manufacturer, I can see revenue by manufacturer and,

159
00:10:07,735 --> 00:10:13,330
From the Products I can.

160
00:10:13,331 --> 00:10:18,620
So I have the data already coming from this table,

161
00:10:18,621 --> 00:10:21,790
so this will conclude the first part of this module.

162
00:10:21,791 --> 00:10:27,170
In the next part we’re going to add another table that is actually

163
00:10:27,171 --> 00:10:32,840
required to do more sophisticated analysis, a calendar table.

164
00:10:32,841 --> 00:10:36,355
And also we’re going to bring data and

165
00:10:36,356 --> 00:10:40,460
mash it up with the data that we brought from SQL Server and

166
00:10:40,461 --> 00:10:43,240
we’re going to analyze all this data together.

167
00:10:43,241 --> 00:10:44,355
See you on the next part.

