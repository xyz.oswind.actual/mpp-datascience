0
00:00:00,870 --> 00:00:16,160
Date tables, in Excel data model, are essential for browsing and calculating data over time.

1
00:00:16,160 --> 00:00:21,600
In this video, I will show several options to create date tables in Excel data model.

2
00:00:21,600 --> 00:00:26,810
Let&#39;s assume that we have the following tables already imported to our data model. We have

3
00:00:26,810 --> 00:00:32,840
the Sales table that contains the sales transactions. We have Products table that contains the list

4
00:00:32,840 --> 00:00:38,570
of all products, Manufacturer table that contains the list of all Manufacturers, and Locations

5
00:00:38,570 --> 00:00:44,370
table that contains the list of all Locations. However, we yet to have a date table in the

6
00:00:44,370 --> 00:00:47,690
data model. We want to create a date table and link it

7
00:00:47,690 --> 00:00:53,210
to the Sales table, so that we can perform analysis on the sales data over time.

8
00:00:53,210 --> 00:01:00,390
In Excel 2016, creating a date table is very simple. The New Date table provides the functionality

9
00:01:00,390 --> 00:01:06,720
to do this in a single click. Excel automatically creates a new table named Calendar. It has

10
00:01:06,720 --> 00:01:11,300
several columns, but only one column that are not made of formulas, which is the Date

11
00:01:11,300 --> 00:01:16,030
column. It also creates a DateHierarchy by default.

12
00:01:16,030 --> 00:01:20,090
You can create a relationship between the newly created Calendar table and the Sales

13
00:01:20,090 --> 00:01:26,330
table, by linking the respective Date columns. Let�s zoom in to the Data View. As you can

14
00:01:26,330 --> 00:01:31,610
see here, the Date table is made of all the dates available in the Sales table. Excel

15
00:01:31,610 --> 00:01:36,110
automatically detects all the dates that are available in the Sales table, and create the

16
00:01:36,110 --> 00:01:42,060
Date column based on those dates. Excel also automatically add the rest of the columns.

17
00:01:42,060 --> 00:01:48,110
The rest of the columns are calculated columns, created using formulas based on the Date column.

18
00:01:48,110 --> 00:01:53,550
They are used to represent a piece of information of the date of interest. For example, the

19
00:01:53,550 --> 00:01:58,080
Year column derives the year from the date in the row. The Month column derives the month

20
00:01:58,080 --> 00:02:04,820
from the date in the row, and so on. If you are running earlier version of Excel,

21
00:02:04,820 --> 00:02:09,700
there are several ways you can add a date table to your Data Model. You can import a

22
00:02:09,699 --> 00:02:15,189
date table from a relational database, or other data source. You can create a date table

23
00:02:15,189 --> 00:02:20,480
in Excel and copy or link it to a new table in Excel data model. Or you can import from

24
00:02:20,480 --> 00:02:25,310
Microsoft Azure Marketplace. Creating a date table in Excel and copying

25
00:02:25,310 --> 00:02:29,739
it into a new table in the Data Model, is really quite easy to do, and it gives you

26
00:02:29,739 --> 00:02:35,329
a lot of flexibility. When you create a date table in Excel, you begin with a single column

27
00:02:35,329 --> 00:02:40,609
with a contiguous range of dates. You can then create additional columns such as Year,

28
00:02:40,609 --> 00:02:46,019
Quarter, Month, and so on, in the Excel worksheet by using Excel formulas, or you can create

29
00:02:46,019 --> 00:02:53,150
them as calculated columns in Excel data model. I will show you how to do the later.

30
00:02:53,150 --> 00:02:59,279
You start in a blank worksheet. Type Date as column header to identify a range of dates.

31
00:02:59,279 --> 00:03:06,639
Type a beginning date, for example 1/1/2013. This date should be the earliest date available

32
00:03:06,639 --> 00:03:11,870
in the Sales table, that you want to include in your analysis. Click the fill handle and

33
00:03:11,870 --> 00:03:25,650
drag it down to a row number that includes an ending date. For example, 12/31/2015.

34
00:03:25,650 --> 00:03:37,159
Copy all rows, including the header, and paste it into the Excel data model. Let�s name

35
00:03:37,159 --> 00:03:47,010
the table Calendar. Leave Use first row as column headers selected, and then click OK.

36
00:03:47,010 --> 00:03:52,299
You can also create a linked table by using Add to Data Model. However, this makes your

37
00:03:52,299 --> 00:03:57,219
workbook unnecessarily large because the workbook has two versions of the date table; one in

38
00:03:57,219 --> 00:04:03,889
Excel and one in the data model. You now have a date table in your Data Model.

39
00:04:03,889 --> 00:04:12,859
You can add new columns such as Year, Month, etc. by using DAX. For example, here I will

40
00:04:12,859 --> 00:04:25,830
create the calculated columns for YEAR using this DAX expression. You can do the same for

41
00:04:25,830 --> 00:04:45,340
MONTH, and so on. Once you have imported the date table into

42
00:04:45,340 --> 00:04:50,919
your data model, you need to link it to the Sales table. You do this by creating a relationship

43
00:04:50,919 --> 00:04:55,630
between them, on the Date column. You can also do this in the Diagram view.

