0
00:00:00,001 --> 00:00:04,744
[MUSIC]

1
00:00:04,745 --> 00:00:07,046
Hello everyone my name is Dany Hoter and

2
00:00:07,047 --> 00:00:11,110
I’m a Senior Program Manager in the Excel team in Microsoft.

3
00:00:11,111 --> 00:00:13,660
And this course is going to be about analyzing and

4
00:00:13,661 --> 00:00:15,130
visualizing data in Excel.

5
00:00:16,320 --> 00:00:18,910
Actually I’m gonna use Excel 2016.

6
00:00:18,911 --> 00:00:21,963
Originally it was supposed to be with Excel 2016, but

7
00:00:21,964 --> 00:00:23,970
we decided to just call it with Excel.

8
00:00:23,971 --> 00:00:28,120
Because you’ll be able to follow this not only with Excel 2016 but

9
00:00:28,121 --> 00:00:30,940
also with Excel 2013 and

10
00:00:30,941 --> 00:00:36,650
with a little bit more difficulty also with Excel 2010.

11
00:00:36,651 --> 00:00:41,130
Now, I’m gonna be covering areas of importing data into Excel,

12
00:00:41,131 --> 00:00:43,680
creating models of data in Excel and

13
00:00:43,681 --> 00:00:48,740
using new tools that were added to Excel in the last versions.

14
00:00:48,741 --> 00:00:51,170
Let’s go straight into our first module.

15
00:00:51,171 --> 00:00:53,720
And in this module, actually I’m going to show you

16
00:00:53,721 --> 00:00:56,040
things which are not necessarily new.

17
00:00:56,041 --> 00:00:59,340
I’m gonna show you data analysis using data in Excel,

18
00:00:59,341 --> 00:01:01,110
in the grid of Excel.

19
00:01:01,111 --> 00:01:04,500
And most of you will be, hopefully familiar to you but

20
00:01:04,501 --> 00:01:06,190
I’m going to cover it and

21
00:01:06,191 --> 00:01:10,100
then show the differences between this classic way of analyzing data

22
00:01:10,101 --> 00:01:13,710
and the new tools that I’m going to show you in for the modules.

23
00:01:13,711 --> 00:01:18,534
So let me open my first file here in Excel and

24
00:01:18,535 --> 00:01:23,495
let’s understand how people are doing data

25
00:01:23,496 --> 00:01:26,861
Analysis and Excel for years.

26
00:01:29,189 --> 00:01:34,196
All right so what do you see here is at least my take

27
00:01:34,197 --> 00:01:40,932
on a classic dashboard in Excel, it had three pivot charts.

28
00:01:40,933 --> 00:01:43,350
Two slicers, right here.

29
00:01:43,351 --> 00:01:44,830
Three pivot charts.

30
00:01:44,831 --> 00:01:47,340
And the slicers are connected to all the pivot charts, so

31
00:01:47,341 --> 00:01:52,620
if I click here on accessories, all the three charts respond.

32
00:01:52,621 --> 00:01:55,060
I can select one, I can select multiple.

33
00:01:55,061 --> 00:01:57,940
And the data for this,

34
00:01:57,941 --> 00:02:03,900
what you see here is actually in the, I can hide,

35
00:02:03,901 --> 00:02:09,710
I can unhide some of the data here.

36
00:02:09,711 --> 00:02:12,790
And you’ll see that actually all the data is right here in Excel.

37
00:02:12,791 --> 00:02:17,340
The data came into Excel, let me unhide also the others because there

38
00:02:17,341 --> 00:02:22,430
are multiple tables here that contain the data.

39
00:02:22,431 --> 00:02:27,510
And this is kind of typical that the data is contained in a few

40
00:02:27,511 --> 00:02:29,420
ranges in Excel.

41
00:02:29,421 --> 00:02:33,940
But the author who created this support needed to combine

42
00:02:33,941 --> 00:02:38,940
all the data into one consecutive range of data.

43
00:02:38,941 --> 00:02:42,340
Because pivot tables and pivot charts, like I was using here.

44
00:02:42,341 --> 00:02:46,160
Needs all the data to be in one range.

45
00:02:46,161 --> 00:02:51,760
So, the first columns here are going all the way from A to E.

46
00:02:51,761 --> 00:02:55,580
The original data that contains transactions.

47
00:02:57,230 --> 00:03:01,590
And then, all the columns coming from F to L

48
00:03:01,591 --> 00:03:04,790
are columns that are actually bringing data from the other

49
00:03:06,960 --> 00:03:11,120
tables into this table to make it one big table.

50
00:03:11,121 --> 00:03:16,510
And for that, the Excel user uses one

51
00:03:16,511 --> 00:03:20,900
of the oldest tools in Excel, the Vlookup function.

52
00:03:20,901 --> 00:03:23,860
So this is actually using two Vlookups,

53
00:03:23,861 --> 00:03:25,950
this is using another Vlookup.

54
00:03:25,951 --> 00:03:28,650
I will not go into the details of explaining to you how to

55
00:03:28,651 --> 00:03:33,000
use Vlookup, because it is not really the purpose of this course

56
00:03:33,001 --> 00:03:36,750
to teach you about Vlookup.

57
00:03:36,751 --> 00:03:42,530
But just as a quick reminder, Vlookup provides a key to another table.

58
00:03:42,531 --> 00:03:43,264
In this case,

59
00:03:43,265 --> 00:03:46,577
it’s the customer and then the table is the customer’s table.

60
00:03:46,578 --> 00:03:51,373
And then it tells you, in this case, to bring column number five and

61
00:03:51,374 --> 00:03:55,665
false tells Vlookup expression to look for an exact match and

62
00:03:55,666 --> 00:03:57,532
not just not exact match.

63
00:03:57,533 --> 00:04:00,251
Now it may seem a little different from Vlookup you

64
00:04:00,252 --> 00:04:03,249
saw before if you haven’t been using tables in Excel.

65
00:04:03,250 --> 00:04:08,270
In this example I’m using tables this is why it’s formatted

66
00:04:08,271 --> 00:04:13,585
this way and also this is why the expressions use column names and

67
00:04:13,586 --> 00:04:18,400
table names instead of a regular reference to Excel.

68
00:04:18,401 --> 00:04:22,180
If you don’t use tables I would recommend really to look at it.

69
00:04:22,181 --> 00:04:25,920
They’ve been around for quite some time in Excel, since 2007, and

70
00:04:25,921 --> 00:04:27,600
they are a great tool.

71
00:04:27,601 --> 00:04:32,490
And it makes for much more readable

72
00:04:32,491 --> 00:04:37,480
formulas and also easier to maintain formulas.

73
00:04:37,481 --> 00:04:41,146
Now let’s go back to our dashboard.

74
00:04:41,147 --> 00:04:47,135
So pretty much you see that I can get nice results.

75
00:04:47,136 --> 00:04:50,745
The dashboard at least to my standards, looks pretty nice.

76
00:04:50,746 --> 00:04:52,215
So what’s wrong in this picture and

77
00:04:52,216 --> 00:04:55,125
why did we need to introduce new tools into Excel in

78
00:04:55,126 --> 00:04:59,995
the latest versions, to go beyond what I was able to do here?

79
00:04:59,996 --> 00:05:01,723
So the challenges here are the following.

80
00:05:01,724 --> 00:05:06,642
First of all the need to combine all the data together with Vlookups.

81
00:05:06,643 --> 00:05:11,320
It has its flaws, its problems.

82
00:05:11,321 --> 00:05:15,170
First of all, Vlookups can get very slow.

83
00:05:15,171 --> 00:05:16,850
Let me give an example here.

84
00:05:16,851 --> 00:05:19,530
If for any reason I go to the customer’s table

85
00:05:19,531 --> 00:05:23,170
that I bring data from into the main table, the sales table, and

86
00:05:23,171 --> 00:05:28,580
I go here and I just change one cell, let’s say this guy.

87
00:05:28,581 --> 00:05:32,120
Marital status of this customer, change from single to married.

88
00:05:32,121 --> 00:05:34,430
Also means that this guy married.

89
00:05:34,431 --> 00:05:39,050
If you notice in the bottom, in the status line it says calculating and

90
00:05:39,051 --> 00:05:40,710
it has a progress bar.

91
00:05:40,711 --> 00:05:42,470
Going, going, going.

92
00:05:42,471 --> 00:05:44,170
Still going.

93
00:05:44,171 --> 00:05:47,900
And only now all the calculations were actually completed.

94
00:05:47,901 --> 00:05:52,880
The reason that it’s a simple operation of changing

95
00:05:52,881 --> 00:05:58,580
one cell took so long is because this range is used in thousands and

96
00:05:58,581 --> 00:06:03,120
thousands of Vlookup expressions, and all of them needed to be calculated.

97
00:06:03,121 --> 00:06:05,010
So, vlookups can be slow.

98
00:06:05,011 --> 00:06:08,480
And this is slow, it can be actually a lot slower.

99
00:06:08,481 --> 00:06:12,420
And now vlookups can also be difficult to maintain and

100
00:06:12,421 --> 00:06:13,810
can be error prone.

101
00:06:13,811 --> 00:06:16,840
Another problem is that the data that we use

102
00:06:18,810 --> 00:06:21,390
is not always as simple as this one.

103
00:06:21,391 --> 00:06:24,896
Here the only problem was to combine it to one range.

104
00:06:24,897 --> 00:06:29,480
But many times I need to do much more operations on the data to

105
00:06:29,481 --> 00:06:34,480
manipulate, to shape it, to make it ready to be used in analysis.

106
00:06:34,481 --> 00:06:38,150
And in order to do that the Excel user had some tools,

107
00:06:38,151 --> 00:06:40,620
you could use manual copy and

108
00:06:40,621 --> 00:06:44,680
paste operations, you can create some macros if you knew how to.

109
00:06:44,681 --> 00:06:49,517
You can use SQL to manipulate the data on the SQL side if you knew

110
00:06:49,518 --> 00:06:50,178
how to.

111
00:06:50,179 --> 00:06:54,276
But none of these solutions was really easy and

112
00:06:54,277 --> 00:06:56,911
accessible for Excel users.

113
00:06:56,912 --> 00:07:01,113
And if you did some manual operations you had to repeat them

114
00:07:01,114 --> 00:07:02,240
every period.

115
00:07:02,241 --> 00:07:07,070
Let’s say if the report is weekly, you had to repeat this every week.

116
00:07:07,071 --> 00:07:09,470
And this became really, really tedious,

117
00:07:09,471 --> 00:07:11,890
and people really did not like it.

118
00:07:11,891 --> 00:07:15,823
So manipulating and making the data ready to go is another thing.

119
00:07:15,824 --> 00:07:18,401
The third one is scale.

120
00:07:18,402 --> 00:07:22,320
The Excel grid is a maximum of a million rows and for

121
00:07:22,321 --> 00:07:24,760
many cases this is not enough.

122
00:07:24,761 --> 00:07:27,501
And even if you go to half a million and

123
00:07:27,502 --> 00:07:32,120
still have columns with Vlookup, it would be terribly slow.

124
00:07:32,121 --> 00:07:36,270
So being able to manipulate millions of rows

125
00:07:36,271 --> 00:07:39,140
is not something that a regular Excel user would

126
00:07:39,141 --> 00:07:42,550
be able to do with regular Excel like this.

127
00:07:42,551 --> 00:07:46,930
So in order to solve these issues and

128
00:07:46,931 --> 00:07:49,830
another one that I want to mention is that some data sources

129
00:07:49,831 --> 00:07:52,960
are not really accessible to native Excel.

130
00:07:52,961 --> 00:07:58,050
So some modern Data sources let’s say if data is coming from Facebook,

131
00:07:58,051 --> 00:08:01,475
from Salesforce from other OData,

132
00:08:03,225 --> 00:08:06,265
some of them are really not accessible to Excel and

133
00:08:06,266 --> 00:08:10,095
people want to use a large variety of data sources.

134
00:08:10,096 --> 00:08:12,255
To solve all these problems we actually

135
00:08:13,490 --> 00:08:17,660
since 2010 have been providing new tools to Excel to be able

136
00:08:18,850 --> 00:08:23,270
to first of all create pivot tables and pivot charts without needing to

137
00:08:23,271 --> 00:08:25,660
combine the data physically into one large range.

138
00:08:25,661 --> 00:08:30,020
So staying with multiple tables that you can use and

139
00:08:30,021 --> 00:08:31,680
create pivot tables directly from.

140
00:08:32,799 --> 00:08:37,300
Second, having a way to manipulate

141
00:08:37,301 --> 00:08:42,130
the data in very powerful ways and a repeatable way.

142
00:08:42,131 --> 00:08:44,160
You do it once you check the data.

143
00:08:44,159 --> 00:08:47,760
Next time you need to import the same data you just click on a button

144
00:08:47,761 --> 00:08:49,980
and it does everything for you.

145
00:08:49,981 --> 00:08:52,690
So this is the third this -  scale.

146
00:08:52,691 --> 00:08:55,060
Being able to get into Excel millions and

147
00:08:55,061 --> 00:08:58,360
millions of rows, even tens of millions if you need to,

148
00:08:58,361 --> 00:09:01,500
is another thing, and with very good performance.

149
00:09:01,501 --> 00:09:05,450
And the last one, which actually I haven’t mentioned as a deficiency

150
00:09:05,451 --> 00:09:10,495
is about creating logic, business logic.

151
00:09:10,496 --> 00:09:13,165
Here, I’m using just one example, if you see here,

152
00:09:13,166 --> 00:09:16,945
on this chart on the right side, there is a margin percent.

153
00:09:18,195 --> 00:09:21,740
It’s a dual axis chart on one side,

154
00:09:21,741 --> 00:09:24,430
it’s showing the margin percent and the other the revenue.

155
00:09:24,431 --> 00:09:27,670
The margin percent is calculated from the revenue and the cost

156
00:09:27,671 --> 00:09:31,360
with something called calculated field for the pivot table.

157
00:09:31,361 --> 00:09:33,630
I won’t even go and show to you exactly how to do it,

158
00:09:33,631 --> 00:09:36,370
it’s been there in Excel for many, many years.

159
00:09:36,371 --> 00:09:39,700
But using calculated fields is very limited.

160
00:09:39,701 --> 00:09:42,360
The level of sophistication for

161
00:09:42,361 --> 00:09:45,310
the business logic you can create is very limited.

162
00:09:45,311 --> 00:09:49,720
And the new tools that I’m going to show you will make it possible for

163
00:09:49,721 --> 00:09:55,190
you to create really, really sophisticated business logic and

164
00:09:55,191 --> 00:09:58,820
do the calculations in a language that is similar to Excel,

165
00:09:58,821 --> 00:10:01,090
but extends the capabilities of Excel.

166
00:10:01,091 --> 00:10:04,030
So this is what we are going to do

167
00:10:04,031 --> 00:10:07,280
in all the other modules coming from here.

168
00:10:07,281 --> 00:10:11,130
I will show you the new tools, the new data modeling Excel,

169
00:10:11,131 --> 00:10:14,950
the new language for calculations, the new tools for querying and

170
00:10:14,951 --> 00:10:16,390
shaping the data.

171
00:10:16,391 --> 00:10:20,930
So this is the end of this module and you

172
00:10:20,931 --> 00:10:26,250
should expect in the next modules to learn about this new tools that

173
00:10:26,251 --> 00:10:31,110
are available now for doing data analysis and visualization in Excel.

174
00:10:31,111 --> 00:10:32,675
Thank you and see you next time.

