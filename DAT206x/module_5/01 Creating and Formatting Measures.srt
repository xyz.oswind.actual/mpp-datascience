0
00:00:00,001 --> 00:00:04,365
[MUSIC]

1
00:00:04,366 --> 00:00:05,580
Welcome back.

2
00:00:05,581 --> 00:00:10,300
This is module number five in our series about data analysis and

3
00:00:10,301 --> 00:00:11,380
visualization in Excel.

4
00:00:11,381 --> 00:00:14,120
We’ll do this in two parts.

5
00:00:14,121 --> 00:00:18,910
And the entire module is about the DAX language.

6
00:00:18,911 --> 00:00:23,015
We already started in previous modules to see some of the basics of

7
00:00:23,016 --> 00:00:26,084
the DAX language, but we go deeper in this one.

8
00:00:26,085 --> 00:00:30,747
And I just want to tell you upfront that even the depth that we

9
00:00:30,748 --> 00:00:35,131
are going to cover in this one is not going to be enough for

10
00:00:35,132 --> 00:00:40,750
you to do real work unless you check other sources that we will provide.

11
00:00:40,751 --> 00:00:45,626
And maybe also we will record other modules specifically about

12
00:00:45,627 --> 00:00:47,520
the DAX language.

13
00:00:47,521 --> 00:00:51,850
But for now let’s start to see,

14
00:00:51,851 --> 00:00:56,830
I’m going to use the same model that I used in the previous module, and

15
00:00:56,831 --> 00:01:01,700
I am going to start by going right there into the manage window.

16
00:01:01,701 --> 00:01:07,660
Remember we have this sales table that doesn’t have the revenue or

17
00:01:07,661 --> 00:01:08,670
the cost calculated.

18
00:01:08,671 --> 00:01:14,610
So, I am going to again calculate it as a calculated column the revenue.

19
00:01:14,611 --> 00:01:19,710
So, I’m going to do equals quantity times list price.

20
00:01:20,720 --> 00:01:24,626
And rename this column to be revenue.

21
00:01:30,529 --> 00:01:32,413
Once I have such a column,

22
00:01:32,414 --> 00:01:36,782
remember this is a calculated column it uses a context we call

23
00:01:36,783 --> 00:01:41,950
the row context so it can use all the other columns in the same row.

24
00:01:41,951 --> 00:01:46,740
And it can also use other columns in other tables provided that there’s

25
00:01:46,741 --> 00:01:52,560
a relationship between this table on the many side and

26
00:01:52,561 --> 00:01:54,540
the other table on the one side.

27
00:01:54,541 --> 00:01:57,580
So, this is calculated columns.

28
00:01:57,581 --> 00:01:59,890
Based on regular columns or

29
00:01:59,891 --> 00:02:03,600
calculated columns, I can create measures.

30
00:02:03,601 --> 00:02:06,320
Measures, I can do it from here, or I can do it, and

31
00:02:06,321 --> 00:02:09,050
I’ll show you later, directly from the pivot table.

32
00:02:09,050 --> 00:02:14,540
So, once I have revenue, let’s say, I can create tote revenue column

33
00:02:14,541 --> 00:02:21,830
equals sum of revenue close parenthesis so

34
00:02:21,831 --> 00:02:28,400
now I have myself a measure that I would be able to use in the pivot.

35
00:02:28,401 --> 00:02:32,260
But I want to show you an alternative way to calculate

36
00:02:32,261 --> 00:02:34,260
actually the same number.

37
00:02:34,261 --> 00:02:39,020
And this is a way that will actually not even require me to

38
00:02:39,021 --> 00:02:42,230
add this calculated column to do the multiplication for

39
00:02:42,231 --> 00:02:43,930
Each row, because what we do here is,

40
00:02:43,931 --> 00:02:48,020
for each row I multiply the order quantity with the list price.

41
00:02:48,021 --> 00:02:49,690
But I can actually have a function,

42
00:02:49,691 --> 00:02:54,290
a measure that will do this multiply for each row

43
00:02:54,291 --> 00:02:58,396
in just one shot without requiring a special calculated column.

44
00:02:58,397 --> 00:03:03,202
So let’s do TotRevenue take 2 as a measure, column, equal, and

45
00:03:03,203 --> 00:03:06,804
now I will use instead of the sum function,

46
00:03:06,805 --> 00:03:11,909
the sum function is a simple function that’s very similar to

47
00:03:11,910 --> 00:03:17,860
the sum function Excel, just takes all the values and sums them.

48
00:03:17,861 --> 00:03:20,820
But now I’m going to use the sumx function.

49
00:03:20,821 --> 00:03:24,600
The sumx function is part of a family of functions in DAX that

50
00:03:24,601 --> 00:03:28,930
together with the averagex, countx, meanx, maxx, and so on,

51
00:03:28,931 --> 00:03:33,670
that their nature is that they expect

52
00:03:33,671 --> 00:03:37,890
a table as their first argument and once they get a table they start

53
00:03:37,891 --> 00:03:41,930
iterating on the rows of this table and for each table applying,

54
00:03:41,931 --> 00:03:45,260
an expression that is going to be the second argument, and

55
00:03:45,261 --> 00:03:48,920
then applying something to the totals of all these expressions.

56
00:03:48,921 --> 00:03:52,150
In this case sumx would just summarize the results of these

57
00:03:52,151 --> 00:03:53,250
expressions.

58
00:03:53,251 --> 00:03:55,586
Countx will count the results that are not null,

59
00:03:55,587 --> 00:03:57,750
averageX will average them and so on.

60
00:03:58,760 --> 00:04:03,980
So this is sumx, on the Internet sales table.

61
00:04:05,180 --> 00:04:07,350
And the second argument is, as I said, is an expression.

62
00:04:07,351 --> 00:04:11,740
And this expression is very similar to the one in which we just did on

63
00:04:12,800 --> 00:04:13,850
in the calculated column.

64
00:04:13,851 --> 00:04:16,560
Because it actually goes in each row and

65
00:04:16,560 --> 00:04:18,610
calculates exactly the same thing.

66
00:04:18,611 --> 00:04:21,038
The quantity times.

67
00:04:21,039 --> 00:04:26,338
The price list.

68
00:04:26,339 --> 00:04:28,617
Or list price, as they called it.

69
00:04:28,618 --> 00:04:30,580
Okay.

70
00:04:30,581 --> 00:04:32,790
I click enter now.

71
00:04:34,290 --> 00:04:35,380
And I will format it.

72
00:04:39,810 --> 00:04:44,129
And right here, you can see that these two

73
00:04:44,130 --> 00:04:48,338
are actually returning the same value.

74
00:04:48,339 --> 00:04:51,057
If I’m going back to Excel, and

75
00:04:51,058 --> 00:04:55,303
then create myself a pivot, and use both of them.

76
00:04:55,304 --> 00:04:58,124
You will see that there is actually no need to use both because they

77
00:04:58,125 --> 00:04:59,620
are exactly the same.

78
00:04:59,621 --> 00:05:03,670
Only one is using a calculated column and

79
00:05:03,671 --> 00:05:06,200
summarizing the results of the calculated column.

80
00:05:06,201 --> 00:05:07,860
While the other is

81
00:05:09,180 --> 00:05:13,870
doing this multiplication while it’s being calculated as a measure.

82
00:05:13,871 --> 00:05:16,390
So if I add to it any,

83
00:05:16,391 --> 00:05:20,600
let’s say country you see that this really just repeats itself.

84
00:05:20,601 --> 00:05:23,110
The total revenue, I misspelled total revenue,

85
00:05:23,111 --> 00:05:24,080
doesn’t matter.

86
00:05:24,081 --> 00:05:26,650
It is exactly as the second one,

87
00:05:26,651 --> 00:05:30,320
only calculated in two different ways.

88
00:05:30,321 --> 00:05:35,889
All right that’s one example of using slightly more complex,

89
00:05:35,890 --> 00:05:37,470
DAX expression.

90
00:05:38,730 --> 00:05:41,900
Let’s do the other measures right there from the Pivot.

91
00:05:41,901 --> 00:05:45,806
So once I am in the field list of the pivot table,

92
00:05:45,807 --> 00:05:50,737
first of all, I can see the measures right here with a special icon

93
00:05:50,738 --> 00:05:53,266
designating them as measures.

94
00:05:53,267 --> 00:05:54,640
This is new for Excel 2016.

95
00:05:54,641 --> 00:05:59,520
You will not see this icon in 2013 and also you will

96
00:05:59,521 --> 00:06:03,020
not be able to do what you can do here which is right click and

97
00:06:03,021 --> 00:06:04,950
edit or delete the measure.

98
00:06:04,951 --> 00:06:09,090
So manipulating the measure from the filters is new for

99
00:06:09,091 --> 00:06:10,980
Excel 2016.

100
00:06:10,981 --> 00:06:15,650
So what I can do is also, from here, from the table, I can add a measure.

101
00:06:17,250 --> 00:06:22,475
So let’s add a measure that is

102
00:06:22,476 --> 00:06:28,750
going to be RevenueAllCountries.

103
00:06:28,751 --> 00:06:30,362
The RevenueAllCountries.

104
00:06:30,363 --> 00:06:33,453
Its purpose is to give me the total for

105
00:06:33,454 --> 00:06:37,460
all countries regardless of the context.

106
00:06:37,461 --> 00:06:40,370
So if I use it for Australia it will give me all countries.

107
00:06:40,371 --> 00:06:43,460
If I use it for Canada it will give me all countries.

108
00:06:43,461 --> 00:06:44,410
Why do I need it?

109
00:06:44,411 --> 00:06:45,720
You’ll see in a moment.

110
00:06:45,721 --> 00:06:48,410
I will not use it directly in the pivot but

111
00:06:48,411 --> 00:06:51,640
I would actually use it in some further calculations.

112
00:06:51,641 --> 00:06:54,710
So in order to do that I will use I will introduce for

113
00:06:54,711 --> 00:06:57,750
the first time a function which is maybe

114
00:06:57,751 --> 00:07:01,180
the mother of all functions in DAX called calculate.

115
00:07:01,181 --> 00:07:02,930
It’s very useful.

116
00:07:02,931 --> 00:07:06,450
It can start very easing gradual, but

117
00:07:06,451 --> 00:07:11,840
it can actually become in some advanced usages it’s not an easy

118
00:07:11,841 --> 00:07:16,760
to master so we’ll recommend really to start using it gradually and

119
00:07:16,761 --> 00:07:21,600
then learn more about it from the resources we’re going to provide.

120
00:07:21,601 --> 00:07:24,344
So the calculate as you can see expect two parameters,

121
00:07:24,345 --> 00:07:25,820
the first is an expression.

122
00:07:25,821 --> 00:07:28,950
So an expression, I can give it an expression

123
00:07:28,951 --> 00:07:32,120
that I already calculated, the total revenue.

124
00:07:32,121 --> 00:07:34,570
Either one, it doesn’t matter which one of the two.

125
00:07:34,571 --> 00:07:38,470
I have two options to actually calculate the same, as we saw.

126
00:07:38,471 --> 00:07:44,480
Now, from this position on, it accepts a series of filters and

127
00:07:44,481 --> 00:07:49,870
this filters can apply changes to the context.

128
00:07:49,871 --> 00:07:53,000
So, as I said in this example,

129
00:07:53,001 --> 00:07:56,900
I want to actually calculate the sales for all countries, regardless

130
00:07:56,901 --> 00:08:02,910
of which country is currently being filtered by the context.

131
00:08:02,911 --> 00:08:06,870
So the way to do that is to create all.

132
00:08:06,871 --> 00:08:08,900
Use the function that’s called all.

133
00:08:08,901 --> 00:08:13,350
We’ll just say, forget about what’s in the context, for

134
00:08:13,351 --> 00:08:15,030
this specific column of country.

135
00:08:16,740 --> 00:08:22,880
So, calculate total revenue based on all countries.

136
00:08:22,881 --> 00:08:26,560
I can check this formula for syntax and it’s okay.

137
00:08:26,561 --> 00:08:33,550
I can also give it a format from here: currency with two decimals.

138
00:08:33,551 --> 00:08:35,920
And now I have the new one.

139
00:08:35,921 --> 00:08:39,640
Now I will add it to the pivot, just to understand it.

140
00:08:39,640 --> 00:08:42,230
Not because really it’s very useful to use in the pivot.

141
00:08:42,231 --> 00:08:45,032
Because it’s purpose is to not be used directly in the pivot, but

142
00:08:45,033 --> 00:08:47,890
rather to be used in another calculation that will

143
00:08:47,891 --> 00:08:48,960
come in a moment.

144
00:08:48,961 --> 00:08:49,900
But let’s save it for now.

145
00:08:51,320 --> 00:08:52,610
When I add it to the pivot, and

146
00:08:52,611 --> 00:08:55,940
let’s just take one of the two identical ones

147
00:08:55,941 --> 00:08:57,860
out because they don’t add anything.

148
00:08:57,861 --> 00:09:01,620
So when I add it to the pivot you see is that it doesn’t matter what is

149
00:09:01,621 --> 00:09:04,160
in the row for country.

150
00:09:04,161 --> 00:09:08,480
This measure always returns the total for all countries.

151
00:09:08,481 --> 00:09:10,990
Now it does respond to other filters.

152
00:09:10,991 --> 00:09:13,500
It’s not that it’s oblivious to any filter.

153
00:09:13,501 --> 00:09:18,060
So if I for example put the year.

154
00:09:18,061 --> 00:09:20,180
Like this.

155
00:09:20,181 --> 00:09:22,420
You see that the revenue for

156
00:09:22,421 --> 00:09:27,985
all countries is showing a different value for each year.

157
00:09:27,986 --> 00:09:31,185
So, it does respond to other filters.

158
00:09:31,186 --> 00:09:35,715
It just is actually does not respond to the change between Australia,

159
00:09:35,716 --> 00:09:37,275
Canada, and so on, between the countries.

160
00:09:37,276 --> 00:09:40,335
It always returns the total of all countries.

161
00:09:40,336 --> 00:09:41,835
Let’s take it out from the pivot,

162
00:09:41,836 --> 00:09:45,805
because as I said, we will need it for another calculation.

163
00:09:46,885 --> 00:09:48,495
And not directly.

164
00:09:48,496 --> 00:09:52,040
So, from here, I will go and add another measure.

165
00:09:52,041 --> 00:09:57,479
And this one is going to be called,

166
00:09:57,480 --> 00:10:02,530
percent from all countries.

167
00:10:02,531 --> 00:10:09,145
And here I will say that I want to use the total revenue.

168
00:10:09,146 --> 00:10:14,320
The total revenue, this one right here.

169
00:10:14,321 --> 00:10:19,120
Divided by the one that we just calculated.

170
00:10:19,121 --> 00:10:20,610
It’s called revenue for all country.

171
00:10:21,660 --> 00:10:24,210
So we do one divided by the other.

172
00:10:24,211 --> 00:10:26,690
Actually, instead of using the operator for

173
00:10:26,691 --> 00:10:31,410
divide it’s better to use a function in DAX called divide.

174
00:10:31,411 --> 00:10:34,340
The reason it is better, because this is a safe divide.

175
00:10:34,341 --> 00:10:37,930
If the denominator is zero, it will return blank and

176
00:10:37,931 --> 00:10:41,400
not give us any weird results.

177
00:10:41,401 --> 00:10:44,290
So, I can check the formula.

178
00:10:44,291 --> 00:10:48,727
It’s okay. And now, if I add this one to

179
00:10:48,728 --> 00:10:55,729
the I

180
00:10:55,730 --> 00:10:59,880
forgot to actually format it.

181
00:10:59,881 --> 00:11:03,926
So let’s say that this is a number and it’s actually a percent.

182
00:11:03,927 --> 00:11:08,050
And have two digits, all right.

183
00:11:08,051 --> 00:11:13,310
So now we see for each country, how the percent for

184
00:11:13,311 --> 00:11:15,380
this country is from the total.

185
00:11:15,381 --> 00:11:21,720
We divided the actual revenue, which is calculated by the context

186
00:11:21,721 --> 00:11:24,800
which include the country with a measure that ignores the country.

187
00:11:24,801 --> 00:11:28,850
So it actually, for the grand total, it gives us 100% and

188
00:11:28,851 --> 00:11:33,390
it’s true that you can actually do something like that in the context

189
00:11:33,391 --> 00:11:37,590
of the pivot to show as the percent from the total but

190
00:11:37,591 --> 00:11:43,550
by doing this with  DAX formulas, we can get far beyond

191
00:11:43,551 --> 00:11:46,550
what we can do actually by using just the features of the pivot.

192
00:11:47,570 --> 00:11:52,660
And you will see later on examples in which the calculate function will

193
00:11:52,661 --> 00:11:55,960
become more and more complex and do more and more sophisticated stuff.

194
00:11:55,961 --> 00:12:01,134
So, in this part we actually covered the beginning of the calculate

195
00:12:01,135 --> 00:12:06,578
function, we saw the use of the sumx function, as a representative for

196
00:12:06,579 --> 00:12:11,479
the family of the x functions, with, together with and

197
00:12:11,480 --> 00:12:16,560
so on and we started to feel the power of the DAX language,

198
00:12:16,561 --> 00:12:18,518
more in the second part.

