Which Excel Version Should I Use?
================

## System Requirements

In order to get the most out of this course, you need to have the
following system requirements:

  - Windows operating system: Windows 7 or later
  - Microsoft Excel on Windows operating system:
      - Microsoft Excel 2016 Professional Plus or standalone edition
      - Microsoft Excel 2013 Professional Plus or standalone edition
      - Microsoft Excel 2010
      - Other versions of Microsoft Excel are not supported.

This course is created using Excel 2016 Professional Plus edition. If
you are using supported editions listed above, you should be able to
complete the labs in this course, although if you are not using Excel
2016, there may be subtle interface differences and you might need to
perform a little workaround. If you are using Excel 2010, we provide
separate lab files for you, so please pay attention in the lab section.

If your Excel installation is not listed in the above list, you can
follow the lectures and attempt the quizzes with just your browser, but
to work on the labs you need to have a supported version /edition of
Microsoft Excel on a Windows machine.

### What to do if?

  - I have Excel 2016 Professional Plus or standalone edition: Great\!
    You can move on to the next section.
  - I have Excel 2013 Professional Plus or standalone edition: Review
    the notes for using supported Excel 2013 for this course.
  - I have Excel 2010: Review the notes for using Excel 2010 for this
    course.
  - I do not have a supported edition of Excel installed: I’d suggest to
    install the trial version of Office 2016 Professional Plus.
  - I have Excel on a Mac: At this time, Neither Excel on Mac nor Excel
    Online has all the features required to complete this course. Some
    of the previous students have suggested to use parallels.

### Using Excel 2013 Professional Plus or StandAlone Edition

1.  You need to have the Professional Plus (Pro Plus) edition of Office
    2013.
2.  Download and install the [Power Query
    add-in](https://www.microsoft.com/en-us/download/details.aspx?id=39379).
3.  Enable Power Pivot by going to File -\> Options -\> Add-ins -\>
    Manage com-Add-ins and select power pivot.

**NOTE:** If you could not get Power Pivot to load in Excel 2013 Pro
Plus and the add-in was not showing in the COM addins, try installing
the add-in using the following: -\> Control Panel -\> Programs and
Features -\> selecting Microsoft Office Pro Plus 2013 -\> selecting
Change -\> Add or Remove Features -\> Continue -\> click the + next to
Microsoft Excel + Add-ins -\> drop down next to PowerPivot and selected
Run from My Computer -\> Continue

Difference with Excel 2016 Professional Plus or Standalone edition:

  - Access the Data Model from the Power Pivot tab and then Manage Data
    Model (there is no Manage Data Model icon in the Data tab)
  - Notice that the query features that are shown as part of the get and
    transform area in the data tab are all available in the Power Query
    tab in the ribbon.
  - Inserting new pivot tables/pivot charts is easier from the Power
    Pivot add-in than from Excel.
  - Measures can be created/modified from the Power Pivot manage window
    or from the Measures/Manage Measures tool in the Power Pivot tab.
  - Search is not available in the field list of a pivot.
  - Creating calendar tables is not available out of the box. You need
    to create a calendar table in Excel and add it to the data mode. The
    table need to have at least one column that is a true date and all
    the rest of the columns are optional.

### Using Excel 2010

1.  Download and install the [Power Query
    add-in](https://www.microsoft.com/en-us/download/details.aspx?id=39379).
    Remember to download the version according to your Excel (32 or 64
    bit).
2.  Download and install the [Power
    Pivot](https://www.microsoft.com/en-us/download/details.aspx?id=29074).
    Remember to download the version according to your Excel (32 or 64
    bit).
3.  We have created numerous workarounds for using Excel 2010 in
    selected exercises and labs - we recommend you review all units of a
    lesson or lab before you begin working to see if there are any
    special instructions.

Difference with Excel 2016 Professional Plus or Standalone edition:

  - Access the Data Model from the Power Pivot tab and then Power Pivot
    Window (there is no Manage Data Model icon in the Data tab and the
    icon to access the Data Model is not called Manage Data Model)
  - Notice that the query features that are shown as part of the get and
    transform area in the data tab are all available in the Power Query
    tab in the ribbon.
  - In Excel 2010, Power Query can only bring data to Excel tables and
    from there you can add them to the data model. Limit the size of
    data to beyond 1 million rows by filtering it to a smaller year
    span.
  - Inserting new power pivot based pivot tables/pivot charts can only
    be done from the Power Pivot add-in.
  - The field list of a power pivot based model have special areas for
    creating slicers.
  - Measures can be created/modified from the Power Pivot manage window
    or from the Measures/Manage Measures tool in the Power Pivot tab.
  - Search is not available in the field list of a pivot.
  - Creating calendar tables is not available out of the box. You need
    to create a calendar table in Excel and add it to the data mode. The
    table need to have at least one column that is a true date and all
    the rest of the columns are optional.
  - Not all Power Query features available in 2013 or 2016 are available
    in the add-in for 2010.

Power Query and Power Pivot in Excel 2010:

In Excel 2010, you cannot bring the data directly to Power Pivot using
Power Query. When you import data using Power Query, you can either
import to Excel worksheet or only create a connection. And then, from
the Power Pivot ribbon you can use Create Linked Table to add the table
to the Power Pivot model. This method limits you to a bit more than 1
million rows in a table. In addition, this will make the size of the
file larger than the equivalent in 2013 / 2016 because the data is
stored both inside and outside the model.

You can bring data directly to Power Pivot using Power Pivot import data
capabilities. Therefore, you can import millions of rows in the same way
as in 2013 or 2016. However, you cannot use the data transformation
features that comes with Power Query.

In this course we exclusively use Power Query for importing data so we
have this limitation. However, for the purpose of the labs, you will be
given an Excel file that already has the data in the data model.
Therefore, you can still complete the labs using Excel 2010.

Pivot Tables, Pivot Charts and Slicers:

  - Inserting new pivot tables and pivot charts which are based on the
    model can be done only from the Power Pivot window or the Power
    Pivot ribbon.
  - Slicers can be created from the field list by dragging columns to
    two special buckets called Slicer vertical and slicers horizontal.
  - When you create a pivot chart from Power Pivot, you’ll see a new
    sheet created with the pivot table behind the pivot chart. In 2010
    you can’t have a pivot chart without a pivot table.
  - You can always hide the sheets that contain the pivot table.

One last tidbit, every time you change something in the model you’ll get
a prompt in Excel to click for refresh.
