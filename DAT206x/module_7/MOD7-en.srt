0
00:00:00,001 --> 00:00:04,425
[MUSIC]

1
00:00:04,426 --> 00:00:05,970
Hello and welcome back.

2
00:00:05,971 --> 00:00:10,811
We are at module seven and in this module we will use

3
00:00:10,812 --> 00:00:15,901
the same model that I used in the previous examples.

4
00:00:15,902 --> 00:00:20,209
But this time we will concentrate on doing the actual visualizations,

5
00:00:20,210 --> 00:00:23,820
adding pivot charts, adding slicers, connecting both.

6
00:00:23,821 --> 00:00:28,900
Go a little more into cube functions which I mentioned but

7
00:00:28,901 --> 00:00:31,020
I didn’t show you the details.

8
00:00:31,021 --> 00:00:34,210
We were using them so going to use those also.

9
00:00:34,211 --> 00:00:38,700
So, using the actual model for creating the visualization.

10
00:00:39,790 --> 00:00:41,780
So, we are starting with Excel.

11
00:00:42,862 --> 00:00:46,230
We have one of the examples I had before, but

12
00:00:46,231 --> 00:00:49,061
actually I’m not going to touch it.

13
00:00:49,062 --> 00:00:53,453
I’m going to open a new worksheet here and start adding stuff to it and

14
00:00:53,454 --> 00:00:56,440
building on the model which is already here.

15
00:00:58,010 --> 00:01:02,630
So first I would like to build some kind of a dashboard.

16
00:01:02,631 --> 00:01:07,060
So, that will probably be built off Pivot Charts.

17
00:01:07,061 --> 00:01:11,914
Now a pivot charts can be standalone, this is not new for 2016

18
00:01:11,915 --> 00:01:16,855
but it was added  in the previous version of Excel, the fact that you

19
00:01:16,856 --> 00:01:21,288
can add a pivot chart without adding the pivot table first.

20
00:01:21,289 --> 00:01:25,917
So I can use Insert Pivot Chart, the default,

21
00:01:25,918 --> 00:01:29,017
again, is to use the workbook Data Model.

22
00:01:29,018 --> 00:01:33,220
So I’m happy with the default and I click OK.

23
00:01:33,221 --> 00:01:36,188
So I got this chart, it’s pretty big.

24
00:01:36,189 --> 00:01:40,260
So maybe because I want to show a few of them, I’ll make it smaller.

25
00:01:40,261 --> 00:01:42,520
One good thing is actually, before we continue,

26
00:01:42,521 --> 00:01:47,450
it’s always a good idea in creating these kind of visualizations, to

27
00:01:47,451 --> 00:01:51,150
turn off a few Excel features like grid lines, the most important one.

28
00:01:51,151 --> 00:01:55,650
We really don’t need the grid lines as we’re not doing any, or

29
00:01:55,651 --> 00:01:58,010
almost any Excel references and so on.

30
00:01:58,011 --> 00:02:01,330
So, here’s the Pivot Chart and the Pivot Chart has the field list.

31
00:02:01,331 --> 00:02:06,815
Now I cleaned the field list by hiding some tables,

32
00:02:06,816 --> 00:02:12,325
and for example, you don’t see here the main sales table.

33
00:02:12,326 --> 00:02:16,785
Actually you see it only as the measures in it, and

34
00:02:16,786 --> 00:02:20,380
it shows as sales with a sigma in front.

35
00:02:20,381 --> 00:02:26,587
So what I did is I’ve hidden all the columns and

36
00:02:26,588 --> 00:02:28,517
I’m left only with the measures.

37
00:02:28,518 --> 00:02:33,247
So, this is the way it actually looks and it’s much easier to work.

38
00:02:33,248 --> 00:02:38,037
And when I’m giving this to a person to build their own visualizations,

39
00:02:38,038 --> 00:02:41,840
I’m trying to minimize the amount of things that they will see.

40
00:02:41,841 --> 00:02:45,280
if they don’t need all the tables, I will hide some of the tables.

41
00:02:45,281 --> 00:02:48,100
If they don’t need columns, I will hide those and so on.

42
00:02:48,101 --> 00:02:51,410
So, I can start selecting.

43
00:02:51,411 --> 00:02:57,210
Let’s say I want to see our sales and I want to see them by segment.

44
00:02:59,950 --> 00:03:03,700
So, here it is, most of our sales are just in a few segments.

45
00:03:03,701 --> 00:03:07,120
And let’s build another one, another Pivot Chart.

46
00:03:07,121 --> 00:03:10,173
I have to go out of the existing one.

47
00:03:10,174 --> 00:03:14,182
 here, Insert /

48
00:03:14,183 --> 00:03:20,340
Pivot Chart once again, OK.

49
00:03:20,341 --> 00:03:25,302
Again, make it a little smaller and put it somewhere here,

50
00:03:25,303 --> 00:03:27,890
maybe making it the same size.

51
00:03:27,891 --> 00:03:32,600
And maybe now I want to see something else.

52
00:03:32,601 --> 00:03:38,180
I want to see the revenue for all and not just for our sales, and

53
00:03:38,181 --> 00:03:43,342
this time I want to break it by the manufacturers.

54
00:03:43,343 --> 00:03:47,790
Actually I added the manufacturers to the product table so

55
00:03:47,791 --> 00:03:49,520
I can choose it from here.

56
00:03:49,521 --> 00:03:55,003
So I’m not really concerned now about making it really nice

57
00:03:55,004 --> 00:04:00,605
looking and playing with all the properties and the design.

58
00:04:00,606 --> 00:04:04,090
You can go and select styles and I like, personally,

59
00:04:04,091 --> 00:04:07,460
the black one but you might like something else.

60
00:04:07,461 --> 00:04:09,550
There’s a lot of options to make this prettier but

61
00:04:09,551 --> 00:04:12,920
I want to concentrate more on the functionality.

62
00:04:12,921 --> 00:04:16,063
Now probably what I want is also to add a bunch of slicers so

63
00:04:16,064 --> 00:04:19,206
that the person who was using this dashboard can actually

64
00:04:19,207 --> 00:04:21,646
control it and slice on different things.

65
00:04:21,647 --> 00:04:24,485
So I can go again to the fit list, and

66
00:04:24,486 --> 00:04:27,790
let’s say I want a slicer by year.

67
00:04:27,791 --> 00:04:32,264
So I’m opening the calendar, in the calendar I have

68
00:04:32,265 --> 00:04:37,151
some hierarchy that I will explain in a moment what it is.

69
00:04:37,152 --> 00:04:40,424
But for the moment I will just pick from the other fields which are not

70
00:04:40,425 --> 00:04:42,000
part of the hierarchy.

71
00:04:42,001 --> 00:04:45,740
I will pick the year and add it as a slicer.

72
00:04:45,741 --> 00:04:49,730
So, here it is as a slicer.

73
00:04:49,731 --> 00:04:55,150
You see that the calendar table has more years in it

74
00:04:55,151 --> 00:04:57,470
than actually there’s data.

75
00:04:57,471 --> 00:05:02,420
So there’s data only from 2011 to 2015, so

76
00:05:02,421 --> 00:05:06,020
the years which don’t have any data are shown here in the bottom,

77
00:05:06,021 --> 00:05:08,210
I can also make them disappear if I want to.

78
00:05:08,211 --> 00:05:13,020
So now when I click on it, it does affect the chart, but

79
00:05:13,021 --> 00:05:15,090
it affects only one chart and not the other.

80
00:05:15,091 --> 00:05:17,690
The reason is that I started the slicer

81
00:05:17,691 --> 00:05:19,890
from the context of this specific chart.

82
00:05:19,891 --> 00:05:25,140
If we want to make this slicer connect to the other chart,

83
00:05:25,141 --> 00:05:31,040
I will have to go either through the options for the slicer,

84
00:05:31,041 --> 00:05:37,200
go to Report Connections, and check the other chart

85
00:05:37,201 --> 00:05:41,880
in my worksheet which is currently named Sheet3.

86
00:05:41,881 --> 00:05:46,334
It’s actually a good idea to go and name the charts and the pivots and

87
00:05:46,335 --> 00:05:49,384
give them meaningful names so it will be easier

88
00:05:49,385 --> 00:05:54,017
to know what slides are connected to which pivot table or pivot chart.

89
00:05:54,018 --> 00:05:59,012
Now that I did that, when I click on years, you probably

90
00:05:59,013 --> 00:06:04,556
notice that both charts respond to my clicking on the slicer.

91
00:06:04,557 --> 00:06:06,714
And I might have more slicers, and

92
00:06:06,715 --> 00:06:10,138
some of the slicers might affect all pivot charts, and

93
00:06:10,139 --> 00:06:15,060
some of the slicers may affect just a subset of the pivot charts.

94
00:06:15,061 --> 00:06:18,574
Slicers can also be connected to elements like pivot charts and

95
00:06:18,575 --> 00:06:22,222
pivot tables, which actually reside even on other worksheets,

96
00:06:22,223 --> 00:06:23,760
not necessarily this one.

97
00:06:23,761 --> 00:06:27,906
So it could be a whole network of elements like pivot tables,

98
00:06:27,907 --> 00:06:32,210
pivot charts and slicers in different worksheets.

99
00:06:32,211 --> 00:06:36,350
I mentioned before, hierarchies, let’s take a look at that.

100
00:06:36,351 --> 00:06:38,550
In order to see the definition of a hierarchy and

101
00:06:38,551 --> 00:06:42,330
explain a little bit what it is, I will go and

102
00:06:42,331 --> 00:06:47,510
look at the Manage window and switch to the diagram view.

103
00:06:47,511 --> 00:06:50,833
If you remember, the diagram view is a view in which

104
00:06:50,834 --> 00:06:55,255
I see the relationships between the tables and I see all the tables.

105
00:06:55,256 --> 00:07:00,034
And also, it’s the location in which I can actually see, or define,

106
00:07:00,035 --> 00:07:01,640
new hierarchies.

107
00:07:01,641 --> 00:07:05,081
So for example, the calendar table that I created through the automatic

108
00:07:05,082 --> 00:07:10,950
tool in Power Pivot already have a hierarchy, name, date hierarchy.

109
00:07:10,951 --> 00:07:16,110
So hierarchy is a collection of fields, here it’s year, month,

110
00:07:16,111 --> 00:07:21,390
and date, that they actually naturally go together.

111
00:07:21,391 --> 00:07:25,440
So usually you go and you drill from year to month to date.

112
00:07:25,441 --> 00:07:27,160
So if you define those three together,

113
00:07:27,161 --> 00:07:30,820
then when you move them to the pivot they go together.

114
00:07:30,821 --> 00:07:34,580
And then it’s easier to go and start by clicking on the plus sign,

115
00:07:34,581 --> 00:07:37,490
drilling from year to month, from month to date and so on.

116
00:07:37,491 --> 00:07:38,480
And as you saw before,

117
00:07:38,481 --> 00:07:42,140
we also have drilling experience on the pivot charts.

118
00:07:42,141 --> 00:07:44,450
You can create your own hierarchies,

119
00:07:44,451 --> 00:07:47,850
sometimes they are natural like here.

120
00:07:47,851 --> 00:07:52,430
I also have on the locations table,

121
00:07:52,431 --> 00:07:56,550
I have a hierarchy called GEO which is country, state, and city.

122
00:07:56,551 --> 00:08:01,690
In theory I could actually create any arbitrary hierarchy and not

123
00:08:01,691 --> 00:08:07,320
just those natural ones which are like geographic one and the time.

124
00:08:07,321 --> 00:08:09,440
Because there’s an advantage,

125
00:08:09,441 --> 00:08:15,300
if there’s a specific way to drill - a drill path that is repeating itself,

126
00:08:15,301 --> 00:08:19,399
it may be a good idea to add it as a hierarchy to one of the tables.

127
00:08:20,690 --> 00:08:25,546
Let’s go back to Excel, and now I want to explain

128
00:08:25,547 --> 00:08:30,046
a little bit about the other option to show and

129
00:08:30,047 --> 00:08:36,576
to visualize data which is not pivot tables and not pivot charts.

130
00:08:36,577 --> 00:08:40,190
And those are the cube functions.

131
00:08:40,191 --> 00:08:43,380
So, if you’ve seen before

132
00:08:43,381 --> 00:08:48,000
someone showing cube functions probably what they did is this way.

133
00:08:48,001 --> 00:08:49,880
They created a pivot table, so

134
00:08:49,881 --> 00:08:54,610
let me create a new pivot table right in the bottom here,

135
00:08:54,611 --> 00:08:59,375
and let me choose Measure.

136
00:08:59,376 --> 00:09:03,196
Maybe this, and something on,

137
00:09:03,197 --> 00:09:07,733
let’s say, drag the Geo hierarchy.

138
00:09:07,734 --> 00:09:11,869
So you notice when I drag the Geo to hierarchy,

139
00:09:11,870 --> 00:09:16,431
I actually can now immediately start drilling from

140
00:09:16,432 --> 00:09:21,220
countries to state and to cities and okay I have this.

141
00:09:21,221 --> 00:09:23,270
Now let’s have something on filter.

142
00:09:25,230 --> 00:09:26,897
Or actually, you know what,

143
00:09:26,898 --> 00:09:30,173
let’s connect this pivot to the existing slicer by year.

144
00:09:30,174 --> 00:09:34,397
To do that, I can go either to the options as I saw before,

145
00:09:34,398 --> 00:09:37,775
of the slicer and connect it to the new pivot.

146
00:09:37,776 --> 00:09:43,100
I can also go to the pivot, and from the special UI for

147
00:09:43,101 --> 00:09:49,273
the pivot I can go to something called Filter Connections and

148
00:09:49,274 --> 00:09:55,465
say that I want to connect this pivot to a slicer called Year.

149
00:09:55,466 --> 00:10:00,132
That I have a few of them because I have also others in other worksheets

150
00:10:00,133 --> 00:10:04,008
but this one is the one that I am looking at in the Sheet3.

151
00:10:04,009 --> 00:10:10,980
When I click OK, now the pivot is connected also to this.

152
00:10:10,981 --> 00:10:14,984
So once I have this pivot, I can actually go, and

153
00:10:14,985 --> 00:10:20,552
again from the analyze area in the ribbon, I can go somewhere called

154
00:10:20,553 --> 00:10:25,748
OLAP Tools and I can use something called Convert to Formulas.

155
00:10:25,749 --> 00:10:27,057
When I did that,

156
00:10:27,058 --> 00:10:32,495
actually the area in which the pivot used to be looks almost the same.

157
00:10:32,496 --> 00:10:36,920
Only that actually I lost the drilling capability, but every one

158
00:10:36,921 --> 00:10:41,250
of the cells is now actually as you see in the formula bar.

159
00:10:41,251 --> 00:10:45,986
It is a formula, there is no pivot anymore.

160
00:10:45,987 --> 00:10:50,015
And the formula is using each one of the sales used here,

161
00:10:50,016 --> 00:10:52,600
one of the cube functions.

162
00:10:52,601 --> 00:10:57,050
There are seven of them and now I will show you, so

163
00:10:57,051 --> 00:11:03,500
I was able to convert the pivot into a bunch of formulas.

164
00:11:03,501 --> 00:11:07,330
Notice that the formulas still are calculated when I click on

165
00:11:07,331 --> 00:11:07,970
the slicer.

166
00:11:07,971 --> 00:11:12,290
So, they actually reference the slicer and

167
00:11:12,291 --> 00:11:14,830
I can continue to control them with the slicer.

168
00:11:14,831 --> 00:11:17,370
Now there are seven cube functions.

169
00:11:17,371 --> 00:11:21,580
I don’t have really the time to go into detail and

170
00:11:21,581 --> 00:11:25,500
I will provide some reference to material that you can learn more

171
00:11:25,501 --> 00:11:30,650
about the cube functions, but let me do just a simple example.

172
00:11:30,651 --> 00:11:35,300
So, one of the functions that is not used in the automatic

173
00:11:35,301 --> 00:11:37,140
conversion is this one, the cube set.

174
00:11:38,780 --> 00:11:42,170
And now when I use the cube functions the first

175
00:11:42,171 --> 00:11:44,170
argument is always the connection name.

176
00:11:44,171 --> 00:11:48,340
And you can just click on the double quote and

177
00:11:48,341 --> 00:11:51,150
it will come and it’s completed with tab.

178
00:11:51,151 --> 00:11:55,693
Double quote again and now another double quote and

179
00:11:55,694 --> 00:11:59,081
it lists the elements in the model

180
00:11:59,082 --> 00:12:02,052
And now I can say, for example,

181
00:12:02,053 --> 00:12:05,907
that I want to go the product dimension and

182
00:12:05,908 --> 00:12:10,970
I want to get manufacturer and all manufacturers.

183
00:12:10,971 --> 00:12:14,340
And now actually I’m writing something that is not,

184
00:12:14,341 --> 00:12:18,130
I added something manually which is the function .children.

185
00:12:18,131 --> 00:12:20,910
This function is actually a MDX function.

186
00:12:20,911 --> 00:12:24,350
MDX is the language that we use to connect to OLAP cubes.

187
00:12:24,351 --> 00:12:28,628
Those functions were created back in 2007 to be used exclusively with

188
00:12:28,629 --> 00:12:29,450
OLAP cubes.

189
00:12:29,451 --> 00:12:33,790
It’s another technology that is a predecessor of the model technology

190
00:12:33,791 --> 00:12:35,570
that we saw in Excel.

191
00:12:35,571 --> 00:12:39,260
But now we can actually reuse it against the model that we

192
00:12:39,261 --> 00:12:40,330
have in Excel.

193
00:12:40,331 --> 00:12:44,260
So, if you want to do a little bit of more advanced stuff and

194
00:12:44,261 --> 00:12:49,150
not just convert pivot tables to functions,

195
00:12:49,151 --> 00:12:53,391
you have to know a little bit of this MDX language.

196
00:12:53,392 --> 00:12:58,554
So, now I can give it a name and I can call it Manufacturers.

197
00:13:03,333 --> 00:13:05,426
And now once I have this,

198
00:13:05,427 --> 00:13:10,428
actually in this cell I have the list of all manufacturers.

199
00:13:10,429 --> 00:13:12,884
And with another function,

200
00:13:12,885 --> 00:13:17,090
I can start calling the members there one by one.

201
00:13:17,091 --> 00:13:22,064
So I can refer to this cell absolutely and

202
00:13:22,065 --> 00:13:26,463
then say give me member number one.

203
00:13:26,464 --> 00:13:29,298
And now I can copy it over and

204
00:13:29,299 --> 00:13:33,990
say now I want member number two from the set.

205
00:13:35,040 --> 00:13:41,600
And once I have them, let me just complete the example by

206
00:13:41,601 --> 00:13:46,000
adding a cube member function, which is just bringing a specific member,

207
00:13:46,001 --> 00:13:50,580
either a measure, or from any other area in the model.

208
00:13:50,581 --> 00:13:54,160
In this case I want a Measure, I want to say that the Measure

209
00:13:54,161 --> 00:13:58,420
is OurSales.

210
00:13:58,421 --> 00:14:03,100
And here I can use a function called CUBEVALUE

211
00:14:03,101 --> 00:14:08,163
that is actually will be using this member here.

212
00:14:11,319 --> 00:14:17,909
But not only this one, so it will use the name of the manufacturer,

213
00:14:17,910 --> 00:14:22,940
and there’s a variable number of parameters.

214
00:14:22,941 --> 00:14:25,780
So, it will use also this element and

215
00:14:25,781 --> 00:14:28,670
it will actually use it this way.

216
00:14:28,671 --> 00:14:36,250
And it will also use the slicer, which is Slicer_Year, let’s go 1.

217
00:14:36,251 --> 00:14:43,010
So once I’ve done, apparently there is no data for this one.

218
00:14:43,011 --> 00:14:47,227
But in this way I can actually

219
00:14:47,228 --> 00:14:51,809
refer to different elements.

220
00:14:51,810 --> 00:14:54,164
And by doing this I can build, and

221
00:14:54,165 --> 00:14:58,973
this will be similar to the ones that are generated automatically.

222
00:14:58,974 --> 00:15:02,316
But now I can make it any shape or form that they want, and

223
00:15:02,317 --> 00:15:04,820
this is the main advantage of this thing.

224
00:15:04,821 --> 00:15:09,510
I could always do something like cut and paste them somewhere else,

225
00:15:09,511 --> 00:15:12,880
which is something it would not be able to do if it was a pivot.

226
00:15:12,881 --> 00:15:15,000
This is possible only because these are functions and

227
00:15:15,001 --> 00:15:18,540
they are easy to move around and I can start building my reports.

228
00:15:18,541 --> 00:15:21,300
And if you remember, when you open the material, you will see

229
00:15:23,200 --> 00:15:27,570
an example called freestyle that is using those very extensively.

230
00:15:27,571 --> 00:15:33,270
So, we saw a few options for visualizations.

231
00:15:33,271 --> 00:15:37,182
We saw how to create a few pivot charts, connect them to one or

232
00:15:37,183 --> 00:15:38,189
more slicers.

233
00:15:38,190 --> 00:15:43,097
And we also saw some of the basics of creating cube functions

234
00:15:43,098 --> 00:15:47,908
either by converting a pivot into functions or formulas,

235
00:15:47,909 --> 00:15:51,730
or by doing manual creation of formulas.

236
00:15:51,731 --> 00:15:53,371
Thank you and see you in the next module.

