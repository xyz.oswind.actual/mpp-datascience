0
00:00:00,383 --> 00:00:03,923
[MUSIC]

1
00:00:03,924 --> 00:00:05,540
Hello, welcome back.

2
00:00:05,541 --> 00:00:08,330
This is module six, and

3
00:00:08,331 --> 00:00:13,050
in this one I’m going to return to the area of queries.

4
00:00:13,051 --> 00:00:18,580
Remember we did some queries from text, we did queries from SQL,

5
00:00:18,581 --> 00:00:22,360
combining with text and then we did some DAX,

6
00:00:22,361 --> 00:00:27,220
and now I’m going back to an example using queries.

7
00:00:27,221 --> 00:00:32,829
In this example, let’s switch to the Excel

8
00:00:32,830 --> 00:00:37,240
screen, and look at this report.

9
00:00:37,241 --> 00:00:41,540
In this report is not our target report.

10
00:00:41,541 --> 00:00:44,190
It’s not that we want to build this report.

11
00:00:44,191 --> 00:00:47,640
The fact is that the scenario here is that

12
00:00:47,641 --> 00:00:50,090
this is what we got as input.

13
00:00:50,091 --> 00:00:51,440
This is the data we have.

14
00:00:52,770 --> 00:00:53,460
And why is that?

15
00:00:53,461 --> 00:00:56,770
This is kind of typical in many organizations.

16
00:00:56,771 --> 00:01:01,440
In some cases, you have access to a very nice data warehouse.

17
00:01:01,441 --> 00:01:03,340
And you’re a business analyst and

18
00:01:03,341 --> 00:01:05,900
you get access to the data warehouse.

19
00:01:05,901 --> 00:01:08,772
And you know the data warehouse and everything’s fine.

20
00:01:08,773 --> 00:01:11,770
In other cases, you don’t have access to the data warehouse, but

21
00:01:11,771 --> 00:01:14,750
you still have access to some

22
00:01:14,751 --> 00:01:18,610
tool that will export the data you need into text files.

23
00:01:18,611 --> 00:01:22,025
And you use these text files, maybe multiple of them, you combine them,

24
00:01:22,026 --> 00:01:25,610
we saw how to create relationships, have multiple tables.

25
00:01:25,611 --> 00:01:26,710
Great.

26
00:01:26,711 --> 00:01:30,520
But in other cases maybe you don’t even have that.

27
00:01:30,521 --> 00:01:34,400
The only thing you have is a reporting system

28
00:01:34,401 --> 00:01:39,540
that creates this report, nice looking headers, repeating headers.

29
00:01:39,541 --> 00:01:40,420
It has data for

30
00:01:40,421 --> 00:01:44,980
each month, you see January through December going across the rows.

31
00:01:46,070 --> 00:01:50,660
And you want to create some analysis of your own based on this.

32
00:01:50,661 --> 00:01:53,390
But you don’t have access to the data behind it.

33
00:01:53,391 --> 00:01:57,340
So what we have to do is to use this as our input.

34
00:01:57,341 --> 00:02:02,560
Looks pretty difficult when you look at it and if you

35
00:02:02,561 --> 00:02:09,500
think about how to do this in Excel, you are up to some real challenges.

36
00:02:09,501 --> 00:02:14,720
And not only it’s challenging to make from this data some useful

37
00:02:14,721 --> 00:02:19,160
format or useful shape that you can use to create your own analyses, but

38
00:02:19,161 --> 00:02:23,420
also you are thinking about the number of times you will have to

39
00:02:23,421 --> 00:02:27,860
repeat the steps of creating from this a useful thing.

40
00:02:27,861 --> 00:02:32,370
So if you are a VBA person you might think of writing some complex

41
00:02:32,371 --> 00:02:36,240
sophisticated macro to do this conversion.

42
00:02:36,241 --> 00:02:40,220
If you’re not you’re just counting your copy paste operations,

43
00:02:40,221 --> 00:02:42,910
how many times you will have to do it, and how many errors.

44
00:02:42,911 --> 00:02:44,570
And maybe thinking of,

45
00:02:44,571 --> 00:02:49,790
can you offload this work to someone else and not have to repeat it.

46
00:02:49,791 --> 00:02:54,780
So let’s see how Excel, 2016 in this case, Power Query

47
00:02:54,781 --> 00:02:59,690
As is embedded in the previous version, can help you to use this and

48
00:02:59,691 --> 00:03:05,800
make it into a useful data to do your analysis.

49
00:03:05,801 --> 00:03:10,250
So I can close this because this is not the file that I’m going to use.

50
00:03:10,251 --> 00:03:13,610
I’m going to actually create an empty file.

51
00:03:13,611 --> 00:03:16,750
And go here and create myself a new query.

52
00:03:16,751 --> 00:03:20,270
The query will be from file and the file is an Excel file.

53
00:03:22,720 --> 00:03:25,740
And the file is called Report.

54
00:03:25,741 --> 00:03:30,280
I click on it, actually the query now reads this data,

55
00:03:30,281 --> 00:03:34,540
shows me there is something called Sheet1.

56
00:03:34,541 --> 00:03:38,510
It reads the data, shows me the preview,

57
00:03:38,511 --> 00:03:42,920
which is not very encouraging because it has all kinds

58
00:03:42,921 --> 00:03:47,450
of things which I know I need to fix before I can use it.

59
00:03:47,451 --> 00:03:54,350
But I click on the edit and I’m hopeful that the query will help me.

60
00:03:54,351 --> 00:03:58,210
Again it tells me that there’s a preview cache and it’s six days old.

61
00:03:58,211 --> 00:03:59,070
I can refresh it.

62
00:03:59,071 --> 00:04:04,600
It doesn’t matter because anyhow the data haven’t changed,

63
00:04:04,601 --> 00:04:08,190
but I can go ahead and refresh the preview.

64
00:04:09,420 --> 00:04:12,090
So now I started to look at this data and

65
00:04:12,091 --> 00:04:14,030
identify what do I need to do.

66
00:04:14,031 --> 00:04:21,140
I see that there are two rows here in the front that definitely

67
00:04:21,141 --> 00:04:26,110
seem to be, I don’t need these rows, so I want to remove them.

68
00:04:26,111 --> 00:04:30,350
I go to the transformation and I see that there is one for removing rows.

69
00:04:30,351 --> 00:04:34,370
I can do both top rows, bottom rows, here in this case top rows.

70
00:04:34,371 --> 00:04:35,380
So the first top row.

71
00:04:35,381 --> 00:04:37,638
How many top rows did I want to remove?

72
00:04:37,639 --> 00:04:39,465
Just two.

73
00:04:39,466 --> 00:04:40,510
Gone.

74
00:04:40,511 --> 00:04:44,370
Now I’m actually at this

75
00:04:44,371 --> 00:04:48,290
row now that it’s going to be useful to use as the headers for

76
00:04:48,291 --> 00:04:51,930
the table that is going to be eventually created from that.

77
00:04:51,931 --> 00:04:53,660
So now I can go and

78
00:04:53,661 --> 00:04:57,539
click on the transformation called Use First Row as Headers.

79
00:04:58,720 --> 00:05:04,580
You see that it kind of promoted the first row, and uses it for headers.

80
00:05:05,670 --> 00:05:10,290
Now, you see that because of the report, if you remember the way it

81
00:05:10,291 --> 00:05:15,170
was, the category is in row one.

82
00:05:15,171 --> 00:05:19,060
Then, this column is empty, and

83
00:05:19,061 --> 00:05:21,480
then there is the total for this category, and so on.

84
00:05:22,910 --> 00:05:27,020
So, what I can do, first of all, every

85
00:05:28,070 --> 00:05:33,360
row that contains the value, the word total, I want to filter it out.

86
00:05:33,361 --> 00:05:35,810
Because the totals, I don’t need them.

87
00:05:35,811 --> 00:05:39,690
So, Text Filters &gt;Does Not Contain.

88
00:05:39,691 --> 00:05:46,540
I want only if the value does not contain the word Total.

89
00:05:46,541 --> 00:05:53,090
So this will take care of all the totals and they will go away.

90
00:05:56,170 --> 00:06:02,060
Oh actually I think I, let me just look again at this.

91
00:06:02,061 --> 00:06:04,910
I can click on this parameters.

92
00:06:04,911 --> 00:06:06,760
Does not show.

93
00:06:09,880 --> 00:06:12,870
Oh, okay, okay, I just, let me.

94
00:06:12,871 --> 00:06:15,890
So, once I realized that there is a mistake here,

95
00:06:15,891 --> 00:06:18,810
that a lot of rows disappeared on me.

96
00:06:18,811 --> 00:06:24,110
And so what I can do is actually I can delete this last step and

97
00:06:24,111 --> 00:06:25,400
get back to where I was.

98
00:06:25,401 --> 00:06:28,690
So, we saw before that we can go back,

99
00:06:28,691 --> 00:06:33,030
insert new rows in the steps, and I can also delete,

100
00:06:33,031 --> 00:06:35,340
this is like I’m doing, but I’m doing here is much simpler.

101
00:06:35,341 --> 00:06:37,980
It’s just deleting the operation that you don’t want.

102
00:06:37,981 --> 00:06:41,130
So let’s do it in a different sequence.

103
00:06:41,131 --> 00:06:43,860
So first I want to actually fill the values for

104
00:06:43,861 --> 00:06:46,750
the categories on all these empty rows.

105
00:06:46,751 --> 00:06:48,020
So that the value for

106
00:06:48,021 --> 00:06:52,790
Mix would be filled down on all the empty rows below it.

107
00:06:52,791 --> 00:06:56,300
So there is a transformation that’s called Fill.

108
00:06:56,301 --> 00:06:59,229
And I will select the column and

109
00:06:59,230 --> 00:07:02,840
this column I want to do a Fill &gt;Down.

110
00:07:05,090 --> 00:07:09,995
So now you see that I have the Mix spread

111
00:07:09,996 --> 00:07:15,207
until the Total, and then in the same way

112
00:07:15,208 --> 00:07:20,290
I have all the others in the field down.

113
00:07:20,291 --> 00:07:24,150
But I still have the totals, the rows that have the totals.

114
00:07:24,151 --> 00:07:29,350
So now, I want to apply Does Not Contain.

115
00:07:29,351 --> 00:07:33,550
So I don’t want the rows that have the word Total in them.

116
00:07:41,880 --> 00:07:42,650
Okay, done.

117
00:07:42,651 --> 00:07:45,720
And you see that this is an iterative process.

118
00:07:45,721 --> 00:07:50,550
You apply something, you see the result, sometimes you may need to go

119
00:07:50,551 --> 00:07:55,430
back and redo it in a different way, there’s no clear answer.

120
00:07:55,431 --> 00:07:58,500
It’s a complex kind of input and

121
00:07:58,501 --> 00:08:02,900
you gradually clean it until it gets to the shape that you need it.

122
00:08:02,901 --> 00:08:07,330
Another thing is that there were headers, repeating headers.

123
00:08:07,331 --> 00:08:11,160
So the first row of headers I’ve used as headers for my report.

124
00:08:11,161 --> 00:08:13,990
But the rest of the headers, I need to get rid of.

125
00:08:13,991 --> 00:08:16,820
So as an example of this header is this row number nine that has

126
00:08:16,821 --> 00:08:18,340
the word Category in it.

127
00:08:18,341 --> 00:08:23,700
So again I can say I want to text filter where everything,

128
00:08:23,701 --> 00:08:29,205
I just want the rows that do not contain the word Category.

129
00:08:31,645 --> 00:08:36,745
So we are gradually getting rid of all the rows we don’t need.

130
00:08:36,746 --> 00:08:39,545
So now it seems that we got rid of all the rows.

131
00:08:39,546 --> 00:08:42,135
But we have a major obstacle here.

132
00:08:42,135 --> 00:08:47,455
We have columns for each month, from January to December.

133
00:08:47,456 --> 00:08:51,770
So 12 columns with values.

134
00:08:51,771 --> 00:08:55,910
Actually there’s an extra column here that is empty so

135
00:08:55,911 --> 00:08:57,362
I’m going to remove it.

136
00:08:57,363 --> 00:08:59,790
There is a total for the year which I also don’t need and

137
00:08:59,791 --> 00:09:01,700
I’m going to remove it.

138
00:09:01,701 --> 00:09:06,817
So now I’m left with the Category, the Manufacturer and the values for

139
00:09:06,818 --> 00:09:12,100
each combination of category and manufacturer for each month.

140
00:09:12,101 --> 00:09:15,030
But the fact that I have a column for

141
00:09:15,031 --> 00:09:19,000
each month is nice for the report, but it’s really not good for

142
00:09:19,001 --> 00:09:23,300
me to use to creating pivot tables in analysis.

143
00:09:23,301 --> 00:09:27,410
What I need is actually a column that will contain the name

144
00:09:27,411 --> 00:09:30,510
of the month, and another column that will contain the value.

145
00:09:30,511 --> 00:09:31,670
So I need to flatten it.

146
00:09:31,671 --> 00:09:33,840
Instead of having multiple columns for each month,

147
00:09:33,841 --> 00:09:37,470
I want each one of these columns to become a row.

148
00:09:37,471 --> 00:09:40,140
So, let’s see how can I do it.

149
00:09:40,141 --> 00:09:46,210
So, I can select from January to December, by Shift + click.

150
00:09:46,211 --> 00:09:49,060
I selected all these columns.

151
00:09:49,061 --> 00:09:53,428
And, I can apply a transformation that is called Unpivot.

152
00:09:53,429 --> 00:10:01,920
The Unpivot, and one simple click created for me two columns.

153
00:10:01,921 --> 00:10:04,882
So everything that used to be column now becomes row, so for

154
00:10:04,883 --> 00:10:08,319
each combination of category and manufacturer, now we have a row for

155
00:10:08,320 --> 00:10:10,290
January, February, March and so on.

156
00:10:10,291 --> 00:10:14,803
And it starts again for the other combination of category and

157
00:10:14,804 --> 00:10:16,050
manufacturer.

158
00:10:16,051 --> 00:10:19,441
Now I can rename the column,

159
00:10:19,442 --> 00:10:24,525
instead of Attributes I can call it Month,

160
00:10:24,526 --> 00:10:29,480
and I can also rename this one to be Sales.

161
00:10:31,140 --> 00:10:33,970
And now actually I’m pretty much done.

162
00:10:33,971 --> 00:10:37,820
I can go to Home &gt;Close &amp;amp; Load.

163
00:10:37,821 --> 00:10:40,780
Let’s do an example of loading this into Excel.

164
00:10:40,781 --> 00:10:43,680
So I do want a table, let’s say, and I don’t want it in the model,

165
00:10:43,681 --> 00:10:45,910
I just want a simple table in Excel.

166
00:10:45,911 --> 00:10:49,790
It’s actually also small, I don’t have a problem of getting over

167
00:10:49,791 --> 00:10:52,710
a million, not by a long shot because this is just a few.

168
00:10:54,410 --> 00:10:56,152
And here it is.

169
00:10:56,153 --> 00:11:01,860
This data, actually the total number is 437 rows,

170
00:11:01,861 --> 00:11:05,850
is the end result of the report that we showed before.

171
00:11:05,851 --> 00:11:10,650
So if I will open once again the report.

172
00:11:12,060 --> 00:11:15,840
We started from here and we got to here, and

173
00:11:15,841 --> 00:11:20,512
now from here, I can actually create a pivot table.

174
00:11:20,513 --> 00:11:24,875
So it’s a simple table, it has rows and columns, it’s ready to go,

175
00:11:24,876 --> 00:11:28,560
it’s formatted correctly and I can use it for my reporting.

176
00:11:28,561 --> 00:11:33,260
So, to summarize what we did in this module, we

177
00:11:34,870 --> 00:11:41,870
didn’t have a proper source of data from a database or from CSV file.

178
00:11:41,871 --> 00:11:43,530
We just had a report and

179
00:11:43,531 --> 00:11:47,820
inside in Excel, our reporting system gave us a report in Excel.

180
00:11:47,821 --> 00:11:51,893
And we started from there using a query, cleaned it, shaped it and

181
00:11:51,894 --> 00:11:56,210
actually created this data set that is useful to do further analysis.

