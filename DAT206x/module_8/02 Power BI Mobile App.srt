0
00:00:00,001 --> 00:00:04,167
[MUSIC]

1
00:00:04,168 --> 00:00:05,200
All right.

2
00:00:05,201 --> 00:00:08,910
Welcome back, this is the last part of this series.

3
00:00:08,911 --> 00:00:11,650
And here, I wanna show you,

4
00:00:11,651 --> 00:00:16,040
as a continuation to what I showed you about Power BI, that once I have

5
00:00:17,280 --> 00:00:22,200
uploaded my Excel file, or if I used another tool to build a model, but

6
00:00:22,201 --> 00:00:26,420
I have a model already in PowerBI, I’ve built my dashboard and reports.

7
00:00:26,421 --> 00:00:27,650
I can use them,

8
00:00:27,651 --> 00:00:32,400
not only from the browser, but also from mobile devices.

9
00:00:32,401 --> 00:00:37,050
So, I brought two with me, and I’m going to use them to show you

10
00:00:37,051 --> 00:00:42,640
the experience from, first, an iPad, and then an iPhone.

11
00:00:43,650 --> 00:00:48,680
So, let us switch to seeing my,

12
00:00:48,681 --> 00:00:52,010
I have this contraption here,

13
00:00:52,011 --> 00:00:56,890
the cable connecting directly to video and we are live from the iPad.

14
00:00:56,891 --> 00:01:01,160
So I can click on, I see the list of my dashboard, I already logged in.

15
00:01:01,161 --> 00:01:07,230
I am authenticated against the same tenant with my Microsoft Account and

16
00:01:07,231 --> 00:01:10,970
I click on one of the dashboards. The dashboard that you saw before.

17
00:01:10,971 --> 00:01:15,620
So now we see the same thing, we see that actually the additional one,

18
00:01:15,621 --> 00:01:19,430
we actually see two similar parts.

19
00:01:19,431 --> 00:01:23,510
Which is good, because we see that actually this is the way

20
00:01:23,511 --> 00:01:29,080
the dashboard was edited by me, just a few minutes ago when

21
00:01:29,081 --> 00:01:33,150
I recorded the previous model, when I showed you Power BI from the browser.

22
00:01:35,500 --> 00:01:42,111
When I click on any one of those, I can go into this single chart or

23
00:01:42,112 --> 00:01:46,041
single tile experience for example.

24
00:01:46,042 --> 00:01:49,977
For example, you’ll see, I’m with my finger I’m moving between

25
00:01:49,978 --> 00:01:53,981
the elements on top that every time I’m parking on one of them it shows

26
00:01:53,982 --> 00:01:55,514
the details for this one.

27
00:01:55,515 --> 00:01:57,663
But I can also I can annotate and

28
00:01:57,664 --> 00:02:02,544
start creating some comments I can share this with some other people,

29
00:02:02,545 --> 00:02:05,970
but I can also open the reports behind it.

30
00:02:05,971 --> 00:02:10,609
So it’s a similar experience, but

31
00:02:10,610 --> 00:02:16,960
just like- oh, interesting let’s try again.

32
00:02:25,652 --> 00:02:30,295
Again, I selected one of the tiles and I clicked under open the report.

33
00:02:30,296 --> 00:02:31,965
Yeah, here it is, so

34
00:02:31,966 --> 00:02:35,745
now I see the report with all the different visualizations.

35
00:02:35,746 --> 00:02:39,665
And you see that actually this was actually a map, but

36
00:02:39,666 --> 00:02:46,545
now last time I touched it, I made it into a column chart.

37
00:02:46,546 --> 00:02:48,455
Which is again, it could be a map, believe me,

38
00:02:48,456 --> 00:02:51,855
this could be visualized as a map in the same way.

39
00:02:51,856 --> 00:02:56,840
But again, when I touch it It actually, filters all others.

40
00:02:56,841 --> 00:02:58,980
And touch it again it goes back.

41
00:02:58,981 --> 00:03:00,810
I can also touch this one and

42
00:03:00,811 --> 00:03:03,250
in the same way it will affect all the others.

43
00:03:03,251 --> 00:03:06,300
So I have this, report effect and

44
00:03:06,301 --> 00:03:12,010
I can also, uncheck one of the years and select another year.

45
00:03:12,011 --> 00:03:16,300
So all the interactivity on the report that I had from the browser

46
00:03:16,301 --> 00:03:20,360
is also available for me here, on this device.

47
00:03:20,361 --> 00:03:21,610
I’m using here an iPad,

48
00:03:21,611 --> 00:03:26,570
similar experience will be from an Android-based tablet or

49
00:03:26,571 --> 00:03:33,050
a Windows-based tablet with the same kind of functionality.

50
00:03:33,051 --> 00:03:38,200
But when I say the same, it’s actually the same, but adjusted to

51
00:03:38,201 --> 00:03:44,300
take advantage of the capabilities of each one platform individually.

52
00:03:44,301 --> 00:03:48,460
Now let me switch to my little brother here, so

53
00:03:48,461 --> 00:03:53,535
we have a smaller form factor which is an iPhone 6.

54
00:03:53,536 --> 00:03:58,055
So with the same cable I am just connecting to the iPhone 6,

55
00:03:58,056 --> 00:03:59,465
holding this in my hand and

56
00:03:59,466 --> 00:04:04,415
now let’s see what the experience on the iPhone looks.

57
00:04:04,416 --> 00:04:10,125
You see here, I see, again, I already logged in, I authenticated.

58
00:04:10,126 --> 00:04:13,715
And I see the different dashboards are available for

59
00:04:13,716 --> 00:04:17,690
me, either that I created them or were shared with me.

60
00:04:17,690 --> 00:04:20,410
By their corresponding authors.

61
00:04:20,411 --> 00:04:22,500
Now because of the form factor,

62
00:04:22,501 --> 00:04:29,010
we cannot really show all the visualizations as a grid of tiles,

63
00:04:29,011 --> 00:04:34,630
we show them just as one dimensional list of tiles going

64
00:04:34,631 --> 00:04:38,190
left to right, row by row.

65
00:04:38,191 --> 00:04:47,140
But I see all the visualizations that were part of this dashboard.

66
00:04:47,141 --> 00:04:53,930
Now when I choose one of the tiles, I can use

67
00:04:53,931 --> 00:04:59,740
this, tool here to move this line, to move between the elements.

68
00:04:59,741 --> 00:05:04,060
And to see more details, what I don’t have from the phone,

69
00:05:04,061 --> 00:05:10,020
because it’s not practical is a way to dive into the report.

70
00:05:10,021 --> 00:05:15,110
So in the iPhone, I am left just at the level of

71
00:05:15,111 --> 00:05:18,640
looking at the different elements of the dashboard and

72
00:05:18,641 --> 00:05:22,610
when I click on one of them I can see it in more detail but

73
00:05:22,611 --> 00:05:26,620
I cannot go and see the report experience.

74
00:05:26,621 --> 00:05:32,890
Power BI is rapidly evolving and the team behind

75
00:05:32,891 --> 00:05:38,380
Power BI is adding new features at a very rapid cadence.

76
00:05:38,381 --> 00:05:42,310
Every month you will see new functionality coming, so

77
00:05:42,311 --> 00:05:49,900
also the mobile applications will update and will show

78
00:05:49,901 --> 00:05:55,870
you different, more functionality and more visualizations and

79
00:05:55,871 --> 00:06:01,430
more gestures that you could use against visualizations.

80
00:06:01,431 --> 00:06:04,180
But I hope you got more

81
00:06:04,181 --> 00:06:07,420
complete understanding of what the offering of Power BI.

82
00:06:07,421 --> 00:06:11,570
You can upload data models either with Excel or

83
00:06:11,571 --> 00:06:15,060
built by other tools like Power BI Desktop.

84
00:06:15,061 --> 00:06:20,310
Once you have a model you can start creating dashboards- And reports.

85
00:06:20,311 --> 00:06:23,930
You can also- Interact with them with this

86
00:06:25,950 --> 00:06:29,020
natural English questions that we saw.

87
00:06:29,021 --> 00:06:34,540
And then finally you can use a variety of phones and tablets,

88
00:06:34,541 --> 00:06:42,010
Android, Windows, or iOS devices to interact with the reports and

89
00:06:42,011 --> 00:06:47,120
the dashboard, share them with your colleagues and get the full benefit

90
00:06:47,121 --> 00:06:52,720
of having a cloud-based solution available on a variety of devices.

91
00:06:52,721 --> 00:06:57,580
So this concludes our series and I hope that you are now

92
00:06:57,581 --> 00:07:00,670
enthused enough and you are interested to learn more.

93
00:07:00,671 --> 00:07:06,070
And it will provide you with more materials to Learn from and,

94
00:07:06,071 --> 00:07:07,800
would like also to hear from you.

95
00:07:07,801 --> 00:07:08,351
Thank you.

