0
00:00:00,396 --> 00:00:04,176
[MUSIC]

1
00:00:04,177 --> 00:00:05,030
Welcome back.

2
00:00:05,031 --> 00:00:09,940
This is Module 8, and in this module I will introduce a totally

3
00:00:09,941 --> 00:00:13,730
new environment which is called Power BI.

4
00:00:13,731 --> 00:00:19,340
Power BI is a service that we provide as Microsoft.

5
00:00:19,341 --> 00:00:22,850
It’s a cloud based service that can

6
00:00:23,960 --> 00:00:29,010
provide the organization with a place to store dashboards and

7
00:00:29,011 --> 00:00:32,580
reports, all kinds of visualizations.

8
00:00:32,581 --> 00:00:38,470
And the reason I’m going to show it is that it’s actually very relevant

9
00:00:38,471 --> 00:00:42,120
to an Excel user because you can actually create the contents.

10
00:00:42,121 --> 00:00:46,470
One of the options to create the contents will be using Excel.

11
00:00:46,471 --> 00:00:50,690
And even if you use one of the other options to create the content,

12
00:00:50,691 --> 00:00:53,730
everything you have learned so far in this series

13
00:00:53,731 --> 00:00:58,360
of modules will be relevant and will help you building this content

14
00:00:58,361 --> 00:01:01,520
even if you are not using Excel because the same data model.

15
00:01:01,521 --> 00:01:05,860
The same technology for queries, the same DAX expressions,

16
00:01:05,861 --> 00:01:10,600
are actually the way you build the content for

17
00:01:10,601 --> 00:01:14,340
this service, which is Power BI.

18
00:01:14,341 --> 00:01:18,090
Power BI comes as a freemium.

19
00:01:18,091 --> 00:01:19,438
So there’s a free model.

20
00:01:19,439 --> 00:01:21,130
You can start using it right away.

21
00:01:21,131 --> 00:01:24,370
In a few seconds, you can just sign up with your company email.

22
00:01:25,580 --> 00:01:29,060
Get an account and start uploading content and

23
00:01:29,061 --> 00:01:32,020
connecting to a variety of data sources.

24
00:01:32,021 --> 00:01:37,170
But there is, you can go and pay for

25
00:01:37,171 --> 00:01:41,200
a paying subscription and then you get additional capabilities.

26
00:01:41,201 --> 00:01:45,952
More storage to store your models and so on.

27
00:01:45,953 --> 00:01:51,170
Now, just before I show you anything I want to say that

28
00:01:51,171 --> 00:01:56,520
I’m going to show just a very narrow aspect of the capabilities of

29
00:01:56,521 --> 00:02:01,080
this service, because I’m actually showing the Excel part in it, and

30
00:02:01,081 --> 00:02:07,550
how to use Excel to upload into the server, or the service.

31
00:02:07,551 --> 00:02:11,960
But this is not the only way, there are other ways to do that.

32
00:02:11,961 --> 00:02:15,000
And there is a lot of other functionality that you should learn,

33
00:02:15,001 --> 00:02:19,500
if you want to learn more about the service, and we will point you to training,

34
00:02:19,501 --> 00:02:21,440
specific training just about Power BI.

35
00:02:22,630 --> 00:02:27,680
So I am here in the browser, have nothing in the browser.

36
00:02:27,681 --> 00:02:32,100
So, and I’m writing msit.powerbi.com

37
00:02:32,101 --> 00:02:39,420
MS IT is Microsoft IT so I’m getting into our tenant in Power BI.

38
00:02:39,421 --> 00:02:43,100
And right away I’m presented with a dashboard.

39
00:02:43,101 --> 00:02:49,990
That is actually showing data coming from which should be familiar

40
00:02:49,991 --> 00:02:54,940
because it’s actually using the same model that I created with Excel.

41
00:02:54,941 --> 00:02:56,520
So what I did.

42
00:02:56,521 --> 00:03:02,860
I uploaded to this service the Excel in a way that actually the service

43
00:03:02,861 --> 00:03:06,860
is now using the data model with its connection, with its expressions,

44
00:03:06,861 --> 00:03:09,880
DAX expressions, everything I did but not the visualization.

45
00:03:09,881 --> 00:03:14,500
It just, and the way I uploaded it right now,

46
00:03:14,501 --> 00:03:17,080
I am just using it as the data model.

47
00:03:17,081 --> 00:03:21,036
And, on top of that, I was able to build two entities.

48
00:03:21,037 --> 00:03:22,600
One is dashboards.

49
00:03:22,601 --> 00:03:23,808
And this is a dashboard,

50
00:03:23,809 --> 00:03:29,140
it’s made of those elements which are called tiles.

51
00:03:29,141 --> 00:03:32,930
And, you can see, right away, that the variety of visualizations is

52
00:03:32,931 --> 00:03:36,630
beyond what you can do with Excel for example showing

53
00:03:38,330 --> 00:03:44,340
data on a map or using this tree map where it shows the area for

54
00:03:44,341 --> 00:03:48,890
each one of the manufacturers which represent their relative size

55
00:03:48,891 --> 00:03:50,320
in sales and so on.

56
00:03:50,321 --> 00:03:52,890
So this is the dashboard.

57
00:03:52,891 --> 00:03:54,580
And once I click.

58
00:03:54,581 --> 00:03:58,550
Now, the dashboard can contain tiles coming from

59
00:03:58,551 --> 00:04:01,520
different reports connected to different data sets.

60
00:04:01,521 --> 00:04:03,940
It can have under the list of data sets  61 00:04:03,941 --&gt; 00:04:07,700 it’s really a large one if I want to add a new data set.

61
00:04:07,701 --> 00:04:14,366
Just to show you lists of things that you can draw data from.

62
00:04:14,367 --> 00:04:17,190
It’s a very, very long list.

63
00:04:17,190 --> 00:04:20,040
So, I’m not going to go into any of that.

64
00:04:20,041 --> 00:04:25,380
Actually the source I was using is a model in Excel, but

65
00:04:25,381 --> 00:04:30,890
if you want to you can add, and if you go back to my area and

66
00:04:30,891 --> 00:04:33,850
to the dashboard, so, I was saying that the dashboard can contain tiles

67
00:04:33,851 --> 00:04:37,630
coming from and connecting to any one of these sources.

68
00:04:37,631 --> 00:04:40,883
Once I click on a tile, by default,

69
00:04:40,884 --> 00:04:46,527
it takes me to the report from which this tile was coming from.

70
00:04:46,528 --> 00:04:49,662
In the report, again, here I’m just browsing it but

71
00:04:49,663 --> 00:04:53,226
in a moment I will be able to, I will show you how can I edit it.

72
00:04:53,227 --> 00:04:58,126
Now for example here I have a slicer that I can

73
00:04:58,127 --> 00:05:01,173
select a different year and

74
00:05:01,174 --> 00:05:06,619
all those other elements respond to the slicer.

75
00:05:06,620 --> 00:05:11,251
Another element here or another capability is to click on one chart

76
00:05:11,252 --> 00:05:16,230
and by clicking one chart, I see the effect of this chart on the others.

77
00:05:16,231 --> 00:05:21,300
So, any chart can be used as a slicer, against all the others.

78
00:05:21,301 --> 00:05:25,620
This is called cross-highlighting.

79
00:05:25,621 --> 00:05:29,930
If I want to, I can go into, because I am the owner of that,

80
00:05:29,931 --> 00:05:34,200
I can go to an editing experience, and start editing this

81
00:05:35,820 --> 00:05:40,190
report and you can see here the list on the right side,

82
00:05:40,191 --> 00:05:43,840
the variety of the different visualization types,

83
00:05:43,841 --> 00:05:48,460
including this kind of map in which each element is a circle.

84
00:05:48,461 --> 00:05:52,780
Representing the size of one of the measures, but there is also another.

85
00:05:52,781 --> 00:05:57,014
So I can go here and change its visualization to something else, for

86
00:05:57,015 --> 00:05:58,943
example, to this kind of map,

87
00:05:58,944 --> 00:06:02,509
in which the area is actually colored and the intensity of

88
00:06:02,510 --> 00:06:06,620
the color represents again the measure that I want to visualize.

89
00:06:07,780 --> 00:06:11,690
And I can switch this to just a table with the numbers, and

90
00:06:11,691 --> 00:06:16,480
so on to any one of the other visualizations.

91
00:06:16,481 --> 00:06:21,908
So now you can see even maybe with more ease when I click on the USA,

92
00:06:21,909 --> 00:06:26,180
you can see how USA affects all of the other charts.

93
00:06:26,181 --> 00:06:32,180
This is a very important property of this environment,

94
00:06:32,181 --> 00:06:34,830
which is based on a technology called power view.

95
00:06:36,330 --> 00:06:43,120
So, this is the chart, the report.

96
00:06:43,121 --> 00:06:45,190
From the report, I could now,

97
00:06:45,191 --> 00:06:50,760
all this actually elements here are already part of the dashboard.

98
00:06:50,761 --> 00:06:55,472
But just to show you when I’m looking at one of

99
00:06:55,473 --> 00:07:00,190
these elements in the report I can pin it and

100
00:07:00,191 --> 00:07:04,360
it will go and pin to the dashboard which is the current dashboard.

101
00:07:04,361 --> 00:07:08,270
So if I switch back to the dashboard.

102
00:07:08,271 --> 00:07:11,699
Okay, it asks me if I want to save the changes that I did and

103
00:07:11,700 --> 00:07:12,870
I said yes.

104
00:07:12,871 --> 00:07:18,590
But now I will see this new element added to the dashboard.

105
00:07:18,591 --> 00:07:23,630
So every time, now in the dashboard I can edit some properties of it,

106
00:07:23,631 --> 00:07:27,463
I can give it a title and,

107
00:07:27,464 --> 00:07:31,190
but now this becomes part of the dashboard, and again,

108
00:07:31,191 --> 00:07:38,300
if I click on it, it will go back to the report that it came from.

109
00:07:38,301 --> 00:07:45,650
Now I’m using this from the dashboard, sorry, from the browser.

110
00:07:45,651 --> 00:07:50,284
We’ll see in the next part of this module we’ll see that

111
00:07:50,285 --> 00:07:53,260
this is not the only option to use.

112
00:07:53,261 --> 00:07:58,120
It’s not just a browser based tool.

113
00:07:58,121 --> 00:08:00,464
Now in order to create the content,

114
00:08:00,465 --> 00:08:03,670
as I said this content I created with the Excel but

115
00:08:03,671 --> 00:08:08,597
there’s also a companion application called Power PI Desktop that you can

116
00:08:08,598 --> 00:08:12,817
use it instead of Excel to create the content, to import the data

117
00:08:12,818 --> 00:08:17,353
using queries to create the DAX expressions, calculate columns,

118
00:08:17,354 --> 00:08:21,225
calculate measures, you can do it with Power PI desktop.

119
00:08:21,226 --> 00:08:25,380
And there are advantages to do that in this way.

120
00:08:25,381 --> 00:08:28,280
There are also advantages of doing this in Excel.

121
00:08:28,281 --> 00:08:33,165
Now, I want show you a new capability of this that was,

122
00:08:33,166 --> 00:08:36,225
the capability that was added recently before this went

123
00:08:36,226 --> 00:08:39,625
into general availability not long ago.

124
00:08:39,626 --> 00:08:41,025
And this is about Excel.

125
00:08:41,025 --> 00:08:42,385
It’s again, relevant to Excel.

126
00:08:42,385 --> 00:08:45,315
I’m going to the Get Data, and

127
00:08:45,316 --> 00:08:48,935
I’m saying that I want to bring data from a file.

128
00:08:48,936 --> 00:08:52,449
And the file is residing on One Drive for Business.

129
00:08:53,690 --> 00:08:57,700
So it’s now connecting to, it knows I am authenticated.

130
00:08:57,701 --> 00:09:03,860
It knows where my files are, and I have a folder here called For

131
00:09:03,861 --> 00:09:09,480
Excel Training, in which I’ve stored some material, and

132
00:09:09,481 --> 00:09:14,250
it’s actually a version of the same file, but I designate it as

133
00:09:14,251 --> 00:09:18,618
to be used as Excel, and we’ll see in a moment what it does.

134
00:09:18,619 --> 00:09:22,800
First, let it start uploading it to the service.

135
00:09:22,801 --> 00:09:25,030
So, once I decided I want to use this one,

136
00:09:25,031 --> 00:09:28,110
it actually tells me, asks me one of two options.

137
00:09:28,111 --> 00:09:29,400
What do I want to do?

138
00:09:29,401 --> 00:09:32,750
Do I want to import the Excel data into the Power BI service, and

139
00:09:32,751 --> 00:09:34,190
this is what I did before.

140
00:09:34,191 --> 00:09:39,218
To import means, use the model in this Excel, and

141
00:09:39,219 --> 00:09:44,366
then start to build reports and dashboards on top or

142
00:09:44,367 --> 00:09:49,511
I just want to connect to it as it sits in OneDrive for

143
00:09:49,512 --> 00:09:54,181
business and here I’m selecting the option to

144
00:09:54,182 --> 00:09:59,000
actually connect and it’s started to do that.

145
00:09:59,001 --> 00:10:01,288
It’s been added as a report, but

146
00:10:01,289 --> 00:10:06,310
the behavior of clicking on this report is completely different.

147
00:10:06,311 --> 00:10:09,830
When I click on it, it opens this dialogue.

148
00:10:09,831 --> 00:10:12,235
It asks me, what do you want to do with it and

149
00:10:12,236 --> 00:10:15,915
you can see that there are options for example to schedule a refresh so

150
00:10:15,916 --> 00:10:20,135
I can actually schedule a refresh that will run everyday

151
00:10:20,136 --> 00:10:24,055
at a specific hour and actually pull data from the data sources and so

152
00:10:24,056 --> 00:10:28,115
on but what I want to do with it now is viewing it.

153
00:10:28,116 --> 00:10:32,065
Now what it does it opens the Excel file and

154
00:10:32,066 --> 00:10:35,720
I will be interacting with it in the similar way that

155
00:10:35,721 --> 00:10:38,510
I was interacting with it when I was in Excel.

156
00:10:38,511 --> 00:10:41,340
So now this should look familiar to you,

157
00:10:41,341 --> 00:10:47,460
we have been looking at this report in previous modules.

158
00:10:47,461 --> 00:10:51,720
And you can see that I can switch between the different ones.

159
00:10:51,721 --> 00:10:53,200
I can click on the slicers,

160
00:10:53,201 --> 00:10:55,760
if you remember I have the two ways to see it.

161
00:10:55,761 --> 00:10:59,240
I can go back here.

162
00:10:59,241 --> 00:11:00,280
Select another year.

163
00:11:00,281 --> 00:11:03,630
So all the slicers, all the functionalities available to me,

164
00:11:03,631 --> 00:11:08,800
only that now I am actually opening this Excel from the browser,

165
00:11:10,700 --> 00:11:13,110
and using the technology that we’ve had for

166
00:11:13,111 --> 00:11:17,110
quite some time called Excel Services or Excel Online.

167
00:11:17,111 --> 00:11:19,740
And it’s only that

168
00:11:19,741 --> 00:11:23,540
it’s now integrated into the experience of Power BI.

169
00:11:23,541 --> 00:11:28,743
So again, I can upload Excel files in two different ways,

170
00:11:28,744 --> 00:11:31,509
one is to be used as the model,

171
00:11:31,510 --> 00:11:35,737
and to build dashboards and reports from it.

172
00:11:35,738 --> 00:11:40,289
And one is to actually upload it as a report with

173
00:11:40,290 --> 00:11:43,340
the visualizations of Excel.

174
00:11:43,341 --> 00:11:44,757
Here it’s pivot chart.

175
00:11:44,758 --> 00:11:48,181
It could be also a pivot with all its functionality and

176
00:11:48,182 --> 00:11:53,130
all its capabilities to do a really very advanced analytics.

177
00:11:53,131 --> 00:11:56,660
It’s all available for you in this way in the browser.

178
00:11:58,510 --> 00:12:00,320
So you can choose between the two.

179
00:12:00,321 --> 00:12:03,430
Once you are in the Excel client by the way.

180
00:12:03,431 --> 00:12:08,810
In the Excel client if I switch back there is a publish option that

181
00:12:08,811 --> 00:12:13,279
can actually push it directly from Excel to the Power BI environment.

182
00:12:15,880 --> 00:12:22,290
So, to summarize, I’ve shown you, oh, I need to show you

183
00:12:22,291 --> 00:12:26,370
one more thing in the Power BI, because it’s really the highlight of

184
00:12:26,371 --> 00:12:31,960
what we can do with this model once it’s there.

185
00:12:31,961 --> 00:12:34,680
It’s something called, you see here,

186
00:12:34,681 --> 00:12:39,120
there is an area, it says ask the questions about the data.

187
00:12:39,121 --> 00:12:45,403
So if I start saying our share which is a.

188
00:12:45,404 --> 00:12:48,870
Am I in the right?

189
00:12:51,909 --> 00:12:52,529
Yeah.

190
00:12:52,530 --> 00:12:56,850
Our share and by year.

191
00:13:00,149 --> 00:13:02,750
And country.

192
00:13:02,751 --> 00:13:08,260
So I can start writing this English statement and

193
00:13:08,261 --> 00:13:11,850
it will create visualizations for me on the fly.

194
00:13:11,851 --> 00:13:14,800
Knowing the metadata.

195
00:13:14,801 --> 00:13:17,660
The names for the measures.

196
00:13:17,661 --> 00:13:24,320
The names for and for the different dimensions,

197
00:13:24,321 --> 00:13:30,270
different fields and it’s a very powerful way for business users

198
00:13:30,271 --> 00:13:34,660
to start asking their questions and getting things beyond what was

199
00:13:34,661 --> 00:13:38,960
presented to them by the author of the dashboard or the report.

200
00:13:38,961 --> 00:13:40,900
And this is something that you can play with,

201
00:13:40,901 --> 00:13:43,310
you can just open an account right away,

202
00:13:43,311 --> 00:13:47,280
get some of the sample dashboards without even uploading anything.

203
00:13:47,281 --> 00:13:51,105
And start playing with these technologies and see for

204
00:13:51,106 --> 00:13:56,297
yourselves how powerful this is in creating visualizations on the fly.

205
00:13:56,298 --> 00:14:00,627
Okay, we have just one more to go, which we’ll keep as a surprise for

206
00:14:00,628 --> 00:14:01,756
the next module.

207
00:14:01,757 --> 00:14:03,216
See you there.

